# PicPay iOS [![Build Status](https://app.bitrise.io/app/2ed9bdb2028425f2/status.svg?token=KT8e6vsh58myJO1QKPb8uw&branch=dev)](https://app.bitrise.io/app/2ed9bdb2028425f2) [![CHANGELOG_PF](https://img.shields.io/badge/CHANGELOG_PF-green)](https://gist.github.com/9a70e5d97aeb61c2a9d6e42a5f556954) [![CHANGELOG_PJ](https://img.shields.io/badge/CHANGELOG_PJ-black)](https://gist.github.com/cb43057172f1aa4f62241de3b33a3e01) [![codecov](https://codecov.io/gh/PicPay/picpay-ios/branch/dev/graph/badge.svg?token=wwAyamc7ZM)](https://codecov.io/gh/PicPay/picpay-ios)

![](https://i.imgur.com/MMUbJuh.png)

**PicPay** is an app that came to change the way we think about money, making it easy to pay and receive money using only one app.

![](https://i.imgur.com/hY9C3pP.png)

## Contents

1. [Getting Started](#Getting-Started)
2. [Running the Project](#Running-the-Project)
   2.1 [Prod and Beta links](#Prod-and-Beta-links)
3. [Most Used Tools](#Most-Used-Tools)
4. [Project Structure](#Project-Structure)
   5.1 [Modules](#Project-Structure)
5. [Git Patterns](#Git-Patterns)
   5.1 [Feature name Pattern](#Feature-name-Pattern)
   5.2 [Pull Request name Pattern](#Pull-Request-name-Pattern)
6. [CHANGELOG](#CHANGELOG)
7. [UI Testing](#UI-Testing)

## Getting Started

First of all, welcome to the **PicPay iOS Team**!

Some important tools are used on the PicPay iOS project and right after you clone the repository to your machine, open the terminal and run the following commands:

### Ruby

There are some environment dependencies that need Ruby to run. In order to use the same Ruby version it is recommended to install [rbenv](https://github.com/rbenv/rbenv) or another Ruby version manager.

```sh
brew install rbenv
```

Add these lines at the end of `~/.zshrc`

```shell
### rbenv
##################################################

# Initialize rbenv
if which rbenv > /dev/null; then
  eval "$(rbenv init -)";

  export PATH="$HOME/.rbenv/shims:$PATH"
fi

# Source rbenv completions
if [[ -s "~/.rbenv/completions/rbenv.zsh" ]]; then
  source "~/.rbenv/completions/rbenv.zsh"
fi
```

(If you're using bash instead of zsh. Add these lines at the end of `.bashrc`, changing references of `zsh` to `bash`)

After that execute, `source ~/.zshrc` or close and open the Terminal.

Install the Ruby version used on the project:

```sh
rbenv install `cat .ruby-version`
```

After that, run:

```sh
make setuptools
```

This script will install the necessary dependencies to compile and run the project.

At the end of this install, you will be able to choose to install git hooks, to automatically generate the project and install all the dependencies after each successfull checkout or merge.

You can see a brief explanation of each one of the dependencies:

#### 1. Cocoapods

The most used dependency manager on iOS projects, Cocoapods has over 67 thousand libraries written in Swift and Objective-C and is used to scale a iOS project.

#### 2. XCodeGen

XcodeGen is a command line tool written in Swift that generates your Xcode project using your folder structure and a project spec.

#### 3. SwiftGen

SwiftGen is a tool to auto-generate Swift code for resources of your projects, to make them type-safe to use.

#### 4. SwiftLint

SwiftLint is a "linter", a common use tool to enforce Swift code style and patterns, warning the programmer about bad behaviors and good practices.

#### 5. Bundler:

Bundler provides a consistent environment for Ruby projects by tracking and installing the exact gems and versions that are needed.

#### 6. Homebrew

Homebrew is a installation manager, very useful to install all the packages separatly.

### To Generate Project, Workspace and install dependencies

To manually generate the .xcodeproj files and install the project dependencies, run on the terminal:

`make generate`

This will perform the following commands:

1. `xcodegen`
2. `xcodegen` on each module folder
3. `bundle exec pod install`

## Running the Project

To run the project, some configurations are necessary. It's good to remember that the bundler will always get the most updated versions at root enforcing an updated execution. To download the certificates and provisioning profiles, just check the `Automatically manage signing` option on Xcode.

### Prod and Beta links

1. [Production Beta](https://testflight.apple.com/join/oZn3jhJU)
2. [Homolog Beta](https://testflight.apple.com/join/8kmsh5dA)

## Most Used Tools

#### 1. XCode Developer Tools

The XCode Developer tools are essencial for every developer and is responsable for most of the command line resources we use daily, like the Git package.

Install it by running on the terminal:

`xcode-select --install`

**OBS:** You can check if you already have this tool installed by running:

`xcode-select --version`

#### 2. Fastlane

To manage all certificates and provisioning profiles, we use Fastlane to do this automatically.

This is installed by the command `make setuptools`

#### 3. Proxyman

We use Proxyman to check backend requests and mock the desired responses. To install it, just go over the [Proxyman](https://proxyman.io) website and follow the steps.

## Project Structure

### Architecture template

At PicPay, we use the VIP-C pattern, where the Coordinator is more like the VIPER Coordinator.
This project defines a template to help with the creation of the files representing the `Interactor`, `Presenter`, `Coordinator`, `Service`, `Factory` and `ViewController`

Here is a visual representation of our architecture:

![](https://github.com/PicPay/picpay-ios/blob/develop/MVVMTemplate/MVVM-C.png)

To install/update the MVVM-C template use the command:

```
make template
```

### Modules

The PicPay iOS Project has some modules in an attempt to separate features into independent components.

#### Creating a Module

```
make module name=ModuleName
```

Reopen the workspace and your new module shall be there.
Don't forget to mark public things as public!

## Git Patterns

At PicPay, we use Gitflow pattern when developing new features, bugfix or hotfix. You can learn more about the gitflow process [here](https://danielkummer.github.io/git-flow-cheatsheet/index.pt_BR.html).

### Feature name Pattern

1. Feature: `feature/<jira-card(-number>-feature-name`
1. Bugfix: `feature/bugfix-<jira-card-number>-bugfix-name`
1. Hotfix: `hotfix/hotfix-name` (only from master)

### Pull Request name Pattern

We generate changelogs based on Pull Request names, use this pattern to create the title:

`Type(squad): [jira-card-number] Descrição da mudança em português`

Valid values of `Type` are: <Added|Changed|Deprecated|Removed|Fixed|Security>
(see more on https://keepachangelog.com/en/1.0.0/#how)

It's good to remember that the Pull Request comments, approvals and requested changes have to be made in English, just the change description on the PR title shoould be in portuguese.

## CHANGELOG

You can check the Changelog here. It's good to know that the CHANGELOG is the only part of the project that is in Portuguese and it's no longer editable.

- [**CHANGELOG.md**](https://gist.github.com/9a70e5d97aeb61c2a9d6e42a5f556954)

## UI Testing

You can check the UI Tests documentation here [UI Testing](https://github.com/PicPay/picpay-ios/blob/dev/UITests.md)

- [**Functional Tests (UITest)**](https://github.com/PicPay/picpay-ios/blob/dev/UITests.md)
