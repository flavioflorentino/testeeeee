public protocol HasAppParameters {
    var appParameters: AppParametersContract { get }
}
