import Foundation

public protocol AppParametersContract {
    var referalCodeBannerDismissDate: Date? { get }
    var displayCreditCardOnboardDelay: Int { get set }

    func getOnboardSocial() -> Bool
    func setOnboardSocial(_ value: Bool)

    func setNotFirstTimeOnApp()

    func hasAtLeastOneFeedOnce() -> Bool
    func setHasAtLeastOneFeedOnce(_ value: Bool)
}
