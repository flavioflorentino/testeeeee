#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#ifdef __OBJC__
    #import "AppParameters.h"
    #import "MBPlanType.h"
    #import "PPRewardProgram.h"
    #import "PPBank.h"
    #import "PPBankAccount.h"
    #import "MBTransaction.h"
    #import "PPParkingCarPlate.h"
    #import "PPParkingTime.h"
    #import "PPStore.h"
    #import "PPWithdrawal.h"
    #import "PPP2PTransaction.h"
    #import "PPReceiptParking.h"
    #import "PPChatLikes.h"
    #import "PPChatMessage.h"
    #import "PPMgmConfigs.h"
    #import "MBItem.h"
    #import "PPContact.h"
    #import "MBConsumer.h"
    #import "PPFeedActivity.h"
    #import "CurrencyFormatter.h"
#endif

//! Project version number for CoreLegacy.
FOUNDATION_EXPORT double CoreLegacyVersionNumber;

//! Project version string for CoreLegacy.
FOUNDATION_EXPORT const unsigned char CoreLegacyVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CoreLegacy/PublicHeader.h>
