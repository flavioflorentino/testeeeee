//
//  CurrencyFormatter.m
//  PicPay
//
//  Created by Diogo Carneiro on 20/01/13.
//
//

#import "CurrencyFormatter.h"

@implementation CurrencyFormatter

+ (NSString *)brazillianRealStringFromNumber: (NSNumber *)number{
    return [[CurrencyFormatter currencyStyleIgnoringRound:[[[CurrencyFormatter currencyStyleIgnoringRound:YES] numberFromString:[[CurrencyFormatter currencyStyleIgnoringRound:YES] stringFromNumber:number]] floatValue] == [number floatValue]] stringFromNumber:number];
}

+ (NSNumber *)brazillianRealNumberFromString: (NSString *)string {
    NSNumberFormatter *currentStyle = [CurrencyFormatter currencyStyleIgnoringRound:NO];
    return [currentStyle numberFromString:string];
}

+ (NSNumberFormatter *)currencyStyleIgnoringRound:(BOOL)ignoreRound{
    NSNumberFormatter *currencyStyle = [[NSNumberFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"pt_BR"];
    [currencyStyle setLocale:locale];
    currencyStyle.usesGroupingSeparator = YES;
    currencyStyle.numberStyle = NSNumberFormatterCurrencyStyle;
    currencyStyle.formatterBehavior = NSNumberFormatterBehaviorDefault;
    if (!ignoreRound) {
        [currencyStyle setRoundingMode: NSNumberFormatterRoundDown];
        [currencyStyle setMaximumFractionDigits:2];
    }
    return currencyStyle;
}

@end
