//
//  CurrencyFormatter.h
//  PicPay
//
//  Created by Diogo Carneiro on 20/01/13.
//
//

#import <Foundation/Foundation.h>

@interface CurrencyFormatter : NSObject

+ (NSString *)brazillianRealStringFromNumber: (NSNumber *)number;
+ (NSNumber *)brazillianRealNumberFromString: (NSString *)string;

@end
