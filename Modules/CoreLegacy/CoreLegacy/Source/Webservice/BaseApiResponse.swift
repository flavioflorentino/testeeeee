import SwiftyJSON
import Foundation

public protocol BaseApiResponse {
    init?(json: JSON)
}
