//
//  MBItem.m
//  MoBuy
//
//  Created by Diogo Carneiro on 03/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MBItem.h"

@implementation MBItem

@synthesize wsId, name, variantId, price, type, sku, weight, quantity, imageSrc, seller, image, description, guid, freeShipping, selectedCombination, combinations;

- (id) init {
    self = [super init];
    if (self != nil) {
		self.guid = [[NSProcessInfo processInfo] globallyUniqueString];
    }
    return self;
}

- (BOOL)completeItem{
	return false;
}

@end
