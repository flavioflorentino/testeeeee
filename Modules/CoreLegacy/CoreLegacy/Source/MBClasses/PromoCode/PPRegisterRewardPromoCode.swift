public final class PPRegisterRewardPromoCode {
    public var code: String = ""
    public var value: String = ""
    public var inviter: PPContact?
    public var webviewUrl: String?
    
    @objc public init(dictionary: [AnyHashable: Any]) {
        guard let inviterData = dictionary["inviter"] as? [AnyHashable: Any] else {
            return
        }
        
        if let contact = inviterData["profile"] as? [AnyHashable: Any] {
            inviter = PPContact(profileDictionary: contact)
        }
        
        if let counts = inviterData["counts"] as? [AnyHashable: Any] {
            inviter?.following = counts["following"] as? Int ?? 0
            inviter?.followers = counts["followers"] as? Int ?? 0
        }
        
        webviewUrl = dictionary["webview"] as? String ?? nil
        
        if let mgmInfo = dictionary["mgm_info"] as? [AnyHashable: Any] {
            code = mgmInfo["referral_code"] as? String ?? ""
            value = mgmInfo["reward_value_str"] as? String ?? ""
        }
    }
}
