//
//  PPP2PTransaction.h
//  PicPay
//
//  Created by Diogo Carneiro on 14/08/13.
//
//

#import <Foundation/Foundation.h>

@interface PPP2PTransaction : NSObject

@property (strong, nonatomic) NSString *wsId;
@property (strong, nonatomic) NSDecimalNumber *sendedValue;
@property (strong, nonatomic) NSArray *receiptItems;

@end
