//
//  PPStore.h
//  PicPay
//
//  Created by Diogo Carneiro on 03/04/13.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PPRewardProgram.h"

#define IsOldParking 1
#define IsNewParking 2


typedef NS_ENUM(NSUInteger,PPStoreProcessor) {
    PPStoreProcessorDefault = 0 ,
    PPStoreProcessorZonaAzul = 1,
};

@interface PPStore: NSObject

@property NSInteger wsId;

@property (strong, nonatomic, nullable) NSString *storeId;
@property (strong, nonatomic, nullable) NSString *sellerId;
@property BOOL isVerified;
@property (strong, nonatomic, nullable) NSString *name;
@property double latitude; 
@property double longitude;

@property BOOL load_logo;


@property (strong, nonatomic, null_unspecified) NSString *address;
@property (strong, nonatomic, null_unspecified) NSString *email;
@property (strong, nonatomic, null_unspecified) NSString *website;
@property (strong, nonatomic, null_unspecified) NSString *descript;
@property int distance;
@property (strong, nonatomic, null_unspecified) NSString *img_url;
@property (strong, nonatomic, null_unspecified) NSString *large_img_url;
@property (strong, nonatomic, null_unspecified) NSString *large_img;
@property (strong, nonatomic, null_unspecified) UIImage  *logo;
@property (strong, nonatomic, null_unspecified) NSString *ppcode;
@property BOOL isCielo;

@property (strong, nonatomic, null_unspecified) PPRewardProgram *rewardProgram;

@property (strong, nonatomic, null_unspecified) NSDecimalNumber *consumerBalance;

@property (strong, nonatomic, null_unspecified) NSMutableArray *phones;
@property (strong, nonatomic, null_unspecified) NSMutableArray *workingHours;

@property (nonatomic) int isParkingPayment;
@property (nonatomic) BOOL isPinnedAtTheTop;
@property PPStoreProcessor processor;

- (void)setProcessorWithString:(NSString * _Null_unspecified) value;
- (NSString * _Null_unspecified) distanceAsString;

+ (UIImage * _Null_unspecified)photoPlaceholder;

/**!
 * Create a store from a profile dictionary
 */
- (instancetype _Null_unspecified)initWithProfileDictionary:(NSDictionary * _Null_unspecified)profile;

@end
