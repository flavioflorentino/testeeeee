//
//  PPFeedActivity.m
//  PicPay
//
//  Created by Diogo Carneiro on 02/09/13.
//
//

#import "PPFeedActivity.h"

@implementation PPFeedActivity

- (UIImage *)image{
	if (image == nil) {
		return [self placeholderImage];
	}else{
		return image;
	}
}

- (UIImage *)placeholderImage{
	return [UIImage imageNamed:@"friends_empty_avatar"];
}

- (BOOL)isCancelled{
	return _actionId == FeedActionTransactionCancel || _actionId == FeedActionP2pSentCancelled || _actionId == FeedActionP2pReceivedCancelled;
}

- (BOOL)isCancelRequested{
    return _actionId == FeedActionP2pCancelRequested;
}

- (BOOL)isUnderReview{
	return _actionId == FeedActionP2pReceivedHold || _actionId == FeedActionP2pSentHold;
}

- (void)setSubtitleColorWithHex:(NSString *)hexString{
    @try {
        unsigned rgbValue = 0;
        NSScanner *scanner = [NSScanner scannerWithString:hexString];
        [scanner setScanLocation:1]; // bypass '#' character
        [scanner scanHexInt:&rgbValue];
        self.subtitleColor = [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
    } @catch (NSException *exception) {
        self.subtitleColor = [UIColor colorWithRed:51.0/255 green:51.0/255 blue:51.0/255 alpha:1];
    }
}

@end
