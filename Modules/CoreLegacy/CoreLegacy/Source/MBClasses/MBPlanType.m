#import "MBPlanType.h"

@implementation MBPlanType
// A = PARKING
// P = ?
// C = ?
// E = EVENTO

// WSItemRequest inicia um plan_type da API
// WSTransaction inicia um plan_type da API

// Está em MBTransaction e Seller

- (void)setPlanTypeId:(char)plan_type_id{
	planTypeId = plan_type_id;
	
    if ([self isKindOfPlan:'P']) {
        self.needAddress = YES;
    }else if ([self isKindOfPlan:'E']){
        self.needAddress = NO;
    }else if ([self isKindOfPlan:'C']){
        self.needAddress = NO;
    }else if ([self isKindOfPlan:'A']){
        self.needAddress = NO;
    }else{
        self.needAddress = YES;
    }
}

- (NSString *)planTypeIdToString{
	return [NSString stringWithFormat:@"%c",planTypeId];
}

- (BOOL)isKindOfPlan:(char)plan_type_id{
	return planTypeId == plan_type_id;
}

@end
