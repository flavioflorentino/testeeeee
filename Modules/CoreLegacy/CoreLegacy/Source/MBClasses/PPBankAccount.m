//
//  PPBankAccount.m
//  PicPay
//
//  Created by Diogo Carneiro on 20/08/13.
//
//

#import "PPBankAccount.h"

@implementation PPBankAccount

- (instancetype _Nullable ) initWithDictionary:(NSDictionary * _Nullable) dictionary {
    self = [super init];
    
    if (self != nil) {
       
        if ([dictionary valueForKey:@"id"] != [NSNull null]){
            self.wsId = [dictionary valueForKey:@"id"];
        }
        
        if ([dictionary valueForKey:@"conta"] != [NSNull null]){
            self.accountNumber = [dictionary valueForKey:@"conta"];
        }
        
        if ([dictionary valueForKey:@"status_name"] != [NSNull null]){
            self.statusName = [dictionary valueForKey:@"status_name"];
        }
        if ([dictionary valueForKey:@"agencia"] != [NSNull null]){
            self.agencyNumber = [dictionary valueForKey:@"agencia"];
        }
        if ([dictionary valueForKey:@"agencia_dv"] != [NSNull null]){
            self.agencyDigit = [dictionary valueForKey:@"agencia_dv"];
        }
        if ([dictionary valueForKey:@"conta_dv"] != [NSNull null]){
            self.accountDigit = [dictionary valueForKey:@"conta_dv"];
        }
        if ([dictionary valueForKey:@"cpf"] != [NSNull null]){
            self.cpf = [dictionary valueForKey:@"cpf"];
        }
        if ([dictionary valueForKey:@"tipo"] != [NSNull null]){
            self.accountType = [dictionary valueForKey:@"tipo"];
        }
        if ([dictionary valueForKey:@"Banco"] != nil && [[dictionary valueForKey:@"Banco"] isKindOfClass:[NSDictionary class]]) {
            self.bank = [[PPBank alloc] initWithDictionary:[dictionary valueForKey:@"Banco"]];
        }
    }
    
    return self;
}

-(id) copyWithZone: (NSZone *) zone {
    PPBankAccount * bankAccount = [[PPBankAccount allocWithZone: zone] init];
    bankAccount.wsId = [self.wsId copy];
    bankAccount.accountNumber = [self.accountNumber copy];
    bankAccount.statusName = [self.statusName copy];
    bankAccount.agencyNumber = [self.agencyNumber copy];
    bankAccount.agencyDigit = [self.agencyDigit copy];
    bankAccount.accountDigit = [self.accountDigit copy];
    bankAccount.cpf = [self.cpf copy];
    bankAccount.accountType = [self.accountType copy];
    bankAccount.bank = [self.bank copy];
    
    return bankAccount;
}

@end
