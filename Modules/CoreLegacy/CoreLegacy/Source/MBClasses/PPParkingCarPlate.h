//
//  PPParkingCarPlate.h
//  PicPay
//
//  Created by Diogo Carneiro on 02/12/15.
//
//

#import <Foundation/Foundation.h>

@interface PPParkingCarPlate : NSObject

@property (strong, nonatomic) NSString *plate;

@end
