//
//  MBItem.h
//  MoBuy
//
//  Created by Diogo Carneiro on 03/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PPRewardProgram.h"

@class Seller;

typedef enum : NSUInteger {
    PPItemThirdPartyProcessorZonaAzul
} PPItemThirdPartyProcessor;


@interface MBItem : NSObject

@property int wsId;

@property (strong, nonatomic)NSDecimalNumber *value; //e-commerce

@property (strong, nonatomic) NSString *guid;
@property (strong, nonatomic) NSString *barcode_string;
@property int variantId;
@property char type;
@property NSInteger quantity;
@property BOOL freeShipping;
@property NSDecimalNumber *price;
@property NSDecimalNumber *weight;
@property (strong, nonatomic) NSString *sku;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *description;
@property (strong, nonatomic) Seller *seller;
@property (strong, nonatomic) NSDictionary *combinations;
@property NSInteger selectedCombination;
@property (strong, nonatomic) NSString *option_variant_names;
@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) NSString *imageSrc;

//events
@property (strong, nonatomic) NSString *ticket_code;
@property (strong, nonatomic) NSString *ticket_datamatrix_url;
@property (strong, nonatomic) NSString *ticket_sent_to;
@property (strong, nonatomic) NSString *event_date;
@property int quantityLimit;

//PAV
@property (strong, nonatomic) NSString *consumer_credit_description;
@property (strong, nonatomic) NSDecimalNumber *consumer_credit_value_seller;
@property (strong, nonatomic) PPRewardProgram *rewardProgram;

//Parking Payment
@property (nonatomic) BOOL isParkingPayment;
@property (strong, nonatomic) NSMutableArray *parkingTimes;
@property (nonatomic, nonatomic) NSInteger defaultTime;
@property (strong, nonatomic) NSMutableArray *parkingPlates;
@property (strong, nonatomic) NSString *parkingInfo;

@property (strong, nonatomic) NSString *storeId;
@property (strong, nonatomic) NSString *storeName;
@property (strong, nonatomic) NSString *isParking;
@property (strong, nonatomic) NSString *sellerId;

@property (strong,nonatomic) NSObject *thirdPartyData;
@property PPItemThirdPartyProcessor thirdPartyProcessor;

- (BOOL)completeItem;

@end


