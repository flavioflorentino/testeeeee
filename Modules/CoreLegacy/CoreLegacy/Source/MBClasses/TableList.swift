//
//  RowData.swift
//  PicPayEmpresas
//
//  ☝🏽☁️ Created by Luiz Henrique Guimaraes on 15/02/17.
//  Copyright © 2017 PicPay. All rights reserved.
//

import Foundation

public final class TableList<DataType> {
    public var sections:[TableListSectionData<DataType>]
    public var pagination:Pagination
    public var isLoading:Bool
    
    public init(){
        sections = [TableListSectionData<DataType>()]
        pagination = Pagination()
        isLoading = false
    }
    
    public subscript(_ input:IndexPath) -> DataType? {
        if sections.count > input.section && sections[input.section].rows.count > input.row {
            return sections[input.section].rows[input.row].data
        }
        
        return nil
    }
    
    public func populate(section:Int, list:[DataType]){
        sections[section].rows.removeAll()
        for data in list{
            sections[section].rows.append(TableListRowData<DataType>(data:data))
        }
    }
    
    public func append(section:Int, list:[DataType]){
        for data in list{
            sections[section].rows.append(TableListRowData<DataType>(data:data))
        }
    }
    
    /// Checks if all sections are empty
    public var isEmpty: Bool {
        var isEmpty = true
        for section in sections {
            if !section.rows.isEmpty {
                isEmpty = false
            }
        }
        return isEmpty
    }
    
}

public final class TableListRowData<DataType> {
    public var data:DataType
    public var isLoading:Bool
    
    public init(data:DataType){
        self.data = data
        self.isLoading = false
    }
}

public final class TableListSectionData<DataType> {
    public var rows:[TableListRowData<DataType>]
    public var title: String = ""
    public var subtitle: String = ""
    public var isLoading: Bool = false
    public var isOpen: Bool = true
    
    public var rowsDataValue: [DataType] {
        var rows:[DataType] = []
        for r in self.rows {
           rows.append(r.data)
        }
        return rows
    }
    
    public init(){
        rows = []
    }
}

public struct Pagination {
    public let hasNext:Bool
    public let nextPage:Int
    
    public init(){
        hasNext = false
        nextPage = 0
    }
}

