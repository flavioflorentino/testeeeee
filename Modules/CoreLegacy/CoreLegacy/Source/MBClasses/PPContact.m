#import "PPContact.h"

@implementation PPContact

#ifdef DEBUG
#define DebugLog( s, ... ) NSLog( @"<%@:(%d)> %@", [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#define DebugLogWs( s, ... ) NSLog( @"%@", [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#else
#define DebugLog( s, ... )
#define DebugLogWs( s, ... )
#endif

#pragma mark - Initializer

- (id)init {
    self = [super init];
    if (self != nil) {
        self.phones = [NSMutableArray new];
        self.emails = [NSMutableArray new];
    }
    return self;
}

- (instancetype)initWith:(MBConsumer *) consumer {
    self = [super init];
    if (self != nil) {
        self.businessAccount = consumer.businessAccount;
        self.following = consumer.following;
        self.isVerified = consumer.verifiedAccount;
        self.imgUrl = consumer.img_url;
        self.phones = [NSMutableArray new];
        self.emails = [NSMutableArray new];
    }
    return self;
}

/**!
 * Create a contact from a profile dictionary
 */
- (instancetype)initWithProfileDictionary:(NSDictionary *)profile{
    self = [super init];
    if (self != nil) {
        self.phones = [[NSMutableArray alloc] init];
        @try {
            if ([profile valueForKey:@"id"] != [NSNull null] && [profile valueForKey:@"id"]!=nil){
                self.wsId = [[profile valueForKey:@"id"] integerValue];
            }
            
            if ([profile valueForKey:@"wsId"] != [NSNull null] && [profile valueForKey:@"wsId"] != nil){
                self.wsId = [[profile valueForKey:@"wsId"] integerValue];
            }
            
            if ([profile valueForKey:@"bio"] != [NSNull null] && [profile valueForKey:@"bio"] != nil){
                self.bio = [NSString stringWithFormat:@"%@",[profile valueForKey:@"bio"]];
            }
            
            self.username = @"";
            if ([profile valueForKey:@"username"] != [NSNull null] && [profile valueForKey:@"username"] != nil){
                self.username =  [NSString stringWithFormat:@"%@",[profile valueForKey:@"username"] ];
            }
            
            self.onlineName = @"";
            if ([profile valueForKey:@"name"] != [NSNull null] && [profile valueForKey:@"name"] != nil){
                self.onlineName = [NSString stringWithFormat:@"%@",[profile valueForKey:@"name"] ];
            }
            
            self.isVerified = [[profile valueForKey:@"verified"] boolValue];
            self.businessAccount = [[profile valueForKey:@"business_account"] boolValue];
            
            if ([profile valueForKey: @"config"] != nil && [[profile valueForKey: @"config"] valueForKey: @"payment_request"]) {
                NSDictionary* config = [profile valueForKey: @"config"];
                self.canReceivePaymentRequest = [[config valueForKey: @"payment_request"] boolValue];
            } else {
                self.canReceivePaymentRequest = YES;
            }
            
            if ( [profile valueForKey:@"is_pro"] != nil &&  [profile valueForKey:@"is_pro"] != [NSNull null]){
                if ( [[profile valueForKey:@"is_pro"] boolValue] ) {
                    self.businessAccount = YES;
                }
            }
            
            if ( [profile valueForKey:@"pro"] != nil &&  [profile valueForKey:@"pro"] != [NSNull null]){
                if ( [[profile valueForKey:@"pro"] boolValue] ) {
                    self.businessAccount = YES;
                }
            }
            
            
            self.imgUrl = [profile valueForKey:@"img_url"] != [NSNull null] ? [NSString stringWithFormat:@"%@",[profile valueForKey:@"img_url"] ] : nil;
            
            if ( [profile valueForKey:@"image_url_small"] != nil &&  [profile valueForKey:@"image_url_small"] != [NSNull null]){
                self.imgUrl = [NSString stringWithFormat:@"%@",[profile valueForKey:@"image_url_small"]];
            }
            
            self.imgUrlLarge = [profile valueForKey:@"img_url_large"] != [NSNull null] && [profile valueForKey:@"img_url_large"] !=nil ? [NSString stringWithFormat:@"%@",[profile valueForKey:@"img_url_large"] ] : nil;
            
            if (self.imgUrl!=nil && self.imgUrlLarge == nil){
                self.imgUrlLarge = [self.imgUrl copy];
            }
            
            if ([profile valueForKey:@"tags"] != nil  && [[profile valueForKey:@"tags"] isKindOfClass:[NSArray class]]) {
                NSArray *tagsList = (NSArray*) [profile valueForKey:@"tags"];
                NSMutableArray *tags = [NSMutableArray array];
                for (id tag in tagsList) {
                    if ( [tag isKindOfClass:[NSString class]] ) {
                        NSString *s = (NSString*) tag;
                        [tags addObject:[NSString stringWithFormat:@"%@",s]];
                    }
                }
                self.tags = tags.copy;
            }
            
            self.isPicpayUser = YES;
            self.contactType = PPContactTypeProfilePopUp;
        }
        @catch (NSException *exception) {
            NSLog(@"ERROR - PPContact initWithProfileDictionary");
        }
    }
    return self;
}

/**!
* METODO TEMPORARIO
* TODO: REMOVER ESSE METODO QUANDO IMPLEMENTAR PERFIL DA LOJA
*/
- (instancetype)initWithStoreProfileDictionary:(NSDictionary *)profile{
    self = [super init];
    if (self != nil) {
        self.phones = [[NSMutableArray alloc] init];
        @try {
            self.isStore = YES;
            self.username = [NSString stringWithFormat:@"%@",[profile valueForKey:@"name"] ];
            self.onlineName = [NSString stringWithFormat:@"%@",[profile valueForKey:@"address"] ];
            self.isVerified = NO;
            self.businessAccount = NO;
            self.imgUrl = [profile valueForKey:@"img_url"] != [NSNull null] ? [NSString stringWithFormat:@"%@",[profile valueForKey:@"img_url"] ] : nil;
            self.imgUrlLarge = [profile valueForKey:@"img_url_large"] != [NSNull null] && [profile valueForKey:@"img_url_large"] !=nil ? [NSString stringWithFormat:@"%@",[profile valueForKey:@"img_url_large"] ] : nil;
            
            if (self.imgUrl!=nil && self.imgUrlLarge == nil){
                self.imgUrlLarge = [self.imgUrl copy];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"ERROR - PPContact initWithProfileDictionary");
        }
    }
    return self;

}

- (NSString *)completeName{
	return completeName;
}

- (void)addPhoneNumber:(NSString *)phoneNumber{
	[self.phones addObject:phoneNumber];
}

- (void)addEmailAddress:(NSString *)emailAddress{
	[self.emails addObject:emailAddress];
}

- (void)setName:(NSString *)firstName lastName:(NSString *)lastName companyName:(NSString *)companyName{
	
	@try {
		completeName = [NSString stringWithFormat:@"%@ %@ %@",
						firstName.length > 0 ? [NSString stringWithFormat:@"%@", firstName] : @"",
						lastName.length > 0 ? lastName : @"",
						companyName.length > 0 ? [NSString stringWithFormat:@"%@", companyName] : @""];
		
		completeName = [completeName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        self.company = companyName;
	}
	@catch (NSException *exception) {
		completeName = @"Sem nome";
	}
}

- (UIImage *)photoPlaceholder{
	return [PPContact photoPlaceholder];
}

+ (UIImage *)photoPlaceholder{
    return [UIImage imageNamed:@"avatar_person"];
}

- (void)encodeWithCoder:(NSCoder *)coder;
{
    [coder encodeObject:completeName forKey:@"completeName"];
	[coder encodeObject:_onlineName forKey:@"onlineName"];
	[coder encodeObject:_imgUrl forKey:@"imgUrl"];
    [coder encodeObject:_phones forKey:@"phones"];
	[coder encodeBool:_isPicpayUser forKey:@"isPicpayUser"];
	[coder encodeObject:_selectedPhone forKey:@"selectedPhone"];
	[coder encodeBool:_businessAccount forKey:@"businessAccount"];
	[coder encodeInteger:_wsId forKey:@"wsId"];
    [coder encodeObject:_username forKey:@"username"];
    [coder encodeBool:_isVerified forKey:@"isVerified"];
    [coder encodeObject:_bio forKey:@"bio"];
}

- (id)initWithCoder:(NSCoder *)coder;
{
    self = [super init];
    if (self != nil)
    {
        completeName = [[coder decodeObjectForKey:@"completeName"] isKindOfClass:NSString.class] ? [coder decodeObjectForKey:@"completeName"] : @"";
		_imgUrl = [[coder decodeObjectForKey:@"imgUrl"] isKindOfClass:NSString.class] ? [coder decodeObjectForKey:@"imgUrl"] : @"";
        _onlineName = [[coder decodeObjectForKey:@"onlineName"] isKindOfClass:NSString.class] ? [coder decodeObjectForKey:@"onlineName"] : @"";
		_phones = [coder decodeObjectForKey:@"phones"];
		_isPicpayUser = [coder decodeBoolForKey:@"isPicpayUser"];
		_selectedPhone = [[coder decodeObjectForKey:@"selectedPhone"] isKindOfClass:NSString.class] ? [coder decodeObjectForKey:@"selectedPhone"] : @""; [coder decodeObjectForKey:@"selectedPhone"];
		_businessAccount = [coder decodeBoolForKey:@"businessAccount"];
		_wsId = [coder decodeIntegerForKey:@"wsId"];
        _username = [[coder decodeObjectForKey:@"username"] isKindOfClass:NSString.class] ? [coder decodeObjectForKey:@"username"] : @"";
        _isVerified = [coder decodeBoolForKey:@"isVerified"];
        _bio = [[coder decodeObjectForKey:@"bio"] isKindOfClass:NSString.class] ? [coder decodeObjectForKey:@"bio"] : @"";
    }
    return self;
}

- (NSArray *)dictionaryRepresentation{
	NSMutableArray *array = [[NSMutableArray alloc] init];
	[array addObject:[self completeName]];
	
	@try {
		[array addObject:[self phones]];
	}
	@catch (NSException *exception) {
		[array addObject:[[NSArray alloc] init]];
	}
	
	@try {
		[array addObject:[self emails]];
	}
	@catch (NSException *exception) {
		[array addObject:[[NSArray alloc] init]];
	}
	
	return array;
}

-(NSString *)description{
    return [self completeName];
}

@end
