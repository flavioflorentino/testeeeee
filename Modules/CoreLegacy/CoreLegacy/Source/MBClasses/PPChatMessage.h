//
//  PPChatMessage.h
//  PicPay
//
//  Created by Diogo Carneiro on 28/07/14.
//
//

#import <Foundation/Foundation.h>

@interface PPChatMessage : NSObject

@property (strong, nonatomic) NSString *message;
@property (strong, nonatomic) NSString *date;
@property (strong, nonatomic) NSString *senderName;
@property (strong, nonatomic) NSURL *senderImageUrl;
@property (strong, nonatomic) NSDate *dateTime;
@property NSInteger consumerId;
@property NSInteger wsId;

@property NSInteger messageRef;
@property BOOL sending;

@end
