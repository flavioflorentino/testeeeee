//
//  PPWithdrawal.h
//  PicPay
//
//  Created by Diogo Carneiro on 26/08/13.
//
//

#import <Foundation/Foundation.h>

@class CashoutScheduleAlert;

@interface PPWithdrawal : NSObject

@property (strong, nonatomic, nullable) NSDate *date;
@property (strong, nonatomic, nullable) NSString *statusCode;
@property (strong, nonatomic, nullable) NSString *statusName;
@property (strong, nonatomic, nullable) NSString *reasonId;
@property (strong, nonatomic, nullable) NSString *reasonDescription;
@property (strong, nonatomic, nullable) NSDecimalNumber *value;
@property BOOL isCancelled;
@property BOOL requireAccountChange;

@property (strong, nonatomic, nullable) NSDate *estimatedCompletionDate;
@property (strong, nonatomic, nullable) NSString *bankAccountDescription;
@property (strong, nonatomic, nullable) NSString *text;
@property (strong, nonatomic, nullable) NSString *textDisclaimer;

@property (strong, nonatomic, nullable) CashoutScheduleAlert *windowInfo;

- (instancetype _Nonnull)initWithDictionary:(NSDictionary * _Nonnull) dict;

@end
