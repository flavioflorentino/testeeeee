//
//  BankListSection.swift
//  PicPay
//
//  ☝🏽☁️ Created by Luiz Henrique Guimarães on 15/05/17.
//
//

import Foundation

public final class BankListSection {
    public let title: String
    public let rows: [PPBank]
    
    public init(title: String, rows:[PPBank]){
        self.title = title
        self.rows = rows
    }
}
