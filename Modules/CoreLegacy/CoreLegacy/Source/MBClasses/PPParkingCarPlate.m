//
//  PPParkingCarPlate.m
//  PicPay
//
//  Created by Diogo Carneiro on 02/12/15.
//
//

#import "PPParkingCarPlate.h"

@implementation PPParkingCarPlate

- (BOOL)isEqual:(id)object {
    PPParkingCarPlate* otherPlate = (PPParkingCarPlate*) object;
    return self.plate == otherPlate.plate;
}

@end
