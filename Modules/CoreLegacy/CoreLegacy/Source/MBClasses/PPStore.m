//
//  PPStore.m
//  PicPay
//
//  Created by Diogo Carneiro on 03/04/13.
//
//

#import "PPStore.h"

@implementation PPStore

#ifdef DEBUG
#define DebugLog( s, ... ) NSLog( @"<%@:(%d)> %@", [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#define DebugLogWs( s, ... ) NSLog( @"%@", [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#else
#define DebugLog( s, ... )
#define DebugLogWs( s, ... )
#endif

- (instancetype)init{
    self = [super init];
    if (self != nil) {
        self.processor = PPStoreProcessorDefault;
        self.isCielo = NO;
    }
    return self;
}

- (NSString * )distanceAsString{
    if (_distance < 100){
            return [NSString stringWithFormat:@"%g m", roundf(_distance)];
    }else if (_distance < 1000){
            return [NSString stringWithFormat:@"%g m", roundf(_distance/5)*5];
    }else if (_distance < 10000){
            return [NSString stringWithFormat:@"%g km", roundf(_distance/100)/10];
    }else{
            return [NSString stringWithFormat:@"%g km", roundf(_distance/1000)];
    }
}

+ (UIImage *)photoPlaceholder{
    return [UIImage imageNamed:@"avatar_place"];
}

- (void)setProcessorWithString:(NSString *) value{
    if ([value isEqual:@"ZonaAzul"]) {
        self.processor = PPStoreProcessorZonaAzul;
    }
}

/**!
 * Create a store from a profile dictionary
 */
- (instancetype)initWithProfileDictionary:(NSDictionary *)profile{
    self = [super init];
    if (self != nil) {
        self.isCielo = NO;
        @try {
            self.wsId = [[profile valueForKey:@"id"] integerValue];
            if ([profile valueForKey:@"seller_id"] != [NSNull null] && [profile valueForKey:@"seller_id"] !=nil){
                self.sellerId = [NSString stringWithFormat:@"%@", [profile valueForKey:@"seller_id"]];
            }
            self.name =  [NSString stringWithFormat:@"%@",[profile valueForKey:@"name"] ];
            self.address =  [NSString stringWithFormat:@"%@",[profile valueForKey:@"address"] ];
            self.img_url = [profile valueForKey:@"img_url"] != [NSNull null] && [profile valueForKey:@"img_url"] != nil ? [NSString stringWithFormat:@"%@",[profile valueForKey:@"img_url"] ] : nil;
            self.large_img_url = [profile valueForKey:@"img_url_large"] != [NSNull null] && [profile valueForKey:@"img_url_large"] !=nil ? [NSString stringWithFormat:@"%@",[profile valueForKey:@"img_url_large"] ] : nil;
            
            if (self.img_url!=nil && self.large_img_url == nil){
                self.large_img_url = [self.img_url copy];
            }
            
            if ([profile valueForKey:@"processor"] != [NSNull null] && [profile valueForKey:@"processor"] !=nil){
                [self setProcessorWithString:[NSString stringWithFormat:@"%@",[profile valueForKey:@"processor"]]];
            }
            
            self.isCielo = [profile valueForKey:@"is_cielo"] != [NSNull null] && [profile valueForKey:@"is_cielo"] != nil ? [profile valueForKey:@"is_cielo"] : NO;
            self.isVerified = [profile valueForKey:@"is_verified"] != [NSNull null] && [profile valueForKey:@"is_verified"] != nil ? [[profile valueForKey:@"is_verified"] boolValue] : NO;
            self.latitude = [[profile valueForKey:@"lat"] doubleValue];
            self.longitude = [[profile valueForKey:@"lng"] doubleValue];

            NSString *isParking = [NSString stringWithFormat:@"%@", [profile valueForKey:@"is_parking"]];
            self.isParkingPayment = INT32_C(isParking.intValue);
        }
        @catch (NSException *exception) {
            NSLog(@"ERROR - PPStore initWithProfileDictionary");
        }
    }
    return self;
}

@end
