//
//  PPContact.h
//  PicPay
//
//  Created by Diogo Carneiro on 30/07/13.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>
#import "MBConsumer.h"

typedef NS_ENUM(NSInteger, PPContactType) {
    PPContactTypeNormal = 1,
    PPContactTypeNearby = 2,
    PPContactTypeSearchResult = 3,
    PPContactTypeProfilePopUp = 4
};

@interface PPContact : NSObject{
	NSString *completeName;
}

@property NSInteger wsId;
@property (strong, nonatomic, nullable) NSMutableArray *phones;
@property (strong, nonatomic, nullable) NSString *selectedPhone;
@property (strong, nonatomic, nullable) NSString *imgUrl;
@property (strong, nonatomic, nullable) NSString *imgUrlLarge;
@property (strong, nonatomic, nullable) NSString *onlineName;
@property (strong, nonatomic, nullable) NSMutableArray *emails;
@property (strong, nonatomic, nullable) NSString *company;
@property (strong, nonatomic, nullable) NSString *username;
@property (strong, nonatomic, nullable) NSString *bio;
@property (strong, nonatomic, nullable) NSArray<NSString*> *tags;
@property BOOL canReceivePaymentRequest;

//properties for MGM feature
@property BOOL isSelectedForSMS;
@property BOOL isSelectedForEmail;
@property (strong, nonatomic, nullable) NSString *phoneToInvite;
@property (strong, nonatomic, nullable) NSString *emailToInvite;
@property BOOL isVerified;
@property NSInteger followers;
@property NSInteger following;

// properties for contact list
@property PPContactType contactType;

@property BOOL isPicpayUser;
@property BOOL businessAccount;
@property BOOL isStore;
@property BOOL isOpenFollowersAndFollowings;
@property BOOL isBlocked;
@property NSInteger followerStatus;

- (void)addPhoneNumber:(NSString * _Nullable)phoneNumber;
- (void)addEmailAddress:(NSString * _Nullable)emailAddress;
- (void)setName:(NSString * _Nullable)firstName lastName:(NSString * _Nullable)lastName companyName:(NSString * _Nullable)companyName;
- (NSString * _Nullable)completeName;
- (UIImage * _Nonnull)photoPlaceholder;
- (NSArray * _Nullable)dictionaryRepresentation;


/**!
 *  Return the placeholder image for contact avatar
 */
+ (UIImage * _Nonnull)photoPlaceholder;

/**!
 * Create a contact from a profile dictionary
 */
- (instancetype _Nonnull)initWithProfileDictionary:(NSDictionary * _Nullable)profile;
- (instancetype _Nonnull)initWith:(MBConsumer * _Nonnull)consumer;
    
/**!
 * METODO TEMPORARIO
 * TODO: REMOVER ESSE METODO QUANDO IMPLEMENTAR PERFIL DA LOJA
 */
- (instancetype _Nonnull)initWithStoreProfileDictionary:(NSDictionary * _Nullable)profilel;

@end
