import Foundation

public class Seller: NSObject {
    @objc public let wsId: Int
    @objc public let name: String
    @objc public let isVerified: Bool
    @objc public let logoUrl: String?
    @objc public let currentPlanType: MBPlanType
    @objc public let paymentConfig: PPPaymentConfig
    
    @objc
    public init(wsId: Int, name: String, isVerified: Bool, logoUrl: String?, currentPlanType: MBPlanType, paymentConfig: PPPaymentConfig) {
        self.wsId = wsId
        self.name = name
        self.isVerified = isVerified
        self.logoUrl = logoUrl
        self.currentPlanType = currentPlanType
        self.paymentConfig = paymentConfig
    }
}
