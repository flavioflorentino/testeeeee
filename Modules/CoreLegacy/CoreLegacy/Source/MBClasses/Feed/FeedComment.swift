//
//  FeedComment.swift
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 29/11/16.
//
//

import UIKit

public final class FeedComment: NSObject {
    public var id:String
    public var text:String
    public var consumerId:String
    public var timeAgo:String
    public var profile:PPContact
    public var isVisible: Bool = true
    
    public init?(jsonDict:[AnyHashable: Any], profile:PPContact){
        guard
            let id = jsonDict["Id"] as? String,
            let text = jsonDict["Text"] as? String,
            let consumerId = jsonDict["ConsumerId"] as? String,
            let timeAgo = jsonDict["TimeAgo"] as? String else{
                return nil
        }
        
        self.id = id
        self.text = text
        self.consumerId = consumerId
        self.timeAgo = timeAgo
        self.profile = profile
        super.init()
    }
    
    public init(text:String, consumerId:String, timeAgo:String, profile:PPContact){
        self.id = "99999999999999999"
        self.text = text
        self.consumerId = consumerId
        self.timeAgo = timeAgo
        self.profile = profile
    }
    
    public func populate(_ comment:FeedComment){
        self.id = comment.id
        self.text = comment.text
        self.timeAgo = comment.timeAgo
        self.profile = comment.profile
    }
    
    override public func isEqual(_ object: Any?) -> Bool {
        return id == (object as? FeedComment)?.id
    }
}
