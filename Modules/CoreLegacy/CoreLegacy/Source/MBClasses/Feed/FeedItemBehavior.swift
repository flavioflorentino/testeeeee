//
//  FeedWidgetGenericBehavior.swift
//  PicPay
//
//  Created by Vagner Orlandi on 18/08/17.
//
//

import UIKit

public enum FeedItemBehaviorType : String {
    case Reload = "reload"
    case ReloadItem = "reload_item"
    case Undefined = ""
}

public final class FeedItemBehavior: NSObject {
    public var type:FeedItemBehaviorType
    public var data: NSObject?
    public var alert: Alert?
    
    public struct Alert {
        public var title: String?
        public var message: String?
        public var buttonTitle: String = "Ok"
        
        public init(jsonDict:[AnyHashable: Any]) {
            self.title = jsonDict["title"] as? String
            self.message = jsonDict["message"] as? String
            if let buttonLabel = jsonDict["button_title"] as? String {
                self.buttonTitle = buttonLabel
            }
        }
    }
    
    public init(jsonDict:[AnyHashable: Any]) {
        self.type = .Undefined
        
        if let name = jsonDict["type"] as? String {
            if let type =  FeedItemBehaviorType(rawValue: name){
                self.type = type
            }
        }
        
        if let data = jsonDict["data"] as? NSObject {
            self.data = data
        }
        
        if let alertDict = jsonDict["alert"] as? [AnyHashable: Any] {
            self.alert = Alert(jsonDict: alertDict)
        }
    }
}
