//
//  ImageFeedWidget.swift
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 10/11/16.
//
//

import UIKit

public struct ImageFeedWidget {
    public var url:String
    public var action:FeedWidgetAction?
    public var icon:String?
    
    public init?(jsonDict:[AnyHashable: Any]) {
        guard
            let url = jsonDict["Url"] as? String
            else{ return nil }
        
        self.url = url
        
        if let icon = jsonDict["Icon"] as? String{
            self.icon = icon
        }
        
        if let action = jsonDict["Action"] as? [AnyHashable: Any]{
            self.action = FeedWidgetAction(jsonDict: action as [NSObject : AnyObject])
        }
    }
}
