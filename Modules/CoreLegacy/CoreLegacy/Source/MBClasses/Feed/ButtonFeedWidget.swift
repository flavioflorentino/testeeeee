import Foundation
import UI

public struct ButtonFeedWidget {
    public var text:String
    public var type:String
    public var action:String
    public var data:String
    public var color:UIColor
    
    public init?(jsonDict:[AnyHashable: Any]) {
        guard
            let textData = jsonDict["Text"] as? [AnyHashable: Any],
            let actionData = jsonDict["Action"] as? [AnyHashable: Any],
            let text = textData["Value"] as? String,
            let action = actionData["Type"] as? String else{
                return nil
        }
        
        self.text   = text
        self.action = action

        if let type = textData["Type"] {
            self.type = type as! String
        } else {
            self.type = ""
        }

        if let data = actionData["Data"] {
            self.data = data as! String
        } else {
            self.data = ""
        }
        
        self.color = #colorLiteral(red: 1, green: 0.737254902, blue: 0, alpha: 1)
        if let colorString = jsonDict["Color"] as? String, let color = Palette.hexColor(with: colorString) {
            self.color = color
        }
    }
}
