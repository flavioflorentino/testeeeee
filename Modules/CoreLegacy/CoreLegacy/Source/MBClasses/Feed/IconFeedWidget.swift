//
//  IconFeedWidget.swift
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 10/11/16.
//
//

import Foundation

public struct IconFeedWidget {
    public var icon:String
    public var color:String
    
    public init?(jsonDict:[AnyHashable: Any]) {
        guard
            let icon = jsonDict["Type"] as? String
            else{ return nil }
        
        self.icon = icon
        
        self.color = ""
        if let color = jsonDict["Color"] as? String {
            self.color = color
        }
    }
}
