import Foundation

public enum FeedItemComponent: String {
    case `default` = "Default"
    case nonInteractive = "NonInteractiveFeedItem"
    case pinned = "PinnedFeedItem"
    case requestDocument = "RequestDocument"
    case whithdrawDetail = "WhithdrawDetail"
    case carousel = "FeedCarouselItem"
    case banner = "FeedBannerItem"
    case payer = "PayerFeedItem"
    case charge = "P2PChargeFeedItem"
}

public enum FeedItemVisibility: String {
	case Private = "2"
	case Friends = "3"
}

@objc
public enum FeedItemVisibilityInt: Int {
	case `private` = 2
	case friends = 3
}

open class FeedItem: NSObject {
    public var id: String
    public var externalId: String
    public var transactionId: String
    public var type: String
    public var component: FeedItemComponent
    public var data: NSObject?
    public var actions: [FeedWidgetAction]
    
    override public var hash: Int {
        return id.hashValue
    }
    
    override public init() {
        id = ""
        type = ""
        externalId = ""
        transactionId = ""
        actions = [FeedWidgetAction]()
        component = .default
        super.init()
    }
    
    public init?(jsonDict: [AnyHashable: Any]) {
        guard let type = jsonDict["Type"] as? String,
            let uicompoment = jsonDict["UIComponent"] as? String else {
            return nil
        }
        
        self.type = type
        self.component = .default

        if let component = FeedItemComponent(rawValue: uicompoment) {
            self.component = component
        }
        
        id = ""
        if let data = jsonDict["Data"] as? NSObject {
            self.data = data
            if let id = data as? String {
                self.id = id
            }
        }
        
        externalId = ""
        if let id = jsonDict["ExternalId"] as? String {
            externalId = id
        }
        
        transactionId = ""
        if let id = jsonDict["TransactionId"] as? String {
            transactionId = id
        }

        self.actions = [FeedWidgetAction]()
        if let actionsData = jsonDict["Actions"] as? [[AnyHashable: Any]] {
            for actiton in actionsData {
                self.actions.append(FeedWidgetAction(jsonDict: actiton as [NSObject: AnyObject]))
            }
        }
    }
    
     override public func isEqual(_ object: Any?) -> Bool {
        if let object = object as? FeedItem {
            return id == object.id
        } else {
            return false
        }
    }
}
