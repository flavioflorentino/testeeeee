//
//  TextCheckFeedwidget.swift
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 10/11/16.
//
//

import Foundation

public struct TextCheckFeedWidget {
    public var value:String
    public var checked:Bool
    public var text:String
    
    public init(jsonDict:[AnyHashable: Any]) {
        self.checked = false
        self.value = ""
        self.text = ""
        
        if let value = jsonDict["Value"] as? String{
            self.value = value
        }
        
        if let text = jsonDict["Text"] as? String{
            self.text = text
        }
        
        self.value = "0"
        if let num = jsonDict["Num"] as? Int{
            self.value = String(num)
        }
        
        if let num = jsonDict["Num"] as? String{
            self.value = String(num)
        }
        
        if let checked = jsonDict["Checked"] as? Bool{
            self.checked = checked
        }
    }
}
