//
//  TextLinkedFeedWidget.swift
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 21/12/16.
//
//

public final class TextLinkedFeedWidget {
    public var value:String
    public var style:String?
    public var links:[String]
    
    public init?(jsonDict:[AnyHashable: Any]) {
        self.links = [String]()
        
        guard
            let value = jsonDict["Value"] as? String
            else{ return nil }
        
        self.value = value
        
        //search links in text
        do{
            let text = value as NSString
            let regex = try NSRegularExpression(pattern: "<lnk.*?href='(.*?)'.*?>(.*?)</lnk>", options: NSRegularExpression.Options.caseInsensitive)
            let range = NSMakeRange(0, text.length)
            let matches = regex.matches(in: text as String, options: .withTransparentBounds, range: range)
            for match in matches {
                let link = text.substring(with: match.range(at: 1))
                self.links.append(link)
            }
            
        } catch _ {
            print("Não foi possivel recuperar links no texto.")
        }
        
        // replace all html links from text
        do{
            let regex = try NSRegularExpression(pattern: "<lnk.*?href='(.*?)'.*?>",
                                             options: NSRegularExpression.Options.caseInsensitive)
            let range = NSMakeRange(0, value.count)
            let modString = regex.stringByReplacingMatches(in: value,
                                                               options: [],
                                                               range: range,
                                                               withTemplate: "[link]")
            
            self.value = String(format:"%@",modString.replacingOccurrences(of: "</lnk>", with: "[/link]"))
        } catch _ {
            print("Não foi possivel remover os links no texto.")
        }
        
        if let style = jsonDict["Style"] as? String {
            self.style = style
        }
    }
}
