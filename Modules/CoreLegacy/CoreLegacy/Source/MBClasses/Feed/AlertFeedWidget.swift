import Foundation

public enum AlertFeedWidgetType: String {
    case warning = "warning"
    case processing = "processing"
    case analysis = "analysis"
    case success = "success"
    case failure = "failure"
}

public struct AlertFeedWidget {
    public var text: String
    public var type: AlertFeedWidgetType?
    
    public init?(jsonDict:[AnyHashable: Any]) {
        guard let text = jsonDict["Text"] as? String,
            let typeDescription = jsonDict["Type"] as? String else {
                return nil
        }
        
        self.text = text
        self.type = AlertFeedWidgetType(rawValue: typeDescription)
    }
}
