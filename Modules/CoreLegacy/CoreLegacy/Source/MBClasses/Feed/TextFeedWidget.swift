//
//  TextFeedWidget.swift
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 10/11/16.
//
//

import UIKit

public struct TextFeedWidget {
    public var value:String
    public var style:String?
    
    public init?(jsonDict:[AnyHashable: Any]) {
        guard
        let value = jsonDict["Value"] as? String
        else{ return nil }
        
        self.value = value.sanitized
        
        if let style = jsonDict["Style"] as? String {
            self.style = style
        }
    }
}
