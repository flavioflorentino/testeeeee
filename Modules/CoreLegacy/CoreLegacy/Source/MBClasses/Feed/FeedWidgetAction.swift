//
//  FeedWidgetAction.swift
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 10/11/16.
//
//

import UIKit

public enum FeedWidgetActionType : String {
    case OpenP2PReceipt = "p2p_receipt"
    case OpenPAVReceipt = "pav_receipt"
    case OpenCardDebitReceipt = "card_debit_receipt"
    case openStoreReceipt = "store_payments_receipt"
    case openPIXReceipt = "pix_receipt"
    case openPIXReturn = "refund_pix_transaction"
    case openPIXReceiptReceivement = "pix_receipt_receivement"
    case OpenZonaAzulReceipt = "zona_azul_receipt"
    case OpenZonaAzulCadReceipt = "zona_azul_receipt_cad"
    case OpenConsumerProfile = "openProfile"
    case OpenDigitalGoods = "openDigitalGoodsProfile"
    case OpenSellerProfile = "openSellerProfile"
    case OpenP2MQrCodeScanner = "open_p2m_qrcode"
    case OpenMembershipProducerProfile = "openMembershipProducerProfile"
    case OpenBoletoProfile = "open_bill_payment_profile"
    case OpenMembershipDetails = "subscription_details"
    case MakeTransactionPrivate = "make_private"
    case p2pExternalTransferReceipt = "p2p_external_transfer_receipt"
    case sendLink = "send_link"
    case Report = "report"
    case Generic = "generic"
    case Undefined = ""
}

public final class FeedWidgetAction: NSObject {
    public var label:String = ""
    public var message:String?
    public var type:FeedWidgetActionType = .Undefined
    public var data:NSObject?
    public var auth: Bool = false
    
    public init(jsonDict:[AnyHashable: Any]) {
        
        if let name = jsonDict["Type"] as? String {
            if let type =  FeedWidgetActionType(rawValue: name){
                self.type = type
            }
        }
        
        if let data = jsonDict["Data"] as? NSObject {
            self.data = data
        }
        
        if let label = jsonDict["Label"] as? String {
            self.label = label
        }
        
        if let message = jsonDict["Message"] as? String {
            self.message = message
        }
        
        if let auth = jsonDict["Auth"] as? Bool {
            self.auth = auth
        }
    }
}
