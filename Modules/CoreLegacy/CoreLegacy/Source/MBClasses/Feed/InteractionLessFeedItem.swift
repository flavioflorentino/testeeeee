//
//  InteractionLessFeedItem.swift
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 21/11/16.
//
//

import UIKit

public final class InteractionLessFeedItem: FeedItem {
    public var image:ImageFeedWidget?
    public var title:TextFeedWidget
    public var text:TextFeedWidget?
    public var money:TextFeedWidget?
    public var timeAgo:TextFeedWidget?
    public var timeAgoIcon: IconFeedWidget?
    
    override public init?(jsonDict:[AnyHashable: Any]) {
        guard
            let config = jsonDict["Config"] as? [AnyHashable: Any],
            let titleData = config["Title"] as? [AnyHashable: Any],
            let title = TextFeedWidget(jsonDict: titleData as [NSObject : AnyObject]) else{
                return nil
        }
        
        self.title = title
        
        super.init(jsonDict: jsonDict as [NSObject : AnyObject])
        
        if let image = config["Image"] as? [AnyHashable: Any]  {
            self.image = ImageFeedWidget(jsonDict: image as [NSObject : AnyObject])
        }
        
        if let text = config["Text"] as? [AnyHashable: Any]  {
            self.text = TextFeedWidget(jsonDict: text as [NSObject : AnyObject])
        }
        if let money = config["MoneyLabel"] as? [AnyHashable: Any]  {
            self.money = TextFeedWidget(jsonDict: money as [NSObject : AnyObject])
        }
        if let timeAgoData = config["TimeAgo"] as? [AnyHashable: Any]  {
            if let timeAgo = timeAgoData["Text"] as? [AnyHashable: Any]{
                self.timeAgo = TextFeedWidget(jsonDict: timeAgo as [NSObject : AnyObject])
            }
            if let timeAgoIcon = timeAgoData["Icon"] as? [AnyHashable: Any]{
                self.timeAgoIcon = IconFeedWidget(jsonDict: timeAgoIcon as [NSObject : AnyObject])
            }
        }
    }
}
