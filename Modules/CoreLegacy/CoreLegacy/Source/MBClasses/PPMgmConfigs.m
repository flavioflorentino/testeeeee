//
//  PPMgmConfigs.m
//  PicPay
//
//  Created by Diogo Carneiro on 23/01/14.
//
//

#import "PPMgmConfigs.h"
#import "CoreLegacy/CoreLegacy-Swift.h"

@implementation PPMgmConfigs

- (instancetype)init {
    return [self initWith:@{}];
}

- (instancetype)initWith:(NSDictionary *)mgmJson {
    self = [super init];
    if (self && mgmJson.allKeys.count > 0) {
        @try {
            self.mgmEnabled = YES;
            @try {
                NSArray *mgmContactsJson = [mgmJson valueForKey:@"Contacts"];
                self.contactsEnabled = YES;
                self.contactsTitle = [mgmContactsJson valueForKey:@"text"];
                self.contactsSubtitle = [mgmContactsJson valueForKey:@"shoutout"];
                self.contactsImgUrl = [mgmContactsJson valueForKey:@"img_url"];
            }
            @catch (NSException *exception) {
                self.contactsEnabled = NO;
            }
            
            @try {
                NSArray *mgmProfileJson = [mgmJson valueForKey:@"Profile"];
                self.profileEnabled = YES;
                self.profileMgmCellTitle = [mgmProfileJson valueForKey:@"text"];
                self.mgmProfileHeader = [mgmProfileJson valueForKey:@"header"];
                self.mgmProfileLine1 = [mgmProfileJson valueForKey:@"line1"];
                self.mgmProfileLine2 = [mgmProfileJson valueForKey:@"line2"];
            }
            @catch (NSException *exception) {
                self.profileEnabled = NO;
            }
            
            @try {
                NSArray *mgmScreenJson = [mgmJson valueForKey:@"Screen"];
                
                self.mgmScreenText = [[mgmScreenJson valueForKey:@"text"] isKindOfClass:[NSString class]] ? [mgmScreenJson valueForKey:@"text"] : nil;
                self.mgmScreenButtonTitle = [[mgmScreenJson valueForKey:@"button"] isKindOfClass:[NSString class]] ? [mgmScreenJson valueForKey:@"button"] : nil;
                self.mgmScreenTitle = [[mgmScreenJson valueForKey:@"title"] isKindOfClass:[NSString class]] ? [mgmScreenJson valueForKey:@"title"] : nil;
                self.mgmScreenMainTitle = [[mgmScreenJson valueForKey:@"mgm3_title"] isKindOfClass:[NSString class]] ? [mgmScreenJson valueForKey:@"mgm3_title"] : nil;
                self.mgmScreenSubtitle = [[mgmScreenJson valueForKey:@"mgm3_subtitle"] isKindOfClass:[NSString class]] ? [mgmScreenJson valueForKey:@"mgm3_subtitle"] : nil;
                self.mgmScreenSubtitle2 = [[mgmScreenJson valueForKey:@"subtitle"] isKindOfClass:[NSString class]] ? [mgmScreenJson valueForKey:@"subtitle"] : nil;
                self.mgmScreenImgUrl = [[mgmScreenJson valueForKey:@"img_url"] isKindOfClass:[NSString class]] ? [mgmScreenJson valueForKey:@"img_url"] : nil;
                self.mgmScreenWarning = [[mgmScreenJson valueForKey:@"warning"] isKindOfClass:[NSString class]] ? [mgmScreenJson valueForKey:@"warning"] : nil;
                self.mgmScreenCompletionAlert = [[mgmScreenJson valueForKey:@"mgm3_completion_alert"] isKindOfClass:[NSString class]] ? [mgmScreenJson valueForKey:@"mgm3_completion_alert"] : nil;
                self.shareLink = [[mgmScreenJson valueForKey:@"share_link"] isKindOfClass:[NSString class]] ? [mgmScreenJson valueForKey:@"share_link"] : nil;
                self.text = [[mgmScreenJson valueForKey:@"text"] isKindOfClass:[NSString class]] ? [mgmScreenJson valueForKey:@"text"] : nil;
                self.shareText = [[mgmScreenJson valueForKey:@"share_text"] isKindOfClass:[NSString class]] ? [mgmScreenJson valueForKey:@"share_text"] : nil;
                self.shareTextFacebook = [[mgmScreenJson valueForKey:@"share_text_facebook"] isKindOfClass:[NSString class]] ? [mgmScreenJson valueForKey:@"share_text_facebook"] : nil;
                self.shareTextShort = [[mgmScreenJson valueForKey:@"share_text_short"] isKindOfClass:[NSString class]] ? [mgmScreenJson valueForKey:@"share_text_short"] : nil;
                self.textSuccess = [[mgmScreenJson valueForKey:@"text_success"] isKindOfClass:[NSString class]] ? [mgmScreenJson valueForKey:@"text_success"] : nil;
                self.mgmScreenTextShareScreen = [[mgmScreenJson valueForKey:@"text_share_screen"] isKindOfClass:[NSString class]] ? [mgmScreenJson valueForKey:@"text_share_screen"] : nil;
                
                @try {
                    NSArray *mgbJson = [mgmScreenJson valueForKey:@"Business"];
                    self.mgbButton = [NSString stringWithFormat:@"%@",[mgbJson valueForKey:@"button"]];
                    self.mgbSubtitle = [NSString stringWithFormat:@"%@",[mgbJson valueForKey:@"subtitle"]];
                    self.mgbShareLink = [NSString stringWithFormat:@"%@",[mgbJson valueForKey:@"share_link"]];
                    self.mgbText = [NSString stringWithFormat:@"%@",[mgbJson valueForKey:@"text"]];
                    self.mgbShareText = [NSString stringWithFormat:@"%@",[mgbJson valueForKey:@"share_text"]];
                    self.mgbShareTextFacebook = [NSString stringWithFormat:@"%@",[mgbJson valueForKey:@"share_text_facebook"]];
                    self.mgbShareTextShort = [NSString stringWithFormat:@"%@",[mgbJson valueForKey:@"share_text_short"]];
                    self.mgbScreenTextShare = [NSString stringWithFormat:@"%@",[mgbJson valueForKey:@"text_share_screen"]];
                    self.mgbScreenShortTextShare = [NSString stringWithFormat:@"%@",[mgbJson valueForKey:@"text_short_share_screen"] ];
                }
                @catch (NSException *exception) {
                    //bug at the server
                }
            }
            @catch (NSException *exception) {
                //bug at the server
            }
        }
        @catch (NSException *exception) {
            self.mgmEnabled = NO;
        }
    }
    return self;
}

@end
