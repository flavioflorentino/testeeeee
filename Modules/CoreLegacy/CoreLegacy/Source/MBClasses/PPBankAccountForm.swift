//
//  PPBankAccountForm.swift
//  PicPay
//
//  Created by Luiz Henrique Guimarães on 17/05/17.
//
//

import UIKit
import Foundation

open class PPBankAccountForm: NSObject {
    
    public var branchLimit: Int = 5
    public var branchDigitLimit: Int = 1
    public var branchDigitKeyboardType: UIKeyboardType = .numberPad
    public var isBranchDigitEnabled = false
    public var accountLimit: Int = 12
    public var accountDigitLimit: Int = 1
    public var cpfLenght: Int = 14
    public var recipientNameLimit: Int = 256
    public var accountDigitKeyboardType: UIKeyboardType = .numberPad
    public var type: FormType = .normal
    
    public enum FormType : String {
        case bancoDoBrasil = "banco_do_brasil"
        case normal = ""
    }
    
    public var accountTypes: [PPBankAccountType] = [
        PPBankAccountType(label: "Conta Corrente", value: "C"),
        PPBankAccountType(label: "Poupança", value: "P")
    ]
    
    public override init() {
        super.init()
    }
    
    @objc public init(dictionary dict: [AnyHashable: Any]) {
        if let typeString = dict["type"] as? String,  let type = FormType(rawValue: typeString){
            self.type = type
        }
        if let branchLimit = dict["branch_limit"] as? Int {
            self.branchLimit = branchLimit
        }
        if let accountLimit = dict["account_limit"] as? Int {
            self.accountLimit = accountLimit
        }
        if let branchDigitLimit = dict["branch_digit_limit"] as? Int {
            self.branchDigitLimit = branchDigitLimit
        }
        if let type = dict["branch_digit_type"] as? String {
            self.branchDigitKeyboardType = type == "number" ? .numberPad : .default
        }
        if let type = dict["account_digit_type"] as? String {
            self.accountDigitKeyboardType = type == "number" ? .numberPad : .default
        }
        if let branchDigitEnabled = dict["branch_digit_enabled"] as? Bool {
            self.isBranchDigitEnabled = branchDigitEnabled
        }
        
        if let accountTypes = dict["account_types"] as? [[AnyHashable: Any]] {
            self.accountTypes.removeAll()
            for type in accountTypes {
                if let value = type["value"] as? String, let label = type["label"] as? String {
                    self.accountTypes.append( PPBankAccountType(label: label, value: value) )
                }
            }
        }
        
        super.init()
    }
}

public final class PPBankAccountType: NSObject {
    
    public var label: String
    public var value: String
    
    public init(label: String, value: String) {
        self.label = label
        self.value = value
        super.init()
    }
}
