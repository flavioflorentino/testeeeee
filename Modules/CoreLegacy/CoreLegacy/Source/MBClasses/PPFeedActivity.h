//
//  PPFeedActivity.h
//  PicPay
//
//  Created by Diogo Carneiro on 02/09/13.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PPContact.h"

typedef enum FeedAction : NSUInteger
{
	FeedActionTransaction = 1,
	FeedActionTransactionCancel = 2,
	FeedActionP2pSent = 3,
	FeedActionP2pReceived = 4,
	FeedActionWithdrawal = 5,
	FeedActionWithdrawalCancel = 6,
	FeedActionReward = 7,
	FeedActionAcquisition = 8,
	FeedActionMgm = 9,
	FeedActionP2pSentCancelled = 10,
	FeedActionP2pReceivedCancelled = 11,
	FeedActionP2pSentHold = 15,
	FeedActionP2pReceivedHold= 16,
    FeedActionP2pCancelRequested=21
} FeedAction;

typedef enum PPFeedActivityMoneyDirection : NSUInteger
{
	PPFeedActivityMoneyDirectionOutgoing=1,
	PPFeedActivityMoneyDirectionIncoming
} PPFeedActivityMoneyDirection;

@protocol PPFeedActivityDelegate <NSObject>

@optional

- (void)feedActivityImageDownloadDidFinish;

@end

@interface PPFeedActivity : NSObject{
	UIImageView *auxImageView; //ImageView only used to cache the remote image
	UIImage *image;
}

@property (strong, nonatomic) id<PPFeedActivityDelegate> delegate;

@property BOOL downloadInProgress;
@property BOOL whomIsPicPayUser;

@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *subtitle;
@property (strong, nonatomic) UIColor *subtitleColor;
@property (strong, nonatomic) NSString *whenString;
@property (strong, nonatomic) NSString *type; //transaction, p2ptransaction etc.
@property (strong, nonatomic) NSString *valueString;
@property (strong, nonatomic) NSString *who;
@property (strong, nonatomic) NSString *whom;
@property (strong, nonatomic) NSString *whom_id;
@property (strong, nonatomic) NSString *imageUrl;
@property (strong, nonatomic) PPContact *contact;
@property NSInteger actionId;

@property BOOL businessTransaction;

@property (strong, nonatomic) NSString *refererId;

@property PPFeedActivityMoneyDirection moneyDirection;

- (BOOL)isCancelled;
- (BOOL)isCancelRequested;
- (BOOL)isUnderReview;
- (UIImage *)image;
- (void)setSubtitleColorWithHex:(NSString *)hexString;
//- (void)downloadImageWithUrl:(NSString *)imgUrl;

@end
