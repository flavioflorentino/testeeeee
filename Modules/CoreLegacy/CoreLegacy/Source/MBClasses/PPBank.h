//
//  PPBank.h
//  PicPay
//
//  Created by Diogo Carneiro on 20/08/13.
//
//

#import <Foundation/Foundation.h>

@class PPBankAccountForm;

@interface PPBank : NSObject <NSCopying>

@property (strong, nonatomic) NSString * _Nullable wsId;
@property (strong, nonatomic) NSString * _Nullable name;
@property (strong, nonatomic) NSString * _Nullable imageUrl;
@property (strong, nonatomic) PPBankAccountForm * _Nullable form;
@property NSDecimalNumber * _Nullable withdrawalFeeValue;

- (NSString *_Nonnull)displayName;
- (instancetype _Nonnull) initWithDictionary:(NSDictionary * _Nonnull) dictionary;
- (BOOL)isOriginalAccount;
    
@end
