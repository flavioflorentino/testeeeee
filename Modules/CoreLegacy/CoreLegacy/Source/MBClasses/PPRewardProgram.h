#import <Foundation/Foundation.h>

@interface PPRewardProgram : NSObject

@property int progress;
@property (strong, nonatomic) NSString *progressDescription;
@property (strong, nonatomic) NSString *progressBarMin;
@property (strong, nonatomic) NSString *progressBarMax;
@property (strong, nonatomic) NSString *programDescription;


@end
