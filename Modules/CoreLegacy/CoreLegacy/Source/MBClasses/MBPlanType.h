#import <Foundation/Foundation.h>

@interface MBPlanType : NSObject{
	char planTypeId;
}

@property BOOL needAddress;

- (void)setPlanTypeId:(char)plan_type_id;
- (NSString *)planTypeIdToString;
- (BOOL)isKindOfPlan:(char)plan_type_id;

@end
