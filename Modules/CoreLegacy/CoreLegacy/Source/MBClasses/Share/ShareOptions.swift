//
//  ShareOptions.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 22/05/17.
//
//

import Foundation
import UI

public enum ShareOptions: String {
    case facebook = "Facebook"
    case twitter = "Twitter"
    case whatsapp = "Whatsapp"
    case sms = "SMS"
    case email = "Email"
    case link = "Copiar link"
    
    public static let availableOptions: [ShareOptions] = [.facebook, .whatsapp, .email]
    
    public func image() -> UIImage? {
        return UIImage(named: "share_\(self.rawValue.lowercased().replacingOccurrences(of: " ", with: "_"))")
    }
    
    public func backgroundcolor() -> UIColor {
        switch self {
        case .facebook:
            return Palette.SocialMediaColor.facebook.color
        case .twitter:
            return Palette.SocialMediaColor.twitter.color
        case .whatsapp:
            return Palette.SocialMediaColor.whatsapp.color
        case .sms:
            return Palette.ppColorAttentive400.color
        case .email:
            return Palette.ppColorNegative500.color
        case .link:
            return Palette.ppColorGrayscale500.color
        }
    }
    
    public func shareText() -> String {
        switch self {
        case .facebook:
            return "Compartilhar no Facebook"
        case .twitter:
            return "Publicar no Twitter"
        case .whatsapp:
            return "Convidar via Whatsapp"
        case .sms:
            return "Convidar via SMS"
        case .email:
            return "Convidar via e-mail"
        case .link:
            return "Copiar link de indicação"
        }
    }
}
