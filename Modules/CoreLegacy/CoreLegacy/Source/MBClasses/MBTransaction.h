//
//  MBTransaction.h
//  MoBuy
//
//  Created by Joao victor V lana on 22/08/12.
//
//

#import <Foundation/Foundation.h>
#import "PPRewardProgram.h"
#import "MBPlanType.h"

typedef NS_ENUM(NSInteger, PPReceiptCategory) {
    PPReceiptCategoryRegular = 1,
    PPReceiptCategoryParking = 2
};

@interface MBTransaction : NSObject

@property NSInteger WsId;
@property BOOL justCreated;

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSDecimalNumber * frete;
@property (nonatomic, retain) NSString * seller_name;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSDecimalNumber * subtotal;
@property (nonatomic, retain) NSDecimalNumber * total;
//@property (nonatomic, retain) NSDecimalNumber * credit;
@property (nonatomic, retain) NSDecimalNumber * credit_picpay;
@property (nonatomic, retain) NSDecimalNumber * credit_seller;
@property (nonatomic, retain) NSMutableArray *items;
@property (nonatomic, retain) NSDecimalNumber *surcharge;

@property (nonatomic, retain) NSDecimalNumber *chargedFromCreditCard;

//filled when plan_type is E
@property (nonatomic, retain) NSString *event_name;
@property (nonatomic, retain) NSString *event_date;
@property (nonatomic, retain) NSString *event_location;
@property (nonatomic, retain) NSString *event_opening;
@property (nonatomic, retain) NSString *event_ticket_text;
@property BOOL show_tickets;
@property (nonatomic, retain) NSString *show_tickets_error;

//filled when plan_type is A
@property (nonatomic, retain) NSString *seller_logo_url;
@property (nonatomic, retain) PPRewardProgram *rewardProgram;

@property (nonatomic, retain) MBPlanType *plan_type;

@property (nonatomic, retain) NSString *usedCreditCard;
@property (nonatomic, retain) NSString *shippingAddress;

// promotional thinks
@property (nonatomic, retain) NSString *congratulation;

@property NSInteger quantidadeParcelas;
@property (nonatomic, retain) NSDecimalNumber *valorParcela;
@property BOOL isCancelled;

@property (strong, nonatomic) NSString *shareText;
@property (strong, nonatomic) NSString *shareUrl;

@property (strong, nonatomic) NSString *receiptText;
@property NSInteger receiptCategory;
@property (strong, nonatomic) NSObject *receiptCategoryData;
@property (strong, nonatomic) NSDictionary *thirdPartyData;

@property (strong, nonatomic) NSArray *receiptItems;

@end
