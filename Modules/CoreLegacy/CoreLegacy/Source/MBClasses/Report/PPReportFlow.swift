//
//  PPReport.swift
//  PicPay
//
//  ☝️☁️ Created by Luiz Henrique Guimarães on 26/06/17.
//
//

import Foundation

public final class PPReportFlow: NSObject {
    
    public enum ScreenType: String {
        case begin = "REPORT_BEGIN"
        case subitems = "REPORT_SUBITEMS"
        case end = "REPORT_END"
        case message = "REPORT_MESSAGE"
        case unknown = "unknown"
    }
    
    public var title: String = ""
    public var screenTitle: String = ""
    public var screenType: ScreenType = .unknown
    public var text: String = ""
    public var items: [PPReportFlow] = []
    public var next: PPReportFlow?
    public var reportType: PPReportRequest.type?
    public var buttonText: String?
    public var action: String?
    public var tag: String?
    
    public init(dictionary d:[AnyHashable: Any]){
        self.title = d["title"] as? String ?? ""
        self.screenTitle = d["screen_title"] as? String ?? ""
        self.text = d["description"] as? String ?? ""
        
        if let type =  d["screen_type"] as? String, let screenType = ScreenType(rawValue: type) {
            self.screenType = screenType
        }
        
        if let title = d["item"] as? String {
            self.title = title
        }
        
        if let tag = d["tag"] as? String {
            self.tag = tag
        }
        
        if let items = d["items"] as? [[AnyHashable: Any]] {
            for i in items {
                self.items.append(PPReportFlow(dictionary: i))
            }
        }
        
        if let items = d["subitems"] as? [[AnyHashable: Any]] {
            for i in items {
                self.items.append(PPReportFlow(dictionary: i))
            }
        }
        
        if let nextScreenData = d["screen"] as? [AnyHashable: Any] {
            self.next = PPReportFlow(dictionary: nextScreenData)
        }
        
        if let button = d["button"] as? String {
            self.buttonText = button
        }
        
        if let action = d["action"] as? String {
            self.action = action
        }
        
        super.init()
    }
    
}
