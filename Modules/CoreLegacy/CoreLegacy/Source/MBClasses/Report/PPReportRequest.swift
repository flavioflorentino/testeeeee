//
//  PPReportRequest.swift
//  PicPay
//
//  ☝️☁️ Created by Luiz Henrique Guimarães on 28/06/17.
//
//

import UIKit

public final class PPReportRequest: NSObject {
    
    public enum type: String {
        case consumer = "CONSUMER"
        case comment = "COMMENT"
        case p2pFeedItem = "FEED"
    }
    
    public var reportType: PPReportRequest.type
    public var reportedId: String
    public var message: String?
    public var action: String?
    public var tag: String?
    
    public init(reportType: PPReportRequest.type, reportedId: String, message: String? = nil, action: String? = nil, tag: String? = nil) {
        self.reportedId = reportedId
        self.reportType = reportType
        self.message = message
        self.action = action
        self.tag = tag
        super.init()
    }
    
    public func toDictionary() -> [AnyHashable: Any] {
        var d = [AnyHashable: Any]()
        d["type"] = reportType.rawValue
        d["reported_id"] = reportedId
        
        if let message = message, message != "" {
            d["message"] = message
        }
        if let action = action, action != "" {
            d["action"] = action
        }
        if let tag = tag, tag != "" {
            d["tag"] = tag
        }
        return d
    }
}
