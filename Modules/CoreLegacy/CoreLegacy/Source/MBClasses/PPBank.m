//
//  PPBank.m
//  PicPay
//
//  Created by Diogo Carneiro on 20/08/13.
//
//

#import "PPBank.h"
#import <CoreLegacy/CoreLegacy-Swift.h>

@implementation PPBank

- (NSString *)displayName{
	return [NSString stringWithFormat:@"%@ - %@", [self wsId], [self name]];
}

- (instancetype) initWithDictionary:(NSDictionary *) dictionary {
    self = [super init];
    if (self != nil) {
        if([dictionary valueForKey:@"code"] != nil && [dictionary valueForKey:@"code"] != [NSNull null]){
            self.wsId = (NSString *) [NSString stringWithFormat:@"%@", [dictionary valueForKey:@"code"]];
        }
        if([dictionary valueForKey:@"id"] != nil && [dictionary valueForKey:@"id"] != [NSNull null]){
            self.wsId = (NSString *) [NSString stringWithFormat:@"%@", [dictionary valueForKey:@"id"]];
        }
        
        if([dictionary valueForKey:@"name"] != nil && [dictionary valueForKey:@"name"] != [NSNull null]){
            self.name = (NSString *) [NSString stringWithFormat:@"%@", [dictionary valueForKey:@"name"]];
        }
        if([dictionary valueForKey:@"nome"] != nil && [dictionary valueForKey:@"nome"] != [NSNull null]){
            self.name = (NSString *) [NSString stringWithFormat:@"%@", [dictionary valueForKey:@"nome"]];
        }
        
        if([dictionary valueForKey:@"img_url"] != nil && [dictionary valueForKey:@"img_url"] != [NSNull null]){
            self.imageUrl = (NSString *) [NSString stringWithFormat:@"%@", [dictionary valueForKey:@"img_url"]];
        }
        if([dictionary valueForKey:@"form"] != nil && [[dictionary valueForKey:@"form"] isKindOfClass:[NSDictionary class]] ){
            self.form = [[PPBankAccountForm alloc] initWithDictionary: [dictionary valueForKey:@"form"]];
        }
        // WORKAROUND API ESTA ENVIANDO form_config NO LUGAR DE form
        // REMOVER QUANDO CORRIGIR
        if([dictionary valueForKey:@"form_config"] != nil && [[dictionary valueForKey:@"form_config"] isKindOfClass:[NSDictionary class]] ){
            self.form = [[PPBankAccountForm alloc] initWithDictionary: [dictionary valueForKey:@"form_config"]];
        }
        
        if([dictionary valueForKey:@"charges_consumer_fee"] != nil && [dictionary valueForKey:@"charges_consumer_fee"] != [NSNull null]){
            self.withdrawalFeeValue =  [dictionary valueForKey:@"charges_consumer_fee"];
        }
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    PPBank * bank = [[PPBank allocWithZone:zone] init];
    bank.wsId = [self.wsId copy];
    bank.name = [self.name copy];
    bank.imageUrl = [self.imageUrl copy];
    bank.form = self.form;
    bank.withdrawalFeeValue = [self.withdrawalFeeValue copy];
    return bank;
}

- (BOOL)isOriginalAccount {
    return [self.wsId isEqualToString:@"212"];
}

@end
