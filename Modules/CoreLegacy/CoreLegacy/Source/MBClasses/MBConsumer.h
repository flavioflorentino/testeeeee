//
//  MBConsumer.h
//  MoBuy
//
//  Created by Joao victor V lana on 08/08/12.
//
//

#import <Foundation/Foundation.h>
#import "PPBankAccount.h"
@interface MBConsumer : NSObject

@property NSInteger wsId;
@property NSDecimalNumber * _Nullable balance;
@property NSDecimalNumber * _Nullable withdrawBalance;
@property (strong, nonatomic, nullable) NSString *birth_date;
@property (strong, nonatomic, nullable) NSString *name;
@property (strong, nonatomic, nullable) NSString *name_mother;
@property (strong, nonatomic, nullable) NSString *email;
@property (strong, nonatomic, nullable) NSString *cpf;
@property (strong, nonatomic, nullable) NSString *share_hash;
@property BOOL businessAccount;
@property BOOL verifiedAccount;
@property BOOL emailConfirmed;
@property BOOL emailPending; //informa se há uma solicitação para alteração de email
@property (strong, nonatomic, nullable) NSMutableArray *consumerLabels;
@property (strong, nonatomic, nullable) PPBankAccount *bankAccount;
@property (strong, nonatomic, nullable) NSString *img_url;
@property (strong, nonatomic, nullable) NSString *verifiedPhoneNumber;
@property (strong, nonatomic, nullable) NSDictionary<NSString *, NSString *> *analyticsProperties;
@property BOOL bypass_sms;
@property BOOL isPicPayLover;
@property BOOL numericKeyboardForPassword;
@property (strong, nonatomic, nullable) NSString *username;
@property NSInteger following;
@property NSInteger followers;

@end
