//
//  PPMgmConfigs.h
//  PicPay
//
//  Created by Diogo Carneiro on 23/01/14.
//
//

#import <Foundation/Foundation.h>

@interface PPMgmConfigs : NSObject

@property BOOL mgmEnabled;
@property BOOL contactsEnabled;
@property BOOL feedEnabled;
@property BOOL profileEnabled;

@property (strong, nonatomic, nullable) NSString *contactsTitle;
@property (strong, nonatomic, nullable) NSString *contactsSubtitle;
@property (strong, nonatomic, nullable) NSString *contactsImgUrl;

@property (strong, nonatomic, nullable) NSString *feedTitle;
@property (strong, nonatomic, nullable) NSString *feedSubtitle;

@property (strong, nonatomic, nullable) NSString *mgmScreenText;
@property (strong, nonatomic, nullable) NSString *mgmScreenButtonTitle;
@property (strong, nonatomic, nullable) NSString *mgmScreenTitle;
@property (strong, nonatomic, nullable) NSString *mgmScreenMainTitle;
@property (strong, nonatomic, nullable) NSString *mgmScreenSubtitle;
@property (strong, nonatomic, nullable) NSString *mgmScreenSubtitle2;//air bnb like
@property (strong, nonatomic, nullable) NSString *mgmScreenImgUrl;
@property (strong, nonatomic, nullable) NSString *mgmScreenWarning;
@property (strong, nonatomic, nullable) NSString *mgmScreenCompletionAlert;

@property (strong, nonatomic, nullable) NSString *text;
@property (strong, nonatomic, nullable) NSString *shareLink;
@property (strong, nonatomic, nullable) NSString *shareText;
@property (strong, nonatomic, nullable) NSString *shareTextFacebook;
@property (strong, nonatomic, nullable) NSString *shareTextShort;

@property (strong, nonatomic, nullable) NSString *textSuccess;

@property (strong, nonatomic, nullable) NSString *mgmProfileHeader;
@property (strong, nonatomic, nullable) NSString *mgmProfileLine1;
@property (strong, nonatomic, nullable) NSString *mgmProfileLine2;
@property (strong, nonatomic, nullable) NSString *mgmScreenTextShareScreen;

@property (strong, nonatomic, nullable) NSString *profileMgmCellTitle;

@property (strong, nonatomic, nullable) NSString *mgbText;
@property (strong, nonatomic, nullable) NSString *mgbShareLink;
@property (strong, nonatomic, nullable) NSString *mgbShareText;
@property (strong, nonatomic, nullable) NSString *mgbShareTextFacebook;
@property (strong, nonatomic, nullable) NSString *mgbShareTextShort;
@property (strong, nonatomic, nullable) NSString *mgbScreenTextShare;
@property (strong, nonatomic, nullable) NSString *mgbScreenShortTextShare;
@property (strong, nonatomic, nullable) NSString *mgbSubtitle;
@property (strong, nonatomic, nullable) NSString *mgbButton;

- (instancetype _Nonnull)initWith:(NSDictionary * _Nonnull)json;

@end
