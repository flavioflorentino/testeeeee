//
//  PPWithdrawal.m
//  PicPay
//
//  Created by Diogo Carneiro on 26/08/13.
//
//

#import "PPWithdrawal.h"
#import <CoreLegacy/CoreLegacy-Swift.h>


@implementation PPWithdrawal

#ifdef DEBUG
#define DebugLog( s, ... ) NSLog( @"<%@:(%d)> %@", [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#define DebugLogWs( s, ... ) NSLog( @"%@", [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#else
#define DebugLog( s, ... )
#define DebugLogWs( s, ... )
#endif

- (instancetype)initWithDictionary:(NSDictionary *) dict {
    self = [super init];
     if (self != nil) {
        NSDictionary *withdrawalJson = [dict valueForKey:@"Withdrawal"];
        self.statusCode = [[withdrawalJson valueForKey:@"status_draw"] isKindOfClass:NSString.class] ? [withdrawalJson valueForKey:@"status_draw"] : @"";
        self.statusName = [[withdrawalJson valueForKey:@"status_name"] isKindOfClass:NSString.class] ? [withdrawalJson valueForKey:@"status_name"] : @"";
        self.value = [NSDecimalNumber decimalNumberWithDecimal:[[withdrawalJson valueForKey:@"value_draw"] decimalValue]];
        self.windowInfo = [[CashoutScheduleAlert alloc] initWithJson: [dict valueForKey:@"window_info"]];
                           
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [dateFormat setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
        //[dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        
        self.date = [dateFormat dateFromString:[withdrawalJson valueForKey:@"request_date"]];
        
        self.estimatedCompletionDate = [[withdrawalJson valueForKey:@"estimated_completion_date"] isKindOfClass:NSString.class] ? [dateFormat dateFromString:[withdrawalJson valueForKey:@"estimated_completion_date"]] : nil;
        self.bankAccountDescription = [[withdrawalJson valueForKey:@"bank_account_description"] isKindOfClass:NSString.class] ? [withdrawalJson valueForKey:@"bank_account_description"] : @"";
        self.text = [[withdrawalJson valueForKey:@"text"] isKindOfClass:NSString.class] ? [withdrawalJson valueForKey:@"text"] : @"";
        self.textDisclaimer = [[withdrawalJson valueForKey:@"text_disclaimer"] isKindOfClass:NSString.class] ? [withdrawalJson valueForKey:@"text_disclaimer"] : @"";
        
        self.reasonId    = [[withdrawalJson valueForKey:@"reason_id"] isKindOfClass:NSString.class] ? [withdrawalJson valueForKey:@"reason_id"] : @"";
        
        @try {
            self.isCancelled = [[withdrawalJson valueForKey:@"is_cancelled"] boolValue] ? true : false;
            if (self.isCancelled) {
                
                self.reasonDescription    = [[[withdrawalJson valueForKey:@"Reason"] valueForKey:@"reason"] isKindOfClass:NSString.class] ? [[withdrawalJson valueForKey:@"Reason"] valueForKey:@"reason"] : @"";
                self.requireAccountChange = [[dict valueForKey:@"require_account_change"] boolValue];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Error ao criar PPWithdrawal com dictionary");
        }
    }
    return self;
}

@end
