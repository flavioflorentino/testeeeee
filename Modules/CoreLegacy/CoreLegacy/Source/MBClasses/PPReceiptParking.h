//
//  PPReceiptParking.h
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 09/06/16.
//
//

#import <Foundation/Foundation.h>

@interface PPReceiptParking : NSObject

@property (strong, nonatomic) NSString *licensePlate;
@property (strong, nonatomic) NSString *expiresDay;
@property (strong, nonatomic) NSString *expiresHour;

@end
