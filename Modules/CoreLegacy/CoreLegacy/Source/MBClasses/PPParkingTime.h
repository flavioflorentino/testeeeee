//
//  PPParkingTime.h
//  PicPay
//
//  Created by Diogo Carneiro on 02/12/15.
//
//

#import <Foundation/Foundation.h>

@interface PPParkingTime : NSObject

@property (strong, nonatomic) NSString *pickerString;
@property (strong, nonatomic) NSDate *expiryDate;
@property (strong, nonatomic) NSString *expiryDateString;
@property (strong, nonatomic) NSDecimalNumber *totalValue;
@property BOOL selected;

@end
