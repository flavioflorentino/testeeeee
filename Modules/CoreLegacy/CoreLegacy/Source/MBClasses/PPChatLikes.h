//
//  PPChatLikes.h
//  PicPay
//
//  Created by Diogo Carneiro on 01/08/14.
//
//

#import <Foundation/Foundation.h>

@interface PPChatLikes : NSObject

@property BOOL liked;
@property (strong, nonatomic) NSString *names;

@end
