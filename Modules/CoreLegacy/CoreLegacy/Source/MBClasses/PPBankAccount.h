//
//  PPBankAccount.h
//  PicPay
//
//  Created by Diogo Carneiro on 20/08/13.
//
//

#import <Foundation/Foundation.h>
#import "PPBank.h"

@interface PPBankAccount : NSObject <NSCopying>

@property (strong, nonatomic) NSString * _Nullable wsId;
@property (strong, nonatomic) PPBank * _Nullable bank;
@property (strong, nonatomic) NSString * _Nullable accountType; //C = conta corrente | P = poupança (Will you use char? NO! http://bit.ly/17Keo2E )
@property (strong, nonatomic) NSString * _Nullable accountNumber;
@property (strong, nonatomic) NSString * _Nullable accountDigit;
@property (strong, nonatomic) NSString * _Nullable agencyNumber;
@property (strong, nonatomic) NSString * _Nullable agencyDigit;
@property (strong, nonatomic) NSString * _Nullable statusName;
@property (strong, nonatomic) NSString * _Nullable cpf;
@property (strong, nonatomic) NSString * _Nullable recipientName;

- (instancetype _Nullable ) initWithDictionary:(NSDictionary * _Nullable) dictionary;

-(id _Nonnull ) copyWithZone: (NSZone *_Nullable) zone;

@end
