import Foundation

public final class PPPaymentConfig: NSObject {
    @objc public var interest: NSNumber?
    @objc public var interestCoefficient: NSDecimalNumber?
    @objc public var minQuotaValue: NSDecimalNumber?
    @objc public var installmentsBusiness: [Any]? = []
    @objc public var maxQuotaQuantity: Int = 0
    /*
     
    Do not, for the love of god use this Shared Instance in a new code!
     
    This is here to comply with legacy code.
    PPPaymentManager and PPPaymenteCalcutor used to use a shared instance from
    AppParameter, since the function that updated that shared instance was called
    16 times trought the code base, I was not able to remove this in a timely
    manner.
     
    */
    @objc
    public static let shared: PPPaymentConfig = PPPaymentConfig()
    
    // Mark: Public Methods
    
    @objc
    public func update(interest: NSNumber?, minQuotaValue: NSDecimalNumber?, maxQuotaQuantity: Int) {
        
        self.interest = interest
        self.setInterestCoefficient(value: interest?.decimalValue)
        self.minQuotaValue = minQuotaValue
        self.maxQuotaQuantity = maxQuotaQuantity
    }
    
    @objc
    public func updateInstallmentsBusiness(_ value: [Any]?) {
        self.installmentsBusiness = value
    }
    
    // Mark: Private Methods
    
    private func setInterestCoefficient(value: Decimal?) {
        guard let value = value else {
            return
        }
        
        let oneFraction = NSDecimalNumber(value: 0.01)
        let one = NSDecimalNumber(integerLiteral: 1)
        interestCoefficient = NSDecimalNumber(decimal: value).multiplying(by: oneFraction).adding(one)
    }
}
