import Foundation

extension NSString {
    var sanitized: String {
        // TODO: REMOVER QUANDO A APPLE CORRIGIR BUG MALDITO
        return self.replacingOccurrences(of: "జ్ఞ‌ా", with: "xx").replacingOccurrences(of: "జ్ఞ&zwnj;ా", with: "xx").replacingOccurrences(of: "జ్ఞ", with: "x").replacingOccurrences(of: ";ా", with: "x")
    }
}
