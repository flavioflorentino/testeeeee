#import "AppParameters.h"
#import <sys/utsname.h>
#import <AdSupport/AdSupport.h>
@import Core;

@implementation AppParameters

@synthesize userToken, searchFilters, updatingSearchFilters;

static AppParameters *_globalParameters = nil;

+ (AppParameters *)globalParameters{
	@synchronized([AppParameters class])
	{
		if (!_globalParameters){
			(void)[[self alloc] init];
		}
		
		return _globalParameters;
	}
	
	return nil;
}

- (id) init {
	self = [super init];
	if (self != nil) {
		_mixpanelABTestingValues = [[NSMutableDictionary alloc] init];
	}
	return self;
}

- (void)setDevelopmentApiHost:(NSString *)apiHost{
	developmentApiHost = apiHost;
    [[KVStore new] setString:apiHost with:KVKeyDevelopmentApiHost];}

- (NSString *)developmentApiHost{
	if (!developmentApiHost) {
		developmentApiHost = [[KVStore new] stringFor:KVKeyDevelopmentApiHost];
	}
	
	return developmentApiHost;
}

+(id)alloc
{
	@synchronized([AppParameters class])
	{
		NSAssert(_globalParameters == nil, @"Attempted to allocate a second instance of a singleton (parameters).");
		_globalParameters = [super alloc];
		
		return _globalParameters;
	}
	
	return nil;
}

- (void)loadPaymentMethods {
    
}

- (void)setPhoneVerified:(BOOL)verified{
    [[KVStore new] setBool:verified with:KVKeyIsPhoneVerified];
}

- (void)setPhoneVerifiedLocally:(BOOL)verified{
    [[KVStore new] setBool:verified with:KVKeyIsPhoneVerified];
}

- (BOOL)phoneVerified {
    return [[KVStore new] boolFor:KVKeyIsPhoneVerified];
}

- (void)setVerifiedNumber:(NSString *)number {
    [[KVStore new] setString:number with:KVKeyVerifiedNumber];
}

- (NSString * _Nullable)verifiedNumber {
    return [[KVStore new] dumbObjCForKey:KVKeyVerifiedNumber];
}

- (BOOL)fourInchesDisplay{
	return ([[UIScreen mainScreen] bounds].size.height == 568)?YES:NO;
}

- (BOOL)threeAndAHalfInchesDisplay{
	return ([[UIScreen mainScreen] bounds].size.height == 480)?YES:NO;
}

- (void)setAuthInfoUpdated:(BOOL)updated {
    [[KVStore new] setBool:updated with:KVKeyAuthInfoUpdated];
}

- (BOOL)authInfoUpdated {
    return [[KVStore new] boolFor:KVKeyAuthInfoUpdated];
}

- (NSString *)appVersion{
	return [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
}

- (BOOL)notFirstTimeOnApp {
    return [[KVStore new] boolFor:KVKeyNotFirstTimeOnApp];
}

- (void)setNotFirstTimeOnApp {
    [[KVStore new] setBool:YES with:KVKeyNotFirstTimeOnApp];
}

- (BOOL)hasAtLeastOneFeedOnce {
    return [[KVStore new] boolFor:KVKeyHasAtLeastOneFeedOnce];
}

- (void)setHasAtLeastOneFeedOnce:(BOOL)to {
    [[KVStore new] setBool:to with:KVKeyHasAtLeastOneFeedOnce];
}

# pragma mark - Register Data
// Phone Verification Methods

- (NSInteger)getPhoneVerificationCallDelay {
    return [[KVStore new] intFor:KVKeyPhoneVerificationCallDelay];
}

- (void)setPhoneVerificationCallDelay:(NSInteger)callDelay {
    [[KVStore new] setInt:callDelay with:KVKeyPhoneVerificationCallDelay];
}

- (BOOL)isEnabledPhoneVerificationCall{
    return [[KVStore new] boolFor:KVKeyIsEnabledPhoneVerificationCall];
}

- (void)setEnabledPhoneVerificationCall:(BOOL)enabled {
    [[KVStore new] setBool:enabled with:KVKeyIsEnabledPhoneVerificationCall];
}

- (NSInteger)getPhoneVerificationSMSDelay {
    return [[KVStore new] intFor:KVKeyPhoneVerificationSMSDelay];
}

- (void)setPhoneVerificationSMSDelay:(NSInteger)smsDelay {
    [[KVStore new] setInt:smsDelay with:KVKeyPhoneVerificationSMSDelay];
}

- (NSInteger)getPhoneVerificationWhatsAppDelay {
    return [[KVStore new] intFor:KVKeyPhoneVerificationWhatsAppDelay];
}

- (void)setPhoneVerificationWhatsAppDelay:(NSInteger)whatsAppDelay {
    [[KVStore new] setInt:whatsAppDelay with:KVKeyPhoneVerificationWhatsAppDelay];
}

- (NSDate *)getPhoneVerificationSentDate {
    return [[KVStore new] dumbObjCForKey:KVKeyPhoneVerificationSentDate];
}

- (void)setPhoneVerificationSentDate:(NSDate *)date {
    [[KVStore new] setDate:date with:KVKeyPhoneVerificationSentDate];
}

- (void)setPhoneVerificationDDD:(NSString *)ddd {
    [[KVStore new] setString:ddd with:KVKeyPhoneVerificationDDD];
}

- (NSString *)getPhoneVerificationDDD {
    return [[KVStore new] stringFor:KVKeyPhoneVerificationDDD];
}

- (void)setPhoneVerificationNumber:(NSString *) number {
    [[KVStore new] setString:number with:KVKeyPhoneVerificationNumber];
}

- (NSString *)getPhoneVerificationNumber {
    return [[KVStore new] stringFor:KVKeyPhoneVerificationNumber];
}

# pragma mark - Others

/*!
 * Save the flag to check if user has already responded to social onboarding
 */
- (void)setOnboardSocial:(BOOL)value {
    [[KVStore new] setBool:value with:KVKeyOnboardSocial];
}

/*!
 * Get the flag to check if user has already responded to social onboarding
 */
- (BOOL)getOnboardSocial{
    return [[KVStore new] boolFor:KVKeyOnboardSocial];
}

@end
