import Foundation

@objcMembers
public final class CashoutScheduleAlert: NSObject {
    public let warnUser: Bool
    public let title: String
    public let text: String
    
    public init(warnUser: Bool, title: String, text: String) {
        self.warnUser = warnUser
        self.title = title
        self.text = text
        super.init()
    }
    
    public convenience init?(json: [String:Any]) {
        
        guard 
            let warnUser = json["warn_user"] as? Bool,
            let title = json["title"] as? String,
            let text = json["text"] as? String else {
                return nil
        }
        self.init(warnUser: warnUser, title: title, text: text)
        
    }
}
