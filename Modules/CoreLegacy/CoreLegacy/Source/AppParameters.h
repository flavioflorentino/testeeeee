#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "PPMgmConfigs.h"

@protocol PhoneVerificationParametersProvider
- (NSInteger) getPhoneVerificationCallDelay;
- (void) setPhoneVerificationCallDelay:(NSInteger) callDelay;
- (BOOL) isEnabledPhoneVerificationCall;
- (void) setEnabledPhoneVerificationCall:(BOOL) enabled;
- (NSInteger) getPhoneVerificationSMSDelay;
- (void) setPhoneVerificationSMSDelay:(NSInteger) callDelay;
- (NSInteger) getPhoneVerificationWhatsAppDelay;
- (void)setPhoneVerificationWhatsAppDelay:(NSInteger) whatsAppDelay;
- (NSDate *_Nullable) getPhoneVerificationSentDate;
- (void) setPhoneVerificationSentDate:(NSDate *_Nullable) date;
- (void) setPhoneVerificationDDD: (NSString *_Nullable) ddd;
- (NSString *_Nullable) getPhoneVerificationDDD;
- (void) setPhoneVerificationNumber: (NSString *_Nullable) number;
- (NSString *_Nullable) getPhoneVerificationNumber;
@end

@protocol AppSessionParametersProvider
- (BOOL)notFirstTimeOnApp;
- (void)setNotFirstTimeOnApp;
@end

@interface AppParameters : NSObject<CLLocationManagerDelegate, PhoneVerificationParametersProvider, AppSessionParametersProvider> {
	NSInteger unreadNotificationsCount;
    NSString *picPayId;
    NSString *installationId;
	NSString *developmentApiHost;
}

@property BOOL sidebarMenuCanPan;

@property BOOL showAntiFraudChecklist;

@property (strong, nonatomic, nullable) NSString *currentOpenFeedItem;

@property (strong, nonatomic) PPMgmConfigs * _Nullable mgmConfigs;

@property (strong, nonatomic) NSMutableDictionary * _Nullable mixpanelABTestingValues;

@property (strong, nonatomic) UIViewController * _Nullable initialViewController;

@property (strong, nonatomic) NSString * _Nullable userToken;
@property (strong, nonatomic) NSString * _Nullable pushToken;

//@property (strong, nonatomic) MBConsumer * _Nullable localConsumer;
@property BOOL profileImageDownloadInProgress;

@property (nonatomic, assign) BOOL isLandscape;

@property (strong, nonatomic) NSArray * _Nullable deepLinkingParams;

@property (strong, nonatomic) NSString * _Nullable remoteRechargeHash;

@property BOOL isUpdatingContacts;

@property (strong, nonatomic) NSArray * _Nullable searchFilters;
@property BOOL updatingSearchFilters;

@property BOOL payment_from_explore_enabled;

@property NSInteger displayCreditCardOnboardDelay;

@property (strong, nonatomic) NSDate * _Nullable referalCodeBannerDismissDate;


- (void)setDevelopmentApiHost:(NSString *_Nullable)apiHost;
- (NSString *_Nullable)developmentApiHost;

//return the Singleton Parameters
+ (AppParameters *_Nonnull)globalParameters;

- (void)setAuthInfoUpdated:(BOOL)updated;

- (BOOL)authInfoUpdated;

- (BOOL)fourInchesDisplay;

- (BOOL)threeAndAHalfInchesDisplay;
 
- (void)setPhoneVerified:(BOOL)verified;
- (BOOL)phoneVerified;

- (void)setVerifiedNumber:(NSString *_Nullable)number;
- (NSString * _Nullable)verifiedNumber;

- (NSString *_Nullable)appVersion;

- (BOOL)notFirstTimeOnApp;
- (void)setNotFirstTimeOnApp;

- (BOOL)hasAtLeastOneFeedOnce;
- (void)setHasAtLeastOneFeedOnce:(BOOL)to;

# pragma mark - Others

/*!
 * Save the flag to check if user has already responded to social onboarding
 */
- (void) setOnboardSocial: (BOOL) value;

/*!
 * Get the flag to check if user has already responded to social onboarding
 */
- (BOOL) getOnboardSocial;

@end
