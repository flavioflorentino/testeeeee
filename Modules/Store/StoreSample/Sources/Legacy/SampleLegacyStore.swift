import UIKit
import Store

final class SampleLegacyStore: LegacyStoreHandler {
    func openReceipt(above viewController: UIViewController, with transactionId: String, type: String) { }
    
    func addressViewController(with content: StoreAddressScreenContent, onAddressSelected: @escaping (StoreAddressResult) -> Void) -> UIViewController {
        UIViewController()
    }
    
    func viewControllerForDigitalGood(with dictionary: [String: Any]) -> UIViewController? {
        UIViewController()
    }
    func routeToPayment(_ item: StorePayment, from viewController: UIViewController) { }
    func viewControllerToWebview(with url: URL) -> UIViewController {
        UIViewController()
    }
}
