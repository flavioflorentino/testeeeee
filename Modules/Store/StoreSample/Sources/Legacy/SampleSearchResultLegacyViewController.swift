import UIKit
import Store

final class SampleSearchResultLegacyViewController: UIViewController, SearchResultLegacyViewController {
    func searchTextChanged(text: String) {}
}
