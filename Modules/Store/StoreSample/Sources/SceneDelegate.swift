import UIKit
import Store

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        if let windowScene = scene as? UIWindowScene {
            let window = UIWindow(windowScene: windowScene)
            let homeStoreViewController = StoreHomeFactory.make(homeId: "STORE_HOME_V2", legacyStoreHandler: SampleLegacyStore(),
                                                                searchHeader: SampleSearchHeaderLegacyView(),
                                                                searchResult: SampleSearchResultLegacyViewController())
            window.rootViewController = UINavigationController(rootViewController: homeStoreViewController)
            self.window = window
            window.makeKeyAndVisible()
        }
    }
}
