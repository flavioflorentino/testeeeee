@testable import Store

final class LegacyStoreHandlerDummy: LegacyStoreHandler {
    func openReceipt(above viewController: UIViewController, with transactionId: String, type: String) {
        
    }
    
    func viewControllerForDigitalGood(with dictionary: [String: Any]) -> UIViewController? {
        return UIViewController()
    }
    
    func addressViewController(
        with content: StoreAddressScreenContent,
        onAddressSelected: @escaping (StoreAddressResult) -> Void
    ) -> UIViewController {
        return UIViewController()
    }
    
    func routeToPayment(_ item: StorePayment, from viewController: UIViewController) {
        
    }
    
    func viewControllerToWebview(with url: URL) -> UIViewController {
        return UIViewController()
    }
}
