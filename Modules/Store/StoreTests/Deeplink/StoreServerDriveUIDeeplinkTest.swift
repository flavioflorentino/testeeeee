import XCTest
@testable import Store

final class StoreServerDrivenUIDeeplinkTests: XCTestCase {
    func testStoreDeeplinkInstantiation_WhenUrlIsValid_shouldReturnInstaceWithScreenId() {
        //Given
        let basePath = "/store/v2"
        let screenId = "screenId"
        
        guard let validDeeplinkUrl = URL(string: "\(basePath)/\(screenId)") else {
            XCTFail("Could not create url for testing")
            return
        }
        
        let sut = StoreServerDriveUIDeeplink.init
          
        //When
        let result = sut(validDeeplinkUrl, [basePath])
        
        //Then
        XCTAssertNotNil(result)
        XCTAssertEqual(screenId, result?.screenId)
        XCTAssertEqual([:], result?.queryParameters)
    }
    
    func testStoreDeeplinkInstantiation_WhenUrlIsInvalid_shouldReturnNil() {
        //Given
        let basePath = "/store/v2"
        let wrongBasePath = "wrongBasePath"
        let screenId = "screenId"
        
        guard let invalidDeeplinkUrl = URL(string: "\(wrongBasePath)/\(screenId)") else {
            XCTFail("Could not create url for testing")
            return
        }
        
        let sut = StoreServerDriveUIDeeplink.init
        
        //When
        let result = sut(invalidDeeplinkUrl, [basePath])
        
        //Then
        XCTAssertNil(result)
    }
    
    func testStoreDeeplinkInstantiation_WhenThereIsNoScreenIdInUrl_shouldReturnNil() {
        //Given
        let basePath = "/store/v2"
        
        guard let invalidDeeplinkUrl = URL(string: basePath) else {
            XCTFail("Could not create url for testing")
            return
        }
        
        let sut = StoreServerDriveUIDeeplink.init
        
        //When
        let result = sut(invalidDeeplinkUrl, [basePath])
        
        //Then
        XCTAssertNil(result)
    }
    
    func testStoreDeeplinkInstantiation_WhenThereIsExtraParametersInUrl_shouldReturnNil() {
        //Given
        let basePath = "/store/v2"
        let parameters = "parameter1/parameter2"
        
        guard let invalidDeeplinkUrl = URL(string: "\(basePath)/\(parameters)") else {
            XCTFail("Could not create url for testing")
            return
        }
        
        let sut = StoreServerDriveUIDeeplink.init
        
        //When
        let result = sut(invalidDeeplinkUrl, [basePath])
        
        //Then
        XCTAssertNil(result)
    }
    
    func testStoreDeeplinkInstantiation_WhenUrlIsValidWithQueryParameters_shouldReturnInstaceWithScreenIdAndQueryParameters() {
        //Given
        let basePath = "/store/v2"
        let screenId = "screenId"
        let queryString = "param1=value1&param2=value2&param3=value3"
        
        guard let validDeeplinkUrl = URL(string: "\(basePath)/\(screenId)?\(queryString)") else {
            XCTFail("Could not create url for testing")
            return
        }
        
        let sut = StoreServerDriveUIDeeplink.init
        
        //When
        let result = sut(validDeeplinkUrl, [basePath])
        
        //Then
        XCTAssertNotNil(result)
        XCTAssertEqual(screenId, result?.screenId)
        XCTAssertEqual(basePath, result?.path)
        XCTAssertEqual([
            "param1": "value1",
            "param2": "value2",
            "param3": "value3",
        ], result?.queryParameters)
    }
    
    func testStoreDeeplinkInstantiation_WhenIsSecondaryBasePath_shouldReturnInstaceWithScreenId() {
        //Given
        let validBasePaths = ["/store/v2", "/compartilhar_loja"]
        let urlBasePath = "/compartilhar_loja"
        let screenId = "screenId"
        
        guard let validDeeplinkUrl = URL(string: "\(urlBasePath)/\(screenId)") else {
            XCTFail("Could not create url for testing")
            return
        }
        
        let sut = StoreServerDriveUIDeeplink.init
        
        //When
        let result = sut(validDeeplinkUrl, validBasePaths)
        
        //Then
        XCTAssertNotNil(result)
        XCTAssertEqual(screenId, result?.screenId)
        XCTAssertEqual([:], result?.queryParameters)
        XCTAssertEqual(urlBasePath, result?.path)
    }
}
