@testable import Store
import XCTest

final class StoreServerDrivenUIDeeplinkResolverTests: XCTestCase {
    
    func testResolverCanHandleDeeplink_WhenUrlIsValidAndUserIsAuthenticated_shouldReturnHandleable() {
        //Given
        let baseUrl = "/store/v2"
        let screenId = "screenId"
        let isAuthenticated = true
        
        guard let validDeeplinkUrl = URL(string: "\(baseUrl)/\(screenId)") else {
            XCTFail("Could not create url for testing")
            return
        }
        
        let sut = StoreServerDrivenUIDeeplinkResolver(
            with: LegacyStoreHandlerDummy(),
            paths: baseUrl
        )
        
        //When
        let result = sut.canHandle(
            url: validDeeplinkUrl,
            isAuthenticated: isAuthenticated
        )
        
        //Then
        XCTAssertEqual(result, .handleable)
    }
    
    func testResolverCanHandleDeeplink_WhenUrlIsValidAndUserIsNotAuthenticated_shouldReturnOnlyWithAuth() {
        //Given
        let baseUrl = "/store/v2"
        let screenId = "screenId"
        let isAuthenticated = false
        
        guard let validDeeplinkUrl = URL(string: "\(baseUrl)/\(screenId)") else {
            XCTFail("Could not create url for testing")
            return
        }
        
        let sut = StoreServerDrivenUIDeeplinkResolver(
            with: LegacyStoreHandlerDummy(),
            paths: baseUrl
        )
        
        //When
        let result = sut.canHandle(
            url: validDeeplinkUrl,
            isAuthenticated: isAuthenticated
        )
        
        //Then
        XCTAssertEqual(result, .onlyWithAuth)
    }
    
    func testResolverCanHandleDeeplink_WhenUrlIsInvalid_shouldReturnNotHandleable() {
        //Given
        let baseUrl = "/store/v2"
        let wrongBaseUrl = "wrongBaseUrl"
        let screenId = "screenId"
        let isAuthenticated = false
        
        guard let invalidDeeplinkUrl = URL(string: "\(wrongBaseUrl)/\(screenId)") else {
            XCTFail("Could not create url for testing")
            return
        }
        
        let sut = StoreServerDrivenUIDeeplinkResolver(
            with: LegacyStoreHandlerDummy(),
            paths: baseUrl
        )
        
        //When
        let result = sut.canHandle(
            url: invalidDeeplinkUrl,
            isAuthenticated: isAuthenticated
        )
        
        //Then
        XCTAssertEqual(result, .notHandleable)
    }
    
    func testResolverCanHandleDeeplink_WhenMissingPathParameter_shouldReturnNotHandleable() {
        //Given
        let baseUrl = "store/v2/"
        let isAuthenticated = false
        
        guard let invalidDeeplinkUrl = URL(string: baseUrl) else {
            XCTFail("Could not create url for testing")
            return
        }
        
        let sut = StoreServerDrivenUIDeeplinkResolver(
            with: LegacyStoreHandlerDummy(),
            paths: baseUrl
        )
        
        //When
        let result = sut.canHandle(
            url: invalidDeeplinkUrl,
            isAuthenticated: isAuthenticated
        )
        
        //Then
        XCTAssertEqual(result, .notHandleable)
    }
    
    func testResolverCanHandleDeeplink_WhenExtraPathParameter_shouldReturnNotHandleable() {
        //Given
        let baseUrl = "store/v2/"
        let pathParameters = "firstParameter/secondParameter"
        let isAuthenticated = false
        
        guard let invalidDeeplinkUrl = URL(string: "\(baseUrl)/\(pathParameters)") else {
            XCTFail("Could not create url for testing")
            return
        }
        
        let sut = StoreServerDrivenUIDeeplinkResolver(
            with: LegacyStoreHandlerDummy(),
            paths: baseUrl
        )
        
        //When
        let result = sut.canHandle(
            url: invalidDeeplinkUrl,
            isAuthenticated: isAuthenticated
        )
        
        //Then
        XCTAssertEqual(result, .notHandleable)
    }	
}
