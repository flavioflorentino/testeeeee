import XCTest
@testable import Store

final class StoreHomeFactoryTests: XCTestCase {
    func testMake_ShouldReturnTheRightViewController() {
        let viewController = StoreHomeFactory.make(
            homeId: "1234",
            legacyStoreHandler: LegacyStoreHandlerDummy(),
            searchHeader: SearchHeaderLegacyViewDummy(),
            searchResult: SearchResultLegacyViewControllerDummy())
        
        XCTAssertTrue(viewController is StoreHomeViewController)
    }
}
