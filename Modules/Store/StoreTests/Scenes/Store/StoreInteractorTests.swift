import Core
import XCTest
import AnalyticsModule
@testable import Store

final class StoreInteractorTests: XCTestCase {
    private var storeSpy = Store.Item("5d16600e2c5c0f0037111d32", "IFood")
    private var serviceStub = StoreServiceStub()
    private var presenterSpy = StorePresenterSpy()
    private var parserSpy = ServerDrivenUIParserSpy()
    private let analyticsSpy = AnalyticsSpy()
    private lazy var dependenciesMock = DependencyContainerMock(analyticsSpy)
    private var remoteActionServiceSpy = ServerDrivenUIServiceStub()
        
    private lazy var sut: StoreInteractor = {
        let sut = StoreInteractor(store: storeSpy,
                                  presenter: presenterSpy,
                                  service: serviceStub,
                                  remoteActionService: remoteActionServiceSpy,
                                  parser: parserSpy,
                                  dependencies: dependenciesMock)
        return sut
    }()
    
    func testLoadComponents_WhenServiceGetSuccess_ShouldDisplayComponents() throws {
        let data = try loadDataObject(fromJSON: FileName.store)
        parserSpy.decode(data)
        let components = parserSpy.getUIComponents()
        serviceStub.result = Result.success(data)
        
        sut.requestComponents()
        XCTAssertEqual(presenterSpy.componentsSpy.count, components.count)
        XCTAssertEqual(presenterSpy.didCallPresentComponents, 1)
    }
    
    func testLoadComponents_WhenServiceGetSuccessButWithInconsistentData_ShouldDisplayErrorMessage() throws {
        let data = try loadDataObject(fromJSON: FileName.store).json(deletingKeyPaths: "body")
        serviceStub.result = Result.success(data)
        sut.requestComponents()
        
        XCTAssertEqual(presenterSpy.componentsSpy.count, 0)
        XCTAssertEqual(presenterSpy.didCallPresentComponents, 0)
        
        XCTAssertEqual(presenterSpy.errorMessageSpy, "Tivemos uma falha no sistema e não conseguimos carregar essa página.")
        XCTAssertEqual(presenterSpy.didCallPresentError, 1)
    }
    
    func testLoadComponents_WhenServiceGetInternetError_ShouldDisplayInternetErrorMessage() {
        serviceStub.result = Result.failure(.connectionFailure)
        sut.requestComponents()
        
        XCTAssertEqual(presenterSpy.componentsSpy.count, 0)
        XCTAssertEqual(presenterSpy.didCallPresentComponents, 0)
        
        XCTAssertEqual(presenterSpy.errorMessageSpy, "Sem conexão com a internet")
        XCTAssertEqual(presenterSpy.didCallPresentError, 1)
    }
    
    func testSetStoreTitle_WhenViewControllerOpensStore_ShouldDisplayStoreTitle() throws {
        let data = try loadDataObject(fromJSON: FileName.store)
        parserSpy.decode(data)
        
        let title = parserSpy.getTitle()
        serviceStub.result = Result.success(data)
        
        sut.requestComponents()
        XCTAssertEqual(presenterSpy.storeTitleSpy, title)
        XCTAssertEqual(presenterSpy.didCallPresentToolbar, 1)
    }
    
    func testSetStoreNavigationRightButton_WhenViewControllerOpensStore_ShouldDisplayRightButton () throws {
        let data = try loadDataObject(fromJSON: FileName.store)
        parserSpy.decode(data)
        
        let button = parserSpy.parseNavigationBarRightButton()
        serviceStub.result = Result.success(data)
        
        sut.requestComponents()
        XCTAssertEqual(presenterSpy.rightButtonTitleSpy, button?.label)
        XCTAssertEqual(presenterSpy.rightButtonActionTypeSpy, button?.action?.type)
        XCTAssertEqual(presenterSpy.didCallPresentRightButton, 1)
    }
    
    func testStartLoading_WhenInteractorStartComponentServiceRequest_ShouldStartDisplayLoading() {
        // Given
        let defaultLoadingCalls: [Bool] = [true]
        
        // When
        sut.requestComponents()
        
        // Then
        XCTAssertTrue(presenterSpy.loadingStyleSpy == .default)
        XCTAssertEqual(presenterSpy.showDefaultLoadingReceivedInvocations, defaultLoadingCalls)
    }
    
    func testStopLoading_WhenInteractorFinishComponentServiceRequest_ShouldStartDisplayLoading() throws {
        // Given
        let data = try loadDataObject(fromJSON: FileName.store)
        serviceStub.result = Result.success(data)
        let defaultLoadingCalls: [Bool] = [true, false]
        
        // When
        sut.requestComponents()
        
        // Then
        XCTAssertTrue(presenterSpy.loadingStyleSpy == .default)
        XCTAssertEqual(presenterSpy.showDefaultLoadingReceivedInvocations, defaultLoadingCalls)
    }
    
    func
    testPerformRemoteRequest_WhenInteractorFinishServiceRequestSuccess_ShouldCallPresentPerformAction() throws {
        // Given
        let action: RemoteAction = try loadCodableObject(fromJSON: FileName.remote)
        let result = ActionResult(action: action)
        let builder = RemoteActionBuilder(action: action)
        
        remoteActionServiceSpy.result = Result.success(builder)
                
        // When
        sut.performRemoteRequest(from: action, and: result)
        
        // Then
        XCTAssertEqual(presenterSpy.performActionTypeSpy, action.type)
        XCTAssertEqual(presenterSpy.didCallPresentPerformAction, 1)
    }
    
    func
    testPerformRemoteRequest_WhenInteractorServiceRequestFailure_ShouldCallPresentAlert() throws {
        // Given
        let action: RemoteAction = try loadCodableObject(fromJSON: FileName.remote)
        let result = ActionResult(action: action)
        
        remoteActionServiceSpy.result = Result.failure(.connectionFailure)
        
        let message = try XCTUnwrap(ApiError.connectionFailure.requestError?.message)
        
        let property = AlertAction.Property(title: Strings.Store.opsTitle, message: message, positiveButton: Strings.Store.iUnderstandTitle)
                
        // When
        sut.performRemoteRequest(from: action, and: result)
        
        // Then
        XCTAssertEqual(presenterSpy.alertPropertySpy, property)
        XCTAssertEqual(presenterSpy.didCallPresentAlert, 1)
    }
    
    func testPerformAction_WhenCalled_ShouldForwardToPresenter() {
        // Given
        let action = DummyAction()
        let result = ActionResult(action: action)
        
        // When
        sut.performAction(result)
        
        // Then
        XCTAssertEqual(presenterSpy.didCallPresentPerformAction, 1)
        XCTAssertEqual(presenterSpy.performActionTypeSpy, action.type)
    }
    
    func testSendAnalytics_WhenStoreItemHasEvent_ShouldSendEvent() throws {
        let analytics =  ServerDrivenUIEvent(name: "test_event", properties: ["property_1": "value_1"])
        let store = Store.Item("Loja Teste", event: analytics)
        let sut = StoreInteractor(store: store,
                              presenter: presenterSpy,
                              service: serviceStub,
                              remoteActionService: remoteActionServiceSpy,
                              parser: parserSpy,
                              dependencies: dependenciesMock)
        sut.sendAnalytics()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        let event = try XCTUnwrap(analytics.event())
        XCTAssertTrue(analyticsSpy.equals(to: event))
    }
    
    func testSendAnalytics_WhenStoreItemHasNoEvent_ShouldNotSendEvent() throws {
        let store = Store.Item("Loja Teste")
        let sut = StoreInteractor(store: store,
                              presenter: presenterSpy,
                              service: serviceStub,
                              remoteActionService: remoteActionServiceSpy,
                              parser: parserSpy,
                              dependencies: dependenciesMock)
        sut.sendAnalytics()
        XCTAssertEqual(analyticsSpy.logCalledCount, 0)
        XCTAssertNil(analyticsSpy.event)
    }
}
