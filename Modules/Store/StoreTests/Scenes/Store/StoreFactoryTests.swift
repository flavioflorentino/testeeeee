import XCTest
@testable import Store

final class StoreFactoryTests: XCTestCase {
    func testMake_WhenStoreItemWithPublicConstructor_ShouldReturnTheRightViewController() {
        let viewController = StoreFactory.make(Store.Item("1234"), legacyStoreHandler: LegacyStoreHandlerDummy())
        
        XCTAssertTrue(viewController is StoreViewController)
    }
    
    func testMake_WhenStoreItemWithInternalConstructor_ShouldReturnTheRightViewController() {
        let storeItem = Store.Item("5d16600e2c5c0f0037111d32", "IFood")
        let viewController = StoreFactory.make(storeItem, legacyStoreHandler: LegacyStoreHandlerDummy())
        
        XCTAssertTrue(viewController is StoreViewController)
    }
}
