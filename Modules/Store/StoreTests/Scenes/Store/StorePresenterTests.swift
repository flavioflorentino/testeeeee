@testable import Store
import XCTest

final class StorePresenterTests: XCTestCase {
    private let viewControllerSpy = StoreDisplaySpy()
    private let coordinatorSpy = StoreCoordinatorSpy()
    
    private lazy var sut: StorePresenter = {
        let presenter = StorePresenter(
            coordinator: coordinatorSpy,
            urlChecker: UrlCheckPassStub()
        )
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    private func getMockedComponents(from data: Data) -> [UIComponent] {
        let parser = ServerDrivenUIParser()
        parser.decode(data)
        return parser.getUIComponents()
    }
    
    func testPresentSuccess_WhenReceiveRequestedComponents_ShouldUpdateViewControllerWithComponents() throws {
        let data = try loadDataObject(fromJSON: FileName.store)
        let components = getMockedComponents(from: data)
        
        sut.presentComponents(components)

        XCTAssertEqual(viewControllerSpy.components.count, components.count)
        XCTAssertEqual(viewControllerSpy.didCallLoadComponents, 1)
        XCTAssertEqual(viewControllerSpy.didCallHideErrorView, 1)
    }
    
    func testPresentToobar_WhenHeaderComponentHasNoMenuItem_ShouldDisplayNoMenuItem() throws {
        let header: HeaderModel = try loadCodableObject(fromJSON: FileName.headerNoMenuItem)

        sut.presentToolbar(with: "", and: header.menu)

        XCTAssertEqual(viewControllerSpy.menuItems.count, 0)
        XCTAssertEqual(viewControllerSpy.didCallSetToobar, 1)
    }
    
    func testPresentToobar_WhenHeaderComponentHasSingleMenuItem_ShouldDisplayASingleMenuItemOnToolbar() throws {
        let header: HeaderModel = try loadCodableObject(fromJSON: FileName.headerSingleMenuItem)

        sut.presentToolbar(with: "", and: header.menu)

        XCTAssertEqual(viewControllerSpy.menuItems.count, 1)
        XCTAssertEqual(viewControllerSpy.didCallSetToobar, 1)
    }
    
    func testPresentToobar_WhenHeaderComponentHasMoreThanTwoMenuItems_ShouldDisplayOnlyTwoMenuItemsOnToolbar() throws {
        let header: HeaderModel = try loadCodableObject(fromJSON: FileName.headerMultipleMenuItems)

        sut.presentToolbar(with: "", and: header.menu)

        XCTAssertEqual(viewControllerSpy.menuItems.count, 4)
        XCTAssertEqual(viewControllerSpy.didCallSetToobar, 1)
    }
    
    func testPresentError_WhenReceiveRequestedComponentsError_ShouldDisplayErrorMessage() throws {
        let errorTitle = "Opa! Erramos por aqui"
        let errorMessage = "errorMessage"
        sut.presentError(errorMessage)
        XCTAssertEqual(viewControllerSpy.errorViewTitle, errorTitle)
        XCTAssertEqual(viewControllerSpy.errorViewMessage, errorMessage)
        XCTAssertEqual(viewControllerSpy.didCallShowErrorView, 1)
    }
    
    func testPresentTitle_WhenReceiveScreenTitle_ShouldDisplayTitle() throws {
        let title = "Title Example"
        sut.presentToolbar(with: title, and: .init(maxVisibleItems: 0, items: []))
        XCTAssertEqual(viewControllerSpy.title, title)
        XCTAssertEqual(viewControllerSpy.didCallSetToobar, 1)
    }
    
    func testPresentRightButton_WhenReceiveRightButtonAttributes_ShouldDisplayRightButton() throws {
        let header: HeaderModel = try loadCodableObject(fromJSON: FileName.header)
        let button = try XCTUnwrap(header.rightButton)
        let action = try XCTUnwrap(button.action)
        let actionResult = ActionResult(action: action.wrapped)
        sut.presentRightButton(with: button.label, and: actionResult)
        
        XCTAssertEqual(viewControllerSpy.rightButtonTitle, button.label)
        XCTAssertEqual(viewControllerSpy.rightButtonActionResultType, button.action?.type)
        XCTAssertEqual(viewControllerSpy.didCallSetNavigationBarRightButton, 1)
    }

    func testPresentAlert_WhenReceiveAlert_ShouldDisplayAlertOnViewController() throws {
        sut.presentAlert(with: .init(title: "", message: "", positiveButton: ""))
        XCTAssertEqual(viewControllerSpy.didCallDisplayPopup, 1)
    }
    
    func testPresentPayment_WhenReceivePayment_ShouldConvertToPaymentItem() throws {
        let action: PaymentAction = try loadCodableObject(fromJSON: FileName.payment)
        
        sut.presentPayment(action.property, with: [:])
        
        XCTAssertEqual(coordinatorSpy.paymentItem?.value, Double(action.property.payment.value))
        XCTAssertEqual(coordinatorSpy.paymentItem?.storeId, action.property.payment.storeId)
        XCTAssertEqual(coordinatorSpy.paymentItem?.infoURL, action.property.payment.infoUrl)
        XCTAssertEqual(coordinatorSpy.paymentItem?.imageURL, action.property.payment.logoUrl)
        XCTAssertEqual(coordinatorSpy.didCallDisplayPayment, 1)
    }
    
    func testPresentAddress_WhenReceiveAddress_ShouldConvertoToScreenContentAndNavigation() throws {
        //Given
        let action: AddressAction = try loadCodableObject(fromJSON: FileName.address)
        
        //When
        sut.presentAddress(action.property)
        
        //Then
        XCTAssertEqual(
            coordinatorSpy.addressScreenContent?.title,
            action.property.title
        )
        XCTAssertEqual(
            coordinatorSpy.addressScreenContent?.text,
            action.property.text
        )
        XCTAssertEqual(
            coordinatorSpy.addressScreenContent?.image,
            action.property.image
        )
        XCTAssertEqual(
            coordinatorSpy.addressScreenContent?.shouldDisplayHeader,
            action.property.shouldDisplayHeader
        )
        
        XCTAssertEqual(
            coordinatorSpy.addressScreenNavigation?.screenId,
            action.property.navigation.screenId
        )
    
        XCTAssertEqual(coordinatorSpy.didCallDisplayAddress, 1)
    }
    
    func testConfigureNavigationBar_WhenFirstComponentIsHeader_ShouldNotifyFalseForCustomization() throws {
        let data = try loadDataObject(fromJSON: FileName.store)
        let components = getMockedComponents(from: data)
        
        sut.presentComponents(components)

        XCTAssertTrue(viewControllerSpy.isForHeader)
        XCTAssertEqual(viewControllerSpy.didCallConfigureNavigationBar, 1)
    }
    
    func testConfigureNavigationBar_WhenFirstComponentIsNotHeader_ShouldNotifyFalseForCustomization() throws {
        let data = try loadDataObject(fromJSON: FileName.store).json(deletingKeyPaths: "body.header")
        let components = getMockedComponents(from: data)
        
        sut.presentComponents(components)

        XCTAssertFalse(viewControllerSpy.isForHeader)
        XCTAssertEqual(viewControllerSpy.didCallConfigureNavigationBar, 1)
    }
    
    func testDefaultLoading_WhenDefaultLoadingIsTriggered_ShouldReceivePresentDefaultLoading() throws {
        sut.presentLoading(isLoading: true, withStyle: .default)
        XCTAssertTrue(viewControllerSpy.loading)
        XCTAssertTrue(viewControllerSpy.loadingStyle == .default)
        XCTAssertEqual(viewControllerSpy.didCallShowLoading, 1)
    }
    
    func testDefaultLoading_WhenDefaultLoadingIsNotLoading_ShouldReceivePresentDefaultLoading() throws {
        sut.presentLoading(isLoading: false, withStyle: .default)
        XCTAssertFalse(viewControllerSpy.loading)
        XCTAssertTrue(viewControllerSpy.loadingStyle == .default)
        XCTAssertEqual(viewControllerSpy.didCallShowLoading, 1)
    }
    
    func testTransparentLoading_WhenTransparentLoadingIsTriggered_ShouldReceivePresentTransparentLoading() throws {
        sut.presentLoading(isLoading: true, withStyle: .transparent)
        XCTAssertTrue(viewControllerSpy.loading)
        XCTAssertTrue(viewControllerSpy.loadingStyle == .transparent)
        XCTAssertEqual(viewControllerSpy.didCallShowLoading, 1)
    }
    
    func testTransparentLoading_WhenTransparentLoadingIsNotLoading_ShouldReceivePresentTransparentLoading() throws {
        sut.presentLoading(isLoading: false, withStyle: .transparent)
        XCTAssertFalse(viewControllerSpy.loading)
        XCTAssertTrue(viewControllerSpy.loadingStyle == .transparent)
        XCTAssertEqual(viewControllerSpy.didCallShowLoading, 1)
    }
    
    func testLoading_WhenTextIsUpdated_ShouldHaveExpectedText() throws {
        sut.presentUpdatedLoadingText("some")
        XCTAssertEqual(viewControllerSpy.loadingText, "some")
        XCTAssertEqual(viewControllerSpy.didCallUpdateLoadingText, 1)
    }
    
    func testOpenUrl_whenUrlIsValid_shouldCallCoordinatorPerformUrlAction() {
        //Given
        let validUrlString = "https://validurl.com"

        //When
        sut.open(urlString: validUrlString)

        //Then
        XCTAssertEqual(coordinatorSpy.urlToBeOpened?.absoluteString, validUrlString)
        XCTAssertEqual(coordinatorSpy.didCallOpenUrl, 1)
    }

    func testOpenUrl_whenUrlIsInvalid_shouldJustReturn() {
        //Given
        let invalidUrlString = ""

        //When
        sut.open(urlString: invalidUrlString)

        //Then
        XCTAssertNil(coordinatorSpy.urlToBeOpened)
        XCTAssertEqual(coordinatorSpy.didCallOpenUrl, 0)
    }
    
    func testPerform_WhenReceiveResult_ShouldForwardToAction() throws {
        // Given
        let remoteAction: RemoteAction = try loadCodableObject(fromJSON: FileName.remote)
        let result = ActionResult(action: remoteAction)
        
        // When
        sut.performAction(result)
        
        // Then
        let actionResultType = try XCTUnwrap(viewControllerSpy.actionResultType)
        XCTAssertEqual(actionResultType, result.action.type)
        XCTAssertEqual(viewControllerSpy.didCallPerform, 1)
    }
}
