import Foundation
@testable import Store

final class UrlCheckPassStub: URLChecking {
    func canOpenURL(_ url: URL) -> Bool { true }
}

final class UrlCheckFailStub: URLChecking {
    func canOpenURL(_ url: URL) -> Bool { false }
}
