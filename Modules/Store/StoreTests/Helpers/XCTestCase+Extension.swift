@testable import Store
import XCTest

extension XCTestCase {
    enum TestError: Error {
        case invalidJson
        case fileNotFound
    }
    
    func loadDataObject(fromJSON fileName: String) throws -> Data {
        let bundle = Bundle(for: type(of: self))
        guard let url = bundle.url(forResource: fileName, withExtension: "json") else {
            XCTFail("Missing File: \(fileName).json")
            throw TestError.fileNotFound
        }
        do {
            return try Data(contentsOf: url)
        } catch {
            throw error
        }
    }
    
    func loadCodableObject<T: Decodable>(fromJSON fileName: String, deletingKeypaths keypaths: String...) throws -> T {
        do {
            let data = try loadDataObject(fromJSON: fileName).json(deletingKeyPaths: keypaths)
            return try JSONDecoder(.convertFromSnakeCase).decode(T.self, from: data)
        } catch {
            throw error
        }
    }
    
    func decodeObject<T: Decodable>(fromJSON json: String) throws -> T {
        guard let data = json.data(using: .utf8) else {
            XCTFail("Error parsing json string")
            throw TestError.invalidJson
        }
        do {
            return try JSONDecoder(.convertFromSnakeCase).decode(T.self, from: data)
        } catch {
            throw error
        }
    }
    
    func assertThrowsKeyNotFound<T: Decodable>(_ expectedKey: String, decoding: T.Type, from data: Data, file: StaticString = #file, line: UInt = #line) {
        XCTAssertThrowsError(try JSONDecoder(.convertFromSnakeCase).decode(decoding, from: data), file: file, line: line) { error in
            if case let .parseError(_, storeError) = error as? StoreError {
                assertThrowsKeyNotFound(storeError, expectedKey, file, line)
            } else {
                assertThrowsKeyNotFound(error, expectedKey, file, line)
            }
        }
    }
    
    private func assertThrowsKeyNotFound(_ error: Error, _ expectedKey: String, _ file: StaticString = #file, _ line: UInt = #line) {
        if case .keyNotFound(let key, _)? = error as? DecodingError {
            XCTAssertEqual(expectedKey, key.stringValue, "Expected missing key '\(key.stringValue)' to equal '\(expectedKey)'.", file: file, line: line)
        } else {
            XCTFail("Expected '.keyNotFound(\(expectedKey))' but got \(error)", file: file, line: line)
        }
    }
}
