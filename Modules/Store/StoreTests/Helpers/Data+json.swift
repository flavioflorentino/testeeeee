import Foundation

extension Data {
    func json(deletingKeyPaths keyPaths: String...) throws -> Data {
        try json(deletingKeyPaths: keyPaths)
    }
    
    func json(deletingKeyPaths keyPaths: [String]) throws -> Data {
        let decoded = try JSONSerialization.jsonObject(with: self, options: .mutableContainers) as AnyObject
        for keyPath in keyPaths {
            decoded.setValue(nil, forKeyPath: keyPath)
        }
        return try JSONSerialization.data(withJSONObject: decoded)
    }
}
