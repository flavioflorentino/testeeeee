import Foundation
@testable import Store

final class DummyAction: Action {
    var type: ActionType {
        return .address
    }
    
    var analytics: ServerDrivenUIEvent? {
        return nil
    }
    
    var inputKeys: [String] {
        [""]
    }
    
    func perform(delegate: ActionPerformDelegate?, result: ActionResult) {
        print("Function called: \(#function)")
    }
}
