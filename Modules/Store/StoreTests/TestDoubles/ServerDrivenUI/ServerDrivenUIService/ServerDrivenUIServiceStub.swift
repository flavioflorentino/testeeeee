import Core
@testable import Store

final class ServerDrivenUIServiceStub: ServerDrivenUIServicing {
    var result: Result<RemoteActionBuilder, ApiError> = .failure(.cancelled)

    func requesValidation(to url: String, requestData data: ServerData, _ callback: @escaping (Result<RemoteActionBuilder, ApiError>) -> Void) {
        callback(result.map { $0 })
    }
}
