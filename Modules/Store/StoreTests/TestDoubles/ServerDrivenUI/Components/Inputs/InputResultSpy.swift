import Foundation
@testable import Store

final class InputResultSpy: ValidatorOutput, InputMaskOutput {
    private(set) var data: Any?
    private(set) var didCallNotifyDataUpdated = 0
    
    private(set) var message: String?
    private(set) var didCallNotifyDataIsInvalid = 0
    
    private(set) var didCallDataIsValid = 0
    
    private(set) var maskedText: String?
    private(set) var didCallUpdateTextWithMaskedText = 0
    
    func notifyDataUpdated(_ data: Any?) {
        self.data = data
        didCallNotifyDataUpdated = +1
    }
    
    func notifyDataIsInvalid(message: String) {
        self.message = message
        didCallNotifyDataIsInvalid = +1
    }
    
    func notifyDataIsValid() {
        didCallDataIsValid = +1
    }
    
    func updateTextWithMaskedText(_ maskedText: String?) {
        self.maskedText = maskedText
        didCallUpdateTextWithMaskedText = +1
    }
}
