@testable import Store

final class ServerDrivenUIParserSpy: ServerDrivenUIParsing {
    private var response: Response?
    
    func decode(_ data: Data) {
        response = try? JSONDecoder(.convertFromSnakeCase).decode(Response.self, from: data)
    }
    
    func getTitle() -> String {
        guard let header = response?.body.header?.component as? HeaderModel else {
            return ""
        }
        return header.title
    }

    func parseNavigationBarRightButton() -> ButtonModel? {
        guard let rightButton = (response?.body.header?.component as? HeaderModel)?.rightButton else {
            return nil
        }
        return rightButton
    }
    
    func getMenu() -> HeaderModel.Menu? {
        guard let header = response?.body.header?.component as? HeaderModel, let menu = header.menu else {
            return nil
        }
        return menu
    }
    
    func getUIComponents() -> [UIComponent] {
        guard let response = response else {
            return []
        }
        let uiComponents = response.body.components.map {
            UIComponentMock(model: $0.component)
        }
        return uiComponents
    }
}
