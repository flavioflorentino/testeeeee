@testable import Store

final class UIComponentMock: UIComponent {
    var isHeader: Bool { false }
    
    let model: Component
    
    init(model: Component) {
        self.model = model
    }
    
    func render(for tableView: UITableView,
                and indexPath: IndexPath,
                actionDelegate: ActionDelegate,
                dataDelegate: DataDelegate) -> UITableViewCell {
        UITableViewCell()
    }
}
