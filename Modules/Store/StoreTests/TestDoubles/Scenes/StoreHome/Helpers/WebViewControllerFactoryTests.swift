import XCTest
import SafariServices
@testable import Store

final class WebViewControllerFactoryTests: XCTestCase {
    func testMake_ShouldReturnTheRightViewController() throws {
        let url = try XCTUnwrap(URL(string: "https://www.example.com/webview"))
        let viewController = WebViewControllerFactory().make(with: url)
        
        XCTAssertTrue(viewController is SFSafariViewController)
    }
}
