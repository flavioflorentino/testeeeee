import UIKit
import Store

final class SearchResultLegacyViewControllerDummy: UIViewController, SearchResultLegacyViewController {
    func searchTextChanged(text: String) {
        // DUMMY: This function doesn't do anything
    }
}
