import UIKit
import Store

final class SearchHeaderLegacyViewDummy: UIView, SearchHeaderLegacyView {
    var leftButtonTapped: (() -> Void)?
    var cancelButtonTapped: (() -> Void)?
    var searchTextChanged: ((String) -> Void)?
    var searchTextDidBeginEditing: ((String) -> Void)?
    var searchTextDidEndEditing: ((String) -> Void)?
}
