import Foundation
import Core
@testable import Store

final class StorePresenterSpy: StorePresenting {
    var viewController: StoreDisplay?
    
    private var delegate = ActionPerformDelegateSpy()
    
    private(set) var storeTitleSpy: String?
    private(set) var menuSpy: HeaderModel.Menu?
    private(set) var didCallPresentToolbar = 0
    
    private(set) var rightButtonTitleSpy: String?
    private(set) var rightButtonActionTypeSpy: ActionType?
    private(set) var didCallPresentRightButton = 0
    
    private(set) var componentsSpy: [UIComponent] = []
    private(set) var didCallPresentComponents = 0
    
    private(set) var loadingStyleSpy: LoadingViewStyle?
    private(set) var showDefaultLoadingReceivedInvocations: [Bool] = []
    
    private(set) var loadingText: String?
    private(set) var didCallPresentUpdatedLoadingText = 0
    
    private(set) var alertPropertySpy: AlertAction.Property?
    private(set) var didCallPresentAlert = 0
    
    private(set) var errorMessageSpy: String?
    private(set) var didCallPresentError = 0
    
    private(set) var performActionTypeSpy: ActionType?
    private(set) var didCallPresentPerformAction = 0
    
    func presentToolbar(with title: String, and menu: HeaderModel.Menu?) {
        menuSpy = menu
        storeTitleSpy = title
        didCallPresentToolbar += 1
    }
    
    func presentRightButton(with title: String, and actionResult: ActionResult) {
        rightButtonTitleSpy = title
        rightButtonActionTypeSpy = actionResult.action.type
        didCallPresentRightButton += 1
    }
    
    func presentComponents(_ components: [UIComponent]) {
        componentsSpy = components
        didCallPresentComponents += 1
    }
    
    func presentLoading(isLoading: Bool, withStyle style: LoadingViewStyle) {
        loadingStyleSpy = style
        showDefaultLoadingReceivedInvocations.append(isLoading)
    }
    
    func presentUpdatedLoadingText(_ text: String) {
        loadingText = text
        didCallPresentUpdatedLoadingText += 1
    }
    
    func presentAlert(with property: AlertAction.Property) {
        alertPropertySpy = property
        didCallPresentAlert += 1
    }
    
    func presentError(_ message: String?) {
        errorMessageSpy = message ?? "Tivemos uma falha no sistema e não conseguimos carregar essa página."
        didCallPresentError += 1
    }
    
    func performAction(_ result: ActionResult) {
        performActionTypeSpy = result.action.type
        result.action.perform(delegate: delegate, result: result)
        didCallPresentPerformAction += 1
    }
}
