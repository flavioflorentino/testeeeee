@testable import Store
import UI
import XCTest

final class StoreCoordinatorSpy: StoreCoordinating {
    var viewController: UIViewController?
    
    private(set) var paymentItem: StorePayment?
    private(set) var didCallDisplayPayment = 0
    
    private(set) var addressScreenContent: StoreAddressScreenContent?
    private(set) var addressScreenNavigation: NavigationAction.Navigation?
    private(set) var didCallDisplayAddress = 0
    
    private(set) var urlToBeOpened: URL?
    private(set) var didCallOpenUrl = 0
    
    func perform(action: StoreAction) {
        switch action {
        case .payment(let item):
            paymentItem = item
            didCallDisplayPayment += 1
        case let .address(content, navigation):
            addressScreenContent = content
            addressScreenNavigation = navigation.navigation
            didCallDisplayAddress += 1
        case let .open(url):
            urlToBeOpened = url
            didCallOpenUrl += 1
        default:
            break
        }
    }
}
