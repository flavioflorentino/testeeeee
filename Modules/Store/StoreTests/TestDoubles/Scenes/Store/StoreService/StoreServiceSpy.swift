import Foundation
import Core
@testable import Store

final class StoreServiceStub: StoreServicing {
    var result: Result<Data, ApiError>?
    func favorite(_ id: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {}
    func unfavorite(_ id: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {}
    func requestComponents(for store: Store.Item, _ callback: @escaping (Result<Data, ApiError>) -> Void) {
        guard let result = result else {
            return
        }
        callback(result.map { $0 })
    }
}
