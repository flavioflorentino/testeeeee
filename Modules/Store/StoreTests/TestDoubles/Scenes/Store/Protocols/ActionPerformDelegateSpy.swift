@testable import Store
import UI
import XCTest

final class ActionPerformDelegateSpy: ActionPerformDelegate {
    
    private(set) var navigationLegacySpy: [String : Any]?
    private(set) var didCallPresentLegacyStore = 0
    
    private(set) var paymentSpy: PaymentAction.Payment?
    private(set) var didCallPresentPayment = 0
    
    private(set) var addressSpy: AddressAction.Property?
    private(set) var didCallPresentAddress = 0
    
    private(set) var alertPropertySpy: AlertAction.Property?
    private(set) var didCallPresentAlert = 0
    
    private(set) var webviewUrlSpy: String?
    private(set) var didCallPresentWebview = 0
    
    private(set) var navigationSpy: NavigationAction.Property?
    private(set) var fullScreenSpy: Bool?
    private(set) var didCallNavigate = 0
    
    private(set) var receiptSpy: ReceiptAction.Property?
    private(set) var didCallReceipt = 0
    
    private(set) var shareSpy: ShareAction.Property?
    private(set) var didCallBuildShareSettings = 0
    
    private(set) var urlSpy: String?
    private(set) var didCallPresentUrl = 0
    
    private(set) var remoteActionSpy: RemoteAction.Property?
    private(set) var actionResultTypeSpy: ActionType?
    private(set) var didCallPresent = 0
    
    func presentLegacyStore(from data: [String : Any]) {
        navigationLegacySpy = data
        didCallPresentLegacyStore += 1
    }
    
    func presentPayment(_ property: PaymentAction.Property, with inputData: [String : Any]) {
        paymentSpy = property.payment
        didCallPresentPayment += 1
    }
    
    func presentAddress(_ property: AddressAction.Property) {
        addressSpy = property
        didCallPresentAddress += 1
    }
    
    func presentAlert(with property: AlertAction.Property) {
        alertPropertySpy = property
        didCallPresentAlert += 1
    }
    
    func presentWebview(with urlString: String) {
        webviewUrlSpy = urlString
        didCallPresentWebview += 1
    }
    
    func present(remoteAction: RemoteAction, result: ActionResult) {
        remoteActionSpy = remoteAction.property
        actionResultTypeSpy = result.action.type
        didCallPresent += 1
    }
    
    func openScreen(with properties: NavigationAction.Property, fullScreen: Bool) {
        navigationSpy = properties
        fullScreenSpy = fullScreen
        didCallNavigate += 1
    }
    
    func openTransactionReceipt(with property: ReceiptAction.Property) {
        receiptSpy = property
        didCallReceipt += 1
    }
    
    func buildShareSettings(with property: ShareAction.Property) {
        shareSpy = property
        didCallBuildShareSettings += 1
    }
    
    func open(urlString: String) {
        urlSpy = urlString
        didCallPresentUrl += 1
    }
}
