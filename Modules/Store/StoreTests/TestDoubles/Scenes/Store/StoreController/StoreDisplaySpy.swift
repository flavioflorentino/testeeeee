@testable import Store
import UI
import XCTest

final class StoreDisplaySpy: StoreDisplay {
    private(set) var components: [UIComponent] = []
    private(set) var didCallLoadComponents = 0
    
    private(set) var didCallDisplayPopup = 0
    
    private(set) var title: String?
    private(set) var menuItems: [UIStoreBarButton] = []
    private(set) var didCallSetToobar = 0
    
    private(set) var rightButtonTitle: String?
    private(set) var rightButtonActionResultType: ActionType?
    private(set) var didCallSetNavigationBarRightButton = 0

    private(set) var loading: Bool = false
    private(set) var loadingStyle: LoadingViewStyle?
    private(set) var didCallShowLoading = 0
    
    private(set) var loadingText: String?
    private(set) var didCallUpdateLoadingText = 0
    
    private(set) var isForHeader: Bool = false
    private(set) var didCallConfigureNavigationBar = 0
    
    private(set) var errorViewTitle: String?
    private(set) var errorViewMessage: String?
    private(set) var didCallShowErrorView = 0
    
    private(set) var didCallHideErrorView = 0
    
    private(set) var remoteAction: RemoteAction?
    private(set) var actionResultType: ActionType?
    private(set) var didCallPerform = 0
    
    func loadComponents(_ components: [UIComponent]) {
        self.components = components
        didCallLoadComponents += 1
    }
    
    func displayPopup(_ popup: PopupViewController) {
        didCallDisplayPopup += 1
    }
    
    func setToolbar(_ title: String, menuItems: [UIStoreBarButton]) {
        self.title = title
        self.menuItems = menuItems
        didCallSetToobar += 1
    }
    
    func setNavigationBarRightButton(with title: String, andActionResult actionResult: ActionResult) {
        rightButtonTitle = title
        rightButtonActionResultType = actionResult.action.type
        didCallSetNavigationBarRightButton += 1
    }
    
    func showLoading(_ isLoading: Bool, withStyle style: LoadingViewStyle) {
        loading = isLoading
        loadingStyle = style
        didCallShowLoading += 1
    }
    
    func updateLoadingText(_ text: String) {
        loadingText = text
        didCallUpdateLoadingText += 1
    }
    
    func configureNavigationBar(isForHeader: Bool) {
        self.isForHeader = isForHeader
        didCallConfigureNavigationBar += 1
    }
    
    func showErrorView(title: String, message: String?) {
        errorViewTitle = title
        errorViewMessage = message
        didCallShowErrorView += 1
    }
    
    func hideErrorView() {
        didCallHideErrorView += 1
    }
    
    func perform(remoteAction: RemoteAction, result: ActionResult) {
        self.remoteAction = remoteAction
        self.actionResultType = result.action.type
        self.didCallPerform += 1
    }
}
