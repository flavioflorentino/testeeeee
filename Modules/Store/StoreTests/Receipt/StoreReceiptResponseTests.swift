@testable import Store
import XCTest

final class StoreReceiptResponseTests: XCTestCase {
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: StoreReceiptResponse = try loadCodableObject(fromJSON: FileName.receiptResponse)
        
        XCTAssertEqual(sut.data.receipt.first?["type"] as? String, "transaction")
        XCTAssertEqual(sut.data.receipt.first?["total"] as? String, "R$ 53,54")
        
        do {
            let list = try XCTUnwrap(sut.data.receipt.first(where: { $0["type"] as? String == "list" }))
            let items = try XCTUnwrap(list["itens"] as? [[AnyHashable: Any]])
            
            for item in items {
                let itemValue = try XCTUnwrap(item["value"] as? [AnyHashable: Any])
                let value = itemValue["value"]
                XCTAssertTrue(value is String)
            }
        } catch {
            XCTFail(error.localizedDescription)
        }
    }
    
    func testDecoding_WhenMissingKeyData_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.receiptResponse)
        assertThrowsKeyNotFound("data", decoding: StoreReceiptResponse.self, from: try sut.json(deletingKeyPaths: "data"))
    }
}
