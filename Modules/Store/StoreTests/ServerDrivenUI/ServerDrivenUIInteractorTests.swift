@testable import Store
import XCTest
import AnalyticsModule
import FeatureFlag
import Core

private final class ComponentMock: Validator {
    var data: Any?
    var rules: [Rule]
    var isSuccess: Bool
    
    init(isSuccess: Bool, data: String) {
        rules = []
        self.data = data
        self.isSuccess = isSuccess
    }
    
    func isDataValid() -> Bool {
        isSuccess
    }
}

private final class ServerDrivenUIOutputsSpy: ServerDrivenUIOutputs {
    private(set) var result: ActionResult?
    private(set) var didCalldidReceiveResultFromComponentAction = 0
    
    func didReceiveResultFromComponentAction(_ result: ActionResult) {
        self.result = result
        didCalldidReceiveResultFromComponentAction = +1
    }
}

final class ServerDrivenUIInteractorTests: XCTestCase {
    private let output = ServerDrivenUIOutputsSpy()
    private let analyticsSpy = AnalyticsSpy()
    private lazy var dependenciesMock = DependencyContainerMock(analyticsSpy)
    private lazy var sut = ServerDrivenUIInteractor(output: output, dependencies: dependenciesMock)
    
    func testCreatePayment_WhenReceivePaymentActionWithAdditionalData_ShouldCreatePaymentObjectWithAdditionalData() throws {
        let component: ButtonModel = try loadCodableObject(fromJSON: FileName.paymentWithAdditionData)
        let mock1 = ComponentMock(isSuccess: true, data: "1")
        let mock2 = ComponentMock(isSuccess: true, data: "2")
        
        sut.dataContainer.add(mock1, for: "mock1")
        sut.dataContainer.add(mock2, for: "mock2")
        
        sut.validateAction(from: component)
        
        let result1 = output.result?.additionalData["mock1"] as? String
        let result2 = output.result?.additionalData["mock2"] as? String
        
        XCTAssertEqual(result1, "1")
        XCTAssertEqual(result2, "2")
        XCTAssertEqual(output.didCalldidReceiveResultFromComponentAction, 1)
    }
    
    func testFailedToCreatePayment_WhenPaymentActionWithAdditionalDataValidationFails_ShouldDoNothing() throws {
        let component: ButtonModel = try loadCodableObject(fromJSON: FileName.paymentWithAdditionData)
        let mock1 = ComponentMock(isSuccess: true, data: "1")
        let mock2 = ComponentMock(isSuccess: false, data: "2")
        
        sut.dataContainer.add(mock1, for: "mock1")
        sut.dataContainer.add(mock2, for: "mock2")
        
        sut.validateAction(from: component)
        
        let result1 = output.result?.additionalData["mock1"] as? String
        let result2 = output.result?.additionalData["mock2"] as? String
        
        XCTAssertEqual(result1, nil)
        XCTAssertEqual(result2, nil)
        XCTAssertEqual(output.didCalldidReceiveResultFromComponentAction, 0)
    }
    
    func testCreatePayment_WhenReceivePaymentActionWithNoAddionalData_ShouldCreatePaymentObjectWithNoAdditionalData() throws {
        let component: ButtonModel = try loadCodableObject(fromJSON: FileName.paymentWithNoAdditionData)
        
        sut.validateAction(from: component)
  
        XCTAssertEqual(output.result?.additionalData.isEmpty, true)
        XCTAssertEqual(output.didCalldidReceiveResultFromComponentAction, 1)
    }
    
    func testSendAnalytics_WhenReceiveActionWithAnalytics_ShouldSendEvent() throws {
        let component: PrimaryItemModel = try loadCodableObject(fromJSON: FileName.actionWithAnalyticsData)
        
        sut.sendAnalytics(from: component)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        let event = try XCTUnwrap(component.action?.analytics?.event())
        XCTAssertTrue(analyticsSpy.equals(to: event))
    }
    
    func testSendAnalytics_WhenReceiveActionWithoutAnalytics_ShouldNotSendEvent() throws {
        let component: PrimaryItemModel = try loadCodableObject(fromJSON: FileName.actionWithoutAnalyticsData)
        
        sut.sendAnalytics(from: component)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 0)
        XCTAssertNil(analyticsSpy.event)
    }
}
