@testable import Store
import XCTest

final class DeeplinkActionTests: XCTestCase {
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: DeeplinkAction = try loadCodableObject(fromJSON: FileName.deeplink)
        
        XCTAssertEqual(sut.type, ActionType.deeplink)
    }
    
    func testDecoding_WhenMissingKeyType_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.deeplink)
        assertThrowsKeyNotFound("type", decoding: DeeplinkAction.self, from: try sut.json(deletingKeyPaths: "type"))
    }
    
    func testDecoding_WhenMissingKeyProperty_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.deeplink)
        assertThrowsKeyNotFound("property", decoding: DeeplinkAction.self, from: try sut.json(deletingKeyPaths: "property"))
    }
    
    func testPerform_WhenReceiveDelegateAndActionResult_ShouldCallOpenURL() throws {
        // Given
        let sut: DeeplinkAction = try loadCodableObject(fromJSON: FileName.deeplink)
        let delegate = ActionPerformDelegateSpy()
        let result = ActionResult(action: sut)
        
        // When
        sut.perform(delegate: delegate, result: result)
        
        // Then
        XCTAssertEqual(result.action.type, .deeplink)
        XCTAssertEqual(delegate.didCallPresentUrl, 1)
        XCTAssertEqual(delegate.urlSpy, sut.property.deeplink.url)
    }
}
