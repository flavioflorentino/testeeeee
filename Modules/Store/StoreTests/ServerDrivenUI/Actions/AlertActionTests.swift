@testable import Store
import XCTest

final class AlertActionTests: XCTestCase {
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: AlertAction = try loadCodableObject(fromJSON: FileName.alert)
        
        XCTAssertEqual(sut.type, ActionType.alert)
    }
    
    func testDecoding_WhenMissingKeyType_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.alert)
        assertThrowsKeyNotFound("type", decoding: AlertAction.self, from: try sut.json(deletingKeyPaths: "type"))
    }
    
    func testDecoding_WhenMissingKeyProperty_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.alert)
        assertThrowsKeyNotFound("property", decoding: AlertAction.self, from: try sut.json(deletingKeyPaths: "property"))
    }
    
    func testPerform_WhenReceiveDelegateAndActionResult_ShouldCallPresentAlert() throws {
        // Given
        let sut: AlertAction = try loadCodableObject(fromJSON: FileName.alert)
        let delegate = ActionPerformDelegateSpy()
        let result = ActionResult(action: sut)
        
        // When
        sut.perform(delegate: delegate, result: result)
        
        // Then
        XCTAssertEqual(result.action.type, .alert)
        XCTAssertEqual(delegate.didCallPresentAlert, 1)
        XCTAssertEqual(delegate.alertPropertySpy, sut.property)
    }
}
