@testable import Store
import XCTest

final class PaymentActionTests: XCTestCase {
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: PaymentAction = try loadCodableObject(fromJSON: FileName.payment)
        
        XCTAssertEqual(sut.type, ActionType.payment)
    }
    
    func testDecoding_WhenMissingKeyType_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.navigation)
        assertThrowsKeyNotFound("type", decoding: NavigationAction.self, from: try sut.json(deletingKeyPaths: "type"))
    }
    
    func testDecoding_WhenMissingKeyProperty_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.navigation)
        assertThrowsKeyNotFound("property", decoding: NavigationAction.self, from: try sut.json(deletingKeyPaths: "property"))
    }
    
    func testPerform_WhenReceiveDelegateAndActionResult_ShouldCallPresentPayment() throws {
        // Given
        let sut: PaymentAction = try loadCodableObject(fromJSON: FileName.payment)
        let delegate = ActionPerformDelegateSpy()
        let result = ActionResult(action: sut)
        
        // When
        sut.perform(delegate: delegate, result: result)
        
        // Then
        XCTAssertEqual(result.action.type, .payment)
        XCTAssertEqual(delegate.didCallPresentPayment, 1)
        XCTAssertEqual(delegate.paymentSpy, sut.property.payment)
    }
}

