@testable import Store
import XCTest

final class NavigationActionTests: XCTestCase {
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: NavigationAction = try loadCodableObject(fromJSON: FileName.navigation)
        
        XCTAssertEqual(sut.type, ActionType.openScreen)
    }
    
    func testDecoding_WhenMissingKeyType_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.navigation)
        assertThrowsKeyNotFound("type", decoding: NavigationAction.self, from: try sut.json(deletingKeyPaths: "type"))
    }
    
    func testDecoding_WhenMissingKeyProperty_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.navigation)
        assertThrowsKeyNotFound("property", decoding: NavigationAction.self, from: try sut.json(deletingKeyPaths: "property"))
    }
    
    func testPerform_WhenReceiveDelegateAndActionResult_ShouldCallOpenScreen() throws {
        // Given
        let sut: NavigationAction = try loadCodableObject(fromJSON: FileName.navigation)
        let delegate = ActionPerformDelegateSpy()
        let result = ActionResult(action: sut)
        
        // When
        sut.perform(delegate: delegate, result: result)
        
        // Then
        XCTAssertEqual(result.action.type, .openScreen)
        XCTAssertEqual(delegate.didCallNavigate, 1)
        XCTAssertEqual(delegate.navigationSpy, sut.property)
        XCTAssertEqual(delegate.fullScreenSpy, false)
    }
}
