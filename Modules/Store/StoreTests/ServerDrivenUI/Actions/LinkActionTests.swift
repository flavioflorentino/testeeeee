@testable import Store
import XCTest

final class LinkActionTests: XCTestCase {
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: LinkAction = try loadCodableObject(fromJSON: FileName.link)
        
        XCTAssertEqual(sut.type, ActionType.webview)
    }
    
    func testDecoding_WhenMissingKeyType_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.link)
        assertThrowsKeyNotFound("type", decoding: LinkAction.self, from: try sut.json(deletingKeyPaths: "type"))
    }
    
    func testDecoding_WhenMissingKeyProperty_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.link)
        assertThrowsKeyNotFound("property", decoding: LinkAction.self, from: try sut.json(deletingKeyPaths: "property"))
    }
    
    func testPerform_WhenReceiveDelegateAndActionResult_ShouldCallOpenWebView() throws {
        // Given
        let sut: LinkAction = try loadCodableObject(fromJSON: FileName.link)
        let delegate = ActionPerformDelegateSpy()
        let result = ActionResult(action: sut)
        
        // When
        sut.perform(delegate: delegate, result: result)
        
        // Then
        XCTAssertEqual(result.action.type, .webview)
        XCTAssertEqual(delegate.didCallPresentWebview, 1)
        XCTAssertEqual(delegate.webviewUrlSpy, sut.property.webview.url)
    }
}
