@testable import Store
import XCTest

final class ReceiptActionTests: XCTestCase {
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: ReceiptAction = try loadCodableObject(fromJSON: FileName.receipt)
        
        XCTAssertEqual(sut.type, ActionType.receipt)
    }
    
    func testDecoding_WhenMissingKeyType_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.receipt)
        assertThrowsKeyNotFound("type", decoding: ReceiptAction.self, from: try sut.json(deletingKeyPaths: "type"))
    }
    
    func testDecoding_WhenMissingKeyProperty_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.receipt)
        assertThrowsKeyNotFound("property", decoding: ReceiptAction.self, from: try sut.json(deletingKeyPaths: "property"))
    }
    
    func testPerform_WhenReceiveDelegateAndActionResult_ShouldCallOpenTransactionReceipt() throws {
        // Given
        let sut: ReceiptAction = try loadCodableObject(fromJSON: FileName.receipt)
        let delegate = ActionPerformDelegateSpy()
        let result = ActionResult(action: sut)
        
        // When
        sut.perform(delegate: delegate, result: result)
        
        // Then
        XCTAssertEqual(result.action.type, .receipt)
        XCTAssertEqual(delegate.didCallReceipt, 1)
        XCTAssertEqual(delegate.receiptSpy, sut.property)
    }
}
