@testable import Store
import XCTest

final class RemoteActionTests: XCTestCase {
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: RemoteAction = try loadCodableObject(fromJSON: FileName.remote)
        
        XCTAssertEqual(sut.type, ActionType.remoteAction)
    }
    
    func testDecoding_WhenMissingKeyType_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.remote)
        assertThrowsKeyNotFound("type", decoding: RemoteAction.self, from: try sut.json(deletingKeyPaths: "type"))
    }
    
    func testDecoding_WhenMissingKeyProperty_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.remote)
        assertThrowsKeyNotFound("property", decoding: RemoteAction.self, from: try sut.json(deletingKeyPaths: "property"))
    }
    
    func testPerform_WhenReceiveDelegateAndActionResult_ShouldCallPresentWithRemoteActionAndActionResult() throws {
        // Given
        let sut: RemoteAction = try loadCodableObject(fromJSON: FileName.remote)
        let delegate = ActionPerformDelegateSpy()
        let result = ActionResult(action: sut)
        
        // When
        sut.perform(delegate: delegate, result: result)
        
        // Then
        XCTAssertEqual(result.action.type, .remoteAction)
        XCTAssertEqual(delegate.didCallPresent, 1)
        XCTAssertEqual(delegate.actionResultTypeSpy, result.action.type)
        XCTAssertEqual(delegate.remoteActionSpy, sut.property)
    }
}
