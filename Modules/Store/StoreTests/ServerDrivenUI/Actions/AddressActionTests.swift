@testable import Store
import XCTest

final class AddressActionTests: XCTestCase {
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: AddressAction = try loadCodableObject(fromJSON: FileName.address)
        
        XCTAssertEqual(sut.type, ActionType.address)
    }
    
    func testDecoding_WhenMissingKeyType_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.address)
        assertThrowsKeyNotFound("type", decoding: AddressAction.self, from: try sut.json(deletingKeyPaths: "type"))
    }
    
    func testDecoding_WhenMissingKeyProperty_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.address)
        assertThrowsKeyNotFound("property", decoding: AddressAction.self, from: try sut.json(deletingKeyPaths: "property"))
    }
    
    func testPerform_WhenReceiveDelegateAndActionResult_ShouldCallPresentAddress() throws {
        // Given
        let sut: AddressAction = try loadCodableObject(fromJSON: FileName.address)
        let delegate = ActionPerformDelegateSpy()
        let result = ActionResult(action: sut)
        
        // When
        sut.perform(delegate: delegate, result: result)
        
        // Then
        XCTAssertEqual(result.action.type, .address)
        XCTAssertEqual(delegate.didCallPresentAddress, 1)
        XCTAssertEqual(delegate.addressSpy, sut.property)
    }
}
