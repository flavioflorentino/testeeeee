@testable import Store
import XCTest

final class NavigationLegacyActionTests: XCTestCase {
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: NavigationLegacyAction = try loadCodableObject(fromJSON: FileName.navigationLegacy)
        
        XCTAssertEqual(sut.type, ActionType.openLegacyDigitalGood)
    }
    
    func testDecoding_WhenMissingKeyType_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.navigationLegacy)
        assertThrowsKeyNotFound("type", decoding: NavigationLegacyAction.self, from: try sut.json(deletingKeyPaths: "type"))
    }
    
    func testDecoding_WhenMissingKeyProperty_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.navigationLegacy)
        assertThrowsKeyNotFound("property", decoding: NavigationLegacyAction.self, from: try sut.json(deletingKeyPaths: "property"))
    }
    
    func testPerform_WhenReceiveDelegateAndActionResult_ShouldCallPresentLegacyStore() throws {
        // Given
        let sut: NavigationLegacyAction = try loadCodableObject(fromJSON: FileName.navigationLegacy)
        let delegate = ActionPerformDelegateSpy()
        let result = ActionResult(action: sut)
        
        // When
        sut.perform(delegate: delegate, result: result)
        
        // Then
        let navigationLegacySpy = try XCTUnwrap(delegate.navigationLegacySpy)
        XCTAssertEqual(result.action.type, .openLegacyDigitalGood)
        XCTAssertEqual(delegate.didCallPresentLegacyStore, 1)
        XCTAssert(navigationLegacySpy.isEqual(to: sut.property.legacyDigitalGood))
    }
}
