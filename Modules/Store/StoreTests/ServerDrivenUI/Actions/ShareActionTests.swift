@testable import Store
import XCTest

final class ShareActionTests: XCTestCase {
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: ShareAction = try loadCodableObject(fromJSON: FileName.share)
        
        XCTAssertEqual(sut.type, ActionType.share)
    }
    
    func testDecoding_WhenMissingKeyType_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.share)
        assertThrowsKeyNotFound("type", decoding: ShareAction.self, from: try sut.json(deletingKeyPaths: "type"))
    }
    
    func testDecoding_WhenMissingKeyProperty_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.share)
        assertThrowsKeyNotFound("property", decoding: ShareAction.self, from: try sut.json(deletingKeyPaths: "property"))
    }
    
    func testPerform_WhenReceiveDelegateAndActionResult_ShouldCallBuildShareSettingsWithShareActionProperty() throws {
        // Given
        let sut: ShareAction = try loadCodableObject(fromJSON: FileName.share)
        let delegate = ActionPerformDelegateSpy()
        let result = ActionResult(action: sut)
        
        // When
        sut.perform(delegate: delegate, result: result)
        
        // Then
        XCTAssertEqual(result.action.type, .share)
        XCTAssertEqual(delegate.didCallBuildShareSettings, 1)
        XCTAssertEqual(delegate.shareSpy, sut.property)
    }
}
