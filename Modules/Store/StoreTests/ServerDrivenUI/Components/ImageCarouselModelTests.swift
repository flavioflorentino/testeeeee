@testable import Store
import XCTest

final class ImageCarouselModelTests: XCTestCase {
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: ImageCarouselModel = try loadCodableObject(fromJSON: FileName.imageCarousel)
        
        XCTAssertEqual(sut.type, ComponentType.imageCarousel)
        XCTAssertEqual(sut.value, "https://image.com/example.png")
    }
    
    func testDecoding_WhenMissingKeyType_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.imageCarousel)
        assertThrowsKeyNotFound("type", decoding: ImageCarouselModel.self, from: try sut.json(deletingKeyPaths: "type"))
    }
    
    func testDecoding_WhenMissingKeyValue_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.imageCarousel)
        assertThrowsKeyNotFound("value", decoding: ImageCarouselModel.self, from: try sut.json(deletingKeyPaths: "value"))
    }
}

