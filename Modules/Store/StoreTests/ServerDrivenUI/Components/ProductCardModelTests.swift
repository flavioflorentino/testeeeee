@testable import Store
import XCTest

final class ProductCardModelTests: XCTestCase {
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: ProductCardModel = try loadCodableObject(fromJSON: FileName.productCard)
        
        XCTAssertEqual(sut.type, ComponentType.productCard)
        XCTAssertEqual(sut.avatar, "https://lh3.googleusercontent.com/FrMp1xgQ4_QjTp7pO0QCc8T24FyZNhhONviUk886e3A53-J9iN_6aBYoEuIHasphjg")
        XCTAssertEqual(sut.spotlight, "Semana top")
        XCTAssertEqual(sut.value, "R$ 50,00")
        XCTAssertEqual(sut.description, "Plano semanal de marmitas no PicPay")
        XCTAssertEqual(sut.action?.type, ActionType.payment)
    }
    
    func testDecoding_WhenMissingKeyAvatar_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.productCard)
        assertThrowsKeyNotFound("avatar", decoding: ProductCardModel.self, from: try sut.json(deletingKeyPaths: "avatar"))
    }
    
    func testDecoding_WhenMissingKeySubtitle_ShouldParseWithNilSubtitle() throws {
        let sut: ProductCardModel = try loadCodableObject(fromJSON: FileName.productCard, deletingKeypaths: "spotlight")

        XCTAssertNil(sut.spotlight)

        XCTAssertEqual(sut.type, .productCard)
        XCTAssertEqual(sut.avatar, "https://lh3.googleusercontent.com/FrMp1xgQ4_QjTp7pO0QCc8T24FyZNhhONviUk886e3A53-J9iN_6aBYoEuIHasphjg")
        XCTAssertEqual(sut.value, "R$ 50,00")
        XCTAssertEqual(sut.description, "Plano semanal de marmitas no PicPay")
        XCTAssertEqual(sut.action?.type, .payment)
    }
    
    func testDecoding_WhenMissingKeyValue_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.productCard)
        assertThrowsKeyNotFound("value", decoding: ProductCardModel.self, from: try sut.json(deletingKeyPaths: "value"))
    }
    
    func testDecoding_WhenMissingKeyADescription_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.productCard)
        assertThrowsKeyNotFound("description", decoding: ProductCardModel.self, from: try sut.json(deletingKeyPaths: "description"))
    }
}
