import Foundation
import XCTest
@testable import Store

final class PrimaryCardModelTests: XCTestCase {
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        //Given
        let json = FileName.primaryCard
        
        //When
        let sut: PrimaryCardModel = try loadCodableObject(fromJSON: json)
        
        //Then
        XCTAssertEqual(sut.type, .primaryCard)
        XCTAssertEqual(sut.image,  "Minha Url")
        XCTAssertEqual(sut.title, "Meu título")
        XCTAssertEqual(sut.subtitle, "Meu subtítulo")
        XCTAssertEqual(sut.action?.type, .openScreen)
    }
    
    func testDecoding_WhenMissingKeyType_ShouldThrowsDecodeError() throws {
        //Given
        let json = FileName.primaryCard
        let typeKey = "type"
        
        //When
        let sut: Data = try loadDataObject(fromJSON: json)
            .json(deletingKeyPaths: typeKey)
        
        //Then
        assertThrowsKeyNotFound(typeKey, decoding: LabelValueModel.self, from: sut)
    }
    
    func testDecoding_WhenMissingKeyTitle_ShouldParseWithNilTitle() throws {
        //Given
        let json = FileName.primaryCard
        let titleKey = "title"
        
        //When
        let sut: PrimaryCardModel = try loadCodableObject(
            fromJSON: json,
            deletingKeypaths: titleKey
        )
        
        //Then
        XCTAssertNil(sut.title)
        
        XCTAssertEqual(sut.type, .primaryCard)
        XCTAssertEqual(sut.image,  "Minha Url")
        XCTAssertEqual(sut.subtitle, "Meu subtítulo")
        XCTAssertEqual(sut.action?.type, .openScreen)
    }
    
    func testDecoding_WhenMissingKeySubtitle_ShouldParseWithNilSubtitle() throws {
        //Given
        let json = FileName.primaryCard
        let subtitleKey = "subtitle"
        
        //When
        let sut: PrimaryCardModel = try loadCodableObject(
            fromJSON: json,
            deletingKeypaths: subtitleKey
        )
        
        //Then
        XCTAssertNil(sut.subtitle)
        
        XCTAssertEqual(sut.type, .primaryCard)
        XCTAssertEqual(sut.image,  "Minha Url")
        XCTAssertEqual(sut.title, "Meu título")
        XCTAssertEqual(sut.action?.type, .openScreen)
    }
    
    func testDecoding_WhenMissingKeyImage_ShouldParseWithNilImage() throws {
        //Given
        let json = FileName.primaryCard
        let imageKey = "image"
        
        //When
        let sut: PrimaryCardModel = try loadCodableObject(
            fromJSON: json,
            deletingKeypaths: imageKey
        )
        
        //Then
        XCTAssertNil(sut.image)
        
        XCTAssertEqual(sut.type, .primaryCard)
        XCTAssertEqual(sut.title, "Meu título")
        XCTAssertEqual(sut.subtitle, "Meu subtítulo")
        XCTAssertEqual(sut.action?.type, .openScreen)
    }
    
    func testDecoding_WhenMissingKeyAction_ShouldParseWithNilAction() throws {
        //Given
        let json = FileName.primaryCard
        let imageKey = "action"
        
        //When
        let sut: PrimaryCardModel = try loadCodableObject(
            fromJSON: json,
            deletingKeypaths: imageKey
        )
        
        //Then
        XCTAssertNil(sut.action)
        
        XCTAssertEqual(sut.type, .primaryCard)
        XCTAssertEqual(sut.image,  "Minha Url")
        XCTAssertEqual(sut.title, "Meu título")
        XCTAssertEqual(sut.subtitle, "Meu subtítulo")
    }
}

