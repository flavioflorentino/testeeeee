import XCTest
@testable import Store

final class LabelValueModelTests: XCTestCase {
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        //Given
        let json = FileName.labelValue
        
        //When
        let sut: LabelValueModel = try loadCodableObject(fromJSON: json)
        
        //Then
        XCTAssertEqual(sut.type, ComponentType.labelValue)
        XCTAssertEqual(sut.label, "Label Text")
        XCTAssertEqual(sut.value, "Value Text")
    }
    
    func testDecoding_WhenMissingKeyType_ShouldThrowsDecodeError() throws {
        //Given
        let json = FileName.labelValue
        let typeKey = "type"
        
        //When
        let sut: Data = try loadDataObject(fromJSON: json)
            .json(deletingKeyPaths: typeKey)
        
        //Then
        assertThrowsKeyNotFound(typeKey, decoding: LabelValueModel.self, from: sut)
    }
    
    func testDecoding_WhenMissingKeyLabel_ShouldThrowsDecodeError() throws {
        //Given
        let json = FileName.labelValue
        let labelKey = "label"
        
        //When
        let sut: Data = try loadDataObject(fromJSON: json)
            .json(deletingKeyPaths: labelKey)
        
        //Then
        assertThrowsKeyNotFound(labelKey, decoding: LabelValueModel.self, from: sut)
    }
    
    func testDecoding_WhenMissingKeyValue_ShouldThrowsDecodeError() throws {
        //Given
        let json = FileName.labelValue
        let valueKey = "value"
        
        //When
        let sut: Data = try loadDataObject(fromJSON: json)
            .json(deletingKeyPaths: valueKey)
        
        //Then
        assertThrowsKeyNotFound(valueKey, decoding: LabelValueModel.self, from: sut)
    }
}
