@testable import Store
import XCTest

final class ItemPriceModelTests: XCTestCase {
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: ItemPriceModel = try loadCodableObject(fromJSON: FileName.itemPrice)
        
        XCTAssertEqual(sut.type, ComponentType.itemPrice)
        XCTAssertEqual(sut.price, "R$ 299,00")
        XCTAssertEqual(sut.oldPrice, "R$ 350,00")
        XCTAssertEqual(sut.installments, "Até 6x R$49,83")
        XCTAssertEqual(sut.spotlight, "15% off")
    }
    
    func testDecoding_WhenMissingKeyPrice_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.itemPrice)
        assertThrowsKeyNotFound("price", decoding: ItemPriceModel.self, from: try sut.json(deletingKeyPaths: "price"))
    }
}
