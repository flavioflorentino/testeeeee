@testable import Store
import XCTest

final class DividerModelTests: XCTestCase {
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: DividerModel = try loadCodableObject(fromJSON: FileName.divider)
        
        XCTAssertEqual(sut.type, ComponentType.divider)
        XCTAssertEqual(sut.color, "#FFFFFF")
    }
    
    func testDecoding_WhenMissingKeyType_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.divider)
        assertThrowsKeyNotFound("type", decoding: DividerModel.self, from: try sut.json(deletingKeyPaths: "type"))
    }
    
    func testDecoding_WhenMissingKeyColor_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.divider)
        assertThrowsKeyNotFound("color", decoding: DividerModel.self, from: try sut.json(deletingKeyPaths: "color"))
    }
}

