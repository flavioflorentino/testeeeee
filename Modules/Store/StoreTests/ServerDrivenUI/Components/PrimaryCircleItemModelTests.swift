@testable import Store
import XCTest

final class PrimaryCircleItemModelTests: XCTestCase {
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: PrimaryCircleItemModel = try loadCodableObject(fromJSON: FileName.primaryCircleItem)
        
        XCTAssertEqual(sut.type, ComponentType.primaryCircleItem)
        XCTAssertEqual(sut.image, "https://www.image.com/example.png")
        XCTAssertEqual(sut.title, "title example")
        XCTAssertEqual(sut.action?.type, .openScreen)
    }
    
    func testDecoding_WhenMissingKeyType_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.primaryCircleItem)
        assertThrowsKeyNotFound("type", decoding: PrimaryCircleItemModel.self, from: try sut.json(deletingKeyPaths: "type"))
    }
    
    func testDecoding_WhenMissingKeyImage_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.primaryCircleItem)
        assertThrowsKeyNotFound("image", decoding: PrimaryCircleItemModel.self, from: try sut.json(deletingKeyPaths: "image"))
    }
    
    func testDecoding_WhenMissingKeyTitle_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.primaryCircleItem)
        assertThrowsKeyNotFound("title", decoding: PrimaryCircleItemModel.self, from: try sut.json(deletingKeyPaths: "title"))
    }
}
