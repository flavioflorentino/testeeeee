@testable import Store
import XCTest

final class HeaderModelTests: XCTestCase {
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: HeaderModel = try loadCodableObject(fromJSON: FileName.header)
        
        XCTAssertEqual(sut.type, ComponentType.primaryHeader)
        XCTAssertEqual(sut.title, "iFood")
        XCTAssertEqual(sut.banner, "https://movile.blog/wp-content/uploads/2018/06/03.png")
        XCTAssertEqual(sut.avatar, "https://lh3.googleusercontent.com/FrMp1xgQ4_QjTp7pO0QCc8T24FyZNhhONviUk886e3A53-J9iN_6aBYoEuIHasphjg")
        XCTAssertEqual(sut.rightButton?.label, "Ajuda")
        XCTAssertEqual(sut.rightButton?.action?.type, ActionType.webview)
    }
    
    func testDecoding_WhenMissingKeyType_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.header)
        assertThrowsKeyNotFound("type", decoding: HeaderModel.self, from: try sut.json(deletingKeyPaths: "type"))
    }
    
    func testDecoding_WhenMissingKeyTitle_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.header)
        assertThrowsKeyNotFound("title", decoding: HeaderModel.self, from: try sut.json(deletingKeyPaths: "title"))
    }
}
