@testable import Store
import XCTest

final class CirclesCarouselModelTests: XCTestCase {
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: CirclesCarouselModel = try loadCodableObject(fromJSON: FileName.circlesCarousel)
        
        XCTAssertEqual(sut.type, ComponentType.circlesCarousel)
        XCTAssertTrue(sut.items.isNotEmpty)
    }
    
    func testDecoding_WhenMissingKeyType_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.circlesCarousel)
        assertThrowsKeyNotFound("type", decoding: CirclesCarouselModel.self, from: try sut.json(deletingKeyPaths: "type"))
    }
    
    func testDecoding_WhenMissingKeyItems_ShouldThrowsDecodeError() throws {
        let data = try loadDataObject(fromJSON: FileName.circlesCarousel).json(deletingKeyPaths: "items")
        let json = try XCTUnwrap(String(data: data, encoding: .utf8))
        let sut: CirclesCarouselModel = try decodeObject(fromJSON: json)
        XCTAssertTrue(sut.items.isEmpty)
    }
}
