@testable import Store
import XCTest

final class CarouselModelTests: XCTestCase {
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: CarouselModel = try loadCodableObject(fromJSON: FileName.carousel)
        
        XCTAssertEqual(sut.type, ComponentType.carousel)
        XCTAssertTrue(sut.items.isNotEmpty)
    }
    
    func testDecoding_WhenMissingKeyType_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.carousel)
        assertThrowsKeyNotFound("type", decoding: CarouselModel.self, from: try sut.json(deletingKeyPaths: "type"))
    }
    
    func testDecoding_WhenMissingKeyItems_ShouldThrowsDecodeError() throws {
        let data = try loadDataObject(fromJSON: FileName.carousel).json(deletingKeyPaths: "items")
        let json = try XCTUnwrap(String(data: data, encoding: .utf8))
        let sut: CarouselModel = try decodeObject(fromJSON: json)
        XCTAssertTrue(sut.items.isEmpty)
    }
}
