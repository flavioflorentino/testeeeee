@testable import Store
import XCTest

final class BannerModelTests: XCTestCase {
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: BannerModel = try loadCodableObject(fromJSON: FileName.banner)
        
        XCTAssertEqual(sut.type, ComponentType.banner)
        XCTAssertEqual(sut.image, "https://image.com/example.png")
        XCTAssertEqual(sut.action?.type, ActionType.openScreen)
    }
    
    func testDecoding_WhenMissingKeyType_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.banner)
        assertThrowsKeyNotFound("type", decoding: BannerModel.self, from: try sut.json(deletingKeyPaths: "type"))
    }
    
    func testDecoding_WhenMissingKeyImage_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.banner)
        assertThrowsKeyNotFound("image", decoding: BannerModel.self, from: try sut.json(deletingKeyPaths: "image"))
    }
}

