@testable import Store
import XCTest

final class OrderStateModelTests: XCTestCase {
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: OrderStateModel = try loadCodableObject(fromJSON: FileName.orderState)
        
        XCTAssertEqual(sut.type, ComponentType.orderState)
        XCTAssertEqual(sut.title, "title")
        XCTAssertEqual(sut.shortDescription, "short_description")
        XCTAssertEqual(sut.description, "description")
        XCTAssertEqual(sut.statusIcon, "success")
        XCTAssertEqual(sut.button?.label, "button_label")
        XCTAssertEqual(sut.button?.style.fontColor, "#FFFFFF")
        XCTAssertEqual(sut.button?.style.backgroundColor, "#FFFFFF")
        XCTAssertEqual(sut.topLineColor, "#FFFFFF")
        XCTAssertEqual(sut.bottomLineColor, "#FFFFFF")
        XCTAssertEqual(sut.titleColor, "#FFFFFF")
        XCTAssertEqual(sut.descriptionColor, "#FFFFFF")
        XCTAssertEqual(sut.action?.type, ActionType.webview)
    }
    
    func testDecoding_WhenMissingKeyType_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.orderState)
        assertThrowsKeyNotFound("type", decoding: OrderStateModel.self, from: try sut.json(deletingKeyPaths: "type"))
    }
    
    func testDecoding_WhenMissingKeyTitle_ShouldThrowsDecodeError() throws {
        let data = try loadDataObject(fromJSON: FileName.orderState).json(deletingKeyPaths: "title")
        let json = try XCTUnwrap(String(data: data, encoding: .utf8))
        let sut: OrderStateModel = try decodeObject(fromJSON: json)
        XCTAssertNil(sut.title)
    }
    
    func testDecoding_WhenMissingKeyTitleColor_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.orderState)
        assertThrowsKeyNotFound("titleColor", decoding: OrderStateModel.self, from: try sut.json(deletingKeyPaths: "title_color"))
    }}
