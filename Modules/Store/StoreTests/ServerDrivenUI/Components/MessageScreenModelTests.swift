@testable import Store
import XCTest

final class MessageScreenModelTests: XCTestCase {
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: MessageScreenModel = try loadCodableObject(fromJSON: FileName.messageScreen)
        
        XCTAssertEqual(sut.type, .messageScreen)
        XCTAssertEqual(sut.image, "https://www.example.com/image.png")
        XCTAssertEqual(sut.title, "Nenhum frete para exibir")
        XCTAssertEqual(sut.description, "Por enquanto esse produto não pode ser entregue no CEP informado.")
        XCTAssertEqual(sut.button?.label, "Escolher outro produto")
        XCTAssertEqual(sut.action?.type, .webview)
    }
    
    func testDecoding_WhenMissingKeyType_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.messageScreen)
        assertThrowsKeyNotFound("type", decoding: MessageScreenModel.self, from: try sut.json(deletingKeyPaths: "type"))
    }
    
    func testDecoding_WhenMissingKeyImage_ShouldParseWithNilImage() throws {
        let sut: MessageScreenModel = try loadCodableObject(fromJSON: FileName.messageScreen, deletingKeypaths: "image")

        XCTAssertNil(sut.image)

        XCTAssertEqual(sut.title, "Nenhum frete para exibir")
        XCTAssertEqual(sut.description, "Por enquanto esse produto não pode ser entregue no CEP informado.")
        XCTAssertEqual(sut.button?.label, "Escolher outro produto")
        XCTAssertEqual(sut.action?.type, .webview)
    }
    
    func testDecoding_WhenMissingKeyTitle_ShouldParseWithNilTitle() throws {
        let sut: MessageScreenModel = try loadCodableObject(fromJSON: FileName.messageScreen, deletingKeypaths: "title")

        XCTAssertNil(sut.title)
        
        XCTAssertEqual(sut.type, .messageScreen)
        XCTAssertEqual(sut.image, "https://www.example.com/image.png")
        XCTAssertEqual(sut.description, "Por enquanto esse produto não pode ser entregue no CEP informado.")
        XCTAssertEqual(sut.button?.label, "Escolher outro produto")
        XCTAssertEqual(sut.action?.type, .webview)
    }
    
    func testDecoding_WhenMissingKeyDescription_ShouldParseWithNilDescription() throws {
        let sut: MessageScreenModel = try loadCodableObject(fromJSON: FileName.messageScreen, deletingKeypaths: "description")

        XCTAssertNil(sut.description)

        XCTAssertEqual(sut.type, .messageScreen)
        XCTAssertEqual(sut.image, "https://www.example.com/image.png")
        XCTAssertEqual(sut.title, "Nenhum frete para exibir")
        XCTAssertEqual(sut.button?.label, "Escolher outro produto")
        XCTAssertEqual(sut.action?.type, .webview)
    }
    
    func testDecoding_WhenMissingKeyButton_ShouldParseWithNilButton() throws {
        let sut: MessageScreenModel = try loadCodableObject(fromJSON: FileName.messageScreen, deletingKeypaths: "button")

        XCTAssertNil(sut.button)

        XCTAssertEqual(sut.type, .messageScreen)
        XCTAssertEqual(sut.image, "https://www.example.com/image.png")
        XCTAssertEqual(sut.title, "Nenhum frete para exibir")
        XCTAssertEqual(sut.description, "Por enquanto esse produto não pode ser entregue no CEP informado.")
        XCTAssertEqual(sut.action?.type, .webview)
    }
    
    func testDecoding_WhenMissingKeyAction_ShouldParseWithNilAction() throws {
        let sut: MessageScreenModel = try loadCodableObject(fromJSON: FileName.messageScreen, deletingKeypaths: "action")

        XCTAssertNil(sut.action)

        XCTAssertEqual(sut.type, .messageScreen)
        XCTAssertEqual(sut.image, "https://www.example.com/image.png")
        XCTAssertEqual(sut.title, "Nenhum frete para exibir")
        XCTAssertEqual(sut.description, "Por enquanto esse produto não pode ser entregue no CEP informado.")
        XCTAssertEqual(sut.button?.label, "Escolher outro produto")
    }
}
