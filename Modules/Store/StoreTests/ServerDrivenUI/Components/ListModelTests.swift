@testable import Store
import XCTest

final class ListModelTests: XCTestCase {
    private let fileName = "listModel"
    
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: ListModel = try loadCodableObject(fromJSON: FileName.list)
        
        XCTAssertEqual(sut.type, ComponentType.productCard)
        XCTAssertTrue(sut.items.isNotEmpty)
    }
    
    func testDecoding_WhenMissingKeyType_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.list)
        assertThrowsKeyNotFound("type", decoding: ListModel.self, from: try sut.json(deletingKeyPaths: "type"))
    }
    
    func testDecoding_WhenMissingKeyItems_ShouldThrowsDecodeError() throws {
        let data = try loadDataObject(fromJSON: FileName.list).json(deletingKeyPaths: "items")
        let json = try XCTUnwrap(String(data: data, encoding: .utf8))
        let sut: ListModel = try decodeObject(fromJSON: json)
        XCTAssertTrue(sut.items.isEmpty)
    }
}
