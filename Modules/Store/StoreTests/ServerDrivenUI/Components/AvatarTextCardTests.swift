@testable import Store
import XCTest

final class AvatarTextCardModelTests: XCTestCase {
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: AvatarTextCardModel = try loadCodableObject(fromJSON: FileName.avatarTextCard)

        XCTAssertEqual(sut.type, ComponentType.avatarTextCard)
        XCTAssertEqual(sut.avatar, "https://lh3.googleusercontent.com/FrMp1xgQ4_QjTp7pO0QCc8T24FyZNhhONviUk886e3A53-J9iN_6aBYoEuIHasphjg")
        XCTAssertEqual(sut.text, "Clique aqui para abastecer")
        XCTAssertEqual(sut.action?.type, ActionType.payment)
    }

    func testDecoding_WhenMissingKeyAvatar_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.avatarTextCard)
        assertThrowsKeyNotFound("avatar", decoding: AvatarTextCardModel.self, from: try sut.json(deletingKeyPaths: "avatar"))
    }

    func testDecoding_WhenMissingKeyValue_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.avatarTextCard)
        assertThrowsKeyNotFound("text", decoding: AvatarTextCardModel.self, from: try sut.json(deletingKeyPaths: "text"))
    }
}
