@testable import Store
import XCTest

final class ActionSubtitleModelTests: XCTestCase {    
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: SectionHeaderModel = try loadCodableObject(fromJSON: FileName.sectionHeader)
        
        XCTAssertEqual(sut.type, ComponentType.sectionHeader)
        XCTAssertEqual(sut.text, "Recargas")
        XCTAssertEqual(sut.action?.type, ActionType.openScreen)
        XCTAssertEqual(sut.actionTitle, "ver mais")
    }
    
    func testDecoding_WhenMissingKeyType_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.sectionHeader)
        assertThrowsKeyNotFound("type", decoding: SectionHeaderModel.self, from: try sut.json(deletingKeyPaths: "type"))
    }
    
    func testDecoding_WhenMissingKeyText_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.sectionHeader)
        assertThrowsKeyNotFound("text", decoding: SectionHeaderModel.self, from: try sut.json(deletingKeyPaths: "text"))
    }
    
    func testDecoding_WhenMissingKeyActionTitle_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.sectionHeader)
        assertThrowsKeyNotFound("actionTitle", decoding: SectionHeaderModel.self, from: try sut.json(deletingKeyPaths: "action_title"))
    }
}
