@testable import Store
import XCTest

final class ItemSummaryModelTests: XCTestCase {
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: ItemSummaryModel = try loadCodableObject(fromJSON: FileName.itemSummary)
        
        XCTAssertEqual(sut.type, ComponentType.itemSummary)
        XCTAssertEqual(sut.image, "https://image.com/example.png")
        XCTAssertEqual(sut.title, "title")
        XCTAssertEqual(sut.summary, "fisrt line \nsecond line \nthird line \nfourth line")
        XCTAssertEqual(sut.action?.type, ActionType.openScreen)
    }
    
    func testDecoding_WhenMissingKeyImage_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.itemSummary)
        assertThrowsKeyNotFound("image", decoding: ItemSummaryModel.self, from: try sut.json(deletingKeyPaths: "image"))
    }
    
    func testDecoding_WhenMissingKeyTitle_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.itemSummary)
        assertThrowsKeyNotFound("title", decoding: ItemSummaryModel.self, from: try sut.json(deletingKeyPaths: "title"))
    }
    
    func testDecoding_WhenMissingKeyASummary_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.itemSummary)
        assertThrowsKeyNotFound("summary", decoding: ItemSummaryModel.self, from: try sut.json(deletingKeyPaths: "summary"))
    }
}
