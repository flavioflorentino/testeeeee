@testable import Store
import XCTest

final class PrimaryItemModelTests: XCTestCase {
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: PrimaryItemModel = try loadCodableObject(fromJSON: FileName.primaryItem)
        
        XCTAssertEqual(sut.type, ComponentType.primaryItem)
        XCTAssertEqual(sut.image, "https://image.com/example.png")
        XCTAssertEqual(sut.title, "Título")
        XCTAssertEqual(sut.subtitle, "Subtítulo")
        XCTAssertEqual(sut.action?.type, .openLegacyDigitalGood)
    }
    
    func testDecoding_WhenMissingKeyType_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.primaryItem)
        assertThrowsKeyNotFound("type", decoding: PrimaryItemModel.self, from: try sut.json(deletingKeyPaths: "type"))
    }
    
    func testDecoding_WhenMissingKeyImage_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.primaryItem)
        assertThrowsKeyNotFound("image", decoding: PrimaryItemModel.self, from: try sut.json(deletingKeyPaths: "image"))
    }
    
    func testDecoding_WhenMissingKeyTitle_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.primaryItem)
        assertThrowsKeyNotFound("title", decoding: PrimaryItemModel.self, from: try sut.json(deletingKeyPaths: "title"))
    }
    
    func testDecoding_WhenMissingKeySubtitle_ShouldParseWithNilSubtitle() throws {
        let sut: PrimaryItemModel = try loadCodableObject(fromJSON: FileName.primaryItem, deletingKeypaths: "subtitle")

        XCTAssertNil(sut.subtitle)

        XCTAssertEqual(sut.type, .primaryItem)
        XCTAssertEqual(sut.title, "Título")
        XCTAssertEqual(sut.image, "https://image.com/example.png")
        XCTAssertEqual(sut.action?.type, .openLegacyDigitalGood)
    }
}
