@testable import Store
import XCTest

final class CarouselProductDetailModelTests: XCTestCase {
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: CarouselProductDetailModel = try loadCodableObject(fromJSON: FileName.carouselProductDetail)
        
        XCTAssertEqual(sut.type, ComponentType.carouselProductDetail)
        XCTAssertTrue(sut.items.isNotEmpty)
    }
    
    func testDecoding_WhenMissingKeyType_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.carouselProductDetail)
        assertThrowsKeyNotFound("type", decoding: CarouselProductDetailModel.self, from: try sut.json(deletingKeyPaths: "type"))
    }
    
    func testDecoding_WhenMissingKeyItems_ShouldThrowsDecodeError() throws {
        let data = try loadDataObject(fromJSON: FileName.carouselProductDetail).json(deletingKeyPaths: "items")
        let json = try XCTUnwrap(String(data: data, encoding: .utf8))
        let sut: CarouselProductDetailModel = try decodeObject(fromJSON: json)
        XCTAssertTrue(sut.items.isEmpty)
    }
}
