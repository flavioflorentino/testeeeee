@testable import Store
import XCTest

final class ButtonModelTests: XCTestCase {
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: ButtonModel = try loadCodableObject(fromJSON: FileName.button)
        
        XCTAssertEqual(sut.type, ComponentType.link)
        XCTAssertEqual(sut.label, "Saiba Mais")
        XCTAssertEqual(sut.action?.type, ActionType.webview)
    }
    
    func testDecoding_WhenMissingKeyType_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.button)
        assertThrowsKeyNotFound("type", decoding: ButtonModel.self, from: try sut.json(deletingKeyPaths: "type"))
    }
    
    func testDecoding_WhenMissingKeyLabel_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.button)
        assertThrowsKeyNotFound("label", decoding: ButtonModel.self, from: try sut.json(deletingKeyPaths: "label"))
    }
}
