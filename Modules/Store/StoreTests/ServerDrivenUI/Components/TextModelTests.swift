@testable import Store
import XCTest

final class TextModelTests: XCTestCase {
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: TextModel = try loadCodableObject(fromJSON: FileName.text)
        
        XCTAssertEqual(sut.type, ComponentType.title)
        XCTAssertEqual(sut.text, "IFood")
        XCTAssertEqual(sut.style?.fontColor, "#00AC4A")
    }
    
    func testDecoding_WhenMissingKeyType_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.text)
        assertThrowsKeyNotFound("type", decoding: TextModel.self, from: try sut.json(deletingKeyPaths: "type"))
    }
    
    func testDecoding_WhenMissingKeyText_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.text)
        assertThrowsKeyNotFound("text", decoding: TextModel.self, from: try sut.json(deletingKeyPaths: "text"))
    }
}
