@testable import Store
import XCTest

final class InputModelTests: XCTestCase {
    private let fileName = "inputModel"
    
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: InputModel = try loadCodableObject(fromJSON: FileName.input)
        
        XCTAssertEqual(sut.type, ComponentType.input)
        XCTAssertEqual(sut.key, "date")
        XCTAssertEqual(sut.inputType, InputType.number)
        XCTAssertEqual(sut.maxSize, 10)
        XCTAssertEqual(sut.isEnabled, true)
        XCTAssertEqual(sut.model?.text, "23062020")
        XCTAssertEqual(sut.model?.label, "Digite a Data")
        XCTAssertEqual(sut.rules.count, 2)
        XCTAssertEqual(sut.mask?.pattern, "$1/$2/$3")
        XCTAssertEqual(sut.mask?.regex, "(\\d{2})(\\d{2})(\\d{4})")
    }
    
    func testDecoding_WhenMissingKeyType_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.input)
        assertThrowsKeyNotFound("type", decoding: InputModel.self, from: try sut.json(deletingKeyPaths: "type"))
    }
    
    func testDecoding_WhenMissingKey_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.input)
        assertThrowsKeyNotFound("key", decoding: InputModel.self, from: try sut.json(deletingKeyPaths: "key"))
    }
    
    func testDecoding_WhenMissingKeyInputType_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.input)
        assertThrowsKeyNotFound("inputType", decoding: InputModel.self, from: try sut.json(deletingKeyPaths: "input_type"))
    }
    
    func testDecoding_WhenMissingKeyIsMaxSize_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.input)
        assertThrowsKeyNotFound("maxSize", decoding: InputModel.self, from: try sut.json(deletingKeyPaths: "max_size"))
    }
}
