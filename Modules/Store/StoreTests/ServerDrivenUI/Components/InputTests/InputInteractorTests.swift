@testable import Store
import XCTest

final class InputInteractorTests: XCTestCase {
    private let output = InputResultSpy()
    private lazy var sut = InputInteractor(bindTo: output)
    
    func testInputDataValidationSuccess_WhenReceiveDataFromInput_ShouldValidateTheData() throws {
        sut.data = "05590-100"
        sut.rules = [Rule(regex: "^(?!\\s*$).+", message: "O campo não pode estar vazio"),
                     Rule(regex: "(\\d{5})-(\\d{3})", message: "CEP inválido")]
        
        let result = sut.isDataValid()

        XCTAssertTrue(result)
        XCTAssertEqual(output.didCallDataIsValid, 1)
    }
    
    func testInputDataValidationFailed_WhenReceiveEmptyDataFromInput_ShouldThrowInvalidData() throws {
        sut.data = ""
        sut.rules = [Rule(regex: "^(?!\\s*$).+", message: "O campo não pode estar vazio"),
                     Rule(regex: "(\\d{5})-(\\d{3})", message: "CEP inválido")]
        
        let result = sut.isDataValid()

        XCTAssertFalse(result)
        XCTAssertEqual(output.message, "O campo não pode estar vazio")
        XCTAssertEqual(output.didCallNotifyDataIsInvalid, 1)
    }
    
    func testInputDataValidationFailed_WhenReceiveWrongDataFromInput_ShouldThrowInvalidData() throws {
        sut.data = "0552"
        sut.rules = [Rule(regex: "^(?!\\s*$).+", message: "O campo não pode estar vazio"),
                     Rule(regex: "(\\d{5})-(\\d{3})", message: "CEP inválido")]
        
        let result = sut.isDataValid()

        XCTAssertFalse(result)
        XCTAssertEqual(output.message, "CEP inválido")
        XCTAssertEqual(output.didCallNotifyDataIsInvalid, 1)
    }
    
    func testMaskInputDataSuccessfully_WhenReceiveTextFromInput_ShouldMaskTheText() throws {
        let mask = Mask(regex: "(\\d{2})(\\d{2})(\\d{4})", pattern: "$1/$2/$3")
        sut.add(mask, to: "25122020", inputType: .number)
        
        XCTAssertEqual(output.maskedText, "25/12/2020")
        XCTAssertEqual(output.didCallUpdateTextWithMaskedText, 1)
    }
}
