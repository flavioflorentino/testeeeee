@testable import Store
import XCTest

final class GenericListModelTests: XCTestCase {
    func testDecoding_WhenNotMissingAnyKey_ShouldParseAllValues() throws {
        let sut: GenericListModel = try loadCodableObject(fromJSON: FileName.genericList)
        
        XCTAssertEqual(sut.type, ComponentType.list)
        XCTAssertTrue(sut.components.isNotEmpty)
    }
    
    func testDecoding_WhenMissingKeyType_ShouldThrowsDecodeError() throws {
        let sut = try loadDataObject(fromJSON: FileName.genericList)
        assertThrowsKeyNotFound("type", decoding: ListModel.self, from: try sut.json(deletingKeyPaths: "type"))
    }
    
    func testDecoding_WhenMissingKeyItems_ShouldThrowsDecodeError() throws {
        let data = try loadDataObject(fromJSON: FileName.genericList).json(deletingKeyPaths: "items")
        let json = try XCTUnwrap(String(data: data, encoding: .utf8))
        let sut: GenericListModel = try decodeObject(fromJSON: json)
        XCTAssertTrue(sut.components.isEmpty)
    }
}
