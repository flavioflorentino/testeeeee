import Foundation

public protocol PaymentStyle: Decodable {}

public struct MarkdownStyle: PaymentStyle {
    public let markdown: String
}

public struct DescriptionStyle: PaymentStyle {
    public let description: String
}

public struct SideBySideStyle: PaymentStyle {
    public let left: String
    public let right: String
    public let popup: SurchargePayment?
    public let buttonIsEnable: Bool
    
    public struct SurchargePayment: Decodable {
        public let title: String
        public let surcharge: Double
        public let description: String
        public let surchargeAmount: Double
    }
}
