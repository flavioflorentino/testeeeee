import Foundation

public struct StorePayment {
    public let title: String
    public let description: String
    public let imageURL: String?
    public let infoURL: String?
    public let value: Double
    public let storeId: String
    public let inputData: [String: Any]
    public let externalData: [ServerData.Model]
    public let styles: [PaymentStyle]
}
