import UIKit
import UI

public enum Store {
    public struct Item {
        let id: String
        let name: String
        let event: ServerDrivenUIEvent?
        let serverData: ServerData
        
        init(_ id: String, _ name: String = "", serverData: ServerData = ServerData(), event: ServerDrivenUIEvent? = nil) {
            self.id = id
            self.name = name
            self.event = event
            self.serverData = serverData
        }
        
        public init(_ id: String) {
            self.init(id, "", serverData: ServerData(), event: nil)
        }
    }
    
    @discardableResult
    public static func present(
        item: Store.Item,
        with legacyStoreHandler: LegacyStoreHandler,
        from presentingViewController: UIViewController? = UIApplication.shared.keyWindow?.lastPresentedViewController
    ) -> Bool {
        if item.id.isEmpty {
            return false
        }
        
        guard let presentingViewController = presentingViewController else {
            return false
        }
        
        let storeViewController = StoreFactory.make(
            item,
            legacyStoreHandler: legacyStoreHandler
        )
        
        let navigationController = UINavigationController(
            rootViewController: storeViewController
        )
        
        presentingViewController.present(
            navigationController,
            animated: true
        )
        
        return true
    }
}
