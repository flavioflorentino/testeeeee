import Foundation

public struct StoreAddressResult {
    let id: String
    let alias: String?
    let street: String?
    let number: String?
    let complement: String?
    let neighborhood: String?
    let city: String?
    let state: String?
    let zipCode: String?
    
    public init(
        id: String,
        alias: String?,
        street: String?,
        number: String?,
        complement: String?,
        neighborhood: String?,
        city: String?,
        state: String?,
        zipCode: String?
    ) {
        self.id = id
        self.alias = alias
        self.street = street
        self.number = number
        self.complement = complement
        self.neighborhood = neighborhood
        self.city = city
        self.state = state
        self.zipCode = zipCode
    }
    
    var serverDataModel: [ServerData.Model] {
        var modelList: [ServerData.Model] = []
    
        let appender: (String, String?) -> Void = { key, value in
            guard let value = value else { return }
            modelList.append(.init(key: key, value: value))
        }
        
        appender("address_id", id)
        appender("alias", alias)
        appender("street", street)
        appender("number", number)
        appender("complement", complement)
        appender("neighborhood", neighborhood)
        appender("city", city)
        appender("state", state)
        appender("zipCode", zipCode)
                
        return modelList
    }
}
