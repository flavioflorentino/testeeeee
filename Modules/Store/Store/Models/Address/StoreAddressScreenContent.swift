import Foundation

public struct StoreAddressScreenContent {
    public let title: String
    public let text: String
    public let image: UIImage
    public let shouldDisplayHeader: Bool
}
