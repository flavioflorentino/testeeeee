import Core
import Foundation

public struct StoreReceiptResponse: Decodable {
    public let data: StoreReceiptData
}

public struct StoreReceiptData: Decodable {
    public let receipt: [[AnyHashable: Any]]
    
    private enum CodingKeys: String, CodingKey {
        case receipt
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let data = try container.decode(AnyCodable.self, forKey: .receipt)
        receipt = data.value as? [[AnyHashable: Any]] ?? []
    }
}
