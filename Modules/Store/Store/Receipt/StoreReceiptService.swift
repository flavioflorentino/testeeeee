import Core
import Foundation

public protocol StoreReceiptServicing {
    func getReceipt(transactionID: String, completion: @escaping (Result<StoreReceiptResponse, ApiError>) -> Void)
}

public final class StoreReceiptService {
    private let dependencies: HasMainQueue

    public init(dependencies: HasMainQueue) {
        self.dependencies = dependencies
    }
}

// MARK: - ReceiptServicing
extension StoreReceiptService: StoreReceiptServicing {
    public func getReceipt(transactionID: String, completion: @escaping (Result<StoreReceiptResponse, ApiError>) -> Void) {
        let endpoint = StoreServiceEndpoint.receipt(id: transactionID)
        Api<StoreReceiptResponse>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
