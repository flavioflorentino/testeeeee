import Foundation
import UIKit

public protocol LegacyStoreHandler: AnyObject {
    func viewControllerForDigitalGood(with dictionary: [String: Any]) -> UIViewController?
    func routeToPayment(_ item: StorePayment, from viewController: UIViewController)
    func addressViewController(
        with content: StoreAddressScreenContent,
        onAddressSelected: @escaping (StoreAddressResult) -> Void
    ) -> UIViewController
    func viewControllerToWebview(with url: URL) -> UIViewController
    func openReceipt(above viewController: UIViewController, with transactionId: String, type: String)
}
