import Foundation

protocol URLOpening {
    func open(
        _ url: URL,
        options: [UIApplication.OpenExternalURLOptionsKey: Any],
        completionHandler: ((Bool) -> Void)?
    )
}

extension URLOpening {
    func open(_ url: URL) {
        open(url, options: [:], completionHandler: nil)
    }
}
