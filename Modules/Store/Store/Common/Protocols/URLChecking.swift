import UIKit

protocol URLChecking {
    func canOpenURL(_ url: URL) -> Bool
}
