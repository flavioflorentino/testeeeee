import Foundation

extension Dictionary where Key == String, Value: Any {
    func isEqual(to other: [String: Any]) -> Bool {
        NSDictionary(dictionary: self).isEqual(to: other)
    }
}
