import Foundation

extension Decoder {
    func decode<T: Decodable>(_ key: String, as type: T.Type = T.self) throws -> T {
        try decode(AnyCodingKey(key), as: type)
    }

    private func decode<T: Decodable, K: CodingKey>(_ key: K, as type: T.Type = T.self) throws -> T {
        let container = try self.container(keyedBy: K.self)
        return try container.decode(type, forKey: key)
    }
}

private struct AnyCodingKey: CodingKey {
    var stringValue: String
    var intValue: Int?

    init(_ string: String) {
        stringValue = string
    }

    init?(stringValue: String) {
        self.stringValue = stringValue
    }

    init?(intValue: Int) {
        self.intValue = intValue
        self.stringValue = String(intValue)
    }
}

extension KeyedDecodingContainer {
    func decode<T: Decodable>(_ type: [T].Type, forKey key: Key) throws -> [T] {
        try decodeIfPresent(type, forKey: key) ?? []
    }

    func decode(_ type: Bool.Type, forKey key: Key) throws -> Bool {
        try decodeIfPresent(type, forKey: key) ?? false
    }
}
