import Foundation
import NewRelic

protocol ErrorTracking {
    func track(error: StoreError, origin: Any, function: String)
}

final class ErrorTracker: ErrorTracking {
    static let shared = ErrorTracker()
    
    func track(
        error: StoreError,
        origin: Any,
        function: String = #function
    ) {
        let attributes = [
            "class": String(describing: origin),
            "function": function
        ]
        
        NewRelic.recordError(error, attributes: attributes)
    }
}
