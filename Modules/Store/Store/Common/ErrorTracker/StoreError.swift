import Foundation

enum StoreError: Error, LocalizedError {
    case parseError(type: Any, error: Error)
    case parseException(message: String)
    case runtimeException(message: String)
    
    var errorDescription: String? {
        switch self {
        case let .parseError(type, error):
            return displayDecodingParseError(String(describing: type), error)
        case let .runtimeException(message):
            return message
        case let .parseException(message):
            return message
        }
    }
    
    private func displayDecodingParseError(_ origin: String, _ error: Error) -> String {
        let message: String
        switch error {
        case let error as DecodingError:
            switch error {
            case let .keyNotFound(key, _):
                message = "Could not find key ''\(key.stringValue)'' in JSON."
            case let .valueNotFound(type, _):
                message = "Could not find type ''\(type)'' in JSON."
            case let .typeMismatch(type, _):
                message = "Type mismatch for type ''\(type)'' in JSON."
            case .dataCorrupted:
                message = "Data found to be corrupted in JSON."
            default:
                message = error.localizedDescription
            }
        default:
            message = error.localizedDescription
        }
        return "Could not parse component/action of type ''\(origin)''. \(message)"
    }
}
