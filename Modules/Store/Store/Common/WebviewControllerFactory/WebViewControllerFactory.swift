import Foundation
import SafariServices

protocol WebViewControllerFactoring {
    func make(with: URL) -> UIViewController
}

final class WebViewControllerFactory: WebViewControllerFactoring {
    func make(with url: URL) -> UIViewController {
        let safariViewController = SFSafariViewController(url: url)
        
        if #available(iOS 11.0, *) {
            safariViewController.configuration.barCollapsingEnabled = true
            safariViewController.configuration.entersReaderIfAvailable = true
        }
        
        return safariViewController
    }
}
