import Foundation
import UI
import UIKit

// MARK: - ServerDrivenUIView Delegate
protocol ServerDrivenUIViewDelegate: AnyObject {
    func viewDidScroll(_ offSet: CGFloat)
    func didReceiveAction(_ result: ActionResult)
}

// MARK: - ServerDrivenUIView
final class ServerDrivenUIView: UIView {
    private enum Layout {
        static let contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 26, right: 0)
        static let estimatedRowHeight: CGFloat = 100
    }
    
    private lazy var dataSource: TableViewDataSource<Int, UIComponent> = {
        let dataSource = TableViewDataSource<Int, UIComponent>(view: tableView)
        dataSource.add(section: 0)
        dataSource.itemProvider = { tableView, indexPath, uiComponent -> UITableViewCell? in
            uiComponent.render(for: tableView, and: indexPath, actionDelegate: self, dataDelegate: self.interactor)
        }
        return dataSource
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.bounces = true
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.tableFooterView = UIView()
        tableView.contentInset = Layout.contentInset
        tableView.showsVerticalScrollIndicator = false
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = Layout.estimatedRowHeight
        return tableView
    }()
    
    var extendUnderNavigationBar = false {
        didSet {
            allowTableViewToBeExtendUnderNavigationBar(extendUnderNavigationBar)
        }
    }
    
    weak var delegate: ServerDrivenUIViewDelegate?

    private lazy var interactor = ServerDrivenUIInteractor(output: self)

    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func load(_ uiComponents: [UIComponent]) {
        dataSource.update(items: uiComponents, from: 0)
    }
}

// MARK: - Private Functions
private extension ServerDrivenUIView {
    func allowTableViewToBeExtendUnderNavigationBar(_ allow: Bool) {
        if #available(iOS 11.0, *) {
            tableView.contentInsetAdjustmentBehavior = allow ? .never : .automatic
        }
    }
}

// MARK: - ActionDelegate and ComponentData
extension ServerDrivenUIView: ActionDelegate {
    func performAction(from component: Component) {
        interactor.sendAnalytics(from: component)
        interactor.validateAction(from: component)
    }
}

// MARK: - UIScrollViewDelegate and UITableViewDelegate
extension ServerDrivenUIView: UIScrollViewDelegate, UITableViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offSet = min(1, scrollView.contentOffset.y / 50)
        delegate?.viewDidScroll(offSet)
    }
}

// MARK: - ViewConfiguration
extension ServerDrivenUIView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(tableView)
    }
    
    func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    func configureViews() {
        backgroundColor = .clear
        tableView.dataSource = dataSource
    }
}

// MARK: - InteractorOutputs
extension ServerDrivenUIView: ServerDrivenUIOutputs {
    func didReceiveResultFromComponentAction(_ result: ActionResult) {
        delegate?.didReceiveAction(result)
    }
}
