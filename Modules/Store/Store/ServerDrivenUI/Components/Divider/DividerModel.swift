import Foundation

struct DividerModel: Component {
    let type: ComponentType
    let action: ActionBuilder?
    let color: String
    
    var uiComponent: UIComponent.Type? { DividerUI.self }
}
