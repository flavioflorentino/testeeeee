import Foundation

typealias DividerUI = GenericUIComponent<DividerModel, DividerView>
