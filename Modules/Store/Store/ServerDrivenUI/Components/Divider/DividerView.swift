import Foundation
import UI
import UIKit

// MARK: - LayoutConstants
private enum Layout {
    enum Divider {
        static let height: CGFloat = 1
    }
}

// MARK: - DividerView
final class DividerView: ComponentView<DividerModel> {
    private lazy var containerView: UIView = {
        let view = UIView()
        return view
    }()
    
    override func buildViewHierarchy() {
        contentView.addSubview(containerView)
    }
    
    override func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.height.equalTo(Layout.Divider.height)
        }
    }
    
    override func configureViews() {
        guard let model = model,
              let dynamicColor = Colors.hexColor(model.color) else { return }
        containerView.backgroundColor = dynamicColor.color
    }
}
