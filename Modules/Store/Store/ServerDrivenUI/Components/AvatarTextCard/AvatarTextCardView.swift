import Foundation
import UI
import UIKit
import AssetsKit

// MARK: - AvatarTextCardViewLayout
private enum Layout {
    enum Disclosure {
        static let width: CGFloat = 15
        static let height: CGFloat = 10
    }
    enum Wrapper {
        static let height: CGFloat = 128
    }
}

// MARK: - AvatarTextCard
final class AvatarTextCardView: ComponentView<AvatarTextCardModel> {
    private lazy var wrapper: UIView = {
        let view = UIView()
        view.viewStyle(CardViewStyle())
            .with(\.cornerRadius, .light)
            .with(\.shadow, .light(color: .black(.light), opacity: .ultraLight))
        return view
    }()

    private lazy var avatar: UIImageView = {
        let imageView = UIImageView()
        imageView.imageStyle(AvatarImageStyle(size: .medium))
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .small))
            .with(\.textAlignment, .left)
        return label
    }()

    private lazy var disclosureIndicator: UIImageView = {
        let imageview = UIImageView()
        imageview.contentMode = .scaleAspectFill
        imageview.image = Resources.Icons.icoGreenRightArrow.image
        return imageview
    }()
    
    override func resetViews(_ views: UIView...) {
        super.resetViews(avatar, valueLabel)
    }

    override func buildViewHierarchy() {
        wrapper.addSubviews(avatar, valueLabel, disclosureIndicator)
        contentView.addSubview(wrapper)
    }

    override func setupConstraints() {
        wrapper.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base00)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base01)
            $0.height.equalTo(Layout.Wrapper.height)
        }
        avatar.snp.makeConstraints {
            $0.centerY.equalTo(wrapper)
            $0.width.equalTo(avatar.size.width)
            $0.height.equalTo(avatar.size.height)
            $0.leading.equalTo(wrapper).offset(Spacing.base02)
            $0.top.greaterThanOrEqualTo(wrapper).offset(Spacing.base01)
            $0.bottom.lessThanOrEqualTo(wrapper).inset(Spacing.base01)
        }
        valueLabel.snp.makeConstraints {
            $0.leading.equalTo(avatar.snp.trailing).offset(Spacing.base02)
            $0.top.equalTo(wrapper).offset(Spacing.base01)
            $0.trailing.equalTo(disclosureIndicator.snp.leading).inset(Spacing.base02)
            $0.centerY.equalTo(wrapper)
        }
        disclosureIndicator.snp.makeConstraints {
            $0.centerY.equalTo(wrapper)
            $0.width.equalTo(Layout.Disclosure.width)
            $0.height.equalTo(Layout.Disclosure.height)
            $0.trailing.equalTo(wrapper).inset(Spacing.base02)
        }
    }

    override func configureViews() {
        guard let model = model else { return }
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapAction)))
        backgroundColor = Colors.backgroundPrimary.color
        valueLabel.text = model.text
        avatar.setImage(url: URL(string: model.avatar))
    }
}
