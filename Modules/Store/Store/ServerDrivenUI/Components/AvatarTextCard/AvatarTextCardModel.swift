import Foundation

struct AvatarTextCardModel: Component {
    var type: ComponentType { .avatarTextCard }
    let action: ActionBuilder?

    let text: String
    let avatar: String

    var uiComponent: UIComponent.Type? { AvatarTextCardUI.self }
}
