import Foundation

typealias AvatarTextCardUI = GenericUIComponent<AvatarTextCardModel, AvatarTextCardView>
