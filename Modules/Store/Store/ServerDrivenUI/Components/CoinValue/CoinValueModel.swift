import Foundation

// MARK: - CoinValue
struct CoinValueModel: Component {
    let type: ComponentType
    let action: ActionBuilder?
    
    let items: [CoinValueItemModel]
    
    var uiComponent: UIComponent.Type? { CoinValueUI.self }
}

// MARK: - CoinValueItem
struct CoinValueItemModel: Component {
    let action: ActionBuilder?
    var type: ComponentType { .coinValue }
    
    let text: String
    
    var uiComponent: UIComponent.Type? { nil }
}
