import Foundation
import UI
import UIKit

/*
This class is intended to serve as a wrapper for the items of the CollectionView that can be selected
*/
private final class Item {
    var isSelected: Bool
    var component: Component
    
    init(_ component: Component, isSelected: Bool = false) {
        self.component = component
        self.isSelected = isSelected
    }
}

// MARK: - LayoutConstants
private enum Layout {
    enum Cell {
        static let outerRadius: CGFloat = size / 2.3
        static let innerRadius: CGFloat = size / 2.7
        static let size: CGFloat = UIScreen.main.bounds.width * 0.2
    }
    
    enum CollectionView {
        static let sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
}

// MARK: - CoinValueView
final class CoinValueView: ComponentView<CoinValueModel> {
    private var items: [Item] = []
    
    private lazy var collectionViewLayout: UICollectionViewLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = 0
        layout.itemSize = CGSize(width: Layout.Cell.size, height: Layout.Cell.size)
        layout.sectionInset = Layout.CollectionView.sectionInset
        return layout
    }()
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .clear
        collectionView.isScrollEnabled = false
        collectionView.register(CoinValueItemCell.self, forCellWithReuseIdentifier: CoinValueItemCell.identifier)
        return collectionView
    }()
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        contentView.frame = bounds
        contentView.layoutIfNeeded()
        return collectionView.contentSize
    }
    
    override func buildViewHierarchy() {
        contentView.addSubview(collectionView)
    }
    
    override func setupConstraints() {
        collectionView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    override func configureViews() {
        guard let model = model else {
            return
        }
        items = model.items.map {
            Item($0)
        }
    }
}

extension CoinValueView: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = items[indexPath.row]
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CoinValueItemCell.identifier, for: indexPath) as? CoinValueItemCell else {
            return UICollectionViewCell()
        }
        cell.configure(item)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectItem(at: indexPath.row)
        actionDelegate?.performAction(from: items[indexPath.row].component)
    }
    
    private func selectItem(at position: Int) {
        items.forEach({
            $0.isSelected = false
        })
        items[position].isSelected = true
        collectionView.reloadData()
    }
}

// MARK: - CoinValueItemView
private final class CoinValueItemCell: UICollectionViewCell, ViewConfiguration {
    private lazy var textLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textAlignment, .left)
        return label
    }()
    
    private lazy var outerView: UIView = {
        let view = UIView()
        view.viewStyle(BackgroundViewStyle(color: .white()))
            .with(\.border, .medium())
            .with(\.shadow, .light(color: .black(.light), opacity: .strong))
        view.layer.cornerRadius = Layout.Cell.outerRadius
        return view
    }()
    
    private lazy var innerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = Layout.Cell.innerRadius
        view.backgroundColor = Colors.grayscale050.color
        return view
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func buildViewHierarchy() {
        innerView.addSubview(textLabel)
        outerView.addSubview(innerView)
        contentView.addSubview(outerView)
    }
    
    func setupConstraints() {
        textLabel.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
        outerView.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(Spacing.base00)
        }
        innerView.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(Spacing.base00)
        }
    }
    
    func configure(_ item: Item) {
        guard let coinValue = item.component as? CoinValueItemModel else {
            return
        }
        selectItem(item)
        textLabel.text = coinValue.text
    }
    
    private func selectItem(_ item: Item) {
        if item.isSelected {
            outerView.layer.borderColor = Colors.branding500.color.cgColor
            innerView.backgroundColor = Colors.branding500.color
            textLabel.textColor = Colors.white.color
        } else {
            outerView.layer.borderColor = Colors.grayscale200.color.cgColor
            innerView.backgroundColor = Colors.grayscale050.color
            textLabel.textColor = Colors.black.color
        }
    }
}
