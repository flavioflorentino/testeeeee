import Foundation

typealias ButtonUI = GenericUIComponent<ButtonModel, ButtonView>
