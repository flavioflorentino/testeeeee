import Foundation

struct ButtonModel: Component {
    let type: ComponentType
    let action: ActionBuilder?
    
    let label: String

    var uiComponent: UIComponent.Type? { ButtonUI.self }
}
