import Foundation
import UI
import UIKit

final class ButtonView: ComponentView<ButtonModel> {
    private lazy var button: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("Cancelar", for: .normal)
        button.addTarget(self, action: #selector(didTapAction), for: .touchUpInside)
        return button
    }()
    
    private func configureStyle() {
        guard let model = model else { return }
        switch model.type {
        case .button:
            button.buttonStyle(PrimaryButtonStyle())
        case .link:
            button.buttonStyle(LinkButtonStyle())
        default:
            return
        }
    }
    
    override func buildViewHierarchy() {
        contentView.addSubview(button)
    }
    
    override func setupConstraints() {
        button.snp.makeConstraints {
            $0.width.equalTo(button.size.height)
            $0.top.bottom.equalToSuperview().inset(Spacing.base00)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base01)
        }
    }
    
    override func configureViews() {
        button.setTitle(model?.label, for: .normal)
        configureStyle()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        let states: [UIControl.State] = [.normal, .highlighted, .selected, .disabled]
        states.forEach {
            button.setBackgroundImage(nil, for: $0)
            button.setAttributedTitle(nil, for: $0)
            button.setTitleColor(nil, for: $0)
        }
    }
}
