import Foundation

final class InputInteractor: Validator, InputMask {
    typealias Outputs = ValidatorOutput & InputMaskOutput
    
    weak var outputs: Outputs?
    var rules: [Rule] = []
    var data: Any?

    init(bindTo view: Outputs) {
        self.outputs = view
    }
    
    func isDataValid() -> Bool {
        for rule in rules where !validate(data, with: rule.regex) {
            outputs?.notifyDataIsInvalid(message: rule.message)
            return false
        }
        outputs?.notifyDataIsValid()
        return true
    }
    
    func add(_ mask: Mask?, to text: String?, inputType: InputType) {
        var text = inputType == .number ? removeNonNumericCharsFromString(text) : text
        if let mask = mask {
            text = text?.replacingOccurrences(of: mask.regex, with: mask.pattern, options: [.regularExpression])
        }
        outputs?.updateTextWithMaskedText(text)
    }
    
    private func validate(_ data: Any?, with pattern: String) -> Bool {
        let text = data as? String ?? ""
        let predicate = NSPredicate(format: "SELF MATCHES %@", pattern)
        return predicate.evaluate(with: text)
    }
    
    private func removeNonNumericCharsFromString(_ text: String?) -> String? {
        guard let text = text else { return nil }
        let allowedChars = Set("1234567890")
        return text.filter { allowedChars.contains($0) }
    }
}
