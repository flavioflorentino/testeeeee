import Foundation
import UI
import UIKit

private enum Layout {
    enum TextField {
        static let height: CGFloat = 48.0
    }
}

final class InputView: ComponentView<InputModel> {
    private var isDeletingText = false
    private lazy var interactor: Validator & InputMask = InputInteractor(bindTo: self)
    
    private lazy var textField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.textColor = Colors.grayscale600.color
        textField.lineColor = Colors.grayscale400.color
        textField.titleColor = Colors.grayscale400.color
        textField.placeholderColor = Colors.grayscale400.color
        textField.selectedLineColor = Colors.branding300.color
        textField.selectedTitleColor = Colors.grayscale400.color
        textField.delegate = self
        textField.addTarget(self, action: #selector(textFieldEditingChanged), for: .editingChanged)
        return textField
    }()
    
    override func buildViewHierarchy() {
        contentView.addSubview(textField)
    }
    
    override func setupConstraints() {
        textField.snp.makeConstraints {
            $0.height.equalTo(Layout.TextField.height)
            $0.top.bottom.equalToSuperview().inset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    override func configureViews() {
        guard let model = model else { return }
        
        interactor.rules = model.rules
        dataDelegate?.dataContainer.add(interactor, for: model.key)

        configureKeyboardType(model.inputType)
        textField.isEnabled = model.isEnabled
        textField.placeholder = model.model?.label
        if let text: String = dataDelegate?.dataContainer.getData(for: model.key) {
            textField.text = text
        } else {
            interactor.add(model.mask, to: model.model?.text, inputType: model.inputType)
        }
    }
    
    private func configureKeyboardType(_ keyboard: InputType) {
        textField.isSecureTextEntry = false
        switch keyboard {
        case .number:
            textField.keyboardType = .numberPad
        case .password:
            textField.isSecureTextEntry = true
            textField.keyboardType = .default
        case .email:
            textField.keyboardType = .emailAddress
        default:
            textField.keyboardType = .default
        }
    }
}

extension InputView: UITextFieldDelegate {
    @objc
    private func textFieldEditingChanged(_ textField: UIPPFloatingTextField) {
        guard let model = model else { return }
        textField.errorMessage = nil
        if isDeletingText {
            updateTextWithMaskedText(textField.text)
        } else {
            interactor.add(model.mask, to: textField.text, inputType: model.inputType)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        isDeletingText = string.isEmpty
        guard let text = textField.text, let maxSize = model?.maxSize else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= maxSize
    }
}

extension InputView: ValidatorOutput, InputMaskOutput {
    func updateTextWithMaskedText(_ maskedText: String?) {
        textField.text = maskedText
        interactor.data = maskedText
    }
    
    func notifyDataIsInvalid(message: String) {
        textField.errorMessage = message
    }
    
    func notifyDataIsValid() {
        textField.errorMessage = nil
    }
    
    func notifyDataUpdated(_ data: Any?) {
        textField.text = data as? String
    }
}
