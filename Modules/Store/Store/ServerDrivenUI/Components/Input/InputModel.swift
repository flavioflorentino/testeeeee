import Foundation

struct Rule: Decodable {
    let regex: String
    let message: String
}

struct Mask: Decodable {
    let regex: String
    let pattern: String
}

enum InputType: String, Decodable {
    case password
    case number
    case email
    case general
}

struct InputModel: Component {
    struct Model: Decodable {
        let text: String?
        let label: String?
    }
    
    let type: ComponentType
    let action: ActionBuilder?

    let key: String
    let mask: Mask?
    let rules: [Rule]
    let maxSize: Int
    let inputType: InputType
    let isEnabled: Bool
    let model: Model?

    var uiComponent: UIComponent.Type? { InputUI.self }
}
