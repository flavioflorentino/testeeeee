import Foundation

enum ComponentType: String, Decodable {
    case link = "button_link"
    case title = "title"
    case input = "input"
    case primaryHeader = "store_header"
    case secondaryHeader = "store_header_centered"
    case button = "button"
    case subtitle = "subtitle"
    case markdown = "markdown_text"
    case mediumText = "medium_text"
    case productCard = "product_card_item"
    case coinValue = "coin_value"
    case sectionHeader = "section_header"
    case primaryItem = "primary_item"
    case list = "list"
    case toolbar = "simple_header"
    case carousel = "carousel"
    case banner = "banner"
    case avatarTextCard = "avatar_text_card"
    case itemPrice = "item_price"
    case carouselProductDetail = "carousel_product_detail"
    case imageCarousel = "image_carousel"
    case divider = "divider"
    case itemSummary = "item_summary"
    case orderState = "order_state"
    case labelValue = "label_value"
    case messageScreen = "message_screen"
    case primaryCard = "primary_card"
    case circlesCarousel = "circles_carousel"
    case primaryCircleItem = "primary_circle_item"
    
    var metatype: Component.Type {
        switch self {
        case .labelValue:
            return LabelValueModel.self
        case .productCard:
            return ListModel.self
        case .list:
            return GenericListModel.self
        case .title, .subtitle, .markdown, .mediumText:
            return TextModel.self
        case .primaryHeader, .secondaryHeader, .toolbar:
            return HeaderModel.self
        case .button, .link:
            return ButtonModel.self
        case .coinValue:
            return CoinValueModel.self
        case .input:
            return InputModel.self
        case .sectionHeader:
            return SectionHeaderModel.self
        case .primaryItem:
            return PrimaryItemModel.self
        case .carousel:
            return CarouselModel.self
        case .banner:
            return BannerModel.self
        case .avatarTextCard:
            return AvatarTextCardModel.self
        case .itemPrice:
            return ItemPriceModel.self
        case .carouselProductDetail:
            return CarouselProductDetailModel.self
        case .imageCarousel:
            return ImageCarouselModel.self
        case .divider:
            return DividerModel.self
        case .itemSummary:
            return ItemSummaryModel.self
        case .orderState:
            return OrderStateModel.self
        case .messageScreen:
            return MessageScreenModel.self
        case .primaryCard:
            return PrimaryCardModel.self
        case .circlesCarousel:
            return CirclesCarouselModel.self
        case .primaryCircleItem:
            return PrimaryCircleItemModel.self
        }
    }
}
