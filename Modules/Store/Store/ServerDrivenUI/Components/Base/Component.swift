import Foundation

protocol Component: Decodable {
    var type: ComponentType { get }
    var action: ActionBuilder? { get }
    var uiComponent: UIComponent.Type? { get }
}
