import UIKit

class GenericUIComponent<Model: Component, View: ComponentView<Model>>: UIComponent {
    let model: Component
    var isHeader: Bool { false }
    
    required init(model: Component) {
        self.model = model
    }
    
    func render(
        for tableView: UITableView,
        and indexPath: IndexPath,
        actionDelegate: ActionDelegate,
        dataDelegate: DataDelegate
    ) -> UITableViewCell {
        let getCellResult = getCell(
            View.self,
            with: Model.self,
            in: tableView,
            for: indexPath
        )
        
        guard
            let cell = getCellResult,
            let model = model as? Model
        else {
            return UITableViewCell()
        }
        
        // Delegates precisam ser definidos antes da model pois ao definir uma model o configureViews() é triggado
        // e alguns componentes já usam esses delegates para definir coisas (ex: InputView)
        cell.actionDelegate = actionDelegate
        cell.dataDelegate = dataDelegate
        
        cell.model = model

        return cell
    }
}

final class GenericHeaderUIComponent<Model: Component, View: ComponentView<Model>>: GenericUIComponent<Model, View> {
    override var isHeader: Bool { true }
}
