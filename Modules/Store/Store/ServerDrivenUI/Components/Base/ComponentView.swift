import Foundation
import UI
import UIKit

class ComponentView<T: Component>: UITableViewCell, ViewConfiguration {
    weak var dataDelegate: DataDelegate?
    weak var actionDelegate: ActionDelegate?
    var model: T? {
        didSet {
            resetViews()
            configureViews()
            configureStyles()
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
        selectionStyle = .none
        backgroundColor = .clear
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func resetView(_ view: UIView) {
        switch view {
        case let avatar as AvatarView:
            avatar.resetImage()
        case let cornerLabel as RoundedCornerLabel:
            cornerLabel.text = nil
        case let imageView as UIImageView:
            imageView.cancelRequest()
            imageView.image = nil
        case let label as UILabel:
            label.text = nil
            label.attributedText = nil
        case let textView as UITextView:
            textView.text = nil
            textView.attributedText = nil
        default:
            return
        }
    }

    func resetViews(_ views: UIView...) {
        views.forEach { resetView($0) }
    }
    
    func buildViewHierarchy() {}
    
    func setupConstraints() {}
    
    func configureViews() {}
    
    func configureStyles() {}

    @objc
    func didTapAction() {
        if let component = model {
            actionDelegate?.performAction(from: component)
        }
    }
}
