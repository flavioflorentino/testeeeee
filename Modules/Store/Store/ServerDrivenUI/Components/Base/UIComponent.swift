import Foundation
import UIKit

protocol UIComponent {
    var model: Component { get }
    init(model: Component)
    var isHeader: Bool { get }
    
    func render(
        for tableView: UITableView,
        and indexPath: IndexPath,
        actionDelegate: ActionDelegate,
        dataDelegate: DataDelegate
    ) -> UITableViewCell
}

extension UIComponent {
    func getCell<C: UITableViewCell, M: Component>(_ cellType: C.Type,
                                                   with modelType: M.Type,
                                                   in tableView: UITableView,
                                                   for indexPath: IndexPath) -> ComponentView<M>? {
        tableView.register(C.self, forCellReuseIdentifier: C.identifier)
        guard let cell = tableView.dequeueReusableCell(withIdentifier: C.identifier, for: indexPath) as? ComponentView<M> else {
            return nil
        }
        return cell
    }
}
