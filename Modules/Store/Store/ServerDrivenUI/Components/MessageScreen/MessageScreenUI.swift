import Foundation

typealias MessageScreenUI = GenericUIComponent<MessageScreenModel, MessageScreenView>
