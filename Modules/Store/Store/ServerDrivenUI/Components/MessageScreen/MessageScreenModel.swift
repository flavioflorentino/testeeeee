import Foundation

struct MessageScreenModel: Component {
    let type: ComponentType
    let action: ActionBuilder?
    
    let image: String?
    let title: String?
    let description: String?
    let button: Button?
    
    var uiComponent: UIComponent.Type? { MessageScreenUI.self }
}

extension MessageScreenModel {
    struct Button: Decodable {
        let label: String
    }
}
