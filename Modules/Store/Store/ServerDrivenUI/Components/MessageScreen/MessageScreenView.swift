import Foundation
import UI
import UIKit

private enum Layout {
    enum Image {
        static let size = CGSize(width: 130, height: 130)
    }
}

final class MessageScreenView: ComponentView<MessageScreenModel> {
    private lazy var avatar: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [titleLabel, descriptionLabel])
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textAlignment, .center)
            .with(\.textColor, .grayscale400())
        return label
    }()
    
    private lazy var button: UIButton = {
        let button = UIButton(type: .custom)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTapAction), for: .touchUpInside)
        return button
    }()
    
    override func resetViews(_ views: UIView...) {
        super.resetViews(descriptionLabel, titleLabel)
    }
    
    override func buildViewHierarchy() {
        contentView.addSubviews(avatar, stackView, button)
    }
    
    override func setupConstraints() {
        avatar.snp.makeConstraints {
            $0.size.equalTo(Layout.Image.size)
            $0.centerX.equalToSuperview()
            $0.bottom.equalTo(contentView.snp.centerY)
        }
        
        stackView.snp.makeConstraints {
            $0.top.equalTo(avatar.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        button.snp.makeConstraints {
            $0.width.equalTo(button.size.height)
            $0.top.greaterThanOrEqualTo(stackView.snp.bottom).offset(Spacing.base06)
            $0.bottom.equalToSuperview().inset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
    }
    
    override func configureViews() {
        guard let model = model else { return }
        let url = URL(string: model.image ?? "")
        avatar.setImage(url: url)
        
        titleLabel.isHidden = model.title == nil
        titleLabel.text = model.title
        
        descriptionLabel.isHidden = model.description == nil
        descriptionLabel.text = model.description
        
        guard let buttonModel = model.button else {
            button.isHidden = true
            return
        }
        button.setTitle(buttonModel.label, for: .normal)
    }
}
