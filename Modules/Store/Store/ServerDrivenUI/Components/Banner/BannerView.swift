import Foundation
import UI
import UIKit

final class BannerView: ComponentView<BannerModel> {
    private lazy var containerView: UIView = {
        let view = UIView()
        view.viewStyle(CardViewStyle())
            .with(\.shadow, .light(color: .black(.dark), opacity: .strong))
        view.layer.masksToBounds = true
        return view
    }()
    
    private lazy var promoImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()

    override func buildViewHierarchy() {
        containerView.addSubview(promoImage)
        contentView.addSubview(containerView)
    }
    
    override func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base00)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base01)
        }
        promoImage.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    override func configureViews() {
        guard let model = model else { return }
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapAction)))
        backgroundColor = Colors.backgroundPrimary.color
        promoImage.setImage(url: URL(string: model.image))
    }
}
