import Foundation

struct BannerModel: Component {
    let type: ComponentType
    let action: ActionBuilder?
    
    var image: String

    var uiComponent: UIComponent.Type? { BannerUI.self }
}
