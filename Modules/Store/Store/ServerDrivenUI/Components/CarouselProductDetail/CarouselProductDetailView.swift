import Foundation
import UI
import UIKit

// MARK: - LayoutConstants
private enum Layout {
    enum Cell {
        static let width: CGFloat = UIScreen.main.bounds.width
        static let height: CGFloat = width * 0.57
        static let size = CGSize(width: width, height: height)
    }
    
    enum CollectionView {
        static let sectionInset: UIEdgeInsets = .zero
    }
}

// MARK: - CarouselProductDetailView
final class CarouselProductDetailView: ComponentView<CarouselProductDetailModel> {
    private var items: [Component] = []
    
    private lazy var collectionViewLayout: UICollectionViewLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = Spacing.base01
        layout.itemSize = Layout.Cell.size
        layout.sectionInset = Layout.CollectionView.sectionInset
        return layout
    }()
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .clear
        collectionView.isPagingEnabled = true
        collectionView.isScrollEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.register(CarouselProductDetailViewCell.self, forCellWithReuseIdentifier: CarouselProductDetailViewCell.identifier)
        return collectionView
    }()
    
    private lazy var pageControl: UIPageControl = {
        let pageControl = UIPageControl()
        pageControl.pageIndicatorTintColor = Colors.grayscale400.color
        pageControl.currentPageIndicatorTintColor = Colors.black.lightColor
        pageControl.backgroundColor = .clear
        return pageControl
    }()
    
    override func buildViewHierarchy() {
        contentView.addSubview(collectionView)
        contentView.addSubview(pageControl)
    }
    
    override func setupConstraints() {
        collectionView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.height.equalTo(Layout.Cell.height)
        }
        pageControl.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.bottom.equalToSuperview().inset(Spacing.base01)
        }
    }
    
    override func configureViews() {
        items = model?.items ?? []
    }
}

extension CarouselProductDetailView: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        pageControl.numberOfPages = items.count
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = items[indexPath.row]
        guard
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CarouselProductDetailViewCell.identifier,
                                                          for: indexPath) as? CarouselProductDetailViewCell
        else {
            return UICollectionViewCell()
        }
        cell.configure(item)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        actionDelegate?.performAction(from: items[indexPath.row])
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        pageControl.currentPage = indexPath.row
    }
}

// MARK: - CarouselProductDetailViewCell
private final class CarouselProductDetailViewCell: UICollectionViewCell, ViewConfiguration {
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = Colors.white.lightColor
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    func buildViewHierarchy() {
        contentView.addSubview(imageView)
    }
    
    func setupConstraints() {
        imageView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    func configure(_ item: Component) {
        guard let imageModel = item as? ImageCarouselModel else {
            return
        }
        imageView.setImage(url: URL(string: imageModel.value))
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.cancelRequest()
        imageView.image = nil
    }
}
