import Foundation

struct CarouselProductDetailModel: Component {
    let type: ComponentType
    let action: ActionBuilder?
    
    let items: [ImageCarouselModel]

    var uiComponent: UIComponent.Type? { CarouselProductDetailUI.self }
}

struct ImageCarouselModel: Component {
    let type: ComponentType
    let action: ActionBuilder?
    
    let value: String
    
    var uiComponent: UIComponent.Type? { nil }
}
