import Foundation

typealias OrderStateUI = GenericUIComponent<OrderStateModel, OrderStateView>
