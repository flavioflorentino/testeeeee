import Foundation
import UI
import UIKit

// MARK: - OrderStateLayout
private enum Layout {
    enum Button {
        static let minimumWidth: CGFloat = 120
        static let titleInsets = Spacing.base02
        static let titleMinimunScaleFactor: CGFloat = 0.6
    }
    
    enum Line {
        static let width: CGFloat = 1
        static let bottonHeight: CGFloat = 70
    }
}

private enum StatusIcon: String {
    case success
    case failure
    case awaiting
    
    var icon: String {
        switch self {
        case .success:
            return Iconography.checkCircle.rawValue
        case .failure:
            return Iconography.exclamationCircle.rawValue
        case .awaiting:
            return Iconography.clock.rawValue
        }
    }
    
    var color: UIColor {
        switch self {
        case .success:
            return Colors.branding600.color
        case .failure:
            return Colors.critical900.color
        case .awaiting:
            return Colors.grayscale400.color
        }
    }
}

// MARK: - OrderState
final class OrderStateView: ComponentView<OrderStateModel> {
    private lazy var topLine: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var statusIcon: UILabel = {
        let label = UILabel()
        label.labelStyle(IconLabelStyle(type: .medium))
        return label
    }()
    
    private lazy var bottomLine: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var descriptionContainerStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [shortDescriptionLabel, titleLabel, descriptionLabel, actionButton])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base00
        stackView.alignment = .leading
        stackView.distribution = .fillProportionally
        stackView.setContentHuggingPriority(.required, for: .vertical)
        return stackView
    }()
    
    private lazy var shortDescriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
            .with(\.textAlignment, .left)
            .with(\.numberOfLines, 1)
            .with(\.textColor, .grayscale300())
        label.setContentCompressionResistancePriority(.required, for: .vertical)
        return label
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textAlignment, .left)
            .with(\.numberOfLines, 1)
        label.setContentCompressionResistancePriority(.required, for: .vertical)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textAlignment, .left)
            .with(\.textColor, .grayscale700())
            .with(\.numberOfLines, 2)
        label.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
        return label
    }()
    
    private lazy var actionButton: UIButton = {
        let button = UIButton(type: .custom)
        button.buttonStyle(PrimaryButtonStyle(size: .small))
        button.addTarget(self, action: #selector(didTapAction), for: .touchUpInside)
        button.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)
        button.titleLabel?.minimumScaleFactor = Layout.Button.titleMinimunScaleFactor
        button.titleLabel?.lineBreakMode = .byTruncatingTail
        button.titleLabel?.numberOfLines = 2
        return button
    }()

    override func resetViews(_ views: UIView...) {
        super.resetViews(descriptionLabel, titleLabel, shortDescriptionLabel)
    }
    
    override func buildViewHierarchy() {
        contentView.addSubviews(topLine, statusIcon, bottomLine, descriptionContainerStackView)
    }
    
    override func setupConstraints() {
        topLine.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.centerX.equalTo(statusIcon)
            $0.width.equalTo(Layout.Line.width)
        }
        
        statusIcon.snp.makeConstraints {
            $0.top.equalTo(topLine.snp.bottom).offset(Spacing.base00)
            $0.leading.equalToSuperview().inset(Spacing.base01)
            $0.centerY.equalTo(titleLabel.snp.centerY)
        }
        
        bottomLine.snp.makeConstraints {
            $0.top.equalTo(statusIcon.snp.bottom).offset(Spacing.base00)
            $0.bottom.equalToSuperview()
            $0.centerX.equalTo(statusIcon)
            $0.width.equalTo(Layout.Line.width)
            $0.height.greaterThanOrEqualTo(Layout.Line.bottonHeight)
        }
        
        descriptionContainerStackView.snp.makeConstraints {
            $0.top.equalToSuperview().inset(Spacing.base01)
            $0.bottom.lessThanOrEqualToSuperview().inset(Spacing.base01)
            $0.leading.equalTo(statusIcon.snp.trailing).offset(Spacing.base01)
            $0.trailing.equalToSuperview().inset(Spacing.base01)
        }
        
        actionButton.snp.makeConstraints {
            $0.width.greaterThanOrEqualTo(Layout.Button.minimumWidth)
            $0.trailing.lessThanOrEqualToSuperview()
        }
        
        actionButton.titleLabel?.snp.remakeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Layout.Button.titleInsets)
            $0.top.bottom.equalToSuperview()
        }
    }
    
    override func configureViews() {
        guard let model = model else { return }
        backgroundColor = Colors.backgroundPrimary.color
        if let topLineColor = model.topLineColor {
            topLine.backgroundColor = Colors.hexColor(topLineColor)?.color ?? .clear
        }
        if let bottomLineColor = model.bottomLineColor {
            bottomLine.backgroundColor = Colors.hexColor(bottomLineColor)?.color ?? .clear
        }
        
        guard let status = StatusIcon(rawValue: model.statusIcon) else { return }
        statusIcon.text = status.icon
        statusIcon.textColor = status.color
        
        shortDescriptionLabel.text = model.shortDescription
        titleLabel.text = model.title
        titleLabel.textColor = Colors.hexColor(model.titleColor)?.color
        descriptionLabel.text = model.description
        if let bottomLineColor = model.descriptionColor {
            descriptionLabel.textColor = Colors.hexColor(bottomLineColor)?.color
        }
        guard let button = model.button else {
            actionButton.isHidden = true
            return
        }
        actionButton.setTitle(button.label, for: .normal)
        actionButton.setTitleColor(Colors.hexColor(button.style.fontColor)?.color, for: .normal)
        actionButton.setBackgroundColor(Colors.hexColor(button.style.backgroundColor)?.color ?? .white, for: .normal)
    }
}
