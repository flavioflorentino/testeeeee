import Foundation

struct OrderStateModel: Component {
    let type: ComponentType
    let action: ActionBuilder?
    
    let shortDescription: String?
    let title: String?
    let description: String?
    let button: Button?
    let statusIcon: String
    let topLineColor: String?
    let bottomLineColor: String?
    let titleColor: String
    let descriptionColor: String?
    
    var uiComponent: UIComponent.Type? { OrderStateUI.self }
}

extension OrderStateModel {
    struct Button: Decodable {
        let label: String
        let style: Style
    }
    
    struct Style: Decodable {
        let backgroundColor: String
        let fontColor: String
    }
}
