import Foundation

struct ItemPriceModel: Component {
    let type: ComponentType
    let action: ActionBuilder?
    
    let price: String
    let oldPrice: String?
    let installments: String?
    let spotlight: String?
    
    var uiComponent: UIComponent.Type? { ItemPriceUI.self }
}
