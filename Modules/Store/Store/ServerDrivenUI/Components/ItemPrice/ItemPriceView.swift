import Foundation
import UI
import UIKit

// MARK: - ItemPriceView
final class ItemPriceView: ComponentView<ItemPriceModel> {
    private lazy var descriptionContainerStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [oldPriceLabel, priceLabel, instalmentsLabel, spotlightLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base00
        stackView.alignment = .leading
        stackView.distribution = .fillProportionally
        stackView.setContentHuggingPriority(.required, for: .vertical)
        return stackView
    }()
    
    private lazy var oldPriceLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale200())
            .with(\.textAlignment, .left)
            .with(\.numberOfLines, 1)
        label.setContentHuggingPriority(.required, for: .vertical)
        label.setContentCompressionResistancePriority(.required, for: .vertical)
        return label
    }()
    
    private lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
            .with(\.textAlignment, .left)
            .with(\.textColor, .black())
            .with(\.numberOfLines, 1)
        label.setContentHuggingPriority(.required, for: .vertical)
        label.setContentCompressionResistancePriority(.required, for: .vertical)
        return label
    }()
    
    private lazy var instalmentsLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .branding600())
            .with(\.textAlignment, .left)
            .with(\.numberOfLines, 1)
        label.setContentHuggingPriority(.required, for: .vertical)
        label.setContentCompressionResistancePriority(.required, for: .vertical)
        return label
    }()
    
    private lazy var spotlightLabel: RoundedCornerLabel = {
        let roundedCorner = RoundedCornerLabel()
        roundedCorner.viewStyle(BackgroundViewStyle(color: .neutral400()))
            .with(\.layer.masksToBounds, true)
            .with(\.cornerRadius, .light)
        roundedCorner.setContentHuggingPriority(.required, for: .horizontal)
        roundedCorner.setContentHuggingPriority(.required, for: .vertical)
        roundedCorner.setContentCompressionResistancePriority(.required, for: .vertical)
        return roundedCorner
    }()

    override func resetViews(_ views: UIView...) {
        super.resetViews(spotlightLabel, instalmentsLabel, priceLabel, oldPriceLabel)
    }
    
    override func buildViewHierarchy() {
        contentView.addSubview(descriptionContainerStackView)
    }
    
    override func setupConstraints() {
        descriptionContainerStackView.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    override func configureViews() {
        guard let model = model else { return }
        priceLabel.text = model.price
        oldPriceLabel.isHidden = model.oldPrice?.isEmpty ?? true
        oldPriceLabel.attributedText = NSAttributedString(
            string: model.oldPrice ?? "",
            attributes: [
                .strikethroughStyle: 1,
                .strikethroughColor: Colors.grayscale200.color
            ])
        instalmentsLabel.isHidden = model.installments?.isEmpty ?? true
        instalmentsLabel.text = model.installments
        spotlightLabel.isHidden = model.spotlight?.isEmpty ?? true
        spotlightLabel.text = model.spotlight
    }
}
