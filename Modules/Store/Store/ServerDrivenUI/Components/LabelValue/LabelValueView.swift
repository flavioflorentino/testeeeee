import UIKit
import UI

// MARK: - LayoutConstants
private enum Layout {
    enum LabelValue {
        static let height: CGFloat = 25
        static let horizontalPadding: CGFloat = Spacing.base02
        static let valueLabelWidthMinDivisibleProportion: CGFloat = 3
    }
}

final class LabelValueView: ComponentView<LabelValueModel> {
    // MARK: - Subviews
    lazy var stack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fill
        stack.alignment = .center
        return stack
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle(type: .default))
            .with(\.textColor, .grayscale750())
            .with(\.textAlignment, .left)
            .with(\.numberOfLines, 1)
        label.setContentCompressionResistancePriority(.required, for: .horizontal)
        return label
    }()
    
    lazy var valueLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle(type: .default))
            .with(\.textColor, .black())
            .with(\.textAlignment, .right)
            .with(\.numberOfLines, 1)
        label.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        return label
    }()

    override func resetViews(_ views: UIView...) {
        super.resetViews(valueLabel, titleLabel)
    }
    
    // MARK: - ComponentView
    override func buildViewHierarchy() {
        stack.addArrangedSubview(titleLabel)
        stack.addArrangedSubview(valueLabel)
        contentView.addSubview(stack)
    }
    
    override func setupConstraints() {
        stack.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()

            make.leading.trailing
                .equalToSuperview()
                .inset(Layout.LabelValue.horizontalPadding)
            
            make.height
                .equalTo(Layout.LabelValue.height)
        }
        
        valueLabel.snp.makeConstraints { make in
            make.width
                .greaterThanOrEqualToSuperview()
                .dividedBy(Layout.LabelValue.valueLabelWidthMinDivisibleProportion)
        }
    }
    
    override func configureViews() {
        guard let model = model else { return }
        titleLabel.text = model.label
        valueLabel.text = model.value
    }
}
