import Foundation

struct LabelValueModel: Component {
    let type: ComponentType
    let action: ActionBuilder?
    
    let label: String
    let value: String
    
    var uiComponent: UIComponent.Type? { LabelValueUI.self }
}
