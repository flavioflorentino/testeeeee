import Foundation

typealias PrimaryItemUI = GenericUIComponent<PrimaryItemModel, PrimaryItemView>
