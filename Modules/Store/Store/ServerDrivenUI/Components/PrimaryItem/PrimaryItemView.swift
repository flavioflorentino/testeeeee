import Foundation
import UI
import UIKit

final class PrimaryItemView: ComponentView<PrimaryItemModel> {
    private lazy var avatar: UIImageView = {
        let imageView = UIImageView()
        imageView.imageStyle(AvatarImageStyle(size: .medium))
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base00
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textAlignment, .left)
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textAlignment, .left)
            .with(\.textColor, .grayscale400())
            .with(\.numberOfLines, 2)
        return label
    }()
    
    override func resetViews(_ views: UIView...) {
        super.resetViews(subtitleLabel, titleLabel, avatar)
    }
    
    override func buildViewHierarchy() {
        contentView.addSubviews(avatar, stackView)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(subtitleLabel)
    }
    
    override func setupConstraints() {
        avatar.snp.makeConstraints {
            $0.centerY.equalTo(contentView)
            $0.top.equalToSuperview().inset(Spacing.base02)
            $0.size.equalTo(avatar.size)
            $0.leading.equalTo(contentView).offset(Spacing.base03)
        }
        
        stackView.snp.makeConstraints {
            $0.centerY.equalTo(contentView)
            $0.height.lessThanOrEqualTo(Sizing.base09)
            $0.leading.equalTo(avatar.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalTo(contentView).offset(-Spacing.base01)
        }
    }
    
    override func configureViews() {
        guard let model = model else { return }
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapAction)))
        titleLabel.text = model.title
        subtitleLabel.text = model.subtitle
        avatar.setImage(url: URL(string: model.image))
    }
}
