import Foundation

struct PrimaryItemModel: Component {
    let type: ComponentType
    let action: ActionBuilder?
    
    let image: String
    let title: String
    let subtitle: String?

    var uiComponent: UIComponent.Type? { PrimaryItemUI.self }
}
