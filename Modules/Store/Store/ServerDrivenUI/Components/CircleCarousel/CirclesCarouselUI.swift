import Foundation

typealias CirclesCarouselUI = GenericUIComponent<CirclesCarouselModel, CirclesCarouselView>
