import Foundation

struct CirclesCarouselModel: Component {
    let type: ComponentType
    let action: ActionBuilder?
    
    let items: [PrimaryCircleItemModel]
    
    var uiComponent: UIComponent.Type? { CirclesCarouselUI.self }
}

struct PrimaryCircleItemModel: Component {
    let type: ComponentType
    var action: ActionBuilder?
    
    let image: String
    let title: String
    
    var uiComponent: UIComponent.Type? { nil }
}
