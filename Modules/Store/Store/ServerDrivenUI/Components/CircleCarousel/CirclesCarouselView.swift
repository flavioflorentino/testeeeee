import Foundation
import UI
import UIKit

// MARK: - LayoutConstants
private enum Layout {
    enum Cell {
        static let width: CGFloat = 96
        static let height: CGFloat = 98
        static let size = CGSize(width: width, height: height)
    }
    
    enum CollectionView {
        static let sectionInset: UIEdgeInsets = .zero
    }
}

// MARK: - CircleCarouselView
final class CirclesCarouselView: ComponentView<CirclesCarouselModel> {
    private var items: [Component] = []
    
    private lazy var collectionViewLayout: UICollectionViewLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = Spacing.base01
        layout.itemSize = Layout.Cell.size
        layout.sectionInset = Layout.CollectionView.sectionInset
        return layout
    }()
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .clear
        collectionView.isScrollEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.register(CircleTextViewCell.self, forCellWithReuseIdentifier: CircleTextViewCell.identifier)
        return collectionView
    }()
    
    override func buildViewHierarchy() {
        contentView.addSubview(collectionView)
    }
    
    override func setupConstraints() {
        collectionView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.height.equalTo(Layout.Cell.height)
        }
    }
    
    override func configureViews() {
        guard let model = model else {
            return
        }
        items = model.items
    }
}

extension CirclesCarouselView: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = items[indexPath.row]
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CircleTextViewCell.identifier,
                                                            for: indexPath) as? CircleTextViewCell else {
            return UICollectionViewCell()
        }
        cell.configure(item)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        actionDelegate?.performAction(from: items[indexPath.row])
    }
}

// MARK: - BannerCollectionViewCell
private final class CircleTextViewCell: UICollectionViewCell, ViewConfiguration {
    private lazy var circleImage: UIImageView = {
        let imageView = UIImageView()
        imageView.imageStyle(AvatarImageStyle(size: .medium))
            .with(\.border, .light(color: .grayscale100()))
            .with(\.contentMode, .scaleAspectFit)
        return imageView
    }()
    
    private lazy var txtLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textAlignment, .center)
            .with(\.numberOfLines, 2)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    func buildViewHierarchy() {
        contentView.addSubview(circleImage)
        contentView.addSubview(txtLabel)
    }
    
    func setupConstraints() {
        circleImage.snp.makeConstraints {
            $0.size.equalTo(circleImage.size)
            $0.top.equalToSuperview().inset(Spacing.base00)
            $0.centerX.equalToSuperview()
        }
        
        txtLabel.snp.makeConstraints {
            $0.top.equalTo(circleImage.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base01)
            $0.bottom.lessThanOrEqualToSuperview().inset(Spacing.base00)
        }
    }
    
    func configure(_ item: Component) {
        backgroundColor = Colors.backgroundPrimary.color
        
        guard let model = item as? PrimaryCircleItemModel else {
            return
        }
        
        circleImage.setImage(url: URL(string: model.image))
        txtLabel.text = model.title
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        circleImage.cancelRequest()
        circleImage.image = nil
        txtLabel.text = nil
    }
}
