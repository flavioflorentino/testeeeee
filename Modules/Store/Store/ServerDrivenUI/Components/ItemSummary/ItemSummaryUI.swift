import Foundation

typealias ItemSummaryUI = GenericUIComponent<ItemSummaryModel, ItemSummaryView>
