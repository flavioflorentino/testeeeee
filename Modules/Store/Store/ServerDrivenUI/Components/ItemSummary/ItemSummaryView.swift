import Foundation
import UI
import UIKit

final class ItemSummaryView: ComponentView<ItemSummaryModel> {
    private lazy var containerStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [avatar, descriptionContainerStackView])
        stackView.alignment = .center
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var descriptionContainerStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [titleLabel, summaryLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base00
        stackView.alignment = .leading
        stackView.distribution = .fillProportionally
        stackView.setContentHuggingPriority(.required, for: .vertical)
        return stackView
    }()
    
    private lazy var avatar: UIImageView = {
        let imageView = UIImageView()
        imageView.imageStyle(AvatarImageStyle(size: .large))
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textAlignment, .left)
            .with(\.numberOfLines, 2)
        label.setContentHuggingPriority(.required, for: .vertical)
        label.setContentCompressionResistancePriority(.required, for: .vertical)
        return label
    }()
    
    private lazy var summaryLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textAlignment, .left)
            .with(\.textColor, .grayscale750())
        label.setContentHuggingPriority(.required, for: .vertical)
        label.setContentCompressionResistancePriority(.required, for: .vertical)
        return label
    }()
    
    override func resetViews(_ views: UIView...) {
        super.resetViews(summaryLabel, titleLabel, avatar)
    }
    
    override func buildViewHierarchy() {
        contentView.addSubview(containerStackView)
    }
    
    override func setupConstraints() {
        containerStackView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
        avatar.snp.makeConstraints {
            $0.width.equalTo(avatar.size.width)
            $0.size.equalTo(avatar.size)
        }
    }
    
    override func configureViews() {
        guard let model = model else { return }
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapAction)))
        backgroundColor = Colors.backgroundPrimary.color
        titleLabel.text = model.title
        summaryLabel.text = model.summary
        avatar.setImage(url: URL(string: model.image))
    }
}
