import Foundation

struct ItemSummaryModel: Component {
    let type: ComponentType
    let action: ActionBuilder?
    
    let image: String
    let title: String
    let summary: String
    
    var uiComponent: UIComponent.Type? { ItemSummaryUI.self }
}
