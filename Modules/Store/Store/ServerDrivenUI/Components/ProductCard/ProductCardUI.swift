import Foundation

typealias ProductCardUI = GenericUIComponent<ProductCardModel, ProductCardView>
