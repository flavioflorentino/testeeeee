import UI
import UIKit
import AssetsKit
import Foundation

// MARK: - ProductCardLayout
private enum Layout {
    enum Disclosure {
        static let width: CGFloat = 15
        static let height: CGFloat = 10
    }
    
    enum Wrapper {
        static let height: CGFloat = 128
    }
}

// MARK: - ProductCard
final class ProductCardView: ComponentView<ProductCardModel> {
    private lazy var wrapper: UIView = {
        let view = UIView()
        view.viewStyle(CardViewStyle())
            .with(\.cornerRadius, .light)
            .with(\.shadow, .light(color: .black(.light), opacity: .ultraLight))
        return view
    }()
    
    private lazy var containerStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [avatar, descriptionContainerStackView, disclosureIndicator])
        stackView.alignment = .center
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var descriptionContainerStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [spotlightLabel, valueLabel, descriptionLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base00
        stackView.alignment = .leading
        stackView.distribution = .fillProportionally
        stackView.setContentHuggingPriority(.required, for: .vertical)
        return stackView
    }()
    
    private lazy var avatar: UIImageView = {
        let imageView = UIImageView()
        imageView.imageStyle(AvatarImageStyle(size: .medium))
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var spotlightLabel: RoundedCornerLabel = {
        let roundedCorner = RoundedCornerLabel()
        roundedCorner.viewStyle(BackgroundViewStyle(color: .neutral400()))
            .with(\.cornerRadius, .light)
            .with(\.layer.masksToBounds, true)
        roundedCorner.setContentHuggingPriority(.required, for: .horizontal)
        roundedCorner.setContentHuggingPriority(.required, for: .vertical)
        roundedCorner.setContentCompressionResistancePriority(.required, for: .vertical)
        return roundedCorner
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .small))
            .with(\.textAlignment, .left)
            .with(\.numberOfLines, 1)
        label.setContentHuggingPriority(.required, for: .vertical)
        label.setContentCompressionResistancePriority(.required, for: .vertical)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textAlignment, .left)
            .with(\.textColor, .grayscale400())
            .with(\.numberOfLines, 1)
        label.setContentHuggingPriority(.required, for: .vertical)
        label.setContentCompressionResistancePriority(.required, for: .vertical)
        return label
    }()
    
    private lazy var disclosureIndicator: UIImageView = {
        let imageview = UIImageView()
        imageview.contentMode = .scaleAspectFill
        imageview.image = Resources.Icons.icoGreenRightArrow.image
        return imageview
    }()
    
    override func resetViews(_ views: UIView...) {
        super.resetViews(descriptionLabel, valueLabel, spotlightLabel, avatar)
    }
    
    override func buildViewHierarchy() {
        wrapper.addSubview(containerStackView)
        contentView.addSubview(wrapper)
    }
    
    override func setupConstraints() {
        wrapper.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base00)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Layout.Wrapper.height)
        }
        containerStackView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        avatar.snp.makeConstraints {
            $0.width.equalTo(avatar.size.width)
            $0.height.equalTo(avatar.size.height)
        }
        disclosureIndicator.snp.makeConstraints {
            $0.width.equalTo(Layout.Disclosure.width)
            $0.height.equalTo(Layout.Disclosure.height)
        }
    }
    
    override func configureViews() {
        guard let model = model else { return }
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapAction)))
        backgroundColor = Colors.backgroundPrimary.color
        valueLabel.text = model.value
        spotlightLabel.isHidden = model.spotlight?.isEmpty ?? true
        spotlightLabel.text = model.spotlight
        descriptionLabel.text = model.description
        avatar.setImage(url: URL(string: model.avatar))
    }
}
