import Foundation

struct ProductCardModel: Component {
    var type: ComponentType { .productCard }
    let action: ActionBuilder?
    
    let value: String
    let avatar: String
    let spotlight: String?
    let description: String

    var uiComponent: UIComponent.Type? { ProductCardUI.self }
}
