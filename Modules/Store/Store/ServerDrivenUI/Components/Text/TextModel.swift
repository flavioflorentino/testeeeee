import Foundation

struct TextModel: Component {
    let type: ComponentType
    let action: ActionBuilder?
    
    let text: String
    let style: Style?

    var uiComponent: UIComponent.Type? { TextUI.self }
}

extension TextModel {
    struct Style: Decodable {
        let fontColor: String
    }
}
