import Foundation
import UI
import UIKit

final class TextView: ComponentView<TextModel> {
    private lazy var textView: UITextView = {
        let text = UITextView()
        text.isEditable = false
        text.isSelectable = true
        text.isScrollEnabled = false
        text.textAlignment = .left
        text.backgroundColor = .clear
        text.textColor = Colors.grayscale700.color
        text.backgroundColor = .clear
        text.textContainerInset = .zero
        text.textContainer.lineFragmentPadding = 0
        text.dataDetectorTypes = [.link]
        return text
    }()
    
    override func resetViews(_ views: UIView...) {
        super.resetViews(textView)
    }
    
    override func buildViewHierarchy() {
        contentView.addSubview(textView)
    }
    
    override func setupConstraints() {
        textView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    private func configureStyle() {
        guard let model = model else { return }
        
        textView.text = model.text
        if let style = model.style {
            textView.textColor = Colors.hexColor(style.fontColor)?.color
        }
        
        switch model.type {
        case .title:
            textView.font = Typography.title(.large).font()
        case .subtitle:
            textView.font = Typography.title(.small).font()
        case .mediumText:
            textView.font = Typography.bodyPrimary(.highlight).font()
        case .markdown:
            textView.font = Typography.bodySecondary().font()
            textView.setHTML(model.text)
        default:
            return
        }
    }
    
    override func configureViews() {
        configureStyle()
    }
}

// MARK: - Markdown Function
private extension UITextView {
    func setHTML(_ string: String?) {
        let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
        ]
        
        guard
            let font = self.font,
            let data = string?.data(using: .utf8, allowLossyConversion: true),
            let attributes = try? NSMutableAttributedString(data: data, options: options, documentAttributes: nil) else {
                text = string
                return
        }
        
        setTextFont(font, on: attributes)
        attributedText = NSAttributedString(attributedString: attributes)
    }
    
    private func setTextFont(_ font: UIFont, on attributes: NSMutableAttributedString) {
        let range = NSRange(location: 0, length: attributes.length)
        attributes.enumerateAttribute(.font, in: range, options: .longestEffectiveRangeNotRequired) { attrib, range, _ in
            guard let htmlFont = attrib as? UIFont else {
                return
            }
            let fontDescriptor = setTraits(
                htmlFont.fontDescriptor.symbolicTraits,
                to: htmlFont.fontDescriptor.withFamily(font.familyName)
            )
            attributes.addAttribute(.font, value: UIFont(descriptor: fontDescriptor, size: font.pointSize), range: range)
            attributes.addAttribute(.foregroundColor, value: Colors.black.color, range: range)
        }
    }
    
    private func setTraits(_ traits: UIFontDescriptor.SymbolicTraits, to fontDescriptor: UIFontDescriptor) -> UIFontDescriptor {
        var descriptor = fontDescriptor
        if (traits.rawValue & UIFontDescriptor.SymbolicTraits.traitBold.rawValue) != 0,
            let descriptorWithBoldTrait = descriptor.withSymbolicTraits(.traitBold) {
            descriptor = descriptorWithBoldTrait
        }
        
        if (traits.rawValue & UIFontDescriptor.SymbolicTraits.traitItalic.rawValue) != 0,
            let descriptorWithItalicTrait = descriptor.withSymbolicTraits(.traitItalic) {
            descriptor = descriptorWithItalicTrait
        }
        return descriptor
    }
}
