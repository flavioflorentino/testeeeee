import Foundation

typealias PrimaryHeaderUI = GenericHeaderUIComponent<HeaderModel, PrimaryHeaderView>
typealias SecondaryHeaderUI = GenericHeaderUIComponent<HeaderModel, SecondaryHeaderView>
