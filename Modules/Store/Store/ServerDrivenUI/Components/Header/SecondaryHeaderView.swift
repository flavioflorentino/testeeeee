import Foundation
import UI
import UIKit
import AssetsKit

// MARK: - HeaderLayout
private enum Layout {
    enum Banner {
        static let height: CGFloat = 110
    }
    enum Gradient {
        static let colours: [CGColor] = [
            UIColor.black.withAlphaComponent(0.5).cgColor,
            UIColor.black.withAlphaComponent(0.5).cgColor,
            UIColor.black.withAlphaComponent(0.5).cgColor
        ]
    }
}

// MARK: - Header
final class SecondaryHeaderView: PrimaryHeaderView {
    override func buildViewHierarchy() {
        gradient = GradientView(colours: Layout.Gradient.colours)
        super.buildViewHierarchy()
    }
    
    override func setupConstraints() {
        banner.snp.makeConstraints {
            $0.height.equalTo(Layout.Banner.height)
            $0.top.leading.trailing.equalToSuperview()
        }
        gradient.snp.makeConstraints {
            $0.top.leading.trailing.bottom.equalTo(banner)
        }
        avatar.snp.makeConstraints {
            $0.centerX.equalTo(banner)
            $0.centerY.equalTo(banner.snp.bottom)
            $0.bottom.equalToSuperview().inset(Spacing.base00)
        }
    }
}
