import Foundation

struct HeaderModel: Component {
    let type: ComponentType
    var action: ActionBuilder? { nil }
    
    let menu: Menu?
    let title: String
    let avatar: String?
    let banner: String?
    let rightButton: ButtonModel?
    
    var uiComponent: UIComponent.Type? {
        switch type {
        case .primaryHeader:
            return PrimaryHeaderUI.self
        case .secondaryHeader:
            return SecondaryHeaderUI.self
        default:
            return nil
        }
    }
}

// MARK: - MenuItems
extension HeaderModel {
    struct Menu: Decodable {
        let maxVisibleItems: Int
        let items: [MenuItem]
    }
    
    struct MenuItem: Decodable {
        let icon: String?
        let action: ActionBuilder?
        let description: String
    }
}
