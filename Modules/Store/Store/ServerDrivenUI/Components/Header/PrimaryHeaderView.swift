import Foundation
import UI
import UIKit
import AssetsKit

// MARK: - HeaderLayout
private enum Layout {
    enum Banner {
        static let height: CGFloat = 220
    }
    enum Button {
        static let width: CGFloat = 150
    }
    enum Gradient {
        static let colours: [CGColor] = [
            UIColor.black.withAlphaComponent(0.1).cgColor,
            UIColor.clear.cgColor,
            UIColor.clear.cgColor
        ]
    }
}

// MARK: - Header
class PrimaryHeaderView: ComponentView<HeaderModel> {
    private var isFirstLoad = true
    
    internal lazy var avatar = AvatarView(size: .large)
    internal lazy var gradient = GradientView(colours: Layout.Gradient.colours)
    internal lazy var banner: UIImageView = {
        let imageview = UIImageView()
        imageview.clipsToBounds = true
        imageview.contentMode = .scaleAspectFill
        return imageview
    }()

    override func resetViews(_ views: UIView...) {
        super.resetViews(avatar, banner)
    }

    override func buildViewHierarchy() {
        contentView.addSubviews(banner, gradient, avatar)
    }
    
    override func setupConstraints() {
        banner.snp.makeConstraints {
            $0.height.equalTo(Layout.Banner.height)
            $0.top.leading.trailing.equalToSuperview()
        }
        gradient.snp.makeConstraints {
            $0.top.leading.trailing.bottom.equalTo(banner)
        }
        avatar.snp.makeConstraints {
            $0.centerY.equalTo(banner.snp.bottom)
            $0.leading.equalTo(banner).offset(Spacing.base03)
            $0.bottom.equalToSuperview().inset(Spacing.base00)
        }
    }
    
    override func configureViews() {
        guard let model = model else { return }
        avatar.setImage(url: URL(string: model.avatar ?? ""))
        banner.setImage(url: URL(string: model.banner ?? ""))
        animateAvatar()
    }
    
    private func animateAvatar() {
        if isFirstLoad {
            isFirstLoad = false
            avatar.growAnimate()
        }
    }
}
