import Foundation

struct SectionHeaderModel: Component {
    let type: ComponentType
    let action: ActionBuilder?

    let text: String
    let actionTitle: String
    
    var uiComponent: UIComponent.Type? { SectionHeaderUI.self }
}
