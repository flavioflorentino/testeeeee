import Foundation
import UI
import UIKit

class SectionHeaderView: ComponentView<SectionHeaderModel> {
    private lazy var txtLabel: UILabel = {
        let textLabel = UILabel()
        textLabel.labelStyle(TitleLabelStyle(type: .small))
        .with(\.textAlignment, .left)
        .with(\.textColor, .grayscale700())
        return textLabel
    }() 
    
    private lazy var button: UIButton = {
        let button = UIButton(type: .custom)
        button.buttonStyle(LinkButtonStyle(size: .small, icon: (.angleRight, .right)))
            .with(\.textAlignment, .right)
            .with(\.typography, .caption(.highlight))
        button.addTarget(self, action: #selector(didTapAction), for: .touchUpInside)
        return button
    }()
    
    override func resetViews(_ views: UIView...) {
        super.resetViews(txtLabel)
    }

    override func buildViewHierarchy() {
        contentView.addSubviews(txtLabel, button)
    }
    
    override func setupConstraints() {
        txtLabel.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        button.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base01)
            $0.trailing.equalToSuperview().inset(Spacing.base01)
        }
    }
    
    override func configureViews() {
        guard let model = model else { return }
        
        txtLabel.text = model.text
        button.setTitle(model.actionTitle, for: .normal)
    }
}
