import Foundation

typealias SectionHeaderUI = GenericUIComponent<SectionHeaderModel, SectionHeaderView>
