import Foundation

struct CarouselModel: Component {
    let type: ComponentType
    let action: ActionBuilder?
    
    let items: [BannerModel]

    var uiComponent: UIComponent.Type? { CarouselUI.self }
}
