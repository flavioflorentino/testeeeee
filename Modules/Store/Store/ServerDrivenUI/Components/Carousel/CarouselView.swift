import Foundation
import UI
import UIKit

// MARK: - LayoutConstants
private enum Layout {
    enum Cell {
        static let width: CGFloat = UIScreen.main.bounds.width * 0.9
        static let height: CGFloat = width * 0.5
        static let size = CGSize(width: width, height: height)
    }
    
    enum CollectionView {
        static let sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}

// MARK: - CarouselView
final class CarouselView: ComponentView<CarouselModel> {
    private var items: [Component] = []
    
    private lazy var collectionViewLayout: UICollectionViewLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = Spacing.base01
        layout.itemSize = Layout.Cell.size
        layout.sectionInset = Layout.CollectionView.sectionInset
        return layout
    }()
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .clear
        collectionView.isScrollEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.register(BannerCollectionViewCell.self, forCellWithReuseIdentifier: BannerCollectionViewCell.identifier)
        return collectionView
    }()
    
    override func buildViewHierarchy() {
        contentView.addSubview(collectionView)
    }
    
    override func setupConstraints() {
        collectionView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.height.equalTo(Layout.Cell.height)
        }
    }
    
    override func configureViews() {
        guard let model = model else {
            return
        }
        items = model.items
    }
}

extension CarouselView: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = items[indexPath.row]
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BannerCollectionViewCell.identifier, for: indexPath) as? BannerCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.configure(item)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        actionDelegate?.performAction(from: items[indexPath.row])
    }
}

// MARK: - BannerCollectionViewCell
private final class BannerCollectionViewCell: UICollectionViewCell, ViewConfiguration {
    private lazy var containerView: UIView = {
        let view = UIView()
        view.viewStyle(CardViewStyle())
            .with(\.shadow, .light(color: .black(.dark), opacity: .strong))
        view.layer.masksToBounds = true
        return view
    }()
    
    private lazy var promoImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func buildViewHierarchy() {
        contentView.addSubview(containerView)
        containerView.addSubview(promoImage)
    }
    
    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base00)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base01)
        }
        promoImage.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    func configure(_ item: Component) {
        guard let banner = item as? BannerModel else {
            return
        }
        promoImage.setImage(url: URL(string: banner.image))
        backgroundColor = Colors.backgroundPrimary.color
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        promoImage.cancelRequest()
        promoImage.image = nil
    }
}
