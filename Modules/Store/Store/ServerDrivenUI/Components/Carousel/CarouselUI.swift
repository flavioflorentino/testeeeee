import Foundation

typealias CarouselUI = GenericUIComponent<CarouselModel, CarouselView>
