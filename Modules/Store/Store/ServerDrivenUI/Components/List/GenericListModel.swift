import Foundation

struct GenericListModel: Component {
    let type: ComponentType
    let action: ActionBuilder?
    
    private let items: [Builder]

    var components: [Component] {
        items.compactMap { $0.component }
    }
    
    var uiComponent: UIComponent.Type? { nil }
}
