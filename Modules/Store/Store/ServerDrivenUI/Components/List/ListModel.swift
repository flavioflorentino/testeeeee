import Foundation

struct ListModel: Component {
    let type: ComponentType
    let action: ActionBuilder?
    
    let items: [ProductCardModel]

    var uiComponent: UIComponent.Type? { nil }
}
