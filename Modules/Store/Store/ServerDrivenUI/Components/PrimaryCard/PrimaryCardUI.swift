import Foundation

typealias PrimaryCardUI = GenericUIComponent<PrimaryCardModel, PrimaryCardView>
