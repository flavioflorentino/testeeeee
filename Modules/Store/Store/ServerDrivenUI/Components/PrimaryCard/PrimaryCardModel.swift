import Foundation

struct PrimaryCardModel: Component {
    let type: ComponentType
    let action: ActionBuilder?
    
    let title: String?
    let subtitle: String?
    let image: String?
    
    var uiComponent: UIComponent.Type? { PrimaryCardUI.self }
}
