import Foundation
import UI
import UIKit
import AssetsKit

// MARK: - PrimaryCardViewLayout
private enum Layout {
    enum PrimaryCardView {
        static let cornerRadius: CGFloat = Spacing.base00
        static let height: CGFloat = Spacing.base08
        static let padding: CGFloat = Spacing.base02
        
        static let mainStackHorizontalPadding: CGFloat = Spacing.base03
        static let mainStackVerticalPadding: CGFloat = Spacing.base02
        static let mainStackSpacing: CGFloat = Spacing.base03
        
        static let iconSize: CGFloat = Spacing.base03
        static let acessorySize: CGFloat = Spacing.base03
    }
}

final class PrimaryCardView: ComponentView<PrimaryCardModel> {
    private lazy var mainStack: UIStackView = {
        let stack = UIStackView()
        stack.spacing = Layout.PrimaryCardView.mainStackSpacing
        stack.alignment = .center
        return stack
    }()
    
    private lazy var titleStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.setContentHuggingPriority(.defaultLow, for: .horizontal)
        stack.distribution = .fillEqually
        return stack
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.grayscale700.color)
            .with(\.numberOfLines, 1)
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .default))
            .with(\.textColor, Colors.grayscale700.color)
            .with(\.numberOfLines, 1)
        return label
    }()
    
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var acessoryImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(asset: Resources.Icons.icoGreenRightArrow)?
            .withRenderingMode(.alwaysTemplate)
        imageView.tintColor = Colors.grayscale700.color
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var wrapper: UIView = {
        let view = UIView()
        view.border = .light(color: .grayscale050(.default))
        view.applyRadiusCorners(radius: Layout.PrimaryCardView.cornerRadius)
        return view
    }()

    override func resetViews(_ views: UIView...) {
        super.resetViews(titleLabel, subtitleLabel, iconImageView)
    }
    
    override func buildViewHierarchy() {
        contentView.addSubview(wrapper)
        wrapper.addSubview(mainStack)
        
        mainStack.addArrangedSubview(iconImageView)
        mainStack.addArrangedSubview(titleStack)
        mainStack.addArrangedSubview(acessoryImageView)
        
        titleStack.addArrangedSubview(titleLabel)
        titleStack.addArrangedSubview(subtitleLabel)
    }
    
    override func setupConstraints() {
        let layout = Layout.PrimaryCardView.self
        
        wrapper.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(layout.padding)
        }
        
        mainStack.snp.makeConstraints { make in
            make.height.equalTo(layout.height)
            make.top.bottom.equalToSuperview()
            make.leading.trailing.equalToSuperview()
                .inset(layout.mainStackHorizontalPadding)
        }
        
        iconImageView.snp.makeConstraints { make in
            make.size.equalTo(layout.iconSize)
        }
        
        acessoryImageView.snp.makeConstraints { make in
            make.size.equalTo(layout.acessorySize)
        }
        
        titleStack.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
                .inset(layout.mainStackVerticalPadding)
        }
    }
    
    override func configureViews() {
        guard let model = model else { return }

        let url = URL(string: model.image ?? "")
        iconImageView.setImage(url: url, placeholder: nil) { [weak self] image in
            self?.iconImageView.image = image?.withRenderingMode(.alwaysTemplate)
            self?.iconImageView.tintColor = Colors.grayscale700.color
        }

        titleLabel.text = model.title
        subtitleLabel.text = model.subtitle
        
        iconImageView.isHidden = url == nil
        titleLabel.isHidden = model.title == nil
        subtitleLabel.isHidden = model.subtitle == nil
        
        let tapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(didTapAction)
        )
        
        addGestureRecognizer(tapGestureRecognizer)
    }
}
