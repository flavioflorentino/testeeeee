import Core
import Dispatch
import Foundation
import FeatureFlag

// MARK: - ServerDrivenUIService
protocol ServerDrivenUIServicing {
    func requesValidation(to url: String, requestData data: ServerData, _ callback: @escaping (Result<RemoteActionBuilder, ApiError>) -> Void)
}

final class ServerDrivenUIService: ServerDrivenUIServicing {
    typealias Dependencies = HasMainQueue & HasFeatureManager
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func requesValidation(to string: String, requestData data: ServerData, _ callback: @escaping (Result<RemoteActionBuilder, ApiError>) -> Void) {
        guard let url = URL(string: string) else {
            ErrorTracker.shared.track(error: .runtimeException(message: "Não foi possível criar uma url com a string: \(string), por isso a função não continuou."), origin: self)
            callback(.failure(.malformedRequest("RemoteAction URL string is not a valid URL.")))
            return
        }
        Api<RemoteActionBuilder>(endpoint: ServerDrivenUIEndpoint.remoteAction(url: url, serverData: data)).execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                callback(result.map(\.model))
            }
        }
    }
}
