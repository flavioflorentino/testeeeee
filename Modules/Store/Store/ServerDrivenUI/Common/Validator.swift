import Foundation

protocol Validator {
    var data: Any? { get set }
    var rules: [Rule] { get set }
    func isDataValid() -> Bool
}

protocol ValidatorOutput: AnyObject {
    func notifyDataUpdated(_ data: Any?)
    func notifyDataIsInvalid(message: String)
    func notifyDataIsValid()
}
