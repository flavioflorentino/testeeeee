import Foundation

protocol InputMask {
    func add(_ mask: Mask?, to text: String?, inputType: InputType)
}

protocol InputMaskOutput: AnyObject {
    func updateTextWithMaskedText(_ maskedText: String?)
}
