import Foundation

public struct ServerData: Codable, Equatable {
    private let inputData: [Model]
    private let externalData: [Model]
    public init(inputData: [Model] = [], externalData: [Model] = []) {
        self.inputData = inputData
        self.externalData = externalData
    }
    
    public struct Model: Codable, Equatable {
        public let key: String
        public let value: String
    }
}
