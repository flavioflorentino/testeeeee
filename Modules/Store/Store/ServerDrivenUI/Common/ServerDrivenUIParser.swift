import Foundation

protocol ServerDrivenUIParsing: AnyObject {
    func decode(_ data: Data)
    func getTitle() -> String
    func getMenu() -> HeaderModel.Menu?
    func getUIComponents() -> [UIComponent]
    func parseNavigationBarRightButton() -> ButtonModel?
}

final class ServerDrivenUIParser: ServerDrivenUIParsing {
    private let headers: [ComponentType] = [.primaryHeader, .secondaryHeader, .toolbar]
    private var response: Response?
    
    /// This function must be called before using any of the other function
    func decode(_ data: Data) {
        response = parseComponents(from: data)
    }
    
    func parseNavigationBarRightButton() -> ButtonModel? {
        if let rightButton = (response?.body.header?.component as? HeaderModel)?.rightButton {
            return rightButton
        }
        return nil
    }
    
    func getTitle() -> String {
        if let header = response?.body.header?.component as? HeaderModel {
            return header.title
        }
        return ""
    }
    
    func getMenu() -> HeaderModel.Menu? {
        if let header = response?.body.header?.component, headers.contains(header.type) {
            if let menu = (header as? HeaderModel)?.menu {
                return menu
            }
        }
        return nil
    }
    
    func getUIComponents() -> [UIComponent] {
        var items: [UIComponent] = []
        if let header = response?.body.header?.component, headers.contains(header.type) {
            items.append(contentsOf: getUIComponent(header))
        }
        response?.body.components.forEach {
            items.append(contentsOf: getUIComponent($0.component))
        }
        return items
    }
}

private extension ServerDrivenUIParser {
    func parseComponents(from data: Data) -> Response? {
        do {
            return try JSONDecoder(.convertFromSnakeCase).decode(Response.self, from: data)
        } catch {
            let message: String
            switch error {
            case let error as StoreError:
                message = error.localizedDescription
            case let error as DecodingError:
                message = "\(error)"
            default:
                message = error.localizedDescription
            }
            ErrorTracker.shared.track(error: .parseException(message: message), origin: self)
            print("\n======================\nServerDrivenUI Parse Response Error:\n\n\(message)\n======================\n")
        }
        return nil
    }
    
    func getUIComponent(_ component: Component) -> [UIComponent] {
        var items: [UIComponent] = []
        if let list = component as? ListModel {
            list.items.forEach {
                items.append(ProductCardUI(model: $0))
            }
            return items
        }
        
        if let list = component as? GenericListModel {
            list.components.forEach {
                items.append(contentsOf: getUIComponent($0))
            }
            return items
        }
        
        if let uiComponent = component.uiComponent?.init(model: component) {
            items.append(uiComponent)
        }
        return items
    }
}

struct Response: Decodable {
    let body: Body
}

struct Body: Decodable {
    let header: Builder?
    let components: [Builder]
}

struct Builder: Decodable {
    enum CodingKeys: CodingKey {
        case type
    }
    let component: Component
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let type = try container.decode(ComponentType.self, forKey: .type)

        do {
            component = try type.metatype.init(from: decoder)
        } catch {
            throw StoreError.parseError(type: type.metatype, error: error)
        }
    }
}
