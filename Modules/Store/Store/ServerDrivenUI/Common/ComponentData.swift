import Foundation

protocol DataDelegate: AnyObject {
    var dataContainer: DataContainer { get }
}

final class DataContainer {
    private var dictionary: [String: Validator] = [:]
    
    func add(_ validator: Validator, for key: String) {
        dictionary[key] = validator
    }
    
    func getData<T>(for key: String) -> T? {
        dictionary[key]?.data as? T
    }
    
    func getValidData(for keys: [String]) -> [String: Any]? {
        var items: [String: Any] = [:]
        for key in keys {
            guard let validator = dictionary[key], validator.isDataValid() else {
                continue
            }
            items[key] = validator.data
        }
        return (items.count == keys.count) ? items : nil
    }
}
