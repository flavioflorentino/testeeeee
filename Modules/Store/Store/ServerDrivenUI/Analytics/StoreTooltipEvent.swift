import Foundation
import AnalyticsModule

public enum StoreTooltipEvent: AnalyticsKeyProtocol {
    case didViewStoreTooltip(count: Int)
    case didViewSettingsTooltip(count: Int)
    
    private var name: String {
        switch self {
        case .didViewStoreTooltip:
            return "Tooltip viewed"
        case .didViewSettingsTooltip:
            return "Tooltip viewed"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .didViewStoreTooltip(count):
            return ["origin": "store", "when": "\(count)x"]
        case let .didViewSettingsTooltip(count):
            return ["origin": "settings", "when": "\(count)x"]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel]
    }
    
    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
