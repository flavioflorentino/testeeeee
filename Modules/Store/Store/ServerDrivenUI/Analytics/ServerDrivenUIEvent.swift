import Foundation
import AnalyticsModule

struct ServerDrivenUIEvent: Decodable {
    let name: String
    let properties: [String: Any]
    
    enum CodingKeys: CodingKey {
        case name
        case properties
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try container.decode(String.self, forKey: .name)
        properties = try container.decode([String: Any].self, forKey: .properties)
    }
    
    init(name: String, properties: [String: Any]) {
        self.name = name
        self.properties = properties
    }
}

extension ServerDrivenUIEvent: AnalyticsKeyProtocol {
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.mixPanel])
    }
}
