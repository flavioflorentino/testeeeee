import Core
import Foundation

enum ServerDrivenUIEndpoint {
    case remoteAction(url: URL, serverData: ServerData)
}

extension ServerDrivenUIEndpoint: ApiEndpointExposable {
    var path: String { "" }
    
    var baseURL: URL {
        switch self {
        case let .remoteAction(url, _):
            return url
        }
    }
    
    var method: HTTPMethod {
        .post
    }
    
    var isTokenNeeded: Bool {
        true
    }
    
    var body: Data? {
        switch self {
        case let .remoteAction(_, serverData):
            let encoder = JSONEncoder()
            encoder.keyEncodingStrategy = .convertToSnakeCase
            return try? encoder.encode(serverData)
        }
    }
}
