import Foundation
import AnalyticsModule

// MARK: - Inputs
protocol ServerDrivenUIInputs: AnyObject {
    func validateAction(from component: Component)
    func sendAnalytics(from component: Component)
}

// MARK: - Outputs
protocol ServerDrivenUIOutputs: AnyObject {
    func didReceiveResultFromComponentAction(_ result: ActionResult)
}

// MARK: - Interactor
final class ServerDrivenUIInteractor: ServerDrivenUIInputs, DataDelegate {
    var dataContainer = DataContainer()
    
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private weak var output: ServerDrivenUIOutputs?
    init(output: ServerDrivenUIOutputs, dependencies: Dependencies = DependencyContainer()) {
        self.output = output
        self.dependencies = dependencies
    }
    
    func sendAnalytics(from component: Component) {
        guard let event = component.action?.analytics else {
            return
        }
        dependencies.analytics.log(event)
    }
    
    func validateAction(from component: Component) {
        let keys = component.action?.inputKeys ?? []
        guard let data = dataContainer.getValidData(for: keys) else {
            ErrorTracker.shared.track(error: .runtimeException(message: "''dataContainer.getValidData'' retornou nulo ao tentar validar as keys: \(keys), por isso a função não continuou."), origin: self)
            return
        }
        createActionResult(from: component, additionalData: data)
    }
    
    private func createActionResult(from component: Component, additionalData data: [String: Any]) {
        guard let action = component.action?.wrapped else {
            ErrorTracker.shared.track(error: .runtimeException(message: "''component.action?.wrapped'' retornou nulo ao tentar buscar uma action. Component: \(component.type.rawValue)"), origin: self)
            return
        }
        let result = ActionResult(action: action, source: component.type, additionalData: data)
        output?.didReceiveResultFromComponentAction(result)
    }
}
