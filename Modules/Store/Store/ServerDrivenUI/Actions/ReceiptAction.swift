import Foundation

struct ReceiptAction: Action {
    let type: ActionType
    let analytics: ServerDrivenUIEvent?
    let property: Property
    let inputKeys: [String]
    
    func perform(delegate: ActionPerformDelegate?, result: ActionResult) {
        delegate?.openTransactionReceipt(with: property)
    }
}

extension ReceiptAction {
    struct Property: Decodable, Equatable {
        let receipt: Receipt
    }
    
    struct Receipt: Decodable, Equatable {
        let transactionId: String
        let transactionType: String
    }
}
