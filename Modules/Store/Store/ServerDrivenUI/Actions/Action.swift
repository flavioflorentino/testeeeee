import Foundation

// MARK: - ActionDelegate
protocol ActionDelegate: AnyObject {
    func performAction(from component: Component)
}

// MARK: - Action Protocol
protocol Action: Decodable {
    var type: ActionType { get }
    var analytics: ServerDrivenUIEvent? { get }
    var inputKeys: [String] { get }
    
    func perform(delegate: ActionPerformDelegate?, result: ActionResult)
}

// MARK: - ActionResult
struct ActionResult {
    let action: Action
    let source: ComponentType?
    let additionalData: [String: Any]
    
    init(action: Action, source: ComponentType? = nil, additionalData: [String: Any] = [:]) {
        self.action = action
        self.source = source
        self.additionalData = additionalData
    }
}

// MARK: - ActionType
enum ActionType: String, Decodable {
    case webview
    case payment
    case favorite
    case remoteAction = "remote_action"
    case openScreen = "open_screen"
    case openLegacyDigitalGood = "open_legacy_digital_good"
    case alert
    case share
    case address = "open_address"
    case receipt = "open_receipt"
    case deeplink = "open_deeplink"

    var metatype: Action.Type {
        switch self {
        case .webview:
            return LinkAction.self
        case .payment:
            return PaymentAction.self
        case .favorite:
            return FavoriteAction.self
        case .openScreen:
            return NavigationAction.self
        case .openLegacyDigitalGood:
            return NavigationLegacyAction.self
        case .remoteAction:
            return RemoteAction.self
        case .alert:
            return AlertAction.self
        case .share:
            return ShareAction.self
        case .address:
            return AddressAction.self
        case .receipt:
            return ReceiptAction.self
        case .deeplink:
            return DeeplinkAction.self
        }
    }
}

// MARK: - ActionBuilder
@dynamicMemberLookup
struct ActionBuilder: Decodable {
    let wrapped: Action

    init(from decoder: Decoder) throws {
        wrapped = try decoder.decode("type", as: ActionType.self).metatype.init(from: decoder)
    }

    subscript<T>(dynamicMember keyPath: KeyPath<Action, T>) -> T {
        wrapped[keyPath: keyPath]
    }
}

struct RemoteActionBuilder: Decodable {
    let action: Action

    init(from decoder: Decoder) throws {
        action = try decoder.decode("action", as: ActionBuilder.self).wrapped
    }
    
    init(action: Action) {
        self.action = action
    }
}
