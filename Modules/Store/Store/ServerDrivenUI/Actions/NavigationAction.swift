import Foundation

struct NavigationAction: Action {
    let type: ActionType
    let analytics: ServerDrivenUIEvent?
    let property: Property
    let inputKeys: [String]
    
    func perform(delegate: ActionPerformDelegate?, result: ActionResult) {
        let fullScreen = result.source == .sectionHeader || result.source == .primaryCard
        delegate?.openScreen(with: property, fullScreen: fullScreen)
    }
}

extension NavigationAction {
    struct Property: Decodable, Equatable {
        let navigation: Navigation
        let externalData: [ServerData.Model]
    }
    
    struct Navigation: Decodable, Equatable {
        let screenId: String
    }
}
