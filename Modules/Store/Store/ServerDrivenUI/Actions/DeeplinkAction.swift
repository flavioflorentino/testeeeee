import Foundation

struct DeeplinkAction: Action {
    let type: ActionType
    let analytics: ServerDrivenUIEvent?
    let property: Property
    let inputKeys: [String]
    
    func perform(delegate: ActionPerformDelegate?, result: ActionResult) {
        delegate?.open(urlString: property.deeplink.url)
    }
}

extension DeeplinkAction {
    struct Property: Decodable, Equatable {
        let deeplink: Deeplink
    }
    
    struct Deeplink: Decodable, Equatable {
        let url: String
    }
}
