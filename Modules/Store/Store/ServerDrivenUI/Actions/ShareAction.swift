import Foundation

struct ShareAction: Action {
    let type: ActionType
    let property: Property
    let inputKeys: [String]
    let analytics: ServerDrivenUIEvent?
    
    func perform(delegate: ActionPerformDelegate?, result: ActionResult) {
        delegate?.buildShareSettings(with: property)
    }
}

extension ShareAction {
    struct Property: Decodable, Equatable {
        let text: String
    }
}
