import Foundation
import UIKit

struct NavigationLegacyAction: Action {
    let type: ActionType
    let analytics: ServerDrivenUIEvent?
    let property: Property
    let inputKeys: [String]
    
    func perform(delegate: ActionPerformDelegate?, result: ActionResult) {
        delegate?.presentLegacyStore(from: property.legacyDigitalGood)
    }
}

extension NavigationLegacyAction {
    private enum NavigationLegacyCodingKeys: CodingKey {
        case legacyDigitalGood
    }
    struct Property: Decodable, Equatable {
        let legacyDigitalGood: [String: Any]
        
        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: NavigationLegacyCodingKeys.self)
            self.legacyDigitalGood = try container.decode([String: Any].self, forKey: .legacyDigitalGood)
        }
        
        init(legacyDigitalGood: [String: Any]) {
            self.legacyDigitalGood = legacyDigitalGood
        }
        
        static func == (lhs: Property, rhs: Property) -> Bool {
            lhs.legacyDigitalGood.isEqual(to: rhs.legacyDigitalGood)
        }
    }
}
