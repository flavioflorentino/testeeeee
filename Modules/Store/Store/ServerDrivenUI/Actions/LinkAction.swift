import Foundation

struct LinkAction: Action {
    let type: ActionType
    let analytics: ServerDrivenUIEvent?
    let property: Property
    let inputKeys: [String]
    
    func perform(delegate: ActionPerformDelegate?, result: ActionResult) {
        delegate?.presentWebview(with: property.webview.url)
    }
}

extension LinkAction {
    struct Property: Decodable, Equatable {
        let webview: Link
    }
    
    struct Link: Decodable, Equatable {
        let url: String
    }
}
