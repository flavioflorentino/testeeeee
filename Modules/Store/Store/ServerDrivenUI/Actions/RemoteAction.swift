import Core
import Dispatch
import Foundation

 struct RemoteAction: Action {
    let type: ActionType
    let property: Property
    let inputKeys: [String]
    let analytics: ServerDrivenUIEvent?
    
    func perform(delegate: ActionPerformDelegate?, result: ActionResult) {
        delegate?.present(remoteAction: self, result: result)
    }
}

extension RemoteAction {
    struct Property: Decodable, Equatable {
        let url: String
        let message: String
        let externalData: [ServerData.Model]
    }
}
