import Foundation

struct AlertAction: Action {
    let type: ActionType
    let analytics: ServerDrivenUIEvent?
    let property: Property
    let inputKeys: [String]
    
    func perform(delegate: ActionPerformDelegate?, result: ActionResult) {
        delegate?.presentAlert(with: property)
    }
}

extension AlertAction {
    struct Property: Decodable, Equatable {
        let title: String
        let message: String
        let positiveButton: String
    }
}
