import Foundation

struct FavoriteAction: Action {
    let type: ActionType
    let analytics: ServerDrivenUIEvent?
    let property: Property
    let inputKeys: [String]
    
    func perform(delegate: ActionPerformDelegate?, result: ActionResult) {
        // TODO: future implementation
    }
}

extension FavoriteAction {
    struct Property: Decodable, Equatable {
        let serviceId: String
    }
}
