import Foundation

struct PaymentAction: Action {
    let type: ActionType
    let analytics: ServerDrivenUIEvent?
    let property: Property
    let inputKeys: [String]
    
    func perform(delegate: ActionPerformDelegate?, result: ActionResult) {
        delegate?.presentPayment(property, with: result.additionalData)
    }
}

extension PaymentAction {
    struct Property: Decodable, Equatable {
        let payment: Payment
        let externalData: [ServerData.Model]
    }
    
    struct Payment: Decodable, Equatable {
        let storeId: String
        let description: String
        let disclaimer: String
        let value: String
        let logoUrl: String
        let infoUrl: String
    }
} 
