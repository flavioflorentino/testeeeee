import Foundation

struct AddressAction: Action {
    let type: ActionType
    let property: Property
    let inputKeys: [String]
    let analytics: ServerDrivenUIEvent?
    
    func perform(delegate: ActionPerformDelegate?, result: ActionResult) {
        delegate?.presentAddress(property)
    }
}

extension AddressAction {
    struct Property: Decodable, Equatable {
        let navigation: Navigation
        let externalData: [ServerData.Model]
        
        // Essa informação não virá do back por enquanto, por isso está fixo, mas a idéia é que no futuro venha do back de forma dinâmica
        let title: String = Strings.Store.Marketplace.DeliveryAddress.title
        let text: String = Strings.Store.Marketplace.DeliveryAddress.text
        let image: UIImage = Assets.addressPin.image
        let shouldDisplayHeader = true
        
        init(from decoder: Decoder) throws {
            navigation = try decoder.decode("navigation")
            externalData = (try? decoder.decode("externalData")) ?? []
        }
        
        init(navigation: AddressAction.Navigation,
             externalData: [ServerData.Model]) {
            self.navigation = navigation
            self.externalData = externalData
        }
    }
    
    struct Navigation: Decodable, Equatable {
        let screenId: String
    }
}
