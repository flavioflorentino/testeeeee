import Core
import Foundation

enum StoreServiceEndpoint {
    case favorite(id: String)
    case unfavorite(id: String)
    case screen(id: String, serverData: ServerData?)
    case receipt(id: String)
}

extension StoreServiceEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .favorite:
            return "favorites"
        case let .unfavorite(id):
            return "favorites/digital_good/\(id)"
        case let .screen(id, _):
            return "template-manager/v2/assembler/\(id)"
        case let .receipt(id):
            return "store-payments/v1/transactions/\(id)/receipt"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .receipt:
            return .get
        case .favorite, .screen:
            return .post
        case .unfavorite:
            return .delete
        }
    }
    
    var isTokenNeeded: Bool {
        true
    }
    
    var body: Data? {
        switch self {
        case let .favorite(id):
            return ["type": "digital_good", "id": id].toData()
        case let .screen(_, serverData):
            return encodeServerData(serverData)
        default:
            return nil
        }
    }
    
    private func encodeServerData(_ data: ServerData?) -> Data? {
        guard let serverData = data else { return nil }
        let enconder = JSONEncoder()
        enconder.keyEncodingStrategy = .convertToSnakeCase
        return try? enconder.encode(serverData)
    }
}
