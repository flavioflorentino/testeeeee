import Foundation

final class StoreResources {
    private init() { }
    
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: StoreResources.self).url(forResource: "StoreResources", withExtension: "bundle") else {
            return Bundle(for: StoreResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: StoreResources.self)
    }()
}
