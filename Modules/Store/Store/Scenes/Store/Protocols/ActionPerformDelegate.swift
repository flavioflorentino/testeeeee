import Foundation

// MARK: - ActionPerformDelegate
protocol ActionPerformDelegate: AnyObject {
    func presentLegacyStore(from data: [String: Any])
    func presentPayment(_ property: PaymentAction.Property, with inputData: [String: Any])
    func presentAddress(_ property: AddressAction.Property)
    func presentAlert(with property: AlertAction.Property)
    func presentWebview(with urlString: String)
    func present(remoteAction: RemoteAction, result: ActionResult)
    func openScreen(with properties: NavigationAction.Property, fullScreen: Bool)
    func openTransactionReceipt(with property: ReceiptAction.Property)
    func buildShareSettings(with property: ShareAction.Property)
    func open(urlString: String)
}
