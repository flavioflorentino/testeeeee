import SafariServices
import UIKit

enum StoreAction {
    case close
    case payment(item: StorePayment)
    case webview(url: URL)
    case openScreen(properties: NavigationAction.Property, fullScreen: Bool)
    case legacyStore(data: [String: Any])
    case share(link: UIActivityViewController)
    case address(content: StoreAddressScreenContent, navigation: NavigationAction.Property)
    case receipt(transactionId: String, transactionType: String)
    case open(url: URL)
}

protocol StoreCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: StoreAction)
}

final class StoreCoordinator {
    weak var viewController: UIViewController?
    private let legacyStoreHandler: LegacyStoreHandler
    private let urlOpener: URLOpening
    private let errorTracking: ErrorTracking
    private let webviewControllerFactory: WebViewControllerFactoring
    
    init(
        legacyStoreHandler: LegacyStoreHandler,
        urlOpener: URLOpening = UIApplication.shared,
        errorTracking: ErrorTracking = ErrorTracker.shared,
        webviewControllerFactory: WebViewControllerFactoring = WebViewControllerFactory()
    ) {
        self.legacyStoreHandler = legacyStoreHandler
        self.urlOpener = urlOpener
        self.errorTracking = errorTracking
        self.webviewControllerFactory = webviewControllerFactory
    }
}

// MARK: - StoreCoordinating
extension StoreCoordinator: StoreCoordinating {
    func perform(action: StoreAction) {
        switch action {
        case .close:
            closeViewController()
        case let .payment(item):
            displayPayment(for: item)
        case let .webview(url):
            openWebview(url)
        case let .address(content, navigation):
            displayAddress(for: content, navigation: navigation)
        case let .openScreen(properties, fullScreen):
            navigateToScreen(with: properties, fullScreen: fullScreen)
        case let .legacyStore(data):
            openLegacyStore(with: data)
        case let .share(link):
            showShareActionSheet(link)
        case let .receipt(transactionId, transactionType):
            openTransactionReceipt(with: transactionId, and: transactionType)
        case let .open(url):
            openUrl(url)
        }
    }
    
    private func openUrl(_ url: URL) {
        urlOpener.open(url)
    }
    
    private func displayPayment(for item: StorePayment) {
        guard let viewController = viewController else {
            return
        }
        legacyStoreHandler.routeToPayment(item, from: viewController)
    }
    
    private func displayAddress(
        for content: StoreAddressScreenContent,
        navigation: NavigationAction.Property
    ) {
        let addressViewController = legacyStoreHandler.addressViewController(with: content) {
            self.onAddressSelected(result: $0, navigation: navigation)
        }
        
        viewController?
            .navigationController?
            .pushViewController(
                addressViewController,
                animated: true
            )
    }   
    
    private func onAddressSelected(
        result: StoreAddressResult,
        navigation: NavigationAction.Property
    ) {
        let navigationProperty = NavigationAction.Property(
            navigation: navigation.navigation,
            externalData: result.serverDataModel + navigation.externalData
        )
        
        navigateToScreen(
            with: navigationProperty,
            fullScreen: false
        )
    }
    
    private func openWebview(_ url: URL) {
        let safariViewController = webviewControllerFactory.make(with: url)
        viewController?.present(safariViewController, animated: true)
    }
    
    private func navigateToScreen(with properties: NavigationAction.Property, fullScreen: Bool) {
        guard let navigationController = viewController?.navigationController else {
            errorTracking.track(error: .runtimeException(message: "A variável ''navigationController'' está nula e a função não prosseguiu."), origin: self, function: #function)
            return
        }
        let store = Store.Item(properties.navigation.screenId, serverData: ServerData(externalData: properties.externalData))
        let storeViewController = StoreFactory.make(store, legacyStoreHandler: legacyStoreHandler)
        
        storeViewController.hidesBottomBarWhenPushed = true
        if !fullScreen && viewController is StoreHomeViewController {
            viewController?.present(UINavigationController(rootViewController: storeViewController), animated: true, completion: nil)
        } else {
            navigationController.setNavigationBarHidden(false, animated: true)
            navigationController.pushViewController(storeViewController, animated: true)
        }
    }
    
    private func openLegacyStore(with data: [String: Any]) {
        let digitalGoodsViewController = legacyStoreHandler.viewControllerForDigitalGood(with: data)
        
        guard let legacyViewController = digitalGoodsViewController else {
            ErrorTracker.shared.track(error: .runtimeException(message: "As variáveis ''legacyStoreHandler'' e/ou  ''legacyViewController'' estão nulas e a funcão não prosseguiu."), origin: self)
            return
        }
        viewController?.present(legacyViewController, animated: true)
    }
    
    private func closeViewController() {
        viewController?.dismiss(animated: true)
    }

    private func showShareActionSheet(_ link: UIActivityViewController) {
        viewController?.present(link, animated: true)
    }
    
    private func openTransactionReceipt(with transactionId: String, and transactionType: String) {
        guard let viewController = viewController else { return }
        legacyStoreHandler.openReceipt(above: viewController, with: transactionId, type: transactionType)
    }
}
