import Core
import Foundation
import AnalyticsModule

protocol StoreInteractorInputs: AnyObject {
    func requestComponents()
    func sendAnalytics()
    func performAction(_ result: ActionResult)
    func performRemoteRequest(from action: RemoteAction, and result: ActionResult)
}

final class StoreInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    private let store: Store.Item
    private let parser: ServerDrivenUIParsing
    private let service: StoreServicing
    private let presenter: StorePresenting
    private let remoteActionService: ServerDrivenUIServicing
        
    private var isLoading = false {
        didSet {
            presenter.presentLoading(isLoading: isLoading, withStyle: .transparent)
        }
    }
    
    init(store: Store.Item,
         presenter: StorePresenting,
         service: StoreServicing,
         remoteActionService: ServerDrivenUIServicing = ServerDrivenUIService(),
         parser: ServerDrivenUIParsing = ServerDrivenUIParser(),
         dependencies: Dependencies = DependencyContainer()) {
        self.store = store
        self.parser = parser
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.remoteActionService = remoteActionService
    }
}

// MARK: - StoreInteractorInputs
extension StoreInteractor: StoreInteractorInputs {
    func sendAnalytics() {
        guard let event = store.event else {
            return
        }
        dependencies.analytics.log(event)
    }
    
    func requestComponents() {
        presenter.presentLoading(isLoading: true, withStyle: .default)
        service.requestComponents(for: store) { [weak self] result in
            self?.handleComponentRequest(result)
            self?.presenter.presentLoading(isLoading: false, withStyle: .default)
        }
    }
    
    func performAction(_ result: ActionResult) {
        presenter.performAction(result)
    }
    
    func performRemoteRequest(from action: RemoteAction, and result: ActionResult) {
        if !isLoading {
            isLoading = true
        }
        presenter.presentUpdatedLoadingText(action.property.message)
        
        let data = createRequestDataModel(action: action, with: result.additionalData)
        
        remoteActionService.requesValidation(to: action.property.url, requestData: data) { [weak self] response in
            var shouldStopLoading = true
            let responseFiltered = response.map(\.action)
            
            switch responseFiltered {
            case .success(let action):
                shouldStopLoading = action.type != .remoteAction
                self?.performAction(ActionResult(action: action))
            case .failure(let error):
                self?.handleActionError(error.requestError?.message)
            }
            
            if shouldStopLoading {
                self?.isLoading = false
            }
        }
    }
}

// MARK: - Private Functions
private extension StoreInteractor {
    func perform<T: Action>(_ actionType: T.Type, for result: ActionResult, _ action: (T) -> Void) {
        if let item = (result.action as? T) {
            action(item)
        } else {
            ErrorTracker.shared.track(error: .runtimeException(message: "Não foi possível fazer o casting do objeto ''Action'': \(result.action) para o tipo ''\(actionType)''"), origin: self)
            handleActionError()
        }
    }
    
    func handleComponentRequest(_ result: Result<Data, ApiError>) {
        switch result {
        case .success(let response):
            parseDataToComponents(data: response)
        case .failure(.connectionFailure):
            handleGeneralError(Strings.Store.noInternet)
        case .failure(let error):
            handleGeneralError(error.requestError?.message)
        }
    }
    
    func parseDataToComponents(data: Data) {
        parser.decode(data)
        
        let menu = parser.getMenu()
        let title = parser.getTitle()
        let components = parser.getUIComponents()
        
        if let rightButton = parser.parseNavigationBarRightButton() {
            if let action = rightButton.action?.wrapped {
                let actionResult = ActionResult(action: action, source: rightButton.type)
                presenter.presentRightButton(with: rightButton.label, and: actionResult)
            }
        }
        
        presenter.presentToolbar(with: title, and: menu)
        if components.isNotEmpty {
            presenter.presentComponents(components)
        } else {
            handleGeneralError()
        }
    }
    
    func handleGeneralError(_ message: String? = nil) {
        presenter.presentError(message)
    }

    func handleActionError(_ message: String? = nil) {
        let message = message ?? Strings.Store.generalActionError
        let property = AlertAction.Property(title: Strings.Store.opsTitle, message: message, positiveButton: Strings.Store.iUnderstandTitle)
        presenter.presentAlert(with: property)
    }
    
    func createRequestDataModel(action: RemoteAction, with data: [String: Any]) -> ServerData {
        let inputData = data.map {
            ServerData.Model(key: $0.key, value: "\($0.value)")
        }
        return ServerData(inputData: inputData, externalData: action.property.externalData)
    }
}
