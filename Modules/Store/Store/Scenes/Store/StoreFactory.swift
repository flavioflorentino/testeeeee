import Foundation

public enum StoreFactory {
    public static func make(_ store: Store.Item, legacyStoreHandler: LegacyStoreHandler) -> UIViewController {
        let service: StoreServicing = StoreService()
        let coordinator: StoreCoordinating = StoreCoordinator(legacyStoreHandler: legacyStoreHandler)
        let presenter: StorePresenting = StorePresenter(coordinator: coordinator)
        let interactor = StoreInteractor(store: store, presenter: presenter, service: service)
        let viewController = StoreViewController(interactor: interactor)
        presenter.viewController = viewController
        coordinator.viewController = viewController
        return viewController
    }
}
