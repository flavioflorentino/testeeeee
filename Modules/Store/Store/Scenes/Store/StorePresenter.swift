import Foundation
import UI
import AssetsKit

protocol StorePresenting: AnyObject {
    var viewController: StoreDisplay? { get set }
    
    func presentToolbar(with title: String, and menu: HeaderModel.Menu?)
    func presentRightButton(with title: String, and actionResult: ActionResult)
    func presentComponents(_ components: [UIComponent])
    func presentLoading(isLoading: Bool, withStyle style: LoadingViewStyle)
    func presentUpdatedLoadingText(_ text: String)
    func presentAlert(with property: AlertAction.Property)
    func presentError(_ message: String?)
    func performAction(_ result: ActionResult)
}

final class StorePresenter {
    weak var viewController: StoreDisplay?
    private let coordinator: StoreCoordinating
    private let urlChecker: URLChecking
    
    init(
        coordinator: StoreCoordinating,
        urlChecker: URLChecking = UIApplication.shared) {
        self.coordinator = coordinator
        self.urlChecker = urlChecker
    }
}

// MARK: - StorePresenting
extension StorePresenter: StorePresenting {
    func presentToolbar(with title: String, and menu: HeaderModel.Menu?) {
        guard let menu = menu else {
            viewController?.setToolbar(title, menuItems: [])
            return
        }
        
        var visibleItems = menu.items
        var hiddenItems: [HeaderModel.MenuItem] = []
        
        if menu.items.count > menu.maxVisibleItems {
            visibleItems = Array(menu.items[0..<menu.maxVisibleItems])
            hiddenItems = Array(menu.items[menu.maxVisibleItems...])
        }
        
        let items = createToobarItems(visibleItems, hiddenItems)
        viewController?.setToolbar(title, menuItems: items)
    }
    
    func presentRightButton(with title: String, and actionResult: ActionResult) {
        viewController?.setNavigationBarRightButton(with: title, andActionResult: actionResult)
    }
    
    func presentComponents(_ components: [UIComponent]) {
        configureNavigationBar(for: components)
        viewController?.loadComponents(components)
        viewController?.hideErrorView()
    }
    
    func presentLoading(isLoading: Bool, withStyle style: LoadingViewStyle) {
        viewController?.showLoading(isLoading, withStyle: style)
    }
    
    func presentUpdatedLoadingText(_ text: String) {
        viewController?.updateLoadingText(text)
    }
    
    func presentAlert(with property: AlertAction.Property) {
        let popupController = PopupViewController(title: property.title, description: property.message)
        popupController.hideCloseButton = false
        popupController.addAction(.init(title: property.positiveButton, style: .fill))
        viewController?.displayPopup(popupController)
    }
    
    func presentError(_ message: String?) {
        let title = Strings.Store.opsTitle
        let message = message ?? Strings.Store.generalError
        viewController?.showErrorView(title: title, message: message)
    }
    
    func performAction(_ result: ActionResult) {
        result.action.perform(delegate: self, result: result)
    }
}

// MARK: - ActionPerformDelegate
extension StorePresenter: ActionPerformDelegate {
    func presentLegacyStore(from data: [String: Any]) {
        coordinator.perform(action: .legacyStore(data: data))
    }
    
    func presentPayment(_ property: PaymentAction.Property, with inputData: [String: Any]) {
        let markdown = MarkdownStyle(markdown: property.payment.disclaimer)
        let item = StorePayment(title: "",
                                description: property.payment.description,
                                imageURL: property.payment.logoUrl,
                                infoURL: property.payment.infoUrl,
                                value: Double(property.payment.value) ?? 0.0,
                                storeId: property.payment.storeId,
                                inputData: inputData,
                                externalData: property.externalData,
                                styles: [markdown])
        coordinator.perform(action: .payment(item: item))
    }
    
    func presentAddress(_ property: AddressAction.Property) {
        let storeAddressItem = StoreAddressScreenContent(
            title: property.title,
            text: property.text,
            image: property.image,
            shouldDisplayHeader: property.shouldDisplayHeader
        )
        
        let navigation = NavigationAction.Navigation(screenId: property.navigation.screenId)
        let navigationProperty = NavigationAction.Property(navigation: navigation, externalData: property.externalData)
        
        coordinator.perform(action:
            .address(
                content: storeAddressItem,
                navigation: navigationProperty
            )
        )
    }
    
    func presentWebview(with urlString: String) {
        guard let url = URL(string: urlString) else {
            trackUrlError(urlString)
            return
        }
        
        let isHTTP = ["http", "https"].contains(url.absoluteString.lowercased())
        coordinator.perform(action: isHTTP ? .webview(url: url) : .open(url: url))
    }
    
    func present(remoteAction: RemoteAction, result: ActionResult) {
        viewController?.perform(remoteAction: remoteAction, result: result)
    }
    
    func openTransactionReceipt(with property: ReceiptAction.Property) {
        coordinator.perform(action: .receipt(transactionId: property.receipt.transactionId,
                                             transactionType: property.receipt.transactionType))
    }

    func open(urlString: String) {
        guard let url = URL(string: urlString) else {
            trackUrlError(urlString)
            return
        }
        
        guard urlChecker.canOpenURL(url) else {
            let errorMessage = "App cannot open url: \(url.absoluteString)"
            let error = StoreError.runtimeException(message: errorMessage)
            ErrorTracker.shared.track(error: error, origin: self)
            return
        }
        
        coordinator.perform(action: .open(url: url))
    }
    
    private func trackUrlError(_ urlString: String, _ function: String = #function) {
        let errorMessage = "Não foi possível criar uma url com a string: \(urlString), por isso a função não continuou."
        let error = StoreError.runtimeException(message: errorMessage)
        
        ErrorTracker.shared.track(error: error, origin: self, function: function)
    }
    
    func openScreen(with properties: NavigationAction.Property, fullScreen: Bool) {
        coordinator.perform(action: .openScreen(properties: properties, fullScreen: fullScreen))
    }
    
    func buildShareSettings(with property: ShareAction.Property) {
        let content = buildContentToShare(with: property)
        coordinator.perform(action: .share(link: content))
    }
}

private extension StorePresenter {
    func createToobarItems(_ visibleItems: [HeaderModel.MenuItem], _ hiddenItems: [HeaderModel.MenuItem]) -> [UIStoreBarButton] {
        var items = createToobarVisibleItems(visibleItems) + createToolbarHiddenItems(hiddenItems)
        items.reverse()
        return items
    }
    
    func createToobarVisibleItems(_ visibleItems: [HeaderModel.MenuItem]) -> [UIStoreBarButton] {
        let items: [UIStoreBarButton] = visibleItems.compactMap { item in
            guard let action = item.action?.wrapped else { return nil }
            let button = UIStoreBarButton(title: item.description, imageUrl: item.icon, action: action)
            return button
        }
        return items
    }
    
    func createToolbarHiddenItems(_ hiddenItems: [HeaderModel.MenuItem]) -> [UIStoreBarButton] {
        var items: [UIStoreBarButton] = []
        if hiddenItems.isNotEmpty {
            let menuItems: [UIStoreBarButton.Item] = hiddenItems.compactMap { item in
                guard let action = item.action?.wrapped else { return nil }
                return .init(title: item.description, actionResult: ActionResult(action: action))
            }
            items.append(UIStoreBarButton(title: Strings.Store.more, image: Assets.iconMore.image, items: menuItems))
        }
        return items
    }
    
    func buildContentToShare(with property: ShareAction.Property) -> UIActivityViewController {
        UIActivityViewController(activityItems: [property.text], applicationActivities: nil)
    }
    
    func configureNavigationBar(for components: [UIComponent]) {
        let isHeader = components.first?.isHeader ?? false
        viewController?.configureNavigationBar(isForHeader: isHeader)
    }
}
