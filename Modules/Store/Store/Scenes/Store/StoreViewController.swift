import UI
import UIKit

protocol StoreDisplay: AnyObject {
    func loadComponents(_ components: [UIComponent])
    func displayPopup(_ popup: PopupViewController)
    func setToolbar(_ title: String, menuItems: [UIStoreBarButton])
    func setNavigationBarRightButton(with title: String, andActionResult actionResult: ActionResult)
    func showLoading(_ isLoading: Bool, withStyle style: LoadingViewStyle)
    func updateLoadingText(_ text: String)
    func configureNavigationBar(isForHeader: Bool)
    func showErrorView(title: String, message: String?)
    func hideErrorView()
    func perform(remoteAction: RemoteAction, result: ActionResult)
}

final class StoreViewController: ViewController<StoreInteractorInputs, UIView>, LoadingViewProtocol {
    private lazy var toolbarWidth = navigationController?.navigationBar.frame.width ?? 0
    private lazy var toolbarHeight: CGFloat = {
        let navigationBarHeight = navigationController?.navigationBar.frame.height ?? 0
        if #available(iOS 11.0, *) {
            return view.safeAreaInsets.top
        }
        return UIApplication.shared.statusBarFrame.height + navigationBarHeight
    }()
    
    let loadingView: LoadingView = {
        let view = LoadingView()
        view.setStyle(.default)
        view.setActivityIndicatorStyle(.gray)
        return view
    }()
    
    private lazy var blurEffectView: UIView = {
        let blurEffect = UIBlurEffect(style: .prominent)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = CGRect(x: 0, y: 0, width: toolbarWidth, height: toolbarHeight)
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        return blurEffectView
    }()

    private lazy var toolbar: UIView = {
        let statusBarView = UIView(frame: CGRect(x: 0, y: 0, width: toolbarWidth, height: toolbarHeight))
        statusBarView.addSubview(blurEffectView)
        return statusBarView
    }()
    
    private var allowNavigationBarToChangeAlpha = false
    private lazy var container: ServerDrivenUIView = {
        let container = ServerDrivenUIView()
        container.delegate = self
        return container
    }()
    
    private lazy var errorView: ErrorView = {
        let view = ErrorView()
        view.isHidden = true
        view.buttonAction = requestComponents
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.sendAnalytics()
        requestComponents()
    }
    
    override func buildViewHierarchy() {
        view.addSubviews(errorView, container)
    }

    override func setupConstraints() {
        errorView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        container.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    override func configureViews() {
        setNavigationbarTransculent()
        view.backgroundColor = Colors.backgroundPrimary.color
        navigationController?.navigationBar.tintColor = Colors.branding600.color
        if #available(iOS 13.0, *) {} else {
            if isPresentationModal() {
                let barButton = UIBarButtonItem(title: Strings.Store.close, style: .plain, target: self, action: #selector(close))
                barButton.tintColor = Colors.branding600.color
                navigationItem.leftBarButtonItem = barButton
            }
        }
    }
    
    @objc
    private func handleBarButtonAction(sender: StoreBarButtonItem) {
        guard let actionResult = sender.actionResult else { return }
        didReceiveAction(actionResult)
    }
}

// MARK: - ServerDrivenUI Delegate
extension StoreViewController: ServerDrivenUIViewDelegate {
    func didReceiveAction(_ result: ActionResult) {
        interactor.performAction(result)
    }
    func viewDidScroll(_ offSet: CGFloat) {
        if allowNavigationBarToChangeAlpha {
            changeNavigationBarAlpha(to: offSet)
        }
    }
}

// MARK: StoreDisplay
extension StoreViewController: StoreDisplay {
    func perform(remoteAction: RemoteAction, result: ActionResult) {
        interactor.performRemoteRequest(from: remoteAction, and: result)
    }
    
    func hideErrorView() {
        errorView.isHidden = true
        container.isHidden = false
    }
    
    func showErrorView(title: String, message: String?) {
        errorView.setMessage(title: title, message: message)
        errorView.isHidden = false
        container.isHidden = true
    }
    
    func showLoading(_ isLoading: Bool, withStyle style: LoadingViewStyle) {
        view.endEditing(isLoading)
        loadingView.setStyle(style)
        isLoading ? startLoadingView() : stopLoadingView()
    }
    
    func updateLoadingText(_ text: String) {
        loadingView.text = text
    }
    
    func displayPopup(_ popup: PopupViewController) {
        showPopup(popup)
    }
    
    func loadComponents(_ components: [UIComponent]) {
        container.load(components)
    }
    
    func setToolbar(_ title: String, menuItems: [UIStoreBarButton]) {
        self.title = title
        if menuItems.isNotEmpty {
            menuItems.forEach { $0.delegate = self }
            navigationItem.setRightBarButtonItems(menuItems, animated: true)
        }
    }
    
    func setNavigationBarRightButton(with title: String, andActionResult actionResult: ActionResult) {
        let barButton = StoreBarButtonItem(title: title, style: .plain, target: self, action: #selector(self.handleBarButtonAction(sender:)))
        barButton.tintColor = Colors.branding600.color
        barButton.actionResult = actionResult
        navigationItem.rightBarButtonItem = barButton
    }
    
    func configureNavigationBar(isForHeader: Bool) {
        view.insertSubview(toolbar, aboveSubview: container)
        toolbar.snp.makeConstraints {
            $0.height.equalTo(toolbarHeight)
            $0.leading.top.trailing.equalToSuperview()
        }
        allowNavigationBarToChangeAlpha = isForHeader
        if isForHeader {
            container.extendUnderNavigationBar = isForHeader
            changeNavigationBarAlpha(to: 0)
        } else {
            changeNavigationBarAlpha(to: 1)
        }
    }
}

// MARK: - StoreBarButtonItemDelegate
extension StoreViewController: StoreBarButtonItemDelegate {
    func barButtonItemDidTap(_ actionResult: ActionResult) {
        interactor.performAction(actionResult)
    }
    
    func barButtonItemNeedsToPresent(_ buttons: [AlertButton]) {
        BottomSheet.show(in: self, buttons)
    }
    
    override func present(_ viewControllerToPresent: UIViewController,
                          animated flag: Bool,
                          completion: (() -> Void)? = nil) {
        if viewControllerToPresent is BottomSheet {
            viewControllerToPresent.modalPresentationStyle = .overFullScreen
        }
        super.present(viewControllerToPresent, animated: flag, completion: completion)
    }
}

// MARK: - Private Functions
private extension StoreViewController {
    func requestComponents() {
        interactor.requestComponents()
    }
    
    func isPresentationModal() -> Bool {
        if let index = navigationController?.viewControllers.firstIndex(of: self), index > 0 {
            return false
        } else if presentingViewController != nil {
            return true
        } else if navigationController?.presentingViewController?.presentedViewController == navigationController {
            return true
        } else if tabBarController?.presentingViewController is UITabBarController {
            return true
        } else {
            return false
        }
    }

    func setNavigationbarTransculent() {
        guard let navbar = navigationController?.navigationBar else { return }
        navbar.setBackgroundImage(UIImage(), for: .default)
        navbar.shadowImage = UIImage()
        navbar.isTranslucent = true
    }
    
    func changeNavigationBarAlpha(to alpha: CGFloat) {
        guard let navbar = navigationController?.navigationBar else { return }
        blurEffectView.alpha = alpha
        navbar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Colors.black.color.withAlphaComponent(alpha)]
        navigationItem.rightBarButtonItems?.forEach { item in
            ((item as? UIStoreBarButton)?.customView as? AvatarView)?.changeBackgroundOpacity(to: alpha)
        }
    }
    
    @objc
    func close() {
        dismiss(animated: true)
    }
}

private class StoreBarButtonItem: UIBarButtonItem {
    var actionResult: ActionResult?
}
