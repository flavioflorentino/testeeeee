import Core
import Dispatch
import Foundation
import FeatureFlag

protocol StoreServicing {
    func requestComponents(for store: Store.Item, _ callback: @escaping (Result<Data, ApiError>) -> Void)
    func favorite(_ id: String, completion: @escaping (Result<NoContent, ApiError>) -> Void)
    func unfavorite(_ id: String, completion: @escaping (Result<NoContent, ApiError>) -> Void)
}

final class StoreService {
    typealias Dependencies = HasMainQueue & HasFeatureManager
    private let dependencies: Dependencies
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
}

// MARK: - StoreServicing
extension StoreService: StoreServicing {
    func favorite(_ id: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        Api<NoContent>(endpoint: StoreServiceEndpoint.favorite(id: id)).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }

    func unfavorite(_ id: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        Api<NoContent>(endpoint: StoreServiceEndpoint.unfavorite(id: id)).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func requestComponents(for store: Store.Item, _ completion: @escaping (Result<Data, ApiError>) -> Void) {
        let endpoint = StoreServiceEndpoint.screen(id: store.id, serverData: store.serverData)
        Api<Data>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
