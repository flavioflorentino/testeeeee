import Foundation
import UIKit

public enum StoreHomeFactory {
    public static func make(homeId: String, legacyStoreHandler: LegacyStoreHandler, searchHeader: SearchHeaderLegacyView, searchResult: SearchResultLegacyViewController) -> UIViewController {
        let service: StoreServicing = StoreService()
        let coordinator: StoreCoordinating = StoreCoordinator(legacyStoreHandler: legacyStoreHandler)
        let presenter: StorePresenting = StorePresenter(coordinator: coordinator)
        let interactor = StoreInteractor(store: Store.Item(homeId), presenter: presenter, service: service)
        let viewController = StoreHomeViewController(interactor: interactor, searchHeader: searchHeader, searchResult: searchResult)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
