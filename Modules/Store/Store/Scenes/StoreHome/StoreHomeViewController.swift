import UI
import UIKit
import SnapKit

final class StoreHomeViewController: ViewController<StoreInteractorInputs, UIView> {
    fileprivate enum Layout {
        static let searchHeaderHeight: CGFloat = 44
    }
    
    private lazy var container: ServerDrivenUIView = {
        let container = ServerDrivenUIView()
        container.delegate = self
        return container
    }()
    
    private lazy var errorView: ErrorView = {
        let view = ErrorView()
        view.isHidden = true
        view.buttonAction = interactor.requestComponents
        return view
    }()
    
    private lazy var loading: UIActivityIndicatorView = {
        let loading = UIActivityIndicatorView(style: .gray)
        loading.color = Colors.grayscale600.color
        loading.hidesWhenStopped = true
        return loading
    }()
    
    let searchHeader: SearchHeaderLegacyView
    let searchResult: SearchResultLegacyViewController
    
    init(interactor: StoreInteractor, searchHeader: SearchHeaderLegacyView, searchResult: SearchResultLegacyViewController) {
        self.searchHeader = searchHeader
        self.searchResult = searchResult
        super.init(interactor: interactor)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.requestComponents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func buildViewHierarchy() {
        view.addSubviews(searchHeader, errorView, container, loading)
        addChild(searchResult)
        view.addSubview(searchResult.view)
    }
    
    override func setupConstraints() {
        searchHeader.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(Layout.searchHeaderHeight)
        }
        errorView.snp.makeConstraints {
            $0.top.equalTo(searchHeader.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
        container.snp.makeConstraints {
            $0.top.equalTo(searchHeader.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
        loading.snp.makeConstraints {
            $0.center.equalTo(container)
        }
        searchResult.view.snp.makeConstraints {
            $0.edges.equalTo(container)
        }
    }
    
    override func configureViews() {
        searchResult.view.isHidden = true
        setupSearchHeader()
    }
    
    override func configureStyles() {
        view.backgroundColor = Colors.backgroundPrimary.color
        searchHeader.backgroundColor = Colors.backgroundPrimary.color
    }
    
    func setupSearchHeader() {
        searchHeader.searchTextDidBeginEditing = { [weak self] text in
            self?.searchResult.view.isHidden = false
            self?.searchResult.searchTextChanged(text: text)
        }
        
        searchHeader.searchTextChanged = { [weak self] text in
            self?.searchResult.searchTextChanged(text: text)
        }
        
        searchHeader.cancelButtonTapped = { [weak self] in
            self?.searchResult.view.isHidden = true
        }
    }
}

// MARK: - ServerDrivenUI Delegate
extension StoreHomeViewController: ServerDrivenUIViewDelegate {
    func didReceiveAction(_ result: ActionResult) {
        interactor.performAction(result)
    }
    
    public func viewDidScroll(_ offSet: CGFloat) {}
}

// MARK: StoreDisplay
extension StoreHomeViewController: StoreDisplay {
    func configureNavigationBar(isForHeader: Bool) {}
    
    func displayPopup(_ popup: PopupViewController) {}
    
    func setToolbar(_ title: String, menuItems: [UIStoreBarButton]) {}
    
    func setNavigationBarRightButton(with title: String, andActionResult actionResult: ActionResult) {}
    
    func showLoading(_ isLoading: Bool, withStyle style: LoadingViewStyle) {
        if isLoading {
            errorView.isHidden = true
            container.isHidden = true
            loading.startAnimating()
        } else {
            loading.stopAnimating()
        }
    }
    
    func updateLoadingText(_ text: String) {}
    
    func showErrorView(title: String, message: String?) {
        errorView.setMessage(title: title, message: message)
        errorView.isHidden = false
        container.isHidden = true
    }
    
    func hideErrorView() {
        errorView.isHidden = true
        container.isHidden = false
    }
    
    func loadComponents(_ components: [UIComponent]) {
        container.load(components)
    }
    
    func perform(remoteAction: RemoteAction, result: ActionResult) {
        interactor.performRemoteRequest(from: remoteAction, and: result)
    }
}
