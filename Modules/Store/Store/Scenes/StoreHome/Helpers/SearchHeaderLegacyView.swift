import UIKit
import Foundation

public protocol SearchHeaderLegacyView: UIView {
    var leftButtonTapped: (() -> Void)? { get set }
    var cancelButtonTapped: (() -> Void)? { get set }
    var searchTextChanged: ((String) -> Void)? { get set }
    var searchTextDidBeginEditing: ((String) -> Void)? { get set }
    var searchTextDidEndEditing: ((String) -> Void)? { get set }
}
