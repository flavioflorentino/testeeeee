import UIKit
import Foundation

public protocol SearchResultLegacyViewController: UIViewController {
    func searchTextChanged(text: String)
}
