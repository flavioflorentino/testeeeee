import Foundation
import Core

public final class StoreServerDrivenUIDeeplinkResolver: DeeplinkResolver {
    private let legacyStoreHandler: LegacyStoreHandler
    private let paths: [String]
    
    private var deeplink: StoreServerDriveUIDeeplink?
    
    public convenience init(with legacyStoreHandler: LegacyStoreHandler, paths: String...) {
        self.init(with: legacyStoreHandler, paths: paths)
    }
    
    public init(with legacyStoreHandler: LegacyStoreHandler, paths: [String]) {
        self.legacyStoreHandler = legacyStoreHandler
        self.paths = paths
    }
    
    @discardableResult
    public func canHandle(url: URL, isAuthenticated: Bool) -> DeeplinkResolverResult {
        guard let deeplink = StoreServerDriveUIDeeplink(url: url, paths: paths) else {
            return .notHandleable
        }
        
        guard isAuthenticated else {
            return .onlyWithAuth
        }
        
        self.deeplink = deeplink
        return .handleable
    }
    
    public func open(url: URL, isAuthenticated: Bool) -> Bool {
        canHandle(url: url, isAuthenticated: isAuthenticated)
        
        guard let deeplink = deeplink else {
            return false
        }
        
        let externalData = deeplink.queryParameters.map { key, value in
            ServerData.Model(key: key, value: value ?? "")
        }

        let serverData = ServerData(externalData: externalData)
        let storeItem = Store.Item(deeplink.screenId, serverData: serverData, event: deeplink.event)
        return Store.present(item: storeItem, with: legacyStoreHandler)
    }
}
