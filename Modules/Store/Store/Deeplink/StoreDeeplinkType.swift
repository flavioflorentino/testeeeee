import Foundation

struct StoreServerDriveUIDeeplink {
    let path: String
    let screenId: String
    let event: ServerDrivenUIEvent?
    let queryParameters: [String: String?]
    
    init?(url: URL, paths: [String]) {
        let pathCheckResult = paths
            .first { url.path.hasPrefix($0) }
        
        guard let validPath = pathCheckResult else {
            return nil
        }
        
        let params = url.path
            .dropFirst(validPath.count)
            .split(separator: "/")
            .map(String.init)
        
        guard params.count == 1 else {
            return nil
        }
        
        let screenIdPosition = 0
        guard params.indices.contains(screenIdPosition) else {
            return nil
        }
        
        self.path = validPath
        self.screenId = params[screenIdPosition]
        self.queryParameters = url.queryParameters
        self.event = ServerDrivenUIEvent(name: "deep_link_store_accessed",
                                         properties: [
                                            "screenId": screenId,
                                            "deepLink": url.absoluteString
                                         ])
    }
    
    init?(url: URL, paths: String...) {
        self.init(url: url, paths: paths)
    }
}
