import Core
import Foundation
import FeatureFlag
import AnalyticsModule

typealias Dependencies = HasMainQueue & HasFeatureManager & HasAnalytics

final class DependencyContainer: Dependencies {
    lazy var mainQueue = DispatchQueue.main
    lazy var featureManager: FeatureManagerContract = FeatureManager.shared
    lazy var analytics: AnalyticsProtocol = Analytics.shared
}
