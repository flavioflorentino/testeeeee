import Foundation
import UIKit
import UI

final class AvatarView: UIView, ViewConfiguration {
    private enum Layout {
        static let backgroundAlpha: CGFloat = 0.6
    }
    
    var image: UIImage? {
        get {
            imageView.image
        }
        set(value) {
            imageView.image = value
        }
    }
    
    lazy var button: UIButton = {
        let button = UIButton()
        button.isUserInteractionEnabled = false
        return button
    }()
    
    private lazy var imageView = UIImageView()
    convenience init(size: Sizing.ImageView = .large, shadow: Shadow.Style = .light(color: .black(.light), opacity: .ultraLight)) {
        self.init(frame: CGRect.zero)
        configureStyle(size, shadow)
        buildLayout()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        button.addSubview(imageView)
        addSubview(button)
    }
    
    func setupConstraints() {
        button.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.size.equalTo(imageView.size)
        }
        imageView.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }
    
    func setImage(url path: URL?) {
        imageView.setImage(url: path)
    }
    
    func resetImage() {
        imageView.cancelRequest()
        imageView.image = nil
    }
    
    func growAnimate() {
        button.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
        UIView.animate(withDuration: 0.35) {
            self.button.transform = CGAffineTransform.identity
        }
    }
    
    func changeBackgroundOpacity(to alpha: CGFloat) {
        imageView.backgroundColor = Colors.backgroundPrimary.color.withAlphaComponent(Layout.backgroundAlpha - alpha)
    }
    
    private func configureStyle(_ size: Sizing.ImageView, _ shadow: Shadow.Style) {
        button.viewStyle(RoundedViewStyle(cornerRadius: .strong)).with(\.shadow, shadow)
        imageView.imageStyle(AvatarImageStyle(size: size)).with(\.backgroundColor, Colors.backgroundPrimary.color.withAlphaComponent(Layout.backgroundAlpha))
    }
}
