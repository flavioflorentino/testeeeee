import Foundation
import UIKit
import UI

final class GradientView: UIView {
    override class var layerClass: AnyClass {
        CAGradientLayer.classForCoder()
    }
    
    init(colours: [CGColor]) {
        super.init(frame: .zero)
        let gradientLayer = layer as? CAGradientLayer
        gradientLayer?.colors = colours
        backgroundColor = .clear
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
