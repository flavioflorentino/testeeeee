import UI
import UIKit

enum LoadingViewStyle {
    case `default`, transparent
}

extension LoadingView {
    typealias Style = LoadingViewStyle
    
    func setStyle(_ style: Style) {
        switch style {
        case .default:
            backgroundColor = Colors.backgroundPrimary.color
            text = nil
        case .transparent:
            backgroundColor = Colors.black.lightColor.withAlphaComponent(0.8)
            setTextColor(Colors.white.lightColor)
        }
    }
}
