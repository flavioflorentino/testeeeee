import Foundation
import UIKit
import UI

protocol StoreBarButtonItemDelegate: AnyObject {
    /// When the user taps on the button this method is called passing the action that needs to be executed
    /// - Parameter actionResult: The action associated with this `UIView`
    func barButtonItemDidTap(_ actionResult: ActionResult)
    
    /// To trigger the menu on devices with iOS versions from 13 or less this delegate method is called to present
    /// a `UIVIewController`
    /// - Parameter viewController: The `UIViewController` that needs to be presented
    func barButtonItemNeedsToPresent(_ buttons: [AlertButton])
}

final class UIStoreBarButton: UIBarButtonItem {
    private var bottomSheetButtons: [AlertButton] = []
    private let avatarView = AvatarView(size: .xSmall, shadow: .none)
    private var actionResult: ActionResult?
    private var menuItems: [UIStoreBarButton.Item] = []
    
    weak var delegate: StoreBarButtonItemDelegate?
    init(title: String, image: UIImage? = nil, action: Action? = nil, items: [Item] = []) {
        super.init()
        setMenuItems(items, and: action)
        createBarItem(title, image, nil)
    }
    
    init(title: String, imageUrl: String? = nil, action: Action? = nil, items: [Item] = []) {
        super.init()
        setMenuItems(items, and: action)
        createBarItem(title, nil, imageUrl)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension UIStoreBarButton {
    private func setMenuItems(_ items: [Item], and action: Action?) {
        menuItems = items
        if let action = action {
            actionResult = ActionResult(action: action)
        }
    }
    
    private func createBarItem(_ title: String, _ image: UIImage? = nil, _ imageUrl: String? = nil) {
        let shouldAddCustomBarItem = (imageUrl != nil) || (image != nil)
        if menuItems.isNotEmpty {
            if shouldAddCustomBarItem {
                addCustomBarItem(imageUrl, image, isMenu: true)
            }
            
            if #available(iOS 14.0, *) {
                createMenu(shouldAddCustomBarItem)
            } else {
                createMenuFallback(shouldAddCustomBarItem)
            }
        } else {
            if shouldAddCustomBarItem {
                addCustomBarItem(imageUrl, image, isMenu: false)
            } else {
                self.action = #selector(didTapAction)
            }
        }
        self.title = title
    }
    
    private func addCustomBarItem(_ imageUrl: String?, _ image: UIImage?, isMenu: Bool) {
        if !isMenu {
            avatarView.button.addTarget(self, action: #selector(didTapAction), for: .touchUpInside)
        }
        if let string = imageUrl, let url = URL(string: string) {
            avatarView.setImage(url: url)
        } else {
            avatarView.image = image
        }
        avatarView.button.isUserInteractionEnabled = true
        customView = avatarView
    }
    
    private func createMenuFallback(_ hasCustomBarItem: Bool) {
        bottomSheetButtons = []
        menuItems.forEach { item in
            let action = AlertButton(title: item.title, color: Colors.black.color, { [weak self] in
                self?.delegate?.barButtonItemDidTap(item.actionResult)
            })
            bottomSheetButtons.append(action)
        }
        if hasCustomBarItem {
            avatarView.button.addTarget(self, action: #selector(itemNeedsToPresent), for: .touchUpInside)
        } else {
            self.action = #selector(didTapAction)
        }
    }
    
    @available(iOS 14.0, *)
    private func createMenu(_ hasCustomBarItem: Bool) {
        let actions = menuItems.compactMap { item in
            UIAction(title: item.title) { [weak self] _ in
                self?.delegate?.barButtonItemDidTap(item.actionResult)
            }
        }
        
        if hasCustomBarItem {
            avatarView.button.showsMenuAsPrimaryAction = true
            avatarView.button.menu = UIMenu(children: actions)
        } else {
            primaryAction = nil
            menu = UIMenu(children: actions)
        }
    }
    
    @objc
    private func didTapAction() {
        guard let delegate = delegate else {
            ErrorTracker.shared.track(error: .runtimeException(message: "''delegate'' não foi definido"), origin: self)
            return
        }
        
        if let actionResult = actionResult {
            delegate.barButtonItemDidTap(actionResult)
        }
    }
    
    @objc
    private func itemNeedsToPresent() {
        guard let delegate = delegate else {
            ErrorTracker.shared.track(error: .runtimeException(message: "''delegate'' não foi definido"), origin: self)
            return
        }
        delegate.barButtonItemNeedsToPresent(bottomSheetButtons)
    }
}

extension UIStoreBarButton {
    struct Item {
        let title: String
        let actionResult: ActionResult
    }
}
