import UI
import UIKit
import AssetsKit
import Foundation

final class ErrorView: UIView {
    var buttonAction: (() -> Void)?

    private lazy var image: UIImageView = {
        let imageview = UIImageView()
        imageview.clipsToBounds = true
        imageview.contentMode = .scaleAspectFill
        imageview.image = Resources.Illustrations.iluPersonError.image
        return imageview
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var button: UIButton = {
        let button = UIButton(type: .custom)
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Strings.Store.tryAgainTitle, for: .normal)
        button.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc
    private func didTapButton() {
        buttonAction?()
    }
    
    func setMessage(title: String?, message: String?) {
        titleLabel.text = title
        messageLabel.text = message
    }
}

// MARK: - ViewConfiguration
extension ErrorView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubviews(image, titleLabel, messageLabel, button)
    }
    
    func setupConstraints() {
        image.snp.makeConstraints {
            $0.width.equalTo(180)
            $0.height.equalTo(180)
            $0.centerX.equalTo(titleLabel)
        }
        titleLabel.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base08)
            $0.trailing.equalToSuperview().inset(Spacing.base08)
            $0.top.equalTo(image.snp.bottom).offset(Spacing.base02)
        }
        messageLabel.snp.makeConstraints {
            $0.leading.trailing.equalTo(titleLabel)
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
        }
        button.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base04)
            $0.trailing.equalToSuperview().inset(Spacing.base04)
            $0.top.equalTo(messageLabel.snp.bottom).offset(Spacing.base06)
        }
    }
    
    func configureViews() {
        backgroundColor = .clear
    }
}
