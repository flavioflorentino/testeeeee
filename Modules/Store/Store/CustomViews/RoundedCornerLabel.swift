import Foundation
import UI
import UIKit

final class RoundedCornerLabel: UIView, ViewConfiguration {
    var text: String? {
        get {
            label.text
        }
        set(value) {
            label.text = value
        }
    }
    
    private lazy var label: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textAlignment, .left)
            .with(\.textColor, .white(.light))
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubview(label)
    }
    
    func setupConstraints() {
        label.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base00)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base01)
        }
    }
}
