import UI
import UIKit
import Foundation

typealias AlertButton = (title: String, color: UIColor?, action: (() -> Void)?)

final class BottomSheet: UIViewController {
    /// Indicates if the BottomSheet has already appeared so we dont re-animate
    private var hasShown = false
    
    /// Callback to notifiy the BottomSheet's caller that some action was performed
    private lazy var callback: (((() -> Void)?) -> Void)? = { [weak self] action in
        self?.dismiss(animated: false, completion: {
            action?()
        })
    }

    /// The buttons'objects used to creste the buttons on the AlertView
    private var buttonsAttributes: [AlertButton] = [] {
        didSet {
            addButtons()
        }
    }
    
    /// Constrols the bottom position of the View
    private var wrapperBottomConstraint: NSLayoutConstraint?
    private lazy var wrapper: UIView = {
        let view = UIView()
        view.viewStyle(BackgroundViewStyle(color: .white()))
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = 0.5
        stackView.axis = .vertical
        stackView.backgroundColor = .clear
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    private lazy var button: UIButton = {
        let button = UIButton(type: .custom)
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle("Cancelar", for: .normal)
        button.addTarget(self, action: #selector(dismissWithNoAction), for: .touchUpInside)
        return button
    }()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !hasShown {
            hasShown = true
            show()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildLayout()
    }
    
    /// Shows the custom alert
    static func show(in host: UIViewController, _ buttons: [AlertButton]) {
        let bottomSheet = BottomSheet()
        bottomSheet.configure(buttons)
        host.present(bottomSheet, animated: false)
    }
}

private extension BottomSheet {
    func configure(_ buttonsAttributes: [AlertButton]) {
        self.buttonsAttributes = buttonsAttributes
    }
    
    func show() {
        wrapperBottomConstraint?.constant = 0
        UIView.animate(withDuration: 0.3, animations: {
            self.view.backgroundColor = Colors.black.lightColor.withAlphaComponent(0.5)
            self.view.layoutIfNeeded()
        })
    }

    /// Adds the buttons to the BottomSheetView
    func addButtons() {
        stackView.arrangedSubviews.forEach({ $0.removeFromSuperview() })
        for (index, attribute) in buttonsAttributes.enumerated() {
            stackView.addArrangedSubview(create(attribute, from: index))
        }
    }

    /// Creates the view of the button
    func create(_ attribute: AlertButton, from index: Int) -> UIButton {
        let button = UIButton()
        button.tag = index
        button.backgroundColor = .clear
        button.setTitleColor(Colors.black.color, for: .normal)
        button.setTitle(attribute.title, for: .normal)
        button.titleLabel?.font = Typography.bodyPrimary().font()
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: Spacing.base03, bottom: 0, right: 0)
        button.snp.makeConstraints {
            $0.height.equalTo(Sizing.base07)
        }
        if #available(iOS 11.0, *) {
            button.contentHorizontalAlignment = .leading
        }
        return button
    }
    
    /// Hides the BottomSheetView Animated
    func hideAndExecute(_ action:(() -> Void)?) {
        wrapperBottomConstraint?.constant = 500
        UIView.animate(withDuration: 0.3,
                       animations: {
            self.view.layoutIfNeeded()
            self.view.backgroundColor = .clear
        }, completion: { _ in
            self.callback?(action)
        })
    }
    
    /// Dismiss the BottomSheet when the user touches the screen
    @objc
    func dismissWithNoAction() {
        hideAndExecute(nil)
    }
    
    /// Notifies the alert's caller to execute the button's action
    @objc
    func buttonAction(_ sender: UIButton) {
        let action = buttonsAttributes[sender.tag].action
        hideAndExecute(action)
    }
}

extension BottomSheet: ViewConfiguration {
    func buildViewHierarchy() {
        wrapper.addSubviews(stackView, button)
        view.addSubview(wrapper)
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissWithNoAction)))
    }
    
    func setupConstraints() {
        wrapperBottomConstraint = wrapper.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 500)
        wrapperBottomConstraint?.isActive = true

        wrapper.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
        }
        stackView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
        }
        button.snp.makeConstraints {
            $0.bottom.equalToSuperview().inset(Spacing.base04)
            $0.trailing.equalToSuperview().inset(Spacing.base03)
            $0.leading.equalToSuperview().offset(Spacing.base03)
            $0.top.equalTo(stackView.snp.bottom).offset(Spacing.base01)
        }
    }
}
