import Foundation
import UIKit
import UI

// MARK: - Tips
public enum TooltipsTip {
    case store
    case settings
    
    var text: String {
        switch self {
        case .store:
            return Strings.Store.onboardingStoreOnTabBar
        case .settings:
            return Strings.Store.onboardingSettingInHome
        }
    }
}
// MARK: - TooltipViewLayout
private enum Layout {
    enum Tooltip {
        static let height: CGFloat = 40
    }
    enum Triangle {
        static let width: CGFloat = 18
        static let height: CGFloat = 18
        static let angle: CGFloat = 1 / 4 * CGFloat.pi
    }
}

// MARK: - TooltipView Arrow Orientations
public enum TooltipVerticalArrowOrientation {
    case top
    case bottom
}

public enum TooltipHorizontalArrowOrientation {
    case left
    case center
    case right
}

// MARK: - TooltipView
public final class TooltipView: UIView, ViewConfiguration {
    private var timer: Timer?
    private var verticalOrientation: TooltipVerticalArrowOrientation = .bottom
    private var horizontalOrientation: TooltipHorizontalArrowOrientation = .right
    private var tip: TooltipsTip = .store

    private lazy var wrapper: UIView = {
        let view = UIView()
        view.viewStyle(RoundedViewStyle(cornerRadius: .medium))
            .with(\.shadow, .medium(color: .black(.light), opacity: .light))
            .with(\.backgroundColor, .neutral800())
        return view
    }()
    
    private lazy var triangleView: UIView = {
        let view = UIView()
        view.viewStyle(RoundedViewStyle(cornerRadius: .light))
            .with(\.backgroundColor, .neutral800())
        let angle = Layout.Triangle.angle
        let rotation = CGAffineTransform(rotationAngle: angle)
        view.transform = rotation
        return view
    }()

    private lazy var textLabel: UILabel = {
        let textLabel = UILabel()
        textLabel.labelStyle(BodySecondaryLabelStyle(type: .highlight))
        .with(\.textAlignment, .center)
        textLabel.textColor = .white
        textLabel.text = tip.text
        return textLabel
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    public init(verticalOrientation: TooltipVerticalArrowOrientation, horizontalOrientation: TooltipHorizontalArrowOrientation, tip: TooltipsTip) {
        self.verticalOrientation = verticalOrientation
        self.horizontalOrientation = horizontalOrientation
        self.tip = tip
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func buildViewHierarchy() {
        wrapper.addSubviews(triangleView, textLabel)
        addSubview(wrapper)
    }
   
    public func startApearrence() {
        self.wrapper.alpha = 1
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(fadeOut), userInfo: nil, repeats: false)
    }
    
    @objc
    private func fadeOut() {
        UIView.animate(
            withDuration: 0.5,
            animations: {
                self.wrapper.alpha = 0
            }, completion: { _ in
                self.timer?.invalidate()
                self.removeFromSuperview()
            })
    }
    
    public func setupConstraints() {
        self.snp.makeConstraints {
            $0.height.equalTo(Layout.Tooltip.height)
        }

        wrapper.snp.makeConstraints {
            switch self.verticalOrientation {
            case .top:
                $0.bottom.equalToSuperview()
            case .bottom:
                $0.top.equalToSuperview()
            }
            $0.leading.trailing.equalToSuperview()
        }
        
        textLabel.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        triangleView.snp.makeConstraints {
            $0.width.equalTo(Layout.Triangle.width)
            $0.height.equalTo(Layout.Triangle.height)
            switch self.verticalOrientation {
            case .top:
                $0.centerY.equalTo(wrapper.snp.top).inset(Spacing.base00)
                $0.top.equalTo(self)
            case .bottom:
                $0.centerY.equalTo(wrapper.snp.bottom).inset(Spacing.base00)
                $0.bottom.equalTo(self)
            }
            switch self.horizontalOrientation {
            case .left:
                $0.leading.equalTo(wrapper.snp.leading).inset(Spacing.base01)
            case .center:
                $0.centerX.equalTo(wrapper.snp.centerX)
            case .right:
                $0.trailing.equalTo(wrapper.snp.trailing).inset(Spacing.base01)
            }
        }
    }
    
    public func configureViews() {
        wrapper.alpha = 0
    }
}
