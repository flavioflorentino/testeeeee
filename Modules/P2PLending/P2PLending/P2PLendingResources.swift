import Foundation

// swiftlint:disable convenience_type
final class P2PLendingResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: P2PLendingResources.self).url(forResource: "P2PLendingResources", withExtension: "bundle") else {
            return Bundle(for: P2PLendingResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: P2PLendingResources.self)
    }()
}
