import Foundation

final class LendingProposalObjectivesFactory {
    static func make(data: GreetingsResponse, proposal: Proposal) -> LendingProposalObjectivesViewController? {
        guard let objectives = data.objectives else {
            return nil
        }
        let container = DependencyContainer()
        let coordinator: LendingProposalObjectivesCoordinating = LendingProposalObjectivesCoordinator(data: data)
        let presenter: LendingProposalObjectivesPresenting = LendingProposalObjectivesPresenter(coordinator: coordinator)
        let viewModel = LendingProposalObjectivesViewModel(presenter: presenter,
                                                           objectives: objectives,
                                                           proposal: proposal,
                                                           dependencies: container)
        let viewController = LendingProposalObjectivesViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
