import UIKit

enum LendingProposalObjectivesAction {
    case friendSelection(_ proposal: Proposal)
}

protocol LendingProposalObjectivesCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: LendingProposalObjectivesAction)
}

final class LendingProposalObjectivesCoordinator: LendingProposalObjectivesCoordinating {
    weak var viewController: UIViewController?
    
    private let data: GreetingsResponse
    
    init(data: GreetingsResponse) {
        self.data = data
    }
    
    func perform(action: LendingProposalObjectivesAction) {
        guard case let .friendSelection(proposal) = action else {
            return
        }
        let controller = LendingProposalFriendsFactory.make(data: data, proposal: proposal)
        viewController?.show(controller, sender: nil)
    }
}
