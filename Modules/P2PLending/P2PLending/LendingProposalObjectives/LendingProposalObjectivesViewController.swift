import UI
import UIKit
import LendingComponents

protocol LendingProposalObjectivesDisplay: AnyObject {
    func display(objectives: [ProposalObjective])
}

extension LendingProposalObjectivesViewController.Layout {
    enum Insets {
        static let tableHeaderViewLayoutMargins: EdgeInsets = {
            var insets: EdgeInsets = .rootView
            insets.top = Spacing.base01
            insets.bottom = Spacing.base01
            return insets
        }()
    }
    
    enum Lenght {
        static let estimatedRowHeight: CGFloat = 50
        static let estimatedHeaderHeight: CGFloat = 44
    }
}

final class LendingProposalObjectivesViewController: ViewController<LendingProposalObjectivesViewModelInputs, UIView> {
    fileprivate enum Layout {}
    
    enum Section {
        case main
    }
    
    enum AccessibilityIdentifier: String {
        case objectivesTableView
    }
    
    // MARK: Header
    
    private lazy var headlineLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.Lending.Proposal.Objectives.headline
        label.labelStyle(TitleLabelStyle(type: .small))
        return label
    }()
    
    private lazy var headerStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [headlineLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base03
        stackView.compatibleLayoutMargins = Layout.Insets.tableHeaderViewLayoutMargins
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    
    // MARK: Body
    
    private lazy var tableHeaderView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.backgroundPrimary.color
        return view
    }()
    
    private(set) lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.separatorStyle = .none
        tableView.estimatedRowHeight = Layout.Lenght.estimatedRowHeight
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(ObjectiveCell.self, forCellReuseIdentifier: ObjectiveCell.identifier)
        tableView.accessibilityIdentifier = AccessibilityIdentifier.objectivesTableView.rawValue
        tableView.backgroundColor = .clear
        tableView.delegate = self
        return tableView
    }()
    
    // MARK: Handler
    
    private lazy var dataSource: TableViewDataSource<Section, ProposalObjective> = {
        let dataSource = TableViewDataSource<Section, ProposalObjective>(view: tableView) { tableView, indexPath, objective -> UITableViewCell? in
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ObjectiveCell.identifier,
                                                           for: indexPath) as? ObjectiveCell else {
                return nil
            }
            cell.setup(for: objective)
            return cell
        }
        dataSource.add(section: .main)
        return dataSource
    }()
    
    // MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.fetchObjectives()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let indexPathForSelectedRow = tableView.indexPathForSelectedRow else {
            return
        }
        tableView.deselectRow(at: indexPathForSelectedRow, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if isMovingFromParent {
            viewModel.sendTrackingEvent(.backButton)
        }
    }

    // MARK: View configuration

    override func buildViewHierarchy() {
        view.addSubview(tableView)
        tableHeaderView.addSubview(headerStackView)
    }
    
    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        headerStackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    override func configureViews() {
        setupNavigationAppearance()
        view.backgroundColor = Colors.backgroundPrimary.color
        view.tintColor = Colors.branding600.color
        tableView.dataSource = dataSource
    }
    
    private func setupNavigationAppearance() {
        navigationItem.title = Strings.Lending.Proposal.Objectives.title
        navigationController?.navigationBar.tintColor = Colors.branding400.color
        navigationController?.view.backgroundColor = Colors.backgroundPrimary.color
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .always
            navigationController?.navigationBar.prefersLargeTitles = true
            navigationController?.navigationBar.sizeToFit()
        }
        if #available(iOS 13.0, *) {
            let transparentBarAppearance = UINavigationBarAppearance()
            transparentBarAppearance.configureWithTransparentBackground()
            let opaqueBarAppearance = UINavigationBarAppearance()
            opaqueBarAppearance.configureWithOpaqueBackground()
            opaqueBarAppearance.backgroundColor = Colors.backgroundSecondary.color
            opaqueBarAppearance.shadowColor = .none
            navigationController?.navigationBar.scrollEdgeAppearance = transparentBarAppearance
            navigationController?.navigationBar.standardAppearance = opaqueBarAppearance
        }
    }
}

extension LendingProposalObjectivesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.selectObjective(at: indexPath)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        tableHeaderView
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        Layout.Lenght.estimatedRowHeight
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        UITableView.automaticDimension
    }
}

// MARK: View Model Outputs
extension LendingProposalObjectivesViewController: LendingProposalObjectivesDisplay {
    func display(objectives: [ProposalObjective]) {
        dataSource.update(items: objectives, from: .main)
    }
}
