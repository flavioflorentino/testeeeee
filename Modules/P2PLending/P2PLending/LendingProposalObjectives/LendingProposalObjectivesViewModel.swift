import Foundation
import AnalyticsModule

protocol LendingProposalObjectivesViewModelInputs: AnyObject {
    func fetchObjectives()
    func selectObjective(at indexPath: IndexPath)
    func sendTrackingEvent(_ type: LendingProposalObjectivesEvent)
}

final class LendingProposalObjectivesViewModel {
    typealias Dependencies = HasAnalytics
    private let presenter: LendingProposalObjectivesPresenting
    private let objectives: [ProposalObjective]
    private let dependencies: Dependencies

    private var proposal: Proposal
    
    init(presenter: LendingProposalObjectivesPresenting, objectives: [ProposalObjective], proposal: Proposal, dependencies: Dependencies) {
        self.presenter = presenter
        self.objectives = objectives
        self.proposal = proposal
        self.dependencies = dependencies
    }
}

extension LendingProposalObjectivesViewModel: LendingProposalObjectivesViewModelInputs {
    func fetchObjectives() {
        presenter.present(objectives: objectives)
    }
    
    func selectObjective(at indexPath: IndexPath) {
        proposal.objective = objectives[indexPath.row]
        dependencies.analytics.log(LendingProposalObjectivesEvent.selectedObjective(proposal.objective?.title ?? ""))
        presenter.didNextStep(action: .friendSelection(proposal))
    }
    
    func sendTrackingEvent(_ type: LendingProposalObjectivesEvent) {
        dependencies.analytics.log(type)
    }
}
