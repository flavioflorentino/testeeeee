import UI

protocol LendingProposalObjectivesPresenting: AnyObject {
    var viewController: LendingProposalObjectivesDisplay? { get set }
    func present(objectives: [ProposalObjective])
    func didNextStep(action: LendingProposalObjectivesAction)
}

final class LendingProposalObjectivesPresenter: LendingProposalObjectivesPresenting {
    private let coordinator: LendingProposalObjectivesCoordinating
    weak var viewController: LendingProposalObjectivesDisplay?

    init(coordinator: LendingProposalObjectivesCoordinating) {
        self.coordinator = coordinator
    }
    
    func present(objectives: [ProposalObjective]) {
        viewController?.display(objectives: objectives)
    }
    
    func didNextStep(action: LendingProposalObjectivesAction) {
        coordinator.perform(action: action)
    }
}
