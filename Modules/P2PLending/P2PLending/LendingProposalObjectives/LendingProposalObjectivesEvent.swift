import Foundation
import AnalyticsModule

enum LendingProposalObjectivesEvent: AnalyticsKeyProtocol {
    case selectedObjective(String)
    case backButton
    
    private var title: String {
        "P2P_LENDING_TOMADOR_OBJETIVO"
    }
    
    private var properties: [String: Any] {
        switch self {
        case .selectedObjective(let objective):
            return [
                AnalyticsKeys.screenName.name: title,
                AnalyticsKeys.buttonName.name: objective.uppercased().replacingOccurrences(of: " ", with: "_"),
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        case .backButton:
            return [
                AnalyticsKeys.screenName.name: title,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.backButton.description,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(AnalyticInteraction.buttonClicked.name, properties: properties, providers: [.eventTracker])
    }
}
