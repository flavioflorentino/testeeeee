import AnalyticsModule
import Foundation

enum LendingInstallmentDetailEvent: AnalyticsKeyProtocol {
    case picpayPayment(Date)
    case bankSlipPayment(Date)
    
    private var today: Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        let date = dateFormatter.string(from: Date())
        guard let today = dateFormatter.date(from: date) else { return Date() }
        return today
    }
    
    private func getScreenName(with dueDate: Date) -> String {
        if dueDate < today {
            return "P2P_LENDING_TOMADOR_PAGAR_PARCELA_VENCIDA"
        }
        
        if dueDate > today {
            return "P2P_LENDING_TOMADOR_PAGAR_PARCELA_ANTECIPADA"
        }
        
        return "P2P_LENDING_TOMADOR_PAGAR_PARCELA_ABERTA"
    }
    
    private var buttonName: String {
        switch self {
        case .picpayPayment:
            return "PAGAR_COM_PICPAY"
        case .bankSlipPayment:
            return "PAGAR_COM_BOLETO"
        }
    }
    
    private var name: String {
        switch self {
        case .picpayPayment, .bankSlipPayment:
            return AnalyticInteraction.buttonClicked.name
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .picpayPayment(let dueDate):
            return [
                AnalyticsKeys.screenName.name: getScreenName(with: dueDate),
                AnalyticsKeys.buttonName.name: buttonName,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        case .bankSlipPayment(let dueDate):
            return [
                AnalyticsKeys.screenName.name: getScreenName(with: dueDate),
                AnalyticsKeys.buttonName.name: buttonName,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.eventTracker])
    }
}
