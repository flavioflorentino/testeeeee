import Core
import Foundation

protocol LendingInstallmentDetailServicing: Servicing {
    func fetchInstallmentDetail(offerIdentifier: UUID, installmentNumber: Int, completion: @escaping ModelCompletionBlock<InstallmentDetail>)
}

final class LendingInstallmentDetailService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    private lazy var decoder: JSONDecoder = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        dateFormatter.locale = Locale(identifier: "pt-BR")
        let decoder = JSONDecoder(.convertFromSnakeCase)
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        return decoder
    }()
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - LendingInstallmentDetailServicing
extension LendingInstallmentDetailService: LendingInstallmentDetailServicing {
    func fetchInstallmentDetail(offerIdentifier: UUID, installmentNumber: Int, completion: @escaping ModelCompletionBlock<InstallmentDetail>) {
        let endpoint = P2PLendingEndpoint.installmentDetail(offerIdentifier: offerIdentifier, installmentNumber: installmentNumber)
        let api = Api<InstallmentDetail>(endpoint: endpoint)
        api.shouldUseDefaultDateFormatter = false
        api.execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
