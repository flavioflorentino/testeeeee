struct InstallmentDetailViewModel: Equatable {
    let valueTitle: String
    let value: String?
    let dueDateDescription: String
    let daysOverdue: String?
    let penaltyValue: String?
    let originalValue: String?
    let shouldDisplayDetailBreakdown: Bool
}
