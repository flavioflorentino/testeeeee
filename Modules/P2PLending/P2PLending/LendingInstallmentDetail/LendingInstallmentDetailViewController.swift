import UI
import UIKit

protocol LendingInstallmentDetailDisplaying: AnyObject {
    func display(installmentDetail: InstallmentDetailViewModel)
    func display(error: Error)
    func startLoading()
    func stopLoading()
}

final class LendingInstallmentDetailViewController: ViewController<LendingInstallmentDetailInteracting, UIView> {
    private typealias Localizable = Strings.Lending.Installment.Detail
    
    private lazy var closeButtonItem = UIBarButtonItem(
        title: GlobalLocalizable.closeButtonTitle,
        style: .plain,
        target: self,
        action: #selector(closeButtonTapped)
    )
    
    private lazy var loadingActivityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(style: .gray)
        activityIndicatorView.color = Colors.grayscale300.color
        return activityIndicatorView
    }()
    
    private lazy var installmentValueTitleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .small))
        label.textAlignment = .center
        return label
    }()
    
    private lazy var installmentValueLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CurrencyLabelStyle(type: .large))
        label.textAlignment = .center
        label.textColor = Colors.neutral600.color
        return label
    }()
    
    private lazy var installmentDueDateLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .highlight))
        label.textAlignment = .center
        return label
    }()
    
    private lazy var installmentValueHeaderStackView: UIStackView = {
        let arrangedSubviews = [installmentValueTitleLabel, installmentValueLabel, installmentDueDateLabel]
        let stackView = UIStackView(arrangedSubviews: arrangedSubviews)
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var installmentDetailBreakdownTitleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .small))
        label.text = Localizable.installmentDetailBreakdownTitle
        return label
    }()
    
    private lazy var installmentDetailBreakdownDelayedDaysItem: HorizontalSummaryItemView = {
        let itemView = HorizontalSummaryItemView()
        itemView.titleLabel.text = Localizable.installmentDetailBreakdownDelayedDaysLabel
        return itemView
    }()
    
    private lazy var installmentDetailBreakdownOriginalValueItem: HorizontalSummaryItemView = {
        let itemView = HorizontalSummaryItemView()
        itemView.titleLabel.text = Localizable.installmentDetailBreakdownOriginalValueLabel
        return itemView
    }()
    
    private lazy var installmentDetailBreakdownDelayInterestItem: HorizontalSummaryItemView = {
        let itemView = HorizontalSummaryItemView()
        itemView.titleLabel.text = Localizable.installmentDetailBreakdownDelayInterestLabel
        return itemView
    }()
    
    private lazy var installmentDetailBreakdownValuesStackView: UIStackView = {
        let arrangedSubviews = [
            installmentDetailBreakdownDelayedDaysItem,
            installmentDetailBreakdownOriginalValueItem,
            installmentDetailBreakdownDelayInterestItem
        ]
        let stackView = UIStackView(arrangedSubviews: arrangedSubviews)
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var installmentNoticeView: ImportantNoticeView = {
        let importantNoticeView = ImportantNoticeView()
        importantNoticeView.messageLabel.text = Localizable.installmentDelayNotice
        return importantNoticeView
    }()
    
    private lazy var installmentDetailBreakdownStackView: UIStackView = {
        let arrangedSubviews = [
            SeparatorView(),
            installmentDetailBreakdownTitleLabel,
            installmentDetailBreakdownValuesStackView,
            SeparatorView(),
            installmentNoticeView
        ]
        let stackView = UIStackView(arrangedSubviews: arrangedSubviews)
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var paymentOptionsSeparator = SeparatorView()
    
    private lazy var picPayPaymentOptionBanner: PaymentOptionBanner = {
        let paymentOption = PaymentOption(
            icon: Assets.lendingGlyphPicpayBalance.image,
            title: Localizable.installmentPayViaPicPayPaymentOptionTitle,
            description: Localizable.installmentPayViaPicPayPaymentOptionDescription
        )
        let paymentOptionBanner = PaymentOptionBanner(paymentOption: paymentOption)
        paymentOptionBanner.delegate = self
        return paymentOptionBanner
    }()
    
    private lazy var bankSlipPaymentOptionBanner: PaymentOptionBanner = {
        let paymentOption = PaymentOption(
            icon: Assets.lendingGlyphBankSlip.image,
            title: Localizable.installmentPayViaBankSlipPaymentOptionTitle,
            description: Localizable.installmentPayViaBankSlipPaymentOptionDescription
        )
        let paymentOptionBanner = PaymentOptionBanner(paymentOption: paymentOption)
        paymentOptionBanner.delegate = self
        return paymentOptionBanner
    }()
    
    private lazy var installmentPaymentOptionsStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [picPayPaymentOptionBanner, bankSlipPaymentOptionBanner])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var rootStackView: UIStackView = {
        let arrangedSubviews = [
            installmentValueHeaderStackView,
            installmentDetailBreakdownStackView,
            paymentOptionsSeparator,
            installmentPaymentOptionsStackView
        ]
        let stackView = UIStackView(arrangedSubviews: arrangedSubviews)
        stackView.axis = .vertical
        stackView.spacing = Spacing.base03
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.compatibleLayoutMargins = .rootView
        return stackView
    }()
    
    private lazy var scrollView = UIScrollView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.fetchInstallmentDetail()
    }

    override func buildViewHierarchy() {
        view.addSubview(loadingActivityIndicatorView)
        view.addSubview(scrollView)
        
        scrollView.addSubview(rootStackView)
    }
    
    override func setupConstraints() {
        loadingActivityIndicatorView.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
        
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        rootStackView.snp.makeConstraints {
            $0.edges.width.equalToSuperview()
        }
    }

    override func configureViews() {
        navigationItem.title = Localizable.title
        view.backgroundColor = Colors.backgroundPrimary.color
        view.tintColor = Colors.branding600.color
        navigationItem.leftBarButtonItem = closeButtonItem
    }
}

@objc
private extension LendingInstallmentDetailViewController {
    func closeButtonTapped() {
        interactor.dismiss()
    }
}

extension LendingInstallmentDetailViewController: PaymentOptionBannerDelegate {
    func paymentOptionBannerTapped(_ banner: PaymentOptionBanner) {
        switch banner {
        case picPayPaymentOptionBanner:
            interactor.proceedToPicPayPayment()
        case bankSlipPaymentOptionBanner:
            interactor.proceedToBankSlipPayment()
        default:
            break
        }
    }
}

// MARK: - LendingInstallmentDetailDisplaying
extension LendingInstallmentDetailViewController: LendingInstallmentDetailDisplaying {
    func display(installmentDetail: InstallmentDetailViewModel) {
        scrollView.alpha = 1.0
        installmentValueTitleLabel.text = installmentDetail.valueTitle
        installmentValueLabel.text = installmentDetail.value
        installmentDueDateLabel.text = installmentDetail.dueDateDescription
        installmentDetailBreakdownDelayedDaysItem.valueLabel.text = installmentDetail.daysOverdue
        installmentDetailBreakdownOriginalValueItem.valueLabel.text = installmentDetail.originalValue
        installmentDetailBreakdownDelayInterestItem.valueLabel.text = installmentDetail.penaltyValue
        installmentDetailBreakdownStackView.isHidden = !installmentDetail.shouldDisplayDetailBreakdown
        paymentOptionsSeparator.isHidden = installmentDetail.shouldDisplayDetailBreakdown
    }
    
    func display(error: Error) {
        let alert = PopupViewController(
            title: GlobalLocalizable.errorAlertTitle,
            description: error.localizedDescription,
            preferredType: .text
        )
        let action = PopupAction(title: GlobalLocalizable.dismissButtonTitle, style: .fill) {
            self.interactor.dismiss()
        }
        alert.addAction(action)
        showPopup(alert)
    }
    
    func startLoading() {
        loadingActivityIndicatorView.startAnimating()
        scrollView.alpha = 0.0
    }
    
    func stopLoading() {
        loadingActivityIndicatorView.stopAnimating()
    }
}
