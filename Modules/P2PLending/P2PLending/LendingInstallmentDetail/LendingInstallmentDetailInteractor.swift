import Foundation
import AnalyticsModule

protocol LendingInstallmentDetailInteracting: AnyObject {
    func fetchInstallmentDetail()
    func proceedToPicPayPayment()
    func proceedToBankSlipPayment()
    func dismiss()
}

final class LendingInstallmentDetailInteractor {
    typealias Dependencies = HasAnalytics
    private let service: LendingInstallmentDetailServicing
    private let presenter: LendingInstallmentDetailPresenting
    private let offerIdentifier: UUID
    private let installmentNumber: Int
    private let dependencies: Dependencies
    
    private var installmentDetail: InstallmentDetail?

    init(service: LendingInstallmentDetailServicing,
         presenter: LendingInstallmentDetailPresenting,
         offerIdentifier: UUID,
         installmentNumber: Int,
         dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.offerIdentifier = offerIdentifier
        self.installmentNumber = installmentNumber
        self.dependencies = dependencies
    }
}

// MARK: - LendingInstallmentDetailInteracting
extension LendingInstallmentDetailInteractor: LendingInstallmentDetailInteracting {
    func fetchInstallmentDetail() {
        presenter.startLoading()
        service.fetchInstallmentDetail(offerIdentifier: offerIdentifier, installmentNumber: installmentNumber) { [weak self] result in
            self?.presenter.stopLoading()
            switch result {
            case .success(let installmentDetail):
                self?.installmentDetail = installmentDetail
                self?.presenter.present(installmentDetail: installmentDetail)
            case .failure(let error):
                self?.presenter.present(error: error)
            }
        }
    }
    
    func proceedToPicPayPayment() {
        guard let installmentDetail = self.installmentDetail else { return }
        dependencies.analytics.log(LendingInstallmentDetailEvent.picpayPayment(installmentDetail.dueDate))
        presenter.presentPicPayPayment(for: installmentDetail)
    }
    
    func proceedToBankSlipPayment() {
        guard let installmentDetail = self.installmentDetail else { return }
        dependencies.analytics.log(LendingInstallmentDetailEvent.bankSlipPayment(installmentDetail.dueDate))
        presenter.didNextStep(action: .payViaBankSlip(installmentDetail: installmentDetail))
    }
    
    func dismiss() {
        presenter.didNextStep(action: .close)
    }
}
