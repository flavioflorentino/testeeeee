import Foundation

protocol LendingInstallmentDetailPresenting: AnyObject {
    var viewController: LendingInstallmentDetailDisplaying? { get set }
    func present(installmentDetail: InstallmentDetail)
    func presentPicPayPayment(for installmentDetail: InstallmentDetail)
    func present(error: Error)
    func startLoading()
    func stopLoading()
    func didNextStep(action: LendingInstallmentDetailAction)
}

final class LendingInstallmentDetailPresenter {
    private typealias Localizable = Strings.Lending.Installment.Detail
    
    private let coordinator: LendingInstallmentDetailCoordinating
    weak var viewController: LendingInstallmentDetailDisplaying?
    
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "pt-BR")
        dateFormatter.setLocalizedDateFormatFromTemplate("MMMMd")
        return dateFormatter
    }()

    init(coordinator: LendingInstallmentDetailCoordinating) {
        self.coordinator = coordinator
    }
    
    private func installmentDetailViewModel(for installmentDetail: InstallmentDetail) -> InstallmentDetailViewModel {
        InstallmentDetailViewModel(
            valueTitle: valueTitle(for: installmentDetail),
            value: installmentDetail.value.toCurrencyString(),
            dueDateDescription: dueDateDescription(for: installmentDetail),
            daysOverdue: installmentDetail.daysOverdue?.description,
            penaltyValue: installmentDetail.penaltyValue?.toCurrencyString(),
            originalValue: installmentDetail.originalValue.toCurrencyString(),
            shouldDisplayDetailBreakdown: shouldDisplayDetailBreakdown(for: installmentDetail)
        )
    }
    
    private func valueTitle(for installmentDetail: InstallmentDetail) -> String {
        guard Calendar.current.compare(Date(), to: installmentDetail.dueDate, toGranularity: .day) == .orderedDescending else {
            return Localizable.installmentValueTitle
        }
        return Localizable.installmentUpdatedValueTitle
    }
    
    private func dueDateDescription(for installmentDetail: InstallmentDetail) -> String {
        guard Calendar.current.compare(Date(), to: installmentDetail.dueDate, toGranularity: .day) == .orderedAscending else {
            return Localizable.installmentDueDateToday
        }
        return Localizable.installmentDueDateDescription(dateFormatter.string(from: installmentDetail.dueDate))
    }
    
    private func shouldDisplayDetailBreakdown(for installmentDetail: InstallmentDetail) -> Bool {
        Calendar.current.compare(Date(), to: installmentDetail.dueDate, toGranularity: .day) == .orderedDescending
    }
}

// MARK: - LendingInstallmentDetailPresenting
extension LendingInstallmentDetailPresenter: LendingInstallmentDetailPresenting {
    func present(installmentDetail: InstallmentDetail) {
        viewController?.display(installmentDetail: installmentDetailViewModel(for: installmentDetail))
    }
    
    func presentPicPayPayment(for installmentDetail: InstallmentDetail) {
        let description = Localizable.installmentDetailDefaultPaymentDescription(
            installmentDetail.number,
            installmentDetail.numberOfInstallments
        )
        didNextStep(action: .payViaPicPay(installmentDetail: installmentDetail, paymentDescription: description))
    }
    
    func present(error: Error) {
        viewController?.display(error: error)
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
    
    func didNextStep(action: LendingInstallmentDetailAction) {
        coordinator.perform(action: action)
    }
}
