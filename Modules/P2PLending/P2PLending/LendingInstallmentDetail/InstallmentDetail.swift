import Foundation

struct InstallmentDetail: Decodable, Equatable {
    let number: Int
    let numberOfInstallments: Int
    let value: Double
    let dueDate: Date
    let digitableLine: String
    let penaltyValue: Double?
    let daysOverdue: Int?
    let originalValue: Double
}
