import UIKit

enum LendingInstallmentDetailFactory {
    static func make(offerIdentifier: UUID, installmentNumber: Int) -> LendingInstallmentDetailViewController {
        let container = DependencyContainer()
        let service: LendingInstallmentDetailServicing = LendingInstallmentDetailService(dependencies: container)
        let coordinator: LendingInstallmentDetailCoordinating = LendingInstallmentDetailCoordinator(dependencies: container)
        let presenter: LendingInstallmentDetailPresenting = LendingInstallmentDetailPresenter(coordinator: coordinator)
        let interactor = LendingInstallmentDetailInteractor(
            service: service,
            presenter: presenter,
            offerIdentifier: offerIdentifier,
            installmentNumber: installmentNumber,
            dependencies: container
        )
        let viewController = LendingInstallmentDetailViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
