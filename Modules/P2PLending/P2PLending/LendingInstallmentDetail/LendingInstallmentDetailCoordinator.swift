import UIKit

enum LendingInstallmentDetailAction: Equatable {
    case payViaPicPay(installmentDetail: InstallmentDetail, paymentDescription: String)
    case payViaBankSlip(installmentDetail: InstallmentDetail)
    case close
}

protocol LendingInstallmentDetailCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: LendingInstallmentDetailAction)
}

final class LendingInstallmentDetailCoordinator: LendingInstallmentDetailCoordinating {
    typealias Dependencies = HasBilletSceneProvider
    let dependencies: Dependencies
    
    weak var viewController: UIViewController?
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func perform(action: LendingInstallmentDetailAction) {
        switch action {
        case let .payViaPicPay(installmentDetail, paymentDescription):
            let digitableLine = installmentDetail.digitableLine
            guard let controller = dependencies.billetSceneProvider?.billetScene(for: digitableLine, with: paymentDescription) else {
                return
            }
            viewController?.show(controller, sender: nil)
        case .payViaBankSlip(let installmentDetail):
            let controller = LendingDigitableLineFactory.make(installmentDetail: installmentDetail)
            viewController?.show(controller, sender: nil)
        case .close:
            viewController?.dismiss(animated: true)
        }
    }
}
