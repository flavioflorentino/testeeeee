import Foundation
import AnalyticsModule

public enum LendingSuccessEvent: AnalyticsKeyProtocol {
    case didFinishFlow(ConsultingAgent)
    case didDeclineOffer
    
    private func getTitle(for agent: ConsultingAgent) -> String {
        agent == .borrower ? "P2P_LENDING_TOMADOR_PROPOSTA_ENVIADA" : "P2P_LENDING_INVESTIDOR_EMPRESTIMO_ENVIADO"
    }
    
    private var declineOfferTitle: String {
        "P2P_LENDING_INVESTIDOR_PROPOSTA"
    }
    
    var name: String {
        switch self {
        case .didFinishFlow:
            return AnalyticInteraction.buttonClicked.name
        case .didDeclineOffer:
            return AnalyticInteraction.dialogOptionSelected.name
        }
    }
    
    var properties: [String: Any] {
        switch self {
        case .didFinishFlow(let agent):
            return [
                AnalyticsKeys.screenName.name: getTitle(for: agent),
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.okButton.description
            ]
        case .didDeclineOffer:
            return [
                AnalyticsKeys.screenName.name: declineOfferTitle,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.dialogName.name: AnalyticsConstants.proposalDeclined.description,
                AnalyticsKeys.optionSelected.name: AnalyticsConstants.declineSelected.description
            ]
        }
    }
    
    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.eventTracker])
    }
}
