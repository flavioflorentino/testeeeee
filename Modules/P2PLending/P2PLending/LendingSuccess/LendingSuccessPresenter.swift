import Foundation

protocol LendingSuccessPresenting: AnyObject {
    var viewController: LendingSuccessDisplay? { get set }
    func present(content: SuccessContent)
    func didNextStep(action: LendingSuccessAction)
}

final class LendingSuccessPresenter {
    private let coordinator: LendingSuccessCoordinating
    weak var viewController: LendingSuccessDisplay?

    init(coordinator: LendingSuccessCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - LendingSuccessPresenting
extension LendingSuccessPresenter: LendingSuccessPresenting {
    func present(content: SuccessContent) {
        viewController?.display(content: content)
    }
    
    func didNextStep(action: LendingSuccessAction) {
        coordinator.perform(action: action)
    }
}
