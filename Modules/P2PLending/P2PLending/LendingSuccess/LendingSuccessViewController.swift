import UIKit
import UI

public protocol LendingSuccessDelegate: AnyObject {
    func lendingSuccessControllerDidDismiss(_ lendingSuccessViewController: UIViewController)
}

protocol LendingSuccessDisplay: AnyObject {
    func display(content: SuccessContent)
}

final class LendingSuccessViewController: ViewController<LendingSuccessInteracting, UIView> {
    fileprivate enum Layout { }
    
    enum AccessibilityIdentifier: String {
        case titleLabel
        case bodyLabel
        case continueButton
    }
    
    private typealias Localizable = Strings.Lending.Success
    
    private lazy var illustrationImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .center
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
        label.textAlignment = .center
        label.accessibilityIdentifier = AccessibilityIdentifier.titleLabel.rawValue
        return label
    }()
    
    private lazy var bodyLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        label.accessibilityIdentifier = AccessibilityIdentifier.bodyLabel.rawValue
        return label
    }()
    
    private lazy var textContentStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [titleLabel, bodyLabel])
        stackView.spacing = Spacing.base01
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var continueButton: UIButton = {
        let button = UIButton(type: .custom)
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Localizable.dismissButtonTitle, for: .normal)
        button.addTarget(self, action: #selector(continueButtonTapped), for: .touchUpInside)
        button.accessibilityIdentifier = AccessibilityIdentifier.continueButton.rawValue
        return button
    }()
    
    private lazy var rootStackView: UIStackView = {
        let arrangedSubviews: [UIView] = [
            illustrationImageView,
            textContentStackView,
            continueButton
        ]
        let stackView = UIStackView(arrangedSubviews: arrangedSubviews)
        stackView.axis = .vertical
        stackView.spacing = Spacing.base03
        return stackView
    }()
    
    private weak var delegate: LendingSuccessDelegate?
    
    init(interactor: LendingSuccessInteracting, delegate: LendingSuccessDelegate) {
        super.init(interactor: interactor)
        self.delegate = delegate
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.fetchContent()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(rootStackView)
    }
    
    override func setupConstraints() {
        rootStackView.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.leading.equalTo(view.layoutMarginsGuide)
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
}

@objc
private extension LendingSuccessViewController {
    func continueButtonTapped() {
        interactor.dismiss()
        delegate?.lendingSuccessControllerDidDismiss(self)
    }
}

// MARK: LendingSuccessDisplay
extension LendingSuccessViewController: LendingSuccessDisplay {
    func display(content: SuccessContent) {
        illustrationImageView.image = content.illustration
        titleLabel.text = content.title
        bodyLabel.attributedText = content.body
    }
}
