import UI
import UIKit

struct DeclinedOfferSuccessContent: SuccessContent {
    private typealias Localizable = Strings.Lending.Opportunity.Offer
    
    let illustration: UIImage? = Assets.lendingFigureDecline.image
    let title: String? = Localizable.offerDeclinedTitle
    let body: NSAttributedString? = NSAttributedString(
        string: Localizable.offerDeclinedBody,
        attributes: [
            .font: Typography.bodyPrimary().font(),
            .foregroundColor: Colors.grayscale600.color
        ]
    )
}
