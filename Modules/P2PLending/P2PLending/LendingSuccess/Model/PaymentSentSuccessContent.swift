import UI
import UIKit

public struct PaymentSentSuccessContent: SuccessContent {
    private typealias Localizable = Strings.Lending.Payment.Sent
    
    public let illustration: UIImage? = Assets.lendingFigureLike.image
    public let title: String? = Localizable.title
    public let body: NSAttributedString? = {
        let attributedText = NSMutableAttributedString(
            string: Localizable.body,
            attributes: [
                .font: Typography.bodyPrimary().font(),
                .foregroundColor: Colors.grayscale600.color
            ]
        )
        let highlightRange = (Localizable.body as NSString).range(of: Localizable.bodyHighlight)
        attributedText.addAttribute(.font, value: Typography.bodyPrimary(.highlight).font(), range: highlightRange)
        return attributedText
    }()
    
    public init() {}
}
