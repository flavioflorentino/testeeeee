import UIKit

public protocol SuccessContent {
    var illustration: UIImage? { get }
    var title: String? { get }
    var body: NSAttributedString? { get }
}
