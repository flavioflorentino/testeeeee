import UI
import UIKit

struct ProposalSentSuccessContent: SuccessContent {
    private typealias Localizable = Strings.Lending.Proposal.Sent
    
    let illustration: UIImage? = Assets.lendingFigureLike.image
    let title: String? = Localizable.title
    let body: NSAttributedString? = {
        let attributedText = NSMutableAttributedString(
            string: Localizable.body,
            attributes: [
                .font: Typography.bodyPrimary().font(),
                .foregroundColor: Colors.grayscale600.color
            ]
        )
        let highlightRange = (Localizable.body as NSString).range(of: Localizable.bodyHighlight)
        attributedText.addAttribute(.font, value: Typography.bodyPrimary(.highlight).font(), range: highlightRange)
        return attributedText
    }()
}
