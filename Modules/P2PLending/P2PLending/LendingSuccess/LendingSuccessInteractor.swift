import Foundation

protocol LendingSuccessInteracting: AnyObject {
    func fetchContent()
    func dismiss()
}

final class LendingSuccessInteractor {
    private let presenter: LendingSuccessPresenting
    private let content: SuccessContent
    
    init(presenter: LendingSuccessPresenting, content: SuccessContent) {
        self.presenter = presenter
        self.content = content
    }
}

// MARK: - LendingSuccessInteracting
extension LendingSuccessInteractor: LendingSuccessInteracting {
    func fetchContent() {
        presenter.present(content: content)
    }
    
    func dismiss() {
        presenter.didNextStep(action: .dismiss)
    }
}
