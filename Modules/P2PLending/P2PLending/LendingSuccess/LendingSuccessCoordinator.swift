import UIKit

enum LendingSuccessAction {
    case dismiss
}

protocol LendingSuccessCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: LendingSuccessAction)
}

final class LendingSuccessCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - LendingSuccessCoordinating
extension LendingSuccessCoordinator: LendingSuccessCoordinating {
    func perform(action: LendingSuccessAction) {
        guard action == .dismiss else {
            return
        }
        viewController?.dismiss(animated: true)
    }
}
