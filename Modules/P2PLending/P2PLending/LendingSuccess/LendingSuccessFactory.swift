import Foundation

public enum LendingSuccessFactory {
    public static func make(content: SuccessContent, delegate: LendingSuccessDelegate) -> UIViewController {
        let coordinator: LendingSuccessCoordinating = LendingSuccessCoordinator()
        let presenter: LendingSuccessPresenting = LendingSuccessPresenter(coordinator: coordinator)
        let interactor = LendingSuccessInteractor(presenter: presenter, content: content)
        let viewController = LendingSuccessViewController(interactor: interactor, delegate: delegate)
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
