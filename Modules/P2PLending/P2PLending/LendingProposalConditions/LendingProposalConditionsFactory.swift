import Foundation

enum LendingProposalConditionsFactory {
    static func make(configuration: ProposalConditionsConfiguration, proposal: Proposal) -> LendingProposalConditionsViewController {
        let container = DependencyContainer()
        let service: LendingProposalConditionsServicing = LendingProposalConditionsService(dependencies: container)
        let coordinator: LendingProposalConditionsCoordinating = LendingProposalConditionsCoordinator(dependencies: container)
        let presenter: LendingProposalConditionsPresenting = LendingProposalConditionsPresenter(coordinator: coordinator)
        let viewModel = LendingProposalConditionsViewModel(
            service: service,
            presenter: presenter,
            configuration: configuration,
            proposal: proposal,
            dependencies: container
        )
        let viewController = LendingProposalConditionsViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
