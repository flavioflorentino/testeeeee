import UIKit

enum LendingProposalConditionsAction {
    case proposalSummary(proposal: Proposal)
    case taxInformation(simulation: ProposalConditionsSimulation)
    case showFAQ(url: URL)
}

protocol LendingProposalConditionsCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: LendingProposalConditionsAction)
}

final class LendingProposalConditionsCoordinator: LendingProposalConditionsCoordinating, URLOpenerCoordinating {
    typealias Dependencies = HasURLOpener
    
    weak var viewController: UIViewController?
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func perform(action: LendingProposalConditionsAction) {
        switch action {
        case let .proposalSummary(proposal):
            let controller = LendingProposalSummaryFactory.make(type: .local(proposal: proposal))
            viewController?.show(controller, sender: nil)
        case let .taxInformation(simulation):
            let controller = ProposalConditionsHelpFactory.make(simulation: simulation)
            viewController?.present(controller, animated: true, completion: nil)
        case let .showFAQ(url):
            showFAQ(for: url, using: dependencies)
        }
    }
}
