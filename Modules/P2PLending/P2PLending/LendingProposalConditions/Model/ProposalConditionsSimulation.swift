import Foundation

struct ProposalConditionsSimulation: Decodable, Equatable {
    let totalAmount: Double
    let totalInterest: Double
    let interestRateAmount: Double
    let installments: Int
    let installmentAmount: Double
    let monthCet: Float
    let iof: Double
    let commission: Double
    let commissionAmount: Double
    let monthRate: Float
    let firstPaymentDate: Date
}
