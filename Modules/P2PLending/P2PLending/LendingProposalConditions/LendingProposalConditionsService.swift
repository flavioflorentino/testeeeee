import Foundation
import Core

protocol LendingProposalConditionsServicing: Servicing {
    func simulateProposal(_ proposal: Proposal, completion: @escaping ModelCompletionBlock<ProposalConditionsSimulation>)
    func cancelSimulateProposalResquest()
}

final class LendingProposalConditionsService: LendingProposalConditionsServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    private var simulateProposalSessionTask: URLSessionTask?
    
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "pt_BR")
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return dateFormatter
    }()
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func simulateProposal(_ proposal: Proposal, completion: @escaping ModelCompletionBlock<ProposalConditionsSimulation>) {
        let api = Api<ProposalConditionsSimulation>(endpoint: P2PLendingEndpoint.proposalConditionsSimulation(proposal: proposal))
        let decoder = JSONDecoder()
        api.shouldUseDefaultDateFormatter = false
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        simulateProposalSessionTask = api.execute(jsonDecoder: decoder) { [weak self] result in
            let mappedResult = result.map(\.model)
            self?.dependencies.mainQueue.async {
                completion(mappedResult)
            }
        }
    }
    
    func cancelSimulateProposalResquest() {
        simulateProposalSessionTask?.cancel()
    }
}
