import Core
import Foundation
import UI
import UIKit

protocol LendingProposalConditionsPresenting: AnyObject {
    var viewController: LendingProposalConditionsDisplay? { get set }
    func setupForm(using configuration: ProposalConditionsConfiguration, and proposal: Proposal)
    func presentProposalConditionsSimulation(_ simulation: ProposalConditionsSimulation, for proposal: Proposal)
    func presentSimulationError()
    func startLoadingSimulation()
    func stopLoadingSimulation()
    func didNextStep(action: LendingProposalConditionsAction)
}

final class LendingProposalConditionsPresenter: LendingProposalConditionsPresenting {
    private typealias Localizable = Strings.Lending.Proposal.Conditions
    
    private let coordinator: LendingProposalConditionsCoordinating
    weak var viewController: LendingProposalConditionsDisplay?
    
    private lazy var percentNumberFormatter: NumberFormatter = {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .percent
        numberFormatter.minimumFractionDigits = 1
        numberFormatter.maximumFractionDigits = 2
        numberFormatter.locale = Locale(identifier: "pt_BR")
        return numberFormatter
    }()

    private func percentFormatted(value: Float?) -> String {
        guard
            let value = value,
            let percentFormattedValue = percentNumberFormatter.string(from: value / 100 as NSNumber)
            else {
                return ""
        }
        return percentFormattedValue
    }
    
    init(coordinator: LendingProposalConditionsCoordinating) {
        self.coordinator = coordinator
    }
    
    func setupForm(using configuration: ProposalConditionsConfiguration, and proposal: Proposal) {
        viewController?.setupInstallmentsStepper(
            minimumValue: Double(configuration.minimumInstallments),
            maximumValue: Double(configuration.maximumInstallments),
            suggestedValue: Double(proposal.installments ?? 0),
            footerText: Localizable.Installments.footer(configuration.minimumInstallments, configuration.maximumInstallments)
        )
        viewController?.setupInterestStepper(
            minimumValue: Double(configuration.minimumTax),
            maximumValue: Double(configuration.maximumTax),
            suggestedValue: Double(proposal.interest ?? 0),
            footerText: Localizable.Interest.footer(
                percentFormatted(value: configuration.minimumTax),
                percentFormatted(value: configuration.maximumTax)
            )
        )
    }
    
    func presentProposalConditionsSimulation(_ simulation: ProposalConditionsSimulation, for proposal: Proposal) {
        let formattedInstallments = "\(simulation.installments)x"
        let formattedInstallmentAmount = simulation.installmentAmount.toCurrencyString() ?? ""
        let fullString = Localizable.footerText(formattedInstallments, formattedInstallmentAmount)
        let fullNSString = fullString as NSString
        let summaryAttributes: [NSAttributedString.Key: Any] = [
            .font: Typography.title(.small).font(),
            .foregroundColor: Colors.grayscale700.color
        ]
        let summaryValuesAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: Colors.neutral600.color]
        let attributedString = NSMutableAttributedString(string: fullString, attributes: summaryAttributes)
        attributedString.addAttributes(summaryValuesAttributes, range: fullNSString.range(of: formattedInstallments))
        attributedString.addAttributes(summaryValuesAttributes, range: fullNSString.range(of: formattedInstallmentAmount))
        viewController?.displayProposalConditionsSummary(attributedString, allowsInteraction: false)
        viewController?.setContinueButtonEnable(state: true)
        viewController?.setTaxInformationButtonEnable(true)
        viewController?.stopLoadingSummary()
    }
    
    func presentSimulationError() {
        let errorMessage = NSAttributedString(
            string: Localizable.simulationErrorText,
            attributes: [
                .font: Typography.bodySecondary().font(),
                .foregroundColor: Colors.grayscale600.color
            ]
        )
        let tryAgainButtonTitle = NSAttributedString(
            string: Localizable.simulationErrorButtonTitle,
            attributes: [
                .font: Typography.bodySecondary().font(),
                .underlineStyle: NSUnderlineStyle.single.rawValue,
                .foregroundColor: Colors.branding600.color
            ]
        )
        let finalAttributtedString = NSMutableAttributedString(attributedString: errorMessage)
        finalAttributtedString.append(tryAgainButtonTitle)
        viewController?.displayProposalConditionsSummary(finalAttributtedString, allowsInteraction: true)
        viewController?.setContinueButtonEnable(state: false)
        viewController?.stopLoadingSummary()
    }
    
    func startLoadingSimulation() {
        viewController?.setContinueButtonEnable(state: false)
        viewController?.setTaxInformationButtonEnable(false)
        viewController?.startLoadingSummary()
    }
    
    func stopLoadingSimulation() {
        viewController?.stopLoadingSummary()
    }
    
    func didNextStep(action: LendingProposalConditionsAction) {
        coordinator.perform(action: action)
    }
}
