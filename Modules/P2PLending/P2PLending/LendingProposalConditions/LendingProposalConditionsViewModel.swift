import Foundation
import AnalyticsModule

protocol LendingProposalConditionsViewModelInputs: AnyObject {
    func fetchConfiguration()
    func simulateProposal()
    func installmentsValueChanged(_ installmentsValue: Double)
    func interestValueChanged(_ interestValue: Double)
    func showFAQ()
    func continueButtonTapped()
    func sendAnalyticsEvent(type: AnalyticInteraction)
    func showTaxInformation()
}

final class LendingProposalConditionsViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let service: LendingProposalConditionsServicing
    private let presenter: LendingProposalConditionsPresenting
    private let configuration: ProposalConditionsConfiguration
    private var proposal: Proposal
    private let shouldDelaySimulationRequests: Bool
    
    private var requestSimulationTimer: Timer?

    init(
        service: LendingProposalConditionsServicing,
        presenter: LendingProposalConditionsPresenting,
        configuration: ProposalConditionsConfiguration,
        proposal: Proposal,
        dependencies: Dependencies,
        shouldDelaySimulationRequests: Bool = true
    ) {
        self.service = service
        self.presenter = presenter
        self.configuration = configuration
        self.proposal = proposal
        self.dependencies = dependencies
        self.shouldDelaySimulationRequests = shouldDelaySimulationRequests
    }
    
    private func performSimulationRequest(_ timer: Timer? = nil) {
        let proposal = self.proposal
        presenter.startLoadingSimulation()
        service.cancelSimulateProposalResquest()
        service.simulateProposal(proposal) { [weak self] result in
            switch result {
            case .success(let simulationResponse):
                self?.presenter.presentProposalConditionsSimulation(simulationResponse, for: proposal)
                self?.proposal.simulation = simulationResponse
            case .failure(let error):
                if case .cancelled = error { break }
                self?.presenter.presentSimulationError()
            }
        }
    }
    
    private func sendAnalyticsEvent(_ event: LendingProposalConditionsEvent) {
        dependencies.analytics.log(event)
    }
}

extension LendingProposalConditionsViewModel: LendingProposalConditionsViewModelInputs {
    func fetchConfiguration() {
        presenter.setupForm(using: configuration, and: proposal)
        performSimulationRequest()
    }
    
    func simulateProposal() {
        guard shouldDelaySimulationRequests else {
            performSimulationRequest()
            return
        }
        requestSimulationTimer?.invalidate()
        requestSimulationTimer = Timer.scheduledTimer(
            withTimeInterval: 0.5,
            repeats: false,
            block: performSimulationRequest(_:)
        )
    }
    
    func installmentsValueChanged(_ installmentsValue: Double) {
        proposal.installments = Int(installmentsValue)
        simulateProposal()
    }
    
    func interestValueChanged(_ interestValue: Double) {
        proposal.interest = Float(interestValue)
        presenter.startLoadingSimulation()
        simulateProposal()
    }
    
    func showFAQ() {
        sendAnalyticsEvent(.helpTapped)
        guard let url = URL(string: "picpay://picpay/helpcenter/article/360049860711") else {
            return
        }
        presenter.didNextStep(action: .showFAQ(url: url))
    }
    
    func continueButtonTapped() {
        sendAnalyticsEvent(.checkProposalTapped)
        presenter.didNextStep(action: .proposalSummary(proposal: proposal))
    }
    
    func sendAnalyticsEvent(type: AnalyticInteraction) {
        switch type {
        case .backButtonTapped:
            sendAnalyticsEvent(.backButtonTapped)
        case .tryAgainTapped:
            sendAnalyticsEvent(.tryAgainTapped)
        default:
            break
        }
    }
    
    func showTaxInformation() {
        sendAnalyticsEvent(.faqTapped)
        guard let simulation = proposal.simulation else {
            return
        }
        presenter.didNextStep(action: .taxInformation(simulation: simulation))
    }
}
