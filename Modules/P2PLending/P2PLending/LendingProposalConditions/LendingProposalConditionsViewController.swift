import UI
import UIKit

protocol LendingProposalConditionsDisplay: AnyObject {
    func setupInstallmentsStepper(minimumValue: Double, maximumValue: Double, suggestedValue: Double, footerText: String?)
    func setupInterestStepper(minimumValue: Double, maximumValue: Double, suggestedValue: Double, footerText: String?)
    func displayProposalConditionsSummary(_ summaryText: NSAttributedString, allowsInteraction: Bool)
    func startLoadingSummary()
    func stopLoadingSummary()
    func setContinueButtonEnable(state: Bool)
    func setTaxInformationButtonEnable(_ isEnable: Bool)
}

final class LendingProposalConditionsViewController: ViewController<LendingProposalConditionsViewModelInputs, UIView> {
    enum AccessibilityIdentifier: String {
        case installmentsStepper
        case interstStepper
        case summaryConditions
        case continueButton
        case faqButton
    }
    
    private lazy var helpButtonItem: UIBarButtonItem = {
        let buttonItem = UIBarButtonItem(image: Assets.lendingGlyphHelp.image,
                                         style: .plain,
                                         target: self,
                                         action: #selector(helpButtonTapped))
        buttonItem.accessibilityLabel = GlobalLocalizable.helpButtonAccessibilityLabel
        return buttonItem
    }()
        
    private(set) lazy var installmentsStepperField: StepperField = {
        let stepperField = StepperField()
        stepperField.headlineText = Strings.Lending.Proposal.Conditions.Installments.headline
        stepperField.descriptionText = Strings.Lending.Proposal.Conditions.Installments.field
        stepperField.stepper.allowsTextEditing = true
        stepperField.stepper.addTarget(self, action: #selector(installmentsStepperValueChanged(_:)), for: .valueChanged)
        stepperField.stepper.accessibilityIdentifier = AccessibilityIdentifier.installmentsStepper.rawValue
        return stepperField
    }()
    
    private(set) lazy var interestStepperField: StepperField = {
        let stepperField = StepperField()
        stepperField.headlineText = Strings.Lending.Proposal.Conditions.Interest.headline
        stepperField.descriptionText = Strings.Lending.Proposal.Conditions.Interest.field
        stepperField.stepper.numberOfFractionDigits = 1
        stepperField.stepper.rightAccessoryText = "%"
        stepperField.stepper.stepValue = 0.1
        stepperField.stepper.allowsTextEditing = true
        stepperField.stepper.addTarget(self, action: #selector(interestStepperValueChanged(_:)), for: .valueChanged)
        stepperField.stepper.accessibilityIdentifier = AccessibilityIdentifier.interstStepper.rawValue
        return stepperField
    }()
    
    private lazy var topStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [installmentsStepperField, interestStepperField])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var conditionsSummaryView: SummaryView = {
        let summaryView = SummaryView()
        summaryView.textLabel.numberOfLines = 0
        summaryView.accessibilityIdentifier = AccessibilityIdentifier.summaryConditions.rawValue
        summaryView.isUserInteractionEnabled = false
        summaryView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tryToSimulateProposalAgain)))
        return summaryView
    }()
    
    private lazy var taxInformationButton: UIButton = {
        let taxInformationButton = UIButton()
        taxInformationButton.setTitle(Strings.Lending.Proposal.Conditions.faqButtonTitle, for: .normal)
        taxInformationButton.buttonStyle(LinkButtonStyle(size: .small))
        taxInformationButton.addTarget(self, action: #selector(taxInformationButtonTapped), for: .touchUpInside)
        taxInformationButton.accessibilityIdentifier = AccessibilityIdentifier.faqButton.rawValue
        taxInformationButton.isEnabled = false
        return taxInformationButton
    }()
    
    private(set) lazy var continueButton: UIButton = {
        let confirmButton = UIButton()
        confirmButton.buttonStyle(PrimaryButtonStyle())
        confirmButton.setTitle(Strings.Lending.Proposal.Conditions.confirmButtonTitle, for: .normal)
        confirmButton.addTarget(self, action: #selector(continueButtonTapped), for: .touchUpInside)
        confirmButton.accessibilityIdentifier = AccessibilityIdentifier.continueButton.rawValue
        return confirmButton
    }()
    
    private lazy var footerStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [conditionsSummaryView, taxInformationButton, continueButton])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [topStackView, footerStackView])
        stackView.spacing = Spacing.base02
        stackView.distribution = .equalSpacing
        stackView.axis = .vertical
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.compatibleLayoutMargins = .rootView
        return stackView
    }()
    
    private lazy var scrollView = UIScrollView()
    
    private lazy var keyboardScrollViewHandler = KeyboardScrollViewHandler(scrollView: scrollView)
    
    private var rootStackViewHeightConstraint: NSLayoutConstraint?

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.fetchConfiguration()
        registerForKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if isMovingFromParent {
            viewModel.sendAnalyticsEvent(type: .backButtonTapped)
        }
    }
 
    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(rootStackView)
    }
    
    override func setupConstraints() {
        scrollView.layout {
            $0.top == view.topAnchor
            $0.trailing == view.trailingAnchor
            $0.bottom == view.bottomAnchor
            $0.leading == view.leadingAnchor
        }
        
        rootStackView.layout {
            $0.top == scrollView.topAnchor
            $0.trailing == scrollView.trailingAnchor
            $0.bottom == scrollView.bottomAnchor
            $0.leading == scrollView.leadingAnchor
            $0.width == scrollView.widthAnchor
            rootStackViewHeightConstraint = $0.height >= view.compatibleSafeAreaLayoutGuide.heightAnchor
        }
    }
    
    override func configureStyles() {
        conditionsSummaryView.textLabel
            .labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale800())
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        view.layoutMargins.bottom = Spacing.base02
        navigationItem.title = Strings.Lending.Proposal.Conditions.title
        navigationItem.rightBarButtonItem = helpButtonItem
    }
    
    private func updateTootStackViewHeightConstraint(constant: CGFloat) {
        rootStackViewHeightConstraint?.constant = constant
        scrollView.setNeedsLayout()
        scrollView.layoutIfNeeded()
    }
}

@objc
extension LendingProposalConditionsViewController {
    // MARK: - Actions
    func installmentsStepperValueChanged(_ stepper: Stepper) {
        viewModel.installmentsValueChanged(stepper.value)
    }
    
    func interestStepperValueChanged(_ stepper: Stepper) {
        viewModel.interestValueChanged(stepper.value)
    }
    
    func continueButtonTapped() {
        viewModel.continueButtonTapped()
    }
    
    func tryToSimulateProposalAgain() {
        viewModel.simulateProposal()
        viewModel.sendAnalyticsEvent(type: .tryAgainTapped)
        conditionsSummaryView.isUserInteractionEnabled = false
    }
    
    func helpButtonTapped() {
        viewModel.showFAQ()
    }
    
    func taxInformationButtonTapped() {
        viewModel.showTaxInformation()
    }
    
    // MARK: - Keyboard management
    func registerForKeyboardNotifications() {
        keyboardScrollViewHandler.registerForKeyboardNotifications()
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        notificationCenter.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    
    func keyboardWillShow(_ notification: Notification) {
        guard
            let keyboardInfo = notification.userInfo,
            let keyboardFrame = keyboardInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect
            else {
                return
        }
        var offsetY = -keyboardFrame.height
        if #available(iOS 11, *) {
            offsetY += view.safeAreaInsets.bottom - Spacing.base02
        }
        updateTootStackViewHeightConstraint(constant: offsetY)
    }
    
    func keyboardWillHide(_ notification: Notification) {
        updateTootStackViewHeightConstraint(constant: 0)
    }
}

// MARK: - View Model Outputs
extension LendingProposalConditionsViewController: LendingProposalConditionsDisplay {
    func setupInstallmentsStepper(minimumValue: Double, maximumValue: Double, suggestedValue: Double, footerText: String?) {
        installmentsStepperField.stepper.minimumValue = minimumValue
        installmentsStepperField.stepper.maximumValue = maximumValue
        installmentsStepperField.stepper.value = suggestedValue
        installmentsStepperField.footerText = footerText
    }
    
    func setupInterestStepper(minimumValue: Double, maximumValue: Double, suggestedValue: Double, footerText: String?) {
        interestStepperField.stepper.minimumValue = minimumValue
        interestStepperField.stepper.maximumValue = maximumValue
        interestStepperField.stepper.value = suggestedValue
        interestStepperField.footerText = footerText
    }
    
    func displayProposalConditionsSummary(_ summaryText: NSAttributedString, allowsInteraction: Bool) {
        conditionsSummaryView.textLabel.attributedText = summaryText
        conditionsSummaryView.isUserInteractionEnabled = allowsInteraction
        taxInformationButton.isEnabled = !allowsInteraction
    }
    
    func startLoadingSummary() {
        conditionsSummaryView.startLoadingAnimation()
    }
    
    func stopLoadingSummary() {
        conditionsSummaryView.stopLoadingAnimation()
    }
    
    func setContinueButtonEnable(state: Bool) {
        continueButton.isEnabled = state
    }
    
    func setTaxInformationButtonEnable(_ isEnable: Bool) {
        taxInformationButton.isEnabled = isEnable
    }
}
