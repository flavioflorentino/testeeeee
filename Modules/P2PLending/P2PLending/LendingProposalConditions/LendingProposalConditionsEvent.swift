import Foundation
import AnalyticsModule

enum LendingProposalConditionsEvent: AnalyticsKeyProtocol {
    case backButtonTapped
    case helpTapped
    case checkProposalTapped
    case tryAgainTapped
    case faqTapped
    
    private var title: String {
        "P2P_LENDING_TOMADOR_ESCOLHA_COMO_PAGAR"
    }
    
    var properties: [String: Any] {
        switch self {
        case .backButtonTapped:
            return [
                AnalyticsKeys.screenName.name: title,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.backButton.description,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        case .helpTapped:
            return [
                AnalyticsKeys.screenName.name: title,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.help.description,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        case .faqTapped:
            return [
                AnalyticsKeys.screenName.name: title,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.knowMoreAboutFees.description,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        case .checkProposalTapped:
            return [
                AnalyticsKeys.screenName.name: title,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.checkProposal.description,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        case .tryAgainTapped:
            return [
                AnalyticsKeys.screenName.name: title,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.tryAgain.description,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(AnalyticInteraction.buttonClicked.name, properties: properties, providers: [.eventTracker])
    }
}
