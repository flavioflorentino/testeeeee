import AnalyticsModule
import Foundation

protocol LendingProposalSummaryViewModelInputs: AnyObject {
    func fetchSummaryInformation()
    func primaryAction()
    func cancelProposal()
    func confirmProposalCancelation()
    func close()
    func showHelp()
    func sendTrackingEvent(type: AnalyticInteraction)
    func showContract()
}

enum ProposalSummaryType {
    case local(proposal: Proposal)
    case remote(offerIdentifier: UUID)
}

final class LendingProposalSummaryViewModel {
    typealias Dependencies = HasAnalytics
    private let presenter: LendingProposalSummaryPresenting
    private let service: LendingProposalSummaryServicing
    private let dependencies: Dependencies
    private var offer: OpportunityOffer?
    
    private var type: ProposalSummaryType

    init(presenter: LendingProposalSummaryPresenting,
         service: LendingProposalSummaryServicing,
         dependencies: Dependencies,
         type: ProposalSummaryType) {
        self.presenter = presenter
        self.service = service
        self.dependencies = dependencies
        self.type = type
    }
    
    private func fetchProposal(identifier: UUID) {
        presenter.startLoading()
        service.fetchProposal(identifier: identifier) { [weak self] result in
            self?.presenter.stopLoading()
            switch result {
            case .success(let offer):
                self?.offer = offer
                self?.presenter.presentSummaryInformation(for: offer)
            case .failure(let error):
                self?.presenter.present(error: error)
            }
        }
    }
    
    private func sendTrackingEvent(_ event: LendingProposalSummaryEvent) {
        dependencies.analytics.log(event)
    }
}

// MARK: - LendingProposalSummaryViewModelInputs
extension LendingProposalSummaryViewModel: LendingProposalSummaryViewModelInputs {
    func fetchSummaryInformation() {
        switch type {
        case .local(let proposal):
            presenter.presentSummaryInformation(for: proposal)
        case .remote(let identifier):
            fetchProposal(identifier: identifier)
        }
    }
    
    func primaryAction() {
        switch type {
        case .local(let proposal):
            sendTrackingEvent(.readContractTapped)
            presenter.didNextStep(action: .ccb(proposal: proposal))
        case .remote(let identifier):
            guard let offer = self.offer, offer.status == .active else { return }
            sendTrackingEvent(.installmentsTapped)
            presenter.didNextStep(action: .showInstallments(offerIdentifier: identifier))
        }
    }
    
    func cancelProposal() {
        presenter.presentCancelationConfirmation()
    }
    
    func confirmProposalCancelation() {
        guard case let .remote(offerIdentifier) = type else {
            return
        }
        presenter.startLoading()
        service.cancelProposal(identifier: offerIdentifier) { [weak self] result in
            self?.presenter.stopLoading()
            switch result {
            case .success:
                self?.close()
                NotificationCenter.default.post(name: .proposalStatusDidChangeNotification, object: offerIdentifier)
            case .failure(let error):
                self?.presenter.present(error: error)
            }
        }
    }
    
    func close() {
        presenter.didNextStep(action: .close)
    }
    
    func showHelp() {
        guard let url = URL(string: "picpay://picpay/helpcenter/article/360049860511") else {
            return
        }
        sendTrackingEvent(.helpTapped)
        presenter.didNextStep(action: .showFAQ(url: url))
    }
    
    func sendTrackingEvent(type: AnalyticInteraction) {
        switch type {
        case .backButtonTapped:
            sendTrackingEvent(.backButtonTapped)
        case .seeMoreTapped:
            sendTrackingEvent(.moreTapped)
        case .seeLessTapped:
            sendTrackingEvent(.lessTapped)
        default:
            break
        }
    }
    
    func showContract() {
        guard let contractLink = offer?.borrower?.contractUrl else { return }
        presenter.presentContract(for: contractLink)
    }
}
