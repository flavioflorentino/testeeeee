import Foundation
import FeatureFlag

protocol LendingProposalSummaryPresenting: AnyObject {
    var viewController: LendingProposalSummaryDisplay? { get set }
    func presentSummaryInformation(for proposal: Proposal)
    func presentSummaryInformation(for offer: OpportunityOffer)
    func presentCancelationConfirmation()
    func startLoading()
    func stopLoading()
    func present(error: Error)
    func presentContract(for url: URL)
    func didNextStep(action: LendingProposalSummaryAction)
}

final class LendingProposalSummaryPresenter {
    private typealias Localizable = Strings.Lending.Proposal.Summary
    typealias Dependencies = HasFeatureManager
    
    private let dependencies: Dependencies
    private let coordinator: LendingProposalSummaryCoordinating
    weak var viewController: LendingProposalSummaryDisplay?
    
    private lazy var percentNumberFormatter: NumberFormatter = {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .percent
        numberFormatter.minimumFractionDigits = 1
        numberFormatter.maximumFractionDigits = 1
        numberFormatter.locale = Locale(identifier: "pt_BR")
        return numberFormatter
    }()
    
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "pt_BR")
        dateFormatter.dateStyle = .short
        return dateFormatter
    }()
    
    init(coordinator: LendingProposalSummaryCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
    
    private func percentFormatted(value: Float?) -> String {
        guard
            let value = value,
            let percentFormattedValue = percentNumberFormatter.string(from: value / 100 as NSNumber)
            else {
                return ""
        }
        return percentFormattedValue
    }
    
    private func formattedFirstPaymentDate(for date: Date?) -> String? {
        guard let date = date else {
            return nil
        }
        return dateFormatter.string(from: date)
    }
    
    private func shouldShowCcbContract(for offer: OpportunityOffer) -> Bool {
        let status: [OpportunityOfferStatus] = [.accepted, .processingPayment, .active, .delayedPayment, .counterOffer, .liquidated]
        return status.contains(offer.status)
    }
    
    private func continueButtonTitle(for offer: OpportunityOffer) -> String? {
        guard offer.status == .active, dependencies.featureManager.isActive(.isP2PLendingBorrowerInstallmentsAvailable) else { return nil }
        return Localizable.installmentsButtonTitle
    }
}

// MARK: - LendingProposalSummaryPresenting
extension LendingProposalSummaryPresenter: LendingProposalSummaryPresenting {
    func presentSummaryInformation(for proposal: Proposal) {
        let installmentsAmountValue = proposal.simulation?.installmentAmount.toCurrencyString() ?? ""
        let summary = ProposalSummary(
            status: nil,
            requestedAmount: proposal.amount.toCurrencyString(),
            friendName: proposal.friend?.name,
            installments: Localizable.installmentsItemValue(proposal.installments ?? 0, installmentsAmountValue),
            totalAmount: proposal.simulation?.totalInterest.toCurrencyString(),
            paymentDateNotice: formattedFirstPaymentDate(for: proposal.simulation?.firstPaymentDate),
            cet: Localizable.cetItemValue(percentFormatted(value: proposal.simulation?.monthCet)),
            interestRate: Localizable.interestRateItemValue(percentFormatted(value: proposal.interest)),
            iof: proposal.simulation?.iof.toCurrencyString(),
            commissionAmount: Localizable.commissionAmount(proposal.simulation?.commissionAmount.toCurrencyString() ?? "",
                                                           proposal.installments ?? 0),
            continueButtonTitle: Localizable.continueButtonTitle,
            showsCancelButton: false,
            showsStatusView: false,
            showsFirstPaymentDate: true,
            showsCcbContract: false
        )
        viewController?.display(proposalSummary: summary)
    }
    
    func presentSummaryInformation(for offer: OpportunityOffer) {
        let installmentsAmountValue = offer.operationDetails.borrowerInstallmentsAmount.toCurrencyString() ?? ""
        let summary = ProposalSummary(
            status: OpportunityOfferStatusViewModel(status: offer.status, agent: .borrower),
            requestedAmount: offer.operationDetails.amount.toCurrencyString(),
            friendName: offer.investor?.name,
            installments: Localizable.installmentsItemValue(offer.operationDetails.installments, installmentsAmountValue),
            totalAmount: offer.operationDetails.totalInterest.toCurrencyString(),
            paymentDateNotice: formattedFirstPaymentDate(for: offer.operationDetails.firstPaymentDate),
            cet: Localizable.cetItemValue(percentFormatted(value: offer.operationDetails.cet)),
            interestRate: Localizable.interestRateItemValue(percentFormatted(value: offer.operationDetails.monthRate)),
            iof: offer.operationDetails.iof.toCurrencyString(),
            commissionAmount: Localizable.commissionAmount(offer.operationDetails.commissionAmount.toCurrencyString() ?? "",
                                                           offer.operationDetails.installments),
            continueButtonTitle: continueButtonTitle(for: offer),
            showsCancelButton: offer.status == .waitingReply,
            showsStatusView: true,
            showsFirstPaymentDate: OpportunityOfferStatus.inProgress.contains(offer.status),
            showsCcbContract: shouldShowCcbContract(for: offer)
        )
        viewController?.display(proposalSummary: summary)
    }
    
    func presentCancelationConfirmation() {
        viewController?.displayCancelationConfirmation()
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
    
    func present(error: Error) {
        viewController?.display(error: error)
    }
    
    func presentContract(for url: URL) {
        let lendingSessionWebPage = LendingSessionWebPage(title: Localizable.ccbTitle, url: url)
        coordinator.perform(action: .contract(webPage: lendingSessionWebPage))
    }
    
    func didNextStep(action: LendingProposalSummaryAction) {
        coordinator.perform(action: action)
    }
}
