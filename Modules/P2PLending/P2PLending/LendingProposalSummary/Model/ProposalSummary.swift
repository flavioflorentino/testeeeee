import UI
import Foundation

struct ProposalSummary: Equatable {
    let status: OpportunityOfferStatusViewModel?
    let requestedAmount: String?
    let friendName: String?
    let installments: String?
    let totalAmount: String?
    let paymentDateNotice: String?
    let cet: String?
    let interestRate: String?
    let iof: String?
    let commissionAmount: String?
    let continueButtonTitle: String?
    let showsCancelButton: Bool
    let showsStatusView: Bool
    let showsFirstPaymentDate: Bool
    let showsCcbContract: Bool
    
    var ccbNotice: NSAttributedString {
        let ccbNotice = NSMutableAttributedString(
            string: Strings.Lending.Proposal.Summary.ccbNotice,
            attributes: secondaryBodyAttributes
        )
        let ccbNoticeHighlight = NSAttributedString(
            string: Strings.Lending.Proposal.Summary.ccbNoticeHighlight,
            attributes: linkAttributes
        )
        ccbNotice.append(ccbNoticeHighlight)
        return ccbNotice
    }
    
    private var secondaryBodyAttributes: [NSAttributedString.Key: Any] {
        [
            .font: Typography.bodySecondary().font(),
            .foregroundColor: Colors.grayscale600.color
        ]
    }
    
    private var linkAttributes: [NSAttributedString.Key: Any] {
        [
            .link: "",
            .font: Typography.bodySecondary().font(),
            .foregroundColor: Colors.branding400.color,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
    }
}
