import UIKit
import UI

protocol LendingProposalSummaryDisplay: AnyObject {
    func display(proposalSummary: ProposalSummary)
    func displayCancelationConfirmation()
    func display(error: Error)
    func startLoading()
    func stopLoading()
}

final class LendingProposalSummaryViewController: ViewController<LendingProposalSummaryViewModelInputs, UIView> {
    enum AccessibilityIdentifier: String {
        case expandableViewButtonToggle
        case individualCostLabel
        case continueButton
        case cancelButton
        case closeButton
    }
    
    private typealias Localizable = Strings.Lending.Proposal.Summary
    
    private lazy var closeButtonItem: UIBarButtonItem = {
        let buttonItem = UIBarButtonItem(title: GlobalLocalizable.closeButtonTitle, style: .plain, target: self, action: #selector(closeButtonTapped))
        buttonItem.accessibilityIdentifier = AccessibilityIdentifier.closeButton.rawValue
        return buttonItem
    }()
    
    private lazy var statusView = StatusView()
    
    private lazy var activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(style: .gray)
        activityIndicatorView.color = Colors.grayscale300.color
        return activityIndicatorView
    }()
    
    private lazy var requestAmountItem: SummaryItemView = {
        let item = SummaryItemView()
        item.titleLabel.text = Localizable.requestedAmountItemTitle
        return item
    }()
    
    private lazy var friendItem: SummaryItemView = {
        let item = SummaryItemView()
        item.titleLabel.text = Localizable.friendItemTitle
        return item
    }()
    
    private lazy var installmentsItem: SummaryItemView = {
        let item = SummaryItemView()
        item.titleLabel.text = Localizable.installmentsItemTitle
        return item
    }()
    
    private lazy var totalAmountItem: SummaryItemView = {
        let item = SummaryItemView()
        item.titleLabel.text = Localizable.totalAmountItemTitle
        return item
    }()
    
    private lazy var paymentDateItem: SummaryItemView = {
        let item = SummaryItemView()
        item.titleLabel.text = Localizable.initialPaymentDateItemTitle
        return item
    }()
    
    private lazy var ccbTextView: UITextView = {
        let textView = TextView()
        textView.isScrollEnabled = false
        textView.isEditable = false
        textView.dataDetectorTypes = [.link]
        textView.delegate = self
        textView.backgroundColor = .clear
        return textView
    }()
    
    // MARK: Expandable area
    
    private lazy var cetItem: SummaryItemView = {
        let item = SummaryItemView()
        item.titleLabel.text = Localizable.cetItemTitle
        return item
    }()
    
    private lazy var individualCostsLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.individualCostsTitle
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale600())
        label.accessibilityIdentifier = AccessibilityIdentifier.individualCostLabel.rawValue
        return label
    }()
    
    private lazy var interestRateItem: SummaryItemView = {
        let item = SummaryItemView()
        item.titleLabel.labelStyle(CaptionLabelStyle())
        item.titleLabel.text = Localizable.interestRateItemTitle
        return item
    }()
    
    private lazy var iofItem: SummaryItemView = {
        let item = SummaryItemView()
        item.titleLabel.labelStyle(CaptionLabelStyle())
        item.titleLabel.text = Localizable.iofItemTitle
        return item
    }()
    
    private lazy var comissionAmountItem: SummaryItemView = {
        let item = SummaryItemView()
        item.titleLabel.labelStyle(CaptionLabelStyle())
        item.titleLabel.text = Localizable.administrativeInterstItemTitle
        return item
    }()
    
    private lazy var individualCostsStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [individualCostsLabel, interestRateItem, iofItem, comissionAmountItem])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var expandableView: ExpandableView = {
        let expandableView = ExpandableView()
        expandableView.expandButton.accessibilityIdentifier = AccessibilityIdentifier.expandableViewButtonToggle.rawValue
        expandableView.delegate = self
        return expandableView
    }()
    
    private lazy var continueButton: UIButton = {
        let continueButton = UIButton()
        continueButton.buttonStyle(PrimaryButtonStyle())
        continueButton.setTitle(Localizable.continueButtonTitle, for: .normal)
        continueButton.addTarget(self, action: #selector(continueButtonTapped), for: .touchUpInside)
        continueButton.accessibilityIdentifier = AccessibilityIdentifier.continueButton.rawValue
        return continueButton
    }()
    
    private lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.cancelProposalButtonTitle, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(cancelButtonTapped), for: .touchUpInside)
        button.accessibilityIdentifier = AccessibilityIdentifier.cancelButton.rawValue
        return button
    }()
    
    private lazy var helpButton: UIBarButtonItem = {
        let helpButton = UIBarButtonItem(image: Assets.lendingGlyphHelp.image,
                                         style: .plain,
                                         target: self,
                                         action: #selector(hepButtonTapped))
        helpButton.accessibilityLabel = GlobalLocalizable.helpButtonAccessibilityLabel
        return helpButton
    }()
    
    private lazy var rootStackView: UIStackView = {
        let items = [
            statusView,
            requestAmountItem,
            friendItem,
            installmentsItem,
            totalAmountItem,
            paymentDateItem,
            expandableView,
            cancelButton,
            ccbTextView,
            continueButton
        ]
        let stackView = UIStackView(arrangedSubviews: items)
        stackView.axis = .vertical
        stackView.spacing = Spacing.base03
        stackView.compatibleLayoutMargins = .rootView
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    
    private lazy var scrollView = UIScrollView()
    
    var shouldDisplayCloseButton = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.fetchSummaryInformation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel.sendTrackingEvent(type: .backButtonTapped)
    }

    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        view.addSubview(activityIndicatorView)
        scrollView.addSubview(rootStackView)
        expandableView.setHeaderView(cetItem)
        expandableView.setContentView(individualCostsStackView)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        rootStackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.width.equalToSuperview()
        }
        
        activityIndicatorView.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        view.tintColor = Colors.branding400.color
        navigationItem.title = Localizable.title
        if shouldDisplayCloseButton {
            navigationItem.leftBarButtonItem = closeButtonItem
        }
        navigationItem.rightBarButtonItem = helpButton
        setupNavigationAppearance()
    }
    
    private func setupNavigationAppearance() {
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .automatic
            navigationController?.navigationBar.prefersLargeTitles = true
            extendedLayoutIncludesOpaqueBars = true
        }
        if #available(iOS 13.0, *) {
            let navigationBarAppearance = UINavigationBarAppearance()
            navigationBarAppearance.configureWithTransparentBackground()
            navigationBarAppearance.backgroundColor = Colors.backgroundPrimary.color
            navigationBarAppearance.shadowColor = .clear
            navigationController?.navigationBar.scrollEdgeAppearance = navigationBarAppearance
            navigationController?.navigationBar.standardAppearance = navigationBarAppearance
        }
    }
}

extension LendingProposalSummaryViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if textView == ccbTextView {
            interactor.showContract()
        }
        return false
    }
}

// MARK: Actions
@objc
extension LendingProposalSummaryViewController {
    func continueButtonTapped() {
        viewModel.primaryAction()
    }
    
    func cancelButtonTapped() {
        viewModel.cancelProposal()
    }
    
    func closeButtonTapped() {
        viewModel.close()
    }
    
    func hepButtonTapped() {
        viewModel.showHelp()
    }
}

// MARK: LendingProposalSummaryDisplay
extension LendingProposalSummaryViewController: LendingProposalSummaryDisplay {
    func display(proposalSummary: ProposalSummary) {
        statusView.statusDisplayModel = proposalSummary.status
        requestAmountItem.valueLabel.text = proposalSummary.requestedAmount
        friendItem.valueLabel.text = proposalSummary.friendName
        installmentsItem.valueLabel.text = proposalSummary.installments
        totalAmountItem.valueLabel.text = proposalSummary.totalAmount
        paymentDateItem.valueLabel.text = proposalSummary.paymentDateNotice
        cetItem.valueLabel.text = proposalSummary.cet
        interestRateItem.valueLabel.text = proposalSummary.interestRate
        iofItem.valueLabel.text = proposalSummary.iof
        comissionAmountItem.valueLabel.text = proposalSummary.commissionAmount
        ccbTextView.attributedText = proposalSummary.ccbNotice
        statusView.isHidden = !proposalSummary.showsStatusView
        continueButton.setTitle(proposalSummary.continueButtonTitle, for: .normal)
        continueButton.isHidden = proposalSummary.continueButtonTitle == nil
        cancelButton.isHidden = !proposalSummary.showsCancelButton
        paymentDateItem.isHidden = !proposalSummary.showsFirstPaymentDate
        ccbTextView.isHidden = !proposalSummary.showsCcbContract
    }
    
    func displayCancelationConfirmation() {
        let contentView = DeclineOfferDialogueContentView(
            title: Localizable.cancelationConfirmationAlertTitle,
            body: Localizable.cancelationConfirmationAlertMessage
        )
        let alert = PopupViewController(title: nil, preferredType: .customView(contentView, shouldRemoveCloseButton: true))
        let confirmAction = PopupAction(title: Localizable.cancelationConfirmationAlertConfirmButtonTitle, style: .destructive) {
            self.interactor.confirmProposalCancelation()
        }
        let dismissAction = PopupAction(title: Localizable.cancelationConfirmationAlertDismissButtonTitle, style: .link)
        
        alert.addAction(confirmAction)
        alert.addAction(dismissAction)
        
        showPopup(alert)
    }
    
    func display(error: Error) {
        let popUpViewController = PopupViewController(
            title: GlobalLocalizable.errorAlertTitle,
            description: error.localizedDescription
        )
        let dismissAction = PopupAction(title: GlobalLocalizable.dismissButtonTitle, style: .default)
        popUpViewController.addAction(dismissAction)
        showPopup(popUpViewController)
    }
    
    func startLoading() {
        scrollView.alpha = 0
        activityIndicatorView.startAnimating()
    }
    
    func stopLoading() {
        scrollView.alpha = 1
        activityIndicatorView.stopAnimating()
    }
}

// MARK: ExpandableViewDelegate
extension LendingProposalSummaryViewController: ExpandableViewDelegate {
    func expandableViewDidExpand(_ expandableView: ExpandableView) {
        interactor.sendTrackingEvent(type: .seeMoreTapped)
    }
    
    func expandableViewDidCollapse(_ expandableView: ExpandableView) {
        interactor.sendTrackingEvent(type: .seeLessTapped)
    }
}
