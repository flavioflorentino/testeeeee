import AnalyticsModule
import Foundation

enum LendingProposalSummaryEvent: AnalyticsKeyProtocol {
    case backButtonTapped
    case helpTapped
    case readContractTapped
    case moreTapped
    case lessTapped
    case installmentsTapped
    
    private var title: String {
        "P2P_LENDING_TOMADOR_RESUMO_DA_PROPOSTA"
    }
    
    var properties: [String: Any] {
        switch self {
        case .backButtonTapped:
            return [
                AnalyticsKeys.screenName.name: title,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.backButton.description
            ]
        case .helpTapped:
            return [
                AnalyticsKeys.screenName.name: title,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.help.description
            ]
            
        case .readContractTapped:
            return [
                AnalyticsKeys.screenName.name: title,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.readContract.description
            ]
        case .moreTapped:
            return [
                AnalyticsKeys.screenName.name: title,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.moreDetails.description
            ]
        case .lessTapped:
            return [
                AnalyticsKeys.screenName.name: title,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.seeLess.description
            ]
        case .installmentsTapped:
            return [
                AnalyticsKeys.screenName.name: title,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.openInstallments.description
            ]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(AnalyticInteraction.buttonClicked.name, properties: properties, providers: [.eventTracker])
    }
}
