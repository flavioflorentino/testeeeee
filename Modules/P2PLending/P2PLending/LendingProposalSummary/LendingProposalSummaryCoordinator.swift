import UIKit

enum LendingProposalSummaryAction {
    case ccb(proposal: Proposal)
    case showInstallments(offerIdentifier: UUID)
    case showFAQ(url: URL)
    case close
    case contract(webPage: LendingSessionWebPage)
}

protocol LendingProposalSummaryCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: LendingProposalSummaryAction)
}

final class LendingProposalSummaryCoordinator: LendingProposalSummaryCoordinating, URLOpenerCoordinating {
    typealias Dependencies = HasURLOpener
    
    weak var viewController: UIViewController?
    private var dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func perform(action: LendingProposalSummaryAction) {
        switch action {
        case .ccb(let proposal):
            let controller = LendingCCBFactory.make(proposal: proposal)
            viewController?.show(controller, sender: nil)
        case .showInstallments(let offerIdentifier):
            let controller = LendingInstallmentListFactory.make(proposalIdentifier: offerIdentifier, agent: .borrower)
            viewController?.show(controller, sender: nil)
        case .showFAQ(let url):
            showFAQ(for: url, using: dependencies)
        case .close:
            viewController?.dismiss(animated: true)
        case .contract(let webPage):
            let contractViewController = UINavigationController(rootViewController: LendingSessionWebViewFactory.make(for: webPage))
            viewController?.present(contractViewController, animated: true, completion: nil)
        }
    }
}
