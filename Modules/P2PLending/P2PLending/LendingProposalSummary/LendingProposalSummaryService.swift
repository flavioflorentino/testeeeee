import Core

protocol LendingProposalSummaryServicing: Servicing {
    func fetchProposal(identifier: UUID, completion: @escaping ModelCompletionBlock<OpportunityOffer>)
    func cancelProposal(identifier: UUID, completion: @escaping ModelCompletionBlock<NoContent>)
}

final class LendingProposalSummaryService: LendingProposalSummaryServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    private lazy var decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        return decoder
    }()
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func fetchProposal(identifier: UUID, completion: @escaping ModelCompletionBlock<OpportunityOffer>) {
        let api = Api<OpportunityOffer>(endpoint: P2PLendingEndpoint.opportunityOffer(identifier: identifier))
        api.shouldUseDefaultDateFormatter = false
        api.execute(jsonDecoder: decoder) { [weak self] result in
            let mappedResult = result.map(\.model)
            self?.dependencies.mainQueue.async {
                completion(mappedResult)
            }
        }
    }
    
    func cancelProposal(identifier: UUID, completion: @escaping ModelCompletionBlock<NoContent>) {
        let api = Api<NoContent>(endpoint: P2PLendingEndpoint.cancelOffer(identifier: identifier))
        api.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
