import Foundation

enum LendingProposalSummaryFactory {
    static func make(type: ProposalSummaryType, shouldDisplayCloseButton: Bool = false) -> LendingProposalSummaryViewController {
        let container = DependencyContainer()
        let coordinator: LendingProposalSummaryCoordinating = LendingProposalSummaryCoordinator(dependencies: container)
        let presenter: LendingProposalSummaryPresenting = LendingProposalSummaryPresenter(coordinator: coordinator, dependencies: container)
        let service: LendingProposalSummaryServicing = LendingProposalSummaryService(dependencies: DependencyContainer())
        let viewModel = LendingProposalSummaryViewModel(presenter: presenter, service: service, dependencies: container, type: type)
        let viewController = LendingProposalSummaryViewController(viewModel: viewModel)

        viewController.shouldDisplayCloseButton = shouldDisplayCloseButton
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
