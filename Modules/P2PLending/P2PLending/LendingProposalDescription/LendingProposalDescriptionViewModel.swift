import Foundation
import AnalyticsModule

protocol LendingProposalDescriptionViewModelInputs: AnyObject {
    func fetchPeopleInformation()
    func continueToProposalAmount(message: String?)
    func sendAnalyticsEvent(_ event: AnalyticInteraction)
}

final class LendingProposalDescriptionViewModel {
    typealias Dependencies = HasAnalytics
    private let presenter: LendingProposalDescriptionPresenting
    private var dependencies: Dependencies
    
    private var proposal: Proposal

    init(presenter: LendingProposalDescriptionPresenting, proposal: Proposal, dependencies: Dependencies) {
        self.presenter = presenter
        self.proposal = proposal
        self.dependencies = dependencies
    }
    
    private func sendAnalyticsEvent(_ event: LendingProposalDescriptionEvent) {
        dependencies.analytics.log(event)
    }
}

extension LendingProposalDescriptionViewModel: LendingProposalDescriptionViewModelInputs {
    func fetchPeopleInformation() {
        guard let friend = proposal.friend, let objective = proposal.objective else {
            return
        }
        presenter.present(friend: friend)
        presenter.present(objective: objective)
    }
    
    func continueToProposalAmount(message: String?) {
        proposal.message = message
        if let message = message, !message.isEmpty {
            sendAnalyticsEvent(.didWriteMessage)
        }
        sendAnalyticsEvent(.makeProposalTapped)
        presenter.didNextStep(action: .proposalAmount(proposal: proposal))
    }
    
    func sendAnalyticsEvent(_ event: AnalyticInteraction) {
        switch event {
        case .textViewDidBeginEditing:
            sendAnalyticsEvent(.textViewdAccessed)
        case .backButtonTapped:
            sendAnalyticsEvent(.backButton)
        default:
            break
        }
    }
}
