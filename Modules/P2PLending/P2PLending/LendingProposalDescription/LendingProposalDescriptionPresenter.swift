import Core
import Foundation

protocol LendingProposalDescriptionPresenting: AnyObject {
    var viewController: LendingProposalDescriptionDisplay? { get set }
    func present(friend: LendingFriend)
    func present(objective: ProposalObjective)
    func didNextStep(action: LendingProposalDescriptionAction)
}

final class LendingProposalDescriptionPresenter: LendingProposalDescriptionPresenting {
    private let coordinator: LendingProposalDescriptionCoordinating
    weak var viewController: LendingProposalDescriptionDisplay?

    init(coordinator: LendingProposalDescriptionCoordinating) {
        self.coordinator = coordinator
    }
    
    func present(friend: LendingFriend) {
        viewController?.display(friend: friend)
    }
    
    func present(objective: ProposalObjective) {
        viewController?.display(objective: objective)
    }
    
    func didNextStep(action: LendingProposalDescriptionAction) {
        coordinator.perform(action: action)
    }
}
