import Foundation
import AnalyticsModule

enum LendingProposalDescriptionEvent: AnalyticsKeyProtocol {
    case backButton
    case textViewdAccessed
    case didWriteMessage
    case makeProposalTapped
    
    private var title: String {
        "P2P_LENDING_TOMADOR_CONTE_PARA_SEU_AMIGO"
    }
    
    var name: String {
        switch self {
        case .backButton, .makeProposalTapped:
            return AnalyticInteraction.buttonClicked.name
        case .textViewdAccessed:
            return AnalyticInteraction.textBoxClicked.name
        case .didWriteMessage:
            return AnalyticInteraction.textBoxFilled.name
        }
    }
    
    var properties: [String: Any] {
        switch self {
        case .backButton:
            return [
                AnalyticsKeys.screenName.name: title,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.backButton.description,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        case .textViewdAccessed, .didWriteMessage:
            return [
                AnalyticsKeys.screenName.name: title,
                AnalyticsKeys.textBoxName.name: AnalyticsConstants.textBoxNameProposalDescription.description,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        case .makeProposalTapped:
            return [
                AnalyticsKeys.screenName.name: title,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.makeProposal.description,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.eventTracker])
    }
}
