import UI
import UIKit

protocol LendingProposalDescriptionDisplay: AnyObject {
    func display(friend: LendingFriend)
    func display(objective: ObjectiveBadgeViewModel)
}

final class LendingProposalDescriptionViewController: ViewController<LendingProposalDescriptionViewModelInputs, UIView> {
    fileprivate enum Layout {}
    
    enum AccessibilityIdentifier: String {
        case textView
        case continueButton
    }
    
    private lazy var lendingFriendView = LendingFriendView()
    
    private lazy var objectiveBadge = ObjectiveBadge()
    
    private(set) lazy var textView: UITextView = {
        let textView = TextView()
        textView.placeholderText = Strings.Lending.Proposal.Description.textViewPlaceholder
        textView.isScrollEnabled = false
        textView.font = Typography.bodyPrimary().font()
        textView.tintColor = Colors.branding300.color
        textView.backgroundColor = .clear
        textView.accessibilityIdentifier = AccessibilityIdentifier.textView.rawValue
        textView.delegate = self
        return textView
    }()
    
    private lazy var descriptionStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [objectiveBadge, SeparatorView(), textView])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var headerStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [lendingFriendView, descriptionStackView])
        stackView.spacing = Spacing.base03
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var privacyImageView: UIImageView = {
        let imageView = UIImageView(image: Assets.lendingGlyphLock.image)
        imageView.tintColor = Colors.grayscale300.color
        imageView.setContentHuggingPriority(.required, for: .horizontal)
        return imageView
    }()
    
    private lazy var privacyLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.Lending.Proposal.Description.privacyLevelText
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale300())
        return label
    }()
    
    private lazy var privacyStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [privacyImageView, privacyLabel])
        stackView.spacing = Spacing.base01
        stackView.alignment = .center
        return stackView
    }()
    
    private(set) lazy var continueButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.Lending.Proposal.Description.continueButtonTitle, for: .normal)
        button.addTarget(self, action: #selector(continueButtonTapped), for: .touchUpInside)
        button.buttonStyle(PrimaryButtonStyle())
        button.accessibilityIdentifier = AccessibilityIdentifier.continueButton.rawValue
        return button
    }()
    
    private lazy var footerStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [privacyStackView, continueButton])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [headerStackView, footerStackView])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        stackView.distribution = .equalSpacing
        stackView.compatibleLayoutMargins = .rootView
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.keyboardDismissMode = .onDrag
        return scrollView
    }()
    
    private lazy var masker = TextViewMasker(textMask: CharacterLimitMask(maximumNumberOfCharacters: 100))
    
    private lazy var keyboardScrollViewHandler = KeyboardScrollViewHandler(scrollView: scrollView)
    private var rootStackViewHeightConstraint: NSLayoutConstraint?

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.fetchPeopleInformation()
        registerForKeyboardNotifications()
        masker.bind(to: textView)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if isMovingFromParent {
            viewModel.sendAnalyticsEvent(.backButtonTapped)
        }
    }

    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(rootStackView)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.directionalEdges.equalToSuperview()
        }
        
        rootStackView.snp.makeConstraints {
            $0.directionalEdges.equalToSuperview()
            $0.width.equalToSuperview()
        }
        
        rootStackView.layout {
            rootStackViewHeightConstraint = $0.height >= view.compatibleSafeAreaLayoutGuide.heightAnchor
        }
    }

    override func configureViews() {
        navigationItem.title = Strings.Lending.Proposal.Description.title
        view.layoutMargins.bottom = Spacing.base02
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    private func updateTootStackViewHeightConstraint(constant: CGFloat) {
        rootStackViewHeightConstraint?.constant = constant
        scrollView.setNeedsLayout()
        scrollView.layoutIfNeeded()
    }
}

@objc
extension LendingProposalDescriptionViewController {
    func continueButtonTapped() {
        textView.resignFirstResponder()
        viewModel.continueToProposalAmount(message: textView.text)
    }
    
    func registerForKeyboardNotifications() {
        keyboardScrollViewHandler.registerForKeyboardNotifications()
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        notificationCenter.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    
    func keyboardWillShow(_ notification: Notification) {
        let keyboardInfo = notification.userInfo
        guard let keyboardFrame = keyboardInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect else {
            return
        }
        var offsetY = -keyboardFrame.height
        if #available(iOS 11, *) {
            offsetY += view.safeAreaInsets.bottom - Spacing.base02
        }
        updateTootStackViewHeightConstraint(constant: offsetY)
    }
    
    func keyboardWillHide(_ notification: Notification) {
        updateTootStackViewHeightConstraint(constant: 0)
    }
}

// MARK: TextView delegate
extension LendingProposalDescriptionViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        viewModel.sendAnalyticsEvent(.textViewDidBeginEditing)
    }
}

// MARK: View Model Outputs
extension LendingProposalDescriptionViewController: LendingProposalDescriptionDisplay {
    func display(friend: LendingFriend) {
        lendingFriendView.setup(with: friend)
    }
    
    func display(objective: ObjectiveBadgeViewModel) {
        objectiveBadge.setup(for: objective)
    }
}
