import Foundation

final class LendingProposalDescriptionFactory {
    static func make(data: GreetingsResponse, proposal: Proposal) -> LendingProposalDescriptionViewController {
        let container = DependencyContainer()
        let coordinator: LendingProposalDescriptionCoordinating = LendingProposalDescriptionCoordinator(data: data)
        let presenter: LendingProposalDescriptionPresenting = LendingProposalDescriptionPresenter(coordinator: coordinator)
        let viewModel = LendingProposalDescriptionViewModel(presenter: presenter, proposal: proposal, dependencies: container)
        let viewController = LendingProposalDescriptionViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
