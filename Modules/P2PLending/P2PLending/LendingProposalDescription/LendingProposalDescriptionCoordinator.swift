import UIKit

enum LendingProposalDescriptionAction {
    case proposalAmount(proposal: Proposal)
}

protocol LendingProposalDescriptionCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: LendingProposalDescriptionAction)
}

final class LendingProposalDescriptionCoordinator: LendingProposalDescriptionCoordinating {
    weak var viewController: UIViewController?
    
    private let data: GreetingsResponse
    
    init(data: GreetingsResponse) {
        self.data = data
    }
    
    func perform(action: LendingProposalDescriptionAction) {
        guard
            case .proposalAmount(let proposal) = action,
            let controller = LendingProposalAmountFactory.make(data: data, proposal: proposal)
            else {
                return
        }
        viewController?.show(controller, sender: nil)
    }
}
