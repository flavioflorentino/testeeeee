import Foundation
import AnalyticsModule

protocol AnalyticsEventInteractionProvider {
    func analyticsEvent(for interaction: AnalyticInteraction) -> AnalyticsEvent
}
