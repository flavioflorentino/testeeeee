import Foundation

enum AnalyticInteraction {
    case searchBarTextDidBeginEditing
    case textViewDidBeginEditing
    case backButtonTapped
    case tryAgainTapped
    case seeMoreTapped
    case seeLessTapped
    case didScrollToBottom
    case dismissed
    case faq
    case buttonClicked
    case screenViewed
    case textBoxClicked
    case textBoxFilled
    case documentRead
    case pushNotificationOpened
    case buttonSelected
    case dialogOptionSelected
    case pushNotification
    case deeplink
    case tabTapped
    case laterTapped
    
    var name: String {
        switch self {
        case .buttonClicked:
            return "Button Clicked"
        case .screenViewed:
            return "Screen Viewed"
        case .textBoxClicked:
            return "Text Box Clicked"
        case .textBoxFilled:
            return "Text Box Filled"
        case .documentRead:
            return "Document Read"
        case .pushNotificationOpened:
            return "Push Notification Opened"
        case .buttonSelected:
            return "Button Selected"
        case .dialogOptionSelected:
            return "Dialog Option Selected"
        case .pushNotification:
            return "Push Notification Opened"
        case .tabTapped:
            return "Tab Clicked"
        case .searchBarTextDidBeginEditing,
             .textViewDidBeginEditing,
             .backButtonTapped,
             .tryAgainTapped,
             .seeMoreTapped,
             .seeLessTapped,
             .didScrollToBottom,
             .dismissed,
             .faq,
             .deeplink,
             .laterTapped:
            return ""
        }
    }
}

enum AnalyticsKeys: String {
    case screenName = "screen_name"
    case buttonName = "button_name"
    case context = "business_context"
    case tabName = "tab_name"
    case textBoxName = "text_box_name"
    case documentType = "document_type"
    case documentDescription = "document_description"
    case pushType = "push_type"
    case notificationOpenFrom = "notification_open_from"
    case notificationId = "notification_id"
    case dialogName = "dialog_name"
    case optionSelected = "option_selected"
    
    var name: String {
        rawValue
    }
}

enum AnalyticsConstants: String {
    case okButton = "OK"
    case seeMoreButton = "SABER_MAIS"
    case accountUpgrade = "UPGRADE_CONTA"
    case chooseSomebodyElse = "ESCOLHER_OUTRA_PESSOA"
    case termsOfUse = "TERMOS_DE_USO"
    case privacyPolicy = "POLITICA_DE_PRIVACIDADE"
    case backButton = "VOLTAR"
    case checkBoxMarked = "LI_E_CONCORDO"
    case help = "AJUDA"
    case crednovo = "CREDNOVO"
    case checkProposal = "CONFERIR_PROPOSTA"
    case p2pLending = "P2P_LENDING"
    case textBoxNameSearch = "PROCURE_POR_ALGUEM"
    case makeProposal = "FAZER_PROPOSTA"
    case textBoxNameProposalDescription = "ESCREVA_OBJETIVO_EMPRESTIMO"
    case choosePayment = "ESCOLHER_PAGAMENTO"
    case knowMoreAboutFees = "SABER_MAIS_SOBRE_TAXAS"
    case tryAgain = "TENTAR_NOVAMENTE"
    case readContract = "LER_CONTRATO"
    case moreDetails = "MAIS_DETALHES"
    case seeLess = "VER_MENOS"
    case sendProposal = "ASSINAR_CONTRATO"
    case contract = "CONTRATO"
    case borrowerContract = "CONTRATO_TOMADOR_P2P_LENDING"
    case push = "PUSH | TELA_NOTIFICACAO"
    case cv = "CERTIFICADO_DE_VINCULACAO"
    case acceptProposal = "ACEITAR_PROPOSTA"
    case declineProposal = "RECUSAR"
    case proposalDeclined = "PROPOSTA_RECUSADA"
    case declineSelected = "RECUSAR_PROPOSTA"
    case laterButton = "FAZER_DEPOIS"
    case addToWallet = "ADICIONAR_DINHEIRO"
    case faqAccountLimits = "SABER_MAIS_SOBRE_LIMITE_DE_CONTAS"
    case openInstallments = "CONFERIR_PARCELAS"
    case finishedProposalTab = "ENCERRADOS"
    case inProgressProposalTab = "EM_ANDAMENTO"
    case faq = "FAQ"
    case openInstallmentsTab = "ABERTAS"
    case paidInstallmentsTab = "PAGAS"
    case receivedInstallmentsTab = "RECEBIDAS"
    case dismissButton = "OK_ENTENDI"
    case getInContact = "ENTRAR_EM_CONTATO"
    case newProposal = "PEDIR_EMPRESTADO"
    case myProposals = "MEUS_PEDIDOS"
    case myInvestments = "MEUS_INVESTIMENTOS"
    case cancelAProposal = "CANCELAR_UM_PEDIDO"
    case notNowButton = "AGORA_NAO"
    case startButton = "COMECAR"
    
    var description: String {
        rawValue
    }
}

enum InteractionType {
    case primaryAction
    case linkAction
    case screenViewed
}
