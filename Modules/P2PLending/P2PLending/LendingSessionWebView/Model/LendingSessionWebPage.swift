import Foundation

struct LendingSessionWebPage: Equatable {
    let title: String
    let url: URL
}
