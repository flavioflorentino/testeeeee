import UIKit

enum LendingSessionWebViewFactory {
    static func make(for webPage: LendingSessionWebPage) -> LendingSessionWebViewViewController {
        let container = DependencyContainer()
        let coordinator: LendingSessionWebViewCoordinating = LendingSessionWebViewCoordinator()
        let presenter: LendingSessionWebViewPresenting = LendingSessionWebViewPresenter(coordinator: coordinator)
        let interactor = LendingSessionWebViewInteractor(presenter: presenter, webPage: webPage, dependencies: container)
        let viewController = LendingSessionWebViewViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
