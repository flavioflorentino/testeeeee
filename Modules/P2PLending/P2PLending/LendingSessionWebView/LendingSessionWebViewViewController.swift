import UI
import UIKit
import WebKit

protocol LendingSessionWebViewDisplaying: AnyObject {
    func displayWebPage(for request: URLRequest, with lendingSessionWebPage: LendingSessionWebPage)
    func displayError()
}

final class LendingSessionWebViewViewController: ViewController<LendingSessionWebViewInteracting, UIView> {
    private lazy var webView: WKWebView = {
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = false
        let configuration = WKWebViewConfiguration()
        configuration.dataDetectorTypes = []
        configuration.preferences = preferences
        let webView = WKWebView(frame: .zero, configuration: configuration)
        webView.allowsBackForwardNavigationGestures = false
        return WKWebView(frame: .zero, configuration: configuration)
    }()
    
    private lazy var closeButton = UIBarButtonItem(title: GlobalLocalizable.closeButtonTitle,
                                                   style: .plain,
                                                   target: self,
                                                   action: #selector(dismissViewController))
    
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.color = Colors.grayscale300.color
        return activityIndicator
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        webView.navigationDelegate = self
        interactor.loadPage()
    }

    override func buildViewHierarchy() {
        view.addSubview(webView)
        view.addSubview(activityIndicator)
    }
    
    override func setupConstraints() {
        webView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        activityIndicator.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        navigationItem.leftBarButtonItem = closeButton
    }
    
    // MARK: - Actions
    @objc
    func dismissViewController() {
        interactor.close()
    }
}

// MARK: - LendingSessionWebViewDisplaying
extension LendingSessionWebViewViewController: LendingSessionWebViewDisplaying {
    func displayWebPage(for request: URLRequest, with lendingSessionWebPage: LendingSessionWebPage) {
        activityIndicator.startAnimating()
        navigationItem.title = lendingSessionWebPage.title
        webView.load(request)
    }
    
    func displayError() {
        let popUpViewController = PopupViewController(
            title: GlobalLocalizable.errorAlertTitle,
            description: GlobalLocalizable.tryLater,
            preferredType: .image(Assets.lendingFigureFailure.image)
        )
        let dismissAction = PopupAction(title: GlobalLocalizable.dismissButtonTitle, style: .default) { [weak self] in
            self?.navigationController?.dismiss(animated: true, completion: nil)
        }
        popUpViewController.addAction(dismissAction)
        showPopup(popUpViewController)
    }
}

// MARK: - WKNavigationDelegate
extension LendingSessionWebViewViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation?) {
        activityIndicator.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        guard let response = navigationResponse.response as? HTTPURLResponse, response.statusCode == 200 else {
            decisionHandler(.cancel)
            interactor.presentError()
            return
        }
        decisionHandler(.allow)
    }
}
