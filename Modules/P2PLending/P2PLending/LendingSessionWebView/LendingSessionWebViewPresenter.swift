import Foundation

protocol LendingSessionWebViewPresenting: AnyObject {
    var viewController: LendingSessionWebViewDisplaying? { get set }
    func presentWebPage(for request: URLRequest, with lendingSessionWebPage: LendingSessionWebPage)
    func didNextStep(action: LendingSessionWebViewAction)
    func presentError()
}

final class LendingSessionWebViewPresenter {
    private let coordinator: LendingSessionWebViewCoordinating
    weak var viewController: LendingSessionWebViewDisplaying?

    init(coordinator: LendingSessionWebViewCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - LendingSessionWebViewPresenting
extension LendingSessionWebViewPresenter: LendingSessionWebViewPresenting {
    func presentWebPage(for request: URLRequest, with lendingSessionWebPage: LendingSessionWebPage) {
        viewController?.displayWebPage(for: request, with: lendingSessionWebPage)
    }
    
    func didNextStep(action: LendingSessionWebViewAction) {
        coordinator.perform(action: action)
    }
    
    func presentError() {
        viewController?.displayError()
    }
}
