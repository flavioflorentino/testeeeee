import UIKit

enum LendingSessionWebViewAction {
    case close
}

protocol LendingSessionWebViewCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: LendingSessionWebViewAction)
}

final class LendingSessionWebViewCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - LendingSessionWebViewCoordinating
extension LendingSessionWebViewCoordinator: LendingSessionWebViewCoordinating {
    func perform(action: LendingSessionWebViewAction) {
        switch action {
        case .close:
            viewController?.dismiss(animated: true)
        }
    }
}
