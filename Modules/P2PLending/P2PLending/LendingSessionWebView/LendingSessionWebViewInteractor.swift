import Foundation
import Core

protocol LendingSessionWebViewInteracting: AnyObject {
    func loadPage()
    func close()
    func presentError()
}

final class LendingSessionWebViewInteractor {
    typealias Dependencies = HasKeychainManager

    private let dependencies: Dependencies
    private let webPage: LendingSessionWebPage
    private let presenter: LendingSessionWebViewPresenting

    init(presenter: LendingSessionWebViewPresenting, webPage: LendingSessionWebPage, dependencies: Dependencies) {
        self.presenter = presenter
        self.webPage = webPage
        self.dependencies = dependencies
    }
}

// MARK: - LendingSessionWebViewInteracting
extension LendingSessionWebViewInteractor: LendingSessionWebViewInteracting {
    func loadPage() {
        let token = dependencies.keychain.getData(key: KeychainKey.token)
        var request = URLRequest(url: webPage.url)
        request.setValue(token, forHTTPHeaderField: "token")
        presenter.presentWebPage(for: request, with: webPage)
    }
    
    func close() {
        presenter.didNextStep(action: .close)
    }
    
    func presentError() {
        presenter.presentError()
    }
}
