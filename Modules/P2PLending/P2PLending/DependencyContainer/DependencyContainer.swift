import Foundation
import Core
import FeatureFlag
import AnalyticsModule
import UIKit

typealias AppDependencies = HasNoDependency
    & HasMainQueue
    & HasUser
    & HasFeatureManager
    & HasKVStore
    & HasAnalytics
    & HasTransactionSceneProvider
    & HasURLOpener
    & HasBilletSceneProvider
    & HasKeychainManager
    & HasLegacy
    & HasPPAuthSwiftContract

final class DependencyContainer: AppDependencies {
    lazy var mainQueue: DispatchQueue = .main
    lazy var userContract: UserContract? = P2PLendingSetup.userContractInstance
    lazy var transactionSceneProvider: TransactionSceneProvider? = P2PLendingSetup.transactionSceneProvider
    lazy var featureManager: FeatureManagerContract = FeatureManager.shared
    lazy var kvStore: KVStoreContract = KVStore()
    lazy var analytics: AnalyticsProtocol = Analytics.shared
    lazy var urlOpener: URLOpener? = UIApplication.shared
    lazy var billetSceneProvider: BilletSceneProvider? = P2PLendingSetup.billetSceneProvider
    lazy var keychain: KeychainManagerContract = KeychainManager()
    lazy var auth: AuthenticationContract? = P2PLendingSetup.auth
    lazy var ppauth: PPAuthSwiftContract = PPAuthManager.shared
}
