import Foundation
import Core

protocol HasNoDependency {}

protocol HasUser {
    var userContract: UserContract? { get }
}

protocol HasTransactionSceneProvider {
    var transactionSceneProvider: TransactionSceneProvider? { get }
}

protocol HasURLOpener {
    var urlOpener: URLOpener? { get }
}

protocol HasBilletSceneProvider {
    var billetSceneProvider: BilletSceneProvider? { get }
}

protocol HasLegacy {
    var auth: AuthenticationContract? { get }
}

protocol HasPPAuthSwiftContract {
    var ppauth: PPAuthSwiftContract { get }
}

public protocol PPAuthSwiftContract {
    func handlePassword()
}

class PPAuthManager: NSObject, PPAuthSwiftContract {
    static let shared = PPAuthManager()

    func handlePassword() {
        P2PLendingSetup.ppAuthInstance?.handlePassword()
    }
}
