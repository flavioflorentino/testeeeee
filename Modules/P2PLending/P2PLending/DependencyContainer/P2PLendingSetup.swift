import Foundation
import UIKit
import UI
import Core

public protocol UserContract {
    var user: PersonConvertible? { get }
}

public protocol TransactionSceneProvider: AnyObject {
    func transactionScene(for borrower: LendingFriend, value: Double, offerIdentifier: UUID) -> UIViewController
}

public protocol BilletSceneProvider {
    func billetScene(for digitableLine: String, with description: String) -> UIViewController
}

public final class P2PLendingSetup {
    static var userContractInstance: UserContract?
    static var navigationInstanceHolder: NavigationContract?
    static var transactionSceneProvider: TransactionSceneProvider?
    static var billetSceneProvider: BilletSceneProvider?
    static var ppAuthInstance: PPAuthSwiftContract?
    static var auth: AuthenticationContract?
    
    private init() {}
    
    public static func inject(instance: UserContract) {
        userContractInstance = instance
    }
    
    public static func inject(instance: NavigationContract) {
        navigationInstanceHolder = instance
    }
    
    public static func inject(instance: TransactionSceneProvider) {
        transactionSceneProvider = instance
    }
    
    public static func inject(instance: BilletSceneProvider) {
        billetSceneProvider = instance
    }
    
    public static func inject(instance: AuthenticationContract) {
        auth = instance
    }
    
    public static func inject(instance: PPAuthSwiftContract) {
        ppAuthInstance = instance
    }
}
