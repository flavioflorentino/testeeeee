import UI
import UIKit
import LendingComponents

protocol LendingInstallmentListDisplaying: AnyObject {
    func displayHeadline(installmentListViewModel: InstallmentListViewModel)
    func displayOpenInstallmentList(_ installmentItemList: [InstallmentItemViewModel])
    func displayPaidnstallmentList(_ installmentItemList: [InstallmentItemViewModel])
    func displayOpenInstallmentListEmptyState(title: String)
    func displayPaidInstallmentListEmptyState(title: String)
    func hideOpenInstallmentListEmptyState()
    func hidePaidInstallmentListEmptyState()
    func startLoading()
    func stopLoading()
    func displayInstallmentAntecipationPopup(image: UIImage, title: String, description: NSMutableAttributedString)
    func displayUnpayableInstallmentPopup(image: UIImage, title: String, description: NSMutableAttributedString)
    func displayErrorAlert(title: String, message: String, image: UIImage)
}

final class LendingInstallmentListViewController: ViewController<LendingInstallmentListInteracting, UIView> {
    private enum Section {
        case main
    }
    
    enum AccessibilityIdentifier: String {
        case openInstallmentListTableView
        case paidInstallmentListTableView
        case openInstallmentsEmptyState
        case paidInstallmentsEmptyState
    }
    
    private typealias Localizable = Strings.Lending.Installment.List
    
    private lazy var loadingActivityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.color = Colors.grayscale400.color
        return activityIndicator
    }()
    
    private lazy var headlineLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale700())
        return label
    }()
    
    private lazy var installmentTitleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
        return label
    }()
    
    private lazy var selectedListSegmentControl: CustomSegmentedControl = {
        let segmentedControl = CustomSegmentedControl()
        segmentedControl.backgroundColor = .clear
        segmentedControl.textColor = Colors.grayscale500.color
        segmentedControl.selectorTextColor = Colors.branding600.color
        segmentedControl.selectorViewColor = Colors.branding600.color
        segmentedControl.selectorIsFullSize = true
        segmentedControl.setIndex(index: 0)
        segmentedControl.addTarget(self, action: #selector(segmentControlValueChanged(_:)), for: .valueChanged)
        return segmentedControl
    }()
    
    private lazy var helpButton: UIBarButtonItem = {
        let buttonItem = UIBarButtonItem(image: Assets.lendingGlyphHelp.image,
                                         style: .plain,
                                         target: self,
                                         action: #selector(helpTapped))
        buttonItem.accessibilityLabel = GlobalLocalizable.helpButtonAccessibilityLabel
        return buttonItem
    }()
    
    private lazy var installmentStackView = UIStackView(arrangedSubviews: [installmentTitleLabel, selectedListSegmentControl])
    
    private lazy var headerStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [headlineLabel, installmentStackView])
        stackView.spacing = Spacing.base03
        stackView.axis = .vertical
        stackView.compatibleLayoutMargins = .rootView
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    
    // MARK: - ScrollView
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.isPagingEnabled = true
        scrollView.delegate = self
        scrollView.showsHorizontalScrollIndicator = false
        return scrollView
    }()
    
    private lazy var scrollViewContentView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [openInstallmentListTableView, paidInstallmentListTableView])
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    // MARK: - TableViews
    private(set) lazy var openInstallmentListTableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.separatorStyle = .singleLine
        tableView.register(InstallmentCell.self, forCellReuseIdentifier: InstallmentCell.identifier)
        tableView.estimatedRowHeight = Sizing.base09
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = .clear
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        tableView.accessibilityIdentifier = AccessibilityIdentifier.openInstallmentListTableView.rawValue
        return tableView
    }()
    
    private(set) lazy var paidInstallmentListTableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.separatorStyle = .singleLine
        tableView.register(InstallmentCell.self, forCellReuseIdentifier: InstallmentCell.identifier)
        tableView.estimatedRowHeight = Sizing.base09
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = .clear
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        tableView.accessibilityIdentifier = AccessibilityIdentifier.paidInstallmentListTableView.rawValue
        return tableView
    }()
    
    private lazy var openInstallmentListDataSource: TableViewDataSource<Section, InstallmentItemViewModel> = {
        let dataSource = TableViewDataSource<Section, InstallmentItemViewModel>(view: openInstallmentListTableView) { tableView, indexPath, item -> UITableViewCell? in
            let cell = tableView.dequeueReusableCell(withIdentifier: InstallmentCell.identifier, for: indexPath) as? InstallmentCell
            cell?.setup(with: item)
            cell?.selectionStyle = .none
            return cell
        }
        dataSource.add(section: .main)
        return dataSource
    }()
    
    private lazy var paidInstallmentListDataSource: TableViewDataSource<Section, InstallmentItemViewModel> = {
        let dataSource = TableViewDataSource<Section, InstallmentItemViewModel>(view: paidInstallmentListTableView) { tableView, indexPath, item -> UITableViewCell? in
            let cell = tableView.dequeueReusableCell(withIdentifier: InstallmentCell.identifier, for: indexPath) as? InstallmentCell
            cell?.setup(with: item)
            cell?.selectionStyle = .none
            return cell
        }
        dataSource.add(section: .main)
        return dataSource
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.fetchInstallmentList()
        openInstallmentListTableView.dataSource = openInstallmentListDataSource
        paidInstallmentListTableView.dataSource = paidInstallmentListDataSource
    }
    
    override func buildViewHierarchy() {
        view.addSubview(headerStackView)
        view.addSubview(loadingActivityIndicator)
        view.addSubview(scrollView)
        
        scrollView.addSubview(scrollViewContentView)
    }
    
    override func setupConstraints() {
        headerStackView.snp.makeConstraints {
            $0.top.trailing.leading.equalToSuperview()
        }
        
        scrollView.snp.makeConstraints {
            $0.trailing.bottom.leading.equalToSuperview()
            $0.top.equalTo(headerStackView.snp.bottom)
        }
        
        scrollViewContentView.snp.makeConstraints {
            $0.edges.height.equalToSuperview()
        }
        
        paidInstallmentListTableView.snp.makeConstraints {
            $0.width.equalTo(scrollView)
        }
        
        openInstallmentListTableView.snp.makeConstraints {
            $0.width.equalTo(scrollView)
        }
        
        loadingActivityIndicator.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        view.tintColor = Colors.branding400.color
        setupNavigationAppearance()
    }
    
    private func setupNavigationAppearance() {
        navigationController?.navigationBar.tintColor = Colors.branding400.color
        navigationController?.view.backgroundColor = Colors.backgroundPrimary.color
        navigationItem.rightBarButtonItem = helpButton
        title = Localizable.title
        
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .always
            navigationController?.navigationBar.prefersLargeTitles = true
        }
        if #available(iOS 13.0, *) {
            let navigationBarAppearance = UINavigationBarAppearance()
            navigationBarAppearance.configureWithTransparentBackground()
            navigationBarAppearance.backgroundColor = Colors.backgroundPrimary.color
            navigationBarAppearance.shadowColor = .clear
            navigationController?.navigationBar.scrollEdgeAppearance = navigationBarAppearance
            navigationController?.navigationBar.standardAppearance = navigationBarAppearance
        }
    }
}
// MARK: - Actions
@objc
private extension LendingInstallmentListViewController {
    func segmentControlValueChanged(_ segmentControl: CustomSegmentedControl) {
        let selectedIndex = CGFloat(segmentControl.selectedIndex)
        let contentOffset = CGPoint(x: selectedIndex * scrollView.bounds.width, y: 0)
        interactor.sendTrackingEvent(for: segmentControl.selectedIndex)
        scrollView.setContentOffset(contentOffset, animated: true)
    }
    
    func helpTapped() {
        interactor.showFAQ()
    }
}

// MARK: - UITableViewDelegate
extension LendingInstallmentListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard tableView == openInstallmentListTableView else {
            return
        }
        interactor.didSelectInstallment(at: indexPath)
    }
}

// MARK: - UIScrollViewDelegate
extension LendingInstallmentListViewController: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        guard scrollView == self.scrollView else {
            return
        }
        let pagesOffset = Array(stride(from: 0, to: scrollView.contentSize.width, by: scrollView.bounds.width))
        if let index = pagesOffset.firstIndex(of: scrollView.contentOffset.x) {
            selectedListSegmentControl.setIndex(index: Int(index))
        }
    }
}

// MARK: - LendingInstallmentListDisplaying
extension LendingInstallmentListViewController: LendingInstallmentListDisplaying {
    func displayHeadline(installmentListViewModel: InstallmentListViewModel) {
        headlineLabel.attributedText = installmentListViewModel.getOutstandingBalanceDescription()
        installmentTitleLabel.text = Localizable.installment
        selectedListSegmentControl.setButtonTitles(buttonTitles: installmentListViewModel.segmentedControlButtonTitles())
    }
    
    func displayOpenInstallmentList(_ installmentItemList: [InstallmentItemViewModel]) {
        openInstallmentListDataSource.update(items: installmentItemList, from: .main)
    }
    
    func displayPaidnstallmentList(_ installmentItemList: [InstallmentItemViewModel]) {
        paidInstallmentListDataSource.update(items: installmentItemList, from: .main)
    }
    
    func displayOpenInstallmentListEmptyState(title: String) {
        let informativeView = InformativeView(image: Assets.lendingFigureUpset.image, title: title)
        informativeView.accessibilityIdentifier = AccessibilityIdentifier.openInstallmentsEmptyState.rawValue
        openInstallmentListTableView.backgroundView = informativeView
    }
    
    func displayPaidInstallmentListEmptyState(title: String) {
        let informativeView = InformativeView(image: Assets.lendingFigureUpset.image, title: title)
        informativeView.accessibilityIdentifier = AccessibilityIdentifier.paidInstallmentsEmptyState.rawValue
        paidInstallmentListTableView.backgroundView = informativeView
    }
    
    func hideOpenInstallmentListEmptyState() {
        openInstallmentListTableView.backgroundView = nil
    }
    
    func hidePaidInstallmentListEmptyState() {
        paidInstallmentListTableView.backgroundView = nil
    }
    
    func startLoading() {
        loadingActivityIndicator.startAnimating()
    }
    
    func stopLoading() {
        loadingActivityIndicator.stopAnimating()
    }
    
    func displayInstallmentAntecipationPopup(image: UIImage, title: String, description: NSMutableAttributedString) {
        let popupAlert = PopupViewController(title: title, preferredType: .image(image))
        popupAlert.setAttributeDescription(description)
        popupAlert.hideCloseButton = true
        
        let getInContactAction = PopupAction(title: Localizable.getInContactButtonTitle, style: .fill) {
            self.interactor.openCustomerSupport()
        }
        let dismissAction = PopupAction(title: GlobalLocalizable.laterDismissButtonTitle, style: .link) {
            self.interactor.sendTrackingEvent(event: .laterButton)
        }
        popupAlert.addAction(getInContactAction)
        popupAlert.addAction(dismissAction)
        
        showPopup(popupAlert)
    }
    
    func displayUnpayableInstallmentPopup(image: UIImage, title: String, description: NSMutableAttributedString) {
        let popupAlert = PopupViewController(title: title, preferredType: .image(image))
        popupAlert.setAttributeDescription(description)
        popupAlert.hideCloseButton = true
        
        let dismissAction = PopupAction(title: GlobalLocalizable.dismissButtonTitle, style: .fill) {
            self.interactor.sendTrackingEvent(event: .dismissButton)
        }
        popupAlert.addAction(dismissAction)
        
        showPopup(popupAlert)
    }
    
    func displayErrorAlert(title: String, message: String, image: UIImage) {
        let primaryAction = ApolloAlertAction(title: GlobalLocalizable.tryAgainButtonTitle) { [weak self] in
            self?.interactor.fetchInstallmentList()
        }
        
        let linkAction = ApolloAlertAction(title: GlobalLocalizable.cancel) { [weak self] in
            self?.dismiss(animated: true, completion: nil)
            self?.navigationController?.popViewController(animated: true)
        }
        
        showApolloAlert(image: image,
                        title: title,
                        subtitle: message,
                        primaryButtonAction: primaryAction,
                        linkButtonAction: linkAction,
                        dismissOnTouchOutside: false)
    }
}
