import Foundation
import AnalyticsModule
import FeatureFlag
import Core

protocol LendingInstallmentListInteracting: AnyObject {
    func fetchInstallmentList()
    func showFAQ()
    func didSelectInstallment(at indexPath: IndexPath)
    func openCustomerSupport()
    func sendTrackingEvent(event: LendingInstallmentListEvent)
    func sendTrackingEvent(for selectedTabIndex: Int)
}

final class LendingInstallmentListInteractor {
    typealias Dependencies = HasAnalytics & HasFeatureManager
    private let dependencies: Dependencies
    private let proposalIdentifier: UUID
    private let agent: ConsultingAgent
    private let service: LendingInstallmentListServicing
    private let presenter: LendingInstallmentListPresenting
    private var installmentListResponse: InstallmentListResponse?

    init(service: LendingInstallmentListServicing,
         presenter: LendingInstallmentListPresenting,
         dependencies: Dependencies,
         proposalIdentifier: UUID,
         agent: ConsultingAgent) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.proposalIdentifier = proposalIdentifier
        self.agent = agent
    }
}

// MARK: - LendingInstallmentListInteracting
extension LendingInstallmentListInteractor: LendingInstallmentListInteracting {
    func fetchInstallmentList() {
        presenter.startLoading()
        service.fetchInstallmentList(for: agent, with: proposalIdentifier) { [weak self] result in
            self?.presenter.stopLoading()
            switch result {
            case .success(let response):
                self?.installmentListResponse = response
                self?.handle(installmentListResponse: response)
                self?.presenter.displayHeadline(with: response)
            case .failure(let error):
                self?.handle(error: error)
            }
        }
    }
    
    func showFAQ() {
        guard let faqURL = URL(string: "picpay://picpay/helpcenter/section/360011533772") else { return }
        dependencies.analytics.log(LendingInstallmentListEvent.faq(agent: agent))
        presenter.didNextStep(action: .showFAQ(url: faqURL))
    }
    
    func didSelectInstallment(at indexPath: IndexPath) {
        guard let installment = installmentListResponse?.inProgress[indexPath.row] else { return }
        switch agent {
        case .borrower:
            handleBorrowerSelection(installment: installment)
        case .investor:
            handleInvestorSelection(installment: installment)
        }
    }
    
    func openCustomerSupport() {
        guard let url = URL(string: "picpay://picpay/helpcenter/reason/clientepf__picpay_card") else { return }
        dependencies.analytics.log(LendingInstallmentListEvent.costumerSupport)
        presenter.didNextStep(action: .customerSupport(url: url))
    }
    
    func sendTrackingEvent(event: LendingInstallmentListEvent) {
        dependencies.analytics.log(event)
    }
    
    func sendTrackingEvent(for selectedTabIndex: Int) {
        selectedTabIndex == 0 ? dependencies.analytics.log(LendingInstallmentListEvent.openInstallmentsTab(agent: agent))
                              : dependencies.analytics.log(LendingInstallmentListEvent.paidInstallmentsTab(agent: agent))
    }
}

private extension LendingInstallmentListInteractor {
    func handle(installmentListResponse: InstallmentListResponse) {
        presenter.shouldDisplayOpenInstallmentsEmptyState(installmentListResponse.inProgress.isEmpty)
        presenter.displayOpenInstallmentList(installmentListResponse.inProgress)
        
        presenter.shouldDisplayPaidInstallmentsEmptyState(installmentListResponse.finished.isEmpty)
        presenter.displayPaidInstallmentList(installmentListResponse.finished)
    }
    
    func sendTrackingEvent(for installmentStatus: InstallmentStatus) {
        switch installmentStatus {
        case .active:
            dependencies.analytics.log(LendingInstallmentListEvent.currentInstallment)
        case .delayed:
            dependencies.analytics.log(LendingInstallmentListEvent.delayedInstallment(agent: agent))
        case .canceled, .paid:
            break
        }
    }
    
    func handleInvestorSelection(installment: InstallmentItem) {
        guard installment.status == .delayed else {
            return
        }
        guard let url = URL(string: "picpay://picpay/helpcenter/article/360049387492") else { return }
        dependencies.analytics.log(LendingInstallmentListEvent.delayedInstallment(agent: agent))
        presenter.presentPendingPaymentFromBorrower(faqUrl: url)
    }
    
    func handleBorrowerSelection(installment: InstallmentItem) {
        guard dependencies.featureManager.isActive(.isP2PLendingBorrowerInstallmentDetailsAvailable) else { return }
        
        guard installment.payable else {
            switch installment.status {
            case .active:
                dependencies.analytics.log(LendingInstallmentListEvent.antecipationInstallment)
                presenter.presentInstallmentAntecipationPopup()
            case .delayed:
                dependencies.analytics.log(LendingInstallmentListEvent.delayedInstallment(agent: agent))
                presenter.presentUnpayableInstallmentPopup()
            case .paid, .canceled:
                break
            }
            return
        }
        sendTrackingEvent(for: installment.status)
        presenter.didNextStep(action: .installmentDetail(offerIdentifier: proposalIdentifier, installmentItem: installment))
    }
    
    func handle(error: ApiError) {
        switch error {
        case .connectionFailure:
            presenter.displayNoConnectionError()
        default:
            presenter.displayGenericError()
        }
    }
}
