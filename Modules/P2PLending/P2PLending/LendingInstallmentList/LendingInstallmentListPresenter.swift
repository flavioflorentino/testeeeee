import UIKit
import UI
import AssetsKit

protocol LendingInstallmentListPresenting: AnyObject {
    var viewController: LendingInstallmentListDisplaying? { get set }
    func displayHeadline(with installmentListResponse: InstallmentListResponse)
    func displayOpenInstallmentList(_ installmentItemList: [InstallmentItem])
    func displayPaidInstallmentList(_ installmentItemList: [InstallmentItem])
    func shouldDisplayOpenInstallmentsEmptyState(_ listIsEmpty: Bool)
    func shouldDisplayPaidInstallmentsEmptyState(_ listIsEmpty: Bool)
    func startLoading()
    func stopLoading()
    func displayNoConnectionError()
    func displayGenericError()
    func presentInstallmentAntecipationPopup()
    func presentUnpayableInstallmentPopup()
    func presentPendingPaymentFromBorrower(faqUrl: URL)
    func didNextStep(action: LendingInstallmentListAction)
}

final class LendingInstallmentListPresenter {
    private typealias Localizable = Strings.Lending.Installment.List

    private let coordinator: LendingInstallmentListCoordinating
    weak var viewController: LendingInstallmentListDisplaying?
    private var agent: ConsultingAgent

    init(coordinator: LendingInstallmentListCoordinating, agent: ConsultingAgent) {
        self.coordinator = coordinator
        self.agent = agent
    }
    
    var descriptionAttributes: [NSAttributedString.Key: Any] = {
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        return [
            NSAttributedString.Key.font: Typography.bodyPrimary(.default).font(),
            NSAttributedString.Key.foregroundColor: Colors.grayscale700.color,
            NSAttributedString.Key.paragraphStyle: paragraph
        ]
    }()
    
    var paidInstallmentsEmptyStateTitle: String {
        switch agent {
        case .borrower:
            return Localizable.paidInstallmentEmptyListTitle
        case .investor:
            return Localizable.receivedInstallmentEmptyListTitle
        }
    }
}

// MARK: - LendingInstallmentListPresenting
extension LendingInstallmentListPresenter: LendingInstallmentListPresenting {
    func displayHeadline(with installmentListResponse: InstallmentListResponse) {
        let viewModel = InstallmentListViewModel(installmentList: installmentListResponse, agent: agent)
        viewController?.displayHeadline(installmentListViewModel: viewModel)
    }
    
    func displayOpenInstallmentList(_ installmentItemList: [InstallmentItem]) {
        let items = installmentItemList.map { InstallmentItemViewModel(item: $0, agent: agent) }
        viewController?.displayOpenInstallmentList(items)
    }
    
    func displayPaidInstallmentList(_ installmentItemList: [InstallmentItem]) {
        let items = installmentItemList.map { InstallmentItemViewModel(item: $0, agent: agent) }
        viewController?.displayPaidnstallmentList(items)
    }
    
    func shouldDisplayOpenInstallmentsEmptyState(_ listIsEmpty: Bool) {
        listIsEmpty ? viewController?.displayOpenInstallmentListEmptyState(title: Localizable.openInstallmentEmptyListTitle)
                    : viewController?.hideOpenInstallmentListEmptyState()
    }
    
    func shouldDisplayPaidInstallmentsEmptyState(_ listIsEmpty: Bool) {
        listIsEmpty ? viewController?.displayPaidInstallmentListEmptyState(title: paidInstallmentsEmptyStateTitle)
                    : viewController?.hidePaidInstallmentListEmptyState()
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
    
    func displayNoConnectionError() {
        viewController?.displayErrorAlert(title: GlobalLocalizable.noConnectionErrorTitle,
                                          message: GlobalLocalizable.noConnectionErrorBody,
                                          image: Resources.Illustrations.iluNoConnectionGreenBackground.image)
    }
    
    func displayGenericError() {
        viewController?.displayErrorAlert(title: GlobalLocalizable.errorAlertTitle,
                                          message: GlobalLocalizable.genericErrorBody,
                                          image: Resources.Illustrations.iluError.image)
    }
    
    func presentInstallmentAntecipationPopup() {
        let attributtedDescription = NSMutableAttributedString(string: Localizable.antecipationPopupDescription,
                                                               attributes: descriptionAttributes)
        viewController?.displayInstallmentAntecipationPopup(image: Resources.Illustrations.iluConstruction.image,
                                                            title: Localizable.antecipationPopupTitle,
                                                            description: attributtedDescription)
    }
    
    func presentUnpayableInstallmentPopup() {
        let attributtedDescription = NSMutableAttributedString(string: Localizable.unpayablePopupDescription,
                                                               attributes: descriptionAttributes)
        viewController?.displayUnpayableInstallmentPopup(image: Resources.Illustrations.iluWarning.image,
                                                         title: Localizable.unpayablePopupTitle,
                                                         description: attributtedDescription)
    }
    
    func presentPendingPaymentFromBorrower(faqUrl: URL) {
        let content = ApolloFeedbackViewContent(image: Resources.Illustrations.iluWarning.image,
                                                title: Localizable.pendingPaymentTitle,
                                                description: NSAttributedString(string: Localizable.pendingPaymentBody),
                                                primaryButtonTitle: GlobalLocalizable.dismissButtonTitle,
                                                secondaryButtonTitle: Localizable.pendingPaymentFaqButtonTitle)
        coordinator.perform(action: .pendingPaymentFromBorrower(content: content, url: faqUrl))
    }
    
    func didNextStep(action: LendingInstallmentListAction) {
        coordinator.perform(action: action)
    }
}
