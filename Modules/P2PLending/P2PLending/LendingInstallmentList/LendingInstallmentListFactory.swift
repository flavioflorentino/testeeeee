import UIKit

enum LendingInstallmentListFactory {
    static func make(proposalIdentifier: UUID, agent: ConsultingAgent) -> LendingInstallmentListViewController {
        let container = DependencyContainer()
        let service: LendingInstallmentListServicing = LendingInstallmentListService(dependencies: container)
        let coordinator: LendingInstallmentListCoordinating = LendingInstallmentListCoordinator(dependencies: container)
        let presenter: LendingInstallmentListPresenting = LendingInstallmentListPresenter(coordinator: coordinator, agent: agent)
        let interactor = LendingInstallmentListInteractor(service: service,
                                                          presenter: presenter,
                                                          dependencies: container,
                                                          proposalIdentifier: proposalIdentifier,
                                                          agent: agent)
        let viewController = LendingInstallmentListViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
