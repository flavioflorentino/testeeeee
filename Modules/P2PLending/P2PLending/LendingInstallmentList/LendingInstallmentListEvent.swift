import Foundation
import AnalyticsModule

enum LendingInstallmentListEvent: AnalyticsKeyProtocol {
    case faq(agent: ConsultingAgent)
    case paidInstallmentsTab(agent: ConsultingAgent)
    case openInstallmentsTab(agent: ConsultingAgent)
    case costumerSupport
    case laterButton
    case dismissButton
    case currentInstallment
    case delayedInstallment(agent: ConsultingAgent)
    case antecipationInstallment
    
    private func getScreenName(for agent: ConsultingAgent) -> String {
        switch agent {
        case .borrower:
            return "P2P_LENDING_TOMADOR_MINHAS_PARCELAS"
        case .investor:
            return "P2P_LENDING_INVESTIDOR_MINHAS_PARCELAS"
        }
    }
    
    private func getPaidIntallmentsTabName (for agent: ConsultingAgent) -> String {
        switch agent {
        case .borrower:
            return AnalyticsConstants.paidInstallmentsTab.description
        case .investor:
            return AnalyticsConstants.receivedInstallmentsTab.description
        }
    }
    
    private var antecipationPopupTitle: String {
        "NAO_E_POSSIVEL_ANTECIPAR_PARCELAS"
    }
    
    private var unpayableInstallmentPopupTitle: String {
        "NAO_E_POSSIVEL_PAGAR_PARCELA"
    }
    
    private var currentInstallmentButtonTitle: String {
        "PARCELA_EM_ABERTO"
    }
    
    private var delayedInstallmentButtonTitle: String {
        "PARCELA_VENCIDA"
    }
    
    private var antecipationInstallmentButtonTitle: String {
        "PARCELA_ANTECIPADA"
    }
    
    private var name: String {
        switch self {
        case .faq, .costumerSupport, .laterButton, .dismissButton, .currentInstallment, .delayedInstallment, .antecipationInstallment:
            return AnalyticInteraction.buttonClicked.name
        case .openInstallmentsTab, .paidInstallmentsTab:
            return AnalyticInteraction.tabTapped.name
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .faq(let agent):
            return [
                AnalyticsKeys.screenName.name: getScreenName(for: agent),
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.faq.description
            ]
        case .openInstallmentsTab(let agent):
            return [
                AnalyticsKeys.screenName.name: getScreenName(for: agent),
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.tabName.name: AnalyticsConstants.openInstallmentsTab.description
            ]
        case .paidInstallmentsTab(let agent):
            return [
                AnalyticsKeys.screenName.name: getScreenName(for: agent),
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.tabName.name: getPaidIntallmentsTabName(for: agent)
            ]
        case .costumerSupport:
            return [
                AnalyticsKeys.screenName.name: getScreenName(for: .borrower),
                AnalyticsKeys.dialogName.name: antecipationPopupTitle,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.optionSelected.name: AnalyticsConstants.getInContact.description
            ]
        case .laterButton:
            return [
                AnalyticsKeys.screenName.name: getScreenName(for: .borrower),
                AnalyticsKeys.dialogName.name: antecipationPopupTitle,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.optionSelected.name: AnalyticsConstants.laterButton.description
            ]
        case .dismissButton:
            return [
                AnalyticsKeys.screenName.name: getScreenName(for: .borrower),
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.dialogName.name: unpayableInstallmentPopupTitle,
                AnalyticsKeys.optionSelected.name: AnalyticsConstants.dismissButton.description
            ]
        case .currentInstallment:
            return [
                AnalyticsKeys.buttonName.name: currentInstallmentButtonTitle,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.screenName.name: getScreenName(for: .borrower)
            ]
        case .delayedInstallment(let agent):
            return [
                AnalyticsKeys.buttonName.name: delayedInstallmentButtonTitle,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.screenName.name: getScreenName(for: agent)
            ]
        case .antecipationInstallment:
            return [
                AnalyticsKeys.buttonName.name: antecipationInstallmentButtonTitle,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.screenName.name: getScreenName(for: .borrower)
            ]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.eventTracker])
    }
}
