import Foundation
import UIKit
import UI
import LendingComponents

struct InstallmentItemViewModel: Equatable {
    private typealias Localizable = Strings.Lending.Installment.List
    
    let item: InstallmentItem
    let agent: ConsultingAgent
    
    var statusTitle: String {
        switch item.status {
        case .active:
            return item.payable ? Localizable.currentInstallmentTitle : Localizable.openInstallmentTitle
        case .delayed:
            return Localizable.overdueInstallmentTitle
        case .paid:
            return paidInstallmentStatusTitle
        case .canceled:
            return Localizable.cancelledInstallmentTitle
        }
    }
    
    var paidInstallmentStatusTitle: String {
        switch agent {
        case .borrower:
            return Localizable.paidInstallmentTitle
        case .investor:
            return Localizable.receivedInstallmentTitle
        }
    }
    
    var statusTextColor: UIColor {
        switch item.status {
        case .active:
            return item.payable ? Colors.neutral600.color : Colors.grayscale500.color
        case .delayed:
            return Colors.critical600.color
        case .paid:
            return Colors.success600.color
        case .canceled:
            return Colors.grayscale500.color
        }
    }
    
    var installmentValueTextColor: UIColor {
        switch item.status {
        case .active:
            return item.payable ? Colors.neutral600.color : Colors.grayscale700.color
        case .delayed:
            return Colors.critical600.color
        case .paid:
            return Colors.grayscale700.color
        case .canceled:
            return Colors.grayscale700.color
        }
    }
    
    var overdueValueAttributtedText: NSAttributedString {
        let attributeString = NSMutableAttributedString(string: "")
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle,
                                     value: 1,
                                     range: NSRange(location: 0, length: attributeString.length))
        return attributeString
    }
    
    func getFormattedDueDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "pt_BR")
        dateFormatter.dateFormat = "dd MMM"
        return dateFormatter.string(from: item.dueDate).uppercased().replacingOccurrences(of: ".", with: "")
    }
}

extension InstallmentItemViewModel: InstallmentCellViewModeling {
    var order: String {
        "\(String(item.number))ª"
    }
    
    var dueDate: String {
        getFormattedDueDate()
    }
    
    var statusDescription: NSAttributedString {
        let attr = NSMutableAttributedString(string: statusTitle)
        attr.addAttributes(
            [.foregroundColor: statusTextColor],
            range: NSRange(location: 0, length: attr.length)
        )
        return attr
    }
    
    var originalValueDescription: NSAttributedString? { nil }
    
    var valueDescription: NSAttributedString? {
        let attr = NSMutableAttributedString(string: item.value.toCurrencyString() ?? "")
        attr.addAttributes(
            [.foregroundColor: installmentValueTextColor],
            range: NSRange(location: 0, length: attr.length)
        )
        return attr
    }
    
    var hideAccessoryView: Bool {
        switch agent {
        case .borrower:
            return item.status == .paid
        case .investor:
            return item.status != .delayed
        }
    }
}
