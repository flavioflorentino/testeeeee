import Foundation

struct InstallmentListResponse: Decodable, Equatable {
    let outstandingBalance: Float
    let inProgress: [InstallmentItem]
    let finished: [InstallmentItem]
}
