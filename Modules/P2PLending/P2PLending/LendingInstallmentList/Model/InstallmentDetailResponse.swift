import Foundation

struct InstallmentDetailResponse: Decodable, Equatable {
    let number: Int
    let value: Int
    let dueDate: String
    let digitableLine: String
    let penaltyValue: Float
    let daysOverdue: Int
    let originalValue: Float
    
    enum CodingKeys: String, CodingKey {
        case number
        case value
        case dueDate = "due_date"
        case digitableLine = "digitable_line"
        case penaltyValue = "penalty_value"
        case daysOverdue = "days_overdue"
        case originalValue = "original_value"
    }
}
