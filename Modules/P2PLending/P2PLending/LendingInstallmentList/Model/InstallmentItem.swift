import Foundation

struct InstallmentItem: Decodable, Equatable {
    let number: Int
    let value: Double
    let dueDate: Date
    let payable: Bool
    let status: InstallmentStatus
}
