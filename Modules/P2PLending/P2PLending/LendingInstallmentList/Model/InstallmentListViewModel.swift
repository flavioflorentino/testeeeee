import Foundation
import UI
import UIKit

struct InstallmentListViewModel: Equatable {
    private typealias Localizable = Strings.Lending.Installment.List
    var installmentList: InstallmentListResponse
    let agent: ConsultingAgent
    
    func getOutstandingBalanceDescription() -> NSAttributedString {
        let boldOutsdandingBalance = "**\(installmentList.outstandingBalance.toCurrencyString() ?? "")**"
        switch agent {
        case .borrower:
            let bodyText = Localizable.outdsdandingBalanceBorrowerBody(boldOutsdandingBalance)
            return bodyText.attributedStringWith(normalFont: Typography.bodyPrimary().font(),
                                                 highlightFont: Typography.bodyPrimary(.highlight).font(),
                                                 normalColor: Colors.grayscale700.color,
                                                 highlightColor: Colors.neutral600.color,
                                                 underline: false)
        case .investor:
            let bodyText = Localizable.outdsdandingBalanceInvestorBody(boldOutsdandingBalance)
            return bodyText.attributedStringWith(normalFont: Typography.bodyPrimary().font(),
                                                 highlightFont: Typography.bodyPrimary(.highlight).font(),
                                                 normalColor: Colors.grayscale700.color,
                                                 highlightColor: Colors.neutral600.color,
                                                 underline: false)
        }
    }
    
    func segmentedControlButtonTitles() -> [String] {
        switch agent {
        case .borrower:
            return [Localizable.open, Localizable.paid]
        case .investor:
            return [Localizable.open, Localizable.received]
        }
    }
}
