import Foundation

enum InstallmentStatus: String, Decodable, CaseIterable {
    case active = "ACTIVE"
    case delayed = "DELAYED"
    case paid = "PAID"
    case canceled = "CANCELED"
}
