import Core
import Foundation

protocol LendingInstallmentListServicing: Servicing {
    func fetchInstallmentList(for agent: ConsultingAgent, with offerID: UUID, completion: @escaping ModelCompletionBlock<InstallmentListResponse>)
}

final class LendingInstallmentListService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter
    }()
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - LendingInstallmentListServicing
extension LendingInstallmentListService: LendingInstallmentListServicing {
    func fetchInstallmentList(for agent: ConsultingAgent, with proposalIdentifier: UUID, completion: @escaping ModelCompletionBlock<InstallmentListResponse>) {
        let api = Api<InstallmentListResponse>(endpoint: P2PLendingEndpoint.installmentList(identifier: proposalIdentifier, agent: agent))
        let decoder = JSONDecoder()
        api.shouldUseDefaultDateFormatter = false
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        api.execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
