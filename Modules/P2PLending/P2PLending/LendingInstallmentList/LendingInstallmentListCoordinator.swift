import UIKit
import UI

enum LendingInstallmentListAction: Equatable {
    case showFAQ(url: URL)
    case installmentDetail(offerIdentifier: UUID, installmentItem: InstallmentItem)
    case customerSupport(url: URL)
    case pendingPaymentFromBorrower(content: ApolloFeedbackViewContent, url: URL)
}

protocol LendingInstallmentListCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: LendingInstallmentListAction)
}

final class LendingInstallmentListCoordinator {
    typealias Dependencies = HasURLOpener
    private let dependencies: Dependencies
    
    weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - LendingInstallmentListCoordinating
extension LendingInstallmentListCoordinator: LendingInstallmentListCoordinating, URLOpenerCoordinating {
    func perform(action: LendingInstallmentListAction) {
        switch action {
        case .showFAQ(let url):
            showFAQ(for: url, using: dependencies)
        case let .installmentDetail(offerIdentifier, installmentItem):
            presentInstallmentDetail(offerIdentifier: offerIdentifier, installmentItem: installmentItem)
        case .customerSupport(let url):
            showFAQ(for: url, using: dependencies)
        case let .pendingPaymentFromBorrower(content, url):
            showPendingPaymentApolloFeedbackController(content: content, url: url)
        }
    }
}

private extension LendingInstallmentListCoordinator {
    func showPendingPaymentApolloFeedbackController(content: ApolloFeedbackViewContent, url: URL) {
        let controller = ApolloFeedbackViewController(content: content)
        controller.didTapPrimaryButton = {
            controller.navigationController?.popViewController(animated: true)
        }
        controller.didTapSecondaryButton = { [weak self] in
            guard let self = self else { return }
            self.showFAQ(for: url, using: self.dependencies)
        }
        viewController?.show(controller, sender: nil)
    }
    
    func presentInstallmentDetail(offerIdentifier: UUID, installmentItem: InstallmentItem) {
        let controller = LendingInstallmentDetailFactory.make(
            offerIdentifier: offerIdentifier,
            installmentNumber: installmentItem.number
        )
        let navigation = UINavigationController(rootViewController: controller)
        viewController?.present(navigation, animated: true, completion: nil)
    }
}
