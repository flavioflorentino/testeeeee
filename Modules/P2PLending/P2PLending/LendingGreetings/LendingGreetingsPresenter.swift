import UIKit
import Foundation
import UI
import Core

protocol LendingGreetingsPresenting: AnyObject {
    var viewController: LendingGreetingsDisplay? { get set }
    func presentFooterText()
    func presentLegalTerms(termsOfUseURL: URL, privacyPolicyURL: URL)
    func presentCrednovoDisclaimer()
    func setContinueButtonEnabled(_ isEnabled: Bool)
    func present(error: Error)
    func startLoading()
    func stopLoading()
    func didNextStep(action: LendingGreetingsAction)
}

final class LendingGreetingsPresenter: LendingGreetingsPresenting {
    private typealias Localizable = Strings.Lending.Greetings
    
    private let coordinator: LendingGreetingsCoordinating
    weak var viewController: LendingGreetingsDisplay?

    init(coordinator: LendingGreetingsCoordinating) {
        self.coordinator = coordinator
    }
    
    func presentFooterText() {
        let footerText = Localizable.footer as NSString
        let crednovoRange = footerText.range(of: Localizable.footerHighlight)
        let attributedString = NSMutableAttributedString(
            string: Localizable.footer,
            attributes: [.font: Typography.bodyPrimary().font(), .foregroundColor: Colors.grayscale600.color]
        )
        attributedString.addAttribute(.link, value: "", range: crednovoRange)
        viewController?.displayFooter(footerText: attributedString)
    }
    
    func presentLegalTerms(termsOfUseURL: URL, privacyPolicyURL: URL) {
        let legalTermsText = Localizable.legalFooterNotice as NSString
        let termsOfUseRange = legalTermsText.range(of: Localizable.legalFooterNoticeTermsOfUse)
        let privacyPolicyRange = legalTermsText.range(of: Localizable.legalFooterNoticePrivacyPolicy)
        let attributedString = NSMutableAttributedString(
            string: Localizable.legalFooterNotice,
            attributes: [.font: Typography.bodySecondary().font(), .foregroundColor: Colors.grayscale600.color]
        )
        attributedString.addAttribute(.link, value: termsOfUseURL, range: termsOfUseRange)
        attributedString.addAttribute(.link, value: privacyPolicyURL, range: privacyPolicyRange)
        viewController?.displayLegalNotice(attributedText: attributedString)
    }
    
    func presentCrednovoDisclaimer() {
        viewController?.displayCrednovoDisclaimer()
    }
    
    func setContinueButtonEnabled(_ isEnabled: Bool) {
        viewController?.setContinueButtonEnabled(isEnabled)
    }
    
    func present(error: Error) {
        viewController?.displayAlert(title: GlobalLocalizable.errorAlertTitle, message: error.localizedDescription)
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
    
    func didNextStep(action: LendingGreetingsAction) {
        coordinator.perform(action: action)
    }
}
