import Core
import Foundation
import AnalyticsModule

protocol LendingGreetingsViewModelInputs: AnyObject {
    func fetchLendingProposalConfiguration()
    func sendTrackingEvent(_ type: LendingGreetingsEvent)
    func showLegalTerms(for url: URL)
    func awarenessAndAcceptanceOfTermsChanged(_ accepted: Bool)
    func showCrednovoDisclaimer()
    func showHelp()
    func close()
    func makeProposal()
}

final class LendingGreetingsViewModel {
    typealias Dependencies = HasAnalytics
    private let service: LendingGreetingsServicing
    private let presenter: LendingGreetingsPresenting
    private let dependencies: Dependencies
    private var greetingsResponse: GreetingsResponse?
    
    init(service: LendingGreetingsServicing, presenter: LendingGreetingsPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
    
    private func didFetch(greetingsResponse: GreetingsResponse) {
        self.greetingsResponse = greetingsResponse
        let greetings = greetingsResponse.greetings
        guard let termsOfUseURL = greetings.termsConditions, let privacyPolicyURL = greetings.privacyPolicy else {
            return
        }
        presenter.presentLegalTerms(termsOfUseURL: termsOfUseURL, privacyPolicyURL: privacyPolicyURL)
        presenter.presentFooterText()
    }
    
    private func startProposal() {
        guard let data = greetingsResponse else {
            return
        }
        let proposal = Proposal(
            amount: 0.0,
            firstPaymentDeadline: 30
        )
        presenter.didNextStep(action: .proposalObjective(configuration: data, proposal: proposal))
    }
    
    private func sendAnalyticsEvent(for url: URL) {
        switch url {
        case greetingsResponse?.greetings.termsConditions:
            dependencies.analytics.log(LendingGreetingsEvent.termsOfUseButtonTapped(.borrower))
        case greetingsResponse?.greetings.privacyPolicy:
            dependencies.analytics.log(LendingGreetingsEvent.privacyPolicyButtonTapped(.borrower))
        default:
            break
        }
    }
    
    private func handle(error: ApiError) {
        guard
            let requestError = error.requestError,
            let identifiableError = IdentifiableGreetingsError(errorCode: requestError.code)
            else {
                presenter.present(error: error)
                return
        }
        switch identifiableError {
        case .openProposalsLimitReached:
            presenter.didNextStep(action: .maximumOpenProposals(identifiableError))
        case .accountUpgrade:
            presenter.didNextStep(action: .accountUpgrade(identifiableError))
        case .oneActiveProposalLimitReached, .transactionValueExceeded, .activeProposalsLimitReached, .currentlyInvestor:
            presenter.didNextStep(action: .detailedError(identifiableError))
        }
    }
}

extension LendingGreetingsViewModel: LendingGreetingsViewModelInputs {
    func fetchLendingProposalConfiguration() {
        presenter.startLoading()
        service.fetchProposalConfiguration { [weak self] result in
            self?.presenter.stopLoading()
            switch result {
            case .success(let greetingsResponse):
                self?.dependencies.analytics.log(LendingGreetingsEvent.suitableBorrower(.borrower))
                self?.didFetch(greetingsResponse: greetingsResponse)
            case .failure(let error):
                self?.handle(error: error)
            }
        }
    }
    
    func sendTrackingEvent(_ type: LendingGreetingsEvent) {
        dependencies.analytics.log(type)
    }
    
    func awarenessAndAcceptanceOfTermsChanged(_ accepted: Bool) {
        dependencies.analytics.log(LendingGreetingsEvent.termsAndPrivacyPolicyCheckBox(.borrower))
        presenter.setContinueButtonEnabled(accepted)
    }
    
    func showCrednovoDisclaimer() {
        dependencies.analytics.log(LendingGreetingsEvent.crednovo(.borrower))
        presenter.presentCrednovoDisclaimer()
    }
    
    func showLegalTerms(for url: URL) {
        sendAnalyticsEvent(for: url)
        presenter.didNextStep(action: .legalTerms(url: url))
    }
    
    func showHelp() {
        dependencies.analytics.log(LendingGreetingsEvent.helpButtonTapped(.borrower))
        guard let faqURL = URL(string: "picpay://picpay/helpcenter/article/360049386752") else {
            return
        }
        presenter.didNextStep(action: .showFAQ(url: faqURL))
    }
    
    func close() {
        presenter.didNextStep(action: .close)
    }
    
    func makeProposal() {
        startProposal()
    }
}
