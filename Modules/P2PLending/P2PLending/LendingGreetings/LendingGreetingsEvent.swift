import Foundation
import AnalyticsModule

enum LendingGreetingsEvent: AnalyticsKeyProtocol {
    case suitableBorrower(ConsultingAgent)
    case termsOfUseButtonTapped(ConsultingAgent)
    case privacyPolicyButtonTapped(ConsultingAgent)
    case backButtonTapped(ConsultingAgent)
    case termsAndPrivacyPolicyCheckBox(ConsultingAgent)
    case helpButtonTapped(ConsultingAgent)
    case crednovo(ConsultingAgent)
    case closeButtonTapped(ConsultingAgent)
    case continueButtonTapped(ConsultingAgent)
    case deeplink(properties: [String: String] = [:])
    
    private var name: String {
        switch self {
        case .suitableBorrower:
            return AnalyticInteraction.screenViewed.name
        case .termsOfUseButtonTapped,
             .privacyPolicyButtonTapped,
             .backButtonTapped,
             .helpButtonTapped,
             .crednovo,
             .closeButtonTapped,
             .continueButtonTapped:
            return AnalyticInteraction.buttonClicked.name
        case .termsAndPrivacyPolicyCheckBox:
            return AnalyticInteraction.buttonSelected.name
        case .deeplink:
            return AnalyticInteraction.pushNotificationOpened.name
        }
    }

    private var properties: [String: Any] {
        switch self {
        case .suitableBorrower:
            return [AnalyticsKeys.screenName.name: getTitle(for: .borrower), AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description]
        case .termsOfUseButtonTapped(let agent):
            return [
                AnalyticsKeys.screenName.name: getTitle(for: agent),
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.termsOfUse.description
            ]
        case .privacyPolicyButtonTapped(let agent):
            return [
                AnalyticsKeys.screenName.name: getTitle(for: agent),
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.privacyPolicy.description
            ]
            
        case .backButtonTapped(let agent):
            return [
                AnalyticsKeys.screenName.name: getTitle(for: agent),
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.backButton.description
            ]
        case .termsAndPrivacyPolicyCheckBox(let agent):
            return [
                AnalyticsKeys.screenName.name: getTitle(for: agent),
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.checkBoxMarked.description
            ]
            
        case .helpButtonTapped(let agent):
            return [
                AnalyticsKeys.screenName.name: getTitle(for: agent),
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.help.description
            ]
            
        case .crednovo(let agent):
            return [
                AnalyticsKeys.screenName.name: getTitle(for: agent),
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.crednovo.description
            ]
            
        case .closeButtonTapped(let agent):
            return [
                AnalyticsKeys.screenName.name: getTitle(for: agent),
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.backButton.description
            ]
            
        case .continueButtonTapped(let agent):
            return [
                AnalyticsKeys.screenName.name: getTitle(for: agent),
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.checkProposal.description
            ]
        case .deeplink(let properties):
            return getDeeplinkProperties(properties)
        }
    }
    
    private func getTitle(for agent: ConsultingAgent) -> String {
        switch agent {
        case .borrower:
            return "P2P_LENDING_TOMADOR_INCIAR_PROPOSTA"
        case .investor:
            return "P2P_LENDING_INVESTIDOR_AJUDE_SEU_AMIGO"
        }
    }
    
    private func getDeeplinkProperties(_ newProperties: [String: String]) -> [String: Any] {
        let properties = [
            AnalyticsKeys.pushType.name: AnalyticsConstants.p2pLending.description,
            AnalyticsKeys.notificationOpenFrom.name: AnalyticsConstants.push.description
        ]
        return properties.merging(newProperties) { _, new in new }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.eventTracker])
    }
}

enum UnsuitableBorrowerType: String {
    case openProposalsLimitReached = "P2P_LENDING_TOMADOR_ALERTA_3_PROPOSTAS_ABERTAS"
    case oneActiveProposalLimitReached = "P2P_LENDING_TOMADOR_ALERTA_1_EMPRESTIMO_ATIVO"
    case transactionValueReached = "P2P_LENDING_TOMADOR_ALERTA_LIMITE_MENSAL_TRANSACOES"
    case accountUpgrade = "P2P_LENDING_TOMADOR_ALERTA_NAO_POSSUI_UPGRADE"
    case currentlyInvestor = "P2P_LENDING_TOMADOR_FINALIZAR_EMPRESTIMO"
    case activeProposalLimitReached = "P2P_LENDING_TOMADOR_LIMITE_EMPRESTIMOS_ATIVOS"
    
    var description: String {
        rawValue
    }
}
