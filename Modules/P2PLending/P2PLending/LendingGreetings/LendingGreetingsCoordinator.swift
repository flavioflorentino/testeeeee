import SafariServices
import AnalyticsModule
import UIKit
import UI

enum LendingGreetingsAction: Equatable {
    case legalTerms(url: URL)
    case proposalObjective(configuration: GreetingsResponse, proposal: Proposal)
    case detailedError(_ detailedError: IdentifiableGreetingsError)
    case maximumOpenProposals(_ detailedError: IdentifiableGreetingsError)
    case accountUpgrade(_ detailedError: IdentifiableGreetingsError)
    case showFAQ(url: URL)
    case close
}

protocol LendingGreetingsCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: LendingGreetingsAction)
}

final class LendingGreetingsCoordinator: LendingGreetingsCoordinating, URLOpenerCoordinating {
    typealias Dependencies = HasURLOpener & HasAnalytics
    private var dependencies: Dependencies
    
    weak var viewController: UIViewController?
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func perform(action: LendingGreetingsAction) {
        switch action {
        case let .legalTerms(url):
            let safariViewController = SFSafariViewController(url: url)
            viewController?.present(safariViewController, animated: true, completion: nil)
        case let .proposalObjective(data, proposal):
            goToProposalObjective(data: data, proposal: proposal)
        case let .detailedError(detailedError):
            showDetailedErrorFeedbackController(detailedError)
        case let .maximumOpenProposals(detailedError):
            showMaximumOpenProposalsFeedbackController(detailedError)
        case let .accountUpgrade(detailedError):
            showAccountUpgradeFeedbackController(detailedError)
        case let .showFAQ(url):
            showFAQ(for: url, using: dependencies)
        case .close:
            viewController?.dismiss(animated: true, completion: nil)
        }
    }
}

private extension LendingGreetingsCoordinator {
    func makeApolloFeedbackViewController(error: IdentifiableGreetingsError) -> ApolloFeedbackViewController {
        let content = error.asFeedbackViewContent
        viewController?.navigationController?.setNavigationBarHidden(true, animated: true)
        return ApolloFeedbackViewController(content: content)
    }
    
    func showMaximumOpenProposalsFeedbackController(_ error: IdentifiableGreetingsError) {
        let apolloFeedbackController = makeApolloFeedbackViewController(error: error)
        
        apolloFeedbackController.onViewDidAppear = {
            self.dependencies.analytics.log(error.analyticsEvent(for: .screenViewed))
        }
        
        apolloFeedbackController.didTapPrimaryButton = { [weak self] in
            let controller = LendingProposalListFactory.make(for: .borrower)
            apolloFeedbackController.navigationController?.setNavigationBarHidden(false, animated: true)
            apolloFeedbackController.navigationItem.setHidesBackButton(true, animated: true)
            apolloFeedbackController.show(controller, sender: nil)
            self?.dependencies.analytics.log(error.analyticsEvent(for: .primaryAction))
        }
        
        apolloFeedbackController.didTapSecondaryButton = { [weak self] in
            self?.dependencies.analytics.log(error.analyticsEvent(for: .linkAction))
            apolloFeedbackController.dismiss(animated: true, completion: nil)
        }
        
        viewController?.show(apolloFeedbackController, sender: nil)
    }
    
    func showAccountUpgradeFeedbackController(_ error: IdentifiableGreetingsError) {
        let apolloFeedbackController = makeApolloFeedbackViewController(error: error)
        
        apolloFeedbackController.onViewDidAppear = {
            self.dependencies.analytics.log(error.analyticsEvent(for: .screenViewed))
        }
        
        apolloFeedbackController.didTapPrimaryButton = {
            self.viewController?.dismiss(animated: true) { [weak self] in
                guard let self = self else { return }
                guard let url = error.asDetailedError.url else { return }
                self.dependencies.analytics.log(error.analyticsEvent(for: .linkAction))
                self.showFAQ(for: url, using: self.dependencies)
            }
        }
        
        apolloFeedbackController.didTapSecondaryButton = {
            self.dependencies.analytics.log(error.analyticsEvent(for: .primaryAction))
            apolloFeedbackController.dismiss(animated: true, completion: nil)
        }
        
        viewController?.show(apolloFeedbackController, sender: nil)
    }
    
    func showDetailedErrorFeedbackController(_ error: IdentifiableGreetingsError) {
        let apolloFeedbackController = makeApolloFeedbackViewController(error: error)
        
        apolloFeedbackController.onViewDidAppear = {
            self.dependencies.analytics.log(error.analyticsEvent(for: .screenViewed))
        }
        
        apolloFeedbackController.didTapPrimaryButton = {
            apolloFeedbackController.dismiss(animated: true, completion: nil)
            self.dependencies.analytics.log(error.analyticsEvent(for: .primaryAction))
        }
        
        apolloFeedbackController.didTapSecondaryButton = { [weak self] in
            guard let self = self else { return }
            guard let url = error.asDetailedError.url else { return }
            self.showFAQ(for: url, using: self.dependencies)
            self.dependencies.analytics.log(error.analyticsEvent(for: .linkAction))
        }
        
        viewController?.show(apolloFeedbackController, sender: nil)
    }
    
    func goToProposalObjective(data: GreetingsResponse, proposal: Proposal) {
        guard let proposalObjectivesViewController = LendingProposalObjectivesFactory.make(data: data, proposal: proposal) else {
            return
        }
        viewController?.show(proposalObjectivesViewController, sender: nil)
    }
}
