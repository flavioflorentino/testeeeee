import UIKit

public enum LendingGreetingsFactory {
    public static func make(shouldDisplayCloseButton: Bool = false) -> UIViewController {
        let container = DependencyContainer()
        let service: LendingGreetingsServicing = LendingGreetingsService(dependencies: container)
        let coordinator: LendingGreetingsCoordinating = LendingGreetingsCoordinator(dependencies: container)
        let presenter: LendingGreetingsPresenting = LendingGreetingsPresenter(coordinator: coordinator)
        let viewModel = LendingGreetingsViewModel(service: service, presenter: presenter, dependencies: container)
        let viewController = LendingGreetingsViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController
        viewController.shouldDisplayCloseButton = shouldDisplayCloseButton

        return viewController
    }
}
