import Foundation
import Core

protocol LendingGreetingsServicing: Servicing {
    func fetchProposalConfiguration(completion: @escaping ModelCompletionBlock<GreetingsResponse>)
}

final class LendingGreetingsService: LendingGreetingsServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func fetchProposalConfiguration(completion: @escaping ModelCompletionBlock<GreetingsResponse>) {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        let request = Api<GreetingsResponse>(endpoint: P2PLendingEndpoint.proposalConfiguration)
        request.execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
