import UI
import UIKit
import LendingComponents

protocol LendingGreetingsDisplay: AnyObject {
    func displayFooter(footerText: NSAttributedString)
    func displayLegalNotice(attributedText: NSAttributedString)
    func setContinueButtonEnabled(_ isEnabled: Bool)
    func displayAlert(title: String?, message: String?)
    func displayCrednovoDisclaimer()
    func startLoading()
    func stopLoading()
}

final class LendingGreetingsViewController: ViewController<LendingGreetingsViewModelInputs, UIView> {
    private typealias Localizable = Strings.Lending.Greetings
    
    enum AccessibilityIdentifier: String {
        case bodyLabel
        case footerLabel
        case legalTermsCheckbox
        case makeProposalButton
    }
    
    // MARK: Loading
    
    private lazy var activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(style: .gray)
        activityIndicatorView.color = Colors.grayscale600.color
        return activityIndicatorView
    }()
    
    private lazy var loadingLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.loadingText
        label.labelStyle(BodyPrimaryLabelStyle())
        label.textColor = Colors.grayscale300.color
        return label
    }()
    
    private lazy var loadingStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [activityIndicatorView, loadingLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        stackView.alignment = .center
        return stackView
    }()
    
    // MARK: Success
    
    private lazy var headlineImageView: UIImageView = {
        let imageView = UIImageView(image: Assets.lendingFigureGreetings.image)
        imageView.contentMode = .center
        return imageView
    }()
    
    private lazy var bodyLabel: UILabel = {
        let textLabel = UILabel()
        textLabel.setContentHuggingPriority(.defaultLow, for: .horizontal)
        textLabel.accessibilityIdentifier = AccessibilityIdentifier.bodyLabel.rawValue
        textLabel.labelStyle(BodyPrimaryLabelStyle(type: .default))
        textLabel.text = Localizable.body
        textLabel.textColor = Colors.grayscale600.color
        return textLabel
    }()
    
    private lazy var firstBulletItemView: BulletItemView = {
        let bulletView = BulletItemView()
        bulletView.number = 1
        bulletView.title = Localizable.firstBulletDescription
        return bulletView
    }()
    
    private lazy var secondBulletItemView: BulletItemView = {
        let bulletView = BulletItemView()
        bulletView.number = 2
        bulletView.title = Localizable.secondBulletDescription
        return bulletView
    }()
    
    private lazy var thirdBulletItemView: BulletItemView = {
        let bulletView = BulletItemView()
        bulletView.number = 3
        bulletView.title = Localizable.thirdBulletDescription
        return bulletView
    }()
    
    private lazy var footerLabel: TextView = {
        let textView = TextView()
        textView.dataDetectorTypes = .link
        textView.isScrollEnabled = false
        textView.setContentHuggingPriority(.defaultLow, for: .horizontal)
        textView.isEditable = false
        textView.backgroundColor = .clear
        textView.accessibilityIdentifier = AccessibilityIdentifier.footerLabel.rawValue
        textView.delegate = self
        textView.linkTextAttributes = [
            .foregroundColor: Colors.branding600.color,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        return textView
    }()
    
    private lazy var bulletStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [firstBulletItemView, secondBulletItemView, thirdBulletItemView])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var legalTermsCheckboxView: CheckboxView = {
        let checkboxView = CheckboxView()
        checkboxView.addTarget(self, action: #selector(legalNoticeCheckboxValueChanged(_:)), for: .valueChanged)
        checkboxView.textView.delegate = self
        checkboxView.checkboxButton.accessibilityIdentifier = AccessibilityIdentifier.legalTermsCheckbox.rawValue
        return checkboxView
    }()
    
    private lazy var makeProposalButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Strings.Lending.Greetings.confirmButtonTitle, for: .normal)
        button.accessibilityIdentifier = AccessibilityIdentifier.makeProposalButton.rawValue
        button.addTarget(self, action: #selector(makeProposalButtonTapped), for: .touchUpInside)
        button.isEnabled = false
        return button
    }()
    
    private lazy var topStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [headlineImageView, bodyLabel, bulletStackView, footerLabel])
        stackView.spacing = Spacing.base04
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var bottomStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [legalTermsCheckboxView, makeProposalButton])
        stackView.spacing = Spacing.base03
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [topStackView, bottomStackView])
        stackView.spacing = Spacing.base04
        stackView.distribution = .equalSpacing
        stackView.axis = .vertical
        stackView.compatibleLayoutMargins = .rootView
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.isHidden = true
        return scrollView
    }()
    
    private lazy var helpButton: UIBarButtonItem = {
        let button = UIBarButtonItem(image: Assets.lendingGlyphHelp.image,
                                     style: .plain,
                                     target: self,
                                     action: #selector(helpButtonTapped))
        button.accessibilityLabel = GlobalLocalizable.helpButtonAccessibilityLabel
        return button
    }()
    
    private lazy var closeButton = UIBarButtonItem(title: GlobalLocalizable.closeButtonTitle,
                                                   style: .plain,
                                                   target: self,
                                                   action: #selector(close))
    
    var shouldDisplayCloseButton = false

     override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.fetchLendingProposalConfiguration()
    }
    
     override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if isMovingFromParent {
            viewModel.sendTrackingEvent(LendingGreetingsEvent.backButtonTapped(.borrower))
        }
    }
 
    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        view.addSubview(loadingStackView)
        scrollView.addSubview(rootStackView)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        loadingStackView.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
        
        rootStackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.width.equalToSuperview()
        }
        
        rootStackView.layout {
            $0.height >= view.compatibleSafeAreaLayoutGuide.heightAnchor
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        navigationController?.view.backgroundColor = Colors.backgroundPrimary.color
        view.tintColor = Colors.branding600.color
        navigationItem.rightBarButtonItem = helpButton
        if shouldDisplayCloseButton {
            navigationItem.leftBarButtonItem = closeButton
        }
        setupNavigationAppearance()
    }
    
    private func setupNavigationAppearance() {
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .automatic
            navigationController?.navigationBar.prefersLargeTitles = true
            navigationController?.navigationBar.isTranslucent = true
        }
        if #available(iOS 13.0, *) {
            let scrollEdgeAppearance = UINavigationBarAppearance()
            let standardAppearance = UINavigationBarAppearance()
            scrollEdgeAppearance.configureWithTransparentBackground()
            standardAppearance.configureWithDefaultBackground()
            navigationController?.navigationBar.scrollEdgeAppearance = scrollEdgeAppearance
            navigationController?.navigationBar.standardAppearance = standardAppearance
        }
    }
}

extension LendingGreetingsViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        switch textView {
        case footerLabel:
            viewModel.showCrednovoDisclaimer()
        case legalTermsCheckboxView.textView:
            viewModel.showLegalTerms(for: URL)
        default:
            break
        }
        return false
    }
}

// MARK: - Actions
@objc
extension LendingGreetingsViewController {
    func makeProposalButtonTapped() {
        viewModel.makeProposal()
    }
    
    func legalNoticeCheckboxValueChanged(_ checkboxView: CheckboxView) {
        viewModel.awarenessAndAcceptanceOfTermsChanged(checkboxView.isSelected)
    }
    
    func helpButtonTapped() {
        viewModel.showHelp()
    }
    
    func close() {
        viewModel.close()
    }
}

// MARK: - View Model Outputs
extension LendingGreetingsViewController: LendingGreetingsDisplay {
    func display(bodyText: NSAttributedString) {
        bodyLabel.attributedText = bodyText
    }
    
    func displayFooter(footerText: NSAttributedString) {
        footerLabel.attributedText = footerText
    }
    
    func displayLegalNotice(attributedText: NSAttributedString) {
        scrollView.isHidden = false
        title = Localizable.title
        legalTermsCheckboxView.textView.attributedText = attributedText
    }
    
    func setContinueButtonEnabled(_ isEnabled: Bool) {
        makeProposalButton.isEnabled = isEnabled
    }
    
    func displayAlert(title: String?, message: String?) {
        let popUp = PopupViewController(
            title: title,
            description: message,
            preferredType: .image(Assets.lendingFigureFailure.image)
        )
        let dismissAction = PopupAction(title: GlobalLocalizable.dismissButtonTitle, style: .fill) {
            guard self.navigationController?.children.first == self else {
                self.navigationController?.popViewController(animated: true)
                return
            }
            self.dismiss(animated: true, completion: nil)
        }
        popUp.addAction(dismissAction)
        showPopup(popUp)
    }
    
    func displayCrednovoDisclaimer() {
        let contentView = DeclineOfferDialogueContentView(
            title: Localizable.crednovoAlertTitle,
            body: Localizable.crednovoAlertBody
        )
        let alertPopup = PopupViewController(title: nil, preferredType: .customView(contentView, shouldRemoveCloseButton: true))
        alertPopup.addAction(.init(title: GlobalLocalizable.dismissButtonTitle, style: .fill))
        showPopup(alertPopup)
    }
    
    func startLoading() {
        activityIndicatorView.startAnimating()
        loadingStackView.isHidden = false
    }
    
    func stopLoading() {
        activityIndicatorView.stopAnimating()
        loadingStackView.isHidden = true
    }
}
