import Foundation
import Core

enum P2PLendingEndpoint {
    case proposalConfiguration
    case proposalConditionsConfiguration(amount: Double)
    case proposalConditionsSimulation(proposal: Proposal)
    case proposals(agent: ConsultingAgent)
    case friends(scope: SearchScope, query: String?)
    case investorEligibility(friend: LendingFriend)
    case authenticatedSend(proposal: Proposal, pin: String)
    case send(proposal: Proposal)
    case opportunityOffer(identifier: UUID)
    case ccb(proposal: Proposal)
    case declineOffer(identifier: UUID)
    case acceptOffer(identifier: UUID)
    case cancelOffer(identifier: UUID)
    case installmentList(identifier: UUID, agent: ConsultingAgent)
    case installmentDetail(offerIdentifier: UUID, installmentNumber: Int)
}

extension P2PLendingEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .proposalConfiguration:
            return "p2plending/configs"
        case .proposalConditionsConfiguration:
            return "p2plending/loan/simulation/config"
        case .proposalConditionsSimulation:
            return "p2plending/loan/simulation"
        case .proposals:
            return "p2plending/loan/offers"
        case .friends:
            return "search"
        case .investorEligibility(let friend):
            return "p2plending/configs/investor-eligibility/\(friend.id)"
        case .authenticatedSend:
            return "p2plending/loan/offers/v2"
        case .send:
            return "p2plending/loan/offers"
        case .opportunityOffer(let identifier):
            return "p2plending/loan/offers/\(identifier.uuidString)"
        case .ccb:
            return "p2plending/loan/simulation/ccb"
        case .declineOffer(let identifier):
            return "p2plending/loan/offers/\(identifier.uuidString)/investor/reject"
        case .acceptOffer(let identifier):
            return "p2plending/loan/offers/\(identifier.uuidString)/investor/accept"
        case .cancelOffer(let identifier):
            return "p2plending/loan/offers/\(identifier.uuidString)/borrower/cancel"
        case .installmentList(let identifier, _):
            return "p2plending/loan/\(identifier.uuidString)/installments"
        case let .installmentDetail(offerIdentifier, installmentNumber):
            return "p2plending/loan/\(offerIdentifier.uuidString)/installment/\(installmentNumber)"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .proposalConfiguration, .proposalConditionsConfiguration, .friends, .opportunityOffer, .proposals, .installmentList, .installmentDetail, .investorEligibility:
            return .get
        case .proposalConditionsSimulation, .authenticatedSend, .ccb, .send:
            return .post
        case .declineOffer, .acceptOffer, .cancelOffer:
            return .put
        }
    }
    
    var parameters: [String: Any] {
        switch self {
        case .proposalConditionsConfiguration(let amount):
            return ["amount": amount]
        case .proposals(let agent), .installmentList( _, let agent):
            return ["profile": agent.rawValue]
        case let .friends(scope, query):
            var parameters: [String: Any] = ["group": scope.rawValue, "page": 0]
            guard let query = query, query.isNotEmpty else {
                return parameters
            }
            parameters["term"] = query
            return parameters
        default:
            return [:]
        }
    }
    
    var body: Data? {
        switch self {
        case .proposalConditionsSimulation(let proposal), .ccb(let proposal):
            var dict: [AnyHashable: Encodable] = [:]
            dict["amount"] = proposal.amount
            dict["interest"] = proposal.interest
            dict["installments"] = proposal.installments
            return dict.toData()
        case let .authenticatedSend(proposal, _), let .send(proposal):
            var dict: [AnyHashable: Encodable] = [:]
            dict["objective_id"] = proposal.objective?.id
            dict["first_payment"] = proposal.firstPaymentDeadline
            dict["amount"] = proposal.amount
            dict["installments"] = proposal.installments
            dict["interest_rate"] = proposal.interest
            dict["message"] = proposal.message
            dict["accepted_financial_risk_fitness"] = proposal.shouldShareCreditAptitude
            dict["investor_id"] = proposal.friend?.id
            return dict.toData()
        default:
            return nil
        }
    }
    
    var customHeaders: [String: String] {
        switch self {
        case let .authenticatedSend( _, pin):
            return ["password": pin]
        default:
            return [:]
        }
    }
}
