import AnalyticsModule
import AssetsKit

enum IdentifiableGreetingsError: String {
    case transactionValueExceeded = "5101"
    case openProposalsLimitReached = "5102"
    case oneActiveProposalLimitReached = "5103"
    case activeProposalsLimitReached = "5107"
    case accountUpgrade = "5104"
    case currentlyInvestor = "5108"
    
    init?(errorCode: String) {
        self.init(rawValue: errorCode)
    }
}

extension IdentifiableGreetingsError: DetailedErrorConvertible {
    private typealias Localizable = Strings.Lending.DetailedError
    
    var asDetailedError: DetailedError {
        var image = Resources.Illustrations.iluWarning.image
        var title = Localizable.greetingsTitle
        var primaryButtonTitle = Localizable.dismissButtonTitle
        let message: String
        let linkButtonTitle: String
        var url: URL?
        switch self {
        case .transactionValueExceeded:
            message = Localizable.transactionValueExceededBody
            linkButtonTitle = Localizable.transactionValueExceededFAQButtonTitle
            url = URL(string: "picpay://picpay/helpcenter/article/360044294592")
        case .openProposalsLimitReached:
            title = Localizable.openProposalsLimitReachedTitle
            message = Localizable.openProposalsLimitReachedBody
            primaryButtonTitle = Localizable.openProposalsLimitReachedPrimaryButtonTitle
            linkButtonTitle = GlobalLocalizable.dismissButtonTitle
            url = nil
        case .oneActiveProposalLimitReached:
            title = Localizable.activeProposalsLimitReachedTitle
            message = Localizable.oneProposalLimitReached
            linkButtonTitle = Localizable.lendingLimitsFaqButtonTitle
            url = URL(string: "picpay://picpay/helpcenter/article/360049860711")
        case .activeProposalsLimitReached:
            title = Localizable.activeProposalsLimitReachedTitle
            message = Localizable.activeProposalsLimitReachedBody
            linkButtonTitle = Localizable.lendingLimitsFaqButtonTitle
            url = URL(string: "picpay://picpay/helpcenter/article/360049860711")
        case .accountUpgrade:
            image = Assets.lendingFigureAccountUpgrade.image
            title = Localizable.accountUpgradeGreetingsTitle
            message = Localizable.accountUpgradeBody
            primaryButtonTitle = Localizable.accountUpgradePrimaryButtonTitle
            linkButtonTitle = Localizable.accountUpgradeLinkButtonTitle
            url = URL(string: "picpay://picpay/upgradeaccount")
        case .currentlyInvestor:
            title = Localizable.currentlyInvestorTitle
            message = Localizable.currentlyInvestorBody
            linkButtonTitle = Localizable.lendingLimitsFaqButtonTitle
            url = URL(string: "picpay://picpay/helpcenter/article/360049860711")
        }
        return DetailedError(
            image: image,
            title: title,
            message: message,
            primaryButtonTitle: primaryButtonTitle,
            linkButtonTitle: linkButtonTitle,
            url: url
        )
    }
}

extension IdentifiableGreetingsError {
    var properties: [String: Any] {
        switch self {
        case .transactionValueExceeded:
            return [
                AnalyticsKeys.screenName.name: UnsuitableBorrowerType.transactionValueReached.description,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        case .oneActiveProposalLimitReached:
            return [
                AnalyticsKeys.screenName.name: UnsuitableBorrowerType.oneActiveProposalLimitReached.description,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        case .openProposalsLimitReached:
            return [
                AnalyticsKeys.screenName.name: UnsuitableBorrowerType.openProposalsLimitReached.description,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        case .accountUpgrade:
            return [
                AnalyticsKeys.screenName.name: UnsuitableBorrowerType.accountUpgrade.description,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        case .activeProposalsLimitReached:
            return [
                AnalyticsKeys.screenName.name: UnsuitableBorrowerType.activeProposalLimitReached.description,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        case .currentlyInvestor:
            return [
                AnalyticsKeys.screenName.name: UnsuitableBorrowerType.currentlyInvestor.description,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        }
    }
    
    var providers: [AnalyticsProvider] {
        [.eventTracker]
    }
    
    func analyticsEvent(for interaction: InteractionType) -> AnalyticsEvent {
        switch interaction {
        case .primaryAction:
            var buttonProperties = properties
            buttonProperties[AnalyticsKeys.buttonName.name] = primaryButtonName
            return AnalyticsEvent(AnalyticInteraction.buttonClicked.name, properties: buttonProperties, providers: providers)
        case .linkAction:
            var buttonProperties = properties
            buttonProperties[AnalyticsKeys.buttonName.name] = linkButtonName
            return AnalyticsEvent(AnalyticInteraction.buttonClicked.name, properties: buttonProperties, providers: providers)
        case .screenViewed:
            return AnalyticsEvent(AnalyticInteraction.screenViewed.name, properties: properties, providers: providers)
        }
    }
}

extension IdentifiableGreetingsError {
    var linkButtonName: String {
        switch self {
        case .accountUpgrade:
            return AnalyticsConstants.accountUpgrade.description
        case .openProposalsLimitReached:
            return AnalyticsConstants.okButton.description
        default:
            return AnalyticsConstants.seeMoreButton.description
        }
    }
    
    var primaryButtonName: String {
        switch self {
        case .openProposalsLimitReached:
            return AnalyticsConstants.cancelAProposal.description
        default:
            return AnalyticsConstants.okButton.description
        }
    }
}
