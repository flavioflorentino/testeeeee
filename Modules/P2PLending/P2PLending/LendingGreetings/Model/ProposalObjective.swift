import Foundation
import LendingComponents
import UI

struct ProposalObjective: Decodable, Equatable {
    let id: String?
    let imgUrl: URL?
    let title: String?
}

extension ProposalObjective: ObjectiveCellModeling {
    var icon: Iconography? { nil }
    var isDisclosureIndicatorHidden: Bool { false }
}
