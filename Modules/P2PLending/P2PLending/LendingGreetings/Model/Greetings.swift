import Foundation

struct Greetings: Decodable, Equatable {
    let privacyPolicy: URL?
    let termsConditions: URL?
}
