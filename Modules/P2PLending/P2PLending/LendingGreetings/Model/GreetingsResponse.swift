import Foundation
struct GreetingsResponse: Decodable, Equatable {
    let greetings: Greetings
    let proposalConfiguration: ProposalAmountConfiguration?
    let objectives: [ProposalObjective]?
}
