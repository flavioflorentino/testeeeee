struct ProposalAmountConfiguration: Decodable, Equatable {
    let minimumAmount: Double
    let maximumAmount: Double
}
