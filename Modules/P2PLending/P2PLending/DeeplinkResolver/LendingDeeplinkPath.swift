enum LendingDeeplinkPath: String {
    case opportunity = "/p2plending/opportunity"
    case newProposal = "/p2plending/new-proposal"
    case investments = "/p2plending/investments"
    case proposals = "/p2plending/proposals"
    case onboarding = "/p2plending/onboarding"
    case installmentPayment = "/p2plending/installment-payment"
    
    var requiredParameters: [String] {
        switch self {
        case .opportunity:
            return ["identifier"]
        case .installmentPayment:
            return ["identifier", "number"]
        case .newProposal, .investments, .proposals, .onboarding:
            return []
        }
    }
}
