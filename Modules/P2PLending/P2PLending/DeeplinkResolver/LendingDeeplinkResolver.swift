import UIKit
import Foundation
import Core
import FeatureFlag
import AnalyticsModule

public final class LendingDeeplinkResolver: DeeplinkResolver {
    public typealias Dependencies = HasFeatureManager & HasKVStore & HasAnalytics
    private let dependencies: Dependencies
    
    public init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    public func canHandle(url: URL, isAuthenticated: Bool) -> DeeplinkResolverResult {
        guard let deeplinkPath = LendingDeeplinkPath(rawValue: url.path) else {
            return .notHandleable
        }
        guard isAuthenticated else {
            return .onlyWithAuth
        }
        return canHandle(deeplinkPath)
    }
    
    public func open(url: URL, isAuthenticated: Bool) -> Bool {
        guard
            isAuthenticated,
            let deeplink = LendingDeeplinkPath(rawValue: url.path),
            let urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false)
            else {
                return false
        }
        return open(deeplink: deeplink, with: urlComponents.queryItems ?? [])
    }
}

private extension LendingDeeplinkResolver {
    func canHandle(_ path: LendingDeeplinkPath) -> DeeplinkResolverResult {
        dependencies.featureManager.isActive(.releaseP2PLending) ? .handleable : .notHandleable
    }
    
    func open(deeplink: LendingDeeplinkPath, with parameters: [URLQueryItem]) -> Bool {
        guard
            deeplinkContainsAllRequiredParameters(deeplink, parameters),
            let controller = P2PLendingSetup.navigationInstanceHolder?.getCurrentNavigation()
            else {
                return false
        }
        switch deeplink {
        case .opportunity:
            return showOpportunity(parameters: parameters, from: controller)
        case .newProposal:
            return showNewProposal(parameters: parameters, from: controller)
        case .investments:
            return showProposals(for: .investor, from: controller)
        case .proposals:
            return showProposals(for: .borrower, from: controller)
        case .onboarding:
            return showOnboarding(from: controller)
        case .installmentPayment:
            return showInstallmentPayment(parameters: parameters, from: controller)
        }
    }
    
    func deeplinkContainsAllRequiredParameters(_ deeplink: LendingDeeplinkPath, _ parameters: [URLQueryItem]) -> Bool {
        deeplink.requiredParameters.isEmpty || parameters.map(\.name).allSatisfy(deeplink.requiredParameters.contains)
    }
    
    func presentNavigationController(rootViewController: UIViewController, from controller: UIViewController, prefersLargeTitles: Bool = true) {
        let navigationController = UINavigationController(rootViewController: rootViewController)
        if #available(iOS 11.0, *) {
            navigationController.navigationBar.prefersLargeTitles = prefersLargeTitles
        }
        controller.present(navigationController, animated: true)
    }
    
    func showOpportunity(parameters: [URLQueryItem], from controller: UIViewController) -> Bool {
        guard
            let identifierStringValue = parameters["identifier"]?.value,
            let identifier = UUID(uuidString: identifierStringValue)
            else {
                return false
        }
        let investorGreetingsViewController = LendingInvestorGreetingsFactory.make(offerIdentifier: identifier)
        
        let properties = parameters.reduce(into: [:]) { properties, parameter in
            properties[AnalyticsKeys.notificationId.name] = parameter.value
        }
        
        presentNavigationController(rootViewController: investorGreetingsViewController, from: controller)
        dependencies.analytics.log(LendingGreetingsEvent.deeplink(properties: properties))
        return true
    }
    
    func showNewProposal(parameters: [URLQueryItem], from controller: UIViewController) -> Bool {
        let onboardingViewController = LendingGreetingsFactory.make(shouldDisplayCloseButton: true)
        let properties = parameters.reduce(into: [:]) { properties, parameter in
            properties[AnalyticsKeys.notificationId.name] = parameter.value
        }
        presentNavigationController(rootViewController: onboardingViewController, from: controller)
        dependencies.analytics.log(LendingGreetingsEvent.deeplink(properties: properties))
        return true
    }
    
    func showProposals(for agent: ConsultingAgent, from controller: UIViewController) -> Bool {
        let investmentsViewController = LendingProposalListFactory.make(for: agent, shouldDisplayCloseButton: true)
        presentNavigationController(rootViewController: investmentsViewController, from: controller)
        return true
    }
    
    func showOnboarding(from controller: UIViewController) -> Bool {
        let onboardingViewController = LendingOnboardingFactory.make(shouldDisplayCloseButton: true)
        presentNavigationController(rootViewController: onboardingViewController, from: controller)
        return true
    }
    
    func showInstallmentPayment(parameters: [URLQueryItem], from controller: UIViewController) -> Bool {
        guard
            let idStringValue = parameters["identifier"]?.value,
            let identifier = UUID(uuidString: idStringValue),
            let installmentNumberStringValue = parameters["number"]?.value,
            let installmentNumber = Int(installmentNumberStringValue)
        else {
            return false
        }
        
        let installmentDetailController = LendingInstallmentDetailFactory.make(offerIdentifier: identifier,
                                                                               installmentNumber: installmentNumber)
        
        presentNavigationController(rootViewController: installmentDetailController, from: controller, prefersLargeTitles: false)
        return true
    }
}
