import UI
import UIKit

extension SummaryView.Layout {
    enum Insets {
        static let defaultLayoutMargins = UIEdgeInsets(
            top: Spacing.base02,
            left: Spacing.base02,
            bottom: Spacing.base02,
            right: Spacing.base02
        )
        @available(iOS 11, *)
        static let defaultDirectionalLayoutMargins = NSDirectionalEdgeInsets(
            top: Spacing.base02,
            leading: Spacing.base02,
            bottom: Spacing.base02,
            trailing: Spacing.base02
        )
    }
}

public final class SummaryView: UIView {
    fileprivate enum Layout {}
    
    public lazy var textLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.setContentCompressionResistancePriority(.required, for: .vertical)
        return label
    }()
    
    private lazy var activityIndicatorView = UIActivityIndicatorView(style: .gray)
    
    private(set) var isLoading = false
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        buildLayout()
    }
}

public extension SummaryView {
    func startLoadingAnimation() {
        textLabel.isHidden = true
        isLoading = true
        activityIndicatorView.startAnimating()
    }
    
    func stopLoadingAnimation() {
        textLabel.isHidden = false
        isLoading = false
        activityIndicatorView.stopAnimating()
    }
}

extension SummaryView: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(textLabel)
        addSubview(activityIndicatorView)
    }
    
    public func setupConstraints() {
        textLabel.layout {
            $0.top == layoutMarginsGuide.topAnchor
            $0.trailing == layoutMarginsGuide.trailingAnchor
            $0.bottom == layoutMarginsGuide.bottomAnchor
            $0.leading == layoutMarginsGuide.leadingAnchor
        }
        
        activityIndicatorView.layout {
            $0.top == layoutMarginsGuide.topAnchor
            $0.trailing == layoutMarginsGuide.trailingAnchor
            $0.bottom == layoutMarginsGuide.bottomAnchor
            $0.leading == layoutMarginsGuide.leadingAnchor
        }
    }
    
    public func configureViews() {
        if #available(iOS 11, *) {
            directionalLayoutMargins = Layout.Insets.defaultDirectionalLayoutMargins
        } else {
            layoutMargins = Layout.Insets.defaultLayoutMargins
        }
        backgroundColor = Colors.backgroundSecondary.color
    }
    
    public func configureStyles() {
        viewStyle(RoundedViewStyle(cornerRadius: .strong))
    }
}
