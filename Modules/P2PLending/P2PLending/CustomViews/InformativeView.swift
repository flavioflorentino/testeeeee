import UI
import UIKit

extension InformativeView.Layout {
    enum Insets {
        static let layoutMargins = UIEdgeInsets(
            top: Spacing.base02,
            left: Spacing.base02,
            bottom: Spacing.base02,
            right: Spacing.base02
        )
    }
}

final class InformativeView: UIView {
    fileprivate enum Layout {}
    
    // MARK: Layout components
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView(image: image)
        imageView.isHidden = image == nil
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = title
        label.isHidden = title == nil
        label.numberOfLines = 0
        label.textAlignment = .center
        label.labelStyle(TitleLabelStyle(type: .medium))
        return label
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.text = message
        label.isHidden = message == nil
        label.numberOfLines = 0
        label.textAlignment = .center
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale600())
        return label
    }()
    
    private lazy var textContentStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [titleLabel, messageLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [imageView, textContentStackView])
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = Spacing.base03
        stackView.preservesSuperviewLayoutMargins = true
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.layoutMargins = Layout.Insets.layoutMargins
        stackView.isUserInteractionEnabled = true
        return stackView
    }()
    
    // MARK: Properties
    
    var image: UIImage? {
        didSet {
            imageView.image = image
            imageView.isHidden = image == nil
        }
    }
    var title: String? {
        didSet {
            titleLabel.text = title
            titleLabel.isHidden = title == nil
        }
    }
    var message: String? {
        didSet {
            messageLabel.text = message
            messageLabel.isHidden = message == nil
        }
    }
    
    // MARK: Life cycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    init(image: UIImage? = nil, title: String? = nil, message: String? = nil) {
        self.image = image
        self.title = title
        self.message = message
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Style
    
    func setTitleLabelStyle<S: LabelStyle>(_ style: S) {
        titleLabel.labelStyle(style)
    }
    
    func setMessageLabelStyle<S: LabelStyle>(_ style: S) {
        messageLabel
            .labelStyle(style)
            .with(\.textColor, .grayscale600())
    }
}
// MARK: View Configuration
extension InformativeView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(rootStackView)
    }
    
    func setupConstraints() {
        rootStackView.snp.makeConstraints {
            $0.leading.trailing.centerY.equalToSuperview()
            $0.height.lessThanOrEqualToSuperview()
        }
    }
    
    func configureViews() {
        isSkeletonable = false
    }
}
