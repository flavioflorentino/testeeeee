import UI
import UIKit

extension ImportantNoticeView.Layout {
    enum Insets {
        static let defaultMargins = EdgeInsets(
            top: Spacing.base02,
            leading: Spacing.base02,
            bottom: Spacing.base02,
            trailing: Spacing.base02
        )
    }
}

final class ImportantNoticeView: UIView {
    fileprivate enum Layout {}
    
    lazy var iconImageView: UIImageView = {
        let imageView = UIImageView(image: Assets.lendingGlyphExclamation.image)
        imageView.setContentHuggingPriority(.required, for: .horizontal)
        imageView.setContentHuggingPriority(.required, for: .vertical)
        imageView.setContentCompressionResistancePriority(.required, for: .horizontal)
        imageView.setContentCompressionResistancePriority(.required, for: .vertical)
        return imageView
    }()
    
    lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
        label.textColor = Colors.grayscale700.color
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [iconImageView, messageLabel])
        stackView.spacing = Spacing.base01
        stackView.alignment = .top
        return stackView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ImportantNoticeView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(stackView)
    }
    
    func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.directionalEdges.equalTo(layoutMarginsGuide)
        }
    }
    
    func configureViews() {
        compatibleLayoutMargins = Layout.Insets.defaultMargins
        backgroundColor = Colors.backgroundSecondary.color
        cornerRadius = .medium
    }
}
