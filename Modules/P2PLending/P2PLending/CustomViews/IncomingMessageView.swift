import AssetsKit
import UI
import UIKit

extension IncomingMessageView.Layout {
    enum Size {
        static let profile = CGSize(width: 32, height: 32)
    }
    
    enum Length {
        static let profileCornerRadius: CGFloat = 16
    }
}

protocol MessageViewDisplay {
    func setMessage(_ message: String?, for sender: PersonConvertible?)
}

final class IncomingMessageView: UIView {
    fileprivate enum Layout {}
    
    private lazy var profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = Layout.Length.profileCornerRadius
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    private lazy var messageView = MessageBubbleView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension IncomingMessageView: MessageViewDisplay {
    func setMessage(_ message: String?, for sender: PersonConvertible?) {
        messageView.message = message
        profileImageView.setImage(url: sender?.imageURL, placeholder: Resources.Placeholders.greenAvatarPlaceholder.image)
    }
}

extension IncomingMessageView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(profileImageView)
        addSubview(messageView)
    }
    
    func setupConstraints() {
        profileImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.profile)
            $0.leading.bottom.equalToSuperview()
            $0.trailing.equalTo(messageView.snp.leading).offset(Spacing.base00)
        }
        
        messageView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.trailing.lessThanOrEqualToSuperview()
        }
    }
}
