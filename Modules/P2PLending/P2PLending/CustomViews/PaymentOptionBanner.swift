import UI
import UIKit

protocol PaymentOptionBannerDelegate: AnyObject {
    func paymentOptionBannerTapped(_ banner: PaymentOptionBanner)
}

struct PaymentOption: Equatable {
    let icon: UIImage?
    let title: String
    let description: String?
}

extension PaymentOptionBanner.Layout {
    enum Inset {
        static let defaultMargins = EdgeInsets(
            top: Spacing.base02,
            leading: Spacing.base02,
            bottom: Spacing.base02,
            trailing: Spacing.base02
        )
    }
}

final class PaymentOptionBanner: UIControl {
    fileprivate enum Layout {}
    
    weak var delegate: PaymentOptionBannerDelegate?
    
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView(image: paymentOption.icon)
        imageView.setContentHuggingPriority(.required, for: .horizontal)
        imageView.setContentHuggingPriority(.required, for: .vertical)
        imageView.setContentCompressionResistancePriority(.required, for: .horizontal)
        imageView.setContentCompressionResistancePriority(.required, for: .vertical)
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
        label.setContentHuggingPriority(.defaultLow, for: .horizontal)
        label.text = paymentOption.title
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
        label.textColor = Colors.grayscale700.color
        label.setContentHuggingPriority(.defaultLow, for: .horizontal)
        label.text = paymentOption.description
        return label
    }()
    
    private lazy var textStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [titleLabel, descriptionLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base00
        return stackView
    }()
    
    private lazy var chevronImageView: UIImageView = {
        let imageView = UIImageView(image: Assets.lendingGlyphRightChrevron.image)
        imageView.setContentHuggingPriority(.required, for: .horizontal)
        imageView.setContentHuggingPriority(.required, for: .vertical)
        imageView.setContentCompressionResistancePriority(.required, for: .horizontal)
        imageView.setContentCompressionResistancePriority(.required, for: .vertical)
        return imageView
    }()
    
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [iconImageView, textStackView, chevronImageView])
        stackView.alignment = .center
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    var paymentOption: PaymentOption {
        didSet {
            setup(for: paymentOption)
        }
    }
    
    init(paymentOption: PaymentOption) {
        self.paymentOption = paymentOption
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup(for paymentOption: PaymentOption) {
        iconImageView.image = paymentOption.icon
        titleLabel.text = paymentOption.title
        descriptionLabel.text = paymentOption.description
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard #available(iOS 13, *), traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) else {
            return
        }
        viewStyle(CardViewStyle())
    }
}

extension PaymentOptionBanner: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(rootStackView)
    }
    
    func setupConstraints() {
        rootStackView.snp.makeConstraints {
            $0.directionalEdges.equalTo(layoutMarginsGuide)
        }
    }
    
    func configureViews() {
        compatibleLayoutMargins = Layout.Inset.defaultMargins
        viewStyle(CardViewStyle())
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(bannerTapped)))
    }
}

@objc
private extension PaymentOptionBanner {
    func bannerTapped() {
        delegate?.paymentOptionBannerTapped(self)
    }
}
