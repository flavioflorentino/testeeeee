import UI
import UIKit

extension SeparatorView.Layout {
    static let intrinsicContentSize = CGSize(width: 1, height: 1)
}

final class SeparatorView: UIView {
    fileprivate enum Layout {}
    
    override var intrinsicContentSize: CGSize {
        Layout.intrinsicContentSize
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = Colors.grayscale100.color
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
