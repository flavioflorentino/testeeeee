import AssetsKit
import UI
import UIKit

extension LendingFriendView.Layout {
    enum Size {
        static let photo = CGSize(width: 40, height: 40)
        static let labelHeight = 20
    }
}

final class LendingFriendView: UIView {
    fileprivate enum Layout {}
    
    private lazy var photoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.backgroundColor = Colors.backgroundSecondary.color
        imageView.layer.cornerRadius = Layout.Size.photo.height / 2
        imageView.isSkeletonable = true
        return imageView
    }()
    
    private lazy var usernameLabel: UILabel = {
        let label = UILabel()
        label.text = " "
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
        label.textColor = Colors.black.color
        label.linesCornerRadius = Int(CornerRadius.medium)
        label.layer.masksToBounds = true
        label.isSkeletonable = true
        return label
    }()
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.text = " "
        label.labelStyle(BodySecondaryLabelStyle())
        label.textColor = Colors.grayscale600.color
        label.linesCornerRadius = Int(CornerRadius.medium)
        label.layer.masksToBounds = true
        label.isSkeletonable = true
        return label
    }()
    
    private lazy var textContentStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [usernameLabel, nameLabel])
        stackView.axis = .vertical
        stackView.isSkeletonable = true
        return stackView
    }()
    
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [photoImageView, textContentStackView])
        stackView.spacing = Spacing.base02
        stackView.alignment = .center
        stackView.isSkeletonable = true
        return stackView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(with friend: LendingFriend) {
        photoImageView.setImage(url: friend.profileUrl, placeholder: Resources.Placeholders.greenAvatarPlaceholder.image)
        usernameLabel.text = "@\(friend.userName)"
        nameLabel.text = friend.name
    }
    
    func cancelImageRequest() {
        photoImageView.cancelRequest()
        photoImageView.image = nil
    }
}

extension LendingFriendView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(rootStackView)
    }
    
    func setupConstraints() {
        nameLabel.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.labelHeight).priority(.high)
        }
        
        usernameLabel.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.labelHeight).priority(.high)
        }
        
        rootStackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        photoImageView.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.photo.height).priority(.high)
            $0.width.equalTo(Layout.Size.photo.width)
        }
    }
    
    func configureViews() {
        backgroundColor = .clear
        isSkeletonable = true
    }
}
