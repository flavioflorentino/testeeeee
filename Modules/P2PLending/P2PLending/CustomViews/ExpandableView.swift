import UI
import UIKit

protocol ExpandableViewDelegate: AnyObject {
    func expandableViewDidExpand(_ expandableView: ExpandableView)
    func expandableViewDidCollapse(_ expandableView: ExpandableView)
}

extension ExpandableView.Layout {
    enum Insets {
        static let innerLayoutMargins = EdgeInsets(
            top: Spacing.base02,
            leading: Spacing.base02,
            bottom: .zero,
            trailing: Spacing.base02
        )
    }
    
    enum Length {
        static let minimumContainerHeight: CGFloat = 16
        static let separatorHeight: CGFloat = 1
    }
    
    enum Size {
        static let seeMoreArrow = CGSize(width: 24, height: 24)
    }
}

final class ExpandableView: UIView {
    fileprivate enum Layout { }
    weak var delegate: ExpandableViewDelegate?
    
    private typealias Localizable = Strings.Lending.Proposal.Summary
    
    private lazy var headerContainerView = UIView()
    private lazy var contentContainerView: UIView = {
        let view = UIView()
        view.isHidden = !isExpanded
        return view
    }()
    
    private lazy var separatorView: UIView = {
        let separator = UIView()
        separator.backgroundColor = Colors.grayscale100.color
        separator.isHidden = !isExpanded
        return separator
    }()
    
    private(set) lazy var expandButton: UIButton = {
        let button = UIButton()
        let angleUpIcon = icon(from: .angleUp)
        let angleDownIcon = icon(from: .angleDown)
        button.setTitle(Localizable.seeMoreButtonTitle, for: .normal)
        button.setTitle(Localizable.seeLessButtonTitle, for: .selected)
        button.setTitle(Localizable.seeMoreButtonTitle, for: [.normal, .highlighted])
        button.setTitle(Localizable.seeLessButtonTitle, for: [.selected, .highlighted])
        button.setImage(angleDownIcon, for: .normal)
        button.setImage(angleUpIcon, for: .selected)
        button.setImage(angleDownIcon, for: [.normal, .highlighted])
        button.setImage(angleUpIcon, for: [.selected, .highlighted])
        button.semanticContentAttribute = .forceRightToLeft
        button.buttonStyle(LinkButtonStyle(size: .default))
        button.addTarget(self, action: #selector(expandButtonTapped(_:)), for: .touchUpInside)
        return button
    }()
    
    private lazy var bottomContainerView = UIView()
    
    private lazy var rootStackView: UIStackView = {
        let content: [UIView] = [
            headerContainerView,
            separatorView,
            contentContainerView,
            bottomContainerView
        ]
        let stackView = UIStackView(arrangedSubviews: content)
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.compatibleLayoutMargins = Layout.Insets.innerLayoutMargins
        return stackView
    }()
    
    var isExpanded: Bool {
        expandButton.isSelected
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Public
    
    func setHeaderView(_ headerView: UIView) {
        setup(view: headerView, inside: headerContainerView)
    }
    
    func setContentView(_ contentView: UIView) {
        setup(view: contentView, inside: contentContainerView)
    }
    
    func setExpanded(_ expanded: Bool) {
        guard isExpanded != expanded else {
            return
        }
        expandButtonTapped(expandButton)
    }
    
    // MARK: Private
    
    private func setup(view: UIView, inside containerView: UIView) {
        containerView.subviews.forEach { $0.removeFromSuperview() }
        containerView.addSubview(view)
        view.snp.makeConstraints { $0.edges.equalToSuperview() }
    }
    
    private func lastParent(for view: UIView) -> UIView? {
        guard let superview = view.superview else {
            return nil
        }
        return lastParent(for: superview) ?? view
    }
    
    private func icon(from iconography: Iconography) -> UIImage? {
        UIImage(iconName: iconography.rawValue,
                font: Typography.icons(.medium).font(),
                color: Colors.branding600.color,
                size: Layout.Size.seeMoreArrow)
    }
}

extension ExpandableView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(rootStackView)
        bottomContainerView.addSubview(expandButton)
    }
    
    func setupConstraints() {
        rootStackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        headerContainerView.snp.makeConstraints {
            $0.height.greaterThanOrEqualTo(Layout.Length.minimumContainerHeight)
        }
        
        separatorView.snp.makeConstraints {
            $0.height.equalTo(Layout.Length.separatorHeight)
        }
        
        contentContainerView.snp.makeConstraints {
            $0.height.greaterThanOrEqualTo(Layout.Length.minimumContainerHeight)
        }
        
        expandButton.snp.makeConstraints {
            $0.top.bottom.centerX.equalToSuperview()
        }
    }
    
    func configureViews() {
        viewStyle(RoundedViewStyle())
            .with(\.border, .light(color: .grayscale100()))
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard #available(iOS 13, *), traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) else {
            return
        }
        layer.borderColor = Colors.grayscale100.color.cgColor
    }
}

@objc
private extension ExpandableView {
    func expandButtonTapped(_ sender: UIButton) {
        sender.isSelected.toggle()
        sender.isSelected ? delegate?.expandableViewDidExpand(self) : delegate?.expandableViewDidCollapse(self)
        contentContainerView.isHidden = !sender.isSelected
        separatorView.isHidden = !sender.isSelected
        let viewToAnimate = lastParent(for: self)
        let propertyAnimator = UIViewPropertyAnimator(duration: 0.3, curve: .easeInOut) {
            viewToAnimate?.layoutIfNeeded()
        }
        propertyAnimator.startAnimation()
    }
}
