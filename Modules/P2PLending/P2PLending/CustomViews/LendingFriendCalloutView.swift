import UI
import UIKit

extension LendingFriendCalloutView.Layout {
    enum Size {
        static let profileImage = CGSize(width: 48, height: 48)
    }
    
    enum Length {
        static let profileImageRadius = Size.profileImage.width / 2
    }
}

final class LendingFriendCalloutView: UIView {
    fileprivate enum Layout {}
    
    private typealias Localizable = Strings.Lending.Opportunity.Offer
    
    lazy var profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = Layout.Length.profileImageRadius
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    lazy var calloutTextLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale600())
        return label
    }()
    
    private lazy var horizontalStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [profileImageView, calloutTextLabel])
        stackView.spacing = Spacing.base01
        stackView.alignment = .center
        return stackView
    }()
    
    lazy var messageLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale600())
        return label
    }()
    
    private lazy var verticalStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [horizontalStackView, messageLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension LendingFriendCalloutView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(verticalStackView)
    }
    
    func setupConstraints() {
        verticalStackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        profileImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.profileImage)
        }
    }
}
