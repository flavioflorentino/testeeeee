import UI
import UIKit

protocol StatusDisplayable {
    var title: String { get }
    var color: UIColor { get }
    var icon: Iconography { get }
}

final class StatusView: UIView {
    fileprivate enum Layout {}
    var statusDisplayModel: StatusDisplayable? {
        didSet {
            titleLabel.text = statusDisplayModel?.title
            iconLabel.text = statusDisplayModel?.icon.rawValue
            iconLabel.textColor = statusDisplayModel?.color
            layer.borderColor = statusDisplayModel?.color.cgColor
        }
    }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.grayscale700.color)
            .with(\.text, statusDisplayModel?.title)
        label.setContentCompressionResistancePriority(.required, for: .horizontal)
        label.setContentHuggingPriority(.required, for: .horizontal)
        return label
    }()
    
    private lazy var iconLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(IconLabelStyle(type: .small))
            .with(\.textColor, statusDisplayModel?.color)
            .with(\.text, statusDisplayModel?.icon.rawValue)
        label.setContentHuggingPriority(.required, for: .horizontal)
        label.setContentCompressionResistancePriority(.required, for: .horizontal)
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [iconLabel, titleLabel])
        stackView.spacing = Spacing.base01
        stackView.alignment = .center
        return stackView
    }()
    
    init(status: StatusDisplayable? = nil) {
        super.init(frame: .zero)
        self.statusDisplayModel = status
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension StatusView: ViewConfiguration {
    func configureStyles() {
        viewStyle(RoundedViewStyle(cornerRadius: .medium))
            .with(\.layer.borderWidth, Border.light)
            .with(\.layer.borderColor, statusDisplayModel?.color.cgColor)
        compatibleLayoutMargins = EdgeInsets(
            top: Spacing.base01,
            leading: Spacing.base01,
            bottom: Spacing.base01,
            trailing: Spacing.base01
        )
    }
    
    func buildViewHierarchy() {
        addSubview(stackView)
    }
    
    func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.top.bottom.centerX.equalTo(layoutMarginsGuide)
            $0.trailing.lessThanOrEqualTo(layoutMarginsGuide)
            $0.leading.greaterThanOrEqualTo(layoutMarginsGuide)
        }
    }
}
