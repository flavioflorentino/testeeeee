import UI
import UIKit

extension BulletItemView.Layout {
    enum Size {
        static let bullet = CGSize(width: 20, height: 20)
    }
    
    enum Length {
        static let bulletCornerRadius: CGFloat = 10
        static let bulletBorderWidth: CGFloat = 1
    }
}

final class BulletItemView: UIView {
    fileprivate enum Layout {}
    
    private lazy var bulletLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
        label.textColor = Colors.neutral600.color
        label.textAlignment = .center
        label.layer.cornerRadius = Layout.Length.bulletCornerRadius
        label.layer.borderWidth = Layout.Length.bulletBorderWidth
        label.layer.borderColor = Colors.neutral600.color.cgColor
        return label
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
        label.textColor = Colors.grayscale600.color
        label.setContentCompressionResistancePriority(.required, for: .vertical)
        return label
    }()
    
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [bulletLabel, titleLabel])
        stackView.alignment = .top
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    var number: Int? {
        didSet {
            bulletLabel.text = number?.description
        }
    }
    
    var title: String? = nil {
        didSet {
            titleLabel.text = title
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension BulletItemView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(rootStackView)
    }
    
    func setupConstraints() {
        rootStackView.snp.makeConstraints {
            $0.directionalEdges.equalToSuperview()
        }
        
        bulletLabel.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.bullet)
        }
    }
}
