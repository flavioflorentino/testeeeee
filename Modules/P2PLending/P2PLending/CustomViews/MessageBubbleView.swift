import UI
import UIKit

extension MessageBubbleView.Layout {
    enum Insets {
        static let message = EdgeInsets(top: 8, leading: 17, bottom: 8, trailing: 8)
    }
}

final class MessageBubbleView: UIView {
    fileprivate enum Layout {}
    
    private lazy var bubbleImageView: UIImageView = {
        let imageView = UIImageView(image: Assets.lendingBackgroundBubble.image)
        imageView.tintColor = Colors.backgroundSecondary.color
        return imageView
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
        label.textColor = Colors.grayscale600.color
        return label
    }()
    
    var message: String? {
        set {
            messageLabel.text = newValue
        }
        get {
            messageLabel.text
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension MessageBubbleView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(bubbleImageView)
        addSubview(messageLabel)
    }
    
    func setupConstraints() {
        bubbleImageView.snp.makeConstraints {
            $0.directionalEdges.equalToSuperview()
        }
        
        messageLabel.snp.makeConstraints {
            $0.directionalEdges.equalTo(layoutMarginsGuide)
        }
    }
    
    func configureViews() {
        compatibleLayoutMargins = Layout.Insets.message
    }
}
