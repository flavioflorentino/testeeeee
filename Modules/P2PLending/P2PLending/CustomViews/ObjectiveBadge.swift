import UI
import UIKit

protocol ObjectiveBadgeViewModel {
    var imageUrl: URL? { get }
    var title: String? { get }
}

extension ObjectiveBadge.Layout {
    enum Size {
        static let objectiveImage = CGSize(width: 16, height: 16)
    }
}

final class ObjectiveBadge: UIView {
    fileprivate enum Layout { }
    
    private lazy var objectiveLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.Lending.Proposal.Description.objectiveText
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale600())
        label.setContentHuggingPriority(.required, for: .horizontal)
        return label
    }()
    
    private lazy var objectiveImageView = UIImageView()
    
    private lazy var objectiveNameLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale600())
        return label
    }()
    
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [objectiveLabel, objectiveImageView, objectiveNameLabel])
        stackView.spacing = Spacing.base00
        stackView.alignment = .center
        return stackView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(for objective: ObjectiveBadgeViewModel) {
        objectiveImageView.setImage(url: objective.imageUrl)
        objectiveNameLabel.text = objective.title
    }
}

extension ObjectiveBadge: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(rootStackView)
    }
    
    func setupConstraints() {
        rootStackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        objectiveImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.objectiveImage)
        }
    }
}
