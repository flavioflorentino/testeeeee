import UI
import UIKit

extension StepperField.Layout {
    enum Length {
        static let separatorHeight: CGFloat = 1
    }
}

final class StepperField: UIView {
    fileprivate enum Layout {}
    
    private lazy var headlineLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .small))
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
        return label
    }()
    
    private lazy var footerLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
            .with(\.textColor, Colors.grayscale700.color)
        return label
    }()
    
    private(set) lazy var stepper = Stepper()
    
    private lazy var valueStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [descriptionLabel, stepper])
        stackView.alignment = .center
        stackView.distribution = .equalSpacing
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var rootStackView: UIStackView = {
        let arrangedSubviews: [UIView] = [
            headlineLabel,
            valueStackView,
            SeparatorView(),
            footerLabel
        ]
        let stackView = UIStackView(arrangedSubviews: arrangedSubviews)
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    var headlineText: String? {
        didSet {
            headlineLabel.text = headlineText
        }
    }
    
    var descriptionText: String? {
        didSet {
            descriptionLabel.text = descriptionText
        }
    }
    
    var footerText: String? {
        didSet {
            footerLabel.text = footerText
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension StepperField: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(rootStackView)
    }
    
    func setupConstraints() {
        rootStackView.layout {
            $0.top == topAnchor
            $0.trailing == trailingAnchor
            $0.bottom == bottomAnchor
            $0.leading == leadingAnchor
        }
    }
}
