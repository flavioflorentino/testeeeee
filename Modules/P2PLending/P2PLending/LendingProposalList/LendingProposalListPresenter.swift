import Foundation
import FeatureFlag
import UI

protocol LendingProposalListPresenting: AnyObject {
    var viewController: LendingProposalListDisplay? { get set }
    func presentHeader(for agent: ConsultingAgent)
    func setReferenceDate(_ date: Date)
    func presentOnGoing(proposals: [ProposalListItem], for agent: ConsultingAgent)
    func presentTerminated(proposals: [ProposalListItem], for agent: ConsultingAgent)
    func setOnGoingEmptyStateVisibility(isEmpty: Bool)
    func setTerminatedEmptyStateVisibility(isEmpty: Bool)
    func present(error: Error)
    func startLoading()
    func stopLoading()
    func didNextStep(action: LendingProposalListAction)
}

final class LendingProposalListPresenter {
    private let coordinator: LendingProposalListCoordinating
    weak var viewController: LendingProposalListDisplay?
    
    private typealias Localizable = Strings.Lending.Proposal.List
    typealias Dependencies = HasFeatureManager
    
    private let dependencies: Dependencies
    private let dayTimeInterval: TimeInterval = 86_400
    private let locale = Locale(identifier: "pt-BR")
    
    private lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .none
        formatter.locale = locale
        return formatter
    }()
    
    private lazy var dateComponentsFormatter: DateComponentsFormatter = {
        var calendar = Calendar.current
        calendar.locale = locale

        let formatter = DateComponentsFormatter()
        formatter.calendar = calendar
        formatter.allowedUnits = [.hour, .minute, .second]
        formatter.collapsesLargestUnit = true
        formatter.maximumUnitCount = 1
        formatter.zeroFormattingBehavior = .dropAll
        formatter.unitsStyle = .full
        return formatter
    }()
    
    private lazy var percentFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.locale = locale
        formatter.numberStyle = .percent
        formatter.maximumFractionDigits = 1
        formatter.positivePrefix = formatter.plusSign
        return formatter
    }()
    
    private var referenceDate: Date?

    init(coordinator: LendingProposalListCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
}

// MARK: - LendingProposalListPresenting
extension LendingProposalListPresenter: LendingProposalListPresenting {
    func presentHeader(for agent: ConsultingAgent) {
        let title, headline: String
        switch agent {
        case .borrower:
            title = Localizable.borrowerTitle
            headline = Localizable.borrowerHeadline
        case .investor:
            title = Localizable.investorTitle
            headline = Localizable.investorHeadline
        }
        viewController?.display(title: title, headline: headline)
    }
    
    func setReferenceDate(_ date: Date) {
        referenceDate = date
    }
    
    func presentOnGoing(proposals: [ProposalListItem], for agent: ConsultingAgent) {
        let proposalsViewModel = proposals.map { item in
            proposalListItemViewModel(for: item, with: agent)
        }
        viewController?.displayOnGoing(proposals: proposalsViewModel)
    }
    
    func presentTerminated(proposals: [ProposalListItem], for agent: ConsultingAgent) {
        let proposalsViewModel = proposals.map { item in
            proposalListItemViewModel(for: item, with: agent)
        }
        viewController?.displayTerminated(proposals: proposalsViewModel)
    }
    
    func setOnGoingEmptyStateVisibility(isEmpty: Bool) {
        guard isEmpty else {
            viewController?.hideEmptyStateForOnGoingList()
            return
        }
        
        viewController?.displayEmptyStateForOnGoingList(title: Localizable.emptyListOnGoingTitle)
    }
    
    func setTerminatedEmptyStateVisibility(isEmpty: Bool) {
        guard isEmpty else {
            viewController?.hideEmptyStateForTerminatedList()
            return
        }
        
        viewController?.displayEmptyStateForTerminatedList(title: Localizable.emptyListTerminatedTitle)
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
    
    func present(error: Error) {
        viewController?.display(error: error)
    }
    
    func didNextStep(action: LendingProposalListAction) {
        coordinator.perform(action: action)
    }
}

private extension LendingProposalListPresenter {
    func proposalListItemViewModel(for proposalListItem: ProposalListItem, with agent: ConsultingAgent) -> ProposalListItemViewModel {
        ProposalListItemViewModel(identifier: proposalListItem.uuid,
                                  friendProfilePhotoURL: proposalListItem.user.imageURL,
                                  friendStatusCallout: calloutAttributedString(for: proposalListItem, with: agent),
                                  requestedAmount: proposalListItem.amount.toCurrencyString(),
                                  totalAmountTitle: totalAmountTitle(for: proposalListItem, and: agent),
                                  totalAmountValue: formattedTotalAmountValue(for: proposalListItem),
                                  formattedDate: formattedDate(for: proposalListItem),
                                  shouldDisplayInstallmentsButton: shouldDisplayInstallmentsButton(for: proposalListItem, and: agent))
    }
    
    func calloutAttributedString(for proposalListItem: ProposalListItem, with agent: ConsultingAgent) -> NSAttributedString {
        let person = proposalListItem.user
        let builder = OpportunityOfferStatusDescriptionBuilder(
            person: person,
            descriptionProvider: statusDescriptionProvider(for: agent)
        )
        return builder.opportunityOfferStatusDescription(for: proposalListItem.status)
    }
    
    func statusDescriptionProvider(for agent: ConsultingAgent) -> OpportunityOfferStatusDescriptionProvider {
        switch agent {
        case .borrower:
            return BorrowerOpportunityOfferStatusDescriptionProvider()
        case .investor:
            return InvestorOpportunityOfferStatusDescriptionProvider()
        }
    }
    
    func totalAmountTitle(for proposalListItem: ProposalListItem, and agent: ConsultingAgent) -> String? {
        switch proposalListItem.status {
        case .waitingReply, .accepted, .processingPayment, .active:
            return agent == .borrower ? Localizable.totalAmountNotPaidBorrowerTitle : Localizable.totalAmountNotPaidInvestorTitle
        case .liquidated:
            return agent == .borrower ? Localizable.totalAmountPaidBorrowerTitle : Localizable.totalAmountPaidInvestorTitle
        default:
            return Localizable.totalAmountPlusTaxTitle
        }
    }
    
    func formattedTotalAmountValue(for proposalListItem: ProposalListItem) -> String? {
        var totalAmountAndCET = proposalListItem.totalInterest.toCurrencyString()
        if let cet = proposalListItem.tax, let formattedCET = percentFormatter.string(from: cet / 100 as NSNumber) {
            totalAmountAndCET?.append(" (\(formattedCET))")
        }
        return totalAmountAndCET
    }
    
    func formattedDate(for proposalListItem: ProposalListItem) -> String? {
        guard let createdAt = proposalListItem.createdAt, let referenceDate = referenceDate else {
            return nil
        }
        
        let negotiationStatus: [OpportunityOfferStatus] = [.waitingReply, .counterOffer]
        
        guard negotiationStatus.contains(proposalListItem.status) else {
            return dateFormatter.string(from: createdAt)
        }
        
        let expirationDate = Date(timeInterval: dayTimeInterval * 2, since: createdAt)
        let expirationRemeaningTime = expirationDate.timeIntervalSince(referenceDate)
        
        guard let formattedRemainingInterval = dateComponentsFormatter.string(from: expirationRemeaningTime) else {
            return dateFormatter.string(from: createdAt)
        }
        
        return Localizable.expirationPhrase(formattedRemainingInterval)
    }
    
    func shouldDisplayInstallmentsButton(for proposalListItem: ProposalListItem, and agent: ConsultingAgent) -> Bool {
        let agentFlag: FeatureConfig = agent == ConsultingAgent.borrower ? .isP2PLendingBorrowerInstallmentsAvailable
                                                                         : .isP2PLendingInvestorInstallmentsAvailable
        return dependencies.featureManager.isActive(agentFlag)
            && proposalListItem.status == .active
    }
}
