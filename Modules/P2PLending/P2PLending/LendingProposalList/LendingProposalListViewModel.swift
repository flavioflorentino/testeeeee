import Foundation
import AnalyticsModule

enum SourceList {
    case onGoing
    case terminated
}

protocol LendingProposalListViewModelInputs: AnyObject {
    func fetchAgentInformation()
    func fetchProposals()
    func refreshProposals()
    func showDetails(forProposalAt indexPath: IndexPath, in sourceList: SourceList)
    func showInstallments(forProposalAt indexPath: IndexPath, in sourceList: SourceList)
    func close()
    func sendAnalyticsEvent(selectedIndex: Int)
}

final class LendingProposalListViewModel {
    typealias Dependencies = HasAnalytics
    private let service: LendingProposalListServicing
    private let presenter: LendingProposalListPresenting
    private let agent: ConsultingAgent
    private let dependencies: Dependencies
    
    private var response: ProposalListResponse?

    init(service: LendingProposalListServicing, presenter: LendingProposalListPresenting, agent: ConsultingAgent, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.agent = agent
        self.dependencies = dependencies
    }
    
    private func handleResponse(_ response: ProposalListResponse) {
        presenter.setReferenceDate(response.requestTime)
        
        presenter.setOnGoingEmptyStateVisibility(isEmpty: response.onGoing.isEmpty)
        presenter.presentOnGoing(proposals: response.onGoing, for: agent)
        
        presenter.setTerminatedEmptyStateVisibility(isEmpty: response.terminated.isEmpty)
        presenter.presentTerminated(proposals: response.terminated, for: agent)
    }
    
    private func resetResults() {
        response = nil
        presenter.setOnGoingEmptyStateVisibility(isEmpty: false)
        presenter.presentOnGoing(proposals: [], for: agent)
        presenter.setTerminatedEmptyStateVisibility(isEmpty: false)
        presenter.presentTerminated(proposals: [], for: agent)
    }
}

// MARK: - LendingProposalListViewModelInputs
extension LendingProposalListViewModel: LendingProposalListViewModelInputs {
    func fetchAgentInformation() {
        presenter.presentHeader(for: agent)
    }
    
    func fetchProposals() {
        presenter.startLoading()
        service.fetchProposals(for: agent) { [weak self] result in
            self?.presenter.stopLoading()
            switch result {
            case let .success(response):
                self?.response = response
                self?.handleResponse(response)
            case let .failure(error):
                self?.presenter.present(error: error)
            }
        }
    }
    
    func refreshProposals() {
        resetResults()
        fetchProposals()
    }
    
    func showDetails(forProposalAt indexPath: IndexPath, in sourceList: SourceList) {
        guard let list = sourceList == .onGoing ? response?.onGoing : response?.terminated, list.count > indexPath.row else {
            return
        }
        let proposal = list[indexPath.row]
        switch agent {
        case .borrower:
            dependencies.analytics.log(LendingProposalListEvent.proposalItemTapped(.borrower))
            presenter.didNextStep(action: .proposalDetails(proposalIdentifier: proposal.uuid))
        case .investor:
            dependencies.analytics.log(LendingProposalListEvent.proposalItemTapped(.investor))
            if proposal.status == .waitingReply {
                presenter.didNextStep(action: .opportunityGreetings(proposalIdentifier: proposal.uuid))
            } else {
                presenter.didNextStep(action: .opportunityDetails(proposalIdentifier: proposal.uuid))
            }
        }
    }
    
    func showInstallments(forProposalAt indexPath: IndexPath, in sourceList: SourceList) {
        guard let list = sourceList == .onGoing ? response?.onGoing : response?.terminated, list.count > indexPath.row else {
            return
        }
        let proposal = list[indexPath.row]
        dependencies.analytics.log(LendingProposalListEvent.installmentsTapped(agent))
        presenter.didNextStep(action: .proposalInstallments(proposalIdentifier: proposal.uuid, agent: agent))
    }
    
    func close() {
        presenter.didNextStep(action: .close)
    }
    
    func sendAnalyticsEvent(selectedIndex: Int) {
        selectedIndex == 0 ? dependencies.analytics.log(LendingProposalListEvent.inProgressProposals(agent))
                           : dependencies.analytics.log(LendingProposalListEvent.finishedProposals(agent))
    }
}
