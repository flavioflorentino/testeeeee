import UIKit

enum LendingProposalListAction: Equatable {
    case proposalDetails(proposalIdentifier: UUID)
    case opportunityDetails(proposalIdentifier: UUID)
    case opportunityGreetings(proposalIdentifier: UUID)
    case proposalInstallments(proposalIdentifier: UUID, agent: ConsultingAgent)
    case close
}

protocol LendingProposalListCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: LendingProposalListAction)
}

final class LendingProposalListCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - LendingProposalListCoordinating
extension LendingProposalListCoordinator: LendingProposalListCoordinating {
    func perform(action: LendingProposalListAction) {
        switch action {
        case .proposalDetails(let proposalIdentifier):
            let summaryViewController = LendingProposalSummaryFactory.make(
                type: .remote(offerIdentifier: proposalIdentifier),
                shouldDisplayCloseButton: true
            )
            let navigationController = UINavigationController(rootViewController: summaryViewController)
            viewController?.present(navigationController, animated: true)
        case .opportunityDetails(let proposalIdentifier):
            let opportunityViewController = LendingOpportunityOfferFactory.make(offerIdentifier: proposalIdentifier)
            let navigationController = UINavigationController(rootViewController: opportunityViewController)
            viewController?.present(navigationController, animated: true)
        case .opportunityGreetings(let proposalIdentifier):
            let investorGreetings = LendingInvestorGreetingsFactory.make(offerIdentifier: proposalIdentifier)
            let navigationController = UINavigationController(rootViewController: investorGreetings)
            viewController?.present(navigationController, animated: true)
        case let .proposalInstallments(proposalIdentifier, agent):
            let installmentListcontroller = LendingInstallmentListFactory.make(proposalIdentifier: proposalIdentifier,
                                                                               agent: agent)
            viewController?.show(installmentListcontroller, sender: nil)
        case .close:
            viewController?.dismiss(animated: true)
        }
    }
}
