import UI
import UIKit

protocol LendingProposalListDisplay: AnyObject {
    func display(title: String, headline: String)
    func displayOnGoing(proposals: [ProposalListItemViewModel])
    func displayTerminated(proposals: [ProposalListItemViewModel])
    func display(error: Error)
    func displayEmptyStateForOnGoingList(title: String)
    func hideEmptyStateForOnGoingList()
    func displayEmptyStateForTerminatedList(title: String)
    func hideEmptyStateForTerminatedList()
    func startLoading()
    func stopLoading()
}

extension LendingProposalListViewController.Layout {
    enum Size {
        static let estimatedCollectionViewItemSize = CGSize(width: 288, height: 250)
    }
}

enum AccessibilityIdentifier: String {
    case emptyOnGoingListStateView
    case emptyTerminatedListStateView
}

final class LendingProposalListViewController: ViewController<LendingProposalListViewModelInputs, UIView> {
    fileprivate enum Layout {}
    
    private enum Section {
        case main
    }
    
    private typealias Localizable = Strings.Lending.Proposal.List
    
    private lazy var closeButtonItem = UIBarButtonItem(
        title: GlobalLocalizable.closeButtonTitle,
        style: .done,
        target: self,
        action: #selector(closeButtonTapped)
    )
    
    private lazy var loadingActivityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.color = Colors.grayscale400.color
        return activityIndicator
    }()
    
    private lazy var headlineLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale600())
        return label
    }()
    
    private lazy var selectedListSegmentControl: CustomSegmentedControl = {
        let items = [Localizable.onGoingProposalsSegmentControlTitle, Localizable.terminatedProposalsSegmentControlTitle]
        let segmentedControl = CustomSegmentedControl(buttonTitles: items)
        segmentedControl.backgroundColor = .clear
        segmentedControl.textColor = Colors.branding600.color
        segmentedControl.selectorTextColor = Colors.branding600.color
        segmentedControl.selectorViewColor = Colors.branding600.color
        segmentedControl.selectorIsFullSize = true
        segmentedControl.setIndex(index: 0)
        segmentedControl.addTarget(self, action: #selector(segmentControlValueChanged(_:)), for: .valueChanged)
        return segmentedControl
    }()
    
    private lazy var headerStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [headlineLabel, selectedListSegmentControl])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base03
        stackView.alignment = .leading
        stackView.compatibleLayoutMargins = .rootView
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    
    // MARK: - Scroll View
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.isPagingEnabled = true
        scrollView.delegate = self
        scrollView.showsHorizontalScrollIndicator = false
        return scrollView
    }()
    
    private lazy var scrollViewContentView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [onGoingProposalsCollectionView, terminatedProposalsCollectionView])
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    // MARK: - Collections
    
    private var collectionViewLayout: UICollectionViewFlowLayout {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = EdgeInsets.rootView.asUIEdgeInsets
        layout.estimatedItemSize = Layout.Size.estimatedCollectionViewItemSize
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = Spacing.base02
        return layout
    }
    
    private lazy var onGoingRefreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshControlValueChanged), for: .valueChanged)
        return refreshControl
    }()
    
    private lazy var terminatedRefreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshControlValueChanged), for: .valueChanged)
        return refreshControl
    }()
    
    private lazy var proposalCellIdentifier = String(describing: ProposalItemCollectionViewCell.self)
    private lazy var itemProvider: CollectionViewDataSource<Section, ProposalListItemViewModel>.ItemProvider = { collectionView, indexPath, item in
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.proposalCellIdentifier, for: indexPath) as? ProposalItemCollectionViewCell
        cell?.setup(with: item)
        cell?.delegate = self
        return cell
    }
    
    private lazy var onGoingProposalsDataSource: CollectionViewDataSource<Section, ProposalListItemViewModel> = {
        let dataSource = CollectionViewDataSource<Section, ProposalListItemViewModel>(view: onGoingProposalsCollectionView,
                                                                                      itemProvider: itemProvider)
        dataSource.add(section: .main)
        return dataSource
    }()
    
    private lazy var terminatedProposalsDataSource: CollectionViewDataSource<Section, ProposalListItemViewModel> = {
        let dataSource = CollectionViewDataSource<Section, ProposalListItemViewModel>(view: terminatedProposalsCollectionView,
                                                                                      itemProvider: itemProvider)
        dataSource.add(section: .main)
        return dataSource
    }()
    
    private lazy var onGoingProposalsCollectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout)
        collectionView.register(ProposalItemCollectionViewCell.self, forCellWithReuseIdentifier: proposalCellIdentifier)
        collectionView.backgroundColor = .clear
        collectionView.delegate = self
        collectionView.refreshControl = onGoingRefreshControl
        return collectionView
    }()
    
    private lazy var terminatedProposalsCollectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout)
        collectionView.register(ProposalItemCollectionViewCell.self, forCellWithReuseIdentifier: proposalCellIdentifier)
        collectionView.backgroundColor = .clear
        collectionView.delegate = self
        collectionView.refreshControl = terminatedRefreshControl
        return collectionView
    }()
    
    var shouldDisplayCloseButton = false
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.fetchAgentInformation()
        viewModel.fetchProposals()
        onGoingProposalsCollectionView.dataSource = onGoingProposalsDataSource
        terminatedProposalsCollectionView.dataSource = terminatedProposalsDataSource
    }

    override func buildViewHierarchy() {
        view.addSubview(headerStackView)
        view.addSubview(scrollView)
        view.addSubview(loadingActivityIndicator)
        
        scrollView.addSubview(scrollViewContentView)
    }
    
    override func setupConstraints() {
        headerStackView.snp.makeConstraints {
            $0.top.trailing.leading.equalToSuperview()
        }
        
        scrollView.snp.makeConstraints {
            $0.trailing.bottom.leading.equalToSuperview()
            $0.top.equalTo(headerStackView.snp.bottom)
        }
        
        scrollViewContentView.snp.makeConstraints {
            $0.edges.height.equalToSuperview()
        }
        
        onGoingProposalsCollectionView.snp.makeConstraints {
            $0.width.equalTo(scrollView)
        }
        
        loadingActivityIndicator.snp.makeConstraints {
            $0.center.equalTo(scrollView)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        view.tintColor = Colors.branding400.color
        setupNavigationAppearance()
        registerForProposalStatusChanges()
    }
    
    private func setupNavigationAppearance() {
        navigationController?.navigationBar.tintColor = Colors.branding400.color
        navigationController?.view.backgroundColor = Colors.backgroundPrimary.color
        if shouldDisplayCloseButton {
            navigationItem.leftBarButtonItem = closeButtonItem
        }
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .always
            navigationController?.navigationBar.prefersLargeTitles = true
            navigationController?.navigationBar.isTranslucent = true
        }
        if #available(iOS 13.0, *) {
            let transparentBarAppearance = UINavigationBarAppearance()
            transparentBarAppearance.configureWithTransparentBackground()
            let opaqueBarAppearance = UINavigationBarAppearance()
            opaqueBarAppearance.configureWithOpaqueBackground()
            opaqueBarAppearance.backgroundColor = Colors.backgroundSecondary.color
            opaqueBarAppearance.shadowColor = .none
            navigationController?.navigationBar.scrollEdgeAppearance = transparentBarAppearance
            navigationController?.navigationBar.standardAppearance = opaqueBarAppearance
        }
    }
    
    private func registerForProposalStatusChanges() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(refreshControlValueChanged),
                                               name: .proposalStatusDidChangeNotification,
                                               object: nil)
    }
}

// MARK: - Actions

@objc
private extension LendingProposalListViewController {
    func segmentControlValueChanged(_ segmentControl: CustomSegmentedControl) {
        let selectedIndex = CGFloat(segmentControl.selectedIndex)
        let contentOffset = CGPoint(x: selectedIndex * scrollView.bounds.width, y: 0)
        interactor.sendAnalyticsEvent(selectedIndex: segmentControl.selectedIndex)
        scrollView.setContentOffset(contentOffset, animated: true)
    }
    
    func closeButtonTapped() {
        interactor.close()
    }
    
    func refreshControlValueChanged() {
        interactor.refreshProposals()
    }
}

extension LendingProposalListViewController: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        guard scrollView == self.scrollView else {
            return
        }
        let pagesOffset = Array(stride(from: 0, to: scrollView.contentSize.width, by: scrollView.bounds.width))
        if let index = pagesOffset.firstIndex(of: scrollView.contentOffset.x) {
            selectedListSegmentControl.setIndex(index: Int(index))
        }
    }
}

extension LendingProposalListViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let sourceList: SourceList = collectionView == onGoingProposalsCollectionView ? .onGoing : .terminated
        interactor.showDetails(forProposalAt: indexPath, in: sourceList)
    }
}

extension LendingProposalListViewController: ProposalItemCollectionViewCellDelegate {
    func collectionViewCellInstallmentsButtonTapped(_ collectionViewCell: UICollectionViewCell) {
        guard let collectionView = collectionViewCell.superview as? UICollectionView,
            let indexPath = collectionView.indexPath(for: collectionViewCell) else {
                return
        }
        let sourceList: SourceList = collectionView == onGoingProposalsCollectionView ? .onGoing : .terminated
        interactor.showInstallments(forProposalAt: indexPath, in: sourceList)
    }
}

// MARK: - LendingProposalListDisplay
extension LendingProposalListViewController: LendingProposalListDisplay {
    func display(title: String, headline: String) {
        self.title = title
        self.headlineLabel.text = headline
    }
    
    func displayOnGoing(proposals: [ProposalListItemViewModel]) {
        onGoingProposalsDataSource.update(items: proposals, from: .main)
    }
    
    func displayTerminated(proposals: [ProposalListItemViewModel]) {
        terminatedProposalsDataSource.update(items: proposals, from: .main)
    }
    
    func display(error: Error) {
        let popupViewController = PopupViewController(title: GlobalLocalizable.errorAlertTitle,
                                                      description: error.localizedDescription,
                                                      preferredType: .text)
        let dismissAction = PopupAction(title: GlobalLocalizable.dismissButtonTitle, style: .default)
        popupViewController.addAction(dismissAction)
        showPopup(popupViewController)
    }
    
    func displayEmptyStateForOnGoingList(title: String) {
        let informativeView = InformativeView(image: Assets.lendingFigureUpset.image, title: title)
        informativeView.accessibilityIdentifier = AccessibilityIdentifier.emptyOnGoingListStateView.rawValue
        onGoingProposalsCollectionView.backgroundView = informativeView
    }
    
    func hideEmptyStateForOnGoingList() {
        onGoingProposalsCollectionView.backgroundView = nil
    }
    
    func displayEmptyStateForTerminatedList(title: String) {
        let informativeView = InformativeView(image: Assets.lendingFigureUpset.image, title: title)
        informativeView.accessibilityIdentifier = AccessibilityIdentifier.emptyTerminatedListStateView.rawValue
        terminatedProposalsCollectionView.backgroundView = informativeView
    }
    
    func hideEmptyStateForTerminatedList() {
        terminatedProposalsCollectionView.backgroundView = nil
    }
    
    func startLoading() {
        onGoingRefreshControl.endRefreshing()
        loadingActivityIndicator.startAnimating()
    }
    
    func stopLoading() {
        loadingActivityIndicator.stopAnimating()
    }
}
