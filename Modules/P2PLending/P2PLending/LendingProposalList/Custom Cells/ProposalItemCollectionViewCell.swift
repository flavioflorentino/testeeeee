import AssetsKit
import UI
import UIKit

extension ProposalItemCollectionViewCell.Layout {
    enum Insets {
        static let contentView = EdgeInsets(top: Spacing.base02,
                                            leading: Spacing.base02,
                                            bottom: Spacing.base02,
                                            trailing: Spacing.base02)
    }
    
    enum Length {
        static let installmentsButtonWidth: CGFloat = 144
    }
}

protocol ProposalItemCollectionViewCellDelegate: AnyObject {
    func collectionViewCellInstallmentsButtonTapped(_ collectionViewCell: UICollectionViewCell)
}

final class ProposalItemCollectionViewCell: UICollectionViewCell {
    fileprivate enum Layout {}
    
    private typealias Localizable = Strings.Lending.Proposal.List
    
    private lazy var friendCalloutView: LendingFriendCalloutView = {
        let view = LendingFriendCalloutView()
        view.profileImageView.setImage(url: URL(string: "https://lorempixel.com/100/100/people/?69521")) // sample data
        view.calloutTextLabel.text = "Andieferson aceitou sua proposta!" // sample data
        view.calloutTextLabel.labelStyle(BodyPrimaryLabelStyle())
        view.messageLabel.isHidden = true
        return view
    }()
    
    private lazy var requestedValueItem: SummaryItemView = {
        let item = SummaryItemView()
        item.titleLabel.text = Localizable.requestedValueItemTitle
        return item
    }()
    
    private lazy var totalValueItem: SummaryItemView = {
        let item = SummaryItemView()
        item.valueLabel
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .neutral600())
        return item
    }()
    
    private lazy var summaryStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [requestedValueItem, totalValueItem])
        stackView.spacing = Spacing.base01
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var installmentsButton: UIButton = {
        let button = UIButton(type: .custom)
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Localizable.installmentsButtonTitle, for: .normal)
        button.addTarget(self, action: #selector(installmentsButtonTapped), for: .touchUpInside)
        return button
    }()
    
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [friendCalloutView, summaryStackView, installmentsButton])
        stackView.spacing = Spacing.base02
        stackView.axis = .vertical
        stackView.alignment = .leading
        return stackView
    }()
    
    private lazy var chevronImageView = UIImageView(image: Assets.lendingGlyphRightChrevron.image)
    
    private lazy var timestampLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
        return label
    }()
    
    weak var delegate: ProposalItemCollectionViewCellDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let bezierPath = UIBezierPath(roundedRect: bounds, cornerRadius: 4)
        contentView.viewStyle(CardViewStyle(cgPath: bezierPath.cgPath))
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        let insets: EdgeInsets = .rootView
        let availableWidth = UIScreen.main.bounds.width - (insets.leading + insets.trailing)
        let targetSize = CGSize(width: availableWidth, height: .leastNonzeroMagnitude)
        let size = contentView.systemLayoutSizeFitting(targetSize,
                                                       withHorizontalFittingPriority: .required,
                                                       verticalFittingPriority: .defaultLow)
        layoutAttributes.size = size
        return layoutAttributes
    }
    
    func setup(with proposalItem: ProposalListItemViewModel) {
        friendCalloutView.profileImageView.setImage(url: proposalItem.friendProfilePhotoURL, placeholder: Resources.Placeholders.greenAvatarPlaceholder.image)
        friendCalloutView.calloutTextLabel.attributedText = proposalItem.friendStatusCallout
        requestedValueItem.valueLabel.text = proposalItem.requestedAmount
        totalValueItem.titleLabel.text = proposalItem.totalAmountTitle
        totalValueItem.valueLabel.text = proposalItem.totalAmountValue
        installmentsButton.isHidden = !proposalItem.shouldDisplayInstallmentsButton
        timestampLabel.text = proposalItem.formattedDate
    }
}

extension ProposalItemCollectionViewCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(rootStackView)
        contentView.addSubview(chevronImageView)
        contentView.addSubview(timestampLabel)
    }
    
    func setupConstraints() {
        rootStackView.snp.makeConstraints {
            $0.edges.equalTo(contentView.layoutMarginsGuide)
        }
        
        friendCalloutView.snp.makeConstraints {
            $0.width.equalToSuperview()
        }
        
        summaryStackView.snp.makeConstraints {
            $0.width.equalToSuperview()
        }
        
        chevronImageView.snp.makeConstraints {
            $0.trailing.centerY.equalTo(contentView.layoutMarginsGuide)
        }
        
        timestampLabel.snp.makeConstraints {
            $0.bottom.trailing.equalTo(contentView.layoutMarginsGuide)
        }
        
        installmentsButton.snp.makeConstraints {
            $0.width.equalTo(Layout.Length.installmentsButtonWidth).priority(.high)
        }
    }
    
    func configureViews() {
        contentView.compatibleLayoutMargins = Layout.Insets.contentView
        contentView.viewStyle(CardViewStyle())
    }
}

@objc
extension ProposalItemCollectionViewCell {
    func installmentsButtonTapped() {
        delegate?.collectionViewCellInstallmentsButtonTapped(self)
    }
}
