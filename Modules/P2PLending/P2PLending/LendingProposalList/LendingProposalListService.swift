import Core
import Foundation

protocol LendingProposalListServicing: Servicing {
    func fetchProposals(for agent: ConsultingAgent, completion: @escaping ModelCompletionBlock<ProposalListResponse>)
}

final class LendingProposalListService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - LendingProposalListServicing
extension LendingProposalListService: LendingProposalListServicing {
    func fetchProposals(for agent: ConsultingAgent, completion: @escaping ModelCompletionBlock<ProposalListResponse>) {
        let api = Api<ProposalListResponse>(endpoint: P2PLendingEndpoint.proposals(agent: agent))
        let decoder = JSONDecoder()
        api.shouldUseDefaultDateFormatter = false
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        api.execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
