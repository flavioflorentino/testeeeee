import Foundation
import UIKit

public enum LendingProposalListFactory {
    public static func make(for agent: ConsultingAgent, shouldDisplayCloseButton: Bool = false) -> UIViewController {
        let container = DependencyContainer()
        let service: LendingProposalListServicing = LendingProposalListService(dependencies: container)
        let coordinator: LendingProposalListCoordinating = LendingProposalListCoordinator()
        let presenter: LendingProposalListPresenting = LendingProposalListPresenter(coordinator: coordinator, dependencies: container)
        let viewModel = LendingProposalListViewModel(service: service, presenter: presenter, agent: agent, dependencies: container)
        let viewController = LendingProposalListViewController(viewModel: viewModel)
        
        viewController.shouldDisplayCloseButton = shouldDisplayCloseButton
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
