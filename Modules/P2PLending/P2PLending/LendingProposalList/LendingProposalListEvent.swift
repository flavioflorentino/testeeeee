import Foundation
import AnalyticsModule

enum LendingProposalListEvent: AnalyticsKeyProtocol {
    case proposalItemTapped(ConsultingAgent)
    case installmentsTapped(ConsultingAgent)
    case inProgressProposals(ConsultingAgent)
    case finishedProposals(ConsultingAgent)
    
    private var screenName: String {
        switch self {
        case .proposalItemTapped(let agent),
             .installmentsTapped(let agent),
             .inProgressProposals(let agent),
             .finishedProposals(let agent):
            switch agent {
            case .borrower:
                return "P2P_LENDING_TOMADOR_MEUS_PEDIDOS"
            case .investor:
                return "P2P_LENDING_INVESTIDOR_MEUS_INVESTIMENTOS"
            }
        }
    }
    
    var name: String {
        switch self {
        case .proposalItemTapped, .installmentsTapped:
            return AnalyticInteraction.buttonClicked.name
        case .inProgressProposals, .finishedProposals:
            return AnalyticInteraction.tabTapped.name
        }
    }
    
    var properties: [String: Any] {
        switch self {
        case .installmentsTapped:
            return [
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.openInstallments.description,
                AnalyticsKeys.screenName.name: screenName
            ]
        case .finishedProposals:
            return [
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.tabName.name: AnalyticsConstants.finishedProposalTab.description,
                AnalyticsKeys.screenName.name: screenName
            ]
        case .inProgressProposals:
            return [
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.tabName.name: AnalyticsConstants.inProgressProposalTab.description,
                AnalyticsKeys.screenName.name: screenName
            ]
        case .proposalItemTapped:
            return [
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.screenName.name: screenName
            ]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.eventTracker])
    }
}
