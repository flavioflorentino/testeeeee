protocol OpportunityOfferStatusDescriptionProvider {
    func opportunityStatusDescription(for status: OpportunityOfferStatus, with person: PersonConvertible) -> String
    func opportunityStatusDescriptionHighlight(for status: OpportunityOfferStatus) -> String
}
