import Foundation

struct ProposalListItemViewModel {
    let identifier: UUID
    let friendProfilePhotoURL: URL?
    let friendStatusCallout: NSAttributedString?
    let requestedAmount: String?
    let totalAmountTitle: String?
    let totalAmountValue: String?
    let formattedDate: String?
    let shouldDisplayInstallmentsButton: Bool
}
