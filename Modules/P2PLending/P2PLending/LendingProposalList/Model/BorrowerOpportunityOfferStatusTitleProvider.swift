struct BorrowerOpportunityOfferStatusTitleProvider: OpportunityOfferStatusTitleProvider {
    private typealias Localizable = Strings.Lending.Status
    
    func opportunityOfferStatusTitle(for status: OpportunityOfferStatus) -> String {
        switch status {
        case .waitingReply:
            return Localizable.borrowerWaitingReply
        case .accepted:
            return Localizable.borrowerAccepted
        case .processingPayment:
            return Localizable.borrowerProcessingPayment
        case .active:
            return Localizable.borrowerActive
        case .delayedPayment:
            return Localizable.borrowerDelayedPayment
        case .counterOffer:
            return Localizable.borrowerCounterOffer
        case .liquidated:
            return Localizable.borrowerLiquidated
        case .declined:
            return Localizable.borrowerDeclined
        case .expired:
            return Localizable.borrowerExpired
        case .canceled:
            return Localizable.borrowerCanceled
        }
    }
}
