import Foundation
import UI

struct OpportunityOfferStatusDescriptionBuilder {
    let person: PersonConvertible
    let descriptionProvider: OpportunityOfferStatusDescriptionProvider
    
    private var stringAttributes: [NSAttributedString.Key: Any] {
        [.font: Typography.bodyPrimary().font(), .foregroundColor: Colors.grayscale700.color]
    }
    
    private var nameHighlightAttributes: [NSAttributedString.Key: Any] {
        [.font: Typography.bodyPrimary(.highlight).font()]
    }
    
    private var statusHighlightAttributes: [NSAttributedString.Key: Any] {
        [.font: Typography.bodyPrimary(.highlight).font(), .foregroundColor: Colors.black.color]
    }
    
    func opportunityOfferStatusDescription(for status: OpportunityOfferStatus) -> NSAttributedString {
        let firstName = person.firstName ?? ""
        let statusDescriptionString = descriptionProvider.opportunityStatusDescription(for: status, with: person)
        let statusDescriptionHighlightString = descriptionProvider.opportunityStatusDescriptionHighlight(for: status)
        let attributedString = NSMutableAttributedString(string: statusDescriptionString, attributes: stringAttributes)
        
        highlightFirstOccurrence(of: firstName, in: attributedString, with: nameHighlightAttributes)
        highlightFirstOccurrence(of: statusDescriptionHighlightString, in: attributedString, with: statusHighlightAttributes)
        
        return attributedString
    }
    
    private func highlightFirstOccurrence(
        of string: String,
        in attributedString: NSMutableAttributedString,
        with highlightAttributes: [NSAttributedString.Key: Any]
    ) {
        let highlightRange = (attributedString.string as NSString).range(of: string)
        attributedString.addAttributes(highlightAttributes, range: highlightRange)
    }
}
