protocol OpportunityOfferStatusTitleProvider {
    func opportunityOfferStatusTitle(for status: OpportunityOfferStatus) -> String
}
