import Foundation

struct ProposalListItem: Decodable, Equatable {
    let user: LendingFriend
    let uuid: UUID
    let amount: Double
    let totalInterest: Double
    let tax: Float?
    let status: OpportunityOfferStatus
    let createdAt: Date?
    let expiredAt: Date?
}
