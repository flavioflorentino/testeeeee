struct InvestorOpportunityOfferStatusTitleProvider: OpportunityOfferStatusTitleProvider {
    private typealias Localizable = Strings.Lending.Status
    
    func opportunityOfferStatusTitle(for status: OpportunityOfferStatus) -> String {
        switch status {
        case .waitingReply:
            return Localizable.investorWaitingReply
        case .accepted:
            return Localizable.investorAccepted
        case .processingPayment:
            return Localizable.investorProcessingPayment
        case .active:
            return Localizable.investorActive
        case .delayedPayment:
            return Localizable.investorDelayedPayment
        case .counterOffer:
            return Localizable.investorCounterOffer
        case .liquidated:
            return Localizable.investorLiquidated
        case .declined:
            return Localizable.investorDeclined
        case .expired:
            return Localizable.investorExpired
        case .canceled:
            return Localizable.investorCanceled
        }
    }
}
