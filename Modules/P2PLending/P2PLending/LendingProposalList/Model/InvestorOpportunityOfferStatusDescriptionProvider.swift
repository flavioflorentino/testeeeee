struct InvestorOpportunityOfferStatusDescriptionProvider: OpportunityOfferStatusDescriptionProvider {
    private typealias Localizable = Strings.Lending.Proposal.List
    
    func opportunityStatusDescription(for status: OpportunityOfferStatus, with person: PersonConvertible) -> String {
        let firstName = person.firstName ?? ""
        switch status {
        case .waitingReply:
            return Localizable.investorWaitingReplyItemCallout(firstName)
        case .accepted:
            return Localizable.investorAcceptedItemCallout(firstName)
        case .processingPayment:
            return Localizable.investorProcessingPaymentItemCallout(firstName)
        case .active:
            return Localizable.investorActiveItemCallout(firstName)
        case .delayedPayment:
            return Localizable.investorDelayedPaymentItemCallout(firstName)
        case .counterOffer:
            return Localizable.investorCounterOfferItemCallout(firstName)
        case .liquidated:
            return Localizable.investorLiquidatedItemCallout(firstName)
        case .declined:
            return Localizable.investorDeclinedItemCallout(firstName)
        case .expired:
            return Localizable.investorExpiredItemCallout(firstName)
        case .canceled:
            return Localizable.investorCanceledItemCallout(firstName)
        }
    }
    
    func opportunityStatusDescriptionHighlight(for status: OpportunityOfferStatus) -> String {
        switch status {
        case .waitingReply:
            return Localizable.investorWaitingReplyItemCalloutHighlight
        case .accepted:
            return Localizable.investorAcceptedItemCalloutHighlight
        case .processingPayment:
            return Localizable.investorProcessingPaymentItemCalloutHighlight
        case .active:
            return Localizable.investorActiveItemCalloutHighlight
        case .delayedPayment:
            return Localizable.investorDelayedPaymentItemCalloutHighlight
        case .counterOffer:
            return Localizable.investorCounterOfferItemCalloutHighlight
        case .liquidated:
            return Localizable.investorLiquidatedItemCalloutHighlight
        case .declined:
            return Localizable.investorDeclinedItemCalloutHighlight
        case .expired:
            return Localizable.investorExpiredItemCalloutHighlight
        case .canceled:
            return Localizable.investorCanceledItemCalloutHighlight
        }
    }
}
