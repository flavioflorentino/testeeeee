struct BorrowerOpportunityOfferStatusDescriptionProvider: OpportunityOfferStatusDescriptionProvider {
    private typealias Localizable = Strings.Lending.Proposal.List
    
    func opportunityStatusDescription(for status: OpportunityOfferStatus, with person: PersonConvertible) -> String {
        let firstName = person.firstName ?? ""
        switch status {
        case .waitingReply:
            return Localizable.borrowerWaitingReplyItemCallout(firstName)
        case .accepted:
            return Localizable.borrowerAcceptedItemCallout(firstName)
        case .processingPayment:
            return Localizable.borrowerProcessingPaymentItemCallout(firstName)
        case .active:
            return Localizable.borrowerActiveItemCallout(firstName)
        case .delayedPayment:
            return Localizable.borrowerDelayedPaymentItemCallout(firstName)
        case .counterOffer:
            return Localizable.borrowerCounterOfferItemCallout(firstName)
        case .liquidated:
            return Localizable.borrowerLiquidatedItemCallout(firstName)
        case .declined:
            return Localizable.borrowerDeclinedItemCallout(firstName)
        case .expired:
            return Localizable.borrowerExpiredItemCallout(firstName)
        case .canceled:
            return Localizable.borrowerCanceledItemCallout(firstName)
        }
    }
    
    func opportunityStatusDescriptionHighlight(for status: OpportunityOfferStatus) -> String {
        switch status {
        case .waitingReply:
            return Localizable.borrowerWaitingReplyItemCalloutHighlight
        case .accepted:
            return Localizable.borrowerAcceptedItemCalloutHighlight
        case .processingPayment:
            return Localizable.borrowerProcessingPaymentItemCalloutHighlight
        case .active:
            return Localizable.borrowerActiveItemCalloutHighlight
        case .delayedPayment:
            return Localizable.borrowerDelayedPaymentItemCalloutHighlight
        case .counterOffer:
            return Localizable.borrowerCounterOfferItemCalloutHighlight
        case .liquidated:
            return Localizable.borrowerLiquidatedItemCalloutHighlight
        case .declined:
            return Localizable.borrowerDeclinedItemCalloutHighlight
        case .expired:
            return Localizable.borrowerExpiredItemCalloutHighlight
        case .canceled:
            return Localizable.borrowerCanceledItemCalloutHighlight
        }
    }
}
