import Foundation

struct ProposalListResponse: Decodable, Equatable {
    let onGoing: [ProposalListItem]
    let terminated: [ProposalListItem]
    let requestTime: Date
    
    enum CodingKeys: String, CodingKey {
        case onGoing = "inProgress"
        case terminated = "finished"
        case requestTime
    }
}
