public enum ConsultingAgent: String {
    case borrower
    case investor
}
