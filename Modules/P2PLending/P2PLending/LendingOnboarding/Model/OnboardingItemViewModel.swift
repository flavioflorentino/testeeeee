import UIKit
import LendingComponents

extension OnboardingItem: LendingProductCellViewModeling {
    private typealias Localizable = Strings.Lending.Onboarding
    
    var title: String {
        switch self {
        case .newProposal:
            return Localizable.newProposalItemTitle
        case .proposals:
            return Localizable.proposalsItemTitle
        case .investments:
            return Localizable.investmentsItemTitle
        }
    }
    
    var description: String {
        switch self {
        case .newProposal:
            return Localizable.newProposalItemDescription
        case .proposals:
            return Localizable.proposalsItemDescription
        case .investments:
            return Localizable.investmentsItemDescription
        }
    }
    
    var image: UIImage {
        switch self {
        case .newProposal:
            return Assets.lendingGlyphMoneyBill.image
        case .proposals:
            return Assets.lendingGlyphBill.image
        case .investments:
            return Assets.lendingGlyphMoneyStack.image
        }
    }
}
