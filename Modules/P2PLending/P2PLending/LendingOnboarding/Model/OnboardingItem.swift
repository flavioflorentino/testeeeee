enum OnboardingItem: CaseIterable {
    case newProposal
    case proposals
    case investments
}
