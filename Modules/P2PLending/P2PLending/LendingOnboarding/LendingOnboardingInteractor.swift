import Foundation
import AnalyticsModule

protocol LendingOnboardingInteracting: AnyObject {
    func fetchOnboardingItems()
    func showItem(at indexPath: IndexPath)
    func showHelp()
    func close()
}

final class LendingOnboardingInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    private let presenter: LendingOnboardingPresenting
    
    private lazy var onboardingItems: [OnboardingItem] = OnboardingItem.allCases
    
    init(presenter: LendingOnboardingPresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
    }
    
    private func sendTrackingEvent(item: OnboardingItem) {
        var event: LendingOnboardingEvent
        switch item {
        case .newProposal:
            event = .newProposal
        case .proposals:
            event = .proposals
        case .investments:
            event = .investments
        }
        dependencies.analytics.log(event)
    }
}

// MARK: - LendingOnboardingInteracting
extension LendingOnboardingInteractor: LendingOnboardingInteracting {
    func fetchOnboardingItems() {
        dependencies.analytics.log(LendingOnboardingEvent.p2pLendingHubViewed)
        presenter.present(onboardingItems: onboardingItems)
    }
    
    func showItem(at indexPath: IndexPath) {
        sendTrackingEvent(item: onboardingItems[indexPath.row])
        presenter.didNextStep(action: .show(item: onboardingItems[indexPath.row]))
    }
    
    func showHelp() {
        guard let url = URL(string: "picpay://picpay/helpcenter/category/360003819012") else { return }
        dependencies.analytics.log(LendingOnboardingEvent.faq)
        presenter.didNextStep(action: .showFAQ(url: url))
    }
    
    func close() {
        presenter.didNextStep(action: .close)
    }
}
