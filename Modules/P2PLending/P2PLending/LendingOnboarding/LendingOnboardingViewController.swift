import UI
import UIKit
import LendingComponents

protocol LendingOnboardingDisplaying: AnyObject {
    func display(items: [LendingProductCellViewModeling])
}

private extension LendingOnboardingViewController.Layout {
    enum Length {
        static let estimatedRowHeight: CGFloat = 97
        static let estimatedSectionHeaderHeight: CGFloat = 242
    }
    
    enum Size {
        static let tableViewFooterView = CGSize(width: .zero, height: 64)
    }
}

final class LendingOnboardingViewController: ViewController<LendingOnboardingInteracting, UIView> {
    private typealias Localizable = Strings.Lending.Onboarding
    
    fileprivate enum Layout { }
    
    private enum Section {
        case main
    }
    
    let cellIdentifier = String(describing: LendingProductTableViewCell.self)
    let sectionHeaderIdentifier = String(describing: LendingHeaderView.self)
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.register(LendingProductTableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        tableView.register(LendingHeaderView.self, forHeaderFooterViewReuseIdentifier: sectionHeaderIdentifier)
        tableView.backgroundColor = Colors.backgroundPrimary.color
        tableView.estimatedRowHeight = Layout.Length.estimatedRowHeight
        tableView.estimatedSectionHeaderHeight = Layout.Length.estimatedSectionHeaderHeight
        tableView.rowHeight = UITableView.automaticDimension
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.delegate = self
        return tableView
    }()
    
    private lazy var tableViewDataSource: TableViewDataSource<Section, LendingProductCellViewModeling> = {
        let dataSource = TableViewDataSource<Section, LendingProductCellViewModeling>(view: tableView) { tableView, indexPath, item -> UITableViewCell? in
            let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier, for: indexPath) as? LendingProductTableViewCell
            cell?.setup(for: item)
            return cell
        }
        dataSource.add(section: .main)
        return dataSource
    }()
    
    private lazy var learnMoreButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Localizable.helpButtonTitle, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(learnMoreButtonTapped), for: .touchUpInside)
        return button
    }()
    
    private lazy var tableViewFooterView = UIView(frame: CGRect(origin: .zero, size: Layout.Size.tableViewFooterView))
    
    private lazy var closeButtonItem = UIBarButtonItem(
        title: GlobalLocalizable.closeButtonTitle,
        style: .done,
        target: self,
        action: #selector(closeButtonTapped)
    )
    
    var shouldDisplayCloseButton = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.fetchOnboardingItems()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationAppearance()
    }

    override func buildViewHierarchy() {
        view.addSubview(tableView)
        tableViewFooterView.addSubview(learnMoreButton)
        tableView.tableFooterView = tableViewFooterView
    }
    
    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        learnMoreButton.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }

    override func configureViews() {
        tableView.dataSource = tableViewDataSource
        view.tintColor = Colors.branding600.color
    }
    
    private func setupNavigationAppearance() {
        navigationController?.navigationBar.tintColor = Colors.branding400.color
        navigationController?.view.backgroundColor = Colors.backgroundPrimary.color
        if shouldDisplayCloseButton {
            navigationItem.leftBarButtonItem = closeButtonItem
        }
        if #available(iOS 11.0, *) {
            navigationItem.backButtonTitle = Localizable.backButtonTitle
            navigationItem.largeTitleDisplayMode = .never
        } else {
            navigationItem.backBarButtonItem = UIBarButtonItem(title: Localizable.backButtonTitle, style: .plain, target: nil, action: nil)
        }
        if #available(iOS 13.0, *) {
            let transparentBarAppearance = UINavigationBarAppearance()
            transparentBarAppearance.configureWithTransparentBackground()
            let opaqueBarAppearance = UINavigationBarAppearance()
            opaqueBarAppearance.configureWithOpaqueBackground()
            opaqueBarAppearance.backgroundColor = Colors.backgroundPrimary.color
            opaqueBarAppearance.shadowColor = .none
            navigationController?.navigationBar.scrollEdgeAppearance = transparentBarAppearance
            navigationController?.navigationBar.standardAppearance = opaqueBarAppearance
        }
    }
}

@objc
private extension LendingOnboardingViewController {
    func learnMoreButtonTapped() {
        interactor.showHelp()
    }
    
    func closeButtonTapped() {
        interactor.close()
    }
}

extension LendingOnboardingViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        interactor.showItem(at: indexPath)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: sectionHeaderIdentifier) as? LendingHeaderView
        let content = LendingHeaderViewContent(image: Assets.lendingFigureGreetings.image,
                                               title: Localizable.headline,
                                               description: Localizable.subhead)
        header?.configureViews(content: content)
        return header
    }
}

// MARK: - LendingOnboardingDisplaying
extension LendingOnboardingViewController: LendingOnboardingDisplaying {
    func display(items: [LendingProductCellViewModeling]) {
        tableViewDataSource.update(items: items, from: .main)
    }
}
