import UIKit

public enum LendingOnboardingFactory {
    public static func make(shouldDisplayCloseButton: Bool = false) -> UIViewController {
        let container = DependencyContainer()
        let coordinator: LendingOnboardingCoordinating = LendingOnboardingCoordinator(dependencies: container)
        let presenter: LendingOnboardingPresenting = LendingOnboardingPresenter(coordinator: coordinator)
        let interactor = LendingOnboardingInteractor(presenter: presenter, dependencies: container)
        let viewController = LendingOnboardingViewController(interactor: interactor)

        viewController.shouldDisplayCloseButton = shouldDisplayCloseButton
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
