import UIKit

enum LendingOnboardingAction: Equatable {
    case show(item: OnboardingItem)
    case showFAQ(url: URL)
    case close
}

protocol LendingOnboardingCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: LendingOnboardingAction)
}

final class LendingOnboardingCoordinator: LendingOnboardingCoordinating, URLOpenerCoordinating {
    typealias Dependencies = HasURLOpener
    private let dependencies: Dependencies
    
    weak var viewController: UIViewController?
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func perform(action: LendingOnboardingAction) {
        switch action {
        case .show(let item):
            show(item: item)
        case .showFAQ(let url):
            showFAQ(for: url, using: dependencies)
        case .close:
            viewController?.dismiss(animated: true)
        }
    }
    
    private func show(item: OnboardingItem) {
        switch item {
        case .newProposal:
            let controller = LendingGreetingsFactory.make(shouldDisplayCloseButton: true)
            let navigationController = UINavigationController(rootViewController: controller)
            viewController?.present(navigationController, animated: true, completion: nil)
        case .proposals:
            let controller = LendingProposalListFactory.make(for: .borrower)
            viewController?.show(controller, sender: nil)
        case .investments:
            let controller = LendingProposalListFactory.make(for: .investor)
            viewController?.show(controller, sender: nil)
        }
    }
}
