import Foundation
import AnalyticsModule

enum LendingOnboardingEvent: AnalyticsKeyProtocol {
    case p2pLendingHubViewed
    case newProposal
    case proposals
    case investments
    case faq
    
    var screenName: String {
        "P2P_LENDING_MENU_INICIAL"
    }
    
    var name: String {
        switch self {
        case .p2pLendingHubViewed:
            return AnalyticInteraction.screenViewed.name
        case .newProposal, .proposals, .investments, .faq:
            return AnalyticInteraction.buttonClicked.name
        }
    }
    
    var properties: [String: Any] {
        switch self {
        case .p2pLendingHubViewed:
            return [
                AnalyticsKeys.screenName.name: screenName,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        case .newProposal:
            return [
                AnalyticsKeys.screenName.name: screenName,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.newProposal.description
            ]
        case .proposals:
            return [
                AnalyticsKeys.screenName.name: screenName,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.myProposals.description
            ]
        case .investments:
            return [
                AnalyticsKeys.screenName.name: screenName,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.myInvestments.description
            ]
        case .faq:
            return [
                AnalyticsKeys.screenName.name: screenName,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.faq.description
            ]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.eventTracker])
    }
}
