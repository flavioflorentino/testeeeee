import UIKit
import AssetsKit

protocol LendingOnboardingPresenting: AnyObject {
    var viewController: LendingOnboardingDisplaying? { get set }
    func present(onboardingItems: [OnboardingItem])
    func didNextStep(action: LendingOnboardingAction)
}

final class LendingOnboardingPresenter {
    private typealias Localizable = Strings.Lending.Onboarding
    
    private let coordinator: LendingOnboardingCoordinating
    weak var viewController: LendingOnboardingDisplaying?

    init(coordinator: LendingOnboardingCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - LendingOnboardingPresenting
extension LendingOnboardingPresenter: LendingOnboardingPresenting {
    func present(onboardingItems: [OnboardingItem]) {
        viewController?.display(items: onboardingItems)
    }
    
    func didNextStep(action: LendingOnboardingAction) {
        coordinator.perform(action: action)
    }
}
