import Foundation
import AnalyticsModule

enum LendingCCBEvent: AnalyticsKeyProtocol {
    case backButtonTapped
    case sendProposalTapped
    case didReadContract
    
    private var title: String {
        "P2P_LENDING_TOMADOR_CONTRATO"
    }
    
    var name: String {
        switch self {
        case .backButtonTapped, .sendProposalTapped:
            return AnalyticInteraction.buttonClicked.name
        case .didReadContract:
            return AnalyticInteraction.documentRead.name
        }
    }
    
    var properties: [String: Any] {
        switch self {
        case .backButtonTapped:
            return [
                AnalyticsKeys.screenName.name: title,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.backButton.description
            ]
        case .sendProposalTapped:
            return [
                AnalyticsKeys.screenName.name: title,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.sendProposal.description
            ]
        case .didReadContract:
            return [
                AnalyticsKeys.screenName.name: title,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.documentType.name: AnalyticsConstants.contract.description,
                AnalyticsKeys.documentDescription.name: AnalyticsConstants.borrowerContract.description
            ]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.eventTracker])
    }
}
