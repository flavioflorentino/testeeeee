import Foundation
import UI

protocol LendingCCBPresenting: AnyObject {
    var viewController: LendingCCBDisplay? { get set }
    func presentDocument(ccbData: CCBData)
    func present(error: Error)
    func startLoading()
    func stopLoading()
    func didNextStep(action: LendingCCBAction)
}

final class LendingCCBPresenter {
    private let coordinator: LendingCCBCoordinating
    weak var viewController: LendingCCBDisplay?
    
    private lazy var locale = Locale(identifier: "pt-BR")
    private lazy var documentMask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cpf)
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateFormatter.locale = locale
        return dateFormatter
    }()
    private lazy var percentFormatter: NumberFormatter = {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .percent
        numberFormatter.maximumFractionDigits = 2
        numberFormatter.locale = locale
        return numberFormatter
    }()
    
    private func percentFormattedValue(_ value: Float?) -> String? {
        guard let value = value else {
            return nil
        }
        return percentFormatter.string(from: (value / 100) as NSNumber)
    }

    init(coordinator: LendingCCBCoordinating) {
        self.coordinator = coordinator
    }
    
    private func replace(key: String, with value: String?, in mutableString: NSMutableString) {
        let range = NSRange(location: 0, length: mutableString.length)
        mutableString.replaceOccurrences(of: "${\(key)}", with: value ?? "-", options: .literal, range: range)
    }
    
    private func dataDictionary(from ccbData: CCBData) -> [String: String?] {
        [
            "borrower_name": ccbData.borrower.name,
            "borrower_document": documentMask.maskedText(from: ccbData.borrower.document),
            "borrower_address": ccbData.borrower.address,
            "borrower_email": ccbData.borrower.email,
            "issue_date": dateFormatter.string(from: ccbData.contract.issueDate),
            "issue_city": ccbData.contract.issueCity,
            "issue_uf": ccbData.contract.issueUf,
            "borrower_username": ccbData.borrower.userName,
            "iof": ccbData.contract.iof.toCurrencyString(),
            "total_requested_amount": ccbData.contract.totalRequestedAmount.toCurrencyString(),
            "comission_amount": ccbData.contract.comissionAmount.toCurrencyString(),
            "finnancial_safe": ccbData.contract.finnancialSafe.toCurrencyString(),
            "requested_amount": ccbData.contract.requestedAmount.toCurrencyString(),
            "month_rate": percentFormattedValue(ccbData.contract.monthRate),
            "year_rate": percentFormattedValue(ccbData.contract.yearRate),
            "default_interest": percentFormattedValue(ccbData.contract.defaultInterest),
            "finnancial_fine": percentFormattedValue(ccbData.contract.finnancialFine),
            "installments": ccbData.contract.installments.description,
            "periodicity": ccbData.contract.periodicity.description,
            "borrower_installments_amount": ccbData.contract.borrowerInstallmentsAmount.toCurrencyString(),
            "pay_day": ccbData.contract.payDay.description,
            "first_dead_line": dateFormatter.string(from: ccbData.contract.firstDeadLine),
            "month_cet": percentFormattedValue(ccbData.contract.monthCet),
            "year_cet": percentFormattedValue(ccbData.contract.monthCet),
            "total_interest": ccbData.contract.totalInterest.toCurrencyString(),
            "a_value": ccbData.attachments.a.value.toCurrencyString(),
            "a_percent": percentFormattedValue(ccbData.attachments.a.percent),
            "b_value": ccbData.attachments.b.value.toCurrencyString(),
            "b_percent": percentFormattedValue(ccbData.attachments.b.percent),
            "c_value": ccbData.attachments.c.value.toCurrencyString(),
            "c_percent": percentFormattedValue(ccbData.attachments.c.percent),
            "c1_value": ccbData.attachments.c1.value.toCurrencyString(),
            "c1_percent": percentFormattedValue(ccbData.attachments.c1.percent),
            "c2_value": ccbData.attachments.c2.value.toCurrencyString(),
            "c2_percent": percentFormattedValue(ccbData.attachments.c2.percent),
            "c3_value": ccbData.attachments.c3.value.toCurrencyString(),
            "c3_percent": percentFormattedValue(ccbData.attachments.c3.percent),
            "c4_value": ccbData.attachments.c4.value.toCurrencyString(),
            "c4_percent": percentFormattedValue(ccbData.attachments.c4.percent)
        ]
    }
    
    func populate(htmlString: NSMutableString, with ccbData: CCBData) {
        let dictionary = dataDictionary(from: ccbData)
        dictionary.forEach { key, value in
            replace(key: key, with: value, in: htmlString)
        }
    }
    
    private func loadHTMLMutableString() -> NSMutableString? {
        let bundle = P2PLendingResources.resourcesBundle
        guard let path = bundle.path(forResource: "CCB", ofType: "html") else {
            return nil
        }
        return try? NSMutableString(contentsOfFile: path, encoding: String.Encoding.utf8.rawValue)
    }
}

// MARK: - LendingCCBPresenting
extension LendingCCBPresenter: LendingCCBPresenting {
    func presentDocument(ccbData: CCBData) {
        guard let htmlString = loadHTMLMutableString() else { return }
        populate(htmlString: htmlString, with: ccbData)
        viewController?.displayDocument(htmlString: htmlString as String)
    }
    
    func present(error: Error) {
        viewController?.display(error: error)
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
    
    func didNextStep(action: LendingCCBAction) {
        coordinator.perform(action: action)
    }
}
