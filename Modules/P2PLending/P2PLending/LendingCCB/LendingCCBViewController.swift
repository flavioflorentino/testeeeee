import UI
import UIKit
import WebKit

protocol LendingCCBDisplay: AnyObject {
    func displayDocument(htmlString: String)
    func display(error: Error)
    func startLoading()
    func stopLoading()
}

final class LendingCCBViewController: ViewController<LendingCCBInteracting, UIView> {
    private typealias Localizable = Strings.Lending.Ccb
    
    enum AccessibilityIdentifier: String {
        case footerText
        case continueButton
    }
    
    private lazy var webView: WKWebView = {
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = false
        let configuration = WKWebViewConfiguration()
        configuration.dataDetectorTypes = []
        configuration.preferences = preferences
        let webView = WKWebView(frame: .zero, configuration: configuration)
        webView.allowsBackForwardNavigationGestures = false
        return WKWebView(frame: .zero, configuration: configuration)
    }()
    
    private lazy var activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(style: .gray)
        activityIndicatorView.color = Colors.grayscale300.color
        return activityIndicatorView
    }()
    
    private lazy var footerLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
        label.textColor = Colors.grayscale700.color
        label.text = Localizable.footer
        label.accessibilityIdentifier = AccessibilityIdentifier.footerText.rawValue
        return label
    }()
    
    private lazy var continueButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.continueButtonTitle, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(continueButtonTapped), for: .touchUpInside)
        button.accessibilityIdentifier = AccessibilityIdentifier.continueButton.rawValue
        return button
    }()
    
    private lazy var footerStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [footerLabel, continueButton])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        stackView.compatibleLayoutMargins = .rootView
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    
    private var shouldSendTrackingEvent: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.fetchDocument()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if isMovingFromParent {
            interactor.sendAnalyticsEvent(type: .backButtonTapped)
        }
    }

    override func buildViewHierarchy() {
        view.addSubview(webView)
        view.addSubview(activityIndicatorView)
        view.addSubview(footerStackView)
    }
    
    override func setupConstraints() {
        webView.snp.makeConstraints {
            $0.top.trailing.leading.equalToSuperview()
        }
        footerStackView.snp.makeConstraints {
            $0.top.equalTo(webView.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
        activityIndicatorView.snp.makeConstraints {
            $0.center.equalTo(webView)
        }
    }

    override func configureViews() {
        title = Localizable.title
        view.backgroundColor = Colors.backgroundPrimary.color
        view.tintColor = Colors.branding600.color
    }
}

@objc
extension LendingCCBViewController {
    func continueButtonTapped() {
        interactor.authAndSendProposal()
    }
}

// MARK: LendingCCBDisplay
extension LendingCCBViewController: LendingCCBDisplay {
    func displayDocument(htmlString: String) {
        webView.loadHTMLString(htmlString, baseURL: nil)
        webView.scrollView.delegate = self
    }
    
    func display(error: Error) {
        let popUpViewController = PopupViewController(
            title: GlobalLocalizable.errorAlertTitle,
            description: error.localizedDescription,
            preferredType: .text
        )
        let dismissAction = PopupAction(title: GlobalLocalizable.dismissButtonTitle, style: .default)
        popUpViewController.addAction(dismissAction)
        
        showPopup(popUpViewController)
    }
    
    func startLoading() {
        webView.alpha = 0
        continueButton.isEnabled = false
        activityIndicatorView.startAnimating()
    }
    
    func stopLoading() {
        webView.alpha = 1
        continueButton.isEnabled = true
        activityIndicatorView.stopAnimating()
    }
}

extension LendingCCBViewController: UIScrollViewDelegate, WKNavigationDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard scrollView.contentOffset.y > (scrollView.contentSize.height - scrollView.frame.size.height) &&
                shouldSendTrackingEvent else {
            return
        }
        interactor.sendAnalyticsEvent(type: .didScrollToBottom)
        shouldSendTrackingEvent = false
    }
}
