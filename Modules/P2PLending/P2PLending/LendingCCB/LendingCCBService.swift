import Core
import Foundation

protocol LendingCCBServicing: Servicing {
    func fetchCCB(for proposal: Proposal, completion: @escaping ModelCompletionBlock<CCBData>)
    func send(proposal: Proposal, pin: String, completion: @escaping ModelCompletionBlock<NoContent>)
    func send(proposal: Proposal, completion: @escaping ModelCompletionBlock<NoContent>)
}

final class LendingCCBService: LendingCCBServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "pt_BR")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter
    }()
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func fetchCCB(for proposal: Proposal, completion: @escaping ModelCompletionBlock<CCBData>) {
        let decoder = JSONDecoder(.convertFromSnakeCase)
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        
        let api = Api<CCBData>(endpoint: P2PLendingEndpoint.ccb(proposal: proposal))
        api.shouldUseDefaultDateFormatter = false
        
        api.execute(jsonDecoder: decoder) { [weak self] result in
            let mappedResult = result.map(\.model)
            self?.dependencies.mainQueue.async {
                completion(mappedResult)
            }
        }
    }
    
    func send(proposal: Proposal, pin: String, completion: @escaping ModelCompletionBlock<NoContent>) {
        let api = Api<NoContent>(endpoint: P2PLendingEndpoint.authenticatedSend(proposal: proposal, pin: pin))
        api.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model)
                completion(mappedResult)
            }
        }
    }
    
    func send(proposal: Proposal, completion: @escaping ModelCompletionBlock<NoContent>) {
        let api = Api<NoContent>(endpoint: P2PLendingEndpoint.send(proposal: proposal))
        api.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model)
                completion(mappedResult)
            }
        }
    }
}
