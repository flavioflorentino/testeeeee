import Foundation
import AnalyticsModule
import Core
import FeatureFlag

protocol LendingCCBInteracting: AnyObject {
    func fetchDocument()
    func authAndSendProposal()
    func sendAnalyticsEvent(type: AnalyticInteraction)
}

final class LendingCCBInteractor {
    typealias Dependencies = HasAnalytics & HasLegacy & HasFeatureManager & HasPPAuthSwiftContract
    private let dependencies: Dependencies
    private let service: LendingCCBServicing
    private let presenter: LendingCCBPresenting
    
    private let proposal: Proposal

    init(service: LendingCCBServicing, presenter: LendingCCBPresenting, proposal: Proposal, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.proposal = proposal
        self.dependencies = dependencies
    }
    
    private func sendAnalyticsEvent(_ event: LendingCCBEvent) {
        dependencies.analytics.log(event)
    }
    
    private func handle(error: ApiError) {
        guard
            let requestError = error.requestError,
            let identifiableError = CCBIdentifiableError(errorCode: requestError.code)
        else {
            presenter.present(error: error)
            return
        }
        
        switch identifiableError {
        case .activeProposalsLimitReached, .currentlyInvestor:
            presenter.didNextStep(action: .detailedError(identifiableError))
        case .accountUpgrade:
            presenter.didNextStep(action: .accountUpgrade(identifiableError))
        case .openProposalsLimitReached:
            presenter.didNextStep(action: .maximumOpenProposals(identifiableError))
        case .invalidPassword:
            dependencies.ppauth.handlePassword()
        }
    }
    
    private func sendProposal(withPin pin: String) {
        presenter.startLoading()
        service.send(proposal: proposal, pin: pin) { [weak self] result in
            self?.presenter.stopLoading()
            switch result {
            case .success:
                guard let self = self else { return }
                self.sendAnalyticsEvent(.sendProposalTapped)
                self.presenter.didNextStep(action: .proposalSent(delegate: self))
            case .failure(let error):
                self?.handle(error: error)
            }
        }
    }
    
    private func sendProposal() {
        presenter.startLoading()
        service.send(proposal: proposal) { [weak self] result in
            self?.presenter.stopLoading()
            switch result {
            case .success:
                guard let self = self else { return }
                self.sendAnalyticsEvent(.sendProposalTapped)
                self.presenter.didNextStep(action: .proposalSent(delegate: self))
            case .failure(let error):
                self?.handle(error: error)
            }
        }
    }
    
    private func handleFailureAuthentication(with status: AuthenticationStatus) {
        switch status {
        case .nilPassword:
            self.presenter.present(error: status)
        case .cancelled:
            return
        }
    }
}

// MARK: - LendingCCBInteracting
extension LendingCCBInteractor: LendingCCBInteracting {
    func fetchDocument() {
        presenter.startLoading()
        service.fetchCCB(for: proposal) { [weak self] result in
            self?.presenter.stopLoading()
            switch result {
            case .success(let ccbData):
                self?.presenter.presentDocument(ccbData: ccbData)
            case .failure(let error):
                self?.presenter.present(error: error)
            }
        }
    }
    
    func authAndSendProposal() {
        if dependencies.featureManager.isActive(.isP2PLendingCCBAuthAvailable) {
            dependencies.auth?.authenticate { [weak self] result in
                switch result {
                case let .success(pin):
                    self?.sendProposal(withPin: pin)
                case let .failure(status):
                    self?.handleFailureAuthentication(with: status)
                }
            }
        } else {
            sendProposal()
        }
    }
    
    func sendAnalyticsEvent(type: AnalyticInteraction) {
        switch type {
        case .backButtonTapped:
            sendAnalyticsEvent(.backButtonTapped)
        case .didScrollToBottom:
            sendAnalyticsEvent(.didReadContract)
        default:
            break
        }
    }
}

// MARK: - LendingSuccessDelegate
extension LendingCCBInteractor: LendingSuccessDelegate {
    func lendingSuccessControllerDidDismiss(_ lendingSuccessViewController: UIViewController) {
        dependencies.analytics.log(LendingSuccessEvent.didFinishFlow(.borrower))
    }
}
