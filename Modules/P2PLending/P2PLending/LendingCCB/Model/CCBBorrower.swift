struct CCBBorrower: Decodable, Equatable {
    let name: String
    let userName: String
    let document: String
    let address: String
    let email: String
}
