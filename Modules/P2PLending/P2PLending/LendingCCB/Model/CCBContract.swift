import Foundation

struct CCBContract: Decodable, Equatable {
    private typealias Localizable = Strings.Lending.Ccb
    
    enum Periodicity: String, Decodable, CustomStringConvertible {
        case monthly
        
        var description: String {
            Localizable.periodicityMonthly
        }
    }
    
    let issueDate: Date
    let issueCity: String
    let issueUf: String
    let comissionAmount: Double
    let firstDeadLine: Date
    let payDay: Int
    let installments: Int
    let totalRequestedAmount: Double
    let requestedAmount: Double
    let borrowerInstallmentsAmount: Double
    let totalInterest: Float
    let monthRate: Float
    let yearRate: Float
    let monthCet: Float
    let yearCet: Float
    let iof: Double
    let periodicity: Periodicity
    let finnancialFine: Float
    let finnancialSafe: Double
    let defaultInterest: Float
}
