import Foundation
import AssetsKit
import AnalyticsModule

enum CCBIdentifiableError: String {
    case openProposalsLimitReached = "5102"
    case activeProposalsLimitReached = "5107"
    case accountUpgrade = "5104"
    case currentlyInvestor = "5108"
    case invalidPassword = "6003"
    
    init?(errorCode: String) {
        self.init(rawValue: errorCode)
    }
}

extension CCBIdentifiableError: DetailedErrorConvertible {
    private typealias Localizable = Strings.Lending.DetailedError
    
    var asDetailedError: DetailedError {
        var image = Resources.Illustrations.iluWarning.image
        var title = Localizable.greetingsTitle
        var primaryButtonTitle = Localizable.dismissButtonTitle
        let message: String
        let linkButtonTitle: String
        var url: URL?
        switch self {
        case .openProposalsLimitReached:
            title = Localizable.openProposalsLimitReachedTitle
            message = Localizable.openProposalsLimitReachedBody
            primaryButtonTitle = Localizable.openProposalsLimitReachedPrimaryButtonTitle
            linkButtonTitle = GlobalLocalizable.dismissButtonTitle
            url = nil
        case .activeProposalsLimitReached:
            title = Localizable.activeProposalsLimitReachedTitle
            message = Localizable.activeProposalsLimitReachedBody
            linkButtonTitle = Localizable.lendingLimitsFaqButtonTitle
            url = URL(string: "picpay://picpay/helpcenter/article/360049860711")
        case .accountUpgrade:
            image = Assets.lendingFigureAccountUpgrade.image
            title = Localizable.accountUpgradeGreetingsTitle
            message = Localizable.accountUpgradeBody
            primaryButtonTitle = Localizable.accountUpgradePrimaryButtonTitle
            linkButtonTitle = Localizable.accountUpgradeLinkButtonTitle
            url = URL(string: "picpay://picpay/upgradeaccount")
        case .currentlyInvestor:
            title = Localizable.currentlyInvestorTitle
            message = Localizable.currentlyInvestorBody
            linkButtonTitle = Localizable.lendingLimitsFaqButtonTitle
            url = URL(string: "picpay://picpay/helpcenter/article/360049860711")
        // O erro .invalidPassword não é usado como detailedError, por isso estou passando strings vazias.
        // Esse erro dispara um método que usa um componente já pronto e não utiliza as informações do detailedError.
        case .invalidPassword:
            message = ""
            linkButtonTitle = ""
        }
        return DetailedError(
            image: image,
            title: title,
            message: message,
            primaryButtonTitle: primaryButtonTitle,
            linkButtonTitle: linkButtonTitle,
            url: url
        )
    }
}

extension CCBIdentifiableError: AnalyticsEventInteractionProvider {
    var properties: [String: Any] {
        switch self {
        case .openProposalsLimitReached, .accountUpgrade, .activeProposalsLimitReached, .currentlyInvestor:
            return [
                AnalyticsKeys.screenName.name: screenName,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        case .invalidPassword:
            return [:]
        }
    }
    
    var providers: [AnalyticsProvider] {
        [.eventTracker]
    }
    
    func analyticsEvent(for interaction: AnalyticInteraction) -> AnalyticsEvent {
        switch interaction {
        case .dismissed:
            var buttonProperties = properties
            buttonProperties[AnalyticsKeys.buttonName.name] = primaryButtonName
            return AnalyticsEvent(AnalyticInteraction.buttonClicked.name, properties: buttonProperties, providers: providers)
        case .deeplink:
            var buttonProperties = properties
            buttonProperties[AnalyticsKeys.buttonName.name] = linkButtonName
            return AnalyticsEvent(AnalyticInteraction.buttonClicked.name, properties: buttonProperties, providers: providers)
        default:
            return AnalyticsEvent(interaction.name, properties: properties, providers: providers)
        }
    }
}

extension CCBIdentifiableError {
    var screenName: String {
        switch self {
        case .openProposalsLimitReached:
            return "P2P_LENDING_TOMADOR_CONTRATO_LIMITE_PEDIDOS"
        case .accountUpgrade:
            return "P2P_LENDING_TOMADOR_CONTRATO_PRECISAMOS_DE_DADOS"
        case .activeProposalsLimitReached:
            return "P2P_LENDING_TOMADOR_CONTRATO_LIMITE_EMPRESTIMOS_ATIVOS"
        case .currentlyInvestor:
            return "P2P_LENDING_TOMADOR_CONTRATO_FINALIZAR_EMPRESTIMO"
        case .invalidPassword:
            return ""
        }
    }
    
    var linkButtonName: String {
        switch self {
        case .accountUpgrade:
            return AnalyticsConstants.notNowButton.description
        case .openProposalsLimitReached:
            return AnalyticsConstants.dismissButton.description
        default:
            return AnalyticsConstants.seeMoreButton.description
        }
    }
    
    var primaryButtonName: String {
        switch self {
        case .accountUpgrade:
            return AnalyticsConstants.startButton.description
        case .openProposalsLimitReached:
            return AnalyticsConstants.cancelAProposal.description
        default:
            return AnalyticsConstants.okButton.description
        }
    }
}
