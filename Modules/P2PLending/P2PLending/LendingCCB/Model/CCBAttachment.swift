struct CCBAttachment: Decodable, Equatable {
    struct Item: Decodable, Equatable {
        let value: Double
        let percent: Float?
    }
    
    let a: Item
    let b: Item
    let c: Item
    let c1: Item
    let c2: Item
    let c3: Item
    let c4: Item
}
