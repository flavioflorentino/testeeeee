struct CCBData: Decodable, Equatable {
    let contract: CCBContract
    let borrower: CCBBorrower
    let attachments: CCBAttachment
}
