import UIKit
import AnalyticsModule
import UI

enum LendingCCBAction {
    case proposalSent(delegate: LendingSuccessDelegate)
    case detailedError(_ detailedError: CCBIdentifiableError)
    case maximumOpenProposals(_ detailedError: CCBIdentifiableError)
    case accountUpgrade(_ detailedError: CCBIdentifiableError)
}

protocol LendingCCBCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: LendingCCBAction)
}

final class LendingCCBCoordinator: URLOpenerCoordinating {
    weak var viewController: UIViewController?
    typealias Dependencies = HasURLOpener & HasAnalytics
    private var dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - LendingCCBCoordinating
extension LendingCCBCoordinator: LendingCCBCoordinating {
    func perform(action: LendingCCBAction) {
        switch action {
        case .proposalSent(let delegate):
            let controller = LendingSuccessFactory.make(content: ProposalSentSuccessContent(), delegate: delegate)
            viewController?.show(controller, sender: nil)
        case .detailedError(let detailedError):
            showDetailedErrorFeedbackController(detailedError)
        case .maximumOpenProposals(let detailedError):
            showMaximumOpenProposalsFeedbackController(detailedError)
        case .accountUpgrade(let detailedError):
            showAccountUpgradeFeedbackController(detailedError)
        }
    }
}

private extension LendingCCBCoordinator {
    func makeApolloFeedbackViewController(error: CCBIdentifiableError) -> ApolloFeedbackViewController {
        let content = error.asFeedbackViewContent
        viewController?.navigationController?.setNavigationBarHidden(true, animated: true)
        return ApolloFeedbackViewController(content: content)
    }
    
    func showMaximumOpenProposalsFeedbackController(_ error: CCBIdentifiableError) {
        let apolloFeedbackController = makeApolloFeedbackViewController(error: error)
        
        apolloFeedbackController.onViewDidAppear = { [weak self] in
            self?.dependencies.analytics.log(error.analyticsEvent(for: .screenViewed))
        }
        
        apolloFeedbackController.didTapPrimaryButton = { [weak self] in
            let controller = LendingProposalListFactory.make(for: .borrower)
            apolloFeedbackController.navigationController?.setNavigationBarHidden(false, animated: true)
            apolloFeedbackController.navigationItem.setHidesBackButton(true, animated: true)
            apolloFeedbackController.show(controller, sender: nil)
            self?.dependencies.analytics.log(error.analyticsEvent(for: .dismissed))
        }
        
        apolloFeedbackController.didTapSecondaryButton = { [weak self] in
            apolloFeedbackController.dismiss(animated: true, completion: nil)
            self?.dependencies.analytics.log(error.analyticsEvent(for: .deeplink))
        }
        
        viewController?.show(apolloFeedbackController, sender: nil)
    }
    
    func showAccountUpgradeFeedbackController(_ error: CCBIdentifiableError) {
        let apolloFeedbackController = makeApolloFeedbackViewController(error: error)
        
        apolloFeedbackController.onViewDidAppear = { [weak self] in
            self?.dependencies.analytics.log(error.analyticsEvent(for: .screenViewed))
        }
        
        apolloFeedbackController.didTapPrimaryButton = {
            self.viewController?.dismiss(animated: true, completion: { [weak self] in
                guard let self = self else { return }
                guard let url = error.asDetailedError.url else { return }
                self.dependencies.analytics.log(error.analyticsEvent(for: .dismissed))
                self.showFAQ(for: url, using: self.dependencies)
            })
        }
        
        apolloFeedbackController.didTapSecondaryButton = { [weak self] in
            self?.dependencies.analytics.log(error.analyticsEvent(for: .deeplink))
            apolloFeedbackController.dismiss(animated: true, completion: nil)
        }
        
        viewController?.show(apolloFeedbackController, sender: nil)
    }
    
    func showDetailedErrorFeedbackController(_ error: CCBIdentifiableError) {
        let apolloFeedbackController = makeApolloFeedbackViewController(error: error)
        
        apolloFeedbackController.onViewDidAppear = {
            self.dependencies.analytics.log(error.analyticsEvent(for: .screenViewed))
        }
        
        apolloFeedbackController.didTapPrimaryButton = { [weak self] in
            self?.dependencies.analytics.log(error.analyticsEvent(for: .dismissed))
            apolloFeedbackController.dismiss(animated: true, completion: nil)
        }
        
        apolloFeedbackController.didTapSecondaryButton = { [weak self] in
            guard let self = self else { return }
            guard let url = error.asDetailedError.url else { return }
            self.dependencies.analytics.log(error.analyticsEvent(for: .deeplink))
            self.showFAQ(for: url, using: self.dependencies)
        }
        
        viewController?.show(apolloFeedbackController, sender: nil)
    }
}
