import Foundation
import UIKit

enum LendingCCBFactory {
    static func make(proposal: Proposal) -> LendingCCBViewController {
        let container = DependencyContainer()
        let service: LendingCCBServicing = LendingCCBService(dependencies: container)
        let coordinator: LendingCCBCoordinating = LendingCCBCoordinator(dependencies: container)
        let presenter: LendingCCBPresenting = LendingCCBPresenter(coordinator: coordinator)
        let interactor = LendingCCBInteractor(service: service, presenter: presenter, proposal: proposal, dependencies: container)
        let viewController = LendingCCBViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
