import Foundation
import Core

protocol LendingProposalAmountServicing: Servicing {
    func fetchProposalConditionsConfiguration(for amount: Double, completion: @escaping ModelCompletionBlock<ProposalConditionsConfiguration>)
}

final class LendingProposalAmountService: LendingProposalAmountServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    private lazy var decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }()
    
    func fetchProposalConditionsConfiguration(for amount: Double, completion: @escaping ModelCompletionBlock<ProposalConditionsConfiguration>) {
        let api = Api<ProposalConditionsConfiguration>(endpoint: P2PLendingEndpoint.proposalConditionsConfiguration(amount: amount))
        api.execute(jsonDecoder: decoder) { [weak self] result in
            let mappedResult = result.map(\.model)
            self?.dependencies.mainQueue.async {
                completion(mappedResult)
            }
        }
    }
}
