import Foundation
import AnalyticsModule

enum LendingProposalAmountEvent: AnalyticsKeyProtocol {
    case backButtonTapped
    case helpButtonTapped
    case choosePaymentButtonTapped
    
    private var title: String {
        "P2P_LENDING_TOMADOR_FACA_UMA_PROPOSTA"
    }
    
    var properties: [String: Any] {
        switch self {
        case .backButtonTapped:
            return [
                AnalyticsKeys.screenName.name: title,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.backButton.description,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        case .helpButtonTapped:
            return [
                AnalyticsKeys.screenName.name: title,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.help.description,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        case .choosePaymentButtonTapped:
            return [
                AnalyticsKeys.screenName.name: title,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.choosePayment.description,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(AnalyticInteraction.buttonClicked.name, properties: properties, providers: [.eventTracker])
    }
}
