enum LendingProposalAmountFactory {
    static func make(data: GreetingsResponse, proposal: Proposal) -> LendingProposalAmountViewController? {
        guard let proposalConfiguration = data.proposalConfiguration else {
            return nil
        }
        let container = DependencyContainer()
        let coordinator: LendingProposalAmountCoordinating = LendingProposalAmountCoordinator(dependencies: container)
        let service: LendingProposalAmountServicing = LendingProposalAmountService(dependencies: container)
        let presenter: LendingProposalAmountPresenting = LendingProposalAmountPresenter(coordinator: coordinator)
        let viewModel = LendingProposalAmountViewModel(
            presenter: presenter,
            service: service,
            proposalConfiguration: proposalConfiguration,
            proposal: proposal,
            dependencies: container
        )
        let viewController = LendingProposalAmountViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
