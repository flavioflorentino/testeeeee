import UI
import UIKit

protocol LendingProposalAmountDisplay: AnyObject {
    func displayAmountValueInstructions(string: NSAttributedString)
    func displayPaymentDateInstructions(string: NSAttributedString)
    func setContinueButtonEnabled(state: Bool)
    func startLoading()
    func stopLoading()
    func display(error: Error)
}

extension LendingProposalAmountViewController.Layout {
    enum Length {
        static let continueButtonHeight: CGFloat = 48.0
    }
}

final class LendingProposalAmountViewController: ViewController<LendingProposalAmountViewModelInputs, UIView> {
    fileprivate enum Layout {}
    
    enum AccessibilityIdentifier: String {
        case headlineLabel
        case amountTextField
        case amountInstructions
        case continueButton
    }
    
    private var activityIndicatorStyle: UIActivityIndicatorView.Style {
        guard
            #available(iOS 13, *),
            traitCollection.userInterfaceStyle == .dark
            else {
                return .gray
        }
        return .white
    }
    
    private lazy var activityIndicatorView = UIActivityIndicatorView(style: activityIndicatorStyle)
    
    private lazy var headlineLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.Lending.Proposal.Amount.headline
        label.accessibilityIdentifier = AccessibilityIdentifier.headlineLabel.rawValue
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, .grayscale900())
        return label
    }()
    
    private lazy var amountTextField: CurrencyField = {
        let textField = CurrencyField()
        textField.textColor = Colors.branding400.color
        textField.fontValue = Typography.title(.xLarge).font()
        textField.fontCurrency = Typography.title(.xLarge).font()
        textField.clearButtonMode = .always
        textField.positionCurrency = .center
        textField.accessibilityIdentifier = AccessibilityIdentifier.amountTextField.rawValue
        textField.tintColor = Colors.branding400.color
        textField.delegate = self
        return textField
    }()
    
    private lazy var amountInstructionsLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.accessibilityIdentifier = AccessibilityIdentifier.amountInstructions.rawValue
        return label
    }()
    
    private lazy var amountStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [amountTextField, amountInstructionsLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var headlineStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [headlineLabel, amountStackView])
        stackView.spacing = Spacing.base02
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var topStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [headlineStackView])
        stackView.spacing = Spacing.base03
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var paymentDateSummaryView: SummaryView = {
        let summaryView = SummaryView()
        summaryView.textLabel.numberOfLines = 0
        return summaryView
    }()
    
    private lazy var continueButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Strings.Lending.Proposal.Amount.confirmButtonTitle, for: .normal)
        button.accessibilityIdentifier = AccessibilityIdentifier.continueButton.rawValue
        button.addTarget(self, action: #selector(continueButtonTapped), for: .touchUpInside)
        button.isEnabled = false
        return button
    }()
    
    private lazy var footerStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [paymentDateSummaryView, continueButton])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [topStackView, footerStackView])
        stackView.spacing = Spacing.base02
        stackView.distribution = .equalSpacing
        stackView.axis = .vertical
        stackView.compatibleLayoutMargins = .rootView
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    
    private lazy var scrollView = UIScrollView()
    
    private lazy var helpButton: UIBarButtonItem = {
        let button = UIBarButtonItem(image: Assets.lendingGlyphHelp.image,
                                     style: .plain,
                                     target: self,
                                     action: #selector(helpButtonTapped))
        button.accessibilityLabel = GlobalLocalizable.helpButtonAccessibilityLabel
        return button
    }()
    
    private lazy var keyboardScrollViewHandler = KeyboardScrollViewHandler(scrollView: scrollView)
    
    private var rootStackViewHeightConstraint: NSLayoutConstraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.fetchAmountValueInstructions()
        registerForKeyboardNotifications()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        amountTextField.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if isMovingFromParent {
            viewModel.sendAnalyticsEvent(type: .backButtonTapped)
        }
    }
    
    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(rootStackView)
    }
    
    override func setupConstraints() {
        scrollView.layout {
            $0.top == view.topAnchor
            $0.trailing == view.trailingAnchor
            $0.bottom == view.bottomAnchor
            $0.leading == view.leadingAnchor
        }
        
        rootStackView.layout {
            $0.top == scrollView.topAnchor
            $0.trailing == scrollView.trailingAnchor
            $0.bottom == scrollView.bottomAnchor
            $0.leading == scrollView.leadingAnchor
            $0.width == scrollView.widthAnchor
            rootStackViewHeightConstraint = $0.height >= view.compatibleSafeAreaLayoutGuide.heightAnchor
        }
        
        continueButton.layout {
            $0.height == Layout.Length.continueButtonHeight
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        view.layoutMargins.bottom = Spacing.base02
        navigationItem.title = Strings.Lending.Proposal.Amount.title
        navigationItem.setRightBarButtonItems([helpButton, UIBarButtonItem(customView: activityIndicatorView)], animated: false)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard
            #available(iOS 13, *),
            traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection)
            else {
                return
        }
        activityIndicatorView.style = activityIndicatorStyle
    }
    
    private func updateTootStackViewHeightConstraint(constant: CGFloat) {
        rootStackViewHeightConstraint?.constant = constant
        scrollView.setNeedsLayout()
        scrollView.layoutIfNeeded()
    }
}

extension LendingProposalAmountViewController: CurrencyFieldDelegate {
    func onValueChange(value: Double) {
        viewModel.amountValueChanged(value)
    }
}

@objc
extension LendingProposalAmountViewController {
    func continueButtonTapped() {
        viewModel.continueButtonTapped()
    }
    
    func registerForKeyboardNotifications() {
        keyboardScrollViewHandler.registerForKeyboardNotifications()
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        notificationCenter.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    
    func keyboardWillShow(_ notification: Notification) {
        let keyboardInfo = notification.userInfo
        guard let keyboardFrame = keyboardInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect else {
            return
        }
        var offsetY = -keyboardFrame.height
        if #available(iOS 11, *) {
            offsetY += view.safeAreaInsets.bottom - Spacing.base02
        }
        updateTootStackViewHeightConstraint(constant: offsetY)
    }
    
    func keyboardWillHide(_ notification: Notification) {
        updateTootStackViewHeightConstraint(constant: .zero)
    }
    
    func helpButtonTapped() {
        viewModel.showHelp()
    }
}

// MARK: View Model Outputs
extension LendingProposalAmountViewController: LendingProposalAmountDisplay {
    func displayAmountValueInstructions(string: NSAttributedString) {
        amountInstructionsLabel.attributedText = string
    }
    
    func displayPaymentDateInstructions(string: NSAttributedString) {
        paymentDateSummaryView.textLabel.attributedText = string
    }
    
    func setContinueButtonEnabled(state: Bool) {
        continueButton.isEnabled = state
    }
    
    func startLoading() {
        activityIndicatorView.startAnimating()
        continueButton.isEnabled = false
    }
    
    func stopLoading() {
        activityIndicatorView.stopAnimating()
        continueButton.isEnabled = true
    }
    
    func display(error: Error) {
        let popUp = PopupViewController(
            title: GlobalLocalizable.errorAlertTitle,
            description: error.localizedDescription,
            preferredType: .image(Assets.lendingFigureFailure.image)
        )
        let dismissAction = PopupAction(
            title: GlobalLocalizable.dismissButtonTitle,
            style: .default
        )
        popUp.addAction(dismissAction)
        showPopup(popUp)
    }
}
