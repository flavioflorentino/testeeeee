import Foundation
import UI

protocol LendingProposalAmountPresenting: AnyObject {
    var viewController: LendingProposalAmountDisplay? { get set }
    func presentAmountValueInstructions(using configuration: ProposalAmountConfiguration)
    func presentPaymentDateInstructions(for proposal: Proposal)
    func presentAmountValueOutOfBounds(using configuration: ProposalAmountConfiguration)
    func setContinueButtonEnabled(state: Bool)
    func startLoading()
    func stopLoading()
    func present(error: Error)
    func didNextStep(action: LendingProposalAmountAction)
}

final class LendingProposalAmountPresenter: LendingProposalAmountPresenting {
    private typealias Localizable = Strings.Lending.Proposal.Amount
    
    private let coordinator: LendingProposalAmountCoordinating
    weak var viewController: LendingProposalAmountDisplay?

    init(coordinator: LendingProposalAmountCoordinating) {
        self.coordinator = coordinator
    }
    
    func presentAmountValueInstructions(using configuration: ProposalAmountConfiguration) {
        let formattedMinimumValue = configuration.minimumAmount.toCurrencyString() ?? ""
        let formattedMaximumValue = configuration.maximumAmount.toCurrencyString() ?? ""
        let string = NSAttributedString(
            string: Localizable.amountValueInstructions(formattedMinimumValue, formattedMaximumValue),
            attributes: [
                .font: Typography.caption().font(),
                .foregroundColor: Colors.grayscale600.color
            ]
        )
        viewController?.displayAmountValueInstructions(string: string)
    }
    
    func presentPaymentDateInstructions(for proposal: Proposal) {
        let paymentDate = proposal.firstPaymentDeadline ?? 0
        let paymentDateString = NSString(string: Localizable.paymentDateFooterText(paymentDate))
        let paymentDateAttributedText = NSMutableAttributedString(
            string: paymentDateString as String,
            attributes: [.font: Typography.bodyPrimary().font(), .foregroundColor: Colors.grayscale600.color]
        )
        let paymentDateStringHighlightRange = paymentDateString.range(of: Localizable.paymentDateFooterHighlightText(paymentDate))
        paymentDateAttributedText.addAttribute(.font,
                                               value: Typography.bodyPrimary(.highlight).font(),
                                               range: paymentDateStringHighlightRange)
        viewController?.displayPaymentDateInstructions(string: paymentDateAttributedText)
    }
    
    func presentAmountValueOutOfBounds(using configuration: ProposalAmountConfiguration) {
        let formattedMinimumValue = configuration.minimumAmount.toCurrencyString() ?? ""
        let formattedMaximumValue = configuration.maximumAmount.toCurrencyString() ?? ""
        let attributedString = NSAttributedString(
            string: Localizable.amountValueInstructions(formattedMinimumValue, formattedMaximumValue),
            attributes: [
                .font: Typography.caption().font(),
                .foregroundColor: Colors.critical600.color
            ]
        )
        viewController?.displayAmountValueInstructions(string: attributedString)
    }
    
    func setContinueButtonEnabled(state: Bool) {
        viewController?.setContinueButtonEnabled(state: state)
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
    
    func present(error: Error) {
        viewController?.display(error: error)
    }
    
    func didNextStep(action: LendingProposalAmountAction) {
        coordinator.perform(action: action)
    }
}
