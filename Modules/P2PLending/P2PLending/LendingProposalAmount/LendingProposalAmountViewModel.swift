import AnalyticsModule
import Foundation

protocol LendingProposalAmountViewModelInputs: AnyObject {
    func fetchAmountValueInstructions()
    func amountValueChanged(_ amountValue: Double)
    func continueButtonTapped()
    func showHelp()
    func sendAnalyticsEvent(type: AnalyticInteraction)
}

final class LendingProposalAmountViewModel {
    typealias Dependencies = HasAnalytics
    private let presenter: LendingProposalAmountPresenting
    private let proposalConfiguration: ProposalAmountConfiguration
    private let service: LendingProposalAmountServicing
    private var dependencies: Dependencies
    
    private var proposal: Proposal
    
    init(
        presenter: LendingProposalAmountPresenting,
        service: LendingProposalAmountServicing,
        proposalConfiguration: ProposalAmountConfiguration,
        proposal: Proposal,
        dependencies: Dependencies
    ) {
        self.presenter = presenter
        self.service = service
        self.proposalConfiguration = proposalConfiguration
        self.proposal = proposal
        self.dependencies = dependencies
    }
    
    private func isAmountValueInBounds(_ amountValue: Double) -> Bool {
        let bounds = proposalConfiguration.minimumAmount...proposalConfiguration.maximumAmount
        return bounds.contains(amountValue)
    }
    
    private func sendAnalyticsEvent(_ event: LendingProposalAmountEvent) {
        dependencies.analytics.log(event)
    }
}

extension LendingProposalAmountViewModel: LendingProposalAmountViewModelInputs {
    func fetchAmountValueInstructions() {
        presenter.presentAmountValueInstructions(using: proposalConfiguration)
        presenter.presentPaymentDateInstructions(for: proposal)
    }
    
    func amountValueChanged(_ amountValue: Double) {
        let isAmountValueInBounds = self.isAmountValueInBounds(amountValue)
        proposal.amount = amountValue
        presenter.setContinueButtonEnabled(state: isAmountValueInBounds)
        if isAmountValueInBounds {
            presenter.presentAmountValueInstructions(using: proposalConfiguration)
        } else {
            presenter.presentAmountValueOutOfBounds(using: proposalConfiguration)
        }
    }
    
    func continueButtonTapped() {
        var proposal = self.proposal
        presenter.startLoading()
        service.fetchProposalConditionsConfiguration(for: proposal.amount) { [weak self] result in
            self?.presenter.stopLoading()
            switch result {
            case .success(let configuration):
                proposal.installments = configuration.defaultInstallments
                proposal.interest = configuration.defaultTax
                self?.sendAnalyticsEvent(.choosePaymentButtonTapped)
                self?.presenter.didNextStep(action: .proposalConditions(configuration: configuration, proposal: proposal))
            case .failure(let error):
                self?.presenter.present(error: error)
            }
        }
    }
    
    func showHelp() {
        guard let url = URL(string: "picpay://picpay/helpcenter/article/360049386752") else {
            return
        }
        sendAnalyticsEvent(.helpButtonTapped)
        presenter.didNextStep(action: .showFAQ(url: url))
    }
    
    func sendAnalyticsEvent(type: AnalyticInteraction) {
        switch type {
        case .backButtonTapped:
            sendAnalyticsEvent(.backButtonTapped)
        default:
            break
        }
    }
}
