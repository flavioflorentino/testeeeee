import UIKit

enum LendingProposalAmountAction {
    case proposalConditions(configuration: ProposalConditionsConfiguration, proposal: Proposal)
    case showFAQ(url: URL)
}

protocol LendingProposalAmountCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: LendingProposalAmountAction)
}

final class LendingProposalAmountCoordinator: LendingProposalAmountCoordinating, URLOpenerCoordinating {
    typealias Dependencies = HasURLOpener
    let dependencies: Dependencies
    
    weak var viewController: UIViewController?
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func perform(action: LendingProposalAmountAction) {
        switch action {
        case let .proposalConditions(configuration, proposal):
            let controller = LendingProposalConditionsFactory.make(configuration: configuration, proposal: proposal)
            viewController?.show(controller, sender: nil)
        case let .showFAQ(url):
            showFAQ(for: url, using: dependencies)
        }
    }
}
