struct Proposal: Equatable {
    var amount: Double
    var installments: Int?
    var interest: Float?
    var objective: ProposalObjective?
    var firstPaymentDeadline: Int?
    var message: String?
    var friend: LendingFriend?
    var shouldShareCreditAptitude: Bool?
    var simulation: ProposalConditionsSimulation?
}
