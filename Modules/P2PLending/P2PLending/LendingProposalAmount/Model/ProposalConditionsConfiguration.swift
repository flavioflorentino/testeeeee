struct ProposalConditionsConfiguration: Decodable, Equatable {
    let maximumInstallments: Int
    let minimumInstallments: Int
    let defaultInstallments: Int
    let minimumInstallmentsValue: Double
    let minimumTax: Float
    let maximumTax: Float
    let defaultTax: Float
}
