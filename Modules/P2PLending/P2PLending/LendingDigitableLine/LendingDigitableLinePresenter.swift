import Foundation
import CoreFormatter
import UI

protocol LendingDigitableLinePresenting: AnyObject {
    var viewController: LendingDigitableLineDisplaying? { get set }
    func present(installmentDetail: InstallmentDetail)
    func presentDigitableLineCopiedFeedback()
}

final class LendingDigitableLinePresenter: LendingDigitableLinePresenting {
    private typealias Localizable = Strings.Lending.Digitable.Line
    
    weak var viewController: LendingDigitableLineDisplaying?
    
    private func formattedDate(for installmentDetail: InstallmentDetail) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "pt-BR")
        dateFormatter.setLocalizedDateFormatFromTemplate("ddMMMM")
        return dateFormatter.string(from: installmentDetail.dueDate)
    }
    
    private func formattedValue(for installmentDetail: InstallmentDetail) -> String {
        CurrencyFormatter.string(from: installmentDetail.value) ?? "-"
    }
    
    private func bodyText(for installmentDetail: InstallmentDetail) -> NSAttributedString {
        let formattedDate = self.formattedDate(for: installmentDetail)
        let formattedValue = self.formattedValue(for: installmentDetail)
        
        let nsstring = Localizable.body(formattedDate, formattedValue) as NSString
        let textAttributes: [NSAttributedString.Key: Any] = [
            .font: Typography.bodyPrimary().font(),
            .foregroundColor: Colors.black.color
        ]
        let dateAttributes: [NSAttributedString.Key: Any] = [.font: Typography.bodyPrimary(.highlight).font()]
        let valueAttributes: [NSAttributedString.Key: Any] = [
            .font: Typography.bodyPrimary(.highlight).font(),
            .foregroundColor: Colors.neutral600.color
        ]
        let bodyText = NSMutableAttributedString(string: nsstring as String, attributes: textAttributes)
        
        bodyText.addAttributes(dateAttributes, range: nsstring.range(of: formattedDate))
        bodyText.addAttributes(valueAttributes, range: nsstring.range(of: formattedValue))
        
        return bodyText
    }
    
    func present(installmentDetail: InstallmentDetail) {
        viewController?.display(bodyText: bodyText(for: installmentDetail), digitableLine: installmentDetail.digitableLine)
    }
    
    func presentDigitableLineCopiedFeedback() {
        viewController?.displayToast(text: Localizable.digitableLineCopied)
    }
}
