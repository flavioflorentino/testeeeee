import UI
import UIKit

protocol LendingDigitableLineDisplaying: AnyObject {
    func display(bodyText: NSAttributedString, digitableLine: String)
    func displayToast(text: String)
}

final class LendingDigitableLineViewController: ViewController<LendingDigitableLineInteracting, UIView> {
    private typealias Localizable = Strings.Lending.Digitable.Line
    
    private lazy var illustrationImageView: UIImageView = {
        let imageView = UIImageView(image: Assets.lendingIllustrationPaperStrip.image)
        imageView.contentMode = .center
        return imageView
    }()
    
    private lazy var bodyLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var digitableLineLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.digitableLineLabel
        label.labelStyle(BodyPrimaryLabelStyle())
        label.textColor = Colors.grayscale700.color
        label.textAlignment = .center
        return label
    }()
    
    private lazy var digitableLineValueLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
        label.textAlignment = .center
        return label
    }()
    
    private lazy var digitableLineStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [digitableLineLabel, digitableLineValueLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var bodyStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [bodyLabel, digitableLineStackView])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base03
        return stackView
    }()
    
    private lazy var primaryButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.primaryButtonTitle, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(primaryActionButtonTapped), for: .touchUpInside)
        return button
    }()
    
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [illustrationImageView, bodyStackView, primaryButton])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base04
        stackView.compatibleLayoutMargins = .rootView
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    
    private lazy var contentView = UIView()
    
    private lazy var scrollView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.fetchInstallmentDetails()
    }

    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(rootStackView)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        contentView.snp.makeConstraints {
            $0.edges.width.equalToSuperview()
            $0.height.greaterThanOrEqualToSuperview()
        }
        
        rootStackView.snp.makeConstraints {
            $0.leading.trailing.centerY.equalToSuperview()
            $0.height.lessThanOrEqualToSuperview()
        }
    }

    override func configureViews() {
        title = Localizable.title
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}

@objc
extension LendingDigitableLineViewController {
    func primaryActionButtonTapped() {
        interactor.copyDigitableLine()
    }
}

// MARK: - LendingDigitableLineDisplaying
extension LendingDigitableLineViewController: LendingDigitableLineDisplaying {
    func display(bodyText: NSAttributedString, digitableLine: String) {
        bodyLabel.attributedText = bodyText
        digitableLineValueLabel.text = digitableLine
    }
    
    func displayToast(text: String) {
        showSnackbar(ApolloSnackbar(text: text, iconType: .success))
    }
}
