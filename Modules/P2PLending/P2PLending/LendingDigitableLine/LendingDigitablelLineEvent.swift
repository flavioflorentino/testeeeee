import AnalyticsModule
import Foundation

enum LendingDigitablelLineEvent: AnalyticsKeyProtocol {
    case copyDigitableLine
    
    private var screenName: String {
        "P2P_LENDING_TOMADOR_PAGAR_PARCELA_PAGAMENTO_BOLETO"
    }
    
    private var name: String {
        switch self {
        case .copyDigitableLine:
            return AnalyticInteraction.buttonClicked.name
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .copyDigitableLine:
            return [
                AnalyticsKeys.screenName.name: screenName,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.buttonName.name: "COPIAR_CODIGO"
            ]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.eventTracker])
    }
}
