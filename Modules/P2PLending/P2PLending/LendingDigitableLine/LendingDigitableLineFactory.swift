import UIKit

enum LendingDigitableLineFactory {
    static func make(installmentDetail: InstallmentDetail) -> LendingDigitableLineViewController {
        let container = DependencyContainer()
        let presenter: LendingDigitableLinePresenting = LendingDigitableLinePresenter()
        let interactor = LendingDigitableLineInteractor(presenter: presenter,
                                                        installmentDetail: installmentDetail,
                                                        pasteboard: UIPasteboard.general,
                                                        dependencies: container)
        let viewController = LendingDigitableLineViewController(interactor: interactor)
        
        presenter.viewController = viewController

        return viewController
    }
}
