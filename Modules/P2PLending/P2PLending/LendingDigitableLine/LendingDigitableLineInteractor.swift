import Foundation
import AnalyticsModule

protocol LendingDigitableLineInteracting: AnyObject {
    func fetchInstallmentDetails()
    func copyDigitableLine()
}

final class LendingDigitableLineInteractor: LendingDigitableLineInteracting {
    typealias Dependencies = HasAnalytics
    private let presenter: LendingDigitableLinePresenting
    private let installmentDetail: InstallmentDetail
    private let pasteboard: StringPasteboardProvider
    private let dependencies: Dependencies

    init(presenter: LendingDigitableLinePresenting, installmentDetail: InstallmentDetail, pasteboard: StringPasteboardProvider, dependencies: Dependencies) {
        self.presenter = presenter
        self.installmentDetail = installmentDetail
        self.pasteboard = pasteboard
        self.dependencies = dependencies
    }
    
    func fetchInstallmentDetails() {
        presenter.present(installmentDetail: installmentDetail)
    }
    
    func copyDigitableLine() {
        dependencies.analytics.log(LendingDigitablelLineEvent.copyDigitableLine)
        pasteboard.string = installmentDetail.digitableLine
        presenter.presentDigitableLineCopiedFeedback()
    }
}
