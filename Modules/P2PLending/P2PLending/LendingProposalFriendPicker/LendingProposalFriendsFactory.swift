import Foundation

enum LendingProposalFriendsFactory {
    static func make(data: GreetingsResponse, proposal: Proposal) -> LendingProposalFriendsViewController {
        let container = DependencyContainer()
        let service: LendingProposalFriendsServicing = LendingProposalFriendsService(dependencies: container)
        let coordinator: LendingProposalFriendsCoordinating = LendingProposalFriendsCoordinator(data: data, dependencies: container)
        let presenter: LendingProposalFriendsPresenting = LendingProposalFriendsPresenter(coordinator: coordinator)
        let viewModel = LendingProposalFriendsViewModel(service: service,
                                                        presenter: presenter,
                                                        proposal: proposal,
                                                        dependencies: container)
        let viewController = LendingProposalFriendsViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
