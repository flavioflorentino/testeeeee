import Core
import UI

protocol LendingProposalFriendsPresenting: AnyObject {
    var viewController: LendingProposalFriendsDisplay? { get set }
    func present(friends: [LendingFriend])
    func presentEmptyState()
    func presentEmptySearchResults()
    func startLoadingList()
    func stopLoadingList()
    func startLoadingFriend(at indexPath: IndexPath)
    func stopLoadingFriend(at indexPath: IndexPath)
    func hideEmptyState()
    func presentEmptyState(error: Error)
    func present(error: Error)
    func didNextStep(action: LendingProposalFriendsAction)
}

final class LendingProposalFriendsPresenter: LendingProposalFriendsPresenting {
    private let coordinator: LendingProposalFriendsCoordinating
    weak var viewController: LendingProposalFriendsDisplay?

    init(coordinator: LendingProposalFriendsCoordinating) {
        self.coordinator = coordinator
    }
    
    func present(friends: [LendingFriend]) {
        viewController?.display(friends: friends)
    }
    
    func presentEmptyState() {
        viewController?.displayEmptyState(
            title: Strings.Lending.Proposal.Friends.emptyListTitle,
            message: Strings.Lending.Proposal.Friends.emptyListMessage
        )
    }
    
    func presentEmptySearchResults() {
        viewController?.displayEmptyState(
            title: Strings.Lending.Proposal.Friends.emptySearchResultsTitle,
            message: Strings.Lending.Proposal.Friends.emptySearchResultsMessage
        )
    }
    
    func startLoadingList() {
        viewController?.hideEmptyState()
        viewController?.startLoadingList()
    }
    
    func stopLoadingList() {
        viewController?.stopLoadingList()
    }
    
    func startLoadingFriend(at indexPath: IndexPath) {
        viewController?.startLoadingFriend(at: indexPath)
    }
    
    func stopLoadingFriend(at indexPath: IndexPath) {
        viewController?.stopLoadingFriend(at: indexPath)
    }
    
    func hideEmptyState() {
        viewController?.hideEmptyState()
    }
    
    func presentEmptyState(error: Error) {
        viewController?.display(friends: [])
        viewController?.displayEmptyState(error: error)
    }
    
    func present(error: Error) {
        viewController?.displayError(error: error)
    }
    
    func didNextStep(action: LendingProposalFriendsAction) {
        coordinator.perform(action: action)
    }
}
