import Foundation
import Core

protocol LendingProposalFriendsServicing: Servicing {
    func fetchFriends(scope: SearchScope, query: String?, completion: @escaping ModelCompletionBlock<[LendingFriend]>)
    func checkInvestorEligibility(for friend: LendingFriend, completion: @escaping ModelCompletionBlock<NoContent>)
}

final class LendingProposalFriendsService: LendingProposalFriendsServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    private lazy var decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }()

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func fetchFriends(scope: SearchScope, query: String?, completion: @escaping ModelCompletionBlock<[LendingFriend]>) {
        let api = Api<[SearchResponse]>(endpoint: P2PLendingEndpoint.friends(scope: scope, query: query))
        api.execute(jsonDecoder: decoder) { [weak self] result in
            let mappedResult = result.map { response -> [LendingFriend] in
                response.model.first?.rows.compactMap(LendingFriend.init(searchResponseRow:)) ?? []
            }
            self?.dependencies.mainQueue.async {
                completion(mappedResult)
            }
        }
    }
    
    func checkInvestorEligibility(for friend: LendingFriend, completion: @escaping ModelCompletionBlock<NoContent>) {
        let api = Api<NoContent>(endpoint: P2PLendingEndpoint.investorEligibility(friend: friend))
        api.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
