import Foundation
import AnalyticsModule

enum LendingProposalFriendsEvent: AnalyticsKeyProtocol {
    case screenViewed(Bool)
    case backButtonTapped(Bool)
    case helpButtonTapped(Bool)
    case searchAccessed(Bool)
    
    var name: String {
        switch self {
        case .screenViewed:
            return AnalyticInteraction.screenViewed.name
        case .backButtonTapped, .helpButtonTapped:
            return AnalyticInteraction.buttonClicked.name
        case .searchAccessed:
            return AnalyticInteraction.textBoxClicked.name
        }
    }
    
    var properties: [String: Any] {
        switch self {
        case .screenViewed(let isFromFriendListError):
            return [
                AnalyticsKeys.screenName.name: getTitle(isFromFriendListError),
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        case .backButtonTapped(let isFromFriendListError):
            return [
                AnalyticsKeys.screenName.name: getTitle(isFromFriendListError),
                AnalyticsKeys.buttonName.name: AnalyticsConstants.backButton.description,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        case .helpButtonTapped(let isFromFriendListError):
            return [
                AnalyticsKeys.screenName.name: getTitle(isFromFriendListError),
                AnalyticsKeys.buttonName.name: AnalyticsConstants.help.description,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        case .searchAccessed(let isFromFriendListError):
            return [
                AnalyticsKeys.screenName.name: getTitle(isFromFriendListError),
                AnalyticsKeys.textBoxName.name: AnalyticsConstants.textBoxNameSearch.description,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        }
    }
    
    private func getTitle(_ isFromFriendListError: Bool) -> String {
        isFromFriendListError ? "P2P_LENDING_TOMADOR_ESCOLHA_UM_AMIGO_ERRO" : "P2P_LENDING_TOMADOR_ESCOLHA_UM_AMIGO"
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.eventTracker])
    }
}
