import UI
import UIKit

extension FriendTableViewCell.Layout {
    enum Size {
        static let photo = CGSize(width: 40, height: 40)
    }
}

final class FriendTableViewCell: UITableViewCell {
    fileprivate enum Layout {}

    static let identifier = String(describing: FriendTableViewCell.self)
    
    private lazy var lendingFriendView = LendingFriendView()
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(style: .gray)
        activityIndicatorView.color = Colors.grayscale300.color
        return activityIndicatorView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(with friend: LendingFriend, isLoading: Bool = false) {
        lendingFriendView.setup(with: friend)
        if isLoading {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        lendingFriendView.cancelImageRequest()
        hideSkeleton()
    }
}

extension FriendTableViewCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(lendingFriendView)
        accessoryView = activityIndicator
    }
    
    func setupConstraints() {
        lendingFriendView.snp.makeConstraints {
            $0.edges.equalTo(contentView.layoutMarginsGuide)
        }
    }
    
    func configureViews() {
        backgroundColor = .clear
        isSkeletonable = true
        contentView.isSkeletonable = true
    }
}
