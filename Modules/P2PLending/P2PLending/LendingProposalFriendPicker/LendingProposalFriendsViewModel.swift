import Foundation
import AnalyticsModule
import Core

protocol LendingProposalFriendsViewModelInputs: AnyObject {
    func fetchFriends()
    func filterFriends(query: String?)
    func selectFriend(at indexPath: IndexPath)
    func sendAnalyticsEvent(_ event: AnalyticInteraction)
    func showHelp()
}

final class LendingProposalFriendsViewModel {
    typealias Dependencies = HasAnalytics
    private let service: LendingProposalFriendsServicing
    private let presenter: LendingProposalFriendsPresenting
    private var proposal: Proposal
    private let shouldDelayFilterRequests: Bool
    private var filterRequestTimer: Timer?
    private var dependencies: Dependencies
    private var isFromFriendListError: Bool = false
    
    private var friends: [LendingFriend] = []

    init(
        service: LendingProposalFriendsServicing,
        presenter: LendingProposalFriendsPresenting,
        proposal: Proposal,
        dependencies: Dependencies,
        shouldDelayFilterRequests: Bool = true
    ) {
        self.service = service
        self.presenter = presenter
        self.proposal = proposal
        self.dependencies = dependencies
        self.shouldDelayFilterRequests = shouldDelayFilterRequests
    }
    
    private func searchScope(for query: String?) -> SearchScope {
        guard query?.isEmpty == false else { return .contacts }
        return .consumers
    }
    
    private func fetchFriends(query: String?) {
        presenter.startLoadingList()
        service.fetchFriends(scope: searchScope(for: query), query: query) { [weak self] result in
            self?.presenter.stopLoadingList()
            switch result {
            case .success(let friends):
                self?.friends = friends
                self?.didFetchFriends(isFiltering: query != nil)
                self?.isFromFriendListError = false
            case .failure(let error):
                self?.presenter.presentEmptyState(error: error)
                self?.sendAnalyticsEvent(.screenViewed(true))
                self?.isFromFriendListError = true
            }
        }
    }
    
    private func didFetchFriends(isFiltering: Bool) {
        presenter.present(friends: friends)
        guard friends.isEmpty else {
            presenter.hideEmptyState()
            return
        }
        guard isFiltering else {
            presenter.presentEmptyState()
            return
        }
        presenter.presentEmptySearchResults()
    }
    
    private func sendAnalyticsEvent(_ event: LendingProposalFriendsEvent) {
        dependencies.analytics.log(event)
    }
    
    private func handle(error: ApiError) {
        guard
            let requestError = error.requestError,
            let identifiableError = IdentifiableFriendsError(rawValue: requestError.code)
        else {
            presenter.present(error: error)
            return
        }
        switch identifiableError {
        case .accountUpgrade:
            presenter.didNextStep(action: .accountUpgrade(error: identifiableError))
        case .activeProposalsLimitReached, .currentlyBorrower:
            presenter.didNextStep(action: .detailedError(error: identifiableError))
        }
    }
}

extension LendingProposalFriendsViewModel: LendingProposalFriendsViewModelInputs {
    func fetchFriends() {
        fetchFriends(query: nil)
    }
    
    func filterFriends(query: String?) {
        guard shouldDelayFilterRequests else {
            fetchFriends(query: query)
            return
        }
        filterRequestTimer?.invalidate()
        filterRequestTimer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { [weak self] _ in
            self?.fetchFriends(query: query)
        }
    }
    
    func selectFriend(at indexPath: IndexPath) {
        let friend = friends[indexPath.row]
        var proposal = self.proposal
        presenter.startLoadingFriend(at: indexPath)
        service.checkInvestorEligibility(for: friend) { [weak self] result in
            self?.presenter.stopLoadingFriend(at: indexPath)
            switch result {
            case .success:
                proposal.friend = friend
                self?.presenter.didNextStep(action: .proposalDescription(proposal: proposal))
            case .failure(let error):
                self?.handle(error: error)
            }
        }
    }
    
    func sendAnalyticsEvent(_ event: AnalyticInteraction) {
        switch event {
        case .searchBarTextDidBeginEditing:
            sendAnalyticsEvent(.searchAccessed(isFromFriendListError))
        case .backButtonTapped:
            sendAnalyticsEvent(.backButtonTapped(isFromFriendListError))
        case .screenViewed:
            sendAnalyticsEvent(.screenViewed(isFromFriendListError))
        default:
            break
        }
    }
    
    func showHelp() {
        guard let faqURL = URL(string: "picpay://picpay/helpcenter/article/360049386452") else {
            return
        }
        sendAnalyticsEvent(.helpButtonTapped(isFromFriendListError))
        presenter.didNextStep(action: .showFAQ(url: faqURL))
    }
}
