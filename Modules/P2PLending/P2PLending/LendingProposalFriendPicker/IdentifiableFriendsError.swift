import AssetsKit
import AnalyticsModule

enum IdentifiableFriendsError: String {
    case accountUpgrade = "5104"
    case activeProposalsLimitReached = "5105"
    case currentlyBorrower = "5106"
}

extension IdentifiableFriendsError: DetailedErrorConvertible {
    private typealias Localizable = Strings.Lending.DetailedError
    
    var asDetailedError: DetailedError {
        var image = Resources.Illustrations.iluWarning.image
        var title: String
        var message: String
        var primaryButtonTitle = Localizable.chooseSomebodyElseButtonTitle
        var linkButtonTitle = Localizable.dismissButtonTitle
        var url: URL?
        switch self {
        case .accountUpgrade:
            image = Resources.Illustrations.iluFillingFormCircledBackground.image
            title = Localizable.searchAccountUpgradeTitle
            message = Localizable.searchAccountUpgradeBody
            primaryButtonTitle = Localizable.chooseSomebodyElseButtonTitle
            linkButtonTitle = Localizable.learnMoreAboutUpgradeButtonTitle
            url = URL(string: "picpay://picpay/helpcenter/article/360057033251")
        case .activeProposalsLimitReached:
            title = Localizable.investorActiveProposalsLimitReachedTitle
            message = Localizable.investorActiveProposalsLimitReachedBody
        case .currentlyBorrower:
            title = Localizable.currentlyBorrowerTitle
            message = Localizable.currentlyBorrowerBody
        }
        return DetailedError(
            image: image,
            title: title,
            message: message,
            primaryButtonTitle: primaryButtonTitle,
            linkButtonTitle: linkButtonTitle,
            url: url
        )
    }
}

extension IdentifiableFriendsError: AnalyticsEventInteractionProvider {
    var properties: [String: Any] {
        switch self {
        case .accountUpgrade, .activeProposalsLimitReached, .currentlyBorrower:
            return [
                AnalyticsKeys.screenName.name: screenName,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        }
    }
    
    var providers: [AnalyticsProvider] {
        [.eventTracker]
    }
    
    func analyticsEvent(for interaction: AnalyticInteraction) -> AnalyticsEvent {
        switch interaction {
        case .dismissed:
            var buttonProperties = properties
            buttonProperties[AnalyticsKeys.buttonName.name] = AnalyticsConstants.chooseSomebodyElse.description
            return AnalyticsEvent(AnalyticInteraction.buttonClicked.name, properties: buttonProperties, providers: providers)
        case .deeplink:
            var buttonProperties = properties
            buttonProperties[AnalyticsKeys.buttonName.name] = linkButtonName
            return AnalyticsEvent(AnalyticInteraction.buttonClicked.name, properties: buttonProperties, providers: providers)
        default:
            return AnalyticsEvent(interaction.name, properties: properties, providers: providers)
        }
    }
}

extension IdentifiableFriendsError {
    var screenName: String {
        switch self {
        case .accountUpgrade:
            return "P2P_LENDING_TOMADOR_ALERTA_INVESTIDOR_NAO_POSSUI_UPGRADE"
        case .activeProposalsLimitReached:
            return "P2P_LENDING_TOMADOR_ALERTA_INVESTIDOR_ATINGIU_LIMITE_CONTRATOS"
        case .currentlyBorrower:
            return "P2P_LENDING_TOMADOR_ALERTA_INVESTIDOR_INVESTIMENTO_ATIVO"
        }
    }
    
    var linkButtonName: String {
        switch self {
        case .accountUpgrade:
            return AnalyticsConstants.faq.description
        case .activeProposalsLimitReached, .currentlyBorrower:
            return AnalyticsConstants.dismissButton.description
        }
    }
}
