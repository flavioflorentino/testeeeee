import UI
import UIKit
import SkeletonView

protocol LendingProposalFriendsDisplay: AnyObject {
    func display(friends: [LendingFriend])
    func displayEmptyState(title: String, message: String)
    func hideEmptyState()
    func displayEmptyState(error: Error)
    func startLoadingList()
    func stopLoadingList()
    func startLoadingFriend(at indexPath: IndexPath)
    func stopLoadingFriend(at indexPath: IndexPath)
    func displayError(error: Error)
}

extension LendingProposalFriendsViewController.Layout {
    enum Length {
        static let continueButtonHeight: CGFloat = 48
        static let searchTextFieldSystemPadding: CGFloat = 6.0
        static let tableViewRowHeight: CGFloat = 64
    }
    
    enum Size {
        static let searchTextFieldLeftView = CGSize(width: 40 - Length.searchTextFieldSystemPadding, height: 24)
    }
    
    enum Point {
        static let searchTextFieldIcon = CGPoint(x: Spacing.base02 - Length.searchTextFieldSystemPadding, y: .zero)
    }
    
    enum Font {
        static let searchTextField: UIFont = .systemFont(ofSize: 14, weight: .regular)
    }
}

final class LendingProposalFriendsViewController: ViewController<LendingProposalFriendsViewModelInputs, UIView> {
    fileprivate enum Layout {}
    
    enum Section {
        case main
    }
    
    enum AccessibilityIdentifier: String {
        case searchBar
        case friendsTableView
        case emptyStateView
        case errorView
        case tryAgainButton
    }
    
    private(set) lazy var searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.placeholder = Strings.Lending.Proposal.Friends.searchBarPlaceholder
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        searchController.searchBar.accessibilityIdentifier = AccessibilityIdentifier.searchBar.rawValue
        if #available(iOS 13.0, *) {
            searchController.searchBar.searchTextField.borderStyle = .none
            searchController.searchBar.searchTextField.layer.cornerRadius = 18
            searchController.searchBar.searchTextField.font = Layout.Font.searchTextField
            searchController.searchBar.searchTextField.textColor = Colors.grayscale300.color
            searchController.searchBar.searchTextField.leftView = searchTextFieldLeftView
            searchController.searchBar.searchTextField.leftViewMode = .always
            searchController.searchBar.searchTextField.backgroundColor = Colors.backgroundPrimary.color
            searchController.searchBar.searchTextField.border = .light(color: .grayscale200())
        }
        return searchController
    }()
    
    private lazy var searchTextFieldLeftView: UIView = {
        let leftView = UIView(frame: CGRect(origin: .zero, size: Layout.Size.searchTextFieldLeftView))
        let iconImageView = UIImageView(image: Assets.lendingGlyphMagnifyingGlass.image)
        
        iconImageView.tintColor = Colors.grayscale200.color
        iconImageView.frame.origin = Layout.Point.searchTextFieldIcon
        
        leftView.addSubview(iconImageView)
        return leftView
    }()
    
    private(set) lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.separatorStyle = .none
        tableView.register(FriendTableViewCell.self, forCellReuseIdentifier: FriendTableViewCell.identifier)
        tableView.rowHeight = Layout.Length.tableViewRowHeight
        tableView.backgroundColor = .clear
        tableView.accessibilityIdentifier = AccessibilityIdentifier.friendsTableView.rawValue
        tableView.delegate = self
        tableView.isSkeletonable = true
        tableView.keyboardDismissMode = .onDrag
        return tableView
    }()
    
    private(set) lazy var tryAgainButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(GlobalLocalizable.tryAgainButtonTitle, for: .normal)
        button.addTarget(self, action: #selector(tryAgainButtonTapped), for: .touchUpInside)
        button.accessibilityIdentifier = AccessibilityIdentifier.tryAgainButton.rawValue
        button.isHidden = true
        return button
    }()
    
    private(set) lazy var helpButton: UIBarButtonItem = {
        let button = UIBarButtonItem(image: Assets.lendingGlyphHelp.image,
                                     style: .plain,
                                     target: self,
                                     action: #selector(helpButtonTapped))
        button.accessibilityLabel = GlobalLocalizable.helpButtonAccessibilityLabel
        return button
    }()
    
    private lazy var dataSource: TableViewDataSource<Section, LendingFriend> = {
        let dataSource = TableViewDataSource<Section, LendingFriend>(view: tableView) { tableView, indexPath, friend -> UITableViewCell? in
            let cell = tableView.dequeueReusableCell(withIdentifier: FriendTableViewCell.identifier, for: indexPath) as? FriendTableViewCell
            cell?.setup(with: friend, isLoading: self.loadingFriendIndexPath == indexPath)
            return cell
        }
        dataSource.skeletonCellIdentifierProvider = { _, _ in
            FriendTableViewCell.identifier
        }
        dataSource.add(section: .main)
        return dataSource
    }()
    
    private var loadingFriendIndexPath: IndexPath?
    private let gradient = SkeletonGradient(baseColor: Colors.grayscale100.color)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.fetchFriends()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.sendAnalyticsEvent(.screenViewed)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if isMovingFromParent {
            viewModel.sendAnalyticsEvent(.backButtonTapped)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.layoutSkeletonIfNeeded()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(tableView)
        view.addSubview(tryAgainButton)
    }
    
    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        tryAgainButton.snp.makeConstraints {
            $0.leading.trailing.bottom.equalTo(view.layoutMarginsGuide)
        }
    }
    
    override func configureViews() {
        definesPresentationContext = true
        view.backgroundColor = Colors.backgroundPrimary.color
        view.layoutMargins.bottom = Spacing.base02
        navigationItem.title = Strings.Lending.Proposal.Friends.title
        navigationItem.rightBarButtonItem = helpButton
        tableView.dataSource = dataSource
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = false
        } else {
            tableView.tableHeaderView = searchController.searchBar
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard #available(iOS 13, *), traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) else {
            return
        }
        searchController.searchBar.searchTextField.border = .light(color: .grayscale200())
    }
}
// MARK: Actions
@objc
private extension LendingProposalFriendsViewController {
    func tryAgainButtonTapped() {
        viewModel.filterFriends(query: searchController.searchBar.text)
    }
    
    func helpButtonTapped() {
        viewModel.showHelp()
    }
}

extension LendingProposalFriendsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        viewModel.selectFriend(at: indexPath)
    }
}

// MARK: Search bar delegate
extension LendingProposalFriendsViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        viewModel.sendAnalyticsEvent(.searchBarTextDidBeginEditing)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel.filterFriends(query: searchText)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        viewModel.fetchFriends()
    }
}

// MARK: View Model Outputs
extension LendingProposalFriendsViewController: LendingProposalFriendsDisplay {
    func display(friends: [LendingFriend]) {
        dataSource.update(items: friends, from: .main)
    }
    
    func displayEmptyState(title: String, message: String) {
        let informativeView = InformativeView(
            image: Assets.lendingFigureUpset.image,
            title: title,
            message: message
        )
        informativeView.accessibilityIdentifier = AccessibilityIdentifier.emptyStateView.rawValue
        tableView.backgroundView = informativeView
        tryAgainButton.isHidden = true
    }
    
    func hideEmptyState() {
        tableView.backgroundView = nil
        tryAgainButton.isHidden = true
    }
    
    func displayEmptyState(error: Error) {
        let informativeView = InformativeView(
            image: Assets.lendingFigureUpset.image,
            title: Strings.Lending.Proposal.Friends.loadingErrorTitle
        )
        informativeView.accessibilityIdentifier = AccessibilityIdentifier.errorView.rawValue
        tryAgainButton.isHidden = false
        tableView.backgroundView = informativeView
    }
    
    func startLoadingList() {
        tableView.allowsSelection = false
        view.showAnimatedGradientSkeleton(usingGradient: gradient)
    }
    
    func stopLoadingList() {
        tableView.allowsSelection = true
        view.hideSkeleton()
    }
    
    func startLoadingFriend(at indexPath: IndexPath) {
        tableView.allowsSelection = false
        loadingFriendIndexPath = indexPath
        tableView.reloadRows(at: [indexPath], with: .fade)
    }
    
    func stopLoadingFriend(at indexPath: IndexPath) {
        tableView.allowsSelection = true
        loadingFriendIndexPath = nil
        tableView.reloadRows(at: [indexPath], with: .fade)
    }
    
    func displayError(error: Error) {
        let alert = PopupViewController(
            title: GlobalLocalizable.errorAlertTitle,
            description: error.localizedDescription,
            preferredType: .text
        )
        let dismissAction = PopupAction(title: GlobalLocalizable.dismissButtonTitle, style: .fill)
        alert.addAction(dismissAction)
        showPopup(alert)
    }
}
