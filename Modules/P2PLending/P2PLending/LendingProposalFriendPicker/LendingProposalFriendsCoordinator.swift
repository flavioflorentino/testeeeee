import UIKit
import UI
import AnalyticsModule

enum LendingProposalFriendsAction: Equatable {
    case proposalDescription(proposal: Proposal)
    case detailedError(error: IdentifiableFriendsError)
    case accountUpgrade(error: IdentifiableFriendsError)
    case showFAQ(url: URL)
}

protocol LendingProposalFriendsCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: LendingProposalFriendsAction)
}

final class LendingProposalFriendsCoordinator: LendingProposalFriendsCoordinating, URLOpenerCoordinating {
    typealias Dependencies = HasURLOpener & HasAnalytics
    var dependencies: Dependencies
    
    weak var viewController: UIViewController?
    
    let data: GreetingsResponse
    
    init(data: GreetingsResponse, dependencies: Dependencies) {
        self.data = data
        self.dependencies = dependencies
    }
    
    func perform(action: LendingProposalFriendsAction) {
        switch action {
        case let .proposalDescription(proposal):
            let controller = LendingProposalDescriptionFactory.make(data: data, proposal: proposal)
            viewController?.show(controller, sender: nil)
        case let .showFAQ(url):
            showFAQ(for: url, using: dependencies)
        case let .detailedError(error):
            showDetailedErrorFeedbackController(error: error)
        case let .accountUpgrade(error):
            showAccountUpgradeErrorFeedbackController(error: error)
        }
    }
}

private extension LendingProposalFriendsCoordinator {
    func makeApolloFeedbackViewController(error: IdentifiableFriendsError) -> ApolloFeedbackViewController {
        ApolloFeedbackViewController(content: error.asFeedbackViewContent)
    }
    
    func showDetailedErrorFeedbackController(error: IdentifiableFriendsError) {
        let feedbackController = makeApolloFeedbackViewController(error: error)
        
        feedbackController.onViewDidAppear = { [weak self] in
            self?.dependencies.analytics.log(error.analyticsEvent(for: .screenViewed))
        }
        
        feedbackController.didTapPrimaryButton = { [weak self] in
            self?.dependencies.analytics.log(error.analyticsEvent(for: .dismissed))
            feedbackController.dismiss(animated: true, completion: nil)
        }
        
        feedbackController.didTapSecondaryButton = { [weak self] in
            self?.dependencies.analytics.log(error.analyticsEvent(for: .deeplink))
            feedbackController.dismiss(animated: true, completion: nil)
        }
        
        viewController?.present(feedbackController, animated: true, completion: nil)
    }
    
    func showAccountUpgradeErrorFeedbackController(error: IdentifiableFriendsError) {
        let feedbackController = makeApolloFeedbackViewController(error: error)
        
        feedbackController.onViewDidAppear = { [weak self] in
            self?.dependencies.analytics.log(error.analyticsEvent(for: .screenViewed))
        }
        
        feedbackController.didTapPrimaryButton = { [weak self] in
            self?.dependencies.analytics.log(error.analyticsEvent(for: .dismissed))
            feedbackController.dismiss(animated: true, completion: nil)
        }
        
        feedbackController.didTapSecondaryButton = { [weak self] in
            guard let url = error.asDetailedError.url, let self = self else { return }
            self.dependencies.analytics.log(error.analyticsEvent(for: .deeplink))
            feedbackController.dismiss(animated: true) {
                self.showFAQ(for: url, using: self.dependencies)
            }
        }
        
        viewController?.present(feedbackController, animated: true, completion: nil)
    }
}
