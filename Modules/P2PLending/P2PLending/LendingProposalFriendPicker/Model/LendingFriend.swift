import Foundation

public struct LendingFriend: Decodable, Equatable {
    public let id: Int
    public let userName: String
    public let name: String?
    public let profileUrl: URL?
    public let contractUrl: URL?
}

extension LendingFriend {
    init?(searchResponseRow: SearchResponseRow) {
        guard let id = Int(searchResponseRow.data.id) else {
            return nil
        }
        self.id = id
        self.userName = searchResponseRow.data.username
        self.name = searchResponseRow.data.name
        self.profileUrl = searchResponseRow.data.imageUrlSmall
        self.contractUrl = nil
    }
}
