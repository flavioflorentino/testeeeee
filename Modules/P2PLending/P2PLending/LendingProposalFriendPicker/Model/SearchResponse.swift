import Foundation

enum SearchScope: String, Equatable {
    case consumers = "CONSUMERS"
    case contacts = "CONTACTS"
}

struct SearchResponse: Decodable {
    let rows: [SearchResponseRow]
}

struct SearchResponseRow: Decodable {
    let data: SearchResponseUser
}

struct SearchResponseUser: Decodable {
    let id: String
    let name: String?
    let username: String
    let imageUrlSmall: URL?
}
