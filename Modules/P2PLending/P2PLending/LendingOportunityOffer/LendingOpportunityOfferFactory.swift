import UIKit
import Foundation

enum LendingOpportunityOfferFactory {
    static func make(offerIdentifier: UUID) -> UIViewController {
        let container = DependencyContainer()
        let service: LendingOpportunityOfferServicing = LendingOpportunityOfferService(dependencies: container)
        let coordinator: LendingOpportunityOfferCoordinating = LendingOpportunityOfferCoordinator(dependencies: container)
        let presenter: LendingOpportunityOfferPresenting = LendingOpportunityOfferPresenter(coordinator: coordinator,
                                                                                            dependencies: container)
        let viewModel = LendingOpportunityOfferViewModel(service: service,
                                                         presenter: presenter,
                                                         offerIdentifier: offerIdentifier,
                                                         dependencies: container)
        let viewController = LendingOpportunityOfferViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
