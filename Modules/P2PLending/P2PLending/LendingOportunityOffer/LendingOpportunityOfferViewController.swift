import UIKit
import UI

protocol LendingOpportunityOfferDisplay: AnyObject {
    func display(offer: OpportunityOfferDisplayModel)
    func display(error: Error)
    func displayDeclineConfirmation(title: String, body: String)
    func startLoading()
    func stopLoading()
}

extension LendingOpportunityOfferViewController.Layout {
    enum Insets {
        static let proposalSummaryMargins = EdgeInsets(
            top: Spacing.base02,
            leading: Spacing.base02,
            bottom: Spacing.base02,
            trailing: Spacing.base02
        )
    }
}

final class LendingOpportunityOfferViewController: ViewController<LendingOpportunityOfferViewModelInputs, UIView> {
    private typealias Localizable = Strings.Lending.Opportunity.Offer
    
    fileprivate enum Layout {}
    
    enum AccessibilityIdentifier: String {
        case continueButton
        case declineButton
    }
    
    private lazy var closeButtonItem = UIBarButtonItem(title: GlobalLocalizable.closeButtonTitle,
                                                       style: .plain,
                                                       target: self,
                                                       action: #selector(closeButtonTapped))
    
    private lazy var helpButtonItem: UIBarButtonItem = {
        let button = UIBarButtonItem(image: Assets.lendingGlyphHelp.image,
                                     style: .plain,
                                     target: self,
                                     action: #selector(helpButtonTapped))
        button.accessibilityLabel = GlobalLocalizable.helpButtonAccessibilityLabel
        return button
    }()
    
    private lazy var statusView = StatusView()
    
    private lazy var friendCalloutLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var friendIncomingMessageView = IncomingMessageView()
    
    private lazy var friendStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [friendCalloutLabel, friendIncomingMessageView])
        stackView.spacing = Spacing.base02
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var requestedAmountPlusIOFItemView: SummaryItemView = {
        let itemView = SummaryItemView()
        itemView.titleLabel.text = Localizable.requestedAmountPlusIOFItemTitle
        return itemView
    }()
    
    private lazy var installmentsItemView: SummaryItemView = {
        let item = SummaryItemView()
        item.titleLabel.text = Localizable.installmentsItemTitle
        return item
    }()
    
    private lazy var totalPaymentReceivedItemView: SummaryItemView = {
        let itemView = SummaryItemView()
        itemView.titleLabel.text = Localizable.totalPaymentReceivedValueItemTitle
        return itemView
    }()
    
    private lazy var paymentDateItemView: SummaryItemView = {
        let itemView = SummaryItemView()
        itemView.titleLabel.text = Localizable.paymentDateItemTitle
        return itemView
    }()
    
    private lazy var appliedInterestRateItemView: SummaryItemView = {
        let itemView = SummaryItemView()
        itemView.titleLabel.text = Localizable.appliedInterestRateItemTitle
        return itemView
    }()
    
    private lazy var numberOfInstallmentsItemView: SummaryItemView = {
        let itemView = SummaryItemView()
        itemView.titleLabel.text = Localizable.numberOfInstallmentsItemTitle
        return itemView
    }()
    
    private lazy var proposalSummaryTitle: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle(type: .highlight))
        return label
    }()
    
    private lazy var proposalSummaryStackView: UIStackView = {
        let stackView = UIStackView(
            arrangedSubviews: [
                proposalSummaryTitle,
                requestedAmountPlusIOFItemView,
                numberOfInstallmentsItemView,
                appliedInterestRateItemView
            ]
        )
        stackView.axis = .vertical
        stackView.spacing = Spacing.base04
        stackView.compatibleLayoutMargins = Layout.Insets.proposalSummaryMargins
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    
    private lazy var proposalSummaryContainer: UIView = {
        let view = UIView()
        view.viewStyle(RoundedViewStyle())
            .with(\.border, .light(color: .grayscale100()))
        return view
    }()
    
    private lazy var cvTextView: UITextView = {
        let textView = TextView()
        textView.isScrollEnabled = false
        textView.isEditable = false
        textView.dataDetectorTypes = [.link]
        textView.delegate = self
        textView.backgroundColor = .clear
        return textView
    }()
    
    private lazy var continueButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.continueButtonTitle, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.accessibilityIdentifier = AccessibilityIdentifier.continueButton.rawValue
        button.addTarget(self, action: #selector(acceptOfferButtonTapped), for: .touchUpInside)
        return button
    }()
    
    private lazy var installmentButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.continueButtonTitle, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.accessibilityIdentifier = AccessibilityIdentifier.continueButton.rawValue
        button.addTarget(self, action: #selector(installmentButtonTapped), for: .touchUpInside)
        return button
    }()
    
    private lazy var declineButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.declineButtonTitle, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.accessibilityIdentifier = AccessibilityIdentifier.declineButton.rawValue
        button.addTarget(self, action: #selector(declineOfferButtonTapped), for: .touchUpInside)
        return button
    }()
    
    private lazy var rootStackView: UIStackView = {
        let arrangedSubviews: [UIView] = [
            statusView,
            friendStackView,
            totalPaymentReceivedItemView,
            installmentsItemView,
            paymentDateItemView,
            proposalSummaryContainer,
            cvTextView,
            continueButton,
            installmentButton,
            declineButton
        ]
        let stackView = UIStackView(arrangedSubviews: arrangedSubviews)
        stackView.spacing = Spacing.base04
        stackView.axis = .vertical
        stackView.compatibleLayoutMargins = .rootView
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    
    private lazy var loadingActivityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(style: .gray)
        activityIndicatorView.color = Colors.grayscale300.color
        return activityIndicatorView
    }()
    
    private lazy var scrollView = UIScrollView()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.fetchOffer()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        view.addSubview(loadingActivityIndicatorView)
        
        scrollView.addSubview(rootStackView)
        
        proposalSummaryContainer.addSubview(proposalSummaryStackView)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        rootStackView.snp.makeConstraints {
            $0.edges.width.equalToSuperview()
        }
        
        proposalSummaryStackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        loadingActivityIndicatorView.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        view.tintColor = Colors.branding400.color
        navigationItem.title = Localizable.title
        navigationItem.leftBarButtonItem = closeButtonItem
        navigationItem.rightBarButtonItem = helpButtonItem
        setupNavigationAppearance()
    }
    
    private func setupNavigationAppearance() {
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .automatic
            navigationController?.navigationBar.prefersLargeTitles = true
            navigationController?.navigationBar.isTranslucent = true
        }
        if #available(iOS 13.0, *) {
            let navigationBarAppearance = UINavigationBarAppearance()
            navigationBarAppearance.configureWithTransparentBackground()
            navigationBarAppearance.backgroundColor = Colors.backgroundPrimary.color
            navigationBarAppearance.shadowColor = .clear
            navigationController?.navigationBar.scrollEdgeAppearance = navigationBarAppearance
            navigationController?.navigationBar.standardAppearance = navigationBarAppearance
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if #available(iOS 13, *), traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) {
            proposalSummaryContainer.layer.borderColor = Colors.grayscale100.color.cgColor
        }
    }
}

extension LendingOpportunityOfferViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if textView == cvTextView {
            viewModel.showContract()
        }
        return false
    }
}

@objc
private extension LendingOpportunityOfferViewController {
    func acceptOfferButtonTapped() {
        viewModel.acceptOffer()
    }
    
    func installmentButtonTapped() {
        viewModel.installmentsTapped()
    }
    
    func declineOfferButtonTapped() {
        viewModel.requestDeclineConfirmation()
    }
    
    func closeButtonTapped() {
        viewModel.close(shouldSendEventTracking: true)
    }
    
    func helpButtonTapped() {
        interactor.showFaq()
    }
}

// MARK: LendingOpportunityOfferDisplay
extension LendingOpportunityOfferViewController: LendingOpportunityOfferDisplay {
    func display(offer: OpportunityOfferDisplayModel) {
        scrollView.isHidden = false
        friendCalloutLabel.attributedText = offer.friendCallout
        friendIncomingMessageView.setMessage(offer.message, for: offer.friend)
        friendIncomingMessageView.isHidden = !offer.shouldShowMessageBubble
        requestedAmountPlusIOFItemView.valueLabel.text = offer.requestedAmountPlusIOF
        installmentsItemView.valueLabel.text = offer.installments
        numberOfInstallmentsItemView.valueLabel.text = offer.numberOfInstallments
        totalPaymentReceivedItemView.valueLabel.text = offer.totalPaymentReceivedValue
        paymentDateItemView.valueLabel.text = offer.paymentDateDescription
        proposalSummaryTitle.text = Localizable.proposalSummaryTitle(offer.friendUserName)
        appliedInterestRateItemView.valueLabel.text = offer.appliedInterestRate
        cvTextView.attributedText = offer.cvNotice
        continueButton.setTitle(offer.primaryButtonTitle, for: .normal)
        statusView.statusDisplayModel = offer.status
        continueButton.isHidden = !offer.shouldDisplayPrimaryButton
        declineButton.isHidden = !offer.shouldDisplaySecondaryButton
        friendStackView.isHidden = !offer.shouldDisplayFriendCallout
        statusView.isHidden = !offer.shouldDisplayStatus
        installmentButton.isHidden = !offer.shouldDisplayInstallmentButton
        installmentButton.setTitle(Localizable.installmentsButtonTitle, for: .normal)
    }
    
    func display(error: Error) {
        let popUp = PopupViewController(
            title: GlobalLocalizable.errorAlertTitle,
            description: error.localizedDescription,
            preferredType: .image(Assets.lendingFigureFailure.image)
        )
        let dismissAction = PopupAction(title: GlobalLocalizable.dismissButtonTitle, style: .default) {
            self.viewModel.close(shouldSendEventTracking: false)
        }
        popUp.addAction(dismissAction)
        showPopup(popUp)
    }
    
    func displayDeclineConfirmation(title: String, body: String) {
        let contentView = DeclineOfferDialogueContentView(title: title, body: body)
        let alertPopup = PopupViewController(title: nil, preferredType: .customView(contentView, shouldRemoveCloseButton: true))
        alertPopup.addAction(.init(title: Localizable.declineOfferButtonTitle, style: .destructive) {
            self.interactor.declineOffer()
        })
        alertPopup.addAction(.init(title: Localizable.dismissButtonTitle, style: .link) {
            self.interactor.sendAnalyticsEvent(type: .dismissed)
        })
        showPopup(alertPopup)
    }
    
    func startLoading() {
        loadingActivityIndicatorView.startAnimating()
        scrollView.isHidden = true
    }
    
    func stopLoading() {
        loadingActivityIndicatorView.stopAnimating()
    }
}
