import Foundation
import UI
import FeatureFlag

protocol LendingOpportunityOfferPresenting: AnyObject {
    var viewController: LendingOpportunityOfferDisplay? { get set }
    func present(offer: OpportunityOffer)
    func present(error: Error)
    func startLoading()
    func stopLoading()
    func showDeclineConfirmationDialogue()
    func presentContract(for url: URL)
    func didNextStep(action: LendingOpportunityOfferAction)
}

final class LendingOpportunityOfferPresenter {
    private typealias Localizable = Strings.Lending.Opportunity.Offer
    typealias Dependencies = HasFeatureManager
    
    private let dependencies: Dependencies
    
    private let coordinator: LendingOpportunityOfferCoordinating
    weak var viewController: LendingOpportunityOfferDisplay?

    init(coordinator: LendingOpportunityOfferCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
    
    lazy var percentNumberFormatter: NumberFormatter = {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .percent
        numberFormatter.minimumFractionDigits = 1
        numberFormatter.maximumFractionDigits = 1
        numberFormatter.locale = Locale(identifier: "pt_BR")
        return numberFormatter
    }()
}

// MARK: - LendingOpportunityOfferPresenting
extension LendingOpportunityOfferPresenter: LendingOpportunityOfferPresenting {
    func present(offer: OpportunityOffer) {
        let installments = Localizable.installmentsItemValue(
            offer.operationDetails.installments,
            offer.operationDetails.investorInstallmentsAmount.toCurrencyString() ?? ""
        )
        let interestRate = percentFormatted(value: offer.operationDetails.monthRate)
        let requestedAmountPlusIOF = offer.operationDetails.amount + offer.operationDetails.iof
        let offerDisplayModel = OpportunityOfferDisplayModel(
            friendCallout: friendCalloutText(for: offer.borrower),
            friend: offer.borrower,
            friendUserName: offer.borrower?.userName ?? "",
            status: OpportunityOfferStatusViewModel(status: offer.status, agent: .investor),
            message: offer.message,
            shouldShowMessageBubble: offer.message?.isNotEmpty ?? false,
            requestedAmountPlusIOF: requestedAmountPlusIOF.toCurrencyString(),
            installments: installments,
            numberOfInstallments: Localizable.numberOfInstallmentsItemValue(offer.operationDetails.installments),
            totalPaymentValue: offer.operationDetails.subtotal.toCurrencyString(),
            totalPaymentReceivedValue: offer.operationDetails.investorTotalInterest.toCurrencyString(),
            paymentDateDescription: Date.forthFormatter.string(from: offer.operationDetails.firstPaymentDate),
            appliedInterestRate: Localizable.appliedInterestRateItemValue(interestRate),
            ir: (-offer.operationDetails.investorIr).toCurrencyString(),
            primaryButtonTitle: primaryButtonTitle(for: offer.status),
            shouldDisplayPrimaryButton: shouldDisplayPrimaryButton(for: offer.status),
            shouldDisplaySecondaryButton: shouldDisplaySecondaryButton(for: offer.status),
            shouldDisplayFriendCallout: shouldDisplayFriendCallout(for: offer.status),
            shouldDisplayStatus: shouldDisplayStatus(for: offer.status),
            shouldDisplayInstallmentButton: shouldDisplayInstallmentsButton(for: offer.status)
        )
        viewController?.display(offer: offerDisplayModel)
    }
    
    func present(error: Error) {
        viewController?.display(error: error)
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
    
    func showDeclineConfirmationDialogue() {
        viewController?.displayDeclineConfirmation(title: Localizable.declineOfferTitle, body: Localizable.declineOfferBody)
    }
    
    func presentContract(for url: URL) {
        let webPage = LendingSessionWebPage(title: Localizable.cv, url: url)
        coordinator.perform(action: .contract(webPage: webPage))
    }
    
    func didNextStep(action: LendingOpportunityOfferAction) {
        coordinator.perform(action: action)
    }
}

private extension LendingOpportunityOfferPresenter {
    func friendCalloutText(for friend: LendingFriend?) -> NSAttributedString? {
        guard let username = friend?.userName else {
            return nil
        }
        let friendCallout = NSMutableAttributedString(
            string: "@\(username)",
            attributes: [
                .font: Typography.bodyPrimary(.highlight).font(),
                .foregroundColor: Colors.grayscale600.color
            ]
        )
        let friendCalloutSufix = NSAttributedString(
            string: Localizable.friendCalloutSufix,
            attributes: [
                .font: Typography.bodyPrimary().font(),
                .foregroundColor: Colors.grayscale600.color
            ]
        )
        friendCallout.append(friendCalloutSufix)
        return friendCallout
    }
    
    func percentFormatted(value: Float?) -> String {
        guard
            let value = value,
            let percentFormattedValue = percentNumberFormatter.string(from: value / 100 as NSNumber)
        else {
            return ""
        }
        return percentFormattedValue
    }
    
    func primaryButtonTitle(for status: OpportunityOfferStatus) -> String? {
        switch status {
        case .waitingReply:
            return Localizable.continueButtonTitle
        case .accepted:
            return Localizable.lendNowButtonTitle
        default:
            return nil
        }
    }
    
    func shouldDisplayPrimaryButton(for status: OpportunityOfferStatus) -> Bool {
        status == .waitingReply || status == .accepted
    }
    
    func shouldDisplaySecondaryButton(for status: OpportunityOfferStatus) -> Bool {
        status == .waitingReply
    }
    
    func shouldDisplayFriendCallout(for status: OpportunityOfferStatus) -> Bool {
        !(status == .liquidated || status == .declined || status == .expired || status == .canceled)
    }
    
    func shouldDisplayStatus(for status: OpportunityOfferStatus) -> Bool {
        true
    }
    
    func shouldDisplayInstallmentsButton(for status: OpportunityOfferStatus) -> Bool {
        status == .active && dependencies.featureManager.isActive(.isP2PLendingInvestorInstallmentsAvailable)
    }
}
