struct OperationDetail: Decodable, Equatable {
    let amount: Double
    let commissionAmount: Double
    let installments: Int
    let monthRate: Float
    let subtotal: Double
    let borrowerInstallmentsAmount: Double
    let iof: Double
    let investorInstallmentsAmount: Double
    let totalInterest: Double
    let investorTotalIncome: Double
    let investorTotalInterest: Double
    let investorIr: Double
    let firstPaymentDate: Date
    let cet: Float
}
