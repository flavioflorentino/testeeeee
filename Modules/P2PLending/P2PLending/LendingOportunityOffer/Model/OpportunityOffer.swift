import Foundation
struct OpportunityOffer: Decodable, Equatable {
    let id: UUID
    let contractOriginal: String
    let status: OpportunityOfferStatus
    let borrower: LendingFriend?
    let investor: LendingFriend?
    let message: String?
    let payDay: Int
    let operationDetails: OperationDetail
    let cv: URL?
    let privacyPolicy: URL
    let termsConditions: URL
}
