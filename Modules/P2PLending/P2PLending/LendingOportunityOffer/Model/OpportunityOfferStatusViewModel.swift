import UI
import UIKit

struct OpportunityOfferStatusViewModel: StatusDisplayable, Equatable {
    var status: OpportunityOfferStatus
    var agent: ConsultingAgent
    private typealias Localizable = Strings.Lending.Status
    
    var title: String {
        let provider = descriptionProvider(for: agent)
        return provider.opportunityOfferStatusTitle(for: status)
    }
    
    private func descriptionProvider(for agent: ConsultingAgent) -> OpportunityOfferStatusTitleProvider {
        switch agent {
        case .borrower:
            return BorrowerOpportunityOfferStatusTitleProvider()
        case .investor:
            return InvestorOpportunityOfferStatusTitleProvider()
        }
    }
    
    var color: UIColor {
        switch status {
        case .waitingReply, .processingPayment, .delayedPayment, .counterOffer:
            return Colors.warning600.color
        case .accepted, .active, .liquidated:
            return Colors.success600.color
        case .declined, .expired, .canceled:
            return Colors.critical600.color
        }
    }
    
    var icon: Iconography {
        switch status {
        case .waitingReply, .processingPayment, .delayedPayment, .counterOffer:
            return .exclamationTriangle
        case .accepted, .active, .liquidated:
            return .checkCircle
        case .declined, .expired, .canceled:
            return .exclamationCircle
        }
    }
}
