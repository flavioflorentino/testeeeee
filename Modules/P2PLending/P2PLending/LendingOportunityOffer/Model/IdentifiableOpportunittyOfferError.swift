import Foundation
import AnalyticsModule
import AssetsKit

enum IdentifiableOpportunittyOfferError: String {
    case unsuitableBorrower = "5301"
    case insufficientFunds = "5302"
    case activeInvestorLimitReached = "5105"
    case currentlyProposal = "5106"
    case activeProposalsLimitReached = "5107"
    case currentlyInvestor = "5108"
    case valueLimitExceeded = "5109"
    
    init?(errorCode: String) {
        self.init(rawValue: errorCode)
    }
}

extension IdentifiableOpportunittyOfferError: DetailedErrorConvertible {
    private typealias Localizable = Strings.Lending.DetailedError
    var asDetailedError: DetailedError {
        let title: String
        let message: String
        var image = Resources.Illustrations.iluWarning.image
        var primaryButtonTitle = Localizable.dismissButtonTitle
        let linkButtonTitle: String?
        var url = URL(string: "picpay://picpay/helpcenter/article/360049860711")
        switch self {
        case .unsuitableBorrower:
            title = Localizable.unsuitableBorrowerTitle
            message = Localizable.unsuitableBorrowerBody
            image = Assets.lendingFigureWarning.image
            primaryButtonTitle = Localizable.dismissButtonTitle
            linkButtonTitle = Localizable.unsuitableBorrowerLimitReachedFAQButtonTitle
            url = URL(string: "https://www.picpay.com") // todo: aguardando o link
        case.insufficientFunds:
            title = Localizable.insufficientFundsTitle
            message = Localizable.insufficientFundsBody
            image = Assets.lendingFigureCashIn.image
            primaryButtonTitle = Localizable.insufficientFundsButtonTitle
            linkButtonTitle = nil
            url = nil
        case .activeInvestorLimitReached:
            title = Localizable.activeInvestorLimitReachedTitle
            message = Localizable.activeInvestorLimitReachedBody
            linkButtonTitle = Localizable.activeInvestorLimitReachedLinkButtonTitle
        case .currentlyProposal:
            title = Localizable.currentProposalTitle
            message = Localizable.currentProposalBody
            linkButtonTitle = Localizable.currentProposalLinkButtonTitle
        case .activeProposalsLimitReached:
            title = Localizable.borrowerActiveProposalsLimitReachedTitle
            message = Localizable.borrowerActiveProposalsLimitReachedBody
            linkButtonTitle = Localizable.borrowerActiveProposalsLimitReachedLinkButtonTitle
        case .currentlyInvestor:
            title = Localizable.opportunityOfferCurrentlyInvestorTitle
            message = Localizable.opportunityOfferCurrentlyInvestorBody
            linkButtonTitle = Localizable.opportunityOfferCurrentlyInvestorLinkButtonTitle
        case .valueLimitExceeded:
            title = Localizable.valueLimitExceededTitle
            message = Localizable.valueLimitExceededBody
            linkButtonTitle = Localizable.valueLimitExceededLinkButtonTitle
        }
        return DetailedError(
            image: image,
            title: title,
            message: message,
            primaryButtonTitle: primaryButtonTitle,
            linkButtonTitle: linkButtonTitle,
            url: url
        )
    }
}

extension IdentifiableOpportunittyOfferError: AnalyticsEventInteractionProvider {
    var properties: [String: Any] {
        switch self {
        case .unsuitableBorrower,
             .insufficientFunds,
             .activeInvestorLimitReached,
             .currentlyProposal,
             .activeProposalsLimitReached,
             .currentlyInvestor,
             .valueLimitExceeded:
            return [
                AnalyticsKeys.screenName.name: screenName,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        }
    }
    
    func analyticsEvent(for interaction: AnalyticInteraction) -> AnalyticsEvent {
        switch interaction {
        case .backButtonTapped:
            var buttonProperties = properties
            buttonProperties[AnalyticsKeys.buttonName.name] = AnalyticsConstants.backButton.description
            return AnalyticsEvent(AnalyticInteraction.buttonClicked.name, properties: buttonProperties, providers: [.eventTracker])
        case .dismissed:
            var buttonProperties = properties
            buttonProperties[AnalyticsKeys.buttonName.name] = primaryButtonName
            return AnalyticsEvent(AnalyticInteraction.buttonClicked.name, properties: buttonProperties, providers: [.eventTracker])
        case .deeplink:
            var buttonProperties = properties
            buttonProperties[AnalyticsKeys.buttonName.name] = linkButtonName
            return AnalyticsEvent(AnalyticInteraction.buttonClicked.name, properties: buttonProperties, providers: [.eventTracker])
        default:
            return AnalyticsEvent(AnalyticInteraction.screenViewed.name, properties: properties, providers: [.eventTracker])
        }
    }
}

extension IdentifiableOpportunittyOfferError {
    var screenName: String {
        switch self {
        case .unsuitableBorrower:
            return "P2P_LENDING_INVESTIDOR_EMPRESTIMO_NAO_FEITO"
        case .insufficientFunds:
            return "P2P_LENDING_INVESTIDOR_ADICIONAR_DINHEIRO"
        case .valueLimitExceeded:
            return "P2P_LENDING_INVESTIDOR_PROPOSTA_LIMITE_VALOR"
        case .activeInvestorLimitReached:
            return "P2P_LENDING_INVESTIDOR_LIMITE_EMPRESTIMOS_ATIVOS"
        case .currentlyProposal:
            return "P2P_LENDING_INVESTIDOR_FINALIZAR_EMPRESTIMO"
        case .activeProposalsLimitReached:
            return "P2P_LENDING_INVESTIDOR_TOMADOR_COM_LIMITE_CONTRATOS_ATIVOS"
        case .currentlyInvestor:
            return "P2P_LENDING_INVESTIDOR_TOMADOR_COM_EMPRESTIMO_ATIVO"
        }
    }
    
    var primaryButtonName: String {
        switch self {
        case .unsuitableBorrower,
             .activeInvestorLimitReached,
             .currentlyProposal,
             .activeProposalsLimitReached,
             .currentlyInvestor,
             .valueLimitExceeded:
            return AnalyticsConstants.okButton.description
        case .insufficientFunds:
            return AnalyticsConstants.addToWallet.description
        }
    }
    
    var linkButtonName: String {
        switch self {
        case .unsuitableBorrower:
            return AnalyticsConstants.faqAccountLimits.description
        case .activeInvestorLimitReached,
             .insufficientFunds,
             .currentlyProposal,
             .activeProposalsLimitReached,
             .currentlyInvestor,
             .valueLimitExceeded:
            return AnalyticsConstants.faq.description
        }
    }
}
