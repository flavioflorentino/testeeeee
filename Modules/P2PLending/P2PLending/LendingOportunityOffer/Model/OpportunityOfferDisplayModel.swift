import UIKit
import Foundation
import UI

struct OpportunityOfferDisplayModel {
    private typealias Localizable = Strings.Lending.Opportunity.Offer
    
    let friendCallout: NSAttributedString?
    let friend: PersonConvertible?
    let friendUserName: String
    let status: OpportunityOfferStatusViewModel
    let message: String?
    let shouldShowMessageBubble: Bool
    let requestedAmountPlusIOF: String?
    let installments: String?
    let numberOfInstallments: String?
    let totalPaymentValue: String?
    let totalPaymentReceivedValue: String?
    let paymentDateDescription: String?
    let appliedInterestRate: String?
    let ir: String?
    let primaryButtonTitle: String?
    let shouldDisplayPrimaryButton: Bool
    let shouldDisplaySecondaryButton: Bool
    let shouldDisplayFriendCallout: Bool
    let shouldDisplayStatus: Bool
    let shouldDisplayInstallmentButton: Bool
    
    private var secondaryBodyAttributes: [NSAttributedString.Key: Any] {
        [
            .font: Typography.bodySecondary().font(),
            .foregroundColor: Colors.grayscale600.color
        ]
    }
    
    private var linkAttributes: [NSAttributedString.Key: Any] {
        [
            .link: "",
            .font: Typography.bodySecondary().font(),
            .foregroundColor: Colors.branding400.color,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
    }
    
    var cvNotice: NSAttributedString? {
        switch status.status {
        case .waitingReply:
            let cvNotice = NSMutableAttributedString(
                string: Localizable.cvNotAcceptedNotice,
                attributes: secondaryBodyAttributes
            )
            let cvNoticeHighlight = NSAttributedString(
                string: "\(Localizable.cv).",
                attributes: linkAttributes
            )
            cvNotice.append(cvNoticeHighlight)
            return cvNotice
        case .accepted:
            let cvNotice = NSMutableAttributedString(
                string: Localizable.cvAcceptedNotice,
                attributes: secondaryBodyAttributes
            )
            let cvNoticeHighlight = NSAttributedString(
                string: "\(Localizable.cv).",
                attributes: linkAttributes
            )
            cvNotice.append(cvNoticeHighlight)
            return cvNotice
        case .processingPayment, .active, .delayedPayment, .counterOffer, .liquidated:
            let cvNotice = NSMutableAttributedString(
                string: Localizable.cvAcceptedNotice,
                attributes: secondaryBodyAttributes
            )
            let cvNoticeHighlight = NSAttributedString(
                string: Localizable.cvNoticeHighlight,
                attributes: linkAttributes
            )
            cvNotice.append(cvNoticeHighlight)
            return cvNotice
        case .canceled, .expired, .declined:
            return nil
        }
    }
}
