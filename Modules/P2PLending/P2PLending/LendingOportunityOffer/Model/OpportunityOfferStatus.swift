enum OpportunityOfferStatus: String, Decodable, CaseIterable {
    case waitingReply = "WAITING_REPLY"
    case accepted = "ACCEPTED"
    case processingPayment = "PROCESSING_PAYMENT"
    case active = "ACTIVE"
    case delayedPayment = "DELAYED_PAYMENT"
    case counterOffer = "COUNTER_OFFER"
    
    case liquidated = "COMPLETED"
    case declined = "REFUSED"
    case expired = "EXPIRED"
    case canceled = "CANCELED"
    
    static var inProgress: [OpportunityOfferStatus] = [.waitingReply, .accepted, .processingPayment, .active, .delayedPayment, .counterOffer]
    static var terminated: [OpportunityOfferStatus] = [.liquidated, .declined, .expired, .canceled]
}
