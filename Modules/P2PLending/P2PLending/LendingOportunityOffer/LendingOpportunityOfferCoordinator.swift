import UIKit
import UI
import AnalyticsModule

enum LendingOpportunityOfferAction {
    case contract(webPage: LendingSessionWebPage)
    case makeTransaction(offer: OpportunityOffer)
    case installmentList(proposalIdentifier: UUID)
    case declineOffer(delegate: LendingSuccessDelegate)
    case detailedError(_ identifiableError: IdentifiableOpportunittyOfferError)
    case insufficientFunds(_ identifiableError: IdentifiableOpportunittyOfferError)
    case close
    case showFAQ(url: URL)
}

protocol LendingOpportunityOfferCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: LendingOpportunityOfferAction)
}

final class LendingOpportunityOfferCoordinator {
    private typealias Localizable = Strings.Lending.Opportunity.Offer
    
    typealias Dependencies = HasTransactionSceneProvider &
                             HasURLOpener &
                             HasAnalytics
    private let dependencies: Dependencies
    
    weak var viewController: UIViewController?
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - LendingOpportunityOfferCoordinating
extension LendingOpportunityOfferCoordinator: LendingOpportunityOfferCoordinating, URLOpenerCoordinating {
    func perform(action: LendingOpportunityOfferAction) {
        switch action {
        case .contract(let webpage):
            let contractViewController = UINavigationController(rootViewController: LendingSessionWebViewFactory.make(for: webpage))
            viewController?.present(contractViewController, animated: true, completion: nil)
        case .makeTransaction(let offer):
            makeTransaction(for: offer)
        case let .installmentList(proposalIdentifier):
            let controller = LendingInstallmentListFactory.make(proposalIdentifier: proposalIdentifier, agent: .investor)
            viewController?.show(controller, sender: nil)
        case .declineOffer(let delegate):
            let successViewController = LendingSuccessFactory.make(content: DeclinedOfferSuccessContent(), delegate: delegate)
            viewController?.show(successViewController, sender: nil)
        case let .detailedError(detailedError):
            let controller = makeDetailedErrorFeedbackController(error: detailedError)
            viewController?.navigationController?.setNavigationBarHidden(true, animated: false)
            viewController?.show(controller, sender: nil)
        case let .insufficientFunds(insufficientFundsError):
            let controller = makeInsufficientFundsFeedbackController(error: insufficientFundsError)
            viewController?.show(controller, sender: nil)
        case .close:
            viewController?.dismiss(animated: true, completion: nil)
        case .showFAQ(let url):
            showFAQ(for: url, using: dependencies)
        }
    }
}

private extension LendingOpportunityOfferCoordinator {
    func makeTransaction(for offer: OpportunityOffer) {
        guard let borrower = offer.borrower, let sceneProvider = dependencies.transactionSceneProvider else {
            return
        }
        let transactionAmount = offer.operationDetails.subtotal
        let transactionController = sceneProvider.transactionScene(for: borrower,
                                                                   value: transactionAmount,
                                                                   offerIdentifier: offer.id)
        viewController?.show(transactionController, sender: nil)
    }
    
    func makeDetailedErrorFeedbackController(error: IdentifiableOpportunittyOfferError) -> ApolloFeedbackViewController {
        let controller = ApolloFeedbackViewController(content: error.asFeedbackViewContent)
        
        controller.onViewDidAppear = { [weak self] in
            self?.dependencies.analytics.log(error.analyticsEvent(for: .screenViewed))
        }
        
        controller.didTapPrimaryButton = { [weak self] in
            self?.dependencies.analytics.log(error.analyticsEvent(for: .dismissed))
            controller.dismiss(animated: true, completion: nil)
        }
        
        controller.didTapSecondaryButton = { [weak self] in
            guard let self = self,
                  let url = error.asDetailedError.url
            else { return }
            self.dependencies.analytics.log(error.analyticsEvent(for: .deeplink))
            self.showFAQ(for: url, using: self.dependencies)
        }
        
        return controller
    }
    
    func makeInsufficientFundsFeedbackController(error: IdentifiableOpportunittyOfferError) -> ApolloFeedbackViewController {
        let controller = ApolloFeedbackViewController(content: error.asFeedbackViewContent)
        
        controller.onViewDidAppear = { [weak self] in
            self?.dependencies.analytics.log(error.analyticsEvent(for: .screenViewed))
        }
        
        controller.didTapPrimaryButton = { [weak self] in
            controller.dismiss(animated: true, completion: nil)
            self?.dependencies.analytics.log(error.analyticsEvent(for: .dismissed))
        }
        
        controller.onViewWillDisapear = { [weak self] isMovingFromParent in
            if isMovingFromParent {
                self?.dependencies.analytics.log(error.analyticsEvent(for: .backButtonTapped))
            }
        }
        
        return controller
    }
}
