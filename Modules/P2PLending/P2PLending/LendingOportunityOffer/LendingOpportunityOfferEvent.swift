import Foundation
import AnalyticsModule

enum LendingOpportunityOfferEvent: AnalyticsKeyProtocol {
    case CVTapped
    case continueTapped
    case declineTapped
    case investorDidDecline
    case laterButtonTapped
    case dismissTapped
    case closeTapped
    case helpTapped
    case installmentTapped
    
    private var title: String {
        "P2P_LENDING_INVESTIDOR_PROPOSTA"
    }
    
    private var declinedProposalTitle: String {
        "P2P_LENDING_INVESTIDOR_RECUSOU_PROPOSTA"
    }
    
    private var name: String {
        switch self {
        case .closeTapped, .helpTapped, .CVTapped, .continueTapped, .declineTapped, .dismissTapped, .installmentTapped:
            return AnalyticInteraction.buttonClicked.name
        case .investorDidDecline, .laterButtonTapped:
            return AnalyticInteraction.dialogOptionSelected.name
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .closeTapped:
            return [
                AnalyticsKeys.screenName.name: title,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.backButton.description,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        case .helpTapped:
            return [
                AnalyticsKeys.screenName.name: title,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.help.description,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        case .CVTapped:
            return [
                AnalyticsKeys.screenName.name: title,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.cv.description,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        case .continueTapped:
            return [
                AnalyticsKeys.screenName.name: title,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.acceptProposal.description,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        case .declineTapped:
            return [
                AnalyticsKeys.screenName.name: title,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.declineProposal.description,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        case .investorDidDecline:
            return [
                AnalyticsKeys.screenName.name: title,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.dialogName.name: AnalyticsConstants.proposalDeclined.description,
                AnalyticsKeys.optionSelected.name: AnalyticsConstants.declineSelected.description
            ]
        case .laterButtonTapped:
            return [
                AnalyticsKeys.screenName.name: title,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.laterButton.description
            ]
        case .dismissTapped:
            return [
                AnalyticsKeys.screenName.name: declinedProposalTitle,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.okButton.description
            ]
        case .installmentTapped:
            return [
                AnalyticsKeys.screenName.name: title,
                AnalyticsKeys.buttonName.name: AnalyticsConstants.openInstallments.description,
                AnalyticsKeys.context.name: AnalyticsConstants.p2pLending.description
            ]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.eventTracker])
    }
}
