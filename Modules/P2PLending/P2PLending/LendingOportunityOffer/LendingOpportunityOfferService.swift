import Foundation
import Core

protocol LendingOpportunityOfferServicing: Servicing {
    func fetchOffer(identifier: UUID, completion: @escaping ModelCompletionBlock<OpportunityOffer>)
    func declineOffer(identifier: UUID, completion: @escaping ModelCompletionBlock<NoContent>)
    func acceptOffer(identifier: UUID, completion: @escaping ModelCompletionBlock<NoContent>)
}

final class LendingOpportunityOfferService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    private lazy var decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        return decoder
    }()

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - LendingOpportunityOfferServicing
extension LendingOpportunityOfferService: LendingOpportunityOfferServicing {
    func fetchOffer(identifier: UUID, completion: @escaping ModelCompletionBlock<OpportunityOffer>) {
        let api = Api<OpportunityOffer>(endpoint: P2PLendingEndpoint.opportunityOffer(identifier: identifier))
        api.shouldUseDefaultDateFormatter = false
        api.execute(jsonDecoder: decoder) { [weak self] result in
            let mappedResult = result.map(\.model)
            self?.dependencies.mainQueue.async {
                completion(mappedResult)
            }
        }
    }
    
    func declineOffer(identifier: UUID, completion: @escaping ModelCompletionBlock<NoContent>) {
        let api = Api<NoContent>(endpoint: P2PLendingEndpoint.declineOffer(identifier: identifier))
        api.execute { [weak self] result in
            let mappedResult = result.map(\.model)
            self?.dependencies.mainQueue.async {
                completion(mappedResult)
            }
        }
    }
    
    func acceptOffer(identifier: UUID, completion: @escaping ModelCompletionBlock<NoContent>) {
        let api = Api<NoContent>(endpoint: P2PLendingEndpoint.acceptOffer(identifier: identifier))
        api.execute { [weak self] result in
            let mappedResult = result.map(\.model)
            self?.dependencies.mainQueue.async {
                completion(mappedResult)
            }
        }
    }
}
