import Foundation
import Core
import AnalyticsModule

protocol LendingOpportunityOfferViewModelInputs: AnyObject {
    func fetchOffer()
    func showContract()
    func acceptOffer()
    func installmentsTapped()
    func declineOffer()
    func requestDeclineConfirmation()
    func close(shouldSendEventTracking: Bool)
    func showFaq()
    func sendAnalyticsEvent(type: AnalyticInteraction)
}

final class LendingOpportunityOfferViewModel {
    typealias Dependencies = HasAnalytics
    private let service: LendingOpportunityOfferServicing
    private let presenter: LendingOpportunityOfferPresenting
    private let offerIdentifier: UUID
    private let dependencies: Dependencies
    
    private var opportunityOffer: OpportunityOffer?

    init(service: LendingOpportunityOfferServicing,
         presenter: LendingOpportunityOfferPresenting,
         offerIdentifier: UUID,
         dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.offerIdentifier = offerIdentifier
        self.dependencies = dependencies
    }
    
    private func sendAnalyticsEvent(_ event: LendingOpportunityOfferEvent) {
        dependencies.analytics.log(event)
    }
}

// MARK: - LendingOpportunityOfferViewModelInputs
extension LendingOpportunityOfferViewModel: LendingOpportunityOfferViewModelInputs {
    func fetchOffer() {
        presenter.startLoading()
        service.fetchOffer(identifier: offerIdentifier) { [weak self] result in
            self?.presenter.stopLoading()
            switch result {
            case .success(let offer):
                self?.opportunityOffer = offer
                self?.presenter.present(offer: offer)
            case .failure(let error):
                self?.presenter.present(error: error)
            }
        }
    }
    
    func showContract() {
        guard let contractLink = opportunityOffer?.investor?.contractUrl else { return }
        sendAnalyticsEvent(.CVTapped)
        presenter.presentContract(for: contractLink)
    }
    
    private func handle(error: ApiError) {
        guard
            let requestError = error.requestError,
            let identifiableError = IdentifiableOpportunittyOfferError(errorCode: requestError.code)
        else {
            presenter.present(error: error)
            return
        }
        switch identifiableError {
        case .insufficientFunds:
            presenter.didNextStep(action: .insufficientFunds(identifiableError))
        case .activeInvestorLimitReached,
             .activeProposalsLimitReached,
             .currentlyInvestor,
             .currentlyProposal,
             .unsuitableBorrower,
             .valueLimitExceeded:
            presenter.didNextStep(action: .detailedError(identifiableError))
        }
    }
    
    func acceptOffer() {
        guard let offer = opportunityOffer, offer.status == .waitingReply || offer.status == .accepted else { return }
        presenter.startLoading()
        service.acceptOffer(identifier: offerIdentifier) { [weak self] result in
            self?.presenter.stopLoading()
            switch result {
            case .success:
                self?.sendAnalyticsEvent(.continueTapped)
                self?.presenter.didNextStep(action: .makeTransaction(offer: offer))
                NotificationCenter.default.post(name: .proposalStatusDidChangeNotification, object: self?.offerIdentifier)
            case .failure(let error):
                self?.handle(error: error)
            }
        }
    }
    
    func installmentsTapped() {
        guard let id = opportunityOffer?.id else { return }
        sendAnalyticsEvent(.installmentTapped)
        presenter.didNextStep(action: .installmentList(proposalIdentifier: id))
    }
    
    func declineOffer() {
        presenter.startLoading()
        service.declineOffer(identifier: offerIdentifier) { [weak self] result in
            self?.presenter.stopLoading()
            switch result {
            case .success:
                guard let self = self else { return }
                self.sendAnalyticsEvent(.investorDidDecline)
                self.presenter.didNextStep(action: .declineOffer(delegate: self))
                NotificationCenter.default.post(name: .proposalStatusDidChangeNotification, object: self.offerIdentifier)
            case .failure(let error):
                self?.presenter.present(error: error)
            }
        }
    }
    
    func requestDeclineConfirmation() {
        sendAnalyticsEvent(.declineTapped)
        presenter.showDeclineConfirmationDialogue()
    }
    
    func close(shouldSendEventTracking: Bool) {
        if shouldSendEventTracking {
            sendAnalyticsEvent(.closeTapped)
        }
        presenter.didNextStep(action: .close)
    }
    
    func showFaq() {
        guard let url = URL(string: "picpay://picpay/helpcenter/article/360050778351") else {
            return
        }
        sendAnalyticsEvent(.helpTapped)
        presenter.didNextStep(action: .showFAQ(url: url))
    }
    
    func sendAnalyticsEvent(type: AnalyticInteraction) {
        switch type {
        case .dismissed:
            sendAnalyticsEvent(.laterButtonTapped)
        default:
            break
        }
    }
}

// MARK: - LendingSuccessDelegate
extension LendingOpportunityOfferViewModel: LendingSuccessDelegate {
    func lendingSuccessControllerDidDismiss(_ lendingSuccessViewController: UIViewController) {
        dependencies.analytics.log(LendingSuccessEvent.didDeclineOffer)
    }
}
