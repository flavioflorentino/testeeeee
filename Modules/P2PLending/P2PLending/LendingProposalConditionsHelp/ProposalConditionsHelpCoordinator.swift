import UIKit

enum ProposalConditionsHelpAction {
    case dismiss
}

protocol ProposalConditionsHelpCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ProposalConditionsHelpAction)
}

final class ProposalConditionsHelpCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - ProposalConditionsHelpCoordinating
extension ProposalConditionsHelpCoordinator: ProposalConditionsHelpCoordinating {
    func perform(action: ProposalConditionsHelpAction) {
        guard case .dismiss = action else {
            return
        }
        viewController?.dismiss(animated: true)
    }
}
