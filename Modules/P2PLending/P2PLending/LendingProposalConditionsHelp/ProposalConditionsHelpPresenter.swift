import Foundation
import UI

protocol ProposalConditionsHelpPresenting: AnyObject {
    var viewController: ProposalConditionsHelpDisplay? { get set }
    func presentInformationBlock(for simulation: ProposalConditionsSimulation)
    func didNextStep(action: ProposalConditionsHelpAction)
}

final class ProposalConditionsHelpPresenter {
    private typealias Localizable = Strings.Lending.Proposal.Conditions.Help
    
    private let coordinator: ProposalConditionsHelpCoordinating
    weak var viewController: ProposalConditionsHelpDisplay?
    
    init(coordinator: ProposalConditionsHelpCoordinating) {
        self.coordinator = coordinator
    }
    
    private func composeInformativeBlock(title: String, body: String) -> NSAttributedString {
        let sharedAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: Colors.grayscale600.color]
        var titleAttributes: [NSAttributedString.Key: Any] = sharedAttributes
        var bodyAttributes: [NSAttributedString.Key: Any] = sharedAttributes
        titleAttributes[.font] = Typography.bodySecondary(.highlight).font()
        titleAttributes[.foregroundColor] = Colors.black.color
        bodyAttributes[.font] = Typography.bodySecondary().font()
        let informationAttributedString = NSMutableAttributedString()
        informationAttributedString.append(NSAttributedString(string: title, attributes: titleAttributes))
        informationAttributedString.append(NSAttributedString(string: "\n\(body)", attributes: bodyAttributes))
        return informationAttributedString
    }
}

// MARK: - ProposalConditionsHelpPresenting
extension ProposalConditionsHelpPresenter: ProposalConditionsHelpPresenting {
    func presentInformationBlock(for simultation: ProposalConditionsSimulation) {
        let attributedString = NSMutableAttributedString()
        attributedString.append(composeInformativeBlock(
            title: Localizable.interestTitle,
            body: Localizable.interestBody
        ))
        attributedString.append(NSAttributedString(string: "\n\n"))
        attributedString.append(composeInformativeBlock(
            title: Localizable.iofTitle,
            body: Localizable.iofBody(simultation.iof.toCurrencyString() ?? "")
        ))
        attributedString.append(NSAttributedString(string: "\n\n"))
        attributedString.append(composeInformativeBlock(
            title: Localizable.commissionTitle,
            body: Localizable.commissionBody(simultation.commissionAmount.toCurrencyString() ?? "")
        ))
        viewController?.displayInformationBlock(text: attributedString)
    }
    
    func didNextStep(action: ProposalConditionsHelpAction) {
        coordinator.perform(action: action)
    }
}
