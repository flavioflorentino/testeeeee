import UIKit
import UI

protocol ProposalConditionsHelpDisplay: AnyObject {
    func displayInformationBlock(text: NSAttributedString)
}

private extension ProposalConditionsHelpViewController.Layout {
    enum Insets {
        static let rootView = EdgeInsets(
            top: Spacing.base04,
            leading: Spacing.base03,
            bottom: Spacing.base04,
            trailing: Spacing.base03
        )
    }
}

final class ProposalConditionsHelpViewController: ViewController<ProposalConditionsHelpViewModelInputs, UIView> {
    fileprivate enum Layout { }
    
    private typealias Localizable = Strings.Lending.Proposal.Conditions.Help
    
    private var dialogTransitioning = DialogTransitioningDelegate()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textAlignment, .center)
        label.text = Localizable.title
        return label
    }()
    
    private lazy var bodyLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    private lazy var dismissButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Localizable.dismissButtonTitle, for: .normal)
        button.addTarget(self, action: #selector(dismissButtonTapped), for: .touchUpInside)
        return button
    }()
    
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [titleLabel, bodyLabel, dismissButton])
        stackView.spacing = Spacing.base04
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.compatibleLayoutMargins = Layout.Insets.rootView
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    
    override init(viewModel: ProposalConditionsHelpViewModelInputs) {
        super.init(viewModel: viewModel)
        transitioningDelegate = dialogTransitioning
        modalPresentationStyle = .custom
        definesPresentationContext = true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.fetchInformation()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        updatePreferredContentSize()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(rootStackView)
    }
    
    override func setupConstraints() {
        rootStackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    private func updatePreferredContentSize() {
        let maximumWidth = UIScreen.main.bounds.size.width - Spacing.base04
        let targetSize = CGSize(width: maximumWidth, height: .leastNonzeroMagnitude)
        preferredContentSize = rootStackView.systemLayoutSizeFitting(
            targetSize,
            withHorizontalFittingPriority: .required,
            verticalFittingPriority: .defaultLow
        )
    }
}

@objc
private extension ProposalConditionsHelpViewController {
    func dismissButtonTapped() {
        viewModel.dismiss()
    }
}

// MARK: ProposalConditionsHelpDisplay
extension ProposalConditionsHelpViewController: ProposalConditionsHelpDisplay {
    func displayInformationBlock(text: NSAttributedString) {
        bodyLabel.attributedText = text
        updatePreferredContentSize()
    }
}
