import Foundation

enum ProposalConditionsHelpFactory {
    static func make(simulation: ProposalConditionsSimulation) -> ProposalConditionsHelpViewController {
        let coordinator: ProposalConditionsHelpCoordinating = ProposalConditionsHelpCoordinator()
        let presenter: ProposalConditionsHelpPresenting = ProposalConditionsHelpPresenter(coordinator: coordinator)
        let viewModel = ProposalConditionsHelpViewModel(presenter: presenter, simulation: simulation)
        let viewController = ProposalConditionsHelpViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
