import Foundation

protocol ProposalConditionsHelpViewModelInputs: AnyObject {
    func fetchInformation()
    func dismiss()
}

final class ProposalConditionsHelpViewModel {
    private let presenter: ProposalConditionsHelpPresenting
    private let simulation: ProposalConditionsSimulation

    init(presenter: ProposalConditionsHelpPresenting, simulation: ProposalConditionsSimulation) {
        self.presenter = presenter
        self.simulation = simulation
    }
}

// MARK: - ProposalConditionsHelpViewModelInputs
extension ProposalConditionsHelpViewModel: ProposalConditionsHelpViewModelInputs {
    func fetchInformation() {
        presenter.presentInformationBlock(for: simulation)
    }
    
    func dismiss() {
        presenter.didNextStep(action: .dismiss)
    }
}
