import Foundation
import UIKit

enum LendingInvestorGreetingsFactory {
    static func make(offerIdentifier: UUID) -> UIViewController {
        let container = DependencyContainer()
        let coordinator: LendingInvestorGreetingsCoordinating = LendingInvestorGreetingsCoordinator(dependencies: container)
        let presenter: LendingInvestorGreetingsPresenting = LendingInvestorGreetingsPresenter(coordinator: coordinator)
        let service: LendingInvestorGreetingsServicing = LendingInvestorGreetingsService(dependencies: container)
        let interactor = LendingInvestorGreetingsInteractor(presenter: presenter,
                                                            service: service,
                                                            offerIdentifier: offerIdentifier,
                                                            dependencies: container)
        let viewController = LendingInvestorGreetingsViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
