import UIKit
import UI
import LendingComponents

protocol LendingInvestorGreetingsDisplay: AnyObject {
    func display(content: LendingInvestorGreetingsViewModel)
    func displayCrednovoDisclaimer()
    func setContinueButton(enabled: Bool)
    func display(error: Error)
    func startLoading()
    func stopLoading()
}

final class LendingInvestorGreetingsViewController: ViewController<LendingInvestorGreetingsInteracting, UIView> {
    private typealias Localizable = Strings.Lending.Investor.Greetings
    
    private lazy var activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(style: .gray)
        activityIndicatorView.color = Colors.grayscale300.color
        return activityIndicatorView
    }()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView(image: Assets.lendingFigureGreetings.image)
        imageView.contentMode = .center
        return imageView
    }()
    
    private lazy var bodyLabel: UILabel = {
        let textLabel = UILabel()
        textLabel.setContentHuggingPriority(.defaultLow, for: .horizontal)
        textLabel.labelStyle(BodyPrimaryLabelStyle(type: .default))
        textLabel.text = Localizable.body
        textLabel.textColor = Colors.grayscale600.color
        return textLabel
    }()
    
    private lazy var footerTextView: UITextView = {
        let textView = TextView()
        textView.dataDetectorTypes = .link
        textView.isScrollEnabled = false
        textView.setContentHuggingPriority(.defaultLow, for: .horizontal)
        textView.isEditable = false
        textView.backgroundColor = .clear
        textView.delegate = self
        textView.linkTextAttributes = [
            .foregroundColor: Colors.branding600.color,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        return textView
    }()
    
    private lazy var firstBulletItemView: BulletItemView = {
        let bulletItemView = BulletItemView()
        bulletItemView.number = 1
        bulletItemView.title = Localizable.firstBulletDescription
        return bulletItemView
    }()
    
    private lazy var secondBulletItemView: BulletItemView = {
        let bulletItemView = BulletItemView()
        bulletItemView.number = 2
        bulletItemView.title = Localizable.secondBulletDescription
        return bulletItemView
    }()
    
    private lazy var thirdBulletItemView: BulletItemView = {
        let bulletItemView = BulletItemView()
        bulletItemView.number = 3
        bulletItemView.title = Localizable.thirdBulletDescription
        return bulletItemView
    }()
    
    private lazy var legalTermsNoticeCheckbox: CheckboxView = {
        let checkboxView = CheckboxView()
        checkboxView.textView.delegate = self
        checkboxView.addTarget(self, action: #selector(legalNoticeCheckboxValueChanged(_:)), for: .valueChanged)
        return checkboxView
    }()
    
    private lazy var continueButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.continueButtonTitle, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(continueButtonTapped), for: .touchUpInside)
        button.isEnabled = false
        return button
    }()
    
    private lazy var bulletsStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [firstBulletItemView, secondBulletItemView, thirdBulletItemView])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var textStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [bodyLabel, bulletsStackView, footerTextView])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base03
        return stackView
    }()
    
    private lazy var headerStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [imageView, textStackView])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base04
        return stackView
    }()
    
    private lazy var footerStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [legalTermsNoticeCheckbox, continueButton])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base03
        return stackView
    }()
    
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [headerStackView, footerStackView])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base04
        stackView.distribution = .equalSpacing
        stackView.compatibleLayoutMargins = .rootView
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.alpha = 0
        return stackView
    }()
    
    private lazy var scrollView = UIScrollView()
    
    // MARK: Navigation items
    
    private lazy var closeButtonItem = UIBarButtonItem(title: GlobalLocalizable.closeButtonTitle,
                                                       style: .plain,
                                                       target: self,
                                                       action: #selector(closeButtonTapped))
    
    private lazy var helpButtonItem: UIBarButtonItem = {
        let buttonItem = UIBarButtonItem(image: Assets.lendingGlyphHelp.image,
                                         style: .plain,
                                         target: self,
                                         action: #selector(helpButtonTapped))
        buttonItem.accessibilityLabel = GlobalLocalizable.helpButtonAccessibilityLabel
        return buttonItem
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.fetchContent()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(rootStackView)
        view.addSubview(activityIndicatorView)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        rootStackView.snp.makeConstraints {
            $0.edges.width.equalToSuperview()
            $0.height.greaterThanOrEqualTo(view.layoutMarginsGuide)
        }
        
        activityIndicatorView.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        view.tintColor = Colors.branding600.color
        navigationItem.leftBarButtonItem = closeButtonItem
        setupNavigationAppearance()
    }
    
    func setupNavigationAppearance() {
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .automatic
            navigationController?.navigationBar.prefersLargeTitles = true
        }
        if #available(iOS 13.0, *) {
            let navigationBarAppearance = UINavigationBarAppearance()
            navigationBarAppearance.configureWithTransparentBackground()
            navigationBarAppearance.backgroundColor = Colors.backgroundPrimary.color
            navigationBarAppearance.shadowColor = .clear
            navigationController?.navigationBar.scrollEdgeAppearance = navigationBarAppearance
            navigationController?.navigationBar.standardAppearance = navigationBarAppearance
        }
    }
}

@objc
extension LendingInvestorGreetingsViewController {
    func legalNoticeCheckboxValueChanged(_ checkboxView: CheckboxView) {
        interactor.awarenessAndAcceptanceOfTermsChanged(checkboxView.isSelected)
    }
    
    func continueButtonTapped() {
        interactor.showOpportunity()
    }
    
    func helpButtonTapped() {
        interactor.showHelp()
    }
    
    func closeButtonTapped() {
        interactor.close()
    }
}

extension LendingInvestorGreetingsViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        switch textView {
        case footerTextView:
            interactor.showCrednovoDisclaimer()
        case legalTermsNoticeCheckbox.textView:
            interactor.showLegalTerm(at: URL)
        default:
            break
        }
        return false
    }
}

// MARK: LendingInvestorGreetingsDisplay
extension LendingInvestorGreetingsViewController: LendingInvestorGreetingsDisplay {
    func display(content: LendingInvestorGreetingsViewModel) {
        navigationItem.title = content.title
        navigationItem.setRightBarButton(helpButtonItem, animated: true)
        footerTextView.attributedText = content.footer
        legalTermsNoticeCheckbox.textView.attributedText = content.legalTermsNotice
        let animator = UIViewPropertyAnimator(duration: 0.3, curve: .easeInOut) {
            self.rootStackView.alpha = 1
        }
        animator.startAnimation()
    }
    
    func displayCrednovoDisclaimer() {
        let contentView = DeclineOfferDialogueContentView(
            title: Localizable.crednovoAlertTitle,
            body: Localizable.crednovoAlertBody
        )
        let alertPopup = PopupViewController(title: nil, preferredType: .customView(contentView, shouldRemoveCloseButton: true))
        alertPopup.addAction(.init(title: GlobalLocalizable.dismissButtonTitle, style: .fill))
        showPopup(alertPopup)
    }
    
    func setContinueButton(enabled: Bool) {
        continueButton.isEnabled = enabled
    }
    
    func display(error: Error) {
        let alert = PopupViewController(
            title: GlobalLocalizable.errorAlertTitle,
            description: error.localizedDescription,
            preferredType: .text
        )
        let dismissAction = PopupAction(title: GlobalLocalizable.dismissButtonTitle, style: .fill) {
            self.dismiss(animated: true)
        }
        alert.addAction(dismissAction)
        showPopup(alert)
    }
    
    func startLoading() {
        activityIndicatorView.startAnimating()
    }
    
    func stopLoading() {
        activityIndicatorView.stopAnimating()
    }
}
