import UI
import Foundation

protocol LendingInvestorGreetingsPresenting: AnyObject {
    var viewController: LendingInvestorGreetingsDisplay? { get set }
    func presentContent(termsOfUseURL: URL, privacyPolicyURL: URL)
    func setContinueButton(enabled: Bool)
    func presentCrednovoDisclaimer()
    func present(error: Error)
    func startLoading()
    func stopLoading()
    func didNextStep(action: LendingInvestorGreetingsAction)
}

final class LendingInvestorGreetingsPresenter {
    private typealias Localizable = Strings.Lending.Investor.Greetings
    
    private let coordinator: LendingInvestorGreetingsCoordinating
    weak var viewController: LendingInvestorGreetingsDisplay?

    init(coordinator: LendingInvestorGreetingsCoordinating) {
        self.coordinator = coordinator
    }
    
    private var footer: NSAttributedString {
        let text = Localizable.footer as NSString
        let highlightRange = text.range(of: Localizable.footerHighlight)
        let attributes: [NSAttributedString.Key: Any] = [
            .font: Typography.bodyPrimary().font(),
            .foregroundColor: Colors.grayscale600.color
        ]
        let attributedString = NSMutableAttributedString(string: Localizable.footer, attributes: attributes)
        attributedString.addAttribute(.link, value: "", range: highlightRange)
        return attributedString
    }
    
    private func legalTermsNotice(termsOfUseURL: URL, privacyPolicyURL: URL) -> NSAttributedString {
        let text = Localizable.legalTermsNotice as NSString
        let privacyPolicyRange = text.range(of: Localizable.legalTermsNoticePrivacyPolicyHighlight)
        let termsAndConditionsRange = text.range(of: Localizable.legalTermsNoticeTermsAndConditionsHighlight)
        let attributes: [NSAttributedString.Key: Any] = [
            .font: Typography.bodySecondary().font(),
            .foregroundColor: Colors.grayscale600.color
        ]
        let attributedString = NSMutableAttributedString(string: Localizable.legalTermsNotice, attributes: attributes)
        attributedString.addAttribute(.link, value: privacyPolicyURL, range: privacyPolicyRange)
        attributedString.addAttribute(.link, value: termsOfUseURL, range: termsAndConditionsRange)
        return attributedString
    }
}

// MARK: - LendingInvestorGreetingsPresenting
extension LendingInvestorGreetingsPresenter: LendingInvestorGreetingsPresenting {
    func presentContent(termsOfUseURL: URL, privacyPolicyURL: URL) {
        let greetingsViewModel = LendingInvestorGreetingsViewModel(
            title: Localizable.title,
            footer: footer,
            legalTermsNotice: legalTermsNotice(termsOfUseURL: termsOfUseURL, privacyPolicyURL: privacyPolicyURL)
        )
        viewController?.display(content: greetingsViewModel)
    }
    
    func setContinueButton(enabled: Bool) {
        viewController?.setContinueButton(enabled: enabled)
    }
    
    func presentCrednovoDisclaimer() {
        viewController?.displayCrednovoDisclaimer()
    }
    
    func present(error: Error) {
        viewController?.display(error: error)
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
    
    func didNextStep(action: LendingInvestorGreetingsAction) {
        coordinator.perform(action: action)
    }
}
