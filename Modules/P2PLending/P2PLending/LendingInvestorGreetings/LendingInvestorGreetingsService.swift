import Foundation
import Core

protocol LendingInvestorGreetingsServicing: Servicing {
    func fetchOpportunityOffer(with identifier: UUID, completion: @escaping ModelCompletionBlock<OpportunityOffer>)
}

final class LendingInvestorGreetingsService: LendingInvestorGreetingsServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    private lazy var decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        return decoder
    }()
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func fetchOpportunityOffer(with identifier: UUID, completion: @escaping ModelCompletionBlock<OpportunityOffer>) {
        let api = Api<OpportunityOffer>(endpoint: P2PLendingEndpoint.opportunityOffer(identifier: identifier))
        api.shouldUseDefaultDateFormatter = false
        api.execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
