import Core
import Foundation
import AnalyticsModule

protocol LendingInvestorGreetingsInteracting: AnyObject {
    func fetchContent()
    func awarenessAndAcceptanceOfTermsChanged(_ accepted: Bool)
    func showOpportunity()
    func showCrednovoDisclaimer()
    func showLegalTerm(at url: URL)
    func showHelp()
    func close()
}

final class LendingInvestorGreetingsInteractor {
    typealias Dependencies = HasAnalytics
    private let presenter: LendingInvestorGreetingsPresenting
    private let service: LendingInvestorGreetingsServicing
    private let offerIdentifier: UUID
    private let dependencies: Dependencies
    private var offer: OpportunityOffer?
    
    init(presenter: LendingInvestorGreetingsPresenting,
         service: LendingInvestorGreetingsServicing,
         offerIdentifier: UUID,
         dependencies: Dependencies) {
        self.presenter = presenter
        self.service = service
        self.offerIdentifier = offerIdentifier
        self.dependencies = dependencies
    }
    
    private func didFetchOffer(_ offer: OpportunityOffer) {
        self.offer = offer
        guard offer.status == .waitingReply else {
            presenter.didNextStep(action: .showOpportunity(identifier: offerIdentifier, animated: false))
            return
        }
        presenter.presentContent(termsOfUseURL: offer.termsConditions, privacyPolicyURL: offer.privacyPolicy)
    }
    
    private func sendAnalyticsEvent(for url: URL) {
        switch url {
        case offer?.termsConditions:
            dependencies.analytics.log(LendingGreetingsEvent.termsOfUseButtonTapped(.investor))
        case offer?.privacyPolicy:
            dependencies.analytics.log(LendingGreetingsEvent.privacyPolicyButtonTapped(.investor))
        default:
            break
        }
    }
}

// MARK: - LendingInvestorGreetingsInteracting
extension LendingInvestorGreetingsInteractor: LendingInvestorGreetingsInteracting {
    func fetchContent() {
        presenter.startLoading()
        service.fetchOpportunityOffer(with: offerIdentifier) { [weak self] result in
            self?.presenter.stopLoading()
            switch result {
            case .success(let offer):
                self?.didFetchOffer(offer)
            case .failure(let error):
                self?.presenter.present(error: error)
            }
        }
    }
    
    func awarenessAndAcceptanceOfTermsChanged(_ accepted: Bool) {
        dependencies.analytics.log(LendingGreetingsEvent.termsAndPrivacyPolicyCheckBox(.investor))
        presenter.setContinueButton(enabled: accepted)
    }
    
    func showOpportunity() {
        dependencies.analytics.log(LendingGreetingsEvent.continueButtonTapped(.investor))
        presenter.didNextStep(action: .showOpportunity(identifier: offerIdentifier, animated: true))
    }
    
    func showCrednovoDisclaimer() {
        dependencies.analytics.log(LendingGreetingsEvent.crednovo(.investor))
        presenter.presentCrednovoDisclaimer()
    }
    
    func showLegalTerm(at url: URL) {
        sendAnalyticsEvent(for: url)
        presenter.didNextStep(action: .showLegalTerms(url: url))
    }
    
    func showHelp() {
        guard let url = URL(string: "picpay://picpay/helpcenter/article/360049387652") else {
            return
        }
        dependencies.analytics.log(LendingGreetingsEvent.helpButtonTapped(.investor))
        presenter.didNextStep(action: .showFAQ(url: url))
    }
    
    func close() {
        dependencies.analytics.log(LendingGreetingsEvent.closeButtonTapped(.investor))
        presenter.didNextStep(action: .close)
    }
}
