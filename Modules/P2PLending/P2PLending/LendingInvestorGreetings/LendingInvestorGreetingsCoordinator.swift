import UIKit
import SafariServices

enum LendingInvestorGreetingsAction {
    case showOpportunity(identifier: UUID, animated: Bool)
    case showLegalTerms(url: URL)
    case showFAQ(url: URL)
    case close
}

protocol LendingInvestorGreetingsCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: LendingInvestorGreetingsAction)
}

final class LendingInvestorGreetingsCoordinator: LendingInvestorGreetingsCoordinating, URLOpenerCoordinating {
    typealias Dependencies = HasURLOpener
    
    private var dependencies: Dependencies
    weak var viewController: UIViewController?
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    func perform(action: LendingInvestorGreetingsAction) {
        switch action {
        case let .showOpportunity(identifier, animated):
            let opportunityController = LendingOpportunityOfferFactory.make(offerIdentifier: identifier)
            viewController?.navigationController?.pushViewController(opportunityController, animated: animated)
        case .showLegalTerms(let url):
            let safariViewController = SFSafariViewController(url: url)
            viewController?.present(safariViewController, animated: true)
        case .showFAQ(let url):
            showFAQ(for: url, using: dependencies)
        case .close:
            viewController?.dismiss(animated: true)
        }
    }
}
