import Foundation

struct LendingInvestorGreetingsViewModel {
    let title: String
    let footer: NSAttributedString
    let legalTermsNotice: NSAttributedString
}
