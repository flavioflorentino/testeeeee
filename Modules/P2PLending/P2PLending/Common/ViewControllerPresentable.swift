import UIKit

protocol ViewControllerPresentable: AnyObject {
    func present(_ viewController: UIViewController, animated: Bool, completion: (() -> Void)?)
}

extension UIViewController: ViewControllerPresentable {}
