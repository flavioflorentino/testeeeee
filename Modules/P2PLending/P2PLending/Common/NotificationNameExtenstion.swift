import Foundation

extension Notification.Name {
    static let proposalStatusDidChangeNotification = Notification.Name("P2PLendingProposalStatusDidChangeNotification")
}
