import Foundation

protocol URLOpenerCoordinating {
    associatedtype ViewController: ViewControllerPresentable
    
    var viewController: ViewController? { get }
    
    func showFAQ(for url: URL, using container: HasURLOpener)
}

extension URLOpenerCoordinating {
    func showFAQ(for url: URL, using container: HasURLOpener) {
        guard container.urlOpener?.canOpenURL(url) == true else {
            return
        }
        container.urlOpener?.open(url, options: [:], completionHandler: nil)
    }
}
