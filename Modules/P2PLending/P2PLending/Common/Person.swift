import Foundation

public protocol PersonConvertible {
    var imageURL: URL? { get }
    var firstName: String? { get }
}

struct Person: PersonConvertible {
    var imageURL: URL?
    var firstName: String?
}
