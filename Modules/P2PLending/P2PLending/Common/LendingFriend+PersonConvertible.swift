import Foundation
extension LendingFriend: PersonConvertible {
    public var imageURL: URL? {
        profileUrl
    }
    
    public var firstName: String? {
        name?.components(separatedBy: .whitespaces).first
    }
}
