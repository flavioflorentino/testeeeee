import UIKit

protocol StringPasteboardProvider: AnyObject {
    var string: String? { get set }
}

extension UIPasteboard: StringPasteboardProvider { }
