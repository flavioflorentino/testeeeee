import UIKit
import UI

struct DetailedError: Equatable {
    let image: UIImage
    let title: String
    let message: String
    let primaryButtonTitle: String
    let linkButtonTitle: String?
    let url: URL?
}

protocol DetailedErrorConvertible {
    var asDetailedError: DetailedError { get }
    var asFeedbackViewContent: ApolloFeedbackViewContent { get }
}

extension DetailedErrorConvertible {
    var asFeedbackViewContent: ApolloFeedbackViewContent {
        let detailedError = asDetailedError
        return ApolloFeedbackViewContent(image: detailedError.image,
                                         title: detailedError.title,
                                         description: NSAttributedString(string: detailedError.message),
                                         primaryButtonTitle: detailedError.primaryButtonTitle,
                                         secondaryButtonTitle: detailedError.linkButtonTitle ?? "")
    }
}
