import XCTest
@testable import P2PLending

private final class ProposalConditionsHelpPresenterSpy: ProposalConditionsHelpPresenting {
    var viewController: ProposalConditionsHelpDisplay?
    
    private(set) var callPresentInformationBlockCount = 0
    private(set) var callDidNextStepCount = 0
    
    private(set) var simulation: ProposalConditionsSimulation?
    private(set) var action: ProposalConditionsHelpAction?
    
    func presentInformationBlock(for simulation: ProposalConditionsSimulation) {
        callPresentInformationBlockCount += 1
        self.simulation = simulation
    }
    
    func didNextStep(action: ProposalConditionsHelpAction) {
        callDidNextStepCount += 1
        self.action = action
    }
}

final class ProposalConditionsHelpViewModelTests: XCTestCase {
    private lazy var spy = ProposalConditionsHelpPresenterSpy()
    private lazy var configuration: ProposalConditionsConfiguration = .mock
    private lazy var simulation: ProposalConditionsSimulation = .mock
    private lazy var sut = ProposalConditionsHelpViewModel(presenter: spy, simulation: simulation)
    
    func testFetchInformation_WhenCalledFromController_ShouldPresentInformationBlock() {
        sut.fetchInformation()
        
        XCTAssertEqual(spy.callPresentInformationBlockCount, 1)
        XCTAssertEqual(spy.simulation, simulation)
    }
    
    func testDismiss_WhenCalledFromController_ShouldCallDidNextStepDismiss() {
        sut.dismiss()
        
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        XCTAssertEqual(spy.action, .dismiss)
    }
}
