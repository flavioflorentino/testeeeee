import XCTest
@testable import P2PLending

final class ProposalConditionsHelpCoordinatorTests: XCTestCase {
    private lazy var spy = ViewControllerSpy()
    private lazy var sut: ProposalConditionsHelpCoordinator = {
        let coordinator = ProposalConditionsHelpCoordinator()
        coordinator.viewController = spy
        return coordinator
    }()
    
    func testPerformActionDismiss_WhenCalledFromPresenter_ShouldDismissViewController() {
        sut.perform(action: .dismiss)
        
        XCTAssertEqual(spy.callDismissCount, 1)
    }
}
