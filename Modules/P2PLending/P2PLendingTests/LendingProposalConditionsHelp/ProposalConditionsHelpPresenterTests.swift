import XCTest
@testable import P2PLending

private final class ProposalConditionsHelpDisplaySpy: ProposalConditionsHelpDisplay {
    private(set) var callDisplayInformationBlockCount = 0
    
    private(set) var text: NSAttributedString?
    
    func displayInformationBlock(text: NSAttributedString) {
        callDisplayInformationBlockCount += 1
        self.text = text
    }
}

private final class ProposalConditionsHelpCoordinatorSpy: ProposalConditionsHelpCoordinating {
    var viewController: UIViewController?
    
    private(set) var callPerformActionCount = 0
    
    private(set) var action: ProposalConditionsHelpAction?
    
    func perform(action: ProposalConditionsHelpAction) {
        callPerformActionCount += 1
        self.action = action
    }
}

final class ProposalConditionsHelpPresenterTests: XCTestCase {
    private typealias Localizable = Strings.Lending.Proposal.Conditions.Help
    
    private lazy var coordinatorSpy = ProposalConditionsHelpCoordinatorSpy()
    private lazy var spy = ProposalConditionsHelpDisplaySpy()
    private lazy var sut: ProposalConditionsHelpPresenter = {
        let presenter = ProposalConditionsHelpPresenter(coordinator: coordinatorSpy)
        presenter.viewController = spy
        return presenter
    }()
    
    func testPresentInformationBlock_WhenCalledFromViewModel_ShouldDisplayInformationBlock() throws {
        let simulation: ProposalConditionsSimulation = .mock
        let formattedIOFValue = simulation.iof.toCurrencyString() ?? ""
        let formattedCommissionAmountValue = simulation.commissionAmount.toCurrencyString() ?? ""
        let expectedFinalText = [
            "\(Localizable.interestTitle)\n\(Localizable.interestBody)\n\n",
            "\(Localizable.iofTitle)\n\(Localizable.iofBody(formattedIOFValue))\n\n",
            "\(Localizable.commissionTitle)\n\(Localizable.commissionBody(formattedCommissionAmountValue))"
        ].joined()
        
        sut.presentInformationBlock(for: simulation)
        
        XCTAssertEqual(spy.callDisplayInformationBlockCount, 1)
        XCTAssertEqual(spy.text?.string, expectedFinalText)
    }
    
    func testDidNextStepDismiss_WhenCalledFromViewModel_ShouldPerformActionDismiss() {
        let action: ProposalConditionsHelpAction = .dismiss
        sut.didNextStep(action: action)
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        XCTAssertEqual(coordinatorSpy.action, action)
    }
}
