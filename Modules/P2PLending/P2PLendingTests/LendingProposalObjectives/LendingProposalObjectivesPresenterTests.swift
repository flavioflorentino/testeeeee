import UI
import XCTest
@testable import P2PLending

private final class LendingProposalObjectivesDisplaySpy: UIViewController, LendingProposalObjectivesDisplay {
    private(set) var callDisplayObjectivesCount = 0
    
    private(set) var objectives: [ProposalObjective]?
    
    func display(objectives: [ProposalObjective]) {
        callDisplayObjectivesCount += 1
        self.objectives = objectives
    }
}

private final class LendingProposalObjectivesCoordinatorSpy: LendingProposalObjectivesCoordinating {
    var viewController: UIViewController?
    
    private(set) var callPerformActionCount = 0
    
    private(set) var action: LendingProposalObjectivesAction?
    
    func perform(action: LendingProposalObjectivesAction) {
        callPerformActionCount += 1
        self.action = action
    }
}

final class LendingProposalObjectivesPresenterTests: XCTestCase {
    private lazy var spy = LendingProposalObjectivesDisplaySpy()
    private lazy var coordinatorSpy = LendingProposalObjectivesCoordinatorSpy()
    private lazy var sut: LendingProposalObjectivesPresenter = {
        let presenter = LendingProposalObjectivesPresenter(coordinator: coordinatorSpy)
        presenter.viewController = spy
        return presenter
    }()
    
    func testPresentObjectives_WhenCalledFromViewModel_ShouldCallDisplayObjectives() {
        let objectives: [ProposalObjective] = [.mock]
        
        sut.present(objectives: objectives)
        
        XCTAssertEqual(spy.callDisplayObjectivesCount, 1)
        XCTAssertEqual(spy.objectives, objectives)
    }
    
    func testDidNextStepFriendSelection_WhenCalledFromViewModel_ShouldCallPerformNextStepFriendSelection() {
        let expectedProposal: Proposal = .mock
        let action: LendingProposalObjectivesAction = .friendSelection(expectedProposal)
        
        sut.didNextStep(action: action)
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        
        guard case let .friendSelection(proposal) = coordinatorSpy.action else {
            XCTFail("wrong action called")
            return
        }
        
        XCTAssertEqual(proposal, expectedProposal)
    }
}
