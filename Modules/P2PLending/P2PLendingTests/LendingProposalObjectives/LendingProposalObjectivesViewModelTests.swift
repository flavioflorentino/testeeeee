import XCTest
import AnalyticsModule
@testable import P2PLending

private final class LendingProposalObjectivesPresenterSpy: LendingProposalObjectivesPresenting {
    var viewController: LendingProposalObjectivesDisplay?
    
    private(set) var callPresentObjectivesCount = 0
    private(set) var callDidNextStepActionCount = 0
    
    private(set) var objectives: [ProposalObjective]?
    private(set) var action: LendingProposalObjectivesAction?
    
    func present(objectives: [ProposalObjective]) {
        callPresentObjectivesCount += 1
        self.objectives = objectives
    }
    
    func didNextStep(action: LendingProposalObjectivesAction) {
        callDidNextStepActionCount += 1
        self.action = action
    }
}

final class LendingProposalObjectivesViewModelTests: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    private lazy var objectives: [ProposalObjective] = [.mock]
    private lazy var spy = LendingProposalObjectivesPresenterSpy()
    private lazy var dependencyMock = DependencyContainerMock(analyticsSpy)
    private lazy var sut = LendingProposalObjectivesViewModel(presenter: spy,
                                                              objectives: objectives,
                                                              proposal: .mock,
                                                              dependencies: dependencyMock)
    
    func testFetchObjectives_WhenCalledFromController_ShouldPresentObjectives() {
        sut.fetchObjectives()
        
        XCTAssertEqual(spy.callPresentObjectivesCount, 1)
        XCTAssertEqual(spy.objectives, objectives)
    }
    
    func testSelectRow_WhenDidSelectObjective_ShouldCallDidNextStepFriendSelectionWithSelectedObjective() {
        let expectedEvent = LendingProposalObjectivesEvent.selectedObjective(objectives[0].title ?? "").event()
        sut.selectObjective(at: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(spy.callDidNextStepActionCount, 1)
        guard case let .friendSelection(proposal) = spy.action else {
            XCTFail("wrong action called")
            return
        }
        
        XCTAssertEqual(proposal.objective, objectives.first)
    }
    
    func testEventTracking_WhenCalledFromController_ShouldSendEventTracking() {
        let expectedEvent = LendingProposalObjectivesEvent.backButton.event()
       
        sut.sendTrackingEvent(.backButton)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
}
