import XCTest
@testable import P2PLending

final class LendingProposalObjectivesCoordinatorTests: XCTestCase {
    private lazy var spy = ViewControllerSpy()
    private lazy var sut: LendingProposalObjectivesCoordinator = {
        let coordinator = LendingProposalObjectivesCoordinator(data: .successData)
        coordinator.viewController = spy
        return coordinator
    }()
    
    func testPerformActionFriendSelection_WhenCalledFromPresenter_ShouldShowFriendsViewController() {
        sut.perform(action: .friendSelection(.mock))
        
        XCTAssertEqual(spy.callShowViewControllerCount, 1)
        XCTAssertTrue(spy.viewController is LendingProposalFriendsViewController)
    }
}
