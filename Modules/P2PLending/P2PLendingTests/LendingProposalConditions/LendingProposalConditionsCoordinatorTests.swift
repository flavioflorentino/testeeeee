import XCTest
@testable import P2PLending

final class LendingProposalConditionsCoordinatorTests: XCTestCase {
    private lazy var spy = ViewControllerSpy()
    private lazy var urlOpener = URLOpenerSpy()
    private lazy var sut: LendingProposalConditionsCoordinator = {
        let coordinator = LendingProposalConditionsCoordinator(dependencies: DependencyContainerMock(urlOpener))
        coordinator.viewController = spy
        return coordinator
    }()
    
    func testPerformActionProposalSummary_WhenCalledFromPresenter_ShouldShowProposalSummaryController() {
        sut.perform(action: .proposalSummary(proposal: .mock))
        
        XCTAssertEqual(spy.callShowViewControllerCount, 1)
        XCTAssertTrue(spy.viewController is LendingProposalSummaryViewController)
    }
    
    func testPerformActionTaxInformation_WhenCalledFromPresenter_ShouldShowProposalConditionsHelpController() {
        sut.perform(action: .taxInformation(simulation: .mock))
        
        XCTAssertEqual(spy.callPresentViewControllerCount, 1)
        XCTAssertTrue(spy.viewController is ProposalConditionsHelpViewController)
    }
    
    func testPerformActionShowFAQ_WhenCalledFromPresenter_ShouldShowFAQ() throws {
        let url = try XCTUnwrap(URL(string: "https://www.picpay.com"))
        
        sut.perform(action: .showFAQ(url: url))
        
        XCTAssertEqual(urlOpener.callCanOpenURLCount, 1)
        XCTAssertEqual(urlOpener.callOpenURLCount, 1)
        XCTAssertEqual(urlOpener.checkedURL, url)
        XCTAssertEqual(urlOpener.openedURL, url)
    }
}
