@testable import P2PLending
import AnalyticsModule
import Core
import XCTest

final class LendingProposalConditionsPresenterSpy: LendingProposalConditionsPresenting {
    var viewController: LendingProposalConditionsDisplay?
    
    private(set) var callSetupFormCount = 0
    private(set) var callPresentProposalConditionsSimulationCount = 0
    private(set) var callPresentSimulationErrorCount = 0
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    private(set) var callDidNextStepCount = 0
    
    private(set) var configuration: ProposalConditionsConfiguration?
    private(set) var proposal: Proposal?
    private(set) var simulation: ProposalConditionsSimulation?
    private(set) var action: LendingProposalConditionsAction?
    
    func setupForm(using configuration: ProposalConditionsConfiguration, and proposal: Proposal) {
        callSetupFormCount += 1
        self.configuration = configuration
        self.proposal = proposal
    }
    
    func presentProposalConditionsSimulation(_ simulation: ProposalConditionsSimulation, for proposal: Proposal) {
        callPresentProposalConditionsSimulationCount += 1
        self.simulation = simulation
        self.proposal = proposal
    }
    
    func presentSimulationError() {
        callPresentSimulationErrorCount += 1
    }
    
    func startLoadingSimulation() {
        callStartLoadingCount += 1
    }
    
    func stopLoadingSimulation() {
        callStopLoadingCount += 1
    }
    
    func didNextStep(action: LendingProposalConditionsAction) {
        callDidNextStepCount += 1
        self.action = action
    }
}

final class LendingProposalConditionsServiceMock: LendingProposalConditionsServicing {
    var expectedSimulateProposalResult: Result<ProposalConditionsSimulation, ApiError> = .success(.mock)
    var callCancelSimulateProposalResquestCount = 0
    
    func simulateProposal(_ proposal: Proposal, completion: @escaping ModelCompletionBlock<ProposalConditionsSimulation>) {
        completion(expectedSimulateProposalResult)
    }
    
    func cancelSimulateProposalResquest() {
        callCancelSimulateProposalResquestCount += 1
    }
}

final class LendingProposalConditionsViewModelTests: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    private lazy var container = DependencyContainerMock(analyticsSpy)
    private lazy var serviceMock = LendingProposalConditionsServiceMock()
    private lazy var spy = LendingProposalConditionsPresenterSpy()
    private lazy var sut = LendingProposalConditionsViewModel(
        service: serviceMock,
        presenter: spy,
        configuration: .mock,
        proposal: .mock,
        dependencies: container,
        shouldDelaySimulationRequests: false
    )
    
    func testFetchConfiguration_WhenReceiveSuccessFromSimulation_ShouldSetupFormAndPresentProposalConditionsSimulation() {
        let configuration = ProposalConditionsConfiguration.mock
        let proposal = Proposal.mock
        let simulation = ProposalConditionsSimulation.mock
        
        sut.fetchConfiguration()
    
        XCTAssertEqual(spy.callSetupFormCount, 1)
        XCTAssertEqual(spy.configuration, configuration)
        XCTAssertEqual(spy.proposal, proposal)
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callPresentProposalConditionsSimulationCount, 1)
        XCTAssertEqual(spy.simulation, simulation)
        XCTAssertEqual(serviceMock.callCancelSimulateProposalResquestCount, 1)
    }
    
    func testFetchConfiguration_WhenReceiveFailureFromSimulation_ShouldSetupFormAndPresentError() {
        let configuration = ProposalConditionsConfiguration.mock
        let proposal = Proposal.mock
        let error: ApiError = .connectionFailure
        
        serviceMock.expectedSimulateProposalResult = .failure(error)
        
        sut.fetchConfiguration()
        
        XCTAssertEqual(spy.callSetupFormCount, 1)
        XCTAssertEqual(spy.configuration, configuration)
        XCTAssertEqual(spy.proposal, proposal)
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callPresentSimulationErrorCount, 1)
        XCTAssertEqual(serviceMock.callCancelSimulateProposalResquestCount, 1)
    }
    
    func testSimulateProposal_WhenReceiveSuccessFromService_ShouldPresentProposalConditionsSimulation() {
        let simulation = ProposalConditionsSimulation.mock
        
        sut.simulateProposal()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callPresentProposalConditionsSimulationCount, 1)
        XCTAssertEqual(spy.simulation, simulation)
        XCTAssertEqual(serviceMock.callCancelSimulateProposalResquestCount, 1)
    }
    
    func testSimulateProposal_WhenReceiveFailureFromService_ShouldPresentError() {
        let error: ApiError = .connectionFailure
        serviceMock.expectedSimulateProposalResult = .failure(error)
        
        sut.simulateProposal()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callPresentSimulationErrorCount, 1)
        XCTAssertEqual(serviceMock.callCancelSimulateProposalResquestCount, 1)
    }
    
    func testSimulateProposal_WhenReceiveCancelledRequestFailureFromService_ShouldContinueSimulation() {
        let error: ApiError = .cancelled
        serviceMock.expectedSimulateProposalResult = .failure(error)
        
        sut.simulateProposal()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(serviceMock.callCancelSimulateProposalResquestCount, 1)
        XCTAssertEqual(spy.callPresentSimulationErrorCount, 0)
    }
    
    func testInstallmentValueDidChange_WhenReceiveSuccessFromService_ShouldPresentProposalConditionsSimulation() {
        let simulation = ProposalConditionsSimulation.mock
        let installmentValue = 1
        var proposal = Proposal.mock
        proposal.installments = installmentValue
        
        sut.installmentsValueChanged(Double(installmentValue))
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callPresentProposalConditionsSimulationCount, 1)
        XCTAssertEqual(serviceMock.callCancelSimulateProposalResquestCount, 1)
        XCTAssertEqual(spy.simulation, simulation)
        XCTAssertEqual(spy.proposal, proposal)
    }
    
    func testInstallmentValueDidChange_WhenReceiveFailureFromService_ShouldPresentError() {
        let error: ApiError = .connectionFailure
        
        serviceMock.expectedSimulateProposalResult = .failure(error)
        
        sut.installmentsValueChanged(1)
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(serviceMock.callCancelSimulateProposalResquestCount, 1)
        XCTAssertEqual(spy.callPresentSimulationErrorCount, 1)
    }
    
    func testInterestValueDidChange_WhenReceiveSuccessFromService_ShouldPresentProposalConditionsSimulation() {
        let simulation = ProposalConditionsSimulation.mock
        let interestValue: Float = 1.0
        var proposal = Proposal.mock
        proposal.interest = interestValue
        
        sut.interestValueChanged(Double(interestValue))
        
        XCTAssertEqual(spy.callStartLoadingCount, 2)
        XCTAssertEqual(serviceMock.callCancelSimulateProposalResquestCount, 1)
        XCTAssertEqual(spy.callPresentProposalConditionsSimulationCount, 1)
        XCTAssertEqual(spy.simulation, simulation)
        XCTAssertEqual(spy.proposal, proposal)
    }
    
    func testInterestValueDidChange_WhenReceiveFailureFromService_ShouldPresentError() {
        let error: ApiError = .connectionFailure
        
        serviceMock.expectedSimulateProposalResult = .failure(error)
        
        sut.interestValueChanged(1)
        
        XCTAssertEqual(spy.callStartLoadingCount, 2)
        XCTAssertEqual(serviceMock.callCancelSimulateProposalResquestCount, 1)
        XCTAssertEqual(spy.callPresentSimulationErrorCount, 1)
    }
    
    func testShowFAQ_WhenCalledFromController_ShouldCallDidNextStepFAQ() {
        let expectedEvent = LendingProposalConditionsEvent.helpTapped.event()
        sut.showFAQ()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        
        guard case .showFAQ = spy.action else {
            XCTFail("wrong action called")
            return
        }
    }
    
    func testShowTaxInformation_WhenCalledFromController_ShouldCallDidNextStepTaxInformation() {
        let expectedEvent = LendingProposalConditionsEvent.faqTapped.event()
        sut.showTaxInformation()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        guard case let .taxInformation(simulation) = spy.action else {
            XCTFail("wrong action called")
            return
        }
        
        XCTAssertEqual(simulation, .mock)
    }
    
    func testContinueButtonTapped_WhenCalledFromController_ShouldCallDidNextStepProposalSummary() {
        let expectedEvent = LendingProposalConditionsEvent.checkProposalTapped.event()
        sut.continueButtonTapped()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        guard case let .proposalSummary(proposal) = spy.action else {
            XCTFail("wrong action called")
            return
        }
        
        XCTAssertEqual(proposal, Proposal.mock)
    }
    
    func  testShowFaq_WhenCalledFromController_ShouldShowFaq() {
        let expectedEvent = LendingProposalConditionsEvent.faqTapped.event()
        sut.showTaxInformation()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testSendAnalyticsEvent_WhenCalledFromBackButtonFromController_ShouldSendEvent() {
        let expectedEvent = LendingProposalConditionsEvent.backButtonTapped.event()
        sut.sendAnalyticsEvent(type: .backButtonTapped)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testSendAnalyticsEvent_WhenCalledFromTryAgainButtonFromController_ShouldSendEvent() {
        let expectedEvent = LendingProposalConditionsEvent.tryAgainTapped.event()
        sut.sendAnalyticsEvent(type: .tryAgainTapped)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
}
