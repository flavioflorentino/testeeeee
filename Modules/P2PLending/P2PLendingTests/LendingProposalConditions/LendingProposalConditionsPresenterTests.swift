@testable import P2PLending
import Core
import XCTest

final class LendingProposalConditionsDisplaySpy: LendingProposalConditionsDisplay {
    private(set) var callSetupInstallmentsStepperCount = 0
    private(set) var callSetupInterestStepperCount = 0
    private(set) var callDisplayProposalConditionsSummaryCount = 0
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    private(set) var callSetContinueButtonEnabledCount = 0
    private(set) var callSetTaxInformationButtonEnableCount = 0
    
    private(set) var installmentMinimumValue: Double?
    private(set) var installmentMaximumValue: Double?
    private(set) var installmentSuggestedValue: Double?
    private(set) var installmentsFooterText: String?
    private(set) var interestMinimumValue: Double?
    private(set) var interestMaximumValue: Double?
    private(set) var interestSuggestedValue: Double?
    private(set) var interestFooterText: String?
    private(set) var summaryText: NSAttributedString?
    private(set) var allowsInteraction: Bool?
    private(set) var continueButtonState: Bool?
    private(set) var taxInformationButtonIsEnable: Bool?
    
    func setupInstallmentsStepper(minimumValue: Double, maximumValue: Double, suggestedValue: Double, footerText: String?) {
        callSetupInstallmentsStepperCount += 1
        installmentMinimumValue = minimumValue
        installmentMaximumValue = maximumValue
        installmentSuggestedValue = suggestedValue
        installmentsFooterText = footerText
    }
    
    func setupInterestStepper(minimumValue: Double, maximumValue: Double, suggestedValue: Double, footerText: String?) {
        callSetupInterestStepperCount += 1
        interestMinimumValue = minimumValue
        interestMaximumValue = maximumValue
        interestSuggestedValue = suggestedValue
        interestFooterText = footerText
    }
    
    func displayProposalConditionsSummary(_ summaryText: NSAttributedString, allowsInteraction: Bool) {
        callDisplayProposalConditionsSummaryCount += 1
        self.summaryText = summaryText
        self.allowsInteraction = allowsInteraction
    }
    
    func startLoadingSummary() {
        callStartLoadingCount += 1
    }
    
    func stopLoadingSummary() {
        callStopLoadingCount += 1
    }
    
    func setContinueButtonEnable(state: Bool) {
        callSetContinueButtonEnabledCount += 1
        continueButtonState = state
    }
    
    func setTaxInformationButtonEnable(_ isEnable: Bool) {
        callSetTaxInformationButtonEnableCount += 1
        taxInformationButtonIsEnable = isEnable
    }
}

final class LendingProposalConditionsCoordinatorSpy: LendingProposalConditionsCoordinating {
    var viewController: UIViewController?
    
    private(set) var callPerformActionCount = 0
    
    private(set) var action: LendingProposalConditionsAction?
    
    func perform(action: LendingProposalConditionsAction) {
        callPerformActionCount += 1
        self.action = action
    }
}

final class LendingProposalConditionsPresenterTests: XCTestCase {
    private lazy var container = DependencyContainerMock()
    private lazy var spy = LendingProposalConditionsDisplaySpy()
    private lazy var coordinatorSpy = LendingProposalConditionsCoordinatorSpy()
    private lazy var sut: LendingProposalConditionsPresenter = {
        let presenter = LendingProposalConditionsPresenter(coordinator: coordinatorSpy)
        presenter.viewController = spy
        return presenter
    }()
    
    func testSetupForm_WhenCalledFromViewModel_ShouldSetupInstallmentsAndInterestSteppers() throws {
        let configuration = ProposalConditionsConfiguration.mock
        let proposal = Proposal.mock
        
        sut.setupForm(using: configuration, and: proposal)
        
        XCTAssertEqual(spy.callSetupInstallmentsStepperCount, 1)
        XCTAssertEqual(spy.callSetupInterestStepperCount, 1)
        XCTAssertEqual(spy.installmentMinimumValue, Double(configuration.minimumInstallments))
        XCTAssertEqual(spy.installmentMaximumValue, Double(configuration.maximumInstallments))
        XCTAssertEqual(spy.installmentSuggestedValue, Double(try XCTUnwrap(proposal.installments)))
        XCTAssertEqual(spy.interestMinimumValue, Double(configuration.minimumTax))
        XCTAssertEqual(spy.interestMaximumValue, Double(configuration.maximumTax))
        XCTAssertEqual(spy.interestSuggestedValue, Double(try XCTUnwrap(proposal.interest)))
    }
    
    func testPresentProposalConditionsSimulation_WhenCalledFromViewModel_ShouldCallDisplayProposalConditionsSummary() throws {
        let simulation = ProposalConditionsSimulation.mock
        let proposal = Proposal.mock
        
        let formattedInstallmentAmount = try XCTUnwrap(simulation.installmentAmount.toCurrencyString())
        let formattedInstallment = "\(simulation.installments)x"
        
        let expectedSummaryText = Strings.Lending.Proposal.Conditions.footerText(formattedInstallment, formattedInstallmentAmount)
        
        sut.presentProposalConditionsSimulation(simulation, for: proposal)
        
        XCTAssertEqual(spy.callDisplayProposalConditionsSummaryCount, 1)
        XCTAssertEqual(spy.summaryText?.string, expectedSummaryText)
        XCTAssertEqual(spy.allowsInteraction, false)
        XCTAssertEqual(spy.callSetContinueButtonEnabledCount, 1)
        XCTAssertEqual(spy.callSetTaxInformationButtonEnableCount, 1)
        XCTAssertEqual(spy.continueButtonState, true)
        XCTAssertEqual(spy.taxInformationButtonIsEnable, true)
        XCTAssertEqual(spy.callStopLoadingCount, 1)
    }
    
    func testPresentSimulationError_WhenCalledFromViewModel_ShouldCallDisplayProposalConditionsSummaryAllowingUserInteraction() {
        let simulationErrorText = Strings.Lending.Proposal.Conditions.simulationErrorText
        let simulationErrorButtonTitle = Strings.Lending.Proposal.Conditions.simulationErrorButtonTitle
        let expectedSummaryText = simulationErrorText + simulationErrorButtonTitle
        sut.presentSimulationError()
        
        XCTAssertEqual(spy.callDisplayProposalConditionsSummaryCount, 1)
        XCTAssertEqual(spy.summaryText?.string, expectedSummaryText)
        XCTAssertEqual(spy.allowsInteraction, true)
        XCTAssertEqual(spy.callSetContinueButtonEnabledCount, 1)
        XCTAssertEqual(spy.continueButtonState, false)
        XCTAssertEqual(spy.callStopLoadingCount, 1)
    }
    
    func testStartLoadingSimulation_WhenCalledFromViewModel_ShouldStartLoadingSimulation() {
        sut.startLoadingSimulation()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callSetContinueButtonEnabledCount, 1)
        XCTAssertEqual(spy.callSetTaxInformationButtonEnableCount, 1)
        XCTAssertEqual(spy.continueButtonState, false)
        XCTAssertEqual(spy.taxInformationButtonIsEnable, false)
    }
    
    func testStopLoadingSimulation() {
        sut.stopLoadingSimulation()
        
        XCTAssertEqual(spy.callStopLoadingCount, 1)
    }
    
    func testDidNextStepProposalSummary_WhenCalledFromViewModel_ShouldPerformActionProposalSummary() {
        let proposalMock = Proposal.mock
        
        sut.didNextStep(action: .proposalSummary(proposal: proposalMock))
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        
        guard case let .proposalSummary(proposal) = coordinatorSpy.action else {
            XCTFail()
            return
        }
        
        XCTAssertEqual(proposal, proposalMock)
    }
}
