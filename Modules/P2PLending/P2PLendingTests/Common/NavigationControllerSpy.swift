import UIKit

final class NavigationControllerSpy: UINavigationController {
    private(set) var callPushViewControllerCount = 0
    private(set) var callPopViewControllerCount = 0
    private(set) var callPresentViewControllerCount = 0
    private(set) var viewController: UIViewController?
    private(set) var animated: Bool?
    
    override func popViewController(animated: Bool) -> UIViewController? {
        callPopViewControllerCount += 1
        return nil
    }
    
    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        callPresentViewControllerCount += 1
        viewController = viewControllerToPresent
        animated = flag
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        super.pushViewController(viewController, animated: animated)
        callPushViewControllerCount += 1
        self.viewController = viewController
        self.animated = animated
    }
}
