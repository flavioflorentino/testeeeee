import P2PLending

final class BilletSceneProviderSpy: BilletSceneProvider {
    private(set) var callBilletSceneCount = 0
    
    private(set) var digitableLine: String?
    private(set) var description: String?
    
    func billetScene(for digitableLine: String, with description: String) -> UIViewController {
        callBilletSceneCount += 1
        self.digitableLine = digitableLine
        self.description = description
        return UIViewController()
    }
}
