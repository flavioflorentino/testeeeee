import Core
@testable import P2PLending

extension Servicing {
    func callCompletionIfNeeded<T>(
        _ completion: @escaping ModelCompletionBlock<T>,
        expectedResult: Result<T, ApiError>?
    ) where T : Decodable {
        guard let expectedResult = expectedResult else {
            return
        }
        completion(expectedResult)
    }
}
