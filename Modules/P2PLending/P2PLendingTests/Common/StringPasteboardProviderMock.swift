@testable import P2PLending

final class StringPasteboardProviderMock: StringPasteboardProvider {
    var string: String?
}
