import Core

final class AuthenticationContractMock: AuthenticationContract {
    var isAuthenticated: Bool = false
    
    var validAuthentication: Bool = true
    var authCanceledByUser: Bool = false
    
    func authenticate(_ completion: @escaping AuthenticationResult) {
        if authCanceledByUser {
            return completion(.failure(.cancelled))
        }
        
        if validAuthentication {
            return completion(.success("12345"))
        } else {
            return completion(.failure(.nilPassword))
        }
    }

    private(set) var disableBiometricAuthenticationCallCount = 0
    func disableBiometricAuthentication() {
        disableBiometricAuthenticationCallCount += 1
    }
}
