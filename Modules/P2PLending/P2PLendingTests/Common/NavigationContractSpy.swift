import UI
import UIKit

final class NavigationContractSpy: NavigationContract {
    private(set) var getCurrentNavigationCallsCount = 0
    private(set) var updateCurrentCoordinatingCallsCount = 0
    private(set) var updateCurrentCoordinatingReceivedInvocations: [Coordinating?] = []
    
    var getCurrentNavigationReturnValue: UINavigationController?
    
    func getCurrentNavigation() -> UINavigationController? {
        getCurrentNavigationCallsCount += 1
        return getCurrentNavigationReturnValue
    }
    
    func updateCurrentCoordinating(_ coordinating: Coordinating?) {
        updateCurrentCoordinatingCallsCount += 1
        updateCurrentCoordinatingReceivedInvocations.append(coordinating)
    }
}
