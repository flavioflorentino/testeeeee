import UIKit

final class ViewControllerSpy: UIViewController {
    private(set) var callDismissCount = 0
    private(set) var callShowViewControllerCount = 0
    private(set) var callPresentViewControllerCount = 0
    
    private(set) var viewController: UIViewController?
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        callDismissCount += 1
        completion?()
    }
    
    override func show(_ vc: UIViewController, sender: Any?) {
        callShowViewControllerCount += 1
        viewController = vc
    }
    
    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        callPresentViewControllerCount += 1
        viewController = viewControllerToPresent
    }
}
