import P2PLending

final class PPAuthSwiftContractSpy: PPAuthSwiftContract {
    private(set) var callHandlePasswordCount = 0
    
    func handlePassword() {
        callHandlePasswordCount += 1
    }
}
