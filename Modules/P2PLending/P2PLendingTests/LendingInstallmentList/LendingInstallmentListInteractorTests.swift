import AnalyticsModule
import Core
import XCTest
import FeatureFlag
import UI
@testable import P2PLending

private final class LendingInstallmentListPresenterSpy: LendingInstallmentListPresenting {
    var viewController: LendingInstallmentListDisplaying?
    
    private(set) var callDisplayHeadline = 0
    private(set) var callDisplayOpenInstallmentListCount = 0
    private(set) var callDisplayPaidInstallmentListCount = 0
    private(set) var callShouldDisplayOpenInstallmentsEmptyStateCount = 0
    private(set) var callShouldDisplayPaidInstallmentsEmptyStateCount = 0
    private(set) var callPresentInstallmentAntecipationPopupCount = 0
    private(set) var callPresentUnpayableInstallmentPopupCount = 0
    private(set) var callPresentPendingPaymentFromBorrower = 0
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    private(set) var callDisplayNoConnectionErrorCount = 0
    private(set) var callDisplayGenericErrorCount = 0
    private(set) var callDidNextStepCount = 0
    
    private(set) var installmentListResponse: InstallmentListResponse?
    private(set) var installmentItemList: [InstallmentItem]?
    private(set) var listIsEmpty: Bool?
    private(set) var action: LendingInstallmentListAction?
    private(set) var faqUrl: URL?
    
    func displayHeadline(with installmentListResponse: InstallmentListResponse) {
        callDisplayHeadline += 1
        self.installmentListResponse = installmentListResponse
    }
    
    func displayOpenInstallmentList(_ installmentItemList: [InstallmentItem]) {
        callDisplayOpenInstallmentListCount += 1
        self.installmentItemList = installmentItemList
    }
    
    func displayPaidInstallmentList(_ installmentItemList: [InstallmentItem]) {
        callDisplayPaidInstallmentListCount += 1
        self.installmentItemList = installmentItemList
    }
    
    func shouldDisplayOpenInstallmentsEmptyState(_ listIsEmpty: Bool) {
        callShouldDisplayOpenInstallmentsEmptyStateCount += 1
        self.listIsEmpty = listIsEmpty
    }
    
    func shouldDisplayPaidInstallmentsEmptyState(_ listIsEmpty: Bool) {
        callShouldDisplayPaidInstallmentsEmptyStateCount += 1
        self.listIsEmpty = listIsEmpty
    }
    
    func presentInstallmentAntecipationPopup() {
        callPresentInstallmentAntecipationPopupCount += 1
    }
    
    func presentUnpayableInstallmentPopup() {
        callPresentUnpayableInstallmentPopupCount += 1
    }
    
    func presentPendingPaymentFromBorrower(faqUrl: URL) {
        self.faqUrl = faqUrl
        callPresentPendingPaymentFromBorrower += 1
    }
    
    func startLoading() {
        callStartLoadingCount += 1
    }
    
    func stopLoading() {
        callStopLoadingCount += 1
    }
    
    func displayNoConnectionError() {
        callDisplayNoConnectionErrorCount += 1
    }
    
    func displayGenericError() {
        callDisplayGenericErrorCount += 1
    }
    
    func didNextStep(action: LendingInstallmentListAction) {
        callDidNextStepCount += 1
        self.action = action
    }
}

private final class LendingInstallmentListServiceMock: LendingInstallmentListServicing {
    var fetchInstallmentListExpectedResult: Result<InstallmentListResponse, ApiError>?
    
    func fetchInstallmentList(for agent: ConsultingAgent, with offerID: UUID, completion: @escaping ModelCompletionBlock<InstallmentListResponse>) {
        callCompletionIfNeeded(completion, expectedResult: fetchInstallmentListExpectedResult)
    }
}

final class LendingInstallmentListInteractorTests: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    lazy var featureManagerMock: FeatureManagerMock = {
        let featureManager = FeatureManagerMock()
        featureManager.override(key: .isP2PLendingBorrowerInstallmentDetailsAvailable, with: true)
        return featureManager
    }()
    private lazy var dependencyMock = DependencyContainerMock(analyticsSpy, featureManagerMock)
    private lazy var identifier = UUID()
    private lazy var serviceMock = LendingInstallmentListServiceMock()
    private lazy var spy = LendingInstallmentListPresenterSpy()
    private func sut(for agent: ConsultingAgent) -> LendingInstallmentListInteractor {
        LendingInstallmentListInteractor(service: serviceMock,
                                         presenter: spy,
                                         dependencies: dependencyMock,
                                         proposalIdentifier: identifier,
                                         agent: agent)
    }
    
    func testFetchInstallmentList_WhenReceiveSuccessFromServer_ShouldPresentList() {
        let sut = self.sut(for: .borrower)
        let expectedResponse = InstallmentListResponse.mock
        serviceMock.fetchInstallmentListExpectedResult = .success(expectedResponse)
        
        sut.fetchInstallmentList()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callStopLoadingCount, 1)
        XCTAssertEqual(spy.installmentListResponse, expectedResponse)
        XCTAssertEqual(spy.callDisplayHeadline, 1)
        XCTAssertEqual(spy.callShouldDisplayOpenInstallmentsEmptyStateCount, 1)
        XCTAssertEqual(spy.callShouldDisplayPaidInstallmentsEmptyStateCount, 1)
        XCTAssertEqual(spy.callDisplayOpenInstallmentListCount, 1)
        XCTAssertEqual(spy.callDisplayPaidInstallmentListCount, 1)
    }
    
    func testFetchInstallmentList_WhenReceiveFailureFromServer_ShouldPresentError() {
        let sut = self.sut(for: .borrower)
        let error: ApiError = .serverError
        serviceMock.fetchInstallmentListExpectedResult = .failure(error)
        
        sut.fetchInstallmentList()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callStopLoadingCount, 1)
        XCTAssertEqual(spy.callDisplayGenericErrorCount, 1)
    }
    
    func testFetchInstallmentList_WhenReceiveFailureFromServerNoConnection_ShouldPresentNoConnectionError() {
        let sut = self.sut(for: .borrower)
        let error: ApiError = .connectionFailure
        serviceMock.fetchInstallmentListExpectedResult = .failure(error)
        
        sut.fetchInstallmentList()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callStopLoadingCount, 1)
        XCTAssertEqual(spy.callDisplayNoConnectionErrorCount, 1)
    }
    
    func testSelectInstallment_WhenCalledFromController_ShouldCallDidNextStepInstallmentDetail() {
        let sut = self.sut(for: .borrower)
        let expectedEvent = LendingInstallmentListEvent.currentInstallment.event()
        let expectedResponse = InstallmentListResponse.mock
        serviceMock.fetchInstallmentListExpectedResult = .success(expectedResponse)
        
        sut.fetchInstallmentList()
        sut.didSelectInstallment(at: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(spy.action, LendingInstallmentListAction.installmentDetail(offerIdentifier: identifier,
                                                                                  installmentItem: expectedResponse.inProgress[0]))
    }
    
    func testSelectAntecipatedInstallment_WhenCalledFromController_ShouldCallInstallmentAntecipationPopup() {
        let sut = self.sut(for: .borrower)
        let expectedEvent = LendingInstallmentListEvent.antecipationInstallment.event()
        let installmentItemMock = InstallmentItem(number: 1,
                                                  value: 100.0,
                                                  dueDate: Date(),
                                                  payable: false,
                                                  status: .active)
        let expectedResponse = InstallmentListResponse(outstandingBalance: 1000.0, inProgress: [installmentItemMock], finished: [])
        serviceMock.fetchInstallmentListExpectedResult = .success(expectedResponse)
        
        sut.fetchInstallmentList()
        sut.didSelectInstallment(at: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(spy.callPresentInstallmentAntecipationPopupCount, 1)
        XCTAssertEqual(spy.installmentListResponse?.inProgress[0], installmentItemMock)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testSelectUnpayableInstallment_WhenCalledFromController_ShouldCallInstallmentUnpayablePopup() {
        let sut = self.sut(for: .borrower)
        let expectedEvent = LendingInstallmentListEvent.delayedInstallment(agent: .borrower).event()
        let installmentItemMock = InstallmentItem(number: 1,
                                                  value: 100.0,
                                                  dueDate: Date(),
                                                  payable: false,
                                                  status: .delayed)
        let expectedResponse = InstallmentListResponse(outstandingBalance: 1000.0, inProgress: [installmentItemMock], finished: [])
        serviceMock.fetchInstallmentListExpectedResult = .success(expectedResponse)
        
        sut.fetchInstallmentList()
        sut.didSelectInstallment(at: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(spy.callPresentUnpayableInstallmentPopupCount, 1)
        XCTAssertEqual(spy.installmentListResponse?.inProgress[0], installmentItemMock)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testDidNextStepShowFAQ_WhenCalledFromController_ShouldCallDidNextStepShowFAQ() throws {
        let sut = self.sut(for: .borrower)
        let expectedUrl = try XCTUnwrap(URL(string: "picpay://picpay/helpcenter/section/360011533772"))
        let expectedEvent = LendingInstallmentListEvent.faq(agent: .borrower).event()
        sut.showFAQ()
        
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(spy.action, LendingInstallmentListAction.showFAQ(url: expectedUrl))
    }
    
    func testDidNextStepSelectInstallment_WhenCalledFromBorrowerController_ShouldCallDidNextStepShowInstallmentDetails() {
        let sut = self.sut(for: .borrower)
        let expectedEvent = LendingInstallmentListEvent.currentInstallment.event()
        let expectedResponse = InstallmentListResponse(outstandingBalance: 1000.0, inProgress: [InstallmentItem.mock()], finished: [])
        serviceMock.fetchInstallmentListExpectedResult = .success(expectedResponse)
        
        sut.fetchInstallmentList()
        sut.didSelectInstallment(at: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        XCTAssertEqual(spy.installmentListResponse, expectedResponse)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testDidNextStepSelectInstallment_WhenCalledFromInvestorController_ShouldCallDidNextStepPresentPendingPaymentFromBorrower() throws {
        let sut = self.sut(for: .investor)
        let faqUrl = try XCTUnwrap(URL(string: "picpay://picpay/helpcenter/article/360049387492"))
        let expectedEvent = LendingInstallmentListEvent.delayedInstallment(agent: .investor).event()
        let expectedResponse = InstallmentListResponse(outstandingBalance: 1000.0, inProgress: [InstallmentItem.mock(for: .delayed)], finished: [])
       
        serviceMock.fetchInstallmentListExpectedResult = .success(expectedResponse)
        
        sut.fetchInstallmentList()
        sut.didSelectInstallment(at: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(spy.callPresentPendingPaymentFromBorrower, 1)
        XCTAssertEqual(spy.installmentListResponse, expectedResponse)
        XCTAssertEqual(spy.faqUrl, faqUrl)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testOpenCustomerSupport_WhenCalledFromController_ShouldCallDidNextStepCustomerSupport() throws {
        let sut = self.sut(for: .borrower)
        let customerSupportURL = try XCTUnwrap(URL(string: "picpay://picpay/helpcenter/reason/clientepf__picpay_card"))
        let expectedEvent = LendingInstallmentListEvent.costumerSupport.event()
        
        sut.openCustomerSupport()
        
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(spy.action, .customerSupport(url: customerSupportURL))
    }
    
    func testSendTrackingEvent_WhenCalledFromController_ShouldSendTrackingEvent() {
        let sut = self.sut(for: .borrower)
        let expectedEvent = LendingInstallmentListEvent.openInstallmentsTab(agent: .borrower).event()
        
        sut.sendTrackingEvent(for: 0)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
}
