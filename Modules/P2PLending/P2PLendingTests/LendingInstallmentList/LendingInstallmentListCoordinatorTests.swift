import XCTest
import UI
@testable import P2PLending

final class LendingInstallmentListCoordinatorTests: XCTestCase {
    private lazy var spy = ViewControllerSpy()
    private lazy var urlOpener = URLOpenerSpy()
    private lazy var sut: LendingInstallmentListCoordinator = {
        let coordinator = LendingInstallmentListCoordinator(dependencies: DependencyContainerMock(urlOpener))
        coordinator.viewController = spy
        return coordinator
    }()
    
    func testPerformActionShowFAQ_WhenCalledFromPresenter_ShouldPresentFAQViewController() throws {
        let url = try XCTUnwrap(URL(string: "https://www.picpay.com"))
        
        sut.perform(action: .showFAQ(url: url))
        
        XCTAssertEqual(urlOpener.callCanOpenURLCount, 1)
        XCTAssertEqual(urlOpener.callOpenURLCount, 1)
        XCTAssertEqual(urlOpener.checkedURL, url)
        XCTAssertEqual(urlOpener.openedURL, url)
    }
    
    func testPerformActionCustomerSupport_WhenCalledFromPresenter_ShouldOpenURL() throws {
        let customerSupportURL = try XCTUnwrap(URL(string: "picpay://picpay/helpcenter/reason/clientepf__picpay_card"))
        
        sut.perform(action: .customerSupport(url: customerSupportURL))
        
        XCTAssertEqual(urlOpener.callCanOpenURLCount, 1)
        XCTAssertEqual(urlOpener.callOpenURLCount, 1)
        XCTAssertEqual(urlOpener.checkedURL, customerSupportURL)
        XCTAssertEqual(urlOpener.openedURL, customerSupportURL)
    }
    
    func testPerformActionPendingPayment_WhenCalledFromPresenter_ShouldShowPendingPaymentApolloFeedbackController() throws {
        let faqUrl = try XCTUnwrap(URL(string: "picpay://picpay/helpcenter/article/360049387492"))
        
        sut.perform(action: .pendingPaymentFromBorrower(content: PendingPaymentApolloViewContentMock().expectedContent,
                                                        url: faqUrl))
        
        XCTAssertEqual(spy.callShowViewControllerCount, 1)
        XCTAssertTrue(spy.viewController is ApolloFeedbackViewController)
    }
    
    func testPerformActionInstallmentDetail_WhenCalledFromPresenter_ShouldPresentIntallmentDetailController() throws {
        let offerIdentifier = try XCTUnwrap(UUID())
        
        sut.perform(action: .installmentDetail(offerIdentifier: offerIdentifier, installmentItem: InstallmentItem.mock(for: .active)))
        
        XCTAssertEqual(spy.callPresentViewControllerCount, 1)
        
        let navigationController = try XCTUnwrap(spy.viewController as? UINavigationController)
        XCTAssertTrue(navigationController.viewControllers.first is LendingInstallmentDetailViewController)
    }
    
}
