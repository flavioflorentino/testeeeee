import Core
import UI
import UIKit
import XCTest
import AssetsKit
@testable import P2PLending

private final class LendingInstallmentListDisplayingSpy: LendingInstallmentListDisplaying {
    private(set) var callDisplayHeadline = 0
    private(set) var callDisplayOpenInstallmentListCount = 0
    private(set) var callDisplayPaidInstallmentListCount = 0
    private(set) var callDisplayOpenInstallmentListEmptyStateCount = 0
    private(set) var callDisplayPaidInstallmentListEmptyStateCount = 0
    private(set) var callHideOpenInstallmentListEmptyStateCount = 0
    private(set) var callHidePaidInstallmentListEmptyStateCount = 0
    private(set) var callDisplayInstallmentAntecipationPopupCount = 0
    private(set) var callDisplayUnpayableInstallmentPopupCount = 0
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    private(set) var callDisplayErrorAlertCount = 0
    
    private(set) var installmentListViewModel: InstallmentListViewModel?
    private(set) var installmentItemList: [InstallmentItemViewModel]?
    private(set) var emptyStateViewTitle: String?
    private(set) var errorTitle: String?
    private(set) var errorMessage: String?
    private(set) var errorImage: UIImage?
    private(set) var installmentAntecipationPopupTitle: String?
    private(set) var installmentAntecipationPopupDescription: NSMutableAttributedString?
    private(set) var installmentAntecipationPopupImage: UIImage?
    private(set) var installmentUnpayablePopupTitle: String?
    private(set) var installmentUnpayablePopupDescription: NSMutableAttributedString?
    private(set) var installmentUnpayablePopupImage: UIImage?
    
    func displayHeadline(installmentListViewModel: InstallmentListViewModel) {
        callDisplayHeadline += 1
        self.installmentListViewModel = installmentListViewModel
    }
    
    func displayOpenInstallmentList(_ installmentItemList: [InstallmentItemViewModel]) {
        callDisplayOpenInstallmentListCount += 1
        self.installmentItemList = installmentItemList
    }
    
    func displayPaidnstallmentList(_ installmentItemList: [InstallmentItemViewModel]) {
        callDisplayPaidInstallmentListCount += 1
        self.installmentItemList = installmentItemList
    }
    
    func displayOpenInstallmentListEmptyState(title: String) {
        callDisplayOpenInstallmentListEmptyStateCount += 1
        self.emptyStateViewTitle = title
    }
    
    func displayPaidInstallmentListEmptyState(title: String) {
        callDisplayPaidInstallmentListEmptyStateCount += 1
        self.emptyStateViewTitle = title
    }
    
    func hideOpenInstallmentListEmptyState() {
        callHideOpenInstallmentListEmptyStateCount += 1
    }
    
    func hidePaidInstallmentListEmptyState() {
        callHidePaidInstallmentListEmptyStateCount += 1
    }
    
    func displayInstallmentAntecipationPopup(image: UIImage, title: String, description: NSMutableAttributedString) {
        callDisplayInstallmentAntecipationPopupCount += 1
        self.installmentAntecipationPopupTitle = title
        self.installmentAntecipationPopupDescription = description
        self.installmentAntecipationPopupImage = image
    }
    
    func displayUnpayableInstallmentPopup(image: UIImage, title: String, description: NSMutableAttributedString) {
        callDisplayUnpayableInstallmentPopupCount += 1
        self.installmentUnpayablePopupTitle = title
        self.installmentUnpayablePopupDescription = description
        self.installmentUnpayablePopupImage = image
    }
    
    func startLoading() {
        callStartLoadingCount += 1
    }
    
    func stopLoading() {
        callStopLoadingCount += 1
    }
    
    func displayErrorAlert(title: String, message: String, image: UIImage) {
        callDisplayErrorAlertCount += 1
        self.errorTitle = title
        self.errorMessage = message
        self.errorImage = image
    }
}

private final class LendingInstallmentListCoordinatorSpy: LendingInstallmentListCoordinating {
    var viewController: UIViewController?
    
    private(set) var callPerformActionCount = 0
    
    private(set) var action: LendingInstallmentListAction?
    
    func perform(action: LendingInstallmentListAction) {
        callPerformActionCount += 1
        self.action = action
    }
}

final class LendingInstallmentListPresenterTests: XCTestCase {
    private typealias Localizable = Strings.Lending.Installment.List
    
    private lazy var coordinatorSpy = LendingInstallmentListCoordinatorSpy()
    private lazy var spy = LendingInstallmentListDisplayingSpy()
    private lazy var sut: LendingInstallmentListPresenter = {
        let presenter = LendingInstallmentListPresenter(coordinator: coordinatorSpy, agent: .borrower)
        presenter.viewController = spy
        return presenter
    }()
    
    func testDisplayHeadline_WhenCalledFromInteractor_ShouldDisplayHeadline() {
        let response = InstallmentListResponse.mock
        let responseViewModel = InstallmentListViewModel(installmentList: response, agent: .borrower)
        
        sut.displayHeadline(with: response)
        
        XCTAssertEqual(spy.callDisplayHeadline, 1)
        XCTAssertEqual(spy.installmentListViewModel, responseViewModel)
    }
    
    func testDisplayOpenInstallmentList_WhenCalledFromInteractor_ShouldDisplayOpenInstallmentList() {
        let expectedInstallmentList = [InstallmentItem.mock(), InstallmentItem.mock(), InstallmentItem.mock()]
        let expectedItems = expectedInstallmentList.map { InstallmentItemViewModel(item: $0, agent: .borrower) }
        
        sut.displayOpenInstallmentList(expectedInstallmentList)
        
        XCTAssertEqual(spy.callDisplayOpenInstallmentListCount, 1)
        XCTAssertEqual(spy.installmentItemList, expectedItems)
    }
    
    func testDisplayPaidInstallmentList_WhenCalledFromInteractor_ShouldDisplayPaidInstallmentList() {
        let expectedInstallmentList = [InstallmentItem.mock(), InstallmentItem.mock(), InstallmentItem.mock()]
        
        let expectedItems = expectedInstallmentList.map { InstallmentItemViewModel(item: $0, agent: .borrower) }
        
        sut.displayPaidInstallmentList(expectedInstallmentList)
        
        XCTAssertEqual(spy.callDisplayPaidInstallmentListCount, 1)
        XCTAssertEqual(spy.installmentItemList, expectedItems)
    }
    
    func testDisplayOpenInstallmentsEmptyState_WhenCalledFromInteractor_ShouldDisplayOpenInstallmentsEmptyState() {
        let expectedInstallmentList: [InstallmentItem] = []
        let expectedTitle = Localizable.openInstallmentEmptyListTitle
        
        sut.shouldDisplayOpenInstallmentsEmptyState(expectedInstallmentList.isEmpty)
        
        XCTAssertEqual(spy.callDisplayOpenInstallmentListEmptyStateCount, 1)
        XCTAssertEqual(spy.emptyStateViewTitle, expectedTitle)
    }
    
    func testDisplayPaidInstallmentsEmptyState_WhenCalledFromInteractor_ShouldDisplayPaidInstallmentsEmptyState() {
        let expectedInstallmentList: [InstallmentItem] = []
        let expectedTitle = Localizable.paidInstallmentEmptyListTitle
        
        sut.shouldDisplayPaidInstallmentsEmptyState(expectedInstallmentList.isEmpty)
        
        XCTAssertEqual(spy.callDisplayPaidInstallmentListEmptyStateCount, 1)
        XCTAssertEqual(spy.emptyStateViewTitle, expectedTitle)
    }
    
    func testPresentUnpayableInstallmentPopup_WhenCalledFromInteractor_ShouldDisplayUnpayableInstallmentPopup() {
        sut.presentUnpayableInstallmentPopup()
        
        XCTAssertEqual(spy.callDisplayUnpayableInstallmentPopupCount, 1)
        XCTAssertEqual(spy.installmentUnpayablePopupTitle, Localizable.unpayablePopupTitle)
        XCTAssertEqual(spy.installmentUnpayablePopupDescription?.string, Localizable.unpayablePopupDescription)
        XCTAssertEqual(spy.installmentUnpayablePopupImage, Resources.Illustrations.iluWarning.image)
    }
    
    func testPresentInstallmentAntecipationPopup_WhenCalledFromInteractor_ShouldPresentInstallmentAntecipationPopup() {
        sut.presentInstallmentAntecipationPopup()
        
        XCTAssertEqual(spy.callDisplayInstallmentAntecipationPopupCount, 1)
        XCTAssertEqual(spy.installmentAntecipationPopupTitle, Localizable.antecipationPopupTitle)
        XCTAssertEqual(spy.installmentAntecipationPopupDescription?.string, Localizable.antecipationPopupDescription)
        XCTAssertEqual(spy.installmentAntecipationPopupImage, Resources.Illustrations.iluConstruction.image)
    }
    
    func testPresentPendingPaymentFromBorrower_WhenCalledFromInteractor_ShouldPerformDidNextStepPendingPaymentFromBorrower() throws {
        let faqUrl = try XCTUnwrap(URL(string: "picpay://picpay/helpcenter/article/360049387492"))
        
        sut.presentPendingPaymentFromBorrower(faqUrl: faqUrl)
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        XCTAssertEqual(coordinatorSpy.action,
                       .pendingPaymentFromBorrower(content: PendingPaymentApolloViewContentMock().expectedContent, url: faqUrl))
    }
    
    func testStartLoading_WhenCalledFromInteractor_ShouldStartLoading() {
        sut.startLoading()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
    }
    
    func testStopLoading_WhenCalledFromInteractor_ShouldStopLoading() {
        sut.stopLoading()
        
        XCTAssertEqual(spy.callStopLoadingCount, 1)
    }
    
    func testDisplayGenericErrorAlert_WhenCalledFromInteractor_ShouldDisplayGenericError() {
        let expectedErrorTitle = GlobalLocalizable.errorAlertTitle
        let expectedErrorBody = GlobalLocalizable.genericErrorBody
        let expectedErrorImage = Resources.Illustrations.iluError.image
        
        sut.displayGenericError()
        
        XCTAssertEqual(spy.callDisplayErrorAlertCount, 1)
        XCTAssertEqual(spy.errorMessage, expectedErrorBody)
        XCTAssertEqual(spy.errorTitle, expectedErrorTitle)
        XCTAssertEqual(spy.errorImage, expectedErrorImage)
    }
    
    func testDisplayNoConnectionErrorAlert_WhenCalledFromInteractor_ShouldDisplayNoConnectionError() {
        let expectedErrorTitle = GlobalLocalizable.noConnectionErrorTitle
        let expectedErrorBody = GlobalLocalizable.noConnectionErrorBody
        let expectedErrorImage = Resources.Illustrations.iluNoConnectionGreenBackground.image
        
        sut.displayNoConnectionError()
        
        XCTAssertEqual(spy.callDisplayErrorAlertCount, 1)
        XCTAssertEqual(spy.errorMessage, expectedErrorBody)
        XCTAssertEqual(spy.errorTitle, expectedErrorTitle)
        XCTAssertEqual(spy.errorImage, expectedErrorImage)
    }
    
    func testDidNextStepShowFAQ_WhenCalledFromPresenter_ShouldPerformNextActionShowFAQ() throws {
        let expectedURL = try XCTUnwrap(URL(string: "https://www.picpay.com"))
            
        sut.didNextStep(action: .showFAQ(url: expectedURL))
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .showFAQ(url: expectedURL))
    }
    
    func testDidNextStepCustomerSupport_WhenCalledFromInteractor_ShouldPerformNextActionCustomerSupport() throws {
        let customerSupportURL = try XCTUnwrap(URL(string: "picpay://picpay/helpcenter/reason/clientepf__picpay_card"))
        
        sut.didNextStep(action: .customerSupport(url: customerSupportURL))
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .customerSupport(url: customerSupportURL))
    }
}
