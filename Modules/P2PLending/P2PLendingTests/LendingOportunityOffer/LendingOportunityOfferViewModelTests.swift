import Core
import XCTest
import AnalyticsModule
@testable import P2PLending

private final class LendingOpportunityOfferPresenterSpy: LendingOpportunityOfferPresenting {
    var viewController: LendingOpportunityOfferDisplay?
    
    private(set) var callPresentOfferCount = 0
    private(set) var callPresentErrorCount = 0
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    private(set) var callDidNextStepCount = 0
    private(set) var callDeclineConfirmationDialogueCount = 0
    private(set) var callPresentContractCount = 0
    
    private(set) var offer: OpportunityOffer?
    private(set) var error: Error?
    private(set) var identifiableError: IdentifiableOpportunittyOfferError?
    private(set) var action: LendingOpportunityOfferAction?
    
    func present(offer: OpportunityOffer) {
        callPresentOfferCount += 1
        self.offer = offer
    }
    
    func present(error: Error) {
        callPresentErrorCount += 1
        self.error = error
    }
    
    func showDeclineConfirmationDialogue() {
        callDeclineConfirmationDialogueCount += 1
    }
    
    func startLoading() {
        callStartLoadingCount += 1
    }
    
    func stopLoading() {
        callStopLoadingCount += 1
    }
    
    func presentContract(for url: URL) {
        callPresentContractCount += 1
    }
    
    func didNextStep(action: LendingOpportunityOfferAction) {
        switch action {
        case let .detailedError(error), let .insufficientFunds(error):
            identifiableError = error
        default:
            break
        }
        callDidNextStepCount += 1
        self.action = action
    }
}

private final class LendingOpportunityOfferServiceMock: LendingOpportunityOfferServicing {
    func fetchContract(identifier: UUID, agent: ConsultingAgent, completion: @escaping ModelCompletionBlock<NoContent>) {
        
    }
    
    var fetchOfferExpectedResult: Result<OpportunityOffer, ApiError>?
    var declineOfferExpectedResult: Result<NoContent, ApiError>?
    var acceptOfferExpectedResult: Result<NoContent, ApiError>?
    
    func fetchOffer(identifier: UUID, completion: @escaping ModelCompletionBlock<OpportunityOffer>) {
        callCompletionIfNeeded(completion, expectedResult: fetchOfferExpectedResult)
    }
    
    func declineOffer(identifier: UUID, completion: @escaping ModelCompletionBlock<NoContent>) {
        callCompletionIfNeeded(completion, expectedResult: declineOfferExpectedResult)
    }
    
    func acceptOffer(identifier: UUID, completion: @escaping ModelCompletionBlock<NoContent>) {
        callCompletionIfNeeded(completion, expectedResult: acceptOfferExpectedResult)
    }
}

final class LendingOpportunityOfferViewModelTests: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    private lazy var spy = LendingOpportunityOfferPresenterSpy()
    private lazy var serviceMock = LendingOpportunityOfferServiceMock()
    private lazy var dependencyMock = DependencyContainerMock(analyticsSpy)
    private lazy var sut = LendingOpportunityOfferViewModel(
        service: serviceMock,
        presenter: spy,
        offerIdentifier: UUID(),
        dependencies: dependencyMock
    )
    
    func testFetchOffer_WhenReceiveSuccessFromServer_ShouldPresentOffer() throws {
        let offer: OpportunityOffer = try .mock()
        serviceMock.fetchOfferExpectedResult = .success(offer)
        
        sut.fetchOffer()
        
        XCTAssertEqual(spy.callPresentOfferCount, 1)
        XCTAssertEqual(spy.offer, offer)
    }
    
    func testFetchOffer_WhenReceiveFailureFromServer_ShouldPresentError() {
        let error: ApiError = .serverError
        serviceMock.fetchOfferExpectedResult = .failure(error)
        
        sut.fetchOffer()
        
        XCTAssertEqual(spy.callPresentErrorCount, 1)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
    }
    
    func testAcceptOffer_WhenReceiveSuccessFromServer_ShouldCallNextStepAcceptOffer() throws {
        let expectedEvent = LendingOpportunityOfferEvent.continueTapped.event()
        let noContent = NoContent()
        let expectedOffer: OpportunityOffer = try .mock(for: .waitingReply)
        serviceMock.fetchOfferExpectedResult = .success(expectedOffer)
        serviceMock.acceptOfferExpectedResult = .success(noContent)
        
        sut.fetchOffer()
        sut.acceptOffer()
        
        XCTAssertEqual(spy.callStartLoadingCount, 2)
        XCTAssertEqual(spy.callStopLoadingCount, 2)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        guard case let .makeTransaction(offer) = spy.action else {
            XCTFail("wrong action called")
            return
        }
        XCTAssertEqual(expectedOffer, offer)
    }
    
    func testAcceptOffer_WhenReceiveFailureFromServer_ShouldpresentError() throws {
        let error: ApiError = .serverError
        serviceMock.fetchOfferExpectedResult = try .success(.mock(for: .waitingReply))
        serviceMock.acceptOfferExpectedResult = .failure(error)
        
        sut.fetchOffer()
        sut.acceptOffer()
        
        XCTAssertEqual(spy.callPresentErrorCount, 1)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
    }
    
    func testAcceptOffer_WhenReceiveFailureWithDetailedError_ShouldCallDidNextStepDetailedError() throws {
        let identifiableError: IdentifiableOpportunittyOfferError = .currentlyProposal
        var requestError = RequestError()
        requestError.code = identifiableError.rawValue
        serviceMock.fetchOfferExpectedResult = try .success(.mock(for: .waitingReply))
        serviceMock.acceptOfferExpectedResult = .failure(.badRequest(body: requestError))
        
        sut.fetchOffer()
        sut.acceptOffer()
        
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        XCTAssertEqual(spy.identifiableError, identifiableError)
    }
    
    func testAcceptOffer_WhenReceiveFailureWithInsufficientFunds_ShouldCallDidNextStepInsufficientFunds() throws {
        let identifiableError: IdentifiableOpportunittyOfferError = .insufficientFunds
        var requestError = RequestError()
        requestError.code = identifiableError.rawValue
        serviceMock.fetchOfferExpectedResult = try .success(.mock(for: .waitingReply))
        serviceMock.acceptOfferExpectedResult = .failure(.badRequest(body: requestError))
        
        sut.fetchOffer()
        sut.acceptOffer()
        
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        XCTAssertEqual(spy.identifiableError, identifiableError)
    }
    
    func testDeclineOffer_WhenReceiveSuccessFromServer_ShouldCallNextStepDeclineOffer() {
        let expectedEvent = LendingOpportunityOfferEvent.investorDidDecline.event()
        let noContent = NoContent()
        serviceMock.declineOfferExpectedResult = .success(noContent)
        
        sut.declineOffer()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callStopLoadingCount, 1)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        guard case .declineOffer = spy.action else {
            XCTFail("wrong action called")
            return
        }
    }
    
    func testDeclineOffer_WhenReceiveFailureFromServer_ShouldPresentError() {
        let error: ApiError = .serverError
        serviceMock.declineOfferExpectedResult = .failure(error)
        
        sut.declineOffer()
        
        XCTAssertEqual(spy.callPresentErrorCount, 1)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
    }
    
    func testRequestDeclineConfirmation_WhenCalledFromController_ShouldShowDeclineConfirmationDialogue() {
        let expectedEvent = LendingOpportunityOfferEvent.declineTapped.event()
        sut.requestDeclineConfirmation()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(spy.callDeclineConfirmationDialogueCount, 1)
    }
    
    func testShowContract_WhenCalledFromController_ShouldCallDidNextStepContract() throws {
        let expectedEvent = LendingOpportunityOfferEvent.CVTapped.event()
        let expectedOffer: OpportunityOffer = try .mock(for: .waitingReply)
        serviceMock.fetchOfferExpectedResult = .success(expectedOffer)
        
        sut.fetchOffer()
        sut.showContract()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testClose_WhenCalledFromController_ShouldCallDidNextStepClose() {
        let expectedEvent = LendingOpportunityOfferEvent.closeTapped.event()
        sut.close(shouldSendEventTracking: true)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        guard case .close = spy.action else {
            XCTFail("wrong action called")
            return
        }
    }
    
    func testSendAnalyticsEvent_WhenCalledFromController_ShouldSendEvent() {
        let expectedEvent = LendingOpportunityOfferEvent.laterButtonTapped.event()
        sut.sendAnalyticsEvent(type: .dismissed)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testShowFaq_WhenCalledFromController_ShouldCallDidNextStepShowFaq() {
        let expectedEvent = LendingOpportunityOfferEvent.helpTapped.event()
        sut.showFaq()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        guard case .showFAQ = spy.action else {
            XCTFail("wrong action called")
            return
        }
    }
    
    func testLendingSuccessControllerDidDismiss_WhenCalledFromLendingSuccessDelegate_ShouldSendEvent() {
        let expectedEvent = LendingSuccessEvent.didDeclineOffer.event()
        let fakeViewController = UIViewController()
        sut.lendingSuccessControllerDidDismiss(fakeViewController)
       
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testInstallmentsTapped_WhenCalledFromController_ShouldCallDidNextStepInstallmentList() throws {
        let expectedOffer: OpportunityOffer = try .mock(for: .active)
        let expectedEvent = LendingOpportunityOfferEvent.installmentTapped.event()
        serviceMock.fetchOfferExpectedResult = .success(expectedOffer)
        
        sut.fetchOffer()
        sut.installmentsTapped()
        
        guard case .installmentList(let offerId) = spy.action else {
            XCTFail("wrong action called")
            return
        }
        
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        XCTAssertEqual(offerId, expectedOffer.id)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
}
