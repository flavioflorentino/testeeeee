import Core
import XCTest
import FeatureFlag
@testable import P2PLending

private final class LendingOpportunityOfferDisplaySpy: LendingOpportunityOfferDisplay {
    private(set) var callDisplayOfferCount = 0
    private(set) var callDisplayErrorCount = 0
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    private(set) var callDisplayDeclineConfirmationCount = 0
    
    private(set) var offer: OpportunityOfferDisplayModel?
    private(set) var error: Error?
    private(set) var title: String?
    private(set) var body: String?
    
    func display(offer: OpportunityOfferDisplayModel) {
        callDisplayOfferCount += 1
        self.offer = offer
    }
    
    func display(error: Error) {
        callDisplayErrorCount += 1
        self.error = error
    }
    
    func displayDeclineConfirmation(title: String, body: String) {
        callDisplayDeclineConfirmationCount += 1
        self.title = title
        self.body = body
    }
    
    func startLoading() {
        callStartLoadingCount += 1
    }
    
    func stopLoading() {
        callStopLoadingCount += 1
    }
}

private final class LendingOpportunityOfferCoordinatorSpy: LendingOpportunityOfferCoordinating {
    var viewController: UIViewController?
    
    private(set) var callPerformActionCount = 0
    private(set) var action: LendingOpportunityOfferAction?
    
    func perform(action: LendingOpportunityOfferAction) {
        callPerformActionCount += 1
        self.action = action
    }
}

final class LendingOpportunityOfferPresenterTests: XCTestCase {
    private typealias Localizable = Strings.Lending.Opportunity.Offer
    private lazy var spy = LendingOpportunityOfferDisplaySpy()
    private lazy var coordinatorSpy = LendingOpportunityOfferCoordinatorSpy()
    
    private lazy var featureManagerMock: FeatureManagerMock = {
        let featureManagerMock = FeatureManagerMock()
        featureManagerMock.override(key: .isP2PLendingInvestorInstallmentsAvailable, with: false)
        return featureManagerMock
    }()
    private lazy var sut: LendingOpportunityOfferPresenter = {
        let container = DependencyContainerMock(featureManagerMock)
        let presenter = LendingOpportunityOfferPresenter(coordinator: coordinatorSpy, dependencies: container)
        presenter.viewController = spy
        return presenter
    }()
    
    func testPresentOffer_WhenStatusIsWaitingReply_ShouldDisplayOfferFormattedForWaitingReply() throws {
        featureManagerMock.override(key: .isP2PLendingInvestorInstallmentsAvailable, with: false)
        let status: OpportunityOfferStatus = .waitingReply
        
        sut.present(offer: try .mock(for: status))
        
        XCTAssertEqual(spy.callDisplayOfferCount, 1)
        XCTAssertEqual(spy.offer, .mock(for: status))
    }
    
    func testPresentOffer_WhenStatusIsAccepted_ShouldDisplayOfferFormattedForAccepted() throws {
        featureManagerMock.override(key: .isP2PLendingInvestorInstallmentsAvailable, with: true)
        let status: OpportunityOfferStatus = .accepted
        sut.present(offer: try .mock(for: status))
        
        XCTAssertEqual(spy.callDisplayOfferCount, 1)
        XCTAssertEqual(spy.offer, .mock(for: status))
    }
    
    func testPresentOffer_WhenStatusIsProcessingPayment_ShouldDisplayOfferFormattedForProcessingPayment() throws {
        featureManagerMock.override(key: .isP2PLendingInvestorInstallmentsAvailable, with: false)
        let status: OpportunityOfferStatus = .processingPayment
        sut.present(offer: try .mock(for: status))
        
        XCTAssertEqual(spy.callDisplayOfferCount, 1)
        XCTAssertEqual(spy.offer, .mock(for: status))
    }
    
    func testPresentOffer_WhenStatusIsActiveAndFeatureFlagForInstallmentIsOn_ShouldDisplayOfferFormattedForActive() throws {
        featureManagerMock.override(key: .isP2PLendingInvestorInstallmentsAvailable, with: true)
        let status: OpportunityOfferStatus = .active

        sut.present(offer: try .mock(for: status))
        
        XCTAssertEqual(spy.callDisplayOfferCount, 1)
        XCTAssertEqual(spy.offer, OpportunityOfferDisplayModel.mock(for: status, isInstallmentsAvailable: true))
    }
    
    func testPresentOffer_WhenStatusIsActiveAndFeatureFlagForInstallmentIsOff_ShouldDisplayOfferFormattedForActive() throws {
        featureManagerMock.override(key: .isP2PLendingInvestorInstallmentsAvailable, with: false)
        let status: OpportunityOfferStatus = .active
        
        sut.present(offer: try .mock(for: status))
        
        XCTAssertEqual(spy.callDisplayOfferCount, 1)
        XCTAssertEqual(spy.offer, .mock(for: status))
    }
    
    func testPresentOffer_WhenStatusIsCounterOffer_ShouldDisplayOfferFormattedForCounterOffer() throws {
        featureManagerMock.override(key: .isP2PLendingInvestorInstallmentsAvailable, with: false)
        let status: OpportunityOfferStatus = .counterOffer
        
        sut.present(offer: try .mock(for: status))
        
        XCTAssertEqual(spy.callDisplayOfferCount, 1)
        XCTAssertEqual(spy.offer, .mock(for: status))
    }
    
    func testPresentOffer_WhenStatusIsLiquidated_ShouldDisplayOfferFormattedForLiquidated() throws {
        featureManagerMock.override(key: .isP2PLendingInvestorInstallmentsAvailable, with: false)
        let status: OpportunityOfferStatus = .liquidated
        
        sut.present(offer: try .mock(for: status))
        
        XCTAssertEqual(spy.callDisplayOfferCount, 1)
        XCTAssertEqual(spy.offer, .mock(for: status))
    }
    
    func testPresentOffer_WhenStatusIsDeclined_ShouldDisplayOfferFormattedForDeclined() throws {
        featureManagerMock.override(key: .isP2PLendingInvestorInstallmentsAvailable, with: false)
        let status: OpportunityOfferStatus = .declined
        
        sut.present(offer: try .mock(for: status))
        
        XCTAssertEqual(spy.callDisplayOfferCount, 1)
        XCTAssertEqual(spy.offer, .mock(for: status))
    }
    
    func testPresentOffer_WhenStatusIsExpired_ShouldDisplayOfferFormattedForExpired() throws {
        featureManagerMock.override(key: .isP2PLendingInvestorInstallmentsAvailable, with: false)
        let status: OpportunityOfferStatus = .expired
        
        sut.present(offer: try .mock(for: status))
        
        XCTAssertEqual(spy.callDisplayOfferCount, 1)
        XCTAssertEqual(spy.offer, .mock(for: status))
    }
    
    func testPresentOffer_WhenStatusIsCanceled_ShouldDisplayOfferFormattedForCanceled() throws {
        featureManagerMock.override(key: .isP2PLendingInvestorInstallmentsAvailable, with: false)
        let status: OpportunityOfferStatus = .canceled
        
        sut.present(offer: try .mock(for: status))
        
        XCTAssertEqual(spy.callDisplayOfferCount, 1)
        XCTAssertEqual(spy.offer, .mock(for: status))
    }
    
    func testPresentError_WhenCalledFromViewModel_ShouldDisplayError() {
        let error: ApiError = .serverError
        sut.present(error: error)
        
        XCTAssertEqual(spy.callDisplayErrorCount, 1)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
    }
    
    func testDidNextStepDetailedError_WhenCalledFromViewModel_ShouldPerformNextStepDetailedError() {
        let identifiableError: IdentifiableOpportunittyOfferError = .activeInvestorLimitReached
        
        sut.didNextStep(action: .detailedError(identifiableError))
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        guard case let .detailedError(detailedError) = coordinatorSpy.action else {
            XCTFail("wrong action called")
            return
        }
        XCTAssertEqual(detailedError.asDetailedError, identifiableError.asDetailedError)
    }
    
    func testDidNextStepDetailedError_WhenCalledFromViewModel_ShouldPerformNextStepInsufficientFunds() {
        let identifiableError: IdentifiableOpportunittyOfferError = .insufficientFunds
        
        sut.didNextStep(action: .insufficientFunds(identifiableError))
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        guard case let .insufficientFunds(detailedError) = coordinatorSpy.action else {
            XCTFail("wrong action called")
            return
        }
        XCTAssertEqual(detailedError.asDetailedError, identifiableError.asDetailedError)
    }
    
    func testPresentDecline_WhenCalledFromViewModel_ShouldDispalyDeclineConfirmation() {
        sut.showDeclineConfirmationDialogue()
       
        XCTAssertEqual(spy.callDisplayDeclineConfirmationCount, 1)
        XCTAssertEqual(spy.title, Localizable.declineOfferTitle)
        XCTAssertEqual(spy.body, Localizable.declineOfferBody)
    }
    
    func testStartLoading_WhenCalledFromViewModel_ShouldStartLoading() {
        sut.startLoading()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
    }
    
    func testStopLoading_WhenCalledFromViewModel_ShouldStopLoading() {
        sut.stopLoading()
        
        XCTAssertEqual(spy.callStopLoadingCount, 1)
    }
    
    func testDidNextStepContract_WhenCalledFromViewModel_ShouldPerformNextStepContract() throws {
        let expectedURL = try XCTUnwrap(URL(string: "https://www.picpay.com"))
        let webPage = LendingSessionWebPage(title: Strings.Lending.Opportunity.Offer.cv, url: expectedURL)
        
        sut.didNextStep(action: .contract(webPage: webPage))
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        guard case .contract = coordinatorSpy.action else {
            XCTFail("wrong action called")
            return
        }
    }
    
    func testDidNextStepAcceptOffer_WhenCalledFromViewModel_ShouldPerformNextStepAcceptOffer() throws {
        let expectedOffer: OpportunityOffer = try .mock(for: .waitingReply)
        sut.didNextStep(action: .makeTransaction(offer: expectedOffer))
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        guard case let .makeTransaction(offer) = coordinatorSpy.action else {
            XCTFail("wrong action called")
            return
        }
        XCTAssertEqual(expectedOffer, offer)
    }
    
    func testDidNextStepDeclineOffer_WhenCalledFromViewModel_ShouldPerformNextStepDeclineOffer() {
        let delegate = LendingSuccessDelegateSpy()
        sut.didNextStep(action: .declineOffer(delegate: delegate))
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        guard case .declineOffer = coordinatorSpy.action else {
            XCTFail("wrong action called")
            return
        }
    }
    
    func testDidNextStepClose_WhenCalledFromViewModel_ShouldCallPerformNextStepClose() {
        sut.didNextStep(action: .close)
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        guard case .close = coordinatorSpy.action else {
            XCTFail("wrong action called")
            return
        }
    }
    
    func testDidNextStepShowFAQ_WhenCalledFromPresenter_ShouldPerformNextActionShowFAQ() throws {
        let expectedURL = try XCTUnwrap(URL(string: "https://www.picpay.com"))
            
        sut.didNextStep(action: .showFAQ(url: expectedURL))
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        guard case let .showFAQ(url) = coordinatorSpy.action else {
            XCTFail("wrong action called")
            return
        }
        
        XCTAssertEqual(url, expectedURL)
    }
    
    func testDidNextStepInstallmentList_WhenCalledFromViewModel_ShouldPerformNextStepInstallmentList() throws {
        let expectedOffer: OpportunityOffer = try .mock(for: .active)
        
        sut.didNextStep(action: .installmentList(proposalIdentifier: expectedOffer.id))
        
        guard case let .installmentList(id) = coordinatorSpy.action else {
            XCTFail("wrong action called")
            return
        }
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        XCTAssertEqual(id, expectedOffer.id)
    }
}
