import XCTest
import SafariServices
import UI
@testable import P2PLending

final class LendingOpportunityOfferCoordinatorTests: XCTestCase {
    private lazy var spy = ViewControllerSpy()
    private lazy var transactionSceneProviderSpy = TransactionSceneProviderSpy()
    private lazy var urlOpener = URLOpenerSpy()
    private lazy var sut: LendingOpportunityOfferCoordinator = {
        let container = DependencyContainerMock(transactionSceneProviderSpy, urlOpener)
        let coordinator = LendingOpportunityOfferCoordinator(dependencies: container)
        coordinator.viewController = spy
        return coordinator
    }()
    
    func testPerformNextActionMakeTransaction_WhenCalledFromPresenter_ShouldShowTransactionScene() throws {
        let offer: OpportunityOffer =  try .mock(for: .waitingReply)
        
        sut.perform(action: .makeTransaction(offer: offer))
        
        XCTAssertEqual(transactionSceneProviderSpy.callTransactionSceneCount, 1)
        XCTAssertEqual(transactionSceneProviderSpy.borrower, offer.borrower)
        XCTAssertEqual(transactionSceneProviderSpy.offerIdentifier, offer.id)
        XCTAssertEqual(transactionSceneProviderSpy.value, offer.operationDetails.subtotal)
        XCTAssertEqual(spy.callShowViewControllerCount, 1)
    }
    
    func testPerformNextActionContract_WhenCalledFromPresenter_ShouldPresentContractPage() throws {
        let expectedURL = try XCTUnwrap(URL(string: "https://www.picpay.com"))
        let webPage = LendingSessionWebPage(title: Strings.Lending.Opportunity.Offer.cv, url: expectedURL)
        
        sut.perform(action: .contract(webPage: webPage))
        
        XCTAssertEqual(spy.callPresentViewControllerCount, 1)
        let navigationController = try XCTUnwrap(spy.viewController as? UINavigationController)
        XCTAssertTrue(navigationController.viewControllers.first is LendingSessionWebViewViewController)
    }
    
    func testPerformNextActionDeclineOffer_WhenCalledFromPresenter_ShouldShowSuccessViewController() {
        let delegate = LendingSuccessDelegateSpy()
        sut.perform(action: .declineOffer(delegate: delegate))
        
        XCTAssertEqual(spy.callShowViewControllerCount, 1)
        XCTAssertTrue(spy.viewController is LendingSuccessViewController)
    }
    
    func testPerformNextActionDetailedError_WhenCalledFromPresenter_ShouldShowLendingDetailedErrorController() {
        let identifiableError: IdentifiableOpportunittyOfferError = .activeInvestorLimitReached
        
        sut.perform(action: .detailedError(identifiableError))
        
        XCTAssertEqual(spy.callShowViewControllerCount, 1)
        XCTAssertTrue(spy.viewController is ApolloFeedbackViewController)
    }
    
    func testPerformNextActionDetailedError_WhenCalledFromPresenter_ShouldShowLendingInsufficientFundsErrorController() {
        let identifiableError: IdentifiableOpportunittyOfferError = .insufficientFunds
        
        sut.perform(action: .insufficientFunds(identifiableError))
        
        XCTAssertEqual(spy.callShowViewControllerCount, 1)
        XCTAssertTrue(spy.viewController is ApolloFeedbackViewController)
    }
    
    func testPerformNextActionClose_WhenCalledFromPresenter_ShouldDismissViewController() {
        sut.perform(action: .close)
        
        XCTAssertEqual(spy.callDismissCount, 1)
    }
    
    func testPerformNextActionShowFAQ_WhenCalledFromPresenter_ShouldShowFAQ() throws {
        let url = try XCTUnwrap(URL(string: "https://www.picpay.com"))
        
        sut.perform(action: .showFAQ(url: url))
        
        XCTAssertEqual(urlOpener.callCanOpenURLCount, 1)
        XCTAssertEqual(urlOpener.callOpenURLCount, 1)
        XCTAssertEqual(urlOpener.checkedURL, url)
        XCTAssertEqual(urlOpener.openedURL, url)
    }
    
    func testPerformActionInstallmentList_WhenCalledFromPresenter_ShouldShowInstallmentListController() throws {
        let offer: OpportunityOffer =  try .mock(for: .active)
        
        sut.perform(action: .installmentList(proposalIdentifier: offer.id))
        
        XCTAssertEqual(spy.callShowViewControllerCount, 1)
        XCTAssertTrue(spy.viewController is LendingInstallmentListViewController)
    }
    
}
