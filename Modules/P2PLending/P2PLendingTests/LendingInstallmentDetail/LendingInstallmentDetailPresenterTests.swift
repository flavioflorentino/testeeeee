import Core
import XCTest
@testable import P2PLending

final class LendingInstallmentDetailDisplaySpy: LendingInstallmentDetailDisplaying {
    private(set) var callDisplayInstallmentDetailCount = 0
    private(set) var callDisplayErrorCount = 0
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    
    private(set) var installmentDetail: InstallmentDetailViewModel?
    private(set) var error: Error?
    
    func display(installmentDetail: InstallmentDetailViewModel) {
        callDisplayInstallmentDetailCount += 1
        self.installmentDetail = installmentDetail
    }
    
    func display(error: Error) {
        callDisplayErrorCount += 1
        self.error = error
    }
    
    func startLoading() {
        callStartLoadingCount += 1
    }
    
    func stopLoading() {
        callStopLoadingCount += 1
    }
}

final class LendingInstallmentDetailCoordinatorSpy: LendingInstallmentDetailCoordinating {
    var viewController: UIViewController?
    
    private(set) var callPerformActionCount = 0
    private(set) var action: LendingInstallmentDetailAction?
    
    func perform(action: LendingInstallmentDetailAction) {
        callPerformActionCount += 1
        self.action = action
    }
}

final class LendingInstallmentDetailPresenterTests: XCTestCase {
    typealias Localizable = Strings.Lending.Installment.Detail
    
    private lazy var spy = LendingInstallmentDetailDisplaySpy()
    private lazy var coordinatorSpy = LendingInstallmentDetailCoordinatorSpy()
    private lazy var sut: LendingInstallmentDetailPresenter = {
        let presenter = LendingInstallmentDetailPresenter(coordinator: coordinatorSpy)
        presenter.viewController = spy
        return presenter
    }()
    
    func testPresentInstallmentDetail_WhenCalledFromInteractor_ShouldDisplayInstallmentDetail() {
        let mock: InstallmentDetail = .mock()
        let expectedInstallmentDetail: InstallmentDetailViewModel = .mock()
        
        sut.present(installmentDetail: mock)
        
        XCTAssertEqual(spy.callDisplayInstallmentDetailCount, 1)
        XCTAssertEqual(spy.installmentDetail, expectedInstallmentDetail)
    }
    
    func testPresentInstallmentDetail_WhenDueDateExpired_ShouldDisplayInstallmentDetailFormattedForExpiredDueDate() throws {
        let yesterday = try XCTUnwrap(Calendar.current.date(byAdding: .day, value: -1, to: Date()))
        let mock: InstallmentDetail = .mock(dueDate: yesterday)
        let expectedInstallmentDetail: InstallmentDetailViewModel = .mock(dueDate: yesterday)
        
        sut.present(installmentDetail: mock)
        
        XCTAssertEqual(spy.callDisplayInstallmentDetailCount, 1)
        XCTAssertEqual(spy.installmentDetail, expectedInstallmentDetail)
    }
    
    func testPresentError_WhenCalledFromInteractor_ShouldDisplayError() {
        let error: ApiError = .connectionFailure
        
        sut.present(error: error)
        
        XCTAssertEqual(spy.callDisplayErrorCount, 1)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
    }
    
    func testStartLoading_WhenCalledFromInteractor_ShouldStartLoading() {
        sut.startLoading()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
    }
    
    func testStopLoading_WhenCalledFromInteractor_ShouldStopLoading() {
        sut.stopLoading()
        
        XCTAssertEqual(spy.callStopLoadingCount, 1)
    }
    
    func testDidNextStepPayViaPicPay_WhenCalledFromInteractor_ShouldCallPerformActionPayViaPicPay() {
        let mock: InstallmentDetail = .mock()
        let description = Localizable.installmentDetailDefaultPaymentDescription(mock.number, mock.numberOfInstallments)
        
        sut.presentPicPayPayment(for: mock)
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .payViaPicPay(installmentDetail: mock, paymentDescription: description))
    }
    
    func testDidNextStepPayViaBankSlip_WhenCalledFromInteractor_ShouldCallPerformActionPayViaBankSlip() {
        let mock: InstallmentDetail = .mock()
        
        sut.didNextStep(action: .payViaBankSlip(installmentDetail: mock))
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .payViaBankSlip(installmentDetail: mock))
    }
    
    func testDidNextStepClose_WhenCalledFromInteractor_ShouldCallPerformActionClose() {
        sut.didNextStep(action: .close)
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .close)
    }
}
