import Core
import XCTest
import AnalyticsModule
@testable import P2PLending

final class LendingInstallmentDetailPresenterSpy: LendingInstallmentDetailPresenting {
    var viewController: LendingInstallmentDetailDisplaying?
    
    private(set) var callPresentInstallmentDetailCount = 0
    private(set) var callPresentPicPayPaymentCount = 0
    private(set) var callPresentErrorCount = 0
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    private(set) var callDidNextStepCount = 0
    
    private(set) var installmentDetail: InstallmentDetail?
    private(set) var error: Error?
    private(set) var action: LendingInstallmentDetailAction?
    
    func present(installmentDetail: InstallmentDetail) {
        callPresentInstallmentDetailCount += 1
        self.installmentDetail = installmentDetail
    }
    
    func presentPicPayPayment(for installmentDetail: InstallmentDetail) {
        callPresentPicPayPaymentCount += 1
        self.installmentDetail = installmentDetail
    }
    
    func present(error: Error) {
        callPresentErrorCount += 1
        self.error = error
    }
    
    func startLoading() {
        callStartLoadingCount += 1
    }
    
    func stopLoading() {
        callStopLoadingCount += 1
    }
    
    func didNextStep(action: LendingInstallmentDetailAction) {
        callDidNextStepCount += 1
        self.action = action
    }
}

final class LendingInstallmentDetailServiceMock: LendingInstallmentDetailServicing {
    var expectedFetchInstallmentDetailResult: Result<InstallmentDetail, ApiError>?
    
    func fetchInstallmentDetail(offerIdentifier: UUID, installmentNumber: Int, completion: @escaping ModelCompletionBlock<InstallmentDetail>) {
        callCompletionIfNeeded(completion, expectedResult: expectedFetchInstallmentDetailResult)
    }
}

final class LendingInstallmentDetailInteractorTests: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    private lazy var spy = LendingInstallmentDetailPresenterSpy()
    private lazy var serviceMock = LendingInstallmentDetailServiceMock()
    private lazy var dependencyMock = DependencyContainerMock(analyticsSpy)
    private lazy var sut = LendingInstallmentDetailInteractor(service: serviceMock,
                                                              presenter: spy,
                                                              offerIdentifier: UUID(),
                                                              installmentNumber: 1,
                                                              dependencies: dependencyMock)
    
    func testFetchInstallmentDetail_WhenReceiveSuccessFromServer_ShouldPresentInstallmentDetail() {
        let mock: InstallmentDetail = .mock()
        serviceMock.expectedFetchInstallmentDetailResult = .success(mock)
        
        sut.fetchInstallmentDetail()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callStopLoadingCount, 1)
        XCTAssertEqual(spy.callPresentInstallmentDetailCount, 1)
        XCTAssertEqual(spy.installmentDetail, mock)
    }
    
    func testFetchInstallmentDetail_WhenReceiveFailureFromServer_ShouldPresentInstallmentDetail() {
        let mock: ApiError = .bodyNotFound
        serviceMock.expectedFetchInstallmentDetailResult = .failure(mock)
        
        sut.fetchInstallmentDetail()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callStopLoadingCount, 1)
        XCTAssertEqual(spy.callPresentErrorCount, 1)
        XCTAssertEqual(spy.error?.localizedDescription, mock.localizedDescription)
    }
    
    func testProceedToPicPayPayment_WhenCalledFromViewController_ShouldCallDidNextStepPayViaPicPay() {
        let mock: InstallmentDetail = .mock()
        let expectedEvent = LendingInstallmentDetailEvent.picpayPayment(mock.dueDate).event()
        serviceMock.expectedFetchInstallmentDetailResult = .success(mock)
        
        sut.fetchInstallmentDetail()
        sut.proceedToPicPayPayment()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(spy.callPresentPicPayPaymentCount, 1)
        XCTAssertEqual(spy.installmentDetail, mock)
    }
    
    func testProceedToBankSlipPayment_WhenCalledFromViewController_ShouldCallDidNextStepPayViaBankSlip() {
        let mock: InstallmentDetail = .mock()
        let expectedEvent = LendingInstallmentDetailEvent.bankSlipPayment(mock.dueDate).event()
        serviceMock.expectedFetchInstallmentDetailResult = .success(mock)
        
        sut.fetchInstallmentDetail()
        sut.proceedToBankSlipPayment()
        
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        XCTAssertEqual(spy.action, .payViaBankSlip(installmentDetail: mock))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testDismiss_WhenCalledFromViewController_ShouldCallDidNextStepClose() {
        sut.dismiss()
        
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        XCTAssertEqual(spy.action, .close)
    }
}
