import XCTest
@testable import P2PLending

final class LendingInstallmentDetailCoordinatorTests: XCTestCase {
    private lazy var spy = ViewControllerSpy()
    private lazy var billetSceneProviderSpy = BilletSceneProviderSpy()
    private lazy var sut: LendingInstallmentDetailCoordinator = {
        let container = DependencyContainerMock(billetSceneProviderSpy)
        let coordinator = LendingInstallmentDetailCoordinator(dependencies: container)
        coordinator.viewController = spy
        return coordinator
    }()
    
    func testPerformActionPayViaBankSlip_WhenCalledFromPresenter_ShouldShowDigitableLine() {
        let installmentDetail: InstallmentDetail = .mock()
        
        sut.perform(action: .payViaBankSlip(installmentDetail: installmentDetail))
        
        XCTAssertEqual(spy.callShowViewControllerCount, 1)
        XCTAssertTrue(spy.viewController is LendingDigitableLineViewController)
    }
    
    func testPerformActionPayViaPicPay_WhenCalledFromPresenter_ShouldCallBilletSceneProvider() {
        let installmentDetail: InstallmentDetail = .mock()
        let description = "Some description"
        
        sut.perform(action: .payViaPicPay(installmentDetail: installmentDetail, paymentDescription: description))
        
        XCTAssertEqual(billetSceneProviderSpy.callBilletSceneCount, 1)
        XCTAssertEqual(billetSceneProviderSpy.digitableLine, installmentDetail.digitableLine)
        XCTAssertEqual(billetSceneProviderSpy.description, description)
        XCTAssertEqual(spy.callShowViewControllerCount, 1)
    }
    
    func testPerformActionClose_WhenCalledFromPresenter_ShouldDismissViewController() {
        sut.perform(action: .close)
        
        XCTAssertEqual(spy.callDismissCount, 1)
    }
}
