import XCTest
import UI
@testable import P2PLending

final class LendingProposalFriendsCoordinatorTests: XCTestCase {
    private lazy var spy = ViewControllerSpy()
    private lazy var urlOpener = URLOpenerSpy()
    private lazy var sut: LendingProposalFriendsCoordinator = {
        let coordinator = LendingProposalFriendsCoordinator(data: .successData, dependencies: DependencyContainerMock(urlOpener))
        coordinator.viewController = spy
        return coordinator
    }()
    
    func testPerformNextActionProposalDescription_WhenCalledFromPresenter_ShouldShowProposalDescription() {
        sut.perform(action: .proposalDescription(proposal: .mock))
        
        XCTAssertEqual(spy.callShowViewControllerCount, 1)
        XCTAssertTrue(spy.viewController is LendingProposalDescriptionViewController)
    }
    
    func testPerformActionDetailedError_WhenCalledFromPresenter_ShouldDisplayApolloFeedbackController() {
        sut.perform(action: .detailedError(error: .activeProposalsLimitReached))
        
        XCTAssertEqual(spy.callPresentViewControllerCount, 1)
        XCTAssertTrue(spy.viewController is ApolloFeedbackViewController)
    }
    
    func testPerformActionAccountUpgrade_WhenCalledFromPresenter_ShouldDisplayApolloFeedbackController() {
        sut.perform(action: .detailedError(error: .accountUpgrade))
        
        XCTAssertEqual(spy.callPresentViewControllerCount, 1)
        XCTAssertTrue(spy.viewController is ApolloFeedbackViewController)
    }
}
