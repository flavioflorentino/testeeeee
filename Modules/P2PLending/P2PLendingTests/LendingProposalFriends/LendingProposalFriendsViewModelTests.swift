import Core
import XCTest
import AnalyticsModule
@testable import P2PLending

private final class LendingProposalFriendsPresenterSpy: LendingProposalFriendsPresenting {
    var viewController: LendingProposalFriendsDisplay?
    
    private(set) var callPresentFriendsCount = 0
    private(set) var callPresentEmptyStateCount = 0
    private(set) var callPresentEmptySearchResultsCount = 0
    private(set) var callHideEmptyStateCount = 0
    private(set) var callPresentEmptyStateErrorCount = 0
    private(set) var callDidNextStepCount = 0
    private(set) var callStartLoadingList = 0
    private(set) var callStopLoadingList = 0
    private(set) var callStartLoadingFriendCount = 0
    private(set) var callStopLoadingFriendCount = 0
    private(set) var callPresentErrorCount = 0
    
    private(set) var friends: [LendingFriend]?
    private(set) var error: Error?
    private(set) var action: LendingProposalFriendsAction?
    private(set) var startLoadingIndexPath: IndexPath?
    private(set) var stopLoadingIndexPath: IndexPath?
    
    func present(friends: [LendingFriend]) {
        callPresentFriendsCount += 1
        self.friends = friends
    }
    
    func presentEmptyState() {
        callPresentEmptyStateCount += 1
    }
    
    func presentEmptySearchResults() {
        callPresentEmptySearchResultsCount += 1
    }
    
    func hideEmptyState() {
        callHideEmptyStateCount += 1
    }
    
    func presentEmptyState(error: Error) {
        callPresentEmptyStateErrorCount += 1
        self.error = error
    }
    
    func didNextStep(action: LendingProposalFriendsAction) {
        callDidNextStepCount += 1
        self.action = action
    }
    
    func startLoadingList() {
        callStartLoadingList += 1
    }
    
    func stopLoadingList() {
        callStopLoadingList += 1
    }

    func startLoadingFriend(at indexPath: IndexPath) {
        callStartLoadingFriendCount += 1
        startLoadingIndexPath = indexPath
    }
    
    func stopLoadingFriend(at indexPath: IndexPath) {
        callStopLoadingFriendCount += 1
        stopLoadingIndexPath = indexPath
    }
    
    func present(error: Error) {
        callPresentErrorCount += 1
        self.error = error
    }
}

private final class LendingProposalFriendsServiceMock: LendingProposalFriendsServicing {
    var expectedFetchFriendsResult: Result<[LendingFriend], ApiError>?
    var expectedCheckInvestorEligibilityResult: Result<NoContent, ApiError>?
    
    func fetchFriends(scope: SearchScope, query: String?, completion: @escaping ModelCompletionBlock<[LendingFriend]>) {
        callCompletionIfNeeded(completion, expectedResult: expectedFetchFriendsResult)
    }
    
    func checkInvestorEligibility(for friend: LendingFriend, completion: @escaping ModelCompletionBlock<NoContent>) {
        callCompletionIfNeeded(completion, expectedResult: expectedCheckInvestorEligibilityResult)
    }
}

final class LendingProposalFriendsViewModelTests: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    private lazy var spy = LendingProposalFriendsPresenterSpy()
    private lazy var serviceMock = LendingProposalFriendsServiceMock()
    private lazy var dependencyMock = DependencyContainerMock(analyticsSpy)
    private lazy var sut = LendingProposalFriendsViewModel(
        service: serviceMock,
        presenter: spy,
        proposal: .mock,
        dependencies: dependencyMock,
        shouldDelayFilterRequests: false
    )
    
    func testFetchFriends_WhenReceiveSuccessFromServer_ShouldCallPresentFriendsAndHideEmptyState() {
        let friends: [LendingFriend] = [.mock, .mock, .mock]
        serviceMock.expectedFetchFriendsResult = .success(friends)
        
        sut.fetchFriends()
        
        XCTAssertEqual(spy.callStartLoadingList, 1)
        XCTAssertEqual(spy.callStopLoadingList, 1)
        XCTAssertEqual(spy.callPresentFriendsCount, 1)
        XCTAssertEqual(spy.callHideEmptyStateCount, 1)
        XCTAssertEqual(spy.friends, friends)
    }
    
    func testFetchFriends_WhenReceiveSuccessFromServerWithEmptyList_ShouldCallPresentFriendsAndPresentEmptyState() {
        let friends: [LendingFriend] = []
        serviceMock.expectedFetchFriendsResult = .success(friends)
        
        sut.fetchFriends()
        
        XCTAssertEqual(spy.callStartLoadingList, 1)
        XCTAssertEqual(spy.callStopLoadingList, 1)
        XCTAssertEqual(spy.callPresentFriendsCount, 1)
        XCTAssertEqual(spy.callPresentEmptyStateCount, 1)
        XCTAssertEqual(spy.friends, friends)
    }
    
    func testFetchFriends_WhenReceiveFailureFromServer_ShouldCallPresentError() {
        let expectedEvent = LendingProposalFriendsEvent.screenViewed(true).event()
        let error: ApiError = .bodyNotFound
        serviceMock.expectedFetchFriendsResult = .failure(.bodyNotFound)
        
        sut.fetchFriends()
        
        XCTAssertEqual(spy.callStartLoadingList, 1)
        XCTAssertEqual(spy.callStopLoadingList, 1)
        XCTAssertEqual(spy.callPresentEmptyStateErrorCount, 1)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testFilterFriends_WhenReceiveSuccessFromServer_ShouldCallPresentFriendsAndHideEmptyState() {
        let friends: [LendingFriend] = [.mock, .mock, .mock]
        serviceMock.expectedFetchFriendsResult = .success(friends)
        
        sut.filterFriends(query: "some query")
        
        XCTAssertEqual(spy.callStartLoadingList, 1)
        XCTAssertEqual(spy.callStopLoadingList, 1)
        XCTAssertEqual(spy.callPresentFriendsCount, 1)
        XCTAssertEqual(spy.callHideEmptyStateCount, 1)
        XCTAssertEqual(spy.friends, friends)
    }
    
    func testFilterFriends_WhenReceiveSuccessFromServerWithEmptyList_ShouldCallPresentFriendsAndPresentEmptySearchResults() {
        let friends: [LendingFriend] = []
        serviceMock.expectedFetchFriendsResult = .success(friends)
        
        sut.filterFriends(query: "some query")
        
        XCTAssertEqual(spy.callStartLoadingList, 1)
        XCTAssertEqual(spy.callStopLoadingList, 1)
        XCTAssertEqual(spy.callPresentFriendsCount, 1)
        XCTAssertEqual(spy.callPresentEmptySearchResultsCount, 1)
        XCTAssertEqual(spy.friends, friends)
    }
    
    func testFilterFriends_WhenReceiveFailureFromServer_ShouldCallPresentError() {
        let error: ApiError = .bodyNotFound
        serviceMock.expectedFetchFriendsResult = .failure(error)
        
        sut.filterFriends(query: "some query")
        
        XCTAssertEqual(spy.callPresentEmptyStateErrorCount, 1)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
    }
    
    func testSelectFriend_WhenReceiveSuccessFromInvestorEligibilityCheck_ShouldCallDidNextStepActionProposalDescription() {
        let friend: LendingFriend = .mock
        let indexPath = IndexPath(row: 0, section: 0)
        var expectedProposal: Proposal = .mock
        expectedProposal.friend = friend
        serviceMock.expectedFetchFriendsResult = .success([friend])
        serviceMock.expectedCheckInvestorEligibilityResult = .success(NoContent())
        
        sut.fetchFriends()
        sut.selectFriend(at: indexPath)
        
        XCTAssertEqual(spy.callStartLoadingFriendCount, 1)
        XCTAssertEqual(spy.startLoadingIndexPath, indexPath)
        XCTAssertEqual(spy.callStopLoadingFriendCount, 1)
        XCTAssertEqual(spy.stopLoadingIndexPath, indexPath)
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        XCTAssertEqual(spy.action, .proposalDescription(proposal: expectedProposal))
    }
    
    func testSelectFriend_WhenReceiveGenericErrorFromInvestorEligibilityCheck_ShouldCallDidNextStepActionProposalDescription() {
        let friend: LendingFriend = .mock
        let indexPath = IndexPath(row: 0, section: 0)
        let error: ApiError = .bodyNotFound
        var expectedProposal: Proposal = .mock
        expectedProposal.friend = friend
        
        serviceMock.expectedFetchFriendsResult = .success([friend])
        serviceMock.expectedCheckInvestorEligibilityResult = .failure(error)
        
        sut.fetchFriends()
        sut.selectFriend(at: indexPath)
        
        XCTAssertEqual(spy.callStartLoadingFriendCount, 1)
        XCTAssertEqual(spy.startLoadingIndexPath, indexPath)
        XCTAssertEqual(spy.callStopLoadingFriendCount, 1)
        XCTAssertEqual(spy.stopLoadingIndexPath, indexPath)
        XCTAssertEqual(spy.callPresentErrorCount, 1)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
    }
    
    func testSelectFriend_WhenReceiveIdentifiableFriendsErrorFromInvestorEligibilityCheck_ShouldCallDidNextStepActionAccountUpgrade() {
        let friend: LendingFriend = .mock
        let indexPath = IndexPath(row: 0, section: 0)
        var requestError = RequestError()
        requestError.code = IdentifiableFriendsError.accountUpgrade.rawValue
        let error: ApiError = .badRequest(body: requestError)
        var expectedProposal: Proposal = .mock
        expectedProposal.friend = friend
        
        serviceMock.expectedFetchFriendsResult = .success([friend])
        serviceMock.expectedCheckInvestorEligibilityResult = .failure(error)
        
        sut.fetchFriends()
        sut.selectFriend(at: indexPath)
        
        XCTAssertEqual(spy.callStartLoadingFriendCount, 1)
        XCTAssertEqual(spy.startLoadingIndexPath, indexPath)
        XCTAssertEqual(spy.callStopLoadingFriendCount, 1)
        XCTAssertEqual(spy.stopLoadingIndexPath, indexPath)
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        XCTAssertEqual(spy.action, .accountUpgrade(error: .accountUpgrade))
    }
    
    func testSelectFriend_WhenReceiveIdentifiableFriendsErrorFromInvestorEligibilityCheck_ShouldCallDidNextStepActionDetailedError() {
        let friend: LendingFriend = .mock
        let indexPath = IndexPath(row: 0, section: 0)
        var requestError = RequestError()
        requestError.code = IdentifiableFriendsError.activeProposalsLimitReached.rawValue
        let error: ApiError = .badRequest(body: requestError)
        var expectedProposal: Proposal = .mock
        expectedProposal.friend = friend
        
        serviceMock.expectedFetchFriendsResult = .success([friend])
        serviceMock.expectedCheckInvestorEligibilityResult = .failure(error)
        
        sut.fetchFriends()
        sut.selectFriend(at: indexPath)
        
        XCTAssertEqual(spy.callStartLoadingFriendCount, 1)
        XCTAssertEqual(spy.startLoadingIndexPath, indexPath)
        XCTAssertEqual(spy.callStopLoadingFriendCount, 1)
        XCTAssertEqual(spy.stopLoadingIndexPath, indexPath)
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        XCTAssertEqual(spy.action, .detailedError(error: .activeProposalsLimitReached))
    }
    
    func testShowHelp_whenCalledFromViewController_ShouldShowHelpPage() {
        let friends: [LendingFriend] = [.mock]
        serviceMock.expectedFetchFriendsResult = .success(friends)
        let expectedEvent = LendingProposalFriendsEvent.helpButtonTapped(false).event()
        
        sut.showHelp()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testSendAnalyticsEvent_WhenCalledFromBackButtonFromController_ShouldSendEvent() {
        let friends: [LendingFriend] = [.mock]
        serviceMock.expectedFetchFriendsResult = .success(friends)
        let expectedEvent = LendingProposalFriendsEvent.backButtonTapped(false).event()
        
        sut.sendAnalyticsEvent(.backButtonTapped)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testSendAnalyticsEvent_WhenCalledFromSearchBarFromController_ShouldSendEvent() {
        let friends: [LendingFriend] = [.mock]
        serviceMock.expectedFetchFriendsResult = .success(friends)
        let expectedEvent = LendingProposalFriendsEvent.searchAccessed(false).event()
        
        sut.sendAnalyticsEvent(.searchBarTextDidBeginEditing)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testAnalyticsTitle_WhenReceiveFailureFromServer_ShouldChangeForErrorAnalyticsTitle() throws {
        let expectedEvent = LendingProposalFriendsEvent.screenViewed(true).event()
        serviceMock.expectedFetchFriendsResult = .failure(.bodyNotFound)
        sut.fetchFriends()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        
        let backButtonEvent = LendingProposalFriendsEvent.backButtonTapped(true).event()
        sut.sendAnalyticsEvent(.backButtonTapped)

        let title = try XCTUnwrap(analyticsSpy.event?.properties[AnalyticsKeys.screenName.name] as? String)

        XCTAssertEqual(title, "P2P_LENDING_TOMADOR_ESCOLHA_UM_AMIGO_ERRO")
        XCTAssertEqual(analyticsSpy.logCalledCount, 2)
        XCTAssertTrue(analyticsSpy.equals(to: backButtonEvent))
    }
    
    func testAnalyticsTitle_WhenReceiveSuccessFromServer_ShouldChangeForRegularAnalyticsTitle() throws {
        let expectedEvent = LendingProposalFriendsEvent.backButtonTapped(false).event()
        let friends: [LendingFriend] = [.mock, .mock, .mock]
        serviceMock.expectedFetchFriendsResult = .success(friends)
        
        sut.fetchFriends()
        
        sut.sendAnalyticsEvent(.backButtonTapped)
        
        let title = try XCTUnwrap(analyticsSpy.event?.properties[AnalyticsKeys.screenName.name] as? String)
        
        XCTAssertEqual(title, "P2P_LENDING_TOMADOR_ESCOLHA_UM_AMIGO")
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
}
