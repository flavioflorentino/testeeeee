import UI
import Core
import XCTest
@testable import P2PLending

private final class LendingProposalFriendsViewControllerSpy: LendingProposalFriendsDisplay {
    private(set) var callDisplayFriendsCount = 0
    private(set) var callDisplayEmptyStateCount = 0
    private(set) var callHideEmptyStateCount = 0
    private(set) var callDisplayEmptyStateErrorCount = 0
    private(set) var callStartLoadingList = 0
    private(set) var callStopLoadingList = 0
    private(set) var callStartLoadingFriendCount = 0
    private(set) var callStopLoadingFriendCount = 0
    private(set) var callDisplayErrorCount = 0
    
    private(set) var friends: [LendingFriend]?
    private(set) var emptyStateTitle: String?
    private(set) var emptyStateMessage: String?
    private(set) var error: Error?
    private(set) var startLoadingIndexPath: IndexPath?
    private(set) var stopLoadingIndexPath: IndexPath?
    
    func display(friends: [LendingFriend]) {
        callDisplayFriendsCount += 1
        self.friends = friends
    }
    
    func displayEmptyState(title: String, message: String) {
        callDisplayEmptyStateCount += 1
        emptyStateTitle = title
        emptyStateMessage = message
    }
    
    func hideEmptyState() {
        callHideEmptyStateCount += 1
    }
    
    func displayEmptyState(error: Error) {
        callDisplayEmptyStateErrorCount += 1
        self.error = error
    }
    
    func startLoadingList() {
        callStartLoadingList += 1
    }
    
    func stopLoadingList() {
        callStopLoadingList += 1
    }
    
    func startLoadingFriend(at indexPath: IndexPath) {
        callStartLoadingFriendCount += 1
        startLoadingIndexPath = indexPath
    }
    
    func stopLoadingFriend(at indexPath: IndexPath) {
        callStopLoadingFriendCount += 1
        stopLoadingIndexPath = indexPath
    }
    
    func displayError(error: Error) {
        callDisplayErrorCount += 1
        self.error = error
    }
}

private final class LendingProposalFriendsCoordinatorSpy: LendingProposalFriendsCoordinating {
    var viewController: UIViewController?
    
    private(set) var callPerformActionCount = 0
    
    private(set) var action: LendingProposalFriendsAction?
    
    func perform(action: LendingProposalFriendsAction) {
        callPerformActionCount += 1
        self.action = action
    }
}

final class LendingProposalFriendsPresenterTests: XCTestCase {
    private lazy var spy = LendingProposalFriendsViewControllerSpy()
    private lazy var coordinatorSpy = LendingProposalFriendsCoordinatorSpy()
    private lazy var sut: LendingProposalFriendsPresenter = {
        let presenter = LendingProposalFriendsPresenter(coordinator: coordinatorSpy)
        presenter.viewController = spy
        return presenter
    }()
    
    func testPresentFriends_WhenCalledFromPresenter_ShouldCallDisplayFriends() {
        let friends: [LendingFriend] = [.mock]
        
        sut.present(friends: friends)
        
        XCTAssertEqual(spy.callDisplayFriendsCount, 1)
        XCTAssertEqual(spy.friends, friends)
    }
    
    func testPresentEmptyState_WhenCalledFromPresenter_ShouldCallDisplayEmptyStateWithEmptyListTitleAndMessage() {
        let expectedEmptyListTitle = Strings.Lending.Proposal.Friends.emptyListTitle
        let expectedEmptyListMessage = Strings.Lending.Proposal.Friends.emptyListMessage
        
        sut.presentEmptyState()
        
        XCTAssertEqual(spy.callDisplayEmptyStateCount, 1)
        XCTAssertEqual(spy.emptyStateTitle, expectedEmptyListTitle)
        XCTAssertEqual(spy.emptyStateMessage, expectedEmptyListMessage)
    }
    
    func testPresentEmptySearchResults_WhenCalledFromPresenter_ShouldCallDisplayEmptyStateWithEmptySearchResultsTitleAndMessage() {
        let expectedEmptySearchResultsTitle = Strings.Lending.Proposal.Friends.emptySearchResultsTitle
        let expectedEmptySearchResultsMessage = Strings.Lending.Proposal.Friends.emptySearchResultsMessage
        
        sut.presentEmptySearchResults()
        
        XCTAssertEqual(spy.callDisplayEmptyStateCount, 1)
        XCTAssertEqual(spy.emptyStateTitle, expectedEmptySearchResultsTitle)
        XCTAssertEqual(spy.emptyStateMessage, expectedEmptySearchResultsMessage)
    }
    
    func testHideEmptyState_WhenCalledFromPresenter_ShouldCallHideEmptyState() {
        sut.hideEmptyState()
        
        XCTAssertEqual(spy.callHideEmptyStateCount, 1)
    }
    
    func testPresentEmptyStateError_WhenCalledFromPresenter_ShouldCallDisplayEmptyStateError() {
        let friends: [LendingFriend] = []
        let error: ApiError = .bodyNotFound
        sut.presentEmptyState(error: error)
        
        XCTAssertEqual(spy.callDisplayEmptyStateErrorCount, 1)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
        XCTAssertEqual(spy.callDisplayFriendsCount, 1)
        XCTAssertEqual(spy.friends, friends)
    }
    
    func testStartLoadingList_WhenCalledFromViewModel_ShouldCallStartLoadingList() {
        sut.startLoadingList()
        XCTAssertEqual(spy.callStartLoadingList, 1)
    }
    
    func testStopLoadingList_WhenCalledFromViewModel_ShouldCallStopLoadingList() {
        sut.stopLoadingList()
        XCTAssertEqual(spy.callStopLoadingList, 1)
    }
    
    func testStartLoadingFriend_WhenCalledFromViewModel_ShouldCallStartLoadingFriend() {
        let indexPath = IndexPath(row: 0, section: 0)
        
        sut.startLoadingFriend(at: indexPath)
        
        XCTAssertEqual(spy.callStartLoadingFriendCount, 1)
        XCTAssertEqual(spy.startLoadingIndexPath, indexPath)
    }
    
    func testStopLoadingFriend_WhenCalledFromViewModel_ShouldCallStopLoadingFriend() {
        let indexPath = IndexPath(row: 0, section: 0)
        
        sut.stopLoadingFriend(at: indexPath)
        
        XCTAssertEqual(spy.callStopLoadingFriendCount, 1)
        XCTAssertEqual(spy.stopLoadingIndexPath, indexPath)
    }
    
    func testPresentError_WhenCalledFromPresenter_ShouldCallDisplayError() {
        let error: ApiError = .bodyNotFound
        sut.present(error: error)
        
        XCTAssertEqual(spy.callDisplayErrorCount, 1)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
    }
    
    func testDidNextStepProposalDescription_WhenCalledFromPresenter_ShouldCallPerformNextStepProposalDescription() {
        let action: LendingProposalFriendsAction = .proposalDescription(proposal: .mock)
        sut.didNextStep(action: action)
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        XCTAssertEqual(coordinatorSpy.action, action)
    }
    
    func testDidNextStepDetailedError_WhenCalledFromPresenter_ShouldCallPerformNextStepDetailed() {
        let action: LendingProposalFriendsAction = .detailedError(error: .accountUpgrade)
        sut.didNextStep(action: action)
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        XCTAssertEqual(coordinatorSpy.action, action)
    }
}
