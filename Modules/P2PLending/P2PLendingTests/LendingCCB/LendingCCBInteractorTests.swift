import Core
import XCTest
import AnalyticsModule
import FeatureFlag
@testable import P2PLending

private final class LendingCCBPresenterSpy: LendingCCBPresenting {
    var viewController: LendingCCBDisplay?
    
    private(set) var callPresentDocumentCount = 0
    private(set) var callPresentErrorCount = 0
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    private(set) var callDidNextStepCount = 0
    
    private(set) var ccbData: CCBData?
    private(set) var error: Error?
    private(set) var action: LendingCCBAction?
    
    func presentDocument(ccbData: CCBData) {
        callPresentDocumentCount += 1
        self.ccbData = ccbData
    }
    
    func present(error: Error) {
        callPresentErrorCount += 1
        self.error = error
    }
    
    func startLoading() {
        callStartLoadingCount += 1
    }
    
    func stopLoading() {
        callStopLoadingCount += 1
    }
    
    func didNextStep(action: LendingCCBAction) {
        callDidNextStepCount += 1
        self.action = action
    }
}

private final class LendingCCBServiceMock: LendingCCBServicing {
    var fetchCCBExpectedResult: Result<CCBData, ApiError>?
    var sendProposalExpectedResult: Result<NoContent, ApiError>?
    var sendCalledCount = 0
    var sendWithPinCalledCount = 0
    
    func fetchCCB(for proposal: Proposal, completion: @escaping ModelCompletionBlock<CCBData>) {
        callCompletionIfNeeded(completion, expectedResult: fetchCCBExpectedResult)
    }
    
    func send(proposal: Proposal, completion: @escaping ModelCompletionBlock<NoContent>) {
        sendCalledCount += 1
        callCompletionIfNeeded(completion, expectedResult: sendProposalExpectedResult)
    }
    
    func send(proposal: Proposal, pin: String, completion: @escaping ModelCompletionBlock<NoContent>) {
        sendWithPinCalledCount += 1
        callCompletionIfNeeded(completion, expectedResult: sendProposalExpectedResult)
    }
}

final class LendingCCBInteractorTests: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    private let featureManagerMock = FeatureManagerMock()
    private let authenticationContractMock = AuthenticationContractMock()
    private let ppAuthSwiftContractSpy = PPAuthSwiftContractSpy()
    private lazy var dependencyMock = DependencyContainerMock(analyticsSpy,
                                                              featureManagerMock,
                                                              authenticationContractMock,
                                                              ppAuthSwiftContractSpy)
    private lazy var serviceMock = LendingCCBServiceMock()
    private lazy var spy = LendingCCBPresenterSpy()
    private lazy var sut = LendingCCBInteractor(service: serviceMock,
                                                presenter: spy,
                                                proposal: .mock,
                                                dependencies: dependencyMock)
    
    func testFetchDocument_WhenReceiveSuccess_ShouldPresentDocument() {
        let ccb: CCBData = .mock
        serviceMock.fetchCCBExpectedResult = .success(ccb)
        
        sut.fetchDocument()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callStopLoadingCount, 1)
        XCTAssertEqual(spy.callPresentDocumentCount, 1)
        XCTAssertEqual(spy.ccbData, ccb)
    }
    
    func testFetchDocument_WhenReceiveFailure_ShouldPresentError() {
        let error: ApiError = .serverError
        serviceMock.fetchCCBExpectedResult = .failure(error)
        
        sut.fetchDocument()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callStopLoadingCount, 1)
        XCTAssertEqual(spy.callPresentErrorCount, 1)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
    }
    
    func testSendProposalWithAuthentication_WhenReceiveAuthenticationFailure_ShouldPresentError() {
        featureManagerMock.override(with: [.isP2PLendingCCBAuthAvailable: true])
        authenticationContractMock.validAuthentication = false
        
        sut.authAndSendProposal()
        
        XCTAssertEqual(spy.callStartLoadingCount, 0)
        XCTAssertEqual(spy.callStopLoadingCount, 0)
        XCTAssertEqual(spy.callPresentErrorCount, 1)
    }
    
    func testSendProposalWithAuthentication_WhenReceiveInvalidPasswordError_ShouldCallHandlePassword() {
        featureManagerMock.override(with: [.isP2PLendingCCBAuthAvailable: true])
        authenticationContractMock.validAuthentication = true
        let identifiableError: CCBIdentifiableError = .invalidPassword
        var requestError = RequestError()
        requestError.code = identifiableError.rawValue
        serviceMock.sendProposalExpectedResult = .failure(.badRequest(body: requestError))
        
        sut.authAndSendProposal()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callStopLoadingCount, 1)
        XCTAssertEqual(ppAuthSwiftContractSpy.callHandlePasswordCount, 1)
    }
    
    func testSendProposalWithAuthentication_WhenReceiveSuccess_ShouldCallDidNextStepProposalSent() {
        serviceMock.sendProposalExpectedResult = .success(NoContent())
        featureManagerMock.override(with: [.isP2PLendingCCBAuthAvailable: true])
        authenticationContractMock.validAuthentication = true
        let expectedEvent = LendingCCBEvent.sendProposalTapped.event()
        
        
        sut.authAndSendProposal()
        
        XCTAssertEqual(serviceMock.sendWithPinCalledCount, 1)
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callStopLoadingCount, 1)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        guard case .proposalSent = spy.action else {
            XCTFail("wrong action called")
            return
        }
    }
    
    func testSendProposalWithAuthentication_WhenReceiveFailure_ShouldPresentError() {
        let error: ApiError = .serverError
        featureManagerMock.override(with: [.isP2PLendingCCBAuthAvailable: true])
        authenticationContractMock.validAuthentication = true
        serviceMock.sendProposalExpectedResult = .failure(error)
        
        sut.authAndSendProposal()
        
        XCTAssertEqual(serviceMock.sendWithPinCalledCount, 1)
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callStopLoadingCount, 1)
        XCTAssertEqual(spy.callPresentErrorCount, 1)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
    }
    
    func testSendProposalWithoutAuthentication_WhenReceiveSuccess_ShouldCallDidNextStepProposalSent() {
        serviceMock.sendProposalExpectedResult = .success(NoContent())
        featureManagerMock.override(with: [.isP2PLendingCCBAuthAvailable: false])
        let expectedEvent = LendingCCBEvent.sendProposalTapped.event()
        
        
        sut.authAndSendProposal()
        
        XCTAssertEqual(serviceMock.sendCalledCount, 1)
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callStopLoadingCount, 1)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        guard case .proposalSent = spy.action else {
            XCTFail("wrong action called")
            return
        }
    }
    
    func testSendProposalWithoutAuthentication_WhenReceiveFailure_ShouldPresentError() {
        let error: ApiError = .serverError
        featureManagerMock.override(with: [.isP2PLendingCCBAuthAvailable: false])
        serviceMock.sendProposalExpectedResult = .failure(error)
        
        sut.authAndSendProposal()
        
        XCTAssertEqual(serviceMock.sendCalledCount, 1)
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callStopLoadingCount, 1)
        XCTAssertEqual(spy.callPresentErrorCount, 1)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
    }
    
    func testSendAnalyticsEvent_WhenCalledFromBackButtonFromController_ShouldSendEvent() {
        let expectedEvent = LendingCCBEvent.backButtonTapped.event()
        sut.sendAnalyticsEvent(type: .backButtonTapped)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testSendAnalyticsEvent_WhenCalledFromDidScrollToBottomFromController_ShouldSendEvent() {
        let expectedEvent = LendingCCBEvent.didReadContract.event()
        sut.sendAnalyticsEvent(type: .didScrollToBottom)
    
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testLendingSuccessControllerDidDismiss_WhenCalledFromLendingSuccessDelegate_ShouldSendEvent() {
        let expectedEvent = LendingSuccessEvent.didFinishFlow(.borrower).event()
        let fakeViewController = UIViewController()
        sut.lendingSuccessControllerDidDismiss(fakeViewController)
       
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
}
