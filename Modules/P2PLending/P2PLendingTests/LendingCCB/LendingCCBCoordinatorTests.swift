import XCTest
import UI
@testable import P2PLending

final class LendingCCBCoordinatorTests: XCTestCase {
    private lazy var spy = ViewControllerSpy()
    private lazy var urlOpenerSpy = URLOpenerSpy()
    private lazy var sut: LendingCCBCoordinator = {
        let coordinator = LendingCCBCoordinator(dependencies: DependencyContainerMock(urlOpenerSpy))
        coordinator.viewController = spy
        return coordinator
    }()
    
    func testPerformActionProposalSent_WhenCalledFromPresenter_ShouldShowLendingSuccessViewController() {
        let delegate = LendingSuccessDelegateSpy()
        sut.perform(action: .proposalSent(delegate: delegate))
        
        XCTAssertEqual(spy.callShowViewControllerCount, 1)
        XCTAssertTrue(spy.viewController is LendingSuccessViewController)
    }
    
    func testPerformActionDetailedError_WhenCalledFromPresenter_ShouldShowApolloFeedbackController() {
        sut.perform(action: .detailedError(CCBIdentifiableError.currentlyInvestor))
        
        XCTAssertEqual(spy.callShowViewControllerCount, 1)
        XCTAssertTrue(spy.viewController is ApolloFeedbackViewController)
    }

    func testPerformActionAccountUpgradeError_WhenCalledFromPresenter_ShouldShowApolloFeedbackController() {
        sut.perform(action: .accountUpgrade(CCBIdentifiableError.accountUpgrade))
        
        XCTAssertEqual(spy.callShowViewControllerCount, 1)
        XCTAssertTrue(spy.viewController is ApolloFeedbackViewController)
    }

    func testPerformActionMaximumOpenProposalsError_WhenCalledFromPresenter_ShouldShowApolloFeedbackController() {
        sut.perform(action: .maximumOpenProposals(CCBIdentifiableError.openProposalsLimitReached))
        
        XCTAssertEqual(spy.callShowViewControllerCount, 1)
        XCTAssertTrue(spy.viewController is ApolloFeedbackViewController)
    }
}
