import XCTest
import Core
@testable import P2PLending

private final class LendingCCBDisplaySpy: LendingCCBDisplay {
    private(set) var callDisplayDocumentCount = 0
    private(set) var callDisplayErrorCount = 0
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    
    private(set) var htmlString: String?
    private(set) var error: Error?
    
    func displayDocument(htmlString: String) {
        callDisplayDocumentCount += 1
        self.htmlString = htmlString
    }
    
    func display(error: Error) {
        callDisplayErrorCount += 1
        self.error = error
    }
    
    func startLoading() {
        callStartLoadingCount += 1
    }
    
    func stopLoading() {
        callStopLoadingCount += 1
    }
}

private final class LendingCCBCoordinatorSpy: LendingCCBCoordinating {
    var viewController: UIViewController?
    
    private(set) var callPerformActionCount = 0
    
    private(set) var action: LendingCCBAction?
    
    func perform(action: LendingCCBAction) {
        callPerformActionCount += 1
        self.action = action
    }
}

final class LendingCCBPresenterTests: XCTestCase {
    private lazy var spy = LendingCCBDisplaySpy()
    private lazy var coordinatorSpy = LendingCCBCoordinatorSpy()
    private lazy var sut: LendingCCBPresenter = {
        let presenter = LendingCCBPresenter(coordinator: coordinatorSpy)
        presenter.viewController = spy
        return presenter
    }()
    
    func testPresentDocument_WhenCalledFromInteractor_ShouldDisplayHTMLStringWithCCBData() throws {
        let ccbData: CCBData = .mock
        
        sut.presentDocument(ccbData: ccbData)
        
        XCTAssertEqual(spy.callDisplayDocumentCount, 1)
        let htmlString = try XCTUnwrap(spy.htmlString)
        XCTAssertTrue(htmlString.contains(ccbData.borrower.name))
        XCTAssertTrue(htmlString.contains(ccbData.contract.issueCity))
    }
    
    func testPresentError_WhenCalledFromInteractor_ShouldDisplayError() {
        let error: ApiError = .serverError
        
        sut.present(error: error)
        
        XCTAssertEqual(spy.callDisplayErrorCount, 1)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
    }
    
    func testStartLoading_WhenCalledFromInteractor_ShouldStartLoading() {
        sut.startLoading()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
    }
    
    func testStopLoading_WhenCalledFromInteractor_ShouldStopLoading() {
        sut.stopLoading()
        
        XCTAssertEqual(spy.callStopLoadingCount, 1)
    }
    
    func testDidNextStepProposalSent_WhenCalledFromInteractor_ShouldCallPerformNextStepProposalSent() {
        let delegate = LendingSuccessDelegateSpy()
        sut.didNextStep(action: .proposalSent(delegate: delegate))
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        guard case .proposalSent = coordinatorSpy.action else {
            XCTFail("wrong action called")
            return
        }
    }
}
