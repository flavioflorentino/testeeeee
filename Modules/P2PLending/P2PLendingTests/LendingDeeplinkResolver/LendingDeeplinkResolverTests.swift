import XCTest
import FeatureFlag
import AnalyticsModule
@testable import P2PLending

final class LendingDeeplinkResolverTests: XCTestCase {
    private let analytics = AnalyticsSpy()
    private lazy var featureManagerMock = FeatureManagerMock()
    private lazy var sut = LendingDeeplinkResolver(dependencies: DependencyContainerMock(featureManagerMock, analytics))
    
    func testCanHandleURL_WhenReceiveHandleableOpportunityURLAndIsAuthenticatedAndFeatureIsEnabled_ShouldReturnHandelable() throws {
        featureManagerMock.override(key: .releaseP2PLending, with: true)
        let deeplinkPath = "picpay://picpay/p2plending/opportunity?identifier=1e037040-935c-37a7-b4d0-005110fd9e00"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        
        let result = sut.canHandle(url: url, isAuthenticated: true)
        
        XCTAssertEqual(result, .handleable)
    }
    
    func testCanHandleURL_WhenReceiveHandleableNewProposalURLAndIsAuthenticatedAndFeatureIsEnabled_ShouldReturnHandelable() throws {
        featureManagerMock.override(key: .releaseP2PLending, with: true)
        let deeplinkPath = "picpay://picpay/p2plending/new-proposal"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        
        let result = sut.canHandle(url: url, isAuthenticated: true)
        
        XCTAssertEqual(result, .handleable)
    }
    
    func testCanHandleURL_WhenReceiveHandleableOpportunityURLAndIsNotAuthenticatedAndFeatureIsEnabled_ShouldReturnOnlyWithAuth() throws {
        featureManagerMock.override(key: .releaseP2PLending, with: true)
        let deeplinkPath = "picpay://picpay/p2plending/opportunity?identifier=1e037040-935c-37a7-b4d0-005110fd9e00"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        
        let result = sut.canHandle(url: url, isAuthenticated: false)
        
        XCTAssertEqual(result, .onlyWithAuth)
    }
    
    func testCanHandleURL_WhenReceiveHandleableNewProposalURLAndIsNotAuthenticatedAndFeatureIsEnabled_ShouldReturnOnlyWithAuth() throws {
        featureManagerMock.override(key: .releaseP2PLending, with: true)
        let deeplinkPath = "picpay://picpay/p2plending/new-proposal"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        
        let result = sut.canHandle(url: url, isAuthenticated: false)
        
        XCTAssertEqual(result, .onlyWithAuth)
    }
    
    func testCanHandleURL_WhenReceiveHandleableOpportunityURLAndIsNotAuthenticatedAndFeatureIsNotEnabled_ShouldReturnOnlyWithAuth() throws {
        featureManagerMock.override(key: .releaseP2PLending, with: false)
        let deeplinkPath = "picpay://picpay/p2plending/opportunity?identifier=1e037040-935c-37a7-b4d0-005110fd9e00"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        
        let result = sut.canHandle(url: url, isAuthenticated: false)
        
        XCTAssertEqual(result, .onlyWithAuth)
    }
    
    func testCanHandleURL_WhenReceiveHandleableNewProposalURLAndIsNotAuthenticatedAndFeatureIsNotEnabled_ShouldReturnOnlyWithAuth() throws {
        featureManagerMock.override(key: .releaseP2PLending, with: false)
        let deeplinkPath = "picpay://picpay/p2plending/new-proposal"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        
        let result = sut.canHandle(url: url, isAuthenticated: false)
        
        XCTAssertEqual(result, .onlyWithAuth)
    }
    
    func testCanHandleURL_WhenReceiveHandleableOpportunityURLAndIsAuthenticatedAndFeatureIsNotEnabled_ShouldReturnNotHandleable() throws {
        featureManagerMock.override(key: .releaseP2PLending, with: false)
        let deeplinkPath = "picpay://picpay/p2plending/opportunity?identifier=1e037040-935c-37a7-b4d0-005110fd9e00"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        
        let result = sut.canHandle(url: url, isAuthenticated: true)
        
        XCTAssertEqual(result, .notHandleable)
    }
    
    func testCanHandleURL_WhenReceiveHandleableNewProposalURLAndIsAuthenticatedAndFeatureIsNotEnabled_ShouldReturnNotHandleable() throws {
        featureManagerMock.override(key: .releaseP2PLending, with: false)
        let deeplinkPath = "picpay://picpay/p2plending/new-proposal"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        
        let result = sut.canHandle(url: url, isAuthenticated: true)
        
        XCTAssertEqual(result, .notHandleable)
    }
    
    func testCanHandleURL_WhenReceiveHandleableInstallmentDetaildAndIsAuthenticatedAndFeatureIsNotEnabled_ShouldReturnNotHandleable() throws {
        featureManagerMock.override(key: .releaseP2PLending, with: false)
        let deeplinkPath = "picpay://picpay/p2plending/installment-payment?identifier=4387855D-9CDC-451E-A7D3-6B9C0A63D316&number=1"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        
        let result = sut.canHandle(url: url, isAuthenticated: true)
        
        XCTAssertEqual(result, .notHandleable)
    }
    
    func testCanHandleURL_WhenReceiveNotHandleableURLAndIsAuthenticatedAndFeatureIsEnabled_ShouldReturnNotHandleable() throws {
        featureManagerMock.override(key: .releaseP2PLending, with: true)
        let deeplinkPath = "picpay://picpay/someOther/opportunity?identifier=1e037040-935c-37a7-b4d0-005110fd9e00"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        let result = sut.canHandle(url: url, isAuthenticated: true)
        
        XCTAssertEqual(result, .notHandleable)
    }
    
    func testOpenOpportunityDeeplink_WhenReceiveOpportunityPathAndIsAuthenticated_ShouldPresentInvestorGreetings() throws {
        let deeplinkPath = "picpay://picpay/p2plending/opportunity?identifier=1e037040-935c-37a7-b4d0-005110fd9e00"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        
        let injectedNavigationController = NavigationControllerSpy()
        
        let navigationContractSpy = NavigationContractSpy()
        navigationContractSpy.getCurrentNavigationReturnValue = injectedNavigationController
        P2PLendingSetup.inject(instance: navigationContractSpy)
        
        let opened = sut.open(url: url, isAuthenticated: true)
        
        XCTAssertTrue(opened)
        
        let navigationController = try XCTUnwrap(injectedNavigationController.viewController as? UINavigationController)
        XCTAssertEqual(injectedNavigationController.callPresentViewControllerCount, 1)
        let viewController = try XCTUnwrap(navigationController.topViewController as? LendingInvestorGreetingsViewController)
        XCTAssertNotNil(viewController)
        XCTAssertEqual(analytics.logCalledCount, 1)
    }
    
    func testOpenNewProposalDeeplink_WhenReceiveNewProposalPathAndIsAuthenticated_ShouldPresentBorrowerGreetings() throws {
        let deeplinkPath = "picpay://picpay/p2plending/new-proposal"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        
        let injectedNavigationController = NavigationControllerSpy()
        
        let navigationContractSpy = NavigationContractSpy()
        navigationContractSpy.getCurrentNavigationReturnValue = injectedNavigationController
        P2PLendingSetup.inject(instance: navigationContractSpy)
        
        let opened = sut.open(url: url, isAuthenticated: true)
        
        XCTAssertTrue(opened)
        
        let navigationController = try XCTUnwrap(injectedNavigationController.viewController as? UINavigationController)
        XCTAssertEqual(injectedNavigationController.callPresentViewControllerCount, 1)
        let viewController = try XCTUnwrap(navigationController.topViewController as? LendingGreetingsViewController)
        XCTAssertNotNil(viewController)
    }
    
    func testOpenOpportunityDeeplink_WhenReceiveOpportunityPathAndIsNotAuthenticated_ShouldDoNothing() throws {
        let deeplinkPath = "picpay://picpay/p2plending/opportunity?identifier=1e037040-935c-37a7-b4d0-005110fd9e00"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        
        let opened = sut.open(url: url, isAuthenticated: false)
        
        XCTAssertFalse(opened)
        XCTAssertEqual(analytics.logCalledCount, 0)
    }
    
    func testOpenNewProposalDeeplink_WhenReceiveNewProposalPathAndIsNotAuthenticated_ShouldDoNothing() throws {
        let deeplinkPath = "picpay://picpay/p2plending/opportunity"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        
        let opened = sut.open(url: url, isAuthenticated: false)
        
        XCTAssertFalse(opened)
    }
    
    func testOpenOpportunityDeeplink_WhenReceiveWrongPathAndIsAuthenticated_ShouldDoNothing() throws {
        let deeplinkPath = "picpay://picpay/some/opportunity?identifier=1e037040-935c-37a7-b4d0-005110fd9e00"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        
        let opened = sut.open(url: url, isAuthenticated: true)
        
        XCTAssertFalse(opened)
        XCTAssertEqual(analytics.logCalledCount, 0)
    }
    
    func testOpenOpportunityDeeplink_WhenOpportunityPathAndIsAuthenticatedAndIsMissingParameters_ShouldDoNothing() throws {
        let deeplinkPath = "picpay://picpay/p2plending/opportunity"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        
        let opened = sut.open(url: url, isAuthenticated: true)
        
        XCTAssertFalse(opened)
        XCTAssertEqual(analytics.logCalledCount, 0)
    }
    
    func testOpenOpportunityDeeplink_WhenOpportunityPathAndIsAuthenticatedAndHasWrongParameters_ShouldDoNothing() throws {
        let deeplinkPath = "picpay://picpay/p2plending/opportunity?identifier=1234"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        
        let opened = sut.open(url: url, isAuthenticated: true)
        
        XCTAssertFalse(opened)
        XCTAssertEqual(analytics.logCalledCount, 0)
    }
    
    func testCanHandleInvestmentsDeeplink_WhenFeatureIsEnabled_ShouldReturnHandleable() throws {
        featureManagerMock.override(key: .releaseP2PLending, with: true)
        let deeplinkPath = "picpay://picpay/p2plending/investments"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        
        let result = sut.canHandle(url: url, isAuthenticated: true)
        
        XCTAssertEqual(result, .handleable)
    }
    
    func testCanHandleInvestmentsDeeplink_WhenFeatureIsNotEnabled_ShouldReturnNotHandleable() throws {
        featureManagerMock.override(key: .releaseP2PLending, with: false)
        let deeplinkPath = "picpay://picpay/p2plending/investments"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        
        let result = sut.canHandle(url: url, isAuthenticated: true)
        
        XCTAssertEqual(result, .notHandleable)
    }
    
    func testCanHandleInvestmentsDeeplink_WhenFeatureIsEnabledAndNotAuthenticated_ShouldReturnOnlyWithAuth() throws {
        featureManagerMock.override(key: .releaseP2PLending, with: true)
        let deeplinkPath = "picpay://picpay/p2plending/investments"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        
        let result = sut.canHandle(url: url, isAuthenticated: false)
        
        XCTAssertEqual(result, .onlyWithAuth)
    }
    
    func testOpenInvestmentsDeeplink_WhenIsAuthenticated_ShouldPresentProposalList() throws {
        let deeplinkPath = "picpay://picpay/p2plending/investments"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        let injectedNavigationController = NavigationControllerSpy()
        let navigationContractSpy = NavigationContractSpy()
        navigationContractSpy.getCurrentNavigationReturnValue = injectedNavigationController
        P2PLendingSetup.inject(instance: navigationContractSpy)
        
        let opened = sut.open(url: url, isAuthenticated: true)
        
        XCTAssertTrue(opened)
    }
    
    func testOpenInvestmentsDeeplink_WhenIsNotAuthenticated_ShouldDoNothing() throws {
        let deeplinkPath = "picpay://picpay/p2plending/investments"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        
        let opened = sut.open(url: url, isAuthenticated: false)
        
        XCTAssertFalse(opened)
    }
    
    func testCanHandleProposalsDeeplink_WhenFeatureIsEnabled_ShouldReturnHandleable() throws {
        featureManagerMock.override(key: .releaseP2PLending, with: true)
        let deeplinkPath = "picpay://picpay/p2plending/proposals"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        
        let result = sut.canHandle(url: url, isAuthenticated: true)
        
        XCTAssertEqual(result, .handleable)
    }
    
    func testCanHandleProposalsDeeplink_WhenFeatureIsNotEnabled_ShouldReturnNotHandleable() throws {
        featureManagerMock.override(key: .releaseP2PLending, with: false)
        let deeplinkPath = "picpay://picpay/p2plending/proposals"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        
        let result = sut.canHandle(url: url, isAuthenticated: true)
        
        XCTAssertEqual(result, .notHandleable)
    }
    
    func testCanHandleProposalsDeeplink_WhenFeatureIsEnabledAndNotAuthenticated_ShouldReturnOnlyWithAuth() throws {
        featureManagerMock.override(key: .releaseP2PLending, with: true)
        let deeplinkPath = "picpay://picpay/p2plending/proposals"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        
        let result = sut.canHandle(url: url, isAuthenticated: false)
        
        XCTAssertEqual(result, .onlyWithAuth)
    }
    
    func testOpenProposalsDeeplink_WhenIsAuthenticated_ShouldPresentProposalList() throws {
        let deeplinkPath = "picpay://picpay/p2plending/proposals"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        let injectedNavigationController = NavigationControllerSpy()
        let navigationContractSpy = NavigationContractSpy()
        navigationContractSpy.getCurrentNavigationReturnValue = injectedNavigationController
        P2PLendingSetup.inject(instance: navigationContractSpy)
        
        let opened = sut.open(url: url, isAuthenticated: true)
        
        XCTAssertTrue(opened)
    }
    
    func testOpenProposalsDeeplink_WhenIsNotAuthenticated_ShouldDoNothing() throws {
        let deeplinkPath = "picpay://picpay/p2plending/proposals"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        
        let opened = sut.open(url: url, isAuthenticated: false)
        
        XCTAssertFalse(opened)
    }
    
    func testCanHandleOnboardingDeeplink_WhenFeatureIsEnabled_ShouldReturnHandleable() throws {
        featureManagerMock.override(key: .releaseP2PLending, with: true)
        let deeplinkPath = "picpay://picpay/p2plending/onboarding"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        
        let result = sut.canHandle(url: url, isAuthenticated: true)
        
        XCTAssertEqual(result, .handleable)
    }
    
    func testCanHandleOnboardingDeeplink_WhenFeatureIsNotEnabled_ShouldReturnNotHandleable() throws {
        featureManagerMock.override(key: .releaseP2PLending, with: false)
        let deeplinkPath = "picpay://picpay/p2plending/onboarding"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        
        let result = sut.canHandle(url: url, isAuthenticated: true)
        
        XCTAssertEqual(result, .notHandleable)
    }
    
    func testCanHandleOnboardingDeeplink_WhenFeatureIsEnabledAndNotAuthenticated_ShouldReturnOnlyWithAuth() throws {
        featureManagerMock.override(key: .releaseP2PLending, with: true)
        let deeplinkPath = "picpay://picpay/p2plending/onboarding"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        
        let result = sut.canHandle(url: url, isAuthenticated: false)
        
        XCTAssertEqual(result, .onlyWithAuth)
    }
    
    func testOpenOnboardingDeeplink_WhenIsAuthenticated_ShouldPresentP2PLendingOnboarding() throws {
        let deeplinkPath = "picpay://picpay/p2plending/onboarding"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        let injectedNavigationController = NavigationControllerSpy()
        let navigationContractSpy = NavigationContractSpy()
        navigationContractSpy.getCurrentNavigationReturnValue = injectedNavigationController
        P2PLendingSetup.inject(instance: navigationContractSpy)
        
        let opened = sut.open(url: url, isAuthenticated: true)
        
        XCTAssertTrue(opened)
    }
    
    func testOpenOnboardingDeeplink_WhenIsNotAuthenticated_ShouldDoNothing() throws {
        let deeplinkPath = "picpay://picpay/p2plending/onboarding"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        
        let opened = sut.open(url: url, isAuthenticated: false)
        
        XCTAssertFalse(opened)
    }
    
    func testOpenInstallmentDetailDeeplink_WhenIsAuthenticated_ShouldPresentInstallmentDetail() throws {
        let deeplinkPath = "picpay://picpay/p2plending/installment-payment?identifier=4387855D-9CDC-451E-A7D3-6B9C0A63D316&number=1"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        let injectedNavigationController = NavigationControllerSpy()
        let navigationContractSpy = NavigationContractSpy()
        navigationContractSpy.getCurrentNavigationReturnValue = injectedNavigationController
        P2PLendingSetup.inject(instance: navigationContractSpy)
        
        let opened = sut.open(url: url, isAuthenticated: true)
        
        XCTAssertTrue(opened)
    }
    
    func testOpenInstallmentDetailDeeplink_WhenIsNotAuthenticated_ShouldDoNothing() throws {
        let deeplinkPath = "picpay://picpay/p2plending/installment-payment?identifier=4387855D-9CDC-451E-A7D3-6B9C0A63D316&number=1"
        let url = try XCTUnwrap(URL(string: deeplinkPath))
        
        let opened = sut.open(url: url, isAuthenticated: false)
        
        XCTAssertFalse(opened)
    }
}
