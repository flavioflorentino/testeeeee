import XCTest
import AnalyticsModule
@testable import P2PLending

final class LendingOnboardingPresenterSpy: LendingOnboardingPresenting {
    var viewController: LendingOnboardingDisplaying?
    
    private(set) var callPresentOnboardingItemsCount = 0
    private(set) var callDidNextStepCount = 0
    
    private(set) var onboardingItems: [OnboardingItem]?
    private(set) var action: LendingOnboardingAction?
    
    func present(onboardingItems: [OnboardingItem]) {
        callPresentOnboardingItemsCount += 1
        self.onboardingItems = onboardingItems
    }
    
    func didNextStep(action: LendingOnboardingAction) {
        callDidNextStepCount += 1
        self.action = action
    }
}

final class LendingOnboardingInteractorTests: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    private lazy var dependencyMock = DependencyContainerMock(analyticsSpy)
    private lazy var spy = LendingOnboardingPresenterSpy()
    private lazy var sut = LendingOnboardingInteractor(presenter: spy, dependencies: dependencyMock)
    
    func testFetchOnboardingItems_WhenCalledFromController_ShouldPresentOnboardingItems() {
        let expectedEvent = LendingOnboardingEvent.p2pLendingHubViewed.event()
        let onboardingItems = OnboardingItem.allCases
        
        sut.fetchOnboardingItems()
        
        XCTAssertEqual(spy.callPresentOnboardingItemsCount, 1)
        XCTAssertEqual(spy.onboardingItems, onboardingItems)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }
    
    func testShowHelp_WhenCalledFromController_ShouldCallDidNextStepShowFAQAction() throws {
        let expectedEvent = LendingOnboardingEvent.faq.event()
        let url = try XCTUnwrap(URL(string: "picpay://picpay/helpcenter/category/360003819012"))
        
        sut.showHelp()
        
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        XCTAssertEqual(spy.action, .showFAQ(url: url))
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }
    
    func testShowOnboardingItem_WhenCalledFromController_ShouldCallDidNextStepShowItem() {
        let indexPath = IndexPath(row: 0, section: 1)
        let item = OnboardingItem.allCases[indexPath.row]
        let expectedEvent = LendingOnboardingEvent.newProposal.event()
        
        sut.showItem(at: indexPath)
        
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        XCTAssertEqual(spy.action, .show(item: item))
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }
}
