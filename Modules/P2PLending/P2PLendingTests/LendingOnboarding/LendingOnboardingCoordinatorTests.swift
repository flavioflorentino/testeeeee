import XCTest
@testable import P2PLending

final class LendingOnboardingCoordinatorTests: XCTestCase {
    private lazy var spy = ViewControllerSpy()
    private lazy var urlOpener = URLOpenerSpy()
    private lazy var sut: LendingOnboardingCoordinator = {
        let coordinator = LendingOnboardingCoordinator(dependencies: DependencyContainerMock(urlOpener))
        coordinator.viewController = spy
        return coordinator
    }()
    
    func testShowItemNewProposal_WhenCalledFromPresenter_ShouldPresentLendingGreetings() throws {
        sut.perform(action: .show(item: .newProposal))
        
        XCTAssertEqual(spy.callPresentViewControllerCount, 1)
        let navigationController = try XCTUnwrap(spy.viewController as? UINavigationController)
        let rootViewController = try XCTUnwrap(navigationController.viewControllers.first)
        XCTAssertTrue(rootViewController is LendingGreetingsViewController)
    }
    
    func testShowItemProposals_WhenCalledFromPresenter_ShouldShowLendingProposalList() {
        sut.perform(action: .show(item: .proposals))
        
        XCTAssertEqual(spy.callShowViewControllerCount, 1)
        XCTAssertTrue(spy.viewController is LendingProposalListViewController)
    }
    
    func testShowItemInvestments_WhenCalledFromPresenter_ShouldShowLendingProposalList() {
        sut.perform(action: .show(item: .investments))
        
        XCTAssertEqual(spy.callShowViewControllerCount, 1)
        XCTAssertTrue(spy.viewController is LendingProposalListViewController)
    }
    
    func testShowFAQ_WhenCalledFromPresenter_ShouldOpenURL() throws {
        let url = try XCTUnwrap(URL(string: "https://www.picpay.com"))
        
        sut.perform(action: .showFAQ(url: url))
        
        XCTAssertEqual(urlOpener.callCanOpenURLCount, 1)
        XCTAssertEqual(urlOpener.callOpenURLCount, 1)
        XCTAssertEqual(urlOpener.checkedURL, url)
        XCTAssertEqual(urlOpener.openedURL, url)
    }
}
