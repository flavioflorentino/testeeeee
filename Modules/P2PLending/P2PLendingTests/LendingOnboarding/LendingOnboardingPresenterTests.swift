import XCTest
import LendingComponents
@testable import P2PLending

final class LendingOnboardingDisplaySpy: LendingOnboardingDisplaying {
    private(set) var callDisplayItemsCount = 0
    
    private(set) var items: [LendingProductCellViewModeling]?
    
    func display(items: [LendingProductCellViewModeling]) {
        callDisplayItemsCount += 1
        self.items = items
    }
}

final class LendingOnboardingCoordinatorSpy: LendingOnboardingCoordinating {
    var viewController: UIViewController?
    
    private(set) var callPerformNextStepCount = 0
    
    private(set) var action: LendingOnboardingAction?
    
    func perform(action: LendingOnboardingAction) {
        callPerformNextStepCount += 1
        self.action = action
    }
}

final class LendingOnboardingPresenterTests: XCTestCase {
    private lazy var spy = LendingOnboardingDisplaySpy()
    private lazy var coordinatorSpy = LendingOnboardingCoordinatorSpy()
    private lazy var sut: LendingOnboardingPresenter = {
        let presenter = LendingOnboardingPresenter(coordinator: coordinatorSpy)
        presenter.viewController = spy
        return presenter
    }()
    
    func testPresentOnboardingItem_WhenCalledFromInteractor_ShouldDisplayItems() throws {
        let items = OnboardingItem.allCases
        
        sut.present(onboardingItems: items)
        
        let presentedItemsViewModel = try XCTUnwrap(spy.items)
        let presentedItems = try XCTUnwrap(presentedItemsViewModel as? [OnboardingItem])
        
        XCTAssertEqual(spy.callDisplayItemsCount, 1)
        XCTAssertEqual(presentedItems, items)
    }
    
    func testDidNextStepShowItem_WhenCalledFromInteractor_ShouldCallPerformActionShowItem() {
        let item: OnboardingItem = .proposals
        
        sut.didNextStep(action: .show(item: item))
        
        XCTAssertEqual(coordinatorSpy.callPerformNextStepCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .show(item: item))
    }
    
    func testDidNextStepShowFAQ_WhenCalledFromInteractor_ShouldCallPerformActionShowFAQ() throws {
        let url = try XCTUnwrap(URL(string: "https://www.picpay.com"))
        
        sut.didNextStep(action: .showFAQ(url: url))
        
        XCTAssertEqual(coordinatorSpy.callPerformNextStepCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .showFAQ(url: url))
    }
}
