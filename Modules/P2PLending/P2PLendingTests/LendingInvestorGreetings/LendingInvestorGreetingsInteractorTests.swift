import Core
import XCTest
import AnalyticsModule
@testable import P2PLending

private final class LendingInvestorGreetingsPresenterSpy: LendingInvestorGreetingsPresenting {
    var viewController: LendingInvestorGreetingsDisplay?
    
    private(set) var callPresentContentCount = 0
    private(set) var callPresentCrednovoDislaimerCount = 0
    private(set) var callSetContinueButtonEnabledCount = 0
    private(set) var callPresentErrorCount = 0
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    private(set) var callDidNextStepCount = 0
    
    private(set) var action: LendingInvestorGreetingsAction?
    private(set) var isContinueButtonEnabled: Bool?
    private(set) var error: Error?
    
    func presentContent(termsOfUseURL: URL, privacyPolicyURL: URL) {
        callPresentContentCount += 1
    }
    
    func presentCrednovoDisclaimer() {
        callPresentCrednovoDislaimerCount += 1
    }
    
    func setContinueButton(enabled: Bool) {
        callSetContinueButtonEnabledCount += 1
        isContinueButtonEnabled = enabled
    }
    
    func present(error: Error) {
        callPresentErrorCount += 1
        self.error = error
    }
    
    func startLoading() {
        callStartLoadingCount += 1
    }
    
    func stopLoading() {
        callStopLoadingCount += 1
    }
    
    func didNextStep(action: LendingInvestorGreetingsAction) {
        callDidNextStepCount += 1
        self.action = action
    }
}

final class LendingInvestorGreetingsServiceMock: LendingInvestorGreetingsServicing {
    var fetchOpportunityOfferExpectedResult: Result<OpportunityOffer, ApiError>?
    
    func fetchOpportunityOffer(with identifier: UUID, completion: @escaping ModelCompletionBlock<OpportunityOffer>) {
        callCompletionIfNeeded(completion, expectedResult: fetchOpportunityOfferExpectedResult)
    }
}

final class LendingInvestorGreetingsInteractorTests: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    private lazy var dependencyMock = DependencyContainerMock(analyticsSpy)
    private lazy var identifier = UUID()
    private lazy var serviceMock = LendingInvestorGreetingsServiceMock()
    private lazy var spy = LendingInvestorGreetingsPresenterSpy()
    private lazy var sut = LendingInvestorGreetingsInteractor(presenter: spy,
                                                              service: serviceMock,
                                                              offerIdentifier: identifier,
                                                              dependencies: dependencyMock)
    
    func testFetchContent_WhenReceiveSuccessAndWaitingReplyStatus_ShouldPresentContent() throws {
        let offer = try OpportunityOffer.mock(for: .waitingReply)
        serviceMock.fetchOpportunityOfferExpectedResult = .success(offer)
       
        sut.fetchContent()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callStopLoadingCount, 1)
        XCTAssertEqual(spy.callPresentContentCount, 1)
    }
    
    func testFetchContent_WhenReceiveSuccessAndNotWaitingReplyStatus_ShouldCallDidNextStepShowOpportunityWithoutAnimation() throws {
        let offer = try OpportunityOffer.mock(for: .declined)
        serviceMock.fetchOpportunityOfferExpectedResult = .success(offer)
        
        sut.fetchContent()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callStopLoadingCount, 1)
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        guard case let .showOpportunity(identifier, animated) = spy.action else {
            XCTFail("wrong action called")
            return
        }
        XCTAssertEqual(identifier, self.identifier)
        XCTAssertFalse(animated)
    }
    
    func testFetchContent_WhenReceiveFailure_ShouldPresentError() {
        let error: ApiError = .bodyNotFound
        serviceMock.fetchOpportunityOfferExpectedResult = .failure(error)
        
        sut.fetchContent()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callStopLoadingCount, 1)
        XCTAssertEqual(spy.callPresentErrorCount, 1)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
    }
    
    func testAwarenessAndAcceptanceOfTermsChanged_WhenAcceptedIsTrue_ShouldSetContinueButtonEnabled() {
        let expectedEvent = LendingGreetingsEvent.termsAndPrivacyPolicyCheckBox(.investor).event()
        let accepted = true
        
        sut.awarenessAndAcceptanceOfTermsChanged(accepted)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(spy.callSetContinueButtonEnabledCount, 1)
        XCTAssertEqual(spy.isContinueButtonEnabled, accepted)
    }
    
    func testAwarenessAndAcceptanceOfTermsChanged_WhenAcceptedIsFalse_ShouldSetContinueButtonDisabled() {
        let expectedEvent = LendingGreetingsEvent.termsAndPrivacyPolicyCheckBox(.investor).event()
        let accepted = false
        
        sut.awarenessAndAcceptanceOfTermsChanged(accepted)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(spy.callSetContinueButtonEnabledCount, 1)
        XCTAssertEqual(spy.isContinueButtonEnabled, accepted)
    }
    
    func testShowCrednovoDisclaimer_WhenCalledFromViewController_ShouldPresentCrednovoDisclaimer() {
        let expectedEvent = LendingGreetingsEvent.crednovo(.investor).event()
        sut.showCrednovoDisclaimer()
        
        XCTAssertEqual(spy.callPresentCrednovoDislaimerCount, 1)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testShowLegalTerms_WhenCalledFromViewController_ShouldShowLegalTerms() throws {
        let offer = try OpportunityOffer.mock(for: .waitingReply)
        serviceMock.fetchOpportunityOfferExpectedResult = .success(offer)
        sut.fetchContent()
        
        sut.showLegalTerm(at: offer.privacyPolicy)
        
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        guard case let .showLegalTerms(url) = spy.action else {
            XCTFail("wrong action called")
            return
        }
        
        XCTAssertEqual(url, offer.privacyPolicy)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }
    
    func testClose_WhenCalledFromController_ShouldCallDidNextStepCloseWithoutMarkingSceneAsSeen() {
        let expectedEvent = LendingGreetingsEvent.closeButtonTapped(.investor).event()
        sut.close()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        guard case .close = spy.action else {
            XCTFail("wrong action called")
            return
        }
    }
    
    func testShowHelp_WhenCalledFromController_ShouldShowCallDidNextStepHelp() {
        let expectedEvent = LendingGreetingsEvent.helpButtonTapped(.investor).event()
        sut.showHelp()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        guard case .showFAQ = spy.action else {
            XCTFail("wrong action called")
            return
        }
    }
    
    func testShowOpportunity_WhenCalledFromController_ShouldCallDidNextStepShowOpportunityAndMarkSceneAsSeen() {
        let expectedEvent = LendingGreetingsEvent.continueButtonTapped(.investor).event()
        sut.showOpportunity()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        guard case let .showOpportunity(identifier, animated) = spy.action else {
            XCTFail("wrong action called")
            return
        }
        XCTAssertEqual(identifier, self.identifier)
        XCTAssertTrue(animated)
    }
}
