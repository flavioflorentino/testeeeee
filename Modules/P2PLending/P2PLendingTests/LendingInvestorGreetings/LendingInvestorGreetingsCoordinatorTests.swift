import XCTest
import SafariServices
@testable import P2PLending

final class LendingInvestorGreetingsCoordinatorTests: XCTestCase {
     private lazy var spy: ViewControllerSpy = {
        guard let viewController = navigationControllerSpy.viewController as? ViewControllerSpy else {
            fatalError()
        }
        return viewController
    }()
    private lazy var navigationControllerSpy = NavigationControllerSpy(rootViewController: ViewControllerSpy())
    private lazy var urlOpener = URLOpenerSpy()
    private lazy var offerIdentifier = UUID()
    private lazy var sut: LendingInvestorGreetingsCoordinator = {
        let coordinator = LendingInvestorGreetingsCoordinator(dependencies: DependencyContainerMock(urlOpener))
        coordinator.viewController = spy
        return coordinator
    }()
    private lazy var window = UIWindow(frame: UIScreen.main.bounds)
    
    func testPerformActionShowOpportunityAnimated_WhenCalledFromPresenter_ShouldPushOpportunityControllerAnimated() {
        let animated = true
        
        sut.perform(action: .showOpportunity(identifier: UUID(), animated: animated))
        
        XCTAssertEqual(navigationControllerSpy.callPushViewControllerCount, 2) // 2 é necessário pq o `init(rootViewController:)` da navigation já faz um push da rootViewController
        XCTAssertTrue(navigationControllerSpy.viewController is LendingOpportunityOfferViewController)
        XCTAssertEqual(navigationControllerSpy.animated, animated)
    }
    
    func testPerformActionShowOpportunityNotAnimated_WhenCalledFromPresenter_ShouldPushOpportunityControllerNotAnimated() {
        let animated = false
        
        sut.perform(action: .showOpportunity(identifier: UUID(), animated: animated))
        
        XCTAssertEqual(navigationControllerSpy.callPushViewControllerCount, 2)
        XCTAssertTrue(navigationControllerSpy.viewController is LendingOpportunityOfferViewController)
        XCTAssertEqual(navigationControllerSpy.animated, animated)
    }
    
    func testPerformActionShowLegalTerms_WhenCalledFromPresenter_ShouldShowSafariViewController() throws {
        let url = try XCTUnwrap(URL(string: "https://www.picpay.com"))
        
        sut.perform(action: .showLegalTerms(url: url))
        
        XCTAssertEqual(spy.callPresentViewControllerCount, 1)
        XCTAssertTrue(spy.viewController is SFSafariViewController)
    }
    
    func testPerformActionClose_WhenCalledFromPresenter_ShouldDismissViewController() {
        sut.perform(action: .close)
        
        XCTAssertEqual(spy.callDismissCount, 1)
    }
    
    func testPerformActionHelp_WhenCalledFromPresenter_ShouldPresentHelpViewController() throws {
        let url = try XCTUnwrap(URL(string: "https://www.picpay.com"))
        
        sut.perform(action: .showFAQ(url: url))
        
        XCTAssertEqual(urlOpener.callCanOpenURLCount, 1)
        XCTAssertEqual(urlOpener.callOpenURLCount, 1)
        XCTAssertEqual(urlOpener.checkedURL, url)
        XCTAssertEqual(urlOpener.openedURL, url)
    }
}
