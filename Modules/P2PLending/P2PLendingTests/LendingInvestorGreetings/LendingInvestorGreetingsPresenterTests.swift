import XCTest
import Core
@testable import P2PLending

private final class LendingInvestorGreetingsCoordinatorSpy: LendingInvestorGreetingsCoordinating {
    var viewController: UIViewController?
    
    private(set) var callPerformActionCount = 0
    private(set) var action: LendingInvestorGreetingsAction?
    
    func perform(action: LendingInvestorGreetingsAction) {
        callPerformActionCount += 1
        self.action = action
    }
}

final class LendingInvestorGreetingsDisplaySpy: LendingInvestorGreetingsDisplay {
    private(set) var callDisplayContentCount = 0
    private(set) var callDisplayCrednovoDisclaimerCount = 0
    private(set) var callSetContinueButtonEnabledCount = 0
    private(set) var callDisplayErrorCount = 0
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    
    private(set) var content: LendingInvestorGreetingsViewModel?
    private(set) var isContinueButtonEnabled: Bool?
    private(set) var error: Error?
    
    func display(content: LendingInvestorGreetingsViewModel) {
        callDisplayContentCount += 1
        self.content = content
    }
    
    func displayCrednovoDisclaimer() {
        callDisplayCrednovoDisclaimerCount += 1
    }
    
    func setContinueButton(enabled: Bool) {
        callSetContinueButtonEnabledCount += 1
        isContinueButtonEnabled = enabled
    }
    
    func display(error: Error) {
        callDisplayErrorCount += 1
        self.error = error
    }
    
    func startLoading() {
        callStartLoadingCount += 1
    }
    
    func stopLoading() {
        callStopLoadingCount += 1
    }
}

final class LendingInvestorGreetingsPresenterTests: XCTestCase {
    private typealias Localizable = Strings.Lending.Investor.Greetings
    private lazy var coordinatorSpy = LendingInvestorGreetingsCoordinatorSpy()
    private lazy var spy = LendingInvestorGreetingsDisplaySpy()
    private lazy var sut: LendingInvestorGreetingsPresenter = {
        let presenter = LendingInvestorGreetingsPresenter(coordinator: coordinatorSpy)
        presenter.viewController = spy
        return presenter
    }()
    
    func testPresentContent_WhenCalledFromInteractor_ShouldDisplayContent() throws {
        let offer = try OpportunityOffer.mock()
        sut.presentContent(termsOfUseURL: offer.termsConditions, privacyPolicyURL: offer.privacyPolicy)
        
        XCTAssertEqual(spy.callDisplayContentCount, 1)
        XCTAssertEqual(spy.content?.title, Localizable.title)
        XCTAssertEqual(spy.content?.footer.string, Localizable.footer)
        XCTAssertEqual(spy.content?.legalTermsNotice.string, Localizable.legalTermsNotice)
    }
    
    func testPresentCrednovoDisclaimer_WhenCalledFromInteractor_ShouldDisplayCrednovoDisclaimer() {
        sut.presentCrednovoDisclaimer()
        
        XCTAssertEqual(spy.callDisplayCrednovoDisclaimerCount, 1)
    }
    
    func testSetContinueButtonEnabled_WhenEnabledIsTrue_ShouldSetContinueButtonEnabledTrue() {
        let enabled = true
        
        sut.setContinueButton(enabled: enabled)
        
        XCTAssertEqual(spy.callSetContinueButtonEnabledCount, 1)
        XCTAssertEqual(spy.isContinueButtonEnabled, enabled)
    }
    
    func testSetContinueButtonEnabled_WhenEnabledIsFalse_ShouldSetContinueButtonEnabledFalse() {
        let enabled = false
        
        sut.setContinueButton(enabled: enabled)
        
        XCTAssertEqual(spy.callSetContinueButtonEnabledCount, 1)
        XCTAssertEqual(spy.isContinueButtonEnabled, enabled)
    }
    
    func testPresentError_WhenCalledFromInteractor_ShouldDisplayError() {
        let error: ApiError = .bodyNotFound
        
        sut.present(error: error)
        
        XCTAssertEqual(spy.callDisplayErrorCount, 1)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
    }
    
    func testStartLoading_WhenCalledFromInteractor_ShouldStartLoading() {
        sut.startLoading()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
    }
    
    func testStopLoading_WhenCalledFromInteractor_ShouldStopLoading() {
        sut.stopLoading()
        
        XCTAssertEqual(spy.callStopLoadingCount, 1)
    }
    
    func testDidNextStepClose_WhenCalledFromInteractor_ShouldPerformActionClose() {
        sut.didNextStep(action: .close)
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        guard case .close = coordinatorSpy.action else {
            XCTFail("wrong action called")
            return
        }
    }
    
    func testDidNextStepHelp_WhenCalledFromInteractor_ShouldPerformActionHelp() throws {
        let url = try XCTUnwrap(URL(string: "https://www.picpay.com"))
        
        sut.didNextStep(action: .showFAQ(url: url))
         
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        guard case .showFAQ = coordinatorSpy.action else {
            XCTFail("wrong action called")
            return
        }
    }
    
    func testDidNextStepShowOpportunity_WhenCalledFromInteractor_ShouldPerformActionShowOpportunity() throws {
        let expectedIdentifier = UUID()
        let expectedAnimationFlag = true
        
        sut.didNextStep(action: .showOpportunity(identifier: expectedIdentifier, animated: expectedAnimationFlag))
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        guard case let .showOpportunity(identifier, animated) = coordinatorSpy.action else {
            XCTFail("wrong action called")
            return
        }
        
        XCTAssertEqual(identifier, expectedIdentifier)
        XCTAssertEqual(animated, expectedAnimationFlag)
    }
}
