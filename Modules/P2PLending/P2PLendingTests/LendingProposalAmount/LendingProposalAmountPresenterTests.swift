import Core
import XCTest
@testable import P2PLending

final class LendingProposalViewControllerSpy: UIViewController, LendingProposalAmountDisplay {
    private(set) var callDisplayAmountValueInstructionsCount = 0
    private(set) var callDisplayPaymentDateInstructionsCount = 0
    private(set) var callSetContinueButtonEnabledCount = 0
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    private(set) var callDisplayErrorCount = 0
    
    private(set) var amountValueInstructionsAttributedString: NSAttributedString?
    private(set) var paymentDateInstrictionsAttributedString: NSAttributedString?
    private(set) var continueButtonEnabledState: Bool?
    private(set) var error: Error?
    
    func displayAmountValueInstructions(string: NSAttributedString) {
        callDisplayAmountValueInstructionsCount += 1
        amountValueInstructionsAttributedString = string
    }
    
    func displayPaymentDateInstructions(string: NSAttributedString) {
        callDisplayPaymentDateInstructionsCount += 1
        paymentDateInstrictionsAttributedString = string
    }
    
    func setContinueButtonEnabled(state: Bool) {
        callSetContinueButtonEnabledCount += 1
        continueButtonEnabledState = state
    }
    
    func startLoading() {
        callStartLoadingCount += 1
    }
    
    func stopLoading() {
        callStopLoadingCount += 1
    }
    
    func display(error: Error) {
        callDisplayErrorCount += 1
        self.error = error
    }
}

final class LendingProposalAmountCoordinatorSpy: LendingProposalAmountCoordinating {
    var viewController: UIViewController?
    
    private(set) var callPerformActionCount = 0
    
    private(set) var action: LendingProposalAmountAction?
    
    func perform(action: LendingProposalAmountAction) {
        callPerformActionCount += 1
        self.action = action
    }
}

final class LendingProposalAmountPresenterTests: XCTestCase {
    private typealias Localizable = Strings.Lending.Proposal.Amount
    private lazy var coordinatorSpy = LendingProposalAmountCoordinatorSpy()
    private lazy var spy = LendingProposalViewControllerSpy()
    private lazy var sut: LendingProposalAmountPresenting = {
        let presenter = LendingProposalAmountPresenter(coordinator: coordinatorSpy)
        presenter.viewController = spy
        return presenter
    }()
    
    func testPresentAmountValueInstructions_WhenCalledFromViewModel_ShouldDisplayAmountValueInstructions() {
        let configuration: ProposalAmountConfiguration = .mock
        let expectedInstructions = Localizable.amountValueInstructions(
            configuration.minimumAmount.toCurrencyString() ?? "",
            configuration.maximumAmount.toCurrencyString() ?? ""
        )
        
        sut.presentAmountValueInstructions(using: configuration)
        
        XCTAssertEqual(spy.callDisplayAmountValueInstructionsCount, 1)
        XCTAssertEqual(spy.amountValueInstructionsAttributedString?.string, expectedInstructions)
    }
    
    func testPresentPaymentDateInstructions_WhenCalledFromViewModel_ShouldDisplayPaymentDateInstructions() {
        var proposal: Proposal = .mock
        let firstPaymentDeadline = 30
        proposal.firstPaymentDeadline = firstPaymentDeadline
        
        sut.presentPaymentDateInstructions(for: proposal)
        
        XCTAssertEqual(spy.callDisplayPaymentDateInstructionsCount, 1)
        XCTAssertEqual(spy.paymentDateInstrictionsAttributedString?.string,
                       Localizable.paymentDateFooterText(firstPaymentDeadline))
    }
    
    func testPresentAmountValueOutOfBoundsMessage_WhenCalledFromViewModel_ShouldDisplayAmountValueInstructionsWithErrorMessage() {
        let configuration: ProposalAmountConfiguration = .mock
        let expectedErrorMessage = Localizable.amountValueInstructions(
            configuration.minimumAmount.toCurrencyString() ?? "",
            configuration.maximumAmount.toCurrencyString() ?? ""
        )
        
        sut.presentAmountValueOutOfBounds(using: configuration)
        
        XCTAssertEqual(spy.callDisplayAmountValueInstructionsCount, 1)
        XCTAssertEqual(spy.amountValueInstructionsAttributedString?.string, expectedErrorMessage)
    }
    
    func testStartLoading_WhenCalledFromViewModel_ShouldStartLoading() {
        sut.startLoading()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
    }
    
    func testStopLoading_WhenCalledFromViewModel_ShouldStopLoading() {
        sut.stopLoading()
        
        XCTAssertEqual(spy.callStopLoadingCount, 1)
    }
    
    func testPresentError_WhenCalledFromViewModel_ShouldDisplayError() {
        let error: ApiError = .serverError
        
        sut.present(error: error)
        
        XCTAssertEqual(spy.callDisplayErrorCount, 1)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
    }
    
    func testDidNextStepProposalConditions_WhenCalledFromViewModel_ShouldCallPerformNexStepProposalConditions() {
        let expectedProposal = Proposal(amount: 300, installments: 12, interest: 0.0)
        let expectedConfiguration: ProposalConditionsConfiguration = .mock
        let action: LendingProposalAmountAction = .proposalConditions(
            configuration: expectedConfiguration,
            proposal: expectedProposal
        )
        
        sut.didNextStep(action: action)
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        guard case let .proposalConditions(configuration, proposal) = coordinatorSpy.action else {
            XCTFail("Wrong action called")
            return
        }
        XCTAssertEqual(proposal, expectedProposal)
        XCTAssertEqual(configuration, expectedConfiguration)
    }
    
    func testDidNextStepShowFAQ_WhenCalledFromViewModel_ShouldPerformNextStepShowFAQ() throws {
        let url = try XCTUnwrap(URL(string: "picpay://picpay/helpcenter/article/360049386752"))
        
        sut.didNextStep(action: .showFAQ(url: url))
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        guard case let .showFAQ(faqUrl) = coordinatorSpy.action else {
            XCTFail("wrong action called")
            return
        }
        XCTAssertEqual(faqUrl, url)
    }
}
