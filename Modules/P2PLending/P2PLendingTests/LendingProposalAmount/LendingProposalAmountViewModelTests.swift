import Core
import XCTest
import AnalyticsModule
@testable import P2PLending

final class LendingProposalAmountPresenterSpy: LendingProposalAmountPresenting {
    var viewController: LendingProposalAmountDisplay?
    
    private(set) var callPresentAmountValueInstructionsCount = 0
    private(set) var callPresentPaymentDateInstructionsCount = 0
    private(set) var callPresentAmountValueOutOfBoundsCount = 0
    private(set) var callSetContinueButtonEnabledCount = 0
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    private(set) var callPresentErrorCount = 0
    private(set) var callDidNextStepCount = 0
    
    private(set) var action: LendingProposalAmountAction?
    private(set) var configuration: ProposalAmountConfiguration?
    private(set) var proposal: Proposal?
    private(set) var continueButtonEnabledState: Bool?
    private(set) var error: Error?
    
    func presentAmountValueInstructions(using configuration: ProposalAmountConfiguration) {
        callPresentAmountValueInstructionsCount += 1
        self.configuration = configuration
    }
    
    func presentPaymentDateInstructions(for proposal: Proposal) {
        callPresentPaymentDateInstructionsCount += 1
        self.proposal = proposal
    }
    
    func presentAmountValueOutOfBounds(using configuration: ProposalAmountConfiguration) {
        callPresentAmountValueOutOfBoundsCount += 1
        self.configuration = configuration
    }
    
    func setContinueButtonEnabled(state: Bool) {
        callSetContinueButtonEnabledCount += 1
        self.continueButtonEnabledState = state
    }
    
    func startLoading() {
        callStartLoadingCount += 1
    }
    
    func stopLoading() {
        callStopLoadingCount += 1
    }
    
    func present(error: Error) {
        callPresentErrorCount += 1
        self.error = error
    }
    
    func didNextStep(action: LendingProposalAmountAction) {
        callDidNextStepCount += 1
        self.action = action
    }
}

private final class LendingProposalAmountServiceMock: LendingProposalAmountServicing {
    var fetchProposalConditionsConfigurationExpectedResult: Result<ProposalConditionsConfiguration, ApiError>?
    
    func fetchProposalConditionsConfiguration(for amount: Double, completion: @escaping ModelCompletionBlock<ProposalConditionsConfiguration>) {
        callCompletionIfNeeded(completion, expectedResult: fetchProposalConditionsConfigurationExpectedResult)
    }
}

final class LendingProposalAmountViewModelTests: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    private lazy var proposal: Proposal = .mock
    private lazy var serviceMock = LendingProposalAmountServiceMock()
    private lazy var dependencyMock = DependencyContainerMock(analyticsSpy)
    private lazy var spy = LendingProposalAmountPresenterSpy()
    private lazy var sut = LendingProposalAmountViewModel(presenter: spy,
                                                          service: serviceMock,
                                                          proposalConfiguration: .mock,
                                                          proposal: proposal,
                                                          dependencies: dependencyMock)
    
    func testFetchAmountInstructions_WhenCalledFromController_ShouldPresentAmountInstructions() {
        sut.fetchAmountValueInstructions()
        
        XCTAssertEqual(spy.callPresentAmountValueInstructionsCount, 1)
        XCTAssertEqual(spy.callPresentPaymentDateInstructionsCount, 1)
        XCTAssertEqual(spy.configuration, .mock)
    }
    
    func testAmountValueDidChange_WhenValueIsValid_ShouldPresentAmountInstructionsAndEnableContinueButton() {
        sut.amountValueChanged(200)
        
        XCTAssertEqual(spy.callPresentAmountValueInstructionsCount, 1)
        XCTAssertEqual(spy.configuration, .mock)
        XCTAssertEqual(spy.callSetContinueButtonEnabledCount, 1)
        XCTAssertEqual(spy.continueButtonEnabledState, true)
    }
    
    func testAmountValueDidChange_WhenValueIsInvalid_ShouldPresentAmountOutOfBoundsAndDisableContinueButton() {
        sut.amountValueChanged(0)
        
        XCTAssertEqual(spy.callPresentAmountValueOutOfBoundsCount, 1)
        XCTAssertEqual(spy.configuration, .mock)
        XCTAssertEqual(spy.callSetContinueButtonEnabledCount, 1)
        XCTAssertEqual(spy.continueButtonEnabledState, false)
    }
    
    func testContinueButtonTapped_WhenReceiveSuccessFromService_ShouldDidNextStepProposalConditions() {
        let expectedEvent = LendingProposalAmountEvent.choosePaymentButtonTapped.event()
        let expectedConfiguration: ProposalConditionsConfiguration = .mock
        serviceMock.fetchProposalConditionsConfigurationExpectedResult = .success(expectedConfiguration)
        
        sut.continueButtonTapped()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callStopLoadingCount, 1)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        guard case let .proposalConditions(configuration, _) = spy.action else {
            XCTFail("invalid action")
            return
        }
        XCTAssertEqual(configuration, expectedConfiguration)
    }
    
    func testHelpButtonTapped_WhenCalledFromViewModel_ShouldCallDidNextStepShowFAQ() {
        let expectedEvent = LendingProposalAmountEvent.helpButtonTapped.event()
        sut.showHelp()
        
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        guard case .showFAQ = spy.action else {
            XCTFail("wrong action called")
            return
        }
    }
    
    func testContinueButtonTapped_WhenReceiveFailureFromService_ShouldPresentError() {
        let error: ApiError = .serverError
        serviceMock.fetchProposalConditionsConfigurationExpectedResult = .failure(error)
        
        sut.continueButtonTapped()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callStopLoadingCount, 1)
        XCTAssertEqual(spy.callPresentErrorCount, 1)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
    }
    
    func testSendAnalyticsEvent_WhenCalledFromBackButtonFromController_ShouldSendEvent() {
        let expectedEvent = LendingProposalAmountEvent.backButtonTapped.event()
        sut.sendAnalyticsEvent(type: .backButtonTapped)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
}
