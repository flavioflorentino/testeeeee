import XCTest
@testable import P2PLending

final class LendingProposalSummaryCoordinatorTests: XCTestCase {
    private lazy var spy = ViewControllerSpy()
    private lazy var urlOpener = URLOpenerSpy()
    private lazy var sut: LendingProposalSummaryCoordinator = {
        let coordinator = LendingProposalSummaryCoordinator(dependencies: DependencyContainerMock(urlOpener))
        coordinator.viewController = spy
        return coordinator
    }()
    
    func testPerformNextStepCCB_WhenCalledFromPresenter_ShouldCBB() throws {
        sut.perform(action: .ccb(proposal: .mock))
        
        XCTAssertEqual(spy.callShowViewControllerCount, 1)
        XCTAssertTrue(spy.viewController is LendingCCBViewController)
    }
    
    func testPerformNextStepClose_WhenCalledFromPresenter_ShouldDismissViewController() {
        sut.perform(action: .close)
        
        XCTAssertEqual(spy.callDismissCount, 1)
    }
    
    func testPerformActionShowFAQ_WhenCalledFromPresenter_ShouldShowFAQ() throws {
        let url = try XCTUnwrap(URL(string: "https://www.picpay.com"))
        
        sut.perform(action: .showFAQ(url: url))
        
        XCTAssertEqual(urlOpener.callCanOpenURLCount, 1)
        XCTAssertEqual(urlOpener.callOpenURLCount, 1)
        XCTAssertEqual(urlOpener.checkedURL, url)
        XCTAssertEqual(urlOpener.openedURL, url)
    }
    
    func testPerformActionShowInstallments_WhenCalledFromPresenter_ShouldShowInstallmentListController() throws {
        let fakeID = try XCTUnwrap(UUID(uuidString: "2c091bff-95e2-4291-a940-21e237292aea"))
        
        sut.perform(action: .showInstallments(offerIdentifier: fakeID))
        
        XCTAssertEqual(spy.callShowViewControllerCount, 1)
        XCTAssertTrue(spy.viewController is LendingInstallmentListViewController)
    }
}
