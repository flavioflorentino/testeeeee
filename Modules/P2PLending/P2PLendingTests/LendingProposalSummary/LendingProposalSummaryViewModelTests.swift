import XCTest
import Core
import AnalyticsModule
@testable import P2PLending

private final class LendingProposalSummaryPresenterSpy: LendingProposalSummaryPresenting {
    var viewController: LendingProposalSummaryDisplay?
    
    private(set) var callPresentSummaryInformationForProposalCount = 0
    private(set) var callPresentSummaryInformationForOfferCount = 0
    private(set) var callPresentCancelationConfirmationCount = 0
    private(set) var callPresentErrorCount = 0
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    private(set) var callDidNextStepCount = 0
    private(set) var callPresentContractCount = 0
    
    private(set) var proposal: Proposal?
    private(set) var offer: OpportunityOffer?
    private(set) var link: URL?
    private(set) var error: Error?
    private(set) var action: LendingProposalSummaryAction?
    
    func presentSummaryInformation(for proposal: Proposal) {
        callPresentSummaryInformationForProposalCount += 1
        self.proposal = proposal
    }
    
    func presentSummaryInformation(for offer: OpportunityOffer) {
        callPresentSummaryInformationForOfferCount += 1
        self.offer = offer
    }
    
    func presentCancelationConfirmation() {
        callPresentCancelationConfirmationCount += 1
    }
    
    func present(error: Error) {
        callPresentErrorCount += 1
        self.error = error
    }
    
    func startLoading() {
        callStartLoadingCount += 1
    }
    
    func stopLoading() {
        callStopLoadingCount += 1
    }
    
    func presentContract(for url: URL) {
        callPresentContractCount += 1
    }
    
    func didNextStep(action: LendingProposalSummaryAction) {
        callDidNextStepCount += 1
        self.action = action
    }
}

final class LendingProposalSummaryServiceMock: LendingProposalSummaryServicing {
    var fetchProposalExpectedResult: Result<OpportunityOffer, ApiError>?
    var cancelProposalExpectedResult: Result<NoContent, ApiError>?
    
    func fetchProposal(identifier: UUID, completion: @escaping ModelCompletionBlock<OpportunityOffer>) {
        callCompletionIfNeeded(completion, expectedResult: fetchProposalExpectedResult)
    }
    
    func cancelProposal(identifier: UUID, completion: @escaping ModelCompletionBlock<NoContent>) {
        callCompletionIfNeeded(completion, expectedResult: cancelProposalExpectedResult)
    }
}

final class LendingProposalSummaryViewModelTests: XCTestCase {
    private lazy var analyticsSpy = AnalyticsSpy()
    private lazy var dependencyContainerMock = DependencyContainerMock(analyticsSpy)
    private lazy var spy = LendingProposalSummaryPresenterSpy()
    private lazy var service = LendingProposalSummaryServiceMock()
    private lazy var proposalMock: Proposal = .mock
    
    private func configuredSut(for type: ProposalSummaryType) -> LendingProposalSummaryViewModel {
        LendingProposalSummaryViewModel(presenter: spy, service: service, dependencies: dependencyContainerMock, type: type)
    }
    
    func testFetchSummaryInformation_WhenLocalProposal_ShouldPresentProposalSummaryInformationForProposal() {
        let sut = configuredSut(for: .local(proposal: proposalMock))
        sut.fetchSummaryInformation()
        
        XCTAssertEqual(spy.callPresentSummaryInformationForProposalCount, 1)
        XCTAssertEqual(spy.proposal, .mock)
    }
    
    func testFetchSummaryInformation_WhenTypeIsRemoteAndReceiveSuccess_ShouldPresentProposalSummaryInformationForOffer() throws {
        let sut = configuredSut(for: .remote(offerIdentifier: UUID()))
        let offer: OpportunityOffer = try .mock(for: .waitingReply)
        service.fetchProposalExpectedResult = .success(offer)
        
        sut.fetchSummaryInformation()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callStopLoadingCount, 1)
        XCTAssertEqual(spy.callPresentSummaryInformationForOfferCount, 1)
        XCTAssertEqual(spy.offer, offer)
    }
    
    func testFetchSummaryInformation_WhenTypeIsRemoteAndReceiveFailure_ShouldPresentError() {
        let sut = configuredSut(for: .remote(offerIdentifier: UUID()))
        let error: ApiError = .bodyNotFound
        service.fetchProposalExpectedResult = .failure(error)
        
        sut.fetchSummaryInformation()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callStopLoadingCount, 1)
        XCTAssertEqual(spy.callPresentErrorCount, 1)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
    }
    
    func testCancelProposal_WhenCalledFromController_ShouldPresentCancelationConfirmation() {
        let sut = configuredSut(for: .remote(offerIdentifier: UUID()))
        
        sut.cancelProposal()
        
        XCTAssertEqual(spy.callPresentCancelationConfirmationCount, 1)
    }
    
    func testConfirmProposalCancelation_WhenTypeIsRemoteAndReceiveSuccess_ShouldCallDidNextStepClose() {
        let sut = configuredSut(for: .remote(offerIdentifier: UUID()))
        service.cancelProposalExpectedResult = .success(NoContent())
        
        sut.confirmProposalCancelation()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callStopLoadingCount, 1)
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        
        guard case .close = spy.action else {
            XCTFail("wrong action called")
            return
        }
    }
    
    func testConfirmProposalCancelation_WhenTypeIsRemoteAndReceiveFailure_ShouldPresentError() {
        let sut = configuredSut(for: .remote(offerIdentifier: UUID()))
        let error: ApiError = .bodyNotFound
        service.cancelProposalExpectedResult = .failure(error)
        
        sut.confirmProposalCancelation()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callStopLoadingCount, 1)
        XCTAssertEqual(spy.callPresentErrorCount, 1)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
    }
    
    func testClose_WhenCalledFromController_ShouldCallDidNextStepClose() {
        let sut = configuredSut(for: .remote(offerIdentifier: UUID()))
        
        sut.close()
        
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        guard case .close = spy.action else {
            XCTFail("wrong action called")
            return
        }
    }
    
    func testPrimaryAction_WhenTypeIsLocalProposal_ShouldCallDidNextStepCCB() {
        let expectedEvent = LendingProposalSummaryEvent.readContractTapped.event()
        let sut = configuredSut(for: .local(proposal: proposalMock))
        sut.primaryAction()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        guard case .ccb = spy.action else {
            XCTFail("wrong action called")
            return
        }
    }
    
    func testPrimaryAction_WhenTypeIsRemoteAndActive_ShouldCallDidNextStepShowInstallments() throws {
        let expectedEvent = LendingProposalSummaryEvent.installmentsTapped.event()
        let sut = configuredSut(for: .remote(offerIdentifier: UUID()))
        service.fetchProposalExpectedResult = .success(try .mock(for: .active))
        
        sut.fetchSummaryInformation()
        sut.primaryAction()
        
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        guard case .showInstallments = spy.action else {
            XCTFail("wrong action called")
            return
        }
    }
    
    func testShowHelp_WhenCalledFromController_ShouldShowHelp() {
        let expectedEvent = LendingProposalSummaryEvent.helpTapped.event()
        let sut = configuredSut(for: .remote(offerIdentifier: UUID()))
        sut.showHelp()
        
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        guard case .showFAQ = spy.action else {
            XCTFail("wrong action called")
            return
        }
    }
    
    func testSendAnalyticsEvent_WhenCalledFromBackButtonFromController_ShouldSendEvent() {
        let expectedEvent = LendingProposalSummaryEvent.backButtonTapped.event()
        let sut = configuredSut(for: .remote(offerIdentifier: UUID()))
        sut.sendTrackingEvent(type: .backButtonTapped)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testSendAnalyticsEvent_WhenCalledFromSeeMoreButtonFromController_ShouldSendEvent() {
        let expectedEvent = LendingProposalSummaryEvent.moreTapped.event()
        let sut = configuredSut(for: .remote(offerIdentifier: UUID()))
        sut.sendTrackingEvent(type: .seeMoreTapped)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testSendAnalyticsEvent_WhenCalledFromSeeLessButtonFromController_ShouldSendEvent() {
        let expectedEvent = LendingProposalSummaryEvent.lessTapped.event()
        let sut = configuredSut(for: .remote(offerIdentifier: UUID()))
        sut.sendTrackingEvent(type: .seeLessTapped)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testPresentContract_WhenCalledFromViewController_ShouldCallPresentContract() throws {
        let sut = configuredSut(for: .remote(offerIdentifier: UUID()))
        service.fetchProposalExpectedResult = .success(try .mock())
        
        sut.fetchSummaryInformation()
        sut.showContract()
        
        XCTAssertEqual(spy.callPresentContractCount, 1)
    }
}
