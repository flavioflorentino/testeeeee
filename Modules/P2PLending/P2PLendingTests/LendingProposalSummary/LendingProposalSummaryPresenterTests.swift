import XCTest
import Core
import FeatureFlag
@testable import P2PLending

private final class LendingProposalSummaryDisplaySpy: LendingProposalSummaryDisplay {
    private(set) var callDisplayProposalSummaryCount = 0
    private(set) var callDisplayCancelationConfirmationCount = 0
    private(set) var callDisplayErrorCount = 0
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    
    private(set) var proposalSummary: ProposalSummary?
    private(set) var error: Error?
    
    func display(proposalSummary: ProposalSummary) {
        callDisplayProposalSummaryCount += 1
        self.proposalSummary = proposalSummary
    }
    
    func displayCancelationConfirmation() {
        callDisplayCancelationConfirmationCount += 1
    }
    
    func display(error: Error) {
        callDisplayErrorCount += 1
        self.error = error
    }
    
    func startLoading() {
        callStartLoadingCount += 1
    }
    
    func stopLoading() {
        callStopLoadingCount += 1
    }
}

private final class LendingProposalSummaryCoordinatorSpy: LendingProposalSummaryCoordinating {
    var viewController: UIViewController?
    
    private(set) var callPerformActionCount = 0
    private(set) var action: LendingProposalSummaryAction?
    
    func perform(action: LendingProposalSummaryAction) {
        callPerformActionCount += 1
        self.action = action
    }
}

final class LendingProposalSummaryPresenterTests: XCTestCase {
    private typealias Localizable = Strings.Lending.Proposal.Summary
    private lazy var coordinatorSpy = LendingProposalSummaryCoordinatorSpy()
    private lazy var featureManagerMock = FeatureManagerMock()
    private lazy var spy = LendingProposalSummaryDisplaySpy()
    private lazy var sut: LendingProposalSummaryPresenter = {
        let container = DependencyContainerMock(featureManagerMock)
        let presenter = LendingProposalSummaryPresenter(coordinator: coordinatorSpy, dependencies: container)
        presenter.viewController = spy
        return presenter
    }()
    
    override func setUp() {
        super.setUp()
        featureManagerMock.override(key: .isP2PLendingBorrowerInstallmentsAvailable, with: true)
    }
    
    func testPresentSummaryInformationForProposal_WhenCalledFromViewModel_ShouldDisplaySummaryInformation() throws {
        let proposal: Proposal = .mock
        let expectedSummary: ProposalSummary = try .mock()
        
        sut.presentSummaryInformation(for: proposal)
        
        XCTAssertEqual(spy.callDisplayProposalSummaryCount, 1)
        XCTAssertEqual(spy.proposalSummary, expectedSummary)
    }
    
    func testPresentSummaryInformationForOffer_WhenCalledFromViewModel_ShouldDisplaySummaryInformation() throws {
        let status: OpportunityOfferStatus = .waitingReply
        
        sut.presentSummaryInformation(for: try .mock(for: status))
        
        XCTAssertEqual(spy.callDisplayProposalSummaryCount, 1)
        XCTAssertEqual(spy.proposalSummary, try .mock(for: status))
    }
    
    func testPresentSummaryInformationForActiveOffer_WhenInstallmentsIsEnabled_ShouldDisplaySummaryInformation() throws {
        featureManagerMock.override(key: .isP2PLendingBorrowerInstallmentsAvailable, with: true)
        let status: OpportunityOfferStatus = .active
        
        sut.presentSummaryInformation(for: try .mock(for: status))
        
        XCTAssertEqual(spy.callDisplayProposalSummaryCount, 1)
        XCTAssertEqual(spy.proposalSummary, try .mock(for: status, isInstallmentsAvailable: true))
    }
    
    func testPresentSummaryInformationForActiveOffer_WhenInstallmentsIsNotEnabled_ShouldDisplaySummaryInformation() throws {
        featureManagerMock.override(key: .isP2PLendingBorrowerInstallmentsAvailable, with: false)
        let status: OpportunityOfferStatus = .active
        
        sut.presentSummaryInformation(for: try .mock(for: status))
        
        XCTAssertEqual(spy.callDisplayProposalSummaryCount, 1)
        XCTAssertEqual(spy.proposalSummary, try .mock(for: status, isInstallmentsAvailable: false))
    }
    
    func testPresentCancelationConfirmation_WhenCalledFromInteractor_ShouldDisplayCancelationConfirmation() {
        sut.presentCancelationConfirmation()
        
        XCTAssertEqual(spy.callDisplayCancelationConfirmationCount, 1)
    }
    
    func testPresentError_WhenCalledFromViewModel_ShouldDisplayError() {
        let error: ApiError = .serverError
        
        sut.present(error: error)
        
        XCTAssertEqual(spy.callDisplayErrorCount, 1)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
    }
    
    func testStartLoading_WhenCalledFromViewModel_ShouldStartLoading() {
        sut.startLoading()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
    }
    
    func testStopLoading_WhenCalledFromViewModel_ShouldStopLoading() {
        sut.stopLoading()
        
        XCTAssertEqual(spy.callStopLoadingCount, 1)
    }
    
    func testDidNextStepClose_WhenCalledFromViewModel_ShouldPerformNextStepClose() {
        sut.didNextStep(action: .close)
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        guard case .close = coordinatorSpy.action else {
            XCTFail("wrong action called")
            return
        }
    }
    
    func testDidNextStepCCB_WhenCalledFromViewModel_ShouldPerformNextStepCCB() {
        let expectedProposal: Proposal = .mock
        sut.didNextStep(action: .ccb(proposal: expectedProposal))
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        guard case .ccb(let proposal) = coordinatorSpy.action else {
            XCTFail("wrong action called")
            return
        }
        XCTAssertEqual(proposal, expectedProposal)
    }
}
