import XCTest
@testable import P2PLending

final class LendingProposalListCoordinatorTests: XCTestCase {
    private lazy var spy = ViewControllerSpy()
    private lazy var sut: LendingProposalListCoordinator = {
        let coordinator = LendingProposalListCoordinator()
        coordinator.viewController = spy
        return coordinator
    }()
    
    func testPerformActionProposalDetails_WhenCalledFromPresenter_ShouldPresentProposalSummary() throws {
        sut.perform(action: .proposalDetails(proposalIdentifier: UUID()))
        
        XCTAssertEqual(spy.callPresentViewControllerCount, 1)
        let navigationController = try XCTUnwrap(spy.viewController as? UINavigationController)
        XCTAssertTrue(navigationController.viewControllers.first is LendingProposalSummaryViewController)
    }
    
    func testPerformActionOpportunityDetails_WhenCalledFromPresenter_ShouldPresentOpportunityOffer() throws {
        sut.perform(action: .opportunityDetails(proposalIdentifier: UUID()))
        
        XCTAssertEqual(spy.callPresentViewControllerCount, 1)
        let navigationController = try XCTUnwrap(spy.viewController as? UINavigationController)
        XCTAssertTrue(navigationController.viewControllers.first is LendingOpportunityOfferViewController)
    }
    
    func testPerformActionOpportunityGreetings_WhenCalledFromPresenter_ShouldPresentInvestorGreetings() throws {
        sut.perform(action: .opportunityGreetings(proposalIdentifier: UUID()))
        
        XCTAssertEqual(spy.callPresentViewControllerCount, 1)
        let navigationController = try XCTUnwrap(spy.viewController as? UINavigationController)
        XCTAssertTrue(navigationController.viewControllers.first is LendingInvestorGreetingsViewController)
    }
    
    func testPerformActionProposalInstallments_WhenCalledFromPresenter_ShouldPresentProposalInstallments() throws {
        sut.perform(action: .proposalInstallments(proposalIdentifier: UUID(), agent: .borrower))
        
        XCTAssertEqual(spy.callShowViewControllerCount, 1)
    }
    
    func testPerformActionClose_WhenCalledFromPresenter_ShouldDismissViewController() {
        sut.perform(action: .close)
        
        XCTAssertEqual(spy.callDismissCount, 1)
    }
}
