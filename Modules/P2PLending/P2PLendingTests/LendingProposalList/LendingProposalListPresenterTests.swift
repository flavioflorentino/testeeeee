import XCTest
import Foundation
import Core
import FeatureFlag
@testable import P2PLending

private final class LendingProposalListDisplaySpy: LendingProposalListDisplay {
    private(set) var callDisplayTitleHeadlineCount = 0
    private(set) var callDisplayOnGoingProposalsCount = 0
    private(set) var callDisplayTerminatedProposalsCount = 0
    private(set) var callDisplayEmptyStateForOnGoingCount = 0
    private(set) var callHideEmptyStateForOnGoingCount = 0
    private(set) var callDisplayEmptyStateForTerminatedCount = 0
    private(set) var callHideEmptyStateForTerminatedCount = 0
    private(set) var callDisplayErrorCount = 0
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    
    private(set) var title: String?
    private(set) var headline: String?
    private(set) var onGoingProposals: [ProposalListItemViewModel]?
    private(set) var terminatedProposals: [ProposalListItemViewModel]?
    private(set) var emptyStateOnGoingTitle: String?
    private(set) var emptyStateTerminatedTitle: String?
    private(set) var error: Error?
    
    func display(title: String, headline: String) {
        callDisplayTitleHeadlineCount += 1
        self.title = title
        self.headline = headline
    }
    
    func displayOnGoing(proposals: [ProposalListItemViewModel]) {
        callDisplayOnGoingProposalsCount += 1
        onGoingProposals = proposals
    }
    
    func displayTerminated(proposals: [ProposalListItemViewModel]) {
        callDisplayTerminatedProposalsCount += 1
        terminatedProposals = proposals
    }
    
    func displayEmptyStateForOnGoingList(title: String) {
        callDisplayEmptyStateForOnGoingCount += 1
        emptyStateOnGoingTitle = title
    }
    
    func hideEmptyStateForOnGoingList() {
        callHideEmptyStateForOnGoingCount += 1
    }
    
    func displayEmptyStateForTerminatedList(title: String) {
        callDisplayEmptyStateForTerminatedCount += 1
        emptyStateTerminatedTitle = title
    }
    
    func hideEmptyStateForTerminatedList() {
        callHideEmptyStateForTerminatedCount += 1
    }
    
    func display(error: Error) {
        callDisplayErrorCount += 1
        self.error = error
    }
    
    func startLoading() {
        callStartLoadingCount += 1
    }
    
    func stopLoading() {
        callStopLoadingCount += 1
    }
}

private final class LendingProposalListCoordinatorSpy: LendingProposalListCoordinating {
    var viewController: UIViewController?
    
    private(set) var callPerformActionCount = 0
    
    private(set) var action: LendingProposalListAction?
    
    func perform(action: LendingProposalListAction) {
        callPerformActionCount += 1
        self.action = action
    }
}

final class LendingProposalListPresenterTests: XCTestCase {
    private typealias Localizable = Strings.Lending.Proposal.List
    private lazy var spy = LendingProposalListDisplaySpy()
    private lazy var coordinatorSpy = LendingProposalListCoordinatorSpy()
    private lazy var featureManagerMock: FeatureManagerMock = {
        let featureManagerMock = FeatureManagerMock()
        featureManagerMock.override(key: .isP2PLendingBorrowerInstallmentsAvailable, with: false)
        featureManagerMock.override(key: .isP2PLendingInvestorInstallmentsAvailable, with: false)
        return featureManagerMock
    }()
    private lazy var sut: LendingProposalListPresenter = {
        let container = DependencyContainerMock(featureManagerMock)
        let presenter = LendingProposalListPresenter(coordinator: coordinatorSpy, dependencies: container)
        presenter.viewController = spy
        return presenter
    }()
    
    func testPresentHeaderForAgent_WhenAgentIsBorrower_ShouldDisplayBorrowerTitleAndHeadline() {
        sut.presentHeader(for: .borrower)
        
        XCTAssertEqual(spy.callDisplayTitleHeadlineCount, 1)
        XCTAssertEqual(spy.title, Localizable.borrowerTitle)
        XCTAssertEqual(spy.headline, Localizable.borrowerHeadline)
    }
    
    func testPresentHeaderForAgent_WhenAgentIsInvestor_ShouldDisplayInvestorTitleAndHeadline() {
        sut.presentHeader(for: .investor)
        
        XCTAssertEqual(spy.callDisplayTitleHeadlineCount, 1)
        XCTAssertEqual(spy.title, Localizable.investorTitle)
        XCTAssertEqual(spy.headline, Localizable.investorHeadline)
    }
    
    func testPresentOnGoingProposalsForBorrower_WhenCalledFromViewModel_ShouldDisplayOnGoingProposalsForBorrower() {
        let agent: ConsultingAgent = .borrower
        let allStatus = OpportunityOfferStatus.allCases
        let proposals = allStatus.map(ProposalListItem.mock(status:))
        let expectedFormattedProposals = allStatus.map { status in
            ProposalListItemViewModel.mock(status: status, agent: agent)
        }
        
        sut.presentOnGoing(proposals: proposals, for: .borrower)
        
        XCTAssertEqual(spy.callDisplayOnGoingProposalsCount, 1)
        XCTAssertEqual(spy.onGoingProposals, expectedFormattedProposals)
    }
    
    func testPresentTerminatedProposalsForBorrower_WhenCalledFromViewModel_ShouldDisplayTerminatedProposalsForBorrower() {
        let agent: ConsultingAgent = .borrower
        let allStatus = OpportunityOfferStatus.allCases
        let proposals = allStatus.map(ProposalListItem.mock(status:))
        let expectedFormattedProposals = allStatus.map { status in
            ProposalListItemViewModel.mock(status: status, agent: agent)
        }
        
        sut.presentTerminated(proposals: proposals, for: agent)
        
        XCTAssertEqual(spy.callDisplayTerminatedProposalsCount, 1)
        XCTAssertEqual(spy.terminatedProposals, expectedFormattedProposals)
    }
    
    func testPresentOnGoingProposalsForInvestor_WhenCalledFromViewModel_ShouldDisplayOnGoingProposalsForInvestor() {
        let agent: ConsultingAgent = .investor
        let allStatus = OpportunityOfferStatus.allCases
        let proposals = allStatus.map(ProposalListItem.mock(status:))
        let expectedFormattedProposals = allStatus.map { status in
            ProposalListItemViewModel.mock(status: status, agent: agent)
        }
        
        sut.presentOnGoing(proposals: proposals, for: agent)
        
        XCTAssertEqual(spy.callDisplayOnGoingProposalsCount, 1)
        XCTAssertEqual(spy.onGoingProposals, expectedFormattedProposals)
    }
    
    func testPresentOnGoingProposalForBorrower_WhenBorrowerInstallmentsIsAvailable_ShouldDisplayInstallmentsButton() {
        featureManagerMock.override(key: .isP2PLendingBorrowerInstallmentsAvailable, with: true)
        let agent: ConsultingAgent = .borrower
        let allStatus = OpportunityOfferStatus.allCases
        let proposals = allStatus.map(ProposalListItem.mock(status:))
        let expectedFormattedProposals = allStatus.map { status in
            ProposalListItemViewModel.mock(status: status, agent: agent, isInstallmentsAvailable: true)
        }
        
        sut.presentOnGoing(proposals: proposals, for: agent)
        
        XCTAssertEqual(spy.callDisplayOnGoingProposalsCount, 1)
        XCTAssertEqual(spy.onGoingProposals, expectedFormattedProposals)
    }
    
    func testPresentOnGoingProposalForInvestor_WhenInvestorInstallmentsIsAvailable_ShouldDisplayInstallmentsButton() {
        featureManagerMock.override(key: .isP2PLendingInvestorInstallmentsAvailable, with: true)
        let agent: ConsultingAgent = .investor
        let allStatus = OpportunityOfferStatus.allCases
        let proposals = allStatus.map(ProposalListItem.mock(status:))
        let expectedFormattedProposals = allStatus.map { status in
            ProposalListItemViewModel.mock(status: status, agent: agent, isInstallmentsAvailable: true)
        }
        
        sut.presentOnGoing(proposals: proposals, for: agent)
        
        XCTAssertEqual(spy.callDisplayOnGoingProposalsCount, 1)
        XCTAssertEqual(spy.onGoingProposals, expectedFormattedProposals)
    }
    
    func testPresentOnGoingProposalForBorrower_WhenBorrowerInstallmentsIsNotAvailable_ShouldNotDisplayInstallmentsButton() {
        featureManagerMock.override(key: .isP2PLendingBorrowerInstallmentsAvailable, with: false)
        let agent: ConsultingAgent = .borrower
        let allStatus = OpportunityOfferStatus.allCases
        let proposals = allStatus.map(ProposalListItem.mock(status:))
        let expectedFormattedProposals = allStatus.map { status in
            ProposalListItemViewModel.mock(status: status, agent: agent, isInstallmentsAvailable: false)
        }
        
        sut.presentOnGoing(proposals: proposals, for: agent)
        
        XCTAssertEqual(spy.callDisplayOnGoingProposalsCount, 1)
        XCTAssertEqual(spy.onGoingProposals, expectedFormattedProposals)
    }
    
    func testPresentOnGoingProposalForInvestor_WhenInvestorInstallmentsIsNotAvailable_ShouldNotDisplayInstallmentsButton() {
        featureManagerMock.override(key: .isP2PLendingInvestorInstallmentsAvailable, with: false)
        let agent: ConsultingAgent = .investor
        let allStatus = OpportunityOfferStatus.allCases
        let proposals = allStatus.map(ProposalListItem.mock(status:))
        let expectedFormattedProposals = allStatus.map { status in
            ProposalListItemViewModel.mock(status: status, agent: agent, isInstallmentsAvailable: false)
        }
        
        sut.presentOnGoing(proposals: proposals, for: agent)
        
        XCTAssertEqual(spy.callDisplayOnGoingProposalsCount, 1)
        XCTAssertEqual(spy.onGoingProposals, expectedFormattedProposals)
    }
    
    func testPresentTerminatedProposalsForInvestor_WhenCalledFromViewModel_ShouldDisplayTerminatedProposalsForInvestor() {
        let agent: ConsultingAgent = .investor
        let allStatus = OpportunityOfferStatus.allCases
        let proposals = allStatus.map(ProposalListItem.mock(status:))
        let expectedFormattedProposals = allStatus.map { status in
            ProposalListItemViewModel.mock(status: status, agent: agent)
        }
        
        sut.presentTerminated(proposals: proposals, for: agent)
        
        XCTAssertEqual(spy.callDisplayTerminatedProposalsCount, 1)
        XCTAssertEqual(spy.terminatedProposals, expectedFormattedProposals)
    }
    
    func testSetOnGoingEmptyStateVisibility_WhenCalledFromViewModel_ShouldDisplayEmptyState() {
        let proposals: [ProposalListItem] = []
        let expectedTitle = Strings.Lending.Proposal.List.emptyListOnGoingTitle
            
        sut.setOnGoingEmptyStateVisibility(isEmpty: proposals.isEmpty)
        
        XCTAssertEqual(spy.callDisplayEmptyStateForOnGoingCount, 1)
        XCTAssertEqual(spy.emptyStateOnGoingTitle, expectedTitle)
    }
    
    func testSetTerminatedEmptyStateVisibility_WhenCalledFromViewModel_ShouldDisplayEmptyState() {
        let proposals: [ProposalListItem] = []
        let expectedTitle = Strings.Lending.Proposal.List.emptyListTerminatedTitle
            
        sut.setTerminatedEmptyStateVisibility(isEmpty: proposals.isEmpty)
        
        XCTAssertEqual(spy.callDisplayEmptyStateForTerminatedCount, 1)
        XCTAssertEqual(spy.emptyStateTerminatedTitle, expectedTitle)
    }
    
    func testSetOnGoingEmptyStateVisibility_WhenCalledFromViewModel_ShouldHideEmptyState() {
        let allStatus = OpportunityOfferStatus.allCases
        let proposals = allStatus.map(ProposalListItem.mock(status:))
        
        sut.setOnGoingEmptyStateVisibility(isEmpty: proposals.isEmpty)
        
        XCTAssertEqual(spy.callHideEmptyStateForOnGoingCount, 1)
    }
    
    func testSetTerminatedEmptyStateVisibility_WhenCalledFromViewModel_ShouldHideEmptyState() {
        let allStatus = OpportunityOfferStatus.allCases
        let proposals = allStatus.map(ProposalListItem.mock(status:))
        
        sut.setTerminatedEmptyStateVisibility(isEmpty: proposals.isEmpty)
        
        XCTAssertEqual(spy.callHideEmptyStateForTerminatedCount, 1)
    }
    
    func testPresentError_WhenCalledFromViewModel_ShouldDisplayError() {
        let error: ApiError = .serverError
        
        sut.present(error: error)
        
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
    }
    
    func testStartLoading_WhenCalledFromViewModel_ShouldStartLoading() {
        sut.startLoading()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
    }
    
    func testStopLoading_WhenCalledFromViewModel_ShouldStopLoading() {
        sut.stopLoading()
        
        XCTAssertEqual(spy.callStopLoadingCount, 1)
    }
    
    func testDidNextStepProposalDetails_WhenCalledFromViewModel_ShouldPerformActionProposalDetails() {
        let uuid = UUID()
        
        sut.didNextStep(action: .proposalDetails(proposalIdentifier: uuid))
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        
        guard case let .proposalDetails(proposalIdentifier) = coordinatorSpy.action else {
            XCTFail("Wrong action called")
            return
        }
        XCTAssertEqual(proposalIdentifier, uuid)
    }
    
    func testDidNextStepProposalInstallments_WhenCalledFromViewModel_ShouldPerformActionProposalInstallmentsForBorrower() {
        let uuid = UUID()
        
        sut.didNextStep(action: .proposalInstallments(proposalIdentifier: uuid, agent: .borrower))
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .proposalInstallments(proposalIdentifier: uuid, agent: .borrower))
    }
    
    func testDidNextStepProposalInstallments_WhenCalledFromViewModel_ShouldPerformActionProposalInstallmentsForInvestor() {
        let uuid = UUID()
        
        sut.didNextStep(action: .proposalInstallments(proposalIdentifier: uuid, agent: .investor))
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .proposalInstallments(proposalIdentifier: uuid, agent: .investor))
    }
    
    func testDidNextStepClose_WhenCalledFromViewModel_ShouldPerformActionClose() {
        sut.didNextStep(action: .close)
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        guard case .close = coordinatorSpy.action else {
            XCTFail("Wrong action called")
            return
        }
    }
}
