import XCTest
import Foundation
import Core
import AnalyticsModule
@testable import P2PLending

private final class LendingProposalListPresenterSpy: LendingProposalListPresenting {
    var viewController: LendingProposalListDisplay?
    
    private(set) var callPresentHeaderCount = 0
    private(set) var callSetReferenceDateCount = 0
    private(set) var callPresentOnGoingProposalsCount = 0
    private(set) var callPresentTerminatedProposalsCount = 0
    private(set) var callSetOnGoingEmptyStateVisibilityCount = 0
    private(set) var callSetTerminatedEmptyStateVisibilityCount = 0
    private(set) var callPresentErrorCount = 0
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    private(set) var callDidNextStepCount = 0
    
    private(set) var agent: ConsultingAgent?
    private(set) var referenceDate: Date?
    private(set) var onGoingProposals: [ProposalListItem]?
    private(set) var terminatedProposals: [ProposalListItem]?
    private(set) var error: Error?
    private(set) var action: LendingProposalListAction?
    
    func presentHeader(for agent: ConsultingAgent) {
        callPresentHeaderCount += 1
        self.agent = agent
    }
    
    func setReferenceDate(_ date: Date) {
        callSetReferenceDateCount += 1
        referenceDate = date
    }
    
    func presentOnGoing(proposals: [ProposalListItem], for agent: ConsultingAgent) {
        callPresentOnGoingProposalsCount += 1
        onGoingProposals = proposals
    }
    
    func presentTerminated(proposals: [ProposalListItem], for agent: ConsultingAgent) {
        callPresentTerminatedProposalsCount += 1
        terminatedProposals = proposals
    }
    
    func setOnGoingEmptyStateVisibility(isEmpty: Bool) {
        callSetOnGoingEmptyStateVisibilityCount += 1
    }
    
    func setTerminatedEmptyStateVisibility(isEmpty: Bool) {
        callSetTerminatedEmptyStateVisibilityCount += 1
    }
    
    func present(error: Error) {
        callPresentErrorCount += 1
        self.error = error
    }
    
    func startLoading() {
        callStartLoadingCount += 1
    }
    
    func stopLoading() {
        callStopLoadingCount += 1
    }
    
    func didNextStep(action: LendingProposalListAction) {
        callDidNextStepCount += 1
        self.action = action
    }
}

private final class LendingProposalListServiceMock: LendingProposalListServicing {
    var fetchProposalsExpectedResult: Result<ProposalListResponse, ApiError>?
    
    func fetchProposals(for agent: ConsultingAgent, completion: @escaping ModelCompletionBlock<ProposalListResponse>) {
        callCompletionIfNeeded(completion, expectedResult: fetchProposalsExpectedResult)
    }
}

final class LendingProposalListViewModelTests: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    private lazy var dependencyMock = DependencyContainerMock(analyticsSpy)
    private lazy var serviceMock = LendingProposalListServiceMock()
    private lazy var spy = LendingProposalListPresenterSpy()
    private func sut(for agent: ConsultingAgent) -> LendingProposalListViewModel {
        .init(service: serviceMock, presenter: spy, agent: agent, dependencies: dependencyMock)
    }
    
    func testFetchAgentInformation_WhenCalledFromController_ShouldPresentHeader() {
        let sut = self.sut(for: .borrower)
        
        sut.fetchAgentInformation()
        
        XCTAssertEqual(spy.callPresentHeaderCount, 1)
        XCTAssertEqual(spy.agent, .borrower)
    }
    
    func testFetchProposals_WhenReceiveSuccessFromService_ShouldSetReferenceDateAndPresentProposals() {
        let sut = self.sut(for: .borrower)
        let response: ProposalListResponse = .mock
        serviceMock.fetchProposalsExpectedResult = .success(response)
        
        sut.fetchProposals()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callStopLoadingCount, 1)
        XCTAssertEqual(spy.callSetReferenceDateCount, 1)
        XCTAssertEqual(spy.callPresentOnGoingProposalsCount, 1)
        XCTAssertEqual(spy.callPresentTerminatedProposalsCount, 1)
        XCTAssertEqual(spy.callSetOnGoingEmptyStateVisibilityCount, 1)
        XCTAssertEqual(spy.callSetTerminatedEmptyStateVisibilityCount, 1)
        XCTAssertEqual(spy.referenceDate, response.requestTime)
        XCTAssertEqual(spy.onGoingProposals, response.onGoing)
        XCTAssertEqual(spy.terminatedProposals, response.terminated)
    }
    
    func testFetchProposals_WhenReceiveFailureFromService_ShouldPresentError() {
        let sut = self.sut(for: .borrower)
        let error: ApiError = .serverError
        serviceMock.fetchProposalsExpectedResult = .failure(error)
        
        sut.fetchProposals()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callStopLoadingCount, 1)
        XCTAssertEqual(spy.callPresentErrorCount, 1)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
    }
    
    func testRefreshProposals_WhenReceiveSuccessFromService_ShouldSetReferenceDateAndPresentProposals() {
        let sut = self.sut(for: .borrower)
        let response: ProposalListResponse = .mock
        serviceMock.fetchProposalsExpectedResult = .success(response)
        
        sut.refreshProposals()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callStopLoadingCount, 1)
        XCTAssertEqual(spy.callSetReferenceDateCount, 1)
        XCTAssertEqual(spy.callPresentOnGoingProposalsCount, 2, "Should call twice: one to clean the list, the other to display updated results")
        XCTAssertEqual(spy.callPresentTerminatedProposalsCount, 2, "Should call twice: one to clean the list, the other to display updated results")
        XCTAssertEqual(spy.callSetOnGoingEmptyStateVisibilityCount, 2, "Should call twice: one to clean the list, the other to display updated results")
        XCTAssertEqual(spy.callSetTerminatedEmptyStateVisibilityCount, 2, "Should call twice: one to clean the list, the other to display updated results")
        XCTAssertEqual(spy.referenceDate, response.requestTime)
        XCTAssertEqual(spy.onGoingProposals, response.onGoing)
        XCTAssertEqual(spy.terminatedProposals, response.terminated)
    }
    
    func testRefreshProposals_WhenReceiveFailureFromService_ShouldPresentError() {
        let sut = self.sut(for: .borrower)
        let error: ApiError = .serverError
        serviceMock.fetchProposalsExpectedResult = .failure(error)
        
        sut.refreshProposals()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callStopLoadingCount, 1)
        XCTAssertEqual(spy.callPresentErrorCount, 1)
        XCTAssertEqual(spy.callPresentOnGoingProposalsCount, 1, "Should call once to clean the list")
        XCTAssertEqual(spy.callPresentTerminatedProposalsCount, 1, "Should call once to clean the list")
        XCTAssertEqual(spy.callSetOnGoingEmptyStateVisibilityCount, 1, "Should call once to clean the list")
        XCTAssertEqual(spy.callSetTerminatedEmptyStateVisibilityCount, 1, "Should call once to clean the list")
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
    }
    
    func testShowDetails_WhenReceiveProposalIdentifierAndIsBorrower_ShouldCallDidNextStepProposalDetails() {
        let sut = self.sut(for: .borrower)
        let uuid = UUID(uuidString: "dd8c205f-3bb4-4f38-a60c-a9169c759850")
        let expectedEvent = LendingProposalListEvent.proposalItemTapped(.borrower).event()
        serviceMock.fetchProposalsExpectedResult = .success(.mock)
        
        sut.fetchProposals()
        sut.showDetails(forProposalAt: IndexPath(row: 0, section: 0), in: .onGoing)
        
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        
        guard case let .proposalDetails(proposalIdentifier) = spy.action else {
            XCTFail("Wrong action called")
            return
        }
        XCTAssertEqual(proposalIdentifier, uuid)
    }
    
    func testShowDetails_WhenReceiveProposalIdentifierAndIsInvestorAndStatusIsWaitingReply_ShouldCallDidNextStepOpportunityGreetings() {
        let sut = self.sut(for: .investor)
        let uuid = UUID(uuidString: "dd8c205f-3bb4-4f38-a60c-a9169c759850")
        let waitingReplyProposal: ProposalListItem = .mock(status: .waitingReply)
        let response = ProposalListResponse(onGoing: [waitingReplyProposal], terminated: [], requestTime: Date())
        serviceMock.fetchProposalsExpectedResult = .success(response)
        
        sut.fetchProposals()
        sut.showDetails(forProposalAt: IndexPath(row: 0, section: 0), in: .onGoing)
        
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        
        guard case let .opportunityGreetings(proposalIdentifier) = spy.action else {
            XCTFail("Wrong action called")
            return
        }
        XCTAssertEqual(proposalIdentifier, uuid)
    }
    
    func testShowDetails_WhenReceiveProposalIdentifierAndIsInvestorAndStatusIsNotWaitingReply_ShouldCallDidNextStepOpportunityDetails() {
        let expectedEvent = LendingProposalListEvent.proposalItemTapped(.investor).event()
        let sut = self.sut(for: .investor)
        let uuid = UUID(uuidString: "dd8c205f-3bb4-4f38-a60c-a9169c759850")
        let processingPaymentProposal: ProposalListItem = .mock(status: .processingPayment)
        let response = ProposalListResponse(onGoing: [processingPaymentProposal], terminated: [], requestTime: Date())
        serviceMock.fetchProposalsExpectedResult = .success(response)
        
        sut.fetchProposals()
        sut.showDetails(forProposalAt: IndexPath(row: 0, section: 0), in: .onGoing)
        
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        
        guard case let .opportunityDetails(proposalIdentifier) = spy.action else {
            XCTFail("Wrong action called")
            return
        }
        XCTAssertEqual(proposalIdentifier, uuid)
    }
    
    func testShowInstallments_WhenReceiveProposalIdentifier_ShouldCallDidNextStepProposalInstallments() throws {
        let expectedEvent = LendingProposalListEvent.installmentsTapped(.borrower).event()
        let sut = self.sut(for: .borrower)
        let uuid = try XCTUnwrap(UUID(uuidString: "dd8c205f-3bb4-4f38-a60c-a9169c759850"))
        serviceMock.fetchProposalsExpectedResult = .success(.mock)
        
        sut.fetchProposals()
        sut.showInstallments(forProposalAt: IndexPath(row: 0, section: 0), in: .onGoing)
        
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(spy.action, .proposalInstallments(proposalIdentifier: uuid, agent: .borrower))
    }
    
    func testClose_WhenCalledFromController_ShouldCallDidNextStepClose() {
        let sut = self.sut(for: .investor)
        
        sut.close()
        
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        guard case .close = spy.action else {
            XCTFail("Wrong action called")
            return
        }
    }
    
    func testSendAnalyticsEventForInProgressProposalsSelected_WhenCalledFromController_ShouldSendEvent() {
        let expectedEvent = LendingProposalListEvent.inProgressProposals(.borrower).event()
        let sut = self.sut(for: .borrower)
       
        sut.sendAnalyticsEvent(selectedIndex: 0)

        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testSendAnalyticsEventForFinishedProposalsSelected_WhenCalledFromController_ShouldSendEvent() {
        let expectedEvent = LendingProposalListEvent.finishedProposals(.borrower).event()
        let sut = self.sut(for: .borrower)
       
        sut.sendAnalyticsEvent(selectedIndex: 1)

        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
}
