import XCTest
@testable import P2PLending

final class LendingSessionWebViewCoordinatorTests: XCTestCase {
    private lazy var spy = ViewControllerSpy()
    private lazy var sut: LendingSessionWebViewCoordinator = {
        let coordinator = LendingSessionWebViewCoordinator()
        coordinator.viewController = spy
        return coordinator
    }()
    
    func testPerformActionClose_WhenCalledFromPresenter_ShouldCloseViewController() {
        sut.perform(action: .close)
        
        XCTAssertEqual(spy.callDismissCount, 1)
    }
}
