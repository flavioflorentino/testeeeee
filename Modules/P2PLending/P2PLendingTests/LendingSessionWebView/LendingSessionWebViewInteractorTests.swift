import XCTest
import Core
@testable import P2PLending

final class LendingSessionWebViewPresenterSpy: LendingSessionWebViewPresenting {
    var viewController: LendingSessionWebViewDisplaying?
    
    private(set) var callPresentWebPageCount = 0
    private(set) var callPresentError = 0
    private(set) var callDidNextStepCount = 0
    
    private(set) var request: URLRequest?
    private(set) var lendingSessionWebPage: LendingSessionWebPage?
    private(set) var action: LendingSessionWebViewAction?
    
    func presentWebPage(for request: URLRequest, with lendingSessionWebPage: LendingSessionWebPage) {
        callPresentWebPageCount += 1
        self.request = request
        self.lendingSessionWebPage = lendingSessionWebPage
    }
    
    func presentError() {
        callPresentError += 1
    }
    
    func didNextStep(action: LendingSessionWebViewAction) {
        callDidNextStepCount += 1
        self.action = action
    }
}

final class LendingSessionWebViewInteractorTests: XCTestCase {
    private lazy var spy = LendingSessionWebViewPresenterSpy()
    private lazy var token: String = "123"
    
    private func interactor(for webPage: LendingSessionWebPage) -> LendingSessionWebViewInteractor {
        let keychainMock = KeychainManagerMock()
        keychainMock.set(key: KeychainKey.token, value: token)
        let dependencies = DependencyContainerMock(keychainMock)
        return LendingSessionWebViewInteractor(presenter: spy, webPage: webPage, dependencies: dependencies)
    }
    
    func testLoadPage_WhenCalledFromViewController_ShouldLoadWebPage() throws {
        let expectedWebPage = try LendingSessionWebPage.mock()
        let sut = interactor(for: expectedWebPage)
        
        sut.loadPage()
        
        XCTAssertEqual(spy.callPresentWebPageCount, 1)
        XCTAssertEqual(spy.lendingSessionWebPage, expectedWebPage)
        XCTAssertEqual(spy.request?.url, expectedWebPage.url)
        XCTAssertEqual(spy.request?.allHTTPHeaderFields?["token"], token)
    }
    
    func testPresentError_WhenCalledFromViewController_ShouldPresentError() throws {
        let expectedWebPage = try LendingSessionWebPage.mock()
        let sut = interactor(for: expectedWebPage)
        
        sut.presentError()
        
        XCTAssertEqual(spy.callPresentError, 1)
    }
    
    func testClose_WhenCalledFromViewController_ShouldClose() throws {
        let expectedWebPage = try LendingSessionWebPage.mock()
        let sut = interactor(for: expectedWebPage)
        
        sut.close()
        
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        XCTAssertEqual(spy.action, .close)
    }
}
