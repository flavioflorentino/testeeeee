import XCTest
import Core
@testable import P2PLending

private final class LendingSessionWebViewDisplaySpy: LendingSessionWebViewDisplaying {
    private(set) var callDisplayWebPageCount = 0
    private(set) var callDisplayError = 0
    private(set) var request: URLRequest?
    private(set) var lendingSessionWebPage: LendingSessionWebPage?
    
    func displayWebPage(for request: URLRequest, with lendingSessionWebPage: LendingSessionWebPage) {
        callDisplayWebPageCount += 1
        self.request = request
        self.lendingSessionWebPage = lendingSessionWebPage
    }
    
    func displayError() {
        callDisplayError += 1
    }
}

private final class LendingSessionWebViewCoordinatorSpy: LendingSessionWebViewCoordinating {
    var viewController: UIViewController?
    
    private(set) var callPerformActionCount = 0
    private(set) var action: LendingSessionWebViewAction?
    
    func perform(action: LendingSessionWebViewAction) {
        callPerformActionCount += 1
        self.action = action
    }
}

final class LendingSessionWebViewPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = LendingSessionWebViewCoordinatorSpy()
    private lazy var spy = LendingSessionWebViewDisplaySpy()
    private lazy var sut: LendingSessionWebViewPresenter = {
        let presenter = LendingSessionWebViewPresenter(coordinator: coordinatorSpy)
        presenter.viewController = spy
        return presenter
    }()
    
    func testPresentWebPage_WhenCalledFromInteractor_ShouldDisplayWebPage() throws {
        let expectedWebPage = try LendingSessionWebPage.mock()
        sut.presentWebPage(for: URLRequest(url: expectedWebPage.url), with: expectedWebPage)
        
        XCTAssertEqual(spy.callDisplayWebPageCount, 1)
        XCTAssertEqual(spy.request?.url, expectedWebPage.url)
        XCTAssertEqual(spy.lendingSessionWebPage, expectedWebPage)
    }
    
    func testPresentError_WhenCalledFromInteractor_ShouldDisplayError() {
        sut.presentError()
        
        XCTAssertEqual(spy.callDisplayError, 1)
    }
    
    func testPerformActionClose_WhenCalledFromInteractor_ShouldCloseViewController() {
        coordinatorSpy.perform(action: .close)
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .close)
    }
}
