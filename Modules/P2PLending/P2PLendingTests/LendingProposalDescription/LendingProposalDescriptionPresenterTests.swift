@testable import P2PLending
import XCTest

private final class LendingProposalDescriptionViewControllerSpy: LendingProposalDescriptionDisplay {
    
    private(set) var callDisplayFriendCount = 0
    private(set) var callDisplayObjectiveCount = 0
    
    private(set) var friend: LendingFriend?
    private(set) var objective: ObjectiveBadgeViewModel?
    
    func display(friend: LendingFriend) {
        callDisplayFriendCount += 1
        self.friend = friend
    }
    
    func display(objective: ObjectiveBadgeViewModel) {
        callDisplayObjectiveCount += 1
        self.objective = objective
    }
}

private final class LendingProposalDescriptionCoordinatorSpy: LendingProposalDescriptionCoordinating {
    var viewController: UIViewController?
    
    private(set) var callPerformActionCount = 0
    
    private(set) var action: LendingProposalDescriptionAction?
    
    func perform(action: LendingProposalDescriptionAction) {
        callPerformActionCount += 1
        self.action = action
    }
}

final class LendingProposalDescriptionPresenterTests: XCTestCase {
    private lazy var spy = LendingProposalDescriptionViewControllerSpy()
    private lazy var coordinatorSpy = LendingProposalDescriptionCoordinatorSpy()
    private lazy var sut: LendingProposalDescriptionPresenter = {
        let presenter = LendingProposalDescriptionPresenter(coordinator: coordinatorSpy)
        presenter.viewController = spy
        return presenter
    }()
    
    func testPresentPeopleInformation_WhenCalledFromViewModel_ShouldCallDisplayPeopleInformationAndFormatUserFirstNameToSubtitleText() {
        let friend: LendingFriend = .mock
        sut.present(friend: friend)
        
        XCTAssertEqual(spy.callDisplayFriendCount, 1)
        XCTAssertEqual(spy.friend, friend)
    }
    
    func testPresentObjective_WhenCalledFromViewModel_ShouldDisplayObjective() {
        let objective: ProposalObjective = .mock
        
        sut.present(objective: objective)
        
        XCTAssertEqual(spy.callDisplayObjectiveCount, 1)
        XCTAssertEqual(spy.objective?.title, objective.title)
        XCTAssertEqual(spy.objective?.imageUrl, objective.imageUrl)
    }
    
    func testDidNextStepProposalSummary_WhenCalledFromViewModel_ShouldCallPerformNextStepProposalSummary() {
        let expectedProposal: Proposal = .mock
        
        sut.didNextStep(action: .proposalAmount(proposal: expectedProposal))
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        guard case let .proposalAmount(proposal) = coordinatorSpy.action else {
            XCTFail("wrong action called")
            return
        }
        XCTAssertEqual(proposal, expectedProposal)
    }
}
