@testable import P2PLending
import XCTest
import AnalyticsModule

private final class LendingProposalDescriptionPresenterSpy: LendingProposalDescriptionPresenting {
    var viewController: LendingProposalDescriptionDisplay?
    
    private(set) var callPresentFriendCount = 0
    private(set) var callPresentObjectiveCount = 0
    private(set) var callDidNextStepCount = 0
    
    private(set) var objective: ProposalObjective?
    private(set) var friend: LendingFriend?
    private(set) var action: LendingProposalDescriptionAction?
    
    func present(friend: LendingFriend) {
        callPresentFriendCount += 1
        self.friend = friend
    }
    
    func present(objective: ProposalObjective) {
        callPresentObjectiveCount += 1
        self.objective = objective
    }
    
    func didNextStep(action: LendingProposalDescriptionAction) {
        self.callDidNextStepCount += 1
        self.action = action
    }
}

final class LendingProposalDescriptionViewModelTests: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    private lazy var dependencyMock = DependencyContainerMock(analyticsSpy)
    private lazy var proposal: Proposal = .mock
    private lazy var spy = LendingProposalDescriptionPresenterSpy()
    private lazy var sut = LendingProposalDescriptionViewModel(presenter: spy, proposal: proposal, dependencies: dependencyMock)
    
    func testFetchPeopleInformation_WhenCalledFromController_ShouldPresentPeopleInformation() {
        let friend: LendingFriend = .mock
        let objective: ProposalObjective = .mock
        
        proposal.friend = friend
        proposal.objective = objective
        
        sut.fetchPeopleInformation()
        
        XCTAssertEqual(spy.callPresentFriendCount, 1)
        XCTAssertEqual(spy.callPresentObjectiveCount, 1)
        XCTAssertEqual(spy.friend, friend)
        XCTAssertEqual(spy.objective, objective)
    }
    
    func testContinueToProposalAmount_WhenCalledFromController_ShouldCallDidNextStepProposalAmount() {
        let message = "some message"
        var expectedProposal = self.proposal
        expectedProposal.message = message
        sut.continueToProposalAmount(message: message)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 2)
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        guard case let .proposalAmount(proposal: proposal) = spy.action else {
            XCTFail("wrong action called")
            return
        }
        XCTAssertEqual(proposal, expectedProposal)
    }
    
    func testSendAnalyticsEvent_WhenCalledFromController_ShouldSendEventOnce() {
        let expectedEvent = LendingProposalDescriptionEvent.makeProposalTapped.event()
        sut.continueToProposalAmount(message: "")
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testSendAnalyticsEvent_WhenCalledFromBackButtonFromController_ShouldSendEvent() {
        let expectedEvent = LendingProposalDescriptionEvent.backButton.event()
        sut.sendAnalyticsEvent(.backButtonTapped)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testSendAnalyticsEvent_WhenCalledFromTextViewFromController_ShouldSendEvent() {
        let expectedEvent = LendingProposalDescriptionEvent.textViewdAccessed.event()
        sut.sendAnalyticsEvent(.textViewDidBeginEditing)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
}
