import XCTest
@testable import P2PLending

final class LendingProposalDescriptionCoordinatorTests: XCTestCase {
    private lazy var spy = ViewControllerSpy()
    private lazy var sut: LendingProposalDescriptionCoordinator = {
        let coordinator = LendingProposalDescriptionCoordinator(data: .successData)
        coordinator.viewController = spy
        return coordinator
    }()
    
    func testPerformProposalAmount_WhenCalledFromPresenter_ShouldShowProposalAmountController() {
        sut.perform(action: .proposalAmount(proposal: .mock))
        
        XCTAssertEqual(spy.callShowViewControllerCount, 1)
        XCTAssertTrue(spy.viewController is LendingProposalAmountViewController)
    }
}
