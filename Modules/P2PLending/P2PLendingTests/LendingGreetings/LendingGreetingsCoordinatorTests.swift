import XCTest
import SafariServices
import UI
@testable import P2PLending

final class LendingGreetingsCoordinatorTests: XCTestCase {
    private lazy var spy = ViewControllerSpy()
    private lazy var navigationSpy = NavigationControllerSpy(rootViewController: spy)
    private lazy var urlOpener = URLOpenerSpy()
    private lazy var sut: LendingGreetingsCoordinator = {
        let coordinator = LendingGreetingsCoordinator(dependencies: DependencyContainerMock(urlOpener))
        coordinator.viewController = spy
        return coordinator
    }()
    private lazy var window = UIWindow(frame: UIScreen.main.bounds)
    
    override func setUp() {
        super.setUp()
        window.addSubview(navigationSpy.view)
        RunLoop.main.run(until: Date())
    }
    
    func testPerformActionProposalObjective_WhenCalledFromPresenter_ShouldShowProposalObjectiveController() {
        sut.perform(action: .proposalObjective(configuration: .successData, proposal: .mock))
        
        XCTAssertEqual(spy.callShowViewControllerCount, 1)
        XCTAssertTrue(spy.viewController is LendingProposalObjectivesViewController)
    }
    
    func testPerformActionPrivacyPolicy_WhenCalledFromPresenter_ShouldShowPrivacyPolicy() throws {
        let url = try XCTUnwrap(URL(string: "https://www.picpay.com"))
        sut.perform(action: .legalTerms(url: url))
        
        XCTAssertEqual(spy.callPresentViewControllerCount, 1)
        XCTAssertTrue(spy.viewController is SFSafariViewController)
    }
    
    func testPerformActionDetailedError_WhenCalledFromPresenter_ShouldShowDetailedErrorController() {
        sut.perform(action: .detailedError(IdentifiableGreetingsError.oneActiveProposalLimitReached))
        
        XCTAssertEqual(spy.callShowViewControllerCount, 1)
        XCTAssertTrue(spy.viewController is ApolloFeedbackViewController)
    }
    
    func testPerformActionShowFAQ_WhenCalledFromPresenter_ShouldShowViewControllerForURL() throws {
        let url = try XCTUnwrap(URL(string: "picpay://picpay/helpcenter/article/360049386752"))
        
        sut.perform(action: .showFAQ(url: url))
        
        XCTAssertEqual(urlOpener.callCanOpenURLCount, 1)
        XCTAssertEqual(urlOpener.callOpenURLCount, 1)
        XCTAssertEqual(urlOpener.checkedURL, url)
        XCTAssertEqual(urlOpener.openedURL, url)
    }
    
    func testPerformActionClose_WhenCalledFromPresenter_ShouldDismissViewController() {
        sut.perform(action: .close)
        
        XCTAssertEqual(spy.callDismissCount, 1)
    }
    
    func testPerformActionAccountUpgradeError_WhenCalledFromPresenter_ShouldShowApolloFeedbackController() {
        sut.perform(action: .accountUpgrade(IdentifiableGreetingsError.accountUpgrade))
        
        XCTAssertEqual(spy.callShowViewControllerCount, 1)
        XCTAssertTrue(spy.viewController is ApolloFeedbackViewController)
    }
    
    func testPerformActionMaximumOpenProposalsError_WhenCalledFromPresenter_ShouldShowApolloFeedbackController() {
        sut.perform(action: .maximumOpenProposals(IdentifiableGreetingsError.openProposalsLimitReached))
        
        XCTAssertEqual(spy.callShowViewControllerCount, 1)
        XCTAssertTrue(spy.viewController is ApolloFeedbackViewController)
    }
}
