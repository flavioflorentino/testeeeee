import XCTest
import Core
@testable import P2PLending

private final class LendingGreetingsViewControllerSpy: LendingGreetingsDisplay {
    
    private(set) var callDisplayFooterTextCount = 0
    private(set) var callDisplayLegalNoticeCount = 0
    private(set) var callSetContinueButtonEnabledCount = 0
    private(set) var callDisplayCredinovoDisclaimerCount = 0
    private(set) var callDisplayAlertCount = 0
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    
    private(set) var footerText: NSAttributedString?
    private(set) var legalNoticeText: NSAttributedString?
    private(set) var continueButtonState: Bool?
    private(set) var alertTitle: String?
    private(set) var alertMessage: String?
    
    func displayFooter(footerText: NSAttributedString) {
        callDisplayFooterTextCount += 1
        self.footerText = footerText
    }
    
    func displayLegalNotice(attributedText: NSAttributedString) {
        callDisplayLegalNoticeCount += 1
        legalNoticeText = attributedText
    }
    
    func setContinueButtonEnabled(_ isEnabled: Bool) {
        callSetContinueButtonEnabledCount += 1
        continueButtonState = isEnabled
    }
    
    func displayAlert(title: String?, message: String?) {
        callDisplayAlertCount += 1
        alertTitle = title
        alertMessage = message
    }
    
    func displayCrednovoDisclaimer() {
        callDisplayCredinovoDisclaimerCount += 1
    }
    
    func startLoading() {
        callStartLoadingCount += 1
    }
    
    func stopLoading() {
        callStopLoadingCount += 1
    }
}

private final class LendingGreetingsCoordinatorSpy: LendingGreetingsCoordinating {
    var viewController: UIViewController?
    
    private(set) var callPerformActionCount = 0
    
    private(set) var action: LendingGreetingsAction?
    
    func perform(action: LendingGreetingsAction) {
        callPerformActionCount += 1
        self.action = action
    }
}

final class LendingGreetingsPresenterTests: XCTestCase {
    private typealias Localizable = Strings.Lending.Greetings
    private lazy var spy = LendingGreetingsViewControllerSpy()
    private lazy var coordinatorSpy = LendingGreetingsCoordinatorSpy()
    private lazy var sut: LendingGreetingsPresenter = {
        let presenter = LendingGreetingsPresenter(coordinator: coordinatorSpy)
        presenter.viewController = spy
        return presenter
    }()
    
    func testPresentFooterText_WhenCalledFromViewModel_ShouldDisplayFooterText() {
        sut.presentFooterText()
        
        XCTAssertEqual(spy.callDisplayFooterTextCount, 1)
        XCTAssertEqual(spy.footerText?.string, Localizable.footer)
    }
    
    func testPresentLegalTerms_WhenCalledFromViewModel_ShouldDisplayLegalNotice() throws {
        let url = try XCTUnwrap(URL(string: "https://www.picpay.com"))
        
        sut.presentLegalTerms(termsOfUseURL: url, privacyPolicyURL: url)
        
        XCTAssertEqual(spy.callDisplayLegalNoticeCount, 1)
        XCTAssertEqual(spy.legalNoticeText?.string, Localizable.legalFooterNotice)
    }
    
    func testSetContinueButtonEnabled_WhenCalledEnabledIsTrue_ShouldCallSetContinueButtonEnabledTrue() {
        let continueButtonState = true
        sut.setContinueButtonEnabled(continueButtonState)
        
        XCTAssertEqual(spy.callSetContinueButtonEnabledCount, 1)
        XCTAssertEqual(spy.continueButtonState, continueButtonState)
    }
    
    func testSetContinueButtonEnabled_WhenCalledEnabledIsFalse_ShouldCallSetContinueButtonEnabledFalse() {
        let continueButtonState = true
        sut.setContinueButtonEnabled(continueButtonState)
        
        XCTAssertEqual(spy.callSetContinueButtonEnabledCount, 1)
        XCTAssertEqual(spy.continueButtonState, continueButtonState)
    }
    
    func testPresentError_WhenCalledFromViewModel_ShouldDisplayErrorAlert() {
        let error: Error = ApiError.connectionFailure
        
        sut.present(error: error)
        
        XCTAssertEqual(spy.callDisplayAlertCount, 1)
        XCTAssertEqual(spy.alertTitle, GlobalLocalizable.errorAlertTitle)
        XCTAssertEqual(spy.alertMessage, error.localizedDescription)
    }
    
    func testPresentCrednovoDisclaimer_WhenCalledFromViewModel_ShouldDisplayCrednovoDisclaimer() {
        sut.presentCrednovoDisclaimer()
        
        XCTAssertEqual(spy.callDisplayCredinovoDisclaimerCount, 1)
    }
    
    func testStartLoading_WhenCalledFromViewModel_ShouldStartLoading() {
        sut.startLoading()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
    }
    
    func testStopLoading_WhenCalledFromViewModel_ShouldStopLoading() {
        sut.stopLoading()
        
        XCTAssertEqual(spy.callStopLoadingCount, 1)
    }
    
    func testDidNextStepProposalObjective_WhenCalledFromViewModel_ShouldPerformNextStepProposalObjective() {
        sut.didNextStep(action: .proposalObjective(configuration: .successData, proposal: .mock))
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        guard case .proposalObjective = coordinatorSpy.action else {
            XCTFail("wrong action called")
            return
        }
    }
    
    func testDidNextStepLegalTerms_WhenCalledFromViewModel_ShouldPerformNextStepLegalTerms() throws {
        let expectedUrl = try XCTUnwrap(URL(string: "https://www.picpay.com"))
        
        sut.didNextStep(action: .legalTerms(url: expectedUrl))
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        guard case let .legalTerms(url) = coordinatorSpy.action else {
            XCTFail("wrong action called")
            return
        }
        XCTAssertEqual(url, expectedUrl)
    }
    
    func testDidNextStepDetailedError_WhenCalledFromViewModel_ShouldPerformNextStepDetailedError() {
        let expectedDetailedError = IdentifiableGreetingsError.oneActiveProposalLimitReached
        
        sut.didNextStep(action: .detailedError(expectedDetailedError))
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        guard case let .detailedError(detailedError) = coordinatorSpy.action else {
            XCTFail("wrong action called")
            return
        }
        XCTAssertEqual(detailedError.asDetailedError, expectedDetailedError.asDetailedError)
    }
    
    func testDidNextStepShowFAQ_WhenCalledFromViewModel_ShouldPerformNextStepShowFAQ() throws {
        let url = try XCTUnwrap(URL(string: "picpay://picpay/helpcenter/article/360049386752"))
        
        sut.didNextStep(action: .showFAQ(url: url))
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        guard case let .showFAQ(faqUrl) = coordinatorSpy.action else {
            XCTFail("wrong action called")
            return
        }
        XCTAssertEqual(faqUrl, url)
    }
    
    func testDidNextStepClose_WhenCalledFromViewModel_ShouldPerformNextStepClose() throws {
        sut.didNextStep(action: .close)
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        guard case .close = coordinatorSpy.action else {
            XCTFail("wrong action called")
            return
        }
    }
}
