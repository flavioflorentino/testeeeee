import XCTest
import Core
import AnalyticsModule
@testable import P2PLending

final class LendingGreetingsServiceMock: LendingGreetingsServicing {
    var fetchProposalConfigurationExpectedResult: Result<GreetingsResponse, ApiError>?
    
    func fetchProposalConfiguration(completion: @escaping ModelCompletionBlock<GreetingsResponse>) {
        callCompletionIfNeeded(completion, expectedResult: fetchProposalConfigurationExpectedResult)
    }
}

final class LendingGreetingsPresenterSpy: LendingGreetingsPresenting {
    var viewController: LendingGreetingsDisplay?
    
    private(set) var callPresentFooterTextCount = 0
    private(set) var callPresentLegalTermsCount = 0
    private(set) var callPresentCredinovoDisplaimerCount = 0
    private(set) var callSetContinueButtonEnabledCount = 0
    private(set) var callPresentErrorCount = 0
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    private(set) var callDidNextStepCount = 0
    
    private(set) var termsOfUseURL: URL?
    private(set) var privacyPolicyURL: URL?
    private(set) var error: Error?
    private(set) var action: LendingGreetingsAction?
    private(set) var continueButtonStatus: Bool?
    
    func presentFooterText() {
        callPresentFooterTextCount += 1
    }
    
    func presentLegalTerms(termsOfUseURL: URL, privacyPolicyURL: URL) {
        callPresentLegalTermsCount += 1
        self.termsOfUseURL = termsOfUseURL
        self.privacyPolicyURL = privacyPolicyURL
    }
    
    func presentCrednovoDisclaimer() {
        callPresentCredinovoDisplaimerCount += 1
    }
    
    func setContinueButtonEnabled(_ isEnabled: Bool) {
        callSetContinueButtonEnabledCount += 1
        continueButtonStatus = isEnabled
    }
    
    func present(error: Error) {
        callPresentErrorCount += 1
        self.error = error
    }
    
    func startLoading() {
        callStartLoadingCount += 1
    }
    
    func stopLoading() {
        callStopLoadingCount += 1
    }
    
    func didNextStep(action: LendingGreetingsAction) {
        callDidNextStepCount += 1
        self.action = action
    }
}

final class LendingGreetingsViewModelTests: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    private lazy var spy = LendingGreetingsPresenterSpy()
    private lazy var serviceMock = LendingGreetingsServiceMock()
    private lazy var dependencyMock = DependencyContainerMock(analyticsSpy)
    private lazy var sut = LendingGreetingsViewModel(service: serviceMock, presenter: spy, dependencies: dependencyMock)
    
    func testFetchProposalConfiguration_WhenReceiveSuccess_ShouldPresentLegalTerms() {
        let expectedEvent = LendingGreetingsEvent.suitableBorrower(.borrower).event()
        let response: GreetingsResponse = .successData
        serviceMock.fetchProposalConfigurationExpectedResult = .success(response)
        
        sut.fetchLendingProposalConfiguration()
        
        XCTAssertEqual(spy.callPresentLegalTermsCount, 1)
        XCTAssertEqual(spy.callPresentFooterTextCount, 1)
        XCTAssertEqual(spy.termsOfUseURL, response.greetings.termsConditions)
        XCTAssertEqual(spy.privacyPolicyURL, response.greetings.privacyPolicy)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testFetchProposalConfiguration_WhenReceiveFailureWithGreetingsError_ShouldCallDidNextStepDetailedError() {
        let identifiableError: IdentifiableGreetingsError = .oneActiveProposalLimitReached
        var requestError = RequestError()
        requestError.code = identifiableError.rawValue
        serviceMock.fetchProposalConfigurationExpectedResult = .failure(.badRequest(body: requestError))
        
        sut.fetchLendingProposalConfiguration()
        
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        guard case let .detailedError(detailedError) = spy.action else {
            XCTFail("Wrong action called")
            return
        }
        XCTAssertEqual(detailedError.asDetailedError, identifiableError.asDetailedError)
    }
    
    func testFetchProposalConfiguration_WhenReceiveFailure_ShouldPresentError() {
        let error: ApiError = .connectionFailure
        serviceMock.fetchProposalConfigurationExpectedResult = .failure(error)
        
        sut.fetchLendingProposalConfiguration()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callStopLoadingCount, 1)
        XCTAssertEqual(spy.callPresentErrorCount, 1)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
    }
    
    
    func testSendTrackingEvent_WhenCalledFromControllerWillDisappear_ShouldSendEvent() {
        let expectedEvent = LendingGreetingsEvent.backButtonTapped(.borrower).event()
        sut.sendTrackingEvent(.backButtonTapped(.borrower))
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testAwarenessAndAcceptanceOfTermsChanged_WhenAcceptedIsTrue_ShouldEnableContinueButton() {
        let expectedEvent = LendingGreetingsEvent.termsAndPrivacyPolicyCheckBox(.borrower).event()
        let status = true
        sut.awarenessAndAcceptanceOfTermsChanged(status)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(spy.callSetContinueButtonEnabledCount, 1)
        XCTAssertEqual(spy.continueButtonStatus, status)
    }
    
    func testAwarenessAndAcceptanceOfTermsChanged_WhenAcceptedIsFalse_ShouldDisableContinueButton() {
        let expectedEvent = LendingGreetingsEvent.termsAndPrivacyPolicyCheckBox(.borrower).event()
        let status = false
        sut.awarenessAndAcceptanceOfTermsChanged(status)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(spy.callSetContinueButtonEnabledCount, 1)
        XCTAssertEqual(spy.continueButtonStatus, status)
    }
    
    func testShowCrednovoDisclaimer_WhenCalledFromController_ShouldCallPresentCrednovoDisclaimer() {
        let expectedEvent = LendingGreetingsEvent.crednovo(.borrower).event()
        sut.showCrednovoDisclaimer()
        
        XCTAssertEqual(spy.callPresentCredinovoDisplaimerCount, 1)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testMakeProposalButtonTapped_WhenCalledFromController_ShouldCallDidNextStepProposalObjective() throws {
        serviceMock.fetchProposalConfigurationExpectedResult = .success(.successData)
        sut.fetchLendingProposalConfiguration()
        
        sut.makeProposal()
        
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        guard case .proposalObjective = spy.action else {
            XCTFail("Wrong action called")
            return
        }
    }
    
    func testShowLegalTerms_WhenCalledFromController_ShouldCallDidNextStepLegalTerms() throws {
        let expectedUrl = try XCTUnwrap(URL(string: "http://www.picpay.com"))
        
        sut.showLegalTerms(for: expectedUrl)
        
        //XCTAssertEqual(analytics.logCalledCount, 1) //TODO: waiting for the right url
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        guard case let .legalTerms(url) = spy.action else {
            XCTFail("Wrong action called")
            return
        }
        XCTAssertEqual(url, expectedUrl)
    }
    
    func testShowHelp_WhenCalledFromController_ShouldCallDidNextStepShowFAQ() {
        let expectedEvent = LendingGreetingsEvent.helpButtonTapped(.borrower).event()
        sut.showHelp()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        guard case .showFAQ = spy.action else {
            XCTFail("wrong action called")
            return
        }
    }
    
    func testClose_WhenCalledFromController_ShouldCallDidNextStepClose() {
        sut.close()
        
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        guard case .close = spy.action else {
            XCTFail("wrong action called")
            return
        }
    }
}
