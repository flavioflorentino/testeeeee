import UI
@testable import P2PLending

extension OpportunityOfferDisplayModel {
    private typealias Localizable = Strings.Lending.Opportunity.Offer
    
    static func mock(for status: OpportunityOfferStatus = .waitingReply, isInstallmentsAvailable: Bool = false) -> OpportunityOfferDisplayModel {
        OpportunityOfferDisplayModel(
            friendCallout: NSAttributedString(string: "@username\(Localizable.friendCalloutSufix)"),
            friend: LendingFriend.mock,
            friendUserName: "@username",
            status: OpportunityOfferStatusViewModel(status: status, agent: .investor),
            message: "Ei, vou fazer um curso TOP que te falei! Será que você consegue me ajudar nessas condições?",
            shouldShowMessageBubble: true,
            requestedAmountPlusIOF: 1_006.20.toCurrencyString(),
            installments: Localizable.installmentsItemValue(10, 100.toCurrencyString() ?? ""),
            numberOfInstallments: Localizable.numberOfInstallmentsItemValue(10),
            totalPaymentValue: 1200.toCurrencyString(),
            totalPaymentReceivedValue: 2000.toCurrencyString(),
            paymentDateDescription: "18/08/2020",
            appliedInterestRate: Localizable.appliedInterestRateItemValue("3,0%"),
            ir: (-30).toCurrencyString(),
            primaryButtonTitle: primaryButtonTitle(for: status),
            shouldDisplayPrimaryButton: status == .waitingReply || status == .accepted,
            shouldDisplaySecondaryButton: status == .waitingReply,
            shouldDisplayFriendCallout: !(status == .liquidated || status == .declined || status == .expired || status == .canceled),
            shouldDisplayStatus: true,
            shouldDisplayInstallmentButton: shouldDisplayInstallmentsButton(for: status, isInstallmentsAvailable: isInstallmentsAvailable)
        )
    }
    
    private static func primaryButtonTitle(for status: OpportunityOfferStatus) -> String? {
        switch status {
        case .waitingReply:
            return Localizable.continueButtonTitle
        case .accepted:
            return Localizable.lendNowButtonTitle
        default:
            return nil
        }
    }
    
    private static func shouldDisplayInstallmentsButton(for status: OpportunityOfferStatus, isInstallmentsAvailable: Bool) -> Bool {
        status == .active && isInstallmentsAvailable
    }
}

extension OpportunityOfferDisplayModel: Equatable {
    public static func == (lhs: OpportunityOfferDisplayModel, rhs: OpportunityOfferDisplayModel) -> Bool {
        return lhs.friendCallout?.string == rhs.friendCallout?.string
            && lhs.friend?.firstName == rhs.friend?.firstName
            && lhs.friend?.imageURL == rhs.friend?.imageURL
            && lhs.status == rhs.status
            && lhs.message == rhs.message
            && lhs.requestedAmountPlusIOF == rhs.requestedAmountPlusIOF
            && lhs.shouldShowMessageBubble == rhs.shouldShowMessageBubble
            && lhs.installments == rhs.installments
            && lhs.totalPaymentValue == rhs.totalPaymentValue
            && lhs.totalPaymentReceivedValue == rhs.totalPaymentReceivedValue
            && lhs.paymentDateDescription == rhs.paymentDateDescription
            && lhs.appliedInterestRate == rhs.appliedInterestRate
            && lhs.ir == rhs.ir
            && lhs.primaryButtonTitle == rhs.primaryButtonTitle
            && lhs.shouldDisplayPrimaryButton == rhs.shouldDisplayPrimaryButton
            && lhs.shouldDisplaySecondaryButton == rhs.shouldDisplaySecondaryButton
            && lhs.shouldDisplayFriendCallout == rhs.shouldDisplayFriendCallout
            && lhs.shouldDisplayStatus == rhs.shouldDisplayStatus
            && lhs.shouldDisplayInstallmentButton == rhs.shouldDisplayInstallmentButton
    }
}
