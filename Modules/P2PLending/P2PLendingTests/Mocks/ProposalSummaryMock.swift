@testable import P2PLending
import XCTest

extension ProposalSummary {
    fileprivate typealias Localizable = Strings.Lending.Proposal.Summary
    
    static func mock(for status: OpportunityOfferStatus? = nil, isInstallmentsAvailable: Bool = false) throws -> ProposalSummary {
        ProposalSummary(
            status: getStatusViewModel(status: status),
            requestedAmount: 1000.00.toCurrencyString(),
            friendName: "Full Name",
            installments: Localizable.installmentsItemValue(10, 100.toCurrencyString()!),
            totalAmount: 1300.00.toCurrencyString(),
            paymentDateNotice: "18/08/2020",
            cet: Localizable.cetItemValue("3,5%"),
            interestRate: Localizable.interestRateItemValue("3,0%"),
            iof: 6.2.toCurrencyString(),
            commissionAmount: "\(try XCTUnwrap(40.toCurrencyString())) parcelado em \(10)x",
            continueButtonTitle: continueButtonTitle(for: status, isInstallmentsAvailable: isInstallmentsAvailable),
            showsCancelButton: status == .waitingReply,
            showsStatusView: status != nil,
            showsFirstPaymentDate: showsFirstPaymentDate(for: status),
            showsCcbContract: status == .active
        )
    }
    
    static private func continueButtonTitle(for status: OpportunityOfferStatus?, isInstallmentsAvailable: Bool) -> String? {
        guard let status = status else { return Localizable.continueButtonTitle }
        guard status == .active && isInstallmentsAvailable else { return nil }
        return Localizable.installmentsButtonTitle
    }
    
    static private func showsFirstPaymentDate(for status: OpportunityOfferStatus?) -> Bool {
        guard let status = status else {
            return true
        }
        return OpportunityOfferStatus.inProgress.contains(status)
    }
    
    static private func getStatusViewModel(status: OpportunityOfferStatus?) -> OpportunityOfferStatusViewModel? {
        guard let status = status else { return nil }
        return OpportunityOfferStatusViewModel(status: status, agent: .borrower)
    }
}
