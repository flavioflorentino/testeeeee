import Foundation
import UI
@testable import P2PLending

extension ProposalListItemViewModel: Equatable {
    private typealias Localizable = Strings.Lending.Proposal.List
    
    static func mock(status: OpportunityOfferStatus, agent: ConsultingAgent, isInstallmentsAvailable: Bool = false) -> ProposalListItemViewModel {
        ProposalListItemViewModel(
            identifier: UUID(uuidString: "dd8c205f-3bb4-4f38-a60c-a9169c759850") ?? UUID(),
            friendProfilePhotoURL: nil,
            friendStatusCallout: description(for: status, agent: agent),
            requestedAmount: 1_000.toCurrencyString(),
            totalAmountTitle: totalAmountTitle(for: status, and: agent),
            totalAmountValue: "\(1_200.toCurrencyString() ?? "") (+20%)",
            formattedDate: nil,
            shouldDisplayInstallmentsButton: shouldDisplayInstallmentsButton(for: status, agent: agent, isInstallmentsAvailable: isInstallmentsAvailable)
        )
    }
    
    private static func shouldDisplayInstallmentsButton(for status: OpportunityOfferStatus, agent: ConsultingAgent, isInstallmentsAvailable: Bool) -> Bool {
        status == .active && isInstallmentsAvailable
    }
    
    private static func description(for status: OpportunityOfferStatus, agent: ConsultingAgent) -> NSAttributedString {
        let statusDescriptionProvider = opportunityOfferStatusDescriptionProvider(for: agent)
        let user = LendingFriend.mock
        let builder = OpportunityOfferStatusDescriptionBuilder(person: user, descriptionProvider: statusDescriptionProvider)
        return builder.opportunityOfferStatusDescription(for: status)
    }
    
    private static func totalAmountTitle(for status: OpportunityOfferStatus, and agent: ConsultingAgent) -> String? {
        switch status {
        case .waitingReply, .accepted, .processingPayment, .active:
            return agent == .borrower ? Localizable.totalAmountNotPaidBorrowerTitle : Localizable.totalAmountNotPaidInvestorTitle
        case .liquidated:
            return agent == .borrower ? Localizable.totalAmountPaidBorrowerTitle : Localizable.totalAmountPaidInvestorTitle
        default:
            return Localizable.totalAmountPlusTaxTitle
        }
    }
    
    private static func opportunityOfferStatusDescriptionProvider(for agent: ConsultingAgent) -> OpportunityOfferStatusDescriptionProvider {
        switch agent {
        case .investor:
            return InvestorOpportunityOfferStatusDescriptionProvider()
        case .borrower:
            return BorrowerOpportunityOfferStatusDescriptionProvider()
        }
    }
    
    public static func == (lhs: ProposalListItemViewModel, rhs: ProposalListItemViewModel) -> Bool {
        lhs.identifier == rhs.identifier
            && lhs.friendProfilePhotoURL == rhs.friendProfilePhotoURL
            && lhs.friendStatusCallout?.string == rhs.friendStatusCallout?.string
            && lhs.requestedAmount == rhs.requestedAmount
            && lhs.totalAmountTitle == rhs.totalAmountTitle
            && lhs.totalAmountValue == rhs.totalAmountValue
            && lhs.formattedDate == rhs.formattedDate
            && lhs.shouldDisplayInstallmentsButton == rhs.shouldDisplayInstallmentsButton
    }
}
