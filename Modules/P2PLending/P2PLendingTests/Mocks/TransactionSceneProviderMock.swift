import P2PLending

final class TransactionSceneProviderSpy: TransactionSceneProvider {
    private(set) var callTransactionSceneCount = 0
    
    private(set) var borrower: LendingFriend?
    private(set) var value: Double?
    private(set) var offerIdentifier: UUID?
    
    func transactionScene(for borrower: LendingFriend, value: Double, offerIdentifier: UUID) -> UIViewController {
        callTransactionSceneCount += 1
        self.borrower = borrower
        self.value = value
        self.offerIdentifier = offerIdentifier
        return UIViewController()
    }
}
