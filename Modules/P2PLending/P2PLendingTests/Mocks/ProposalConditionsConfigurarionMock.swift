@testable import P2PLending

extension ProposalConditionsConfiguration {
    static var mock: ProposalConditionsConfiguration {
        ProposalConditionsConfiguration(
            maximumInstallments: 12,
            minimumInstallments: 1,
            defaultInstallments: 12,
            minimumInstallmentsValue: 100,
            minimumTax: 0.0,
            maximumTax: 3.0,
            defaultTax: 0.0
        )
    }
}
