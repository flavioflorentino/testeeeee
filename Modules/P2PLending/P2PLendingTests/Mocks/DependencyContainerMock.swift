import Core
import FeatureFlag
import AnalyticsModule

@testable import P2PLending

final class DependencyContainerMock: AppDependencies {
    lazy var mainQueue: DispatchQueue = resolve(default: .main)
    lazy var userContract: UserContract? = resolve(default: UserContractMock())
    lazy var featureManager: FeatureManagerContract = resolve(default: FeatureManagerMock())
    lazy var kvStore: KVStoreContract = resolve(default: KVStoreMock())
    lazy var analytics: AnalyticsProtocol = resolve(default: AnalyticsSpy())
    lazy var transactionSceneProvider: TransactionSceneProvider? = resolve(default: TransactionSceneProviderSpy())
    lazy var urlOpener: URLOpener? = resolve(default: URLOpenerSpy())
    lazy var billetSceneProvider: BilletSceneProvider? = resolve(default: BilletSceneProviderSpy())
    lazy var keychain: KeychainManagerContract = resolve(default: KeychainManagerMock())
    lazy var auth: AuthenticationContract? = resolve(default: AuthenticationContractMock())
    lazy var ppauth: PPAuthSwiftContract = resolve(default: PPAuthSwiftContractSpy())
    
    private let dependencies: [Any]
    
    init(_ dependencies: Any...) {
        self.dependencies = dependencies
    }
}

extension DependencyContainerMock {
    func resolve<T>(default: T) -> T {
        guard let mock = dependencies.first(where: { $0 is T }) as? T else {
            return `default`
        }
        return mock
    }
}
