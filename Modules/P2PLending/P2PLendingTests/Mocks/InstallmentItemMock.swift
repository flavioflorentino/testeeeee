import Foundation
@testable import P2PLending

extension InstallmentItem {
    static func mock(for status: InstallmentStatus = .active) -> InstallmentItem {
        InstallmentItem(number: 1,
                        value: 100,
                        dueDate: Date(),
                        payable: true,
                        status: status)
    }
}
