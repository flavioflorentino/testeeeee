@testable import P2PLending

struct UserContractMock: UserContract {
    var user: PersonConvertible?
    
    init(user: PersonConvertible = Person(imageURL: nil, firstName: "John")) {
        self.user = user
    }
}
