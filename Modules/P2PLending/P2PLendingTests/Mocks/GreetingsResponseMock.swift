@testable import P2PLending

extension GreetingsResponse {
    static var successData: GreetingsResponse {
        GreetingsResponse(
            greetings: .mock,
            proposalConfiguration: .mock,
            objectives: [ProposalObjective](repeating: .mock, count: 20)
        )
    }
}

extension Greetings {
    static var mock: Greetings {
        Greetings(
            privacyPolicy: URL(string: "https://www.picpay.com"),
            termsConditions: URL(string: "https://www.picpay.com")
        )
    }
}

extension ProposalAmountConfiguration {
    static var mock: ProposalAmountConfiguration {
        ProposalAmountConfiguration(
            minimumAmount: 100.0,
            maximumAmount: 15_000.0
        )
    }
}

extension ProposalObjective {
    static var mock: ProposalObjective {
        ProposalObjective(
            id: "d05f32ec-2891-4cd5-a108-12e2f3e0f41e",
            imgUrl: nil,
            title: "Some objective"
        )
    }
}
