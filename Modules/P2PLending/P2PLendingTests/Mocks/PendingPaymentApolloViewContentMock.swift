import UI
@testable import P2PLending
import AssetsKit

struct PendingPaymentApolloViewContentMock {
    private typealias Localizable = Strings.Lending.Installment.List
    
    let expectedContent = ApolloFeedbackViewContent(image: Resources.Illustrations.iluWarning.image,
                                                    title: Localizable.pendingPaymentTitle,
                                                    description: NSAttributedString(string: Localizable.pendingPaymentBody),
                                                    primaryButtonTitle: GlobalLocalizable.dismissButtonTitle,
                                                    secondaryButtonTitle: Localizable.pendingPaymentFaqButtonTitle)
}

