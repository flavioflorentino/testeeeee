@testable import P2PLending

extension ProposalConditionsSimulation {
    static var mock: ProposalConditionsSimulation {
        ProposalConditionsSimulation(
            totalAmount: 1000,
            totalInterest: 1300,
            interestRateAmount: 3,
            installments: 12,
            installmentAmount: 100,
            monthCet: 3.5,
            iof: 6.2,
            commission: 4,
            commissionAmount: 40,
            monthRate: 3,
            firstPaymentDate: Date(timeIntervalSinceReferenceDate: 619469951.311318)
        )
    }
}
