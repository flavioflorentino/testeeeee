import P2PLending

final class URLOpenerSpy: URLOpener {
    private(set) var callCanOpenURLCount = 0
    private(set) var callOpenURLCount = 0
    
    private(set) var checkedURL: URL?
    private(set) var openedURL: URL?
    private(set) var openedURLOptions: [UIApplication.OpenExternalURLOptionsKey: Any]?
    
    func canOpenURL(_ url: URL) -> Bool {
        self.checkedURL = url
        callCanOpenURLCount += 1
        return true
    }
    
    func open(_ url: URL, options: [UIApplication.OpenExternalURLOptionsKey: Any], completionHandler: ((Bool) -> Void)?) {
        callOpenURLCount += 1
        self.openedURL = url
        self.openedURLOptions = options
    }
}
