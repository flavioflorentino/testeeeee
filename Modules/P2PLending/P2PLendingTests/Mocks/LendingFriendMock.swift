@testable import P2PLending

extension LendingFriend {
    static var mock: LendingFriend {
        LendingFriend(
            id: 13,
            userName: "username",
            name: "Full Name",
            profileUrl: nil,
            contractUrl: URL(string: "https://www.picpay.com")
        )
    }
}
