import UIKit
@testable import P2PLending

extension DetailedError {
    private typealias Localizable = Strings.Lending.DetailedError
    
    static var mock: DetailedError {
        DetailedError(
            image: Assets.lendingFigureWarning.image,
            title: Localizable.greetingsTitle,
            message: Localizable.activeProposalsLimitReachedBody,
            primaryButtonTitle: Localizable.dismissButtonTitle,
            linkButtonTitle: Localizable.lendingLimitsFaqButtonTitle,
            url: URL(string: "https://www.picpay.com/site")
        )
    }
}
