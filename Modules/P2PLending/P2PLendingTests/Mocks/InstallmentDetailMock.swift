@testable import P2PLending

extension InstallmentDetail {
    static func mock(dueDate: Date = Date()) -> InstallmentDetail {
        InstallmentDetail(
            number: 1,
            numberOfInstallments: 12,
            value: 200,
            dueDate: dueDate,
            digitableLine: "12345",
            penaltyValue: 20.0,
            daysOverdue: 2,
            originalValue: 180
        )
    }
}
