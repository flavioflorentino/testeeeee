import Foundation
@testable import P2PLending

extension ProposalListItem {
    static func mock(status: OpportunityOfferStatus = .waitingReply) -> ProposalListItem {
        ProposalListItem(
            user: .mock,
            uuid: UUID(uuidString: "dd8c205f-3bb4-4f38-a60c-a9169c759850") ?? UUID(),
            amount: 1000,
            totalInterest: 1200,
            tax: 20,
            status: status,
            createdAt: Date(timeIntervalSinceReferenceDate: 617663257),
            expiredAt: Date(timeIntervalSinceReferenceDate: 617663257)
        )
    }
}
