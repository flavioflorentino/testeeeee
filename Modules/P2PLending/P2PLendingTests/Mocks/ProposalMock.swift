@testable import P2PLending

extension Proposal {
    static var mock: Proposal {
        Proposal(
            amount: 1000,
            installments: 10,
            interest: 3.0,
            objective: nil,
            firstPaymentDeadline: 30,
            message: "Some message",
            friend: .mock,
            shouldShareCreditAptitude: nil,
            simulation: .mock
        )
    }
}
