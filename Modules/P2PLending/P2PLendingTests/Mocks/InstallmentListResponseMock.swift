import Foundation
@testable import P2PLending

extension InstallmentListResponse {
    static var mock: InstallmentListResponse {
        InstallmentListResponse(outstandingBalance: 100.0, inProgress: [InstallmentItem.mock()], finished: [InstallmentItem.mock()])
    }
}
