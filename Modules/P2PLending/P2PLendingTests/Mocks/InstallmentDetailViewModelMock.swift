import Foundation
@testable import P2PLending

extension InstallmentDetailViewModel {
    private typealias Localizable = Strings.Lending.Installment.Detail
    
    private static var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "pt-BR")
        dateFormatter.setLocalizedDateFormatFromTemplate("MMMMd")
        return dateFormatter
    }()
    
    static func mock(dueDate: Date = Date()) -> InstallmentDetailViewModel {
        InstallmentDetailViewModel(
            valueTitle: valueTitle(for: dueDate),
            value: 200.toCurrencyString(),
            dueDateDescription: dueDateDescription(for: dueDate),
            daysOverdue: "2",
            penaltyValue: 20.toCurrencyString(),
            originalValue: 180.toCurrencyString(),
            shouldDisplayDetailBreakdown: shouldDisplayDetailBreakdown(for: dueDate)
        )
    }
    
    private static func valueTitle(for dueDate: Date) -> String {
        guard Calendar.current.compare(Date(), to: dueDate, toGranularity: .day) == .orderedDescending else {
            return Localizable.installmentValueTitle
        }
        return Localizable.installmentUpdatedValueTitle
    }
    
    private static func dueDateDescription(for dueDate: Date) -> String {
        guard Calendar.current.compare(Date(), to: dueDate, toGranularity: .day) == .orderedAscending else {
            return Localizable.installmentDueDateToday
        }
        return Localizable.installmentDueDateDescription(dateFormatter.string(from: dueDate))
    }
    
    private static func shouldDisplayDetailBreakdown(for dueDate: Date) -> Bool {
        Calendar.current.compare(Date(), to: dueDate, toGranularity: .day) == .orderedDescending
    }
}
