@testable import P2PLending
import XCTest

extension LendingSessionWebPage {
    static func mock() throws -> LendingSessionWebPage {
        let url = try XCTUnwrap(URL(string: "https://www.picpay.com"))
        return LendingSessionWebPage(title: "Contrato", url: url)
    }
}
