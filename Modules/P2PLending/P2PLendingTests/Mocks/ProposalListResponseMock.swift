import Foundation
@testable import P2PLending

extension ProposalListResponse {
    static var mock: ProposalListResponse {
        ProposalListResponse(
            onGoing: [.mock(status: .waitingReply), .mock(status: .active)],
            terminated: [.mock(status: .canceled), .mock(status: .expired)],
            requestTime: Date(timeIntervalSinceReferenceDate: 617663257)
        )
    }
}
