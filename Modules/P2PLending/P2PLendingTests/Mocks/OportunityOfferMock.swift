@testable import P2PLending
import XCTest

extension OpportunityOffer {
    static func mock(for status: OpportunityOfferStatus = .waitingReply) throws -> OpportunityOffer {
        let safeUrl = try XCTUnwrap(URL(string: "https://www.picpay.com"))
        let uuid = try XCTUnwrap(UUID(uuidString: "2c091bff-95e2-4291-a940-21e237292aea"))
        return OpportunityOffer(
            id: uuid,
            contractOriginal: "OFCC3G1606411053687",
            status: status,
            borrower: .mock,
            investor: .mock,
            message: "Ei, vou fazer um curso TOP que te falei! Será que você consegue me ajudar nessas condições?",
            payDay: 30,
            operationDetails: .mock,
            cv: safeUrl,
            privacyPolicy: safeUrl,
            termsConditions: safeUrl
        )
    }
}
