@testable import P2PLending

extension OperationDetail {
    static var mock: OperationDetail {
        OperationDetail(
            amount: 1000,
            commissionAmount: 40,
            installments: 10,
            monthRate: 3.0,
            subtotal: 1200,
            borrowerInstallmentsAmount: 100.0,
            iof: 6.2,
            investorInstallmentsAmount: 100.0,
            totalInterest: 1300.0,
            investorTotalIncome: 1000,
            investorTotalInterest: 2000,
            investorIr: 30,
            firstPaymentDate: Date(timeIntervalSinceReferenceDate: 619469951.311318),
            cet: 3.5
        )
    }
}
