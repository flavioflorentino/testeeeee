@testable import P2PLending

extension CCBData {
    static var mock = CCBData(
        contract: .mock,
        borrower: .mock,
        attachments: .mock
    )
}

extension CCBContract {
    static var mock = CCBContract(
        issueDate: Date(),
        issueCity: "São Paulo",
        issueUf: "SP",
        comissionAmount: 20,
        firstDeadLine: Date(),
        payDay: 20,
        installments: 12,
        totalRequestedAmount: 2200,
        requestedAmount: 2000,
        borrowerInstallmentsAmount: 200,
        totalInterest: 2020,
        monthRate: 1,
        yearRate: 2,
        monthCet: 2,
        yearCet: 1,
        iof: 20,
        periodicity: .monthly,
        finnancialFine: 2,
        finnancialSafe: 100,
        defaultInterest: 1
    )
}

extension CCBBorrower {
    static var mock = CCBBorrower(
        name: "Some name",
        userName: "someusername",
        document: "12345678912",
        address: "some address",
        email: "some@email.com"
    )
}

extension CCBAttachment {
    static var mock = CCBAttachment(
        a: .mock,
        b: .mock,
        c: .mock,
        c1: .mock,
        c2: .mock,
        c3: .mock,
        c4: .mock
    )
}

extension CCBAttachment.Item {
    static var mock = CCBAttachment.Item(value: 200, percent: 2.0)
}
