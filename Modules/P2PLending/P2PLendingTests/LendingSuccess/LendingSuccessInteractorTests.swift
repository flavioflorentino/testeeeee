import XCTest
import AnalyticsModule
@testable import P2PLending

final class LendingSuccessDelegateSpy: LendingSuccessDelegate {
    private(set) var didFinishFlowCount = 0
    
    func lendingSuccessControllerDidDismiss(_ lendingSuccessViewController: UIViewController) {
        didFinishFlowCount += 1
    }
}

private final class LendingSuccessPresenterSpy: LendingSuccessPresenting {
    var viewController: LendingSuccessDisplay?
    
    private(set) var callDidNextStepCount = 0
    private(set) var callPresentContentCount = 0
    private(set) var callDidFinishFlowCount = 0
    
    private(set) var action: LendingSuccessAction?
    private(set) var content: SuccessContent?
    
    func present(content: SuccessContent) {
        callPresentContentCount += 1
        self.content = content
    }
    
    func didNextStep(action: LendingSuccessAction) {
        callDidNextStepCount += 1
        self.action = action
    }
}

final class LendingSuccessInteractorTests: XCTestCase {
    private lazy var spy = LendingSuccessPresenterSpy()
    private lazy var content = ProposalSentSuccessContent()
    private lazy var sut = LendingSuccessInteractor(presenter: spy, content: content)
    
    func testFetchContent_WhenCalledFromController_ShouldPresentContent() {
        sut.fetchContent()
        
        XCTAssertEqual(spy.callPresentContentCount, 1)
        XCTAssertEqual(spy.content?.title, content.title)
        XCTAssertEqual(spy.content?.body?.string, content.body?.string)
    }
    
    func testDismiss_WhenCalledFromController_ShouldCallDidNextStepDismiss() {
        sut.dismiss()
        
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        XCTAssertEqual(spy.action, .dismiss)
    }
}
