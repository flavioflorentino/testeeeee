import XCTest
@testable import P2PLending

private final class LendingSuccessDisplaySpy: LendingSuccessDisplay {
    private(set) var callDisplayContentCount = 0
    private(set) var content: SuccessContent?
    
    func display(content: SuccessContent) {
        callDisplayContentCount += 1
        self.content = content
    }
}

private final class LendingSuccessCoordinatorSpy: LendingSuccessCoordinating {
    var viewController: UIViewController?
    
    private(set) var callPerformActionCount = 0
    
    private(set) var action: LendingSuccessAction?
    
    func perform(action: LendingSuccessAction) {
        callPerformActionCount += 1
        self.action = action
    }
}

final class LendingSuccessPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = LendingSuccessCoordinatorSpy()
    private lazy var spy = LendingSuccessDisplaySpy()
    private lazy var sut: LendingSuccessPresenter = {
        let presenter = LendingSuccessPresenter(coordinator: coordinatorSpy)
        presenter.viewController = spy
        return presenter
    }()
    
    func testPresentContent_WhenCalledFromInteractor_ShouldDisplayContent() {
        let content = ProposalSentSuccessContent()
        sut.present(content: content)
        
        XCTAssertEqual(spy.callDisplayContentCount, 1)
        XCTAssertEqual(spy.content?.title, content.title)
        XCTAssertEqual(spy.content?.body, content.body)
    }
    
    func testDidNextStepDismiss_WhenCalledFromViewModel_ShouldPerformNextStepDismiss() {
        let action: LendingSuccessAction = .dismiss
        sut.didNextStep(action: action)
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        XCTAssertEqual(coordinatorSpy.action, action)
    }
}
