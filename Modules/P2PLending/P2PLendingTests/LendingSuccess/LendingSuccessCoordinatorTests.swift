import XCTest
@testable import P2PLending

final class LendingSuccessCoordinatorTests: XCTestCase {
    private lazy var spy = ViewControllerSpy()
    private lazy var sut: LendingSuccessCoordinator = {
        let coordinator = LendingSuccessCoordinator()
        coordinator.viewController = spy
        return coordinator
    }()
    
    func testPerformActionDismiss_WhenCalledFromPresenter_ShouldDismissViewController() {
        sut.perform(action: .dismiss)
        
        XCTAssertEqual(spy.callDismissCount, 1)
    }
}
