import XCTest
import AnalyticsModule
@testable import P2PLending

final class LendingDigitableLinePresenterSpy: LendingDigitableLinePresenting {
    var viewController: LendingDigitableLineDisplaying?
    
    private(set) var callPresentInstallmentDetailCount = 0
    private(set) var callPresentDigitableLineCopiedFeedbackCount = 0
    
    private(set) var installmentDetail: InstallmentDetail?
    
    func present(installmentDetail: InstallmentDetail) {
        callPresentInstallmentDetailCount += 1
        self.installmentDetail = installmentDetail
    }
    
    func presentDigitableLineCopiedFeedback() {
        callPresentDigitableLineCopiedFeedbackCount += 1
    }
}

final class LendingDigitableLineInteractorTests: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    private lazy var installmentDetail: InstallmentDetail = .mock()
    private lazy var pasteboard = StringPasteboardProviderMock()
    private lazy var spy = LendingDigitableLinePresenterSpy()
    private lazy var dependencyContainerMock = DependencyContainerMock(analyticsSpy)
    private lazy var sut = LendingDigitableLineInteractor(presenter: spy,
                                                          installmentDetail: installmentDetail,
                                                          pasteboard: pasteboard,
                                                          dependencies: dependencyContainerMock)
    
    func testFetchInstallmentInformation_WhenCalledFromController_ShouldPresentInstallmentDetail() {
        sut.fetchInstallmentDetails()
        
        XCTAssertEqual(spy.callPresentInstallmentDetailCount, 1)
        XCTAssertEqual(spy.installmentDetail, installmentDetail)
    }
    
    func testCopyDigitableLine_WhenCalledFromController_ShouldSetDigitableLineToPasteboardAndReportCopiedFeedback() {
        let expectedEvent = LendingDigitablelLineEvent.copyDigitableLine.event()
        sut.copyDigitableLine()
        
        XCTAssertEqual(spy.callPresentDigitableLineCopiedFeedbackCount, 1)
        XCTAssertEqual(installmentDetail.digitableLine, pasteboard.string)
        XCTAssertNotNil(pasteboard.string)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
}
