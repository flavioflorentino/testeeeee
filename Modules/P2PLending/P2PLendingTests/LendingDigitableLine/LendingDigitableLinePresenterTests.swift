import XCTest
import CoreFormatter
@testable import P2PLending

final class LendingDigitableLineDisplaySpy: LendingDigitableLineDisplaying {
    private(set) var callDisplayBodyTextDigitableLineCount = 0
    private(set) var callDisplayToastCount = 0
    
    private(set) var bodyText: NSAttributedString?
    private(set) var digitableLine: String?
    private(set) var text: String?
    
    func display(bodyText: NSAttributedString, digitableLine: String) {
        callDisplayBodyTextDigitableLineCount += 1
        self.bodyText = bodyText
        self.digitableLine = digitableLine
    }
    
    func displayToast(text: String) {
        callDisplayToastCount += 1
        self.text = text
    }
}

final class LendingDigitableLinePresenterTests: XCTestCase {
    private typealias Localizable = Strings.Lending.Digitable.Line
    
    private lazy var spy = LendingDigitableLineDisplaySpy()
    private lazy var sut: LendingDigitableLinePresenter = {
        let presenter = LendingDigitableLinePresenter()
        presenter.viewController = spy
        return presenter
    }()
    
    func testPresentInstallmentDetail_WhenCalledFromInteractor_ShouldDisplayBodyTextAndDigitableLine() throws {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "pt-BR")
        dateFormatter.setLocalizedDateFormatFromTemplate("ddMMMM")
        
        let date = Date()
        let installmentDetail: InstallmentDetail = .mock(dueDate: date)
        let formattedValue = try XCTUnwrap(CurrencyFormatter.string(from: installmentDetail.value))
        let expectedBodyString = Localizable.body(dateFormatter.string(from: date), formattedValue)
        
        sut.present(installmentDetail: installmentDetail)
        
        XCTAssertEqual(spy.callDisplayBodyTextDigitableLineCount, 1)
        XCTAssertEqual(spy.bodyText?.string, expectedBodyString)
        XCTAssertEqual(spy.digitableLine, installmentDetail.digitableLine)
    }
    
    func testPresentDigitableLineCopiedFeedback_WhenCalledFromInteractor_ShouldDisplayToastWithCopiedFeedbackText() {
        sut.presentDigitableLineCopiedFeedback()
        
        XCTAssertEqual(spy.callDisplayToastCount, 1)
        XCTAssertEqual(spy.text, Localizable.digitableLineCopied)
    }
}
