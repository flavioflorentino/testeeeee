import Foundation

public protocol HasSocketManager {
    var socketManager: SocketManageable { get }
}

public protocol SocketManageable {
    func connect(_ socket: Socket)

    func publish(socketIdentifier: String,
                 content: Data,
                 additionalProperties: [String: Any],
                 completion: @escaping ((Error?) -> Void))

    func disconnect(socketIdentifier: String)
    func disconnectAll()
}

public final class SocketManager: SocketManageable {
    public static let shared = SocketManager()
    
    private init() { }
    
    var sockets = [String: Socket]()
    
    public func connect(_ socket: Socket) {
        socket.connect()
        sockets.updateValue(socket, forKey: socket.identifier)
    }
    
    public func publish(socketIdentifier: String,
                        content: Data,
                        additionalProperties: [String: Any] = [:],
                        completion: @escaping ((Error?) -> Void) = { _ in }) {
        sockets[socketIdentifier]?.publish(content: content,
                                           additionalProperties: additionalProperties,
                                           completion: completion)
    }
    
    public func disconnect(socketIdentifier: String) {
        sockets[socketIdentifier]?.disconnect()
    }
    
    public func disconnectAll() {
        sockets.forEach {
            $0.value.disconnect()
        }
    }
}
