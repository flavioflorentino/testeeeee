import Foundation

public protocol Socket {
    typealias PublishCompletion = ((Error?) -> Void)
    
    var identifier: String { get }
    
    // Connection
    func connect()
    func disconnect()
    
    // Publishing
    func publish(content: Data,
                 additionalProperties: [String: Any],
                 completion: @escaping PublishCompletion)
}
