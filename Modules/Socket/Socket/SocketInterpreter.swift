import Foundation

public protocol SocketInterpreter {
    func interpret(data: Data)
}
