import Foundation

public protocol SocketConfiguration {
    var url: URL? { get }
    var port: UInt32 { get }
    var tls: Bool { get }
    var additionalHeaders: [String: String] { get }
    
    var shouldReconnect: Bool { get }
    var clientId: Int { get }
    var connectionId: String { get }
    var userName: String { get }
    var password: String { get }
}

public extension SocketConfiguration {
    var shouldReconnect: Bool { false }
    var clientId: Int { Int() }
    var connectionId: String { String() }
    var userName: String { String() }
    var password: String { String() }
}
