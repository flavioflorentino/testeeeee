import XCTest
import UIKit

@testable import DynamicRegistration

private class RegistrationFlowControlCoordinatingSpy: RegistrationFlowControlCoordinating {
    private(set) var performActionCallsCount = 0
    private(set) var performActionReceivedInvocations: [RegistrationFlowControlAction] = []
    
    func perform(action: RegistrationFlowControlAction) {
        performActionCallsCount += 1
        performActionReceivedInvocations.append(action)
    }
}

final class NavigationControllerSpy: UINavigationController {
    private(set) var pushViewControllerCallsCount = 0
    private(set) var popViewControllerCallsCount = 0
    private(set) var dimissCallsCount = 0
    private(set) var presentCount: Int = 0

    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        super.present(viewControllerToPresent, animated: flag, completion: completion)
        presentCount += 1
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        pushViewControllerCallsCount += 1
        super.pushViewController(viewController, animated: false)
    }
    
    override func popViewController(animated: Bool) -> UIViewController? {
        popViewControllerCallsCount += 1
        return super.popViewController(animated: false)
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        dimissCallsCount += 1
        super.dismiss(animated: flag, completion: completion)
    }
}

final class RegistrationFlowControlCoordinatorTests: XCTestCase {
    private let rootNavigationControllerSpy = NavigationControllerSpy()
    private let navigationControllerSpy = NavigationControllerSpy()
    
    private lazy var sut = RegistrationFlowControlCoordinator(
        productKey: "card",
        rootNavigation: rootNavigationControllerSpy,
        navigation: navigationControllerSpy
    )
    
    func testStart_WhenFlowIsStarted_ShouldOpenSectionsList() {
        sut.start()
        XCTAssertEqual(rootNavigationControllerSpy.presentCount, 1)
        XCTAssert(navigationControllerSpy.viewControllers.last is SectionsListViewController)
    }
    
    func testPerformClose_WhenUserCloseFlow_ShouldDismissNavigation() {
        sut.perform(action: .close)
        XCTAssertEqual(navigationControllerSpy.dimissCallsCount, 1)
    }
    
    func testPerformDidOpenSection_WhenUserTapInSection_ShouldOpenFirstFormStep() {
        let formSteps: [FormStep] = [
            .init(screenKey: "screenKey 1", title: "formStep1", subtitle: "subtitle", fields: []),
            .init(screenKey: "screenKey 2", title: "formStep2", subtitle: "subtitle", fields: []),
            .init(screenKey: "screenKey 3", title: "formStep3", subtitle: "subtitle", fields: [])
        ]
        let sectionItem = SectionItemForm(sectionKey: "section 1", title: "title", status: .empty, formSteps: formSteps)
        let sectionInfo = SectionInfo(productKey: "card", section: sectionItem, productTemplateVersion: "1.1", updatedFormSteps: [:])
        sut.perform(action: .didOpenFormSection(section: sectionInfo))
        XCTAssertEqual(navigationControllerSpy.pushViewControllerCallsCount, 1)
        XCTAssert(navigationControllerSpy.viewControllers.last is FormStepViewController)
    }
    
    func testPerformDidNextFormStep_WhenUserTapInNextFormStep_ShouldOpenNextFormStep() {
        let firstFormStep = FormStep(screenKey: "screenKey 1", title: "formStep1", subtitle: "subtitle", fields: [])
        let secondFormStep = FormStep(screenKey: "screenKey 1", title: "formStep1", subtitle: "subtitle", fields: [])
        let thirdFormStep = FormStep(screenKey: "screenKey 3", title: "formStep3", subtitle: "subtitle", fields: [])
        let formSteps: [FormStep] = [firstFormStep, secondFormStep, thirdFormStep]
        let sectionItem = SectionItemForm(sectionKey: "section 1", title: "title", status: .empty, formSteps: formSteps)
        let sectionInfo = SectionInfo(productKey: "card", section: sectionItem, productTemplateVersion: "1.1", updatedFormSteps: [:])
        sut.perform(action: .didOpenFormSection(section: sectionInfo))
        XCTAssertEqual(sut.getNextFormStepFor(firstFormStep)?.screenKey, secondFormStep.screenKey)
        sut.perform(action: .didNextFormStep(formStep: firstFormStep, sectionInfo: sectionInfo))
        XCTAssertEqual(navigationControllerSpy.pushViewControllerCallsCount, 2)
        XCTAssert(navigationControllerSpy.viewControllers.last is FormStepViewController)
    }
    
    func testPerformFinish_WhenFinishRegistration_ShouldDismissNavigation() {
        sut.perform(action: .finish)
        XCTAssertEqual(navigationControllerSpy.dimissCallsCount, 1)
    }
}
