import Core
import XCTest
import UI

@testable import DynamicRegistration

private class SectionsListDisplaySpy: SectionsListDisplay {
    private(set) var renderFormHeaderFormCallsCount = 0
    private(set) var renderFormHeaderFormReceivedInvocations: [SectionsForm] = []
    private(set) var renderSectionsCallsCount = 0
    private(set) var renderSectionsReceivedInvocations: [[SectionItemForm]] = []
    private(set) var showLoadingCallsCount = 0
    private(set) var showErrorCallsCount = 0
    private(set) var showErrorReceivedInvocations: [StatefulFeedbackViewModel] = []
    private(set) var showConfirmButtonShowCallsCount = 0
    private(set) var showConfirmButtonShowReceivedInvocations: [Bool] = []
    private(set) var showPopupErrorFinishCallsCount = 0
    private(set) var showPopupErrorSubmitIntegrationCallsCount = 0
    private(set) var showPopupErrorSubmitIntegrationReceivedInvocations: [(action: SectionItemForm.Action, params: SectionItemForm.ActionParam)] = []
    
    func renderFormHeader(form: SectionsForm) {
        renderFormHeaderFormCallsCount += 1
        renderFormHeaderFormReceivedInvocations.append(form)
    }

    func renderSections(sections: [SectionItemForm]) {
        renderSectionsCallsCount += 1
        renderSectionsReceivedInvocations.append(sections)
    }
    func showLoading() {
        showLoadingCallsCount += 1
    }

    func showError(_ statefulError: StatefulFeedbackViewModel) {
        showErrorCallsCount += 1
        showErrorReceivedInvocations.append(statefulError)
    }
    
    func showConfirmButton(show: Bool) {
        showConfirmButtonShowCallsCount += 1
        showConfirmButtonShowReceivedInvocations.append(show)
    }
    
    func showPopupErrorFinish() {
        showPopupErrorFinishCallsCount += 1
    }
    
    func showPopuErrorSubmitIntegration(_ action: SectionItemForm.Action, params: SectionItemForm.ActionParam, sectionInfo: SectionInfo) {
        showPopupErrorSubmitIntegrationCallsCount += 1
        showPopupErrorSubmitIntegrationReceivedInvocations.append((action: action, params: params))
    }
}

private class SectionsListCoordinatingSpy: SectionsListCoordinating {
    var viewController: UIViewController?
    private(set) var performActionCallsCount = 0
    private(set) var performActionReceivedInvocations: [SectionsListAction] = []

    func perform(action: SectionsListAction) {
        performActionCallsCount += 1
        performActionReceivedInvocations.append(action)
    }
}

final class SectionsListPresenterTests: XCTestCase {
    private var viewControllerSpy = SectionsListDisplaySpy()
    private let coordinatorSpy = SectionsListCoordinatingSpy()
    
    private lazy var sut: SectionsListPresenter = {
        let sut = SectionsListPresenter(coordinator: coordinatorSpy)
        sut.viewController = viewControllerSpy
        return sut
    }()
    
    func testPresentSections_WhenReceiveSuccess_ShouldPresentSections() throws {
        let section = SectionItemForm(sectionKey: "sectionKey", title: "Section title", status: .complete, formSteps: [])
        let sectionForm = SectionsForm(title: "Main Title", subtitle: "Subtitle", productKey: "card", productTemplateVersion: "1.0", sections: [section])
        sut.presentFormWith(sectionsForm: sectionForm)
        XCTAssertEqual(viewControllerSpy.renderFormHeaderFormCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.renderSectionsCallsCount, 1)
        let sectionsForm = try XCTUnwrap(viewControllerSpy.renderFormHeaderFormReceivedInvocations.last)
        XCTAssertEqual(sectionsForm.title, "Main Title")
        XCTAssertEqual(sectionsForm.subtitle, "Subtitle")
        let sectionRendered = try XCTUnwrap(viewControllerSpy.renderSectionsReceivedInvocations.last?.first)
        XCTAssertEqual(sectionRendered.sectionKey, "sectionKey")
        XCTAssertEqual(sectionRendered.title, "Section title")
        XCTAssertEqual(sectionRendered.status, .complete)
    }
    
    func testShowLoading_WhenViewControllerIsLoaded_ShouldShowLoading() {
        sut.showLoading()
        XCTAssertEqual(viewControllerSpy.showLoadingCallsCount, 1)
    }
    
    func testShowError_WhenReceiveSomeError_ShouldShowError() {
        sut.showError(error: ApiError.serverError)
        XCTAssertEqual(viewControllerSpy.showErrorCallsCount, 1)
        XCTAssertTrue(viewControllerSpy.showErrorReceivedInvocations.isNotEmpty)
    }
    
    func testShowPopupErrorFinish_WhenHaveErrorDuringFinish_ShouldShowPopupError() {
        sut.showPopupErrorFinish()
        XCTAssertEqual(viewControllerSpy.showPopupErrorFinishCallsCount, 1)
    }
    
    func testShowPopuErrorSubmitIntegration_WhenHaveErrorSubmittingIntegration_ShouldShowPopupError() throws {
        let sectionItem = SectionItemForm(
            sectionKey: "sectionKey",
            title: "Biometry title 1",
            status: .empty,
            callToAction: .startBiometryModule
        )
        
        let sectionInfo = SectionInfo(
            productKey: "card",
            section: sectionItem,
            productTemplateVersion: "1.1",
            updatedFormSteps: [:]
        )
        
        sut.showPopuErrorSubmitIntegration(.startBiometryModule, params: .init(key: "key1", value: "TO_VERIFY"), sectionInfo: sectionInfo)
        XCTAssertEqual(viewControllerSpy.showPopupErrorSubmitIntegrationCallsCount, 1)
        let params = try XCTUnwrap(viewControllerSpy.showPopupErrorSubmitIntegrationReceivedInvocations.last?.params)
        XCTAssertEqual(params.key, "key1")
        XCTAssertEqual(params.value, "TO_VERIFY")
    }
}
