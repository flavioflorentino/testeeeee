import XCTest
import Core
import AnalyticsModule

@testable import DynamicRegistration

private class SectionsListPresentingSpy: SectionsListPresenting {
    var viewController: SectionsListDisplay?
    private(set) var presentFormWithSectionsFormCallsCount = 0
    private(set) var presentFormWithSectionsFormReceivedInvocations: [SectionsForm] = []
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didNextStepActionReceivedInvocations: [SectionsListAction] = []
    private(set) var showLoadingCallsCount = 0
    private(set) var showErrorErrorCallsCount = 0
    private(set) var showErrorErrorReceivedInvocations: [Error] = []
    private(set) var showConfirmButtonShowCallsCount = 0
    private(set) var showConfirmButtonShowReceivedInvocations: [Bool] = []
    private(set) var showPopupErrorFinishCallsCount = 0
    private(set) var showPopupErrorSubmitIntegrationCallsCount = 0
    private(set) var showPopupErrorSubmitIntegrationReceivedInvocations: [(action: SectionItemForm.Action, params: SectionItemForm.ActionParam)] = []

    func presentFormWith(sectionsForm: SectionsForm) {
        presentFormWithSectionsFormCallsCount += 1
        presentFormWithSectionsFormReceivedInvocations.append(sectionsForm)
    }

    func didNextStep(action: SectionsListAction) {
        didNextStepActionCallsCount += 1
        didNextStepActionReceivedInvocations.append(action)
    }
    
    func showLoading() {
        showLoadingCallsCount += 1
    }

    func showError(error: Error) {
        showErrorErrorCallsCount += 1
        showErrorErrorReceivedInvocations.append(error)
    }
    
    func showConfirmButton(show: Bool) {
        showConfirmButtonShowCallsCount += 1
        showConfirmButtonShowReceivedInvocations.append(show)
    }
    
    func showPopupErrorFinish() {
        showPopupErrorFinishCallsCount += 1
    }
    
    func showPopuErrorSubmitIntegration(_ action: SectionItemForm.Action, params: SectionItemForm.ActionParam, sectionInfo: SectionInfo) {
        showPopupErrorSubmitIntegrationCallsCount += 1
        showPopupErrorSubmitIntegrationReceivedInvocations.append((action: action, params: params))
    }
}

private final class SectionsListServicingMock: SectionsListServicing {
    func submitIntegration(_ payload: SectionFormUpdatePayload, completion: @escaping CompletionUpdateIntegrationSectionResult) {
        guard isSuccess else {
            completion(.failure(.serverError))
            return
        }
        
        completion(.success(.init(productKey: "key1", sectionKey: "sectionKey", status: .complete)))
    }
    
    var isSuccess = true

    func sectionsFormWith(productKey: String, completion: @escaping CompletionSectionsFormResult) {
        guard isSuccess else {
            completion(.failure(.serverError))
            return
        }
        let section = SectionItemForm(sectionKey: "sectionKey", title: "Section title", status: .complete, formSteps: [])
        completion(.success(.init(title: "Main Title", subtitle: "Subtitle", productKey: "card", productTemplateVersion: "1.0", sections: [section])))
    }
    
    func finish(productKey: String, completion: @escaping CompletionFinishRegistrationResult) {
        guard isSuccess else {
            completion(.failure(.serverError))
            return
        }
        
        completion(.success(NoContent()))
    }
}

final class SectionsListInteractorTests: XCTestCase {
    private var analyticsSpy = AnalyticsSpy()
    private var presenterSpy = SectionsListPresentingSpy()
    private let serviceMock = SectionsListServicingMock()
    
    private lazy var sut = SectionsListInteractor(
        productKey: "origin",
        service: serviceMock,
        presenter: presenterSpy,
        dependencies: DependencyContainerMock(analyticsSpy)
    )
    
    func testLoadSectionsForm_WhenReceiveSuccess_ShouldPresentSections() {
        serviceMock.isSuccess = true
        sut.loadSectionsForm()
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentFormWithSectionsFormCallsCount, 1)
        XCTAssertEqual(presenterSpy.showConfirmButtonShowCallsCount, 1)
        XCTAssertEqual(presenterSpy.showErrorErrorCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentFormWithSectionsFormReceivedInvocations.last?.title, "Main Title")
    }
    
    func testLoadSectionsForm_WhenReceiveError_ShouldShowError() {
        serviceMock.isSuccess = false
        sut.loadSectionsForm()
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentFormWithSectionsFormCallsCount, 0)
        XCTAssertEqual(presenterSpy.showConfirmButtonShowCallsCount, 1)
        XCTAssertEqual(presenterSpy.showErrorErrorCallsCount, 1)
        XCTAssertTrue(presenterSpy.presentFormWithSectionsFormReceivedInvocations.isEmpty)
    }

    func testDidOpenSection_ShouldLogOpenSectionEvent() {
        sut.loadSectionsForm()
        let section = SectionItemForm(sectionKey: "sectionKey", title: "Section title", status: .complete, formSteps: [])
        let expectedEvent = COPEvent.openFormSection(productKey: "origin", sectionKey: section.sectionKey)
        sut.didOpenSection(section: section)

        XCTAssertEqual(analyticsSpy.logCalledCount, 2)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent.event()))
    }

    func testDidClose_ShouldLogCloseSectionEvent() {
        let expectedEvent = COPEvent.closed(productKey: "origin")
        sut.didClose()

        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent.event()))
    }
    
    func testFinishRegistration_WhenReceiveSuccess_ShouldCallNextStep() {
        serviceMock.isSuccess = true
        sut.didFinish()
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
    }
    
    func testFinishRegistration_WhenReceiveError_ShouldShowPopupError() {
        serviceMock.isSuccess = false
        sut.didFinish()
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.showPopupErrorFinishCallsCount, 1)
    }
    
    func testHasPendingSections_WhenHasPendingSection_ShouldReturnTrue() {
        let sectionPending = SectionItemForm(sectionKey: "key1", title: "Section title 1", status: .empty, formSteps: [])
        let sectionNotPending = SectionItemForm(sectionKey: "key2", title: "Section title 2", status: .complete, formSteps: [])
        let hasPendingSections = sut.hasPendingSections([sectionPending, sectionNotPending])
        XCTAssertTrue(hasPendingSections)
    }
    
    func testHasPendingSections_WhenHasNotPendingSection_ShouldReturnFalse() {
        let sectionNotPending1 = SectionItemForm(sectionKey: "key1", title: "Section title 1", status: .complete, formSteps: [])
        let sectionNotPending2 = SectionItemForm(sectionKey: "key2", title: "Section title 2", status: .complete, formSteps: [])
        let hasPendingSections = sut.hasPendingSections([sectionNotPending1, sectionNotPending2])
        XCTAssertFalse(hasPendingSections)
    }
    
    func testNeedToShowConfirmButton_WhenHasPendingSection_ShouldNotShowConfirmButton() {
        let sectionPending = SectionItemForm(sectionKey: "key1", title: "Section title 1", status: .empty, formSteps: [])
        let sectionNotPending = SectionItemForm(sectionKey: "key2", title: "Section title 2", status: .complete, formSteps: [])
        let needToShowConfirmButton = sut.needToShowConfirmButton([sectionPending, sectionNotPending])
        XCTAssertFalse(needToShowConfirmButton)
    }
    
    func testNeedToShowConfirmButton_WhenHasNotPendingSection_ShouldShowConfirmButton() {
        let sectionNotPending1 = SectionItemForm(sectionKey: "key1", title: "Section title 1", status: .complete, formSteps: [])
        let sectionNotPending2 = SectionItemForm(sectionKey: "key2", title: "Section title 2", status: .complete, formSteps: [])
        let needToShowConfirmButton = sut.needToShowConfirmButton([sectionNotPending1, sectionNotPending2])
        XCTAssertTrue(needToShowConfirmButton)
    }
    
    func testSubmitIntegration_WhenUserStartedBiometryWithSuccess_ShouldPresenterFormUpdated() throws {
        serviceMock.isSuccess = true
        sut.loadSectionsForm()
        let expectedEvent = COPEvent.submitIntegrationResult(productKey: "card",
                                                             sectionKey: "sectionKey",
                                                             screenKey: "BIOMETRY_MODULE",
                                                             status: .success)
        
        let sectionItem = SectionItemForm(
            sectionKey: "sectionKey",
            title: "Biometry title 1",
            status: .empty,
            callToAction: .startBiometryModule
        )
        
        let sectionInfo = SectionInfo(
            productKey: "card",
            section: sectionItem,
            productTemplateVersion: "1.1",
            updatedFormSteps: [:]
        )
        
        sut.submitIntegrationResult(
            .startBiometryModule,
            params: .init(key: "bio key1", value: "TO_VERIFY"),
            sectionInfo: sectionInfo
        )
        
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.presentFormWithSectionsFormCallsCount, 2)
        XCTAssertEqual(presenterSpy.showConfirmButtonShowCallsCount, 2)
        XCTAssertEqual(analyticsSpy.logCalledCount, 2)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent.event()))
    }
    
    func testSubmitIntegration_WhenUserStartedBiometryWithFailure_ShouldPresenterFormUpdated() throws {
        serviceMock.isSuccess = false
        let expectedEvent = COPEvent.submitIntegrationResult(productKey: "card",
                                                             sectionKey: "sectionKey",
                                                             screenKey: "BIOMETRY_MODULE",
                                                             status: .error)
        
        let sectionItem = SectionItemForm(
            sectionKey: "sectionKey",
            title: "Biometry title 1",
            status: .empty,
            callToAction: .startBiometryModule
        )
        
        let sectionInfo = SectionInfo(
            productKey: "card",
            section: sectionItem,
            productTemplateVersion: "1.1",
            updatedFormSteps: [:]
        )
        
        sut.submitIntegrationResult(
            .startBiometryModule,
            params: .init(key: "bio key1", value: "TO_VERIFY"),
            sectionInfo: sectionInfo
        )
        
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.showPopupErrorSubmitIntegrationCallsCount, 1)
        let params = try XCTUnwrap(presenterSpy.showPopupErrorSubmitIntegrationReceivedInvocations.last?.params)
        XCTAssertEqual(params.key, "bio key1")
        XCTAssertEqual(params.value, "TO_VERIFY")
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent.event()))
    }
}

extension SectionItemForm {
    init(sectionKey: String, title: String, status: SectionStatus, formSteps: [FormStep] = [], callToAction: Action? = nil, actionParam: ActionParam? = nil) {
        self.init(sectionKey: sectionKey, title: title, sectionIcon: .documents, status: status, formSteps: formSteps, actionParam: actionParam, callToAction: callToAction)
    }
}
