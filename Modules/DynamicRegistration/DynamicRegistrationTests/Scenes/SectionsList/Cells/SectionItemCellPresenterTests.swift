import XCTest
import UIKit
import UI
@testable import DynamicRegistration

private class SectionItemDisplaySpy: SectionItemDisplay {
    private(set) var updateTitleTitleCallsCount = 0
    private(set) var updateTitleTitleReceivedInvocations: [String] = []
    private(set) var updateContainerBorderColorColorCallsCount = 0
    private(set) var updateContainerBorderColorColorReceivedInvocations: [CGColor] = []
    private(set) var updateContainerBackgroundColorColorCallsCount = 0
    private(set) var updateContainerBackgroundColorColorReceivedInvocations: [UIColor] = []
    private(set) var updateStatusIconImageCallsCount = 0
    private(set) var updateStatusIconImageReceivedInvocations: [UIImage?] = []
    private(set) var updateTitleColorColorCallsCount = 0
    private(set) var updateTitleColorColorReceivedInvocations: [UIColor] = []
    private(set) var updateSectionImageColorColorCallsCount = 0
    private(set) var updateSectionImageColorColorReceivedInvocations: [UIColor] = []
    private(set) var updateSectionImageImageCallsCount = 0
    
    func updateTitle(title: String) {
        updateTitleTitleCallsCount += 1
        updateTitleTitleReceivedInvocations.append(title)
    }

    func updateContainerBorderColor(color: CGColor) {
        updateContainerBorderColorColorCallsCount += 1
        updateContainerBorderColorColorReceivedInvocations.append(color)
    }

    func updateContainerBackgroundColor(color: UIColor) {
        updateContainerBackgroundColorColorCallsCount += 1
        updateContainerBackgroundColorColorReceivedInvocations.append(color)
    }

    func updateStatusIcon(image: UIImage?) {
        updateStatusIconImageCallsCount += 1
        updateStatusIconImageReceivedInvocations.append(image)
    }

    func updateTitleColor(color: UIColor) {
        updateTitleColorColorCallsCount += 1
        updateTitleColorColorReceivedInvocations.append(color)
    }

    func updateSectionImageColor(color: UIColor) {
        updateSectionImageColorColorCallsCount += 1
        updateSectionImageColorColorReceivedInvocations.append(color)
    }
    
    func updateSectionImage(image: UIImage?) {
        updateSectionImageImageCallsCount += 1
    }
}

final class SectionItemCellPresenterTests: XCTestCase {
    private var viewSpy = SectionItemDisplaySpy()
    
    private lazy var sut: SectionItemCellPresenter = {
        let sut = SectionItemCellPresenter()
        sut.view = viewSpy
        return sut
    }()

    func testConfigureWithSectionItem_WhenCellIsLocked_ShouldConfigureViewProperly() throws {
        let sectionItemEmpty1 = SectionItemForm(sectionKey: "sectionKey 1", title: "Section title 1", status: .empty, formSteps: [])
        let sectionItemEmpty2 = SectionItemForm(sectionKey: "sectionKey 2", title: "Section title 2", status: .empty, formSteps: [])
        let sections = [sectionItemEmpty1, sectionItemEmpty2]
        
        sut.configureWith(sectionItemEmpty2, sectionIndex: 1, sections: sections)
        
        XCTAssertEqual(viewSpy.updateSectionImageColorColorCallsCount, 1)
        XCTAssertEqual(viewSpy.updateStatusIconImageCallsCount, 1)
        let image = try XCTUnwrap(viewSpy.updateStatusIconImageReceivedInvocations.last)
        XCTAssertNil(image)
        XCTAssertEqual(viewSpy.updateTitleTitleCallsCount, 1)
        XCTAssertEqual(viewSpy.updateTitleTitleCallsCount, 1)
        XCTAssertEqual(viewSpy.updateContainerBackgroundColorColorCallsCount, 1)
        XCTAssertEqual(viewSpy.updateContainerBorderColorColorCallsCount, 1)
    }
    
    func testConfigureWithSectionItem_WhenCellIsNotLocked_ShouldConfigureViewProperly() throws {
        let sectionItemEmpty1 = SectionItemForm(sectionKey: "sectionKey 1", title: "Section title 1", status: .empty, formSteps: [])
        let sectionItemEmpty2 = SectionItemForm(sectionKey: "sectionKey 2", title: "Section title 2", status: .empty, formSteps: [])
        let sections = [sectionItemEmpty1, sectionItemEmpty2]
        
        sut.configureWith(sectionItemEmpty1, sectionIndex: 0, sections: sections)
        
        XCTAssertEqual(viewSpy.updateSectionImageColorColorCallsCount, 1)
        XCTAssertEqual(viewSpy.updateStatusIconImageCallsCount, 1)
        let image = try XCTUnwrap(viewSpy.updateStatusIconImageReceivedInvocations.last)
        XCTAssertNotNil(image)
        XCTAssertEqual(viewSpy.updateTitleTitleCallsCount, 1)
        XCTAssertEqual(viewSpy.updateSectionImageImageCallsCount, 1)
        XCTAssertEqual(viewSpy.updateContainerBackgroundColorColorCallsCount, 1)
        XCTAssertEqual(viewSpy.updateContainerBorderColorColorCallsCount, 1)
    }
    
    func testGetCurrentPendingIndex_WhenAllSectionsIsEmpty_ShouldReturnFirst() {
        let sectionItemEmpty1 = SectionItemForm(sectionKey: "sectionKey 1", title: "Section title 1", status: .empty, formSteps: [])
        let sectionItemEmpty2 = SectionItemForm(sectionKey: "sectionKey 2", title: "Section title 2", status: .empty, formSteps: [])
        let sections = [sectionItemEmpty1, sectionItemEmpty2]
        let currentPendingIndex = sut.getCurrentPendingIndexFor(sections)
        XCTAssertEqual(currentPendingIndex, 0)
    }
    
    func testGetCurrentPendingIndex_WhenAllSectionsIsCompleted_ShouldReturnNil() {
        let sectionItemEmpty1 = SectionItemForm(sectionKey: "sectionKey 1", title: "Section title 1", status: .complete, formSteps: [])
        let sectionItemEmpty2 = SectionItemForm(sectionKey: "sectionKey 2", title: "Section title 2", status: .complete, formSteps: [])
        let sections = [sectionItemEmpty1, sectionItemEmpty2]
        let currentPendingIndex = sut.getCurrentPendingIndexFor(sections)
        XCTAssertNil(currentPendingIndex)
    }
    
    func testGetCurrentPendingIndex_WhenLastSectionIsEmpty_ShouldReturnLast() {
        let sectionItemEmpty1 = SectionItemForm(sectionKey: "sectionKey 1", title: "Section title 1", status: .complete, formSteps: [])
        let sectionItemEmpty2 = SectionItemForm(sectionKey: "sectionKey 2", title: "Section title 2", status: .complete, formSteps: [])
        let sectionItemEmpty3 = SectionItemForm(sectionKey: "sectionKey 3", title: "Section title 2", status: .empty, formSteps: [])
        let sections = [sectionItemEmpty1, sectionItemEmpty2, sectionItemEmpty3]
        let currentPendingIndex = sut.getCurrentPendingIndexFor(sections)
        XCTAssertEqual(currentPendingIndex, 2)
    }
    
    func testGetCurrentPendingIndex_WhenHasSomeErrorSection_ShouldFirstPenging() {
        let sectionItemEmpty1 = SectionItemForm(sectionKey: "sectionKey 1", title: "Section title 1", status: .complete, formSteps: [])
        let sectionItemEmpty2 = SectionItemForm(sectionKey: "sectionKey 2", title: "Section title 2", status: .warning, formSteps: [])
        let sectionItemEmpty3 = SectionItemForm(sectionKey: "sectionKey 3", title: "Section title 2", status: .complete, formSteps: [])
        let sections = [sectionItemEmpty1, sectionItemEmpty2, sectionItemEmpty3]
        let currentPendingIndex = sut.getCurrentPendingIndexFor(sections)
        XCTAssertEqual(currentPendingIndex, 1)
    }
    
    func testIsLocked_WhenAllIsEmpty_ShouldReturnFirstAsNotLocked() {
        let sectionItemEmpty1 = SectionItemForm(sectionKey: "sectionKey 1", title: "Section title 1", status: .empty, formSteps: [])
        let sectionItemEmpty2 = SectionItemForm(sectionKey: "sectionKey 2", title: "Section title 2", status: .empty, formSteps: [])
        let sectionItemEmpty3 = SectionItemForm(sectionKey: "sectionKey 3", title: "Section title 2", status: .empty, formSteps: [])
        let sections = [sectionItemEmpty1, sectionItemEmpty2, sectionItemEmpty3]
        let firstIndexIsLocked = sut.isLocked(index: 0, sections: sections)
        XCTAssertFalse(firstIndexIsLocked)
        let secondIndexIsLocked = sut.isLocked(index: 1, sections: sections)
        XCTAssertTrue(secondIndexIsLocked)
        let thirdIndexIsLocked = sut.isLocked(index: 2, sections: sections)
        XCTAssertTrue(thirdIndexIsLocked)
    }
    
    func testIsLocked_WhenAllIsSuccess_ShouldAllAsNotLocked() {
        let sectionItemEmpty1 = SectionItemForm(sectionKey: "sectionKey 1", title: "Section title 1", status: .complete, formSteps: [])
        let sectionItemEmpty2 = SectionItemForm(sectionKey: "sectionKey 2", title: "Section title 2", status: .complete, formSteps: [])
        let sectionItemEmpty3 = SectionItemForm(sectionKey: "sectionKey 3", title: "Section title 2", status: .complete, formSteps: [])
        let sections = [sectionItemEmpty1, sectionItemEmpty2, sectionItemEmpty3]
        let firstIndexIsLocked = sut.isLocked(index: 0, sections: sections)
        XCTAssertFalse(firstIndexIsLocked)
        let secondIndexIsLocked = sut.isLocked(index: 1, sections: sections)
        XCTAssertFalse(secondIndexIsLocked)
        let thirdIndexIsLocked = sut.isLocked(index: 2, sections: sections)
        XCTAssertFalse(thirdIndexIsLocked)
    }
    
    func testIsLocked_WhenSecondIsEmpty_ShouldReturnOnlyNextAsLocked() {
        let sectionItemEmpty1 = SectionItemForm(sectionKey: "sectionKey 1", title: "Section title 1", status: .complete, formSteps: [])
        let sectionItemEmpty2 = SectionItemForm(sectionKey: "sectionKey 2", title: "Section title 2", status: .empty, formSteps: [])
        let sectionItemEmpty3 = SectionItemForm(sectionKey: "sectionKey 3", title: "Section title 2", status: .empty, formSteps: [])
        let sections = [sectionItemEmpty1, sectionItemEmpty2, sectionItemEmpty3]
        let firstIndexIsLocked = sut.isLocked(index: 0, sections: sections)
        XCTAssertFalse(firstIndexIsLocked)
        let secondIndexIsLocked = sut.isLocked(index: 1, sections: sections)
        XCTAssertFalse(secondIndexIsLocked)
        let thirdIndexIsLocked = sut.isLocked(index: 2, sections: sections)
        XCTAssertTrue(thirdIndexIsLocked)
    }
}
