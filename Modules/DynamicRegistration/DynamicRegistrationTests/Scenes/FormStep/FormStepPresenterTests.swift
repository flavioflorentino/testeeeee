import XCTest
import UI
import Core

@testable import DynamicRegistration

private class FormStepDisplaySpy: FormStepDisplay {
    private(set) var updateSectionTitleSectionTitleCallsCount = 0
    private(set) var updateSectionTitleSectionTitleReceivedInvocations: [String] = []
    private(set) var updateTitleTitleCallsCount = 0
    private(set) var updateTitleTitleReceivedInvocations: [String] = []
    private(set) var updateSubtitleSubtitleCallsCount = 0
    private(set) var updateSubtitleSubtitleReceivedInvocations: [String] = []
    private(set) var updateProgressValueCallsCount = 0
    private(set) var updateProgressValueReceivedInvocations: [Float] = []
    private(set) var updateTotalStepsStepsCallsCount = 0
    private(set) var updateTotalStepsStepsReceivedInvocations: [String] = []
    private(set) var renderFieldViewsFieldViewsCallsCount = 0
    private(set) var renderFieldViewsFieldViewsReceivedInvocations: [[FieldView]] = []
    private(set) var enableConfirmButtonCallsCount = 0
    private(set) var enableConfirmButtonReceivedInvocations: [Bool] = []
    private(set) var setupConfirmButtonTitleCallsCount = 0
    private(set) var setupConfirmButtonTitleReceivedInvocations: [String] = []
    private(set) var showLoadingCallsCount = 0
    private(set) var hideKeyboardCallsCount = 0
    private(set) var showPopupErrorCallsCount = 0
    private(set) var didTapFieldReceivedInvocations: [FieldForm] = []
    private(set) var didUpdateFieldReceivedInvocations: [FieldForm] = []

    func updateSectionTitle(sectionTitle: String) {
        updateSectionTitleSectionTitleCallsCount += 1
        updateSectionTitleSectionTitleReceivedInvocations.append(sectionTitle)
    }

    func updateTitle(title: String) {
        updateTitleTitleCallsCount += 1
        updateTitleTitleReceivedInvocations.append(title)
    }

    func updateSubtitle(subtitle: String) {
        updateSubtitleSubtitleCallsCount += 1
        updateSubtitleSubtitleReceivedInvocations.append(subtitle)
    }

    func updateProgress(value: Float) {
        updateProgressValueCallsCount += 1
        updateProgressValueReceivedInvocations.append(value)
    }

    func updateTotalSteps(steps: String) {
        updateTotalStepsStepsCallsCount += 1
        updateTotalStepsStepsReceivedInvocations.append(steps)
    }

    func renderFieldViews(fieldViews: [FieldView]) {
        renderFieldViewsFieldViewsCallsCount += 1
        renderFieldViewsFieldViewsReceivedInvocations.append(fieldViews)
    }

    func enableConfirmButton(_ enable: Bool) {
        enableConfirmButtonCallsCount += 1
        enableConfirmButtonReceivedInvocations.append(enable)
    }
    
    func setupConfirmButtonTitle(_ title: String) {
        setupConfirmButtonTitleCallsCount += 1
        setupConfirmButtonTitleReceivedInvocations.append(title)
    }

    func showLoading() {
        showLoadingCallsCount += 1
    }

    func hideKeyboard() {
        hideKeyboardCallsCount += 1
    }
    
    func showPopupError() {
        showPopupErrorCallsCount += 1
    }

    func didTapField(field: FieldForm, screenKey: String) {
        didTapFieldReceivedInvocations.append(field)
    }
    
    func didUpdateField(_ fieldForm: FieldForm) {
        didUpdateFieldReceivedInvocations.append(fieldForm)
    }
}

private class FormStepCoordinatingSpy: FormStepCoordinating {
    var viewController: UIViewController?
    private(set) var performActionCallsCount = 0
    private(set) var performActionReceivedInvocations: [FormStepAction] = []

    func perform(action: FormStepAction) {
        performActionCallsCount += 1
        performActionReceivedInvocations.append(action)
    }
}

final class FormStepPresenterTests: XCTestCase {
    private var viewControllerSpy = FormStepDisplaySpy()
    private let coordinatorSpy = FormStepCoordinatingSpy()
    
    private lazy var sut: FormStepPresenter = {
        let sut = FormStepPresenter(coordinator: coordinatorSpy)
        sut.viewController = viewControllerSpy
        return sut
    }()
    
    func testShowLoading_WhenIsLoadingData_ShouldDisplayLoadingAnimation() {
        sut.showLoading()
        XCTAssertEqual(viewControllerSpy.showLoadingCallsCount, 1)
    }
    
    func testHideKeyboard_WhenUserGoToNextForm_ShouldDismissKeyboard() {
        sut.hideKeyboard()
        XCTAssertEqual(viewControllerSpy.hideKeyboardCallsCount, 1)
    }
    
    func testDidTapField_WhenCalledFromInteractor_ShouldCallViewController() {
        let fieldForm = FieldForm(
            fieldKey: "stateFieldKey",
            fieldType: .singleSelection,
            placeholder: "",
            value: .init(value: "initial value", id: 1),
            validators: [],
            dependencies: []
        )
        sut.didTapField(field: fieldForm, screenKey: "screen 1")
        XCTAssertEqual(viewControllerSpy.didTapFieldReceivedInvocations.count, 1)
        XCTAssertEqual(viewControllerSpy.didTapFieldReceivedInvocations.last?.fieldKey, "stateFieldKey")
    }
    
    func testDidUpdateField_WhenCalledFromInteractor_ShouldCallViewController() {
        let fieldForm = FieldForm(
            fieldKey: "stateFieldKey",
            fieldType: .singleSelection,
            placeholder: "",
            value: .init(value: "initial value", id: 1),
            validators: [],
            dependencies: []
        )
        sut.didUpdateField(fieldForm)
        XCTAssertEqual(viewControllerSpy.didUpdateFieldReceivedInvocations.count, 1)
        XCTAssertEqual(viewControllerSpy.didUpdateFieldReceivedInvocations.last?.fieldKey, "stateFieldKey")
    }
    
    func testDidNextStep_WhenUserGoToNextForm_ShouldCallCoordinator() {
        let formStep = FormStep(screenKey: "", title: "", subtitle: "", fields: [])
        let sectionItem = SectionItemForm(sectionKey: "section 1", title: "title", status: .empty, formSteps: [formStep])
        let sectionInfo = SectionInfo(productKey: "card", section: sectionItem, productTemplateVersion: "1.1", updatedFormSteps: [:])
        sut.didNextStep(action: .nextFormStep(formStep: formStep, sectionInfo: sectionInfo))
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
    }
    
    func testDidNextStep_WhenUserFinishSection_ShouldCallCoordinator() {
        let formStep = FormStep(screenKey: "", title: "", subtitle: "", fields: [])
        let sectionItem = SectionItemForm(sectionKey: "section 1", title: "title", status: .empty, formSteps: [formStep])
        let sectionInfo = SectionInfo(productKey: "card", section: sectionItem, productTemplateVersion: "1.1", updatedFormSteps: [:])
        sut.didNextStep(action: .updateSection(SectionFormUpdate(productKey: "", sectionKey: "", status: .complete), sectionInfo: sectionInfo))
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
    }
    
    func testShowError_WhenReceiveServerError_ShouldDisplayError() {
        sut.showPopupError()
        XCTAssertEqual(viewControllerSpy.showPopupErrorCallsCount, 1)
    }
    
    func testUpdateFormIsValid_WhenIsValid_ShouldEnableConfirmButton() throws {
        sut.updateFormIsValid(true)
        XCTAssertEqual(viewControllerSpy.enableConfirmButtonCallsCount, 1)
        let enableConfirmButton = try XCTUnwrap(viewControllerSpy.enableConfirmButtonReceivedInvocations.last)
        XCTAssertTrue(enableConfirmButton)
    }
    
    func testUpdateFormIsValid_WhenIsInvalid_ShouldDisableConfirmButton() throws {
        sut.updateFormIsValid(false)
        XCTAssertEqual(viewControllerSpy.enableConfirmButtonCallsCount, 1)
        let enableConfirmButton = try XCTUnwrap(viewControllerSpy.enableConfirmButtonReceivedInvocations.last)
        XCTAssertFalse(enableConfirmButton)
    }
    
    func testDisplayFormStep_WhenViewIsLoaded_ShouldDisplayContent() {
        let firstFormStep = FormStep(
            screenKey: "screenKey 1",
            title: "Form Title 1",
            subtitle: "",
            fields: [.init(fieldKey: "fieldKey", fieldType: .text, placeholder: "", value: nil, validators: [], dependencies: [])]
        )
        let secondFormStep = FormStep(screenKey: "screenKey 2", title: "Form Title 2", subtitle: "", fields: [])
        let sectionItem = SectionItemForm(sectionKey: "Section Title", title: "title", status: .empty, formSteps: [firstFormStep, secondFormStep])
        sut.displayFormStep(formStep: firstFormStep,
                            fields: firstFormStep.fields,
                            section: sectionItem,
                            productKey: "ProductKey",
                            needUpdateSection: false)
        XCTAssertEqual(viewControllerSpy.updateTitleTitleCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.updateTitleTitleReceivedInvocations.last, "Form Title 1")
        XCTAssertEqual(sut.getCurrentStepPosition(formStep: firstFormStep, section: sectionItem), 1)
        XCTAssertEqual(viewControllerSpy.updateTotalStepsStepsCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.updateTotalStepsStepsReceivedInvocations.last, "1/2")
        XCTAssertEqual(viewControllerSpy.updateProgressValueCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.updateProgressValueReceivedInvocations.last, 0.5)
        XCTAssertEqual(viewControllerSpy.renderFieldViewsFieldViewsCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.renderFieldViewsFieldViewsReceivedInvocations.last?.count, 1)
        XCTAssertEqual(viewControllerSpy.setupConfirmButtonTitleCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.setupConfirmButtonTitleReceivedInvocations.last, "Continuar")
    }
}
