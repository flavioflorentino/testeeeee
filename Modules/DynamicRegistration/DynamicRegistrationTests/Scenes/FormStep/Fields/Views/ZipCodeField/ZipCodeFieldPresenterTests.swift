import XCTest
@testable import DynamicRegistration

private class ZipCodeFieldDisplayingSpy: ZipCodeFieldDisplaying {
    private(set) var showLoadingShowCallsCount = 0
    private(set) var showLoadingShowReceivedInvocations: [Bool] = []
    private(set) var enableFieldCallsCount = 0
    private(set) var enableFieldReceivedInvocations: [Bool] = []
    private(set) var updatePayloadCallsCount = 0
    private(set) var updatePayloadReceivedInvocations: [[FieldPayload]] = []
    private(set) var showAlertErrorTitleDescriptionActionImageCallsCount = 0
    // swiftlint:disable:next large_tuple
    private(set) var showAlertErrorTitleDescriptionActionImageReceivedInvocations: [(title: String, description: String, action: String, image: UIImage)] = []
    
    func showLoading(show: Bool) {
        showLoadingShowCallsCount += 1
        showLoadingShowReceivedInvocations.append(show)
    }
    
    func enableField(_ enable: Bool) {
        enableFieldCallsCount += 1
        enableFieldReceivedInvocations.append(enable)
    }
    
    func updatePayload(_ payload: [FieldPayload]) {
        updatePayloadCallsCount += 1
        updatePayloadReceivedInvocations.append(payload)
    }
    
    func showAlertError(title: String, description: String, action: String, image: UIImage) {
        showAlertErrorTitleDescriptionActionImageCallsCount += 1
        showAlertErrorTitleDescriptionActionImageReceivedInvocations.append((title: title, description: description, action: action, image: image))
    }
}

final class ZipCodeFieldPresenterTests: XCTestCase {
    private var viewSpy = ZipCodeFieldDisplayingSpy()
    
    private lazy var sut: ZipCodeFieldPresenter = {
        let sut = ZipCodeFieldPresenter()
        sut.view = viewSpy
        return sut
    }()
    
    func testEnableField_WhenIsCalledFromInteractor_ShouldUpdateViewProperly() throws {
        sut.enableField(true)
        XCTAssertEqual(viewSpy.enableFieldCallsCount, 1)
        var result = try XCTUnwrap(viewSpy.enableFieldReceivedInvocations.last)
        XCTAssertTrue(result)
        sut.enableField(false)
        XCTAssertEqual(viewSpy.enableFieldCallsCount, 2)
        result = try XCTUnwrap(viewSpy.enableFieldReceivedInvocations.last)
        XCTAssertFalse(result)
    }
    
    func testHideLoading_WhenIsCalledFromInteractor_ShouldUpdateViewProperly() throws {
        sut.hideLoading()
        XCTAssertEqual(viewSpy.showLoadingShowCallsCount, 1)
        let result = try XCTUnwrap(viewSpy.showLoadingShowReceivedInvocations.last)
        XCTAssertFalse(result)
    }
    
    func testShowLoading_WhenIsCalledFromInteractor_ShouldUpdateViewProperly() throws {
        sut.showLoading()
        XCTAssertEqual(viewSpy.showLoadingShowCallsCount, 1)
        let result = try XCTUnwrap(viewSpy.showLoadingShowReceivedInvocations.last)
        XCTAssertTrue(result)
    }
    
    func testShowZipCodeError_WhenIsCalledFromInteractor_ShouldUpdateViewProperly() throws {
        sut.showZipCodeError()
        XCTAssertEqual(viewSpy.showAlertErrorTitleDescriptionActionImageCallsCount, 1)
        let result = try XCTUnwrap(viewSpy.showAlertErrorTitleDescriptionActionImageReceivedInvocations.last)
        XCTAssertEqual(result.title, "Não encontramos o CEP")
        XCTAssertEqual(result.description, "O CEP informado consta como inexistente na nossa consulta.")
        XCTAssertEqual(result.action, "Ok, entendi")
    }
    
    func testUpdatePayload_WhenIsCalledFromInteractor_ShouldUpdateViewProperly() throws {
        let payload: [FieldPayload] = [
            .init(fieldKey: "STREET_KEY", value: .init(value: "streetName")),
            .init(fieldKey: "NEIGHBORHOOD_KEY", value: .init(value: "neighborhood")),
            .init(fieldKey: "CITY_KEY", value: .init(value: "city", id: 20)),
            .init(fieldKey: "STATE_KEY", value: .init(value: "stateName", id: 10))
        ]
        sut.updatePayload(payload)
        XCTAssertEqual(viewSpy.updatePayloadCallsCount, 1)
        let result = try XCTUnwrap(viewSpy.updatePayloadReceivedInvocations.last)
        XCTAssertEqual(result.count, 4)
        XCTAssertEqual(result.first?.fieldKey, "STREET_KEY")
        XCTAssertEqual(result.first?.value?.name?.value as? String, "streetName")
    }
}
