import XCTest
import Core
import AnalyticsModule

@testable import DynamicRegistration

private class ZipCodeFieldServicingMock: ZipCodeFieldServicing {
    var isSuccess = false
    
    func addressBy(_ zipCode: String, then completion: @escaping (Swift.Result<Address, ApiError>) -> Void) -> URLSessionTask? {
        guard isSuccess else {
            completion(.failure(.serverError))
            return nil
        }
        
        completion(
            .success(
                .init(
                    zipCode: "zipCode",
                    addressComplement: "addressComplement",
                    city: "cityName",
                    cityId: "cityId",
                    neighborhood: "neighborhoodName",
                    neighborhoodId: "neighborhoodId",
                    state: "stateName",
                    stateId: "stateId",
                    stateName: "stateName",
                    streetId: "streetId",
                    streetName: "streetName",
                    streetType: "streetType",
                    street: "streetName"
                )
            )
        )
        return nil
    }
}

private class ZipCodeFieldPresentingSpy: ZipCodeFieldPresenting {
    var view: ZipCodeFieldDisplaying?
    
    private(set) var showLoadingCallsCount = 0
    private(set) var hideLoadingCallsCount = 0
    private(set) var enableFieldCallsCount = 0
    private(set) var enableFieldReceivedInvocations: [Bool] = []
    private(set) var updatePayloadCallsCount = 0
    private(set) var updatePayloadReceivedInvocations: [[FieldPayload]] = []
    private(set) var showZipCodeErrorCallsCount = 0
    
    func showLoading() {
        showLoadingCallsCount += 1
    }
    
    func hideLoading() {
        hideLoadingCallsCount += 1
    }
    
    func enableField(_ enable: Bool) {
        enableFieldCallsCount += 1
        enableFieldReceivedInvocations.append(enable)
    }
    
    func updatePayload(_ payload: [FieldPayload]) {
        updatePayloadCallsCount += 1
        updatePayloadReceivedInvocations.append(payload)
    }
    
    func showZipCodeError() {
        showZipCodeErrorCallsCount += 1
    }
}

final class ZipCodeFieldInteractorTests: XCTestCase {
    private var analyticsSpy = AnalyticsSpy()
    private var presenterSpy = ZipCodeFieldPresentingSpy()
    private let serviceMock = ZipCodeFieldServicingMock()
    private let analyticsKeys = FieldAnalyticsKeys(productKey: "COP_123", sectionKey: "Section_1", screenKey: "Screen_1")
    private let fieldForm = FieldForm(
        fieldKey: "stateFieldKey",
        fieldType: .singleSelection,
        placeholder: "",
        value: .init(value: "initial value", id: 1),
        validators: [],
        dependencies: []
    )
    private lazy var sut = ZipCodeFieldInteractor(
        analyticsKeys: analyticsKeys,
        field: fieldForm,
        dependencies: DependencyContainerMock(analyticsSpy),
        service: serviceMock,
        presenter: presenterSpy
    )
    
    func testSetupInitial_WhenSetupInitialWithEmptyValue_ShouldDisableField() throws {
        sut.setupInitial(nil)
        XCTAssertEqual(presenterSpy.enableFieldCallsCount, 1)
        let result = try XCTUnwrap(presenterSpy.enableFieldReceivedInvocations.last)
        XCTAssertFalse(result)
    }
    
    func testSetupInitial_WhenSetupInitialWithIncompleteValue_ShouldDisableField() throws {
        sut.setupInitial("0000000")
        XCTAssertEqual(presenterSpy.enableFieldCallsCount, 1)
        let result = try XCTUnwrap(presenterSpy.enableFieldReceivedInvocations.last)
        XCTAssertFalse(result)
    }
    
    func testSetupInitial_WhenSetupInitialWithCompleteValue_ShouldEnableField() throws {
        sut.setupInitial("00000000")
        XCTAssertEqual(presenterSpy.enableFieldCallsCount, 1)
        let result = try XCTUnwrap(presenterSpy.enableFieldReceivedInvocations.last)
        XCTAssertTrue(result)
    }
    
    func testValidateZipCodeFormat_WhenIsCalledFromSetupInitial_ShouldValidateZipCodeProperly() throws {
        XCTAssertNil(sut.validateZipCodeFormat(nil))
        XCTAssertNil(sut.validateZipCodeFormat("0"))
        XCTAssertNil(sut.validateZipCodeFormat("0000"))
        XCTAssertNil(sut.validateZipCodeFormat("0000000"))
        XCTAssertNotNil(sut.validateZipCodeFormat("00000000"))
    }
    
    func testValidateZipCode_WhenInputValidValuAndHaveSuccess_ShouldUpdateAddressPayload() throws {
        let expectedEvent = COPEvent.openField(productKey: "COP_123",
                                               sectionKey: "Section_1",
                                               screenKey: "Screen_1",
                                               fieldKey: "stateFieldKey",
                                               fieldType: "singleSelection",
                                               status: .success)
        serviceMock.isSuccess = true
        sut.validateZipCode("00000000")
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.updatePayloadCallsCount, 1)
        XCTAssertEqual(presenterSpy.enableFieldCallsCount, 2)
        let firstEnableField = try XCTUnwrap(presenterSpy.enableFieldReceivedInvocations.first)
        XCTAssertFalse(firstEnableField)
        let lastEnableField = try XCTUnwrap(presenterSpy.enableFieldReceivedInvocations.last)
        XCTAssertTrue(lastEnableField)
        let result = try XCTUnwrap(presenterSpy.updatePayloadReceivedInvocations.last)
        let street = try XCTUnwrap(result.first(where: { $0.fieldKey == "STREET_NAME" }))
        let neighborhood = try XCTUnwrap(result.first(where: { $0.fieldKey == "NEIGHBORHOOD" }))
        let city = try XCTUnwrap(result.first(where: { $0.fieldKey == "CITY" }))
        let state = try XCTUnwrap(result.first(where: { $0.fieldKey == "STATE" }))
        
        XCTAssertEqual(street.value?.name?.value as? String, "streetName")
        XCTAssertEqual(neighborhood.value?.name?.value as? String, "neighborhoodName")
        XCTAssertEqual(city.value?.name?.value as? String, "cityName")
        XCTAssertEqual(state.value?.name?.value as? String, "stateName")
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent.event()))
    }
    
    func testValidateZipCode_WhenInputValidValuAndHaveError_ShouldShowError() throws {
        let expectedEvent = COPEvent.openField(productKey: "COP_123",
                                               sectionKey: "Section_1",
                                               screenKey: "Screen_1",
                                               fieldKey: "stateFieldKey",
                                               fieldType: "singleSelection",
                                               status: .error)
        serviceMock.isSuccess = false
        sut.validateZipCode("00000000")
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.updatePayloadCallsCount, 0)
        XCTAssertEqual(presenterSpy.enableFieldCallsCount, 1)
        let enableField = try XCTUnwrap(presenterSpy.enableFieldReceivedInvocations.last)
        XCTAssertFalse(enableField)
        XCTAssertEqual(presenterSpy.showZipCodeErrorCallsCount, 1)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent.event()))
    }
    
    func testValidateZipCode_WhenInputIncompleteValue_ShouldDisableField() throws {
        serviceMock.isSuccess = true
        sut.validateZipCode("00000")
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 0)
        XCTAssertEqual(presenterSpy.updatePayloadCallsCount, 0)
        XCTAssertEqual(presenterSpy.enableFieldCallsCount, 1)
        let enableField = try XCTUnwrap(presenterSpy.enableFieldReceivedInvocations.last)
        XCTAssertFalse(enableField)
    }
}
