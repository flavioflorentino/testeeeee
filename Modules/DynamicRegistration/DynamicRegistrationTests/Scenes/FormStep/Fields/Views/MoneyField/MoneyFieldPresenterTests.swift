import XCTest
@testable import DynamicRegistration

private class MoneyFieldDisplaySpy: MoneyFieldDisplay {
    private(set) var updateValueDisplayValuePayloadValueCallsCount = 0
    private(set) var updateValueDisplayValuePayloadValueReceivedInvocations: [(displayValue: String?, payloadValue: String?)] = []

    func updateValue(displayValue: String?, payloadValue: String?) {
        updateValueDisplayValuePayloadValueCallsCount += 1
        updateValueDisplayValuePayloadValueReceivedInvocations.append((displayValue: displayValue, payloadValue: payloadValue))
    }
}

final class MoneyFieldPresenterTests: XCTestCase {
    private var viewSpy = MoneyFieldDisplaySpy()
    
    private lazy var sut: MoneyFieldPresenter = {
        let sut = MoneyFieldPresenter()
        sut.view = viewSpy
        return sut
    }()
    
    func testValueChanged_WhenInputSomeValidValue_ShouldFormatDisplayValue() throws {
        var input = "012"
        sut.valueChanged(input)
        var result = try XCTUnwrap(viewSpy.updateValueDisplayValuePayloadValueReceivedInvocations.last)
        XCTAssertEqual(result.displayValue, "R$ 0,12")
        XCTAssertEqual(result.payloadValue, "0.12")

        input = "1000000"
        sut.valueChanged(input)
        result = try XCTUnwrap(viewSpy.updateValueDisplayValuePayloadValueReceivedInvocations.last)
        XCTAssertEqual(result.displayValue, "R$ 10.000,00")
        XCTAssertEqual(result.payloadValue, "10000.00")

        input = "R$ 10.000,00"
        sut.valueChanged(input)
        result = try XCTUnwrap(viewSpy.updateValueDisplayValuePayloadValueReceivedInvocations.last)
        XCTAssertEqual(result.displayValue, "R$ 10.000,00")
        XCTAssertEqual(result.payloadValue, "10000.00")

        input = "10.000,00"
        sut.valueChanged(input)
        result = try XCTUnwrap(viewSpy.updateValueDisplayValuePayloadValueReceivedInvocations.last)
        XCTAssertEqual(result.displayValue, "R$ 10.000,00")
        XCTAssertEqual(result.payloadValue, "10000.00")

        input = "10000,00"
        sut.valueChanged(input)
        result = try XCTUnwrap(viewSpy.updateValueDisplayValuePayloadValueReceivedInvocations.last)
        XCTAssertEqual(result.displayValue, "R$ 10.000,00")
        XCTAssertEqual(result.payloadValue, "10000.00")

        input = "10000.00"
        sut.valueChanged(input)
        result = try XCTUnwrap(viewSpy.updateValueDisplayValuePayloadValueReceivedInvocations.last)
        XCTAssertEqual(result.displayValue, "R$ 10.000,00")
        XCTAssertEqual(result.payloadValue, "10000.00")

        input = "1234567890123456789"
        sut.valueChanged(input)
        result = try XCTUnwrap(viewSpy.updateValueDisplayValuePayloadValueReceivedInvocations.last)
        XCTAssertEqual(result.displayValue, "R$ 12.345.678.901.234.567,89")
        XCTAssertEqual(result.payloadValue, "12345678901234567.89")
    }
    
    func testValueChanged_WhenValidBigValue_ShouldFormatNormally() throws {
        let input = "12345678901234567890123456789123456789"
        sut.valueChanged(input)
        let result = try XCTUnwrap(viewSpy.updateValueDisplayValuePayloadValueReceivedInvocations.last)
        XCTAssertEqual(result.displayValue, "R$ 123.456.789.012.345.678.901.234.567.891.234.567,89")
        XCTAssertEqual(result.payloadValue, "123456789012345678901234567891234567.89")
    }
    
    func testValueChanged_WhenInvalidBigValue_ShouldClearValue() throws {
        let input = "123456789012345678901234567891234567899"
        sut.valueChanged(input)
        let result = try XCTUnwrap(viewSpy.updateValueDisplayValuePayloadValueReceivedInvocations.last)
        XCTAssertNil(result.displayValue)
        XCTAssertNil(result.payloadValue)
    }
    
    func testValueChanged_WhenInputInvalidValue_ShouldClearValue() throws {
        var input: String?
        sut.valueChanged(input)
        var result = try XCTUnwrap(viewSpy.updateValueDisplayValuePayloadValueReceivedInvocations.last)
        XCTAssertNil(result.displayValue)
        XCTAssertNil(result.payloadValue)
        
        input = "aaa"
        sut.valueChanged(input)
        result = try XCTUnwrap(viewSpy.updateValueDisplayValuePayloadValueReceivedInvocations.last)
        XCTAssertNil(result.displayValue)
        XCTAssertNil(result.payloadValue)
        
        input = ","
        sut.valueChanged(input)
        result = try XCTUnwrap(viewSpy.updateValueDisplayValuePayloadValueReceivedInvocations.last)
        XCTAssertNil(result.displayValue)
        XCTAssertNil(result.payloadValue)
        
        input = "."
        sut.valueChanged(input)
        result = try XCTUnwrap(viewSpy.updateValueDisplayValuePayloadValueReceivedInvocations.last)
        XCTAssertNil(result.displayValue)
        XCTAssertNil(result.payloadValue)
        
        input = "O"
        sut.valueChanged(input)
        result = try XCTUnwrap(viewSpy.updateValueDisplayValuePayloadValueReceivedInvocations.last)
        XCTAssertNil(result.displayValue)
        XCTAssertNil(result.payloadValue)
        
        input = "R$"
        sut.valueChanged(input)
        result = try XCTUnwrap(viewSpy.updateValueDisplayValuePayloadValueReceivedInvocations.last)
        XCTAssertNil(result.displayValue)
        XCTAssertNil(result.payloadValue)
    }
    
    func testCanInput_WhenInputValidValues_ShouldReturnTrue() {
        var result = sut.canInput("1", updatedText: "21")
        XCTAssertTrue(result)
        
        result = sut.canInput("0", updatedText: "21")
        XCTAssertTrue(result)
        
        result = sut.canInput("6", updatedText: "21")
        XCTAssertTrue(result)
    }
    
    func testCanInput_WhenInputInvalidValues_ShouldReturnFalse() {
        var result = sut.canInput("a", updatedText: "21")
        XCTAssertFalse(result)
        
        result = sut.canInput(".", updatedText: "21")
        XCTAssertFalse(result)
        
        result = sut.canInput("R$", updatedText: "21")
        XCTAssertFalse(result)
        
        result = sut.canInput("-", updatedText: "21")
        XCTAssertFalse(result)
        
        result = sut.canInput(",", updatedText: "21")
        XCTAssertFalse(result)
    }
    
    func testCanInput_WhenInputInvalidBigNumber_ShouldReturnFalse() {
        let result = sut.canInput("9", updatedText: "123456789012345678901234567891234567899")
        XCTAssertFalse(result)
    }
}
