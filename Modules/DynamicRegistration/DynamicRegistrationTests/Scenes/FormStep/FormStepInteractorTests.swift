import XCTest
import Core
import AnalyticsModule

@testable import DynamicRegistration

private class FormStepPresentingSpy: FormStepPresenting {
    var viewController: FormStepDisplay?
    private(set) var displayFormStepFormStepSectionCallsCount = 0
    // swiftlint:disable:next large_tuple
    private(set) var displayFormStepReceivedInvocations: [(formStep: FormStep, fields: [FieldForm], section: SectionItemForm, productKey: String, needUpdateSection: Bool)] = []
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didNextStepActionReceivedInvocations: [FormStepAction] = []
    private(set) var updateFormIsValidCallsCount = 0
    private(set) var updateFormIsValidReceivedInvocations: [Bool] = []
    private(set) var showLoadingCallsCount = 0
    private(set) var showErrorErrorCallsCount = 0
    private(set) var showErrorErrorReceivedInvocations: [Error] = []
    private(set) var hideKeyboardCallsCount = 0
    private(set) var showPopupErrorCallsCount = 0
    private(set) var didTapFieldReceivedInvocations: [FieldForm] = []
    private(set) var didUpdateFieldReceivedInvocations: [FieldForm] = []
    
    func displayFormStep(formStep: FormStep, fields: [FieldForm], section: SectionItemForm, productKey: String, needUpdateSection: Bool) {
        displayFormStepFormStepSectionCallsCount += 1
        displayFormStepReceivedInvocations.append((formStep: formStep, fields: fields, section: section, productKey: productKey, needUpdateSection: needUpdateSection))
    }
    
    func didNextStep(action: FormStepAction) {
        didNextStepActionCallsCount += 1
        didNextStepActionReceivedInvocations.append(action)
    }
    
    func updateFormIsValid(_ isValid: Bool) {
        updateFormIsValidCallsCount += 1
        updateFormIsValidReceivedInvocations.append(isValid)
    }
    
    func showLoading() {
        showLoadingCallsCount += 1
    }
    
    func showError(error: Error) {
        showErrorErrorCallsCount += 1
        showErrorErrorReceivedInvocations.append(error)
    }
    
    func hideKeyboard() {
        hideKeyboardCallsCount += 1
    }
    
    func showPopupError() {
        showPopupErrorCallsCount += 1
    }
    
    func didTapField(field: FieldForm, screenKey: String) {
        didTapFieldReceivedInvocations.append(field)
    }
    
    func didUpdateField(_ fieldForm: FieldForm) {
        didUpdateFieldReceivedInvocations.append(fieldForm)
    }
}

private class FormStepServicingMock: FormStepServicing {
    var isSuccess = true
    private(set) var updateSectionSectionInfoReceivedInvocations: [SectionInfo] = []
    
    func updateSection(sectionInfo: SectionInfo, completion: @escaping CompletionUpdateSectionResult) {
        updateSectionSectionInfoReceivedInvocations.append(sectionInfo)
        guard isSuccess else {
            completion(.failure(.serverError))
            return
        }
        
        completion(.success(.init(productKey: "productKey", sectionKey: "sectionKey", status: .complete)))
    }
}

// swiftlint:disable:next type_body_length
final class FormStepInteractorTests: XCTestCase {
    private var analyticsSpy = AnalyticsSpy()
    private var presenterSpy = FormStepPresentingSpy()
    private let serviceMock = FormStepServicingMock()
    private let firstFormStep = FormStep(screenKey: "screenKey 1", title: "Title Form 1", subtitle: "", fields: [])
    private let secondFormStep = FormStep(screenKey: "screenKey 2", title: "Title Form 2", subtitle: "", fields: [])
    private lazy var sectionItem = SectionItemForm(
        sectionKey: "section 1",
        title: "title",
        status: .empty,
        formSteps: [firstFormStep, secondFormStep]
    )
    private lazy var sectionInfo = SectionInfo(productKey: "card", section: sectionItem, productTemplateVersion: "1.1", updatedFormSteps: [:])
    
    private lazy var sut = FormStepInteractor(
        dependencies: DependencyContainerMock(analyticsSpy),
        presenter: presenterSpy,
        formStep: secondFormStep,
        sectionInfo: sectionInfo,
        service: serviceMock
    )
    
    func testValidateFields_WhenAllFieldsAreValid_ShouldUpdateForm() throws {
        let fieldFake1 = try XCTUnwrap(makeFieldValidate(fieldKey: "field 1"))
        let fieldFake2 = try XCTUnwrap(makeFieldValidate(fieldKey: "field 2"))
        fieldFake1.isValid = true
        fieldFake2.isValid = true
        
        sut.validateFields(fields: [fieldFake1, fieldFake2])
        XCTAssertEqual(presenterSpy.updateFormIsValidCallsCount, 1)
        let updateFormIsValid = try XCTUnwrap(presenterSpy.updateFormIsValidReceivedInvocations.last)
        XCTAssertTrue(updateFormIsValid)
    }
    
    func testValidateFields_WhenSomeFieldIsInvalid_ShouldUpdateForm() throws {
        let fieldFake1 = try XCTUnwrap(makeFieldValidate(fieldKey: "field 1"))
        let fieldFake2 = try XCTUnwrap(makeFieldValidate(fieldKey: "field 2"))
        fieldFake1.isValid = true
        fieldFake2.isValid = false
        
        sut.validateFields(fields: [fieldFake1, fieldFake2])
        XCTAssertEqual(presenterSpy.updateFormIsValidCallsCount, 1)
        let updateFormIsValid = try XCTUnwrap(presenterSpy.updateFormIsValidReceivedInvocations.last)
        XCTAssertFalse(updateFormIsValid)
    }
    
    func testSetupInitialForm_WhenViewIsLoaded_ShouldDisplayFormStep() {
        sut.setupInitialForm()
        XCTAssertEqual(presenterSpy.displayFormStepFormStepSectionCallsCount, 1)
        let formStepSectionResult = presenterSpy.displayFormStepReceivedInvocations.last
        XCTAssertEqual(formStepSectionResult?.formStep.screenKey, secondFormStep.screenKey)
        XCTAssertEqual(formStepSectionResult?.section.sectionKey, sectionItem.sectionKey)
    }
    
    func testDidConfirm_WhenDoNotNeedUpdateSection_ShouldUpdateRemotely() throws {
        let expectedEvent = COPEvent.nextFormStep(productKey: "card", sectionKey: "section 1", screenKey: "screenKey 1")
        serviceMock.isSuccess = true
        
        sut = FormStepInteractor(
            dependencies: DependencyContainerMock(analyticsSpy),
            presenter: presenterSpy,
            formStep: firstFormStep,
            sectionInfo: sectionInfo,
            service: serviceMock
        )
        
        sut.didConfirm(newValues: [.init(fieldKey: "newKey", value: FieldValue(value: "val123"))])

        XCTAssertEqual(presenterSpy.hideKeyboardCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 0)
        XCTAssertEqual(serviceMock.updateSectionSectionInfoReceivedInvocations.count, 0)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent.event()))
    }
    
    func testDidConfirm_WhenNeedUpdateSection_ShouldUpdateRemotely() throws {
        let expectedEvent = COPEvent.submitFormSection(productKey: "card", sectionKey: "section 1", screenKey: "screenKey 2", status: .success)
        serviceMock.isSuccess = true
        sut.didConfirm(newValues: [.init(fieldKey: "newKey", value: FieldValue(value: "val123"))])
        XCTAssertEqual(presenterSpy.hideKeyboardCallsCount, 1)
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 1)
        let sectionInfo = try XCTUnwrap(serviceMock.updateSectionSectionInfoReceivedInvocations.last)
        let updatedFieldsCurrentFormStep = sectionInfo.updatedFormSteps[secondFormStep.screenKey] ?? []
        XCTAssertTrue(updatedFieldsCurrentFormStep.contains(where: { $0.fieldKey == "newKey" }))
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent.event()))
    }
    
    func testDidConfirm_WhenNeedUpdateSectionAndServerReturnError_ShouldShowError() throws {
        let expectedEvent = COPEvent.submitFormSection(productKey: "card", sectionKey: "section 1", screenKey: "screenKey 2", status: .error)
        serviceMock.isSuccess = false
        sut.didConfirm(newValues: [.init(fieldKey: "newKey", value: FieldValue(value: "val123"))])
        XCTAssertEqual(presenterSpy.hideKeyboardCallsCount, 1)
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 1)
        let sectionInfo = try XCTUnwrap(serviceMock.updateSectionSectionInfoReceivedInvocations.last)
        let updatedFieldsCurrentFormStep = sectionInfo.updatedFormSteps[secondFormStep.screenKey] ?? []
        XCTAssertTrue(updatedFieldsCurrentFormStep.contains(where: { $0.fieldKey == "newKey" }))
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.showPopupErrorCallsCount, 1)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent.event()))
    }
    
    func testDidTapField_WhenCalledFromViewController_ShouldCallPresenter() throws {
        sut.didTapField(field: .init(
            fieldKey: "stateFieldKey",
            fieldType: .singleSelection,
            placeholder: "",
            value: .init(value: "Pernambuco", id: 5),
            validators: [],
            dependencies: []
        ))
        XCTAssertEqual(presenterSpy.didTapFieldReceivedInvocations.count, 1)
        XCTAssertEqual(presenterSpy.didTapFieldReceivedInvocations.last?.fieldKey, "stateFieldKey")
    }
    
    func testDidUpdateField_WhenCalledFromViewController_ShouldCallPresenter() {
        let fieldForm = FieldForm(
            fieldKey: "stateFieldKey",
            fieldType: .singleSelection,
            placeholder: "",
            value: .init(value: "initial value", id: 1),
            validators: [],
            dependencies: []
        )
        setupSutWithFields([fieldForm])
        
        sut.didUpdateField(fieldForm)
        XCTAssertEqual(presenterSpy.didUpdateFieldReceivedInvocations.count, 1)
        XCTAssertEqual(presenterSpy.didUpdateFieldReceivedInvocations.last?.fieldKey, "stateFieldKey")
    }
    
    func testNeedUpdateSection_WhenIsLastFormStep_ShouldReturnTrue() {
        XCTAssertTrue(sut.needUpdateSection(secondFormStep))
    }
    
    func testNeedUpdateSection_WhenIsNotLastFormStep_ShouldReturnFalse() {
        XCTAssertFalse(sut.needUpdateSection(firstFormStep))
    }
    
    func testGetCurrentFieldValues_WhenHasFilledValues_ShouldReturnCurrentFieldValue() throws {
        setupSutWithFields([
            .init(
                fieldKey: "fieldKey",
                fieldType: .singleSelection,
                placeholder: "",
                value: .init(value: "initial value", id: 1),
                validators: [],
                dependencies: []
            )
        ])

        let fieldValues = sut.getCurrentFieldValues()
        let fieldValue = try? XCTUnwrap(fieldValues.first?.value)
        XCTAssertEqual(fieldValue?.id, 1)
        XCTAssertEqual(fieldValue?.name?.value as? String, "initial value")
    }
    
    func testGetUpdatedFieldValues_WhenHasNewValues_ShouldReturnValueChanged() throws {
        setupSutWithFields([
            .init(
                fieldKey: "fieldKey",
                fieldType: .singleSelection,
                placeholder: "",
                value: .init(value: "initial value", id: 1),
                validators: [],
                dependencies: []
            )
        ],
        updatedFields: [
            .init(fieldKey: "fieldKey", value: .init(value: "changed value", id: 2))
        ])

        let fieldValues = sut.getUpdatedFieldValues()
        let fieldValue = try? XCTUnwrap(fieldValues.first?.value)
        XCTAssertEqual(fieldValue?.id, 2)
        XCTAssertEqual(fieldValue?.name?.value as? String, "changed value")
    }
    
    func testGetChangedPathFields_WhenHasSomePathChanged_ShouldBeReturned() {
        setupSutWithFields([
            .init(
                fieldKey: "stateFieldKey",
                fieldType: .singleSelection,
                placeholder: "",
                value: .init(value: "Pernambuco", id: 5),
                validators: [],
                dependencies: []
            ),
            .init(
                fieldKey: "cityFieldKey",
                fieldType: .singleSelection,
                placeholder: "",
                value: .init(value: "Recife", id: 1),
                validators: [],
                dependencies: [.init(dependencyType: .id, dependencyField: "stateFieldKey")],
                url: "path?={stateFieldKey}"
            )
        ],
        updatedFields: [
            .init(fieldKey: "stateFieldKey", value: .init(value: "São Paulo", id: 2))
        ])

        let fieldForms = sut.getChangedPathFields()
        XCTAssertTrue(fieldForms.isNotEmpty)
        let fieldForm = try? XCTUnwrap(fieldForms.first)
        XCTAssertEqual(fieldForm?.fieldKey, "cityFieldKey")
        
        let validFields = sut.getValidFields()
        XCTAssertTrue(validFields.isNotEmpty)
        let cityField = try? XCTUnwrap(validFields.first { $0.fieldKey == "cityFieldKey" })
        XCTAssertNil(cityField?.value)
    }
    
    func testGetChangedPathFields_WhenHasNotChanges_ShouldReturnEmpty() {
        setupSutWithFields([
            .init(
                fieldKey: "stateFieldKey",
                fieldType: .singleSelection,
                placeholder: "",
                value: .init(value: "Pernambuco", id: 5),
                validators: [],
                dependencies: []
            ),
            .init(
                fieldKey: "cityFieldKey",
                fieldType: .singleSelection,
                placeholder: "",
                value: .init(value: "Recife", id: 1),
                validators: [],
                dependencies: [.init(dependencyType: .id, dependencyField: "stateFieldKey")],
                url: "path?={stateFieldKey}"
            )
        ],
        updatedFields: [
            .init(fieldKey: "stateFieldKey", value: .init(value: "Pernambuco", id: 5))
        ])

        let fieldForms = sut.getChangedPathFields()
        XCTAssertTrue(fieldForms.isEmpty)
    }
    
    private func makeFieldValidate(fieldKey: String) -> FieldValidateFake? {
        let fakeKeys = FieldAnalyticsKeys(productKey: "ProductKey",
                                          sectionKey: "section 1",
                                          screenKey: "screen 1")
        let fakeForm = FieldForm(fieldKey: fieldKey,
                                 fieldType: .text,
                                 placeholder: "",
                                 value: nil,
                                 validators: [],
                                 dependencies: [])
        return FieldValidateFake(field: fakeForm, analyticsKeys: fakeKeys)
    }
    
    private func setupSutWithFields(_ fields: [FieldForm], updatedFields: [FieldPayload] = []) {
        let firstFormStep = FormStep(
            screenKey: "screenKey 1",
            title: "Title Form 1",
            subtitle: "",
            fields: fields
        )
        let sectionItem = SectionItemForm(
            sectionKey: "section 1",
            title: "title",
            status: .empty,
            formSteps: [firstFormStep, secondFormStep]
        )
        
        let sectionInfo = SectionInfo(
            productKey: "card",
            section: sectionItem,
            productTemplateVersion: "1.1",
            updatedFormSteps: [firstFormStep.screenKey: updatedFields]
        )
        
        sut = FormStepInteractor(
            dependencies: DependencyContainerMock(analyticsSpy),
            presenter: presenterSpy,
            formStep: firstFormStep,
            sectionInfo: sectionInfo,
            service: serviceMock
        )
    }
}

private class FieldValidateFake: FieldValidate {
    var isValid = false
    var valueChanged: () -> Void = {}
    var value: FieldPayload
    var didTap: (FieldForm) -> Void = { _ in }
    var didUpdateFieldValues: ([FieldPayload]) -> Void = { _ in }
    var showLoading: (Bool) -> Void = { _ in }
    
    required init?(field: FieldForm, analyticsKeys: FieldAnalyticsKeys) {
        value = .init(fieldKey: field.fieldKey, value: field.value)
    }
}
