import XCTest
import UI
import Core

@testable import DynamicRegistration

private class SingleSelectionDisplayingSpy: SingleSelectionDisplaying {
    private(set) var displayContentCallsCount = 0
    private(set) var displayContentReceivedInvocations: [[SingleSelectionItem]] = []
    private(set) var displayTitleCallsCount = 0
    private(set) var displayTitleReceivedInvocations: [String] = []
    private(set) var showLoadingCallsCount = 0
    private(set) var hideLoadingCallsCount = 0
    private(set) var showSearchBarCallsCount = 0
    private(set) var showSearchBarReceivedInvocations: [Bool] = []
    private(set) var showErrorCallsCount = 0
    private(set) var showErrorReceivedInvocations: [StatefulFeedbackViewModel] = []
    private(set) var showNoResultsCallsCount = 0
    private(set) var showNoResultsReceivedInvocations: [StatefulFeedbackViewModel] = []

    func displayContent(_ singleSelectionItems: [SingleSelectionItem]) {
        displayContentCallsCount += 1
        displayContentReceivedInvocations.append(singleSelectionItems)
    }

    func displayTitle(_ title: String) {
        displayTitleCallsCount += 1
        displayTitleReceivedInvocations.append(title)
    }

    func showLoading() {
        showLoadingCallsCount += 1
    }

    func hideLoading() {
        hideLoadingCallsCount += 1
    }

    func showSearchBar(_ show: Bool) {
        showSearchBarCallsCount += 1
        showSearchBarReceivedInvocations.append(show)
    }

    func showError(_ statefulModel: StatefulFeedbackViewModel) {
        showErrorCallsCount += 1
        showErrorReceivedInvocations.append(statefulModel)
    }

    func showNoResults(_ statefulModel: StatefulFeedbackViewModel) {
        showNoResultsCallsCount += 1
        showNoResultsReceivedInvocations.append(statefulModel)
    }
}

private class SingleSelectionCoordinatingSpy: SingleSelectionCoordinating {
    var viewController: UIViewController?
    var didUpdate: (FieldForm) -> Void = { _ in }
    private(set) var performActionCallsCount = 0
    private(set) var performActionReceivedInvocations: [SingleSelectionAction] = []

    func perform(action: SingleSelectionAction) {
        performActionCallsCount += 1
        performActionReceivedInvocations.append(action)
    }
}

final class SingleSelectionPresenterTests: XCTestCase {
    private var viewControllerSpy = SingleSelectionDisplayingSpy()
    private let coordinatorSpy = SingleSelectionCoordinatingSpy()
    
    private lazy var sut: SingleSelectionPresenter = {
        let sut = SingleSelectionPresenter(coordinator: coordinatorSpy)
        sut.viewController = viewControllerSpy
        return sut
    }()
    
    func testDisplayContent_WhenIsCalledFromInteractor_ShouldDisplayContentOnViewController() {
        sut.displayContent([.init(id: 1, name: "abc")])
        XCTAssertEqual(viewControllerSpy.displayContentCallsCount, 1)
    }
    
    func testDisplayTitle_WhenIsCalledFromInteractor_ShouldDisplayTitleOnViewController() {
        sut.displayTitle("Abc")
        XCTAssertEqual(viewControllerSpy.displayTitleCallsCount, 1)
    }
    
    func testSetupSeachBar_WhenIsCalledFromInteractor_ShouldNotDisplaySearchBarOnViewController() throws {
        sut.setupSearchBar(3)
        let showSearchBar = try XCTUnwrap(viewControllerSpy.showSearchBarReceivedInvocations.last)
        XCTAssertFalse(showSearchBar)
    }
    
    func testSetupSeachBar_WhenIsCalledFromInteractor_ShouldDisplaySearchBarOnViewController() throws {
        sut.setupSearchBar(8)
        let showSearchBar = try XCTUnwrap(viewControllerSpy.showSearchBarReceivedInvocations.last)
        XCTAssertTrue(showSearchBar)
    }
    
    func testDidNextStep_WhenIsCalledFromInteractor_ShouldCallCoordinator() {
        sut.didNextStep(action: .close)
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
    }
    
    func testShowLoading_WhenIsCalledFromInteractor_ShouldShowLoadingOnViewController() {
        sut.showLoading()
        XCTAssertEqual(viewControllerSpy.showLoadingCallsCount, 1)
    }
    
    func testHideLoading_WhenIsCalledFromInteractor_ShouldHideLoadingOnViewController() {
        sut.hideLoading()
        XCTAssertEqual(viewControllerSpy.hideLoadingCallsCount, 1)
    }
    
    func testShowError_WhenIsCalledFromInteractor_ShouldShowErrorOnViewController() {
        sut.showError()
        XCTAssertEqual(viewControllerSpy.showErrorCallsCount, 1)
    }
    
    func testShowNoResults_WhenIsCalledFromInteractor_ShouldShowNoResultsOnViewController() {
        sut.showNoResults()
        XCTAssertEqual(viewControllerSpy.showNoResultsCallsCount, 1)
    }
}
