import XCTest
import Core
import AnalyticsModule

@testable import DynamicRegistration

private class SingleSelectionPresentingSpy: SingleSelectionPresenting {
    var viewController: SingleSelectionDisplaying?
    private(set) var displayContentCallsCount = 0
    private(set) var displayContentReceivedInvocations: [[SingleSelectionItem]] = []
    private(set) var displayTitleCallsCount = 0
    private(set) var displayTitleReceivedInvocations: [String?] = []
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didNextStepActionReceivedInvocations: [SingleSelectionAction] = []
    private(set) var setupSearchBarCallsCount = 0
    private(set) var setupSearchBarReceivedInvocations: [Int] = []
    private(set) var showLoadingCallsCount = 0
    private(set) var hideLoadingCallsCount = 0
    private(set) var showErrorCallsCount = 0
    private(set) var showNoResultsCallsCount = 0
    
    func displayContent(_ singleSelectionItems: [SingleSelectionItem]) {
        displayContentCallsCount += 1
        displayContentReceivedInvocations.append(singleSelectionItems)
    }

    func displayTitle(_ title: String?) {
        displayTitleCallsCount += 1
        displayTitleReceivedInvocations.append(title)
    }

    func didNextStep(action: SingleSelectionAction) {
        didNextStepActionCallsCount += 1
        didNextStepActionReceivedInvocations.append(action)
    }

    func setupSearchBar(_ totalItemsCount: Int) {
        setupSearchBarCallsCount += 1
        setupSearchBarReceivedInvocations.append(totalItemsCount)
    }

    func showLoading() {
        showLoadingCallsCount += 1
    }

    func hideLoading() {
        hideLoadingCallsCount += 1
    }
   
    func showError() {
        showErrorCallsCount += 1
    }

    func showNoResults() {
        showNoResultsCallsCount += 1
    }
}

private class SingleSelectionServicingMock: SingleSelectionServicing {
    var isSuccess = true

    func getData(path: String, completion: @escaping CompletionSingleSelectionResult) {
        guard isSuccess else {
            completion(.failure(.serverError))
            return
        }
        
        completion(.success([.init(id: 1, name: "Recife"), .init(id: 2, name: "Olinda")]))
    }
}

final class SingleSelectionInteractorTests: XCTestCase {
    private var analyticsSpy = AnalyticsSpy()
    private var presenterSpy = SingleSelectionPresentingSpy()
    private let serviceMock = SingleSelectionServicingMock()
    private lazy var fieldForm = FieldForm(
        fieldKey: "fieldKey",
        fieldType: .singleSelection,
        placeholder: nil,
        value: nil,
        validators: [],
        dependencies: [],
        url: "path"
    )
    
    private let firstFormStep = FormStep(screenKey: "screenKey 1", title: "Title Form 1", subtitle: "", fields: [])
    private let secondFormStep = FormStep(screenKey: "screenKey 2", title: "Title Form 2", subtitle: "", fields: [])
    private lazy var sectionItem = SectionItemForm(
        sectionKey: "section 1",
        title: "title",
        status: .empty,
        formSteps: [firstFormStep, secondFormStep]
    )
    private lazy var sectionInfo = SectionInfo(productKey: "card", section: sectionItem, productTemplateVersion: "1.1", updatedFormSteps: [:])
    
    private lazy var sut = SingleSelectionInteractor(
        fieldForm: fieldForm,
        sectionInfo: sectionInfo,
        screenKey: "Screen 1",
        dependencies: DependencyContainerMock(analyticsSpy),
        service: serviceMock,
        presenter: presenterSpy
    )
    
    func testClose_WhenIsCalledFromViewController_ShouldCallDidNextStep() {
        sut.close()
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
    }
    
    func testViewDidLoad_WhenIsCalledFromViewController_ShouldCallDisplayTitle() {
        sut.viewDidLoad()
        XCTAssertEqual(presenterSpy.displayTitleCallsCount, 1)
    }
    
    func testLoadData_WhenHasSuccess_ShouldPresentInformation() {
        let expectedEvent = COPEvent.openField(productKey: "card",
                                               sectionKey: "section 1",
                                               screenKey: "Screen 1",
                                               fieldKey: "fieldKey",
                                               fieldType: "singleSelection",
                                               status: .success)
        serviceMock.isSuccess = true
        sut.loadData()
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayContentCallsCount, 1)
        XCTAssertEqual(presenterSpy.setupSearchBarCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent.event()))
    }
    
    func testLoadData_WhenIsCalledFromViewController_ShouldPresentInformation() {
        let expectedEvent = COPEvent.openField(productKey: "card",
                                               sectionKey: "section 1",
                                               screenKey: "Screen 1",
                                               fieldKey: "fieldKey",
                                               fieldType: "singleSelection",
                                               status: .error)
        serviceMock.isSuccess = false
        sut.loadData()
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayContentCallsCount, 0)
        XCTAssertEqual(presenterSpy.showErrorCallsCount, 1)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent.event()))
    }
    
    func testSearchBy_WhenIsSearchedByQuery_ShouldCallDisplayContent() {
        serviceMock.isSuccess = true
        sut.loadData()
        sut.searchBy("Recife")
        let result = presenterSpy.displayContentReceivedInvocations.last
        XCTAssertEqual(result?.count, 1)
        XCTAssertEqual(result?.first?.name, "Recife")
    }
}
