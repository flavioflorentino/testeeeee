import XCTest

@testable import DynamicRegistration

class DeeplinkCOPTypeTests: XCTestCase {
    func testDeeplinkCOPType_WhenReceiveValidURL_ShouldInstantiateRegister() throws {
        let url = try XCTUnwrap(URL(string: "picpay://picpay/cop?productKey=COP_DEV_INTERNAL_STRUCTURES"))
        let sut = DeeplinkCOPType(url: url)
        XCTAssertNotNil(sut)
        XCTAssertEqual(sut, .register(productKey: "COP_DEV_INTERNAL_STRUCTURES"))
    }
    
    func testDeeplinkCOPType_WhenReceiveURLWithCallback_ShouldInstantiateRegisterWithCallbackAction() throws {
        let url = try XCTUnwrap(URL(string: "picpay://picpay/cop?productKey=COP_DEV_INTERNAL_STRUCTURES&callbackDeepLink=picpay://picpay/wallet"))
        let sut = DeeplinkCOPType(url: url)
        XCTAssertNotNil(sut)
        let expectedCallbackURL = try XCTUnwrap(URL(string: "picpay://picpay/wallet"))
        XCTAssertEqual(sut, .registerWithCallbackAction(productKey: "COP_DEV_INTERNAL_STRUCTURES", callbackUrl: expectedCallbackURL))
    }
    
    func testDeeplinkCOPType_WhenReceiveIncompleteURL_ShouldNotInstantiate() throws {
        var url = try XCTUnwrap(URL(string: "picpay://picpay/cop"))
        var sut = DeeplinkCOPType(url: url)
        XCTAssertNil(sut)
        
        url = try XCTUnwrap(URL(string: "picpay://picpay/cop?callbackDeepLink=picpay://picpay/wallet"))
        sut = DeeplinkCOPType(url: url)
        XCTAssertNil(sut)
    }
    
    func testDeeplinkCOPType_WhenInvalidURL_ShouldNotInstantiate() throws {
        let url = try XCTUnwrap(URL(string: "picpay://picpay/co"))
        let sut = DeeplinkCOPType(url: url)
        XCTAssertNil(sut)
    }
}
