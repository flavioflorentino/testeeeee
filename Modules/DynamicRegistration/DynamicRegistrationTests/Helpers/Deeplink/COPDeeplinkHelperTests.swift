import XCTest

@testable import DynamicRegistration

class COPDeeplinkHelperTests: XCTestCase {
    func testCOPDeeplinkHelper_WhenReceiveRegister_ShouldReturnRegistrationFlowControlCoordinator() {
        let navigation = UINavigationController()
        let sut = COPDeeplinkHelper(copType: .register(productKey: "productKey"))
        let coordinator = sut.getCoordinating(navigation)
        XCTAssertNotNil(coordinator)
        XCTAssertTrue(coordinator is RegistrationFlowControlCoordinator)
    }
    
    func testCOPDeeplinkHelper_WhenReceiveRegisterWithCallback_ShouldReturnRegistrationFlowControlCoordinator() throws {
        let navigation = UINavigationController()
        let callbackURL = try XCTUnwrap(URL(string: "picpay://picpay/wallet"))
        let sut = COPDeeplinkHelper(copType: .registerWithCallbackAction(productKey: "productKey", callbackUrl: callbackURL))
        let coordinator = sut.getCoordinating(navigation)
        XCTAssertNotNil(coordinator)
    }
}
