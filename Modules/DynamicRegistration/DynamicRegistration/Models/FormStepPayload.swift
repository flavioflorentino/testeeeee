struct FormStepPayload: Encodable {
    let screenKey: String
    let fields: [FieldPayload]
}
