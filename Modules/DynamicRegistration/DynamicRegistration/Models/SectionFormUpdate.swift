struct SectionFormUpdate: Decodable {
    let productKey: String
    let sectionKey: String
    let status: SectionStatus
}
