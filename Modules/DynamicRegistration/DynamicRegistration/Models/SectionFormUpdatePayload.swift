struct SectionFormUpdatePayload: Encodable {
    let productTemplateVersion: String
    let productKey: String
    let sectionKey: String
    let formSteps: [FormStepPayload]
}
