struct SingleSelectionItem: Decodable {
    let id: Int
    let name: String
}
