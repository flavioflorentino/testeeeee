struct Validator: Decodable {
    let type: ValidatorType
    let value: String
    let errorMessage: String
    
    enum ValidatorType: String, Codable {
        case regex
        case notExists
        
        init(from decoder: Decoder) throws {
            self = try ValidatorType(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .notExists
        }
        
        var isValid: Bool {
            self != .notExists
        }
    }
}

extension Array where Element == Validator {
    var isValid: Bool {
        let hasInvalidType = contains(where: { $0.type == .notExists })
        return !hasInvalidType
    }
}
