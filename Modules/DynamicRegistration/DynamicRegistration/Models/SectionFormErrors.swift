import Foundation

enum SectionFormErrors: Error {
    case invalidTypes
}
