enum SectionStatus: String, Codable {
    case complete = "COMPLETE"
    case error = "ERROR"
    case warning = "WARNING"
    case empty = "EMPTY"
    case notExists
    
    init(from decoder: Decoder) throws {
        self = try SectionStatus(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .notExists
    }
    
    var isValid: Bool {
        self != .notExists
    }
}
