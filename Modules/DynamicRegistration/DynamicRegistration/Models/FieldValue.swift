import Core

struct FieldValue: Codable {
    var id: Int?
    var name: AnyCodable?
    
    init(value: Any?, id: Int? = nil) {
        self.id = id
        if let value = value {
            self.name = AnyCodable(value: value)
        }
    }
}
