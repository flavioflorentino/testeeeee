struct SectionItemForm: Decodable {
    let sectionKey: String
    let title: String
    let sectionIcon: SectionIcon
    var status: SectionStatus
    // swiftlint:disable:next discouraged_optional_collection
    var formSteps: [FormStep]?
    let actionParam: ActionParam?
    let callToAction: Action?
        
    enum Action: String, Decodable {
        case startBiometryModule
        case startDocumentoscopyModule
        case startValidationIdentity
        case notExists
        
        var screenKey: String? {
            switch self {
            case .startDocumentoscopyModule:
                return "DOCUMENTOSCOPY_MODULE"
            case .startBiometryModule:
                return "BIOMETRY_MODULE"
            case .startValidationIdentity:
                return "VALIDATION_IDENTITY_MODULE"
            default:
                return nil
            }
        }
        
        var fieldKey: String? {
            switch self {
            case .startDocumentoscopyModule:
                return "DOCUMENTOSCOPY_STATUS"
            case .startBiometryModule:
                return "BIOMETRY_STATUS"
            case .startValidationIdentity:
                return "VALIDATION_IDENTITY_STATUS"
            default:
                return nil
            }
        }
        
        init(from decoder: Decoder) throws {
            self = try Action(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .notExists
        }
    }
    
    struct ActionParam: Decodable {
        let key: String
        let value: String
    }
    
    mutating func updateWith(status: SectionStatus, updatedFormSteps: FormStepsFieldValues) {
        self.status = status
        let formSteps = self.formSteps ?? []
        for (index, formStep) in formSteps.enumerated() {
            var formStep = formStep
            formStep.updateFieldValues(updatedFormSteps[formStep.screenKey] ?? [])
            
            self.formSteps?[index] = formStep
        }
    }
}

extension Array where Element == SectionItemForm {
    var isValid: Bool {
        let hasInvalidFormStepOrInvalidStatus = contains(where: {
            !($0.formSteps ?? []).isValid || !$0.status.isValid || $0.callToAction == .notExists
        })
        return !hasInvalidFormStepOrInvalidStatus
    }
}
