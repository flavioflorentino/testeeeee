import Foundation

struct FieldDependency: Decodable {
    let dependencyType: DependencyType
    let dependencyField: String
    
    enum DependencyType: String, Codable {
        case id
        case name
        case notExists
        
        init(from decoder: Decoder) throws {
            self = try DependencyType(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .notExists
        }
        
        var isValid: Bool {
            self != .notExists
        }
    }
}
