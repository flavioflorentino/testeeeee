import Foundation

struct Address: Decodable {
    var zipCode: String?
    var addressComplement: String?
    var city: String?
    var cityId: String?
    var neighborhood: String?
    var neighborhoodId: String?
    var state: String?
    var stateId: String?
    var stateName: String?
    var streetId: String?
    var streetName: String?
    var streetType: String?
    var street: String?
}
