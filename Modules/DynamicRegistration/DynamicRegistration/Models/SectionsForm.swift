struct SectionsForm: Decodable {
    let title: String
    let subtitle: String
    let productKey: String
    let productTemplateVersion: String
    var sections: [SectionItemForm]
    
    var isValid: Bool {
        sections.isValid
    }
}
