enum SectionIcon: String, Decodable {
    case person = "PERSON"
    case house = "HOUSE"
    case verticalPage = "VERTICAL_PAGE"
    case smile = "SMILE"
    case horizontalPage = "HORIZONTAL_PAGE"
    case documents = "DOCUMENTS"
    case generic = "GENERIC"
    
    init(from decoder: Decoder) throws {
        self = try SectionIcon(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .generic
    }
}
