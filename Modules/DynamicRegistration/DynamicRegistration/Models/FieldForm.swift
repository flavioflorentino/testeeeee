import Core

struct FieldForm: Decodable {
    let fieldKey: String
    let fieldType: FieldType
    let placeholder: String?
    var value: FieldValue?
    // swiftlint:disable discouraged_optional_collection
    let validators: [Validator]?
    let dependencies: [FieldDependency]?
    // swiftlint:enable discouraged_optional_collection
    var url: String?
    
    enum CodingKeys: String, CodingKey {
        case fieldKey
        case fieldType
        case placeholder = "placeHolder"
        case value
        case validators
        case dependencies
        case url
    }
    
    enum FieldType: String, Codable {
        case text
        case date
        case singleSelection
        case number
        case money
        case zipCode
        case notExists
        
        init(from decoder: Decoder) throws {
            self = try FieldType(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .notExists
        }
        
        var isValid: Bool {
            self != .notExists
        }
    }
}

extension Array where Element == FieldForm {
    var isValid: Bool {
        let hasInvalidTypeOrInvalidValidator = contains(where: { fieldForm in
            fieldForm.fieldType == .notExists ||
                !(fieldForm.validators ?? []).isValid
        })
        
        return !hasInvalidTypeOrInvalidValidator
    }
}

extension FieldForm {
    func getPathWithValues(_ fieldValues: [FieldPayload]) -> String {
        var path = url ?? ""
        
        fieldValues.forEach { fieldValue in
            let target = "{\(fieldValue.fieldKey)}"
            if path.contains(target) {
                let dependencyValue = getDependencyValue(fieldValue.fieldKey, fieldValue: fieldValue)
                path = path.replacingOccurrences(of: target, with: dependencyValue ?? "")
            }
        }
        return path
    }
    
    func getDependencyValue(_ fieldKey: String, fieldValue: FieldPayload) -> String? {
        guard let dependency = dependencies?
                .first(where: { $0.dependencyField == fieldKey }) else {
            return nil
        }
        
        switch dependency.dependencyType {
        case .id:
            return String(fieldValue.value?.id ?? 0)
        case .name:
            return (fieldValue.value?.name?.value as? String)
        default:
            return nil
        }
    }
}
