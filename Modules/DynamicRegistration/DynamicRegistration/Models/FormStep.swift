import Core

struct FormStep: Decodable {
    let screenKey: String
    let title: String
    let subtitle: String
    var fields: [FieldForm]
    
    mutating func updateFieldValues(_ values: [FieldPayload]) {
        for (index, field) in fields.enumerated() {
            guard let newValue = values.first(where: { $0.fieldKey == field.fieldKey })?.value else {
                fields[index].value = nil
                continue
            }
            fields[index].value = newValue
        }
    }
}

extension Array where Element == FormStep {
    var isValid: Bool {
        let hasInvalidField = contains(where: { !$0.fields.isValid })
        return !hasInvalidField
    }
}
