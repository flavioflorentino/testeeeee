import Core

struct FieldPayload: Encodable {
    let fieldKey: String
    let value: FieldValue?
}
