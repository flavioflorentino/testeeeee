import Foundation
import Validator

enum ValidatorRules {
    enum ValidateError: Error, CustomStringConvertible, ValidationError {
        case customMessage(message: String)
        case invalidDate
        case invalidNumber
        
        var description: String {
            message
        }
        
        var message: String {
            switch self {
            case let .customMessage(message):
                return message
            case .invalidDate:
                return Strings.ValidatorRules.invalidDate
            case .invalidNumber:
                return Strings.ValidatorRules.invalidNumber
            }
        }
    }
    
    static func make(_ validators: [Validator]) -> ValidationRuleSet<String> {
        var validationRules = ValidationRuleSet<String>()
        
        validators.forEach { validator in
            guard let regexRule = regexValidator(validator) else { return }
            validationRules.add(rule: regexRule)
        }
        
        return validationRules
    }
    
    static func regexValidator(_ validator: Validator) -> ValidationRulePattern? {
        guard validator.type == .regex, regexPatternIsValid(validator.value) else {
            return nil
        }
        return ValidationRulePattern(pattern: validator.value, error: ValidateError.customMessage(message: validator.errorMessage))
    }
    
    static func dateRules(dateLength: Int) -> ValidationRuleSet<String> {
        var rules = ValidationRuleSet<String>()

        let conditionRule = ValidationRuleCondition<String>(error: ValidateError.invalidDate) { dateString -> Bool in
            guard let dateString = dateString, Date.forthFormatter.date(from: dateString) != nil else {
                return false
            }
            return true
        }
        
        rules.add(rule: ValidationRuleLength(min: dateLength, error: ValidateError.invalidDate))
        rules.add(rule: conditionRule)
        return rules
    }
    
    static func numberValidator() -> ValidationRulePattern {
        ValidationRulePattern(pattern: "[0-9]", error: ValidateError.invalidNumber)
    }
    
    private static func regexPatternIsValid(_ pattern: String) -> Bool {
        (try? NSRegularExpression(pattern: pattern, options: [])) != nil
    }
}
