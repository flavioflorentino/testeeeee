import Foundation
import UI

protocol SectionsListPresenting: AnyObject {
    var viewController: SectionsListDisplay? { get set }
    func presentFormWith(sectionsForm: SectionsForm)
    func didNextStep(action: SectionsListAction)
    func showLoading()
    func showError(error: Error)
    func showPopupErrorFinish()
    func showPopuErrorSubmitIntegration(_ action: SectionItemForm.Action, params: SectionItemForm.ActionParam, sectionInfo: SectionInfo)
    func showConfirmButton(show: Bool)
}

final class SectionsListPresenter {
    private let coordinator: SectionsListCoordinating
    weak var viewController: SectionsListDisplay?

    init(coordinator: SectionsListCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - SectionsListPresenting
extension SectionsListPresenter: SectionsListPresenting {
    func showConfirmButton(show: Bool) {
        viewController?.showConfirmButton(show: show)
    }
    
    func showLoading() {
        viewController?.showLoading()
    }
    
    func showError(error: Error) {
        let statefulError = StatefulFeedbackViewModel(
            image: Assets.icoAttention.image,
            content: (
                title: Strings.SectionsList.genericErrorTitle,
                description: Strings.SectionsList.genericErrorMessage
        ),
            button: (image: nil, title: Strings.Default.retry),
            secondaryButton: (image: nil, title: Strings.Default.notNow)
        )
        viewController?.showError(statefulError)
    }
    
    func presentFormWith(sectionsForm: SectionsForm) {
        viewController?.renderFormHeader(form: sectionsForm)
        viewController?.renderSections(sections: sectionsForm.sections)
    }
    
    func didNextStep(action: SectionsListAction) {
        coordinator.perform(action: action)
    }
    
    func showPopupErrorFinish() {
        viewController?.showPopupErrorFinish()
    }
    
    func showPopuErrorSubmitIntegration(_ action: SectionItemForm.Action, params: SectionItemForm.ActionParam, sectionInfo: SectionInfo) {
        viewController?.showPopuErrorSubmitIntegration(action, params: params, sectionInfo: sectionInfo)
    }
}
