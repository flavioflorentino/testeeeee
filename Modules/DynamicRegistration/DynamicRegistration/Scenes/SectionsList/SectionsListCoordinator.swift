import UIKit

enum SectionsListAction {
    case openFormSection(sectionInfo: SectionInfo)
    case openModuleSection(action: SectionItemForm.Action, params: SectionItemForm.ActionParam, sectionInfo: SectionInfo)
    case close
    case finish
}

protocol SectionsListCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: SectionsListAction)
}

final class SectionsListCoordinator {
    private weak var flowCoordinator: RegistrationFlowControlCoordinating?
    
    weak var viewController: UIViewController?

    init(flowCoordinator: RegistrationFlowControlCoordinating) {
        self.flowCoordinator = flowCoordinator
    }
}

// MARK: - SectionsListCoordinating
extension SectionsListCoordinator: SectionsListCoordinating {
    func perform(action: SectionsListAction) {
        switch action {
        case let .openFormSection(section):
            flowCoordinator?.perform(action: .didOpenFormSection(section: section))
        case .close:
            flowCoordinator?.perform(action: .close)
        case .finish:
            flowCoordinator?.perform(action: .finish)
        case let .openModuleSection(action, params, section):
            flowCoordinator?.perform(action: .didOpenModuleSection(action: action, params: params, sectionInfo: section))
        }
    }
}
