import SnapKit
import UI
import UIKit

protocol SectionItemDisplay: AnyObject {
    func updateTitle(title: String)
    func updateContainerBorderColor(color: CGColor)
    func updateContainerBackgroundColor(color: UIColor)
    func updateStatusIcon(image: UIImage?)
    func updateTitleColor(color: UIColor)
    func updateSectionImage(image: UIImage?)
    func updateSectionImageColor(color: UIColor)
}

private extension SectionItemCell.Layout {
    enum Sizes {
        static let iconStatus = 22
    }
}

final class SectionItemCell: UITableViewCell {
    fileprivate enum Layout {}
    
    private let presenter: SectionItemCellPresenting
    
    private(set) lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .small))
            .with(\.textAlignment, .left)
        label.numberOfLines = 0
        return label
    }()
    
    private(set) lazy var container: UIView = {
        let view = UIView()
        view.border = .light(color: .grayscale100())
        view.layer.cornerRadius = CornerRadius.light
        return view
    }()
    
    private(set) lazy var iconSectionImageView = UIImageView()
    private(set) lazy var iconStatusImageView = UIImageView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        presenter = SectionItemCellPresenter()
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        presenter.view = self
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureWith(_ section: SectionItemForm, sectionIndex: Int, sections: [SectionItemForm]) {
        presenter.configureWith(section, sectionIndex: sectionIndex, sections: sections)
    }
}

// MARK: ViewConfiguration
extension SectionItemCell: ViewConfiguration {
    func buildViewHierarchy() {
        container.addSubview(iconSectionImageView)
        container.addSubview(titleLabel)
        container.addSubview(iconStatusImageView)
        contentView.addSubview(container)
    }
    
    func configureViews() {
        selectionStyle = .none
        backgroundColor = .clear
    }
    
    func setupConstraints() {
        container.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base00)
            $0.bottom.equalToSuperview().offset(-Spacing.base00)
            $0.height.equalTo(Sizing.base09)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        iconSectionImageView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.height.width.lessThanOrEqualTo(Sizing.base04)
        }
        
        titleLabel.snp.makeConstraints {
            $0.leading.equalTo(iconSectionImageView.snp.trailing).offset(Spacing.base01)
            $0.top.equalToSuperview().offset(Spacing.base01)
            $0.bottom.trailing.equalToSuperview().offset(-Spacing.base01)
        }
        
        iconStatusImageView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview().offset(-Spacing.base03)
            $0.width.height.equalTo(Layout.Sizes.iconStatus)
        }
    }
}

extension SectionItemCell: SectionItemDisplay {
    func updateTitle(title: String) {
        titleLabel.text = title
    }
    
    func updateSectionImage(image: UIImage?) {
        iconSectionImageView.image = image
    }
    
    func updateSectionImageColor(color: UIColor) {
        iconSectionImageView.tintColor = color
    }
    
    func updateContainerBorderColor(color: CGColor) {
        container.layer.borderColor = color
    }
    
    func updateContainerBackgroundColor(color: UIColor) {
        container.backgroundColor = color
    }
    
    func updateStatusIcon(image: UIImage?) {
        iconStatusImageView.image = image
    }
    
    func updateTitleColor(color: UIColor) {
        titleLabel.textColor = color
    }
}
