import Foundation
import UI
import UIKit
import AssetsKit

protocol SectionItemCellPresenting: AnyObject {
    var view: SectionItemDisplay? { get set }
    func configureWith(_ section: SectionItemForm, sectionIndex: Int, sections: [SectionItemForm])
}

final class SectionItemCellPresenter {
    weak var view: SectionItemDisplay?
    
    func iconSectionsFor(_ status: SectionIcon) -> UIImage {
        switch status {
        case .documents:
            return Assets.icoSectionDocuments.image
        case .horizontalPage:
            return Assets.icoSectionHorizontalPage.image
        case .house:
            return Assets.icoSectionHouse.image
        case .person:
            return Assets.icoSectionPerson.image
        case .smile:
            return Assets.icoSectionSmile.image
        case .verticalPage:
            return Assets.icoSectionVerticalPage.image
        case .generic:
            return Assets.icoSectionGeneric.image
        }
    }
    
    func iconStatusFor(_ status: SectionStatus) -> UIImage? {
        switch status {
        case .complete:
            return Assets.sectionSuccess.image
        case .warning:
            return Assets.sectionWarning.image
        case .error:
            return Assets.sectionError.image
        case .empty:
            return Resources.Icons.icoGreenRightArrow.image
        case .notExists:
            return nil
        }
    }
    
    func sectionIconColorFor(_ status: SectionStatus) -> UIColor {
        switch status {
        case .complete:
            return Colors.branding600.color
        case .warning:
            return Colors.warning600.color
        case .error:
            return Colors.critical600.color
        case .empty:
            return Colors.success600.color
        case .notExists:
            return Colors.grayscale100.color
        }
    }
    
    func borderColorFor(_ status: SectionStatus) -> UIColor {
        switch status {
        case .complete:
            return Colors.branding600.color
        case .warning:
            return Colors.warning600.color
        case .error:
            return Colors.critical600.color
        case .empty, .notExists:
            return Colors.grayscale100.color
        }
    }
    
    func getCurrentPendingIndexFor(_ sections: [SectionItemForm]) -> Int? {
        sections.firstIndex(where: { $0.status != .complete })
    }
    
    func isLocked(index: Int, sections: [SectionItemForm]) -> Bool {
        guard let currentIndex = getCurrentPendingIndexFor(sections) else {
            return false
        }
        return index > currentIndex
    }
}

// MARK: - SectionItemCellPresenting
extension SectionItemCellPresenter: SectionItemCellPresenting {
    func configureWith(_ section: SectionItemForm, sectionIndex: Int, sections: [SectionItemForm]) {
        view?.updateTitle(title: section.title)
        view?.updateSectionImage(image: iconSectionsFor(section.sectionIcon))
        
        guard isLocked(index: sectionIndex, sections: sections) else {
            view?.updateSectionImageColor(color: sectionIconColorFor(section.status))
            view?.updateStatusIcon(image: iconStatusFor(section.status))
            view?.updateTitleColor(color: Colors.grayscale700.color)
            view?.updateContainerBackgroundColor(color: .clear)
            view?.updateContainerBorderColor(color: borderColorFor(section.status).cgColor)
            return
        }
        
        view?.updateSectionImageColor(color: Colors.grayscale400.color)
        view?.updateStatusIcon(image: nil)
        view?.updateTitleColor(color: Colors.grayscale400.color)
        view?.updateContainerBackgroundColor(color: Colors.grayscale050.color)
        view?.updateContainerBorderColor(color: Colors.grayscale100.color.cgColor)
    }
}
