import Core
import Foundation

protocol SectionsListServicing {
    func finish(productKey: String, completion: @escaping CompletionFinishRegistrationResult)
    func sectionsFormWith(productKey: String, completion: @escaping CompletionSectionsFormResult)
    func submitIntegration(_ payload: SectionFormUpdatePayload, completion: @escaping CompletionUpdateIntegrationSectionResult)
}

typealias CompletionSectionsFormResult = (Result<SectionsForm, ApiError>) -> Void
typealias CompletionFinishRegistrationResult = (Result<NoContent, ApiError>) -> Void
typealias CompletionUpdateIntegrationSectionResult = (Result<(SectionFormUpdate), ApiError>) -> Void

final class SectionsListService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - SectionsListServicing
extension SectionsListService: SectionsListServicing {
    func submitIntegration(_ payload: SectionFormUpdatePayload, completion: @escaping CompletionUpdateIntegrationSectionResult) {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        Api<SectionFormUpdate>(endpoint: RegisterEndpoint.updateSection(payload))
            .execute(jsonDecoder: decoder) { [weak self] result in
                let mappedResult = result
                    .map(\.model)

                self?.dependencies.mainQueue.async {
                    completion(mappedResult)
                }
            }
    }
    
    func finish(productKey: String, completion: @escaping CompletionFinishRegistrationResult) {
        Api<NoContent>(endpoint: RegisterEndpoint.finishRegistration(productKey: productKey))
            .execute { [weak self] result in
                let mappedResult = result
                    .map(\.model)
                self?.dependencies.mainQueue.async {
                    completion(mappedResult)
                }
            }
    }
    
    func sectionsFormWith(productKey: String, completion: @escaping CompletionSectionsFormResult) {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        Api<SectionsForm>(endpoint: RegisterEndpoint.formWith(productKey: productKey))
            .execute(jsonDecoder: decoder) { [weak self] result in
                let mappedResult = result
                    .map(\.model)
                self?.dependencies.mainQueue.async {
                    completion(mappedResult)
                }
            }
    }
}
