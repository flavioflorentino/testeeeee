import Foundation
import UIKit

enum SectionsListFactory {
    static func make(productKey: String, flowCoordinator: RegistrationFlowControlCoordinating) -> UIViewController {
        let container = DependencyContainer()
        let service: SectionsListServicing = SectionsListService(dependencies: container)
        let coordinator: SectionsListCoordinating = SectionsListCoordinator(flowCoordinator: flowCoordinator)
        let presenter: SectionsListPresenting = SectionsListPresenter(coordinator: coordinator)
        let interactor = SectionsListInteractor(productKey: productKey, service: service, presenter: presenter, dependencies: container)
        let viewController = SectionsListViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
