import Foundation
import AnalyticsModule

protocol SectionsListInteracting: AnyObject {
    func loadSectionsForm()
    func didOpenSection(section: SectionItemForm)
    func didFinish()
    func didClose()
    func updateSectionWith(sectionFormUpdate: SectionFormUpdate, updatedFormSteps: FormStepsFieldValues)
    func submitIntegrationResult(_ action: SectionItemForm.Action, params: SectionItemForm.ActionParam, sectionInfo: SectionInfo)
}

final class SectionsListInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let service: SectionsListServicing
    private let presenter: SectionsListPresenting
    private let productKey: String
    private var sectionForm: SectionsForm?

    init(productKey: String,
         service: SectionsListServicing,
         presenter: SectionsListPresenting,
         dependencies: Dependencies) {
        self.productKey = productKey
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
    
    func hasPendingSections(_ sections: [SectionItemForm]) -> Bool {
        sections.contains(where: { $0.status != .complete })
    }
    
    func needToShowConfirmButton(_ sections: [SectionItemForm]) -> Bool {
        !hasPendingSections(sections)
    }
    
    func getCurrentIndex() -> Int? {
        guard let sections = sectionForm?.sections, sections.isNotEmpty else {
            return nil
        }
        
        return sections.firstIndex(where: { $0.status != .complete }) ?? (sections.count - 1)
    }
    
    func canOpen(section: SectionItemForm) -> Bool {
        guard
            let currentIndex = getCurrentIndex(),
            let sectionIndex = getSectionIndex(sectionKey: section.sectionKey) else {
            return false
        }
        return sectionIndex <= currentIndex
    }
    
    func getSectionIndex(sectionKey: String) -> Int? {
        sectionForm?.sections.firstIndex(where: { $0.sectionKey == sectionKey })
    }
}

// MARK: - SectionsListInteracting
extension SectionsListInteractor: SectionsListInteracting {
    func loadSectionsForm() {
        presenter.showLoading()
        service.sectionsFormWith(productKey: self.productKey) { [weak self] result in
            guard let self = self else {
                return
            }
            
            switch result {
            case .success(let sectionsForm):
                guard sectionsForm.isValid else {
                    self.presenter.showError(error: SectionFormErrors.invalidTypes)
                    self.presenter.showConfirmButton(show: false)
                    return
                }
                
                self.dependencies.analytics.log(COPEvent.accessed(productKey: self.productKey,
                                                                  version: sectionsForm.productTemplateVersion,
                                                                  status: .success))
                
                self.sectionForm = sectionsForm
                self.presenter.presentFormWith(sectionsForm: sectionsForm)
                self.presenter.showConfirmButton(show: self.needToShowConfirmButton(sectionsForm.sections))
            case .failure(let error):
                self.dependencies.analytics.log(COPEvent.accessed(productKey: self.productKey, status: .error))
                self.presenter.showError(error: error)
                self.presenter.showConfirmButton(show: false)
            }
        }
    }
    
    func didOpenSection(section: SectionItemForm) {
        guard let sectionForm = sectionForm, canOpen(section: section) else {
            return
        }
        
        let sectionInfo = SectionInfo(
            productKey: productKey,
            section: section,
            productTemplateVersion: sectionForm.productTemplateVersion,
            updatedFormSteps: [:]
        )
        
        if let action = sectionInfo.section.callToAction,
           let params = sectionInfo.section.actionParam {
            dependencies.analytics.log(COPEvent.openModuleSection(productKey: productKey,
                                                                  sectionKey: section.sectionKey,
                                                                  action: action))
            presenter.didNextStep(action: .openModuleSection(action: action,
                                                             params: params,
                                                             sectionInfo: sectionInfo))
            return
        }
        
        dependencies.analytics.log(COPEvent.openFormSection(productKey: productKey,
                                                            sectionKey: section.sectionKey))
        presenter.didNextStep(action: .openFormSection(sectionInfo: sectionInfo))
    }
    
    func didFinish() {
        presenter.showLoading()
        service.finish(productKey: self.productKey) { [weak self] result in
            guard let self = self else {
                return
            }
            switch result {
            case .success:
                self.dependencies.analytics.log(COPEvent.registered(productKey: self.productKey, status: .success))
                self.presenter.didNextStep(action: .finish)
            case .failure:
                self.dependencies.analytics.log(COPEvent.registered(productKey: self.productKey, status: .error))
                self.presenter.showPopupErrorFinish()
            }
        }
    }
    
    func didClose() {
        presenter.didNextStep(action: .close)
        dependencies.analytics.log(COPEvent.closed(productKey: productKey))
    }
    
    func updateSectionWith(sectionFormUpdate: SectionFormUpdate, updatedFormSteps: FormStepsFieldValues) {
        guard
            var sectionForm = sectionForm,
            let sectionIndex = getSectionIndex(sectionKey: sectionFormUpdate.sectionKey),
            var updatedSection = sectionForm.sections[safe: sectionIndex] else {
            return
        }
        
        updatedSection.updateWith(status: sectionFormUpdate.status, updatedFormSteps: updatedFormSteps)
        sectionForm.sections[sectionIndex] = updatedSection
        self.sectionForm = sectionForm
        presenter.presentFormWith(sectionsForm: sectionForm)
        presenter.showConfirmButton(show: needToShowConfirmButton(sectionForm.sections))
    }
    
    func submitIntegrationResult(_ action: SectionItemForm.Action, params: SectionItemForm.ActionParam, sectionInfo: SectionInfo) {
        guard let callToAction = sectionInfo.section.callToAction,
              let screenKey = callToAction.screenKey,
              let fieldKey = callToAction.fieldKey else {
            return
        }
        
        let fields: [FieldPayload] = [.init(fieldKey: fieldKey, value: .init(value: params.value))]
        
        let payload = SectionFormUpdatePayload(
            productTemplateVersion: sectionInfo.productTemplateVersion,
            productKey: sectionInfo.productKey,
            sectionKey: sectionInfo.section.sectionKey,
            formSteps: [FormStepPayload(screenKey: screenKey, fields: fields)]
        )
        
        presenter.showLoading()
        
        service.submitIntegration(payload) { [weak self] result in
            guard let self = self else {
                return
            }
            switch result {
            case .success(let sectionFormUpdate):
                self.dependencies.analytics.log(COPEvent.submitIntegrationResult(productKey: sectionInfo.productKey,
                                                                                 sectionKey: sectionInfo.section.sectionKey,
                                                                                 screenKey: screenKey,
                                                                                 status: .success))
                self.updateSectionWith(sectionFormUpdate: sectionFormUpdate, updatedFormSteps: [screenKey: fields])
            case .failure:
                self.dependencies.analytics.log(COPEvent.submitIntegrationResult(productKey: sectionInfo.productKey,
                                                                                 sectionKey: sectionInfo.section.sectionKey,
                                                                                 screenKey: screenKey,
                                                                                 status: .error))
                self.presenter.showPopuErrorSubmitIntegration(action, params: params, sectionInfo: sectionInfo)
            }
        }
    }
}
