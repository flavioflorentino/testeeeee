import Core
import UI
import SnapKit
import UIKit
import AssetsKit

private extension SectionsListViewController.Layout {
    enum Sizes {
        static let headerImage = 180
        static let divider = 1
    }
}

protocol SectionsListDisplay: AnyObject {
    func renderFormHeader(form: SectionsForm)
    func renderSections(sections: [SectionItemForm])
    func showLoading()
    func showError(_ statefulError: StatefulFeedbackViewModel)
    func showConfirmButton(show: Bool)
    func showPopupErrorFinish()
    func showPopuErrorSubmitIntegration(_ action: SectionItemForm.Action, params: SectionItemForm.ActionParam, sectionInfo: SectionInfo)
}

final class SectionsListViewController: ViewController<SectionsListInteracting, UIView> {
    fileprivate enum Layout {}
    
    // MARK: View Properties
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.register(SectionItemCell.self, forCellReuseIdentifier: SectionItemCell.identifier)
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        return tableView
    }()
    
    private var bottomViewTopConstraintSafeArea: Constraint?
    private var bottomViewTopConstraint: Constraint?
    
    private lazy var headerView = UIView()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.sectionFormHeader.image
        imageView.isHidden = true
        return imageView
    }()
    
    private lazy var closeButton: UIButton = {
        let button = UIButton()
        button.setImage(Resources.Icons.icoClose.image, for: .normal)
        button.addTarget(self, action: #selector(didTapCloseButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
            .with(\.textAlignment, .center)
            .with(\.textColor, Colors.black.color)
            .with(\.numberOfLines, 0)
        label.setContentHuggingPriority(UILayoutPriority.defaultHigh, for: .horizontal)
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.textAlignment, .center)
            .with(\.textColor, Colors.grayscale700.color)
            .with(\.numberOfLines, 0)
        label.setContentHuggingPriority(UILayoutPriority.defaultHigh, for: .horizontal)
        return label
    }()
    
    private lazy var pinnedContainer: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.backgroundPrimary.color
        return view
    }()
    
    private lazy var divider: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale100.color
        view.isHidden = true
        return view
    }()
    
    private lazy var stackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.spacing = .zero
        view.alignment = .center
        return view
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Strings.SectionsList.confirmButton, for: .normal)
        button.addTarget(self, action: #selector(didPressConfirm), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    
    // MARK: Data Source
    private lazy var dataSource: TableViewDataSource<Int, SectionItemForm> = {
        let dataSource = TableViewDataSource<Int, SectionItemForm>(view: tableView)
        dataSource.add(section: 0)
        dataSource.itemProvider = { tableView, indexPath, sectionItemForm -> UITableViewCell? in
            let cellIdentifier = SectionItemCell.identifier
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? SectionItemCell
            
            let sections = dataSource.data[0] ?? []
            cell?.configureWith(sectionItemForm, sectionIndex: indexPath.row, sections: sections)
            return cell
        }
        return dataSource
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.loadSectionsForm()
    }

    override func buildViewHierarchy() {
        view.addSubview(closeButton)
        headerView.addSubview(imageView)
        headerView.addSubview(titleLabel)
        headerView.addSubview(subtitleLabel)
        stackView.addArrangedSubview(divider)
        stackView.addArrangedSubview(confirmButton)
        stackView.setSpacing(Spacing.base02, after: divider)
        stackView.setSpacing(Spacing.base02, after: confirmButton)
        pinnedContainer.addSubview(stackView)
        view.addSubview(tableView)
        view.addSubview(pinnedContainer)
    }
    
    override func setupConstraints() {
        closeButton.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top).offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        imageView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base04)
            $0.centerX.equalToSuperview()
            $0.width.height.equalTo(Layout.Sizes.headerImage)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base03)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        subtitleLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base03)
        }
        
        tableView.snp.makeConstraints {
            $0.top.equalTo(closeButton.snp.bottom)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(pinnedContainer.snp.top)
        }
        
        pinnedContainer.snp.makeConstraints { [weak self] make in
            make.leading.trailing.equalToSuperview()
            self?.bottomViewTopConstraint = make.bottom.equalToSuperview().priority(.low).constraint
            self?.bottomViewTopConstraintSafeArea = make.bottom.equalTo(view.compatibleSafeArea.bottom).constraint
        }
        
        divider.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(Layout.Sizes.divider)
        }
        
        stackView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.bottom.trailing.equalToSuperview()
        }
        
        confirmButton.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02).priority(.required)
            $0.trailing.equalToSuperview().offset(-Spacing.base02).priority(.required)
        }
    }

    override func configureViews() {
        tableView.dataSource = dataSource
    }
    
    override func configureStyles() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func showPopupError(retryAction: @escaping () -> Void) {
        let popup = PopupViewController(
            title: Strings.SectionsList.genericErrorTitle,
            description: Strings.SectionsList.genericErrorMessage,
            preferredType: .image(Assets.icoAttention.image)
        )
        
        popup.addAction(
            .init(
                title: Strings.Default.retry,
                style: .fill,
                completion: {
                    retryAction()
                })
        )
        
        popup.addAction(
            .init(
                title: Strings.Default.notNow,
                style: .default,
                completion: { [weak self] in
                    self?.endState()
                })
        )
        
        popup.hideCloseButton = true
        popup.modalPresentationStyle = .custom
        popup.transitioningDelegate = self
        popup.definesPresentationContext = true
        self.present(popup, animated: true, completion: nil)
    }
}

// MARK: SectionsListDisplay
extension SectionsListViewController: SectionsListDisplay {
    func showPopupErrorFinish() {
        showPopupError { [weak self] in
            self?.interactor.didFinish()
        }
    }
    
    func showPopuErrorSubmitIntegration(_ action: SectionItemForm.Action, params: SectionItemForm.ActionParam, sectionInfo: SectionInfo) {
        showPopupError { [weak self] in
            self?.interactor.submitIntegrationResult(action, params: params, sectionInfo: sectionInfo)
        }
    }
    
    func showConfirmButton(show: Bool) {
        stackView.arrangedSubviews.forEach { $0.isHidden = !show }
        
        if show {
            bottomViewTopConstraintSafeArea?.activate()
            bottomViewTopConstraint?.deactivate()
        } else {
            bottomViewTopConstraintSafeArea?.deactivate()
            bottomViewTopConstraint?.activate()
        }
    }
    
    func showLoading() {
        beginState()
    }
    
    func showError(_ statefulError: StatefulFeedbackViewModel) {
        endState(model: statefulError)
    }
    
    func renderFormHeader(form: SectionsForm) {
        imageView.isHidden = false
        titleLabel.text = form.title
        subtitleLabel.text = form.subtitle
    }
    
    func renderSections(sections: [SectionItemForm]) {
        dataSource.update(items: sections, from: 0)
        endState()
    }
    
    func submitIntegrationResult(_ action: SectionItemForm.Action, params: SectionItemForm.ActionParam, sectionInfo: SectionInfo) {
        interactor.submitIntegrationResult(action, params: params, sectionInfo: sectionInfo)
    }
    
    func updateSection(_ sectionFormUpdate: SectionFormUpdate, updatedFormSteps: FormStepsFieldValues) {
        interactor.updateSectionWith(sectionFormUpdate: sectionFormUpdate, updatedFormSteps: updatedFormSteps)
    }
}

@objc
extension SectionsListViewController {
    private func didPressConfirm() {
        interactor.didFinish()
    }
    
    func didTapCloseButton(_ sender: UIButton) {
        interactor.didClose()
    }
}

extension SectionsListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let section = dataSource.data[0]?[safe: indexPath.row] else {
            return
        }
        interactor.didOpenSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        headerView.systemLayoutSizeFitting(tableView.frame.size).height
    }
}

// MARK: - StatefulTransitionViewing
extension SectionsListViewController: StatefulTransitionViewing {
    func statefulViewForError() -> StatefulViewing {
        StatefulFeedbackView()
    }

    func didTryAgain() {
        interactor.loadSectionsForm()
    }
    
    func didTapSecondaryButton() {
        interactor.didClose()
    }
}
