import UIKit

enum SingleSelectionFactory {
    static func make(field: FieldForm,
                     sectionInfo: SectionInfo,
                     screenKey: String,
                     didUpdate: @escaping (FieldForm) -> Void) -> SingleSelectionViewController {
        let container = DependencyContainer()
        let service: SingleSelectionServicing = SingleSelectionService(dependencies: container)
        let coordinator: SingleSelectionCoordinating = SingleSelectionCoordinator(didUpdate: didUpdate)
        let presenter: SingleSelectionPresenting = SingleSelectionPresenter(coordinator: coordinator)
        let interactor = SingleSelectionInteractor(fieldForm: field,
                                                   sectionInfo: sectionInfo,
                                                   screenKey: screenKey,
                                                   dependencies: container,
                                                   service: service,
                                                   presenter: presenter)
        let viewController = SingleSelectionViewController(interactor: interactor)
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
