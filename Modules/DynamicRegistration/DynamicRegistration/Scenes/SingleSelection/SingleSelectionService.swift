import Core
import Foundation

protocol SingleSelectionServicing {
    func getData(path: String, completion: @escaping CompletionSingleSelectionResult)
}

typealias CompletionSingleSelectionResult = (Result<[SingleSelectionItem], ApiError>) -> Void

final class SingleSelectionService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - SingleSelectionServicing
extension SingleSelectionService: SingleSelectionServicing {
    func getData(path: String, completion: @escaping CompletionSingleSelectionResult) {
        Api<[SingleSelectionItem]>(endpoint: RegisterEndpoint.singleSelection(path: path))
            .execute { [weak self] result in
                let mappedResult = result
                    .map(\.model)
                self?.dependencies.mainQueue.async {
                    completion(mappedResult)
                }
            }
    }
}
