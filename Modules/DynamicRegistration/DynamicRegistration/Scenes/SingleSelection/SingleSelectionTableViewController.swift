import UI
import UIKit

class SingleSelectionTableViewController: UITableViewController, StatefulTransitionViewing {
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
    }
    
    func configureViews() {
        tableView.register(SingleSelectionCell.self, forCellReuseIdentifier: SingleSelectionCell.identifier)
        tableView.separatorStyle = .singleLine
        tableView.tableFooterView = UIView()
        tableView.separatorInset = .zero
        tableView.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }
}
