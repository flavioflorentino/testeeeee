import Foundation
import AnalyticsModule

protocol SingleSelectionInteracting: AnyObject {
    func loadData()
    func viewDidLoad()
    func searchBy(_ query: String?)
    func close()
    func didSelect(singleSelectionItem: SingleSelectionItem)
}

final class SingleSelectionInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    private var fieldForm: FieldForm
    private var sectionInfo: SectionInfo
    private var screenKey: String
    private let service: SingleSelectionServicing
    private let presenter: SingleSelectionPresenting
    private var singleSelectionItems: [SingleSelectionItem] = []
    
    init(fieldForm: FieldForm,
         sectionInfo: SectionInfo,
         screenKey: String,
         dependencies: Dependencies,
         service: SingleSelectionServicing,
         presenter: SingleSelectionPresenting) {
        self.fieldForm = fieldForm
        self.sectionInfo = sectionInfo
        self.screenKey = screenKey
        self.dependencies = dependencies
        self.service = service
        self.presenter = presenter
    }
    
    func getNormalizedPath(path: String) -> String {
        let allFieldValues = sectionInfo
            .updatedFormSteps
            .flatMap { $0.value }
        
        var path = fieldForm.getPathWithValues(allFieldValues)
        
        if path.starts(with: "/") {
            path.removeFirst()
        }
        
        return path
    }
}

// MARK: - SingleSelectionInteracting
extension SingleSelectionInteractor: SingleSelectionInteracting {
    func close() {
        presenter.didNextStep(action: .close)
    }
    
    func viewDidLoad() {
        presenter.displayTitle(fieldForm.placeholder)
    }
    
    func searchBy(_ query: String?) {
        guard let query = query, query.isNotEmpty else {
            presenter.displayContent(singleSelectionItems)
            return
        }
        let normalizedQuery = query.uppercased().folding(options: .diacriticInsensitive, locale: .current)
        let searchedList = singleSelectionItems
            .filter { $0.name.uppercased().folding(options: .diacriticInsensitive, locale: .current).contains(normalizedQuery) }
        
        presenter.displayContent(searchedList)
    }
    
    func loadData() {
        guard let path = fieldForm.url else {
            presenter.showError()
            trackSingleSelectEvent(status: .error)
            return
        }
        presenter.showLoading()
        let normalizedPath = getNormalizedPath(path: path)
        service.getData(path: normalizedPath) { [weak self] result in
            guard let self = self else {
                return
            }
            switch result {
            case let .success(singleSelectionItems):
                self.trackSingleSelectEvent(status: .success)
                self.singleSelectionItems = singleSelectionItems
                self.presenter.hideLoading()
                self.presenter.displayContent(singleSelectionItems)
                self.presenter.setupSearchBar(singleSelectionItems.count)
            case .failure:
                self.presenter.showError()
                self.trackSingleSelectEvent(status: .error)
            }
        }
    }
    
    func didSelect(singleSelectionItem: SingleSelectionItem) {
        fieldForm.value = FieldValue(value: singleSelectionItem.name, id: singleSelectionItem.id)
        presenter.didNextStep(action: .didUpdate(fieldForm: fieldForm))
    }
}

private extension SingleSelectionInteractor {
    func trackSingleSelectEvent(status: COPEvent.Status) {
        dependencies.analytics.log(COPEvent.openField(productKey: sectionInfo.productKey,
                                                      sectionKey: sectionInfo.section.sectionKey,
                                                      screenKey: screenKey,
                                                      fieldKey: fieldForm.fieldKey,
                                                      fieldType: fieldForm.fieldType.rawValue,
                                                      status: status))
    }
}
