import UI
import UIKit
import AssetsKit
import SnapKit

protocol SingleSelectionDisplaying: AnyObject {
    func displayContent(_ singleSelectionItems: [SingleSelectionItem])
    func displayTitle(_ title: String)
    func showLoading()
    func hideLoading()
    func showSearchBar(_ show: Bool)
    func showError(_ statefulModel: StatefulFeedbackViewModel)
    func showNoResults(_ statefulModel: StatefulFeedbackViewModel)
}

private extension SingleSelectionViewController.Layout {
    enum Size {
        static let searchHeight: CGFloat = 36
        static let pushViewWidth: CGFloat = 38
    }
    enum Spacing {
        static let topSpacing: CGFloat = 135
        static let titleTopSpacing: CGFloat = 19
    }
    enum Radius {
        static let pushView: CGFloat = 2
        static let searchView: CGFloat = 40
    }
}

final class SingleSelectionViewController: ViewController<SingleSelectionInteracting, UIView> {
    fileprivate enum Layout { }
  
    private var searchHeightConstraint: Constraint?
    private lazy var contentViewController = UIViewController()
    
    private lazy var tableViewController: SingleSelectionTableViewController = {
        let tableViewController = SingleSelectionTableViewController()
        tableViewController.tableView.delegate = self
        
        return tableViewController
    }()
    
    private lazy var pushView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale100.color
        view.layer.cornerRadius = Layout.Radius.pushView
        return view
    }()
    
    private lazy var searchView: SearchComponentView = {
        let searchView = SearchComponentView(placeholder: Strings.SingleSelection.searchBarPlaceholder)
        searchView.isHidden = true
        view.layer.cornerRadius = Layout.Radius.searchView
        searchView.textField.border = .light(color: .grayscale200())
        searchView.textField.delegate = self
        searchView.textField.addTarget(
            self,
            action: #selector(textFieldValueChanged(_:)),
            for: UIControl.Event.editingChanged
        )
        return searchView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .small))
            .with(\.textAlignment, .center)
            .with(\.textColor, Colors.black.color)
        return label
    }()
    
    private lazy var closeButton: UIButton = {
        let button = UIButton()
        button.setImage(Resources.Icons.icoClose.image, for: .normal)
        button.addTarget(self, action: #selector(didTapCloseButton), for: .touchUpInside)
        return button
    }()
    
    // MARK: Data Source
    private lazy var dataSource: TableViewDataSource<Int, SingleSelectionItem> = {
        let dataSource = TableViewDataSource<Int, SingleSelectionItem>(view: tableViewController.tableView)
        dataSource.add(section: 0)
        dataSource.itemProvider = { tableView, indexPath, singleSelectionItem -> UITableViewCell? in
            let cellIdentifier = SingleSelectionCell.identifier
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? SingleSelectionCell
            
            cell?.configureWith(singleSelectionItem)
            return cell
        }
        return dataSource
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateSheetFrame()
        interactor.loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        preferredContentSize = preferredSize
    }
    
    override func buildViewHierarchy() {
        view.addSubview(pushView)
        view.addSubview(titleLabel)
        view.addSubview(closeButton)
        
        contentViewController.view.addSubview(searchView)
        contentViewController.view.addSubview(tableViewController.view)
        view.addSubview(contentViewController.view)
    }
    
    override func setupConstraints() {
        pushView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base01)
            $0.centerX.equalToSuperview()
            $0.height.equalTo(Sizing.base00)
            $0.width.equalTo(Layout.Size.pushViewWidth)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(pushView.snp.bottom).offset(Layout.Spacing.titleTopSpacing)
            $0.centerX.equalToSuperview()
            $0.trailing.lessThanOrEqualTo(closeButton.snp.leading)
        }
        
        closeButton.snp.makeConstraints {
            $0.centerY.equalTo(titleLabel.snp.centerY)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        contentViewController.view.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.greaterThanOrEqualTo(view.compatibleSafeArea.bottom)
        }
        
        setupStructureConstraints()
    }
    
    private func setupStructureConstraints() {
        searchView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.trailing.equalToSuperview()
            searchHeightConstraint = make.height.equalTo(Layout.Size.searchHeight).constraint
        }
        
        tableViewController.view.snp.makeConstraints {
            $0.top.equalTo(searchView.snp.bottom).offset(Spacing.base00)
            $0.leading.trailing.equalToSuperview()
        }
        
        let bottomConstraint = BottomConstraintUpKeyboard(item: tableViewController.view, toItem: contentViewController.view)
        contentViewController.view.addConstraint(bottomConstraint)
    }
    
    override func configureViews() {
        tableViewController.tableView.dataSource = dataSource
        showSearchBar(false)
    }
    
    override func configureStyles() {
        view.backgroundColor = Colors.backgroundPrimary.color
        view.cornerRadius = .strong
    }
    
    var preferredSize: CGSize {
        CGSize(width: view.frame.width, height: maxFrameHeight)
    }

    var maxFrameHeight: CGFloat {
        UIScreen.main.bounds.height - Layout.Spacing.topSpacing
    }
    
    private func updateSheetFrame() {
        preferredContentSize = preferredSize
        let origin = CGPoint(x: 0, y: UIScreen.main.bounds.height - preferredContentSize.height)
        view.frame = CGRect(origin: origin, size: preferredContentSize)
    }
    
    @objc
    func didTapCloseButton(_ sender: UIButton) {
        interactor.close()
    }
}

// MARK: - SingleSelectionDisplaying
extension SingleSelectionViewController: SingleSelectionDisplaying {
    func displayContent(_ singleSelectionItems: [SingleSelectionItem]) {
        dataSource.update(items: singleSelectionItems, from: 0)
        tableViewController.tableView.reloadData()
        tableViewController.endState()
    }
    
    func showSearchBar(_ show: Bool) {
        searchHeightConstraint?.layoutConstraints.first?.constant = show ? Layout.Size.searchHeight : 0
        searchView.isHidden = !show
    }
    
    func displayTitle(_ title: String) {
        titleLabel.text = title
    }
    
    func hideLoading() {
        tableViewController.endState(animated: false)
    }
    
    func showLoading() {
        tableViewController.beginState()
    }
    
    func showError(_ statefulModel: StatefulFeedbackViewModel) {
        tableViewController.endState(model: statefulModel)
    }
    func showNoResults(_ statefulModel: StatefulFeedbackViewModel) {
        tableViewController.endState(model: statefulModel)
    }
}

extension SingleSelectionViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let singleSelectionItem = dataSource.data[0]?[safe: indexPath.row] else {
            return
        }
        interactor.didSelect(singleSelectionItem: singleSelectionItem)
    }
}

@objc
extension SingleSelectionViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
    }
    
    private func textFieldValueChanged(_ sender: UIPPFloatingTextField) {
        interactor.searchBy(sender.text)
    }
}
