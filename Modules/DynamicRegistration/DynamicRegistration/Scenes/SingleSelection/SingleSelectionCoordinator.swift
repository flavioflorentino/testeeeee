import UIKit

enum SingleSelectionAction {
    case close
    case didUpdate(fieldForm: FieldForm)
}

protocol SingleSelectionCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var didUpdate: (FieldForm) -> Void { get set }
    func perform(action: SingleSelectionAction)
}

final class SingleSelectionCoordinator {
    var didUpdate: (FieldForm) -> Void

    weak var viewController: UIViewController?

    init(didUpdate: @escaping (FieldForm) -> Void) {
        self.didUpdate = didUpdate
    }
}

// MARK: - SingleSelectionCoordinating
extension SingleSelectionCoordinator: SingleSelectionCoordinating {
    func perform(action: SingleSelectionAction) {
        switch action {
        case .close:
            viewController?.dismiss(animated: true)
        case let .didUpdate(fieldForm):
            didUpdate(fieldForm)
            viewController?.dismiss(animated: true)
        }
    }
}
