import SnapKit
import UI
import UIKit

final class SingleSelectionCell: UITableViewCell {
    private(set) lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.textAlignment, .left)
            .with(\.textColor, Colors.black.color)
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureWith(_ singleSelectionItem: SingleSelectionItem) {
        titleLabel.text = singleSelectionItem.name
    }
}

// MARK: ViewConfiguration
extension SingleSelectionCell: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(titleLabel)
    }
    
    func configureViews() {
        selectionStyle = .none
        backgroundColor = .clear
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.bottom.leading.equalToSuperview().inset(Spacing.base02)
        }
    }
}
