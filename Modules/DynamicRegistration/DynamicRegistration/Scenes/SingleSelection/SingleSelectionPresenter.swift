import Foundation
import UI
import AssetsKit

protocol SingleSelectionPresenting: AnyObject {
    var viewController: SingleSelectionDisplaying? { get set }
    func displayContent(_ singleSelectionItems: [SingleSelectionItem])
    func displayTitle(_ title: String?)
    func didNextStep(action: SingleSelectionAction)
    func setupSearchBar(_ totalItemsCount: Int)
    func showLoading()
    func hideLoading()
    func showError()
    func showNoResults()
}

final class SingleSelectionPresenter {
    private let coordinator: SingleSelectionCoordinating
    weak var viewController: SingleSelectionDisplaying?

    init(coordinator: SingleSelectionCoordinating) {
        self.coordinator = coordinator
    }
    
    func isNeedShowSearch(itemsCount: Int) -> Bool {
        itemsCount >= 8
    }
}

// MARK: - SingleSelectionPresenting
extension SingleSelectionPresenter: SingleSelectionPresenting {
    func displayContent(_ singleSelectionItems: [SingleSelectionItem]) {
        if singleSelectionItems.isEmpty {
            showNoResults()
        } else {
            viewController?.displayContent(singleSelectionItems)
        }
    }
    
    func displayTitle(_ title: String?) {
        viewController?.displayTitle(title ?? "")
    }
    
    func setupSearchBar(_ totalItemsCount: Int) {
        viewController?.showSearchBar(isNeedShowSearch(itemsCount: totalItemsCount))
    }
    
    func didNextStep(action: SingleSelectionAction) {
        coordinator.perform(action: action)
    }
    
    func showLoading() {
        viewController?.showLoading()
    }
    
    func hideLoading() {
        viewController?.hideLoading()
    }
    
    func showError() {
        let statefulError = StatefulFeedbackViewModel(
            image: Assets.icoAttention.image,
            content: (
                title: nil,
                description: Strings.SingleSelection.genericErrorMessage
            ),
            button: nil,
            secondaryButton: nil
        )
        viewController?.showError(statefulError)
    }
    
    func showNoResults() {
        let statefulError = StatefulFeedbackViewModel(
            image: Resources.Illustrations.iluNotFoundCircledBackground.image,
            content: (
                title: nil,
                description: Strings.SingleSelection.noResults
        ),
            button: nil,
            secondaryButton: nil
        )
        viewController?.showNoResults(statefulError)
    }
}
