import Foundation
import UIKit
import UI
import IdentityValidation
import Core

typealias SectionInfo = (productKey: String, section: SectionItemForm, productTemplateVersion: String, updatedFormSteps: FormStepsFieldValues)

enum RegistrationFlowControlAction {
    case didOpenFormSection(section: SectionInfo)
    case didOpenModuleSection(action: SectionItemForm.Action, params: SectionItemForm.ActionParam, sectionInfo: SectionInfo)
    case didNextFormStep(formStep: FormStep, sectionInfo: SectionInfo)
    case close
    case finish
    case updateSection(_ sectionFormUpdate: SectionFormUpdate, sectionInfo: SectionInfo)
}

protocol RegistrationFlowControlCoordinating: AnyObject {
    func perform(action: RegistrationFlowControlAction)
}

public final class RegistrationFlowControlCoordinator: Coordinating, RegistrationFlowControlCoordinating {
    public var childViewController: [UIViewController] = []
    public var viewController: UIViewController?
    
    private let productKey: String
    private let callbackUrl: URL?
    private let rootNavigationController: UINavigationController
    private let navigationController: UINavigationController
    private var currentSection: SectionItemForm?
    private var currentFormStep: FormStep?
    private var identityValidationCoordinator: IDValidationFlowCoordinator?
    private let dependencies: HasKeychainManager = DependencyContainer()
    
    public init(
        productKey: String,
        rootNavigation: UINavigationController,
        navigation: UINavigationController = UINavigationController(),
        callbackUrl: URL? = nil
    ) {
        self.productKey = productKey
        self.callbackUrl = callbackUrl
        self.rootNavigationController = rootNavigation
        self.navigationController = navigation
    }
    
    func openCallToAction(_ action: SectionItemForm.Action, params: SectionItemForm.ActionParam, sectionInfo: SectionInfo) {
        guard let viewController = navigationController.topViewController,
              let token = dependencies.keychain.getData(key: KeychainKey.token),
              action == .startBiometryModule || action == .startDocumentoscopyModule || action == .startValidationIdentity
        else { return }

        identityValidationCoordinator = IDValidationFlowCoordinator(
            from: viewController,
            token: token,
            flow: params.value,
            completion: { [weak self] status in
                guard let status = status else {
                    return
                }
                
                self?.updateSection(action, params: .init(key: "flow", value: status.rawValue), sectionInfo: sectionInfo)
            }
        )
        
        identityValidationCoordinator?.start()
    }
    
    func perform(action: RegistrationFlowControlAction) {
        switch action {
        case let .didOpenFormSection(sectionInfo):
            self.currentSection = sectionInfo.section
            
            guard let formStep = sectionInfo.section.formSteps?.first else {
                return
            }
            currentFormStep = formStep
            openCurrentFormStep(sectionInfo: sectionInfo)
        case let .didOpenModuleSection(action, params, sectionInfo):
            self.currentSection = sectionInfo.section
            openCallToAction(action, params: params, sectionInfo: sectionInfo)
        case .finish:
            navigationController.dismiss(animated: true) { [weak self] in
                guard let callbackUrl = self?.callbackUrl else {
                    return
                }
                COPSetup.deeplinkInstance?.open(url: callbackUrl)
            }
        case .close:
            navigationController.dismiss(animated: true)
        case let .didNextFormStep(formStep, sectionInfo):
            currentSection = sectionInfo.section
            guard let nextFormStep = getNextFormStepFor(formStep) else {
                return
            }
            
            currentFormStep = nextFormStep
            openCurrentFormStep(sectionInfo: sectionInfo)
        case let .updateSection(sectionFormUpdate, sectionInfo):
            navigationController.popToRootViewController(animated: false)
            guard let viewController = navigationController.viewControllers.first as? SectionsListViewController else {
                return
            }
            viewController.updateSection(sectionFormUpdate, updatedFormSteps: sectionInfo.updatedFormSteps)
        }
    }
    
    func openCurrentFormStep(sectionInfo: SectionInfo) {
        guard let formStep = currentFormStep else {
            return
        }
        
        let viewController = FormStepFactory.make(
            formStep: formStep,
            sectionInfo: sectionInfo,
            flowCoordinator: self
        )
        navigationController.pushViewController(viewController, animated: true)
    }
    
    func getNextFormStepFor(_ formStep: FormStep) -> FormStep? {
        guard let currentIndex = currentSection?.formSteps?.firstIndex(where: { $0.screenKey == formStep.screenKey }),
              let nextFormStep = currentSection?.formSteps?[safe: currentIndex + 1] else {
            return nil
        }
        
        return nextFormStep
    }
    
    func updateSection(_ action: SectionItemForm.Action, params: SectionItemForm.ActionParam, sectionInfo: SectionInfo) {
        guard let viewController = navigationController.viewControllers.first as? SectionsListViewController else {
            return
        }
        viewController.submitIntegrationResult(action, params: params, sectionInfo: sectionInfo)
    }
    
    public func start() {
        let sectionViewController = SectionsListFactory.make(productKey: productKey, flowCoordinator: self)
        navigationController.viewControllers = [sectionViewController]
        rootNavigationController.present(navigationController, animated: true)
    }
}
