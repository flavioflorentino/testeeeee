import Core
import Foundation

protocol FormStepServicing {
    func updateSection(sectionInfo: SectionInfo, completion: @escaping CompletionUpdateSectionResult)
}

typealias CompletionUpdateSectionResult = (Result<SectionFormUpdate, ApiError>) -> Void

final class FormStepService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - SectionsListServicing
extension FormStepService: FormStepServicing {
    func updateSection(sectionInfo: SectionInfo, completion: @escaping CompletionUpdateSectionResult) {
        let formSteps = sectionInfo.updatedFormSteps
            .map { FormStepPayload(screenKey: $0.key, fields: $0.value) }
        let payload = SectionFormUpdatePayload(
            productTemplateVersion: sectionInfo.productTemplateVersion,
            productKey: sectionInfo.productKey,
            sectionKey: sectionInfo.section.sectionKey,
            formSteps: formSteps
        )
        
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        Api<SectionFormUpdate>(endpoint: RegisterEndpoint.updateSection(payload))
            .execute(jsonDecoder: decoder) { [weak self] result in
                let mappedResult = result
                    .map(\.model)
                self?.dependencies.mainQueue.async {
                    completion(mappedResult)
                }
            }
    }
}
