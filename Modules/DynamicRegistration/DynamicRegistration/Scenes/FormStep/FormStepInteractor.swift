import Foundation
import Core
import AnalyticsModule

typealias SingleSelectionParameters = [String: FieldValue]

protocol FormStepInteracting: AnyObject {
    func setupInitialForm()
    func didConfirm(newValues: [FieldPayload])
    func didTapField(field: FieldForm)
    func didUpdateField(_ fieldForm: FieldForm)
    func validateFields(fields: [FieldValidate])
    func didUpdateFieldValues(_ payload: [FieldPayload])
}

final class FormStepInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    private let service: FormStepServicing
    private let presenter: FormStepPresenting
    private var formStep: FormStep
    private var sectionInfo: SectionInfo
    
    init(dependencies: Dependencies,
         presenter: FormStepPresenting,
         formStep: FormStep,
         sectionInfo: SectionInfo,
         service: FormStepServicing) {
        self.dependencies = dependencies
        self.presenter = presenter
        self.formStep = formStep
        self.sectionInfo = sectionInfo
        self.service = service
    }
    
    func needUpdateSection(_ formStep: FormStep) -> Bool {
        sectionInfo.section.formSteps?.last?.screenKey == formStep.screenKey
    }
    
    func getChangedPathFields() -> [FieldForm] {
        let singleSelectionFields = formStep.fields.filter({ $0.fieldType == .singleSelection })
        var changedPathFields = [FieldForm]()
        singleSelectionFields.forEach { singleSelectionField in
            let oldUrlPath = singleSelectionField.getPathWithValues(getCurrentFieldValues())
            let newUrlPath = singleSelectionField.getPathWithValues(getUpdatedFieldValues())
            if oldUrlPath != newUrlPath {
                changedPathFields.append(singleSelectionField)
            }
        }
        
        return changedPathFields
    }
    
    func getCurrentFieldValues() -> [FieldPayload] {
        sectionInfo
            .section
            .formSteps?
            .flatMap { $0.fields }
            .map { FieldPayload(fieldKey: $0.fieldKey, value: $0.value ) } ?? []
    }
    
    func getUpdatedFieldValues() -> [FieldPayload] {
        sectionInfo
            .updatedFormSteps
            .flatMap { $0.value }
    }
    
    func getValidFields() -> [FieldForm] {
        var fields = formStep.fields
        let changedFieldParameters = getChangedPathFields()
        for (index, field) in fields.enumerated() {
            if changedFieldParameters.contains(where: { $0.fieldKey == field.fieldKey }) {
                fields[index].value = nil
            }
        }
        return fields
    }
}

// MARK: - FormStepInteracting
extension FormStepInteractor: FormStepInteracting {
    func didTapField(field: FieldForm) {
        presenter.didTapField(field: field, screenKey: formStep.screenKey)
    }
    
    func didUpdateField(_ fieldForm: FieldForm) {
        guard let index = formStep.fields.firstIndex(where: { $0.fieldKey == fieldForm.fieldKey }) else {
            return
        }
        
        formStep.fields[index] = fieldForm
        presenter.didUpdateField(fieldForm)
    }
    
    func validateFields(fields: [FieldValidate]) {
        let hasInvalidField = fields.contains { !$0.isValid }
        presenter.updateFormIsValid(!hasInvalidField)
    }
    
    func didConfirm(newValues: [FieldPayload]) {
        presenter.hideKeyboard()
        sectionInfo.updatedFormSteps[formStep.screenKey] = newValues
        
        guard needUpdateSection(formStep) else {
            presenter.didNextStep(action: .nextFormStep(formStep: formStep, sectionInfo: sectionInfo))
            dependencies.analytics.log(COPEvent.nextFormStep(productKey: sectionInfo.productKey,
                                                             sectionKey: sectionInfo.section.sectionKey,
                                                             screenKey: formStep.screenKey))
            return
        }
        
        presenter.showLoading()
        
        service.updateSection(sectionInfo: sectionInfo) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case let .success(sectionFormUpdate):
                self.presenter.didNextStep(action: .updateSection(sectionFormUpdate, sectionInfo: self.sectionInfo))
                self.dependencies.analytics.log(COPEvent.submitFormSection(productKey: self.sectionInfo.productKey,
                                                                           sectionKey: self.sectionInfo.section.sectionKey,
                                                                           screenKey: self.formStep.screenKey,
                                                                           status: .success))
            case .failure:
                self.presenter.showPopupError()
                self.dependencies.analytics.log(COPEvent.submitFormSection(productKey: self.sectionInfo.productKey,
                                                                           sectionKey: self.sectionInfo.section.sectionKey,
                                                                           screenKey: self.formStep.screenKey,
                                                                           status: .error))
            }
        }
    }
    
    func setupInitialForm() {
        let fields = getValidFields()
        presenter.displayFormStep(formStep: formStep,
                                  fields: fields,
                                  section: sectionInfo.section,
                                  productKey: sectionInfo.productKey,
                                  needUpdateSection: needUpdateSection(formStep))
    }
    
    func didUpdateFieldValues(_ payload: [FieldPayload]) {
        var formSteps = sectionInfo
                   .section
                   .formSteps ?? []
        for index in formSteps.indices {
            formSteps[index].updateFieldValues(payload)
        }
        sectionInfo.section.formSteps = formSteps
    }
}
