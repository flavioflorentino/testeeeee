import Foundation
import UI

protocol FormStepPresenting: AnyObject {
    var viewController: FormStepDisplay? { get set }
    func displayFormStep(formStep: FormStep,
                         fields: [FieldForm],
                         section: SectionItemForm,
                         productKey: String,
                         needUpdateSection: Bool)
    func didTapField(field: FieldForm, screenKey: String)
    func didUpdateField(_ fieldForm: FieldForm)
    func didNextStep(action: FormStepAction)
    func updateFormIsValid(_ isValid: Bool)
    func showLoading()
    func showPopupError()
    func hideKeyboard()
}

final class FormStepPresenter {
    private let coordinator: FormStepCoordinating
    weak var viewController: FormStepDisplay?

    init(coordinator: FormStepCoordinating) {
        self.coordinator = coordinator
    }
    
    func getCurrentStepPosition(formStep: FormStep, section: SectionItemForm) -> Int? {
        guard let currentIndex = section.formSteps?.firstIndex(where: { $0.screenKey == formStep.screenKey }) else {
            return nil
        }
        
        return currentIndex + 1
    }
}

// MARK: - FormStepPresenting
extension FormStepPresenter: FormStepPresenting {
    func didTapField(field: FieldForm, screenKey: String) {
        viewController?.didTapField(field: field, screenKey: screenKey)
    }
    
    func didUpdateField(_ fieldForm: FieldForm) {
        viewController?.didUpdateField(fieldForm)
    }
    
    func hideKeyboard() {
        viewController?.hideKeyboard()
    }
    
    func displayFormStep(formStep: FormStep,
                         fields: [FieldForm],
                         section: SectionItemForm,
                         productKey: String,
                         needUpdateSection: Bool) {
        viewController?.updateSectionTitle(sectionTitle: section.title)
        viewController?.updateTitle(title: formStep.title)
        viewController?.updateSubtitle(subtitle: formStep.subtitle)
        if let currentStepPosition = getCurrentStepPosition(formStep: formStep, section: section) {
            let total = section.formSteps?.count ?? 0
            viewController?.updateTotalSteps(steps: "\(currentStepPosition)/\(total)")
            let progress = Float(currentStepPosition) / Float(total)
            viewController?.updateProgress(value: progress)
        }
        
        let confirmButtonTitle = needUpdateSection ? Strings.FormStep.confirmButton : Strings.FormStep.continueButton
        viewController?.setupConfirmButtonTitle(confirmButtonTitle)
        let analyticsKeys = FieldAnalyticsKeys(productKey: productKey,
                                               sectionKey: section.sectionKey,
                                               screenKey: formStep.screenKey)
        let fieldViews = fields.compactMap { FieldFactory.make(field: $0, analyticsKeys: analyticsKeys) }
        viewController?.renderFieldViews(fieldViews: fieldViews)
    }
    
    func didNextStep(action: FormStepAction) {
        coordinator.perform(action: action)
    }
    
    func updateFormIsValid(_ isValid: Bool) {
        viewController?.enableConfirmButton(isValid)
    }
    
    func showLoading() {
        viewController?.showLoading()
    }
    
    func showPopupError() {
        viewController?.showPopupError()
    }
}
