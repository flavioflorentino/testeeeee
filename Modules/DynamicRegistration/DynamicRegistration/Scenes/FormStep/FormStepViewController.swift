import UI
import UIKit

protocol FormStepDisplay: AnyObject {
    func updateSectionTitle(sectionTitle: String)
    func updateTitle(title: String)
    func updateSubtitle(subtitle: String)
    func updateProgress(value: Float)
    func updateTotalSteps(steps: String)
    func renderFieldViews(fieldViews: [FieldView])
    func enableConfirmButton(_ enable: Bool)
    func setupConfirmButtonTitle(_ title: String)
    func didTapField(field: FieldForm, screenKey: String)
    func didUpdateField(_ fieldForm: FieldForm)
    func showLoading()
    func showPopupError()
    func hideKeyboard()
}

private extension FormStepViewController.Layout {
    enum Spacing {
        static let backIcon: CGFloat = 17
    }
    
    enum Style {
        static let alphaMask: CGFloat = 0.56
    }
}

final class FormStepViewController: ViewController<FormStepInteracting, UIView>, StatefulTransitionViewing {
    fileprivate enum Layout {}
    // swiftlint:disable weak_delegate
    private var transitionDelegate: BottomSheetTransitioningDelegate?
    private let sectionInfo: SectionInfo
    
    private lazy var backButton: UIButton = {
        let button = UIButton()
        button.setImage(Assets.arrowLeft.image, for: .normal)
        button.addTarget(self, action: #selector(didTapBackButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var sectionTitle: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, Colors.black.color)
        return label
    }()
    
    private lazy var scrollView = UIScrollView()
    
    private lazy var progressBarTitleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.grayscale400.color)
            .with(\.text, Strings.FormStep.progressLabel)
        return label
    }()
    
    private lazy var progressBarStepsLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.grayscale400.color)
        return label
    }()
    
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activity = UIActivityIndicatorView(style: .white)
        activity.color = Colors.brandingBase.color
        return activity
    }()
    
    private lazy var containerLoading: UIView = {
       let container = UIView()
        container.backgroundColor = UIColor.white
        container.cornerRadius = .strong
        return container
    }()
    
    private lazy var containerMaskLoading: UIView = {
       let mask = UIView()
        mask.backgroundColor = UIColor.black.withAlphaComponent(Layout.Style.alphaMask)
        mask.isHidden = true
        return mask
    }()
    
    init(sectionInfo: SectionInfo, interactor: FormStepInteracting) {
        self.sectionInfo = sectionInfo
        super.init(interactor: interactor)
    }
    
    private lazy var progressBarView: UIProgressView = {
        let progressBar = UIProgressView()
        progressBar.progressTintColor = Colors.success600.color
        progressBar.layer.cornerRadius = CornerRadius.light
        progressBar.clipsToBounds = true
        progressBar.layer.sublayers?[safe: 1]?.cornerRadius = CornerRadius.light
        progressBar.subviews[safe: 1]?.clipsToBounds = true
        return progressBar
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textColor, Colors.black.color)
            .with(\.numberOfLines, 0)
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale700.color)
            .with(\.numberOfLines, 0)
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.spacing = Spacing.base00
        view.alignment = .fill
        return view
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Strings.FormStep.confirmButton, for: .normal)
        button.addTarget(self, action: #selector(didPressConfirm), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.setupInitialForm()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(backButton)
        view.addSubview(sectionTitle)
        view.addSubview(scrollView)
        view.addSubview(containerMaskLoading)
        containerMaskLoading.addSubview(containerLoading)
        containerLoading.addSubview(activityIndicator)
        scrollView.addSubview(progressBarTitleLabel)
        scrollView.addSubview(progressBarStepsLabel)
        scrollView.addSubview(progressBarView)
        scrollView.addSubview(titleLabel)
        scrollView.addSubview(subtitleLabel)
        scrollView.addSubview(stackView)
        view.addSubview(confirmButton)
    }
    
    override func setupConstraints() {
        backButton.snp.makeConstraints {
            $0.top.leading.equalToSuperview().offset(Layout.Spacing.backIcon)
        }
        
        sectionTitle.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.centerY.equalTo(backButton.snp.centerY)
        }
        
        scrollView.snp.makeConstraints {
            $0.top.equalTo(backButton.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(confirmButton.snp.top).offset(-Spacing.base00)
        }
        
        progressBarTitleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.leading.equalTo(progressBarView.snp.leading)
        }
        
        progressBarStepsLabel.snp.makeConstraints {
            $0.centerY.equalTo(progressBarTitleLabel.snp.centerY)
            $0.trailing.equalTo(progressBarView.snp.trailing)
        }
        
        progressBarView.snp.makeConstraints {
            $0.top.equalTo(progressBarTitleLabel.snp.bottom).offset(Spacing.base00)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.centerX.equalToSuperview()
            $0.height.equalTo(Sizing.base01)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(progressBarView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        subtitleLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        stackView.snp.makeConstraints {
            $0.top.equalTo(subtitleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.bottom.equalToSuperview()
        }
        
        confirmButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom)
        }
        
        containerMaskLoading.snp.makeConstraints {
            $0.width.height.centerY.centerX.equalToSuperview()
        }
        
        containerLoading.snp.makeConstraints {
            $0.width.height.equalTo(Sizing.base09)
            $0.centerY.centerX.equalToSuperview()
        }
        
        activityIndicator.snp.makeConstraints {
            $0.centerX.centerY.equalToSuperview()
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    func didUpdateContent() {
        let fields = stackView.arrangedSubviews.compactMap { $0 as? FieldValidate }
        interactor.validateFields(fields: fields)
    }
}

// MARK: FormStepDisplay
extension FormStepViewController: FormStepDisplay {
    func hideKeyboard() {
        view.endEditing(true)
    }
    
    func updateProgress(value: Float) {
        progressBarView.progress = value
    }
    
    func updateTotalSteps(steps: String) {
        progressBarStepsLabel.text = steps
    }
    
    func updateSectionTitle(sectionTitle title: String) {
        sectionTitle.text = title
    }
    
    func updateSubtitle(subtitle: String) {
        subtitleLabel.text = subtitle
    }
    
    func updateTitle(title: String) {
        titleLabel.text = title
    }
    
    func renderFieldViews(fieldViews: [FieldView]) {
        stackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
        fieldViews.forEach {
            stackView.addArrangedSubview($0)
            var fieldView = $0
            fieldView.valueChanged = { [weak self] in
                self?.didUpdateContent()
            }
            fieldView.didTap = { [weak self] field in
                self?.interactor.didTapField(field: field)
            }
            fieldView.didUpdateFieldValues = { [weak self] fieldValues in
                self?.interactor.didUpdateFieldValues(fieldValues)
            }
            fieldView.showLoading = { [weak self] showLoading in
                showLoading ? self?.startLoading() : self?.stopLoading()
            }
        }
        
        didUpdateContent()
    }
    
    func enableConfirmButton(_ enable: Bool) {
        confirmButton.isEnabled = enable
    }
    
    func setupConfirmButtonTitle(_ title: String) {
        confirmButton.setTitle(title, for: .normal)
    }
    
    func showLoading() {
        beginState()
    }
    
    func showPopupError() {
        endState()
        let popup = PopupViewController(
            title: Strings.SectionsList.genericErrorTitle,
            description: Strings.SectionsList.genericErrorMessage,
            preferredType: .image(Assets.icoAttention.image)
        )
        
        popup.addAction(
            .init(
                title: Strings.Default.retry,
                style: .fill,
                completion: { [weak self] in
                    self?.didPressConfirm()
                })
        )
        
        popup.addAction(
            .init(
                title: Strings.Default.notNow,
                style: .default
              )
        )
        
        popup.hideCloseButton = true
        popup.modalPresentationStyle = .custom
        popup.transitioningDelegate = self
        popup.definesPresentationContext = true
        present(popup, animated: true, completion: nil)
    }
    
    func didTapField(field: FieldForm, screenKey: String) {
        guard field.fieldType == .singleSelection else {
            return
        }
        
        let controller = SingleSelectionFactory.make(
            field: field,
            sectionInfo: sectionInfo,
            screenKey: screenKey,
            didUpdate: { [weak self] fieldForm in
                self?.interactor.didUpdateField(fieldForm)
            }
        )
        transitionDelegate = BottomSheetTransitioningDelegate()
        controller.modalPresentationStyle = .custom
        controller.transitioningDelegate = transitionDelegate
        present(controller, animated: true)
    }
    
    func didUpdateField(_ fieldForm: FieldForm) {
        let fieldView = stackView
            .arrangedSubviews
            .compactMap { $0 as? FieldView }
            .first { $0.value.fieldKey == fieldForm.fieldKey }
        
        fieldView?.updateContent(fieldForm)
    }
    
    func startLoading() {
        containerMaskLoading.isHidden = false
        activityIndicator.startAnimating()
    }
    
    func stopLoading() {
        containerMaskLoading.isHidden = true
        activityIndicator.stopAnimating()
    }
}

@objc
extension FormStepViewController {
    private func didPressConfirm() {
        let fieldValues = stackView.arrangedSubviews
            .compactMap { $0 as? FieldValidate }
            .map(\.value)
        
        interactor.didConfirm(newValues: fieldValues)
    }
    
    func didTapBackButton(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}
