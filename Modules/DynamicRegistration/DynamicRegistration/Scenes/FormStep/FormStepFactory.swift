import Foundation
import UIKit

enum FormStepFactory {
    static func make(
        formStep: FormStep,
        sectionInfo: SectionInfo,
        flowCoordinator: RegistrationFlowControlCoordinating
    ) -> FormStepViewController {
        let container = DependencyContainer()
        let coordinator: FormStepCoordinating = FormStepCoordinator(flowCoordinator: flowCoordinator)
        let service: FormStepServicing = FormStepService(dependencies: container)
        let presenter: FormStepPresenting = FormStepPresenter(coordinator: coordinator)
        let interactor = FormStepInteractor(
            dependencies: container,
            presenter: presenter,
            formStep: formStep,
            sectionInfo: sectionInfo,
            service: service
        )
        let viewController = FormStepViewController(
            sectionInfo: sectionInfo,
            interactor: interactor
        )

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
