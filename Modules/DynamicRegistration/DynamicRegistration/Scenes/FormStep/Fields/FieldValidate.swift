import UI

typealias FieldAnalyticsKeys = (productKey: String, sectionKey: String, screenKey: String)

protocol FieldValidate {
    init?(field: FieldForm, analyticsKeys: FieldAnalyticsKeys)
    var isValid: Bool { get }
    var valueChanged: () -> Void { get set }
    var didTap: (FieldForm) -> Void { get set }
    var didUpdateFieldValues: ([FieldPayload]) -> Void { get set }
    var showLoading: (Bool) -> Void { get set }
    var value: FieldPayload { get }
    func updateContent(_ fieldForm: FieldForm)
}

extension FieldValidate {
    func validateIfIsNotEmpty(textField: UIPPFloatingTextField) {
        guard let value = textField.text, value.isNotEmpty else {
            return
        }
        validate(textField: textField)
    }
    
    func validate(textField: UIPPFloatingTextField) {
        let result = textField.validate()
        switch result {
        case let .invalid(errors):
            guard let error = errors.first else {
                textField.errorMessage = nil
                return
            }
            textField.errorMessage = String(describing: error)
        case .valid:
            textField.errorMessage = nil
        }
        
        valueChanged()
    }
    
    func updateContent(_ fieldForm: FieldForm) {}
}
