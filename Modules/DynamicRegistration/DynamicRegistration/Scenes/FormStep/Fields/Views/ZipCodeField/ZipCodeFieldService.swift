import Core
import Foundation
import Address

protocol ZipCodeFieldServicing {
    func addressBy(_ zipCode: String, then completion: @escaping (Swift.Result<Address, ApiError>) -> Void) -> URLSessionTask?
}

final class ZipCodeFieldService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - ZipCodeFieldServicing
extension ZipCodeFieldService: ZipCodeFieldServicing {
    func addressBy(_ zipCode: String, then completion: @escaping (Swift.Result<Address, ApiError>) -> Void) -> URLSessionTask? {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        let api = Api<Address>(endpoint: AddressEndpoint.searchBy(cep: zipCode))
        
        return api.execute(jsonDecoder: decoder) { result in
            self.dependencies.mainQueue.async {
                completion(result.map { $0.model })
            }
        }
    }
}
