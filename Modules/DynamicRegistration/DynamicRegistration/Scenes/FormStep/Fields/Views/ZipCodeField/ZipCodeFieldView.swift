import UI
import SnapKit
import Validator
import Core

protocol ZipCodeFieldDisplaying: AnyObject {
    func showLoading(show: Bool)
    func enableField(_ enable: Bool)
    func updatePayload(_ payload: [FieldPayload])
    func showAlertError(title: String, description: String, action: String, image: UIImage)
}

final class ZipCodeFieldView: UIView, FieldValidate {
    let field: FieldForm
    var valueChanged: () -> Void = {}
    var didTap: (FieldForm) -> Void = { _ in }
    var didUpdateFieldValues: ([FieldPayload]) -> Void = { _ in }
    var showLoading: (Bool) -> Void = { _ in }
    private let zipCodeMaskFormat = "00000-000"
    private let interactor: ZipCodeFieldInteracting
    private var enableField = false {
        didSet {
            valueChanged()
        }
    }
    
    var isValid: Bool {
        enableField
    }
    
    var value: FieldPayload {
        .init(fieldKey: field.fieldKey, value: FieldValue(value: textField.text))
    }
    
    private lazy var zipCodeMask: CustomStringMask = {
        CustomStringMask(mask: zipCodeMaskFormat, maskTokenSet: [.decimalDigits])
    }()
    
    private lazy var zipCodeMasker: TextFieldMasker = {
        TextFieldMasker(textMask: zipCodeMask)
    }()
    
    private lazy var textField: UIPPFloatingTextField = {
        var textfield = UIPPFloatingTextField()
        textfield.placeholder = field.placeholder
        textfield.text = field.value?.name?.value as? String
        textfield.keyboardType = .numberPad
        textfield.maskString = zipCodeMaskFormat
        textfield.delegate = self
        textfield.addTarget(
            self,
            action: #selector(textFieldValueChanged(_:)),
            for: UIControl.Event.editingChanged)
        textfield.textColor = Colors.grayscale700.color
        let doneToolBar = DoneToolBar(doneText: Strings.Default.ok)
        textfield.inputAccessoryView = doneToolBar
        return textfield
    }()
    
    required init?(field: FieldForm, analyticsKeys: FieldAnalyticsKeys) {
        guard field.fieldType == .zipCode else {
            return nil
        }
        
        self.field = field
        let container = DependencyContainer()
        let service: ZipCodeFieldServicing = ZipCodeFieldService(dependencies: container)
        let presenter: ZipCodeFieldPresenting = ZipCodeFieldPresenter()
        self.interactor = ZipCodeFieldInteractor(analyticsKeys: analyticsKeys,
                                                 field: field,
                                                 dependencies: container,
                                                 service: service,
                                                 presenter: presenter)

        super.init(frame: .zero)

        presenter.view = self
        buildLayout()
        validateIfIsNotEmpty(textField: textField)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ZipCodeFieldView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubviews(textField)
    }
    
    func setupConstraints() {
        textField.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureViews() {
        zipCodeMasker.bind(to: textField)
        let unmaskedText = zipCodeMask.unmaskedText(from: textField.text)
        interactor.setupInitial(unmaskedText)
    }
}

@objc
extension ZipCodeFieldView: UITextFieldDelegate {
    private func textFieldValueChanged(_ sender: UIPPFloatingTextField) {
        validate(textField: sender)
        let unmaskedText = zipCodeMask.unmaskedText(from: sender.text)
        interactor.validateZipCode(unmaskedText)
    }
}

extension ZipCodeFieldView: ZipCodeFieldDisplaying {
    func showLoading(show: Bool) {
        self.showLoading(show)
    }
    
    func enableField(_ enable: Bool) {
        self.enableField = enable
    }
    
    func updatePayload(_ payload: [FieldPayload]) {
        self.didUpdateFieldValues(payload)
    }
    
    func showAlertError(title: String, description: String, action: String, image: UIImage) {
        let popup = PopupViewController(
            title: title,
            description: description,
            preferredType: .image(image)
        )
        popup.addAction(.init(title: action, style: .fill))
        textField.errorMessage = Strings.ZipCodeField.errorMessage
        
        PopupPresenter().showPopup(popup)
    }
}
