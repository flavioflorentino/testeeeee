import Foundation
import AnalyticsModule
import Core

protocol ZipCodeFieldInteracting: AnyObject {
    func setupInitial(_ zipCode: String?)
    func validateZipCode(_ zipCode: String?)
}

final class ZipCodeFieldInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    private let productKey: String
    private let sectionKey: String
    private let screenKey: String
    private let field: FieldForm
    private let service: ZipCodeFieldServicing
    private let presenter: ZipCodeFieldPresenting
    private let ZIP_CODE_SIZE = 8
    private var currentTask: URLSessionTask?
    
    private let STREET_KEY = "STREET_NAME"
    private let NEIGHBORHOOD_KEY = "NEIGHBORHOOD"
    private let CITY_KEY = "CITY"
    private let STATE_KEY = "STATE"

    init(analyticsKeys: FieldAnalyticsKeys,
         field: FieldForm,
         dependencies: Dependencies,
         service: ZipCodeFieldServicing,
         presenter: ZipCodeFieldPresenting) {
        self.screenKey = analyticsKeys.screenKey
        self.productKey = analyticsKeys.productKey
        self.sectionKey = analyticsKeys.sectionKey
        self.field = field
        self.dependencies = dependencies
        self.service = service
        self.presenter = presenter
    }
    
    func validateZipCodeFormat(_ zipCode: String?) -> String? {
        guard let zipCode = zipCode, zipCode.count == ZIP_CODE_SIZE else {
            return nil
        }
        
        return zipCode
    }
}

// MARK: - ZipCodeFieldInteracting
extension ZipCodeFieldInteractor: ZipCodeFieldInteracting {
    func setupInitial(_ zipCode: String?) {
        guard validateZipCodeFormat(zipCode) != nil else {
            presenter.enableField(false)
            return
        }
        
        presenter.enableField(true)
    }
    
    func validateZipCode(_ zipCode: String?) {
        currentTask?.cancel()
        
        guard let zipCode = validateZipCodeFormat(zipCode) else {
            presenter.enableField(false)
            return
        }
        
        presenter.showLoading()
        presenter.enableField(false)
        currentTask = service.addressBy(zipCode) { [weak self] result in
            guard let self = self else { return }
            
            defer {
                self.presenter.hideLoading()
                self.currentTask = nil
            }
            
            switch result {
            case let .success(address):
                self.handleAddress(address)
                self.trackZipCodeEvent(status: .success)
            case let .failure(error):
                self.trackZipCodeEvent(status: .error)
                if case ApiError.cancelled = error {
                    return
                }
                debugPrint(error)
                self.presenter.showZipCodeError()
            }
        }
    }
}

private extension ZipCodeFieldInteractor {
    func handleAddress(_ address: Address) {
        let payload: [FieldPayload] = [
            .init(fieldKey: self.STREET_KEY, value: .init(value: address.streetName)),
            .init(fieldKey: self.NEIGHBORHOOD_KEY, value: .init(value: address.neighborhood)),
            .init(fieldKey: self.CITY_KEY, value: .init(value: address.city, id: Int(address.cityId ?? ""))),
            .init(fieldKey: self.STATE_KEY, value: .init(value: address.stateName, id: Int(address.stateId ?? "")))
        ]
        presenter.updatePayload(payload)
        presenter.enableField(true)
    }

    func trackZipCodeEvent(status: COPEvent.Status) {
        dependencies.analytics.log(COPEvent.openField(productKey: productKey,
                                                      sectionKey: sectionKey,
                                                      screenKey: screenKey,
                                                      fieldKey: field.fieldKey,
                                                      fieldType: field.fieldType.rawValue,
                                                      status: status))
    }
}
