import Foundation
import AssetsKit

protocol ZipCodeFieldPresenting: AnyObject {
    var view: ZipCodeFieldDisplaying? { get set }
    func showLoading()
    func hideLoading()
    func enableField(_ enable: Bool)
    func updatePayload(_ payload: [FieldPayload])
    func showZipCodeError()
}

final class ZipCodeFieldPresenter {
    weak var view: ZipCodeFieldDisplaying?
}

// MARK: - ZipCodeFieldPresenting
extension ZipCodeFieldPresenter: ZipCodeFieldPresenting {
    func showLoading() {
        view?.showLoading(show: true)
    }
    
    func hideLoading() {
        view?.showLoading(show: false)
    }
    
    func enableField(_ enable: Bool) {
        view?.enableField(enable)
    }
    
    func updatePayload(_ payload: [FieldPayload]) {
        view?.updatePayload(payload)
    }
    
    func showZipCodeError() {
        view?.showAlertError(
            title: Strings.ZipCodeField.Alert.title,
            description: Strings.ZipCodeField.Alert.description,
            action: Strings.ZipCodeField.Alert.action,
            image: Resources.Illustrations.iluError.image
        )
    }
}
