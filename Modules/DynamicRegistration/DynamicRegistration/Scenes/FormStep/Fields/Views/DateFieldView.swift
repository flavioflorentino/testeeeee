import UI
import SnapKit
import Validator
import Core

final class DateFieldView: UIView, FieldValidate {
    let field: FieldForm
    var valueChanged: () -> Void = {}
    var didTap: (FieldForm) -> Void = { _ in }
    var didUpdateFieldValues: ([FieldPayload]) -> Void = { _ in }
    var showLoading: (Bool) -> Void = { _ in }
    private let dateLenght = 10
    private let dateMaskFormat = "00/00/0000"
    
    var isValid: Bool {
        textField.validate().isValid
    }
    
    var value: FieldPayload {
        .init(fieldKey: field.fieldKey, value: FieldValue(value: textField.text))
    }
    
    private lazy var dateMask: TextFieldMasker = {
        let mask = CustomStringMask(mask: dateMaskFormat, maskTokenSet: [.decimalDigits])
        return TextFieldMasker(textMask: mask)
    }()
    
    private lazy var textField: UIPPFloatingTextField = {
        var textfield = UIPPFloatingTextField()
        textfield.placeholder = field.placeholder
        textfield.text = field.value?.name?.value as? String
        textfield.keyboardType = .numberPad
        textfield.maskString = dateMaskFormat
        textfield.validationRules = ValidatorRules.dateRules(dateLength: dateLenght)
        textfield.delegate = self
        textfield.addTarget(
            self,
            action: #selector(textFieldValueChanged(_:)),
            for: UIControl.Event.editingChanged)
        textfield.textColor = Colors.grayscale700.color
        let doneToolBar = DoneToolBar(doneText: Strings.Default.ok)
        textfield.inputAccessoryView = doneToolBar
        return textfield
    }()
    
    required init?(field: FieldForm, analyticsKeys: FieldAnalyticsKeys) {
        guard field.fieldType == .date else {
            return nil
        }
        
        self.field = field
        super.init(frame: .zero)

        buildLayout()
        validateIfIsNotEmpty(textField: textField)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension DateFieldView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubviews(textField)
    }
    
    func setupConstraints() {
        textField.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureViews() {
        dateMask.bind(to: textField)
    }
}

@objc
extension DateFieldView: UITextFieldDelegate {
    private func textFieldValueChanged(_ sender: UIPPFloatingTextField) {
        validate(textField: sender)
    }
}
