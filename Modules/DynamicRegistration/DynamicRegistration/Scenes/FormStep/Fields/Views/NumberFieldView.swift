import UI
import SnapKit
import Validator

final class NumberFieldView: UIView, FieldValidate {
    let field: FieldForm
    var valueChanged: () -> Void = {}
    var didTap: (FieldForm) -> Void = { _ in }
    var didUpdateFieldValues: ([FieldPayload]) -> Void = { _ in }
    var showLoading: (Bool) -> Void = { _ in }
    private let numberValidator = ValidatorRules.numberValidator()
    
    var isValid: Bool {
        textField.validate().isValid
    }
    
    var value: FieldPayload {
        .init(fieldKey: field.fieldKey, value: FieldValue(value: textField.text))
    }
    
    private lazy var textField: UIPPFloatingTextField = {
        var textfield = UIPPFloatingTextField()
        textfield.placeholder = field.placeholder
        textfield.keyboardType = .numberPad
        textfield.text = field.value?.name?.value as? String
        textfield.delegate = self
        textfield.addTarget(
            self,
            action: #selector(textFieldValueChanged(_:)),
            for: UIControl.Event.editingChanged)
        textfield.textColor = Colors.grayscale700.color
        textfield.validationRules = ValidatorRules.make(field.validators ?? [])
        let doneToolBar = DoneToolBar(doneText: Strings.Default.ok)
        textfield.inputAccessoryView = doneToolBar
        return textfield
    }()
    
    required init?(field: FieldForm, analyticsKeys: FieldAnalyticsKeys) {
        guard field.fieldType == .number else {
            return nil
        }
        
        self.field = field
        super.init(frame: .zero)

        buildLayout()
        validateIfIsNotEmpty(textField: textField)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension NumberFieldView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubviews(textField)
    }
    
    func setupConstraints() {
        textField.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
}

@objc
extension NumberFieldView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
    }
    
    private func textFieldValueChanged(_ sender: UIPPFloatingTextField) {
        validate(textField: sender)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard string.isNotEmpty else {
            return true
        }
        
        return numberValidator.validate(input: string)
    }
}
