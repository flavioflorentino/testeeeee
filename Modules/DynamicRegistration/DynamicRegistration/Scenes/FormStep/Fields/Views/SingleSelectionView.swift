import UI
import UIKit
import SnapKit

final class SingleSelectionView: UIView, FieldValidate {
    var field: FieldForm
    var valueChanged: () -> Void = {}
    var didTap: (FieldForm) -> Void = { _ in }
    var didUpdateFieldValues: ([FieldPayload]) -> Void = { _ in }
    var showLoading: (Bool) -> Void = { _ in }
    
    var isValid: Bool {
        textField.validate().isValid
    }
    
    var value: FieldPayload {
        .init(fieldKey: field.fieldKey, value: field.value)
    }
    
    private lazy var textField: UIPPFloatingTextField = {
        var textfield = UIPPFloatingTextField()
        textfield.placeholder = field.placeholder
        textfield.text = field.value?.name?.value as? String
        textfield.delegate = self
        textfield.textColor = Colors.grayscale700.color
        textfield.validationRules = ValidatorRules.make(field.validators ?? [])
        return textfield
    }()
    
    private lazy var arrowDownIconLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(IconLabelStyle(type: .medium))
            .with(\.textColor, .grayscale700())
            .with(\.text, Iconography.angleDown.rawValue)
        return label
    }()
    
    required init?(field: FieldForm, analyticsKeys: FieldAnalyticsKeys) {
        guard field.fieldType == .singleSelection else {
            return nil
        }
        
        self.field = field
        super.init(frame: .zero)
        
        buildLayout()
        validateIfIsNotEmpty(textField: textField)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateContent(_ fieldForm: FieldForm) {
        self.field = fieldForm
        textField.text = fieldForm.value?.name?.value as? String
        validate(textField: textField)
    }
}

extension SingleSelectionView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubviews(textField)
        addSubviews(arrowDownIconLabel)
    }
    
    func setupConstraints() {
        textField.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        arrowDownIconLabel.snp.makeConstraints {
            $0.bottom.equalTo(textField.snp.bottom).inset(Spacing.base00)
            $0.trailing.equalTo(textField.snp.trailing).inset(Spacing.base00)
        }
    }
}

@objc
extension SingleSelectionView: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        didTap(field)
        return false
    }
}
