import UI
import SnapKit
import Validator

protocol MoneyFieldDisplay: AnyObject {
    func updateValue(displayValue: String?, payloadValue: String?)
}

final class MoneyFieldView: UIView, FieldValidate {
    let field: FieldForm
    var valueChanged: () -> Void = {}
    var didTap: (FieldForm) -> Void = { _ in }
    var didUpdateFieldValues: ([FieldPayload]) -> Void = { _ in }
    var showLoading: (Bool) -> Void = { _ in }
    private let presenter: MoneyFieldPresenting
    private var payloadValue: String?
    
    var isValid: Bool {
        textField.validate().isValid
    }
    
    var value: FieldPayload {
        .init(fieldKey: field.fieldKey, value: FieldValue(value: payloadValue))
    }
    
    private lazy var textField: UIPPFloatingTextField = {
        var textfield = UIPPFloatingTextField()
        textfield.placeholder = field.placeholder
        textfield.keyboardType = .numberPad
        textfield.delegate = self
        textfield.addTarget(
            self,
            action: #selector(textFieldValueChanged(_:)),
            for: UIControl.Event.editingChanged)
        textfield.textColor = Colors.grayscale700.color
        textfield.validationRules = ValidatorRules.make(field.validators ?? [])
        let doneToolBar = DoneToolBar(doneText: Strings.Default.ok)
        textfield.inputAccessoryView = doneToolBar
        return textfield
    }()
    
    required init?(field: FieldForm, analyticsKeys: FieldAnalyticsKeys) {
        guard field.fieldType == .money else {
            return nil
        }
        
        self.field = field
        presenter = MoneyFieldPresenter()
        super.init(frame: .zero)

        presenter.view = self
        buildLayout()
        validateIfIsNotEmpty(textField: textField)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension MoneyFieldView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubviews(textField)
    }
    
    func setupConstraints() {
        textField.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureViews() {
        presenter.valueChanged(field.value?.name?.value as? String)
    }
}

@objc
extension MoneyFieldView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
    }
    
    private func textFieldValueChanged(_ sender: UIPPFloatingTextField) {
        presenter.valueChanged(sender.text)
        validate(textField: sender)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var updatedText: String?
        if let currentText = textField.text,
           let textRange = Range(range, in: currentText) {
            updatedText = currentText.replacingCharacters(in: textRange, with: string)
        }
        
        return presenter.canInput(string, updatedText: updatedText)
    }
}

extension MoneyFieldView: MoneyFieldDisplay {
    func updateValue(displayValue: String?, payloadValue: String?) {
        textField.text = displayValue
        self.payloadValue = payloadValue
    }
}
