import Foundation
import UI
import UIKit
import Core

protocol MoneyFieldPresenting: AnyObject {
    var view: MoneyFieldDisplay? { get set }
    func valueChanged(_ text: String?)
    func canInput(_ text: String?, updatedText: String?) -> Bool
}

final class MoneyFieldPresenter: MoneyFieldPresenting {
    weak var view: MoneyFieldDisplay?
    private let MAX_LENGTH = 38
    private let numberValidator = ValidatorRules.numberValidator()
    
    func valueChanged(_ text: String?) {
        guard
            let text = text,
            let valueWithoutFormatting = getValueWithoutFormatting(text),
            valueWithoutFormatting.isNotEmpty,
            valueWithoutFormatting.count <= MAX_LENGTH,
            let normalizedValue = getNormalizedValue(valueWithoutFormatting) else {
            view?.updateValue(displayValue: nil, payloadValue: nil)
            return
        }
        
        view?.updateValue(
            displayValue: getDisplayValue(normalizedValue),
            payloadValue: getPayloadValue(normalizedValue)
        )
    }
    
    func getNormalizedValue(_ value: String) -> NSDecimalNumber? {
        let number = NSDecimalNumber(string: value)
        if number.decimalValue <= 0 {
            return nil
        }
        
        return number.dividing(by: 100)
    }
    
    func canInput(_ text: String?, updatedText: String?) -> Bool {
        guard let text = text, text.isNotEmpty else {
            return true
        }
        
        let result = updatedText ?? ""
        let inputValueSize = getValueWithoutFormatting(result)?.count ?? 0
        if inputValueSize > MAX_LENGTH {
            return false
        }
        
        return numberValidator.validate(input: text)
    }
    
    func getValueWithoutFormatting(_ text: String) -> String? {
        guard let regex = try? NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive) else {
            return nil
        }
        
        return regex.stringByReplacingMatches(
            in: text,
            options: .reportProgress,
            range: NSRange(location: 0, length: text.count),
            withTemplate: ""
        )
    }
    
    func getPayloadValue(_ value: NSDecimalNumber) -> String? {
        NumberFormatter.toString(
            with: value,
            numberStle: .decimal
        )
    }
    
    func getDisplayValue(_ value: NSDecimalNumber) -> String? {
        NumberFormatter.toString(
            with: value,
            numberStle: .currency
        )
    }
}
