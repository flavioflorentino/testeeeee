import Foundation
import UIKit

typealias FieldView = UIView & FieldValidate
typealias FormStepsFieldValues = [String: [FieldPayload]]

enum FieldFactory {
    private static let fieldTypes: [FieldView.Type] = [
        TextFieldView.self,
        DateFieldView.self,
        SingleSelectionView.self,
        NumberFieldView.self,
        MoneyFieldView.self,
        ZipCodeFieldView.self
    ]
    
    static func make(field: FieldForm, analyticsKeys: FieldAnalyticsKeys) -> FieldView? {
        fieldTypes.lazy.compactMap { $0.init(field: field, analyticsKeys: analyticsKeys) }.first
    }
}
