import UIKit

enum FormStepAction {
    case nextFormStep(formStep: FormStep, sectionInfo: SectionInfo)
    case updateSection(_ sectionFormUpdate: SectionFormUpdate, sectionInfo: SectionInfo)
}

protocol FormStepCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: FormStepAction)
}

final class FormStepCoordinator {
    private weak var flowCoordinator: RegistrationFlowControlCoordinating?

    weak var viewController: UIViewController?

    init(flowCoordinator: RegistrationFlowControlCoordinating) {
        self.flowCoordinator = flowCoordinator
    }
}

// MARK: - FormStepCoordinating
extension FormStepCoordinator: FormStepCoordinating {
    func perform(action: FormStepAction) {
        switch action {
        case let .nextFormStep(formStep, sectionInfo):
            flowCoordinator?.perform(action: .didNextFormStep(formStep: formStep, sectionInfo: sectionInfo))
        case let .updateSection(sectionFormUpdate, sectionValues):
            flowCoordinator?.perform(action: .updateSection(sectionFormUpdate, sectionInfo: sectionValues))
        }
    }
}
