import Foundation

// swiftlint:disable convenience_type
final class DynamicRegistrationResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: DynamicRegistrationResources.self).url(forResource: "DynamicRegistrationResources", withExtension: "bundle") else {
            return Bundle(for: DynamicRegistrationResources.self)
        }
        return Bundle(url: url) ?? Bundle(for: DynamicRegistrationResources.self)
    }()
}
