import Core

enum RegisterEndpoint {
    case formWith(productKey: String)
    case finishRegistration(productKey: String)
    case updateSection(_ payload: SectionFormUpdatePayload)
    case singleSelection(path: String)
}

extension RegisterEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .formWith:
            return "registration-manager/register/flow/screen/main"
        case .updateSection:
            return "registration-manager/register/flow/screen/section"
        case let .finishRegistration(productKey):
            return "registration-manager/register/flow/screen/finish?productKey=\(productKey)"
        case let .singleSelection(path):
            return path
        }
    }
    
    var parameters: [String: Any] {
        switch self {
        case let .formWith(productKey):
            return ["productKey": productKey]
        default:
            return [:]
        }
    }
    
    var body: Data? {
        guard case .updateSection(let payload) = self else { return nil }
        return try? JSONEncoder().encode(payload)
    }
    
    var method: HTTPMethod {
        switch self {
        case .formWith, .singleSelection:
            return .get
        case .updateSection, .finishRegistration:
            return .post
        }
    }
}
