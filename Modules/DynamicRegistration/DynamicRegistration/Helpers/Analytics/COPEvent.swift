import AnalyticsModule

enum COPEvent: AnalyticsKeyProtocol {
    case accessed(productKey: String, version: String = "", status: Status)
    case closed(productKey: String)
    case registered(productKey: String, status: Status)
    case openFormSection(productKey: String, sectionKey: String)
    case openModuleSection(productKey: String, sectionKey: String, action: SectionItemForm.Action)
    case nextFormStep(productKey: String, sectionKey: String, screenKey: String)
    case submitFormSection(productKey: String, sectionKey: String, screenKey: String, status: Status)
    case submitIntegrationResult(productKey: String, sectionKey: String, screenKey: String, status: Status)
    case openField(productKey: String, sectionKey: String, screenKey: String, fieldKey: String, fieldType: String, status: Status)
    
    enum Status: String {
        case error = "ERRO"
        case success = "SUCESSO"
    }
    
    private var name: String {
        switch self {
        case .accessed:
            return "COP Accessed"
        case .closed:
            return "Button Clicked"
        case .registered:
            return "COP Registered"
        case .openFormSection,
             .openModuleSection,
             .nextFormStep,
             .submitFormSection,
             .submitIntegrationResult,
             .openField:
            return "COP Interacted"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .accessed(productKey, version, status):
            return [
                "product_key": productKey,
                "cop_version": version,
                "status": status
            ]
        case let .closed(productKey):
            return [
                "business_context": "COP_\(productKey)",
                "button_clicked": "FECHAR",
                "screen_name": "COP_LISTA_SECOES"
            ]
        case let .registered(productKey, status):
            return [
                "product_key": productKey,
                "status": status
            ]
        case let .openFormSection(productKey, sectionKey):
            return [
                "interaction_type": "ABRIR_SECAO",
                "product_key": productKey,
                "section_key": sectionKey
            ]
        case let .openModuleSection(productKey, sectionKey, callToAction):
            return [
                "interaction_type": "ABRIR_SECAO_MODULO",
                "product_key": productKey,
                "section_key": sectionKey,
                "callToAction": callToAction
            ]
        case let .submitFormSection(productKey, sectionKey, screenKey, status):
            return [
                "interaction_type": "CONFIRMAR_SECAO",
                "product_key": productKey,
                "section_key": sectionKey,
                "screen_key": screenKey,
                "status": status
            ]
        case let .submitIntegrationResult(productKey, sectionKey, screenKey, status):
            return [
                "interaction_type": "CONFIRMAR_SECAO_MODULO",
                "product_key": productKey,
                "section_key": sectionKey,
                "screen_key": screenKey,
                "status": status
            ]
        case let .nextFormStep(productKey, sectionKey, screenKey):
            return [
                "interaction_type": "CONTINUAR_PROXIMO_PASSO",
                "product_key": productKey,
                "section_key": sectionKey,
                "screen_key": screenKey
            ]
        case let .openField(productKey, sectionKey, screenKey, fieldKey, fieldType, status):
            return [
                "interaction_type": "CARREGAR_CAMPO",
                "product_key": productKey,
                "section_key": sectionKey,
                "screen_key": screenKey,
                "field_key": fieldKey,
                "field_type": fieldType,
                "status": status
            ]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.mixPanel])
    }
}
