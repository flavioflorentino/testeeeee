import Foundation

enum DeeplinkCOPType: Equatable {
    static let PATH = "cop"
    
    case register(productKey: String)
    case registerWithCallbackAction(productKey: String, callbackUrl: URL)
    
    var needAuthentication: Bool {
        true
    }
    
    init?(url: URL) {
        guard
            let path = url.pathComponents.first(where: { $0 != "/" }),
            let productKey = url.queryParameters["productKey"] as? String,
            path == DeeplinkCOPType.PATH,
            productKey.isNotEmpty
            else {
                return nil
        }
        
        if let callbackDeeplink = url.queryParameters["callbackDeepLink"] as? String,
           let callbackUrl = URL(string: callbackDeeplink) {
            self = .registerWithCallbackAction(productKey: productKey, callbackUrl: callbackUrl)
            return
        }
        
        self = .register(productKey: productKey)
    }
}
