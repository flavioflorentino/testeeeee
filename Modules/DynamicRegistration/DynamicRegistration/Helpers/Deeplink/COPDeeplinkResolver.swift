import UIKit
import Foundation
import Core
import UI

public struct COPDeeplinkResolver: DeeplinkResolver {
    public init() {}
    
    public func canHandle(url: URL, isAuthenticated: Bool) -> DeeplinkResolverResult {
        guard let deeplinkCOPType = DeeplinkCOPType(url: url) else {
            return .notHandleable
        }
        
        if deeplinkCOPType.needAuthentication && !isAuthenticated {
            return .onlyWithAuth
        }
        
        return .handleable
    }
    
    public func open(url: URL, isAuthenticated: Bool) -> Bool {
        guard
            let deeplinkCOPType = DeeplinkCOPType(url: url),
            canHandle(url: url, isAuthenticated: isAuthenticated) == .handleable
            else {
                return false
        }
        
        let deeplinkHandler = COPDeeplinkHelper(copType: deeplinkCOPType)
        
        return deeplinkHandler.handleDeeplink()
    }
}
