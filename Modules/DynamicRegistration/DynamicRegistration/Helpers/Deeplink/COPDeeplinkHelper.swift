import UIKit
import UI

final class COPDeeplinkHelper {
    let copType: DeeplinkCOPType
    
    init(copType: DeeplinkCOPType) {
        self.copType = copType
    }
    
    func handleDeeplink() -> Bool {
        guard let navigation = COPSetup.navigationInstance?.getCurrentNavigation()
        else {
            return false
        }
        
        let coordinating = getCoordinating(navigation)
        COPSetup.navigationInstance?.updateCurrentCoordinating(coordinating)
        coordinating?.start()
        
        return true
    }
    
    func getCoordinating(_ navigation: UINavigationController) -> Coordinating? {
        switch copType {
        case .register(let productKey):
            return RegistrationFlowControlCoordinator(productKey: productKey, rootNavigation: navigation)
        case let .registerWithCallbackAction(productKey, callbackUrl):
            return RegistrationFlowControlCoordinator(productKey: productKey, rootNavigation: navigation, callbackUrl: callbackUrl)
        }
    }
}
