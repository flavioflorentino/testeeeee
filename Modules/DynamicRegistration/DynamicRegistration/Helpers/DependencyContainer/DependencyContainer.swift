import Core
import Foundation
import AnalyticsModule

typealias AppDependencies =
    HasNoDependency &
    HasMainQueue &
    HasKeychainManager &
    HasAnalytics

final class DependencyContainer: AppDependencies {
    lazy var keychain: KeychainManagerContract = KeychainManager()
    lazy var mainQueue = DispatchQueue.main
    lazy var analytics: AnalyticsProtocol = Analytics.shared
}
