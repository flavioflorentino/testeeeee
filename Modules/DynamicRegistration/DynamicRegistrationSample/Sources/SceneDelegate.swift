import UIKit
import DynamicRegistration
import Core

typealias Dependencies = HasKeychainManager

final class DependencyContainer: Dependencies {
    lazy var keychain: KeychainManagerContract = KeychainManager()
}

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    private let dependencies: Dependencies = DependencyContainer()
    var window: UIWindow?
    let navigationController = UINavigationController()
    lazy var coordinator = RegistrationFlowControlCoordinator(productKey: "COP_DEV", rootNavigation: navigationController)

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        if let windowScene = scene as? UIWindowScene {
            dependencies.keychain.set(key: KeychainKey.token, value: "3c71843ce7cf42e2a455bfa583a894c2")

            let window = UIWindow(windowScene: windowScene)
            let viewController = UIViewController()
            window.rootViewController = viewController
            self.window = window
            window.makeKeyAndVisible()
            viewController.present(navigationController, animated: true)
            coordinator.start()
        }
    }
}
