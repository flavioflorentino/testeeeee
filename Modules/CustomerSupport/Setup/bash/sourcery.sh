#!/bin/sh

#Verify if the ZENDESK_JWT_APP_ID, ZENDESK_JWT_CLIENT_ID, ZENDESK_ANONYMOUS_APP_ID, ZENDESK_ANONYMOUS_CLIENT_ID and ZENDESK_URL exist amd it is not empty.
if [ -n "${ZENDESK_JWT_APP_ID}" ] && [ -n "${ZENDESK_JWT_CLIENT_ID}" ] && [ -n "${ZENDESK_ANONYMOUS_APP_ID}" ] && [ -n "${ZENDESK_ANONYMOUS_CLIENT_ID}" ]; then
     #Create the Credentials with the keys to Zendesk release environment
     echo "Gerando credenciais do ZENDESK para ambiente de Produção"
     sourcery --config Setup/Sourcery/sourcery-prod.yml
 else
     #Create the Credentials with the keys to Zendesk sandbox environment
     echo "Gerando credenciais do ZENDESK para ambiente de Desenvolvimento"
     sourcery --config Setup/Sourcery/sourcery-develop.yml 
fi
