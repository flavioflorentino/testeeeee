import UIKit

enum ChatBotAction {
    case close
}

protocol ChatBotCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ChatBotAction)
}

final class ChatBotCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - ChatBotCoordinating
extension ChatBotCoordinator: ChatBotCoordinating {
    func perform(action: ChatBotAction) {
        switch action {
        case .close:
            guard  let navigationController = viewController?.navigationController else {
                return
            }
            if navigationController.presentingViewController != nil {
                navigationController.dismiss(animated: true, completion: nil)
            } else {
                navigationController.popToRootViewController(animated: true)
            }
        }
    }
}
