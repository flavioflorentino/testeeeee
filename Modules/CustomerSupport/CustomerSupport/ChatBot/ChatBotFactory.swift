import Foundation
import UIKit

public enum ChatBotFactory {
    public static func make(urlPath: String) -> UIViewController {
        let container = DependencyContainer()
        let service: ChatBotServicing = ChatBotService()
        let coordinator: ChatBotCoordinating = ChatBotCoordinator()
        let presenter: ChatBotPresenting = ChatBotPresenter(coordinator: coordinator)
        let viewModel = ChatBotViewModel(service: service, presenter: presenter, dependencies: container, urlPath: urlPath)
        let viewController = ChatBotViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
