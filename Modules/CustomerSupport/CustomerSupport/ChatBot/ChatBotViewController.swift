import Core
import Foundation
import UI
import UIKit
import WebKit

protocol ChatBotDisplay: AnyObject, LoadingViewProtocol {
    func displayWebView(_ url: URL)
}

private extension ChatBotViewController.Layout {
    enum Size {
        static let imageHeight: CGFloat = 90.0
    }
}

final class ChatBotViewController: ViewController<ChatBotViewModelInputs, UIView> {
    fileprivate enum Layout { }

    private lazy var webView: WKWebView = {
        let web = WKWebView()
        web.isMultipleTouchEnabled = true
        web.navigationDelegate = self
        return web
    }()
    
    private lazy var doneButtonItem = UIBarButtonItem(
        title: Strings.ChatBot.DoneButton.title,
        style: .done,
        target: self,
        action: #selector(didTapDoneButton)
    )
    
    lazy var loadingView = LoadingView()
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.load()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        }
    }

    override func buildViewHierarchy() {
        view.addSubview(webView)
    }
    
    override func setupConstraints() {
        webView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }

    override func configureViews() {
        navigationItem.rightBarButtonItem = doneButtonItem
    }
}

@objc
private extension ChatBotViewController {
    func didTapDoneButton() {
        viewModel.close()
    }
}

// MARK: ChatBotDisplay
extension ChatBotViewController: ChatBotDisplay {
    func displayWebView(_ url: URL) {
        let request = URLRequest(url: url)
        webView.load(request)
    }
}

extension ChatBotViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation?) {
        stopLoadingView()
        viewModel.didFinishLoad(url: webView.url)
        webView.evaluateJavaScript("document.title") { value, _ in
            self.title = value as? String
        }
    }
    
    func webView(
        _ webView: WKWebView,
        didReceive challenge: URLAuthenticationChallenge,
        completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void
    ) {
        let pinningHandler = PinningHandler()
        pinningHandler.handle(challenge: challenge, completionHandler: completionHandler)
    }
}
