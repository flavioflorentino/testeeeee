import Foundation

protocol ChatBotPresenting: AnyObject {
    var viewController: ChatBotDisplay? { get set }
    func presentWebView(_ url: URL)
    func didNextStep(action: ChatBotAction)
}

final class ChatBotPresenter {
    private let coordinator: ChatBotCoordinating
    weak var viewController: ChatBotDisplay?

    init(coordinator: ChatBotCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - ChatBotPresenting
extension ChatBotPresenter: ChatBotPresenting {
    func presentWebView(_ url: URL) {
        viewController?.startLoadingView()
        viewController?.displayWebView(url)
    }
    
    func didNextStep(action: ChatBotAction) {
        coordinator.perform(action: action)
    }
}
