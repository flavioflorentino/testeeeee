import Foundation

protocol ChatBotViewModelInputs: AnyObject {
    func load()
    func close()
    func didFinishLoad(url: URL?)
}

final class ChatBotViewModel {
    private enum WsWebViewAction: String {
        case closeWebview = "api/closeWebview"
    }
    
    typealias Dependencies = CustomerDependencies
    private let dependencies: Dependencies
    private let service: ChatBotServicing
    private let presenter: ChatBotPresenting
    private let urlPath: String

    init(service: ChatBotServicing, presenter: ChatBotPresenting, dependencies: Dependencies, urlPath: String) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.urlPath = urlPath
    }
}

// MARK: - ChatBotViewModelInputs
extension ChatBotViewModel: ChatBotViewModelInputs {
    func load() {
        guard let url = URL(string: urlPath) else {
            return
        }
        presenter.presentWebView(url)
    }
    
    func close() {
        presenter.didNextStep(action: .close)
    }
    
    func didFinishLoad(url: URL?) {
        guard let url = url,
            url.absoluteString.contains(WsWebViewAction.closeWebview.rawValue) else {
            return
        }
        presenter.didNextStep(action: .close)
    }
}
