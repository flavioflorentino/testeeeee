import Foundation

protocol ChatBotServicing {}

final class ChatBotService {}

// MARK: - ChatBotServicing
extension ChatBotService: ChatBotServicing {}
