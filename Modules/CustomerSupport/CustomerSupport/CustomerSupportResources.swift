import Foundation

// swiftlint:disable convenience_type
final class CustomerSupportResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: CustomerSupportResources.self).url(forResource: "CustomerSupportResources", withExtension: "bundle") else {
            return Bundle(for: CustomerSupportResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: CustomerSupportResources.self)
    }()
}
