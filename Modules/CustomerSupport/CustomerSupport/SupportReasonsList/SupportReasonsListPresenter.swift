import Foundation

protocol SupportReasonsListPresenting: AnyObject {
    var viewController: SupportReasonsListDisplay? { get set }
    func present(reasons: [SupportReason])
    func presentError()
    func presentAnotherReasonPopUp()
    func dismissAnotherReasonPopUp()
    func presentNotSelectableReasonPopUp()
    func dismissNotSelectableReasonPopUp(completion: (() -> Void)?)
    func didNextStep(action: SupportReasonsListAction)
}

final class SupportReasonsListPresenter {
    private let coordinator: SupportReasonsListCoordinating
    weak var viewController: SupportReasonsListDisplay?

    init(coordinator: SupportReasonsListCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - SupportReasonsListPresenting
extension SupportReasonsListPresenter: SupportReasonsListPresenting {
    func present(reasons: [SupportReason]) {
        viewController?.display(reasons: reasons)
    }

    func presentError() {
        viewController?.displayError()
    }

    func didNextStep(action: SupportReasonsListAction) {
        coordinator.perform(action: action)
    }
    
    func presentAnotherReasonPopUp() {
        viewController?.displayAnotherReasonPopUp()
    }
    
    func dismissAnotherReasonPopUp() {
        viewController?.dismissAnotherReasonPopUp()
    }

    func presentNotSelectableReasonPopUp() {
        viewController?.displayNotSelectableReasonPopUp()
    }

    func dismissNotSelectableReasonPopUp(completion: (() -> Void)?) {
        viewController?.dismissNotSelectableReasonPopUp(completion: completion)
    }
}
