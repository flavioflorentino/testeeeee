import Foundation

public enum SupportReasonsListFactory {
    public static func make() -> UIViewController {
        let containter = DependencyContainer()
        let service: SupportReasonsListServicing = SupportReasonsListService(dependencies: containter)
        let coordinator: SupportReasonsListCoordinating = SupportReasonsListCoordinator()
        let presenter: SupportReasonsListPresenting = SupportReasonsListPresenter(coordinator: coordinator)
        let viewModel = SupportReasonsListInteractor(dependencies: containter, service: service, presenter: presenter)
        let viewController = SupportReasonsListViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
