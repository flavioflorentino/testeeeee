import Core
import Foundation

protocol SupportReasonsListServicing {
    func requestReasonList(completion: @escaping (Result<[SupportReason], ApiError>) -> Void)
}

final class SupportReasonsListService {
    private let dependencies: CustomerDependencies

    init(dependencies: CustomerDependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - SupportReasonsListServicing
extension SupportReasonsListService: SupportReasonsListServicing {
    func requestReasonList(completion: @escaping (Result<[SupportReason], ApiError>) -> Void) {
        Api<[SupportReason]>(endpoint: HelpCenterSupportReasonListEndpoint.all).execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) {  [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model)
                completion(mappedResult)
            }
        }
    }
}
