import Core
import UIKit

enum SupportReasonsListAction: Equatable {
    case ticketList
    case newTicket(reason: SupportReason)
    case deeplink(urlPath: String)
    case chatBot(url: String)
}

protocol SupportReasonsListCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: SupportReasonsListAction)
}

final class SupportReasonsListCoordinator {
    weak var viewController: UIViewController?
}

private extension SupportReasonsListCoordinator {
    func openChatBot(urlPath: String) {
        let nextStepController = ChatBotFactory.make(urlPath: urlPath)
        viewController?.navigationController?.pushViewController(nextStepController, animated: true)
    }

    func openNewTicket(with reason: SupportReason) {
        let nextStepController = TicketRequestFactory.make(reason: reason)
        viewController?.navigationController?.pushViewController(nextStepController, animated: true)
    }

    func openDeeplink(urlPath: String?) {
        guard let urlPath = urlPath,
              let url = URL(string: urlPath),
              let deeplinkInstance = CustomerSupportManager.shared.deeplinkInstance else {
            openTicketList()
            return
        }
        deeplinkInstance.open(url: url)
    }

    func openTicketList() {
        let nextStepController = TicketListFactory.make()
        viewController?.navigationController?.pushViewController(nextStepController, animated: true)
    }
}

// MARK: - SupportReasonsListCoordinating
extension SupportReasonsListCoordinator: SupportReasonsListCoordinating {
    func perform(action: SupportReasonsListAction) {
        switch action {
        case .ticketList:
            openTicketList()

        case .newTicket(let reason):
            openNewTicket(with: reason)

        case .deeplink(let urlPath):
            openDeeplink(urlPath: urlPath)

        case .chatBot(let urlPath):
            openChatBot(urlPath: urlPath)
        }
    }
}
