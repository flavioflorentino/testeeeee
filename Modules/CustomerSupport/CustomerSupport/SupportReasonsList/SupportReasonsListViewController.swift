import AssetsKit
import Foundation
import SnapKit
import UI
import UIKit

protocol SupportReasonsListDisplay: AnyObject {
    func dismissAnotherReasonPopUp()
    func displayAnotherReasonPopUp()
    func displayNotSelectableReasonPopUp()
    func dismissNotSelectableReasonPopUp(completion: (() -> Void)?)
    func display(reasons: [SupportReason])
    func displayError()
}

final class SupportReasonsListViewController: ViewController<SupportReasonsListViewModelInputs, UIView> {
    enum AccessibilityIdentifier: String {
        case reasonsTableView
    }
    
    private lazy var headlineLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.ReasonsList.headline
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var tableHeaderView = UIView()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.tableHeaderView = tableHeaderView
        tableView.register(SupportReasonsListTableViewCell.self, forCellReuseIdentifier: SupportReasonsListTableViewCell.identifier)
        tableView.accessibilityIdentifier = AccessibilityIdentifier.reasonsTableView.rawValue
        return tableView
    }()

    private lazy var dataSource: TableViewDataSource<Int, SupportReason> = {
        let dataSource = TableViewDataSource<Int, SupportReason>(view: tableView)
        dataSource.itemProvider = { tableView, indexPath, item -> UITableViewCell? in
            let cell = tableView.dequeueReusableCell(withIdentifier: SupportReasonsListTableViewCell.identifier, for: indexPath) as? SupportReasonsListTableViewCell
            cell?.setup(with: item)
            return cell
        }
        return dataSource
    }()
    
    private lazy var anotherReasonsPopUp: PopupViewController = {
        let popUp = PopupViewController(
            title: Strings.ReasonsList.AnotherReasonPopUp.title,
            description: Strings.ReasonsList.AnotherReasonPopUp.body,
            image: nil
        )
        
        popUp.hideCloseButton = true
        
        let confirmAction = PopupAction(title: Strings.ReasonsList.AnotherReasonPopUp.confirmButton, style: .link) { [weak self] in
            self?.interactor.confirmActionFromAnotherReasonPopUp()
        }
        
        let selectReasonAction = PopupAction(title: Strings.ReasonsList.AnotherReasonPopUp.selectAnotherReasonButton, style: .fill) { [weak self] in
            self?.interactor.selectReasonActionFromAnotherReasonPopUp()
        }
        
        popUp.addAction(selectReasonAction)
        popUp.addAction(confirmAction)
        
        return popUp
    }()

    private lazy var notSelectableReasonPopUp: PopupViewController = {
        let popUp = PopupViewController(
            title: Strings.ReasonsList.NotSelectableReasonPopUp.title,
            description: Strings.ReasonsList.NotSelectableReasonPopUp.body,
            image: nil
        )

        popUp.hideCloseButton = true

        let confirmAction = PopupAction(title: Strings.ReasonsList.NotSelectableReasonPopUp.confirmButton, style: .fill) { [weak self] in
            self?.interactor.confirmActionFromNotSelectableReasonPopUp()
        }
        let cancelAction = PopupAction(title: Strings.ReasonsList.NotSelectableReasonPopUp.cancel, style: .simpleText) { [weak self] in
            self?.interactor.confirmActionFromNotSelectableReasonPopUp()
        }

        popUp.addAction(confirmAction)
        popUp.addAction(cancelAction)

        return popUp
    }()

    // MARK: Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        resizeTableViewHeaderIfNeeded()
    }
    
    private func resizeTableViewHeaderIfNeeded() {
        guard let headerView = tableView.tableHeaderView else {
            return
        }
        
        let size = headerView.systemLayoutSizeFitting(
            CGSize(width: tableView.frame.width, height: .leastNonzeroMagnitude),
            withHorizontalFittingPriority: .required,
            verticalFittingPriority: .defaultLow
        )
        
        if headerView.frame.size.height != size.height {
            headerView.frame.size.height = size.height
            tableView.tableHeaderView = headerView
            tableView.layoutIfNeeded()
        }
    }
    
    // MARK: View Configuration
    
    override func buildViewHierarchy() {
        view.addSubview(tableView)
        tableHeaderView.addSubview(headlineLabel)
    }
    
    override func setupConstraints() {
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        headlineLabel.snp.makeConstraints { make in
            make.margins.equalTo(tableHeaderView).inset(Spacing.base02)
        }
    }
    
    override func configureViews() {
        tableView.dataSource = dataSource
        tableView.delegate = self
        view.backgroundColor = Colors.backgroundPrimary.color
        navigationItem.title = Strings.ReasonsList.title
        NavigationAppearanceHelper.applyAppearance(at: self, largeTitleDisplayMode: .always)
    }
    
    override func configureStyles() {
        tableView.backgroundColor = Colors.backgroundPrimary.color
        tableView.tableFooterView = UIView()
        headlineLabel
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale850())
    }
}

private extension SupportReasonsListViewController {
    func loadData() {
        beginState()
        interactor.fetchReasons()
    }
}

// MARK: - StatefulTransitionViewing
extension SupportReasonsListViewController: StatefulTransitionViewing {
    func didTryAgain() {
        loadData()
    }
}

// MARK: - SupportReasonsListDisplay
extension SupportReasonsListViewController: SupportReasonsListDisplay {
    func display(reasons: [SupportReason]) {
        dataSource.add(items: reasons, to: 0)
        endState()
    }

    func displayError() {
        let button: StatefulButton = (image: nil, title: Strings.HelpCenter.Error.TryAgainButton.title)
        let content: StatefulContent = (title: Strings.HelpCenter.Error.title, description: Strings.HelpCenter.Error.body)
        let model = StatefulErrorViewModel(image: Resources.Illustrations.iluNotFound.image, content: content, button: button)
        endState(animated: true) {
            DispatchQueue.main.async {
                self.endState(model: model)
            }
        }
    }
    
    func displayAnotherReasonPopUp() {
        present(anotherReasonsPopUp, animated: true)
    }
    
    func dismissAnotherReasonPopUp() {
        anotherReasonsPopUp.dismiss(animated: true)
    }

    func displayNotSelectableReasonPopUp() {
        present(notSelectableReasonPopUp, animated: true)
    }

    func dismissNotSelectableReasonPopUp(completion: (() -> Void)?) {
        notSelectableReasonPopUp.dismiss(animated: true, completion: completion)
    }
}

extension SupportReasonsListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        interactor.selectReasons(at: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
