import Foundation
import UI
import UIKit

extension SupportReasonsListTableViewCell.Layout {
    enum Icon {
        static var size = CGSize(width: 8, height: 13)
    }
}

final class SupportReasonsListTableViewCell: UITableViewCell {
    fileprivate enum Layout {}
    
    private lazy var chevronImage: UIImage = {
        var image = UIImage()
        if let moduleImage = UIImage(
            named: "Chevron",
            in: Bundle(for: SupportReasonsListTableViewCell.self),
            compatibleWith: nil) {
            image = moduleImage
        } else if let appImage = UIImage(
            named: "Chevron",
            in: Bundle.main,
            compatibleWith: nil) {
            image = appImage
        }
        
        return image
    }()
    
    private lazy var disclosureIndicatorImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = chevronImage
        imageView.tintColor = Colors.branding600.color
        return imageView
    }()
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale850())
        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale400())
        label.numberOfLines = 2
        return label
    }()

    private lazy var verticalLabelsStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [nameLabel, descriptionLabel])
        stackView.spacing = Spacing.base01
        stackView.axis = .vertical
        return stackView
    }()

    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [verticalLabelsStackView, disclosureIndicatorImageView])
        stackView.spacing = Spacing.base02
        stackView.alignment = .center
        return stackView
    }()
    
    static let identifier = String(describing: SupportReasonsListTableViewCell.self)
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    func setup(with reason: SupportReason) {
        nameLabel.text = reason.name
        descriptionLabel.text = reason.description
    }
}

extension SupportReasonsListTableViewCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(stackView)
    }
    
    func setupConstraints() {
        stackView.snp.makeConstraints { make in
            make.edges.equalTo(contentView).inset(Spacing.base02)
        }
        disclosureIndicatorImageView.snp.makeConstraints { make in
            make.size.equalTo(Layout.Icon.size)
        }
    }
    
    func configureViews() {
        backgroundColor = .clear
    }
}
