import Core
import FeatureFlag
import Foundation

protocol SupportReasonsListViewModelInputs: AnyObject {
    func fetchReasons()
    func requestAnotherReasonPopUp()
    func confirmActionFromAnotherReasonPopUp()
    func selectReasonActionFromAnotherReasonPopUp()
    func confirmActionFromNotSelectableReasonPopUp()
    func selectReasons(at indexPath: IndexPath)
}

final class SupportReasonsListInteractor {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    private let service: SupportReasonsListServicing
    private let presenter: SupportReasonsListPresenting

    private var reasons: [SupportReason] = []
    private var lastSelectedReason: SupportReason?
    private let anotherReasonTag: String = "clientepf__outros"
    private let exceptionActionType: String = "open_ticket"

    init(dependencies: Dependencies, service: SupportReasonsListServicing, presenter: SupportReasonsListPresenting) {
        self.dependencies = dependencies
        self.service = service
        self.presenter = presenter
    }
}

private extension SupportReasonsListInteractor {
    func handle(reason: SupportReason) {
        let actionType = SupportResolutions(rawValue: reason.action.type)
        switch actionType {
        case .chatBot:
            goToChatBot(urlPath: reason.action.url)

        case .ticket:
            presenter.didNextStep(action: .newTicket(reason: reason))

        case .openTicket:
            presenter.presentNotSelectableReasonPopUp()

        default:
            goToChatBot()
        }
    }

    func goToTicket(from supportReason: SupportReason) {
        guard let ticketDeeplink = supportReason.action.url else {
            presenter.didNextStep(action: .ticketList)
            return
        }
        presenter.didNextStep(action: .deeplink(urlPath: ticketDeeplink))
    }

    func goToChatBot(urlPath: String? = nil) {
        let chatBotPath = urlPath ?? dependencies.featureManager.text(.chatbotCovid19)
        presenter.didNextStep(action: .chatBot(url: chatBotPath))
    }
}

// MARK: - SupportReasonsListViewModelInputs
extension SupportReasonsListInteractor: SupportReasonsListViewModelInputs {
    func fetchReasons() {
        service.requestReasonList { [weak self] result in
            switch result {
            case .success(let reasonList):
                self?.reasons = reasonList
                self?.presenter.present(reasons: reasonList)
            case .failure:
                self?.presenter.presentError()
            }
        }
    }
    
    func requestAnotherReasonPopUp() {
        presenter.presentAnotherReasonPopUp()
    }
    
    func confirmActionFromAnotherReasonPopUp() {
        guard let lastSelectedReason = lastSelectedReason else {
            presenter.didNextStep(action: .ticketList)
            return
        }
        handle(reason: lastSelectedReason)
    }
    
    func selectReasonActionFromAnotherReasonPopUp() {
        presenter.dismissAnotherReasonPopUp()
    }

    func confirmActionFromNotSelectableReasonPopUp() {
        presenter.dismissNotSelectableReasonPopUp { [weak self] in
            guard let lastSelectedReason = self?.lastSelectedReason else {
                self?.presenter.didNextStep(action: .ticketList)
                return
            }
            self?.goToTicket(from: lastSelectedReason)
        }
    }

    func selectReasons(at indexPath: IndexPath) {
        let reason = reasons[indexPath.row]
        lastSelectedReason = reason

        guard reason.value == anotherReasonTag else {
            handle(reason: reason)
            return
        }
        
        presenter.presentAnotherReasonPopUp()
    }
}
