import Core
import Foundation
import UI

public final class CustomerSupportManager {
    public static let shared = CustomerSupportManager()

    var deeplinkInstance: DeeplinkContract?
    var navigationInstance: NavigationContract?
    public var thirdPartyInstance: ThirdPartySupportSDKContract? {
        assert(privateThirdPartyInstance != nil, "ThirdPartySupportSDKContract has not been injected")
        return privateThirdPartyInstance
    }
    
    private var privateThirdPartyInstance: ThirdPartySupportSDKContract?
    
    private init() {}

    // MARK: - Registrations
    public func registerUserLoggedCredentials(accessToken: String) {
        thirdPartyInstance?.activate(
            appId: Credentials.JWT.apiID,
            clientId: Credentials.JWT.clientID
        )
        thirdPartyInstance?.registerJWT(by: accessToken)
    }
    
    public func registerAnonymousUserCredentials() {
        thirdPartyInstance?.activate(
            appId: Credentials.Anonymous.apiID,
            clientId: Credentials.Anonymous.clientID
        )
        thirdPartyInstance?.registerAnonymous()
    }
    
    public func register(notificationToken: String) {
        privateThirdPartyInstance?.register(notificationToken: notificationToken)
    }

    // MARK: - Injections
    public func inject(instance: ThirdPartySupportSDKContract) {
        privateThirdPartyInstance = instance
    }
    
    public func inject(instance: NavigationContract) {
        navigationInstance = instance
    }

    public func inject(instance: DeeplinkContract) {
        deeplinkInstance = instance
    }

    // MARK: - Set
    public func setDebugMode(enabled: Bool) {
        thirdPartyInstance?.setDebugMode(enabled: enabled)
    }
    
    public func setLocalizableFile(fileName: String) {
        thirdPartyInstance?.setLocalizableFile(fileName: fileName)
    }
}
