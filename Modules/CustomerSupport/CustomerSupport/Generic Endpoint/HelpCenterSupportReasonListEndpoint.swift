import Core
import Foundation

enum HelpCenterSupportReasonListEndpoint: ApiEndpointExposable {
    case all
    case specific(reasonTag: String)

    var path: String {
        switch self {
        case .all:
            return "/zendesk-support/contact-reasons"
        case .specific(let reasonTag):
            return "/zendesk-support/contact-reasons?tag=\(reasonTag)"
        }
    }
}
