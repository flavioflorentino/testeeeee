import Foundation
import UIKit

enum FAQAction: Equatable {
    case ticketList
    case close
    case openArticle(id: String)
}

protocol FAQCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: FAQAction)
}

final class FAQCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - FAQCoordinating
extension FAQCoordinator: FAQCoordinating {
    func perform(action: FAQAction) {
        switch action {
        case .ticketList:
            let ticketListController = TicketListFactory.make()
            viewController?.navigationController?.pushViewController(ticketListController, animated: true)
        case .close:
            viewController?.dismiss(animated: true, completion: nil)
        case .openArticle(let id):
            let articleController = FAQFactory.make(option: .article(id: id))
            viewController?.navigationController?.pushViewController(articleController, animated: true)
        }
    }
}
