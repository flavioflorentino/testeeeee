import Foundation
import UIKit
import UI

extension FAQSearchEmptyView.Layout {
    enum Image {
        static let height: CGFloat = 160
        static let width: CGFloat = 128
    }
}

final class FAQSearchEmptyView: UIView {
    fileprivate enum Layout {}
    
    lazy var emptyImageView = UIImageView(image: Assets.empty.image)
    
    lazy var title: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
            .with(\.textAlignment, .center)
        label.text = Strings.FaqSearchResult.title
        return label
    }()
    
    lazy var subtitle: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textAlignment, .center)
            .with(\.textColor, Colors.grayscale600.color)
        label.text = Strings.FaqSearchResult.body
        return label
    }()
    
    lazy var verticalStackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [
            title,
            subtitle
        ])
        stack.axis = .vertical
        stack.spacing = Spacing.base02
        return stack
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension FAQSearchEmptyView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(emptyImageView)
        addSubview(verticalStackView)
    }
    
    func setupConstraints() {
        emptyImageView.snp.makeConstraints {
            $0.height.equalTo(Layout.Image.height)
            $0.width.equalTo(Layout.Image.width)
            $0.bottom.equalTo(verticalStackView.snp.top).offset(-Spacing.base02)
            $0.centerX.equalTo(verticalStackView.snp.centerX)
        }
        verticalStackView.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.centerWithinMargins.equalToSuperview().offset(Spacing.base08)
        }
    }
}
