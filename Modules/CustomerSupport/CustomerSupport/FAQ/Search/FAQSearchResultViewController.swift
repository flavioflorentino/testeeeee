import UI

protocol FAQSearchResultDisplay: AnyObject, LoadingViewProtocol {
    func reloadArticles()
    func idle()
    func displayEmptyView()
}

private extension FAQSearchResultViewController.Layout {
    enum FooterView {
        static let height: CGFloat = 60.0
    }
}

final class FAQSearchResultViewController: ViewController<FAQViewModelInputs, UIView> {
    fileprivate enum Layout { }
    
    lazy var loadingView = LoadingView()
    
    private lazy var fetchingMoreIndicator: UIActivityIndicatorView = {
        let activity = UIActivityIndicatorView()
        activity.isHidden = true
        return activity
    }()
    
    private lazy var footerView: UIView = {
        let frame = CGRect(origin: .zero, size: CGSize(width: tableView.frame.size.width, height: Layout.FooterView.height))
        let footer = UIView(frame: frame)
        footer.addSubview(fetchingMoreIndicator)
        fetchingMoreIndicator.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
        return footer
    }()
    
    private lazy var tableView: UITableView = {
        let table = UITableView()
        table.register(
            ArticleCell.self,
            forCellReuseIdentifier: ArticleCell.identifier
        )
        table.estimatedRowHeight = 40
        table.rowHeight = UITableView.automaticDimension
        table.dataSource = self
        table.delegate = self
        return table
    }()
    
    override func buildViewHierarchy() {
        view.addSubview(tableView)
    }
    
    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    override func configureViews() {
        tableView.tableFooterView = footerView
    }
}

private extension FAQSearchResultViewController {
    func startFetchingMoreActivityIndicator() {
        fetchingMoreIndicator.startAnimating()
        fetchingMoreIndicator.isHidden = false
    }
    
    func stopFetchingMoreActivityIndicator() {
        fetchingMoreIndicator.stopAnimating()
        fetchingMoreIndicator.isHidden = true
    }
}

// MARK: TicketListResultDisplay
extension FAQSearchResultViewController: FAQSearchResultDisplay {
    func reloadArticles() {
        tableView.reloadData()
    }
    
    func idle() {
        tableView.backgroundView = nil
        stopLoadingView()
        stopFetchingMoreActivityIndicator()
    }
    
    func displayEmptyView() {
        tableView.backgroundView = FAQSearchEmptyView()
        stopLoadingView()
        stopFetchingMoreActivityIndicator()
    }
}

extension FAQSearchResultViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.searchResultArticles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ArticleCell.identifier, for: indexPath) as? ArticleCell else {
            return UITableViewCell()
        }
        
        if let article = viewModel.searchResultArticles[safe: indexPath.row] {
            cell.titleLabel.text = article.title
            cell.descriptionLabel.text = article.description
        }
        
        return cell
    }
}

extension FAQSearchResultViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let minimum = viewModel.searchResultArticles.count - 2
        if indexPath.row > minimum {
            startFetchingMoreActivityIndicator()
            viewModel.fetchMoreSearchResulsIfNeeded()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.select(indexPath: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
