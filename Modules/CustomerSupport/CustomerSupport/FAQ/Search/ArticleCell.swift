import Foundation
import SnapKit
import UI
import UIKit

private extension ArticleCell.Layout {
    enum Title {
        static let margin = Spacing.base02
    }
    enum Description {
        static let margin = Spacing.base02
    }
}

final class ArticleCell: UITableViewCell {
    static let identifier = String(describing: ArticleCell.self)
    fileprivate enum Layout { }
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
        return label
    }()
    
    lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ArticleCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(descriptionLabel)
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview().inset(Layout.Title.margin)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base00)
            $0.leading.trailing.bottom.equalToSuperview().inset(Layout.Description.margin)
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
}
