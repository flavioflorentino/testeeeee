import Foundation
import UI
import UIKit

protocol FAQPresenting: AnyObject {
    var viewController: FAQDisplay? { get set }
    var searchResultController: FAQSearchResultDisplay? { get set }
    func didNextStep(action: FAQAction)
    func presentThirdParty(fAQController: UIViewController)
    func presentSearchBar()
    func present(errorMessage: String)
    func reloadData()
    func uploadView()
    func presentLoading()
    func presentEmptyView()
}

final class FAQPresenter {
    private let coordinator: FAQCoordinating
    weak var viewController: FAQDisplay?
    weak var searchResultController: FAQSearchResultDisplay?
    
    init(coordinator: FAQCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - FAQPresenting
extension FAQPresenter: FAQPresenting {
    func presentThirdParty(fAQController: UIViewController) {
        viewController?.displayThirdParty(fAQController: fAQController)
    }
    
    func didNextStep(action: FAQAction) {
        coordinator.perform(action: action)
    }
    
    func present(errorMessage: String) {
        viewController?.display(errorMessage: errorMessage)
    }
    
    func presentSearchBar() {
        viewController?.displaySearch()
    }
    
    func reloadData() {
        searchResultController?.reloadArticles()
    }
    
    func uploadView() {
        searchResultController?.idle()
    }
    
    func presentLoading() {
        searchResultController?.startLoadingView()
    }
    
    func presentEmptyView() {
        searchResultController?.displayEmptyView()
    }
}
