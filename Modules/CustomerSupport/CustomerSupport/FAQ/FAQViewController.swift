import Core
import Foundation
import SnapKit
import UI
import UIKit

protocol FAQDisplay: AnyObject {
    func displayThirdParty(fAQController: UIViewController)
    func displaySearch()
    func display(errorMessage: String)
}

final class FAQViewController: ViewController<FAQViewModelInputs, UIView> {
    private let searchResultController: FAQSearchResultViewController
    private lazy var ticketButton: UIBarButtonItem = {
        UIBarButtonItem(barButtonSystemItem: .compose,
                        target: self,
                        action: #selector(tapTicketList(sender:)))
    }()
    
    init(viewModel: FAQViewModelInputs, searchResultController: FAQSearchResultViewController) {
        self.searchResultController = searchResultController
        super.init(viewModel: viewModel)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationItemButtons()
        viewModel.setupController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setNavigationStyle()
    }
    
    override func configureViews() {
        title = Strings.Faq.title
        NavigationAppearanceHelper.applyAppearance(at: self, largeTitleDisplayMode: .never)
    }
}

// MARK: Private Funtions
private extension FAQViewController {
    func setNavigationItemButtons() {
        navigationItem.rightBarButtonItem = ticketButton
        
        if navigationController?.viewControllers.first == self {
            navigationItem.leftBarButtonItem = UIBarButtonItem(
                title: Strings.TicketList.cancel,
                style: .plain,
                target: self,
                action: #selector(tapCancel(sender:))
            )
        }
    }

    func setNavigationStyle() {
        navigationController?.navigationBar.backgroundColor = Colors.backgroundPrimary.color
        navigationController?.navigationBar.tintColor = Colors.branding600.color
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        }
    }
    
    func display(aChildViewController: UIViewController) {
        view.addSubview(aChildViewController.view)
        aChildViewController.view.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        addChild(aChildViewController)
        
        aChildViewController.didMove(toParent: self)
        
        aChildViewController.navigationController?.delegate = self
    }
    
    @objc
    func tapTicketList(sender: UIBarButtonItem) {
        viewModel.ticketButtonTapped()
    }
    
    @objc
    func tapCancel(sender: UIBarButtonItem) {
        viewModel.cancelButtonTapped()
    }
}

// MARK: FAQDisplay
extension FAQViewController: FAQDisplay {
    func display(errorMessage: String) {
        searchResultController.dismiss(animated: true)
    }
    
    func displaySearch() {
        if #available(iOS 11.0, *) {
            let searchController = UISearchController(searchResultsController: searchResultController)
            searchController.delegate = self
            searchController.searchBar.autocapitalizationType = .none
            searchController.dimsBackgroundDuringPresentation = true
            searchController.obscuresBackgroundDuringPresentation = true
            searchController.searchBar.delegate = self
            
            navigationItem.searchController = searchController
        }
    }
    
    func displayThirdParty(fAQController: UIViewController) {
        display(aChildViewController: fAQController)
    }
}

extension FAQViewController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if Validator.isArticleControler(viewController) {
            viewController.navigationItem.rightBarButtonItem = ticketButton
        }
    }
}

// MARK: - UISearch
extension FAQViewController: UISearchControllerDelegate {
    func didDismissSearchController(_ searchController: UISearchController) {
        viewModel.clearSearchResult()
    }
}

extension FAQViewController: UISearchBarDelegate {
    func searchBarResultsListButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text else {
            return
        }
        viewModel.search(query: text)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        viewModel.clearSearchResult()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let text = searchBar.text,
            text.count > 3 else {
            return
        }
        viewModel.search(query: text)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text else {
            return
        }
        viewModel.search(query: text)
    }
}
