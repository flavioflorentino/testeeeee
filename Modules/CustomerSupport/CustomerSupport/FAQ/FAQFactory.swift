import Foundation
import UI
import UIKit

public enum FAQFactory {
    public static func make(option: FAQOptions = .home) -> UIViewController {
        let container = DependencyContainer()
        let service: FAQServicing = FAQService()
        let coordinator: FAQCoordinating = FAQCoordinator()
        let presenter: FAQPresenting = FAQPresenter(coordinator: coordinator)
        let viewModel = FAQViewModel(dependencies: container, service: service, presenter: presenter, option: option)
        let searchResultController = FAQSearchResultViewController(viewModel: viewModel)
        let viewController = FAQViewController(viewModel: viewModel, searchResultController: searchResultController)

        coordinator.viewController = viewController
        presenter.viewController = viewController
        presenter.searchResultController = searchResultController

        return viewController
    }
}
