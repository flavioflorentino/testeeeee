import Foundation

protocol FAQViewModelInputs: AnyObject {
    var searchResultArticles: [Article] { get }
    func setupController()
    func ticketButtonTapped()
    func cancelButtonTapped()
    func clearSearchResult()
    func search(query: String)
    func select(indexPath: IndexPath)
    func fetchMoreSearchResulsIfNeeded()
}

final class FAQViewModel {
    typealias Dependencies = HasZendesk
    private let dependencies: Dependencies
    private let service: FAQServicing
    private let presenter: FAQPresenting
    private let option: FAQOptions
    
    private(set) var searchResultArticles = [Article]()
    private var articlesResearch: ArticlesResearch?
    
    private var hasResearchEnded = false
    
    init(dependencies: Dependencies, service: FAQServicing, presenter: FAQPresenting, option: FAQOptions = .home) {
        self.dependencies = dependencies
        self.service = service
        self.presenter = presenter
        self.option = option
    }
}

// MARK: - FAQViewModelInputs
extension FAQViewModel: FAQViewModelInputs {
    func setupController() {
        guard let faqController = dependencies.zendeskContainer?.buildFAQController(option: option) else {
            return
        }
        presenter.presentThirdParty(fAQController: faqController)
        
        guard case .article = option else {
            presenter.presentSearchBar()
            return
        }
    }
    
    func ticketButtonTapped() {
        presenter.didNextStep(action: .ticketList)
    }
    
    func cancelButtonTapped() {
        clearSearchResult()
        presenter.didNextStep(action: .close)
    }
    
    func clearSearchResult() {
        hasResearchEnded = false
        articlesResearch = nil
        searchResultArticles = []
        presenter.reloadData()
        presenter.uploadView()
    }
    
    func select(indexPath: IndexPath) {
        let article = searchResultArticles[indexPath.row]
        presenter.didNextStep(action: .openArticle(id: "\(article.id)"))
    }
    
    func search(query: String) {
        presenter.presentLoading()
        hasResearchEnded = false
        articlesResearch = ArticlesResearch(query: query)
        requestSearch()
    }
    
    func fetchMoreSearchResulsIfNeeded() {
        articlesResearch?.page += 1
        requestSearch()
    }
}

private extension FAQViewModel {
    func requestSearch() {
        guard !hasResearchEnded, let articlesResearch = articlesResearch else {
            presenter.uploadView()
            return
        }
        
        dependencies.zendeskContainer?.search(articlesResearch) { [weak self] result in
            switch result {
            case .success(let articleResult):
                self?.handleResult(articleResult)
            case .failure(let error):
                self?.presenter.present(errorMessage: error.localizedDescription)
            }
        }
    }
    
    func handleResult(_ result: ResultArticlesResearch) {
        articlesResearch?.page = result.page
        
        if result.page > 1 {
            searchResultArticles.append(contentsOf: result.articles)
        } else {
            searchResultArticles = result.articles
        }
        
        hasResearchEnded = result.articles.isEmpty
        
        if searchResultArticles.isEmpty {
            presenter.presentEmptyView()
        } else {
            presenter.uploadView()
        }
        
        presenter.reloadData()
    }
}
