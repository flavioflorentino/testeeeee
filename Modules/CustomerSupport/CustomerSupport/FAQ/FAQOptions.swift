import Foundation

public enum FAQOptions {
    case home
    case section(id: String)
    case category(id: String)
    case article(id: String)
}

extension FAQOptions: Hashable {}
