import Core
import Foundation

final class FlowTicketRequestInteractor: CustomerSupportFlowInteracting {
    private let service: CustomerSupportFlowTicketRequestServicing
    let presenter: CustomerSupportFlowPresenting
    let reasonTag: String?

    init(presenter: CustomerSupportFlowPresenting, service: CustomerSupportFlowTicketRequestServicing, reasonTag: String? = nil) {
        self.presenter = presenter
        self.service = service
        self.reasonTag = reasonTag
    }

    func requestData() {
        guard let reasonTag = reasonTag else {
            presenter.didNextStep(action: .supportReasonsList)
            return
        }
        service.requestReason(from: reasonTag) { [weak self] result in
            switch result {
            case .failure:
                self?.presenter.didNextStep(action: .supportReasonsList)
            case .success(let reasonListResult):
                let reasonList = [SupportReason](reasonListResult)
                guard let reason = reasonList.first(where: { $0.value == reasonTag }) else {
                    self?.presenter.didNextStep(action: .supportReasonsList)
                    return
                }
                self?.presenter.didNextStep(action: .requestTicket(reason: reason))
            }
        }
    }
}
