import UIKit

enum CustomerSupportFlowAction: Equatable {
    case supportReasonsList
    case requestTicket(reason: SupportReason)
}

protocol CustomerSupportFlowCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: CustomerSupportFlowAction)
}

final class CustomerSupportFlowCoordinator {
    weak var viewController: UIViewController?
}

private extension CustomerSupportFlowCoordinator {
    func go(to viewControllerToPush: UIViewController) {
        if let mainNavigationController = viewController?.navigationController {
            mainNavigationController.setViewControllers([viewControllerToPush], animated: true)
        } else {
            viewController?.present(UINavigationController(rootViewController: viewControllerToPush), animated: true)
        }
    }
}

// MARK: - CustomerSupportFlowCoordinating
extension CustomerSupportFlowCoordinator: CustomerSupportFlowCoordinating {
    func perform(action: CustomerSupportFlowAction) {
        switch action {
        case .supportReasonsList:
            let supportReasonsListController = SupportReasonsListFactory.make()
            go(to: supportReasonsListController)
        case .requestTicket(let reason):
            let requestTicketController = TicketRequestFactory.make(reason: reason)
            go(to: requestTicketController)
        }
    }
}
