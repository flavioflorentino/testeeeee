import Foundation

protocol CustomerSupportFlowPresenting: AnyObject {
    var viewController: CustomerSupportFlowDisplaying? { get set }
    func displayLoading()
    func displayError()
    func didNextStep(action: CustomerSupportFlowAction)
}

final class CustomerSupportFlowPresenter {
    private let coordinator: CustomerSupportFlowCoordinating
    weak var viewController: CustomerSupportFlowDisplaying?

    init(coordinator: CustomerSupportFlowCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - CustomerSupportFlowPresenting
extension CustomerSupportFlowPresenter: CustomerSupportFlowPresenting {
    func displayLoading() {
        viewController?.displayLoading()
    }

    func displayError() {
        viewController?.displayError()
    }
    
    func didNextStep(action: CustomerSupportFlowAction) {
        coordinator.perform(action: action)
    }
}
