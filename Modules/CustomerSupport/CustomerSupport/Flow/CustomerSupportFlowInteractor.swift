import Foundation

protocol CustomerSupportFlowInteracting: AnyObject {
    var presenter: CustomerSupportFlowPresenting { get }
    func requestData()
}
