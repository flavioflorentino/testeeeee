import Core
import Foundation

protocol CustomerSupportFlowTicketRequestServicing {
    func requestReason(from: String, completion: @escaping (Result<[SupportReason], ApiError>) -> Void)
}

final class CustomerSupportFlowTicketRequestService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - TicketRequestServicing
extension CustomerSupportFlowTicketRequestService: CustomerSupportFlowTicketRequestServicing {
    func requestReason(from: String, completion: @escaping (Result<[SupportReason], ApiError>) -> Void) {
        let endpoint = HelpCenterSupportReasonListEndpoint.all

        Api<[SupportReason]>(endpoint: endpoint).execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) {  [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model)
                completion(mappedResult)
            }
        }
    }
}
