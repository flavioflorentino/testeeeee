import AssetsKit
import Core
import UI
import UIKit

protocol CustomerSupportFlowDisplaying: AnyObject {
    func displayLoading()
    func displayError()
}

final class CustomerSupportFlowViewController: ViewController<CustomerSupportFlowInteracting, UIView> {
    typealias Dependencies = HasMainQueue

    let dependencies: Dependencies

    init(dependencies: Dependencies, interactor: CustomerSupportFlowInteracting) {
        self.dependencies = dependencies
        super.init(interactor: interactor)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.requestData()
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}

// MARK: Private Functions
private extension CustomerSupportFlowViewController {
    func loadData() {
        interactor.requestData()
    }
}

// MARK: - CustomerSupportFlowDisplaying
extension CustomerSupportFlowViewController: CustomerSupportFlowDisplaying {
    func displayLoading() {
        beginState()
    }

    func displayError() {
        let button: StatefulButton = (image: nil, title: Strings.HelpCenter.Error.TryAgainButton.title)
        let content: StatefulContent = (title: Strings.HelpCenter.Error.title, description: Strings.HelpCenter.Error.body)
        let model = StatefulErrorViewModel(image: Resources.Illustrations.iluNotFound.image, content: content, button: button)
        endState(animated: true) { [weak self] in
            self?.dependencies.mainQueue.async { [weak self] in
                self?.endState(model: model)
            }
        }
    }
}

// MARK: - StatefulTransitionViewing
extension CustomerSupportFlowViewController: StatefulTransitionViewing {
    func didTryAgain() {
        loadData()
    }
}
