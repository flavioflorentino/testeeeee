import UIKit

public enum CustomerSupportFlowFactory {
    public static func makeTicketRequest(reasonTag: String? = nil) -> UIViewController {
        let container = DependencyContainer()
        let service: CustomerSupportFlowTicketRequestServicing = CustomerSupportFlowTicketRequestService(dependencies: container)
        let coordinator: CustomerSupportFlowCoordinating = CustomerSupportFlowCoordinator()
        let presenter: CustomerSupportFlowPresenting = CustomerSupportFlowPresenter(coordinator: coordinator)
        let interactor: CustomerSupportFlowInteracting = FlowTicketRequestInteractor(presenter: presenter, service: service, reasonTag: reasonTag)
        let viewController = CustomerSupportFlowViewController(dependencies: container, interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
