import Core
import Foundation
import SnapKit
import UI
import UIKit

protocol TicketListDisplay: AnyObject, LoadingViewProtocol {
    func displayThirdParty(ticketController: UIViewController)
}

final class TicketListViewController: ViewController<TicketListInteractorInputs, UIView> {
    lazy var loadingView = LoadingView()
    
    private lazy var newTicketButton: UIBarButtonItem = {
        UIBarButtonItem(
            barButtonSystemItem: .compose,
            target: self,
            action: #selector(tapTicketList(sender:))
        )
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationItemButtons()
        interactor.buildThirdPartyTicketList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setNavigationStyle()
    }
    
    override func configureStyles() {
        title = Strings.TicketList.title
        NavigationAppearanceHelper.applyAppearance(at: self, largeTitleDisplayMode: .never)
    }
}

private extension TicketListViewController {
    func setNavigationItemButtons() {
        navigationItem.rightBarButtonItem = newTicketButton
        
        if navigationController?.viewControllers.first == self {
            navigationItem.leftBarButtonItem = UIBarButtonItem(
                title: Strings.TicketList.cancel,
                style: .plain,
                target: self,
                action: #selector(tapCancel(sender:))
            )
        }
    }
    
    func setNavigationStyle() {
        navigationController?.navigationBar.backgroundColor = Colors.backgroundPrimary.color
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        }
    }
    
    func display(aChildViewController: UIViewController) {
        view.addSubview(aChildViewController.view)
        aChildViewController.view.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        addChild(aChildViewController)
        
        aChildViewController.didMove(toParent: self)
    }
    
    @objc
    func tapTicketList(sender: UIBarButtonItem) {
        interactor.ticketButtonTapped()
    }
    
    @objc
    func tapCancel(sender: UIBarButtonItem) {
        interactor.cancelButtonTapped()
    }
}

// MARK: - TicketListDisplay
extension TicketListViewController: TicketListDisplay {
    func displayThirdParty(ticketController: UIViewController) {
        display(aChildViewController: ticketController)
    }
}
