import UIKit

enum TicketListAction: Equatable {
    case contactReasonList
    case close
}

protocol TicketListCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: TicketListAction)
}

final class TicketListCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - TicketListCoordinating
extension TicketListCoordinator: TicketListCoordinating {
    func perform(action: TicketListAction) {
        switch action {
        case .contactReasonList:
            let controller = SupportReasonsListFactory.make()
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case .close:
            viewController?.dismiss(animated: true, completion: nil)
        }
    }
}
