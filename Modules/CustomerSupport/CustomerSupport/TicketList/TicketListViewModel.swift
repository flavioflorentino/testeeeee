import Foundation

protocol TicketListInteractorInputs: AnyObject {
    func buildThirdPartyTicketList()
    func ticketButtonTapped()
    func cancelButtonTapped()
}

final class TicketListInteractor {
    typealias Dependencies = CustomerDependencies
    private let dependencies: Dependencies
    private let presenter: TicketListPresenting
    
    private let ticketId: String?

    init(dependencies: Dependencies, presenter: TicketListPresenting, ticketId: String? = nil) {
        self.dependencies = dependencies
        self.presenter = presenter
        self.ticketId = ticketId
    }
}

// MARK: - TicketListInteractorInputs
extension TicketListInteractor: TicketListInteractorInputs {
    func buildThirdPartyTicketList() {
        guard let ticketController = dependencies.zendeskContainer?.buildTicketListController(ticketId: ticketId) else {
            return
        }
        presenter.presentThirdParty(ticketController: ticketController)
    }
    
    func ticketButtonTapped() {
        presenter.presentLoading()
        presenter.didNextStep(action: .contactReasonList)
    }
    
    func cancelButtonTapped() {
        presenter.didNextStep(action: .close)
    }
}
