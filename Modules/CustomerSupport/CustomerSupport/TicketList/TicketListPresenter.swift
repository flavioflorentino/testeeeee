import Foundation
import UI
import UIKit

protocol TicketListPresenting: AnyObject {
    var viewController: TicketListDisplay? { get set }
    func presentLoading()
    func hideLoading()
    func presentThirdParty(ticketController: UIViewController)
    func didNextStep(action: TicketListAction)
}

final class TicketListPresenter {
    private let coordinator: TicketListCoordinating
    weak var viewController: TicketListDisplay?

    init(coordinator: TicketListCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - TicketListPresenting
extension TicketListPresenter: TicketListPresenting {
    func presentLoading() {
        viewController?.startLoadingView()
    }
    
    func hideLoading() {
        viewController?.stopLoadingView()
    }
    
    func presentThirdParty(ticketController: UIViewController) {
        viewController?.displayThirdParty(ticketController: ticketController)
    }
    
    func didNextStep(action: TicketListAction) {
        viewController?.stopLoadingView()
        coordinator.perform(action: action)
    }
}
