import Foundation
import UI
import UIKit

public enum TicketListFactory {
    public static func make(ticketId: String? = nil) -> UIViewController {
        let container = DependencyContainer()
        let coordinator: TicketListCoordinating = TicketListCoordinator()
        let presenter: TicketListPresenting = TicketListPresenter(coordinator: coordinator)
        let viewModel = TicketListInteractor(dependencies: container, presenter: presenter, ticketId: ticketId)
        let viewController = TicketListViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
