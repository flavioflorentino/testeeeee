import AnalyticsModule
import Foundation

enum HelpCenterContentType: String, Equatable {
    case article
    case section
    case category
    case featuredArticle
}

enum ContentViewedEventStatus: String, Equatable {
    case success
    case error
}

enum HelpCenterEvent: AnalyticsKeyProtocol, Equatable {
    case openSearch
    case search(query: String)
    case openContent(type: HelpCenterContentType, id: String)
    case tapContactUs
    case tapMessageHistory
    case homeViewed(success: Bool)
    case contentViewed(status: ContentViewedEventStatus, type: HelpCenterContentType)

    private var name: String {
        switch self {
        case .openSearch:
            return "HC Open Search Clicked"
        case .search:
            return "HC Search Triggered"
        case .openContent:
            return "HC Open Content Clicked"
        case .tapContactUs:
            return "HC Contact Us Card Clicked"
        case .tapMessageHistory:
            return "HC Opened Ticket Card Clicked"
        case .homeViewed:
            return "HC Home Viewed"
        case .contentViewed:
            return "HC Content Viewed"
        }
    }

    private var properties: [String: Any] {
        switch self {
        case .openSearch:
            return ["": ""]
        case .search(let query):
            return ["query": query]
        case .tapContactUs:
            return ["": ""]
        case .tapMessageHistory:
            return ["": ""]
        case .homeViewed(let success):
            return ["screen_loading": success ? "success" : "error"]
        case let .contentViewed(status, type):
            return ["screen_loading": status.rawValue, "content_type": type.rawValue]
        case let .openContent(type, id):
            return ["content_type": type.rawValue, "content_id": id]
        }
    }

    private var providers: [AnalyticsProvider] {
        [.mixPanel]
    }

    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
