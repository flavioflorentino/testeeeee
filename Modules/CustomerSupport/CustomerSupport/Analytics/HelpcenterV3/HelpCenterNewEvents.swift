import AnalyticsModule
import Foundation

enum HelpCenterNewEvents: AnalyticsKeyProtocol, Equatable {
    case accessed(HelpCenterAccessable)
    case interacted(HelpCenterAccessable)
    case buttonClicked(HelpCenterButtonClickable)
    case screen(ScreenInteraction, HelpCenterViewable)
    case search(SearchInteraction, HelpCenterSearchable)
    
    enum SearchInteraction: String {
        case started = "Search Started"
        case returned = "Search Returned"
        case selected = "Search Option Selected"
    }
    
    enum ScreenInteraction: String {
        case viewed = "Screen Viewed"
        case read = "Screen All Read"
    }
    
    private var name: String {
        switch self {
        case .accessed:
            return "Helpcenter Accessed"
        case .interacted:
            return "Helpcenter Interacted"
        case .buttonClicked:
            return "Button Clicked"
        case let .screen(interaction, _):
            return interaction.rawValue
        case let .search(interaction, _):
            return interaction.rawValue
        }
    }

    private var properties: [String: Any] {
        switch self {
        case let .accessed(properties),
             let .interacted(properties):
            return properties.toDictionary
        case let .buttonClicked(properties):
            return properties.toDictionary
        case let .screen(_, properties):
            return properties.toDictionary
        case let .search(_, properties):
            return properties.toDictionary
        }
    }

    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties)
    }
    
    static func == (lhs: HelpCenterNewEvents, rhs: HelpCenterNewEvents) -> Bool {
        switch (lhs, rhs) {
        case let (.accessed(lhs), .accessed(rhs)):
            return lhs.toDictionary == rhs.toDictionary
        case let (.interacted(lhs), .interacted(rhs)):
            return lhs.toDictionary == rhs.toDictionary
        case let (.buttonClicked(lhs), .buttonClicked(rhs)):
            return lhs.toDictionary == rhs.toDictionary
        case let (.screen(lhsInteraction, lhsProperties), .screen(rhsInteraction, rhsProperties)):
            return lhsInteraction == rhsInteraction && lhsProperties.toDictionary == rhsProperties.toDictionary
        case let (.search(lhsInteraction, lhsProperties), .search(rhsInteraction, rhsProperties)):
            return lhsInteraction == rhsInteraction && lhsProperties.toDictionary == rhsProperties.toDictionary
        default:
            return false
        }
    }
}
