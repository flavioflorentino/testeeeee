import Foundation

struct HomeEventErrorProperties: HelpCenterAccessable {
    let screenName: ScreenName
    let isError: Bool
    
    var toDictionary: [String: String] {
        [
            "is_error": "\(isError)",
            "screen_name": screenName.rawValue
        ]
    }
}
