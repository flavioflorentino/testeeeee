import Foundation

struct HomeEventProperties: HelpCenterAccessable {
    let screenName: ScreenName
    let hasOpenTicket: Bool
    let isCategoryEmpty: Bool
    let isError: Bool
    let isFatEmpty: Bool
    
    var toDictionary: [String: String] {
        [
            "has_open_ticket": "\(hasOpenTicket)",
            "is_category_empty": "\(isCategoryEmpty)",
            "is_faq_empty": "\(isFatEmpty)",
            "is_error": "\(isError)",
            "screen_name": screenName.rawValue
        ]
    }
}
