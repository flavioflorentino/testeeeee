import Foundation

protocol DictionaryConvertible {
    var toDictionary: [String: String] { get }
}

protocol HelpCenterAccessable: DictionaryConvertible {}
protocol HelpCenterViewable: DictionaryConvertible {}
protocol HelpCenterButtonClickable: DictionaryConvertible {}
protocol HelpCenterSearchable: DictionaryConvertible {}
