import Foundation

enum ButtonName: String {
    case newChat = "HELPCENTER_NOVA_CONVERSA"
    case seeMore = "MOSTRAR_MAIS_PEDIDOS_AJUDA"
    case needHelp = "PRECISA_DE_AJUDA"
    case support = "SUPORTE_PICPAY"
    case search = "BUSCA"
}
