import Foundation

struct ButtonClickedEventProperties: HelpCenterButtonClickable {
    let screenName: ScreenName
    let buttonName: ButtonName
    
    var toDictionary: [String: String] {
        [
            "screen_name": screenName.rawValue,
            "button_name": buttonName.rawValue,
            "business_context": "HELPCENTER"
        ]
    }
}
