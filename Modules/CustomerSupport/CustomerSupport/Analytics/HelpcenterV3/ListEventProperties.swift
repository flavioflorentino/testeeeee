import Foundation

struct ListEventProperties: HelpCenterAccessable {
    let screenName: ScreenName
    let section: String?
    let category: String?
    let article: String?
    
    var toDictionary: [String: String] {
        [
            "screen_name": screenName.rawValue,
            "section_id": section,
            "article_id": article,
            "category_id": category
        ]
        .compactMapValues { $0 }
        .filter { $0.value.isNotEmpty }
    }
}
