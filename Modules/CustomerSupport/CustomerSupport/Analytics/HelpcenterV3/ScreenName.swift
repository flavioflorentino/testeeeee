import Foundation

enum ScreenName: String {
    case home = "HELPCENTER_INICIO"
    case section = "HELPCENTER_LISTA_SECOES"
    case categories = "HELPCENTER_LISTA_ARTIGOS"
    case article = "HELPCENTER_ARTIGO"
    case chat = "HELPCENTER_CONVERSAS"
}
