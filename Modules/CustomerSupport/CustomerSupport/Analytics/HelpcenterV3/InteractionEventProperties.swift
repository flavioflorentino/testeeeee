import Foundation

struct InteractionEventProperties: HelpCenterAccessable {
    let screenName: ScreenName
    let interactionType: InteractionType
    let section: String?
    let category: String?
    let article: String?
    
    var toDictionary: [String: String] {
        [
            "screen_name": screenName.rawValue,
            "interaction_type": interactionType.rawValue,
            "section_id": section,
            "article_id": article,
            "category_id": category
        ]
        .compactMapValues { $0 }
        .filter { $0.value.isNotEmpty }
    }
}
