import Foundation

enum InteractionType: String {
    case helpful = "ARTIGO_AJUDOU"
    case notHelpful = "ARTIGO_NAO_AJUDOU"
    case contact = "ENTRAR_EM_CONTATO"
    case article = "CLICAR_ARTIGO"
    case section = "CLICAR_SESSAO"
    case category = "CLICAR_CATEGORIA"
}
