import Foundation

public struct TicketCustomField {
    public let id: Int64
    public let value: String

    public init(id: Int64, value: String) {
        self.id = id
        self.value = value
    }
}
