import UIKit

public protocol ThirdPartySupportSDKContract {
    func activate(appId: String, clientId: String)
    func registerJWT(by userId: String)
    func registerAnonymous()
    func register(notificationToken: String)
    func setDebugMode(enabled: Bool)
    func setLocalizableFile(fileName: String) 
    
    // MARK: Show Functions
    func buildFAQController(option: FAQOptions) -> UIViewController
    func buildTicketListController(ticketId: String?) -> UIViewController
    
    // MARK: Article
    func articles(labels: [String], completion: @escaping (Result<ResultArticlesResearch, Error>) -> Void)
    func search(_ searchArticle: ArticlesResearch, completion: @escaping (Result<ResultArticlesResearch, Error>) -> Void)
    func articleBy(query: String) -> FAQOptions
    
    // MARK: Request
    func requestTicket(
        title: String,
        message: String,
        tags: [String],
        extraCustomFields: [TicketCustomField],
        completion: @escaping (Result<Void, Error>) -> Void
    )

    /// Verify if a ticket is open
    /// - Parameters:
    ///   - subjects: subject of tickets you want verity
    ///   - completion: return the first ticketId if there is not a ticket it will be nil
    func isAnyTicketOpen(_ subjects: [String], completion: @escaping (_ ticketId: String?) -> Void)
}

public extension ThirdPartySupportSDKContract {
    func buildFAQController(option: FAQOptions = .home) -> UIViewController {
        buildFAQController(option: option)
    }
    
    func buildTicketListController(ticketId: String? = nil) -> UIViewController {
        buildTicketListController(ticketId: ticketId)
    }
}
