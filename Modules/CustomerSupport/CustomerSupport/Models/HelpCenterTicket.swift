import Core
import Foundation

struct HelpCenterTicket: Decodable, Equatable {
    let id: Int
    let status: String
    let created: String
    let updated: String
    let subject: String
    let description: String
    let tags: [String]
    let comments: [HelpCenterTicketComment]
    
    enum CodingKeys: String, CodingKey {
        case status, id, subject, description, tags, comments
        case created = "created_at"
        case updated = "updated_at"
    }
    
    var updatedDate: Date? {
        Date.seventhFormatter.date(from: updated)
    }
    
    var commentListSortedByDate: [HelpCenterTicketComment] {
        comments.sorted {
            guard
                let firstDate = $0.createdDate,
                let secondDate = $1.createdDate
                else { return false }
            return firstDate < secondDate
        }
    }
}
