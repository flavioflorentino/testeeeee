public protocol CustomerSupportContract {
    func registerUserLoggedCredentials(accessToken: String)
    func register(notificationToken: String)
}
