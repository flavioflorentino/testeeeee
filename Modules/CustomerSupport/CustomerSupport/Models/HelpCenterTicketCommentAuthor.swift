import Foundation

struct HelpCenterTicketCommentAuthor: Decodable, Equatable {
    let id: Int
    let name: String
}
