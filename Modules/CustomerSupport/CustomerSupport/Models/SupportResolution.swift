import Foundation

enum SupportResolutions: String, Decodable {
    case ticket
    case chatBot = "chatbot"
    case openTicket = "open_ticket"
}
