import Foundation

struct HelpCenterArticle: Decodable, Equatable {
    let id: Int
    let title: String
    let snippet: String?
    let section: HelpCenterItem
    let category: HelpCenterItem
    
    var article: Article {
        Article(id: id, title: title, description: snippet)
    }
}
