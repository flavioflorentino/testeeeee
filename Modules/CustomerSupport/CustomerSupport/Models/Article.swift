import Foundation

public struct Article: Decodable, Equatable, HelpCenterListModel {
    private enum CodingKeys: String, CodingKey {
        case id
        case title
        case description = "snippet"
    }

    public let id: Int
    public let title: String
    public let description: String?

    public init(id: Int, title: String, description: String? = nil) {
        self.id = id
        self.title = title
        self.description = description
    }
}
