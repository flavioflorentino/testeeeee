import Foundation

struct SupportAction: Decodable {
    let type: String
    let url: String?
}

extension SupportAction: Equatable { }
