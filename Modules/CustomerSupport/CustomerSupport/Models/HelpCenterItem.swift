import Foundation

struct HelpCenterItem: Decodable, Equatable {
    var id: Int
    var name: String
}
