import Foundation

struct HelpCenterMessageHistoryBody: HelpCenterObjectProtocol {
    var title: String {
        Strings.HelpCenter.MessageHistoryCell.title
    }

    var description: String? {
        Strings.HelpCenter.MessageHistoryCell.body
    }
}
