import Foundation

public struct ResultArticlesResearch: Equatable {
    public let articles: [Article]
    public let page: Int
    
    public init(articles: [Article], page: Int) {
        self.articles = articles
        self.page = page
    }
}
