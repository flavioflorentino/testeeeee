import Foundation

public struct ArticlesResearch {
    public let query: String
    public var page: Int = 1
}
