import Core
import Foundation

struct HelpCenterTicketComment: Decodable, Equatable {
    let id: Int
    let body: String
    let created: String
    let author: HelpCenterTicketCommentAuthor
    
    enum CodingKeys: String, CodingKey {
        case id, body, author
        case created = "created_at"
    }
    
    var createdDate: Date? {
        Date.seventhFormatter.date(from: created)
    }
    
    var formattedDate: String {
        guard let createdDate = createdDate else { return "" }
        return Date.sixthFormatter.string(from: createdDate)
    }
}
