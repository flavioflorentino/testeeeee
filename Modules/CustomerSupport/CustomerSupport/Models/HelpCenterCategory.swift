import Foundation

public struct HelpCenterCategory: Decodable, Equatable, HelpCenterObjectProtocol {
    let id: Int
    let name: String
    public let description: String?
    public let timestamp: String?
    
    public var title: String {
        name
    }

    public init(id: Int, name: String, description: String? = nil, timestamp: String? = nil) {
        self.id = id
        self.name = name
        self.description = description
        self.timestamp = timestamp
    }
}
