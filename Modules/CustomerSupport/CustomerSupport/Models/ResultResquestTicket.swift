import Foundation

public enum ResultResquestTicket {
    case success
    case failure(error: Error)
}
