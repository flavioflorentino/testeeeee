import Foundation

struct SupportReason: Decodable {
    let id: Int
    let name: String
    let value: String
    let description: String
    let `default`: Bool
    let action: SupportAction
}

extension SupportReason: Equatable { }
