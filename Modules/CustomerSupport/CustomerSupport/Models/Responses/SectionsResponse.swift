import Foundation

struct SectionsResponse: HelpCenterResponse {
    let perPage: Int
    let previousPage: Int?
    let count: Int
    let nextPage: Int?
    let page: Int
    let pageCount: Int
    let sections: [HelpCenterSection]

    init(perPage: Int, previousPage: Int?, count: Int, nextPage: Int?, page: Int, pageCount: Int, sections: [HelpCenterSection]) {
        self.perPage = perPage
        self.previousPage = previousPage
        self.count = count
        self.nextPage = nextPage
        self.page = page
        self.pageCount = pageCount
        self.sections = sections
    }
}
