import Foundation

struct ArticlesResponse: HelpCenterResponse {
    let perPage: Int
    let previousPage: Int?
    let count: Int
    let nextPage: Int?
    let page: Int
    let pageCount: Int
    let articles: [Article]

    init(perPage: Int, previousPage: Int?, count: Int, nextPage: Int?, page: Int, pageCount: Int, articles: [Article]) {
        self.perPage = perPage
        self.previousPage = previousPage
        self.count = count
        self.nextPage = nextPage
        self.page = page
        self.pageCount = pageCount
        self.articles = articles
    }
}
