import Foundation

protocol HelpCenterResponse: Decodable {
    var perPage: Int { get }
    var previousPage: Int? { get }
    var count: Int { get }
    var nextPage: Int? { get }
    var page: Int { get }
    var pageCount: Int { get }
}
