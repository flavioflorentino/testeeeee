import Foundation

struct HomeResponse: Decodable, Equatable {
    let username: String
    let tickets: [HelpCenterTicket]
    let articles: [HelpCenterArticle]
    let categories: [HelpCenterCategory]
    let hadTickets: Bool
    
    enum CodingKeys: String, CodingKey {
        case username, categories, tickets
        case articles = "promoted_articles"
        case hadTickets = "had_any_ticket"
    }
    
    var ticketListSortedByDate: [HelpCenterTicket] {
        tickets.sorted {
            guard
                let firstDate = $0.updatedDate,
                let secondDate = $1.updatedDate
                else { return false }
            return firstDate < secondDate
        }
    }
}
