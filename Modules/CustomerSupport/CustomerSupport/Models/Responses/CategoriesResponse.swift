import Foundation

struct CategoriesResponse: HelpCenterResponse {
    let perPage: Int
    let previousPage: Int?
    let count: Int
    let nextPage: Int?
    let page: Int
    let pageCount: Int
    let categories: [HelpCenterCategory]

    init(perPage: Int, previousPage: Int?, count: Int, nextPage: Int?, page: Int, pageCount: Int, categories: [HelpCenterCategory]) {
        self.perPage = perPage
        self.previousPage = previousPage
        self.count = count
        self.nextPage = nextPage
        self.page = page
        self.pageCount = pageCount
        self.categories = categories
    }
}
