import Foundation

enum HelpcenterOptions: String {
    case article
    case section
    case category
    case home
    case query
}
