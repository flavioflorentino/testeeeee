import Foundation

public struct HelpCenterSection: Decodable, Equatable, HelpCenterListModel {
    public let id: Int
    let name: String
    public let description: String?
    let category: HelpCenterCategory

    public var title: String {
        name
    }

    public init(id: Int, name: String, category: HelpCenterCategory, description: String? = nil) {
        self.id = id
        self.name = name
        self.description = description
        self.category = category
    }
}
