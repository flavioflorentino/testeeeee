import Foundation

struct HelpCenterContactMessage: HelpCenterObjectProtocol {
    var title: String {
        Strings.HelpCenter.ContactUs.title
    }

    var description: String? {
        Strings.HelpCenter.ContactUs.body
    }
}
