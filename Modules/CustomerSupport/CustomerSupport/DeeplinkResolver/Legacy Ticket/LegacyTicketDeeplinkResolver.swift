import Core
import FeatureFlag
import Foundation

public final class LegacyTicketDeeplinkResolver {
    public typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    private let defaultPath = "/hc/open/ticket"

    public init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension LegacyTicketDeeplinkResolver: DeeplinkResolver {
    public func canHandle(url: URL, isAuthenticated: Bool) -> DeeplinkResolverResult {
        guard url.path.contains(defaultPath) else {
            return .notHandleable
        }
        guard isAuthenticated else {
            return .onlyWithAuth
        }

        return .handleable
    }

    public func open(url: URL, isAuthenticated: Bool) -> Bool {
        guard isAuthenticated else {
            return false
        }
        return open(with: url.pathComponents)
    }
}

private extension LegacyTicketDeeplinkResolver {
    func open(with components: [String]) -> Bool {
        let lastIndex = components.count - 1
        guard let ticketPathIndex = components.firstIndex(of: "ticket")?.advanced(by: 1), ticketPathIndex <= lastIndex else {
            showTicketList()
            return true
        }

        let ticketId = components[ticketPathIndex]

        showChat(ticketId: ticketId)

        return true
    }

    func showChat(ticketId: String) {
        guard let currentNavigation = CustomerSupportManager.shared.navigationInstance?.getCurrentNavigation() else {
            return
        }
        let ticketController = TicketListFactory.make(ticketId: ticketId)
        let navigation = UINavigationController(rootViewController: ticketController)

        currentNavigation.present(navigation, animated: true)
    }

    func showTicketList() {
        guard let currentNavigation = CustomerSupportManager.shared.navigationInstance?.getCurrentNavigation() else {
            return
        }
        let ticketController = TicketListFactory.make()
        let navigation = UINavigationController(rootViewController: ticketController)

        currentNavigation.present(navigation, animated: true)
    }
}
