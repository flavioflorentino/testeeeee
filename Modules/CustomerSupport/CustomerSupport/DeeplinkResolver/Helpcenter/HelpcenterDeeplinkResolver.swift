import Core
import FeatureFlag
import Foundation

public final class HelpcenterDeeplinkResolver: DeeplinkResolver {
    public init() {}

    public func canHandle(url: URL, isAuthenticated: Bool) -> DeeplinkResolverResult {
        guard HelpcenterDeeplinkHelper.isAnyHelpcenterOption(from: url) else {
            return .notHandleable
        }
        guard isAuthenticated else {
            return .onlyWithAuth
        }
        return .handleable
    }

    public func open(url: URL, isAuthenticated: Bool) -> Bool {
        guard isAuthenticated,
              let controller = CustomerSupportManager.shared.navigationInstance?.getCurrentNavigation() else {
            return false
        }
        return open(url: url, isAuthenticated: isAuthenticated, from: controller)
    }

    @discardableResult
    public func open(url: URL, isAuthenticated: Bool, from viewController: UIViewController) -> Bool {
        guard isAuthenticated else {
            return false
        }
        return open(with: url, from: viewController)
    }
}

private extension HelpcenterDeeplinkResolver {
    func open(with url: URL, from viewController: UIViewController) -> Bool {
        var option: HelpcenterOptions?
        var id: String?

        if let query = url.query {
            option = .query
            id = HelpcenterDeeplinkHelper.idFrom(query: query)
        } else {
            option = HelpcenterDeeplinkHelper.option(from: url)
            id = HelpcenterDeeplinkHelper.id(from: url)
        }

        showHelpcenter(from: viewController, with: option, id: id)

        return true
    }

    func showHelpcenter(from viewController: UIViewController, with option: HelpcenterOptions? = nil, id: String? = nil) {
        let faqOptionConstant = faqOption(from: option, id: id)

        let faqController = FAQFactory.make(option: faqOptionConstant)
        let navigation = UINavigationController(rootViewController: faqController)

        viewController.present(navigation, animated: true)
    }

    func openBy(query: String) -> FAQOptions {
        faqOption(from: .query, id: query)
    }

    func faqOption(from helpcenterOption: HelpcenterOptions? = nil, id: String? = nil) -> FAQOptions {
        guard let option = helpcenterOption,
              let id = id else {
            return .home
        }
        switch option {
        case .article:
            return .article(id: id)
        case .section:
            return .section(id: id)
        case .category:
            return .category(id: id)
        case .home:
            return .home
        case .query:
            return CustomerSupportManager.shared.thirdPartyInstance?.articleBy(query: id) ?? .home
        }
    }
}
