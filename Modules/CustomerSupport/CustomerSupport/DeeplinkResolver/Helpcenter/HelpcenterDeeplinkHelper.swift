import Foundation

enum HelpcenterDeeplinkHelper {
    private static let helpcenterIdentifier = "helpcenter"
    private static let queryIdentifier = "query"

    static func isAnyHelpcenterOption(from url: URL) -> Bool {
        let pathComponents = url.pathComponents.filter { $0 != "/" }

        guard pathComponents.first == helpcenterIdentifier else { return false }

        if url.query != nil {
            return true
        }

        guard option(from: pathComponents) != nil else { return false }
        return true
    }

    static func option(from components: [String]) -> HelpcenterOptions? {
        guard let helpcenterOptionsIndex = components.firstIndex(of: helpcenterIdentifier) else {
            return nil
        }

        if helpcenterOptionsIndex == components.endIndex - 1 {
            return .home
        }

        let optionIndex = helpcenterOptionsIndex.advanced(by: 1)
        let helpcenterOptionString = components[optionIndex]
        return HelpcenterOptions(rawValue: helpcenterOptionString)
    }

    static func option(from url: URL) -> HelpcenterOptions? {
        let components = url.absoluteString.split(separator: "/").map {
            String($0)
        }
        return option(from: components)
    }

    static func idFrom(query: String) -> String? {
        let components = query.split(separator: "=").map {
            String($0)
        }

        return components.last
    }

    static func id(from url: URL) -> String? {
        let components = url.absoluteString.split(separator: "/").map {
            String($0)
        }
        guard let helpcenterOptionsIdIndex = components.firstIndex(of: helpcenterIdentifier)?.advanced(by: 2),
              helpcenterOptionsIdIndex < components.count else {
            return nil
        }
        let id = components[helpcenterOptionsIdIndex]
        guard NumberFormatter().number(from: id) != nil else {
            return nil
        }
        return id
    }
}
