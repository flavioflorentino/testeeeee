import Core
import FeatureFlag
import Foundation

public final class RequestTicketByReasonDeeplinkResolver {
    private let defaultPath = "/helpcenter/reason"

    public init() {}
}

extension RequestTicketByReasonDeeplinkResolver: DeeplinkResolver {
    public func canHandle(url: URL, isAuthenticated: Bool) -> DeeplinkResolverResult {
        guard url.path.contains(defaultPath) else {
            return .notHandleable
        }
        guard isAuthenticated else {
            return .onlyWithAuth
        }

        return .handleable
    }

    public func open(url: URL, isAuthenticated: Bool) -> Bool {
        guard isAuthenticated,
              let controller = CustomerSupportManager.shared.navigationInstance?.getCurrentNavigation() else {
            return false
        }
        return open(url: url, isAuthenticated: isAuthenticated, from: controller)
    }

    public func open(url: URL, isAuthenticated: Bool, from viewController: UIViewController) -> Bool {
        guard isAuthenticated else {
            return false
        }
        return open(with: url, from: viewController)
    }
}

private extension RequestTicketByReasonDeeplinkResolver {
    func open(with url: URL, from viewController: UIViewController) -> Bool {
        let reasonTagString = reasonTag(from: url)

        presentTicketRequest(withReasonTag: reasonTagString, on: viewController)

        return true
    }

    func reasonTag(from url: URL) -> String {
        let components = url.pathComponents
        let lastIndex = components.count - 1
        guard let reasonTagIndex = components.firstIndex(of: "reason")?.advanced(by: 1), reasonTagIndex <= lastIndex else {
            return ""
        }

        return components[reasonTagIndex]
    }

    func presentTicketRequest(withReasonTag reasonTag: String, on viewController: UIViewController) {
        let ticketRequest = CustomerSupportFlowFactory.makeTicketRequest(reasonTag: reasonTag)
        let navigation = UINavigationController(rootViewController: ticketRequest)

        viewController.present(navigation, animated: true)
    }
}
