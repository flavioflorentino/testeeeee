import AnalyticsModule
import Core
import FeatureFlag
import Foundation

typealias CustomerDependencies = HasNoDependency & HasMainQueue & HasFeatureManager & HasZendesk & HasAnalytics

final class DependencyContainer: CustomerDependencies {
    lazy var mainQueue = DispatchQueue.main
    lazy var featureManager: FeatureManagerContract = FeatureManager.shared
    lazy var zendeskContainer = CustomerSupportManager.shared.thirdPartyInstance
    lazy var analytics: AnalyticsProtocol = Analytics.shared
}
