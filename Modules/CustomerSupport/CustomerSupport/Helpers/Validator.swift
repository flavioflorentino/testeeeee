import Foundation
import UIKit

enum Validator {
    static func isArticleControler(_ viewController: UIViewController) -> Bool {
        let viewControllerClass = String(describing: viewController.classForCoder)
        return isZendeskClass(viewController) && viewControllerClass.contains("Article")
    }
    
    static func isZendeskClass(_ any: AnyObject) -> Bool {
        let viewControllerClass = String(describing: type(of: any))
        return viewControllerClass.contains("ZDK")
    }
}
