import Foundation
import UIKit

enum NavigationAppearanceHelper {
    static func applyAppearance(at viewController: UIViewController, largeTitleDisplayMode: UINavigationItem.LargeTitleDisplayMode) {
        if #available(iOS 11.0, *) {
            viewController.navigationItem.largeTitleDisplayMode = largeTitleDisplayMode
        }
        viewController.navigationController?.navigationBar.backgroundColor = viewController.view.backgroundColor
    }
}
