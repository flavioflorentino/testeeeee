protocol HasNoDependency {}

public protocol HasZendesk {
    var zendeskContainer: ThirdPartySupportSDKContract? { get }
}

public protocol HasCustomerSupport {
    var customerSupport: CustomerSupportContract { get }
}
