#if DEBUG
import Foundation

public final class ZendeskWrapperMock: ThirdPartySupportSDKContract {
    public private(set) var didCallActivate = 0
    public private(set) var didCallRegisterJWT = 0
    public private(set) var didCallRegisterAnonymous = 0
    public private(set) var didCallRegister = 0
    public private(set) var didCallSetDebugMode = 0
    public private(set) var didCallSetLocalizableFile = 0
    public private(set) var didCallRequestTicket = 0
    public private(set) var didCallIsAnyTicketOpen = 0
    public private(set) var didCallSearchArticles = 0
    public private(set) var didCallArticlesLabel = 0
    public private(set) var didCallArticleByQuery = 0
    public private(set) var didCallBuildFAQController = 0
    public private(set) var didCallBuildTicketListController = 0

    public private(set) var requestTicketCompletionResult: Result<Void, Error>?
    public private(set) var isAnyTicketSubjects = [String]()
    public private(set) var articlesQuery: String?
    public private(set) var buildTicketId: String?
    public private(set) var buildTicketOption: FAQOptions?
    public var isAnyTicketOpenCompletionResult: String?
    public var searchCompletionResult: Result<ResultArticlesResearch, Error>?
    public var articlesCompletionResult: Result<ResultArticlesResearch, Error>?
    public var articlesByQueryResult: FAQOptions = .home
    
    public init() { }

    public func activate(appId: String, clientId: String) {
        didCallActivate += 1
    }

    public func registerJWT(by userId: String) {
        didCallRegisterJWT += 1
    }

    public func registerAnonymous() {
        didCallRegisterAnonymous += 1
    }

    public func register(notificationToken: String) {
        didCallRegister += 1
    }

    public func setDebugMode(enabled: Bool) {
        didCallSetDebugMode += 1
    }

    public func setLocalizableFile(fileName: String) {
        didCallSetLocalizableFile += 1
    }

    public func requestTicket(
        title: String,
        message: String,
        tags: [String],
        extraCustomFields: [TicketCustomField],
        completion: @escaping (Result<Void, Error>) -> Void
    ) {
        didCallRequestTicket += 1
        guard let requestTicketCompletionResult = requestTicketCompletionResult else { return }

        completion(requestTicketCompletionResult)
    }

    public func isAnyTicketOpen(_ subjects: [String], completion: @escaping (String?) -> Void) {
        didCallIsAnyTicketOpen += 1
        isAnyTicketSubjects = subjects

        completion(isAnyTicketOpenCompletionResult)
    }

    public func search(_ searchArticle: ArticlesResearch, completion: @escaping (Result<ResultArticlesResearch, Error>) -> Void) {
        didCallSearchArticles += 1
        guard let searchCompletionResult = searchCompletionResult else { return }

        completion(searchCompletionResult)
    }

    public func articles(labels: [String], completion: @escaping (Result<ResultArticlesResearch, Error>) -> Void) {
        didCallArticlesLabel += 1
        guard let articlesCompletionResult = articlesCompletionResult else { return }

        completion(articlesCompletionResult)
    }

    public func articleBy(query: String) -> FAQOptions {
        articlesQuery = query
        didCallArticleByQuery += 1
        return articlesByQueryResult
    }

    public func buildFAQController(option: FAQOptions = .home) -> UIViewController {
        didCallBuildFAQController += 1
        buildTicketOption = option
        return UIViewController()
    }

    public func buildTicketListController(ticketId: String? = nil) -> UIViewController {
        didCallBuildTicketListController += 1
        buildTicketId = ticketId
        return UIViewController()
    }
}
#endif
