import Core
import Foundation

protocol HelpCenterListPresenting: AnyObject {
    var viewController: HelpCenterListDisplaying? { get set }
    func present(objects: [HelpCenterListModel], title: String?)
    func present(error: ApiError)
    func didNextStep(action: HelpCenterListAction)
}

final class HelpCenterListPresenter {
    private let coordinator: HelpCenterListCoordinating
    weak var viewController: HelpCenterListDisplaying?

    init(coordinator: HelpCenterListCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - HelpCenterListPresenting
extension HelpCenterListPresenter: HelpCenterListPresenting {
    func present(objects: [HelpCenterListModel], title: String?) {
        viewController?.display(title: title)
        viewController?.display(objects: objects)
    }

    func present(error: ApiError) {
        viewController?.displayError()
    }
    
    func didNextStep(action: HelpCenterListAction) {
        coordinator.perform(action: action)
    }
}
