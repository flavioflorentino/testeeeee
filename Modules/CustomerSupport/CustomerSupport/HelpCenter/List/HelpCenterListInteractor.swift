import AnalyticsModule
import Foundation

protocol HelpCenterListInteracting: AnyObject {
    func requestHelpCenterObjectsList()
    func requestDidSelect(_ item: HelpCenterListModel)
}

final class HelpCenterListInteractor {
    typealias Dependency = HasAnalytics
    private let dependencies: Dependency
    private let service: HelpCenterListServicing
    private let presenter: HelpCenterListPresenting
    private let title: String?
    private let screenName: ScreenName
    private let sectionId: Int?
    private let categoryId: Int?
    
    init(
        service: HelpCenterListServicing,
        presenter: HelpCenterListPresenting,
        screenName: ScreenName,
        dependencies: Dependency,
        sectionId: Int?,
        categoryId: Int?,
        title: String? = nil
    ) {
        self.service = service
        self.presenter = presenter
        self.title = title
        self.screenName = screenName
        self.dependencies = dependencies
        self.sectionId = sectionId
        self.categoryId = categoryId
    }
    
    private func trackView() {
        let section = sectionId == nil ? "" : String(describing: sectionId ?? 0)
        let category = categoryId == nil ? "" : String(describing: categoryId ?? 0)
        
        switch screenName {
        case .section, .categories:
            let eventProperties = ListEventProperties(
                screenName: screenName,
                section: section,
                category: category,
                article: nil
            )
            dependencies.analytics.log(HelpCenterNewEvents.accessed(eventProperties))
        default:
            break
        }
    }
}

// MARK: - HelpCenterListInteracting
extension HelpCenterListInteractor: HelpCenterListInteracting {
   func requestHelpCenterObjectsList() {
        trackView()
        service.requestList { [weak self] result in
            switch result {
            case .success(let helpCenterObjects):
                self?.presenter.present(objects: helpCenterObjects, title: self?.title)

            case .failure(let apiError):
                self?.presenter.present(error: apiError)
            }
        }
    }

    func requestDidSelect(_ item: HelpCenterListModel) {
        presenter.didNextStep(action: .selected(item: item))
    }
}
