import UIKit

public enum HelpCenterListFactory {
    private static func make(
        service: HelpCenterListServicing,
        coordinator: HelpCenterListCoordinating,
        screenName: ScreenName,
        title: String? = nil,
        sectionId: Int? = nil,
        categoryId: Int? = nil
    ) -> UIViewController {
        let container = DependencyContainer()
        let coordinator: HelpCenterListCoordinating = coordinator
        let presenter: HelpCenterListPresenting = HelpCenterListPresenter(coordinator: coordinator)
        let interactor = HelpCenterListInteractor(
            service: service,
            presenter: presenter,
            screenName: screenName,
            dependencies: container,
            sectionId: sectionId,
            categoryId: categoryId,
            title: title
        )
        let viewController = HelpCenterListViewController(interactor: interactor)

        coordinator.viewController = viewController

        presenter.viewController = viewController

        return viewController
    }

    public static func makeArticleListFrom(sectionId: Int, withTitle title: String? = nil) -> UIViewController {
        let container = DependencyContainer()
        let articlesListService = HelpCenterListArticlesSectionService(dependencies: container, sectionId: sectionId)
        let articlesListCoordinator = HelpCenterListArticlesSectionCoordinator()

        return make(
            service: articlesListService,
            coordinator: articlesListCoordinator,
            screenName: .categories,
            title: title,
            sectionId: sectionId
        )
    }

    public static func makeSectionListFrom(categoryId: Int, withTitle title: String? = nil) -> UIViewController {
        let container = DependencyContainer()
        let sectionsListService = HelpCenterListSectionService(dependencies: container, categoryId: categoryId)
        let sectionsListCoordinator = HelpCenterListSectionsCoordinator()

        return make(
            service: sectionsListService,
            coordinator: sectionsListCoordinator,
            screenName: .section,
            title: title,
            categoryId: categoryId
        )
    }
}
