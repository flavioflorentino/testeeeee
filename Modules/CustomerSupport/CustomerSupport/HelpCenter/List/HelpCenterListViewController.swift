import AssetsKit
import Foundation
import UI
import UIKit

protocol HelpCenterListDisplaying: AnyObject {
    func display(objects: [HelpCenterListModel])
    func display(title: String?)
    func displayError()
}

private extension HelpCenterListViewController.Layout {
    enum Size {
        static let cellHeight: CGFloat = 90.0
        static let disclouseImageSize = CGSize(width: 24, height: 24)
        static let tableViewEdges: CGFloat = 0.0
        static let estimatedRowHeight: CGFloat = 60.0
        static let HeaderSectionHeight: CGFloat = 80.0
    }
}

final class HelpCenterListViewController: ViewController<HelpCenterListInteracting, UIView> {
    fileprivate enum Layout { }

    private lazy var tableView: UITableView = {
        let table = UITableView()
        table.separatorStyle = .none
        table.estimatedRowHeight = Layout.Size.estimatedRowHeight
        table.rowHeight = UITableView.automaticDimension
        table.register(HelpCenterHeaderSectionView.self, forHeaderFooterViewReuseIdentifier: HelpCenterHeaderSectionView.identifier)
        table.register(OnlyTitleCell.self, forCellReuseIdentifier: OnlyTitleCell.identifier)
        return table
    }()

    private lazy var dataSource: TableViewDataSource<Int, HelpCenterListModel> = {
        let dataSource = TableViewDataSource<Int, HelpCenterListModel>(view: tableView)
        dataSource.itemProvider = { tableView, indexPath, item -> UITableViewCell? in
            guard let cell = tableView.dequeueReusableCell(withIdentifier: OnlyTitleCell.identifier, for: indexPath) as? OnlyTitleCell else {
                return UITableViewCell()
            }
            cell.setup(with: item)
            return cell
        }
        return dataSource
    }()

    private var headerTitle: String?

    override func viewDidLoad() {
        super.viewDidLoad()

        loadData()
    }

    override func buildViewHierarchy() {
        view.addSubview(tableView)
    }

    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }

    override func configureViews() {
        tableView.delegate = self
        tableView.dataSource = dataSource
        title = Strings.HelpCenter.title
    }
}

private extension HelpCenterListViewController {
    func loadData() {
        beginState()
        interactor.requestHelpCenterObjectsList()
    }
}

// MARK: - HelpCenterListDisplaying
extension HelpCenterListViewController: HelpCenterListDisplaying {
    func display(title: String?) {
        headerTitle = title
    }

    func display(objects: [HelpCenterListModel]) {
        dataSource.add(items: objects, to: 0)
        endState()
    }

    func displayError() {
        if #available(iOS 11.0, *) {
            navigationItem.searchController = nil
        }
        let button: StatefulButton = (image: nil, title: Strings.HelpCenter.Error.TryAgainButton.title)
        let content: StatefulContent = (title: Strings.HelpCenter.Error.title, description: Strings.HelpCenter.Error.body)
        let model = StatefulErrorViewModel(image: Resources.Illustrations.iluNotFound.image, content: content, button: button)
        endState(animated: true) {
            DispatchQueue.main.async {
                self.endState(model: model)
            }
        }
    }
}

extension HelpCenterListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let item = dataSource.item(at: indexPath) else { return }
        interactor.requestDidSelect(item)
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let headerTitle = self.headerTitle, !headerTitle.isEmpty else { return nil }

        let identifier = HelpCenterHeaderSectionView.identifier
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: identifier) as? HelpCenterHeaderSectionView
        header?.setup(
            headerTitle: headerTitle,
            width: tableView.bounds.width,
            tintColor: tableView.backgroundColor
        )

        return header
    }
}

extension HelpCenterListViewController: StatefulTransitionViewing {
    func didTryAgain() {
        loadData()
    }
}
