import Core
import Foundation

public final class HelpCenterListSectionService {
    public typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    let categoryId: Int

    public init(dependencies: Dependencies, categoryId: Int) {
        self.dependencies = dependencies
        self.categoryId = categoryId
    }
}

extension HelpCenterListSectionService: HelpCenterListServicing {
    public func requestList(completion: @escaping ((Result<[HelpCenterListModel], ApiError>) -> Void)) {
        let endpoint = HelpCenterListSectionEndpoint(categoryId: categoryId)

        Api<SectionsResponse>(endpoint: endpoint).execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) {  [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model)
                switch mappedResult {
                case .success(let sectionsResponse):
                    completion(.success(sectionsResponse.sections))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
}
