import Core
import Foundation

struct HelpCenterListSectionEndpoint: ApiEndpointExposable {
    let categoryId: Int

    var path: String {
        "/zendesk-support/helpcenter/categories/\(categoryId)/sections"
    }

    init(categoryId: Int) {
        self.categoryId = categoryId
    }
}
