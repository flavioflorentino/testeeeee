import Foundation

final class HelpCenterListSectionsCoordinator {
    weak var viewController: UIViewController?
}

extension HelpCenterListSectionsCoordinator: HelpCenterListCoordinating {
    func perform(action: HelpCenterListAction) {
        switch action {
        case .selected(let item):
            let helpArticleList = HelpCenterListFactory.makeArticleListFrom(sectionId: item.id, withTitle: item.title)
            viewController?.navigationController?.pushViewController(helpArticleList, animated: true)
        }
    }
}
