import Core
import Foundation

public protocol HelpCenterListServicing {
    func requestList(completion: @escaping ((Result<[HelpCenterListModel], ApiError>) -> Void))
}
