import Core
import Foundation

struct HelpCenterListArticlesSectionEndpoint: ApiEndpointExposable {
    let sectionId: Int

    var path: String {
        "/zendesk-support/helpcenter/sections/\(sectionId)/articles"
    }

    init(sectionId: Int) {
        self.sectionId = sectionId
    }
}
