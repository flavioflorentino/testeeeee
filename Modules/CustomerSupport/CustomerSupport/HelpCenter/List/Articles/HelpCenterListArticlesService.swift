import Core
import Foundation

public final class HelpCenterListArticlesSectionService {
    public typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    let sectionId: Int

    public init(dependencies: Dependencies, sectionId: Int) {
        self.dependencies = dependencies
        self.sectionId = sectionId
    }
}

extension HelpCenterListArticlesSectionService: HelpCenterListServicing {
    public func requestList(completion: @escaping ((Result<[HelpCenterListModel], ApiError>) -> Void)) {
        let endpoint = HelpCenterListArticlesSectionEndpoint(sectionId: sectionId)

        Api<ArticlesResponse>(endpoint: endpoint).execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) {  [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model)
                switch mappedResult {
                case .success(let sectionsResponse):
                    completion(.success(sectionsResponse.articles))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
}
