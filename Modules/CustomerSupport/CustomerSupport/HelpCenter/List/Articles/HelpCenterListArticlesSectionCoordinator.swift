import Foundation
import UIKit

final class HelpCenterListArticlesSectionCoordinator {
    weak var viewController: UIViewController?
}

extension HelpCenterListArticlesSectionCoordinator: HelpCenterListCoordinating {
    func perform(action: HelpCenterListAction) {
        switch action {
        case .selected(let item):
            let articleController = FAQFactory.make(option: .article(id: "\(item.id)"))
            viewController?.navigationController?.pushViewController(articleController, animated: true)
        }
    }
}
