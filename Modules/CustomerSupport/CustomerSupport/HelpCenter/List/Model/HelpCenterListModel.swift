import Foundation

public protocol HelpCenterListModel: Decodable {
    var id: Int { get }
    var title: String { get }
}
