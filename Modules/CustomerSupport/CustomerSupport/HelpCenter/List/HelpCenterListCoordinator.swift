import UIKit

public enum HelpCenterListAction {
    case selected(item: HelpCenterListModel)
}

public protocol HelpCenterListCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: HelpCenterListAction)
}
