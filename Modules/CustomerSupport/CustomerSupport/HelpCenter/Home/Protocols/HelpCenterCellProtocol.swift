import Foundation

protocol HelpCenterCellProtocol {
    func setup(with helpCenterObject: HelpCenterObjectProtocol)
}
