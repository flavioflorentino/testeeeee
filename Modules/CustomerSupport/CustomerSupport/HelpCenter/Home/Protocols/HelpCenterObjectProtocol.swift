import Foundation

public protocol HelpCenterObjectProtocol {
    var title: String { get }
    var description: String? { get }
    var timestamp: String? { get }
}

extension HelpCenterObjectProtocol {
    var timestamp: String? { "" }
}
