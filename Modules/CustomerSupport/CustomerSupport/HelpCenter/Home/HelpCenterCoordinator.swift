import UIKit

enum HelpCenterAction: Equatable {
    case openArticle(id: String)
    case openCategory(id: Int, title: String)
    case openMessageHistory
    case openNewTicket
    case close
    case openTicket(ticketId: String)
}

protocol HelpCenterCoordinating: ArticleResearchCoordinatingDelegate {
    var viewController: UIViewController? { get set }
    func perform(action: HelpCenterAction)
}

final class HelpCenterCoordinator {
    weak var viewController: UIViewController?
}

private extension HelpCenterCoordinator {
    func handleOpenArticle(with id: String) {
        push(FAQFactory.make(option: .article(id: id)))
    }
    
    func push(_ vc: UIViewController) {
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK: - HelpCenterCoordinating
extension HelpCenterCoordinator: HelpCenterCoordinating {
    func perform(action: HelpCenterAction) {
        switch action {
        case .openArticle(let id):
            handleOpenArticle(with: id)
        case let .openCategory(id, title):
            push(HelpCenterListFactory.makeSectionListFrom(categoryId: id, withTitle: title))
        case .openMessageHistory:
            push(TicketListFactory.make())
        case .openNewTicket:
            push(SupportReasonsListFactory.make())
        case let .openTicket(ticketId):
            push(TicketListFactory.make(ticketId: ticketId))
        case .close:
            viewController?.dismiss(animated: true, completion: nil)
        }
    }

    func perform(action: ArticleResearchAction) {
        switch action {
        case .openArticle(let id):
            handleOpenArticle(with: id)
        }
    }
}
