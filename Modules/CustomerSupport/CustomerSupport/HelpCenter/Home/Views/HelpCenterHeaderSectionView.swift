import Foundation
import UI
import UIKit
import SnapKit

protocol HelpCenterHeaderSectionViewDelegate: AnyObject {
    func showMore()
}

private extension HelpCenterHeaderSectionView.Layout {
    static let showMoreButtonWidth: CGFloat = 105.0
    static let height: CGFloat = 40.0
}

final class HelpCenterHeaderSectionView: UITableViewHeaderFooterView {
    fileprivate enum Layout { }
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .medium))
        return label
    }()
    
    lazy var showMoreButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(
            LinkButtonStyle(
                size: .small,
                icon: (
                    name: .angleRight,
                    alignment: .right
                )
            )
        )
        button.setTitle(Strings.HelpCenter.showMore, for: .normal)
        button.addTarget(self, action: #selector(showMoreTapped), for: .touchUpInside)
        return button
    }()

    private weak var delegate: HelpCenterHeaderSectionViewDelegate?
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(
        width: CGFloat,
        tintColor: UIColor?,
        item: HelpCenterTableModel,
        delegate: HelpCenterHeaderSectionViewDelegate? = nil
    ) {
        let size = CGSize(width: width, height: HelpCenterHeaderSectionView.Layout.height)
        frame = CGRect(origin: .zero, size: size)
        self.tintColor = tintColor
        self.delegate = delegate
        titleLabel.text = HelpCenterSectionsHelper.titleHeader(from: item)
        showMoreButton.isHidden = !item.showMore
    }
    
    func setup(
        headerTitle: String,
        width: CGFloat,
        tintColor: UIColor?
    ) {
        let size = CGSize(width: width, height: HelpCenterHeaderSectionView.Layout.height)
        frame = CGRect(origin: .zero, size: size)
        self.tintColor = tintColor
        titleLabel.text = headerTitle
        showMoreButton.isHidden = true
    }
    
    @objc
    private func showMoreTapped() {
        delegate?.showMore()
    }
}

extension HelpCenterHeaderSectionView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(titleLabel)
        addSubview(showMoreButton)
    }

    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().inset(Spacing.base03)
            $0.bottom.equalToSuperview().inset(Spacing.base01)
            $0.leading.equalToSuperview().inset(Spacing.base02)
        }
        
        showMoreButton.snp.makeConstraints {
            $0.leading.equalTo(titleLabel.snp.trailing).offset(Spacing.base00)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.height.equalTo(titleLabel.snp.height)
            $0.width.equalTo(Layout.showMoreButtonWidth)
            $0.centerY.equalTo(titleLabel.snp.centerY)
        }
    }
}
