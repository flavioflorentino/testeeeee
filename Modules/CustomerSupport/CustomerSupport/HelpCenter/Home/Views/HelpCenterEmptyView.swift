import AssetsKit
import Foundation
import UIKit
import UI

protocol HelpCenterEmptyViewDelegate: AnyObject {
    func didTapContactUsButton()
}

extension HelpCenterEmptyView.Layout {
    enum Image {
        static let size = CGSize(width: 144, height: 144)
    }
}

final class HelpCenterEmptyView: UIView {
    fileprivate enum Layout {}

    weak var delegate: HelpCenterEmptyViewDelegate?
    override var canBecomeFirstResponder: Bool {
        true
    }

    private lazy var tapGesture: UITapGestureRecognizer = {
        let gesture = UITapGestureRecognizer()
        gesture.addTarget(self, action: #selector(didTapScreen))
        return gesture
    }()

    private lazy var emptyImageView = UIImageView(image: Resources.Illustrations.iluInteractionP2PWithBackground.image)

    private lazy var title: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
            .with(\.textAlignment, .center)
        label.text = Strings.FaqSearchResult.title
        return label
    }()

    private lazy var subtitle: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textAlignment, .center)
            .with(\.textColor, Colors.grayscale600.color)
        label.text = Strings.FaqSearchResult.body
        return label
    }()

    private lazy var contactUsButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(LinkButtonStyle())

        let normalAttributes: [NSAttributedString.Key: Any] = [
            .font: Typography.linkPrimary().font(),
            .foregroundColor: Colors.branding700.color,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        let normalAttributesString = NSMutableAttributedString(string: Strings.HelpCenter.ContactUs.linkTitle, attributes: normalAttributes)

        button.setAttributedTitle(normalAttributesString, for: .normal)

        button.addTarget(self, action: #selector(didTapContactUsButton), for: .touchUpInside)
        return button
    }()

    private lazy var verticalStackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [
            emptyImageView,
            title,
            subtitle,
            contactUsButton
        ])
        stack.axis = .vertical
        stack.alignment = .center
        stack.spacing = Spacing.base02
        return stack
    }()

    init(frame: CGRect = .zero, delegate: HelpCenterEmptyViewDelegate? = nil ) {
        self.delegate = delegate

        super.init(frame: frame)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

@objc
private extension HelpCenterEmptyView {
    func didTapContactUsButton() {
        delegate?.didTapContactUsButton()
    }

    func didTapScreen() {
        becomeFirstResponder()
    }
}

extension HelpCenterEmptyView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(verticalStackView)
    }

    func setupConstraints() {
        emptyImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Image.size)
        }
        verticalStackView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.centerWithinMargins.equalToSuperview()
        }
    }

    func configureViews() {
        addGestureRecognizer(tapGesture)
    }
}
