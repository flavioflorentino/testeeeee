import UIKit

enum HelpCenterSectionsHelper {
    typealias HelpCenterCellType = HelpCenterCellProtocol & UITableViewCell
    
    static func cell(from item: HelpCenterTableModel, in tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell? {
        let identifier: String
        switch item.section {
        case .helpCenterCategory:
            identifier = TitleDescriptionTableViewCell.identifier
        case .contactUs:
            identifier = ContactUsCell.identifier
        case .frequentlyAsked:
            identifier = OnlyTitleCell.identifier
        case .messageHistory:
            identifier = MessagesHistoryCell.identifier
        case .greetings:
            identifier = GreetingsCell.identifier
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? HelpCenterCellType
        cell?.setup(with: item)
        
        return cell
    }
    
    static func titleHeader(from item: HelpCenterTableModel) -> String? {
        switch item.section {
        case .contactUs, .messageHistory:
            return Strings.HelpCenter.doYouNeedAnyHelpYet
        case .frequentlyAsked:
            return Strings.HelpCenter.frequentlyAskedQuestions
        case .helpCenterCategory:
            return Strings.HelpCenter.categories
        default:
            return nil
        }
    }
}
