import AssetsKit
import Foundation
import UI
import UIKit

extension GreetingsCell.Layout {
    enum Image {
        static var size = CGSize(width: 106, height: 113)
    }
}

final class GreetingsCell: UITableViewCell, Identifiable {
    fileprivate enum Layout {}

    private lazy var titleLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.labelStyle(TitleLabelStyle(type: .large))
        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.textColor, .grayscale600())
        return label
    }()

    private lazy var labelsStackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [
            titleLabel,
            descriptionLabel
        ])
        stack.axis = .vertical
        stack.spacing = Spacing.base03
        return stack
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension GreetingsCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(labelsStackView)
    }

    func setupConstraints() {
        labelsStackView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.trailing.bottom.equalToSuperview().inset(Spacing.base02)
        }
    }

    func configureViews() {
        selectionStyle = .none
    }
}

extension GreetingsCell: HelpCenterCellProtocol {
    func setup(with helpCenterObject: HelpCenterObjectProtocol) {
        titleLabel.text = helpCenterObject.title
        descriptionLabel.text = helpCenterObject.description
    }
}
