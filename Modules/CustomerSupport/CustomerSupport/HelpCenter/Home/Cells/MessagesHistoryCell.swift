import AssetsKit
import Foundation
import UI
import UIKit

extension MessagesHistoryCell.Layout {
    static var timeLabelWidth = 35
}

final class MessagesHistoryCell: UITableViewCell, Identifiable {
    fileprivate enum Layout {}

    private lazy var cardView: UIView = {
        let view = UIView()
        view
            .viewStyle(CardViewStyle())
            .with(\.clipsToBounds, true)
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.numberOfLines, 1)
            .with(\.lineBreakMode, .byTruncatingTail)
        return label
    }()
    
    private lazy var timeLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.labelStyle(CaptionLabelStyle())
            .with(\.textColor, .grayscale500())
        return label
    }()

    private lazy var labelsStackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [
            titleLabel,
            descriptionLabel
        ])
        stack.axis = .vertical
        stack.spacing = Spacing.base00
        return stack
    }()

    private lazy var ppImageView: UIImageView = {
        let image = Resources.Logos.picpayLogoAvatar.image
        let imageView = UIImageView(image: image)
        imageView.imageStyle(AvatarImageStyle(hasBorder: true))
        return imageView
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension MessagesHistoryCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(cardView)
        cardView.addSubview(ppImageView)
        cardView.addSubview(labelsStackView)
        cardView.addSubview(timeLabel)
    }

    func setupConstraints() {
        cardView.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(Spacing.base01)
        }

        ppImageView.snp.makeConstraints {
            $0.top.leading.equalToSuperview().offset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base02)
            $0.width.equalTo(ppImageView.snp.height)
        }
        
        labelsStackView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalTo(ppImageView.snp.trailing).offset(Spacing.base01)
        }

        timeLabel.snp.makeConstraints {
            $0.leading.equalTo(labelsStackView.snp.trailing).offset(Spacing.base01)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.centerY.equalToSuperview()
            $0.width.equalTo(Layout.timeLabelWidth)
        }
    }

    func configureViews() {
        selectionStyle = .none
    }
}

extension MessagesHistoryCell: HelpCenterCellProtocol {
    func setup(with helpCenterObject: HelpCenterObjectProtocol) {
        titleLabel.text = helpCenterObject.title
        descriptionLabel.text = helpCenterObject.description
        timeLabel.text = helpCenterObject.timestamp
    }
}
