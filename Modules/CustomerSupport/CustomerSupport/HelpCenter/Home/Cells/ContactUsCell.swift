import AssetsKit
import Foundation
import UI
import UIKit

extension ContactUsCell.Layout {
    enum Image {
        static var p2pSize = CGSize(width: 77, height: 81)
        static var baseHeight = 90
    }
}

final class ContactUsCell: UITableViewCell, Identifiable {
    fileprivate enum Layout {}

    private lazy var cardView: UIView = {
        let view = UIView()
        view
            .viewStyle(CardViewStyle())
            .with(\.clipsToBounds, true)
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale400())
        return label
    }()
    
    private lazy var labelsStackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [
            titleLabel,
            descriptionLabel
        ])
        stack.axis = .vertical
        stack.spacing = Spacing.base01
        return stack
    }()

    private lazy var baseImageView = UIImageView(image: Assets.contactUsBackgroundCell.image)
    private lazy var ilustrationP2PImageView = UIImageView(image: Resources.Illustrations.iluInteractionP2P.image)

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ContactUsCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(cardView)
        cardView.addSubviews(
            baseImageView,
            labelsStackView,
            ilustrationP2PImageView
        )
    }

    func setupConstraints() {
        cardView.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(Spacing.base01)
        }
        
        baseImageView.snp.makeConstraints {
            $0.top.trailing.bottom.equalToSuperview()
            $0.width.equalTo(Layout.Image.baseHeight)
        }

        labelsStackView.snp.makeConstraints {
            $0.top.bottom.leading.equalToSuperview().inset(Spacing.base02)
            $0.trailing.equalTo(baseImageView.snp.leading).offset(Spacing.base01)
        }

        ilustrationP2PImageView.snp.makeConstraints {
            $0.trailing.equalTo(baseImageView).offset(-Spacing.base02)
            $0.centerY.equalToSuperview()
            $0.size.equalTo(Layout.Image.p2pSize)
        }
    }

    func configureViews() {
        selectionStyle = .none
    }
}

extension ContactUsCell: HelpCenterCellProtocol {
    func setup(with helpCenterObject: HelpCenterObjectProtocol) {
        titleLabel.text = helpCenterObject.title
        descriptionLabel.text = helpCenterObject.description
    }
}
