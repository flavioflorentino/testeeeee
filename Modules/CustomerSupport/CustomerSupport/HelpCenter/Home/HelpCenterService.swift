import Core
import Foundation
import FeatureFlag

protocol HelpCenterServicing {
    var isNewHome: Bool { get }
    func requestFrequentlyAskedQuestions(completion: @escaping (Result<ArticlesResponse, ApiError>) -> Void)
    func requestCategories(completion: @escaping (Result<CategoriesResponse, ApiError>) -> Void)
    func fetchFAQHome(completion: @escaping (Result<HomeResponse, ApiError>) -> Void)
}

final class HelpCenterService {
    typealias Dependencies = HasMainQueue & HasFeatureManager
    private let dependencies: Dependencies
    
    var isNewHome: Bool {
        dependencies.featureManager.isActive(.isNewFAQHomeEndpointAvailable)
    }
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - HelpCenterServicing
extension HelpCenterService: HelpCenterServicing {
    func fetchFAQHome(completion: @escaping (Result<HomeResponse, ApiError>) -> Void) {
        execute(endpoint: HelpCenterHomeEndpoint.home, decoder: JSONDecoder(), completion: completion)
    }
    
    func requestFrequentlyAskedQuestions(completion: @escaping (Result<ArticlesResponse, ApiError>) -> Void) {
        execute(endpoint: HelpCenterEndpoint.articles, completion: completion)
    }
    
    func requestCategories(completion: @escaping (Result<CategoriesResponse, ApiError>) -> Void) {
        execute(endpoint: HelpCenterEndpoint.categories, completion: completion)
    }
    
    private func execute<T: Decodable>(
        endpoint: ApiEndpointExposable,
        decoder: JSONDecoder = JSONDecoder(.convertFromSnakeCase),
        completion: @escaping (Result<T, ApiError>) -> Void
    ) {
        Api<T>(endpoint: endpoint).execute(jsonDecoder: decoder) {  [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
