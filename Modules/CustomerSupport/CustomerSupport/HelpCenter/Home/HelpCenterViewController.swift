import AssetsKit
import Foundation
import UI
import UIKit

protocol HelpCenterDisplaying: AnyObject {
    func displaySearch()
    func displayError()
    func display(items: [HelpCenterTableModel], in section: Int)
    func startLoading()
    func stopLoading()
}

private extension HelpCenterViewController.Layout {
    enum TableView {
        static let estimatedRowHeight: CGFloat = 60.0
    }
}

final class HelpCenterViewController: ViewController<HelpCenterInteracting, UIView>, LoadingViewProtocol {
    fileprivate enum Layout { }

    private let articleResearchResultController: ArticleResearchViewController

    private lazy var tableView: UITableView = {
        let table = UITableView()
        table.separatorStyle = .none
        table.estimatedRowHeight = Layout.TableView.estimatedRowHeight
        table.rowHeight = UITableView.automaticDimension
        table.register(OnlyTitleCell.self, forCellReuseIdentifier: OnlyTitleCell.identifier)
        table.register(TitleDescriptionTableViewCell.self, forCellReuseIdentifier: TitleDescriptionTableViewCell.identifier)
        table.register(HelpCenterHeaderSectionView.self, forHeaderFooterViewReuseIdentifier: HelpCenterHeaderSectionView.identifier)
        table.register(MessagesHistoryCell.self, forCellReuseIdentifier: MessagesHistoryCell.identifier)
        table.register(ContactUsCell.self, forCellReuseIdentifier: ContactUsCell.identifier)
        table.register(GreetingsCell.self, forCellReuseIdentifier: GreetingsCell.identifier)
        return table
    }()
    
    private lazy var dataSource: TableViewDataSource<Int, HelpCenterTableModel> = {
        let dataSource = TableViewDataSource<Int, HelpCenterTableModel>(view: tableView)
        dataSource.itemProvider = { tableView, indexPath, item -> UITableViewCell? in
            HelpCenterSectionsHelper.cell(from: item, in: tableView, at: indexPath)
        }
        return dataSource
    }()

    private lazy var closeItem = UIBarButtonItem(
        title: Strings.HelpCenter.close,
        style: .plain,
        target: self,
        action: #selector(tapCloseItem(sender:))
    )

    lazy var loadingView: LoadingView = {
        let loading = LoadingView()
        return loading
    }()

    init(interactor: HelpCenterInteracting, articleResearchResultController: ArticleResearchViewController) {
        self.articleResearchResultController = articleResearchResultController
        super.init(viewModel: interactor)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.loadViewInfo()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        setupNavigationItemButtons()
        setupNavigationBarAppearance()
    }

    override func buildViewHierarchy() {
        view.addSubview(tableView)
    }
    
    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }

    override func configureViews() {
        tableView.delegate = self
        tableView.dataSource = dataSource
    }
}

private extension HelpCenterViewController {
    func setupNavigationItemButtons() {
        if navigationController?.viewControllers.first === self {
            navigationItem.leftBarButtonItem = closeItem
        }
    }
  
    func setupNavigationBarAppearance() {
        title = Strings.HelpCenter.title
        navigationController?.navigationBarStyle = .standard

        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.backgroundColor = Colors.white.color
            navigationController?.navigationBar.standardAppearance = appearance
            navigationController?.navigationBar.compactAppearance = appearance
            navigationController?.navigationBar.scrollEdgeAppearance = appearance
        } else {
            navigationController?.navigationBar.backgroundColor = Colors.white.color
        }
    }

    func loadData() {
        beginState()
        interactor.loadViewInfo()
    }
}

@objc
private extension HelpCenterViewController {
    func tapCloseItem(sender: UIBarButtonItem) {
        interactor.requestToClose()
    }
}

// MARK: - HelpCenterDisplaying
extension HelpCenterViewController: HelpCenterDisplaying {
    func displaySearch() {
        if #available(iOS 11.0, *) {
            if navigationItem.searchController == nil {
                let searchController = UISearchController(searchResultsController: articleResearchResultController)
                searchController.delegate = articleResearchResultController
                searchController.searchBar.autocapitalizationType = .none
                searchController.dimsBackgroundDuringPresentation = true
                searchController.obscuresBackgroundDuringPresentation = true
                searchController.searchBar.delegate = articleResearchResultController
                searchController.searchBar.placeholder = Strings.HelpCenter.whatIsYourQuestion

                navigationItem.searchController = searchController
                navigationItem.hidesSearchBarWhenScrolling = false
            }
        }
    }
    
    func display(items: [HelpCenterTableModel], in section: Int) {
        dataSource.add(items: items, to: section)
        endState()
    }

    func displayError() {
        if #available(iOS 11.0, *) {
            navigationItem.searchController = nil
        }
        let button: StatefulButton = (image: nil, title: Strings.HelpCenter.Error.TryAgainButton.title)
        let content: StatefulContent = (title: Strings.HelpCenter.Error.title, description: Strings.HelpCenter.Error.body)
        let model = StatefulErrorViewModel(image: Resources.Illustrations.iluNotFound.image, content: content, button: button)
        endState(animated: true) {
            DispatchQueue.main.async {
                self.endState(model: model)
            }
        }
    }
    
    func startLoading() {
        startLoadingView()
    }
    
    func stopLoading() {
        stopLoadingView()
    }
}

extension HelpCenterViewController: StatefulTransitionViewing {
    func didTryAgain() {
        loadData()
    }
}

extension HelpCenterViewController: HelpCenterEmptyViewDelegate {
    func didTapContactUsButton() {
        interactor.requestNewTicket()
    }
}

extension HelpCenterViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let indexPath = IndexPath(row: 0, section: section)
        guard
            let item = dataSource.item(at: indexPath),
            item.section != .greetings
            else { return UIView() }
        
        let identifier = HelpCenterHeaderSectionView.identifier
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: identifier) as? HelpCenterHeaderSectionView
        header?.setup(
            width: tableView.bounds.width,
            tintColor: tableView.backgroundColor,
            item: item,
            delegate: self
        )
                
        return header
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = dataSource.item(at: indexPath)
        interactor.requestDidSelect(item)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension HelpCenterViewController: HelpCenterHeaderSectionViewDelegate {
    func showMore() {
        interactor.requestMessageHistory()
    }
}
