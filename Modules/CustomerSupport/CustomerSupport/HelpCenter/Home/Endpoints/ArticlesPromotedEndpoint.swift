import Core
import Foundation

enum HelpCenterEndpoint: ApiEndpointExposable {
    case articles
    case categories

    var path: String {
        switch self {
        case .articles:
            return "/zendesk-support/helpcenter/articles/promoted"
        case .categories:
            return "/zendesk-support/helpcenter/categories"
        }
    }
}
