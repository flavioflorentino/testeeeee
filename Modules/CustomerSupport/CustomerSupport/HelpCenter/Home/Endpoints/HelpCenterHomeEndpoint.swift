import Core
import Foundation

enum HelpCenterHomeEndpoint: ApiEndpointExposable {
    case home
}

extension HelpCenterHomeEndpoint {
    var path: String {
        "zendesk-support/helpcenter/home"
    }
}
