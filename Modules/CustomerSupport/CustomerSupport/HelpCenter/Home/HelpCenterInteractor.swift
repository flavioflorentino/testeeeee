import AnalyticsModule
import Core
import Foundation

enum HelpCenterState {
    case hasTicket
    case hasNotTicket
    
    var sections: [HelpCenterTableModel.Section] {
        switch self {
        case .hasTicket:
            return [.messageHistory, .frequentlyAsked, .helpCenterCategory]
        case .hasNotTicket:
            return [.frequentlyAsked, .helpCenterCategory, .contactUs]
        }
    }
    
    var newSections: [HelpCenterTableModel.Section] {
        switch self {
        case .hasTicket:
            return [.greetings, .messageHistory, .frequentlyAsked, .helpCenterCategory]
        case .hasNotTicket:
            return [.greetings, .frequentlyAsked, .helpCenterCategory, .contactUs]
        }
    }
}

protocol HelpCenterInteracting: AnyObject {
    func loadViewInfo()
    func requestArticle(id: Int)
    func requestCategory(id: Int, title: String)
    func requestMessageHistory()
    func requestNewTicket()
    func requestDidSelect(_ item: HelpCenterTableModel?)
    func requestToClose()
}

final class HelpCenterInteractor {
    typealias Dependencies = HasMainQueue & HasZendesk & HasAnalytics
    private let dependencies: Dependencies
    private let service: HelpCenterServicing
    private let presenter: HelpCenterPresenting

    private var frequentlyAskedArticles: [Article] = []
    private var categories: [HelpCenterCategory] = []
    private var state: HelpCenterState = .hasNotTicket
    private var error: ApiError?
    private lazy var titlesBySectionIndex: [String] = []
    private var tickets: [HelpCenterTicket] = []

    init(service: HelpCenterServicing, presenter: HelpCenterPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - HelpCenterInteracting
extension HelpCenterInteractor: HelpCenterInteracting {
    func loadViewInfo() {
        presenter.startLoading()
        service.isNewHome ? fetchFAQInfos() : fetchOldFAQInfos()
    }

    func requestArticle(id: Int) {
        if service.isNewHome {
            let eventProperties = InteractionEventProperties(
                screenName: .home,
                interactionType: .article,
                section: nil,
                category: nil,
                article: String(describing: id)
            )
            dependencies.analytics.log(HelpCenterNewEvents.interacted(eventProperties))
        } else {
            dependencies.analytics.log(HelpCenterEvent.openContent(type: .article, id: "\(id)"))
        }
        presenter.didNextStep(action: .openArticle(id: "\(id)"))
    }

    func requestCategory(id: Int, title: String) {
        if service.isNewHome {
            let eventProperties = InteractionEventProperties(
                screenName: .home,
                interactionType: .category,
                section: nil,
                category: String(describing: id),
                article: nil
            )
            dependencies.analytics.log(HelpCenterNewEvents.interacted(eventProperties))
        } else {
            dependencies.analytics.log(HelpCenterEvent.openContent(type: .category, id: "\(id)"))
        }
        presenter.didNextStep(action: .openCategory(id: id, title: title))
    }

    func requestMessageHistory() {
        if service.isNewHome {
            let eventProperties = ButtonClickedEventProperties(screenName: .home, buttonName: .seeMore)
            dependencies.analytics.log(HelpCenterNewEvents.buttonClicked(eventProperties))
        } else {
            dependencies.analytics.log(HelpCenterEvent.tapMessageHistory)
        }
        presenter.didNextStep(action: .openMessageHistory)
    }

    func requestNewTicket() {
        if service.isNewHome {
            let eventProperties = ButtonClickedEventProperties(screenName: .home, buttonName: .needHelp)
            dependencies.analytics.log(HelpCenterNewEvents.buttonClicked(eventProperties))
        } else {
            dependencies.analytics.log(HelpCenterEvent.tapContactUs)
        }

        presenter.didNextStep(action: .openNewTicket)
    }
    
    func openTicket() {
        guard let ticket = tickets.last else { return }
        presenter.didNextStep(action: .openTicket(ticketId: "\(ticket.id)"))
        
        let eventProperties = ButtonClickedEventProperties(screenName: .home, buttonName: .support)
        dependencies.analytics.log(HelpCenterNewEvents.buttonClicked(eventProperties))
    }

    func requestToClose() {
        presenter.didNextStep(action: .close)
    }

    func requestDidSelect(_ item: HelpCenterTableModel?) {
        guard let item = item else { return }
        
        switch item.section {
        case .frequentlyAsked:
            guard let id = item.id else { return }
            requestArticle(id: id)
        case .helpCenterCategory:
            guard let id = item.id else { return }
            requestCategory(id: id, title: item.title)
        case .messageHistory:
            service.isNewHome ? openTicket() : requestMessageHistory()
        case .contactUs:
            service.isNewHome ? requestNewTicket() : requestMessageHistory()
        case .greetings:
            break
        }
    }
}

// MARK: - New FAQ Endpoint
private extension HelpCenterInteractor {
    func fetchFAQInfos() {
        service.fetchFAQHome { [weak self] result in
            guard let self = self else { return }
            switch result {
            case let .success(response):
                self.handleNewHomeSuccess(with: response)
            case .failure:
                self.presenter.presentError()
                self.trackError()
            }
        }
    }
    
    func handleNewHomeSuccess(with response: HomeResponse) {
        tickets = response.ticketListSortedByDate
        frequentlyAskedArticles = response.articles.map { $0.article }
        categories = response.categories
        state = tickets.isEmpty ? .hasNotTicket : .hasTicket
        
        trackHelpcenterAccessed(with: response)
        presenter.presentSearch()
        presenter.presentNewHome(sections: state.newSections, response: response)
        presenter.stopLoading()
    }
    
    func trackHelpcenterAccessed(with response: HomeResponse) {
        let hasTickets = response.tickets.isNotEmpty
        let isCategoryEmpty = response.categories.isEmpty
        let isFatEmpty = response.articles.isEmpty
        
        let eventProperties = HomeEventProperties(
            screenName: .home,
            hasOpenTicket: hasTickets,
            isCategoryEmpty: isCategoryEmpty,
            isError: false,
            isFatEmpty: isFatEmpty
        )
        dependencies.analytics.log(HelpCenterNewEvents.accessed(eventProperties))
    }
    
    func trackError() {
        let eventProperties = HomeEventErrorProperties(
            screenName: .home,
            isError: true
        )
        dependencies.analytics.log(HelpCenterNewEvents.accessed(eventProperties))
    }
}

// MARK: - New FAQ Endpoint
private extension HelpCenterInteractor {
    func fetchOldFAQInfos() {
        let group = DispatchGroup()
        requestCategories(group)
        requestFrequentlyAskedQuestions(group)
        requestContactUsSection(group)

        group.notify(queue: dependencies.mainQueue) { [weak self] in
            self?.handleResult()
        }
    }
    
    func requestFrequentlyAskedQuestions(_ group: DispatchGroup) {
        group.enter()
        service.requestFrequentlyAskedQuestions { [weak self] result in
            switch result {
            case .success(let response):
                self?.frequentlyAskedArticles = response.articles
                self?.dependencies.analytics.log(HelpCenterEvent.contentViewed(status: .success, type: .article))
            case .failure(let error):
                self?.error = error
                self?.dependencies.analytics.log(HelpCenterEvent.contentViewed(status: .error, type: .article))
            }
            group.leave()
        }
    }

    func requestCategories(_ group: DispatchGroup) {
        group.enter()
        service.requestCategories { [weak self] result in
            switch result {
            case .success(let response):
                self?.categories = response.categories
                self?.dependencies.analytics.log(HelpCenterEvent.contentViewed(status: .success, type: .category))
            case .failure(let error):
                self?.error = error
                self?.dependencies.analytics.log(HelpCenterEvent.contentViewed(status: .error, type: .category))
            }
            group.leave()
        }
    }

    func requestContactUsSection(_ group: DispatchGroup) {
        group.enter()
        dependencies.zendeskContainer?.isAnyTicketOpen([]) { [weak self] ticketId in
            self?.state = ticketId != nil ? .hasTicket : .hasNotTicket
            group.leave()
        }
    }

    func handleResult() {
        guard error == nil else {
            dependencies.analytics.log(HelpCenterEvent.homeViewed(success: false))
            presenter.presentError()
            return
        }

        dependencies.analytics.log(HelpCenterEvent.homeViewed(success: true))
        presenter.presentSearch()

        presenter.present(
            sections: state.sections,
            articles: frequentlyAskedArticles,
            categories: categories
        )
        
        presenter.stopLoading()
    }
}
