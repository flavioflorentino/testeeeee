import UIKit

public enum HelpCenterFactory {
    public static func make() -> UIViewController {
        let container = DependencyContainer()
        let service: HelpCenterServicing = HelpCenterService(dependencies: container)
        let coordinator: HelpCenterCoordinating = HelpCenterCoordinator()
        let presenter: HelpCenterPresenting = HelpCenterPresenter(coordinator: coordinator)
        let interactor = HelpCenterInteractor(service: service, presenter: presenter, dependencies: container)

        let emptyView = HelpCenterEmptyView()
        let searchController = ArticleResearchFactory.make(coordinatorDelegate: coordinator, emptyView: emptyView)
        let viewController = HelpCenterViewController(interactor: interactor, articleResearchResultController: searchController)
        emptyView.delegate = viewController

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
