import Foundation

protocol HelpCenterPresenting: AnyObject {
    var viewController: HelpCenterDisplaying? { get set }
    func didNextStep(action: HelpCenterAction)
    func presentSearch()
    func presentError()
    func startLoading()
    func stopLoading()
    func present(
        sections: [HelpCenterTableModel.Section],
        articles: [Article],
        categories: [HelpCenterCategory]
    )
    func presentNewHome(
        sections: [HelpCenterTableModel.Section],
        response: HomeResponse
    )
}

final class HelpCenterPresenter {
    private let coordinator: HelpCenterCoordinating
    weak var viewController: HelpCenterDisplaying?

    init(coordinator: HelpCenterCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - HelpCenterPresenting
extension HelpCenterPresenter: HelpCenterPresenting {
    func didNextStep(action: HelpCenterAction) {
        coordinator.perform(action: action)
    }

    func presentNewHome(
        sections: [HelpCenterTableModel.Section],
        response: HomeResponse
    ) {
        sections.enumerated().forEach { order, section in
            self.display(
                items: present(section, response: response),
                in: order
            )
        }
    }
    
    func present(
        sections: [HelpCenterTableModel.Section],
        articles: [Article],
        categories: [HelpCenterCategory]
    ) {
        sections.enumerated().forEach { order, section in
            self.display(
                items: present(
                    section,
                    articles: articles,
                    categories: categories
                ),
                in: order
            )
        }
    }
    
    func presentSearch() {
        viewController?.displaySearch()
    }

    func presentError() {
        viewController?.displayError()
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
}

private extension HelpCenterPresenter {
    func display(items: [HelpCenterTableModel], in order: Int) {
        guard items.isNotEmpty else { return }
        viewController?.display(items: items, in: order)
    }
    
    func present(
        _ section: HelpCenterTableModel.Section,
        articles: [Article],
        categories: [HelpCenterCategory]
    ) -> [HelpCenterTableModel] {
        switch section {
        case .frequentlyAsked:
            return presentFequentlyAsked(questions: articles)
        case .helpCenterCategory:
            return presentCategories(categories)
        case .messageHistory:
            return presentMessageHistory()
        case .contactUs:
            return presentContactUs()
        case .greetings:
            return []
        }
    }
    
    func present(
        _ section: HelpCenterTableModel.Section,
        response: HomeResponse
    ) -> [HelpCenterTableModel] {
        switch section {
        case .frequentlyAsked:
            let articles = response.articles.map { $0.article }
            return presentFequentlyAsked(questions: articles)
        case .helpCenterCategory:
            return presentCategories(response.categories)
        case .messageHistory:
            return presentMessageHistory(with: response)
        case .contactUs:
            return presentContactUs(showMore: response.hadTickets)
        case .greetings:
            if response.username.isEmpty { return [] }
            return presentGreetings(user: response.username)
        }
    }
    
    func presentGreetings(user: String) -> [HelpCenterTableModel] {
        [
            HelpCenterTableModel(
                id: nil,
                title: Strings.HelpCenter.greetingsTitle(user),
                description: Strings.HelpCenter.greetingsSubtitle,
                section: .greetings,
                showMore: false,
                timestamp: nil
            )
        ]
    }
    
    func presentFequentlyAsked(questions: [Article]) -> [HelpCenterTableModel] {
        questions.map {
            HelpCenterTableModel(
                id: $0.id,
                title: $0.title,
                description: $0.description,
                section: .frequentlyAsked,
                showMore: false,
                timestamp: nil
            )
        }
    }

    func presentCategories(_ categories: [HelpCenterCategory]) -> [HelpCenterTableModel] {
        categories.map {
            HelpCenterTableModel(
                id: $0.id,
                title: $0.title,
                description: $0.description,
                section: .helpCenterCategory,
                showMore: false,
                timestamp: nil
            )
        }
    }

    func presentContactUs(showMore: Bool = false) -> [HelpCenterTableModel] {
        let contact = HelpCenterContactMessage()
        return [
            HelpCenterTableModel(
                id: nil,
                title: contact.title,
                description: contact.description,
                section: .contactUs,
                showMore: showMore,
                timestamp: nil
            )
        ]
    }

    func presentMessageHistory() -> [HelpCenterTableModel] {
        let history = HelpCenterMessageHistoryBody()
        return [
            HelpCenterTableModel(
                id: nil,
                title: history.title,
                description: history.description,
                section: .messageHistory,
                showMore: false,
                timestamp: ""
            )
        ]
    }
    
    func presentMessageHistory(with response: HomeResponse) -> [HelpCenterTableModel] {
        guard
            let lastTicket = response.ticketListSortedByDate.last,
            let mostRecentComment = lastTicket.commentListSortedByDate.last
        else { return [] }
        
        return [
            HelpCenterTableModel(
                id: nil,
                title: mostRecentComment.author.name,
                description: mostRecentComment.body,
                section: .messageHistory,
                showMore: response.hadTickets,
                timestamp: mostRecentComment.formattedDate
            )
        ]
    }
}
