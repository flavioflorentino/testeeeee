import Foundation

struct TableViewModel {
    var id: String?
    var title: String
    var description: String
}
