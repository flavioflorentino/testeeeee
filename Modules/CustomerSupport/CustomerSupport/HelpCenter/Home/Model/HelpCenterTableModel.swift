import Foundation

struct HelpCenterTableModel: HelpCenterObjectProtocol, Equatable {
    let id: Int?
    let title: String
    let description: String?
    let section: Section
    let showMore: Bool
    let timestamp: String?
    
    enum Section {
        case frequentlyAsked
        case helpCenterCategory
        case messageHistory
        case contactUs
        case greetings
    }
}
