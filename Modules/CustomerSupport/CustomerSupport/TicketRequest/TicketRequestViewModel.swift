import Foundation

protocol TicketRequestViewModelInputs: AnyObject {
    func createRequest(message: String?)
    func fetchFormSetup()
    func update(message: String?)
}

final class TicketRequestViewModel {
    private let dependencies: CustomerDependencies
    private let service: TicketRequestServicing
    private let presenter: TicketRequestPresenting
    private let reason: SupportReason

    private lazy var minimumCharacterMessageCount = dependencies.featureManager.int(.featureMinimumCharactersTicketMessage) ?? 25
    private let maximumCharacterMessageCount = 1_000

    init(dependencies: CustomerDependencies, service: TicketRequestServicing, presenter: TicketRequestPresenting, reason: SupportReason) {
        self.dependencies = dependencies
        self.service = service
        self.presenter = presenter
        self.reason = reason
    }
}

private extension TicketRequestViewModel {
    func verifyMinimumCharacterCount(message: String) -> Bool {
        message.count < minimumCharacterMessageCount
    }
}

// MARK: - TicketRequestViewModelInputs
extension TicketRequestViewModel: TicketRequestViewModelInputs {
    func fetchFormSetup() {
        presenter.presentFormConfiguration(title: reason.name, maximumCharacterTicket: maximumCharacterMessageCount)
        presenter.present(messageHint: Strings.RequestTicket.underTextViewMessage(minimumCharacterMessageCount))
    }
    
    func update(message: String?) {
        guard let message = message else {
            presenter.present(errorMessage: Strings.RequestTicket.emptyMessageError)
            return
        }
        
        if verifyMinimumCharacterCount(message: message) {
            presenter.present(errorMessage: Strings.RequestTicket.minimumCharacterCountError(minimumCharacterMessageCount))
        } else {
            presenter.present(messageHint: Strings.RequestTicket.underTextViewMessage(minimumCharacterMessageCount))
        }
        
        presenter.presentCharacterCount(for: message, maximumCharacterTicket: maximumCharacterMessageCount)
    }
    
    func createRequest(message: String?) {
        guard let message = message, !message.isEmpty else {
            presenter.present(errorMessage: Strings.RequestTicket.emptyMessageError)
            return
        }
        
        guard !verifyMinimumCharacterCount(message: message) else {
            presenter.present(errorMessage: Strings.RequestTicket.minimumCharacterCountError(minimumCharacterMessageCount))
            return
        }
        
        presenter.presentLoading()
        
        service.requestTicket(title: reason.name, message: message, tags: [reason.value]) { [weak self] result in
            switch result {
            case .success:
                self?.presenter.dismiss()
                 
            case .failure:
                self?.presenter.hideLoading()
                self?.presenter.present(errorMessage: Strings.RequestTicket.error)  
            }
        }
    }
}
