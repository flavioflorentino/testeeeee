import Foundation

enum TicketRequestFactory {
    static func make(reason: SupportReason) -> TicketRequestViewController {
        let container: CustomerDependencies = DependencyContainer()
        let service: TicketRequestServicing = TicketRequestService(dependencies: container)
        let coordinator: TicketRequestCoordinating = TicketRequestCoordinator()
        let presenter: TicketRequestPresenting = TicketRequestPresenter(coordinator: coordinator)
        let viewModel = TicketRequestViewModel(dependencies: container, service: service, presenter: presenter, reason: reason)
        let viewController = TicketRequestViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
