import Foundation

protocol TicketRequestPresenting: AnyObject {
    var viewController: TicketRequestDisplay? { get set }
    func dismiss()
    func present(messageHint: String)
    func present(errorMessage: String)
    func presentFormConfiguration(title: String, maximumCharacterTicket: Int)
    func presentCharacterCount(for message: String?, maximumCharacterTicket: Int)
    func presentLoading()
    func hideLoading()
    func didNextStep(action: TicketRequestAction)
}

final class TicketRequestPresenter {
    private let coordinator: TicketRequestCoordinating
    weak var viewController: TicketRequestDisplay?
    
    init(coordinator: TicketRequestCoordinating) {
        self.coordinator = coordinator
    }
}

private extension TicketRequestPresenter {
    func formatedCountCharacterMessage(maximumCharacterTicket: Int, for message: String? = nil) -> String {
        "\(message?.count ?? 0)/\(maximumCharacterTicket)"
    }
}

// MARK: - TicketRequestPresenting
extension TicketRequestPresenter: TicketRequestPresenting {
    func dismiss() {
        didNextStep(action: .close)
    }
    
    func present(messageHint: String) {
        viewController?.display(messageHint: messageHint)
    }
    
    func present(errorMessage: String) {
        viewController?.displayTextField(errorMessage: errorMessage)
    }
    
    func presentFormConfiguration(title: String, maximumCharacterTicket: Int) {
        viewController?.displayFormConfiguration(title: title, maximumCharacterTicket: maximumCharacterTicket)
        viewController?.displayCharacterCount(message: formatedCountCharacterMessage(maximumCharacterTicket: maximumCharacterTicket))
    }
    
    func presentCharacterCount(for message: String?, maximumCharacterTicket: Int) {
        viewController?.displayCharacterCount(message: formatedCountCharacterMessage(maximumCharacterTicket: maximumCharacterTicket, for: message))
    }
    
    func presentLoading() {
        viewController?.startLoadingView()
        viewController?.dismissKeyboard()
    }
    
    func hideLoading() {
        viewController?.stopLoadingView()
    }
    
    func didNextStep(action: TicketRequestAction) {
        coordinator.perform(action: action)
    }
}
