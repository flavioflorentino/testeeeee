import Foundation

protocol TicketRequestServicing {
    func requestTicket(title: String, message: String, tags: [String], completion: @escaping (Result<Void, Error>) -> Void)
}

final class TicketRequestService {
    private let dependencies: CustomerDependencies
    
    init(dependencies: CustomerDependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - TicketRequestServicing
extension TicketRequestService: TicketRequestServicing {
    func requestTicket(title: String, message: String, tags: [String], completion: @escaping (Result<Void, Error>) -> Void) {
        CustomerSupportManager.shared.thirdPartyInstance?.requestTicket(title: title, message: message, tags: tags, extraCustomFields: []) { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case .success:
                    completion(.success(()))
                case let .failure(error):
                    completion(.failure(error))
                }
            }
        }
    }
}
