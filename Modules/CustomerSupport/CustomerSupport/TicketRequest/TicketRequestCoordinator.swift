import Foundation
import UIKit

enum TicketRequestAction {
    case close
}

protocol TicketRequestCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: TicketRequestAction)
}

final class TicketRequestCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - TicketRequestCoordinating
extension TicketRequestCoordinator: TicketRequestCoordinating {
    func perform(action: TicketRequestAction) {
        guard case .close = action else {
           return
        }
        
        if viewController?.navigationController?.popToRootViewController(animated: true) == nil {
            viewController?.navigationController?.dismiss(animated: true)
        }
    }
}
