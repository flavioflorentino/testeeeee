import Foundation
import UI
import UIKit

protocol TicketRequestDisplay: AnyObject, LoadingViewProtocol {
    func displayTextField(errorMessage: String)
    func displayFormConfiguration(title: String, maximumCharacterTicket: Int)
    func displayCharacterCount(message: String?)
    func display(messageHint: String)
    func dismissKeyboard()
}

final class TicketRequestViewController: ViewController<TicketRequestViewModelInputs, UIView> {
    private var viewHasMoved = false
    private let messageTextColorDefault = Colors.black.color
    private let messageTextColorError = Colors.critical600.color
    
    // MARK: Header
    lazy var loadingView = LoadingView()
    
    private lazy var keyboardHandler = KeyboardScrollViewHandler(scrollView: scrollView)
    
    private lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.keyboardDismissMode = .interactive
        return scroll
    }()
    
    private lazy var rootStackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [
            headerStackView,
            messageStackView,
            confirmButton
        ])
        stack.axis = .vertical
        stack.spacing = Spacing.base03
        stack.compatibleLayoutMargins = .rootView
        stack.isLayoutMarginsRelativeArrangement = true
        return stack
    }()
    
    private lazy var headerStackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [
            titleLabel, bodyLabel
        ])
        stack.axis = .vertical
        stack.spacing = Spacing.base02
        return stack
    }()
    
    private lazy var messageStackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [
            messageTextView, underTextViewStack
        ])
        stack.axis = .vertical
        stack.spacing = Spacing.base00
        return stack
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.RequestTicket.headline
        label.numberOfLines = 0
        label
            .labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textColor, .black(.default))
        return label
    }()
    
    private lazy var bodyLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.RequestTicket.headline
        label.numberOfLines = 0
        label.labelStyle(BodyPrimaryLabelStyle())
        return label
    }()
    
    private lazy var messageTextView: TextView = {
        let textView = TextView()
        textView.backgroundColor = .clear
        textView.delegate = self
        textView.placeholderText = Strings.RequestTicket.messageTextPlaceholder
        textView.isScrollEnabled = false
        textView.isFloatingPlaceholder = true
        textView.isSeparatorVisible = true
        textView.font = Typography.bodyPrimary().font()
        return textView
    }()
    
    private lazy var underMessageTextViewLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
        return label
    }()
    
    private lazy var characterCountLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
        label.setContentHuggingPriority(.required, for: .horizontal)
        return label
    }()
    
    private lazy var underTextViewStack: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [
            underMessageTextViewLabel,
            characterCountLabel
        ])
        stack.axis = .horizontal
        return stack
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.RequestTicket.button, for: .normal)
        button
            .buttonStyle(PrimaryButtonStyle())
            .with(\.typography, .bodyPrimary())
            .with(\.textAlignment, .center)
        button.addTarget(self, action: #selector(didTapConfirm(_:)), for: .touchUpInside)
        return button
    }()
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.fetchFormSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        }
    }
    
    // MARK: View Configuration
    override func buildViewHierarchy() {
        scrollView.addSubview(rootStackView)
        view.addSubview(scrollView)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        rootStackView.snp.makeConstraints {
            $0.edges.width.equalToSuperview()
        }
    }
    
    override func configureViews() {
        title = Strings.RequestTicket.title
        view.tintColor = Colors.branding600.color
        view.backgroundColor = Colors.backgroundPrimary.color
        NavigationAppearanceHelper.applyAppearance(at: self, largeTitleDisplayMode: .always)
        setupTapRecognizer()
        keyboardHandler.registerForKeyboardNotifications()
    }
}

private extension TicketRequestViewController {
    func confirm() {
        viewModel.createRequest(message: messageTextView.text)
    }
    
    func setupTapRecognizer() {
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapScreen(_:)))
        view.addGestureRecognizer(tapRecognizer)
    }
}

@objc
private extension TicketRequestViewController {
    func didTapConfirm(_ sender: UIButton) {
        messageTextView.resignFirstResponder()
        confirm()
    }
    
    func didTapScreen(_ sender: UITapGestureRecognizer) {
        messageTextView.resignFirstResponder()
    }
}

// MARK: TicketRequestDisplay
extension TicketRequestViewController: TicketRequestDisplay {
    func display(messageHint: String) {
        underMessageTextViewLabel.textColor = messageTextColorDefault
        underMessageTextViewLabel.text = messageHint
    }
    
    func displayTextField(errorMessage: String) {
        underMessageTextViewLabel.textColor = messageTextColorError
        underMessageTextViewLabel.text = errorMessage
    }
    
    func displayFormConfiguration(title: String, maximumCharacterTicket: Int) {
        titleLabel.text = title
        
        messageTextView.textMask = CharacterLimitMask(maximumNumberOfCharacters: maximumCharacterTicket)
    }
    
    func displayCharacterCount(message: String?) {
        characterCountLabel.text = message
    }
    
    func dismissKeyboard() {
        messageTextView.resignFirstResponder()
    }
}

// MARK: UITextFieldDelegate
extension TicketRequestViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        viewModel.update(message: textView.text)
    }
}
