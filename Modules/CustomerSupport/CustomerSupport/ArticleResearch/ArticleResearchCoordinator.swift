import UIKit

enum ArticleResearchAction {
    case openArticle(id: String)
}

protocol ArticleResearchCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ArticleResearchAction)
}

protocol ArticleResearchCoordinatingDelegate: AnyObject {
    func perform(action: ArticleResearchAction)
}

final class ArticleResearchCoordinator {
    weak var viewController: UIViewController?
    weak var delegate: ArticleResearchCoordinatingDelegate?

    init(delegate: ArticleResearchCoordinatingDelegate) {
        self.delegate = delegate
    }
}

// MARK: - ArticleResearchCoordinating
extension ArticleResearchCoordinator: ArticleResearchCoordinating {
    func perform(action: ArticleResearchAction) {
        delegate?.perform(action: action)
    }
}
