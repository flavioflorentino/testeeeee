import UIKit

enum ArticleResearchFactory {
    static func make(coordinatorDelegate: ArticleResearchCoordinatingDelegate, emptyView: UIView = ArticleResearchEmptyView()) -> ArticleResearchViewController {
        let container = DependencyContainer()
        let coordinator: ArticleResearchCoordinating = ArticleResearchCoordinator(delegate: coordinatorDelegate)
        let presenter: ArticleResearchPresenting = ArticleResearchPresenter(coordinator: coordinator, emptyView: emptyView)
        let interactor = ArticleResearchInteractor(presenter: presenter, dependencies: container)
        let viewController = ArticleResearchViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
