import UI
import UIKit

protocol ArticleResearchDisplaying: AnyObject, LoadingViewProtocol {
    func display(articles: [Article])
    func append(articles: [Article])
    func idle()
    func display(emptyView: UIView)
}

private extension ArticleResearchViewController.Layout {
    enum FooterView {
        static let height: CGFloat = 60.0
    }
}

final class ArticleResearchViewController: ViewController<ArticleResearchInteracting, UIView> {
    fileprivate enum Layout { }

    lazy var loadingView = LoadingView()

    private lazy var fetchingMoreIndicator: UIActivityIndicatorView = {
        let activity = UIActivityIndicatorView()
        activity.isHidden = true
        return activity
    }()

    private lazy var footerView: UIView = {
        let frame = CGRect(origin: .zero, size: CGSize(width: tableView.frame.size.width, height: Layout.FooterView.height))
        let footer = UIView(frame: frame)
        footer.addSubview(fetchingMoreIndicator)
        fetchingMoreIndicator.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
        return footer
    }()

    private lazy var tableView: UITableView = {
        let table = UITableView()
        table.register(
            ArticleCell.self,
            forCellReuseIdentifier: ArticleCell.identifier
        )
        table.estimatedRowHeight = Spacing.base05
        table.rowHeight = UITableView.automaticDimension
        table.register(OnlyTitleCell.self, forCellReuseIdentifier: OnlyTitleCell.identifier)
        return table
    }()

    private lazy var dataSource: TableViewDataSource<Int, Article> = {
        let dataSource = TableViewDataSource<Int, Article>(view: tableView)
        dataSource.add(section: 0)
        dataSource.itemProvider = { tableView, indexPath, item -> UITableViewCell? in 
            guard let cell = tableView.dequeueReusableCell(withIdentifier: OnlyTitleCell.identifier, for: indexPath) as? OnlyTitleCell else {
                return UITableViewCell()
            }

            cell.setup(with: item)

            return cell
        }
        return dataSource
    }()

    private var searchResultArticles = [Article]()

    override func buildViewHierarchy() {
        view.addSubview(tableView)
    }

    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }

    override func configureViews() {
        tableView.tableFooterView = footerView
        tableView.dataSource = dataSource
        tableView.delegate = self
    }
}

private extension ArticleResearchViewController {
    func startFetchingMoreActivityIndicator() {
        fetchingMoreIndicator.startAnimating()
        fetchingMoreIndicator.isHidden = false
    }

    func stopFetchingMoreActivityIndicator() {
        fetchingMoreIndicator.stopAnimating()
        fetchingMoreIndicator.isHidden = true
    }
}

// MARK: TicketListResultDisplay
extension ArticleResearchViewController: ArticleResearchDisplaying {
    func display(articles: [Article]) {
        idle()
        dataSource.update(items: articles, from: 0)
    }

    func append(articles: [Article]) {
        idle()
        dataSource.add(items: articles, to: 0)
    }

    func idle() {
        tableView.backgroundView = nil
        stopFetchingMoreActivityIndicator()
    }

    func display(emptyView: UIView) {
        tableView.backgroundView = emptyView
        stopFetchingMoreActivityIndicator()
    }
}

extension ArticleResearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let minimum = searchResultArticles.count - 2
        if indexPath.row > minimum && indexPath.row > 0 {
            startFetchingMoreActivityIndicator()
            interactor.fetchMoreSearchResulsIfNeeded()
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let article = dataSource.item(at: indexPath) else {
            return
        }
        interactor.select(article: article)
    }
}

// MARK: - UISearch
extension ArticleResearchViewController: UISearchControllerDelegate {
    func didDismissSearchController(_ searchController: UISearchController) {
        interactor.clearSearchResult()
    }
}

extension ArticleResearchViewController: UISearchBarDelegate {
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        interactor.requestBeginEditing()
        return true
    }

    func searchBarResultsListButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text else {
            return
        }
        interactor.search(query: text)
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        interactor.clearSearchResult()
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let text = searchBar.text,
            text.count > 3 else {
            return
        }
        interactor.search(query: text)
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text else {
            return
        }
        interactor.search(query: text)
    }
}
