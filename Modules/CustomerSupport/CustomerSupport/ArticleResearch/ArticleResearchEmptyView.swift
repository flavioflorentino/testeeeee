import UIKit
import UI

extension ArticleResearchEmptyView.Layout {
    enum Image {
        static let size = CGSize(width: 128, height: 160)
    }
}

final class ArticleResearchEmptyView: UIView {
    fileprivate enum Layout {}

    private lazy var emptyImageView = UIImageView(image: Assets.empty.image)

    private lazy var title: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
            .with(\.textAlignment, .center)
        label.text = Strings.FaqSearchResult.title
        return label
    }()

    private lazy var subtitle: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textAlignment, .center)
            .with(\.textColor, Colors.grayscale600.color)
        label.text = Strings.FaqSearchResult.body
        return label
    }()

    private lazy var verticalStackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [
            title,
            subtitle
        ])
        stack.axis = .vertical
        stack.spacing = Spacing.base02
        return stack
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ArticleResearchEmptyView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(emptyImageView)
        addSubview(verticalStackView)
    }

    func setupConstraints() {
        emptyImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Image.size)
            $0.bottom.equalTo(verticalStackView.snp.top).offset(-Spacing.base02)
            $0.centerX.equalTo(verticalStackView.snp.centerX)
        }
        verticalStackView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.centerWithinMargins.equalToSuperview().offset(Spacing.base08)
        }
    }
}
