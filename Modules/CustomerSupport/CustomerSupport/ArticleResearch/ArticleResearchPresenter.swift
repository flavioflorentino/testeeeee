import Foundation

protocol ArticleResearchPresenting: AnyObject {
    var viewController: ArticleResearchDisplaying? { get set }
    func didNextStep(action: ArticleResearchAction)
    func presentError()
    func present(articles: [Article])
    func append(articles: [Article])
    func hideLoading()
    func presentLoading()
    func presentEmptyView()
    func presentIdle()
}

final class ArticleResearchPresenter {
    private let coordinator: ArticleResearchCoordinating
    private let emptyView: UIView
    weak var viewController: ArticleResearchDisplaying?

    init(coordinator: ArticleResearchCoordinating, emptyView: UIView) {
        self.coordinator = coordinator
        self.emptyView = emptyView
    }
}

// MARK: - ArticleResearchPresenting
extension ArticleResearchPresenter: ArticleResearchPresenting {
    func presentError() {
        viewController?.display(emptyView: emptyView)
    }

    func hideLoading() {
        viewController?.stopLoadingView()
    }

    func presentLoading() {
        viewController?.startLoadingView()
    }

    func presentEmptyView() {
        viewController?.display(emptyView: emptyView)
    }

    func presentIdle() {
        viewController?.idle()
    }

    func present(articles: [Article]) {
        viewController?.display(articles: articles)
    }

    func append(articles: [Article]) {
        viewController?.append(articles: articles)
    }

    func didNextStep(action: ArticleResearchAction) {
        coordinator.perform(action: action)
    }
}
