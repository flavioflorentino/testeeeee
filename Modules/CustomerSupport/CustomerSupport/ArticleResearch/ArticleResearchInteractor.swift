import AnalyticsModule
import FeatureFlag
import Foundation

protocol ArticleResearchInteracting: AnyObject {
    func cancelButtonTapped()
    func clearSearchResult()
    func search(query: String)
    func select(article: Article)
    func fetchMoreSearchResulsIfNeeded()
    func requestBeginEditing()
}

final class ArticleResearchInteractor {
    typealias Dependencies = HasZendesk & HasAnalytics & HasFeatureManager
    private let dependencies: Dependencies

    private let presenter: ArticleResearchPresenting

    private(set) var searchResultArticles = [Article]()
    private var articlesResearch: ArticlesResearch?

    private var hasResearchEnded = false

    init(presenter: ArticleResearchPresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

private extension ArticleResearchInteractor {
    func requestSearch() {
        guard !hasResearchEnded, let articlesResearch = articlesResearch else {
            presenter.presentIdle()
            return
        }
        dependencies.analytics.log(HelpCenterEvent.search(query: articlesResearch.query))
        dependencies.zendeskContainer?.search(articlesResearch) { [weak self] result in
            self?.presenter.hideLoading()
            switch result {
            case .success(let articleResult):
                self?.handleResult(articleResult)
            case .failure:
                self?.presenter.presentError()
            }
        }
    }

    func handleResult(_ result: ResultArticlesResearch) {
        articlesResearch?.page = result.page

        if result.page == 1 {
            if result.articles.isEmpty {
                presenter.presentEmptyView()
                return
            } else {
                presenter.present(articles: result.articles)
            }
        } else {
            presenter.append(articles: result.articles)
            hasResearchEnded = result.articles.isEmpty
        }
    }
}

// MARK: - ArticleResearchInteracting
extension ArticleResearchInteractor: ArticleResearchInteracting {
    func cancelButtonTapped() {
        clearSearchResult()
    }

    func clearSearchResult() {
        hasResearchEnded = false
        articlesResearch = nil
        searchResultArticles = []
        presenter.present(articles: [])
    }

    func select(article: Article) {
        presenter.didNextStep(action: .openArticle(id: "\(article.id)"))
    }

    func search(query: String) {
        presenter.present(articles: [])
        presenter.presentLoading()
        hasResearchEnded = false
        articlesResearch = ArticlesResearch(query: query)
        requestSearch()
    }

    func fetchMoreSearchResulsIfNeeded() {
        articlesResearch?.page += 1
        requestSearch()
    }

    func requestBeginEditing() {
        if dependencies.featureManager.isActive(.isNewFAQHomeEndpointAvailable) {
            let eventProperties = ButtonClickedEventProperties(screenName: .home, buttonName: .search)
            dependencies.analytics.log(HelpCenterNewEvents.buttonClicked(eventProperties))
        } else {
            dependencies.analytics.log(HelpCenterEvent.openSearch)
        }
    }
}
