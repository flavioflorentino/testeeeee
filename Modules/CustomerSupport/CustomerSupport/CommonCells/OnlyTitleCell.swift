import Foundation
import UI
import UIKit

extension OnlyTitleCell.Layout {
    enum Icon {
        static var size = CGSize(width: 20, height: 13)
    }
    enum Line {
        static var height: CGFloat = 1.0
    }
}

final class OnlyTitleCell: UITableViewCell {
    fileprivate enum Layout {}

    private lazy var titleLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.textColor, .grayscale600())
        return label
    }()

    private lazy var disclosureIndicator: UILabel = {
        let iconLabel = UILabel()
        iconLabel.text = Iconography.angleRightB.rawValue
        iconLabel.labelStyle(IconLabelStyle(type: .medium))
            .with(\.textColor, Colors.branding600.color)
        return iconLabel
    }()

    private lazy var bottomLine: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale100.color
        return view
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension OnlyTitleCell: HelpCenterCellProtocol {
    func setup(with helpCenterObject: HelpCenterObjectProtocol) {
        titleLabel.text = helpCenterObject.title
    }
    func setup(with helpCenterListObject: HelpCenterListModel) {
        titleLabel.text = helpCenterListObject.title
    }
}

extension OnlyTitleCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubviews(titleLabel, disclosureIndicator, bottomLine)
    }

    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.leading.top.equalToSuperview().offset(Spacing.base02)
            $0.bottom.equalTo(bottomLine.snp.top).offset(-Spacing.base02)
        }
        disclosureIndicator.snp.makeConstraints {
            $0.centerY.equalTo(titleLabel)
            $0.leading.equalTo(titleLabel.snp.trailing).offset(Spacing.base01)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.size.equalTo(Layout.Icon.size)
        }
        bottomLine.snp.makeConstraints {
            $0.bottom.equalToSuperview()
            $0.leading.equalTo(contentView).offset(Spacing.base02)
            $0.trailing.equalTo(contentView).offset(-Spacing.base02)
            $0.height.equalTo(Layout.Line.height)
        }
    }
}
