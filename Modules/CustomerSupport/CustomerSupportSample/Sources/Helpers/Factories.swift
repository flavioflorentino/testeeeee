import CustomerSupport

final class Factories {
    enum Options {
        case home(delegate: HomeCoordinatorDelegate?)
        case faq
        case tickets
        case chatBot
        case helpCenter
        case list(id: Int)
        case customerSupportFlow(reasonTag: String?)
    }
    
    static func controller(from options: Options) -> UIViewController {
        switch options {
        case .home(let delegate):
            return HomeFactory.make(delegate)
        case .faq:
            return FAQFactory.make()
        case .tickets:
            return TicketListFactory.make()
        case .chatBot:
            return ChatBotFactory.make(urlPath: "https://cdn.picpay.com/Chatbot/bot.html")
        case .helpCenter:
            return HelpCenterFactory.make()
        case .list(let id):
            let sectionList = HelpCenterListFactory.makeSectionListFrom(categoryId: id, withTitle: "Testando")
            return UINavigationController(rootViewController: sectionList)
        case .customerSupportFlow(let reasonTag):
            return CustomerSupportFlowFactory.makeTicketRequest(reasonTag: reasonTag)
        }
    }
}
