import Core

typealias CustomerSuportDependencies = HasNoDependency & HasMainQueue

final class DependencyContainer: CustomerSuportDependencies {
    lazy var mainQueue = DispatchQueue.main
}
