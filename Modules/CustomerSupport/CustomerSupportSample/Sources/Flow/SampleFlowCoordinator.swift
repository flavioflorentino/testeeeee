import CustomerSupport
import UI
import UIKit

final class SampleFlowCoordinator: Coordinating {
    private let navigationController: UINavigationController = {
        let navigation = UINavigationController()
        navigation.navigationBar.largeTitleTextAttributes = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 28.0, weight: .bold)
        ]
        navigation.navigationBar.tintColor = Colors.branding600.color
        return navigation
    }()
    
    var childViewController: [UIViewController] = []
    var viewController: UIViewController?
    
    func start() {
        let controller = HomeFactory.make(self)
        childViewController.append(navigationController)
        childViewController.append(controller)
        navigationController.setViewControllers([controller], animated: false)
    }
    
    private func push(option: Factories.Options) {
        let controller = Factories.controller(from: option)
        childViewController.append(controller)
        navigationController.pushViewController(controller, animated: true)
    }
    
    private func present(option: Factories.Options) {
        var controller = Factories.controller(from: option)
        switch option {
        case .helpCenter, .chatBot:
            controller = UINavigationController(rootViewController: controller)
        default:
            break
        }
        childViewController.append(controller)
        navigationController.present(controller, animated: true)
    }
    
    func dismiss(controller: UIViewController) {
        childViewController.removeAll(where: { $0 == controller })
        
        if navigationController.topViewController == controller {
            navigationController.popViewController(animated: true)
        } else {
            navigationController.popToViewController(controller, animated: true)
        }
    }
}

extension SampleFlowCoordinator: HomeCoordinatorDelegate {
    func didRequestPresentToFactory(option: Factories.Options) {
        present(option: option)
    }
    
    func didRequestPushToFactory(option: Factories.Options) {
        push(option: option)
    }
}
