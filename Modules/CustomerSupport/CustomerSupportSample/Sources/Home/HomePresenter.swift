import CustomerSupport

protocol HomePresenting: AnyObject {
    var viewController: HomeDisplay? { get set }
    func display(options: [HomeScreenOptionModel])
    func didNextStep(action: HomeAction)
}

final class HomePresenter: HomePresenting {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let coordinator: HomeCoordinating
    weak var viewController: HomeDisplay?

    init(coordinator: HomeCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
    
    func display(options: [HomeScreenOptionModel]) {
        viewController?.display(options: options)
    }
    
    func didNextStep(action: HomeAction) {
        coordinator.perform(action: action)
    }
}
