import CustomerSupport
import UI
import UIKit

protocol HomeDisplay: AnyObject {
    func display(options: [HomeScreenOptionModel])
}

private extension HomeViewController.Layout {
    enum Size {
        static let imageHeight: CGFloat = 90.0
    }
}

final class HomeViewController: ViewController<HomeInteractorInputs, UIView> {
    fileprivate enum Layout { }
    
    lazy var tableView = UITableView(frame: .zero, style: .plain)
    
    private var dataSource: TableViewHandler<String, HomeScreenOptionModel, UITableViewCell>?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        interactor.displayOptions()
    }
    
    public override func buildViewHierarchy() {
        view.addSubview(tableView)
    }
    
    public override func configureViews() {
        tableView.tableFooterView = UIView()
    }
    
    public override func setupConstraints() {
        tableView.layout {
            $0.leading == view.leadingAnchor
            $0.top == view.topAnchor
            $0.trailing == view.trailingAnchor
            $0.bottom == view.bottomAnchor
        }
    }
    
    public override func configureStyles() {}
}

// MARK: View Model Outputs
extension HomeViewController: HomeDisplay {
    func display(options: [HomeScreenOptionModel]) {
        tableView.register(
            UITableViewCell.self,
            forCellReuseIdentifier: String(describing: UITableViewCell.self)
        )
        
        dataSource = TableViewHandler(
            data: [Section(items: options)],
            configureCell: { _, item, cell in
                cell.textLabel?.text = item.name
            }
        )
        
        tableView.dataSource = dataSource
        tableView.delegate = self
        tableView.reloadData()
    }
}

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        interactor.select(index: indexPath.row)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
