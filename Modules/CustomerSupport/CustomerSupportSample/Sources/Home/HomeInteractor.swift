import CustomerSupport
import Foundation

protocol HomeInteractorInputs: AnyObject {
    func displayOptions()
    func select(index: Int)
}

final class HomeInteractor {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies
    
    private let presenter: HomePresenting
    
    private let options: [HomeScreenOptionModel] = [
        HomeScreenOptionModel(name: "FAQ", presentationMode: .push, factory: .faq),
        HomeScreenOptionModel(name: "Tickets", presentationMode: .push, factory: .tickets),
        HomeScreenOptionModel(name: "ChatBot", presentationMode: .present, factory: .chatBot),
        HomeScreenOptionModel(name: "HelpCenter", presentationMode: .present, factory: .helpCenter),
        HomeScreenOptionModel(name: "Section List", presentationMode: .present, factory: .list(id: 360003611612)),
        HomeScreenOptionModel(name: "Flow | Support Reason List", presentationMode: .push, factory: .customerSupportFlow(reasonTag: nil)),
        HomeScreenOptionModel(name: "Flow | Request Ticket", presentationMode: .push, factory: .customerSupportFlow(reasonTag: "clientepf__adicionar_dinheiro"))
    ]

    init(presenter: HomePresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

extension HomeInteractor: HomeInteractorInputs {
    func displayOptions() {
        presenter.display(options: options)
    }
    
    func select(index: Int) {
        let option = options[index]
        switch option.presentationMode {
        case .present:
            presenter.didNextStep(action: .present(to: option.factory))
        case .push:
            presenter.didNextStep(action: .push(to: option.factory))
        }
    }
}
