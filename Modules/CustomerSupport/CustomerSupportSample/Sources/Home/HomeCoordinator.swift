import CustomerSupport
import UIKit

enum HomeAction {
    case push(to: Factories.Options)
    case present(to: Factories.Options)
}

protocol HomeCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: HomeCoordinatorDelegate? { get set }
    func perform(action: HomeAction)
}

protocol HomeCoordinatorDelegate: AnyObject {
    func didRequestPushToFactory(option: Factories.Options)
    func didRequestPresentToFactory(option: Factories.Options)
}

final class HomeCoordinator: HomeCoordinating {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    weak var delegate: HomeCoordinatorDelegate?
    weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func perform(action: HomeAction) {
        switch action {
        case .push(let option):
            delegate?.didRequestPushToFactory(option: option)
        case .present(let option):
            delegate?.didRequestPresentToFactory(option: option)
        }
    }
}
