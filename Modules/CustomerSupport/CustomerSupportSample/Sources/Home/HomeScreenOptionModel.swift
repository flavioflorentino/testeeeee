import CustomerSupport

struct HomeScreenOptionModel {
    let name: String
    let presentationMode: PresentationMode
    let factory: Factories.Options
}

enum PresentationMode {
    case push
    case present
}
