import CustomerSupport

final class HomeFactory {
    static func make() -> UIViewController {
        HomeFactory.make(nil)
    }
    
    static func make(_ coordinatorDelegate: HomeCoordinatorDelegate?) -> UIViewController {
        let container = DependencyContainer()
        let coordinator: HomeCoordinating = HomeCoordinator(dependencies: container)
        coordinator.delegate = coordinatorDelegate
        let presenter: HomePresenting = HomePresenter(coordinator: coordinator, dependencies: container)
        let interactor = HomeInteractor(presenter: presenter, dependencies: container)
        let viewController = HomeViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
