import CustomerSupport
import Foundation
import SupportSDK
import ZendeskCoreSDK

final class SampleCustomerSupportWrapper: ThirdPartySupportSDKContract {
    lazy var requestProvider = ZDKRequestProvider()
    lazy var helpCenterProvider = ZDKHelpCenterProvider()
    
    private var url: String {
        return "https://picpay21589831114.zendesk.com"
    }
    
    private func zendeskID(for id: String) -> String {
        "consumer_\(id)"
    }
    
    public func activate(appId: String, clientId: String) {
        Zendesk.initialize(
            appId: appId,
            clientId: clientId,
            zendeskUrl: url
        )
        Support.initialize(withZendesk: Zendesk.instance)
    }
    
    public func registerJWT(by userId: String) {
        let identity = Identity.createJwt(token: zendeskID(for: userId))
        Zendesk.instance?.setIdentity(identity)
    }
    
    public func registerAnonymous() {
        let identity = Identity.createAnonymous()
        Zendesk.instance?.setIdentity(identity)
    }
    
    public func register(notificationToken: String) {
        guard let zendeskInstance = Zendesk.instance else {
            assertionFailure("Zendesk Instance is null")
            return
        }
        let locale = Locale.preferredLanguages.first ?? "pt-BR"
        ZDKPushProvider(zendesk: zendeskInstance).register(deviceIdentifier: zendeskID(for: notificationToken), locale: locale) { _, error in
            if let error = error {
                assertionFailure("Couldn't register device: \(notificationToken). Error: \(error.localizedDescription)")
            }
        }
    }
    
    public func setDebugMode(enabled: Bool) {
        CoreLogger.enabled = enabled
    }
    
    public func setLocalizableFile(fileName: String) {
        ZDKLocalization.registerTableName(fileName)
    }
    
    // MARK: Show Functions
    func buildFAQController(option: FAQOptions) -> UIViewController {
        switch option {
        case .home:
            return HelpCenterUi.buildHelpCenterOverviewUi()
        case .section(let id):
            let helpCenterConfiguration = HelpCenterUiConfiguration()
            helpCenterConfiguration.groupType = .section
            return faqController(configuration: helpCenterConfiguration, id: id)
        case .category(let id):
            let helpCenterConfiguration = HelpCenterUiConfiguration()
            helpCenterConfiguration.groupType = .category
            return faqController(configuration: helpCenterConfiguration, id: id)
        case .article(let id):
            return HelpCenterUi.buildHelpCenterArticleUi(withArticleId: id)
        }
    }
    
    private func faqController(configuration: HelpCenterUiConfiguration, id: String) -> UIViewController {
        guard let intId = Int(id) else {
            return HelpCenterUi.buildHelpCenterOverviewUi()
        }
        let numberId = NSNumber(value: intId)
        configuration.groupIds = [numberId]
        return HelpCenterUi.buildHelpCenterOverviewUi(withConfigs: [configuration])
    }
    
    public func buildTicketListController(ticketId: String?) -> UIViewController {
        guard let ticketId = ticketId else {
            return RequestUi.buildRequestList()
        }
        return RequestUi.buildRequestUi(requestId: ticketId)
    }
    
    // MARK: APIProviders
    
    /// Create a Ticket
    /// - Parameters:
    ///   - title: Ticket title
    ///   - message: message to be inside the ticket
    ///   - tags: Array of tags which can be used in a ticket
    ///   - completion: result from request with an only erro paramete which if is null there is no error.
    public func requestTicket(
        title: String,
        message: String,
        tags: [String],
        extraCustomFields: [TicketCustomField],
        completion: @escaping (Result<Void, Error>) -> Void
    ) {
        let request = ZDKCreateRequest()
        request.subject = title
        request.requestDescription = message
        request.tags = tags

        let mappedCustomFields = extraCustomFields.compactMap { CustomField(fieldId: Int64($0.id), value: $0.value) }
        request.customFields = mappedCustomFields

        let provider = ZDKRequestProvider()
        provider.createRequest(request) { _, error in
            if let error = error {
                completion(.failure(error))
            } else {
                completion(.success(()))
            }
        }
    }
    

    /// Verify if is any ticket open, based on *subjects*
    /// - Parameters:
    ///   - subjects: A list of subjects which a ticket can be inserted it
    ///   - completion: result with ticketId. If the result is nil. It was not any Ticket found it.
    public func isAnyTicketOpen(_ subjects: [String], completion: @escaping (_ ticketId: String?) -> Void) {
        let provider = ZDKRequestProvider()
        let notSolvedStatus = ["new", "open", "pending", "hold"]
        provider.getAllRequests { response, _ in
            let firstTicket = response?.requests.first { ticket in
                //Verifica se o ticket cumpri com os status
                guard notSolvedStatus.contains(ticket.status) else {
                    return false
                }

                guard !subjects.isEmpty else {
                    //Se a lista de Subjects ta vazia é considerado que se quer todos os assuntos, por isso ja retorna verdadeiro.
                    return true
                }

                guard let subject = ticket.subject else {
                    return false
                }

                return subjects.contains(subject)
            }
            completion(firstTicket?.requestId)
        }
    }
    
    func search(_ searchArticle: ArticlesResearch, completion: @escaping (Result<ResultArticlesResearch, Error>) -> Void) {
        let search = ZDKHelpCenterSearch()
        search.query = searchArticle.query
        search.page = NSNumber(value: searchArticle.page)
        helpCenterProvider.searchArticles(search) { arrayObjectResult, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let zendeskArticles = arrayObjectResult as? [ZDKHelpCenterArticle] else {
                let result = ResultArticlesResearch(articles: [], page: search.page.intValue)
                completion(.success(result))
                return
            }
            
            let articles = zendeskArticles.map {
                Article(id: $0.identifier.intValue, title: $0.title, description: $0.articleParents)
            }
            
            let result = ResultArticlesResearch(articles: articles, page: search.page.intValue)
            completion(.success(result))
        }
    }

    func articles(labels: [String], completion: @escaping (Result<ResultArticlesResearch, Error>) -> Void) {
        helpCenterProvider.getArticlesByLabels(labels) { arrayObjectResult, error in
            if let error = error {
                completion(.failure(error))
                return
            }

            guard let zendeskArticles = arrayObjectResult as? [ZDKHelpCenterArticle] else {
                let result = ResultArticlesResearch(articles: [], page: 0)
                completion(.success(result))
                return
            }

            let articles = zendeskArticles.map {
                Article(id: $0.identifier.intValue, title: $0.title, description: $0.articleParents)
            }

            let result = ResultArticlesResearch(articles: articles, page: 0)
            completion(.success(result))
        }
    }

    func articleBy(query: String) -> FAQOptions {
        .home
    }
}
