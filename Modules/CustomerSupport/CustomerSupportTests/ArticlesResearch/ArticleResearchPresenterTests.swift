@testable import CustomerSupport
import Core
import Foundation
import FeatureFlag
import UI
import XCTest

private final class ArticleResearchCoordinatSpy: ArticleResearchCoordinating {
    private(set) var diCallPerformAction = 0
    private(set) var action: ArticleResearchAction?
    var viewController: UIViewController? = UIViewController()

    func perform(action: ArticleResearchAction) {
        diCallPerformAction += 1
        self.action = action
    }
}

private class ArticleResearchDisplayingSpy: ArticleResearchDisplaying {
    var loadingView: LoadingView = LoadingView()

    //MARK: - startLoadingView
    private(set) var startLoadingViewCallsCount = 0

    func startLoadingView() {
        startLoadingViewCallsCount += 1
    }

    //MARK: - stopLoadingView
    private(set) var stopLoadingViewCallsCount = 0

    func stopLoadingView() {
        stopLoadingViewCallsCount += 1
    }


    //MARK: - display
    private(set) var displayArticlesCallsCount = 0
    private(set) var displayArticlesReceivedInvocations: [[Article]] = []

    func display(articles: [Article]) {
        displayArticlesCallsCount += 1
        displayArticlesReceivedInvocations.append(articles)
    }

    //MARK: - append
    private(set) var appendArticlesCallsCount = 0
    private(set) var appendArticlesReceivedInvocations: [[Article]] = []

    func append(articles: [Article]) {
        appendArticlesCallsCount += 1
        appendArticlesReceivedInvocations.append(articles)
    }

    //MARK: - idle
    private(set) var idleCallsCount = 0

    func idle() {
        idleCallsCount += 1
    }

    //MARK: - displayEmptyView
    private(set) var displayEmptyViewCallsCount = 0
    private(set) var appendEmptyViewInvocations: [UIView] = []

    func display(emptyView: UIView) {
        displayEmptyViewCallsCount += 1
        appendEmptyViewInvocations.append(emptyView)
    }
}

final class ArticleResearchPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = ArticleResearchCoordinatSpy()
    private lazy var displaySpy = ArticleResearchDisplayingSpy()
    private lazy var sut: ArticleResearchPresenter = {
        let presenter = ArticleResearchPresenter(coordinator: coordinatorSpy, emptyView: UIView())
        presenter.viewController = displaySpy
        return presenter
    }()

    func testPresentErrorMessage_WhenCalledFromInteractor_ShouldCallDisplayEmptyViewOneTime() {
        sut.presentError()

        XCTAssertEqual(displaySpy.displayEmptyViewCallsCount, 1)
    }

    func testPresentIdle_WhenCalledFromInteractor_ShouldCallDisplayEmptyViewOneTime() {
        sut.presentIdle()

        XCTAssertEqual(displaySpy.idleCallsCount, 1)
    }

    func testPresentLoading_WhenCalledFromInteractor_ShouldCallDisplayEmptyViewOneTime() {
        sut.presentLoading()

        XCTAssertEqual(displaySpy.startLoadingViewCallsCount, 1)
    }

    func testHideLoading_WhenCalledFromInteractor_ShouldCallDisplayEmptyViewOneTime() {
        sut.hideLoading()

        XCTAssertEqual(displaySpy.stopLoadingViewCallsCount, 1)
    }

    func testPresentEmptyView_WhenCalledFromInteractor_ShouldCallDisplayEmptyViewOneTime() {
        sut.presentEmptyView()

        XCTAssertEqual(displaySpy.displayEmptyViewCallsCount, 1)
    }

    func testPresentArticles_WhenCalledFromInteractor_ShouldCallDisplayEmptyViewOneTime() {
        let articles = [
            Article(id: 0, title: "Article 0"),
            Article(id: 1, title: "Article 1"),
            Article(id: 2, title: "Article 2")
        ]
        sut.present(articles: articles)

        XCTAssertEqual(displaySpy.displayArticlesCallsCount, 1)
        XCTAssert(displaySpy.displayArticlesReceivedInvocations.contains(articles))
    }

    func testAppendArticles_WhenCalledFromInteractor_ShouldCallDisplayEmptyViewOneTime() {
        let articles = [
            Article(id: 0, title: "Article 0"),
            Article(id: 1, title: "Article 1"),
            Article(id: 2, title: "Article 2")
        ]
        sut.append(articles: articles)

        XCTAssertEqual(displaySpy.appendArticlesCallsCount, 1)
        XCTAssert(displaySpy.appendArticlesReceivedInvocations.contains(articles))
    }

    func testDidNextStop_WhenCalledFromInteractor_ShouldCallDisplayEmptyViewOneTime() {
        let action = ArticleResearchAction.openArticle(id: "an id")
        sut.didNextStep(action: action)

        XCTAssertEqual(coordinatorSpy.diCallPerformAction, 1)
    }
}
