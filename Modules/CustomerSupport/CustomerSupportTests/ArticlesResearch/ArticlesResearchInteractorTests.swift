@testable import CustomerSupport
import AnalyticsModule
import Core
import Foundation
import FeatureFlag
import XCTest

private class ArticleResearchPresentingSpy: ArticleResearchPresenting {
    var viewController: ArticleResearchDisplaying?

    //MARK: - didNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didNextStepActionReceivedInvocations: [ArticleResearchAction] = []

    func didNextStep(action: ArticleResearchAction) {
        didNextStepActionCallsCount += 1
        didNextStepActionReceivedInvocations.append(action)
    }

    //MARK: - presentError
    private(set) var presentErrorMessageCallsCount = 0

    func presentError() {
        presentErrorMessageCallsCount += 1
    }

    //MARK: - present
    private(set) var presentArticlesCallsCount = 0
    private(set) var presentArticlesReceivedInvocations: [[Article]] = []

    func present(articles: [Article]) {
        presentArticlesCallsCount += 1
        presentArticlesReceivedInvocations.append(articles)
    }

    //MARK: - append
    private(set) var appendArticlesCallsCount = 0
    private(set) var appendArticlesReceivedInvocations: [[Article]] = []

    func append(articles: [Article]) {
        appendArticlesCallsCount += 1
        appendArticlesReceivedInvocations.append(articles)
    }

    //MARK: - hideLoading
    private(set) var hideLoadingCallsCount = 0

    func hideLoading() {
        hideLoadingCallsCount += 1
    }

    //MARK: - presentLoading
    private(set) var presentLoadingCallsCount = 0

    func presentLoading() {
        presentLoadingCallsCount += 1
    }

    //MARK: - presentEmptyView
    private(set) var presentEmptyViewCallsCount = 0

    func presentEmptyView() {
        presentEmptyViewCallsCount += 1
    }

    //MARK: - presentIdle
    private(set) var presentIdleCallsCount = 0

    func presentIdle() {
        presentIdleCallsCount += 1
    }
}

final class ArticlesResearchInteractorTests: XCTestCase {
    private lazy var wrapperSpy = ZendeskWrapperMock()
    private lazy var analyticsSpy = AnalyticsSpy()
    private lazy var featureManagerSpy = FeatureManagerMock()
    private lazy var containerStub = CustomerSupportContainerMock(
        wrapper: wrapperSpy,
        analytics: analyticsSpy,
        featureManager: featureManagerSpy
    )
    private lazy var spy = ArticleResearchPresentingSpy()
    private lazy var sut = ArticleResearchInteractor(presenter: spy, dependencies: containerStub)

    func testCancelButtonTapped_WhenCalledFromController_ShouldCallPresentThirdPartyOneTime() {
        sut.cancelButtonTapped()

        XCTAssertEqual(spy.presentArticlesCallsCount, 1)
        XCTAssertEqual(spy.presentArticlesReceivedInvocations.count, 1)
    }

    func testClearSearchResult_WhenCalledFromController_ShouldCallPresentThirdPartyOneTime() {
        sut.clearSearchResult()

        XCTAssertEqual(spy.presentArticlesCallsCount, 1)
        XCTAssertEqual(spy.presentArticlesReceivedInvocations.count, 1)
    }

    func testSearchQuery_WhenSearchIsSuccessfullyAndItHasOnePage_ShouldCallPresentArtciclesTwiceAndLoadingAndHidingJustOneTime() {
        let articles = [
            Article(id: 0, title: "Article 0"),
            Article(id: 1, title: "Article 1"),
            Article(id: 2, title: "Article 2")
        ]
        let articleResearchResult = ResultArticlesResearch(articles: articles, page: 1)
        wrapperSpy.searchCompletionResult = .success(articleResearchResult)

        sut.search(query: "Anything")

        XCTAssertEqual(spy.presentArticlesCallsCount, 2)
        XCTAssertEqual(spy.presentEmptyViewCallsCount, 0)
        XCTAssertEqual(spy.appendArticlesCallsCount, 0)
        XCTAssertEqual(spy.presentErrorMessageCallsCount, 0)
        XCTAssertEqual(spy.presentLoadingCallsCount, 1)
        XCTAssertEqual(spy.hideLoadingCallsCount, 1)
        XCTAssert(spy.presentArticlesReceivedInvocations.contains(articles))
    }

    func testSearchQuery_WhenSearchIsSuccessfullyAndItIsInTheSecondOnePage_ShouldCallPresentArticlesAndAppendArticlesAndLoadingAndHidingJustOneTime() {
        let articles = [
            Article(id: 0, title: "Article 0"),
            Article(id: 1, title: "Article 1"),
            Article(id: 2, title: "Article 2")
        ]
        let articleResearchResult = ResultArticlesResearch(articles: articles, page: 2)
        wrapperSpy.searchCompletionResult = .success(articleResearchResult)

        sut.search(query: "Anything")

        XCTAssertEqual(spy.presentArticlesCallsCount, 1)
        XCTAssertEqual(spy.appendArticlesCallsCount, 1)
        XCTAssertEqual(spy.presentEmptyViewCallsCount, 0)
        XCTAssertEqual(spy.presentErrorMessageCallsCount, 0)
        XCTAssertEqual(spy.presentLoadingCallsCount, 1)
        XCTAssertEqual(spy.hideLoadingCallsCount, 1)
        XCTAssertEqual(spy.presentArticlesReceivedInvocations.count, 1)
        XCTAssert(spy.appendArticlesReceivedInvocations.contains(articles))
    }

    func testSearchQuery_WhenSearchIsSuccessfullyButEmpty_ShouldCallPresentArticlesAndPresentEmptyViewAndLoadingAndHidingJustOneTime() {
        let articles = [Article]()
        let articleResearchResult = ResultArticlesResearch(articles: articles, page: 1)
        wrapperSpy.searchCompletionResult = .success(articleResearchResult)

        sut.search(query: "Anything")

        XCTAssertEqual(spy.presentArticlesCallsCount, 1)
        XCTAssertEqual(spy.presentEmptyViewCallsCount, 1)
        XCTAssertEqual(spy.appendArticlesCallsCount, 0)
        XCTAssertEqual(spy.presentErrorMessageCallsCount, 0)
        XCTAssertEqual(spy.presentLoadingCallsCount, 1)
        XCTAssertEqual(spy.hideLoadingCallsCount, 1)
        XCTAssertEqual(spy.presentArticlesReceivedInvocations.count, 1)
        XCTAssertEqual(spy.appendArticlesReceivedInvocations.count, 0)
    }

    func testSearchQuery_WhenSearchHasFailed_ShouldCallPresentErrorMessageAndLoadingAndHidingJustOneTime() {
        let apiError = ApiError.bodyNotFound
        wrapperSpy.searchCompletionResult = .failure(apiError)

        sut.search(query: "Anything")

        XCTAssertEqual(spy.presentArticlesCallsCount, 1)
        XCTAssertEqual(spy.appendArticlesCallsCount, 0)
        XCTAssertEqual(spy.presentErrorMessageCallsCount, 1)
        XCTAssertEqual(spy.presentLoadingCallsCount, 1)
        XCTAssertEqual(spy.hideLoadingCallsCount, 1)
        XCTAssertEqual(spy.presentArticlesReceivedInvocations.count, 1)
        XCTAssertEqual(spy.appendArticlesReceivedInvocations.count, 0)
    }
    
    func testRequestBeginEditing_WhenIsNewHome_ShouldTrackEvent() {
        featureManagerSpy.override(key: .isNewFAQHomeEndpointAvailable, with: true)
        let expectedProperties = ButtonClickedEventProperties(screenName: .home, buttonName: .search)
        
        sut.requestBeginEditing()
        
        XCTAssertTrue(
            analyticsSpy.equals(
                to: HelpCenterNewEvents.buttonClicked(expectedProperties).event()
            )
        )
    }
    
    func testRequestBeginEditing_WhenIsNotNewHome_ShouldTrackEvent() {
        featureManagerSpy.override(key: .isNewFAQHomeEndpointAvailable, with: false)
                
        sut.requestBeginEditing()
        
        XCTAssertTrue(
            analyticsSpy.equals(
                to: HelpCenterEvent.openSearch.event()
            )
        )
    }
}
