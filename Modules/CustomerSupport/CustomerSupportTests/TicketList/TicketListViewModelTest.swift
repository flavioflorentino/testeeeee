@testable import CustomerSupport
import AnalyticsModule
import FeatureFlag
import XCTest

fileprivate final class TicketListCoordinatorSpy: TicketListCoordinating {
    private(set) var action: TicketListAction?
    private(set) var didCallPerformAction = 0
    var viewController: UIViewController? = UIViewController()
    
    func perform(action: TicketListAction) {
        self.action = action
    }
}

fileprivate final class TicketListPresenterSpy: TicketListPresenting {
    private(set) var didCallPresentLoading = 0
    private(set) var didCallHideLoading = 0
    private(set) var didCallPresentThirdParty = 0
    private(set) var didCallDidNextStep = 0
    private(set) var TicketListController: UIViewController?
    private(set) var action: TicketListAction?
    
    var viewController: TicketListDisplay?
    
    func presentLoading() {
        didCallPresentLoading += 1
    }
    
    func hideLoading() {
        didCallHideLoading += 1
    }
    
    func presentThirdParty(ticketController: UIViewController) {
        didCallPresentThirdParty += 1
    }
    
    func didNextStep(action: TicketListAction) {
        self.action = action
        didCallDidNextStep += 1
    }
}

fileprivate final class DependecyContainerStub: CustomerDependencies {
    var analytics: AnalyticsProtocol = AnalyticsSpy()

    private(set) var didCallMainQueue = 0
    
    var mainQueue: DispatchQueue {
        didCallMainQueue += 1
        return DispatchQueue.main
    }
    
    var zendeskContainer: ThirdPartySupportSDKContract?
    
    var featureManager: FeatureManagerContract {
        FeatureManager.shared
    }
    
    init(wrapper: ThirdPartySupportSDKContract?) {
        self.zendeskContainer = wrapper
    }
}

final class TicketListInteractorTests: XCTestCase {
    private lazy var wrapperSpy = ZendeskWrapperMock()
    private lazy var containerStub = DependecyContainerStub(wrapper: wrapperSpy)
    private lazy var presenterSpy = TicketListPresenterSpy()
    private var ticketId: String?
    private lazy var sut = TicketListInteractor(dependencies: containerStub, presenter: presenterSpy, ticketId: ticketId)
    
    func testBuildThirdPartyTicketList_WhenCalledFromControllerWithoutID_ShouldCallPresentThirdPartyOneTime() {
        sut.buildThirdPartyTicketList()

        XCTAssertEqual(wrapperSpy.buildTicketId, nil)
        XCTAssertEqual(wrapperSpy.didCallBuildTicketListController, 1)
        XCTAssertEqual(presenterSpy.didCallPresentThirdParty, 1)
    }

    func testBuildThirdPartyTicketList_WhenCalledFromControllerWithID_ShouldCallPresentThirdPartyOneTime() {
        ticketId = "any ticket id"
        sut.buildThirdPartyTicketList()

        XCTAssertEqual(wrapperSpy.buildTicketId, ticketId)
        XCTAssertEqual(wrapperSpy.didCallBuildTicketListController, 1)
        XCTAssertEqual(presenterSpy.didCallPresentThirdParty, 1)
    }
    
    func testTicketButtonTapped_WhenCalledFromControllerWithTicket_ShouldSendTheActionTicketRequest() {
        sut.ticketButtonTapped()
        
        XCTAssertEqual(presenterSpy.didCallPresentLoading, 1)
        XCTAssertEqual(presenterSpy.didCallDidNextStep, 1)
        XCTAssertEqual(presenterSpy.action, TicketListAction.contactReasonList)
    }
}
