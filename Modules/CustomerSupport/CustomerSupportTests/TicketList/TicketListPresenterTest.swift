@testable import CustomerSupport
import UI
import XCTest

fileprivate final class TicketListCoordinatorSpy: TicketListCoordinating {
    private(set) var action: TicketListAction?
    private(set) var didCallPerformAction = 0
    var viewController: UIViewController? = UIViewController()
    
    func perform(action: TicketListAction) {
        self.action = action
    }
}

fileprivate final class TicketListViewControllerSpy: TicketListDisplay {
    var loadingView = LoadingView()
    
    private(set) var didDisplayThirdParty = 0
    private(set) var didStartLoadingView = 0
    private(set) var didStopLoadingView = 0
    private(set) var ticketController: UIViewController?
    
    func displayThirdParty(ticketController: UIViewController) {
        didDisplayThirdParty += 1
        self.ticketController = ticketController
    }
    
    func startLoadingView() {
        didStartLoadingView += 1
    }
    
    func stopLoadingView() {
        didStopLoadingView += 1
    }
}

final class TicketListPresenterTest: XCTestCase {
    private lazy var coordinator = TicketListCoordinatorSpy()
    private lazy var viewController = TicketListViewControllerSpy()
    private lazy var sut: TicketListPresenter = {
        let presenter = TicketListPresenter(coordinator: coordinator)
        presenter.viewController = viewController
        return presenter
    }()
    
    func testPresentThirdParty_WhenCalledFromViewModel_ShouldCallPresentThirdPartyOneTime() {
        let anyViewController = UIViewController()
        
        sut.presentThirdParty(ticketController: anyViewController)
        
        XCTAssertEqual(viewController.ticketController, anyViewController)
        XCTAssertEqual(viewController.didDisplayThirdParty, 1)
    }
}
