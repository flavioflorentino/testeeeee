@testable import CustomerSupport
import Foundation
import XCTest

final class RequestTicketByReasonDeeplinkResolverTests: XCTestCase {
    private lazy var viewControllerSpy = ViewControllerSpy()
    private lazy var sut = RequestTicketByReasonDeeplinkResolver()

    func testCanHandleUrl_WhenTheReasonDeeplinkIsCorrect_And_IsAuthenticated_ShouldReturnHandleable() throws {
        let deeplink = "picpay://picpay/helpcenter/reason/"
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.canHandle(url: url, isAuthenticated: true)

        XCTAssertEqual(result, .handleable)
    }

    func testCanHandleUrl_WhenTheReasonDeeplinkIsNotCorrect_And_IsAuthenticated_ShouldReturnHandleable() throws {
        let deeplink = "picpay://picpay/helpcenter/reason_not_correct/"
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.canHandle(url: url, isAuthenticated: true)

        XCTAssertEqual(result, .handleable)
    }

    func testCanHandleUrl_WhenTheReasonDeeplinkIsCorrect_And_IsNotAuthenticated_ShouldReturnHandleable() throws {
        let deeplink = "picpay://picpay/helpcenter/reason/"
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.canHandle(url: url, isAuthenticated: false)

        XCTAssertEqual(result, .onlyWithAuth)
    }

    func testCanHandleUrl_WhenTheReasonDeeplinkIsNotCorrect_And_IsNotAuthenticated_ShouldReturnHandleable() throws {
        let deeplink = "picpay://picpay/helpcenter/reason_not_correct/"
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.canHandle(url: url, isAuthenticated: false)

        XCTAssertEqual(result, .onlyWithAuth)
    }

    func testOpenURlFromViewController_WhenTheReasonDeeplinkIsCorrect_And_IsAuthenticated_ShouldReturnOnlyAuth() throws {
        let deeplink = "picpay://picpay/helpcenter/reason/"
        let url = try XCTUnwrap(URL(string: deeplink))

        let shouldOpen = sut.open(url: url, isAuthenticated: true, from: viewControllerSpy)

        let openedController = try XCTUnwrap(viewControllerSpy.presentViewControllerParameter)

        XCTAssertEqual(viewControllerSpy.didCallPresentViewController, 1)
        XCTAssertTrue(openedController.isKind(of: UINavigationController.self))
        XCTAssertTrue(shouldOpen)
    }
}
