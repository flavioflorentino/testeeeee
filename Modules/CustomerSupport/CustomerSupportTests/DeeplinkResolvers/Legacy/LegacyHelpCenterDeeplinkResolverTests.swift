@testable import CustomerSupport
import Foundation
import Core
import FeatureFlag
import XCTest

private class LegacyTicketDeeplinkResolverContainer: HasFeatureManager {
    var featureManager: FeatureManagerContract

    init(featureManager: FeatureManagerContract) {
        self.featureManager = featureManager
    }
}

final class LegacyTicketDeeplinkResolverTests: XCTestCase {
    private lazy var featureMock = FeatureManagerMock()
    private lazy var containerStub = LegacyTicketDeeplinkResolverContainer(featureManager: featureMock)
    private lazy var sut = LegacyTicketDeeplinkResolver(dependencies: containerStub)

    func testCanHandleUrl_WhenIsUnauthenticatedAndFeatureIsActivated_ShouldReturnOnlyAuth() throws {
        let deeplink = "picpay://picpay/hc/open/ticket"
        let url = try XCTUnwrap(URL(string: deeplink))
        let result = sut.canHandle(url: url, isAuthenticated: false)

        XCTAssertEqual(result, .onlyWithAuth)
    }

    func testCanHandleUrl_WhenIsAnValidURLAndIsAuthenticated_ShouldReturnOnlyAuth() throws {
        let deeplink = "picpay://picpay/hc/open/ticket"
        let url = try XCTUnwrap(URL(string: deeplink))
        let result = sut.canHandle(url: url, isAuthenticated: true)

        XCTAssertEqual(result, .handleable)
    }

    func testOpenURL_WhenIsAnValidURLWithoutIdAndIsNotAuthenticated_ShouldReturnOnlyAuth() throws {
        let deeplink = "picpay://picpay/hc/open/ticket"
        let url = try XCTUnwrap(URL(string: deeplink))
        let result = sut.open(url: url, isAuthenticated: false)

        XCTAssertEqual(result, false)
    }

    func testOpenURL_WhenIsAnValidURLWithoutIdAndIsAuthenticated_ShouldReturnOnlyAuth() throws {
        let deeplink = "picpay://picpay/hc/open/ticket"
        let url = try XCTUnwrap(URL(string: deeplink))
        let result = sut.open(url: url, isAuthenticated: true)

        XCTAssertEqual(result, true)
    }

    func testOpenURL_WhenIsAnValidURLWithIdAndIsAuthenticated_ShouldReturnOnlyAuth() throws {
        let id = 123456
        let deeplink = "picpay://picpay/hc/open/ticket/\(id)"
        let url = try XCTUnwrap(URL(string: deeplink))
        let result = sut.open(url: url, isAuthenticated: true)

        XCTAssertEqual(result, true)
    }
}
