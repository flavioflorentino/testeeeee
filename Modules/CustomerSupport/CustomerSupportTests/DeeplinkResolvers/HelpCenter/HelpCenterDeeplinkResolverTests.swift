@testable import CustomerSupport
import Foundation
import XCTest

final class HelpCenterDeeplinkResolverTests: XCTestCase {
    private lazy var viewControllerSpy = ViewControllerSpy()
    private lazy var sut = HelpcenterDeeplinkResolver()

    func testCanHandleUrl_WhenIsAnArticleURLAndIsAuthenticated_ShouldReturnOnlyAuth() throws {
        let deeplink = "picpay://picpay/helpcenter/article/"
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.canHandle(url: url, isAuthenticated: true)

        XCTAssertEqual(result, .handleable)
    }

    func testCanHandleUrl_WhenIsAnSectionURLAndIsAuthenticated_ShouldReturnOnlyAuth() throws {
        let deeplink = "picpay://picpay/helpcenter/section/"
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.canHandle(url: url, isAuthenticated: true)

        XCTAssertEqual(result, .handleable)
    }

    func testCanHandleUrl_WhenIsAnCategoryURLAndIsAuthenticated_ShouldReturnOnlyAuth() throws {
        let deeplink = "picpay://picpay/helpcenter/category/"
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.canHandle(url: url, isAuthenticated: true)

        XCTAssertEqual(result, .handleable)
    }

    func testCanHandleUrl_WhenIsAnArticleURLAndIsNotAuthenticated_ShouldReturnOnlyAuth() throws {
        let deeplink = "picpay://picpay/helpcenter/article/"
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.canHandle(url: url, isAuthenticated: false)

        XCTAssertEqual(result, .onlyWithAuth)
    }

    func testCanHandleUrl_WhenIsAnSectionURLAndIsNotAuthenticated_ShouldReturnOnlyAuth() throws {
        let deeplink = "picpay://picpay/helpcenter/section/"
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.canHandle(url: url, isAuthenticated: false)

        XCTAssertEqual(result, .onlyWithAuth)
    }

    func testCanHandleUrl_WhenIsAnCategoryURLAndIsNotAuthenticated_ShouldReturnOnlyAuth() throws {
        let deeplink = "picpay://picpay/helpcenter/category/"
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.canHandle(url: url, isAuthenticated: false)

        XCTAssertEqual(result, .onlyWithAuth)
    }

    func testOpenURlFromViewController_WhenIsAnArticleURLAndIsAuthenticated_ShouldReturnOnlyAuth() throws {
        let deeplink = "picpay://picpay/helpcenter/article/"
        let url = try XCTUnwrap(URL(string: deeplink))

        let shouldOpen = sut.open(url: url, isAuthenticated: true, from: viewControllerSpy)

        let openedController = try XCTUnwrap(viewControllerSpy.presentViewControllerParameter)

        XCTAssertEqual(viewControllerSpy.didCallPresentViewController, 1)
        XCTAssertTrue(openedController.isKind(of: UINavigationController.self))
        XCTAssertTrue(shouldOpen)
    }

    func testOpenURlFromViewController_WhenIsAnSectionURLAndIsAuthenticated_ShouldReturnOnlyAuth() throws {
        let deeplink = "picpay://picpay/helpcenter/article/"
        let url = try XCTUnwrap(URL(string: deeplink))

        let shouldOpen = sut.open(url: url, isAuthenticated: true, from: viewControllerSpy)

        let openedController = try XCTUnwrap(viewControllerSpy.presentViewControllerParameter)

        XCTAssertEqual(viewControllerSpy.didCallPresentViewController, 1)
        XCTAssertTrue(openedController.isKind(of: UINavigationController.self))
        XCTAssertTrue(shouldOpen)
    }

    func testOpenURlFromViewController_WhenIsAnCategoryURLAndIsAuthenticated_ShouldReturnOnlyAuth() throws {
        let deeplink = "picpay://picpay/helpcenter/article/"
        let url = try XCTUnwrap(URL(string: deeplink))

        let shouldOpen = sut.open(url: url, isAuthenticated: true, from: viewControllerSpy)

        let openedController = try XCTUnwrap(viewControllerSpy.presentViewControllerParameter)

        XCTAssertEqual(viewControllerSpy.didCallPresentViewController, 1)
        XCTAssertTrue(openedController.isKind(of: UINavigationController.self))
        XCTAssertTrue(shouldOpen)
    }
}
