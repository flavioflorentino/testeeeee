@testable import CustomerSupport
import Foundation
import XCTest

final class HelpcenterDeeplinkHelperTests: XCTestCase {
    lazy var sut = HelpcenterDeeplinkHelper.self

    // MARK: - IsAnyHelpcenterOption
    func testIsAnyHelpcenterOption_WhenIsAnMisspeledURL_ShouldReturnOnlyAuth() throws {
        let deeplink = "picpay://picpay/helpcent/article/"
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.isAnyHelpcenterOption(from: url)

        XCTAssertEqual(result, false)
    }

    func testIsAnyHelpcenterOption_WhenIsAnArticleURL_ShouldReturnOnlyAuth() throws {
        let deeplink = "picpay://picpay/helpcenter/article/"
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.isAnyHelpcenterOption(from: url)

        XCTAssertEqual(result, true)
    }

    func testIsAnyHelpcenterOption_WhenIsAnSectionURL_ShouldReturnOnlyAuth() throws {
        let deeplink = "picpay://picpay/helpcenter/section/"
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.isAnyHelpcenterOption(from: url)

        XCTAssertEqual(result, true)
    }

    func testIsAnyHelpcenterOption_WhenIsAnCategoryURL_ShouldReturnOnlyAuth() throws {
        let deeplink = "picpay://picpay/helpcenter/category/"
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.isAnyHelpcenterOption(from: url)

        XCTAssertEqual(result, true)
    }

    // MARK: - OptionFromComponents
    func testOptionFromComponents_WhenIsAnInvalidURL_ShouldReturnOnlyAuth() throws {
        let deeplink = "picpay://picpay/helpcenter/any_thing/"
        let url = try XCTUnwrap(URL(string: deeplink))
        let components = url.pathComponents

        let result = sut.option(from: components)

        XCTAssertEqual(result, nil)
    }

    func testOptionFromComponents_WhenIsAnArticleURL_ShouldReturnOnlyAuth() throws {
        let deeplink = "picpay://picpay/helpcenter/article/"
        let url = try XCTUnwrap(URL(string: deeplink))
        let components = url.pathComponents

        let result = sut.option(from: components)

        XCTAssertEqual(result, .article)
    }

    func testOptionFromComponents_WhenIsAnSectionURL_ShouldReturnOnlyAuth() throws {
        let deeplink = "picpay://picpay/helpcenter/section/"
        let url = try XCTUnwrap(URL(string: deeplink))
        let components = url.pathComponents

        let result = sut.option(from: components)

        XCTAssertEqual(result, .section)
    }

    func testOptionFromComponents_WhenIsAnCategoryURL_ShouldReturnOnlyAuth() throws {
        let deeplink = "picpay://picpay/helpcenter/category/"
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.option(from: url)

        XCTAssertEqual(result, .category)
    }

    // MARK: - OptionFromURL
    func testOptionFromURL_WhenIsAnInvalidURL_ShouldReturnOnlyAuth() throws {
        let deeplink = "picpay://picpay/helpcenter/any_thing/"
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.option(from: url)

        XCTAssertEqual(result, nil)
    }

    func testOptionFromURL_WhenIsAnArticleURL_ShouldReturnOnlyAuth() throws {
        let deeplink = "picpay://picpay/helpcenter/article/"
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.option(from: url)

        XCTAssertEqual(result, .article)
    }

    func testOptionFromURL_WhenIsAnSectionURL_ShouldReturnOnlyAuth() throws {
        let deeplink = "picpay://picpay/helpcenter/section/"
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.option(from: url)

        XCTAssertEqual(result, .section)
    }

    func testOptionFromURL_WhenIsAnCategoryURL_ShouldReturnOnlyAuth() throws {
        let deeplink = "picpay://picpay/helpcenter/category/"
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.option(from: url)

        XCTAssertEqual(result, .category)
    }

    // MARK: - IdFromQuery
    func testIdFromQuery_WhenIsAnInvalidURL_ShouldReturnOnlyAuth() throws {
        let id = 123456
        let deeplink = "picpay://picpay/helpcenter/any_thing?\(id)"
        let url = try XCTUnwrap(URL(string: deeplink))
        let query = try XCTUnwrap(url.query)

        let result = sut.idFrom(query: query)

        XCTAssertEqual(result, "\(id)")
    }

    func testIdFromQuery_WhenIsAnArticleURL_ShouldReturnOnlyAuth() throws {
        let id = 123456
        let deeplink = "picpay://picpay/helpcenter/article?\(id)"
        let url = try XCTUnwrap(URL(string: deeplink))
        let query = try XCTUnwrap(url.query)

        let result = sut.idFrom(query: query)

        XCTAssertEqual(result, "\(id)")
    }

    func testIdFromQuery_WhenIsAnSectionURL_ShouldReturnOnlyAuth() throws {
        let id = 123456
        let deeplink = "picpay://picpay/helpcenter/section?\(id)"
        let url = try XCTUnwrap(URL(string: deeplink))
        let query = try XCTUnwrap(url.query)

        let result = sut.idFrom(query: query)

        XCTAssertEqual(result, "\(id)")
    }

    func testIdFromQuery_WhenIsAnCategoryURL_ShouldReturnOnlyAuth() throws {
        let id = 123456
        let deeplink = "picpay://picpay/helpcenter/category?\(id)"
        let url = try XCTUnwrap(URL(string: deeplink))
        let query = try XCTUnwrap(url.query)

        let result = sut.idFrom(query: query)

        XCTAssertEqual(result, "\(id)")
    }

    // MARK: - IdFromURL
    func testIdFromURL_WhenIsAnInvalidURL_ShouldReturnOnlyAuth() throws {
        let id = 123456
        let deeplink = "picpay://picpay/helpcenter/any_thing/\(id)"
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.id(from: url)

        XCTAssertEqual(result, "\(id)")
    }

    func testIdFromURL_WhenIsAnArticleURL_ShouldReturnOnlyAuth() throws {
        let id = 123456
        let deeplink = "picpay://picpay/helpcenter/article/\(id)"
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.id(from: url)

        XCTAssertEqual(result, "\(id)")
    }

    func testIdFromURL_WhenIsAnSectionURL_ShouldReturnOnlyAuth() throws {
        let id = 123456
        let deeplink = "picpay://picpay/helpcenter/section/\(id)"
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.id(from: url)

        XCTAssertEqual(result, "\(id)")
    }

    func testIdFromURL_WhenIsAnCategoryURL_ShouldReturnOnlyAuth() throws {
        let id = 123456
        let deeplink = "picpay://picpay/helpcenter/category/\(id)"
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.id(from: url)

        XCTAssertEqual(result, "\(id)")
    }
}
