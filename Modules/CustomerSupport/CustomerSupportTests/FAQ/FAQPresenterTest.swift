@testable import CustomerSupport
import UI
import XCTest

fileprivate final class FAQCoordinatorSpy: FAQCoordinating {
    private(set) var action: FAQAction?
    var viewController: UIViewController? = UIViewController()
    
    func perform(action: FAQAction) {
        self.action = action
    }
}

fileprivate final class FAQViewControllerSpy: FAQDisplay {
    private(set) var fAQController: UIViewController?
    private(set) var didDisplayThirdParty = 0
    private(set) var didDisplaySearch = 0
    private(set) var didDisplayErrorMessage = 0
    
    private(set) var displayErrorMessage: String?
    
    func displayThirdParty(fAQController: UIViewController) {
        didDisplayThirdParty += 1
        self.fAQController = fAQController
    }
    
    func displaySearch() {
        didDisplaySearch += 1
    }
    
    func display(errorMessage: String) {
        didDisplayErrorMessage += 1
        
        displayErrorMessage = errorMessage
    }
}

fileprivate final class FAQSearchResultViewControllerSpy: FAQSearchResultDisplay {
    private(set) var loadingView = LoadingView()
    private(set) var didReloadArticles = 0
    private(set) var didIdle = 0
    private(set) var didEmptyView = 0
    private(set) var didStartLoadingView = 0
    private(set) var didStopLoadingView = 0
    
    func startLoadingView() {
        didStartLoadingView += 1
    }
    
    func stopLoadingView() {
        didStopLoadingView += 1
    }
    
    func reloadArticles() {
        didReloadArticles += 1
    }
    
    func idle() {
        didIdle += 1
    }
    
    func displayEmptyView() {
        didEmptyView += 1
    }
}

final class FAQPresenterTest: XCTestCase {
    private lazy var coordinatorSpy = FAQCoordinatorSpy()
    private lazy var viewControllerSpy = FAQViewControllerSpy()
    private lazy var resultSearchViewControllerSpy = FAQSearchResultViewControllerSpy()
    private lazy var sut: FAQPresenter = {
        let presenter = FAQPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        presenter.searchResultController = resultSearchViewControllerSpy
        return presenter
    }()
    
    func testPresentThirdParty_WhenCalledFromViewModel_ShouldCallPresentThirdPartyOneTime() {
        let anyViewController = UIViewController()
        
        sut.presentThirdParty(fAQController: anyViewController)
        
        XCTAssertEqual(viewControllerSpy.fAQController, anyViewController)
        XCTAssertEqual(viewControllerSpy.didDisplayThirdParty, 1)
    }
    
    func testPresentSearchBar_WhenCalledFromViewModel_ShouldCallDisplaySearchOneTime() {
        sut.presentSearchBar()
        
        XCTAssertEqual(viewControllerSpy.didDisplaySearch, 1)
    }
    
    func testPresentLoading_WhenCalledFromViewModel_ShouldCallDisplayLoadinghOneTime() {
        sut.presentLoading()
        
        XCTAssertEqual(resultSearchViewControllerSpy.didStartLoadingView, 1)
    }
    
    func testReloadData_WhenCalledFromViewModel_ShouldCallReloadDataOneTime() {
        sut.reloadData()
        
        XCTAssertEqual(resultSearchViewControllerSpy.didReloadArticles, 1)
    }
    
    func testPresentEmptyView_WhenCalledFromViewModel_ShouldCallEmptyViewOneTime() {
        sut.presentEmptyView()
        
        XCTAssertEqual(resultSearchViewControllerSpy.didEmptyView, 1)
    }
    
    func testUploadView_WhenCalledFromViewModel_ShouldCallIdleOneTime() {
        sut.uploadView()
        
        XCTAssertEqual(resultSearchViewControllerSpy.didIdle, 1)
    }
    
    func testPresentErrorMessage_WhenCalledFromViewModel_ShouldCallDisplayErrorMessageOneTime() {
        let errorMessage = "Error Message"
        sut.present(errorMessage: errorMessage)
        
        XCTAssertEqual(viewControllerSpy.didDisplayErrorMessage, 1)
        XCTAssertEqual(viewControllerSpy.displayErrorMessage, errorMessage)
    }
}
