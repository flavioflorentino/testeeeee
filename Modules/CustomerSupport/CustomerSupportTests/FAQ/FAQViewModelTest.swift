@testable import CustomerSupport
import AnalyticsModule
import FeatureFlag
import XCTest

fileprivate final class FAQPresenterSpy: FAQPresenting {
    var viewController: FAQDisplay?
    var searchResultController: FAQSearchResultDisplay?
    
    private(set) var FAQController: UIViewController?
    private(set) var action: FAQAction?
    private(set) var didCallPresentThirdParty = 0
    private(set) var didCallDidNextStep = 0
    private(set) var didCallPresentSearchBar = 0
    private(set) var didCallPresentErrorMessage = 0
    private(set) var didCallPresentLoading = 0
    private(set) var didCallHideLoading = 0
    private(set) var didCallReloadData = 0
    private(set) var didCallUploadView = 0
    private(set) var didCallPresentEmptyView = 0
    
    func presentThirdParty(fAQController: UIViewController) {
        didCallPresentThirdParty += 1
    }
    
    func didNextStep(action: FAQAction) {
        self.action = action
        didCallDidNextStep += 1
    }
    
    func presentSearchBar() {
        didCallPresentSearchBar += 1
    }
    
    func present(errorMessage: String) {
        didCallPresentErrorMessage += 1
    }
    
    func presentLoading() {
        didCallPresentLoading += 1
    }
    
    func hideLoading() {
        didCallHideLoading += 1
    }
    
    func reloadData() {
        didCallReloadData += 1
    }
    
    func uploadView() {
        didCallUploadView += 1
    }
    
    func presentEmptyView() {
        didCallPresentEmptyView += 1
    }
}

fileprivate final class DependecyContainerStub: CustomerDependencies {
    var analytics: AnalyticsProtocol = AnalyticsSpy()
    var mainQueue: DispatchQueue {
        DispatchQueue.main
    }
    
    var zendeskContainer: ThirdPartySupportSDKContract?
    
    var featureManager: FeatureManagerContract {
        FeatureManager.shared
    }
    
    init(wrapper: ThirdPartySupportSDKContract?) {
        self.zendeskContainer = wrapper
    }
}

final class FAQViewModelTests: XCTestCase {
    private lazy var wrapperSpy = ZendeskWrapperMock()
    private lazy var containerStub = DependecyContainerStub(wrapper: wrapperSpy)
    private lazy var spy = FAQPresenterSpy()
    private lazy var service = FAQService()
    private lazy var option = FAQOptions.home
    private lazy var sut = FAQViewModel(dependencies: containerStub, service: service, presenter: spy, option: option)
    
    func testSetupController_WhenCalledFromControllerWithOptionDifferentFromArticle_ShouldCallPresentThirdPartyOneTime() {
        sut.setupController()
        
        XCTAssertEqual(spy.didCallPresentThirdParty, 1)
        XCTAssertEqual(spy.didCallPresentSearchBar, 1)
    }
    
    func testSetupController_WhenCalledFromControllerWithOptionArticle_ShouldCallPresentThirdPartyOneTime() {
        option = .article(id: "")
        sut.setupController()
        
        XCTAssertEqual(spy.didCallPresentThirdParty, 1)
        XCTAssertEqual(spy.didCallPresentSearchBar, 0)
    }
    
    func testTicketButtonTapped_WhenCalledFromController_ShouldSendTheActionTicketList() {
        sut.ticketButtonTapped()
        
        XCTAssertEqual(spy.didCallDidNextStep, 1)
        XCTAssertEqual(spy.action, FAQAction.ticketList)
    }
    
    func testSearchQuery_WhenResultsAreEmpty_ShouldCallPresentEmptyView() {
        let articlesResult: [Article] = []
        let result = ResultArticlesResearch(articles: articlesResult, page: 1)
        wrapperSpy.searchCompletionResult = .success(result)
        sut.search(query: "Parametro é Indiferente")
        
        XCTAssertEqual(wrapperSpy.didCallSearchArticles, 1)
        XCTAssertEqual(sut.searchResultArticles, articlesResult)
        XCTAssertEqual(spy.didCallPresentLoading, 1)
        XCTAssertEqual(spy.didCallReloadData, 1)
        XCTAssertEqual(spy.didCallPresentEmptyView, 1)
        XCTAssertEqual(spy.didCallUploadView, 0)
    }
    
    func testSearchQuery_WhenCalledFromController_ShouldFillSearchResultArticles() {
        let articlesResult = [
            Article(id: 1, title: "Title 1", description: "Description 1"),
            Article(id: 2, title: "Title 2", description: "Description 2"),
            Article(id: 3, title: "Title 3", description: "Description 3")
        ]
        let result = ResultArticlesResearch(articles: articlesResult, page: 1)
        wrapperSpy.searchCompletionResult = .success(result)
        sut.search(query: "Parametro é Indiferente")
        
        XCTAssertEqual(wrapperSpy.didCallSearchArticles, 1)
        XCTAssertEqual(sut.searchResultArticles, articlesResult)
        XCTAssertEqual(spy.didCallPresentLoading, 1)
        XCTAssertEqual(spy.didCallReloadData, 1)
        XCTAssertEqual(spy.didCallUploadView, 1)
        XCTAssertEqual(spy.didCallPresentEmptyView, 0)
    }
    
    func testFetchMoreSearchResulsIfNeeded_WhenCalledFromController_ShouldAppendTheNewResultAtSearchResultArticles() {
        let baseArticlesResult = [
            Article(id: 1, title: "Title 1", description: "Description 1"),
            Article(id: 2, title: "Title 2", description: "Description 2"),
            Article(id: 3, title: "Title 3", description: "Description 3")
        ]
        let baseResult = ResultArticlesResearch(articles: baseArticlesResult, page: 1)
        wrapperSpy.searchCompletionResult = .success(baseResult)
        sut.search(query: "Parametro é Indiferente")
        
        let moreArticlesResult = [
            Article(id: 4, title: "Title 4", description: "Description 4"),
            Article(id: 5, title: "Title 5", description: "Description 5")
        ]
        let moreResult = ResultArticlesResearch(articles: moreArticlesResult, page: 2)
        wrapperSpy.searchCompletionResult = .success(moreResult)
        
        var testSample = baseArticlesResult
        testSample.append(contentsOf: moreArticlesResult)
        
        sut.fetchMoreSearchResulsIfNeeded()
        
        XCTAssertEqual(wrapperSpy.didCallSearchArticles, 2)
        XCTAssertEqual(sut.searchResultArticles, testSample)
        XCTAssertEqual(spy.didCallReloadData, 2)
        XCTAssertEqual(spy.didCallUploadView, 2)
        XCTAssertEqual(spy.didCallPresentEmptyView, 0)
    }
    
    func testCancelButtonTapped_WhenCalledFromController_CleanTheSearchResultArticlesArray() {
        let articlesResult = [
            Article(id: 1, title: "Title 1", description: "Description 1"),
            Article(id: 2, title: "Title 2", description: "Description 2"),
            Article(id: 3, title: "Title 3", description: "Description 3")
        ]
        let result = ResultArticlesResearch(articles: articlesResult, page: 1)
        wrapperSpy.searchCompletionResult = .success(result)
        sut.search(query: "Parametro é Indiferente")
        
        sut.cancelButtonTapped()
        
        XCTAssertTrue(sut.searchResultArticles.isEmpty)
        XCTAssertEqual(spy.didCallReloadData, 2)
        XCTAssertEqual(spy.didCallUploadView, 2)
    }
}
