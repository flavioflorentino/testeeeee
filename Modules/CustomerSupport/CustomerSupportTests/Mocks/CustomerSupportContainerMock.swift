@testable import CustomerSupport
import AnalyticsModule
import Foundation
import FeatureFlag
import XCTest

final class CustomerSupportContainerMock: CustomerDependencies {
    var mainQueue: DispatchQueue
    var zendeskContainer: ThirdPartySupportSDKContract?

    var analytics: AnalyticsProtocol
    var featureManager: FeatureManagerContract

    init(wrapper: ThirdPartySupportSDKContract? = ZendeskWrapperMock(),
         mainQueue: DispatchQueue = .main,
         analytics: AnalyticsProtocol = AnalyticsSpy(),
         featureManager: FeatureManagerContract = FeatureManagerMock()) {
        self.zendeskContainer = wrapper
        self.mainQueue = mainQueue
        self.analytics = analytics
        self.featureManager = featureManager
    }
}
