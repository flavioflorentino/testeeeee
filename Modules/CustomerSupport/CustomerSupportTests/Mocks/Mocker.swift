import Foundation
import XCTest

enum MockerError: Error {
    case decodeError
    case nilData
    case bundleError
    case dataContentsError
}

final class Mocker {
    func decode<T: Decodable>(_ type: T.Type, from data: Data?) throws -> T {
        guard let data = data else {
            throw MockerError.nilData
        }
        
        do {
            return try XCTUnwrap(try? JSONDecoder().decode(type, from: data))
        } catch {
            throw MockerError.decodeError
        }
    }

    func data(_ fileName: String, ofType fileType: String = "json") throws -> Data {
        guard let file = Bundle(for: type(of: self)).path(forResource: fileName, ofType: fileType) else {
            throw MockerError.bundleError
        }
        
        do {
            return try XCTUnwrap(try? Data(contentsOf: URL(fileURLWithPath: file)))
        } catch {
            throw MockerError.dataContentsError
        }
    }

    func decode<T: Decodable>(_ type: T.Type, fromResource resource: String, ofType fileType: String = "json") throws -> T {
        try XCTUnwrap(try decode(type, from: data(resource, ofType: fileType)))
    }
}
