import Foundation
import UIKit

final class NavigationControllerSpy: UINavigationController {
    lazy var didCallPushViewController = 0

    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        didCallPushViewController += 1
    }
}
