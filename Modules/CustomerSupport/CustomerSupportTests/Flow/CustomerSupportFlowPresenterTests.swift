@testable import CustomerSupport
import Core
import Foundation
import XCTest

private class CustomerSupportFlowCoordinatingSpy: CustomerSupportFlowCoordinating {
    var viewController: UIViewController?

    //MARK: - perform
    private(set) var performActionCallsCount = 0
    private(set) var performActionReceivedInvocations: [CustomerSupportFlowAction] = []

    func perform(action: CustomerSupportFlowAction) {
        performActionCallsCount += 1
        performActionReceivedInvocations.append(action)
    }
}

private class CustomerSupportFlowDisplayingSpy: CustomerSupportFlowDisplaying {

    //MARK: - displayLoading
    private(set) var displayLoadingCallsCount = 0

    func displayLoading() {
        displayLoadingCallsCount += 1
    }

    //MARK: - displayError
    private(set) var displayErrorCallsCount = 0

    func displayError() {
        displayErrorCallsCount += 1
    }
}

final class CustomerSupportFlowPresenterTests: XCTestCase {
    private lazy var displaySpy = CustomerSupportFlowDisplayingSpy()
    private lazy var viewControllerSpy: UIViewController = ViewControllerSpy()
    private lazy var coordinatorSpy: CustomerSupportFlowCoordinatingSpy = {
        let coordinator = CustomerSupportFlowCoordinatingSpy()
        coordinator.viewController = viewControllerSpy
        return coordinator
    }()
    private lazy var sut: CustomerSupportFlowPresenter = {
        let presenter = CustomerSupportFlowPresenter(coordinator: coordinatorSpy)
        presenter.viewController = displaySpy
        return presenter
    }()

    func testDisplayLoading_WhenCalledFromInteractor_ShouldCallDisplayLoading() {
        sut.displayLoading()

        XCTAssertEqual(displaySpy.displayLoadingCallsCount, 1)
        XCTAssertEqual(displaySpy.displayErrorCallsCount, 0)
    }

    func testDisplayError_WhenCalledFromInteractor_ShouldCallDisplayLoading() {
        sut.displayError()

        XCTAssertEqual(displaySpy.displayLoadingCallsCount, 0)
        XCTAssertEqual(displaySpy.displayErrorCallsCount, 1)
    }

    func testDidNextStep_WhenActionIsSupportReasonList_ShouldCallDidNextStepWithActionSupportReasonList() {
        sut.didNextStep(action: .supportReasonsList)

        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        XCTAssert(coordinatorSpy.performActionReceivedInvocations.contains(where: { $0 == .supportReasonsList }))
    }

    func testDidNextStep_WhenActionIsRequestTicket_ShouldCallDidNextStepWithActionRequestTicket() {
        let reason = SupportReason(id: 0, name: "Name", value: "Value", description: "Description", default: false, action: SupportAction(type: "Any type", url: "URL"))
        sut.didNextStep(action: .requestTicket(reason: reason))

        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        XCTAssert(coordinatorSpy.performActionReceivedInvocations.contains(where: { $0 == .requestTicket(reason: reason) }))
    }
}
