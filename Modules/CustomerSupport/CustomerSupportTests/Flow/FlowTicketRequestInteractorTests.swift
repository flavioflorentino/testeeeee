@testable import CustomerSupport
import Core
import Foundation
import XCTest

private final class CustomerSupportFlowTicketRequestServicingMock: CustomerSupportFlowTicketRequestServicing {

    //MARK: - requestList
    private(set) var requestReasonCallsCount = 0
    private(set) var requestReasonCompletionReceivedInvocations: [((Result<[SupportReason], ApiError>) -> Void)] = []
    var result: Result<[SupportReason], ApiError>?

    func requestReason(from: String, completion: @escaping (Result<[SupportReason], ApiError>) -> Void) {
        requestReasonCallsCount += 1
        requestReasonCompletionReceivedInvocations.append(completion)
        guard let result = result else { return }
        completion(result)
    }
}

private final class CustomerSupportFlowPresentingSpy: CustomerSupportFlowPresenting {
    var viewController: CustomerSupportFlowDisplaying?

    //MARK: - displayLoading
    private(set) var displayLoadingCallsCount = 0

    func displayLoading() {
        displayLoadingCallsCount += 1
    }

    //MARK: - displayError
    private(set) var displayErrorCallsCount = 0

    func displayError() {
        displayErrorCallsCount += 1
    }

    //MARK: - didNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didNextStepActionReceivedInvocations: [CustomerSupportFlowAction] = []

    func didNextStep(action: CustomerSupportFlowAction) {
        didNextStepActionCallsCount += 1
        didNextStepActionReceivedInvocations.append(action)
    }
}


final class FlowTicketRequestInteractorTests: XCTestCase {
    private lazy var presenterSpy = CustomerSupportFlowPresentingSpy()
    private lazy var serviceMock = CustomerSupportFlowTicketRequestServicingMock()
    private lazy var mainQueue = DispatchQueue(label: "HelpCenterInteractorTest")
    private var reasonTag: String?
    private lazy var sut = FlowTicketRequestInteractor(presenter: presenterSpy, service: serviceMock, reasonTag: reasonTag)
    private let timeout = 0.01

    func testRequestData_WhenSuccessful_AndTheReasonListIsNil_ShouldCallSupportReasonListTicket() {
        let supportReasons = [
            SupportReason(id: 0, name: "Name 1", value: "Value 1", description: "Description 1", default: true, action: SupportAction(type: "Any Type", url: "Any URL")),
            SupportReason(id: 0, name: "Name 2", value: "Value 2", description: "Description 2", default: true, action: SupportAction(type: "Any Type", url: "Any URL"))
        ]
        serviceMock.result = .success(supportReasons)

        reasonTag = nil

        sut.requestData()

        let expectation = XCTestExpectation()
        mainQueue.async {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: timeout)

        XCTAssertEqual(serviceMock.requestReasonCallsCount, 0)
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssert(presenterSpy.didNextStepActionReceivedInvocations.contains(where: { $0 == .supportReasonsList }))
    }

    func testRequestData_WhenSuccessful_AndTheReasonListExists_ShouldCallSupportReasonListTicket() {
        let supportReason = SupportReason(id: 0, name: "Name 1", value: "Value 1", description: "Description 1", default: true, action: SupportAction(type: "Any Type", url: "Any URL"))

        let supportReasons = [
            supportReason,
            SupportReason(id: 0, name: "Name 2", value: "Value 2", description: "Description 2", default: true, action: SupportAction(type: "Any Type", url: "Any URL"))
        ]
        serviceMock.result = .success(supportReasons)

        reasonTag = "Value 1"

        sut.requestData()

        let expectation = XCTestExpectation()
        mainQueue.async {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: timeout)

        XCTAssertEqual(serviceMock.requestReasonCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssert(presenterSpy.didNextStepActionReceivedInvocations.contains(where: { $0 == .requestTicket(reason: supportReason) }))
    }

    func testRequestData_WhenSuccessful_AndTheReasonListNotExists_ShouldCallSupportReasonListTicket() {
        let supportReasons = [
            SupportReason(id: 0, name: "Name 1", value: "Value 1", description: "Description 1", default: true, action: SupportAction(type: "Any Type", url: "Any URL")),
            SupportReason(id: 0, name: "Name 2", value: "Value 2", description: "Description 2", default: true, action: SupportAction(type: "Any Type", url: "Any URL"))
        ]
        serviceMock.result = .success(supportReasons)

        reasonTag = "I am fake"

        sut.requestData()

        let expectation = XCTestExpectation()
        mainQueue.async {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: timeout)

        XCTAssertEqual(serviceMock.requestReasonCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssert(presenterSpy.didNextStepActionReceivedInvocations.contains(where: { $0 == .supportReasonsList }))
    }

    func testRequestData_WhenFailure_AndTheReasonListExists_ShouldCallSupportReasonListTicket() {
        serviceMock.result = .failure(.bodyNotFound)

        reasonTag = "Value 1"

        sut.requestData()

        let expectation = XCTestExpectation()
        mainQueue.async {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: timeout)

        XCTAssertEqual(serviceMock.requestReasonCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssert(presenterSpy.didNextStepActionReceivedInvocations.contains(where: { $0 == .supportReasonsList }))
    }

    func testRequestData_WhenFailure_AndTheReasonListIsNil_ShouldCallSupportReasonListTicket() {
        serviceMock.result = .failure(.bodyNotFound)

        reasonTag = nil

        sut.requestData()

        let expectation = XCTestExpectation()
        mainQueue.async {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: timeout)

        XCTAssertEqual(serviceMock.requestReasonCallsCount, 0)
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssert(presenterSpy.didNextStepActionReceivedInvocations.contains(where: { $0 == .supportReasonsList }))
    }
}
