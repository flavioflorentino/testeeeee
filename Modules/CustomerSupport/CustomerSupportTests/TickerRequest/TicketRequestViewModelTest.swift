@testable import CustomerSupport
import AnalyticsModule
import FeatureFlag
import XCTest

fileprivate final class TicketRequestCoordinatorSpy: TicketRequestCoordinating {
    private(set) var action: TicketRequestAction?
    private(set) var didCallPerformAction = 0
    var viewController: UIViewController? = UIViewController()
    
    func perform(action: TicketRequestAction) {
        self.action = action
    }
}

fileprivate final class TicketRequestPresenterSpy: TicketRequestPresenting {
    private(set) var didCallDismiss = 0
    private(set) var didCallPresentMessageHint = 0
    private(set) var didCallPresentErrorMessage = 0
    private(set) var didCallPresentFormConfiguration = 0
    private(set) var didCallPresentCharacterCount = 0
    private(set) var didCallPresentLoading = 0
    private(set) var didCallHideLoading = 0
    private(set) var didCallDidNextStep = 0
    private(set) var ticketRequestController: UIViewController?
    private(set) var messageHint: String?
    private(set) var errorMessage: String?
    private(set) var title: String?
    private(set) var formConfigurationMaximumCharacterTicket: Int?
    private(set) var message: String?
    private(set) var characterCountMaximumCharacterTicket: Int?
    private(set) var action: TicketRequestAction?
    
    var viewController: TicketRequestDisplay?
    
    func dismiss() {
        didCallDismiss += 1
    }
    
    func present(messageHint: String) {
        self.messageHint = messageHint
        didCallPresentMessageHint += 1
    }
    
    func present(errorMessage: String) {
        self.errorMessage = errorMessage
        didCallPresentErrorMessage += 1
    }
    
    func presentFormConfiguration(title: String, maximumCharacterTicket: Int) {
        self.title = title
        self.formConfigurationMaximumCharacterTicket = maximumCharacterTicket
        didCallPresentFormConfiguration += 1
    }
    
    func presentCharacterCount(for message: String?, maximumCharacterTicket: Int) {
        self.message = message
        self.characterCountMaximumCharacterTicket = maximumCharacterTicket
        didCallPresentCharacterCount += 1
    }
    
    func presentLoading() {
        didCallPresentLoading += 1
    }
    
    func hideLoading() {
        didCallHideLoading += 1
    }
    
    func didNextStep(action: TicketRequestAction) {
        self.action = action
        didCallDidNextStep += 1
    }
}

fileprivate final class TicketRequestServiceSpy: TicketRequestServicing {
    private(set) var title: String?
    private(set) var message: String?
    private(set) var tags: [String]?
    var response: (Result<Void, Error>)?
    private(set) var didRequestTicket = 0
    
    func requestTicket(
        title: String,
        message: String,
        tags: [String],
        completion: @escaping (Result<Void, Error>) -> Void
    ) {
        didRequestTicket += 1
        self.title = title
        self.message = message
        self.tags = tags
        if let response = response {
            completion(response)
        }
    }
}

fileprivate final class ErroTest: Error {
    var localizedDescription: String {
        return "Error TicketRequestViewModelTests"
    }
}

fileprivate final class DependecyContainerStub: CustomerDependencies {
    var analytics: AnalyticsProtocol = AnalyticsSpy()
    var mainQueue: DispatchQueue {
        DispatchQueue.main
    }
    
    var zendeskContainer: ThirdPartySupportSDKContract?
    
    var featureManager: FeatureManagerContract {
        FeatureManager.shared
    }
    
    init(wrapper: ThirdPartySupportSDKContract?) {
        self.zendeskContainer = wrapper
    }
}

final class TicketRequestViewModelTests: XCTestCase {
    private lazy var wrapperSpy = ZendeskWrapperMock()
    private lazy var containerStub = DependecyContainerStub(wrapper: wrapperSpy)

    private lazy var ticketAction = SupportAction(type: "ticket", url: nil)
    private lazy var reason = SupportReason(id: 0, name: "Test-Name-0", value: "Test-value-0", description: "Test-description-0", default: true, action: ticketAction)
    private lazy var spy = TicketRequestPresenterSpy()
    private lazy var service = TicketRequestServiceSpy()
    private lazy var sut = TicketRequestViewModel(dependencies: containerStub, service: service, presenter: spy, reason: reason)
    
    func testFetchFormSetup_WhenCalledFromController_ShouldPresentTitleAndMaimumCharacterAndMessageHintMessage() {
        sut.fetchFormSetup()
        XCTAssertEqual(spy.didCallPresentFormConfiguration, 1)
        XCTAssertEqual(spy.title, reason.name)
        XCTAssertEqual(spy.didCallPresentMessageHint, 1)
    }
    
    func testUpdateMessage_WhenCalledFromControllerWithEmptyMessage_ShouldPresentTitleAndMaimumCharacterAndMessageHintMessage() {
        let emptyMessage = String()
        sut.update(message: emptyMessage)
        XCTAssertEqual(spy.didCallPresentErrorMessage, 1)
        XCTAssertEqual(spy.didCallPresentCharacterCount, 1)
    }
    
    func testUpdateMessage_WhenCalledFromControllerWithMessageLessThanMinimumCharactersLimit_ShouldPresentTitleAndMaimumCharacterAndMessageHintMessage() {
        let message = "< Minimum characters"
        sut.update(message: message)
        XCTAssertEqual(spy.didCallPresentErrorMessage, 1)
        XCTAssertEqual(spy.didCallPresentCharacterCount, 1)
    }
    
    func testUpdateMessage_WhenCalledFromController_ShouldPresentTitleAndMaimumCharacterAndMessageHintMessage() {
        let message = "A valid message with more characters than the minimum limit"
        sut.update(message: message)
        XCTAssertEqual(spy.didCallPresentErrorMessage, 0)
        XCTAssertEqual(spy.didCallPresentCharacterCount, 1)
        XCTAssertEqual(spy.message, message)
    }
    
    func testCreateRequest_WhenCalledFromControllerAndTheMessageEmpty_ShouldPresentAnErrorMessage() {
        let emptyMessage = String()
        service.response = .success(())
        sut.createRequest(message: emptyMessage)
        XCTAssertEqual(spy.didCallPresentErrorMessage, 1)
        XCTAssertEqual(spy.errorMessage, Strings.RequestTicket.emptyMessageError)
        XCTAssertEqual(spy.didCallPresentLoading, 0)
        XCTAssertEqual(spy.didCallDismiss, 0)
        XCTAssertNil(service.message)
        XCTAssertNil(service.tags)
        XCTAssertNil(service.title)
    }
    
    func testCreateRequest_WhenCalledFromControllerAndTheMessageIsLessThanTheMinimumCharacterCount_ShouldPresentAnErrorMessage() {
        let message = "Less than 25 Characters"
        service.response = .success(())
        sut.createRequest(message: message)
        XCTAssertEqual(spy.didCallPresentErrorMessage, 1)
        XCTAssertEqual(spy.didCallPresentLoading, 0)
        XCTAssertEqual(spy.didCallDismiss, 0)
        XCTAssertNil(service.message)
        XCTAssertNil(service.tags)
        XCTAssertNil(service.title)
    }
    
    func testCreateRequest_WhenCalledFromControllerAndResponseIsSuccessfull_ShouldCallDismiss() {
        let message = "Message big enought to pass the minimum character counter"
        service.response = .success(())
        sut.createRequest(message: message)
        XCTAssertEqual(spy.didCallPresentLoading, 1)
        XCTAssertEqual(spy.didCallDismiss, 1)
        XCTAssertEqual(service.message, message)
        XCTAssertEqual(service.title, reason.name)
        XCTAssert(service.tags?.contains(reason.value) ?? false)
    }
    
    func testCreateRequest_WhenCalledFromControllerAndResponseIsUnsuccessfull_ShouldCallCreateRequestOneTime() {
        let message = "Message big enought to pass the minimum character counter"
        let error = ErroTest()
        service.response = .failure(error)
        sut.createRequest(message: message)
        XCTAssertEqual(spy.didCallPresentLoading, 1)
        XCTAssertEqual(spy.didCallPresentErrorMessage, 1)
        XCTAssertEqual(service.message, message)
        XCTAssertEqual(service.title, reason.name)
        XCTAssert(service.tags?.contains(reason.value) ?? false)
    }
}
