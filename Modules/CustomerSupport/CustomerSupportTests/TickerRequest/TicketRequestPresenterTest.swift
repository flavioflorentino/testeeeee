@testable import CustomerSupport
import UI
import XCTest

fileprivate final class TicketRequestCoordinatorSpy: TicketRequestCoordinating {
    private(set) var action: TicketRequestAction?
    private(set) var didCallPerformAction = 0
    var viewController: UIViewController? = UIViewController()
    
    func perform(action: TicketRequestAction) {
        didCallPerformAction += 1
        self.action = action
    }
}

fileprivate final class TicketRequestViewControllerSpy: TicketRequestDisplay {
    public lazy var loadingView = LoadingView()
    
    private(set) var didDisplayTextField = 0
    private(set) var didDisplayFormConfiguration = 0
    private(set) var didDisplayCharacterCount = 0
    private(set) var didDisplayMessageHint = 0
    private(set) var didDismissKeyboard = 0
    private(set) var didStartLoadingView = 0
    private(set) var didStopLoadingView = 0
    private(set) var title: String?
    private(set) var maximumCharacterTicket: Int?
    private(set) var message: String?
    private(set) var messageHint: String?
    private(set) var errorMessage: String?
    private(set) var ticketController: UIViewController?
    
    func displayTextField(errorMessage: String) {
        didDisplayTextField += 1
        self.errorMessage = errorMessage
    }
    
    func displayFormConfiguration(title: String, maximumCharacterTicket: Int) {
        self.title = title
        self.maximumCharacterTicket = maximumCharacterTicket
        didDisplayFormConfiguration += 1
    }
    
    func displayCharacterCount(message: String?) {
        self.message = message
        didDisplayCharacterCount += 1
    }
    
    func display(messageHint: String) {
        self.messageHint = messageHint
        didDisplayMessageHint += 1
    }
    
    func dismissKeyboard() {
        didDismissKeyboard += 1
    }
    
    func startLoadingView() {
        didStartLoadingView += 1
    }
    
    func stopLoadingView() {
        didStopLoadingView += 1
    }
}

final class TicketRequestPresenterTest: XCTestCase {
    private lazy var coordinator = TicketRequestCoordinatorSpy()
    private lazy var viewController = TicketRequestViewControllerSpy()
    private lazy var sut: TicketRequestPresenter = {
        let presenter = TicketRequestPresenter(coordinator: coordinator)
        presenter.viewController = viewController
        return presenter
    }()
    
    func testDismmiss_WhenCalledFromViewModel_ShouldCallDidNextStepCloseOneTime() {
        sut.dismiss()
        
        XCTAssertEqual(coordinator.didCallPerformAction, 1)
        XCTAssertEqual(coordinator.action, TicketRequestAction.close)
    }
    
    func testPresentMessageHint_WhenCalledFromViewModel_ShouldCallDisplayMessageHintOneTime() {
        let messageHint = "Message Hint"
        sut.present(messageHint: messageHint)
        
        XCTAssertEqual(viewController.didDisplayMessageHint, 1)
        XCTAssertEqual(viewController.messageHint, messageHint)
    }
    
    func testPresentError_WhenCalledFromViewModel_ShouldCallPresentErrorOneTime() {
        let errorMessage = "Error message test"
        sut.present(errorMessage: errorMessage)
        
        XCTAssertEqual(viewController.didDisplayTextField, 1)
        XCTAssertEqual(viewController.errorMessage, errorMessage)
    }
    
    func testPresentFormConfiguration_WhenCalledFromViewModel_ShouldCallDisplayFormConfigurationAndDisplayCharacterCountOneTime() {
        let title = "A Title"
        let maximumCharacterCount = Int.random(in: 0...9)
        sut.presentFormConfiguration(title: title, maximumCharacterTicket: maximumCharacterCount)
        
        XCTAssertEqual(viewController.didDisplayFormConfiguration, 1)
        XCTAssertEqual(viewController.didDisplayCharacterCount, 1)
        XCTAssertEqual(viewController.title, title)
        XCTAssertEqual(viewController.maximumCharacterTicket, maximumCharacterCount)
    }
    
    func testPresentCharacterCount_WhenCalledFromViewModel_ShouldCallDisplayCharacterCountOneTime() {
        let message = "A Message"
        let maximumCharacterCount = 1234
        sut.presentCharacterCount(for: message, maximumCharacterTicket: maximumCharacterCount)
        
        XCTAssertEqual(viewController.didDisplayCharacterCount, 1)
    }
    
    func testPresentLoading_WhenCalledFromViewModel_ShouldCallStartLoadingAndDismmissKeyboardOneTime() {
        sut.presentLoading()
        
        XCTAssertEqual(viewController.didStartLoadingView, 1)
        XCTAssertEqual(viewController.didDismissKeyboard, 1)
    }
    
    func testHideLoading_WhenCalledFromViewModel_ShouldCallStopLoadingOneTime() {
        sut.hideLoading()
        
        XCTAssertEqual(viewController.didStopLoadingView, 1)
    }
    
    func testDidNextStep_WhenCalledFromPresenter_ShouldCallDidNextStepOneTime() {
        let action = TicketRequestAction.close
        sut.didNextStep(action: action)
        
        XCTAssertEqual(coordinator.didCallPerformAction, 1)
        XCTAssertEqual(coordinator.action, action)
    }
}
