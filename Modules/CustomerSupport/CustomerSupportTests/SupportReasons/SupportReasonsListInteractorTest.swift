@testable import CustomerSupport
import Core
import FeatureFlag
import XCTest

fileprivate final class SupportReasonsListPresenterSpy: SupportReasonsListPresenting {
    private(set) var didCallPresentReasons = 0
    private(set) var didCallPresentError = 0
    private(set) var didCallPresentAnotherReasonPopUp = 0
    private(set) var didCallDismissAnotherReasonPopUp = 0
    private(set) var didCallDidNextStep = 0
    private(set) var didCallPresentNotSelectableReasonPopUp = 0
    private(set) var didCallDismissNotSelectableReasonPopUp = 0
    private(set) var reasons: [SupportReason] = []
    private(set) var selectedReason: SupportReason?
    private(set) var action: SupportReasonsListAction?
    
    var viewController: SupportReasonsListDisplay?
    
    func present(reasons: [SupportReason]) {
        self.reasons = reasons
        didCallPresentReasons += 1
    }
    
    func presentError() {
        didCallPresentError += 1
    }

    func presentAnotherReasonPopUp() {
        didCallPresentAnotherReasonPopUp += 1
    }
    
    func dismissAnotherReasonPopUp() {
        didCallDismissAnotherReasonPopUp += 1
    }

    func presentNotSelectableReasonPopUp() {
        didCallPresentNotSelectableReasonPopUp += 1
    }

    func dismissNotSelectableReasonPopUp(completion: (() -> Void)?) {
        didCallDismissNotSelectableReasonPopUp += 1
    }

    func didNextStep(action: SupportReasonsListAction) {
        didCallDidNextStep += 1
        self.action = action
    }
}

fileprivate final class SupportReasonsListServiceMock: SupportReasonsListServicing {
    private(set) var didFetchReasons = 0
    var requestReasonListCompletionResult: Result<[SupportReason], ApiError>?

    func requestReasonList(completion: @escaping (Result<[SupportReason], ApiError>) -> Void) {
        didFetchReasons += 1

        guard let completionResult = requestReasonListCompletionResult else { return }
        completion(completionResult)
    }

    private(set) var didRequestTicketExistence = 0
    var requestTicketExistenceCompletionResult: Bool?
    func requestTicketExistence(completion: @escaping (Bool) -> Void) {
        didRequestTicketExistence += 1

        guard let completionResult = requestTicketExistenceCompletionResult else { return }
        completion(completionResult)
    }
}

final class SupportReasonsListViewModelTest: XCTestCase {
    private lazy var featureManagerMock = FeatureManagerMock()
    private lazy var containerMock = CustomerSupportContainerMock(featureManager: featureManagerMock)
    private lazy var presenterSpy = SupportReasonsListPresenterSpy()
    private lazy var serviceSpy = SupportReasonsListServiceMock()
    private lazy var sut = SupportReasonsListInteractor(dependencies: containerMock, service: serviceSpy, presenter: presenterSpy)
    private lazy var chatBotAction = SupportAction(type: "chatbot", url: "anything")
    private lazy var ticketAction = SupportAction(type: "ticket", url: nil)
    
    func testFetchReasons_WhenCalledFromController_ShouldReasonsOneTime() {
        let reasons = [
            SupportReason(id: 0, name: "Test-name-1", value: "Teste-value-1", description: "test-descruption-1", default: true, action: chatBotAction),
            SupportReason(id: 1, name: "Test-name-2", value: "Teste-value-2", description: "test-descruption-2", default: false, action: ticketAction),
            SupportReason(id: 2, name: "Test-name-3", value: "Teste-value-3", description: "test-descruption-3", default: true, action: chatBotAction)
        ]

        serviceSpy.requestReasonListCompletionResult = .success(reasons)
        sut.fetchReasons()

        XCTAssertEqual(serviceSpy.didFetchReasons, 1)
        XCTAssertEqual(presenterSpy.reasons, reasons)
        XCTAssertEqual(presenterSpy.didCallPresentReasons, 1)
        XCTAssertEqual(presenterSpy.didCallPresentError, 0)
    }

    func testFetchReasons_WhenServiceFail_ShouldReasonsOneTime() {
        serviceSpy.requestReasonListCompletionResult = .failure(.bodyNotFound)
        sut.fetchReasons()

        XCTAssertEqual(serviceSpy.didFetchReasons, 1)
        XCTAssertEqual(presenterSpy.reasons, [])
        XCTAssertEqual(presenterSpy.didCallPresentReasons, 0)
        XCTAssertEqual(presenterSpy.didCallPresentError, 1)
    }
    
    func testTicketButtonTapped_WhenCalledFromController_ShouldSendTheActionTicketList() {
        let indexPath = IndexPath(row: 1, section: 0)
        let reasons = [
            SupportReason(id: 0, name: "Test-name-1", value: "Teste-value-1", description: "test-descruption-1", default: true, action: chatBotAction),
            SupportReason(id: 1, name: "Test-name-2", value: "Teste-value-2", description: "test-descruption-2", default: false, action: ticketAction),
            SupportReason(id: 2, name: "Test-name-3", value: "Teste-value-3", description: "test-descruption-3", default: true, action: chatBotAction)
        ]

        let reason = reasons[indexPath.row]
        
        serviceSpy.requestReasonListCompletionResult = .success(reasons)
        sut.fetchReasons()
        sut.selectReasons(at: indexPath)

        XCTAssertEqual(presenterSpy.didCallPresentReasons, 1)
        XCTAssertEqual(presenterSpy.didCallPresentError, 0)
        XCTAssertEqual(presenterSpy.didCallDidNextStep, 1)
        XCTAssertEqual(presenterSpy.action, SupportReasonsListAction.newTicket(reason: reason))
    }

    func testSelectReason_WhenSupportReasonHasAChatBotAction_ShouldSendTheActionChatBot() {
        let indexPath = IndexPath(row: 0, section: 0)
        let reasons = [
            SupportReason(id: 0, name: "Test-name-1", value: "Teste-value-1", description: "test-descruption-1", default: true, action: chatBotAction),
            SupportReason(id: 1, name: "Test-name-2", value: "Teste-value-2", description: "test-descruption-2", default: false, action: ticketAction),
            SupportReason(id: 2, name: "Test-name-3", value: "Teste-value-3", description: "test-descruption-3", default: true, action: chatBotAction)
        ]

        serviceSpy.requestReasonListCompletionResult = .success(reasons)
        sut.fetchReasons()
        sut.selectReasons(at: indexPath)

        XCTAssertEqual(presenterSpy.didCallPresentReasons, 1)
        XCTAssertEqual(presenterSpy.didCallPresentError, 0)
        XCTAssertEqual(presenterSpy.didCallDidNextStep, 1)
        XCTAssertEqual(presenterSpy.action, SupportReasonsListAction.chatBot(url: chatBotAction.url ?? "fail"))
    }

    func testSelectReason_WhenSupportReasonHasAChatBotActionHasURLNil_ShouldSendTheActionChatBotWithURLFromFeatureManager() {
        let indexPath = IndexPath(row: 0, section: 0)
        let reasons = [
            SupportReason(id: 0, name: "Test-name-1", value: "Teste-value-1", description: "test-descruption-1", default: true, action: SupportAction(type: "chatBot", url: nil)),
            SupportReason(id: 1, name: "Test-name-2", value: "Teste-value-2", description: "test-descruption-2", default: false, action: ticketAction),
            SupportReason(id: 2, name: "Test-name-3", value: "Teste-value-3", description: "test-descruption-3", default: true, action: chatBotAction)
        ]

        let featureManagerChatBotURL = "feature.manager.chat.bot"

        featureManagerMock.override(keys: .chatbotCovid19, with: featureManagerChatBotURL)

        serviceSpy.requestReasonListCompletionResult = .success(reasons)
        sut.fetchReasons()
        sut.selectReasons(at: indexPath)

        XCTAssertEqual(presenterSpy.didCallPresentReasons, 1)
        XCTAssertEqual(presenterSpy.didCallPresentError, 0)
        XCTAssertEqual(presenterSpy.didCallDidNextStep, 1)
        XCTAssertEqual(presenterSpy.action, SupportReasonsListAction.chatBot(url: featureManagerChatBotURL))
    }
    
    func testTicketButtonTapped_WhenCalledSelecteAnAnotherReasonTag_ShouldSendTheActionTicketList() {
        let indexPath = IndexPath(row: 0, section: 0)
        let reasons = [
            SupportReason(id: 1, name: "Test-name-2", value: "clientepf__outros", description: "test-descruption-2", default: false, action: ticketAction)
        ]
        
        serviceSpy.requestReasonListCompletionResult = .success(reasons)
        sut.fetchReasons()
        sut.selectReasons(at: indexPath)

        XCTAssertEqual(presenterSpy.didCallPresentReasons, 1)
        XCTAssertEqual(presenterSpy.didCallPresentError, 0)
        XCTAssertEqual(presenterSpy.didCallPresentAnotherReasonPopUp, 1)
    }

    func testSelectReason_WhenReasonIsAnException_ShouldPresentNotSelectableReasonPopUp() {
        let indexPath = IndexPath(row: 0, section: 0)
        let exceptionAction = SupportAction(type: "open_ticket", url: "picpay://helpcenter/ticket/123456")
        let reasons = [
            SupportReason(id: 1, name: "Test-name-1", value: "Test-value", description: "test-description-1", default: false, action: exceptionAction)
        ]

        serviceSpy.requestReasonListCompletionResult = .success(reasons)
        sut.fetchReasons()
        sut.selectReasons(at: indexPath)

        XCTAssertEqual(presenterSpy.didCallPresentReasons, 1)
        XCTAssertEqual(presenterSpy.didCallPresentError, 0)
        XCTAssertEqual(presenterSpy.didCallPresentNotSelectableReasonPopUp, 1)
    }
}
