@testable import CustomerSupport
import XCTest

fileprivate final class SupportReasonsListCoordinatorSpy: SupportReasonsListCoordinating {
    private(set) var didCallPerform = 0
    private(set) var action: SupportReasonsListAction?
    var viewController: UIViewController? = UIViewController()
    
    func perform(action: SupportReasonsListAction) {
        didCallPerform += 1
        self.action = action
    }
}

fileprivate final class SupportReasonsListViewControllerSpy: SupportReasonsListDisplay {
    private(set) var didDisplayReasons = 0
    private(set) var didDisplayError = 0
    private(set) var didDisplayAnotherReasonPopUp = 0
    private(set) var didDismissAnotherReasonPopUp = 0
    private(set) var reasons: [SupportReason] = []
    private(set) var didDisplayNotSelectableReasonPopUp = 0
    private(set) var didDismissNotSelectableReasonPopUp = 0
    
    func display(reasons: [SupportReason]) {
        didDisplayReasons += 1
        self.reasons = reasons
    }

    func displayError() {
        didDisplayError += 1
    }
    
    func displayAnotherReasonPopUp() {
        didDisplayAnotherReasonPopUp += 1
    }
    
    func dismissAnotherReasonPopUp() {
        didDismissAnotherReasonPopUp += 1
    }

    func displayNotSelectableReasonPopUp() {
        didDisplayNotSelectableReasonPopUp += 1
    }

    func dismissNotSelectableReasonPopUp(completion: (() -> Void)?) {
        didDismissNotSelectableReasonPopUp += 1
    }
}

final class SupportReasonsListPresenterTest: XCTestCase {
    private lazy var coordinatorSpy = SupportReasonsListCoordinatorSpy()
    private lazy var viewControllerSpy = SupportReasonsListViewControllerSpy()
    private lazy var sut: SupportReasonsListPresenter = {
        let presenter = SupportReasonsListPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    private lazy var chatBotAction = SupportAction(type: "chatbot", url: "anything")
    private lazy var ticketAction = SupportAction(type: "ticket", url: nil)
    private lazy var reasonsSample = [
        SupportReason(id: 0, name: "Test-name-1", value: "Teste-value-1", description: "test-descruption-1", default: true, action: chatBotAction),
        SupportReason(id: 1, name: "Test-name-2", value: "Teste-value-2", description: "test-descruption-2", default: false, action: ticketAction),
        SupportReason(id: 2, name: "Test-name-3", value: "Teste-value-3", description: "test-descruption-3", default: true, action: chatBotAction)
    ]

    func testPresentReasons_WhenCalledFromViewModel_ShouldCallDidDisplayReasonsOneTime() {
        sut.present(reasons: reasonsSample)
        
        XCTAssertEqual(viewControllerSpy.didDisplayReasons, 1)
        XCTAssertEqual(reasonsSample, viewControllerSpy.reasons)
    }

    func testPresentError_WhenCalledFromViewModel_ShouldCallDidDisplayReasonsOneTime() {
        sut.presentError()

        XCTAssertEqual(viewControllerSpy.didDisplayError, 1)
    }
    
    func testPresentAnotherReasonsPopUp_WhenCalledFromViewModel_ShouldCallDidDisplayReasonsOneTime() {
        sut.presentAnotherReasonPopUp()
        
        XCTAssertEqual(viewControllerSpy.didDisplayAnotherReasonPopUp, 1)
        XCTAssertEqual(viewControllerSpy.didDismissAnotherReasonPopUp, 0)
    }
    
    func testDismissAnotherReasonsPopUp_WhenCalledFromViewModel_ShouldCallDidDisplayReasonsOneTime() {
        sut.dismissAnotherReasonPopUp()
        
        XCTAssertEqual(viewControllerSpy.didDismissAnotherReasonPopUp, 1)
        XCTAssertEqual(viewControllerSpy.didDisplayAnotherReasonPopUp, 0)
    }
    
    func testDidNextStep_WhenCalledFromViewModel_ShouldCallDidPerformNextStepOneTime() {
        guard let reason = reasonsSample.first else {
            XCTFail("reasonsSample list is empty")
            return
        }

        let action: SupportReasonsListAction = .newTicket(reason: reason)
        sut.didNextStep(action: action)
        
        XCTAssertEqual(coordinatorSpy.didCallPerform, 1)
    }
}
