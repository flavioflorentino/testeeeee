@testable import CustomerSupport
import Core
import Foundation
import XCTest

private class HelpCenterListDisplayingSpy: HelpCenterListDisplaying {

    //MARK: - display
    private(set) var displayObjectsCallsCount = 0
    private(set) var displayObjectsReceivedInvocation: [HelpCenterListModel] = []

    func display(objects: [HelpCenterListModel]) {
        displayObjectsCallsCount += 1
        displayObjectsReceivedInvocation = objects
    }

    //MARK: - display
    private(set) var displayTitleCallsCount = 0
    private(set) var displayTitleReceivedInvocation: String?

    func display(title: String?) {
        displayTitleCallsCount += 1
        displayTitleReceivedInvocation = title
    }

    //MARK: - displayError
    private(set) var displayErrorCallsCount = 0

    func displayError() {
        displayErrorCallsCount += 1
    }
}

private class HelpCenterListCoordinatingSpy: HelpCenterListCoordinating {
    var viewController: UIViewController?

    //MARK: - perform
    private(set) var performActionCallsCount = 0
    private(set) var performAction: HelpCenterListAction?

    func perform(action: HelpCenterListAction) {
        performActionCallsCount += 1
        performAction = action
    }
}

final class HelpCenterListPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = HelpCenterListCoordinatingSpy()
    private lazy var displaySpy = HelpCenterListDisplayingSpy()
    private lazy var sut: HelpCenterListPresenter = {
        let presenter =  HelpCenterListPresenter(coordinator: coordinatorSpy)
        presenter.viewController = displaySpy
        return presenter
    }()

    func testPresentObjects_WhenCalledFromInteractorWithTitle_ShouldCallDisplaObjectsFromDisplay() {
        let objects = [
            Article(id: 12345, title: "Test 1"),
            Article(id: 43324, title: "Test 2"),
            Article(id: 54332, title: "Test 3"),
        ]

        let title = "A title"

        sut.present(objects: objects, title: title)

        XCTAssertEqual(displaySpy.displayObjectsCallsCount, 1)
        XCTAssertEqual(displaySpy.displayTitleCallsCount, 1)
        XCTAssertEqual(displaySpy.displayErrorCallsCount, 0)
        XCTAssertEqual(displaySpy.displayTitleReceivedInvocation, title)
    }

    func testPresentObjects_WhenCalledFromInteractorWithoutTitle_ShouldCallDisplaObjectsFromDisplay() {
        let objects = [
            Article(id: 12345, title: "Test 1"),
            Article(id: 43324, title: "Test 2"),
            Article(id: 54332, title: "Test 3"),
        ]

        sut.present(objects: objects, title: nil)

        XCTAssertEqual(displaySpy.displayObjectsCallsCount, 1)
        XCTAssertEqual(displaySpy.displayTitleCallsCount, 1)
        XCTAssertEqual(displaySpy.displayErrorCallsCount, 0)
        XCTAssertEqual(displaySpy.displayTitleReceivedInvocation, nil)
    }

    func testPresentError_WhenCalledFromInteractor_ShouldCallDisplaErrorFromDisplay() {
        sut.present(error: .serverError)

        XCTAssertEqual(displaySpy.displayObjectsCallsCount, 0)
        XCTAssertEqual(displaySpy.displayTitleCallsCount, 0)
        XCTAssertEqual(displaySpy.displayErrorCallsCount, 1)
    }

    func testDidNextStep_WhenCalledFromInteractor_ShouldCallPerformActionFromCoordinator() {
        let article = Article(id: 923213, title: "A title")
        sut.didNextStep(action: .selected(item: article))

        XCTAssertEqual(displaySpy.displayObjectsCallsCount, 0)
        XCTAssertEqual(displaySpy.displayTitleCallsCount, 0)
        XCTAssertEqual(displaySpy.displayErrorCallsCount, 0)
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
    }
}
