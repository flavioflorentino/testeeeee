@testable import CustomerSupport
import Core
import Foundation
import XCTest

final class HelpCenterListSectionsCoordinatorTests: XCTestCase {
    private lazy var viewControllerSpy = ViewControllerSpy()
    private lazy var navigationControllerSpy = NavigationControllerSpy(rootViewController: viewControllerSpy)
    private lazy var sut: HelpCenterListSectionsCoordinator = {
        let coordinator = HelpCenterListSectionsCoordinator()
        coordinator.viewController = viewControllerSpy
        return coordinator
    }()

    func testPerformAction_WhenCalledFromPresenter_ShouldPushToFAQViewController() {
        let category = HelpCenterCategory(id: 12345678, name: "Categoria Test")
        let section = HelpCenterSection(id: 87654321, name: "Seção Test", category: category)
        sut.perform(action: .selected(item: section))

        XCTAssertEqual(navigationControllerSpy.didCallPushViewController, 1)
    }
}
