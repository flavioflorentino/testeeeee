@testable import CustomerSupport
import Foundation
import XCTest

final class HelpCenterListSectionEndpointTests: XCTestCase {
    private lazy var categoryId = 123654
    private lazy var sut = HelpCenterListSectionEndpoint(categoryId: categoryId)

    func testPath_WehnCalledFromService_ShouldProvideThePathWithCategoryID() {
        let path = sut.path

        XCTAssertEqual("/zendesk-support/helpcenter/categories/123654/sections", path)
    }
}
