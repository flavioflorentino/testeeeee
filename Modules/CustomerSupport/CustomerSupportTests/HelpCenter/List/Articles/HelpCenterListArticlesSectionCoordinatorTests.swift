@testable import CustomerSupport
import Core
import Foundation
import XCTest

final class HelpCenterListArticlesSectionCoordinatorTests: XCTestCase {
    private lazy var viewControllerSpy = ViewControllerSpy()
    private lazy var navigationControllerSpy = NavigationControllerSpy(rootViewController: viewControllerSpy)
    private lazy var sut: HelpCenterListArticlesSectionCoordinator = {
        let coordinator = HelpCenterListArticlesSectionCoordinator()
        coordinator.viewController = viewControllerSpy
        return coordinator
    }()

    func testPerformAction_WhenCalledFromPresenter_ShouldPushToFAQViewController() {
        let article = Article(id: 12345678, title: "Testing")
        sut.perform(action: .selected(item: article))

        XCTAssertEqual(navigationControllerSpy.didCallPushViewController, 1)
    }
}
