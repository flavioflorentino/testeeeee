@testable import CustomerSupport
import Foundation
import XCTest

final class HelpCenterListArticlesSectionServiceEndpointTests: XCTestCase {
    private lazy var sectionId = 87651234
    private lazy var sut = HelpCenterListArticlesSectionEndpoint(sectionId: sectionId)

    func testPath_WehnCalledFromService_ShouldProvideThePathWithCategoryID() {
        let path = sut.path

        XCTAssertEqual("/zendesk-support/helpcenter/sections/87651234/articles", path)
    }
}
