@testable import CustomerSupport
import UI
import XCTest

final class HelpCenterCoordinatorTests: XCTestCase {
    private lazy var viewControllerStub: UIViewController = UIViewController()
    private lazy var navigationControllerSpy = NavigationControllerSpy(rootViewController: viewControllerStub)
    private lazy var sut: HelpCenterCoordinator = {
        let coordinator = HelpCenterCoordinator()
        coordinator.viewController = viewControllerStub
        return coordinator
    }()

    func testPerformAction_WhenActionIsOpenArticle_ShouldCallPushViewControllerWithFAQViewControllerAsParameter() {
        let id = "An Id"
        let action = HelpCenterAction.openArticle(id: id)

        sut.perform(action: action)

        XCTAssertEqual(navigationControllerSpy.didCallPushViewController, 1)
    }

    func testPerformAction_WhenActionIsOpenCategory_ShouldCallPushViewControllerWithFAQViewControllerAsParameter() {
        let id = 12345
        let title = "A Title"
        let action = HelpCenterAction.openCategory(id: id, title: title)

        sut.perform(action: action)
 
        XCTAssertEqual(navigationControllerSpy.didCallPushViewController, 1)
    }

    func testPerformAction_WhenActionIsMessageHistory_ShouldCallPushViewControllerWithTicketListViewControllerAsParameter() {
        let action = HelpCenterAction.openMessageHistory

        sut.perform(action: action)

        XCTAssertEqual(navigationControllerSpy.didCallPushViewController, 1)
    }

    func testPerformAction_WhenActionIsNewTicket_ShouldCallPushViewControllerWithTicketListViewControllerAsParameter() {
        let action = HelpCenterAction.openNewTicket

        sut.perform(action: action)

        XCTAssertEqual(navigationControllerSpy.didCallPushViewController, 1)
    }
}
