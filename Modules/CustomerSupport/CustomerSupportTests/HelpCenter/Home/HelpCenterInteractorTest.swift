@testable import CustomerSupport
import AnalyticsModule
import Core
import Foundation
import UI
import XCTest

fileprivate final class HelpCenterServiceSpy: HelpCenterServicing {
    private(set) var didCallRequestFrequentlyAskedQuestions = 0
    private(set) var didCallRequestCategories = 0
    var articlesResultOfRequestFrequentlyAskedQuestions: Result<ArticlesResponse, ApiError>?
    var articlesResultOfRequestCategories: Result<CategoriesResponse, ApiError>?
    var isNewHome: Bool = false
    var homeResponse: Result<HomeResponse, ApiError> = .failure(.bodyNotFound)
    
    func requestFrequentlyAskedQuestions(completion: @escaping (Result<ArticlesResponse, ApiError>) -> Void) {
        didCallRequestFrequentlyAskedQuestions += 1

        guard let articlesResultOfRequestFrequentlyAskedQuestions = articlesResultOfRequestFrequentlyAskedQuestions else {
            XCTFail("requestFrequentlyAskedQuestions must be set")
            return
        }
        completion(articlesResultOfRequestFrequentlyAskedQuestions)
    }

    func requestCategories(completion: @escaping (Result<CategoriesResponse, ApiError>) -> Void) {
        didCallRequestCategories += 1

        do {
            let articlesResultOfRequestCategoriesSafe = try XCTUnwrap(articlesResultOfRequestCategories)
            completion(articlesResultOfRequestCategoriesSafe)
        } catch {
            XCTFail(error.localizedDescription)
        }
    }
    
    func fetchFAQHome(completion: @escaping (Result<HomeResponse, ApiError>) -> Void) {
        completion(homeResponse)
    }
}

private final class HelpCenterPresentingSpy: HelpCenterPresenting {
    var viewController: HelpCenterDisplaying?

    private(set) var didNextStepActionCallsCount = 0
    private(set) var didNextStepActionReceivedInvocations: [HelpCenterAction] = []
    private(set) var presentSearchCallsCount = 0
    private(set) var presentErrorCallsCount = 0
    private(set) var startLoadingCallsCount = 0
    private(set) var stopLoadingCallsCount = 0
    private(set) var presentStateArticlesCategoriesCallsCount = 0
    private(set) var presentSectionsCallCount = 0
    private(set) var presentNewHomeCallCount = 0
    private(set) var presentStateArticlesCategoriesReceivedInvocations: [(sections: [HelpCenterTableModel.Section], articles: [Article], categories: [HelpCenterCategory])] = []
    private(set) var presentNewHomeSections: [HelpCenterTableModel.Section]?
    private(set) var presentNewHomeResponse: HomeResponse?
    private(set) var presentSections: [HelpCenterTableModel.Section]?
    private(set) var presentSectionsCategories: [HelpCenterCategory]?
    private(set) var presentSectionsArticles: [Article]?
    
    func didNextStep(action: HelpCenterAction) {
        didNextStepActionCallsCount += 1
        didNextStepActionReceivedInvocations.append(action)
    }

    func presentSearch() {
        presentSearchCallsCount += 1
    }

    func presentError() {
        presentErrorCallsCount += 1
    }

    func startLoading() {
        startLoadingCallsCount += 1
    }

    func stopLoading() {
        stopLoadingCallsCount += 1
    }

    func present(sections: [HelpCenterTableModel.Section], articles: [Article], categories: [HelpCenterCategory]) {
        presentStateArticlesCategoriesCallsCount += 1
        presentStateArticlesCategoriesReceivedInvocations.append((sections: sections, articles: articles, categories: categories))
    }
    
    func presentNewHome(sections: [HelpCenterTableModel.Section], response: HomeResponse) {
        presentNewHomeCallCount += 1
        presentNewHomeResponse = response
        presentNewHomeSections = sections
    }
}

final class HelpCenterInteractorTest: XCTestCase {
    private let timeout: Double = 0.5
    private lazy var mainQueue = DispatchQueue(label: "HelpCenterInteractorTest")
    private lazy var zendeskWrapperMock = ZendeskWrapperMock()
    private lazy var analyticsSpy = AnalyticsSpy()
    private lazy var dependency = CustomerSupportContainerMock(wrapper: zendeskWrapperMock, mainQueue: mainQueue, analytics: analyticsSpy)
    private lazy var serviceSpy = HelpCenterServiceSpy()
    private lazy var presenterSpy = HelpCenterPresentingSpy()
    private lazy var sut: HelpCenterInteractor = HelpCenterInteractor(service: serviceSpy, presenter: presenterSpy, dependencies: dependency)

    func testLoadViewInfo_WhenIsSuccessfullyAndHasNoTicket_ShouldCallPresentLoadingAndPresentFrequentlyAskedQuestionsAndPresentCategoriesOnce() {
        let questions = [
            Article(id: 0, title: "Test 0"),
            Article(id: 1, title: "Test 1"),
            Article(id: 2, title: "Test 2")
        ]

        let categories = [
            HelpCenterCategory(id: 0, name: "Category 0"),
            HelpCenterCategory(id: 1, name: "Category 1"),
            HelpCenterCategory(id: 2, name: "Category 2")
        ]

        let articlesResponse = ArticlesResponse(perPage: 3, previousPage: 0, count: 1, nextPage: 0, page: 1, pageCount: 1, articles: questions)
        let categoriesResponse = CategoriesResponse(perPage: 3, previousPage: 0, count: 1, nextPage: 0, page: 1, pageCount: 1, categories: categories)

        serviceSpy.articlesResultOfRequestFrequentlyAskedQuestions = .success(articlesResponse)
        serviceSpy.articlesResultOfRequestCategories = .success(categoriesResponse)
//        zendeskWrapperMock.isAnyTicketOpenCompletionResult = nil

        sut.loadViewInfo()

        let expectation = XCTestExpectation()
        mainQueue.async {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: timeout)

        XCTAssertTrue(analyticsSpy.equals(to: HelpCenterEvent.contentViewed(status: .success, type: .category).event(), HelpCenterEvent.contentViewed(status: .success, type: .article).event(), HelpCenterEvent.homeViewed(success: true).event()))
        XCTAssertEqual(serviceSpy.didCallRequestFrequentlyAskedQuestions, 1)
        XCTAssertEqual(serviceSpy.didCallRequestCategories, 1)
        XCTAssertEqual(zendeskWrapperMock.didCallIsAnyTicketOpen, 1)
        XCTAssertEqual(try serviceSpy.articlesResultOfRequestFrequentlyAskedQuestions?.get().articles, questions)
        XCTAssertEqual(try serviceSpy.articlesResultOfRequestCategories?.get().categories, categories)
        
        XCTAssertEqual(presenterSpy.startLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentStateArticlesCategoriesCallsCount, 1)

        XCTAssertTrue(
            presenterSpy.presentStateArticlesCategoriesReceivedInvocations.contains(
                where: { $0.sections == HelpCenterState.hasNotTicket.sections }
            )
        )
    }

    func testLoadViewInfo_WhenIsSuccessfullyAndHasTicket_ShouldCallPresentLoadingAndPresentFrequentlyAskedQuestionsAndPresentCategoriesOnce() {
        let questions = [
            Article(id: 0, title: "Test 0"),
            Article(id: 1, title: "Test 1"),
            Article(id: 2, title: "Test 2")
        ]

        let categories = [
            HelpCenterCategory(id: 0, name: "Category 0"),
            HelpCenterCategory(id: 1, name: "Category 1"),
            HelpCenterCategory(id: 2, name: "Category 2")
        ]

        let articlesResponse = ArticlesResponse(perPage: 3, previousPage: 0, count: 1, nextPage: 0, page: 1, pageCount: 1, articles: questions)
        let categoriesResponse = CategoriesResponse(perPage: 3, previousPage: 0, count: 1, nextPage: 0, page: 1, pageCount: 1, categories: categories)

        serviceSpy.articlesResultOfRequestFrequentlyAskedQuestions = .success(articlesResponse)
        serviceSpy.articlesResultOfRequestCategories = .success(categoriesResponse)
        zendeskWrapperMock.isAnyTicketOpenCompletionResult = "Any Id"

        sut.loadViewInfo()

        let expectation = XCTestExpectation()
        mainQueue.async {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: timeout)

        XCTAssertTrue(
            analyticsSpy.equals(
                to: HelpCenterEvent.contentViewed(status: .success,type: .category).event(),
                HelpCenterEvent.contentViewed(status: .success, type: .article).event(),
                HelpCenterEvent.homeViewed(success: true).event()
            )
        )
        XCTAssertEqual(serviceSpy.didCallRequestFrequentlyAskedQuestions, 1)
        XCTAssertEqual(serviceSpy.didCallRequestCategories, 1)
        XCTAssertEqual(try serviceSpy.articlesResultOfRequestFrequentlyAskedQuestions?.get().articles, questions)
        XCTAssertEqual(try serviceSpy.articlesResultOfRequestCategories?.get().categories, categories)
        XCTAssertEqual(presenterSpy.presentStateArticlesCategoriesCallsCount, 1)
        XCTAssertTrue(
            presenterSpy.presentStateArticlesCategoriesReceivedInvocations.contains(
                where: {
                    $0.sections == HelpCenterState.hasTicket.sections &&
                        $0.articles == questions &&
                        $0.categories == categories
                }
            )
        )
    }

    func testLoadViewInfo_WhenHasArticlesFailed_ShouldCallPresentLoadingAndPresentFrequentlyAskedQuestionsAndPresentCategoriesOnce() {
        let categories = [
            HelpCenterCategory(id: 0, name: "Category 0"),
            HelpCenterCategory(id: 1, name: "Category 1"),
            HelpCenterCategory(id: 2, name: "Category 2")
        ]
        let categoriesResponse = CategoriesResponse(perPage: 3, previousPage: 0, count: 1, nextPage: 0, page: 1, pageCount: 1, categories: categories)
        serviceSpy.articlesResultOfRequestCategories = .success(categoriesResponse)

        let error = ApiError.connectionFailure
        serviceSpy.articlesResultOfRequestFrequentlyAskedQuestions = .failure(error)

        sut.loadViewInfo()

        let expectation = XCTestExpectation()
        mainQueue.async {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: timeout)

        XCTAssertTrue(analyticsSpy.equals(to: HelpCenterEvent.contentViewed(status: .success, type: .category).event(),
                                          HelpCenterEvent.contentViewed(status: .error, type: .article).event(), HelpCenterEvent.homeViewed(success: false).event()))
        XCTAssertEqual(serviceSpy.didCallRequestFrequentlyAskedQuestions, 1)
        XCTAssertEqual(serviceSpy.didCallRequestCategories, 1)
        XCTAssertNotNil(serviceSpy.articlesResultOfRequestFrequentlyAskedQuestions?.mapError({$0}))
        XCTAssertEqual(try serviceSpy.articlesResultOfRequestCategories?.get().categories, categories)
        XCTAssertEqual(presenterSpy.presentStateArticlesCategoriesCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentErrorCallsCount, 1)
    }

    func testLoadViewInfo_WhenHasCategoryFailed_ShouldCallPresentLoadingAndPresentFrequentlyAskedQuestionsAndPresentCategoriesOnce() {
        let questions = [
            Article(id: 0, title: "Test 0"),
            Article(id: 1, title: "Test 1"),
            Article(id: 2, title: "Test 2")
        ]

        let articlesResponse = ArticlesResponse(perPage: 3, previousPage: 0, count: 1, nextPage: 0, page: 1, pageCount: 1, articles: questions)
        serviceSpy.articlesResultOfRequestFrequentlyAskedQuestions = .success(articlesResponse)

        let error = ApiError.connectionFailure
        serviceSpy.articlesResultOfRequestCategories = .failure(error)

        sut.loadViewInfo()

        let expectation = XCTestExpectation()
        mainQueue.async {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: timeout)

        XCTAssertTrue(analyticsSpy.equals(to: HelpCenterEvent.contentViewed(status: .error, type: .category).event(),
                                          HelpCenterEvent.contentViewed(status: .success, type: .article).event(), HelpCenterEvent.homeViewed(success: false).event()))
        XCTAssertEqual(serviceSpy.didCallRequestFrequentlyAskedQuestions, 1)
        XCTAssertEqual(serviceSpy.didCallRequestCategories, 1)
        XCTAssertEqual(try serviceSpy.articlesResultOfRequestFrequentlyAskedQuestions?.get().articles, questions)
        XCTAssertNotNil(serviceSpy.articlesResultOfRequestCategories?.mapError({$0}))
        XCTAssertEqual(presenterSpy.presentStateArticlesCategoriesCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentErrorCallsCount, 1)
    }

    func testLoadViewInfo_WhenHasBothServicesFailed_ShouldCallPresentLoadingAndPresentFrequentlyAskedQuestionsAndPresentCategoriesOnce() {
        let error = ApiError.connectionFailure
        serviceSpy.articlesResultOfRequestCategories = .failure(error)
        serviceSpy.articlesResultOfRequestFrequentlyAskedQuestions = .failure(error)

        sut.loadViewInfo()

        let expectation = XCTestExpectation()
        mainQueue.async {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: timeout)

        XCTAssertTrue(analyticsSpy.equals(to: HelpCenterEvent.contentViewed(status: .error, type: .category).event(),
                                          HelpCenterEvent.contentViewed(status: .error, type: .article).event(), HelpCenterEvent.homeViewed(success: false).event()))
        XCTAssertEqual(serviceSpy.didCallRequestFrequentlyAskedQuestions, 1)
        XCTAssertEqual(serviceSpy.didCallRequestCategories, 1)
        XCTAssertNotNil(serviceSpy.articlesResultOfRequestFrequentlyAskedQuestions?.mapError({$0}))
        XCTAssertNotNil(serviceSpy.articlesResultOfRequestCategories?.mapError({$0}))
        XCTAssertEqual(presenterSpy.presentStateArticlesCategoriesCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentErrorCallsCount, 1)
    }

    func testLoadViewInfo_WhenIsFrequenqutlyAskedSectionRequest_ShouldCallPresentLoadingAndPresentFrequentlyAskedQuestionsAndPresentCategoriesOnce() {
        let item = HelpCenterTableModel(id: 1, title: "Teste 1", description: nil, section: .frequentlyAsked, showMore: false, timestamp: nil)
        let id = 1
        sut.requestDidSelect(item)

        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentStateArticlesCategoriesCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentErrorCallsCount, 0)
        XCTAssertTrue(presenterSpy.didNextStepActionReceivedInvocations.contains(where: {$0 == .openArticle(id: "\(id)")}))
    }

    func testLoadViewInfo_WhenIsHelpCenterCategorySectionRequest_ShouldCallPresentLoadingAndPresentFrequentlyAskedQuestionsAndPresentCategoriesOnce() {
        let item = HelpCenterTableModel(id: 1, title: "Category 0", description: nil, section: .helpCenterCategory, showMore: false, timestamp: nil)
        let id = 1
        let title = "Category 0"
        sut.requestDidSelect(item)

        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentStateArticlesCategoriesCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentErrorCallsCount, 0)
        XCTAssertTrue(presenterSpy.didNextStepActionReceivedInvocations.contains(where: {$0 == .openCategory(id: id, title: title)}))
    }

    func testLoadViewInfo_WhenIsMessageHistorySectionRequest_ShouldCallPresentLoadingAndPresentFrequentlyAskedQuestionsAndPresentCategoriesOnce() {
        let item = HelpCenterTableModel(id: nil, title: "Message", description: nil, section: .messageHistory, showMore: false, timestamp: nil)
        sut.requestDidSelect(item)

        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentStateArticlesCategoriesCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentErrorCallsCount, 0)
        XCTAssertTrue(presenterSpy.didNextStepActionReceivedInvocations.contains(where: {$0 == .openMessageHistory}))
    }

    func testLoadViewInfo_WhenIsContactUsSectionRequest_ShouldCallOpenMessageHistory() {
        let item = HelpCenterTableModel(id: nil, title: "Contact US", description: nil, section: .contactUs, showMore: false, timestamp: nil)
        sut.requestDidSelect(item)

        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentStateArticlesCategoriesCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentErrorCallsCount, 0)
        XCTAssertTrue(presenterSpy.didNextStepActionReceivedInvocations.contains(where: {$0 == .openMessageHistory}))
    }
    
    func testLoadViewInfo_WhenIsNewHomeAndContactUsSectionRequest_ShouldCallOpenNewTicket() {
        serviceSpy.isNewHome = true
        let item = HelpCenterTableModel(id: nil, title: "Contact US", description: nil, section: .contactUs, showMore: false, timestamp: nil)
        sut.requestDidSelect(item)

        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentStateArticlesCategoriesCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentErrorCallsCount, 0)
        XCTAssertTrue(presenterSpy.didNextStepActionReceivedInvocations.contains(where: {$0 == .openNewTicket}))
    }

    func testRequestToClose_WhenCalledFromViewController_ShouldCallCloseNextStepActionOnce() {
        sut.requestToClose()

        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertTrue(presenterSpy.didNextStepActionReceivedInvocations.contains(where: {$0 == .close}))
    }
    
    func testRequestNewTicket_ShouldPresentNewTicketView() {
        sut.requestNewTicket()
        
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations, [.openNewTicket])
    }

    func testOpenTicket_WhenThereIsNoTickets_ShouldNotOpenAnyTicket() {
        sut.openTicket()
        
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations, [])
    }
    
    func testOpenTicket_WhenThereIsTickets_ShouldOpenTicketById() {
        serviceSpy.isNewHome = true
        serviceSpy.homeResponse = .success(createResponseData())
        sut.loadViewInfo()
        
        sut.openTicket()
        
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations, [.openTicket(ticketId: "16640")])
    }

    func testRequestDidSelect_WhenObjectSelectedIsNil_ShouldPresentNothing() {
        sut.requestDidSelect(nil)
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 0)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations, [])
    }
    
    func testRequestDidSelect_WhenObjectSelectedIsFAQAndIdIsNil_ShouldNotPresentFAQSection() {
        let item = HelpCenterTableModel(
            id: nil,
            title: "",
            description: "",
            section: .frequentlyAsked,
            showMore: false,
            timestamp: nil
        )
        
        sut.requestDidSelect(item)
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 0)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations, [])
    }
    
    func testRequestDidSelect_WhenObjectSelectedIsFAQAndIdIsCorrect_ShouldPresentFAQSection() {
        let item = HelpCenterTableModel(
            id: 1,
            title: "",
            description: "",
            section: .frequentlyAsked,
            showMore: false,
            timestamp: nil
        )
        
        sut.requestDidSelect(item)
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations, [.openArticle(id: "1")])
    }
    
    func testRequestDidSelect_WhenObjectSelectedIsHelpCenterAndIdIsCorrect_ShouldPresentArticlesSection() {
        let item = HelpCenterTableModel(
            id: 1,
            title: "Title",
            description: "",
            section: .helpCenterCategory,
            showMore: false,
            timestamp: nil
        )
        
        sut.requestDidSelect(item)
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations, [.openCategory(id: 1, title: "Title")])
    }
    
    func testRequestDidSelect_WhenObjectSelectedIsHelpCenterAndIdIsNil_ShouldNotPresentArticlesSection() {
        let item = HelpCenterTableModel(
            id: nil,
            title: "",
            description: "",
            section: .helpCenterCategory,
            showMore: false,
            timestamp: nil
        )
        
        sut.requestDidSelect(item)
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 0)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations, [])
    }
    
    func testRequestDidSelect_WhenObjectSelectedIsMessageHistoryAndItIsNewHome_ShouldPresentTicketView() {
        serviceSpy.isNewHome = true
        serviceSpy.homeResponse = .success(createResponseData())
        sut.loadViewInfo()
        let item = HelpCenterTableModel(
            id: nil,
            title: "",
            description: "",
            section: .messageHistory,
            showMore: false,
            timestamp: nil
        )
        
        sut.requestDidSelect(item)
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations, [.openTicket(ticketId: "16640")])
    }
    
    func testRequestDidSelect_WhenObjectSelectedIsMessageHistory_ShouldPresentMessageHistoryView() {
        let item = HelpCenterTableModel(
            id: nil,
            title: "",
            description: "",
            section: .messageHistory,
            showMore: false,
            timestamp: nil
        )
        
        sut.requestDidSelect(item)
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations, [.openMessageHistory])
    }
    
    func testRequestDidSelect_WhenObjectSelectedIsContactUsAndItIsNewHome_ShouldPresentNewTicketView() {
        serviceSpy.isNewHome = true
        let item = HelpCenterTableModel(
            id: nil,
            title: "",
            description: "",
            section: .contactUs,
            showMore: false,
            timestamp: nil
        )
        
        sut.requestDidSelect(item)
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations, [.openNewTicket])
    }
    
    func testRequestDidSelect_WhenObjectSelectedIsContactUsAndItIsNotNewHome_ShouldPresentMessageHistoryView() {
        let item = HelpCenterTableModel(
            id: nil,
            title: "",
            description: "",
            section: .contactUs,
            showMore: false,
            timestamp: nil
        )
        
        sut.requestDidSelect(item)
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations, [.openMessageHistory])
    }
    
    func testRequestDidSelect_WhenObjectSelectedIsGreetingsAndItIsNotNewHome_ShouldPresentNothing() {
        let item = HelpCenterTableModel(
            id: nil,
            title: "",
            description: "",
            section: .greetings,
            showMore: false,
            timestamp: nil
        )
        
        sut.requestDidSelect(item)
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 0)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations, [])
    }
    
    func testRequestDidSelect_WhenObjectSelectedIsMessageHistoryAndItIsNewHomeAndUserHasNoTickets_ShouldPresentNothing() {
        serviceSpy.isNewHome = true
        serviceSpy.homeResponse = .success(createResponseData(from: "helpcenter-home-no-tickets"))
        sut.loadViewInfo()
        let item = HelpCenterTableModel(
            id: nil,
            title: "",
            description: "",
            section: .messageHistory,
            showMore: false,
            timestamp: nil
        )
        
        sut.requestDidSelect(item)
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 0)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations, [])
    }
    
    func testOpenTicket_WhenItIsNewHomeAndUserHasNoTickets_ShouldPresentNothing() {
        serviceSpy.isNewHome = true
        serviceSpy.homeResponse = .success(createResponseData(from: "helpcenter-home-no-tickets"))
        sut.loadViewInfo()
        
        sut.openTicket()
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 0)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations, [])
    }
    
    func testRequestArticle_WhenIsNewHome_ShouldPresentArticleAndTrackEvent() {
        serviceSpy.isNewHome = true
        let expectedProperties = InteractionEventProperties(
            screenName: .home,
            interactionType: .article,
            section: nil,
            category: nil,
            article: String(describing: 12345)
        )
        
        sut.requestArticle(id: 12345)
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations, [.openArticle(id: "12345")])
        XCTAssertTrue(
            analyticsSpy.equals(
                to: HelpCenterNewEvents.interacted(expectedProperties).event()
            )
        )
    }
    
    func testRequestArticle_WhenIsNotNewHome_ShouldPresentArticleAndTrackEvent() {
        sut.requestArticle(id: 12345)
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations, [.openArticle(id: "12345")])
        XCTAssertTrue(
            analyticsSpy.equals(
                to: HelpCenterEvent.openContent(type: .article, id: "12345").event()
            )
        )
    }
    
    func testRequestCategory_WhenIsNewHome_ShouldPresentCategoryAndTrackEvent() {
        serviceSpy.isNewHome = true
        let expectedProperties = InteractionEventProperties(
            screenName: .home,
            interactionType: .category,
            section: nil,
            category: String(describing: 12345),
            article: nil
        )
        
        sut.requestCategory(id: 12345, title: "Category")
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations, [.openCategory(id: 12345, title: "Category")])
        XCTAssertTrue(
            analyticsSpy.equals(
                to: HelpCenterNewEvents.interacted(expectedProperties).event()
            )
        )
    }
    
    func testRequestCategory_WhenIsNotNewHome_ShouldPresentCategoryAndTrackEvent() {
        sut.requestCategory(id: 12345, title: "Category")
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations, [.openCategory(id: 12345, title: "Category")])
        XCTAssertTrue(
            analyticsSpy.equals(
                to: HelpCenterEvent.openContent(type: .category, id: "12345").event()
            )
        )
    }
    
    func testRequestMessageHistory_WhenIsNewHome_ShouldPresentMessageHistoryAndTrackEvent() {
        serviceSpy.isNewHome = true
        let expectedProperties = ButtonClickedEventProperties(screenName: .home, buttonName: .seeMore)
        
        sut.requestMessageHistory()
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations, [.openMessageHistory])
        XCTAssertTrue(
            analyticsSpy.equals(
                to: HelpCenterNewEvents.buttonClicked(expectedProperties).event()
            )
        )
    }
    
    func testRequestMessageHistory_WhenIsNotNewHome_ShouldPresentMessageHistoryAndTrackEvent() {
        sut.requestMessageHistory()
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations, [.openMessageHistory])
        XCTAssertTrue(
            analyticsSpy.equals(
                to: HelpCenterEvent.tapMessageHistory.event()
            )
        )
    }

    func testLoadViewInfo_WhenIsNewHomeAndRequestSucceeds_ShouldTrackEvent() {
        serviceSpy.isNewHome = true
        serviceSpy.homeResponse = .success(createResponseData())
        let expectedProperties = HomeEventProperties(
            screenName: .home,
            hasOpenTicket: true,
            isCategoryEmpty: false,
            isError: false,
            isFatEmpty: false
        )
        
        sut.loadViewInfo()
        
        XCTAssertTrue(
            analyticsSpy.equals(
                to: HelpCenterNewEvents.accessed(expectedProperties).event()
            )
        )
    }
    
    func testLoadViewInfo_WhenIsNewHomeAndRequestFails_ShouldPresentErrorAndTrackEvent() {
        serviceSpy.isNewHome = true
        serviceSpy.homeResponse = .failure(.bodyNotFound)
        let expectedProperties = HomeEventErrorProperties(
            screenName: .home,
            isError: true
        )
        
        sut.loadViewInfo()
        
        XCTAssertEqual(presenterSpy.presentErrorCallsCount, 1)
        XCTAssertTrue(
            analyticsSpy.equals(
                to: HelpCenterNewEvents.accessed(expectedProperties).event()
            )
        )
    }
}

private extension HelpCenterInteractorTest {
    func createResponseData(from resource: String = "helpcenter-home-tickets") -> HomeResponse {
        do {
            return try Mocker().decode(HomeResponse.self, fromResource: resource)
        } catch {
            fatalError("Error while decoding mock data: \(error)")
        }
    }
}
