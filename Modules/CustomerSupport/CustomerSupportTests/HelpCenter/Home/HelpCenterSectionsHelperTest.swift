@testable import CustomerSupport
import XCTest

class HelpCenterSectionsHelperTest: XCTestCase {
    func testTitleSection_WhenCalledTitleHeader_ShouldReturnTextContactUs() {
        let expected =  Strings.HelpCenter.doYouNeedAnyHelpYet
        let model = HelpCenterTableModel(id: nil, title: "aaa", description: nil, section: .contactUs, showMore: false, timestamp: nil)
        XCTAssertEqual(HelpCenterSectionsHelper.titleHeader(from: model), expected)
    }
    
    func testTitleSection_WhenCalledTitleHeader_ShouldReturnTextFrequentlyAsked() {
        let expected = Strings.HelpCenter.frequentlyAskedQuestions
        let model = HelpCenterTableModel(id: nil, title: "aaa", description: nil, section: .frequentlyAsked, showMore: false, timestamp: nil)
        XCTAssertEqual(HelpCenterSectionsHelper.titleHeader(from: model), expected)
    }
    
    func testTitleSection_WhenCalledTitleHeader_ShouldReturnTextCategory() {
        let expected = Strings.HelpCenter.categories
        let model = HelpCenterTableModel(id: nil, title: "aaa", description: nil, section: .helpCenterCategory, showMore: false, timestamp: nil)
        XCTAssertEqual(HelpCenterSectionsHelper.titleHeader(from: model), expected)
    }
    
    func testTitleSection_WhenCalledTitleHeader_ShouldReturnTextHistory() {
        let expected = Strings.HelpCenter.doYouNeedAnyHelpYet
        let model = HelpCenterTableModel(id: nil, title: "aaa", description: nil, section: .messageHistory, showMore: false, timestamp: nil)
        XCTAssertEqual(HelpCenterSectionsHelper.titleHeader(from: model), expected)
    }
    
    func testCellSection_WhenCalledGreetingsCell_ShouldReturnNil() {
        let model = HelpCenterTableModel(id: nil, title: "aaa", description: nil, section: .greetings, showMore: false, timestamp: nil)
        XCTAssertNil(HelpCenterSectionsHelper.titleHeader(from: model))
    }
}
