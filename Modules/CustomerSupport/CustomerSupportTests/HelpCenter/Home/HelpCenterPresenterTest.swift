@testable import CustomerSupport
import UI
import XCTest

fileprivate class HelpCenterCoordinatorSpy: HelpCenterCoordinating {
    var viewController: UIViewController?

    private(set) var didCallPerform = 0
    private(set) var didCallPerformArticleResearchAction = 0
    private(set) var action: HelpCenterAction?
    private(set) var articleAction: ArticleResearchAction?

    func perform(action: HelpCenterAction) {
        didCallPerform += 1
        self.action = action
    }

    func perform(action: ArticleResearchAction) {
        didCallPerformArticleResearchAction += 1
        articleAction = action
    }
}

private final class HelpCenterDisplayingSpy: HelpCenterDisplaying {
    private(set) var displaySearchCallsCount = 0
    private(set) var displayErrorCallsCount = 0
    private(set) var displayItemsInCallsCount = 0
    private(set) var displayItemsInReceivedInvocations: [(items: [HelpCenterTableModel], section: Int)] = []
    private(set) var startLoadingCallsCount = 0
    private(set) var stopLoadingCallsCount = 0

    func displaySearch() {
        displaySearchCallsCount += 1
    }

    func displayError() {
        displayErrorCallsCount += 1
    }

    func display(items: [HelpCenterTableModel], in section: Int) {
        displayItemsInCallsCount += 1
        displayItemsInReceivedInvocations.append((items: items, section: section))
    }

    func startLoading() {
        startLoadingCallsCount += 1
    }

    func stopLoading() {
        stopLoadingCallsCount += 1
    }
}

final class HelpCenterPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = HelpCenterCoordinatorSpy()
    private lazy var viewControllerSpy = HelpCenterDisplayingSpy()
    private lazy var sut: HelpCenterPresenter = {
        let presenter = HelpCenterPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testPerformAction_WhenCalledFromIteractor_PerFormAction() {
        sut.didNextStep(action: .openArticle(id: "aaa"))
        sut.didNextStep(action: .openCategory(id: 12345, title: "Testing"))
        sut.didNextStep(action: .openMessageHistory)
        sut.didNextStep(action: .openNewTicket)
        XCTAssertEqual(coordinatorSpy.didCallPerformArticleResearchAction, 0)
        XCTAssertEqual(coordinatorSpy.didCallPerform, 4)
    }
    
    func testStartLoading_WhenCalledFromIteractor_ShouldCallStartLoading() {
        sut.startLoading()
        XCTAssertEqual(viewControllerSpy.startLoadingCallsCount, 1)
    }
    
    func testStoptLoading_WhenCalledFromIteractor_ShouldCallStopLoading() {
        sut.stopLoading()
        XCTAssertEqual(viewControllerSpy.stopLoadingCallsCount, 1)
    }
    
    func testDisplayError_WhenCalledFromIteractor_ShouldCallDisplayError() {
        sut.presentError()
        XCTAssertEqual(viewControllerSpy.displayErrorCallsCount, 1)
    }
    
    func testDisplaySearch_WhenCalledFromIteractor_ShouldCallDisplaySearch() {
        sut.presentSearch()
        XCTAssertEqual(viewControllerSpy.displaySearchCallsCount, 1)
    }

    func testPresentFrequentlyAsked_WhenCalledFromViewModel_ShouldCallHideLoadingAndDisplayFrequentlyAskedOneTime() {
        let testQuestions = [
            Article(id: 0, title: "Teste 1"),
            Article(id: 1, title: "Teste 2"),
            Article(id: 2, title: "Teste 3")
        ]
        let expectedModels = [
            HelpCenterTableModel(id: 0, title: "Teste 1", description: nil, section: .frequentlyAsked, showMore: false, timestamp: nil),
            HelpCenterTableModel(id: 1, title: "Teste 2", description: nil, section: .frequentlyAsked, showMore: false, timestamp: nil),
            HelpCenterTableModel(id: 2, title: "Teste 3", description: nil, section: .frequentlyAsked, showMore: false, timestamp: nil)
        ]
        
        sut.present(sections: HelpCenterState.hasTicket.sections, articles: testQuestions, categories: [])
        
        XCTAssertEqual(viewControllerSpy.displayErrorCallsCount, 0)
        XCTAssertEqual(viewControllerSpy.displayItemsInCallsCount, 2)
        XCTAssertEqual(viewControllerSpy.displaySearchCallsCount, 0)
        XCTAssertTrue(viewControllerSpy.displayItemsInReceivedInvocations.contains(where: { element -> Bool in
            element.items == expectedModels
        }))
    }

    func testPresentCategories_WhenCalledFromViewModel_ShouldCallHideLoadingAndDisplayCategoriesOneTime() {
        let testCategories = [
            HelpCenterCategory(id: 0, name: "Teste 1"),
            HelpCenterCategory(id: 1, name: "Teste 2"),
            HelpCenterCategory(id: 2, name: "Teste 3")
        ]
        let expectedModels = [
            HelpCenterTableModel(id: 0, title: "Teste 1", description: nil, section: .helpCenterCategory, showMore: false, timestamp: nil),
            HelpCenterTableModel(id: 1, title: "Teste 2", description: nil, section: .helpCenterCategory, showMore: false, timestamp: nil),
            HelpCenterTableModel(id: 2, title: "Teste 3", description: nil, section: .helpCenterCategory, showMore: false, timestamp: nil)
        ]

        sut.present(sections: HelpCenterState.hasTicket.sections, articles: [], categories: testCategories)

        XCTAssertEqual(viewControllerSpy.displayErrorCallsCount, 0)
        XCTAssertEqual(viewControllerSpy.displayItemsInCallsCount, 2)
        XCTAssertEqual(viewControllerSpy.displaySearchCallsCount, 0)
        XCTAssertTrue(viewControllerSpy.displayItemsInReceivedInvocations.contains(where: { element -> Bool in
            element.items == expectedModels
        }))
    }

    func testPresentContactUs_WhenCalledFromViewModel_ShouldCallDisplayContactUsOneTime() {
        let message = HelpCenterContactMessage()
        let expectedModels = [
            HelpCenterTableModel(
                id: nil,
                title: message.title,
                description: message.description,
                section: .contactUs,
                showMore: false,
                timestamp: nil
            )
        ]

        sut.present(sections: HelpCenterState.hasNotTicket.sections, articles: [], categories: [])
        
        XCTAssertEqual(viewControllerSpy.displayErrorCallsCount, 0)
        XCTAssertEqual(viewControllerSpy.displayItemsInCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.displaySearchCallsCount, 0)
        XCTAssertTrue(viewControllerSpy.displayItemsInReceivedInvocations.contains(where: { element -> Bool in
            element.items == expectedModels
        }))
    }

    func testPresenteMessageHistory_WhenCalledFromViewModel_ShouldCallDisplayMessageHistoryOneTime() {
        let message = HelpCenterMessageHistoryBody()
        let expectedModels = [
            HelpCenterTableModel(
                id: nil,
                title: message.title,
                description: message.description,
                section: .messageHistory,
                showMore: false,
                timestamp: ""
            )
        ]
        
        sut.present(sections: HelpCenterState.hasTicket.sections, articles: [], categories: [])

        XCTAssertEqual(viewControllerSpy.displayErrorCallsCount, 0)
        XCTAssertEqual(viewControllerSpy.displayItemsInCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.displaySearchCallsCount, 0)
        XCTAssertTrue(viewControllerSpy.displayItemsInReceivedInvocations.contains(where: { element -> Bool in
            element.items == expectedModels
        }))
    }

    func testPresentError_WhenCalledFromViewModel_ShouldCallDisplayErrorOneTime() {
        sut.presentError()

        XCTAssertEqual(viewControllerSpy.displayErrorCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.displayItemsInCallsCount, 0)
        XCTAssertEqual(viewControllerSpy.displaySearchCallsCount, 0)
    }
    
    func testPresentNewHome_WhenResponseIsEmpty_ShouldNotDisplayFrequentlyAskedQuestionsSection() {
        let homeResponse = HomeResponse(
            username: "",
            tickets: [],
            articles: [],
            categories: [],
            hadTickets: false
        )
        
        sut.presentNewHome(sections: [.frequentlyAsked], response: homeResponse)
        
        XCTAssertEqual(viewControllerSpy.displayItemsInCallsCount, 0)
    }
    
    func testPresentNewHome_WhenResponseHasCategories_ShouldDisplayHelpCenterCategoriesSection() {
        let expectedModel: [HelpCenterTableModel] = [
            .init(
                id: 1,
                title: "Title 1",
                description: "Snippet 1",
                section: .helpCenterCategory,
                showMore: false,
                timestamp: nil
            ),
            .init(
                id: 2,
                title: "Title 2",
                description: "Snippet 2",
                section: .helpCenterCategory,
                showMore: false,
                timestamp: nil
            ),
            .init(
                id: 3,
                title: "Title 3",
                description: "Snippet 3",
                section: .helpCenterCategory,
                showMore: false,
                timestamp: nil
            )
        ]
        let categories: [HelpCenterCategory] = [
            .init(id: 1, name: "Title 1", description: "Snippet 1"),
            .init(id: 2, name: "Title 2", description: "Snippet 2"),
            .init(id: 3, name: "Title 3", description: "Snippet 3")
        ]
        let homeResponse = HomeResponse(
            username: "",
            tickets: [],
            articles: [],
            categories: categories,
            hadTickets: false
        )
        
        sut.presentNewHome(sections: [.helpCenterCategory], response: homeResponse)
        
        XCTAssertEqual(viewControllerSpy.displayItemsInCallsCount, 1)
        guard let items = viewControllerSpy.displayItemsInReceivedInvocations.first?.items else {
            return XCTFail("Array of HelpCenterTableModel inside invocations is empty")
        }
        XCTAssertEqual(items, expectedModel)
    }
    
    func testPresentNewHome_WhenResponseHasArticles_ShouldDisplayFrequentlyAskedQuestionsSection() {
        let expectedModel: [HelpCenterTableModel] = [
            .init(
                id: 1,
                title: "Title 1",
                description: "Snippet 1",
                section: .frequentlyAsked,
                showMore: false,
                timestamp: nil
            ),
            .init(
                id: 2,
                title: "Title 2",
                description: "Snippet 2",
                section: .frequentlyAsked,
                showMore: false,
                timestamp: nil
            )
        ]
        let articles: [HelpCenterArticle] = [
            .init(
                id: 1,
                title: "Title 1",
                snippet: "Snippet 1",
                section: .init(id: 11, name: "Section 1"),
                category: .init(id: 12, name: "Category 1")
            ),
            .init(
                id: 2,
                title: "Title 2",
                snippet: "Snippet 2",
                section: .init(id: 21, name: "Section 2"),
                category: .init(id: 22, name: "Category 2")
            )
        ]
        let homeResponse = HomeResponse(
            username: "",
            tickets: [],
            articles: articles,
            categories: [],
            hadTickets: false
        )
        
        sut.presentNewHome(sections: [.frequentlyAsked], response: homeResponse)
        
        XCTAssertEqual(viewControllerSpy.displayItemsInCallsCount, 1)
        guard let items = viewControllerSpy.displayItemsInReceivedInvocations.first?.items else {
            return XCTFail("Array of HelpCenterTableModel inside invocations is empty")
        }
        XCTAssertEqual(items, expectedModel)
    }
    
    func testPresentNewHome_WhenUserAlreadyHadTickets_ShouldDisplayContactUsSection() {
        let expectedModel: [HelpCenterTableModel] = [
            .init(
                id: nil,
                title: "Precisa de ajuda?",
                description: "Toque aqui pra falar com a gente!\nVamos te ajudar.",
                section: .contactUs,
                showMore: true,
                timestamp: nil
            )
        ]
        let homeResponse = HomeResponse(
            username: "",
            tickets: [],
            articles: [],
            categories: [],
            hadTickets: true
        )
        
        sut.presentNewHome(sections: [.contactUs], response: homeResponse)
        
        XCTAssertEqual(viewControllerSpy.displayItemsInCallsCount, 1)
        guard let items = viewControllerSpy.displayItemsInReceivedInvocations.first?.items else {
            return XCTFail("Array of HelpCenterTableModel inside invocations is empty")
        }
        
        XCTAssertEqual(items, expectedModel)
    }
    
    func testPresentNewHome_WhenUserNeverHadTickets_ShouldNotDisplayContactUsSection() {
        let expectedModel: [HelpCenterTableModel] = [
            .init(
                id: nil,
                title: "Precisa de ajuda?",
                description: "Toque aqui pra falar com a gente!\nVamos te ajudar.",
                section: .contactUs,
                showMore: false,
                timestamp: nil
            )
        ]
        let homeResponse = HomeResponse(
            username: "",
            tickets: [],
            articles: [],
            categories: [],
            hadTickets: false
        )
        
        sut.presentNewHome(sections: [.contactUs], response: homeResponse)
        
        XCTAssertEqual(viewControllerSpy.displayItemsInCallsCount, 1)
        guard let items = viewControllerSpy.displayItemsInReceivedInvocations.first?.items else {
            return XCTFail("Array of HelpCenterTableModel inside invocations is empty")
        }
        
        XCTAssertEqual(items, expectedModel)
    }
    
    func testPresentNewHome_WhenResponseHasUserName_ShouldDisplayGreetingsSection() {
        let expectedModel: [HelpCenterTableModel] = [
            .init(
                id: nil,
                title: "Oi,\n@JoaoEstevesAqui",
                description: "Aqui você conhece tudo sobre o PicPay.\nPrecisa de ajuda? Conta com a gente!",
                section: .greetings,
                showMore: false,
                timestamp: nil
            )
        ]
        let homeResponse = HomeResponse(
            username: "JoaoEstevesAqui",
            tickets: [],
            articles: [],
            categories: [],
            hadTickets: false
        )
        
        sut.presentNewHome(sections: [.greetings], response: homeResponse)
        
        XCTAssertEqual(viewControllerSpy.displayItemsInCallsCount, 1)
        guard let items = viewControllerSpy.displayItemsInReceivedInvocations.first?.items else {
            return XCTFail("Array of HelpCenterTableModel inside invocations is empty")
        }
        
        XCTAssertEqual(items, expectedModel)
    }
    
    func testPresentNewHome_WhenResponseHasNoUserName_ShouldNotDisplayGreetingsSection() {
        let homeResponse = HomeResponse(
            username: "",
            tickets: [],
            articles: [],
            categories: [],
            hadTickets: false
        )
        
        sut.presentNewHome(sections: [.greetings], response: homeResponse)
        
        XCTAssertEqual(viewControllerSpy.displayItemsInCallsCount, 0)
    }
    
    func testPresentNewHome_WhenResponseHasTickets_ShouldDisplayMessageHistorySection() {
        let expectedModel: [HelpCenterTableModel] = [
            .init(
                id: nil,
                title: "Name 2",
                description: "Message 2",
                section: .messageHistory,
                showMore: true,
                timestamp: "19:11"
            )
        ]
        let tickets: [HelpCenterTicket] = [
            .init(
                id: 1,
                status: "Status 1",
                created: "2021-03-31T19:11:08Z",
                updated: "2021-03-31T20:10:18Z",
                subject: "Subject 1",
                description: "Description 1",
                tags: ["tag__test"],
                comments: [
                    .init(id: 11, body: "Message 1", created: "2021-03-30T19:11:08Z", author: .init(id: 21, name: "Name 1")),
                    .init(id: 12, body: "Message 2", created: "2021-03-31T19:11:08Z", author: .init(id: 22, name: "Name 2")),
                ]
            )
        ]
        let homeResponse = HomeResponse(
            username: "",
            tickets: tickets,
            articles: [],
            categories: [],
            hadTickets: true
        )
        
        sut.presentNewHome(sections: [.messageHistory], response: homeResponse)
        
        XCTAssertEqual(viewControllerSpy.displayItemsInCallsCount, 1)
        guard let items = viewControllerSpy.displayItemsInReceivedInvocations.first?.items else {
            return XCTFail("Array of HelpCenterTableModel inside invocations is empty")
        }

        XCTAssertEqual(items, expectedModel)
    }
    
    func testPresentNewHome_WhenResponseHasNoTickets_ShouldNotDisplayMessageHistorySection() {
        let homeResponse = HomeResponse(
            username: "",
            tickets: [],
            articles: [],
            categories: [],
            hadTickets: true
        )
        
        sut.presentNewHome(sections: [.messageHistory], response: homeResponse)
        
        XCTAssertEqual(viewControllerSpy.displayItemsInCallsCount, 0)
    }
    
    func testPresentNewHome_WhenResponseHasTicketsAndTicketHasNoComments_ShouldNotDisplayMessageHistorySection() {
        let tickets: [HelpCenterTicket] = [
            .init(
                id: 1,
                status: "Status 1",
                created: "2021-03-31T19:11:08Z",
                updated: "2021-03-31T20:10:18Z",
                subject: "Subject 1",
                description: "Description 1",
                tags: ["tag__test"],
                comments: []
            )
        ]
        let homeResponse = HomeResponse(
            username: "",
            tickets: tickets,
            articles: [],
            categories: [],
            hadTickets: true
        )
        
        sut.presentNewHome(sections: [.messageHistory], response: homeResponse)
        
        XCTAssertEqual(viewControllerSpy.displayItemsInCallsCount, 0)
    }
}
