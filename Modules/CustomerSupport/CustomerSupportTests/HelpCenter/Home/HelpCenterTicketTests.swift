@testable import CustomerSupport
import UI
import XCTest

final class HelpCenterTicketTests: XCTestCase {
    
    func testUpdatedProperty_WhenUpdateAtPropertyIsEmpty_ShouldReturnNilDate() {
        let sut = HelpCenterTicket(
            id: 10,
            status: "",
            created: "",
            updated: "",
            subject: "",
            description: "",
            tags: [],
            comments: []
        ).updatedDate
        
        XCTAssertNil(sut)
    }
    
    func testUpdatedProperty_WhenUpdateAtPropertyIsInWrongFormat_ShouldReturnNilDate() {
        let sut = HelpCenterTicket(
            id: 10,
            status: "",
            created: "2021-03-30",
            updated: "2021-03-30",
            subject: "",
            description: "",
            tags: [],
            comments: []
        ).updatedDate
        
        XCTAssertNil(sut)
    }
    
    func testSortCommentListByDate_WhenCommentListIsNotEmpty_ShouldReturnAscendantSortedList() {
        let comments = [
            HelpCenterTicketComment(
                id: 10,
                body: "",
                created: "2021-03-31T19:11:08Z",
                author: HelpCenterTicketCommentAuthor(id: 10, name: "")
            ),
            HelpCenterTicketComment(
                id: 20,
                body: "",
                created: "2021-03-31T19:12:08Z",
                author: HelpCenterTicketCommentAuthor(id: 20, name: "")
            ),
            HelpCenterTicketComment(
                id: 30,
                body: "",
                created: "2021-03-30T19:11:08Z",
                author: HelpCenterTicketCommentAuthor(id: 30, name: "")
            )
        ]
      
        let sut = HelpCenterTicket(
            id: 10,
            status: "",
            created: "",
            updated: "",
            subject: "",
            description: "",
            tags: [],
            comments: comments
        ).commentListSortedByDate
        
        XCTAssertEqual(sut.first, comments.last)
    }
    
    func testSortTicketListByDate_WhenCommentComesWithNoDate_ShouldReturnUnsortedList() {
        let comments = [
            HelpCenterTicketComment(
                id: 10,
                body: "",
                created: "",
                author: HelpCenterTicketCommentAuthor(id: 11, name: "")
            ),
            HelpCenterTicketComment(
                id: 20,
                body: "",
                created: "",
                author: HelpCenterTicketCommentAuthor(id: 21, name: "")
            ),
            HelpCenterTicketComment(
                id: 30,
                body: "",
                created: "",
                author: HelpCenterTicketCommentAuthor(id: 31, name: "")
            )
        ]
      
        let sut = HelpCenterTicket(
            id: 1,
            status: "",
            created: "",
            updated: "",
            subject: "",
            description: "",
            tags: [],
            comments: comments
        ).commentListSortedByDate

        XCTAssertEqual(sut.last?.id, comments.last?.id)
    }
}
