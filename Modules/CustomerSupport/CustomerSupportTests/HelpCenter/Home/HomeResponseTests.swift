@testable import CustomerSupport
import UI
import XCTest

final class HomeResponseTests: XCTestCase {
    func testSortTicketListByDate_WhenTicketListIsNotEmpty_ShouldReturnAscendantSortedList() {
        let tickets = [
            HelpCenterTicket(
                id: 20,
                status: "",
                created: "",
                updated: "2021-03-31T17:05:20Z",
                subject: "",
                description: "",
                tags: [],
                comments: []
            ),
            HelpCenterTicket(
                id: 10,
                status: "",
                created: "",
                updated: "2021-03-30T19:11:08Z",
                subject: "",
                description: "",
                tags: [],
                comments: []
            )
        ]
        
        let sut = HomeResponse(
            username: "",
            tickets: tickets,
            articles: [],
            categories: [],
            hadTickets: false
        ).ticketListSortedByDate
        
        XCTAssertEqual(sut.last, tickets.first)
    }
    
    func testSortTicketListByDate_WhenTicketComesWithNoDate_ShouldReturnUnsortedList() {
        let tickets = [
            HelpCenterTicket(
                id: 20,
                status: "",
                created: "",
                updated: "",
                subject: "",
                description: "",
                tags: [],
                comments: []
            ),
            HelpCenterTicket(
                id: 10,
                status: "",
                created: "",
                updated: "",
                subject: "",
                description: "",
                tags: [],
                comments: []
            )
        ]
        
        let sut = HomeResponse(
            username: "",
            tickets: tickets,
            articles: [],
            categories: [],
            hadTickets: false
        ).ticketListSortedByDate
        
        XCTAssertEqual(sut.last?.id, tickets.last?.id)
    }
}
