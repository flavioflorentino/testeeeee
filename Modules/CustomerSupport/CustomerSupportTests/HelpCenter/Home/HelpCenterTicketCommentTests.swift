@testable import CustomerSupport
import UI
import XCTest

final class HelpCenterTicketCommentTests: XCTestCase {
    func testCreatedProperty_WhenCreatedAtPropertyIsEmpty_ShouldReturnNilDate() {
        let sut = HelpCenterTicketComment(
            id: 10,
            body: "",
            created: "",
            author: HelpCenterTicketCommentAuthor(id: 11, name: "")
        ).createdDate

        XCTAssertNil(sut)
    }
    
    func testCreatedProperty_WhenCreatedAtPropertyIsInWrongFormat_ShouldReturnNilDate() {
        let sut = HelpCenterTicketComment(
            id: 10,
            body: "",
            created: "2021-03-30",
            author: HelpCenterTicketCommentAuthor(id: 11, name: "")
        ).createdDate

        XCTAssertNil(sut)
    }
    
    func testCreatedProperty_WhenCreatedAtPropertyIsInCorrectFormat_ShouldReturnDateObject() throws {
        let expectedDate = "2021-03-31T19:11:08Z"
    
        let sut = HelpCenterTicketComment(
            id: 10,
            body: "",
            created: expectedDate,
            author: HelpCenterTicketCommentAuthor(id: 11, name: "")
        )

        let createdDate = try XCTUnwrap(sut.createdDate)

        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        let stringDate = dateFormatter.string(from: createdDate)

        XCTAssertEqual(stringDate, expectedDate)
    }
    
    func testCreatedProperty_WhenCreatedAtPropertyIsInIncorrectFormat_ShouldReturnFormattedDate() {
        let sut = HelpCenterTicketComment(
            id: 10,
            body: "",
            created: "2021-03-31",
            author: HelpCenterTicketCommentAuthor(id: 11, name: "")
        )

        XCTAssertEqual(sut.formattedDate, "")
    }
    
    func testCreatedProperty_WhenCreatedAtPropertyIsInCorrectFormat_ShouldReturnFormattedDate() {
        let sut = HelpCenterTicketComment(
            id: 10,
            body: "",
            created: "2021-03-31T19:11:08Z",
            author: HelpCenterTicketCommentAuthor(id: 11, name: "")
        )

        XCTAssertEqual(sut.formattedDate, "19:11")
    }
}
