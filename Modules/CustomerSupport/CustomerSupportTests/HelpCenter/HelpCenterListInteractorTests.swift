@testable import CustomerSupport
import Core
import Foundation
import AnalyticsModule
import XCTest

private class HelpCenterListServicingMock: HelpCenterListServicing {

    //MARK: - requestList
    private(set) var requestListCompletionCallsCount = 0
    private(set) var requestListCompletionReceivedInvocations: [((Result<[HelpCenterListModel], ApiError>) -> Void)] = []
    var result: Result<[HelpCenterListModel], ApiError>?

    func requestList(completion: @escaping ((Result<[HelpCenterListModel], ApiError>) -> Void)) {
        requestListCompletionCallsCount += 1
        requestListCompletionReceivedInvocations.append(completion)
        guard let result = result else { return }
        completion(result)
    }
}

private final class HelpCenterListPresentingSpy: HelpCenterListPresenting {
    var viewController: HelpCenterListDisplaying?

    //MARK: - present
    private(set) var presentObjectsTitleCallsCount = 0
    private(set) var presentObjectsObjects: [HelpCenterListModel] = []
    private(set) var presentObjectsTitle: String?

    func present(objects: [HelpCenterListModel], title: String?) {
        presentObjectsTitleCallsCount += 1
        presentObjectsObjects = objects
        presentObjectsTitle = title
    }

    //MARK: - present
    private(set) var presentErrorCallsCount = 0
    private(set) var presentErrorReceivedInvocations: [ApiError] = []

    func present(error: ApiError) {
        presentErrorCallsCount += 1
        presentErrorReceivedInvocations.append(error)
    }

    //MARK: - didNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didNextStepActionReceivedInvocations: [HelpCenterListAction] = []

    func didNextStep(action: HelpCenterListAction) {
        didNextStepActionCallsCount += 1
        didNextStepActionReceivedInvocations.append(action)
    }
}

final class HelpCenterListInteractorTests: XCTestCase {
    private lazy var presenterSpy = HelpCenterListPresentingSpy()
    private lazy var serviceMock = HelpCenterListServicingMock()
    private lazy var analyticsSpy = AnalyticsSpy()
    private lazy var dependency = CustomerSupportContainerMock(analytics: analyticsSpy)
    private lazy var title = "A test title"
    private lazy var sut = HelpCenterListInteractor(
        service: serviceMock,
        presenter: presenterSpy,
        screenName: .home,
        dependencies: dependency,
        sectionId: 54321,
        categoryId: 98765,
        title: title
    )

    func testRequestHelpCenterObjectsList_WhenSuccessful_ShouldCallPresentObjectsFromPresenter() {
        let articles = [
            Article(id: 12345, title: "Test 1"),
            Article(id: 32542, title: "Test 2"),
        ]
        serviceMock.result = .success(articles)

        sut.requestHelpCenterObjectsList()

        XCTAssertEqual(presenterSpy.presentObjectsTitleCallsCount, 1)
        XCTAssert(presenterSpy.presentObjectsObjects.allSatisfy({articles.contains($0 as! Article)}))
    }

    func testRequestHelpCenterObjectsList_WhenFailute_ShouldCallPresentErrorFromPresenter() {
        serviceMock.result = .failure(.bodyNotFound)

        sut.requestHelpCenterObjectsList()

        XCTAssertEqual(presenterSpy.presentErrorCallsCount, 1)
    }

    func testRequestDidSelect_WhenCalledFromViewControler_ShouldCallPresentDidNextStepFromPresenter() {
        let article = Article(id: 123456, title: "Test")

        sut.requestDidSelect(article)

        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
    }
    
    func testRequestHelpCenterObjectsList_WhenScreenIsSection_ShouldTrackEvent() {
        let sut = HelpCenterListInteractor(
            service: serviceMock,
            presenter: presenterSpy,
            screenName: .section,
            dependencies: dependency,
            sectionId: 54321,
            categoryId: nil,
            title: title
        )
        let expectedProperties = ListEventProperties(
            screenName: .section,
            section: "54321",
            category: nil,
            article: nil
        )
                                   
        sut.requestHelpCenterObjectsList()
        
        XCTAssertTrue(
            analyticsSpy.equals(
                to: HelpCenterNewEvents.accessed(expectedProperties).event()
            )
        )
    }
    
    func testRequestHelpCenterObjectsList_WhenScreenIsCategory_ShouldTrackEvent() {
        let sut = HelpCenterListInteractor(
            service: serviceMock,
            presenter: presenterSpy,
            screenName: .categories,
            dependencies: dependency,
            sectionId: 54321,
            categoryId: 98765,
            title: title
        )
        let expectedProperties = ListEventProperties(
            screenName: .categories,
            section: "54321",
            category: "98765",
            article: nil
        )
                                   
        sut.requestHelpCenterObjectsList()
        
        XCTAssertTrue(
            analyticsSpy.equals(
                to: HelpCenterNewEvents.accessed(expectedProperties).event()
            )
        )
    }
}
