import UI

public enum LoanOfferFlow {
    case hire
    case registration(status: RegistrationStatus)
    case warning(_ warning: ApolloFeedbackViewContent)
}

public protocol LoanOfferCoordinating: Coordinating {
    func start(flow: LoanOfferFlow, didFinish: @escaping () -> Void)
    func finish()
    func close()
}

extension LoanOfferCoordinating {
    func start(flow: LoanOfferFlow) {
        start(flow: flow, didFinish: {})
    }
}

public final class LoanOfferCoordinator: LoanOfferCoordinating {
    public var childViewController: [UIViewController] = []
    public var viewController: UIViewController?
    private let navigationController: UINavigationController
    private var childCoordinators: [Coordinating] = []
    
    private let origin: OriginType
    private let walletNeedReload = "WalletNeedReload"
    var didFinish: () -> Void = {}
    
    public init(with navigationController: UINavigationController, from origin: OriginType) {
        self.navigationController = navigationController
        self.origin = origin
    }
    
    public func start(flow: LoanOfferFlow, didFinish: @escaping () -> Void = {}) {
        self.didFinish = didFinish
        
        switch flow {
        case .hire:
            startHireFlow()
        case let .registration(status):
            startRegistration(action: .onboarding(status: status))
        case let .warning(warning):
            startWarning(warning)
        }
    }
    
    public func finish() {
        navigationController.dismiss(animated: true) {
            self.navigationController.popToRootViewController(animated: true)
            
            let notification = NSNotification.Name(rawValue: self.walletNeedReload)
            NotificationCenter.default.post(name: notification, object: nil)
            self.childCoordinators = []
            self.didFinish()
        }
    }
    
    public func close() {
        navigationController.popViewController(animated: true)
        childCoordinators = []
        didFinish()
    }
    
    deinit {
        childCoordinators = []
    }
}

private extension LoanOfferCoordinator {
    func startHireFlow() {
        let hire = LoanHireFlowCoordinator(from: navigationController,
                                           with: self)
        hire.perform(action: .intro(origin: origin))
        childCoordinators.append(hire)
    }
    
    func startRegistration(action: RegistrationAction) {
        let registration = LoanRegistrationFlowCoordinator(from: navigationController, with: self)
        registration.perform(action: action)
        childCoordinators.append(registration)
    }
    
    func startWarning(_ warning: ApolloFeedbackViewContent) {
        let controller = ApolloFeedbackViewController(content: warning)
        controller.didTapPrimaryButton = { self.finish() }
        controller.navigationItem.hidesBackButton = true
        navigationController.pushViewController(controller, animated: true)
    }
}
