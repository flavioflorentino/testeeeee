struct ConfirmSimulationResponse: Decodable {
    let contractId: String
    let simulationId: String
}
