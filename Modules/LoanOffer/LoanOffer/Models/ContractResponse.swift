public struct ContractResponse: Decodable, Equatable {
    let id: String
    let portion: Int
    let requestedValue: String
    let totalValue: String
    let installmentValue: String
    let monthlyCETLabel: String
    let monthlyCETPercent: String
    let monthlyIOFLabel: String
    let iofTotalValue: String
    let monthlyInterestLabel: String
    let monthlyInterestPercent: String
    let firstDueDate: String
    let dueDay: Int
    
    public static func == (lhs: ContractResponse, rhs: ContractResponse) -> Bool {
        lhs.id == rhs.id &&
            lhs.portion == rhs.portion &&
            lhs.requestedValue == rhs.requestedValue &&
            lhs.totalValue == rhs.totalValue &&
            lhs.installmentValue == rhs.installmentValue &&
            lhs.monthlyCETLabel == rhs.monthlyCETLabel &&
            lhs.monthlyCETPercent == rhs.monthlyCETPercent &&
            lhs.monthlyIOFLabel == rhs.monthlyIOFLabel &&
            lhs.iofTotalValue == rhs.iofTotalValue &&
            lhs.monthlyInterestLabel == rhs.monthlyInterestLabel &&
            lhs.monthlyInterestPercent == rhs.monthlyInterestPercent &&
            lhs.firstDueDate == rhs.firstDueDate &&
            lhs.dueDay == rhs.dueDay
    }
}
