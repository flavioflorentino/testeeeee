import UI
import LendingComponents

public enum LoanReason: String, CaseIterable, Decodable {
    case build = "BUILD"
    case business = "BUSINESS"
    case vehicle = "CAR"
    case travel = "TRAVEL"
    case debt = "DEBITS"
    case health = "MEDICAL"
    case shopping = "SHOPPING"
    case education = "EDUCATION"
    case other = "OTHERS"
    
    var description: String {
        switch self {
        case .build:
            return Strings.Reason.build
        case .business:
            return Strings.Reason.business
        case .vehicle:
            return Strings.Reason.vehicle
        case .travel:
            return Strings.Reason.travel
        case .debt:
            return Strings.Reason.debt
        case .health:
            return Strings.Reason.health
        case .shopping:
            return Strings.Reason.shopping
        case .education:
            return Strings.Reason.education
        case .other:
            return Strings.Reason.other
        }
    }
}

extension LoanReason: ObjectiveCellModeling {
    public var icon: Iconography? {
        switch self {
        case .build:
            return Iconography.screw
        case .business:
            return Iconography.shop
        case .vehicle:
            return Iconography.car
        case .travel:
            return Iconography.plane
        case .debt:
            return Iconography.receiptAlt
        case .health:
            return Iconography.medkit
        case .shopping:
            return Iconography.shoppingCart
        case .education:
            return Iconography.bookOpen
        case .other:
            return Iconography.wallet
        }
    }
    
    public var title: String? {
        description
    }
    
    public var imgUrl: URL? { nil }
    public var isDisclosureIndicatorHidden: Bool { true }
}
