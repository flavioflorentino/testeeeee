public protocol LoanAnalyticsStatus {}

public enum LoanAuthenticateStatus: String, LoanAnalyticsStatus {
    case cancelled = "Cancelado"
    case confirmed = "Confirmado"
}

public enum LoanErrorStatus: String, LoanAnalyticsStatus {
    case hub = "Acesso Hub"
    case timeLimit = "Horário Limite"
    case onboarding = "Onboarding CP"
    case simulation = "Simulação"
    case confirmation = "Acesso Confirmação"
    case confirmationWithoutAcceptance = "Confirmação sem aceite"
    case authentication = "Autenticação"
    case confirmationError = "Confirmação de contratação"
    case deviceChanging = "Dispositivo trocado recentemente"
}
