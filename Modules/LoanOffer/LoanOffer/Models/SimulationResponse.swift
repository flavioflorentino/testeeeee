struct SimulationResponse: Decodable {
    enum CodingKeys: String, CodingKey {
        case simulationId = "id"
        case installments = "creditPortions"
    }
    
    let simulationId: String
    let installments: [SimulationInstallmentsResponse]
}

public struct SimulationInstallmentsResponse: Decodable, Equatable {
    enum CodingKeys: String, CodingKey {
        case installmentNumber = "portion"
        case installmentValue = "value"
        case monthlyInterestPercent
        case monthlyCETPercent
        case totalValue
        case firstInstallmentDueDate
    }
    
    let installmentNumber: Int
    let installmentValue: String
    let monthlyInterestPercent: String
    let monthlyCETPercent: String
    let totalValue: String
    let firstInstallmentDueDate: Date?
}
