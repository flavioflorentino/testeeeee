struct FeeViewModel {
    let monthlyCET: String
    let monthlyInterestLabel: String
    let monthlyInterestPercent: String
    let monthlyIOFLabel: String
    let monthlyIOFPercent: String
}
