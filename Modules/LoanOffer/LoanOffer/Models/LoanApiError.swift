public enum LoanApiError: String, Equatable {
    case timeLimit = "PERSONAL_CREDIT_0039"
    case unavailableLimit = "PERSONAL_CREDIT_0053"
    case proposalInProgress = "PERSONAL_CREDIT_0049"
    case proposalTimeout = "PERSONAL_CREDIT_0050"
    case deviceChangingInvalidation = "PERSONAL_CREDIT_0059"
    case noOffer = "PERSONAL_CREDIT_0016"
}

public enum UnavailabilityReason: String {
    case limit = "Limit"
    case offer = "Offer"
}
