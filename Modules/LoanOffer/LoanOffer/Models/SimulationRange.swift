public struct SimulationRange: Decodable, Equatable {
    enum CodingKeys: String, CodingKey {
        case minimumCredit = "minimumOperationValue"
        case maximumCredit = "maximumOperationValue"
        case registrationStatus
        case bankId
        case daysToDueDate = "daysToFirstInstallmentDueDate"
    }
    
    let minimumCredit: String
    let maximumCredit: String
    let bankId: String
    let registrationStatus: RegistrationStatus
    let daysToDueDate: Int?

    public func analyticsParamFlow() -> String {
        switch registrationStatus {
        case .approved:
            return "Contratação"
        default:
            return "Cadastro"
        }
    }
    
    public static func == (lhs: SimulationRange, rhs: SimulationRange) -> Bool {
        lhs.minimumCredit == rhs.minimumCredit
            && lhs.maximumCredit == rhs.maximumCredit
            && lhs.registrationStatus == rhs.registrationStatus
            && lhs.bankId == rhs.bankId
            && lhs.daysToDueDate == rhs.daysToDueDate
    }
}
