public enum RegistrationStatus: String, Decodable, Equatable {
    case registrate = "NEED_REGISTRATE"
    case incomplete = "NEED_COMPLETE"
    case approved = "ALREADY_COMPLETE"
    case analysis = "UNDER_ANALYSIS"
    case review = "NEED_REVIEW"
    case denied = "DENIED"
    case unknowed
}

public struct OnboardingRegistrationStatus: Decodable {
    let status: RegistrationStatus
}
