public enum FAQArticle: String {
    case intro = "360050390651"
    case simulation = "360049937232"
    case terms = "360049937892"
    case denied = "360049938112"
}
