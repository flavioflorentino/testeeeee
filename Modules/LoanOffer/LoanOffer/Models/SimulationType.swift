enum SimulationType: String {
    case standart = "STANDARD"
    case operation = "OPERATION"
    case installment = "INSTALLMENT"
}
