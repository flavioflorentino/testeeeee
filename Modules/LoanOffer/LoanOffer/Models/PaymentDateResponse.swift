import Foundation

struct PaymentDateResponse: Decodable {
    let validDatesList: [Date]
    
    enum CodingKeys: String, CodingKey {
        case validDatesList = "listFirstInstallmentDueDate"
    }
}
