import Foundation

struct WaitlistResponse: Decodable {
    let success: Bool
}
