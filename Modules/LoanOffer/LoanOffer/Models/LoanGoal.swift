import Foundation

struct LoanGoal: Decodable {
    let category: LoanReason
    let description: String?
}
