import Foundation

// swiftlint:disable convenience_type
final class LoanOfferResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: LoanOfferResources.self).url(forResource: "LoanOfferResources", withExtension: "bundle") else {
            return Bundle(for: LoanOfferResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: LoanOfferResources.self)
    }()
}
