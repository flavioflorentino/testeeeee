import UIKit

public protocol LoanOfferFAQContract {
    func faqScene(for url: URL) -> UIViewController
}

public extension LoanOfferFAQContract {
    func faqScene(for url: URL) -> UIViewController {
        faqScene(for: url)
    }
}
