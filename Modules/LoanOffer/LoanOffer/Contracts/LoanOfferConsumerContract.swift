public protocol LoanOfferConsumerContract {
    var consumerBalance: String { get }
    var consumerId: Int { get }
}
