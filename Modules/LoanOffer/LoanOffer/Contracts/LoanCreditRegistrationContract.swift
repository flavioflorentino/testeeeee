import UIKit

public typealias CreditRegistrationCompletion = (Result<UIViewController, CreditRegistrationError>) -> Void

public enum CreditRegistrationError: Error {
    case unknown
}

public protocol LoanCreditRegistrationContract {
    func start(with navigation: UINavigationController, status: RegistrationStatus, completion: @escaping CreditRegistrationCompletion)
}
