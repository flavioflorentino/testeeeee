import Foundation

public protocol LoanOfferCustomerSupportContract {
    func show(article: FAQArticle)
}
