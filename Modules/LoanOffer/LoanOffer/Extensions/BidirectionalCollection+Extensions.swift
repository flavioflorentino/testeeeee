extension BidirectionalCollection where Iterator.Element: Equatable {
    typealias Element = Self.Iterator.Element

    func next(_ item: Element, loop: Bool = false) -> Element? {
        guard let itemIndex = firstIndex(of: item) else {
            return nil
        }
        
        let lastItem = (index(after: itemIndex) == endIndex)
        
        return loopThrough(loop, itemIndex: lastItem) {
            self[index(after: itemIndex)]
        }
    }

    func previous(_ item: Element, loop: Bool = false) -> Element? {
        guard let itemIndex = firstIndex(of: item) else {
            return nil
        }
        
        let firstItem = (itemIndex == startIndex)
        
        return loopThrough(loop, itemIndex: firstItem) {
            self[index(before: itemIndex)]
        }
    }
    
    private func loopThrough(_ loop: Bool, itemIndex: Bool, _ completion: () -> Element?) -> Element? {
        switch (loop, itemIndex) {
        case (true, true):
            return self.first
        case (false, true):
            return nil
        default:
            return completion()
        }
    }
}
