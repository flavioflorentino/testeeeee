import Foundation
import UIKit

enum OnboardingRegistrationAction: Equatable {
    case registration(controller: UIViewController)
    case cancel
    case finish
}

protocol OnboardingRegistrationCoordinating: AnyObject {
    init(delegate: LoanRegistrationFlowCoordinating)
    
    func perform(action: OnboardingRegistrationAction)
}

final class OnboardingRegistrationCoordinator: OnboardingRegistrationCoordinating {
    weak var delegate: LoanRegistrationFlowCoordinating?
    
    init(delegate: LoanRegistrationFlowCoordinating) {
        self.delegate = delegate
    }
    
    func perform(action: OnboardingRegistrationAction) {
        switch action {
        case .registration(let controller):
            delegate?.perform(action: .registration(controller: controller))
        case .cancel:
            delegate?.perform(action: .close)
        case .finish:
            delegate?.perform(action: .finish)
        }
    }
}
