public enum OnboardingState: Equatable {
    case start
    case incomplete
    case review
}

protocol OnboardingRegistrationPresenting: AnyObject {
    var viewController: OnboardingRegistrationDisplay? { get set }
    func setupView(for state: OnboardingState)
    func startLoading()
    func stopLoading()
    func showError()
    func didNextStep(action: OnboardingRegistrationAction)
}

final class OnboardingRegistrationPresenter {
    private let coordinator: OnboardingRegistrationCoordinating
    weak var viewController: OnboardingRegistrationDisplay?
    
    init(coordinator: OnboardingRegistrationCoordinating) {
        self.coordinator = coordinator
    }
}

extension OnboardingRegistrationPresenter: OnboardingRegistrationPresenting {
    func setupView(for state: OnboardingState) {
        let onboarding = Strings.Registration.Onboarding.self
        let title: String
        let subtitle: String
        let button: String
        
        switch state {
        case .start:
            title = onboarding.Start.title
            subtitle = onboarding.Start.subtitle
            button = onboarding.Start.Button.next
        case .incomplete:
            title = onboarding.Incomplete.title
            subtitle = onboarding.Incomplete.subtitle
            button = onboarding.Incomplete.Button.next
        case .review:
            title = onboarding.Review.title
            subtitle = onboarding.Review.subtitle
            button = onboarding.Review.Button.next
        }
        
        viewController?.setup(title: title,
                              subtitle: subtitle,
                              buttonTitle: button)
    }
    
    func startLoading() {
        viewController?.startLoading()
    }

     func stopLoading() {
        viewController?.stopLoading()
    }

     func showError() {
        viewController?.displayError()
    }
    
    func didNextStep(action: OnboardingRegistrationAction) {
        coordinator.perform(action: action)
    }
}
