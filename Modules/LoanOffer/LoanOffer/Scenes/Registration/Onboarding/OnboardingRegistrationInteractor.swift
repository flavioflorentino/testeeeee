import AnalyticsModule
import UIKit

protocol OnboardingRegistrationInteractorInputs: AnyObject {
    func loadData()
    func nextStep(navigation: UINavigationController?)
    func cancel()
}

final class OnboardingRegistrationInteractor {
    typealias Dependencies = HasAnalytics & HasLegacy
    private let dependencies: Dependencies
    private let service: OnboardingRegistrationServicing
    private let presenter: OnboardingRegistrationPresenting
    private var registrationStatus: RegistrationStatus
    private var originForState: OriginType {
        switch registrationStatus {
        case .incomplete:
            return .registrationOnboardingIncomplete
        case .review:
            return .registrationOnboardingReview
        default:
            return .registrationOnboardingStart
        }
    }

    private var onboardingState: OnboardingState {
        switch registrationStatus {
        case .incomplete:
            return .incomplete
        case .review:
            return .review
        default:
            return .start
        }
    }
    
    init(presenter: OnboardingRegistrationPresenting,
         service: OnboardingRegistrationServicing,
         dependencies: Dependencies,
         withStatus registrationStatus: RegistrationStatus) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.registrationStatus = registrationStatus
    }
}

extension OnboardingRegistrationInteractor: OnboardingRegistrationInteractorInputs {
    func loadData() {
        if registrationStatus == .unknowed {
            fetchRegistrationStatus()
        } else {
            setupView()
            dependencies.analytics.log(LoanEvent.didEnter(to: originForState, from: .termsReview))
        }
    }
    
    func nextStep(navigation: UINavigationController?) {
        guard let navigation = navigation else {
            return
        }
        
        presenter.startLoading()
            
        dependencies.legacy.creditRegistration?.start(with: navigation, status: registrationStatus) { result in
            self.presenter.stopLoading()
            
            switch result {
            case .success(let controller):
                self.presenter.didNextStep(action: .registration(controller: controller))
            case .failure:
                self.presenter.showError()
            }
        }
    }
    
    func cancel() {
        presenter.didNextStep(action: onboardingState == .review ? .finish : .cancel)
    }
}

private extension OnboardingRegistrationInteractor {
    func fetchRegistrationStatus() {
        presenter.startLoading()
        service.fetchRegistrationStatus { [weak self] result in
            self?.presenter.stopLoading()
            switch result {
            case let .success(onboardingRegistration):
                self?.registrationStatus = onboardingRegistration.status
                self?.setupView()
            case .failure:
                self?.presenter.showError()
            }
        }
    }

    func setupView() {
        presenter.setupView(for: onboardingState)
    }
}
