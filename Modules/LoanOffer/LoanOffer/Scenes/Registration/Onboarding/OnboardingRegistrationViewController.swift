import UI
import AssetsKit

protocol OnboardingRegistrationDisplay: AnyObject {
    func setup(title: String, subtitle: String, buttonTitle: String)
    func startLoading()
    func stopLoading()
    func displayError()
}

private extension OnboardingRegistrationViewController.Layout {
    enum Image {
        static let size = CGSize(width: 174, height: 158)
    }
}

final class OnboardingRegistrationViewController: ViewController<OnboardingRegistrationInteractorInputs, UIView>, LoadingViewProtocol {
    fileprivate enum Layout { }
    
    lazy var loadingView: LoadingView = {
        let loading = LoadingView()
        loading.text = Strings.Terms.Loading.text
        return loading
    }()
    
    private lazy var image: UIImageView = {
        let imageview = UIImageView(image: Assets.registration.image)
        imageview.contentMode = .scaleAspectFit
        return imageview
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .large))
            .with(\.textColor, .black())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var nextButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(nextStep), for: .touchUpInside)
        button
            .buttonStyle(PrimaryButtonStyle())
            .with(\.backgroundColor, (color: .brandingBase(), state: .normal))
        return button
    }()
    
    private lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.Generic.notNow, for: .normal)
        button.addTarget(self, action: #selector(cancel), for: .touchUpInside)
        button
            .buttonStyle(LinkButtonStyle())
            .with(\.textColor, (color: .brandingBase(), state: .normal))
        return button
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
        interactor.loadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func buildViewHierarchy() {
        view.addSubview(image)
        view.addSubview(titleLabel)
        view.addSubview(subtitleLabel)
        view.addSubview(nextButton)
        view.addSubview(cancelButton)
    }
    
    override func setupConstraints() {
        image.snp.makeConstraints {
            $0.size.equalTo(Layout.Image.size)
            $0.centerX.equalToSuperview()
            $0.top.equalToSuperview().offset(Spacing.base10)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(image.snp.bottom).offset(Spacing.base03)
            $0.leading.equalToSuperview().offset(Spacing.base04)
            $0.trailing.equalToSuperview().offset(-Spacing.base04)
        }
        
        subtitleLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        nextButton.snp.makeConstraints {
            $0.top.equalTo(subtitleLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        cancelButton.snp.makeConstraints {
            $0.top.equalTo(nextButton.snp.bottom).offset(Spacing.base02)
            $0.centerX.equalToSuperview()
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}

@objc
private extension OnboardingRegistrationViewController {
    func nextStep() {
        interactor.nextStep(navigation: navigationController)
    }
    
    func cancel() {
        interactor.cancel()
    }
}

extension OnboardingRegistrationViewController: OnboardingRegistrationDisplay {
    func setup(title: String, subtitle: String, buttonTitle: String) {
        titleLabel.text = title
        subtitleLabel.text = subtitle
        nextButton.setTitle(buttonTitle, for: .normal)
    }
    
    func startLoading() {
        startLoadingView()
    }
    
    func stopLoading() {
        stopLoadingView()
    }
    
    func displayError() {
        let primaryButtonAction = ApolloAlertAction(title: Strings.Generic.tryAgain) { [weak self] in
            guard let self = self else { return }
            self.interactor.nextStep(navigation: self.navigationController)
        }
        
        let linkButtonAction = ApolloAlertAction(title: Strings.Generic.notNow) { [weak self] in
            self?.interactor.cancel()
        }
        
        showApolloAlert(type: .sticker(style: .error),
                        title: Strings.Generic.title,
                        subtitle: Strings.Generic.message,
                        primaryButtonAction: primaryButtonAction,
                        linkButtonAction: linkButtonAction)
    }
}
