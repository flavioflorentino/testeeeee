import Foundation
import Core

typealias RegistrationStatusCompletion = (Result<OnboardingRegistrationStatus, ApiError>) -> Void

protocol OnboardingRegistrationServicing {
    func fetchRegistrationStatus(completion: @escaping RegistrationStatusCompletion)
}

final class OnboardingRegistrationService: OnboardingRegistrationServicing {
    typealias Dependency = HasMainQueue
    let dependencies: Dependency

    init(dependencies: Dependency) {
        self.dependencies = dependencies
    }

    func fetchRegistrationStatus(completion: @escaping RegistrationStatusCompletion) {
        let api = Api<OnboardingRegistrationStatus>(endpoint: LoanHireServiceEndpoint.registrationStatus)
        api.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
