enum OnboardingRegistrationFactory {
    static func make(
        delegate: LoanRegistrationFlowCoordinating,
        withStatus status: RegistrationStatus
    ) -> OnboardingRegistrationViewController {
        let dependencies = LoanDependencyContainer()
        let service = OnboardingRegistrationService(dependencies: dependencies)
        let coordinator: OnboardingRegistrationCoordinating = OnboardingRegistrationCoordinator(delegate: delegate)
        let presenter: OnboardingRegistrationPresenting = OnboardingRegistrationPresenter(coordinator: coordinator)
        let interactor = OnboardingRegistrationInteractor(
            presenter: presenter,
            service: service,
            dependencies: dependencies,
            withStatus: status)
        let viewController = OnboardingRegistrationViewController(interactor: interactor)

        presenter.viewController = viewController
        
        return viewController
    }
}
