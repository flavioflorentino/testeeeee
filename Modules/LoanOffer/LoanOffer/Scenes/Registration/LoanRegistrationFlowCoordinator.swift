import UI

public enum RegistrationAction: Equatable {
    case onboarding(status: RegistrationStatus)
    case finish
    case close
    case registration(controller: UIViewController)
}

protocol LoanRegistrationFlowCoordinating: Coordinating {
    func perform(action: RegistrationAction)
}

final class LoanRegistrationFlowCoordinator: LoanRegistrationFlowCoordinating {
    var childViewController: [UIViewController] = []
    var viewController: UIViewController?
    var navigationController: UINavigationController
    private weak var delegate: LoanOfferCoordinating?
    
    init(from navigationController: UINavigationController, with delegate: LoanOfferCoordinating) {
        self.delegate = delegate
        self.navigationController = navigationController
    }

    func perform(action: RegistrationAction) {
        switch action {
        case .finish:
            delegate?.finish()
        case .close:
            navigationController.popViewController(animated: true)
        default:
            handleFlow(action)
        }
    }
    
    private func handleFlow(_ action: RegistrationAction) {
        let viewController: UIViewController
        
        switch action {
        case let .onboarding(status):
            viewController = OnboardingRegistrationFactory.make(delegate: self, withStatus: status)
        case .registration(let controller):
            viewController = controller
        default:
            return
        }
        
        navigationController.pushViewController(viewController, animated: true)
    }
}
