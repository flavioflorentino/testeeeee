enum ReasonFactory {
    static func make(with delegate: LoanHireFlowCoordinating,
                     range: SimulationRange?,
                     from origin: OriginType = .onboarding) -> ReasonViewController {
        let dependencies = LoanDependencyContainer()
        let coordinator: ReasonCoordinating = ReasonCoordinator(with: delegate)
        let presenter: ReasonPresenting = ReasonPresenter(coordinator: coordinator)
        let interactor = ReasonInteractor(presenter: presenter, from: origin, dependencies: dependencies, range: range)
        let viewController = ReasonViewController(interactor: interactor)

        presenter.viewController = viewController

        return viewController
    }
}
