protocol ReasonCoordinating: AnyObject {
    var delegate: LoanHireFlowCoordinating? { get set }
    
    init(with delegate: LoanHireFlowCoordinating)
    
    func reasonSelected(reason: LoanReason)
}

final class ReasonCoordinator: ReasonCoordinating {
    weak var delegate: LoanHireFlowCoordinating?
    
    init(with delegate: LoanHireFlowCoordinating) {
        self.delegate = delegate
    }
    
    func reasonSelected(reason: LoanReason) {
        delegate?.perform(action: .simulation(reason))
    }
}
