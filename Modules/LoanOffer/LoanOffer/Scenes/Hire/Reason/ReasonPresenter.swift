protocol ReasonPresenting: AnyObject {
    var viewController: ReasonDisplay? { get set }
    func presentReasons()
    func reasonSelected(reason: LoanReason)
}

final class ReasonPresenter: ReasonPresenting {
    private let coordinator: ReasonCoordinating
    weak var viewController: ReasonDisplay?
    private(set) var reasons = LoanReason.allCases
    
    init(coordinator: ReasonCoordinating) {
        self.coordinator = coordinator
    }
    
    func presentReasons() {
        shuffleReasonsAndSetOtherReasonToLastPosition()
        viewController?.displayReasons(reasons)
    }
     
    func reasonSelected(reason: LoanReason) {
        coordinator.reasonSelected(reason: reason)
    }
}

private extension ReasonPresenter {
    func shuffleReasonsAndSetOtherReasonToLastPosition() {
        reasons = reasons
            .shuffled()
            .sorted { previous, _ in
                previous != .other
            }
    }
}
