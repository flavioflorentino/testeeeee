import AnalyticsModule

protocol ReasonInteractorInputs: AnyObject {
    func setupReasons()
    func proceed(with indexpath: IndexPath)
}

final class ReasonInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let presenter: ReasonPresenting
    private var origin: OriginType
    private(set) var reasons = LoanReason.allCases
    private let range: SimulationRange?
    
    init(presenter: ReasonPresenting, from origin: OriginType, dependencies: Dependencies, range: SimulationRange?) {
        self.presenter = presenter
        self.origin = origin
        self.dependencies = dependencies
        self.range = range
    }
}

extension ReasonInteractor: ReasonInteractorInputs {
    func setupReasons() {
        presenter.presentReasons()
        setupAnalytics()
    }
    
    func proceed(with indexpath: IndexPath) {
        let reason = reasons[indexpath.row]
        dependencies.analytics.log(LoanHireEvent.reason(reason))
        origin = .simulation
        presenter.reasonSelected(reason: reason)
    }
    
    private func setupAnalytics() {
        guard let paramFlow: String = range?.analyticsParamFlow() else { return }
        dependencies.analytics.log(LoanEvent.didEnter(to: .reason, from: origin, params: ["Flow": paramFlow]))
    }
}
