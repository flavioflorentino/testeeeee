import UI
import LendingComponents

protocol ReasonDisplay: AnyObject {
    func displayReasons(_ reasons: [LoanReason])
}

extension ReasonViewController.Layout {
    enum Insets {
        static let tableHeaderViewLayoutMargins: EdgeInsets = {
            var insets: EdgeInsets = .rootView
            insets.top = Spacing.base01
            insets.bottom = Spacing.base01
            return insets
        }()
    }
    
    enum Lenght {
        static let estimatedRowHeight: CGFloat = 50
        static let estimatedHeaderHeight: CGFloat = 44
    }
}

final class ReasonViewController: ViewController<ReasonInteractorInputs, UIView> {
    fileprivate enum Layout {}
    
    enum Section {
        case main
    }
    
    enum AccessibilityIdentifier: String {
        case reasonsTableView
    }
    
    private(set) lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.separatorStyle = .none
        tableView.estimatedRowHeight = Layout.Lenght.estimatedRowHeight
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(ObjectiveCell.self, forCellReuseIdentifier: ObjectiveCell.identifier)
        tableView.accessibilityIdentifier = AccessibilityIdentifier.reasonsTableView.rawValue
        tableView.backgroundColor = .clear
        tableView.delegate = self
        return tableView
    }()
    
    private lazy var dataSource: TableViewDataSource<Section, LoanReason> = {
        let dataSource = TableViewDataSource<Section, LoanReason>(view: tableView) { tableView, indexPath, reason -> UITableViewCell? in
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ObjectiveCell.identifier,
                                                           for: indexPath) as? ObjectiveCell else {
                return nil
            }
            cell.setup(for: reason)
            return cell
        }
        dataSource.add(section: .main)
        return dataSource
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpLoanNavigationAppearance()
        interactor.setupReasons()
    }

    override func buildViewHierarchy() {
        view.addSubview(tableView)
    }
    
    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }

    override func configureViews() {
        title = Strings.Reason.title
        view.backgroundColor = Colors.backgroundPrimary.color
        tableView.dataSource = dataSource
    }
    
    private func setUpLoanNavigationAppearance() {
        guard let navigation = navigationController else {
            return
        }

        let bar = navigation.navigationBar
        bar.isTranslucent = true
        
        if #available(iOS 13.0, *) {
            bar.prefersLargeTitles = true
            navigation.navigationItem.largeTitleDisplayMode = .always
        }
        
        let backButton = UIBarButtonItem()
        backButton.title = String()
        bar.topItem?.backBarButtonItem = backButton
    }
}

extension ReasonViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        interactor.proceed(with: indexPath)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        Layout.Lenght.estimatedRowHeight
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        UITableView.automaticDimension
    }
}

extension ReasonViewController: ReasonDisplay {
    func displayReasons(_ reasons: [LoanReason]) {
        dataSource.update(items: reasons, from: .main)
    }
}
