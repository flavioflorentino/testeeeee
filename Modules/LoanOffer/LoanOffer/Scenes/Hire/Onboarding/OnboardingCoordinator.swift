import UIKit
import Foundation
import UI

enum OnboardingAction {
    case reason(range: SimulationRange)
}

protocol OnboardingCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: OnboardingAction)
}

final class OnboardingCoordinator {
    weak var viewController: UIViewController?
    weak var delegate: LoanHireFlowCoordinating?

    init(delegate: LoanHireFlowCoordinating) {
        self.delegate = delegate
    }
}

// MARK: - OnboardingCoordinating
extension OnboardingCoordinator: OnboardingCoordinating {
    func perform(action: OnboardingAction) {
        switch action {
        case let .reason(range):
            delegate?.perform(action: .reason(range))
        }
    }
}
