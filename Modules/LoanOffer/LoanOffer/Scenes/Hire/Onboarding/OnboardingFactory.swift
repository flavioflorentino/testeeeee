enum OnboardingFactory {
    static func make(with delegate: LoanHireFlowCoordinating, from origin: OriginType) -> OnboardingViewController {
        let container = LoanDependencyContainer()
        let coordinator: OnboardingCoordinating = OnboardingCoordinator(delegate: delegate)
        let presenter: OnboardingPresenting = OnboardingPresenter(coordinator: coordinator)
        let interactor = OnboardingInteractor(presenter: presenter, dependencies: container, origin: origin)
        let viewController = OnboardingViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
