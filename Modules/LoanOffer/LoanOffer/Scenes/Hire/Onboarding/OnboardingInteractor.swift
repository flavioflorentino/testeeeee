import Core
import AnalyticsModule

protocol OnboardingInteracting: AnyObject {
    func setupOnboarding()
    func nextStep()
    func trackEnterView()
}

final class OnboardingInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    private let presenter: OnboardingPresenting
    private var origin: OriginType

    init(presenter: OnboardingPresenting, dependencies: Dependencies, origin: OriginType) {
        self.presenter = presenter
        self.dependencies = dependencies
        self.origin = origin
    }
}

// MARK: - OnboardingInteracting
extension OnboardingInteractor: OnboardingInteracting {
    func setupOnboarding() {
        presenter.updateLabelMessage()
        presenter.setupBulletRows()
    }
    
    func nextStep() { }
    
    func trackEnterView() {
        dependencies.analytics.log(LoanEvent.didEnter(to: .onboarding, from: origin))
    }
}
