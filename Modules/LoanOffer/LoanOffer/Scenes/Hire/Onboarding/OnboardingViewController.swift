import UI
import UIKit
import AssetsKit

protocol OnboardingDisplaying: AnyObject {
    func displayLabelMessage(summary: NSAttributedString)
    func addBulletRow(icon: Iconography, description: String)
}

final class OnboardingViewController: ViewController<OnboardingInteracting, UIView> {
    fileprivate enum Layout { }
    
    enum Accessibility: String {
        case nextButtton
    }
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsVerticalScrollIndicator = false
        scrollView.isHidden = false
        return scrollView
    }()
    
    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var image: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Resources.Illustrations.iluHoldingMoneyOnboarding.image
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var contentView = UIView()
    
    private lazy var summaryLabel = UILabel()
    
    private lazy var disclaimerLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.text, Strings.Onboarding.disclaimer)
        return label
    }()
    
    private lazy var bulletStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = Spacing.base02
        return stack
    }()
    
    private lazy var nextButton: UIButton = {
        let button = UIButton()
        button.accessibilityIdentifier = Accessibility.nextButtton.rawValue
        button.setTitle(Strings.Intro.button, for: .normal)
        button.addTarget(self, action: #selector(nextStep), for: .touchUpInside)
        button.isHidden = false
        button.buttonStyle(PrimaryButtonStyle())
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.setupOnboarding()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        interactor.trackEnterView()
    }

    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentStackView)
        
        contentStackView.addArrangedSubview(image)
        contentStackView.addArrangedSubview(contentView)
        
        contentView.addSubview(summaryLabel)
        contentView.addSubview(disclaimerLabel)
        contentView.addSubview(bulletStackView)
        view.addSubview(nextButton)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(nextButton.snp.top)
        }
        
        contentStackView.snp.makeConstraints {
            $0.top.equalTo(view.snp.topMargin).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
            $0.width.equalTo(scrollView)
        }
        
        summaryLabel.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        disclaimerLabel.snp.makeConstraints {
            $0.top.equalTo(summaryLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        bulletStackView.snp.makeConstraints {
            $0.top.equalTo(disclaimerLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        nextButton.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).offset(-Spacing.base03)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        contentView.backgroundColor = .clear
        summaryLabel.numberOfLines = 0
    }
}

@objc
private extension OnboardingViewController {
    func nextStep() {
        interactor.nextStep()
    }
}

// MARK: - OnboardingDisplaying
extension OnboardingViewController: OnboardingDisplaying {
    func displayLabelMessage(summary: NSAttributedString) {
        summaryLabel.attributedText = summary
    }
    
    func addBulletRow(icon: Iconography, description: String) {
        let imageTextView = ImageTextView()
        imageTextView.setImage(icon, andDescription: description)
        
        bulletStackView.addArrangedSubview(imageTextView)
    }
}
