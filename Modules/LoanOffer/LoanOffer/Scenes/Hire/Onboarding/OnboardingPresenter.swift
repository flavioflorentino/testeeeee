import Foundation
import UI
import AssetsKit
import Core

protocol OnboardingPresenting: AnyObject {
    var viewController: OnboardingDisplaying? { get set }
    func updateLabelMessage()
    func setupBulletRows()
    func didNextStep(with range: SimulationRange)
}

final class OnboardingPresenter {
    private let coordinator: OnboardingCoordinating
    weak var viewController: OnboardingDisplaying?
    
    init(coordinator: OnboardingCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - OnboardingPresenting
extension OnboardingPresenter: OnboardingPresenting {
    func updateLabelMessage() {
        viewController?.displayLabelMessage(summary: setupSummaryDescription())
    }
    
    func setupBulletRows() {
        [
            (icon: Iconography.wallet, description: Strings.Onboarding.walletBullet),
            (icon: Iconography.calendarAlt, description: Strings.Onboarding.calendarBullet),
            (icon: Iconography.percentage, description: Strings.Onboarding.percentageBullet)
        ].forEach { bullet in
            viewController?.addBulletRow(icon: bullet.icon, description: bullet.description)
        }
    }
    
    func didNextStep(with range: SimulationRange) {
        coordinator.perform(action: .reason(range: range))
    }
}

private extension OnboardingPresenter {
    func setupSummaryDescription() -> NSMutableAttributedString {
        Strings.Onboarding.summary("viajar").attributedStringWith(normalFont: Typography.title(.medium).font(),
                                                                  highlightFont: Typography.title(.medium).font(),
                                                                  normalColor: Colors.black.color,
                                                                  highlightColor: Colors.brandingBase.color,
                                                                  underline: false,
                                                                  lineHeight: 24)
    }
}
