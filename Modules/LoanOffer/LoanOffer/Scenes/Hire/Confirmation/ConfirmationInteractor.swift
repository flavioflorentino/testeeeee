import AnalyticsModule
import Core

protocol ConfirmationInteractorInputs: AnyObject {
    func trackEnterView()
    func nextStep()
    func cancel()
}

final class ConfirmationInteractor {
    typealias Dependencies = HasAnalytics & HasLegacy
    private let dependencies: Dependencies

    private let service: ConfirmationServicing
    private let presenter: ConfirmationPresenting
    private let contractId: String
    private let reason: LoanReason
    
    init(service: ConfirmationServicing, presenter: ConfirmationPresenting, dependencies: Dependencies, contractId: String, reason: LoanReason) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.contractId = contractId
        self.reason = reason
    }
}

private extension ConfirmationInteractor {
    func handleSuccessAuthentication(withPin pin: String) {
        self.dependencies.analytics.log(LoanHireEvent.didAuthenticate(status: .confirmed))
        presenter.startLoading()
        
        service.confirm(with: contractId, reason: reason, pin: pin) { [weak self] response in
            guard let self = self else { return }
            self.presenter.stopLoading()
            
            switch response {
            case .success:
                self.dependencies.analytics.log(LoanEvent.didEnter(to: .approved, from: .doubleConfirmation))
                self.presenter.didNextStep()

            case let .failure(error):
                self.handle(error)
            }
        }
    }
    
    func handleFailureAuthentication(with status: AuthenticationStatus) {
        switch status {
        case .cancelled:
            dependencies.analytics.log(LoanHireEvent.didAuthenticate(status: .cancelled))
        case .nilPassword:
            dependencies.analytics.log(LoanEvent.didReceiveError(error: .authentication))
            presenter.showError()
        }
    }
    
    func handle(_ error: ApiError) {
        switch error.requestError?.code {
        case LoanApiError.timeLimit.rawValue:
            presenter.showTimeLimitWarning()
            dependencies.analytics.log(LoanEvent.didReceiveError(error: .timeLimit))
        case LoanApiError.proposalInProgress.rawValue,
             LoanApiError.proposalTimeout.rawValue:
            presenter.showProposalInProgressWarning()
            dependencies.analytics.log(LoanEvent.didReceiveError(error: .confirmationError))
        case LoanApiError.deviceChangingInvalidation.rawValue:
            presenter.showDeviceChangingWarning()
            dependencies.analytics.log(LoanEvent.didReceiveError(error: .deviceChanging))
        default:
            dependencies.analytics.log(LoanEvent.didReceiveError(error: .confirmationError))
            presenter.showError()
        }
    }
}

extension ConfirmationInteractor: ConfirmationInteractorInputs {
    func trackEnterView() {
        dependencies.analytics.log(LoanEvent.didEnter(to: .doubleConfirmation, from: .termsHire))
    }
    
    func nextStep() {
        dependencies.analytics.log(LoanEvent.didEnter(to: .authenticate, from: .doubleConfirmation))
        
        guard let auth = dependencies.legacy.auth else {
            presenter.showError()
            return
        }
        
        auth.authenticate { [weak self] result in
            switch result {
            case let .success(pin):
                self?.handleSuccessAuthentication(withPin: pin)
            case let .failure(status):
                self?.handleFailureAuthentication(with: status)
            }
        }
    }
    
    func cancel() {
        presenter.didCancel()
    }
}
