import Core

typealias ConfirmationCompletionBlock = (Result<ConfirmSimulationResponse, ApiError>) -> Void

protocol ConfirmationServicing {
    func confirm(with simulationId: String, reason: LoanReason, pin: String, _ completion: @escaping ConfirmationCompletionBlock)
}

final class ConfirmationService: ConfirmationServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func confirm(with simulationId: String, reason: LoanReason, pin: String, _ completion: @escaping ConfirmationCompletionBlock) {
        let endpoint = LoanHireServiceEndpoint.confirmation(simulationId: simulationId,
                                                            reason: reason,
                                                            pin: pin)
        let api = Api<ConfirmSimulationResponse>(endpoint: endpoint)

        api.execute { result in
            self.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
