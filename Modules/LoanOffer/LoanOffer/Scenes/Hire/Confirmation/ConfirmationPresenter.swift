import UI
import Foundation
import AssetsKit

protocol ConfirmationPresenting: AnyObject {
    var viewController: ConfirmationDisplay? { get set }
    func didCancel()
    func didNextStep()
    func startLoading()
    func stopLoading()
    func showError()
    func showProposalInProgressWarning()
    func showTimeLimitWarning()
    func showDeviceChangingWarning()
}

final class ConfirmationPresenter {
    private let coordinator: ConfirmationCoordinating
    weak var viewController: ConfirmationDisplay?

    init(coordinator: ConfirmationCoordinating) {
        self.coordinator = coordinator
    }
}

extension ConfirmationPresenter: ConfirmationPresenting {
    func didCancel() {
        coordinator.perform(action: .cancel)
    }

    func didNextStep() {
        let content = ApolloFeedbackViewContent(image: Assets.confirmationSuccess.image,
                                                title: Strings.Success.header,
                                                description: NSAttributedString(string: Strings.Success.body),
                                                primaryButtonTitle: Strings.Generic.ok,
                                                secondaryButtonTitle: "")
        coordinator.perform(action: .warning(content))
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
    
    func showError() {
        viewController?.displayError()
    }

    func showProposalInProgressWarning() {
        let content = ApolloFeedbackViewContent(image: Assets.confirmationSuccess.image,
                                                title: Strings.ProposalInProgress.Warning.title,
                                                description: NSAttributedString(string: Strings.ProposalInProgress.Warning.subtitle),
                                                primaryButtonTitle: Strings.Generic.ok,
                                                secondaryButtonTitle: "")
        coordinator.perform(action: .warning(content))
    }

    func showTimeLimitWarning() {
        let content = ApolloFeedbackViewContent(image: Assets.unavailable.image,
                                                title: Strings.Alerts.Unavailable.title,
                                                description: NSAttributedString(string: Strings.Alerts.Unavailable.subtitle),
                                                primaryButtonTitle: Strings.Generic.ok,
                                                secondaryButtonTitle: "")
        coordinator.perform(action: .warning(content))
    }
    
    func showDeviceChangingWarning() {
        let content = ApolloFeedbackViewContent(image: Resources.Illustrations.iluWarning.image,
                                                title: Strings.Alerts.DeviceChanging.title,
                                                description: NSAttributedString(string: Strings.Alerts.DeviceChanging.message),
                                                primaryButtonTitle: Strings.Generic.ok,
                                                secondaryButtonTitle: "")
        coordinator.perform(action: .warning(content))
    }
}
