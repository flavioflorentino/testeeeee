import Foundation
import UI

enum ConfirmationAction: Equatable {
    case cancel
    case warning(_ warning: ApolloFeedbackViewContent)
    case finish
    
    static func == (lhs: ConfirmationAction, rhs: ConfirmationAction) -> Bool {
        switch (lhs, rhs) {
        case
            (.finish, .finish),
            (.cancel, .cancel):
            return true
        case let (.warning(lhsWarning), .warning(rhsWarning)):
            return lhsWarning == rhsWarning
        default:
            return false
        }
    }
}

protocol ConfirmationCoordinating: AnyObject {
    init(delegate: LoanHireFlowCoordinating)
    
    func perform(action: ConfirmationAction)
}

final class ConfirmationCoordinator: ConfirmationCoordinating {
    weak var delegate: LoanHireFlowCoordinating?
    
    init(delegate: LoanHireFlowCoordinating) {
        self.delegate = delegate
    }

    func perform(action: ConfirmationAction) {
        switch action {
        case .cancel:
            delegate?.perform(action: .back)
        case let .warning(warning):
            delegate?.perform(action: .warning(warning))
        case .finish:
            delegate?.perform(action: .finish)
        }
    }
}
