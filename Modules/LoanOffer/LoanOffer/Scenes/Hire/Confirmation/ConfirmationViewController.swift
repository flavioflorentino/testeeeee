import UI
import AssetsKit

protocol ConfirmationDisplay: AnyObject {
    func startLoading()
    func stopLoading()
    func displayError()
}

private extension ConfirmationViewController.Layout {
    enum Size {
        static let image = CGSize(width: 138, height: 128)
    }
}

final class ConfirmationViewController: ViewController<ConfirmationInteractorInputs, UIView>, LoadingViewProtocol {
    fileprivate enum Layout { }

    enum Accessibility: String {
        case hireLoanButton
        case cancelButton
    }
    
    private lazy var image: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = Assets.confirmation.image
        return imageView
    }()
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .large))
            .with(\.textColor, .black())
            .with(\.textAlignment, .center)
        label.text = Strings.Confirmation.title
        return label
    }()
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        label.text = Strings.Confirmation.subtitle
        return label
    }()
    private lazy var nextButton: UIButton = {
        let button = UIButton()
        button.accessibilityIdentifier = Accessibility.hireLoanButton.rawValue
        button.setTitle(Strings.Confirmation.Button.confirm, for: .normal)
        button.addTarget(self, action: #selector(nextStep), for: .touchUpInside)
        button
            .buttonStyle(PrimaryButtonStyle())
            .with(\.backgroundColor, (color: .brandingBase(), state: .normal))
        return button
    }()
    private lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.accessibilityIdentifier = Accessibility.cancelButton.rawValue
        button.setTitle(Strings.Generic.notNow, for: .normal)
        button.addTarget(self, action: #selector(cancel), for: .touchUpInside)
        button
            .buttonStyle(LinkButtonStyle())
            .with(\.textColor, (color: .brandingBase(), state: .normal))
        return button
    }()
    lazy var loadingView: LoadingView = {
        let loading = LoadingView()
        loading.text = Strings.Terms.Loading.text
        return loading
    }()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
        interactor.trackEnterView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func buildViewHierarchy() {
        view.addSubview(image)
        view.addSubview(titleLabel)
        view.addSubview(subtitleLabel)
        view.addSubview(nextButton)
        view.addSubview(cancelButton)
    }
    
    override func setupConstraints() {
        image.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base12)
            $0.centerX.equalToSuperview()
            $0.size.equalTo(Layout.Size.image)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(image.snp.bottom).offset(Spacing.base03)
            $0.leading.equalToSuperview().offset(Spacing.base04)
            $0.trailing.equalToSuperview().offset(-Spacing.base04)
        }
        
        subtitleLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base04)
            $0.trailing.equalToSuperview().offset(-Spacing.base04)
        }
        
        nextButton.snp.makeConstraints {
            $0.top.equalTo(subtitleLabel.snp.bottom).offset(Spacing.base06)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        cancelButton.snp.makeConstraints {
            $0.top.equalTo(nextButton.snp.bottom).offset(Spacing.base02)
            $0.centerX.equalToSuperview()
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}

@objc
private extension ConfirmationViewController {
    func nextStep() {
        interactor.nextStep()
    }
    
    func cancel() {
        interactor.cancel()
    }
}

extension ConfirmationViewController: ConfirmationDisplay {
    func startLoading() {
        startLoadingView()
    }
    
    func stopLoading() {
        stopLoadingView()
    }
    
    func displayError() {
        let primaryButtonAction = ApolloAlertAction(title: Strings.Generic.tryAgain) {
            self.interactor.nextStep()
        }
        
        let linkButtonAction = ApolloAlertAction(title: Strings.Generic.notNow) { }
        
        showApolloAlert(type: .sticker(style: .error),
                        title: Strings.Generic.title,
                        subtitle: Strings.Alerts.Contract.message,
                        primaryButtonAction: primaryButtonAction,
                        linkButtonAction: linkButtonAction)
    }
}
