enum ConfirmationFactory {
    static func make(with delegate: LoanHireFlowCoordinating, contractId: String, reason: LoanReason) -> ConfirmationViewController {
        let container = LoanDependencyContainer()
        let service: ConfirmationServicing = ConfirmationService(dependencies: container)
        let coordinator: ConfirmationCoordinating = ConfirmationCoordinator(delegate: delegate)
        let presenter: ConfirmationPresenting = ConfirmationPresenter(coordinator: coordinator)
        let interactor = ConfirmationInteractor(service: service, presenter: presenter, dependencies: container, contractId: contractId, reason: reason)
        let viewController = ConfirmationViewController(interactor: interactor)

        presenter.viewController = viewController

        return viewController
    }
}
