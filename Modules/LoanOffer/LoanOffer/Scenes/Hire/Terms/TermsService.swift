import Core

typealias ContractCompletionBlock = (Result<ContractPDFResponse, ApiError>) -> Void

protocol TermsServicing {
    func fetchCCB(
        with simulationId: String,
        _ completion: @escaping ContractCompletionBlock
    )
}

final class TermsService: TermsServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func fetchCCB(with simulationId: String, _ completion: @escaping ContractCompletionBlock) {
        let api = Api<ContractPDFResponse>(endpoint: LoanHireServiceEndpoint.ccbURL(simulationId: simulationId))

        execute(api, andThen: completion)
    }
    
    private func execute<T>(_ api: Api<T>, andThen completion: @escaping (Result<T, ApiError>) -> Void) {
        api.execute { result in
            self.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
