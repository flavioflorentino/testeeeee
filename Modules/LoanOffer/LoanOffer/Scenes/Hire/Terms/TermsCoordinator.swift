import Core
import SafariServices
import UI
import Foundation

enum TermsAction: Equatable {
    case safari(SFSafariViewController)
    case openFAQ
    case confirmation(contractId: String)
    case registration(status: RegistrationStatus)
    case warning(_ warning: ApolloFeedbackViewContent)
    case finish
    
    static func == (lhs: TermsAction, rhs: TermsAction) -> Bool {
        switch (lhs, rhs) {
        case (.finish, .finish),
             (.openFAQ, .openFAQ):
            return true
        case let (.registration(lhsStatus), .registration(rhsStatus)):
            return lhsStatus == rhsStatus
        case let (.safari(lhsSafari), .safari(rhsSafari)):
            return lhsSafari == rhsSafari
        case let (.confirmation(lhsContract), .confirmation(rhsContract)):
            return lhsContract == rhsContract
        case let (.warning(lhsWarning), .warning(rhsWarning)):
            return lhsWarning == rhsWarning
        default:
            return false
        }
    }
}

protocol TermsCoordinating: AnyObject {
    func perform(action: TermsAction)
}

final class TermsCoordinator: TermsCoordinating {
    typealias Dependencies = HasLegacy
    private let dependencies: Dependencies
    
    weak var delegate: LoanHireFlowCoordinating?
    
    init(delegate: LoanHireFlowCoordinating, dependencies: Dependencies) {
        self.delegate = delegate
        self.dependencies = dependencies
    }
    
    func perform(action: TermsAction) {
        switch action {
        case let .safari(controller):
            delegate?.perform(action: .present(controller))
        case .openFAQ:
            dependencies.legacy.customerSupport?.show(article: .terms)
        case let .confirmation(contractId):
            delegate?.perform(action: .confirmation(contractId: contractId))
        case let .registration(status):
            delegate?.perform(action: .registrationOnboarding(status: status))
        case let .warning(warning):
            delegate?.perform(action: .warning(warning))
        case .finish:
            delegate?.perform(action: .finish)
        }
    }
}
