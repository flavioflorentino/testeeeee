import Foundation
import UI

private extension FeesView.Layout {
    enum Image {
        static let size = CGSize(width: 16, height: 16)
    }
    enum Height {
        static let expandableLabel: CGFloat = 18
    }
}

final class FeesView: UIView {
    fileprivate enum Layout {}
    
    private lazy var tapGesture = UITapGestureRecognizer(target: self, action: #selector(toggleState))

    private lazy var containerStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = Spacing.base01
        return stack
    }()
    
    private lazy var divider: UIView = {
        let divider = UIView()
        divider.backgroundColor = Colors.grayscale050.color
        return divider
    }()
    
    private lazy var totalFeesTitleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale400())
        label.text = Strings.Terms.FeeView.totalFees
        label.set(lineHeight: 24)
        return label
    }()
    
    private lazy var totalFeesLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, .grayscale700())
        label.set(lineHeight: 24)
        return label
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.isHidden = true
        return view
    }()
    
    private lazy var detailTitleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale400())
        label.text = Strings.Terms.FeeView.detail
        label.set(lineHeight: 24)
        return label
    }()
    
    private lazy var nominalFeeTitleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale400())
        label.set(lineHeight: 24)
        return label
    }()
    
    private lazy var nominalFeeLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, .grayscale700())
        label.set(lineHeight: 24)
        return label
    }()
    
    private lazy var iofTitleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale400())
        label.set(lineHeight: 24)
        return label
    }()
    
    private lazy var iofLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, .grayscale700())
        label.set(lineHeight: 24)
        return label
    }()
    
    private lazy var expandableFooterView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var expandableButtonLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var expandableArrowImage: UIImageView = {
        let imageview = UIImageView()
        imageview.contentMode = .scaleAspectFit
        imageview.image = Assets.arrowDown.image
        return imageview
    }()
        
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(_ fees: FeeViewModel) {
        totalFeesLabel.text = fees.monthlyCET
        nominalFeeTitleLabel.text = fees.monthlyInterestLabel
        nominalFeeLabel.text = fees.monthlyInterestPercent
        iofTitleLabel.text = fees.monthlyIOFLabel
        iofLabel.text = fees.monthlyIOFPercent
    }
}

private extension FeesView {
    @objc
    func toggleState() {
        UIView.animate(withDuration: 0.5) {
            self.contentView.isHidden.toggle()
            self.setupToggleStateLabelText()
            self.rotateArrowImage()
            self.controlInfosAlpha()
        }
    }
    
    func setupToggleStateLabelText() {
        let toggleLabelText = contentView.isHidden
            ? Strings.Terms.FeeView.closed
            : Strings.Terms.FeeView.opened
        
        let attributes: [NSAttributedString.Key: Any] = [
            .font: Typography.bodySecondary().font(),
            .foregroundColor: Colors.brandingBase.color
        ]
        
        let attributedText = NSMutableAttributedString(string: toggleLabelText, attributes: attributes)
        expandableButtonLabel.attributedText = attributedText
    }
    
    func setupFeeViewConstraints() {
        detailTitleLabel.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(nominalFeeTitleLabel.snp.top).offset(-Spacing.base01)
        }
        
        nominalFeeTitleLabel.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(nominalFeeLabel.snp.top).offset(-Spacing.base00)
        }
        
        nominalFeeLabel.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(iofTitleLabel.snp.top).offset(-Spacing.base02)
        }
        
        iofTitleLabel.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(iofLabel.snp.top).offset(-Spacing.base00)
        }
        
        iofLabel.snp.makeConstraints {
            $0.bottom.leading.trailing.equalToSuperview()
        }
    }
    
    func controlInfosAlpha() {
        let alpha: CGFloat = contentView.isHidden ? 0 : 1
        
        detailTitleLabel.alpha = alpha
        nominalFeeTitleLabel.alpha = alpha
        nominalFeeLabel.alpha = alpha
        iofTitleLabel.alpha = alpha
        iofLabel.alpha = alpha
    }
    
    func rotateArrowImage() {
        expandableArrowImage.transform = contentView.isHidden ? .identity : CGAffineTransform(rotationAngle: .pi)
    }
}

extension FeesView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(containerStackView)
        containerStackView.addArrangedSubview(totalFeesTitleLabel)
        containerStackView.addArrangedSubview(totalFeesLabel)
        containerStackView.addArrangedSubview(divider)
        
        containerStackView.addArrangedSubview(contentView)
        contentView.addSubviews(detailTitleLabel, nominalFeeTitleLabel, nominalFeeLabel, iofTitleLabel, iofLabel)
        
        containerStackView.addArrangedSubview(expandableFooterView)
        expandableFooterView.addSubviews(expandableButtonLabel, expandableArrowImage)
    }

    func setupConstraints() {
        containerStackView.snp.makeConstraints {
            $0.top.leading.equalToSuperview().offset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base01)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        divider.snp.makeConstraints {
            $0.height.equalTo(Border.light)
        }
        
        setupFeeViewConstraints()
        
        expandableButtonLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base01)
            $0.bottom.equalToSuperview().offset(-Spacing.base01)
            $0.trailing.equalTo(expandableArrowImage.snp.leading).offset(-Spacing.base02)
            $0.centerX.equalToSuperview().offset(-Spacing.base02)
            $0.height.equalTo(Layout.Height.expandableLabel)
        }
        
        expandableArrowImage.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.size.equalTo(Layout.Image.size)
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.groupedBackgroundSecondary.color
        
        layer.borderColor = Colors.grayscale100.color.cgColor
        layer.borderWidth = Border.light
        layer.cornerRadius = CornerRadius.medium
        
        clipsToBounds = true
        isUserInteractionEnabled = true
        translatesAutoresizingMaskIntoConstraints = false
        
        expandableFooterView.addGestureRecognizer(tapGesture)
        
        setupToggleStateLabelText()
    }
}
