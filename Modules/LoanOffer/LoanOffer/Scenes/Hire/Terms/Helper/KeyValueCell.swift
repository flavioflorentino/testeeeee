import UI

extension KeyValueCell.Layout {
    enum Size {
        static let height: CGFloat = 24
    }
}

final class KeyValueCell: UIView {
    fileprivate enum Layout { }
    
    private lazy var keyLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale400())
        return label
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textColor, .grayscale700())
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .leading
        stackView.spacing = Spacing.base00
        
        return stackView
    }()
    var key = ""
    var value = ""
    
    init(frame: CGRect = .zero, key: String = "", value: String = "") {
        super.init(frame: frame)
        self.key = key
        self.value = value
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        setupView()
    }
}

private extension KeyValueCell {
    func addComponents() {
        stackView.addArrangedSubview(keyLabel)
        stackView.addArrangedSubview(valueLabel)
        addSubview(stackView)
    }
    
    func layoutComponents() {
        stackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        keyLabel.snp.makeConstraints {
            $0.leading.trailing.top.equalToSuperview()
            $0.height.equalTo(Layout.Size.height)
        }
        
        valueLabel.snp.makeConstraints {
            $0.top.equalTo(keyLabel.snp.bottom).offset(Spacing.base00)
            $0.leading.trailing.bottom.equalToSuperview()
            $0.height.equalTo(Layout.Size.height)
        }
    }
    
    func setupView() {
        keyLabel.text = key
        valueLabel.text = value
    }
}
