import Foundation
import UI

public protocol AgreementContractViewDelegate: AnyObject {
    func didTapAgreement(didTap: Bool)
    func didOpen(_ url: URL)
}

private extension AgreementContractView.Layout {
    enum Size {
        static let button = CGSize(width: 24, height: 24)
        static let height: CGFloat = 72
    }
}

final class AgreementContractView: UIView {
    fileprivate enum Layout { }
    
    enum Accessibility: String {
        case radialButton
    }
    
    weak var delegate: AgreementContractViewDelegate?
    
    private lazy var contractView: UIView = {
        let view = UIView()
        view.layer.borderColor = Colors.grayscale100.color.cgColor
        view.layer.borderWidth = Border.light
        view.layer.cornerRadius = CornerRadius.light
        view.backgroundColor = Colors.groupedBackgroundSecondary.color
        
        return view
    }()
    
    private lazy var radialButton: UIButton = {
        let button = UIButton()
        button.accessibilityIdentifier = Accessibility.radialButton.rawValue
        button.setImage(UI.Assets.grayCheckmark.image, for: .normal)
        button.setImage(UI.Assets.greenFilledCheckmark.image, for: .selected)
        button.addTarget(self, action: #selector(didConfirm), for: .touchUpInside)
        return button
    }()
    
    private lazy var contractText: UITextView = {
        let text = UITextView()
        text.font = Typography.bodyPrimary().font()
        text.text = Strings.Terms.Acceptance.agreement
        text.textColor = Colors.grayscale700.color
        text.isEditable = false
        text.isSelectable = true
        text.isScrollEnabled = false
        text.backgroundColor = .clear
        text.textContainerInset = .zero
        text.textContainer.lineFragmentPadding = 0
        return text
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addComponents()
        setupConstraints()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension AgreementContractView {
    func addComponents() {
        contractView.addSubview(radialButton)
        contractView.addSubview(contractText)
        addSubview(contractView)
    }
    
    func setupConstraints() {
        contractView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        radialButton.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base01)
            $0.size.equalTo(Layout.Size.button)
        }
        
        contractText.snp.makeConstraints {
            $0.leading.equalTo(radialButton.snp.trailing).offset(Spacing.base01)
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base00)
        }
    }
    
    @objc
    func didConfirm() {
        radialButton.isSelected.toggle()
        contractView.layer.borderColor = radialButton.isSelected
            ? Colors.brandingBase.color.cgColor
            : Colors.grayscale200.color.cgColor
        delegate?.didTapAgreement(didTap: radialButton.isSelected)
    }
}

extension AgreementContractView {
    func setupContractAttributes(attributedText: NSAttributedString, andLinkAttributes linkAttributes: [NSAttributedString.Key: Any]) {
        contractText.attributedText = attributedText
        contractText.linkTextAttributes = linkAttributes
        contractText.delegate = self
    }
}

extension AgreementContractView: UITextViewDelegate {
    func textView(
        _ textView: UITextView,
        shouldInteractWith URL: URL,
        in characterRange: NSRange,
        interaction: UITextItemInteraction
    ) -> Bool {
        delegate?.didOpen(URL)
        return false
    }
}
