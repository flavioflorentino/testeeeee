import AnalyticsModule
import Core
import Foundation

protocol TermsInteractorInputs: AnyObject {
    func viewDidLoad()
    func acceptTerms(_ accepted: Bool)
    func nextStep()
    func openContract(_ url: URL)
    func trackEnterView()
    func openFAQ()
}

final class TermsInteractor {
    typealias Dependencies = HasAnalytics & HasSafariView & HasLegacy
    private let dependencies: Dependencies
    private let service: TermsServicing
    private let presenter: TermsPresenting
    private(set) var termsAccepted = false
    private let contract: ContractResponse
    private let registrationStatus: RegistrationStatus
    private var origin: OriginType
    private var state: TermsState {
        switch registrationStatus {
        case .registrate, .incomplete:
            return .review
        default:
            return .hire
        }
    }
    
    private var paramAnalytics: String {
        switch registrationStatus {
        case .approved:
            return "Contratação"
        default:
            return "Cadastro"
        }
    }
    
    init(
        service: TermsServicing,
        presenter: TermsPresenting,
        contract: ContractResponse,
        dependencies: Dependencies,
        withRegistrationStatus status: RegistrationStatus?,
        from origin: OriginType
    ) {
        self.service = service
        self.presenter = presenter
        self.contract = contract
        self.dependencies = dependencies
        self.registrationStatus = status ?? .registrate
        self.origin = origin
    }
}

extension TermsInteractor: TermsInteractorInputs {
    func viewDidLoad() {
        presenter.setupView(for: state, with: contract)
    }
    
    func acceptTerms(_ accepted: Bool) {
        dependencies.analytics.log(LoanHireEvent.didAcceptTerms(accepted: accepted))
        termsAccepted = accepted
    }
    
    func nextStep() {
        guard state == .hire else {
            origin = .registrationOnboarding
            presenter.didStartRegistration(with: registrationStatus)
            return
        }
        
        guard termsAccepted else {
            dependencies.analytics.log(LoanEvent.didReceiveError(error: .confirmationWithoutAcceptance))
            presenter.showAgreementAlert()
            return
        }
        
        origin = .doubleConfirmation
        presenter.didNextStepOfHiring(withContractId: contract.id)
    }
    
    func openContract(_ url: URL) {
        let contractView: OriginType
        if url.absoluteString.contains("ccb") {
            contractView = .contractView
            openCCB()
        } else {
            contractView = .contractSummary
            openSafari(withURL: Strings.Terms.Acceptance.Link.summary)
        }
        dependencies.analytics.log(LoanEvent.didEnter(to: contractView, from: .termsHire, params: ["Flow": paramAnalytics]))
    }
    
    func trackEnterView() {
        let viewState: OriginType = state == .hire ? .termsHire : .termsReview
        dependencies.analytics.log(LoanEvent.didEnter(to: viewState, from: origin, params: ["Flow": paramAnalytics]))
    }
    
    func openFAQ() {
        presenter.openFAQ()
    }
}

private extension TermsInteractor {
    /// CCB stands for Cédula de Crédito Bancário.
    func openCCB() {
        presenter.startLoading()
        
        service.fetchCCB(with: contract.id) { [weak self] response in
            guard let self = self else {
                return
            }
            
            self.presenter.stopLoading()
            
            switch response {
            case let .success(link):
                self.openSafari(withURL: link.url)
            case let .failure(error):
                self.handle(error)
            }
        }
    }
    
    func openSafari(withURL url: String) {
        guard
            case let .success(controller) = dependencies.safari.controller(for: url)
            else {
                presenter.showErrorOnContractURL()
                return
        }
        
        presenter.open(safariViewController: controller)
    }
    
    func handle(_ error: ApiError) {
        if let errorCode = error.requestError?.code,
            errorCode == LoanApiError.timeLimit.rawValue {
            presenter.showTimeLimitWarning()
            dependencies.analytics.log(LoanEvent.didReceiveError(error: .timeLimit))
        } else {
            presenter.showErrorOnContractURL()
        }
    }
}
