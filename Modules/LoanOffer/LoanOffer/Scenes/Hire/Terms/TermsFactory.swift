enum TermsFactory {
    static func make(
        with delegate: LoanHireFlowCoordinating,
        andContract contract: ContractResponse,
        withRegistrationStatus status: RegistrationStatus?,
        from origin: OriginType = .simulation
    ) -> TermsViewController {
        let dependencies = LoanDependencyContainer()
        let service: TermsServicing = TermsService(dependencies: dependencies)
        let coordinator: TermsCoordinating = TermsCoordinator(delegate: delegate, dependencies: dependencies)
        let presenter: TermsPresenting = TermsPresenter(coordinator: coordinator)
        let interactor = TermsInteractor(
            service: service,
            presenter: presenter,
            contract: contract,
            dependencies: dependencies,
            withRegistrationStatus: status,
            from: origin)
        let viewController = TermsViewController(interactor: interactor)

        presenter.viewController = viewController
        
        return viewController
    }
}
