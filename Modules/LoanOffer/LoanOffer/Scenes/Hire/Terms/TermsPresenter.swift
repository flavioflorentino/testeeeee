import Core
import Foundation
import SafariServices
import UI

enum TermsState {
    case review
    case hire
}

protocol TermsPresenting: AnyObject {
    var viewController: TermsDisplay? { get set }
    func didNextStepOfHiring(withContractId contractId: String)
    func didStartRegistration(with status: RegistrationStatus)
    func setupView(for state: TermsState, with contract: ContractResponse)
    func showAgreementAlert()
    func startLoading()
    func stopLoading()
    func showErrorOnContractURL()
    func openFAQ()
    func open(safariViewController controller: SFSafariViewController)
    func showTimeLimitWarning()
    func finish()
}

final class TermsPresenter {
    private let coordinator: TermsCoordinating
    weak var viewController: TermsDisplay?
    private var state: TermsState = .review
    
    init(coordinator: TermsCoordinating) {
        self.coordinator = coordinator
    }
}

private extension TermsPresenter {
    var acceptanceTermsAttributes: [NSAttributedString.Key: Any] {
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 12
        
        return [
            .font: Typography.bodyPrimary().font(),
            .foregroundColor: Colors.grayscale700.color,
            .paragraphStyle: style
        ]
    }
    
    var linkAttributes: [NSAttributedString.Key: Any] {
        [
            .font: Typography.bodyPrimary(.highlight).font(),
            .underlineStyle: NSUnderlineStyle.single.rawValue,
            .foregroundColor: Colors.brandingBase.color
        ]
    }
    
    func generateContractInfo(from contract: ContractResponse) -> [KeyValueCell] {
        let terms = Strings.Terms.self
        var info = [
            KeyValueCell(key: terms.requestedValueLabel, value: contract.requestedValue),
            KeyValueCell(key: terms.totalValueLabel, value: contract.totalValue),
            KeyValueCell(key: terms.installmentsLabel, value: terms.installments(contract.portion, contract.installmentValue))
        ]
        
        if state == .hire {
            info.insert(KeyValueCell(key: terms.dueDayLabel, value: terms.dueDay(contract.dueDay)), at: 1)
            info.insert(KeyValueCell(key: terms.firstDueDateLabel, value: contract.firstDueDate), at: 4)
        }
        
        return info
    }
    
    func createFeeViewModel(with contract: ContractResponse) -> FeeViewModel {
        FeeViewModel(monthlyCET: Strings.Terms.totalFees(contract.monthlyCETPercent, contract.monthlyCETLabel),
                     monthlyInterestLabel: contract.monthlyInterestLabel,
                     monthlyInterestPercent: Strings.Terms.monthlyInterest(contract.monthlyInterestPercent),
                     monthlyIOFLabel: contract.monthlyIOFLabel,
                     monthlyIOFPercent: contract.iofTotalValue)
    }
    
    func addAttributes(to attributedText: NSMutableAttributedString, url: URL, highlight: String) {
        let attributes: [NSAttributedString.Key: Any] = [
            .link: url,
            .font: Typography.bodyPrimary(.highlight).font(),
            .underlineStyle: NSUnderlineStyle.single.rawValue,
            .foregroundColor: Colors.brandingBase.color
        ]
        let range = NSString(string: Strings.Terms.Acceptance.agreement).range(of: highlight)
        attributedText.addAttributes(attributes, range: range)
    }
    
    func updateAgreementLink() {
        let placeholderSummaryLink = Strings.Terms.Acceptance.Link.summary
        let placeholderCcbLink = Strings.Terms.Acceptance.Link.ccb

        guard
            let summaryUrl = URL(string: placeholderSummaryLink),
            let ccbUrl = URL(string: placeholderCcbLink)
            else {
                return
        }
        
        let attributedText = NSMutableAttributedString(string: Strings.Terms.Acceptance.agreement,
                                                       attributes: acceptanceTermsAttributes)

        addAttributes(to: attributedText, url: ccbUrl, highlight: Strings.Terms.Acceptance.Highlight.ccb)
        addAttributes(to: attributedText, url: summaryUrl, highlight: Strings.Terms.Acceptance.Highlight.summary)
        
        viewController?.setupContractAttributes(attributedText: attributedText, andLinkAttributes: linkAttributes)
    }
    
    func updateFeeView(with contract: ContractResponse) {
        let fees = createFeeViewModel(with: contract)
        viewController?.display(fees)
    }
    
    func updateViewInfos(with contract: ContractResponse) {
        let infos = generateContractInfo(from: contract)
        viewController?.displayContract(infos)
    }
    
    func updateButtonTitle() {
        let buttonTitle = state == .review
            ? Strings.Terms.Button.review
            : Strings.Terms.Button.hire
        
        viewController?.displayNextButton(title: buttonTitle)
    }
    
    func updateAcceptance() {
        let isReviewState = state == .review
        
        viewController?.hideAcceptanceView(isReviewState)
        
        if !isReviewState {
            updateAgreementLink()
        }
    }
}

extension TermsPresenter: TermsPresenting {
    func didStartRegistration(with status: RegistrationStatus) {
        coordinator.perform(action: .registration(status: status))
    }
    
    func didNextStepOfHiring(withContractId contractId: String) {
        coordinator.perform(action: .confirmation(contractId: contractId))
    }

    func setupView(for state: TermsState, with contract: ContractResponse) {
        self.state = state
        
        updateViewInfos(with: contract)
        updateFeeView(with: contract)
        updateAcceptance()
        updateButtonTitle()
    }
    
    func showAgreementAlert() {
        viewController?.displayAgreementAlert()
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
        
    func showErrorOnContractURL() {
        viewController?.displayErrorForContractURL()
    }
    
    func openFAQ() {
         coordinator.perform(action: .openFAQ)
    }
    
    func open(safariViewController controller: SFSafariViewController) {
        coordinator.perform(action: .safari(controller))
    }
    
    func showTimeLimitWarning() {
        let content = ApolloFeedbackViewContent(image: Assets.unavailable.image,
                                                title: Strings.Alerts.Unavailable.title,
                                                description: NSAttributedString(string: Strings.Alerts.Unavailable.subtitle),
                                                primaryButtonTitle: Strings.Generic.ok,
                                                secondaryButtonTitle: "")
        coordinator.perform(action: .warning(content))
    }
    
    func finish() {
        coordinator.perform(action: .finish)
    }
}
