import Foundation
import UI
import AssetsKit

protocol TermsDisplay: AnyObject {
    func setupContractAttributes(attributedText: NSAttributedString, andLinkAttributes linkAttributes: [NSAttributedString.Key: Any])
    func displayAgreementAlert()
    func displayContract(_ infos: [KeyValueCell])
    func display(_ fees: FeeViewModel)
    func startLoading()
    func stopLoading()
    func displayErrorForContractURL()
    func hideAcceptanceView(_ visibility: Bool)
    func displayNextButton(title: String)
}

final class TermsViewController: ViewController<TermsInteractorInputs, UIView>, LoadingViewProtocol {
    enum Accessibility: String {
        case hireLoanButton
        case alertOkButton
    }
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsVerticalScrollIndicator = false
        return scrollView
    }()
    
    private lazy var contentStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = Spacing.base03
        return stack
    }()
    
    private lazy var feesView = FeesView()
    
    private lazy var disclaimerMessageLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.Terms.disclaimerMessage
        label
            .labelStyle(CaptionLabelStyle())
            .with(\.textColor, .grayscale300())
        return label
    }()
    
    private lazy var contractView: AgreementContractView = {
        let view = AgreementContractView()
        view.delegate = self
        return view
    }()
    
    private lazy var nextButton: ConfirmButton = {
        let button = ConfirmButton()
        button.accessibilityIdentifier = Accessibility.hireLoanButton.rawValue
        button.addTarget(self, action: #selector(nextStep), for: .touchUpInside)
        button
            .buttonStyle(PrimaryButtonStyle())
            .with(\.backgroundColor, (color: .brandingBase(), state: .normal))
        return button
    }()
    
    lazy var loadingView: LoadingView = {
        let loading = LoadingView()
        loading.text = Strings.Terms.Loading.text
        return loading
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        interactor.trackEnterView()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentStackView)
        view.addSubview(nextButton)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.bottom.equalTo(nextButton.snp.top).offset(-Spacing.base02)
        }
        
        contentStackView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.leading.trailing.bottom.width.equalToSuperview()
        }
        
        nextButton.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).offset(-Spacing.base02)
        }
    }
    
    override func configureViews() {
        title = Strings.Terms.title
        view.backgroundColor = Colors.backgroundPrimary.color
        
        let backButton = UIBarButtonItem()
        backButton.title = String()
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        let infoButton = UIBarButtonItem(image: Resources.Icons.icoQuestion.image,
                                         style: .plain,
                                         target: self,
                                         action: #selector(didTapInfo))

        navigationItem.rightBarButtonItem = infoButton
    }
}

@objc
private extension TermsViewController {
    func nextStep() {
        interactor.nextStep()
    }
    
    func didTapInfo() {
        interactor.openFAQ()
    }
}

private extension TermsViewController {
    func setupViewComponents() {
        contentStackView.addArrangedSubview(feesView)
        contentStackView.addArrangedSubview(contractView)
        contentStackView.addArrangedSubview(disclaimerMessageLabel)
        contentStackView.addArrangedSubview(UIView())
        
        if #available(iOS 11, *) {
            contentStackView.setCustomSpacing(Spacing.base02, after: feesView)
            contentStackView.setCustomSpacing(Spacing.base02, after: contractView)
            contentStackView.setCustomSpacing(Spacing.base06, after: disclaimerMessageLabel)
        }
    }
}

extension TermsViewController: TermsDisplay {    
    func setupContractAttributes(
        attributedText: NSAttributedString,
        andLinkAttributes linkAttributes: [NSAttributedString.Key: Any]
    ) {
        contractView.setupContractAttributes(attributedText: attributedText,
                                             andLinkAttributes: linkAttributes)
    }
    
    func displayContract(_ infos: [KeyValueCell]) {
        for keyValueItem in infos {
            contentStackView.addArrangedSubview(keyValueItem)
            
            if #available(iOS 11, *) {
                contentStackView.setCustomSpacing(Spacing.base03, after: keyValueItem)
            }
        }
        
        setupViewComponents()
    }
    
    func display(_ fees: FeeViewModel) {
        feesView.set(fees)
    }
    
    func displayAgreementAlert() {
        let primaryAlertButton = ApolloAlertAction(title: Strings.Generic.ok) {}
        
        showApolloAlert(type: .sticker(style: .warning),
                        title: Strings.Alerts.Auth.title,
                        subtitle: Strings.Alerts.Auth.message,
                        primaryButtonAction: primaryAlertButton )
        
        let offset: CGFloat = 100.0
        let frame = CGRect(x: 0.0,
                           y: scrollView.contentSize.height - offset,
                           width: scrollView.contentSize.width,
                           height: scrollView.contentSize.height + offset)
        scrollView.scrollRectToVisible(frame, animated: true)
    }

    func startLoading() {
        startLoadingView()
        navigationController?.isNavigationBarHidden = true
    }
    
    func stopLoading() {
        stopLoadingView()
        navigationController?.isNavigationBarHidden = false
    }
    
    func displayErrorForContractURL() {
        let primaryButtonAction = ApolloAlertAction(title: Strings.Generic.ok) {}
        
        showApolloAlert(type: .sticker(style: .error),
                        title: Strings.Generic.title,
                        subtitle: Strings.Generic.message,
                        primaryButtonAction: primaryButtonAction)
    }
    
    func hideAcceptanceView(_ visibility: Bool) {
        contractView.isHidden = visibility
    }
    
    func displayNextButton(title: String) {
        nextButton.setTitle(title, for: .normal)
    }
}

extension TermsViewController: AgreementContractViewDelegate {
    func didTapAgreement(didTap: Bool) {
        interactor.acceptTerms(didTap)
    }
    
    func didOpen(_ url: URL) {
        interactor.openContract(url)
    }
}
