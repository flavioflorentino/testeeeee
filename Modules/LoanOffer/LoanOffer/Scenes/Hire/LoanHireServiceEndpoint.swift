import Core

enum LoanHireServiceEndpoint {
    case limits
    case calendars
    case simulation(value: String, firstDueDate: String?, bankId: String)
    case chooseSimulation(simulationId: String, installmentNumber: Int)
    case confirmation(simulationId: String, reason: LoanReason, pin: String)
    case ccbURL(simulationId: String)
    case addToWaitList(consumerId: Int)
    case registrationStatus
}

extension LoanHireServiceEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .limits:
            return "personal-credit/limits"
        case .calendars:
            return "personal-credit/simulations/calendars"
        case .simulation:
            return "personal-credit/simulations"
        case let .chooseSimulation(simulationId, _):
            return "personal-credit/simulations/\(simulationId)/proposals"
        case .confirmation:
            return "personal-credit/contracts"
        case let .ccbURL(simulationId):
            return "personal-credit/simulations/\(simulationId)/previews"
        case .addToWaitList:
            return "personal-credit/offers/interest-queue"
        case .registrationStatus:
            return "personal-credit/consumer/registration/status"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case
            .calendars,
            .ccbURL,
            .registrationStatus,
            .limits:
            return .get
        default:
            return .post
        }
    }
    
    var body: Data? {
        switch self {
        case let .simulation(value, firstDueDate, bankId):
            var params: [String: Any] = ["value": value, "bankId": bankId]
            if let firstDueDate = firstDueDate {
                params["installmentFirstDueDate"] = firstDueDate
            }
            return params.toData()
        case let .chooseSimulation(_, installmentNumber):
            return ["portion": installmentNumber].toData()
        case let .confirmation(simulationId, reason, _):
            return [
                "simulationId": simulationId,
                "loanGoalIn": [
                    "category": reason.rawValue
                ]
            ].toData()
        default:
            return nil
        }
    }

    var customHeaders: [String: String] {
        switch self {
        case let .confirmation(_, _, pin):
            return ["password": pin]
        case let .addToWaitList(consumerId):
            return ["consumer_id": String(consumerId)]
        default:
            return [:]
        }
    }
}
