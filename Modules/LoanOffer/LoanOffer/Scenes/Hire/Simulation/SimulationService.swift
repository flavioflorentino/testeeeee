import Core

typealias SimulationCompletionBlock = (Result<SimulationResponse, ApiError>) -> Void
typealias ChoseSimulationCompletionBlock = (Result<ContractResponse, ApiError>) -> Void

protocol SimulationServicing {
    func simulate(for value: String, firstDueDate: Date?, bankId: String, _ completion: @escaping SimulationCompletionBlock)
    func choseSimulation(with simulationId: String, forInstallment number: Int, firstDueDate: Date?, _ completion: @escaping ChoseSimulationCompletionBlock)
}

final class SimulationService: SimulationServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func simulate(for value: String, firstDueDate: Date?, bankId: String, _ completion: @escaping SimulationCompletionBlock) {
        var endpoint = LoanHireServiceEndpoint.simulation(value: value, firstDueDate: nil, bankId: bankId)
        
        if let firstDueDate = firstDueDate {
            endpoint = LoanHireServiceEndpoint.simulation(
                value: value,
                firstDueDate: Date.fifthFormatter.string(from: firstDueDate),
                bankId: bankId
            )
        }
        
        let api = Api<SimulationResponse>(endpoint: endpoint)
        execute(api, andThen: completion)
    }

    func choseSimulation(
        with simulationId: String,
        forInstallment number: Int,
        firstDueDate: Date?,
        _ completion: @escaping ChoseSimulationCompletionBlock
    ) {
        let endpoint = LoanHireServiceEndpoint.chooseSimulation(simulationId: simulationId, installmentNumber: number)
        let api = Api<ContractResponse>(endpoint: endpoint)
        
        execute(api, andThen: completion)
    }
    
    private func execute<T>(_ api: Api<T>, andThen completion: @escaping (Result<T, ApiError>) -> Void) {
        let decoder = JSONDecoder(.useDefaultKeys)
        decoder.dateDecodingStrategy = .formatted(Date.forthFormatter)
        
        api.shouldUseDefaultDateFormatter = false
        
        api.execute(jsonDecoder: decoder) { result in
            self.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
