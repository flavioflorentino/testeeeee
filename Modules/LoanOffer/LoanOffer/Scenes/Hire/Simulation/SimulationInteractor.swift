import AnalyticsModule
import Core
import Foundation
import FeatureFlag
import AssetsKit

enum RetryAction: Equatable {
    case simulate
    case confirm(installment: Int)
}

protocol SimulationInteractorInputs: AnyObject {
    var displayDueDate: Bool { get }
    func fetchSimulation()
    func simulate(with value: String?)
    func getNextInstallment()
    func getPreviousInstallment()
    func choseSimulation(with installmentNumber: Int)
    func trackFeesView()
    func onConfirmErrorAlert(action: RetryAction)
    func stopButtonLoading()
    func trackEnterView()
    func trackChangeSimulationValue()
    func openFAQ()
    func openCalendarView()
    func didTapDueDate()
}

final class SimulationInteractor {
    typealias Dependencies = HasAnalytics & HasSafariView & HasLegacy & HasFeatureManager
    private let dependencies: Dependencies
    
    private let service: SimulationServicing
    private let presenter: SimulationPresenting
    private let range: SimulationRange?
    private var currentInstallment: SimulationInstallmentsResponse?
    private var simulation: SimulationResponse?
    private var sortedInstallments: [SimulationInstallmentsResponse] {
        simulation?.installments.sorted {
            $0.installmentNumber < $1.installmentNumber
        } ?? []
    }
    
    private var initialDueDate: Date?
    private var requestedValue: String?
    private var selectedDueDate: Date?
    
    private var origin: OriginType
    private var minimumLimit: String {
        self.requestedValue ?? self.range?.minimumCredit ?? ""
    }
    
    var displayDueDate: Bool {
        dependencies.featureManager.isActive(.isFeatureLoanSimulationDueDateAvailable) && isRegistrationCompleted()
    }
    
    var isFirstCalendarView = true
    
    private var originForStatus: OriginType {
        switch range?.registrationStatus {
        case .registrate, .incomplete:
            return .termsReview
        default:
            return .termsHire
        }
    }
    
    init(
        service: SimulationServicing,
        presenter: SimulationPresenting,
        range: SimulationRange?,
        from origin: OriginType,
        dependencies: Dependencies
    ) {
        self.service = service
        self.presenter = presenter
        self.range = range
        self.origin = origin
        self.dependencies = dependencies
    }
}

extension SimulationInteractor: SimulationInteractorInputs {
    func didTapDueDate() {
        if isFirstCalendarView {
            presenter.showDueDateInfo()
            isFirstCalendarView = false
        } else {
            openCalendarView()
        }
    }
    
    func openCalendarView() {
        guard let date = selectedDueDate else { return }
        presenter.presentCalendarView(with: date,
                                      daysToDueDate: range?.daysToDueDate,
                                      delegate: self)
    }

    func fetchSimulation() {
        updateRange()
        presenter.set(minimumLimit)
        presenter.setupTitleDescription(isRegistrationCompleted: isRegistrationCompleted())
        presenter.setupLoanAmountQuestionDescription(isRegistrationCompleted: isRegistrationCompleted())
        presenter.startLoading()
        simulate()
    }

    func simulate(with value: String? = nil) {
        guard let range = range else { return }

        if let value = value {
            requestedValue = value
        }
        
        presenter.changeNextButtonState(toEnabled: false)
        presenter.changeStepperViewState(to: .disabled)
        presenter.changeInstallmentsInfoViewState(to: .loading)
        
        service.simulate(for: minimumLimit, firstDueDate: selectedDueDate, bankId: range.bankId) { [weak self] response in
            guard let self = self else { return }
            
            switch response {
            case let .success(simulation):
                self.simulation = simulation
                self.updateInstallmentsList()
                self.updateLastInstallment()
            case let .failure(error):
                self.handle(error)
            }
            
            self.presenter.stopLoading()
        }
    }
    
    func getNextInstallment() {
        guard
            let currentInstallment = currentInstallment,
            let installment = sortedInstallments.next(currentInstallment)
        else { return }
        
        update(installment)
        dependencies.analytics.log(LoanHireEvent.didChangeSimulationInstallments)
    }
    
    func getPreviousInstallment() {
        guard
            let currentInstallment = currentInstallment,
            let installment = sortedInstallments.previous(currentInstallment)
        else { return }
        
        update(installment)
        dependencies.analytics.log(LoanHireEvent.didChangeSimulationInstallments)
    }
    
    func choseSimulation(with installmentNumber: Int) {
        presenter.changeStepperViewState(to: .disabled)
        presenter.startButtonLoading()
        
        guard let simulationId = simulation?.simulationId else {
            showSimulationErrorAndSendTrack(with: .confirm(installment: installmentNumber))
            return
        }
        
        service.choseSimulation(with: simulationId, forInstallment: installmentNumber, firstDueDate: selectedDueDate) { [weak self] response in
            guard let self = self else { return }
            
            self.presenter.stopButtonLoading()
            
            switch response {
            case let .success(contract):
                self.origin = self.originForStatus
                self.presenter.nextStep(with: contract)
            case let .failure(error):
                self.handle(error, with: .confirm(installment: installmentNumber))
            }
        }
        presenter.changeStepperViewState(to: .enabled)
    }
    
    func trackFeesView() {
        dependencies.analytics.log(LoanHireEvent.didCheckFeesInfo)
    }
    
    func onConfirmErrorAlert(action: RetryAction) {
        if case let .confirm(installmentNumber) = action {
            choseSimulation(with: installmentNumber)
        } else {
            simulate(with: requestedValue)
        }
    }

    func stopButtonLoading() {
        presenter.stopButtonLoading()
    }
    
    func trackEnterView() {
        guard let paramFlow: String = range?.analyticsParamFlow() else { return }
        dependencies.analytics.log(LoanEvent.didEnter(to: .simulation, from: origin, params: ["Flow": paramFlow]))
    }
    
    func trackChangeSimulationValue() {
        dependencies.analytics.log(LoanHireEvent.didChangeSimulationValue)
    }
    
    func openFAQ() {
        presenter.openFAQ()
    }
}

private extension SimulationInteractor {
    func isRegistrationCompleted() -> Bool {
        guard let registrationStatus = range?.registrationStatus else { return false }
        
        return registrationStatus == .approved
    }
    
    func updateLastInstallment() {
        guard let lastInstallment = getInstallmentForCurrentNumberIfExists() else {
            showSimulationErrorAndSendTrack()
            return
        }
        
        update(lastInstallment)
    }
    
    func update(_ installment: SimulationInstallmentsResponse) {
        currentInstallment = installment
        
        if displayDueDate {
            initialDueDate = initialDueDate == nil ? installment.firstInstallmentDueDate : initialDueDate
            selectedDueDate = installment.firstInstallmentDueDate
            presenter.setupDueDate(installment.firstInstallmentDueDate)
        }
        
        presenter.changeNextButtonState(toEnabled: true)
        
        presenter.setupInstallmentsView(with: installment.installmentNumber,
                                        andInstallmentValue: installment.installmentValue)
        
        let daysToFirstInstallment = getDaysFromInitialDueDate()
        
        let event = LoanHireEvent.didSimulate(requestedValue: minimumLimit,
                                              finalValue: "\(NumberFormatter.toNumber(from: installment.totalValue))",
                                              installment: installment,
                                              selectedDueDate: selectedDueDate,
                                              daysToFirstInstallment: daysToFirstInstallment)
        dependencies.analytics.log(event)
    }
    
    func getDaysFromInitialDueDate() -> String? {
        guard let initialDueDate = initialDueDate,
              let selectedDueDate = selectedDueDate,
              let days = Calendar.current.dateComponents([.day], from: initialDueDate, to: selectedDueDate).day
        else {
            return nil
        }
        
        return "\(days)"
    }
    
    func updateInstallmentsList() {
        let list = sortedInstallments.map({ $0.installmentNumber })
        guard list.isNotEmpty else {
            showSimulationErrorAndSendTrack()
            return
        }
        
        presenter.setupInstallments(list: list)
    }
    
    func showSimulationErrorAndSendTrack(with action: RetryAction = .simulate) {
        presenter.showError(with: action)
        dependencies.analytics.log(LoanEvent.didReceiveError(error: .simulation))
    }

    func showTimeLimitErrorAndSendTrack() {
        presenter.showTimeLimitWarning()
        dependencies.analytics.log(LoanEvent.didReceiveError(error: .timeLimit))
    }
    
    func handle(_ error: ApiError, with action: RetryAction = .simulate) {
        if let errorCode = error.requestError?.code,
           errorCode == LoanApiError.timeLimit.rawValue {
            showTimeLimitErrorAndSendTrack()
        } else {
            showSimulationErrorAndSendTrack(with: action)
        }
    }
    
    func updateRange() {
        guard let range = range else {
            showSimulationErrorAndSendTrack()
            return
        }
        
        let minValueDouble = NumberFormatter.toNumber(from: range.minimumCredit)
        let maxValueDouble = NumberFormatter.toNumber(from: range.maximumCredit,
                                                      defaultValue: .greatestFiniteMagnitude)
        
        guard maxValueDouble >= minValueDouble else {
            showSimulationErrorAndSendTrack()
            return
        }
        
        presenter.setupTextfield(minValueDouble...maxValueDouble)
    }
    
    func getInstallmentForCurrentNumberIfExists() -> SimulationInstallmentsResponse? {
        sortedInstallments.first {
            $0.installmentNumber == currentInstallment?.installmentNumber
        } ?? sortedInstallments.last
    }
}

extension SimulationInteractor: PaymentDateDelegate {
    func didSelect(date: Date) {
        selectedDueDate = date
        presenter.setupDueDate(date)
        simulate(with: minimumLimit)
    }
}
