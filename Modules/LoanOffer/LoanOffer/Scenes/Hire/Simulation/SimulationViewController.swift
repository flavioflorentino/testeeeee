import Foundation
import AnalyticsModule
import AssetsKit
import UI
import UIKit
import FeatureFlag

protocol SimulationDisplay: AnyObject {
    func set(_ maximumLimit: String)
    func setupTextfield(_ range: ClosedRange<Double>)
    func displayDueDateInfo()
    func setupDueDate(_ dueDateText: NSAttributedString)
    func setupInstallments(_ list: [Int])
    func setupLoanAmountQuestionDescription(description: String)
    func setupTitleDescription(title: String)
    func displayError(with action: RetryAction)
    func changeStepperViewState(to state: StepperViewState)
    func changeInstallmentsInfoViewState(to state: InstallmentsInfoViewState)
    func startLoading()
    func stopLoading()
    func startButtonLoading()
    func stopButtonLoading()
    func enableNextButton(_ isEnabled: Bool)
}

private extension SimulationViewController.Layout {
    enum Height {
        static let nextButton: CGFloat = 48
        static let line: CGFloat = 1
    }
}

final class SimulationViewController: ViewController<SimulationInteractorInputs, UIView>, LoadingViewProtocol {
    fileprivate enum Layout {}
    
    enum Accessibility: String {
        case alertOkButton
        case infoButton
        case dismissInfoButton
        case nextButton
        case dateButton
        case backgroundView
    }
    
    lazy var loadingView: LoadingView = {
        let loading = LoadingView()
        loading.text = Strings.Generic.Loading.text
        return loading
    }()

    private lazy var loanAmountQuestionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
        return label
    }()
    
    private lazy var loanAmountTextField: LoanCurrencyField = {
        let textField = LoanCurrencyField()
        textField.delegate = self
        return textField
    }()

    private lazy var dueDateQuestionLabel: UILabel = {
        let label = createLabel(for: Strings.Simulation.dueDateQuestion)
        label.isHidden = !interactor.displayDueDate
        return label
    }()

    private lazy var dueDateLabel: UILabel = {
        let label = UILabel()
        label.isUserInteractionEnabled = true
        label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapDueDate)))
        label.isHidden = !interactor.displayDueDate
        return label
    }()
    
    private lazy var installmentsQuestionLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.Simulation.installmentsQuestion
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
        return label
    }()
    
    private lazy var installmentsInfoView = InstallmentsInfoView()
    
    private lazy var stepperBackgroundView = UIView()
    private lazy var stepperView = StepperView(delegate: self)
    private lazy var stepperLabel = createLabel(
        for: Strings.Simulation.installmentsLabel,
        withColor: Colors.grayscale400,
        andFont: Typography.bodyPrimary().font()
    )
    
    private lazy var nextButton: ConfirmButton = {
        let button = ConfirmButton()
        button.accessibilityIdentifier = Accessibility.nextButton.rawValue
        button.setTitle(Strings.Simulation.button, for: .normal)
        button.addTarget(self, action: #selector(nextStep), for: .touchUpInside)
        button
            .buttonStyle(PrimaryButtonStyle())
            .with(\.textColor, (color: .white(.light), state: .disabled))
            .with(\.backgroundColor, (color: .grayscale300(), state: .disabled))
        button.isEnabled = false
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.fetchSimulation()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        interactor.trackEnterView()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        interactor.stopButtonLoading()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(loanAmountQuestionLabel)
        view.addSubview(loanAmountTextField)
        view.addSubview(dueDateQuestionLabel)
        view.addSubview(dueDateLabel)
        view.addSubview(installmentsQuestionLabel)
        view.addSubview(installmentsInfoView)
        view.addSubview(stepperBackgroundView)
        stepperBackgroundView.addSubview(stepperLabel)
        stepperBackgroundView.addSubview(stepperView)
        view.addSubview(nextButton)
    }
    
    override func setupConstraints() {
        loanAmountQuestionLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        loanAmountTextField.snp.makeConstraints {
            $0.top.equalTo(loanAmountQuestionLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        dueDateQuestionLabel.snp.makeConstraints {
            $0.top.equalTo(loanAmountTextField.snp.bottom).offset(Spacing.base06)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        dueDateLabel.snp.makeConstraints {
            $0.top.equalTo(dueDateQuestionLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        installmentsQuestionLabel.snp.makeConstraints {
            if interactor.displayDueDate {
                $0.top.equalTo(dueDateLabel.snp.bottom).offset(Spacing.base06)
            } else {
                $0.top.equalTo(loanAmountTextField.snp.bottom).offset(Spacing.base06)
            }
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        installmentsInfoView.snp.makeConstraints {
            $0.top.equalTo(stepperBackgroundView.snp.bottom).offset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        stepperBackgroundView.snp.makeConstraints {
            $0.top.equalTo(installmentsQuestionLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        stepperLabel.snp.makeConstraints {
            $0.top.leading.bottom.equalToSuperview()
        }
        
        stepperView.snp.makeConstraints {
            $0.top.trailing.bottom.equalToSuperview()
        }
        
        nextButton.snp.makeConstraints {
            $0.height.equalTo(Layout.Height.nextButton)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).offset(-Spacing.base02)
        }
    }

    override func configureViews() {
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        }

        stepperView.changeState(to: .disabled)
        installmentsInfoView.changeState(to: .error)
        
        view.backgroundColor = Colors.backgroundPrimary.color
        
        view.accessibilityIdentifier = Accessibility.backgroundView.rawValue
        
        let backButton = UIBarButtonItem()
        backButton.title = String()
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        let infoButton = UIBarButtonItem(image: Resources.Icons.icoQuestion.image,
                                         style: .plain,
                                         target: self,
                                         action: #selector(didTapInfo))

        navigationItem.rightBarButtonItem = infoButton
        navigationController?.navigationBar.isTranslucent = false
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(didTapView))
        view.addGestureRecognizer(gesture)
    }
    
    @objc
    func didTapView() {
        guard loanAmountTextField.valueTextField.isFirstResponder else { return }
        loanAmountTextField.valueTextField.resignFirstResponder()
    }
}

private extension SimulationViewController {
    func createLabel(
        for text: String,
        withColor palette: DynamicColorType = Colors.grayscale700,
        andFont font: UIFont = Typography.bodyPrimary(.highlight).font()
    ) -> UILabel {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = font
        label.text = text
        label.textColor = palette.color
        label.set(lineHeight: 24)
        return label
    }
    
    func didError(with action: RetryAction) {
        let primaryButtonAction = ApolloAlertAction(title: Strings.Generic.tryAgain) {
            self.interactor.onConfirmErrorAlert(action: action)
        }
        
        let linkButtonAction = ApolloAlertAction(title: Strings.Generic.notNow) {
            self.cancelAlert(with: action)
        }
        
        showApolloAlert(type: .sticker(style: .error),
                        title: Strings.Generic.title,
                        subtitle: Strings.Generic.message,
                        primaryButtonAction: primaryButtonAction,
                        linkButtonAction: linkButtonAction)
    }
    
    private func cancelAlert(with action: RetryAction) {
        changeStepperViewState(to: .disabled)
        if case .simulate = action {
            changeInstallmentsInfoViewState(to: .error)
        }
    }
}

@objc
private extension SimulationViewController {
    func nextStep() {
        interactor.choseSimulation(with: stepperView.currentIndex)
    }

    func didTapInfo() {
        interactor.openFAQ()
    }

    func didTapDueDate() {
        interactor.didTapDueDate()
    }
}

extension SimulationViewController: SimulationDisplay {
    func setupLoanAmountQuestionDescription(description: String) {
        loanAmountQuestionLabel.text = description
    }

    func setupTitleDescription(title: String) {
        self.title = title
    }

    func setupDueDate(_ dueDateText: NSAttributedString) {
        dueDateLabel.attributedText = dueDateText
    }
    
    func displayDueDateInfo() {
        let primaryAlertButton = ApolloAlertAction(title: Strings.Generic.ok) {
            self.interactor.openCalendarView()
        }
        
        showApolloAlert(image: Resources.Illustrations.iluPersonInfoPurple.image,
                        title: Strings.Simulation.InstallmentDueDate.Info.title,
                        subtitle: Strings.Simulation.InstallmentDueDate.Info.subtitle,
                        primaryButtonAction: primaryAlertButton )
    }

    func set(_ maximumLimit: String) {
        loanAmountTextField.value = maximumLimit
    }
    
    func setupTextfield(_ range: ClosedRange<Double>) {
        loanAmountTextField.range = range
    }
    
    func setupInstallments(_ list: [Int]) {
        stepperView.set(list)
    }
    
    func displayError(with action: RetryAction) {
        didError(with: action)
    }
    
    func changeStepperViewState(to state: StepperViewState) {
        stepperView.changeState(to: state)
    }
    
    func changeInstallmentsInfoViewState(to state: InstallmentsInfoViewState) {
        installmentsInfoView.changeState(to: state)
    }
    
    func startLoading() {
        startLoadingView()
    }
    
    func stopLoading() {
        stopLoadingView()
    }
    
    func startButtonLoading() {
        nextButton.startLoadingAnimating()
    }
    
    func stopButtonLoading() {
        nextButton.stopLoadingAnimating(isEnabled: false)
    }
    
    func enableNextButton(_ isEnabled: Bool) {
        nextButton.isEnabled = isEnabled
    }
}

extension SimulationViewController: LoanCurrencyFieldDelegate {
    func value(_ outOfRange: Bool) {
        stepperView.changeState(to: .disabled)

        if outOfRange {
            installmentsInfoView.changeState(to: .error)
        }

        enableNextButton(false)
    }
    
    func didResign(without error: Bool) {
        guard error else { return }
        
        interactor.trackChangeSimulationValue()
        interactor.simulate(with: loanAmountTextField.formattedValue)
    }
}

extension SimulationViewController: StepperViewDelegate {
    func didIncrease() {
        interactor.getNextInstallment()
    }
    
    func didDecrease() {
        interactor.getPreviousInstallment()
    }
}
