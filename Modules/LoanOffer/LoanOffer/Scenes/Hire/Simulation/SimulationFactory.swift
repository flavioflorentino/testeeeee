enum SimulationFactory {
    static func make(
        with delegate: LoanHireFlowCoordinating,
        range: SimulationRange?,
        from origin: OriginType = .reason
    ) -> SimulationViewController {
        let dependencies = LoanDependencyContainer()
        let service: SimulationServicing = SimulationService(dependencies: dependencies)
        let coordinator: SimulationCoordinating = SimulationCoordinator(delegate: delegate, dependencies: dependencies)
        let presenter: SimulationPresenting = SimulationPresenter(coordinator: coordinator)
        let interactor = SimulationInteractor(
            service: service,
            presenter: presenter,
            range: range,
            from: origin,
            dependencies: dependencies
        )
        let viewController = SimulationViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController
        
        return viewController
    }
}
