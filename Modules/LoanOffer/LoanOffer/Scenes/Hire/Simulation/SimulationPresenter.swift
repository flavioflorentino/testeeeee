import Core
import Foundation
import UI

protocol SimulationPresenting: AnyObject {
    var viewController: SimulationDisplay? { get set }
    func set(_ maximumLimit: String)
    func setupLoanAmountQuestionDescription(isRegistrationCompleted: Bool)
    func setupTitleDescription(isRegistrationCompleted: Bool)
    func setupInstallmentsView(with installments: Int, andInstallmentValue installmentValue: String)
    func setupTextfield(_ range: ClosedRange<Double>)
    func setupInstallments(list: [Int])
    func showError(with action: RetryAction)
    func nextStep(with contract: ContractResponse)
    func changeStepperViewState(to state: StepperViewState)
    func changeInstallmentsInfoViewState(to state: InstallmentsInfoViewState)
    func changeNextButtonState(toEnabled enabled: Bool)
    func startLoading()
    func stopLoading()
    func startButtonLoading()
    func stopButtonLoading()
    func openFAQ()
    func showTimeLimitWarning()
    func finish()
    func setupDueDate(_ date: Date?)
    func presentCalendarView(with date: Date, daysToDueDate: Int?, delegate: PaymentDateDelegate)
    func showDueDateInfo()
}

final class SimulationPresenter {
    private let coordinator: SimulationCoordinating
    weak var viewController: SimulationDisplay?
    
    init(coordinator: SimulationCoordinating) {
        self.coordinator = coordinator
    }
}

extension SimulationPresenter: SimulationPresenting {
    func presentCalendarView(with date: Date, daysToDueDate: Int?, delegate: PaymentDateDelegate) {
        coordinator.perform(action: .openCalendar(selectedDate: date, origin: .simulation, daysToDueDate: daysToDueDate, delegate: delegate))
    }

    func setupDueDate(_ date: Date?) {
        guard let date = date else { return }
        let formattedDate = Date.forthFormatter.string(from: date)
            .attributedStringWith(
                normalFont: Typography.title(.medium).font(),
                highlightFont: Typography.title(.medium).font(),
                normalColor: Colors.success600.color,
                highlightColor: Colors.success600.color,
                underline: true)
        viewController?.setupDueDate(formattedDate)
    }
    
    func showDueDateInfo() {
        viewController?.displayDueDateInfo()
    }

    func set(_ maximumLimit: String) {
        viewController?.set(maximumLimit)
    }
    
    func setupInstallmentsView(with installments: Int, andInstallmentValue installmentValue: String) {
        viewController?.changeInstallmentsInfoViewState(to: .success("\(installments)", installmentValue))
    }
    
    func setupTextfield(_ range: ClosedRange<Double>) {
        viewController?.setupTextfield(range)
    }

    func setupLoanAmountQuestionDescription(isRegistrationCompleted: Bool) {
        let description = isRegistrationCompleted ? Strings.Simulation.loanAmountQuestionRegistrationCompleted : Strings.Simulation.loanAmountQuestion
        viewController?.setupLoanAmountQuestionDescription(description: description)
    }

    func setupTitleDescription(isRegistrationCompleted: Bool) {
        let title = isRegistrationCompleted ? Strings.Simulation.titleWithRegistrationCompleted : Strings.Simulation.title
        viewController?.setupTitleDescription(title: title)
    }
    
    func setupInstallments(list: [Int]) {
        guard list.isNotEmpty else {
            showError()
            return
        }
        viewController?.setupInstallments(list)
    }
    
    func showError(with action: RetryAction = .simulate) {
        viewController?.displayError(with: action)
        
        if case .confirm = action {
            viewController?.stopButtonLoading()
        }
    }
    
    func nextStep(with contract: ContractResponse) {
        coordinator.perform(action: .terms(contract: contract))
    }
    
    func changeStepperViewState(to state: StepperViewState) {
        viewController?.changeStepperViewState(to: state)
    }
    
    func changeInstallmentsInfoViewState(to state: InstallmentsInfoViewState) {
        viewController?.changeInstallmentsInfoViewState(to: state)
    }
    
    func changeNextButtonState(toEnabled enabled: Bool) {
        viewController?.enableNextButton(enabled)
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
    
    func startButtonLoading() {
        viewController?.startButtonLoading()
    }
    
    func stopButtonLoading() {
        viewController?.stopButtonLoading()
    }
    
    func openFAQ() {
        coordinator.perform(action: .openFAQ)
    }

    func showTimeLimitWarning() {
        let content = ApolloFeedbackViewContent(image: Assets.unavailable.image,
                                                title: Strings.Alerts.Unavailable.title,
                                                description: NSAttributedString(string: Strings.Alerts.Unavailable.subtitle),
                                                primaryButtonTitle: Strings.Generic.ok,
                                                secondaryButtonTitle: "")
        coordinator.perform(action: .warning(content))
    }
    
    func finish() {
        coordinator.perform(action: .finish)
    }
}
