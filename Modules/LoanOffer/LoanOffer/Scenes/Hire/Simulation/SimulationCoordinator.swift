import Core
import UI
import Foundation

enum SimulationAction: Equatable {
    case terms(contract: ContractResponse)
    case openFAQ
    case openCalendar(selectedDate: Date, origin: OriginType, daysToDueDate: Int?, delegate: PaymentDateDelegate)
    case warning(_ warning: ApolloFeedbackViewContent)
    case finish
    
    static func == (lhs: SimulationAction, rhs: SimulationAction) -> Bool {
        switch (lhs, rhs) {
        case (.finish, .finish),
             (.openFAQ, .openFAQ):
            return true
        case let (.terms(lhsContract), .terms(rhsContract)):
            return lhsContract == rhsContract
        case let (.warning(lhsWarning), .warning(rhsWarning)):
            return lhsWarning == rhsWarning
        default:
            return false
        }
    }
}

protocol SimulationCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: SimulationAction)
}

final class SimulationCoordinator: SimulationCoordinating {
    weak var viewController: UIViewController?
    
    typealias Dependencies = HasLegacy
    private let dependencies: Dependencies
    
    weak var delegate: LoanHireFlowCoordinating?
    
    init(delegate: LoanHireFlowCoordinating, dependencies: Dependencies) {
        self.delegate = delegate
        self.dependencies = dependencies
    }
    
    func perform(action: SimulationAction) {
        switch action {
        case let .terms(contract):
            delegate?.perform(action: .terms(contract: contract))
        case .openFAQ:
            dependencies.legacy.customerSupport?.show(article: .simulation)
        case let .openCalendar(selectedDate, origin, days, delegate):
            let controller = PaymentDateFactory.make(selectedDate: selectedDate, daysToDueDate: days, origin: origin, delegate: delegate)
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case let .warning(warning):
            delegate?.perform(action: .warning(warning))
        case .finish:
            delegate?.perform(action: .finish)
        }
    }
}
