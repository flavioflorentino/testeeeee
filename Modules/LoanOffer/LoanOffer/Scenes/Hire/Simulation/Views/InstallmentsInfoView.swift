import Foundation
import UI

private extension InstallmentsInfoView.Layout {
    enum Height {
        static let background: CGFloat = 82
    }
}

enum InstallmentsInfoViewState: Equatable {
    case loading
    case error
    case success(_ installments: String, _ installmentValue: String)
}

final class InstallmentsInfoView: UIView {
    fileprivate struct Layout {}
    
    enum Accessibility: String {
        case installmentInfoLabel
    }
    
    private lazy var backgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.backgroundSecondary.color
        view.layer.cornerRadius = CornerRadius.strong
        return view
    }()
    
    private lazy var installmentsInfoLabel = UILabel()
    
    private lazy var loading: UIActivityIndicatorView = {
        let loading = UIActivityIndicatorView()
        loading.hidesWhenStopped = true
        loading.startAnimating()
        return loading
    }()
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func changeState(to state: InstallmentsInfoViewState) {
        state == .loading ? loading.startAnimating() : loading.stopAnimating()
        
        if case let .success(installments, installmentValue) = state {
            installmentsInfoLabel.isHidden = false
            installmentsInfoLabel.attributedText = attributedText(for: installments,
                                                                  and: installmentValue)
        } else {
            installmentsInfoLabel.isHidden = true
        }
    }
}

private extension InstallmentsInfoView {
    func attributedText(for installments: String, and installmentValue: String) -> NSAttributedString {
        let infoViewTitle = Strings.Simulation.infoViewTitle
        let title = infoViewTitle.attributedStringWith(normalFont: Typography.bodyPrimary(.highlight).font(),
                                                       highlightFont: Typography.bodyPrimary(.highlight).font(),
                                                       normalColor: Colors.grayscale700.color,
                                                       highlightColor: Colors.grayscale700.color,
                                                       textAlignment: .center,
                                                       underline: false,
                                                       lineHeight: 24)
        
        let installmentsInfo = Strings.Simulation.installmentsInfo(installments, installmentValue)
        let info = installmentsInfo.attributedStringWith(normalFont: Typography.title(.medium).font(),
                                                         highlightFont: Typography.title(.medium).font(),
                                                         normalColor: Colors.grayscale700.color,
                                                         highlightColor: Colors.brandingBase.color,
                                                         textAlignment: .center,
                                                         underline: false,
                                                         lineHeight: 24)
        
        let finalString: NSMutableAttributedString = title
        finalString.append(info)
        
        return finalString
    }
}

extension InstallmentsInfoView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(backgroundView)
        backgroundView.addSubview(loading)
        backgroundView.addSubview(installmentsInfoLabel)
    }
    
    func setupConstraints() {
        backgroundView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.height.equalTo(Layout.Height.background)
        }
        
        loading.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
        
        installmentsInfoLabel.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    func configureViews() {
        backgroundColor = .clear
        translatesAutoresizingMaskIntoConstraints = false
        installmentsInfoLabel.numberOfLines = 0
        installmentsInfoLabel.accessibilityIdentifier = Accessibility.installmentInfoLabel.rawValue
    }
}
