import UI

private extension UIPPSimpleInputTextField.Layout {
    enum Size {
        static let button = CGSize(width: 18, height: 18)
    }
}

final class UIPPSimpleInputTextField: UITextField {
    fileprivate enum Layout { }
    
    enum Accessibility: String {
        case clearTextButton
    }
    
    private lazy var clearButton: UIButton = {
        let button = UIButton()
        button.accessibilityIdentifier = Accessibility.clearTextButton.rawValue
        button.addTarget(self, action: #selector(clearText), for: .touchUpInside)
        button.setImage(Assets.aliasClearText.image, for: .normal)
        return button
    }()

    override func draw(_ rect: CGRect) {
        borderStyle = .none
        layoutIfNeeded()
        setAccessoryView()
        layer.masksToBounds = true
    }
}

private extension UIPPSimpleInputTextField {
    func setAccessoryView() {
        addSubview(clearButton)
        
        clearButton.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.button)
            $0.trailing.centerY.equalToSuperview()
        }
    }
    
    @objc
    func clearText() {
        text = ""
    }
}
