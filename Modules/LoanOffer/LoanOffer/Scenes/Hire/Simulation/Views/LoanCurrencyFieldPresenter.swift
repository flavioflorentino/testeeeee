import UIKit
import Foundation

protocol LoanCurrencyFieldPresenterInputs {
    func setup(initialValue: String, range: ClosedRange<Double>)
    func changeInputValue(value: String)
    func inputValue(value: String)
}

protocol LoanCurrencyFieldPresenterOutputs: AnyObject {
    func highlightColor()
    func emptyColor()
    func deleteText()
    func updateTextValue(text: String?)
    func updateValue(value: Double)
    func showError(_ message: String)
    func showPlaceholder(_ placeholder: String)
}

protocol LoanCurrencyFieldPresenterProtocol: AnyObject {
    var inputs: LoanCurrencyFieldPresenterInputs { get }
    var outputs: LoanCurrencyFieldPresenterOutputs? { get set }
    func update(_ range: ClosedRange<Double>)
}

final class LoanCurrencyFieldPresenter {        
    private var downLimit: Double = 0
    private var upperLimit: Double = 0
    private var limit = Int.max
    
    var inputs: LoanCurrencyFieldPresenterInputs { self }
    weak var outputs: LoanCurrencyFieldPresenterOutputs?
}

private extension LoanCurrencyFieldPresenter {
    func getFormattedCurrency(text: String) -> NSNumber {
        guard let amount = Double(text) else {
            return 0.0
        }
        
        return NSNumber(value: (amount / 100))
    }

    func formatterCurrency(text: String) {
        let number = getFormattedCurrency(text: text)
        changeValue(value: number)
        showError(value: number)
    }
    
    func changeValue(value: NSNumber) {
        outputs?.updateValue(value: value.doubleValue)
    }
    
    func showError(value: NSNumber) {
        guard upperLimit > 0.0 else {
            return
        }
        
        let showError = value.doubleValue < downLimit || value.doubleValue > upperLimit
        
        if showError {
            let formattedNumber = NumberFormatter.toCurrencyString(
                with: value.doubleValue < downLimit ? downLimit : upperLimit)
            let message = value.doubleValue < downLimit
                ? Strings.Simulation.minimumValueError(formattedNumber ?? "")
                : Strings.Simulation.maximumValueError(formattedNumber ?? "")
            
            outputs?.showError(message)
        } else {
            showPlaceholder()
        }
    }
    
    func showPlaceholder() {
        guard
            let downLimitFormatted = NumberFormatter.toCurrencyString(with: downLimit),
            let upperLimitFormatted = NumberFormatter.toCurrencyString(with: upperLimit)
            else {
                return
        }
        
        outputs?.showPlaceholder(Strings.Simulation.rangePlaceholder(downLimitFormatted,
                                                                     upperLimitFormatted))
    }
}

extension LoanCurrencyFieldPresenter: LoanCurrencyFieldPresenterProtocol {
    func update(_ range: ClosedRange<Double>) {
        self.downLimit = range.lowerBound
        self.upperLimit = range.upperBound
        
        showPlaceholder()
    }
}

extension LoanCurrencyFieldPresenter: LoanCurrencyFieldPresenterInputs {
    func setup(initialValue: String, range: ClosedRange<Double>) {
        update(range)
        inputValue(value: initialValue)
        changeInputValue(value: initialValue)
    }

    func inputValue(value: String) {
        let text = value.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
        
        guard text.count <= limit else {
            outputs?.deleteText()
            return
        }
        
        formatterCurrency(text: text)
    }
    
    func changeInputValue(value: String) {
        outputs?.updateTextValue(text: value)
    }
}
