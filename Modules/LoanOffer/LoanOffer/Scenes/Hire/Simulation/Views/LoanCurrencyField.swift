import UI

protocol LoanCurrencyFieldDelegate: AnyObject {
    func value(_ outOfRange: Bool)
    func didResign(without error: Bool)
}

private extension LoanCurrencyField.Layout {
    enum Size {
        static let textHeight: CGFloat = 28
    }
}

final class LoanCurrencyField: UIView {
    fileprivate enum Layout {}
    
    enum Accessibility: String {
        case quantityTextField
    }
    
    private lazy var presenter: LoanCurrencyFieldPresenterProtocol = {
        let presenter = LoanCurrencyFieldPresenter()
        presenter.outputs = self
        
        return presenter
    }()
    
    private lazy var errorLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
        return label
    }()
    
    lazy var valueTextField: UIPPSimpleInputTextField = {
        let textField = UIPPSimpleInputTextField()
        textField.delegate = self
        textField.font = Typography.title(.medium).font()
        textField.textColor = Colors.brandingBase.color
        textField.keyboardType = .numberPad
        textField.addTarget(self, action: #selector(textDidChange(_:)), for: .editingChanged)
        return textField
    }()
    
    private lazy var border: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale200.color
        
        return view
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .leading
        stackView.spacing = Spacing.base01
        
        return stackView
    }()
    
    private(set) var error = false
    
    var value: String = "" {
        didSet {
            presenter.inputs.changeInputValue(value: value)
        }
    }
    
    var formattedValue: String {
        replaceNonBreakableSpace()
    }
    
    var range: ClosedRange<Double> = 0.0...0.0 {
        didSet {
            presenter.update(range)
        }
    }
    
    var text: String? {
        valueTextField.text
    }
    
    weak var delegate: LoanCurrencyFieldDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        setupView()
    }
}

private extension LoanCurrencyField {
    func setupView() {
        self.accessibilityIdentifier = Accessibility.quantityTextField.rawValue
        backgroundColor = .clear
        presenter.inputs.setup(initialValue: value, range: range)
    }
    
    func addComponents() {
        stackView.addArrangedSubview(valueTextField)
        stackView.addArrangedSubview(border)
        stackView.addArrangedSubview(errorLabel)
        addSubview(stackView)
        
        if #available(iOS 11, *) {
            stackView.setCustomSpacing(Spacing.base00, after: valueTextField)
            stackView.setCustomSpacing(Spacing.base02, after: border)
        } else {
            let spacing00 = UIView()
            spacing00.layout {
                $0.height == Spacing.base00
            }
            stackView.insertSubview(spacing00, belowSubview: valueTextField)
            
            let spacing02 = UIView()
            spacing02.layout {
                $0.height == Spacing.base02
            }
            stackView.insertSubview(spacing02, belowSubview: border)
        }
    }
    
    func layoutComponents() {
        stackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        valueTextField.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(Layout.Size.textHeight)
        }
        
        border.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(Border.light)
        }
        
        errorLabel.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
        }
    }
    
    func replaceNonBreakableSpace() -> String {
        value.replacingOccurrences(of: " ", with: " ")
    }
}

@objc
extension LoanCurrencyField {
    func textDidChange(_ textField: UITextField) {
        guard let text = textField.text else {
            return
        }
        presenter.inputs.inputValue(value: text)
    }
}

extension LoanCurrencyField: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        highlightColor()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        emptyColor()
        resignResponder()
    }
}

extension LoanCurrencyField: LoanCurrencyFieldPresenterOutputs {
    func showError(_ message: String) {
        errorLabel.text = message
        errorLabel.textColor = .critical600()
    }
    
    func showPlaceholder(_ placeholder: String) {
        errorLabel.text = placeholder
        errorLabel.textColor = .grayscale700()
    }
    
    func highlightColor() {
        border.backgroundColor = Colors.brandingBase.color
    }
    
    func emptyColor() {
        border.backgroundColor = Colors.grayscale200.color
    }
    
    func deleteText() {
        valueTextField.deleteBackward()
    }
    
    func updateTextValue(text: String?) {
        valueTextField.text = text
    }
    
    func updateValue(value: Double) {
        guard let formatted = NumberFormatter.toCurrencyString(with: value) else {
            return
        }
        self.value = formatted
        
        error = (range.upperBound.isZero && range.lowerBound.isZero) || range.contains(value)
        delegate?.value(!error)
    }
    
    func resignResponder() {
        if valueTextField.text == nil || valueTextField.text?.isEmpty == true {
            guard let formatted = NumberFormatter.toCurrencyString(with: range.lowerBound) else {
                return
            }
            presenter.inputs.changeInputValue(value: formatted)
        }
        
        delegate?.didResign(without: error)
    }
}
