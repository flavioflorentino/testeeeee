import Foundation
import UI

enum StepperButtonState: Equatable {
    case enabled
    case disabled
}

enum StepperButtonType {
    case plus
    case minus
}

final class StepperButton: UIButton {
    private(set) var type: StepperButtonType
    
    init(type: StepperButtonType, frame: CGRect = .zero) {
        self.type = type
        super.init(frame: frame)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func changeState(to state: StepperButtonState) {
        var asset: ImageAsset
        
        switch (type, state) {
        case (.plus, .enabled):
            asset = Assets.plusEnabled
        case (.minus, .enabled):
            asset = Assets.minusEnabled
        case (.plus, .disabled):
            asset = Assets.plusDisabled
        case (.minus, .disabled):
            asset = Assets.minusDisabled
        }
        
        setBackgroundImage(asset.image, for: .normal)
        isEnabled = state == .enabled
    }
}
