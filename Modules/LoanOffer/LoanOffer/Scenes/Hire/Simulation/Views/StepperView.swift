import Foundation
import UI

protocol StepperViewDelegate: AnyObject {
    func didIncrease()
    func didDecrease()
}

enum StepperViewState: Equatable {
    case enabled
    case disabled
}

private extension StepperView.Layout {
    enum Size {
        static let `default` = CGSize(width: 32, height: 32)
    }
}

final class StepperView: UIView {
    fileprivate struct Layout {}
    
    enum Accessibility: String {
        case plusButton
        case minusButton
        case quantityLabel
    }
    
    private lazy var plusButton: StepperButton = {
        let button = StepperButton(type: .plus)
        button.accessibilityIdentifier = Accessibility.plusButton.rawValue
        button.addTarget(self, action: #selector(increase), for: .touchUpInside)
        return button
    }()
    
    private lazy var quantityLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = Accessibility.quantityLabel.rawValue
        label.font = Typography.title(.medium).font()
        label.textColor = Colors.black.color
        label.textAlignment = .center
        label.text = "0"
        label.set(lineHeight: 24)
        return label
    }()
    
    private lazy var minusButton: StepperButton = {
        let button = StepperButton(type: .minus)
        button.accessibilityIdentifier = Accessibility.minusButton.rawValue
        button.addTarget(self, action: #selector(decrease), for: .touchUpInside)
        return button
    }()

    private(set) var currentIndex: Int = 0
    private(set) var list: [Int] = []
    private(set) weak var delegate: StepperViewDelegate?
    private var timer: Timer?
    
    init(delegate: StepperViewDelegate, frame: CGRect = .zero, with initialState: StepperViewState = .disabled) {
        self.delegate = delegate
        super.init(frame: frame)
        
        buildLayout()
        
        minusButton.addTarget(self, action: #selector(buttonDown), for: .touchDown)
        minusButton.addTarget(self, action: #selector(buttonUp), for: [.touchUpInside, .touchUpOutside])
        plusButton.addTarget(self, action: #selector(buttonDown), for: .touchDown)
        plusButton.addTarget(self, action: #selector(buttonUp), for: [.touchUpInside, .touchUpOutside])
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func changeState(to state: StepperViewState) {
        let disabled = state == .disabled
        plusButton.changeState(to: disabled ? .disabled : .enabled)
        minusButton.changeState(to: disabled ? .disabled : .enabled)
    }
    
    func set(_ list: [Int]) {
        self.list = list
        usePreviousIndexIfPossible()
    }
}

private extension StepperView {
    func usePreviousIndexIfPossible() {
        guard list.contains(currentIndex) else {
            setCurrent(list.last ?? 0)
            return
        }
        setCurrent(currentIndex)
    }
    
    func setCurrent(_ index: Int) {
        currentIndex = index
        quantityLabel.text = "\(index)"
        checkLimits()
    }
    
    func checkLimits() {
        if currentIndex == list.first && currentIndex == list.last {
            minusButton.changeState(to: .disabled)
            plusButton.changeState(to: .disabled)
            quantityLabel.textColor = Palette.ppColorGrayscale400.color
        } else if currentIndex == list.first {
            plusButton.changeState(to: .enabled)
            minusButton.changeState(to: .disabled)
            quantityLabel.textColor = Palette.ppColorGrayscale600.color
        } else if currentIndex == list.last {
            minusButton.changeState(to: .enabled)
            plusButton.changeState(to: .disabled)
            quantityLabel.textColor = Palette.ppColorGrayscale600.color
        } else {
            minusButton.changeState(to: .enabled)
            plusButton.changeState(to: .enabled)
            quantityLabel.textColor = Palette.ppColorGrayscale600.color
        }
    }
}

@objc
private extension StepperView {
    func increase() {
        guard let index = list.next(currentIndex) else {
            return
        }
        
        setCurrent(index)
        delegate?.didIncrease()
    }
    
    func decrease() {
        guard let index = list.previous(currentIndex) else {
            return
        }
        
        setCurrent(index)
        delegate?.didDecrease()
    }
    
    func buttonDown(_ sender: UIButton) {
        timer = Timer.scheduledTimer(timeInterval: 0.3,
                                     target: self,
                                     selector: #selector(rapidFire),
                                     userInfo: sender,
                                     repeats: true)
    }

    func buttonUp(_ sender: UIButton) {
        timer?.invalidate()
        timer = nil
    }

    func rapidFire() {
        guard let button = timer?.userInfo as? UIButton else {
            return
        }
        
        button == minusButton ? decrease() : increase()
    }
}
    
extension StepperView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(minusButton)
        addSubview(quantityLabel)
        addSubview(plusButton)
    }

    func setupConstraints() {
        plusButton.snp.makeConstraints {
            $0.top.bottom.trailing.equalToSuperview()
            $0.size.equalTo(Layout.Size.default)
        }
        
        quantityLabel.snp.makeConstraints {
            $0.leading.equalTo(minusButton.snp.trailing).offset(Spacing.base01)
            $0.trailing.equalTo(plusButton.snp.leading).offset(-Spacing.base01)
            $0.size.equalTo(Layout.Size.default)
        }
        
        minusButton.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.default)
            $0.leading.equalToSuperview()
        }
    }
    
    func configureViews() {
        backgroundColor = .clear
        translatesAutoresizingMaskIntoConstraints = false
    }
}
