import Foundation
import UIKit
import UI
import AssetsKit

protocol IntroDisplay: AnyObject {
    func updateViewMessages(
        with summary: NSAttributedString,
        andDisclaimer disclaimer: NSAttributedString,
        shouldHideFooter hidden: Bool
    )
    func displayError()
    func startLoading(title: String)
    func stopLoading()
    func addUserToWaitList()
    func addBulletRow(icon: Iconography, description: String)
}

private extension IntroViewController.Layout {
    enum Height {
        static let summaryLabel: CGFloat = 72
    }
}

final class IntroViewController: ViewController<IntroInteractorInputs, UIView> {
    fileprivate enum Layout {}
    
    enum Accessibility: String {
        case nextButton
    }
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsVerticalScrollIndicator = false
        scrollView.isHidden = true
        return scrollView
    }()
    
    private lazy var contentStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = Spacing.base02
        return stack
    }()
    
    private lazy var image: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.addMoney.image
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var contentView = UIView()
    
    private lazy var summaryLabel = UILabel()
    
    private lazy var summaryDisclaimerLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.Intro.descriptionDisclaimer
        label
            .labelStyle(CaptionLabelStyle())
            .with(\.textColor, .grayscale400())
        return label
    }()
    
    private lazy var disclaimerLabel = UILabel()
    
    private lazy var nextButton: UIButton = {
       let button = UIButton()
        button.accessibilityIdentifier = Accessibility.nextButton.rawValue
        button.setTitle(Strings.Intro.button, for: .normal)
        button.addTarget(self, action: #selector(nextStep), for: .touchUpInside)
        button.isHidden = true
        button.buttonStyle(PrimaryButtonStyle())
        return button
    }()
    
    lazy var loadingView: LoadingAnimationView = {
        let loading = LoadingAnimationView()
        loading.isHidden = true
        return loading
    }()

    private lazy var bulletStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = Spacing.base02
        return stack
    }()
        
    private lazy var disclaimerFooterLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.Intro.disclaimerFooter
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale700())
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.fetchRange()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpLoanNavigationAppearance()
        interactor.trackEnterView()
    }

    override func buildViewHierarchy() {
        view.addSubview(loadingView)
        view.addSubview(scrollView)
        scrollView.addSubview(contentStackView)

        contentStackView.addArrangedSubview(image)
        contentStackView.addArrangedSubview(contentView)
        
        contentView.addSubview(summaryLabel)
        contentView.addSubview(summaryDisclaimerLabel)
        contentView.addSubview(disclaimerLabel)
        contentView.addSubview(bulletStackView)
        contentView.addSubview(disclaimerFooterLabel)
        view.addSubview(nextButton)
        interactor.setupBulletRows()
    }
    
    override func setupConstraints() {
        loadingView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.size.equalToSuperview()
        }

        scrollView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(nextButton.snp.top)
        }
        
        contentStackView.snp.makeConstraints {
            $0.top.equalTo(view.snp.topMargin).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
            $0.width.equalTo(scrollView)
        }

        summaryLabel.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        summaryDisclaimerLabel.snp.makeConstraints {
            $0.top.equalTo(summaryLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        disclaimerLabel.snp.makeConstraints {
            $0.top.equalTo(summaryDisclaimerLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        bulletStackView.snp.makeConstraints {
            $0.top.equalTo(disclaimerLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        disclaimerFooterLabel.snp.makeConstraints {
            $0.top.equalTo(bulletStackView.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.bottom.equalToSuperview().offset(-Spacing.base02)
            $0.bottom.equalTo(scrollView).offset(-Spacing.base02)
        }
        
        nextButton.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).offset(-Spacing.base02)
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        contentView.backgroundColor = .clear
        summaryLabel.numberOfLines = 0
        disclaimerLabel.numberOfLines = 0
    }
    
    private func setUpLoanNavigationAppearance() {
        guard let navigation = navigationController else {
            return
        }
        navigation.setupLoanAppearance()
        let infoButton = UIBarButtonItem(image: Resources.Icons.icoQuestion.image,
                                         style: .plain,
                                         target: self,
                                         action: #selector(didTapInfo))
        navigationItem.rightBarButtonItem = infoButton
    }
}

@objc
private extension IntroViewController {
    func nextStep() {
        interactor.nextStep()
    }
    
    func didTapInfo() {
        interactor.openFAQ()
    }
}

extension IntroViewController: IntroDisplay {
    func updateViewMessages(
        with summary: NSAttributedString,
        andDisclaimer disclaimer: NSAttributedString,
        shouldHideFooter hidden: Bool
    ) {
        summaryLabel.attributedText = summary
        disclaimerLabel.attributedText = disclaimer
        scrollView.isHidden = false
        nextButton.isHidden = false
        disclaimerFooterLabel.isHidden = hidden
    }
    
    func startLoading(title: String) {
        loadingView.isHidden = false
        loadingView.start(with: title)
        navigationController?.isNavigationBarHidden = true
    }
    
    func stopLoading() {
        loadingView.isHidden = true
        navigationController?.isNavigationBarHidden = false
    }
    
    func displayError() {
        let primaryButtonAction = ApolloAlertAction(title: Strings.Generic.tryAgain) { [weak self] in
            self?.interactor.fetchRange()
        }
        
        let linkButtonAction = ApolloAlertAction(title: Strings.Generic.notNow) { [weak self] in
            self?.interactor.close()
        }
        
        showApolloAlert(type: .sticker(style: .error),
                        title: Strings.Generic.title,
                        subtitle: Strings.Generic.message,
                        primaryButtonAction: primaryButtonAction,
                        linkButtonAction: linkButtonAction)
    }
    
    func addBulletRow(icon: Iconography, description: String) {
        let imageTextView = ImageTextView()
        imageTextView.setImage(icon, andDescription: description)
        
        bulletStackView.addArrangedSubview(imageTextView)
    }

    func addUserToWaitList() {
        interactor.addUserToWaitlist()
    }
}
