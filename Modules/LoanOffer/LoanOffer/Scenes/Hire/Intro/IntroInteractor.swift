import AnalyticsModule
import Core

protocol IntroInteractorInputs: AnyObject {
    func fetchRange()
    func addUserToWaitlist()
    func trackEnterView()
    func nextStep()
    func close()
    func openFAQ()
    func setupBulletRows()
}

final class IntroInteractor {
    typealias Dependencies = HasAnalytics & HasSafariView & HasLegacy
    private let dependencies: Dependencies
    
    private let presenter: IntroPresenting
    private let service: IntroServicing
    private var origin: OriginType
    private var range: SimulationRange?
    
    init(with presenter: IntroPresenting, service: IntroServicing, from origin: OriginType, dependencies: Dependencies) {
        self.presenter = presenter
        self.service = service
        self.origin = origin
        self.dependencies = dependencies
    }
}

extension IntroInteractor: IntroInteractorInputs {
    func fetchRange() {
        presenter.startLoading(title: Strings.Intro.Loading.title)
        
        service.fetchSimulationRange { [weak self] response in
            guard let self = self else { return }
            self.presenter.stopLoading()
            switch response {
            case let .success(range):
                self.range = range
                self.handleRegistrationStatus()
            case let .failure(error):
                self.handle(error)
            }
        }
    }

    func addUserToWaitlist() {
        presenter.startLoading(title: "")
        service.addToWaitList { [weak self] result in
            guard let self = self else { return }
            self.presenter.stopLoading()
            switch result {
            case .success:
                self.presenter.showWaitListAddedSuccess()
                self.dependencies.analytics.log(LoanHireEvent.didRequestLoanNotification)
            case let .failure(error):
                self.handle(error)
            }
        }
    }
    
    func trackEnterView() {
        guard let paramFlow: String = range?.analyticsParamFlow() else { return }
        dependencies.analytics.log(LoanEvent.didEnter(to: .onboarding, from: origin, params: ["Flow": paramFlow]))
    }

    func nextStep() {
        guard let range = range else {
            return
        }
        origin = .reason
        presenter.didNextStep(with: range)
    }
    
    func close() {
        presenter.close()
    }

    func openFAQ() {
        presenter.openFAQ()
    }
    
    func setupBulletRows() {
        presenter.setupBulletRows()
    }
}

private extension IntroInteractor {
    func handleRegistrationStatus() {
        guard let status = range?.registrationStatus else {
            presentAndLogError()
            return
        }
        
        switch status {
        case .denied:
            presenter.showRegistrationDenied()
        case .review:
            presenter.showRegistrationReview()
        case .analysis:
            presenter.showRegistrationAnalysis()
        default:
            setupView()
        }
    }

    func setupView() {
        guard
            let range = range,
            range.minimumCredit.isNotEmpty,
            range.maximumCredit.isNotEmpty
            else {
                presentAndLogError()
                return
        }
        
        let footerIsHidden = range.registrationStatus != .approved
        
        presenter.setupView(with: range.maximumCredit, daysToDueDate: range.daysToDueDate, shouldHideFooter: footerIsHidden)
    }
    
    func presentAndLogError() {
        dependencies.analytics.log(LoanEvent.didReceiveError(error: .onboarding))
        presenter.presentError()
    }
    
    func handle(_ error: ApiError) {
        switch error.requestError?.code {
        case LoanApiError.timeLimit.rawValue:
            presenter.showTimeLimitError()
            dependencies.analytics.log(LoanEvent.didReceiveError(error: .timeLimit))
        case LoanApiError.unavailableLimit.rawValue:
            presenter.showUnavailableLimit()
            dependencies.analytics.log(LoanHireEvent.unavailableLimit(origin: origin, reason: .limit))
        case LoanApiError.noOffer.rawValue:
            presenter.showUnavailableLimit()
            dependencies.analytics.log(LoanHireEvent.unavailableLimit(origin: origin, reason: .offer))
        case LoanApiError.proposalInProgress.rawValue:
            presenter.showProposalInProgress()
        default:
            presentAndLogError()
        }
    }
}
