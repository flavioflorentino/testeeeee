enum IntroFactory {
    static func make(with delegate: LoanHireFlowCoordinating, from origin: OriginType) -> IntroViewController {
        let dependencies = LoanDependencyContainer()
        let service = IntroService(dependencies: dependencies)
        let coordinator: IntroCoordinating = IntroCoordinator(delegate: delegate, dependencies: dependencies)
        let presenter: IntroPresenting = IntroPresenter(coordinator: coordinator)
        let interactor = IntroInteractor(with: presenter, service: service, from: origin, dependencies: dependencies)
        let viewController = IntroViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController
        
        return viewController
    }
}
