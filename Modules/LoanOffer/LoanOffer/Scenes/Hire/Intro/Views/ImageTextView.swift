import Foundation
import UIKit
import UI

final class ImageTextView: UIView {
    fileprivate enum Layout {}
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale700())
        return label
    }()

    private lazy var iconLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(IconLabelStyle(type: .medium))
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        label.setContentCompressionResistancePriority(.required, for: .horizontal)
        return label
    }()
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setImage(_ icon: Iconography, andDescription description: String) {
        iconLabel.text = icon.rawValue
        descriptionLabel.text = description
    }
}

extension ImageTextView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(iconLabel)
        addSubview(descriptionLabel)
    }
    
    func setupConstraints() {
        iconLabel.snp.makeConstraints {
            $0.top.bottom.leading.equalToSuperview()
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.bottom.trailing.equalToSuperview()
            $0.leading.equalTo(iconLabel.snp.trailing).offset(Spacing.base01)
        }
    }
    
    func configureViews() {
        backgroundColor = .clear
    }
}
