import Core
import Foundation
import UI
import AssetsKit

protocol IntroPresenting: AnyObject {
    var viewController: IntroDisplay? { get set }
    func setupView(with minimumLimit: String, daysToDueDate: Int?, shouldHideFooter hidden: Bool)
    func didNextStep(with range: SimulationRange)
    func presentError()
    func startLoading(title: String)
    func stopLoading()
    func close()
    func openFAQ()
    func showRegistrationReview()
    func setupBulletRows()
    func showTimeLimitError()
    func showRegistrationDenied()
    func showRegistrationAnalysis()
    func showUnavailableLimit()
    func showProposalInProgress()
    func showWaitListAddedSuccess()
}

final class IntroPresenter {
    private let coordinator: IntroCoordinating
    weak var viewController: IntroDisplay?

    init(coordinator: IntroCoordinating) {
        self.coordinator = coordinator
    }
}

extension IntroPresenter: IntroPresenting {
    func setupView(with minimumLimit: String,
                   daysToDueDate: Int?,
                   shouldHideFooter hidden: Bool) {
        var summaryRawText = String()

        if let days = daysToDueDate {
            summaryRawText = Strings.Intro.Description.With.daysToDueDate(minimumLimit, days)
        } else {
            summaryRawText = Strings.Intro.description(minimumLimit)
        }

        let summary = summaryRawText.attributedStringWith(normalFont: Typography.title(.medium).font(),
                                                          highlightFont: Typography.title(.medium).font(),
                                                          normalColor: Colors.black.color,
                                                          highlightColor: Colors.brandingBase.color,
                                                          underline: false,
                                                          lineHeight: 24)
        
        let disclaimer = Strings.Intro.disclaimer.attributedStringWith(normalFont: Typography.bodyPrimary().font(),
                                                                       highlightFont: Typography.bodyPrimary(.highlight).font(),
                                                                       normalColor: Colors.grayscale700.color,
                                                                       highlightColor: Colors.grayscale700.color,
                                                                       underline: false,
                                                                       lineHeight: 24)
        
        viewController?.updateViewMessages(with: summary, andDisclaimer: disclaimer, shouldHideFooter: true)
    }
    
    func didNextStep(with range: SimulationRange) {
        coordinator.perform(action: .reason(range: range))
    }

    func startLoading(title: String) {
        viewController?.startLoading(title: title)
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
    
    func presentError() {
        viewController?.displayError()
    }
    
    func close() {
        coordinator.perform(action: .close)
    }
    
    func openFAQ() {
        coordinator.perform(action: .openFAQ(article: .intro))
    }
    
    func showRegistrationReview() {
        coordinator.perform(action: .registrationReview)
    }
    
    func showWarning(_ warning: ApolloFeedbackViewContent) {
        coordinator.perform(action: .warning(warning))
    }
    
    func finish() {
        coordinator.perform(action: .finish)
    }
    
    func setupBulletRows() {
        [
            (icon: Iconography.receiptAlt, description: Strings.Intro.billetBullet),
            (icon: Iconography.usersAlt, description: Strings.Intro.friendsBullet),
            (icon: Iconography.creditCard, description: Strings.Intro.cardBullet)
        ]
        .forEach { bullet in
            viewController?.addBulletRow(icon: bullet.icon, description: bullet.description)
        }
    }

    func showTimeLimitError() {
        let content = ApolloFeedbackViewContent(image: Assets.unavailable.image,
                                                title: Strings.Alerts.Unavailable.title,
                                                description: NSAttributedString(string: Strings.Alerts.Unavailable.subtitle),
                                                primaryButtonTitle: Strings.Generic.ok,
                                                secondaryButtonTitle: "")
        coordinator.perform(action: .warning(content))
    }

    func showRegistrationDenied() {
        let content = ApolloFeedbackViewContent(image: Assets.error.image,
                                                title: Strings.Denied.header,
                                                description: NSAttributedString(string: Strings.Denied.body),
                                                primaryButtonTitle: Strings.Generic.ok,
                                                secondaryButtonTitle: Strings.Generic.learnMore)
        coordinator.perform(action: .registrationDeniedWarning(content, article: .denied))
    }

    func showRegistrationAnalysis() {
        let subtitle = Strings.Registration.Pending.subtitle
        let description = subtitle.attributedStringWith(normalFont: Typography.bodyPrimary().font(),
                                                        highlightFont: Typography.bodyPrimary(.highlight).font(),
                                                        normalColor: Colors.grayscale700.color,
                                                        highlightColor: Colors.grayscale700.color,
                                                        textAlignment: .center,
                                                        underline: false)
        let content = ApolloFeedbackViewContent(image: Assets.clock.image,
                                                title: Strings.Registration.Pending.title,
                                                description: description,
                                                primaryButtonTitle: Strings.Generic.ok,
                                                secondaryButtonTitle: "")
        coordinator.perform(action: .warning(content))
    }

    func showUnavailableLimit() {
        let content = ApolloFeedbackViewContent(image: Resources.Illustrations.iluWarning.image,
                                                title: Strings.UnavailableCredit.Warning.title,
                                                description: NSAttributedString(string: Strings.UnavailableCredit.Warning.subtitle),
                                                primaryButtonTitle: Strings.UnavailableCredit.Warning.waitlistButtonTitle,
                                                secondaryButtonTitle: Strings.Generic.notNow)
        coordinator.perform(action: .unavailableLimitWarning(content) {
            self.viewController?.addUserToWaitList()
        })
    }

    func showProposalInProgress() {
        let content = ApolloFeedbackViewContent(image: Assets.confirmationSuccess.image,
                                                title: Strings.ProposalInProgress.Warning.title,
                                                description: NSAttributedString(string: Strings.ProposalInProgress.Warning.subtitle),
                                                primaryButtonTitle: Strings.Generic.ok,
                                                secondaryButtonTitle: "")
        coordinator.perform(action: .warning(content))
    }

    func showWaitListAddedSuccess() {
        let content = ApolloFeedbackViewContent(image: Resources.Illustrations.iluSuccess.image,
                                                title: Strings.WaitlistAdded.Warning.title,
                                                description: NSAttributedString(string: Strings.WaitlistAdded.Warning.subtitle),
                                                primaryButtonTitle: Strings.Generic.ok,
                                                secondaryButtonTitle: "")
        coordinator.perform(action: .warning(content))
    }
}
