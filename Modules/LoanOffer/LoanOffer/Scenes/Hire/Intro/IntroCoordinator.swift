import Core
import UI
import Foundation

enum IntroAction: Equatable {
    case reason(range: SimulationRange)
    case close
    case openFAQ(article: FAQArticle)
    case registrationReview
    case warning(_ warning: ApolloFeedbackViewContent)
    case registrationDeniedWarning(_ warning: ApolloFeedbackViewContent, article: FAQArticle)
    case unavailableLimitWarning(_ warning: ApolloFeedbackViewContent, completion: (() -> Void))
    case finish
    
    static func == (lhs: IntroAction, rhs: IntroAction) -> Bool {
        switch (lhs, rhs) {
        case
            (.close, .close),
            (.finish, .finish),
            (.registrationReview, .registrationReview):
            return true
        case let (.reason(lhsRange), .reason(rhsRange)):
            return lhsRange == rhsRange
        case let (.warning(lhsWarning), .warning(rhsWarning)):
            return lhsWarning == rhsWarning
        case let (.unavailableLimitWarning(lhsWarning, _), .unavailableLimitWarning(rhsWarning, _)):
            return lhsWarning == rhsWarning
        case let (.openFAQ(lhsArticle), .openFAQ(rhsArticle)):
            return lhsArticle == rhsArticle
        case let (.registrationDeniedWarning(lhsWarning, lhsArticle), .registrationDeniedWarning(rhsWarning, rhsArticle)):
                return lhsWarning == rhsWarning && lhsArticle == rhsArticle
        default:
            return false
        }
    }
}

protocol IntroCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: IntroAction)
}

final class IntroCoordinator: IntroCoordinating {
    weak var viewController: UIViewController?

    typealias Dependencies = HasLegacy
    private let dependencies: Dependencies
    weak var delegate: LoanHireFlowCoordinating?
    
    init(delegate: LoanHireFlowCoordinating, dependencies: Dependencies) {
        self.delegate = delegate
        self.dependencies = dependencies
    }
    
    func perform(action: IntroAction) {
        switch action {
        case let .reason(range):
            delegate?.perform(action: .reason(range))
        case let .openFAQ(article):
            dependencies.legacy.customerSupport?.show(article: article)
        case .close:
            delegate?.perform(action: .back)
        case .registrationReview:
            delegate?.perform(action: .registrationOnboarding(status: .review))
        case let .warning(warning):
            delegate?.perform(action: .warning(warning))
        case .finish:
            delegate?.perform(action: .finish)
        case let .registrationDeniedWarning(warning, article):
            showRegistrationDeniedWarning(warning, article: article)
        case let .unavailableLimitWarning(warning, completion):
            showUnavailableLimitWarning(warning, completion: completion)
        }
    }
}

extension IntroCoordinator {
    func showRegistrationDeniedWarning(_ warning: ApolloFeedbackViewContent, article: FAQArticle) {
        guard let navigationController = viewController?.navigationController else { return }
        let controller = ApolloFeedbackViewController(content: warning)
        controller.didTapPrimaryButton = { self.perform(action: .finish) }
        controller.didTapSecondaryButton = { self.perform(action: .openFAQ(article: article)) }
        controller.navigationItem.hidesBackButton = true
        navigationController.pushViewController(controller, animated: true)
    }

    func showUnavailableLimitWarning(_ warning: ApolloFeedbackViewContent, completion: @escaping (() -> Void)) {
        guard let navigationController = viewController?.navigationController else { return }
        let controller = ApolloFeedbackViewController(content: warning)
        controller.didTapPrimaryButton = {
            completion()
            navigationController.popViewController(animated: true)
        }
        controller.didTapSecondaryButton = { self.perform(action: .finish) }
        controller.navigationItem.hidesBackButton = true
        navigationController.pushViewController(controller, animated: true)
    }
}
