import Foundation
import Core

typealias SimulationRangeCompletion = (Result<SimulationRange, ApiError>) -> Void

protocol IntroServicing {
    func fetchSimulationRange(completion: @escaping SimulationRangeCompletion)
    func addToWaitList(completion: @escaping (Result<NoContent, ApiError>) -> Void)
}

final class IntroService: IntroServicing {
    typealias Dependency = HasMainQueue & HasLegacy
    let dependencies: Dependency
    
    init(dependencies: Dependency) {
        self.dependencies = dependencies
    }
    
    func fetchSimulationRange(completion: @escaping SimulationRangeCompletion) {
        let api = Api<SimulationRange>(endpoint: LoanHireServiceEndpoint.limits)
        api.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }

    func addToWaitList(completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        guard let consumerId = dependencies.legacy.consumer?.consumerId else { return }
        let api = Api<NoContent>(endpoint: LoanHireServiceEndpoint.addToWaitList(consumerId: consumerId))
        api.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
