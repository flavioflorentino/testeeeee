import SafariServices
import UI

public enum HireAction: Equatable {
    case intro(origin: OriginType)
    case reason(_ range: SimulationRange)
    case simulation(_ reason: LoanReason)
    case terms(contract: ContractResponse)
    case confirmation(contractId: String)
    case registrationOnboarding(status: RegistrationStatus)
    case finish
    case back
    case present(_ controller: UIViewController)
    case push(_ controller: UIViewController)
    case warning(_ warning: ApolloFeedbackViewContent)
    
    public static func == (lhs: HireAction, rhs: HireAction) -> Bool {
        switch (lhs, rhs) {
        case
            (.simulation, .simulation),
            (.confirmation, .confirmation),
            (.finish, .finish),
            (.back, .back),
            (.warning, .warning):
            return true
        case let (.reason(lhsContract), .reason(rhsContract)):
            return lhsContract == rhsContract
        case let (.intro(lhsContract), .intro(rhsContract)):
            return lhsContract == rhsContract
        case let (.terms(lhsContract), .terms(rhsContract)):
            return lhsContract == rhsContract
        case let (.push(lhsController), .push(rhsController)):
            return lhsController == rhsController
        case let (.present(lhsController), .present(rhsController)):
            return lhsController == rhsController
        case let (.registrationOnboarding(lhsStatus), .registrationOnboarding(rhsStatus)):
            return lhsStatus == rhsStatus
        default:
            return false
        }
    }
}

protocol LoanHireFlowCoordinating: Coordinating {
    func perform(action: HireAction)
}

final class LoanHireFlowCoordinator: LoanHireFlowCoordinating {
    var childViewController: [UIViewController] = []
    var viewController: UIViewController?
    var navigationController: UINavigationController
    private weak var delegate: LoanOfferCoordinating?

    private var range: SimulationRange?
    private var reason: LoanReason = .other
    
    init(from navigationController: UINavigationController,
         with delegate: LoanOfferCoordinating) {
        self.delegate = delegate
        self.navigationController = navigationController
    }

    func perform(action: HireAction) {
        switch action {
        case let .push(controller):
            navigationController.pushViewController(controller, animated: true)
        case let .present(controller):
            navigationController.present(controller, animated: true)
        case .back:
            navigationController.popViewController(animated: true)
        case .finish:
            delegate?.finish()
        case let .registrationOnboarding(status):
            delegate?.start(flow: .registration(status: status))
        case let .warning(warning):
            delegate?.start(flow: .warning(warning))
        default:
            handleFlow(action)
        }
    }
    
    private func handleFlow(_ action: HireAction) {
        let controller: UIViewController
        
        switch action {
        case let .intro(origin):
            controller = IntroFactory.make(with: self, from: origin)
        case let .reason(range):
            self.range = range
            controller = ReasonFactory.make(with: self, range: range)
        case let .simulation(reason):
            self.reason = reason
            controller = SimulationFactory.make(with: self,
                                                range: range)
        case let .terms(contract):
            controller = TermsFactory.make(with: self,
                                           andContract: contract,
                                           withRegistrationStatus: range?.registrationStatus)
        case let .confirmation(contractId):
            controller = ConfirmationFactory.make(with: self, contractId: contractId, reason: reason)
        default:
            return
        }
        
        navigationController.pushViewController(controller, animated: true)
    }
}
