import Foundation
import AnalyticsModule

protocol PaymentDateDelegate: AnyObject {
    func didSelect(date: Date)
}

protocol PaymentDateInteracting: AnyObject {
    var selectedDate: Date { get }
    var availableDates: [Date] { get }
    func setupAnalytics()
    func setupViews()
    func fetchAvailableDates()
    func selectedDateChanged(date: Date)
    func isAvailable(date: Date) -> Bool
    func close()
}

final class PaymentDateInteractor {
    private let service: PaymentDateServicing
    private let presenter: PaymentDatePresenting
    
    var selectedDate: Date
    var numberOfMonths: Int?
    var availableDates: [Date] = [] {
        didSet {
            setNumberOfMonths()
        }
    }
    var isInitialDateChanged = false
    
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    private let daysToDueDate: Int?
    private let origin: OriginType

    init(selectedDate: Date,
         daysToDueDate: Int?,
         service: PaymentDateServicing,
         presenter: PaymentDatePresenting,
         origin: OriginType,
         dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.selectedDate = selectedDate
        self.origin = origin
        self.dependencies = dependencies
        self.daysToDueDate = daysToDueDate
    }
}

extension PaymentDateInteractor: PaymentDateInteracting {
    func setupAnalytics() {
        dependencies.analytics.log(LoanEvent.didEnter(to: .paymentDate, from: origin))
    }
    
    func setupViews() {
        presenter.presentDescription(with: daysToDueDate)
    }

    func fetchAvailableDates() {
        presenter.startLoading()

        service.getValidDates(completion: { [weak self] response in
            self?.presenter.stopLoading()

            guard case let .success(response) = response else {
                self?.presenter.presentError()
                return
            }

            self?.availableDates = response.validDatesList
            self?.presenter.presentCalendar(selectedDate: self?.selectedDate ?? Date(), numberOfMonths: self?.numberOfMonths)
        })
    }
    
    func selectedDateChanged(date: Date) {
        logDateChangedIfNeeded()
        selectedDate = date
    }
    
    func isAvailable(date: Date) -> Bool {
        availableDates.contains(date)
    }
    
    func setNumberOfMonths() {
        guard let lastDate = availableDates.last else { return }
        
        let lastDateComponents = Calendar.current.dateComponents([.year, .month, .day], from: lastDate)
        let todayComponents = Calendar.current.dateComponents([.year, .month, .day], from: Date())
        let months = Calendar.current.dateComponents([.month, .year], from: todayComponents, to: lastDateComponents).month
        
        if let months = months {
            numberOfMonths = 1 + months
        }
    }
    
    func close() {
        presenter.close(selectedDate: selectedDate)
    }
    
    private func logDateChangedIfNeeded() {
        if !isInitialDateChanged {
            dependencies.analytics.log(LoanHireEvent.didSimulationDateChanged)
            isInitialDateChanged = true
        }
    }
}
