import Core
import Foundation

protocol PaymentDateServicing {
    func getValidDates(completion: @escaping (Result<PaymentDateResponse, ApiError>) -> Void)
}

final class PaymentDateService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension PaymentDateService: PaymentDateServicing {
    func getValidDates(completion: @escaping (Result<PaymentDateResponse, ApiError>) -> Void) {
        let decoder = JSONDecoder(.useDefaultKeys)
        decoder.dateDecodingStrategy = .formatted(Date.fifthFormatter)
        
        let api = Api<PaymentDateResponse>(endpoint: LoanHireServiceEndpoint.calendars)
        api.shouldUseDefaultDateFormatter = false
        
        api.execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
