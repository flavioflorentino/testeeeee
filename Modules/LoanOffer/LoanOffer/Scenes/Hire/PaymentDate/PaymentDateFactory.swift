import UIKit

enum PaymentDateFactory {
    static func make(selectedDate: Date, daysToDueDate: Int?, origin: OriginType, delegate: PaymentDateDelegate) -> PaymentDateViewController {
        let dependencies = LoanDependencyContainer()
        let service: PaymentDateServicing = PaymentDateService(dependencies: dependencies)
        let coordinator: PaymentDateCoordinating = PaymentDateCoordinator(delegate: delegate)
        let presenter: PaymentDatePresenting = PaymentDatePresenter(coordinator: coordinator)
        let interactor = PaymentDateInteractor(selectedDate: selectedDate,
                                               daysToDueDate: daysToDueDate,
                                               service: service,
                                               presenter: presenter,
                                               origin: origin,
                                               dependencies: dependencies)
        let viewController = PaymentDateViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
