import UIKit

enum PaymentDateAction: Equatable {
    case close(selectedDate: Date)
}

protocol PaymentDateCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: PaymentDateAction)
}

final class PaymentDateCoordinator {
    weak var viewController: UIViewController?
    private weak var delegate: PaymentDateDelegate?
    
    init(delegate: PaymentDateDelegate) {
        self.delegate = delegate
    }
}

extension PaymentDateCoordinator: PaymentDateCoordinating {
    func perform(action: PaymentDateAction) {
        if case .close(let selectedDate) = action {
            delegate?.didSelect(date: selectedDate)
            if let navigation = viewController?.navigationController {
                navigation.popViewController(animated: true)
            } else {
                viewController?.dismiss(animated: true)
            }
        }
    }
}
