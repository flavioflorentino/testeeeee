import Foundation

protocol PaymentDatePresenting: AnyObject {
    var viewController: PaymentDateDisplaying? { get set }
    func startLoading()
    func stopLoading()
    func presentCalendar(selectedDate: Date, numberOfMonths: Int?)
    func presentDescription(with dueDays: Int?)
    func presentError()
    func close(selectedDate: Date)
}

final class PaymentDatePresenter {
    private let coordinator: PaymentDateCoordinating
    weak var viewController: PaymentDateDisplaying?

    init(coordinator: PaymentDateCoordinating) {
        self.coordinator = coordinator
    }
}

extension PaymentDatePresenter: PaymentDatePresenting {
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
    
    func presentCalendar(selectedDate: Date, numberOfMonths: Int?) {
        viewController?.displayCalendar(selectedDate: selectedDate, numberOfMonths: numberOfMonths)
    }
    
    func presentDescription(with dueDays: Int?) {
        guard let dueDays = dueDays else { return }
        let text = Strings.Simulation.InstallmentDueDate.description(dueDays)
        let accessibilityText = Strings.Simulation.InstallmentDueDate.accessibilityDescription(dueDays)
        
        viewController?.displayDescription(text: text, acessibilityText: accessibilityText)
    }
    
    func presentError() {
        viewController?.displayError()
    }
    
    func close(selectedDate: Date) {
        coordinator.perform(action: .close(selectedDate: selectedDate))
    }
}
