import UI
import UIKit
import AssetsKit

protocol PaymentDateDisplaying: AnyObject {
    func startLoading()
    func stopLoading()
    func displayCalendar(selectedDate: Date, numberOfMonths: Int?)
    func displayDescription(text: String, acessibilityText: String)
    func displayError()
}

final class PaymentDateViewController: ViewController<PaymentDateInteracting, UIView>, LoadingViewProtocol {
    fileprivate enum Layout { }
    
    lazy var loadingView: LoadingView = {
        let loading = LoadingView()
        loading.text = Strings.Generic.Loading.text
        return loading
    }()
    
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return dateFormatter
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.Simulation.InstallmentDueDate.title
        label
            .labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, .black())
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.Simulation.InstallmentDueDate.defaultDescription
        label.accessibilityLabel = Strings.Simulation.InstallmentDueDate.defaultAccessibilityDescription
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, Colors.grayscale700.color)
        return label
    }()
    
    private lazy var calendarView: VerticalCalendarView = {
        let calendarView = VerticalCalendarFactory.make(selectedDate: interactor.selectedDate, delegate: self, validator: self)
        calendarView.isHidden = true
        return calendarView
    }()
    
    private let separatorView = SeparatorView()
    
    private lazy var chooseDateButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.Simulation.InstallmentDueDate.chooseDate, for: .normal)
        button.addTarget(self, action: #selector(close), for: .touchUpInside)
        button.buttonStyle(PrimaryButtonStyle())
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.setupViews()
        interactor.setupAnalytics()
        interactor.fetchAvailableDates()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
        view.addSubview(calendarView)
        view.addSubview(separatorView)
        view.addSubview(chooseDateButton)
    }
    
    override func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        calendarView.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(separatorView.snp.top).offset(-Spacing.base03)
        }
        
        separatorView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(chooseDateButton.snp.top).offset(-Spacing.base02)
        }
        
        chooseDateButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).offset(-Spacing.base03)
        }
    }
    
    override func configureViews() {
        let backButton = UIBarButtonItem()
        backButton.title = String()
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        }
        
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }
}

@objc
private extension PaymentDateViewController {
    func close() {
        interactor.close()
    }
}

extension PaymentDateViewController: VerticalCalendarValidatorDelegate {
    func isAvailable(date: Date) -> Bool {
        interactor.isAvailable(date: date)
    }
}

extension PaymentDateViewController: VerticalCalendarDelegate {
    func didSelectDate(date: Date) {
        interactor.selectedDateChanged(date: date)
    }
}

extension PaymentDateViewController: PaymentDateDisplaying {
    func displayCalendar(selectedDate: Date, numberOfMonths: Int?) {
        calendarView.loadData(selectedDate: selectedDate, numberOfMonths: numberOfMonths)
        calendarView.isHidden = false
    }
    
    func displayDescription(text: String, acessibilityText: String) {
        descriptionLabel.text = text
        descriptionLabel.accessibilityLabel = acessibilityText
    }
    
    func displayError() {
        let primaryButtonAction = ApolloAlertAction(title: Strings.Generic.tryAgain) {
            self.interactor.fetchAvailableDates()
        }
        
        let linkButtonAction = ApolloAlertAction(title: Strings.Generic.notNow) {
            self.interactor.close()
        }
        
        showApolloAlert(type: .sticker(style: .error),
                        title: Strings.Generic.title,
                        subtitle: Strings.Generic.message,
                        primaryButtonAction: primaryButtonAction,
                        linkButtonAction: linkButtonAction)
    }
    
    func startLoading() {
        startLoadingView()
    }
    
    func stopLoading() {
        stopLoadingView()
    }
}
