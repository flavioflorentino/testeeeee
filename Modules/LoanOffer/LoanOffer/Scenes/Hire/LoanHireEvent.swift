import AnalyticsModule

public enum LoanHireEvent: AnalyticsKeyProtocol, Equatable {
    case didChangeSimulationValue
    case didChangeSimulationInstallments
    case didSimulate(
            requestedValue: String,
            finalValue: String,
            installment: SimulationInstallmentsResponse,
            selectedDueDate: Date?,
            daysToFirstInstallment: String?
         )
    case didSimulationDateChanged
    case didCheckFeesInfo
    case didAcceptTerms(accepted: Bool)
    case didAuthenticate(status: LoanAuthenticateStatus)
    case reason(LoanReason)
    case unavailableLimit(origin: OriginType, reason: UnavailabilityReason)
    case didRequestLoanNotification
    
    private var name: String {
        switch self {
        case .didChangeSimulationValue:
            return "Lending Simulation Value"
        case .didChangeSimulationInstallments:
            return "Lending Simulation Installment"
        case .didSimulate:
            return "Lending Simulation Results"
        case .didSimulationDateChanged:
            return "Lending Simulation Date"
        case .didCheckFeesInfo:
            return "Lending Simulation Interest View"
        case .didAcceptTerms:
            return "Lending Contract Accepted"
        case .didAuthenticate:
            return "Lending Authentication operation"
        case .reason:
            return "Selected Lending Reason"
        case .unavailableLimit:
            return "User Without Offer Accessed"
        case .didRequestLoanNotification:
            return "Requested Loan Notification"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .reason(reason):
            return ["reason": reason.description]
        case let .didSimulate(requestedValue, finalValue, installment, selectedDueDate, daysToFirstInstallment):
            var params = [
                "requested_value": requestedValue,
                "final_value": finalValue,
                "installment": "\(installment.installmentNumber)",
                "installment_value": installment.installmentValue,
                "monthly_interest_rate": installment.monthlyInterestPercent,
                "monthly_CET": installment.monthlyCETPercent
            ]
            
            if let selectedDueDate = selectedDueDate, let daysToFirstInstallment = daysToFirstInstallment {
                params["first_installment_date"] = Date.forthFormatter.string(from: selectedDueDate)
                params["days_to_first_installment"] = daysToFirstInstallment
            }
            
            return params
        case let .didAcceptTerms(accepted):
            return ["accepted": accepted]
        case let .didAuthenticate(status):
            return ["operation": status.rawValue]
        case let .unavailableLimit(origin, reason):
            return [
                "origin": origin.value,
                "reason": reason.rawValue
            ]
        case .didRequestLoanNotification:
            return ["reason": "Limit"]
        default:
            return [:]
        }
    }
    
    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.mixPanel])
    }
}
