enum LoanOfferDeeplinkType {
    private static let personalCredit = "personal-credit"

    case offer(origin: String)
    case registration

    init?(url: URL) {
        guard LoanOfferDeeplinkType.personalCredit == url.pathComponents.first(where: { $0 != "/" }) else {
            return nil
        }

        var params = url.pathComponents.filter { $0 != "/" && $0 != LoanOfferDeeplinkType.personalCredit }

        guard let path = params.first else {
            return nil
        }
        params.removeFirst()

        switch path {
        case "offer":
            let origin = params.isEmpty ? "feed" : params.joined()
            self = .offer(origin: origin)

        case "registration":
            self = .registration

        default:
            return nil
        }
    }
}

extension LoanOfferDeeplinkType: Equatable {
    public static func == (lhs: LoanOfferDeeplinkType, rhs: LoanOfferDeeplinkType) -> Bool {
        switch (lhs, rhs) {
        case let (.offer(lhsOrigin), .offer(rhsOrigin)):
            return lhsOrigin == rhsOrigin
        default:
            return lhs == rhs
        }
    }
}
