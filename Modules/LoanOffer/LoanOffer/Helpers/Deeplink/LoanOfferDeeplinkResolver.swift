import Foundation
import Core

public struct LoanOfferDeeplinkResolver: DeeplinkResolver {
    public init() { }

    public func canHandle(url: URL, isAuthenticated: Bool) -> DeeplinkResolverResult {
        guard LoanOfferDeeplinkType(url: url) != nil else {
            return .notHandleable
        }
        guard isAuthenticated else {
            return .onlyWithAuth
        }
        return .handleable
    }

    public func open(url: URL, isAuthenticated: Bool) -> Bool {
        guard
            let deeplinkType = LoanOfferDeeplinkType(url: url),
            canHandle(url: url, isAuthenticated: isAuthenticated) == .handleable
        else {
            return false
        }
        return open(deeplink: deeplinkType)
    }
}

private extension LoanOfferDeeplinkResolver {
    func open(deeplink: LoanOfferDeeplinkType) -> Bool {
        guard let navigationController = LoanOfferLegacySetup.shared.navigationInstance?.getCurrentNavigation() else { return false }
        switch deeplink {
        case let .offer(origin):
            openHireFlow(navigationController: navigationController, origin: origin)
        case .registration:
            openRegistrationFlow(navigationController: navigationController)
        }
        return true
    }
}

private extension LoanOfferDeeplinkResolver {
    func openHireFlow(navigationController: UINavigationController, origin: String) {
        let coordinator = LoanOfferCoordinator(with: navigationController, from: .deepLink(origin))
        LoanOfferLegacySetup.shared.navigationInstance?.updateCurrentCoordinating(coordinator)
        coordinator.start(flow: .hire) {
            LoanOfferLegacySetup.shared.navigationInstance?.updateCurrentCoordinating(nil)
        }
    }

    func openRegistrationFlow(navigationController: UINavigationController) {
        let coordinator = LoanOfferCoordinator(with: navigationController, from: .notification)
        LoanOfferLegacySetup.shared.navigationInstance?.updateCurrentCoordinating(coordinator)
        coordinator.start(flow: .registration(status: .unknowed)) {
            LoanOfferLegacySetup.shared.navigationInstance?.updateCurrentCoordinating(nil)
        }
    }
}
