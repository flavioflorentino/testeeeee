import AnalyticsModule

public enum LoanEvent: AnalyticsKeyProtocol, Equatable {
    case didSeeOffer(origin: OriginType, bankId: String)
    case didEnter(to: OriginType, from: OriginType, params: [String: String] = [:])
    case didReceiveError(error: LoanErrorStatus)
    
    private var name: String {
        switch self {
        case .didSeeOffer:
            return "Saw Credit Offer"
        case let .didEnter(to, _, _):
            return "Lending \(to.name) Accessed"
        case .didReceiveError:
            return "Lending Error"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .didSeeOffer(origin, bankId):
            return [
                "origin": origin.name,
                "partner": bankId
            ]
        case let .didEnter(_, from, params):
            var parameters = params
            parameters["origin"] = from.value
            return parameters
        case let .didReceiveError(action):
            return ["action": action.rawValue]
        }
    }
    
    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.mixPanel])
    }
}
