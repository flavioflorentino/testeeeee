public enum OriginType: Equatable {
    case offer
    case onboarding
    case reason
    case simulation
    case termsReview
    case termsHire
    case doubleConfirmation
    case contractSummary
    case contractView
    case authenticate
    case approved
    case denied
    case registrationOnboarding
    case registrationOnboardingStart
    case registrationOnboardingIncomplete
    case registrationOnboardingReview
    case hub
    case notification
    case deepLink(_ origin: String)
    case simulationDate
    case paymentDate
    
    var name: String {
        switch self {
        case .offer:
            return "Saw Credit Offer"
        case .onboarding:
            return "Onboarding"
        case .reason:
            return "Reason"
        case .simulation:
            return "Simulation"
        case .termsHire:
            return "Contract Confirmation"
        case .termsReview:
            return "Simulation Conditions"
        case .contractSummary:
            return "Contract Summary"
        case .contractView:
            return "Contract View"
        case .authenticate:
            return "Authentication"
        case .doubleConfirmation:
            return "Double Confirmation"
        case .approved:
            return "Approved"
        case .denied:
            return "Register Denied"
        case
            .registrationOnboarding,
            .registrationOnboardingStart:
            return "Onboarding Register"
        case .registrationOnboardingIncomplete:
            return "Continue Register"
        case .registrationOnboardingReview:
            return "Update Register"
        case .deepLink:
            return "Deeplink"
        case .simulationDate:
            return "Simulation Date"
        case .paymentDate:
            return "Date Pick"
        case .hub:
            return "Hub"
        case .notification:
            return "Notification"
        }
    }
    
    var value: String {
        switch self {
        case
            .offer,
            .registrationOnboardingReview:
            return "Adicionar Dinheiro"
        case .onboarding:
            return "Onboarding CP"
        case .reason:
            return "Motivo"
        case .simulation:
            return "Simulação"
        case .termsHire:
            return "Confirmação Contrato"
        case .doubleConfirmation:
            return "Dupla Confirmação"
        case .registrationOnboarding:
            return "Onboarding Cadastro"
        case
            .termsReview,
            .registrationOnboardingStart,
            .registrationOnboardingIncomplete:
            return "Condições da simulação"
        case let .deepLink(origin):
            return origin
        case .hub:
            return "Hub"
        default:
            return ""
        }
    }
}
