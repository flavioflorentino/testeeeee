protocol HasSafariView {
    var safari: SafariViewDependency { get }
}

protocol HasLegacy {
    var legacy: LoanOfferLegacySetupContract { get }
}
