import Core
import UI
import UIKit

public protocol LoanOfferLegacySetupContract: AnyObject {
    var consumer: LoanOfferConsumerContract? { get set }
    var faq: LoanOfferFAQContract? { get set }
    var creditRegistration: LoanCreditRegistrationContract? { get set }
    var auth: AuthenticationContract? { get set }
    var customerSupport: LoanOfferCustomerSupportContract? { get set }
}

public final class LoanOfferLegacySetup: LoanOfferLegacySetupContract {
    public static let shared = LoanOfferLegacySetup()
    
    public var consumer: LoanOfferConsumerContract?
    public var faq: LoanOfferFAQContract?
    public var creditRegistration: LoanCreditRegistrationContract?
    public var auth: AuthenticationContract?
    public var customerSupport: LoanOfferCustomerSupportContract?
    public var navigationInstance: NavigationContract?

    private init() {}
    
    public func inject(instance: Any) {
        switch instance {
        case let instance as LoanOfferConsumerContract:
            consumer = instance
        case let instance as LoanOfferFAQContract:
            faq = instance
        case let instance as LoanCreditRegistrationContract:
            creditRegistration = instance
        case let instance as AuthenticationContract:
            auth = instance
        case let instance as LoanOfferCustomerSupportContract:
            customerSupport = instance
        case let instance as NavigationContract:
            navigationInstance = instance
        default:
            assertionFailure("🚨\nAttempted to inject unexpected depedency: \(instance)\n")
        }
    }
}
