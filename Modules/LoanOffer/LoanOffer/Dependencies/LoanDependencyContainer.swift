import AnalyticsModule
import Core
import FeatureFlag
import Foundation

typealias LoanDependencies =
    HasMainQueue &
    HasAnalytics &
    HasFeatureManager &
    HasSafariView &
    HasLegacy

final class LoanDependencyContainer: LoanDependencies {
    lazy var mainQueue = DispatchQueue.main
    lazy var analytics: AnalyticsProtocol = Analytics.shared
    lazy var safari: SafariViewDependency = SafariViewManager.shared
    lazy var legacy: LoanOfferLegacySetupContract = LoanOfferLegacySetup.shared
    lazy var featureManager: FeatureManagerContract = FeatureManager.shared
}
