import SafariServices
import Foundation

enum SafariError: Error, Equatable {
    case nilUrl
    case emptyUrl
    case couldNotOpen(_ url: String)
    
    var localizedDescription: String {
        switch self {
        case .nilUrl:
            return "It was not possible to open a nil URL"
        case .emptyUrl:
            return "It was not possible to open an empty URL"
        case let .couldNotOpen(url):
            return "It was not possible to open current URL: \(url)"
        }
    }
}

public protocol URLOpener {
    func canOpenURL(_ url: URL) -> Bool
}

extension UIApplication: URLOpener {}
    
protocol SafariViewDependency: AnyObject {
    func controller(for urlPath: String?, urlOpener: URLOpener) -> Result<SFSafariViewController, SafariError>
}

extension SafariViewDependency {
    func controller(for urlPath: String?) -> Result<SFSafariViewController, SafariError> {
        controller(for: urlPath, urlOpener: UIApplication.shared)
    }
}

final class SafariViewManager: SafariViewDependency {
    static let shared: SafariViewDependency = SafariViewManager()
    
    private init() {}
    
    func controller(for urlPath: String?, urlOpener: URLOpener) -> Result<SFSafariViewController, SafariError> {
        guard let path = urlPath else {
            return .failure(.nilUrl)
        }
        
        guard
            path.isNotEmpty,
            let url = URL(string: path)
            else {
                return .failure(.emptyUrl)
        }
        
        guard urlOpener.canOpenURL(url) else {
            return .failure(.couldNotOpen(path))
        }
        
        return .success(SFSafariViewController(url: url))
    }
}
