import XCTest
@testable import LoanOffer

final private class ReasonControllerSpy: ReasonDisplay {
    private(set) var displayReasonsCallCount = 0
    private(set) var reasons: [LoanReason]?

    func displayReasons(_ reasons: [LoanReason]) {
        displayReasonsCallCount += 1
        self.reasons = reasons
    }
}

final private class ReasonCoordinatorSpy: ReasonCoordinating {
    var delegate: LoanHireFlowCoordinating?
    private(set) var nextStepCallCount = 0
    private(set) var contract: ContractResponse?
    
    init(with delegate: LoanHireFlowCoordinating) {
        self.delegate = delegate
    }
    
    func reasonSelected(reason: LoanReason) {
        nextStepCallCount += 1
        delegate?.perform(action: .simulation(reason))
    }
}

final class ReasonPresenterTests: XCTestCase {
    private let delegateSpy = LoanHireFlowCoordinatingSpy()
    private lazy var coordinatorSpy = ReasonCoordinatorSpy(with: delegateSpy)
    private let controllerSpy = ReasonControllerSpy()
    
    private lazy var sut: ReasonPresenter = {
        let sut = ReasonPresenter(coordinator: coordinatorSpy)
        sut.viewController = controllerSpy
        return sut
    }()
    
    func testPresentReasons_WhenEnterReasonsView_ShouldDisplayRandomReasonsListWithOtherReasonAtLast() {
        sut.presentReasons()
        
        XCTAssertEqual(controllerSpy.displayReasonsCallCount, 1)
        XCTAssertEqual(sut.reasons.last, .other)
    }
    
    func testNextStep_ShouldProceedToSimulation() {
        sut.reasonSelected(reason: .debt)
        
        XCTAssertNotNil(coordinatorSpy.delegate)
        XCTAssertEqual(coordinatorSpy.nextStepCallCount, 1)
        XCTAssertEqual(delegateSpy.performedAction, .simulation(.debt))
    }
}
