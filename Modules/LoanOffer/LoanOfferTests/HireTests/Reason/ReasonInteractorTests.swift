import AnalyticsModule
import XCTest
@testable import LoanOffer
@testable import Core

final private class ReasonPresenterSpy: ReasonPresenting {
    var viewController: ReasonDisplay?
    private(set) var presentReasonsCallCount = 0
    private(set) var reasonSelectedCallCount = 0

    func presentReasons() {
        presentReasonsCallCount += 1
    }
    
    func reasonSelected(reason: LoanReason) {
        reasonSelectedCallCount += 1
    }
}

final class ReasonInteractorTests: XCTestCase {
    private let presenterSpy = ReasonPresenterSpy()
    private let dependencies = LoanDependencyContainerMock(AnalyticsSpy())
    private let range = SimulationRange(minimumCredit: "100", maximumCredit: "200", bankId: "", registrationStatus: .approved, daysToDueDate: 10)
    private lazy var sut = ReasonInteractor(presenter: presenterSpy, from: .onboarding, dependencies: dependencies, range: range)
    
    func testSetupReasons_ShouldPresentReasons() {
        sut.setupReasons()
        
        XCTAssertEqual(presenterSpy.presentReasonsCallCount, 1)
    }
    
    func testProceedWithReason_ShouldGoToNextStep() {
        sut.proceed(with: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(presenterSpy.reasonSelectedCallCount, 1)
    }
    
    func testTrackSelectedReason_ShouldGoToNextStep() throws {
        let expectedReason = LoanReason.debt
        let expectedEvent = LoanHireEvent.reason(expectedReason)
        
        sut.proceed(with: IndexPath(row: 4, section: 0))
                    
        try assertReasons(with: expectedEvent, expectedReason: expectedReason)
    }
    
    func testTrackEnterView_WhenComesFromOnboarding_ShouldLogEventDidEnterFromOnboarding() throws {
        let expectedEvent = LoanEvent.didEnter(to: .reason, from: .onboarding, params: ["Flow" : range.analyticsParamFlow()])
        
        sut.setupReasons()
        
        XCTAssertEqual(presenterSpy.presentReasonsCallCount, 1)
        try assertOrigins(with: expectedEvent, originType: .onboarding)
    }
    
    func testTrackEnterView_WhenComesFromSimulation_ShouldLogEventDidEnterFromSimulation() throws {
        let expectedEvent = LoanEvent.didEnter(to: .reason, from: .simulation, params: ["Flow" : range.analyticsParamFlow()])
        sut.proceed(with: IndexPath(row: 0, section: 0))

        sut.setupReasons()
        
        XCTAssertEqual(presenterSpy.presentReasonsCallCount, 1)
        try assertOrigins(with: expectedEvent, originType: .simulation)
    }
    
    private func assertOrigins(with expectedEvent: LoanEvent, originType: OriginType) throws {
        let analytics = try XCTUnwrap(dependencies.analytics as? AnalyticsSpy)
        let origin = try XCTUnwrap(analytics.value(forKey: "origin"))

        XCTAssertTrue(analytics.equals(to: expectedEvent.event()))
        XCTAssertEqual(origin, originType.value)
    }

    private func assertReasons(with expectedEvent: LoanHireEvent, expectedReason: LoanReason) throws {
        let analytics = try XCTUnwrap(dependencies.analytics as? AnalyticsSpy)
        let reason = try XCTUnwrap(analytics.value(forKey: "reason"))

        XCTAssertTrue(analytics.equals(to: expectedEvent.event()))
        XCTAssertEqual(reason, expectedReason.description)
    }
}
