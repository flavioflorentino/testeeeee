import XCTest
@testable import LoanOffer

final class ReasonCoordinatorTests: XCTestCase {
    private lazy var delegateSpy = LoanHireFlowCoordinatingSpy()
    private lazy var sut = ReasonCoordinator(with: delegateSpy)
    
    func testDidNextStep_ShouldGoToSimulation() {
        sut.reasonSelected(reason: .debt)
        
        XCTAssertEqual(delegateSpy.performedAction, .simulation(.debt))
        XCTAssertEqual(delegateSpy.performActionCallCount, 1)
    }
}
