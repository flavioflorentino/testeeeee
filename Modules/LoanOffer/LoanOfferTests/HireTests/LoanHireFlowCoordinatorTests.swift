import XCTest
@testable import LoanOffer

final class LoanHireFlowCoordinatorTests: XCTestCase {
    private lazy var navigationSpy = UINavigationControllerSpy()
    private lazy var delegateSpy = LoanOfferCoordinatingSpy()
    private lazy var sut = LoanHireFlowCoordinator(from: navigationSpy,
                                                   with: delegateSpy)
    
    func testPerformAction_WhenActionIsPush_ShouldPushViewController() {
        sut.perform(action: .push(UIViewController()))
        
        XCTAssertEqual(navigationSpy.pushCallCount, 1)
        XCTAssertNotNil(navigationSpy.pushedViewController)
    }
    
    func testPerformAction_WhenActionIsPresent_ShouldPresentViewController() {
        sut.perform(action: .present(UIViewController()))
        
        XCTAssertEqual(navigationSpy.presentCallCount, 1)
        XCTAssertNotNil(navigationSpy.viewControllerToPresent)
    }
    
    func testPerformAction_WhenActionIsBack_ShouldPopViewController() {
        sut.perform(action: .back)
        
        XCTAssertEqual(navigationSpy.popCallCount, 1)
    }
    
    func testPerformAction_WhenActionIsFinish_ShouldFinishHireFlow() {
        sut.perform(action: .finish)
        
        XCTAssertEqual(delegateSpy.finishHireFlowCallCount, 1)
    }
    
    func testPerformAction_WhenActionIsIntro_ShouldStartIntroView() {
        sut.perform(action: .intro(origin: .offer))
        
        XCTAssertEqual(navigationSpy.pushCallCount, 1)
        XCTAssert(navigationSpy.pushedViewController is IntroDisplay)
    }
    
    func testPerformAction_WhenActionIsReason_ShouldStartReasonView() {
        let dumbSimulationRange = SimulationRange(minimumCredit: "",
                                                  maximumCredit: "",
                                                  bankId: "",
                                                  registrationStatus: .approved,
                                                  daysToDueDate: 90)
        sut.perform(action: .reason(dumbSimulationRange))
        
        XCTAssertEqual(navigationSpy.pushCallCount, 1)
        XCTAssert(navigationSpy.pushedViewController is ReasonDisplay)
    }
    
    func testPerformAction_WhenActionIsSimulation_ShouldStartSimulationView() {
        sut.perform(action: .simulation(.debt))
        
        XCTAssertEqual(navigationSpy.pushCallCount, 1)
        XCTAssert(navigationSpy.pushedViewController is SimulationDisplay)
    }
    
    func testPerformAction_WhenActionIsTerms_ShouldStartTermsView() {
        let dumbContract = ContractResponse(id: "",
                                            portion: 0,
                                            requestedValue: "",
                                            totalValue: "",
                                            installmentValue: "",
                                            monthlyCETLabel: "",
                                            monthlyCETPercent: "",
                                            monthlyIOFLabel: "",
                                            iofTotalValue: "",
                                            monthlyInterestLabel: "",
                                            monthlyInterestPercent: "",
                                            firstDueDate: "",
                                            dueDay: 0)
        sut.perform(action: .terms(contract: dumbContract))
        
        XCTAssertEqual(navigationSpy.pushCallCount, 1)
        XCTAssert(navigationSpy.pushedViewController is TermsDisplay)
    }
    
    func testPerformAction_WhenActionIsConfirmation_ShouldStartConfirmationView() {
        sut.perform(action: .confirmation(contractId: ""))
        
        XCTAssertEqual(navigationSpy.pushCallCount, 1)
        XCTAssert(navigationSpy.pushedViewController is ConfirmationDisplay)
    }
}
