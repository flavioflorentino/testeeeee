import AnalyticsModule
import SafariServices
import XCTest
@testable import LoanOffer
@testable import Core

final private class TermsPresenterSpy: TermsPresenting {
    var viewController: TermsDisplay?

    private(set) var didNextStepOfHiringCallCount = 0
    private(set) var didStartRegistrationCallCount = 0
    private(set) var showAgreementAlertCallCount = 0
    private(set) var stopLoadingCallCount = 0
    private(set) var startLoadingCallCount = 0
    private(set) var showErrorOnContractURLCallCount = 0
    private(set) var openSafariCallCount = 0
    private(set) var openFAQCallCount = 0
    private(set) var setupViewCallCount = 0
    private(set) var finishCallCount = 0
    private(set) var showTimeLimitWarningCallCount = 0
    
    private(set) var contractId: String?
    private(set) var contract: ContractResponse?
    private(set) var state: TermsState?
    private(set) var status: RegistrationStatus?

    func didNextStepOfHiring(withContractId contractId: String) {
        self.contractId = contractId
        didNextStepOfHiringCallCount += 1
    }
    
    func showAgreementAlert() {
        showAgreementAlertCallCount += 1
    }
    
    func startLoading() {
        startLoadingCallCount += 1
    }
    
    func stopLoading() {
        stopLoadingCallCount += 1
    }
    
    func showErrorOnContractURL() {
        showErrorOnContractURLCallCount += 1
    }
    
    func openFAQ() {
        openFAQCallCount += 1
    }
    
    func open(safariViewController controller: SFSafariViewController) {
        openSafariCallCount += 1
    }
    
    func didStartRegistration(with status: RegistrationStatus) {
        didStartRegistrationCallCount += 1
        self.status = status
    }
    
    func setupView(for state: TermsState, with contract: ContractResponse) {
        setupViewCallCount += 1
        self.contract = contract
        self.state = state
    }
    
    func showTimeLimitWarning() {
        showTimeLimitWarningCallCount += 1
    }
    
    func finish() {
        finishCallCount += 1
    }
}

final private class TermsServiceMock: TermsServicing {
    var response: Result<ContractPDFResponse, ApiError> = .failure(.serverError)
    
    func fetchCCB(with simulationId: String, _ completion: @escaping ContractCompletionBlock) {
        completion(response)
    }
}

final class TermsInteractorTests: XCTestCase {
    private let serviceMock = TermsServiceMock()
    private let presenterSpy = TermsPresenterSpy()
    private let contract = ContractResponse(id: "5e3da85f021ee2720d41476f",
                                            portion: 2,
                                            requestedValue: "R$2.000,00",
                                            totalValue: "R$2.180,00",
                                            installmentValue: "R$1.090,00",
                                            monthlyCETLabel: "CET",
                                            monthlyCETPercent: "64,50%",
                                            monthlyIOFLabel: "Valor total do IOF financiado",
                                            iofTotalValue: "64,50%",
                                            monthlyInterestLabel: "Taxa Nominal",
                                            monthlyInterestPercent: "4,50%",
                                            firstDueDate: "10/04/2020",
                                            dueDay: 10)
    private let analytics = AnalyticsSpy()
    private lazy var dependencies = LoanDependencyContainerMock(analytics,
                                                                SafariViewManagerMock(),
                                                                LoanOfferSetupMock(.faq))
    private lazy var sut = TermsInteractor(service: serviceMock,
                                          presenter: presenterSpy,
                                          contract: contract,
                                          dependencies: dependencies,
                                          withRegistrationStatus: .approved,
                                          from: .simulation)
    
    private func createSUT(withRegistrationStatus status: RegistrationStatus) -> TermsInteractor {
        TermsInteractor(service: serviceMock,
                       presenter: presenterSpy,
                       contract: contract,
                       dependencies: dependencies,
                       withRegistrationStatus: status,
                       from: .simulation)
    }
    
    func testViewDidLoad_WhenServiceMockIsSuccess_ShouldPresentContract() {
        sut.viewDidLoad()
        
        XCTAssertEqual(presenterSpy.setupViewCallCount, 1)
        XCTAssertEqual(presenterSpy.contract?.id, contract.id)
        XCTAssertEqual(presenterSpy.state, .hire)
    }
        
    func testAcceptTerms_WhenPassedParameter_ShouldChangeSutVariable() {
        sut.acceptTerms(true)
        XCTAssertTrue(sut.termsAccepted)
        sut.acceptTerms(false)
        XCTAssertFalse(sut.termsAccepted)
    }
    
    func testNextStep_WhenCalledByPresenter_ShouldPresentAgreementAlert() {
        let event = LoanEvent.didReceiveError(error: .confirmationWithoutAcceptance)

        sut.nextStep()
        
        XCTAssertEqual(presenterSpy.showAgreementAlertCallCount, 1)
        XCTAssertTrue(analytics.equals(to: event.event()))
    }

    func testNextStep_WhenUserCancelAuthentication_ShouldNotPresentError() {
        let sut = createSUT(withRegistrationStatus: .registrate)

        sut.nextStep()

        XCTAssertEqual(presenterSpy.didStartRegistrationCallCount, 1)
        XCTAssertEqual(presenterSpy.status, .registrate)
    }
    
    func testNextStep_WhenRegistrationNeedsToBeFinished_ShouldShowRegistrationView() {
        let sut = createSUT(withRegistrationStatus: .incomplete)
        
        sut.nextStep()

        XCTAssertEqual(presenterSpy.didStartRegistrationCallCount, 1)
        XCTAssertEqual(presenterSpy.status, .incomplete)
    }
    
    func testNextStep_WhenRegistrationIsCompleted_ShouldShowConfirmationView() {
        sut.acceptTerms(true)

        sut.nextStep()

        XCTAssertEqual(presenterSpy.didNextStepOfHiringCallCount, 1)
        XCTAssertEqual(presenterSpy.contractId, contract.id)
    }
        
    func testDidOpenCCB_WhenContractURLIsAvailable_ShouldOpenCCB() throws {
        let urlString = "https://ccb"
        let url = try XCTUnwrap(URL(string: urlString))
        serviceMock.response = .success(ContractPDFResponse(url: urlString))
        
        sut.openContract(url)
        
        XCTAssertEqual(presenterSpy.startLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.openSafariCallCount, 1)
    }
    
    func testDidOpenCCB_WhenContractURLIsNotAvailable_ShouldShowError() throws {
        let url = try XCTUnwrap(URL(string: "https://ccb"))
        sut.openContract(url)
        
        XCTAssertEqual(presenterSpy.startLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.showErrorOnContractURLCallCount, 1)
    }
    
    func testDidOpenContractSummary_WhenContractURLIsAvailable_ShouldOpenContractSummary() throws {
        let url = try XCTUnwrap(URL(string: "https://summary"))
        sut.openContract(url)
        
        XCTAssertEqual(presenterSpy.openSafariCallCount, 1)
    }
    
    func testTrack_WhenAcceptTerms_ShouldLogEventDidAcceptTerms() {
        sut.acceptTerms(true)
        
        check(event: LoanHireEvent.didAcceptTerms(accepted: true))
    }

    func testTrackEnterView_ShouldLogEventDidEnter() {
        let event = LoanEvent.didEnter(to: .termsHire, from: .simulation, params: ["Flow" : "Contratação"])
        
        sut.trackEnterView()
        
        XCTAssertTrue(analytics.equals(to: event.event()))
    }
    
    func testOpenFaq_ShouldOpenSafariView() {
        guard let faq = dependencies.legacy.faq as? FAQWrapperMock else {
            XCTFail("FAQ dependency could not be initiated")
            return
        }
        
        faq.expectedResult = FAQWebViewControllerMock()
        
        sut.openFAQ()
        
        XCTAssertEqual(presenterSpy.openFAQCallCount, 1)
    }
    
    func testNextStep_WhenReviewState_ShouldPresentNextStepOfReviewing() {
        let sut = TermsInteractor(service: serviceMock,
                                 presenter: presenterSpy,
                                 contract: contract,
                                 dependencies: dependencies,
                                 withRegistrationStatus: .registrate,
                                 from: .simulation)
        
        sut.nextStep()
        
        XCTAssertEqual(presenterSpy.didStartRegistrationCallCount, 1)
    }
    
    func testShowWarning_ShouldDisplayWarningView() throws {
        let dumbRequestError = RequestError(title: "title",
                                            message: "message",
                                            code: LoanApiError.timeLimit.rawValue)
        let urlString = "https://ccb"
        let url = try XCTUnwrap(URL(string: urlString))
        
        serviceMock.response = .failure(.badRequest(body: dumbRequestError))

        sut.openContract(url)

        XCTAssertEqual(presenterSpy.showTimeLimitWarningCallCount, 1)
    }
    
    private func check(event: LoanHireEvent) {
        XCTAssertTrue(analytics.equals(to: event.event()))
    }
}
