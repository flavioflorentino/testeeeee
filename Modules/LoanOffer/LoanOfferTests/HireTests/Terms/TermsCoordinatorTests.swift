import SafariServices
import XCTest
import UI
@testable import LoanOffer

final class TermsCoordinatorTests: XCTestCase {
    private lazy var delegateSpy = LoanHireFlowCoordinatingSpy()
    private let dependencies = LoanDependencyContainerMock(
        LoanOfferSetupMock(.deeplink)
    )
    private lazy var sut = TermsCoordinator(delegate: delegateSpy, dependencies: dependencies)
    
    func testPerformActionTerms_ShouldGoToTerms() {
        let dumbContractId = "dumb"
        sut.perform(action: .confirmation(contractId: dumbContractId))
        
        XCTAssertEqual(delegateSpy.performActionCallCount, 1)
        XCTAssertEqual(delegateSpy.performedAction, .confirmation(contractId: dumbContractId))
    }
    
    func testPerformActionOpenSafari_ShouldOpenSafariController() throws {
        let url = try XCTUnwrap(URL(string: "https://picpay.com"))
        let dumbSafariController = SFSafariViewController(url: url)
        sut.perform(action: .safari(dumbSafariController))
        
        XCTAssertEqual(delegateSpy.performActionCallCount, 1)
        XCTAssertEqual(delegateSpy.performedAction, .present(dumbSafariController))
    }
    
    func testPerformActionRegistration_ShouldGoToRegistration() {
        sut.perform(action: .registration(status: .approved))
        
        XCTAssertEqual(delegateSpy.performActionCallCount, 1)
        XCTAssertEqual(delegateSpy.performedAction, .registrationOnboarding(status: .approved))
    }
    
    func testPerformActionFinish_ShouldCallDelegate() {
        sut.perform(action: .finish)
        
        XCTAssertEqual(delegateSpy.performedAction, .finish)
    }
    
    func testPerformActionWarning_ShouldCallDelegate() {
        let dumbWarning = ApolloFeedbackViewContent(image: Assets.unavailable.image,
                                                    title: "title",
                                                    description: NSAttributedString(string: "description"),
                                                    primaryButtonTitle: "buttonTitle",
                                                    secondaryButtonTitle: "")
        sut.perform(action: .warning(dumbWarning))
        
        XCTAssertEqual(delegateSpy.performedAction, .warning(dumbWarning))
    }
}
