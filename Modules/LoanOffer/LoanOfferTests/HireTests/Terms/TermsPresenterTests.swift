import XCTest
import SafariServices
import UI
@testable import LoanOffer

final private class TermsViewControllerMock: TermsDisplay {
    private(set) var stopLoadingCallCount = 0
    private(set) var startLoadingCallCount = 0
    private(set) var displayCallCount = 0
    private(set) var displayContractCallCount = 0
    private(set) var displayAgreementAlertCallCount = 0
    private(set) var setupContractAttributesCallCount = 0
    private(set) var displayErrorForContractURLCallCount = 0
    private(set) var hideAcceptanceViewCallCount = 0
    private(set) var displayNextButtonCallCount = 0
    
    private(set) var infos: [KeyValueCell]?
    private(set) var fees: FeeViewModel?
    private(set) var attributedText: NSAttributedString?
    private(set) var contractUrl: URL?
    private(set) var hideAcceptanceViewVisibility: Bool?
    
    func setupContractAttributes(attributedText: NSAttributedString,
                                 andLinkAttributes linkAttributes: [NSAttributedString.Key: Any]) {
        self.attributedText = attributedText
        setupContractAttributesCallCount += 1
    }
    
    func displayAgreementAlert() {
        displayAgreementAlertCallCount += 1
    }
    
    func displayContract(_ infos: [KeyValueCell]) {
        self.infos = infos
        displayContractCallCount += 1
    }
    
    func display(_ fees: FeeViewModel) {
        self.fees = fees
        displayCallCount += 1
    }
    
    func startLoading() {
        startLoadingCallCount += 1
    }
    
    func stopLoading() {
        stopLoadingCallCount += 1
    }

    func displayErrorForContractURL() {
        displayErrorForContractURLCallCount += 1
    }
    
    func hideAcceptanceView(_ visibility: Bool) {
        hideAcceptanceViewCallCount += 1
        hideAcceptanceViewVisibility = visibility
    }
    
    func displayNextButton(title: String) {
        displayNextButtonCallCount += 1
    }
}

final private class TermsCoordinatorMock: TermsCoordinating {
    var viewController: UIViewController?
    
    private(set) var performActionCallCount = 0
    
    private(set) var delegate: LoanHireFlowCoordinating?
    private(set) var performedAction: TermsAction?
    
    init(with delegate: LoanHireFlowCoordinating) {
        self.delegate = delegate
    }

    func perform(action: TermsAction) {
        performActionCallCount += 1
        performedAction = action
    }
}

final class TermsPresenterTests: XCTestCase {
    private let delegateSpy = LoanHireFlowCoordinatingSpy()
    private lazy var coordinatorSpy = TermsCoordinatorMock(with: delegateSpy)
    private let controllerSpy = TermsViewControllerMock()
    
    private lazy var sut: TermsPresenter = {
        let sut = TermsPresenter(coordinator: coordinatorSpy)
        sut.viewController = controllerSpy
        return sut
    }()
    
    private let contract = ContractResponse(id: "5e3da85f021ee2720d41476f",
                                            portion: 2,
                                            requestedValue: "R$ 2.000,00",
                                            totalValue: "R$ 2.180,00",
                                            installmentValue: "R$ 1.090,00",
                                            monthlyCETLabel: "CET",
                                            monthlyCETPercent: "64,50%",
                                            monthlyIOFLabel: "Valor total do IOF financiado",
                                            iofTotalValue: "64,50%",
                                            monthlyInterestLabel: "Taxa Nominal",
                                            monthlyInterestPercent: "4,50%",
                                            firstDueDate: "10/04/2020",
                                            dueDay: 10)
    
    func testDidNextStep_WhenCalledByPresenter_ShouldCallCoordinator() {
        sut.didNextStepOfHiring(withContractId: contract.id)
        
        guard
            case let .confirmation(contractId) = coordinatorSpy.performedAction
        else {
            XCTFail("Action should be confirmation")
            return
        }
        XCTAssertEqual(contractId, contract.id)
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .confirmation(contractId: contract.id))
    }

    func testsetupView_WhenContractIsPassed_ShouldDisplayFees() {
        sut.setupView(for: .hire, with: contract)
        XCTAssertEqual(controllerSpy.displayCallCount, 1)
        XCTAssertEqual(controllerSpy.displayContractCallCount, 1)
        XCTAssertEqual(controllerSpy.setupContractAttributesCallCount, 1)
    }
    
    func testUpdateAgreementLink_WhenCalledByInteractor_ShouldSetupContractAttributes() {
        sut.setupView(for: .review, with: contract)
        
        XCTAssertEqual(controllerSpy.setupContractAttributesCallCount, 0)
    }

    func testShowAgreementAlert_WhenCalledByPresenter_ShouldViewControllerDisplayAgreement() {
        sut.showAgreementAlert()
        XCTAssertEqual(controllerSpy.displayAgreementAlertCallCount, 1)
    }
    
    func testStartLoading_WhenCalledByPresenter_ShouldViewControllerLoad() {
        sut.startLoading()
        XCTAssertEqual(controllerSpy.startLoadingCallCount, 1)
    }
    
    func testStopLoading_WhenCalledByPresenter_ShouldViewControllerStopLoad() {
        sut.stopLoading()
        XCTAssertEqual(controllerSpy.stopLoadingCallCount, 1)
    }
    
    func testOpenContract_WhenCalledByPresenter_ShouldOpenSafariViewController() throws {
        let url = try XCTUnwrap(URL(string: "https://picpay.com"))
        let safariViewController = SFSafariViewController(url: url)
        sut.open(safariViewController: safariViewController)
        
        guard
            case .safari = coordinatorSpy.performedAction
        else {
            XCTFail("Action should be safari")
            return
        }
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .safari(safariViewController))
    }
    
    func testShowErrorOnContractURL_WhenCalledByPresenter_ShouldDisplayError() {
        sut.showErrorOnContractURL()
        
        XCTAssertEqual(controllerSpy.displayErrorForContractURLCallCount, 1)
    }
    
    func testDidNextStepOfReviewing_ShouldPerformRegistrationActionInCoordinator() {
        sut.didStartRegistration(with: .approved)
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .registration(status: .approved))
    }
    
    func testShowWarning_ShouldDisplayWarningView() {
        let dumbWarning = ApolloFeedbackViewContent(image: Assets.unavailable.image,
                                                    title: Strings.Alerts.Unavailable.title,
                                                    description: NSAttributedString(string: Strings.Alerts.Unavailable.subtitle),
                                                    primaryButtonTitle: Strings.Generic.ok,
                                                    secondaryButtonTitle: "")
        sut.showTimeLimitWarning()
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .warning(dumbWarning))
    }
    
    func testFinish_ShouldFinishHireFlow() {
        sut.finish()
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .finish)
    }
}
