import AnalyticsModule
import XCTest
@testable import LoanOffer
@testable import Core

final private class ConfirmationPresenterSpy: ConfirmationPresenting {

    var viewController: ConfirmationDisplay?
    
    private(set) var showErrorCallCount = 0
    private(set) var stopLoadingCallCount = 0
    private(set) var startLoadingCallCount = 0
    private(set) var cancelCallCount = 0
    private(set) var didNextStepCallCount = 0
    private(set) var showProposalInProgressWarningCallCount = 0
    private(set) var showTimeLimitWarningCallCount = 0
    private(set) var showDeviceChangingWarningCallCount = 0

    func showError() {
        showErrorCallCount += 1
    }
    
    func startLoading() {
        startLoadingCallCount += 1
    }
    
    func stopLoading() {
        stopLoadingCallCount += 1
    }
    
    func didCancel() {
        cancelCallCount += 1
    }

    func didNextStep() {
        didNextStepCallCount += 1
    }

    func showProposalInProgressWarning() {
        showProposalInProgressWarningCallCount += 1
    }

    func showTimeLimitWarning() {
        showTimeLimitWarningCallCount += 1
    }
    
    func showDeviceChangingWarning() {
        showDeviceChangingWarningCallCount += 1
    }
}

final private class ConfirmationServiceMock: ConfirmationServicing {
    var response: Result<ConfirmSimulationResponse, ApiError> = .failure(.connectionFailure)
    
    func confirm(with simulationId: String, reason: LoanReason, pin: String, _ completion: @escaping ConfirmationCompletionBlock) {
        completion(response)
    }
}

final class ConfirmationInteractorTests: XCTestCase {
    private let serviceMock = ConfirmationServiceMock()
    private let presenterSpy = ConfirmationPresenterSpy()
    private let analytics = AnalyticsSpy()
    private lazy var dependencies = LoanDependencyContainerMock(
        analytics,
        LoanOfferSetupMock(.auth)
    )
    private lazy var sut = ConfirmationInteractor(
        service: serviceMock,
        presenter: presenterSpy,
        dependencies: dependencies,
        contractId: "1234",
        reason: .debt
    )
    
    func testAuthenticate_WhenServiceSuccessAndPresenterSuccessAndTermsAccepted_ShouldPresentLoadingAndCallNextStep() {
        guard let auth = dependencies.legacy.auth as? AuthenticationWrapperMock else {
            XCTFail("Authentication dependency could not be initiated")
            return
        }
        auth.expectedResult = .success("pin")
        serviceMock.response = .success(ConfirmSimulationResponse(contractId: "5e3da85dsfw20d41476f",
                                                                  simulationId: "5e3da85f021ee2720d41476f"))
        
        sut.nextStep()

        XCTAssertEqual(presenterSpy.startLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepCallCount, 1)
        
        let enterAuth = LoanEvent.didEnter(to: .authenticate, from: .doubleConfirmation)
        let confirmEvent = LoanHireEvent.didAuthenticate(status: .confirmed)
        let enterApproved = LoanEvent.didEnter(to: .approved, from: .doubleConfirmation)
        XCTAssertTrue(analytics.equals(to: enterAuth.event(),
                                       confirmEvent.event(),
                                       enterApproved.event()))
    }
    
    func testAuthenticate_WhenServiceFail_ShouldPresentLoadingAndShowError() {
        guard let auth = dependencies.legacy.auth as? AuthenticationWrapperMock else {
            XCTFail("Authentication dependency could not be initiated")
            return
        }
        auth.expectedResult = .success("pin")
        
        sut.nextStep()
        
        XCTAssertEqual(presenterSpy.startLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.showErrorCallCount, 1)
        
        let event = LoanEvent.didReceiveError(error: .confirmationError)
        XCTAssertTrue(analytics.equals(to: event.event()))
    }
    
    func testAuthenticate_WhenServiceAndAuthenticationFail_ShouldPresentError() {
        guard let auth = dependencies.legacy.auth as? AuthenticationWrapperMock else {
            XCTFail("Authentication dependency could not be initiated")
            return
        }
        auth.expectedResult = .failure(.nilPassword)
        sut.nextStep()

        XCTAssertEqual(presenterSpy.showErrorCallCount, 1)
        
        let event = LoanEvent.didReceiveError(error: .authentication)
        XCTAssertTrue(analytics.equals(to: event.event()))
    }
    
    func testAuthenticate_WhenUserCancelAuthentication_ShouldNotPresentError() {
        guard let auth = dependencies.legacy.auth as? AuthenticationWrapperMock else {
            XCTFail("Authentication dependency could not be initiated")
            return
        }
        auth.expectedResult = .failure(.cancelled)
        sut.nextStep()

        XCTAssertEqual(presenterSpy.showErrorCallCount, 0)
        let event = LoanHireEvent.didAuthenticate(status: .cancelled)
        XCTAssertTrue(analytics.equals(to: event.event()))
    }
    
    func testCancel_ShouldGoBackToPreviousScreen() {
        sut.cancel()
        
        XCTAssertEqual(presenterSpy.cancelCallCount, 1)
    }

    func testTrackEnterView_ShouldTrackEnterView() {
        let event = LoanEvent.didEnter(
            to: .doubleConfirmation,
            from: .termsHire
        )
        
        sut.trackEnterView()

        XCTAssertTrue(analytics.equals(to: event.event()))
    }
    
    func testShowWarning_ShouldDisplayWarningView() {
        let dumbRequestError = RequestError(title: "title",
                                            message: "message",
                                            code: LoanApiError.timeLimit.rawValue)

        guard let auth = dependencies.legacy.auth as? AuthenticationWrapperMock else {
            XCTFail("Authentication dependency could not be initiated")
            return
        }
        auth.expectedResult = .success("pin")
        serviceMock.response = .failure(.badRequest(body: dumbRequestError))
        
        sut.nextStep()

        XCTAssertEqual(presenterSpy.showTimeLimitWarningCallCount, 1)
    }

    func testNextStep_WhenProposalTimeout_ShouldDisplayProposalInProgressWarning() {
        let dumbRequestError = RequestError(title: "title",
                                            message: "message",
                                            code: LoanApiError.proposalTimeout.rawValue)

        guard let auth = dependencies.legacy.auth as? AuthenticationWrapperMock else {
            XCTFail("Authentication dependency could not be initiated")
            return
        }
        auth.expectedResult = .success("pin")

        serviceMock.response = .failure(.badRequest(body: dumbRequestError))

        sut.nextStep()

        XCTAssertEqual(presenterSpy.showProposalInProgressWarningCallCount, 1)
    }

    func testNextStep_WhenProposalInProgress_ShouldDisplayProposalInProgressWarning() {
        let dumbRequestError = RequestError(title: "title",
                                            message: "message",
                                            code: LoanApiError.proposalInProgress.rawValue)

        guard let auth = dependencies.legacy.auth as? AuthenticationWrapperMock else {
            XCTFail("Authentication dependency could not be initiated")
            return
        }
        auth.expectedResult = .success("pin")

        serviceMock.response = .failure(.badRequest(body: dumbRequestError))

        sut.nextStep()

        XCTAssertEqual(presenterSpy.showProposalInProgressWarningCallCount, 1)
    }
    
    func testShowWarning_WhenDeviceHasChanged_ShouldDisplayWarningView() throws {
        let dumbRequestError = RequestError(title: "title",
                                            message: "message",
                                            code: LoanApiError.deviceChangingInvalidation.rawValue)

        let auth = try XCTUnwrap(dependencies.legacy.auth as? AuthenticationWrapperMock)
        auth.expectedResult = .success("pin")
        serviceMock.response = .failure(.badRequest(body: dumbRequestError))
        
        sut.nextStep()

        XCTAssertEqual(presenterSpy.showDeviceChangingWarningCallCount, 1)
    }
}
