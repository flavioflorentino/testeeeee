import XCTest
import Foundation
import UI
@testable import LoanOffer

final private class ConfirmationControllerSpy: ConfirmationDisplay {
    private(set) var displayErrorCallCount = 0
    private(set) var startLoadingCallCount = 0
    private(set) var stopLoadingCallCount = 0
    
    func displayError() {
        displayErrorCallCount += 1
    }
    
    func startLoading() {
        startLoadingCallCount += 1
    }
    
    func stopLoading() {
        stopLoadingCallCount += 1
    }
}

final private class ConfirmationCoordinatorSpy: ConfirmationCoordinating {
    private(set) var delegate: LoanHireFlowCoordinating?
    private(set) var performActionCallCount = 0
    private(set) var performedAction: ConfirmationAction?
    
    init(delegate: LoanHireFlowCoordinating) {
        self.delegate = delegate
    }

    func perform(action: ConfirmationAction) {
        performActionCallCount += 1
        performedAction = action
    }
}

final class ConfirmationPresenterTests: XCTestCase {
    private let delegateSpy = LoanHireFlowCoordinatingSpy()
    private lazy var coordinatorSpy = ConfirmationCoordinatorSpy(delegate: delegateSpy)
    private let controllerSpy = ConfirmationControllerSpy()
    
    private lazy var sut: ConfirmationPresenter = {
        let sut = ConfirmationPresenter(coordinator: coordinatorSpy)
        sut.viewController = controllerSpy
        return sut
    }()
    
    func testStartLoading_ShouldDisplayLoading() {
        sut.startLoading()
        
        XCTAssertEqual(controllerSpy.startLoadingCallCount, 1)
    }
    
    func testStopLoading_ShouldHideLoading() {
        sut.stopLoading()
        
        XCTAssertEqual(controllerSpy.stopLoadingCallCount, 1)
    }
    
    func testPresentError_ShouldDisplayErrorAlert() {
        sut.showError()
        
        XCTAssertEqual(controllerSpy.displayErrorCallCount, 1)
    }
    
    func testClose_ShouldDismissFlow() {
        sut.didCancel()
        
        guard case .cancel = coordinatorSpy.performedAction else {
            XCTFail("Should have performed cancel action")
            return
        }
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
    }

    func testDidNextStep_ShouldPresentFormattedWarning() {
        let dumbWarning = ApolloFeedbackViewContent(image: Assets.confirmationSuccess.image,
                                                    title: Strings.Success.header,
                                                    description: NSAttributedString(string: Strings.Success.body),
                                                    primaryButtonTitle: Strings.Generic.ok,
                                                    secondaryButtonTitle: "")
        sut.didNextStep()

        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .warning(dumbWarning))
    }

    func testShowProposalInProgressWarning_ShouldPresentFormattedWarning() {
        let dumbWarning = ApolloFeedbackViewContent(image: Assets.confirmationSuccess.image,
                                                    title: Strings.ProposalInProgress.Warning.title,
                                                    description: NSAttributedString(string: Strings.ProposalInProgress.Warning.subtitle),
                                                    primaryButtonTitle: Strings.Generic.ok,
                                                    secondaryButtonTitle: "")
        sut.showProposalInProgressWarning()

        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .warning(dumbWarning))
    }

    func testShowTimeLimitWarning_ShouldPresentFormattedWarning() {
        let dumbWarning = ApolloFeedbackViewContent(image: Assets.unavailable.image,
                                                    title: Strings.Alerts.Unavailable.title,
                                                    description: NSAttributedString(string: Strings.Alerts.Unavailable.subtitle),
                                                    primaryButtonTitle: Strings.Generic.ok,
                                                    secondaryButtonTitle: "")
        sut.showTimeLimitWarning()

        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .warning(dumbWarning))
    }
}
