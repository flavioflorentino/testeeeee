import XCTest
import UI
import Foundation
@testable import LoanOffer

final class ConfirmationCoordinatorTests: XCTestCase {
    private lazy var delegateSpy = LoanHireFlowCoordinatingSpy()
    private lazy var sut = ConfirmationCoordinator(delegate: delegateSpy)
    
    func testPerformActionClose_ShouldCallDelegate() {
        sut.perform(action: .cancel)
        
        XCTAssertEqual(delegateSpy.performedAction, .back)
    }
    
    func testPerformActionFinish_ShouldCallDelegate() {
        sut.perform(action: .finish)
        
        XCTAssertEqual(delegateSpy.performedAction, .finish)
    }
    
    func testPerformActionWarning_ShouldCallDelegate() {
        let dumbWarning = ApolloFeedbackViewContent(image: Assets.unavailable.image,
                                                    title: "title",
                                                    description: NSAttributedString(string: "description"),
                                                    primaryButtonTitle: "buttonTitle",
                                                    secondaryButtonTitle: "")
        sut.perform(action: .warning(dumbWarning))
        
        XCTAssertEqual(delegateSpy.performedAction, .warning(dumbWarning))
    }
}
