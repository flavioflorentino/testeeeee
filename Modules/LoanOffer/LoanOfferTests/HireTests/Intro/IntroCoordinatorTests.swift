import XCTest
import Foundation
import UI
@testable import LoanOffer

final class IntroCoordinatorTests: XCTestCase {
    private lazy var delegateSpy = LoanHireFlowCoordinatingSpy()
    private let dependencies = LoanDependencyContainerMock(
        LoanOfferSetupMock(.deeplink)
    )
    private lazy var sut = IntroCoordinator(delegate: delegateSpy, dependencies: dependencies)
    
    func testPerformActionRange_WithSimulationRange_ShouldGoToReason() {
        let dumbRange = SimulationRange(minimumCredit: "",
                                        maximumCredit: "",
                                        bankId: "",
                                        registrationStatus: .approved,
                                        daysToDueDate: 90)
        
        sut.perform(action: .reason(range: dumbRange))
        
        XCTAssertEqual(delegateSpy.performedAction, .reason(dumbRange))
        XCTAssertEqual(delegateSpy.performActionCallCount, 1)
    }
    
    func testPerformActionClose_ShouldClose() {
        sut.perform(action: .close)
        
        XCTAssertEqual(delegateSpy.performActionCallCount, 1)
        XCTAssertEqual(delegateSpy.performedAction, .back)
    }
    
    func testPerformActionReview_ShouldGoToRegistrationReview() {
        sut.perform(action: .registrationReview)
        
        XCTAssertEqual(delegateSpy.performActionCallCount, 1)
        XCTAssertEqual(delegateSpy.performedAction, .registrationOnboarding(status: .review))
    }
    
    func testPerformActionFinish_ShouldCallDelegate() {
        sut.perform(action: .finish)
        
        XCTAssertEqual(delegateSpy.performedAction, .finish)
    }
    
    func testPerformActionWarning_ShouldCallDelegate() {
        let dumbWarning = ApolloFeedbackViewContent(image: Assets.unavailable.image,
                                                    title: "title",
                                                    description: NSAttributedString(string: "description"),
                                                    primaryButtonTitle: "buttonTitle",
                                                    secondaryButtonTitle: "")
        sut.perform(action: .warning(dumbWarning))
        
        XCTAssertEqual(delegateSpy.performedAction, .warning(dumbWarning))
    }
}
