import XCTest
import Core
import AssetsKit
import UI
import Foundation
@testable import LoanOffer

final private class IntroControllerSpy: IntroDisplay {
    private(set) var updateViewMessagesCallCount = 0
    private(set) var displayErrorCallCount = 0
    private(set) var startLoadingCallCount = 0
    private(set) var stopLoadingCallCount = 0
    private(set) var addBulletRowCallCount = 0
    private(set) var addUserToWaitListCallCount = 0
    
    private(set) var summaryText: NSAttributedString?
    private(set) var disclaimerText: NSAttributedString?
    
    func updateViewMessages(with summary: NSAttributedString,
                            andDisclaimer disclaimer: NSAttributedString,
                            shouldHideFooter hidden: Bool) {
        updateViewMessagesCallCount += 1
        summaryText = summary
        disclaimerText = disclaimer
    }
    
    func displayError() {
        displayErrorCallCount += 1
    }
    
    func startLoading(title: String) {
        startLoadingCallCount += 1
    }
    
    func stopLoading() {
        stopLoadingCallCount += 1
    }

    func addBulletRow(icon: Iconography, description: String) {
        addBulletRowCallCount += 1
    }

    func addUserToWaitList() {
        addUserToWaitListCallCount += 1
    }

}

final private class IntroCoordinatorSpy: IntroCoordinating {
    var viewController: UIViewController?
    
    private(set) var delegate: LoanHireFlowCoordinating?
    private(set) var performActionCallCount = 0
    private(set) var performedAction: IntroAction?
    
    init(delegate: LoanHireFlowCoordinating, dependencies: LoanDependencies = LoanDependencyContainerMock()) {
        self.delegate = delegate
    }
    
    func perform(action: IntroAction) {
        performActionCallCount += 1
        performedAction = action
    }
}

final class IntroPresenterTests: XCTestCase {
    private let delegateSpy = LoanHireFlowCoordinatingSpy()
    private lazy var coordinatorSpy = IntroCoordinatorSpy(delegate: delegateSpy)
    private let controllerSpy = IntroControllerSpy()
    
    private lazy var sut: IntroPresenter = {
        let sut = IntroPresenter(coordinator: coordinatorSpy)
        sut.viewController = controllerSpy
        return sut
    }()
    
    func testDidNextStep_ShouldCallCoordinator() {
        let dumbRange = SimulationRange(minimumCredit: "",
                                        maximumCredit: "",
                                        bankId: "",
                                        registrationStatus: .approved,
                                        daysToDueDate: 90)
        
        sut.didNextStep(with: dumbRange)
        
        XCTAssertEqual(coordinatorSpy.performedAction, .reason(range: dumbRange))
    }
    
    func testShowRegistrationReview_ShouldPerformActionRegistrationReview() {
        sut.showRegistrationReview()
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .registrationReview)
    }
    
    func testUpdateMessages_WhenDaysToDueDateNil_ShouldDisplaySummaryAndDisclaimer() {
        sut.setupView(with: "R$ 14.567,78", daysToDueDate: nil, shouldHideFooter: false)
        
        let description = "Adicione até R$ 14.567,78 na sua carteira!*"
        let disclaimer = "Pague o que quiser com o valor adicionado no seu PicPay. Veja algumas das opções:"

        XCTAssertEqual(controllerSpy.summaryText?.string, description)
        XCTAssertEqual(controllerSpy.disclaimerText?.string, disclaimer)
    }

    func testUpdateMessages_WhenDaysToDueDateNotNil_ShouldDisplaySummaryAndDisclaimer() {
        sut.setupView(with: "R$ 14.567,78", daysToDueDate: 90, shouldHideFooter: false)

        let description = "Adicione até R$ 14.567,78 na sua carteira e pague em até 90 dias!"
        let disclaimer = "Pague o que quiser com o valor adicionado no seu PicPay. Veja algumas das opções:"

        XCTAssertEqual(controllerSpy.summaryText?.string, description)
        XCTAssertEqual(controllerSpy.disclaimerText?.string, disclaimer)
    }
    
    func testStartLoading_ShouldDisplayLoading() {
        sut.startLoading(title: "")

        XCTAssertEqual(controllerSpy.startLoadingCallCount, 1)
    }
    
    func testStopLoading_ShouldHideLoading() {
        sut.stopLoading()
        
        XCTAssertEqual(controllerSpy.stopLoadingCallCount, 1)
    }
    
    func testPresentError_ShouldDisplayErrorAlert() {
        sut.presentError()
        
        XCTAssertEqual(controllerSpy.displayErrorCallCount, 1)
    }
    
    func testClose_ShouldDismissFlow() {
        sut.close()
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .close)
    }
    
    func testSetupBulletRows_WhenViewLoads_ShouldShowBulletRows() {
        sut.setupBulletRows()
        
        XCTAssertEqual(controllerSpy.addBulletRowCallCount, 3)
    }
    
    func testFinish_ShouldFinishHireFlow() {
        sut.finish()
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .finish)
    }

    func testShowTimeLimitError_ShouldPresentFormattedWarning() {
        let warning = ApolloFeedbackViewContent(image: Assets.unavailable.image,
                                                title: Strings.Alerts.Unavailable.title,
                                                description: NSAttributedString(string: Strings.Alerts.Unavailable.subtitle),
                                                primaryButtonTitle: Strings.Generic.ok,
                                                secondaryButtonTitle: "")
        sut.showTimeLimitError()

        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .warning(warning))

    }

    func testShowRegistrationDenied_ShouldPresentFormattedWarning() {
        let warning = ApolloFeedbackViewContent(image: Assets.error.image,
                                                title: Strings.Denied.header,
                                                description: NSAttributedString(string: Strings.Denied.body),
                                                primaryButtonTitle: Strings.Generic.ok,
                                                secondaryButtonTitle: Strings.Generic.learnMore)
        sut.showRegistrationDenied()

        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .registrationDeniedWarning(warning, article: .denied))
    }

    func testShowUnavailableLimit_ShouldPresentFormattedWarning() {
        let warning = ApolloFeedbackViewContent(image: Resources.Illustrations.iluWarning.image,
                                                title: Strings.UnavailableCredit.Warning.title,
                                                description: NSAttributedString(string: Strings.UnavailableCredit.Warning.subtitle),
                                                primaryButtonTitle: Strings.UnavailableCredit.Warning.waitlistButtonTitle,
                                                secondaryButtonTitle: Strings.Generic.notNow)
        sut.showUnavailableLimit()

        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .unavailableLimitWarning(warning, completion: { }))
    }

    func testShowProposalInProgress_ShouldPresentFormattedWarning() {
        let warning = ApolloFeedbackViewContent(image: Assets.confirmationSuccess.image,
                                                title: Strings.ProposalInProgress.Warning.title,
                                                description: NSAttributedString(string: Strings.ProposalInProgress.Warning.subtitle),
                                                primaryButtonTitle: Strings.Generic.ok,
                                                secondaryButtonTitle: "")
        sut.showProposalInProgress()

        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .warning(warning))
    }

    func testShowWaitListAddedSuccess_ShouldPresentFormattedWarning() {
        let warning = ApolloFeedbackViewContent(image: Resources.Illustrations.iluSuccess.image,
                                                title: Strings.WaitlistAdded.Warning.title,
                                                description: NSAttributedString(string: Strings.WaitlistAdded.Warning.subtitle),
                                                primaryButtonTitle: Strings.Generic.ok,
                                                secondaryButtonTitle: "")
        sut.showWaitListAddedSuccess()

        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .warning(warning))
    }
}
