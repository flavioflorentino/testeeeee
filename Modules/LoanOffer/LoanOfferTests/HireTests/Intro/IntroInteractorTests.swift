import AnalyticsModule
import XCTest
import AssetsKit
@testable import Core
@testable import LoanOffer

final private class IntroServiceSpy: IntroServicing {
    var simulationResponse: Result<SimulationRange, ApiError> = .failure(.serverError)
    var waitListResponse: Result<NoContent, ApiError> = .failure(.serverError)
    
    func fetchSimulationRange(completion: @escaping SimulationRangeCompletion) {
        completion(simulationResponse)
    }
    func addToWaitList(completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        completion(waitListResponse)
    }
}

final private class IntroPresenterSpy: IntroPresenting {
    var viewController: IntroDisplay?
    
    private(set) var setupViewCallCount = 0
    private(set) var nextStepCallCount = 0
    private(set) var presentErrorCallCount = 0
    private(set) var startLoadingCallCount = 0
    private(set) var stopLoadingCallCount = 0
    private(set) var closeCallCount = 0
    private(set) var openFAQCallCount = 0
    private(set) var showRegistrationReviewCallCount = 0
    private(set) var setupBulletRowsCallCount = 0
    private(set) var showTimeLimitErrorCallCount = 0
    private(set) var showRegistrationDeniedCallCount = 0
    private(set) var showRegistrationAnalysisCallCount = 0
    private(set) var showUnavailableLimitCallCount = 0
    private(set) var showProposalInProgressCallCount = 0
    private(set) var showWaitListAddedSuccessCallCount = 0
    
    private(set) var range: SimulationRange?
    private(set) var minimumLimit: String?
    private(set) var daysToDueDate: Int?

    func setupView(with minimumLimit: String, daysToDueDate: Int?, shouldHideFooter hidden: Bool) {
        setupViewCallCount += 1
        self.minimumLimit = minimumLimit
        self.daysToDueDate = daysToDueDate
    }

    func openFAQ() {
        openFAQCallCount += 1
    }
    
    func didNextStep(with range: SimulationRange) {
        nextStepCallCount += 1
        self.range = range
    }
    
    func presentError() {
        presentErrorCallCount += 1
    }
    
    func startLoading(title: String) {
        startLoadingCallCount += 1
    }
    
    func stopLoading() {
        stopLoadingCallCount += 1
    }
    
    func close() {
        closeCallCount += 1
    }
    
    func showRegistrationReview() {
        showRegistrationReviewCallCount += 1
    }
    
    func setupBulletRows() {
        setupBulletRowsCallCount += 1
    }
    
    func showTimeLimitError() {
        showTimeLimitErrorCallCount += 1
    }

    func showRegistrationDenied() {
        showRegistrationDeniedCallCount += 1
    }

    func showRegistrationAnalysis() {
        showRegistrationAnalysisCallCount += 1
    }

    func showUnavailableLimit() {
        showUnavailableLimitCallCount += 1
    }

    func showProposalInProgress() {
        showProposalInProgressCallCount += 1
    }

    func showWaitListAddedSuccess() {
        showWaitListAddedSuccessCallCount += 1
    }
}

final class IntroInteractorTests: XCTestCase {
    private let presenterSpy = IntroPresenterSpy()
    private let serviceSpy = IntroServiceSpy()
    private lazy var sut = IntroInteractor(with: presenterSpy,
                                           service: serviceSpy,
                                           from: .offer,
                                           dependencies: dependencies)
    private let analytics = AnalyticsSpy()
    private lazy var dependencies = LoanDependencyContainerMock(analytics,
                                                                SafariViewManagerMock(),
                                                                LoanOfferSetupMock(.faq))
    private let range: SimulationRange = SimulationRange(minimumCredit: "R$ 100,00",
                                                         maximumCredit: "R$ 10.000,00",
                                                         bankId: "212",
                                                         registrationStatus: .denied,
                                                         daysToDueDate: 90)
    
    func testFetchRange_WhenServerError_ShouldPresentError() throws {
        serviceSpy.simulationResponse = .failure(.serverError)
        
        sut.fetchRange()
        
        XCTAssertEqual(presenterSpy.startLoadingCallCount, 1)
        try assertError()
    }
    
    func testFetchRange_WhenRegistrationStatusIsDenied_ShouldShowDeniedView() {
        serviceSpy.simulationResponse = .success(range)
        sut.fetchRange()

        XCTAssertEqual(presenterSpy.startLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.showRegistrationDeniedCallCount, 1)
    }
    
    func testFetchRange_WhenRegistrationStatusIsNeedReview_ShouldShowReviewView() {
        serviceSpy.simulationResponse = .success(SimulationRange(minimumCredit: "R$ 100,00",
                                                                 maximumCredit: "R$ 10.000,00",
                                                                 bankId: "212",
                                                                 registrationStatus: .review,
                                                                 daysToDueDate: 90))
        sut.fetchRange()
        
        XCTAssertEqual(presenterSpy.startLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.showRegistrationReviewCallCount, 1)
    }
    
    func testFetchRange_WhenRegistrationStatusIsUnderAnalysis_ShouldShowPendingRegistrationView() {
        serviceSpy.simulationResponse = .success(SimulationRange(minimumCredit: "R$ 100,00",
                                                                 maximumCredit: "R$ 10.000,00",
                                                                 bankId: "212",
                                                                 registrationStatus: .analysis,
                                                                 daysToDueDate: 90))
        sut.fetchRange()

        XCTAssertEqual(presenterSpy.startLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.showRegistrationAnalysisCallCount, 1)
    }
    
    func testFetchRange_WhenRangeIsEmpty_ShouldPresentErrorAndTrackErrorEvent() throws {
        serviceSpy.simulationResponse = .success(SimulationRange(minimumCredit: "",
                                                                 maximumCredit: "",
                                                                 bankId: "212",
                                                                 registrationStatus: .approved,
                                                                 daysToDueDate: 90))
        
        sut.fetchRange()
        
        XCTAssertEqual(presenterSpy.startLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallCount, 1)
        try assertError()
    }
    
    func testFetchRange_WhenRegistrationStatusIsCompleted_ShouldSetupViewWithMaximumLimit() {
        serviceSpy.simulationResponse = .success(SimulationRange(minimumCredit: "R$ 100,00",
                                                                 maximumCredit: "R$ 10.000,00",
                                                                 bankId: "212",
                                                                 registrationStatus: .approved,
                                                                 daysToDueDate: 90))
        
        sut.fetchRange()
        
        XCTAssertEqual(presenterSpy.startLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.setupViewCallCount, 1)
        XCTAssertEqual(presenterSpy.minimumLimit, "R$ 10.000,00")
    }

    func testFetchRange_WhenTimeLimitReached_ShouldDisplayTimeLimitWarning() {
        let dumbRequestError = RequestError(title: "title",
                                            message: "message",
                                            code: LoanApiError.timeLimit.rawValue)
        let expectedEvent = LoanEvent.didReceiveError(error: .timeLimit)

        serviceSpy.simulationResponse = .failure(.badRequest(body: dumbRequestError))

        sut.fetchRange()

        XCTAssertTrue(analytics.equals(to: expectedEvent.event()))
        XCTAssertEqual(presenterSpy.showTimeLimitErrorCallCount, 1)
    }

    func testFetchRange_WhenUnavailableLimit_ShouldPresentUnavailableLimitWarning() {
        let dumbRequestError = RequestError(title: "title",
                                            message: "message",
                                            code: LoanApiError.unavailableLimit.rawValue)
        let expectedEvent = LoanHireEvent.unavailableLimit(origin: .offer, reason: .limit)
        serviceSpy.simulationResponse = .failure(.badRequest(body: dumbRequestError))

        sut.fetchRange()

        XCTAssertTrue(analytics.equals(to: expectedEvent.event()))
        XCTAssertEqual(presenterSpy.showUnavailableLimitCallCount, 1)
    }

    func testFetchRange_WhenNoOffer_ShouldPresentUnavailableLimitWarning() {
        let dumbRequestError = RequestError(title: "title",
                                            message: "message",
                                            code: LoanApiError.noOffer.rawValue)

        let expectedEvent = LoanHireEvent.unavailableLimit(origin: .offer, reason: .offer)

        serviceSpy.simulationResponse = .failure(.badRequest(body: dumbRequestError))

        sut.fetchRange()
        
        XCTAssertTrue(analytics.equals(to: expectedEvent.event()))
        XCTAssertEqual(presenterSpy.showUnavailableLimitCallCount, 1)
    }

    func testAddUserToWaitlist_WhenAddUserToWaitList_ShouldPresentSuccessFeedback() {
        serviceSpy.waitListResponse = .success(NoContent())
        sut.addUserToWaitlist()

        XCTAssertEqual(presenterSpy.startLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.showWaitListAddedSuccessCallCount, 1)
    }

    func testAddUserToWaitlist_WhenFailToAddUserToWaitList_ShouldPresentErrorAndTrackErrorEvent() throws {
        sut.addUserToWaitlist()

        XCTAssertEqual(presenterSpy.startLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallCount, 1)
        try assertError()
    }
    
    func testNextStep_WhenRangeIsNil_ShouldNotCallNextStep() {
        sut.nextStep()
        
        XCTAssertEqual(presenterSpy.nextStepCallCount, 0)
    }
    
    func testNextStep_WhenRangeIsOkAndRegistrationStatusIsCompleted_ShouldCallNextStep() {
        serviceSpy.simulationResponse = .success(SimulationRange(minimumCredit: "R$ 100,00",
                                                                 maximumCredit: "R$ 10.000,00",
                                                                 bankId: "212",
                                                                 registrationStatus: .approved,
                                                                 daysToDueDate: 90))
        sut.fetchRange()
        
        sut.nextStep()
        
        XCTAssertEqual(presenterSpy.nextStepCallCount, 1)
    }
    
    func testNextStep_WhenRangeIsOkAndRegistrationStatusIsNeedComplete_ShouldCallNextStep() {
        serviceSpy.simulationResponse = .success(SimulationRange(minimumCredit: "R$ 100,00",
                                                                 maximumCredit: "R$ 10.000,00",
                                                                 bankId: "212",
                                                                 registrationStatus: .incomplete,
                                                                 daysToDueDate: 90))
        sut.fetchRange()
        
        sut.nextStep()
        
        XCTAssertEqual(presenterSpy.nextStepCallCount, 1)
    }
    
    func testNextStep_WhenRangeIsOkAndRegistrationStatusIsNeedRegistrate_ShouldCallNextStep() {
        serviceSpy.simulationResponse = .success(SimulationRange(minimumCredit: "R$ 100,00",
                                                                 maximumCredit: "R$ 10.000,00",
                                                                 bankId: "212",
                                                                 registrationStatus: .registrate,
                                                                 daysToDueDate: 90))
        sut.fetchRange()
        
        sut.nextStep()
        
        XCTAssertEqual(presenterSpy.nextStepCallCount, 1)
    }
    
    func testClose_ShouldDismissFlow() {
        sut.close()
        
        XCTAssertEqual(presenterSpy.closeCallCount, 1)
    }
    
    func testTrackEnterView_WhenComesFromOffer_ShouldLogEventDidEnterFromOffer() throws {
        let expectedEvent = LoanEvent.didEnter(to: .onboarding, from: .offer, params: ["Flow" : range.analyticsParamFlow()])
        serviceSpy.simulationResponse = .success(range)
        sut.fetchRange()
        sut.trackEnterView()
        
        try assertOrigins(with: expectedEvent, originType: .offer)
    }
    
    func testOpenFaq_ShouldOpenSafariView() {
        guard let faq = dependencies.legacy.faq as? FAQWrapperMock else {
            XCTFail("FAQ dependency could not be initiated")
            return
        }
        
        faq.expectedResult = FAQWebViewControllerMock()
        
        sut.openFAQ()
        
        XCTAssertEqual(presenterSpy.openFAQCallCount, 1)
    }
    
    func testSetupBulletRows_WhenViewLoads_ShouldShowBulletRows() {
        sut.setupBulletRows()
        
        XCTAssertEqual(presenterSpy.setupBulletRowsCallCount, 1)
    }
    
    private func assertOrigins(with expectedEvent: LoanEvent, originType: OriginType) throws {
        let analytics = try XCTUnwrap(dependencies.analytics as? AnalyticsSpy)
        let origin = try XCTUnwrap(analytics.value(forKey: "origin"))
        
        XCTAssertTrue(analytics.equals(to: expectedEvent.event()))
        XCTAssertEqual(origin, originType.value)
    }
    
    private func assertError() throws {
        let expectedEvent = LoanEvent.didReceiveError(error: .onboarding)
        
        XCTAssertEqual(presenterSpy.presentErrorCallCount, 1)
        
        let analytics = try XCTUnwrap(dependencies.analytics as? AnalyticsSpy)
        XCTAssertTrue(analytics.equals(to: expectedEvent.event()))
    }
}
