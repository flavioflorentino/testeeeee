import SafariServices
import Core
import XCTest
import UI
import Foundation
@testable import LoanOffer

final private class SimulationControllerSpy: SimulationDisplay {
    private(set) var setMaximumLimitCallCount = 0
    private(set) var setupTextFieldCallCount = 0
    private(set) var setupInstallmentsListCallCount = 0
    private(set) var setupFeesMessageCallCount = 0
    private(set) var presentErrorCallCount = 0
    private(set) var changeStepperViewStateCallCount = 0
    private(set) var ChangeInstallmentsInfoViewStateCallCount = 0
    private(set) var startLoadingCallCount = 0
    private(set) var stopLoadingCallCount = 0
    private(set) var startButtonLoadingCallCount = 0
    private(set) var stopButtonLoadingCallCount = 0
    private(set) var enableNextButtonCallCount = 0
    private(set) var hideDetailsButtonCallCount = 0
    private(set) var setupDueDateCallCount = 0
    private(set) var displayCalendarViewCallCount = 0
    private(set) var displayDueDateInfoCallCount = 0
    private(set) var setupLoanAmountQuestionDescriptionCount = 0
    private(set) var setupTitleDescriptionCount = 0

    private(set) var textfieldRange: ClosedRange<Double>?
    private(set) var maximumLimit: String?
    private(set) var minValue: String?
    private(set) var maxValue: String?
    private(set) var installmentsList: [Int]?
    private(set) var monthlyFee: String?
    private(set) var cetFee: String?
    private(set) var loanAmountQuestionDescription: String?
    private(set) var titleDescription: String?
    private(set) var action: RetryAction?
    private(set) var stepperViewState: StepperViewState?
    private(set) var installmentViewState: InstallmentsInfoViewState?
    private(set) var nextButtonEnabled: Bool?
    private(set) var detailsButtonInvisible: Bool?

    func set(_ maximumLimit: String) {
        self.maximumLimit = maximumLimit
        setMaximumLimitCallCount += 1
    }

    func setupTextfield(_ range: ClosedRange<Double>) {
        textfieldRange = range
        setupTextFieldCallCount += 1
    }
    
    func setupInstallments(_ list: [Int]) {
        setupInstallmentsListCallCount += 1
        installmentsList = list
    }
    
    func setupFeesMessage(with monthlyFee: String, and cetFee: String) {
        setupFeesMessageCallCount += 1
        self.monthlyFee = monthlyFee
        self.cetFee = cetFee
    }

    func displayError(with action: RetryAction) {
        presentErrorCallCount += 1
        self.action = action
    }
    
    func changeStepperViewState(to state: StepperViewState) {
        changeStepperViewStateCallCount += 1
        stepperViewState = state
    }
    
    func changeInstallmentsInfoViewState(to state: InstallmentsInfoViewState) {
        ChangeInstallmentsInfoViewStateCallCount += 1
        installmentViewState = state
    }
    
    func startLoading() {
        startLoadingCallCount += 1
    }
    
    func stopLoading() {
        stopLoadingCallCount += 1
    }

    func startButtonLoading() {
        startButtonLoadingCallCount += 1
    }
    
    func stopButtonLoading() {
        stopButtonLoadingCallCount += 1
    }
    
    func hideDetailsButton(_ invisible: Bool) {
        hideDetailsButtonCallCount += 1
        detailsButtonInvisible = invisible
    }
    
    func enableNextButton(_ enabled: Bool) {
        enableNextButtonCallCount += 1
        nextButtonEnabled = enabled
    }

    func setupDueDate(_ dueDateText: NSAttributedString) {
        setupDueDateCallCount += 1
    }

    func displayCalendarView(with currentDate: Date) {
        displayCalendarViewCallCount += 1
    }
    
    func displayDueDateInfo() {
        displayDueDateInfoCallCount += 1
    }
    
    func setupLoanAmountQuestionDescription(description: String) {
        loanAmountQuestionDescription = description
        setupLoanAmountQuestionDescriptionCount += 1
    }
    
    func setupTitleDescription(title: String) {
        titleDescription = title
        setupTitleDescriptionCount += 1
    }
}

final private class SimulationCoordinatorSpy: SimulationCoordinating {
    var viewController: UIViewController?
    
    private(set) var delegate: LoanHireFlowCoordinating?
    private(set) var performActionCallCount = 0
    private(set) var performedAction: SimulationAction?
    private(set) var contract: ContractResponse?
    
    init(delegate: LoanHireFlowCoordinating) {
        self.delegate = delegate
    }

    func perform(action: SimulationAction) {
        performActionCallCount += 1
        performedAction = action
    }
}

final class SimulationPresenterTests: XCTestCase, PaymentDateDelegate {
    private let delegateSpy = LoanHireFlowCoordinatingSpy()
    private lazy var coordinatorSpy = SimulationCoordinatorSpy(delegate: delegateSpy)
    private let controllerSpy = SimulationControllerSpy()
    private let maximumLimit: String = "R$ 10.000,00"
    
    private lazy var sut: SimulationPresenter = {
        let sut = SimulationPresenter(coordinator: coordinatorSpy)
        sut.viewController = controllerSpy
        return sut
    }()
    
    func testViewDidLoad_WhenSuccess_ShouldDisplayMaximumLimit() {
        sut.set(maximumLimit)
        
        XCTAssertEqual(controllerSpy.setMaximumLimitCallCount, 1)
        XCTAssertEqual(controllerSpy.maximumLimit, maximumLimit)
    }
    
    func testSetupInstallmentsView_ShouldConfigInstallmentsView() {
        sut.setupInstallmentsView(with: 1, andInstallmentValue: "R$ 17.346,15")
        
        XCTAssertEqual(controllerSpy.ChangeInstallmentsInfoViewStateCallCount, 1)
        
        if case let .success(installments, installmentValue) = controllerSpy.installmentViewState {
            XCTAssertEqual(installments, "1")
            XCTAssertEqual(installmentValue, "R$ 17.346,15")
        } else {
            XCTFail("Info view state should be success")
        }
    }
    
    func testSetupTextfield_ShouldSetupTextfieldWithRangeBetweenMinAndMaxCreditLimits() {
        let range: ClosedRange<Double> = 1000.0...50000.0
        
        sut.setupTextfield(range)
        
        XCTAssertEqual(controllerSpy.setupTextFieldCallCount, 1)
        XCTAssertEqual(controllerSpy.textfieldRange, range)
    }
    
    func testSetupTextfieldRange_WhenMaxValueHasErrorOnFormatting_ShouldSetupRangeBetweenMinAndDoubleGreatestFiniteMagnitude() {
        let range: ClosedRange<Double> = 1000.0...Double.greatestFiniteMagnitude
        
        sut.setupTextfield(range)
        
        XCTAssertEqual(controllerSpy.setupTextFieldCallCount, 1)
        XCTAssertEqual(controllerSpy.textfieldRange, range)
    }
    
    func testSetupInstallmentsList_ShouldSetupStepperList() {
        let list = [1, 5, 10]
        sut.setupInstallments(list: list)
        
        XCTAssertEqual(controllerSpy.setupInstallmentsListCallCount, 1)
        XCTAssertEqual(controllerSpy.installmentsList, list)
    }
    
    func testSetupInstallmentsList_WhenListIsEmpty_ShouldPresentError() {
        let list: [Int] = []
        sut.setupInstallments(list: list)
        
        XCTAssertEqual(controllerSpy.presentErrorCallCount, 1)
    }
    
    func testShowError_WhenAnErrorOccured_ShouldPresentError() {
        sut.showError()
        
        XCTAssertEqual(controllerSpy.presentErrorCallCount, 1)
    }
    
    func testNextStep_ShouldCallPresenter() {
        let contract = ContractResponse(id: "5e3da85f021ee2720d41476f",
                                        portion: 2,
                                        requestedValue: "R$2.000,00",
                                        totalValue: "R$2.180,00",
                                        installmentValue: "R$1.090,00",
                                        monthlyCETLabel: "CET",
                                        monthlyCETPercent: "64,50%",
                                        monthlyIOFLabel: "Valor total do IOF financiado",
                                        iofTotalValue: "64,50%",
                                        monthlyInterestLabel: "Taxa Nominal",
                                        monthlyInterestPercent: "4,50%",
                                        firstDueDate: "10/04/2020",
                                        dueDay: 10)
        
        sut.nextStep(with: contract)
        
        guard
            case let .terms(contract: passedContract) = coordinatorSpy.performedAction
        else {
            XCTFail("Should have performed open FAQ action")
            return
        }
        XCTAssertEqual(passedContract, contract)
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
    }
    
    func testShowError_WhenActionIsConfirm_ShouldPassInstallmentNumber() {
        sut.showError(with: .confirm(installment: 10))
        
        XCTAssertEqual(controllerSpy.stopButtonLoadingCallCount, 1)
        if case let .confirm(installment) = controllerSpy.action {
            XCTAssertEqual(installment, 10)
        } else {
            XCTFail("Error action should be confirm with installment number of 10")
        }
    }
    
    func testChangeStepperViewState_WhenStateIsDisabled_ShouldChangeStateToDisabled() {
        sut.changeStepperViewState(to: .disabled)
        
        XCTAssertEqual(controllerSpy.changeStepperViewStateCallCount, 1)
        XCTAssertEqual(controllerSpy.stepperViewState, .disabled)
    }
    
    func testChangeStepperViewState_WhenStateIsEnabled_ShouldChangeStateToEnabled() {
        sut.changeStepperViewState(to: .enabled)
        
        XCTAssertEqual(controllerSpy.changeStepperViewStateCallCount, 1)
        XCTAssertEqual(controllerSpy.stepperViewState, .enabled)
    }
    
    func testChangeInstallmentsInfoViewState_WhenStateIsError_ShouldChangeStateToError() {
        sut.changeInstallmentsInfoViewState(to: .error)
        
        XCTAssertEqual(controllerSpy.ChangeInstallmentsInfoViewStateCallCount, 1)
        XCTAssertEqual(controllerSpy.installmentViewState, .error)
    }
    
    func testChangeInstallmentsInfoViewState_WhenStateIsLoading_ShouldChangeStateToLoading() {
        sut.changeInstallmentsInfoViewState(to: .loading)
        
        XCTAssertEqual(controllerSpy.ChangeInstallmentsInfoViewStateCallCount, 1)
        XCTAssertEqual(controllerSpy.installmentViewState, .loading)
    }
    
    func testChangeInstallmentsInfoViewState_WhenStateIsSuccess_ShouldPassInstallmentAndValue() {
        sut.changeInstallmentsInfoViewState(to: .success("10", "R$ 1.000,00"))
        
        XCTAssertEqual(controllerSpy.ChangeInstallmentsInfoViewStateCallCount, 1)
        
        if case let .success(installmentNumber, installmentValue) = controllerSpy.installmentViewState {
            XCTAssertEqual(installmentNumber, "10")
            XCTAssertEqual(installmentValue, "R$ 1.000,00")
        } else {
            XCTFail("Error action should be success with installment number of 10 and value R$ 1.000,00")
        }
    }
    
    func testChangeNextButtonState_WhenStateIsEnabled_ShouldChangeStateToEnabled() {
        sut.changeNextButtonState(toEnabled: true)
        
        XCTAssertEqual(controllerSpy.enableNextButtonCallCount, 1)
        XCTAssertEqual(controllerSpy.nextButtonEnabled, true)
    }
    
    func testChangeNextButtonState_WhenStateIsDisabled_ShouldChangeStateToDisabled() {
        sut.changeNextButtonState(toEnabled: false)
        
        XCTAssertEqual(controllerSpy.enableNextButtonCallCount, 1)
        XCTAssertEqual(controllerSpy.nextButtonEnabled, false)
    }
    
    func testStartButtonLoading_ShouldStartButtonLoading() {
        sut.startButtonLoading()
        
        XCTAssertEqual(controllerSpy.startButtonLoadingCallCount, 1)
    }
    
    func testStopButtonLoading_ShouldStopButtonLoading() {
        sut.stopButtonLoading()
        
        XCTAssertEqual(controllerSpy.stopButtonLoadingCallCount, 1)
    }
    
    func testShowTimeLimitWarning_ShouldDisplayWarningView() {
        let dumbWarning = ApolloFeedbackViewContent(image: Assets.unavailable.image,
                                                    title: Strings.Alerts.Unavailable.title,
                                                    description: NSAttributedString(string: Strings.Alerts.Unavailable.subtitle),
                                                    primaryButtonTitle: Strings.Generic.ok,
                                                    secondaryButtonTitle: "")
        sut.showTimeLimitWarning()
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .warning(dumbWarning))
    }
    
    func testFinish_ShouldFinishHireFlow() {
        sut.finish()
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .finish)
    }

    func testDisplayCalendarView_ShouldDisplayCalendarView() {
        let date = Date()
        sut.presentCalendarView(with: date, daysToDueDate: 90, delegate: self)

        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
    }

    func testSetupDueDate_ShouldSetupDueDate() {
        sut.setupDueDate(Date())

        XCTAssertEqual(controllerSpy.setupDueDateCallCount, 1)
    }
    
    func testShowDueDateInfo_ShouldDisplayDueDateInfo() {
        sut.showDueDateInfo()
        
        XCTAssertEqual(controllerSpy.displayDueDateInfoCallCount, 1)
    }
    
    func testSetupLoanAmountQuestionDescriptionWithRegistrationCompleted() {
        let questionDescriptionExpectation: String = Strings.Simulation.loanAmountQuestionRegistrationCompleted
        
        sut.setupLoanAmountQuestionDescription(isRegistrationCompleted: true)
        
        XCTAssertEqual(controllerSpy.setupLoanAmountQuestionDescriptionCount, 1)
        XCTAssertEqual(controllerSpy.loanAmountQuestionDescription, questionDescriptionExpectation)
    }
    
    func testSetupTitleDescriptionCountWithRegistrationCompleted() {
        let titleExpectation: String = Strings.Simulation.titleWithRegistrationCompleted
        
        sut.setupTitleDescription(isRegistrationCompleted: true)
        
        XCTAssertEqual(controllerSpy.setupTitleDescriptionCount, 1)
        XCTAssertEqual(controllerSpy.titleDescription, titleExpectation)
    }
    
    func testSetupLoanAmountQuestionDescriptionWithRegistrationIncomplete() {
        let questionDescriptionExpectation: String = Strings.Simulation.loanAmountQuestion
        
        sut.setupLoanAmountQuestionDescription(isRegistrationCompleted: false)
        
        XCTAssertEqual(controllerSpy.setupLoanAmountQuestionDescriptionCount, 1)
        XCTAssertEqual(controllerSpy.loanAmountQuestionDescription, questionDescriptionExpectation)
    }
    
    func testSetupTitleDescriptionCountWithRegistrationIncomplete() {
        let titleExpectation: String = Strings.Simulation.title
        
        sut.setupTitleDescription(isRegistrationCompleted: false)
        
        XCTAssertEqual(controllerSpy.setupTitleDescriptionCount, 1)
        XCTAssertEqual(controllerSpy.titleDescription, titleExpectation)
    }
    
    func didSelect(date: Date) { }
}
