import XCTest
import UI
import Foundation
@testable import LoanOffer

final class SimulationCoordinatorTests: XCTestCase {
    private lazy var delegateSpy = LoanHireFlowCoordinatingSpy()
    private let dependencies = LoanDependencyContainerMock(
        LoanOfferSetupMock(.deeplink)
    )
    private lazy var sut = SimulationCoordinator(delegate: delegateSpy, dependencies: dependencies)
    
    func testPerformActionTerms_ShouldGoToTerms() {
        let dumbContract = ContractResponse(id: "",
                                            portion: 0,
                                            requestedValue: "",
                                            totalValue: "",
                                            installmentValue: "",
                                            monthlyCETLabel: "",
                                            monthlyCETPercent: "",
                                            monthlyIOFLabel: "",
                                            iofTotalValue: "",
                                            monthlyInterestLabel: "",
                                            monthlyInterestPercent: "",
                                            firstDueDate: "",
                                            dueDay: 0)
        sut.perform(action: .terms(contract: dumbContract))
        
        XCTAssertEqual(delegateSpy.performedAction, .terms(contract: dumbContract))
        XCTAssertEqual(delegateSpy.performActionCallCount, 1)
    }
    
    func testPerformActionFinish_ShouldCallDelegate() {
        sut.perform(action: .finish)
        
        XCTAssertEqual(delegateSpy.performedAction, .finish)
    }
    
    func testPerformActionWarning_ShouldCallDelegate() {
        let dumbWarning = ApolloFeedbackViewContent(image: Assets.unavailable.image,
                                                    title: "title",
                                                    description: NSAttributedString(string: "description"),
                                                    primaryButtonTitle: "buttonTitle",
                                                    secondaryButtonTitle: "")
        sut.perform(action: .warning(dumbWarning))
        
        XCTAssertEqual(delegateSpy.performedAction, .warning(dumbWarning))
    }
}
