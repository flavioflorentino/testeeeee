import XCTest
import Core
import AnalyticsModule

@testable import LoanOffer

final private class PaymentDateServiceMock: PaymentDateServicing {
    var response: Result<PaymentDateResponse, ApiError> = .failure(.serverError)
    
    func getValidDates(completion: @escaping (Result<PaymentDateResponse, ApiError>) -> Void) {
        completion(response)
    }
}

final private class PaymentDatePresenterMock: PaymentDatePresenting {
    var viewController: PaymentDateDisplaying?
    
    private(set) var startLoadingCallCount = 0
    private(set) var stopLoadingCallCount = 0
    private(set) var presentCalendarCallCount = 0
    private(set) var presentDescriptionCallCount = 0
    private(set) var presentErrorCallCount = 0
    private(set) var closeCallCount = 0
    
    func startLoading() {
        startLoadingCallCount += 1
    }
    
    func stopLoading() {
        stopLoadingCallCount += 1
    }
    
    func presentCalendar(selectedDate: Date, numberOfMonths: Int?) {
        presentCalendarCallCount += 1
    }
    
    func presentDescription(with dueDays: Int?) {
        presentDescriptionCallCount += 1
    }
    
    func presentError() {
        presentErrorCallCount += 1
    }
    
    func close(selectedDate: Date) {
        closeCallCount += 1
    }
}

final class PaymentDateInteractorTests: XCTestCase {
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return dateFormatter
    }()
    
    private var selectedDate = Date()
    
    private let dependencies = LoanDependencyContainerMock(AnalyticsSpy())
    private let serviceMock = PaymentDateServiceMock()
    private let presenterMock = PaymentDatePresenterMock()
    private lazy var sut = PaymentDateInteractor(
        selectedDate: selectedDate,
        daysToDueDate: nil,
        service: serviceMock,
        presenter: presenterMock,
        origin: .simulation,
        dependencies: dependencies)
    
    func testSetupViews_ShouldCallPresentDescription() {
        sut.setupViews()
        XCTAssertEqual(presenterMock.presentDescriptionCallCount, 1)
    }
    
    func testFetchAvailableDates_WhenFetchSucceed_ShouldCallPresentCalendar() {
        serviceMock.response = .success(PaymentDateResponse(validDatesList: []))
        sut.fetchAvailableDates()
        XCTAssertEqual(presenterMock.presentCalendarCallCount, 1)
    }
    
    func testSelectedDateChanged_WhenSelectADate_ShouldSetSelectedDate() throws {
        sut.selectedDate = try XCTUnwrap(dateFormatter.date(from: "16/11/1985"), "'baseDate' variable cannot be nil")
        
        let newDate = try XCTUnwrap(dateFormatter.date(from: "18/11/1985"), "'baseDate' variable cannot be nil")
        
        sut.selectedDateChanged(date: newDate)
        XCTAssertEqual(sut.selectedDate, newDate)
    }
    
    func testIsAvailable_GivenAnAvailableDate_IsAvailableShouldReturnTrue() throws {
        let availableDate = try XCTUnwrap(dateFormatter.date(from: "16/11/1985"), "'baseDate' variable cannot be nil")
        
        sut.availableDates.append(availableDate)
        
        XCTAssertTrue(sut.isAvailable(date: availableDate))
    }
    
    func testIsAvailable_GivenAnUnavailableDate_IsAvailableShouldReturnFalse() throws {
        let unavailableDate = try XCTUnwrap(dateFormatter.date(from: "17/11/1985"), "'baseDate' variable cannot be nil")
        
        XCTAssertFalse(sut.isAvailable(date: unavailableDate))
    }
    
    func testNumberOfMonths_GivenThreeMonthsAheadDate_ShouldReturnFourMonthsToLoad() throws {
        let lastDate = try XCTUnwrap(Calendar.current.date(byAdding: .month, value: 3, to: Date()))
        serviceMock.response = .success(PaymentDateResponse(validDatesList: [lastDate]))
        
        sut.fetchAvailableDates()
        
        XCTAssertEqual(sut.numberOfMonths, 4, "'numberOfMonths' should add 1 as current month")
    }
    
    func testClose_WhenSelectButtonTapped_ShouldCallClose() {
        sut.close()
        XCTAssertEqual(presenterMock.closeCallCount, 1)
    }

    func testSetupAnalytics_WhenComesFromSimulation_ShouldLogEventDidEnterFromSimulation() throws {
        let expectedEvent = LoanEvent.didEnter(to: .paymentDate, from: .simulation)
        sut.setupAnalytics()

        let analytics = try XCTUnwrap(dependencies.analytics as? AnalyticsSpy)
        XCTAssertTrue(analytics.equals(to: expectedEvent.event()))
    }
}
