import Core
import XCTest
@testable import LoanOffer

final private class PaymentDateCoordinatorSpy: PaymentDateCoordinating {
    var viewController: UIViewController?
    
    private(set) var performActionCallCount = 0
    private(set) var performedAction: PaymentDateAction?
    
    func perform(action: PaymentDateAction) {
        performActionCallCount += 1
        performedAction = action
    }
}

final private class PaymentDateControllerSpy: PaymentDateDisplaying {
    private(set) var startLoadingCallCount = 0
    private(set) var stopLoadingCallCount = 0
    private(set) var displayCalendarCallCount = 0
    private(set) var displayDescriptionCallCount = 0
    private(set) var displayAccessibilityDescriptionCallCount = 0
    private(set) var displayErrorCallCount = 0
    
    private(set) var descriptionPresented: String?
    private(set) var displayAccessibilityDescriptionPresented: String?
    
    func startLoading() {
        startLoadingCallCount += 1
    }
    
    func stopLoading() {
        stopLoadingCallCount += 1
    }
    
    func displayCalendar(selectedDate: Date, numberOfMonths: Int?) {
        displayCalendarCallCount += 1
    }
    
    func displayDescription(text: String, acessibilityText: String) {
        displayDescriptionCallCount += 1
        descriptionPresented = text
        displayAccessibilityDescriptionPresented = acessibilityText
    }
    
    func displayError() {
        displayErrorCallCount += 1
    }
}

final class PaymentDatePresenterTests: XCTestCase {
    private lazy var coordinatorSpy = PaymentDateCoordinatorSpy()
    private let controllerSpy = PaymentDateControllerSpy()
    
    private lazy var sut: PaymentDatePresenter = {
        let sut = PaymentDatePresenter(coordinator: coordinatorSpy)
        sut.viewController = controllerSpy
        return sut
    }()
    
    func testStartLoading_WhenCallStartLoading_ShouldDisplayLoading() {
        sut.startLoading()
        XCTAssertEqual(controllerSpy.startLoadingCallCount, 1)
    }
    
    func testStopLoading_WhenCallStopLoading_ShouldHideLoading() {
        sut.stopLoading()
        XCTAssertEqual(controllerSpy.stopLoadingCallCount, 1)
    }
    
    func testSetupDescription_WhenDueDaysIsNotNil_ShouldDisplayDescription() {
        sut.presentDescription(with: 90)
        
        XCTAssertEqual(controllerSpy.descriptionPresented, "Você tem até 90 dias para fazer o primeiro pagamento. O dia escolhido será o mesmo para as próximas parcelas.")
        XCTAssertEqual(controllerSpy.displayAccessibilityDescriptionPresented, "Você tem até 90 dias para fazer o primeiro pagamento. O dia escolhido será o mesmo para as próximas parcelas. Essas são as datas disponíveis, toque duas vezes para escolher o melhor dia pra você.")
        XCTAssertEqual(controllerSpy.displayDescriptionCallCount, 1)
        XCTAssertEqual(controllerSpy.displayDescriptionCallCount, 1)
    }
    
    func testSetupDescription_WhenDueDaysIsNil_ShouldNotDisplayDescription() {
        sut.presentDescription(with: nil)
        XCTAssertEqual(controllerSpy.displayDescriptionCallCount, 0)
    }
    
    func testpresentError_WhenCallpresentError_ShouldDisplayError() {
        sut.presentError()
        XCTAssertEqual(controllerSpy.displayErrorCallCount, 1)
    }
    
    func testClose_WhenCallClose_ShouldClose() {
        let selectedDate = Date()
        
        sut.close(selectedDate: selectedDate)
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .close(selectedDate: selectedDate))
    }
}
