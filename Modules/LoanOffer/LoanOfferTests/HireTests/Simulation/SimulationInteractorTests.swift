import AnalyticsModule
import FeatureFlag
import XCTest
@testable import LoanOffer
@testable import Core

final private class SimulationPresenterSpy: SimulationPresenting {
    var viewController: SimulationDisplay?

    private(set) var setMaximumLimitCallCount = 0
    private(set) var setupInstallmentsViewCallCount = 0
    private(set) var setupTextFieldCallCount = 0
    private(set) var setupInstallmentsListCallCount = 0
    private(set) var showErrorCallCount = 0
    private(set) var nextStepCallCount = 0
    private(set) var changeStepperViewStateCallCount = 0
    private(set) var changeInstallmentsInfoViewStateCallCount = 0
    private(set) var setupViewCallCount = 0
    private(set) var startLoadingCallCount = 0
    private(set) var stopLoadingCallCount = 0
    private(set) var startButtonLoadingCallCount = 0
    private(set) var stopButtonLoadingCallCount = 0
    private(set) var changeNextButtonEnableCallCount = 0
    private(set) var openFAQCallCount = 0
    private(set) var finishCallCount = 0
    private(set) var showTimeLimitWarningCallCount = 0
    private(set) var setupDueDateCallCount = 0
    private(set) var presentCalendarViewCallCount = 0
    private(set) var showDueDateInfoCallCount = 0
    private(set) var setupLoanAmountQuestionDescriptionCount = 0
    private(set) var setupTitleDescriptionCount = 0
    
    private(set) var maximumLimit: String?
    private(set) var installments: Int?
    private(set) var installmentValue: String?
    private(set) var minValue: String?
    private(set) var maxValue: String?
    private(set) var installmentsList: [Int]?
    private(set) var contract: ContractResponse?
    private(set) var action: RetryAction?
    private(set) var stepperViewState: StepperViewState?
    private(set) var installmentViewState: InstallmentsInfoViewState?
    private(set) var nextButtonEnabled: Bool?

    func set(_ maximumLimit: String) {
        self.maximumLimit = maximumLimit
        setMaximumLimitCallCount += 1
    }
    
    func setupInstallmentsView(with installments: Int, andInstallmentValue installmentValue: String) {
        self.installments = installments
        self.installmentValue = installmentValue
        setupInstallmentsViewCallCount += 1
    }
    
    func setupTextfield(_ range: ClosedRange<Double>) {
        minValue = range.lowerBound.toCurrencyString()?.replacingOccurrences(of: " ", with: " ")
        maxValue = range.upperBound.toCurrencyString()?.replacingOccurrences(of: " ", with: " ")
        setupTextFieldCallCount += 1
    }
    
    func setupInstallments(list: [Int]) {
        installmentsList = list
        setupInstallmentsListCallCount += 1
    }
    
    func nextStep(with contract: ContractResponse) {
        self.contract = contract
        nextStepCallCount += 1
    }
    
    func showError(with action: RetryAction) {
        showErrorCallCount += 1
        self.action = action
    }
    
    func changeStepperViewState(to state: StepperViewState) {
        changeStepperViewStateCallCount += 1
        stepperViewState = state
    }
    
    func changeInstallmentsInfoViewState(to state: InstallmentsInfoViewState) {
        changeInstallmentsInfoViewStateCallCount += 1
        installmentViewState = state
    }
    
    func setupView(installmentState: InstallmentsInfoViewState) {
        setupViewCallCount += 1
        installmentViewState = installmentState
    }
    
    func startLoading() {
        startLoadingCallCount += 1
    }
    
    func stopLoading() {
        stopLoadingCallCount += 1
    }
    
    func startButtonLoading() {
        startButtonLoadingCallCount += 1
    }
    
    func stopButtonLoading() {
        stopButtonLoadingCallCount += 1
    }
    
    func changeNextButtonState(toEnabled enabled: Bool) {
        changeNextButtonEnableCallCount += 1
        nextButtonEnabled = enabled
    }
    
    func openFAQ() {
        openFAQCallCount += 1
    }
    
    func showTimeLimitWarning() {
        showTimeLimitWarningCallCount += 1
    }
    
    func finish() {
        finishCallCount += 1
    }

    func setupDueDate(_ date: Date?) {
        setupDueDateCallCount += 1
    }

    func presentCalendarView(with date: Date, daysToDueDate: Int?, delegate: PaymentDateDelegate) {
        presentCalendarViewCallCount += 1
    }
    
    func showDueDateInfo() {
        showDueDateInfoCallCount += 1
    }
    
    func setupLoanAmountQuestionDescription(isRegistrationCompleted: Bool) {
        setupLoanAmountQuestionDescriptionCount += 1
    }
    
    func setupTitleDescription(isRegistrationCompleted: Bool) {
        setupTitleDescriptionCount += 1
    }
}

final private class SimulationServiceMock: SimulationServicing {
    var simulateResponse: Result<SimulationResponse, ApiError> = .failure(.serverError)
    var choseSimulationResponse: Result<ContractResponse, ApiError> = .failure(.serverError)

    private(set) var simulatedValue: String?
    
    func simulate(for value: String, firstDueDate: Date?, bankId: String, _ completion: @escaping SimulationCompletionBlock) {
        simulatedValue = value
        completion(simulateResponse)
    }
    
    func choseSimulation(
        with simulationId: String,
        forInstallment number: Int,
        firstDueDate: Date?,
        _ completion: @escaping ChoseSimulationCompletionBlock
    ) {
        completion(choseSimulationResponse)
    }
}


final class SimulationInteractorTests: XCTestCase {
    private let serviceMock = SimulationServiceMock()
    private let presenterSpy = SimulationPresenterSpy()
    private let minimumLimit: String = "R$ 100,00"
    private let range = SimulationRange(minimumCredit: "R$ 100,00",
                                        maximumCredit: "R$ 10.000,00",
                                        bankId: "212",
                                        registrationStatus: .approved,
                                        daysToDueDate: 90)
    private let analytics = AnalyticsSpy()
    private lazy var dependencies = LoanDependencyContainerMock(featureManagerMock,
                                                                analytics,
                                                                SafariViewManagerMock(),
                                                                LoanOfferSetupMock(.faq))
    private let successSimulationResponse = SimulationResponse(
        simulationId: "simulation_id",
        installments: [SimulationInstallmentsResponse(installmentNumber: 10,
                                                      installmentValue: "R$ 5.789,17",
                                                      monthlyInterestPercent: "5,79%",
                                                      monthlyCETPercent: "10,13%",
                                                      totalValue: "R$ 5.789,17",
                                                      firstInstallmentDueDate: Date()),
                       SimulationInstallmentsResponse(installmentNumber: 1,
                                                      installmentValue: "R$ 57.891,73",
                                                      monthlyInterestPercent: "4,79%",
                                                      monthlyCETPercent: "8,13%",
                                                      totalValue: "R$ 5.789,17",
                                                      firstInstallmentDueDate: Date()),
                       SimulationInstallmentsResponse(installmentNumber: 5,
                                                      installmentValue: "R$ 11.578,37",
                                                      monthlyInterestPercent: "4,90%",
                                                      monthlyCETPercent: "9,30%",
                                                      totalValue: "R$ 5.789,17",
                                                      firstInstallmentDueDate: Date())]
    )
    private let wrongRangeSimulationResponse = SimulationResponse(
        simulationId: "simulation_id",
        installments: [SimulationInstallmentsResponse(installmentNumber: 10,
                                                      installmentValue: "R$ 5.789,17",
                                                      monthlyInterestPercent: "5,79%",
                                                      monthlyCETPercent: "10,13%",
                                                      totalValue: "R$ 5.789,17",
                                                      firstInstallmentDueDate: Date()),
                       SimulationInstallmentsResponse(installmentNumber: 1,
                                                      installmentValue: "R$ 57.891,73",
                                                      monthlyInterestPercent: "4,79%",
                                                      monthlyCETPercent: "8,13%",
                                                      totalValue: "R$ 5.789,17",
                                                      firstInstallmentDueDate: Date()),
                       SimulationInstallmentsResponse(installmentNumber: 5,
                                                      installmentValue: "R$ 11.578,37",
                                                      monthlyInterestPercent: "4,90%",
                                                      monthlyCETPercent: "9,30%",
                                                      totalValue: "R$ 5.789,17",
                                                      firstInstallmentDueDate: Date())]
    )
    private let successChoseSimulationResponse = ContractResponse(id: "5e3da85f021ee2720d41476f",
                                                                  portion: 2,
                                                                  requestedValue: "R$2.000,00",
                                                                  totalValue: "R$2.180,00",
                                                                  installmentValue: "R$1.090,00",
                                                                  monthlyCETLabel: "CET",
                                                                  monthlyCETPercent: "64,50%",
                                                                  monthlyIOFLabel: "Valor total do IOF financiado",
                                                                  iofTotalValue: "64,50%",
                                                                  monthlyInterestLabel: "Taxa Nominal",
                                                                  monthlyInterestPercent: "4,50%",
                                                                  firstDueDate: "10/04/2020",
                                                                  dueDay: 10)
    
    private let featureManagerMock: FeatureManagerMock = {
        let featureManagerMock = FeatureManagerMock()
        featureManagerMock.override(keys: .isFeatureLoanSimulationDueDateAvailable, with: true)
        return featureManagerMock
    }()
    
    private lazy var sut = SimulationInteractor(service: serviceMock,
                                                presenter: presenterSpy,
                                                range: range,
                                                from: .reason,
                                                dependencies: dependencies)
    
    func testViewDidLoad_ShouldDisplayMaximumLimit() {
        sut.fetchSimulation()
        
        XCTAssertEqual(presenterSpy.setMaximumLimitCallCount, 1)
        XCTAssertEqual(presenterSpy.changeNextButtonEnableCallCount, 1)
        XCTAssertEqual(presenterSpy.maximumLimit, minimumLimit)
    }
    
    func testFirstSimulationFlag_WhenDisplayDueDateFeatureFlagIsOff_ShouldNotSetupDueDate() {
        serviceMock.simulateResponse = .success(successSimulationResponse)
        featureManagerMock.override(key: .isFeatureLoanSimulationDueDateAvailable, with: false)
        
        sut.fetchSimulation()
        
        XCTAssertEqual(presenterSpy.setupDueDateCallCount, 0)
    }
    
    func testFirstSimulationFlag_WhenDisplayDueDateFeatureFlagIsOn_ShouldSetupDueDate() {
        serviceMock.simulateResponse = .success(successSimulationResponse)
        featureManagerMock.override(key: .isFeatureLoanSimulationDueDateAvailable, with: true)
        
        sut.fetchSimulation()
        
        XCTAssertEqual(presenterSpy.setupDueDateCallCount, 1)
    }
    
    func testViewDidLoad_WhenItIsNotTheFirstTimeTappingDueDate_ShouldDisplayFirstInstallmentDueDateCalendarView() {
        featureManagerMock.override(key: .isFeatureLoanSimulationDueDateAvailable, with: true)
        sut.isFirstCalendarView = true
        sut.didTapDueDate()
        XCTAssertEqual(presenterSpy.showDueDateInfoCallCount, 1)
    }
    
    func testViewDidLoad_WhenItIsTheFirstTimeTappingDueDate_ShouldDisplayFirstInstallmentDueDateInfoAlert() {
        serviceMock.simulateResponse = .success(successSimulationResponse)
        
        sut.fetchSimulation()
        sut.isFirstCalendarView = false
        sut.didTapDueDate()
        
        XCTAssertEqual(presenterSpy.presentCalendarViewCallCount, 1)
    }
    
    func testViewDidLoad_WhenItIsTheFirstTimeLoadingSimulation_ShouldDisplayFullLoadingView() {
        sut.fetchSimulation()
        XCTAssertEqual(presenterSpy.startLoadingCallCount, 1)
    }
    
    func testSimulate_WhenSimulationSucceed_ShouldSimulateWithMaximumLimit() {
        serviceMock.simulateResponse = .success(successSimulationResponse)

        sut.simulate()

        XCTAssertEqual(presenterSpy.changeNextButtonEnableCallCount, 2)
        XCTAssertEqual(presenterSpy.nextButtonEnabled, true)
        XCTAssertEqual(presenterSpy.changeStepperViewStateCallCount, 1)
        XCTAssertEqual(presenterSpy.stepperViewState, .disabled)
        XCTAssertEqual(presenterSpy.changeInstallmentsInfoViewStateCallCount, 1)
        XCTAssertEqual(presenterSpy.installmentViewState, .loading)
        
        XCTAssertEqual(presenterSpy.setupInstallmentsListCallCount, 1)
        XCTAssertEqual(presenterSpy.setupInstallmentsViewCallCount, 1)
    }
    
    func testSimulate_WhenUserChangesValueSimulationSucceed_ShouldSimulateWithMaximumLimit() {
        serviceMock.simulateResponse = .success(successSimulationResponse)

        sut.simulate(with: "R$ 2.000,00")

        XCTAssertEqual(presenterSpy.changeNextButtonEnableCallCount, 2)
        XCTAssertEqual(presenterSpy.nextButtonEnabled, true)
        XCTAssertEqual(presenterSpy.changeStepperViewStateCallCount, 1)
        XCTAssertEqual(presenterSpy.stepperViewState, .disabled)
        XCTAssertEqual(presenterSpy.changeInstallmentsInfoViewStateCallCount, 1)
        XCTAssertEqual(presenterSpy.installmentViewState, .loading)
        
        XCTAssertEqual(presenterSpy.setupInstallmentsListCallCount, 1)
        XCTAssertEqual(presenterSpy.setupInstallmentsViewCallCount, 1)
        
        XCTAssertEqual(serviceMock.simulatedValue, "R$ 2.000,00")
    }
    
    func testSimulate_WhenSimulationFailsWithWrongRange_ShouldShowError() {
        serviceMock.simulateResponse = .success(wrongRangeSimulationResponse)
        let sut = SimulationInteractor(service: serviceMock,
                                       presenter: presenterSpy,
                                       range: SimulationRange(minimumCredit: "R$ 10.000,00",
                                                              maximumCredit: "R$ 100,00",
                                                              bankId: "212",
                                                              registrationStatus: .approved,
                                                              daysToDueDate: 90),
                                       from: .reason,
                                       dependencies: dependencies)
        
        sut.fetchSimulation()

        XCTAssertEqual(presenterSpy.showErrorCallCount, 1)
        XCTAssertEqual(presenterSpy.action, .simulate)
    }
    
    func testSimulate_WhenRangeIsNil_ShouldShowError() {
        serviceMock.simulateResponse = .success(wrongRangeSimulationResponse)
        let sut = SimulationInteractor(service: serviceMock,
                                       presenter: presenterSpy,
                                       range: nil,
                                       from: .reason,
                                       dependencies: dependencies)
        
        sut.fetchSimulation()

        XCTAssertEqual(presenterSpy.showErrorCallCount, 1)
        XCTAssertEqual(presenterSpy.action, .simulate)
    }
    
    func testSimulate_WhenSimulationFailure_ShouldReturnEmptySimulationsList() {
        sut.simulate()

        XCTAssertEqual(presenterSpy.showErrorCallCount, 1)
        XCTAssertEqual(presenterSpy.action, .simulate)
    }

    func testGetPreviousInstallment_WhenSimulationIsSuccess_ShouldShowPreviousInstallment() {
        serviceMock.simulateResponse = .success(successSimulationResponse)
        sut.simulate()

        sut.getPreviousInstallment()

        XCTAssertEqual(presenterSpy.changeNextButtonEnableCallCount, 3)
        XCTAssertEqual(presenterSpy.nextButtonEnabled, true)
        XCTAssertEqual(presenterSpy.setupInstallmentsViewCallCount, 2)
    }

    func testGetADifferentInstallment_WhenSimulationIsSuccess_ShouldShowMiddleInstallment() {
        serviceMock.simulateResponse = .success(successSimulationResponse)
        sut.simulate()

        sut.getPreviousInstallment()
        sut.getPreviousInstallment()
        sut.getNextInstallment()

        XCTAssertEqual(presenterSpy.setupInstallmentsViewCallCount, 4)
        XCTAssertEqual(presenterSpy.changeNextButtonEnableCallCount, 5)
        XCTAssertEqual(presenterSpy.nextButtonEnabled, true)
    }
    
    func testNextStep_ShouldCallPresenter() {
        serviceMock.simulateResponse = .success(successSimulationResponse)
        serviceMock.choseSimulationResponse = .success(successChoseSimulationResponse)
        
        sut.simulate()
        
        sut.choseSimulation(with: 1)
        
        XCTAssertEqual(presenterSpy.changeStepperViewStateCallCount, 3)
        XCTAssertEqual(presenterSpy.stepperViewState, .enabled)
        XCTAssertEqual(presenterSpy.startButtonLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.stopButtonLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.nextStepCallCount, 1)
    }
    
    func testNextStep_WhenDidNotSimulate_ShouldShowError() {
        sut.choseSimulation(with: 1)
        
        XCTAssertEqual(presenterSpy.showErrorCallCount, 1)
        if case let .confirm(installmentNumber) = presenterSpy.action {
            XCTAssertEqual(installmentNumber, 1)
        } else {
            XCTFail("Action should be confirm with installment number 1")
        }
    }
    
    func testNextStep_WhenErrorOccur_ShouldShowError() {
        serviceMock.simulateResponse = .success(successSimulationResponse)
        sut.simulate()
        
        sut.choseSimulation(with: 1)
        
        XCTAssertEqual(presenterSpy.showErrorCallCount, 1)
        if case let .confirm(installmentNumber) = presenterSpy.action {
            XCTAssertEqual(installmentNumber, 1)
        } else {
            XCTFail("Action should be confirm with installment number 1")
        }
    }
    
    func testGetNextInstallment_WhenSimulateFails_ShouldNotUpdateStepper() {
        sut.getNextInstallment()
        
        XCTAssertEqual(presenterSpy.setupInstallmentsViewCallCount, 0)
    }
    
    func testGetPreviousInstallment_WhenSimulateFails_ShouldNotUpdateStepper() {
        sut.getPreviousInstallment()
        
        XCTAssertEqual(presenterSpy.setupInstallmentsViewCallCount, 0)
    }
    
    func testOnConfirmErrorAlert_WhenActionIsConfirm_ShouldChoseSimulation() {
        serviceMock.choseSimulationResponse = .success(successChoseSimulationResponse)
        
        sut.onConfirmErrorAlert(action: .confirm(installment: 1))
        
        XCTAssertEqual(presenterSpy.changeStepperViewStateCallCount, 1)
        XCTAssertEqual(presenterSpy.stepperViewState, .disabled)
        XCTAssertEqual(presenterSpy.startButtonLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.showErrorCallCount, 1)
        if case let .confirm(installmentNumber) = presenterSpy.action {
            XCTAssertEqual(installmentNumber, 1)
        } else {
            XCTFail("Action should be confirm with installment number 1")
        }
    }
    
    func testOnConfirmErrorAlert_WhenActionIsSimulate_ShouldSimulate() {
        serviceMock.simulateResponse = .success(successSimulationResponse)
        
        sut.onConfirmErrorAlert(action: .simulate)
        
        XCTAssertEqual(presenterSpy.changeNextButtonEnableCallCount, 2)
        XCTAssertEqual(presenterSpy.nextButtonEnabled, true)
        XCTAssertEqual(presenterSpy.changeStepperViewStateCallCount, 1)
        XCTAssertEqual(presenterSpy.stepperViewState, .disabled)
        XCTAssertEqual(presenterSpy.changeInstallmentsInfoViewStateCallCount, 1)
        XCTAssertEqual(presenterSpy.installmentViewState, .loading)
        
        XCTAssertEqual(presenterSpy.setupInstallmentsListCallCount, 1)
        XCTAssertEqual(presenterSpy.setupInstallmentsViewCallCount, 1)
    }

    func testStopButtonLoading_ShouldStopLoading() {
        sut.stopButtonLoading()
        
        XCTAssertEqual(presenterSpy.stopButtonLoadingCallCount, 1)
    }
    
    func testTrack_WhenPreviousSimulation_ShouldLogEventSimulationInstallmentsChange() {
        serviceMock.simulateResponse = .success(successSimulationResponse)
        sut.simulate()

        sut.getPreviousInstallment()
        
        let event = LoanHireEvent.didChangeSimulationInstallments
        XCTAssertTrue(analytics.equals(to: event.event()))
    }
    
    func testTrack_WhenDueDateFeatureFlagIsOn_ShouldLogCompleteEventDidSimulate() {
        featureManagerMock.override(key: .isFeatureLoanSimulationDueDateAvailable, with: true)
        serviceMock.simulateResponse = .success(successSimulationResponse)
        
        sut.simulate()
        
        let event = LoanHireEvent.didSimulate(requestedValue: minimumLimit,
                                              finalValue: "5789.17",
                                              installment: createInstallment(),
                                              selectedDueDate: Date(),
                                              daysToFirstInstallment: "0")
        XCTAssertTrue(analytics.equals(to: event.event()))
    }
    
    func testTrack_WhenSelectedDueDateChanged_ShouldDaysToFirstInstallmentLogCorrectly() throws {
        let initialDate = try XCTUnwrap(Date.forthFormatter.date(from: "16/11/2020"))
        let selectedDate = try XCTUnwrap(Date.forthFormatter.date(from: "20/11/2020"))
        
        var installment = createInstallment(date: initialDate)
        var response = createSimulation(installment: installment)
        serviceMock.simulateResponse = .success(response)
        
        sut.fetchSimulation()
        
        installment = createInstallment(date: selectedDate)
        response = createSimulation(installment: installment)
        serviceMock.simulateResponse = .success(response)
        
        sut.didSelect(date: selectedDate)
        
        let event = LoanHireEvent.didSimulate(requestedValue: minimumLimit,
                                              finalValue: "5789.17",
                                              installment: installment,
                                              selectedDueDate: selectedDate,
                                              daysToFirstInstallment: "4")
        XCTAssertTrue(analytics.equals(to: event.event()))
    }
    
    func testTrack_WhenDueDateFeatureFlagIsOff_ShouldLogEventDidSimulate() {
        featureManagerMock.override(key: .isFeatureLoanSimulationDueDateAvailable, with: false)
        serviceMock.simulateResponse = .success(successSimulationResponse)
        
        sut.simulate()
        
        let event = LoanHireEvent.didSimulate(requestedValue: minimumLimit,
                                              finalValue: "5789.17",
                                              installment: createInstallment(),
                                              selectedDueDate: nil,
                                              daysToFirstInstallment: nil)
        XCTAssertTrue(analytics.equals(to: event.event()))
    }
    
    func testTrack_WhenShowFeesView_ShouldLogEventDidCheckFeesInfo() {
        sut.trackFeesView()
        
        let event = LoanHireEvent.didCheckFeesInfo
        XCTAssertTrue(analytics.equals(to: event.event()))
    }
    
    func testTrack_WhenEnterView_ShouldLogEventDidEnter() {
        sut.trackEnterView()
        
        let event = LoanEvent.didEnter(to: .simulation, from: .reason, params: ["Flow" : range.analyticsParamFlow()])
        XCTAssertTrue(analytics.equals(to: event.event()))
    }
    
    func testTrack_WhenEnterViewFromTerms_ShouldLogEventDidEnter() {
        let sut = SimulationInteractor(service: serviceMock,
                                       presenter: presenterSpy,
                                       range: range,
                                       from: .termsHire,
                                       dependencies: dependencies)
        sut.trackEnterView()
        
        let event = LoanEvent.didEnter(to: .simulation, from: .termsHire, params: ["Flow" : range.analyticsParamFlow()])
        XCTAssertTrue(analytics.equals(to: event.event()))
    }
    
    func testTrack_WhenChangeSimulationValue_ShouldLogEventDidChangeSimulationValue() {
        sut.trackChangeSimulationValue()
        
        let event = LoanHireEvent.didChangeSimulationValue
        XCTAssertTrue(analytics.equals(to: event.event()))
    }
    
    func testTrack_When_ShouldLogEventDidReceiveError() {
        sut.choseSimulation(with: 0)
        
        let event = LoanEvent.didReceiveError(error: .simulation)
        XCTAssertTrue(analytics.equals(to: event.event()))
    }
    
    func testOpenFaq_ShouldOpenFAQWebView() {
        guard let faq = dependencies.legacy.faq as? FAQWrapperMock else {
            XCTFail("FAQ dependency could not be initiated")
            return
        }
        faq.expectedResult = FAQWebViewControllerMock()
        
        sut.openFAQ()
        
        XCTAssertEqual(presenterSpy.openFAQCallCount, 1)
    }

    func testOpenCalendarView_ShouldPresentCalendarView() {
        serviceMock.simulateResponse = .success(successSimulationResponse)
        
        sut.fetchSimulation()
        sut.openCalendarView()
        
        XCTAssertEqual(presenterSpy.presentCalendarViewCallCount, 1)
    }
    
    func testShowWarning_ShouldDisplayWarningView() {
        let dumbRequestError = RequestError(title: "title",
                                            message: "message",
                                            code: LoanApiError.timeLimit.rawValue)
        serviceMock.simulateResponse = .failure(.badRequest(body: dumbRequestError))
        sut.simulate()

        XCTAssertEqual(presenterSpy.showTimeLimitWarningCallCount, 1)
    }
    
    func testShowInfoDueDate_WhenTapOnDueDate_ShouldShowInfoAboutDueDate() {
        sut.didTapDueDate()
        
        XCTAssertEqual(presenterSpy.showDueDateInfoCallCount, 1)
    }
    
    func testSetupTitleDescription() {
        sut.fetchSimulation()
        
        XCTAssertEqual(presenterSpy.setupTitleDescriptionCount, 1)
    }
    
    func testSetupLoanAmountQuestionDescription() {
        sut.fetchSimulation()
        
        XCTAssertEqual(presenterSpy.setupLoanAmountQuestionDescriptionCount, 1)
    }
    
    private func createSimulation(installment: SimulationInstallmentsResponse? = nil) -> SimulationResponse {
        guard let installment = installment else {
            return SimulationResponse(
                simulationId: "1",
                installments: [createInstallment()]
            )
        }
        
        return SimulationResponse(
            simulationId: "1",
            installments: [installment]
        )
    }
    
    private func createInstallment(date: Date = Date()) -> SimulationInstallmentsResponse {
        SimulationInstallmentsResponse(
            installmentNumber: 10,
            installmentValue: "R$ 5.789,17",
            monthlyInterestPercent: "5,79%",
            monthlyCETPercent: "10,13%",
            totalValue: "R$ 5.789,17",
            firstInstallmentDueDate: date
        )
    }
}
