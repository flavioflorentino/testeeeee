import XCTest
@testable import LoanOffer

final class OnboardingRegistrationCoordinatorTests: XCTestCase {
    private lazy var delegateSpy = LoanRegistrationFlowCoordinatingSpy()
    private lazy var sut = OnboardingRegistrationCoordinator(delegate: delegateSpy)
    
    func testPerformActionRegistration_ShouldCallDelegate() {
        let viewController = UIViewController()
        sut.perform(action: .registration(controller: viewController))
        
        XCTAssertEqual(delegateSpy.performedAction, .registration(controller: viewController))
        XCTAssertEqual(delegateSpy.performActionCallCount, 1)
    }
    
    func testPerformActionCancel_ShouldCallDelegate() {
        sut.perform(action: .cancel)
        
        XCTAssertEqual(delegateSpy.performedAction, .close)
    }
    
    func testPerformActionFinish_ShouldCallDelegate() {
        sut.perform(action: .finish)
        
        XCTAssertEqual(delegateSpy.performedAction, .finish)
    }
}

