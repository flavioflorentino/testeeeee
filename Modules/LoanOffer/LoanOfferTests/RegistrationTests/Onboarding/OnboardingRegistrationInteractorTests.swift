import AnalyticsModule
import XCTest
@testable import LoanOffer
@testable import Core

final private class OnboardingRegistrationPresenterSpy: OnboardingRegistrationPresenting {
    var viewController: OnboardingRegistrationDisplay?
    private(set) var setupViewCallCount = 0
    private(set) var didNextStepCallCount = 0
    private(set) var startLoadingCallCount = 0
    private(set) var stopLoadingCallCount = 0
    private(set) var showErrorCallCount = 0
    private(set) var state: OnboardingState?
    private(set) var action: OnboardingRegistrationAction?
    
    func setupView(for state: OnboardingState) {
        setupViewCallCount += 1
        self.state = state
    }
    
    func didNextStep(action: OnboardingRegistrationAction) {
        didNextStepCallCount += 1
        self.action = action
    }
    
    func startLoading() {
        startLoadingCallCount += 1
    }
    
    func stopLoading() {
        stopLoadingCallCount += 1
    }
    
    func showError() {
        showErrorCallCount += 1
    }
}

final private class OnboardingRegistrationServiceSpy: OnboardingRegistrationServicing {
    var registrationResponse: Result<OnboardingRegistrationStatus, ApiError> = .failure(.serverError)
    var fetchRegistrationStatusCallCount = 0

    func fetchRegistrationStatus(completion: @escaping RegistrationStatusCompletion) {
        fetchRegistrationStatusCallCount += 1
        completion(registrationResponse)
    }
}

final class OnboardingRegistrationInteractorTests: XCTestCase {
    private let presenterSpy = OnboardingRegistrationPresenterSpy()
    private let serviceSpy = OnboardingRegistrationServiceSpy()
    private lazy var dependencies = LoanDependencyContainerMock(AnalyticsSpy(), LoanOfferSetupMock(.creditRegistration))

    func createSUT(forStatus status: RegistrationStatus) -> OnboardingRegistrationInteractor {
        OnboardingRegistrationInteractor(presenter: presenterSpy,
                                         service: serviceSpy,
                                         dependencies: dependencies,
                                         withStatus: status)
    }
    
    func testSetupView_WhenRegistrationStatusIsIncomplete_ShouldSetupViewWithCorrectState() {
        let sut = createSUT(forStatus: .incomplete)
        
        sut.loadData()
        
        XCTAssertEqual(presenterSpy.setupViewCallCount, 1)
        XCTAssertEqual(serviceSpy.fetchRegistrationStatusCallCount, 0)
        XCTAssertEqual(presenterSpy.state, .incomplete)
    }
    
    func testSetupView_WhenRegistrationStatusIsReview_ShouldSetupViewWithCorrectState() {
        let sut = createSUT(forStatus: .review)
        
        sut.loadData()
        
        XCTAssertEqual(presenterSpy.setupViewCallCount, 1)
        XCTAssertEqual(serviceSpy.fetchRegistrationStatusCallCount, 0)
        XCTAssertEqual(presenterSpy.state, .review)
    }
    
    func testSetupView_WhenRegistrationStatusIsRegistrate_ShouldSetupViewWithCorrectState() {
        let sut = createSUT(forStatus: .registrate)
        
        sut.loadData()
        
        XCTAssertEqual(presenterSpy.setupViewCallCount, 1)
        XCTAssertEqual(serviceSpy.fetchRegistrationStatusCallCount, 0)
        XCTAssertEqual(presenterSpy.state, .start)
    }

    func testSetupView_WhenRegistrationStatusIsUnknowed_ShouldFetchRegistrationState() {
        serviceSpy.registrationResponse = .success(OnboardingRegistrationStatus(status: .incomplete))

        let sut = createSUT(forStatus: .unknowed)

        sut.loadData()

        XCTAssertEqual(presenterSpy.setupViewCallCount, 1)
        XCTAssertEqual(serviceSpy.fetchRegistrationStatusCallCount, 1)
        XCTAssertEqual(presenterSpy.state, .incomplete)
    }
    
    func testCancel_WhenRegistrationStatusIsReview_ShouldPerformFinishAction() {
        let sut = createSUT(forStatus: .review)
        
        sut.cancel()
        
        XCTAssertEqual(presenterSpy.didNextStepCallCount, 1)
        XCTAssertEqual(presenterSpy.action, .finish)
    }
    
    func testCancel_WhenRegistrationStatusIsIncomplete_ShouldPerformCancelAction() {
        let sut = createSUT(forStatus: .incomplete)
        
        sut.cancel()
        
        XCTAssertEqual(presenterSpy.didNextStepCallCount, 1)
        XCTAssertEqual(presenterSpy.action, .cancel)
    }
}
