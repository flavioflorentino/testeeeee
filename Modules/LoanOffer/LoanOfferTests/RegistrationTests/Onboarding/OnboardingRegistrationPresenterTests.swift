import XCTest
@testable import LoanOffer

final private class OnboardingRegistrationControllerSpy: OnboardingRegistrationDisplay {
    private(set) var setupCallCount = 0
    private(set) var startLoadingCount = 0
    private(set) var stopLoadingCount = 0
    private(set) var displayErrorCount = 0
    private(set) var title: String?
    private(set) var subtitle: String?
    private(set) var buttonTitle: String?
    
    func setup(title: String, subtitle: String, buttonTitle: String) {
        setupCallCount += 1
        self.title = title
        self.subtitle = subtitle
        self.buttonTitle = buttonTitle
    }
    
    func startLoading() {
        startLoadingCount += 1
    }
    
    func stopLoading() {
        stopLoadingCount += 1
    }
    
    func displayError() {
        displayErrorCount += 1
    }
}

final private class OnboardingRegistrationCoordinatorSpy: OnboardingRegistrationCoordinating {
    private(set) var delegate: LoanRegistrationFlowCoordinating?
    private(set) var performActionCallCount = 0
    private(set) var performedAction: OnboardingRegistrationAction?
    
    init(delegate: LoanRegistrationFlowCoordinating) {
        self.delegate = delegate
    }
    
    func perform(action: OnboardingRegistrationAction) {
        performActionCallCount += 1
        performedAction = action
    }
}

final class OnboardingRegistrationPresenterTests: XCTestCase {
    private let delegateSpy = LoanRegistrationFlowCoordinatingSpy()
    private lazy var coordinatorSpy = OnboardingRegistrationCoordinatorSpy(delegate: delegateSpy)
    private let controllerSpy = OnboardingRegistrationControllerSpy()
    
    private lazy var sut: OnboardingRegistrationPresenter = {
        let sut = OnboardingRegistrationPresenter(coordinator: coordinatorSpy)
        sut.viewController = controllerSpy
        return sut
    }()

    func testSetupView_WhenStateIsStart_ShouldDisplayCorrectTexts() {
        sut.setupView(for: .start)
        
        XCTAssertEqual(controllerSpy.setupCallCount, 1)
        XCTAssertEqual(controllerSpy.title, "Agora precisamos fazer\no seu cadastro")
        XCTAssertEqual(controllerSpy.subtitle, "Para solicitar o seu empréstimo vamos precisar de alguns dados pessoais, é coisa rápida! Lembre-se de separar o seu documento RG, CNH, RNE ou Funcional.")
        XCTAssertEqual(controllerSpy.buttonTitle, "Começar")
    }
    
    func testSetupView_WhenStateIsIncomplete_ShouldDisplayCorrectTexts() {
        sut.setupView(for: .incomplete)
        
        XCTAssertEqual(controllerSpy.setupCallCount, 1)
        XCTAssertEqual(controllerSpy.title, "Continue o seu cadastro de\nonde você parou")
        XCTAssertEqual(controllerSpy.subtitle, "Falta pouco para você finalizar o seu cadastro, complete as informações restantes para solicitar o seu empréstimo.")
        XCTAssertEqual(controllerSpy.buttonTitle, "Continuar")
    }
    
    func testSetupView_WhenStateIsReview_ShouldDisplayCorrectTexts() {
        sut.setupView(for: .review)
        
        XCTAssertEqual(controllerSpy.setupCallCount, 1)
        XCTAssertEqual(controllerSpy.title, "Atualize as informações do seu cadastro")
        XCTAssertEqual(controllerSpy.subtitle, "Analisamos o seu cadastro e, para seguir com a sua solicitação, precisamos de mais algumas informações. Falta pouco!")
        XCTAssertEqual(controllerSpy.buttonTitle, "Continuar")
    }
    
    func testDidNextStep_WhenActionIsRegistration_ShouldCallPerformRegistrationAction() {
        let viewController = UIViewController()
        sut.didNextStep(action: .registration(controller: viewController))
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .registration(controller: viewController))
    }
    
    func testDidNextStep_WhenActionIsCancel_ShouldCallPerformCancelAction() {
        sut.didNextStep(action: .cancel)
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .cancel)
    }
    
    func testDidNextStep_WhenActionIsFinish_ShouldCallPerformFinishAction() {
        sut.didNextStep(action: .finish)
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .finish)
    }
}
