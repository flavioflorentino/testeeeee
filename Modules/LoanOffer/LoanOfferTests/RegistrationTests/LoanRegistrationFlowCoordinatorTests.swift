import XCTest
@testable import LoanOffer

final class LoanRegistrationFlowCoordinatorTests: XCTestCase {
    private lazy var navigationSpy = UINavigationControllerSpy()
    private lazy var delegateSpy = LoanOfferCoordinatingSpy()
    private lazy var sut = LoanRegistrationFlowCoordinator(
        from: navigationSpy,
        with: delegateSpy
    )
    
    func testPerformAction_WhenActionIsFinish_ShouldFinishRegistrationFlow() {
        sut.perform(action: .finish)
        
        XCTAssertEqual(delegateSpy.finishHireFlowCallCount, 1)
    }
    
    func testPerformAction_WhenActionIsClose_ShouldCloseView() {
        sut.perform(action: .close)
        
        XCTAssertEqual(navigationSpy.popCallCount, 1)
    }
    
    func testPerformAction_WhenActionIsOnboarding_ShouldStartOnboardingView() {
        sut.perform(action: .onboarding(status: .approved))
        
        XCTAssertEqual(navigationSpy.pushCallCount, 1)
        XCTAssertNotNil(navigationSpy.pushedViewController)
        XCTAssert(navigationSpy.pushedViewController is OnboardingRegistrationViewController)
    }
}
