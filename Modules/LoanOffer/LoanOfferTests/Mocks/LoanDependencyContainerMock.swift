import AnalyticsModule
import Core
import FeatureFlag
import XCTest
@testable import LoanOffer

/// Don't use this class for testing, use LoanDependencyContainerMock
final class LoanDependencyContainer {
    init() {
        fatalError("Use a LoanDependencyContainerMock on test target")
    }
}

final class LoanDependencyContainerMock: LoanDependencies {
    lazy var mainQueue: DispatchQueue = resolve()
    lazy var analytics: AnalyticsProtocol = resolve()
    lazy var safari: SafariViewDependency = resolve()
    lazy var legacy: LoanOfferLegacySetupContract = resolve()
    lazy var featureManager: FeatureManagerContract = resolve()
    lazy var creditRegistration: LoanCreditRegistrationContract = resolve()
    
    private let dependencies: [Any]
    
    init(_ dependencies: Any...) {
        self.dependencies = dependencies
    }
}

extension LoanDependencyContainerMock {
    func resolve<T>() -> T {
        let resolved = dependencies.compactMap { $0 as? T }
                
        switch resolved.first {
        case .none:
            fatalError("LoanDependencyContainerMock could not resolve dependency: \(T.self)\n")
        case .some where resolved.count > 1:
            fatalError("LoanDependencyContainerMock resolved mutiple dependencies for: \(T.self)\n")
        case .some(let mock):
            return mock
        }
    }
    
    func resolve() -> DispatchQueue {
        dependencies.compactMap { $0 as? DispatchQueue }.first ?? DispatchQueue(label: "LoanDependencyContainerMock")
    }
}
