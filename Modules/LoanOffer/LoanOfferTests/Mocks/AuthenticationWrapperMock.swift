import Core
@testable import LoanOffer

final class AuthenticationWrapperMock: AuthenticationContract {
    private(set) var authenticateCallCount = 0
    private(set) var disableBiometricAuthenticationCallCount = 0
    var isAuthenticated: Bool {
        false
    }
    var expectedResult: Result<String, AuthenticationStatus>?
    
    func authenticate(_ completion: @escaping AuthenticationResult) {
        authenticateCallCount += 1
        
        guard let result = expectedResult else {
            debugPrint("\n🚨 Please initialize expected result\n")
            return
        }
        
        completion(result)
    }

    func disableBiometricAuthentication() {
        disableBiometricAuthenticationCallCount += 1
    }
}
