@testable import LoanOffer

final class LoanRegistrationFlowCoordinatingSpy: LoanRegistrationFlowCoordinating {
    var childViewController: [UIViewController] = []
    var viewController: UIViewController?

    private(set) var performActionCallCount = 0
    private(set) var performedAction: RegistrationAction?
        
    func perform(action: RegistrationAction) {
        performActionCallCount += 1
        performedAction = action
    }
}
