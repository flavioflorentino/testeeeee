@testable import LoanOffer

final class FAQWrapperMock: LoanOfferFAQContract {
    private(set) var controllerCallCount = 0
    private(set) var faqUrl: URL?
    
    var expectedResult: UIViewController?
    
    func faqScene(for url: URL) -> UIViewController {
        controllerCallCount += 1
        faqUrl = url
        
        guard let result = expectedResult else {
            fatalError("\n🚨 Please initialize expected result\n")
        }
        
        return result
    }
}
