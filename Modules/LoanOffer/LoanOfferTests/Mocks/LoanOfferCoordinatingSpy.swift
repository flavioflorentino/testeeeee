@testable import LoanOffer

final class LoanOfferCoordinatingSpy: LoanOfferCoordinating {
    var childViewController: [UIViewController] = []
    var viewController: UIViewController?
    
    private(set) var startFlowCallCount = 0
    private(set) var finishHireFlowCallCount = 0
    private(set) var closeCallCount = 0
    private(set) var startedFlow: LoanOfferFlow?
    
    func start(flow: LoanOfferFlow, didFinish: @escaping () -> Void) {
        startFlowCallCount += 1
        startedFlow = flow
    }

    func finish() {
        finishHireFlowCallCount += 1
    }
    
    func close() {
        closeCallCount += 1
    }
}
