import Core
@testable import LoanOffer

enum LegacyDependencies {
    case consumer
    case faq
    case creditRegistration
    case auth
    case deeplink
}

final class LoanOfferSetupMock: LoanOfferLegacySetupContract {
    var consumer: LoanOfferConsumerContract?
    var faq: LoanOfferFAQContract?
    var creditRegistration: LoanCreditRegistrationContract?
    var auth: AuthenticationContract?
    var deeplink: DeeplinkContract?
    var customerSupport: LoanOfferCustomerSupportContract?
    
    init(_ dependencies: LegacyDependencies...) {
        dependencies.forEach {
            switch $0 {
            case .consumer:
                consumer = ConsumerWrapperMock()
            case .faq:
                faq = FAQWrapperMock()
            case .creditRegistration:
                creditRegistration = CreditRegistrationContractMock()
            case .auth:
                auth = AuthenticationWrapperMock()
            case .deeplink:
                deeplink = DeeplinkWrapperMock()
            }
        }
    }
}
