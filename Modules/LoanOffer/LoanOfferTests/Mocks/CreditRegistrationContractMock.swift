@testable import LoanOffer

final class CreditRegistrationContractMock: LoanCreditRegistrationContract {
    private(set) var openCreditRegistrationFlowCount = 0
    
    func start(with navigation: UINavigationController, status: RegistrationStatus, completion: @escaping CreditRegistrationCompletion) {
        openCreditRegistrationFlowCount += 1
    }
}
