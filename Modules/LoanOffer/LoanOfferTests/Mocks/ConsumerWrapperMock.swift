@testable import LoanOffer

final class ConsumerWrapperMock: LoanOfferConsumerContract {
    var consumerBalance: String = ""
    var consumerId: Int = 0
}

