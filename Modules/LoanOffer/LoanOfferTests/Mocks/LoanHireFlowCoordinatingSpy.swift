@testable import LoanOffer

final class LoanHireFlowCoordinatingSpy: LoanHireFlowCoordinating {
    var childViewController: [UIViewController] = []
    var viewController: UIViewController?

    private(set) var performActionCallCount = 0
    private(set) var performedAction: HireAction?
        
    func perform(action: HireAction) {
        performActionCallCount += 1
        performedAction = action
    }
}
