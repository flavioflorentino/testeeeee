import Foundation
import Core
@testable import LoanOffer
import XCTest

final class LoanOfferDeeplinkResolverTests: XCTestCase {
    private var sut = LoanOfferDeeplinkResolver()

    func testCanHandleUrl_WhenIsValidOfferUrlAndUserIsAuthenticated_ShouldReturnHandleable() throws {
        let deeplink = "picpay://picpay/personal-credit/offer"
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.canHandle(url: url, isAuthenticated: true)

        XCTAssertEqual(result, .handleable)
    }

    func testCanHandleUrl_WhenIsValidOfferUrlAndUserIsNotAuthenticated_ShouldReturnOnlyWithAuth() throws {
        let deeplink = "picpay://picpay/personal-credit/offer"
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.canHandle(url: url, isAuthenticated: false)

        XCTAssertEqual(result, .onlyWithAuth)
    }

    func testCanHandleUrl_WhenIsInvalidOfferUrlAndUserIsAuthenticated_ShouldReturnNotHandleable() throws {
        let deeplink = "picpay://picpay/offer"
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.canHandle(url: url, isAuthenticated: true)

        XCTAssertEqual(result, .notHandleable)
    }
}
