import Foundation
import Core
@testable import LoanOffer
import XCTest

final class LoanOfferDeeplinkPathTests: XCTestCase {

    func testInit_WhenIsValidOfferUrlAndOriginIsEmpty_ShouldReturnOriginAsFeed() throws {
        let deeplink = "picpay://picpay/personal-credit/offer"
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = LoanOfferDeeplinkType(url: url)

        XCTAssertEqual(result, .offer(origin: "feed"))
    }

    func testInit_WhenIsValidOfferUrl_ShouldReturnOrigin() throws {
        let deeplink = "picpay://picpay/personal-credit/offer/origin"
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = LoanOfferDeeplinkType(url: url)

        XCTAssertEqual(result, .offer(origin: "origin"))
    }
}
