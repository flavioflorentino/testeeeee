import Foundation

public final class Sentinel: SentinelProtocol {
    public static let shared = Sentinel()
    private init() {}
    
    /// Track, type, name, and attributes for send sentinel, this information is important, with these attributes, you a set custom alert for your provider or a exist alert
    /// - Parameter event: object or enum, that conforms to the protocol
    public func track(_ event: SentinelEventProtocol) {
        for provider in event.providers {
            switch provider {
            case .newRelic:
                trackNewRelic(with: event)
            }
        }
    }
}

extension Sentinel {
    private func trackNewRelic(with event: SentinelEventProtocol) {
        var newAttributes: [String: Any] = [:]
        
        for (key, value) in event.attributes {
            if let value = value as? String, value.isEmpty {
                newAttributes[key] = nil
            } else {
                newAttributes[key] = value
            }
        }
        
        SentinelSetup.appNewRelicInstance?.track(event.type, name: event.name, attributes: newAttributes)
    }
}
