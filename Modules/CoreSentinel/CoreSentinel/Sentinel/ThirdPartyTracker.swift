import Foundation

public protocol ThirdPartyTracker {
    func track(_ type: String, name: String?, attributes: [String: Any]?)
}

public protocol NewRelicContract: ThirdPartyTracker {}
