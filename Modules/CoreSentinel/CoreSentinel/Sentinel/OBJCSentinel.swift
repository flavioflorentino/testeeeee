import Foundation

@objcMembers
public final class OBJCSentinel: NSObject {
    /// Track, type, name, and attributes for send sentinel, this information is important, with these attributes, you a set custom alert for your provider or a exist alert
    /// - Parameters:
    ///   - type: type of params has tracked
    ///   - name: name for type
    ///   - attributes: custom attributes for more information
    ///   - providers: what's platform receive information?
    public static func track(type: String, name: String?, attributes: [String: Any], providers: [String]) {
        let providers = OBJCSentinel().convertProviders(providers)
        let eventTracker = OBJCEventTracker(type: type, name: name, attributes: attributes, providers: providers)
        Sentinel.shared.track(eventTracker)
    }
}

extension OBJCSentinel {
    private func convertProviders(_ stringProviders: [String]) -> [SentinelProvider] {
        stringProviders.compactMap(SentinelProvider.init(rawValue:))
    }
}

private struct OBJCEventTracker: SentinelEventProtocol {
    var type: String
    var name: String?
    var attributes: [String: Any?]
    var providers: [SentinelProvider]
}
