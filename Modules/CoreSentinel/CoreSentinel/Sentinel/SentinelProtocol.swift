import Foundation

public protocol SentinelProtocol {
    func track(_ event: SentinelEventProtocol)
}

public protocol SentinelEventProtocol {
    var type: String { get }
    var name: String? { get }
    var attributes: [String: Any?] { get }
    var providers: [SentinelProvider] { get }
}
