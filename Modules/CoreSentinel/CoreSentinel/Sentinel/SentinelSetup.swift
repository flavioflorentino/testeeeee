import Foundation

public class SentinelSetup {
    static var appNewRelicInstance: NewRelicContract?
    
    public static func inject(_ instance: NewRelicContract?) {
        appNewRelicInstance = instance
    }
}
