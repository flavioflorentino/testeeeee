#!/usr/bin/swift

import Foundation

struct Icon: Decodable {
    let name: String
    let code: String
    
    enum CodingKeys: String, CodingKey {
        case name
        case code = "unicode"
    }
}

guard let json = FileManager.default.contents(atPath: "./unicons.json") else {
    fatalError("Could not find JSON metadata file")
}

let decoder = JSONDecoder()

let icons = try decoder.decode([Icon].self, from: json)

var sortedIcons = icons.sorted {
    $0.name < $1.name
}

var uniconsEnum = ""

uniconsEnum += """
// swiftlint:disable type_body_length file_length
import Foundation

public enum Iconography: String, CaseIterable {\n
"""

sortedIcons.forEach { icon in
    let enumKeyName = icon.name.filteredKeywords().numbersSpelledOut().camelCased(with: "-")
    
    uniconsEnum += """
        case \(enumKeyName) = \"\\u{\(icon.code)}\"\n
    """
}

uniconsEnum += "}\n"

FileManager.default.createFile(atPath: "../UI/DesignSystem/Atoms/Iconography.swift", contents: uniconsEnum.data(using: .utf8), attributes: nil)

public extension String {
    internal var numbers: String {
        filter { "0"..."9" ~= $0 }
    }
    
    func camelCased(with separator: Character) -> String {
        let splitted = split(separator: separator)
        return splitted.reduce(into: "") { acc, name in
            acc = "\(acc)\(!acc.isEmpty ? String(name.capitalized) : String(name))"
        }
    }
    
    func filteredKeywords() -> String {
        switch self {
        case "import":
            return "`import`"
        case "repeat":
            return "`repeat`"
        case "snow-flake":
            return "snow-flake-two"
        default:
            return self
        }
    }
    
    func numbersSpelledOut() -> String {
        guard !self.numbers.isEmpty, let number = Int(self.numbers) else {
            return self
        }

        let numberFormatter = NumberFormatter()
        numberFormatter.locale = Locale(identifier: "en-US")
        numberFormatter.numberStyle = .spellOut

        return "\(numberFormatter.string(from: NSNumber(value: number)) ?? "")-\(self)"
                .replacingOccurrences(of: self.numbers, with: "")
                .replacingOccurrences(of: " ", with: "")
    }
}
