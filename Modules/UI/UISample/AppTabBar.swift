import SwiftUI
import UI

struct AppTabBar: View {
    private enum Tab: CaseIterable {
        case home, tokens, components
        
        var properties: (title: String, imageName: String, view: AnyView) {
            switch self {
            case .home:
                return ("Início", "ico_apollo", HomeFactory.make())
            case .tokens:
                return ("Tokens", "ico_tokens", TokensFactory.make())
            case .components:
                return ("Componentes", "ico_components", ComponentsFactory.make())
            }
        }
    }
    
    var body: some View {
        TabView {
            ForEach(Tab.allCases, id: \.self) { tab in
                tab.properties.view.tabItem {
                    Image(tab.properties.imageName)
                    Text(tab.properties.title)
                }
            }
        }
        .accentColor(Color(Colors.branding600.color))
    }
}

struct AppTabBar_Previews: PreviewProvider {
    static var previews: some View {
        AppTabBar()
    }
}
