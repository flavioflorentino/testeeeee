import SwiftUI
import UI

struct LabelStack: View {
    private let title: String
    private let subtitle: String
    
    init(title: String, subtitle: String) {
        self.title = title
        self.subtitle = subtitle
    }
    
    var body: some View {
        VStack(alignment: .leading,
               spacing: Spacing.base00) {
            Text(title)
                .font(.title3)
                .fontWeight(.bold)
            Text(subtitle)
                .foregroundColor(Color(Colors.grayscale500.color))
        }
    }
}

struct LabelStack_Previews: PreviewProvider {
    static var previews: some View {
        LabelStack(title: "Title", subtitle: "Subtitle")
    }
}
