import Combine
import Foundation
import UIKit.NSDataAsset

public enum JSONError: Error {
    case notFoundFile
    case jsonParse
}

extension JSONError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .notFoundFile:
            return "Arquivo json não encontrado"
        case .jsonParse:
            return "Não foi possível realizar o parse do arquivo"
        }
    }
}
struct Json<T: Decodable> {
    func decode(_ fileName: String) -> AnyPublisher<T, Error> {
        let dataAssetName = NSDataAssetName(fileName)
        guard let data = NSDataAsset(name: dataAssetName, bundle: Bundle.main)?.data else {
            return Fail(error: JSONError.notFoundFile).eraseToAnyPublisher()
        }
        
        return Just(data)
            .decode(type: T.self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
}
