import Foundation

struct DSElementModel: Decodable, Identifiable {
    var id: Int
    let name: String
    let imageName: String
    let status: String
}
