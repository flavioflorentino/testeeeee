import Foundation

struct GroupedTokenModel: Decodable, Identifiable {
    var id = UUID()
    
    let key: String
    let tokens: [TokenModel]
    
    private enum CodingKeys: String, CodingKey {
        case key, tokens
    }
}
