import Foundation

struct TokenModel: Decodable, Identifiable {
    var id = UUID()
    
    let parentId: Int
    let title: String
    let subtitle: String
    let value: String
    
    private enum CodingKeys: String, CodingKey {
        case parentId, title, subtitle, value
    }
}
