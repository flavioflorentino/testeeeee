import SwiftUI

struct ComponentsView<ViewModel>: View where ViewModel: ComponentsViewModelable {
    @ObservedObject var viewModel: ViewModel
    
    var body: some View {
        NavigationView {
            List(viewModel.dataSource) { item in
                let destination = DetailFactory.make(from: item)
                NavigationLink(destination: destination) {
                    TokenCellView(model: item)
                }
            }
            .navigationBarTitle("Components")
        }.onAppear {
            self.viewModel.fetchComponents()
        }
    }
}

struct ComponentsView_Previews: PreviewProvider {
    static var previews: some View {
        ComponentsView(viewModel: ComponentsViewModel())
    }
}
