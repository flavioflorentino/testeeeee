import Combine

protocol ComponentsViewModelable: ObservableObject {
    var dataSource: [DSElementModel] { get set }
    func fetchComponents()
}

final class ComponentsViewModel: ComponentsViewModelable {
    @Published var dataSource: [DSElementModel] = []
    
    private var disposables = Set<AnyCancellable>()
    private let worker: ComponentsWorkable
    
    init(worker: ComponentsWorkable = ComponentsWorker()) {
        self.worker = worker
    }
    
    func fetchComponents() {
        worker.fetchComponents()
            .map { $0 }
            .sink { [weak self] value in
                if case .failure = value {
                    self?.dataSource = []
                }
            } receiveValue: { [weak self] news in
                self?.dataSource = news
            }
            .store(in: &disposables)
    }
}
