import SwiftUI

enum ComponentsFactory {
    static func make() -> AnyView {
        let worker = ComponentsWorker()
        let viewModel = ComponentsViewModel(worker: worker)
        let view = ComponentsView(viewModel: viewModel)
        return AnyView(view)
    }
}
