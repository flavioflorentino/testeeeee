import Combine
protocol ComponentsWorkable: AnyObject {
    func fetchComponents() -> AnyPublisher<[DSElementModel], Error>
}

final  class ComponentsWorker: ComponentsWorkable {
    func fetchComponents() -> AnyPublisher<[DSElementModel], Error> {
       Json<[DSElementModel]>().decode("components")
    }
}
