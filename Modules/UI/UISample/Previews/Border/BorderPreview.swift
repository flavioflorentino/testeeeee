import SwiftUI
import UI

struct BorderPreview: View {
    let value: CGFloat
    
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: CornerRadius.medium)
                .fill(Color(Colors.branding050.color))
            RoundedRectangle(cornerRadius: CornerRadius.medium)
                .stroke(Color(Colors.branding600.color),
                        lineWidth: value)
        }
        .frame(width: 50,
               height: 50,
               alignment: Alignment.center)
    }
}

struct BorderPreview_Previews: PreviewProvider {
    static var previews: some View {
        BorderPreview(value: BorderMapper.map(from: "strong"))
    }
}
