import SwiftUI
import UI

struct BorderMapper {
    static func map(from value: String) -> CGFloat {
        switch value {
        case "none":
            return Border.none
        case "light":
            return Border.light
        case "medium":
            return Border.medium
        case "strong":
            return Border.strong
        default:
            fatalError("Invalid border value: \(value)")
        }
    }
    
    private init() {}
}
