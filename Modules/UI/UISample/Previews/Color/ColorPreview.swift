import SwiftUI
import UI

struct ColorPreview: View {
    let value: Color
    
    var body: some View {
        Rectangle()
            .fill(value)
            .frame(width: 100,
                   height: 50,
                   alignment: Alignment.center)
    }
}

struct ColorPreview_Previews: PreviewProvider {
    static var previews: some View {
        ColorPreview(value: Color(Colors.externalTwitter.color))
    }
}
