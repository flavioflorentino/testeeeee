import SwiftUI
import UI

struct AvatarMapper {
    static func map(from value: String) -> Sizing.ImageView {
        switch value {
        case "xSmall":
            return .xSmall
        case "small":
            return .small
        case "medium":
            return .medium
        case "large":
            return .large
        case "xLarge":
            return .xLarge
        default:
            fatalError("Invalid border value: \(value)")
        }
    }
    
    private init() {}
}
