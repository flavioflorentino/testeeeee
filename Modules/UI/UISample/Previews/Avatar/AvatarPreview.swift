import Foundation
import SwiftUI
import UI

struct AvatarPreview: View {
    let value: Sizing.ImageView
    
    var body: some View {
        Image("thumb_avatar")
            .resizable()
            .frame(width: value.rawValue.width,
                   height: value.rawValue.height,
                   alignment: .center)
    }
}

struct AvatarPreview_Previews: PreviewProvider {
    static var previews: some View {
        AvatarPreview(value: AvatarMapper.map(from: "medium"))
    }
}
