import SwiftUI
import UI

//swiftlint:disable object_literal
struct FeedbackCardViewController: UIViewControllerRepresentable {
    enum FeedbackCardType: String {
        case lowEmphasysFullText
        case lowEmphasysbuttonOnRight
        case lowEmphasysOneAction
        case lowEmphasysTwoAction
        case highEmphasysFullText
        case highEmphasysbuttonOnRight
        case highEmphasysOneAction
        case highEmphasysTwoAction
        
        
        init(name: String) {
            guard let type = FeedbackCardType(rawValue: name) else {
                fatalError("Invalid header name:\(name)")
            }
            self = type
        }
    }
    private let title: String
    private let type: FeedbackCardType
    
    init(title: String, type: String) {
        self.title = title
        self.type = FeedbackCardType(name: type)
    }

    func makeUIViewController(context: Context) -> some UIViewController {
        let navigationController: UINavigationController
        
        let primaryAction = ApolloFeedbackCardButtonAction(title: "Primeira Ação") {
            print("Primeira Ação")
        }
        
        let secondaryAction = ApolloFeedbackCardButtonAction(title: "Segunda Ação") {
            print("Segunda Ação")
        }

        switch type {
        case .lowEmphasysFullText:
            let controller = FeedbackCardController(layoutType: .onlyText, emphasys: .low)
            navigationController = UINavigationController(rootViewController: controller)
        case .lowEmphasysbuttonOnRight:
            let controller = FeedbackCardController(layoutType: .buttonOnRight(primaryAction), emphasys: .low)
            navigationController = UINavigationController(rootViewController: controller)
        case .lowEmphasysOneAction:
            let controller = FeedbackCardController(layoutType: .oneAction(primaryAction), emphasys: .low)
            navigationController = UINavigationController(rootViewController: controller)
        case .lowEmphasysTwoAction:
            let controller = FeedbackCardController(layoutType: .twoActions(primaryAction, secondaryAction), emphasys: .low)
            navigationController = UINavigationController(rootViewController: controller)
        case .highEmphasysFullText:
            let controller = FeedbackCardController(layoutType: .onlyText, emphasys: .high)
            navigationController = UINavigationController(rootViewController: controller)
        case .highEmphasysbuttonOnRight:
            let controller = FeedbackCardController(layoutType: .buttonOnRight(primaryAction), emphasys: .high)
            navigationController = UINavigationController(rootViewController: controller)
        case .highEmphasysOneAction:
            let controller = FeedbackCardController(layoutType: .oneAction(primaryAction), emphasys: .high)
            navigationController = UINavigationController(rootViewController: controller)
        case .highEmphasysTwoAction:
            let controller = FeedbackCardController(layoutType: .twoActions(primaryAction, secondaryAction), emphasys: .high)
            navigationController = UINavigationController(rootViewController: controller)
        }
        
        return navigationController
    }
    
    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {
        guard let navigation = uiViewController as? UINavigationController else { return }
        navigation.viewControllers.first?.title = title
    }
}
//swiftlint:enable object_literal
private final class FeedbackCardController: UIViewController, ViewConfiguration {
    let emphasys: ApolloFeedbackCardViewEmphasisType
    let layoutType: ApolloFeedbackCardViewLayoutType
    let defaultText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras sed nibh ut ex pretium ultrices nec vitae diam."
    
    private lazy var successFeedbackCard = ApolloFeedbackCard(description: defaultText, iconType: .success, layoutType: layoutType, emphasisType: emphasys)
    private lazy var informationFeedbackCard = ApolloFeedbackCard(description: defaultText, iconType: .information, layoutType: layoutType, emphasisType: emphasys)
    private lazy var warningFeedbackCard = ApolloFeedbackCard(description: defaultText, iconType: .warning, layoutType: layoutType, emphasisType: emphasys)
    private lazy var errorFeedbackCard = ApolloFeedbackCard(description: defaultText, iconType: .error, layoutType: layoutType, emphasisType: emphasys)
    
    init(layoutType: ApolloFeedbackCardViewLayoutType, emphasys: ApolloFeedbackCardViewEmphasisType) {
        self.layoutType = layoutType
        self.emphasys = emphasys
        super.init(nibName: nil, bundle: nil)
        buildLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        view.addSubview(successFeedbackCard)
        view.addSubview(informationFeedbackCard)
        view.addSubview(warningFeedbackCard)
        view.addSubview(errorFeedbackCard)
    }
    
    func setupConstraints() {
        successFeedbackCard.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base06)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        informationFeedbackCard.snp.makeConstraints {
            $0.top.equalTo(successFeedbackCard.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        warningFeedbackCard.snp.makeConstraints {
            $0.top.equalTo(informationFeedbackCard.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        errorFeedbackCard.snp.makeConstraints {
            $0.top.equalTo(warningFeedbackCard.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.greaterThanOrEqualTo(Spacing.base03)
        }
    }
    
    func configureViews() {
        view.backgroundColor = Colors.groupedBackgroundPrimary.color
    }
}
