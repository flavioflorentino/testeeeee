import SwiftUI
import UI

struct LoaderAlertDetailView<ViewModel>: View where ViewModel: DetailViewModelable {
    @ObservedObject var viewModel: ViewModel
    @State private var selectedItem: TokenModel?

    let title: String
    
    var body: some View {
        List {
            ForEach(viewModel.dataSource) { section in
                Section(header: getHeader(with: section.key)) {
                    ForEach(section.tokens) { header in
                        let destination = LoaderAlertViewController(title: header.title, type: header.value)
                        NavigationLink(destination: destination) {
                            LabelStack(title: header.title, subtitle: header.subtitle)
                                .padding(.vertical, Spacing.base01)
                        }
                    }
                }
            }
        }
        .navigationBarTitle(title)
        .onAppear {
            self.viewModel.fetchTokens(with: title)
        }
    }
    
    func getHeader(with title: String) -> Text {
        Text(title)
            .foregroundColor(Color(Colors.grayscale500.color))
    }
}
