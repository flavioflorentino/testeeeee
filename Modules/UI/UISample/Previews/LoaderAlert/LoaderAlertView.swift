import SwiftUI
import UI

//swiftlint:disable object_literal
struct LoaderAlertViewController: UIViewControllerRepresentable {
    enum LoaderAlertType: String {
        case `default`
        case withText
        case longText
        
        
        init(name: String) {
            guard let type = LoaderAlertType(rawValue: name) else {
                fatalError("Invalid header name:\(name)")
            }
            self = type
        }
    }
    private let title: String
    private let type: LoaderAlertType
    
    init(title: String, type: String) {
        self.title = title
        self.type = LoaderAlertType(name: type)
    }

    func makeUIViewController(context: Context) -> some UIViewController {
        let navigationController: UINavigationController
        let shortText = "Aguarde..."
        let longText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras sed nibh ut ex pretiu"
        
        switch type {
        case .default:
            let controller = LoaderAlertController(text: "")
            navigationController = UINavigationController(rootViewController: controller)
        case .withText:
            let controller = LoaderAlertController(text: shortText)
            navigationController = UINavigationController(rootViewController: controller)
        case .longText:
            let controller = LoaderAlertController(text: longText)
            navigationController = UINavigationController(rootViewController: controller)
            
        }
        
        return navigationController
    }
    
    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {
        guard let navigation = uiViewController as? UINavigationController else { return }
        navigation.viewControllers.first?.title = title
    }
}

//swiftlint:enable object_literal
private final class LoaderAlertController: UIViewController, ViewConfiguration, ApolloLoadable {
    private let text: String
    private var timer: Timer?
    
    private lazy var loadButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle("Iniciar Load", for: .normal)
        button.addTarget(self, action: #selector(startLoadAction), for: .touchUpInside)
        return button
    }()
    
    init(text: String) {
        self.text = text
        super.init(nibName: nil, bundle: nil)
        buildLayout()
    }
    
    required init?(coder: NSCoder) { nil }
    
    func buildViewHierarchy() {
        view.addSubview(loadButton)
    }
    
    func setupConstraints() {
        loadButton.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureViews() {
        view.backgroundColor = Colors.groupedBackgroundPrimary.color
    }
    
    private func createTimerToStopLoad() {
        timer = Timer.scheduledTimer(
            timeInterval: 5,
            target: self,
            selector: #selector(stopLoadAction),
            userInfo: nil,
            repeats: false
        )
    }
}

@objc
extension LoaderAlertController {
    private func startLoadAction() {
        if text.isEmpty {
            startApolloLoader()
        } else {
            startApolloLoader(loadingText: text)
        }
        
        createTimerToStopLoad()
    }
    
    private func stopLoadAction() {
        timer?.invalidate()
        stopApolloLoader()
    }
}
