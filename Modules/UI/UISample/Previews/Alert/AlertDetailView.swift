import SwiftUI
import UI

struct AlertDetailView<ViewModel>: View where ViewModel: DetailViewModelable {
    @ObservedObject var viewModel: ViewModel
    @State private var selectedItem: TokenModel?

    let title: String
    
    var body: some View {
        List {
            ForEach(viewModel.dataSource) { section in
                Section(header: getHeader(with: section.key)) {
                    ForEach(section.tokens) { alert in
                        let destination = AlertViewController(type: alert.value)
                        NavigationLink(destination: destination) {
                            LabelStack(title: alert.title, subtitle: alert.subtitle)
                                .padding(.vertical, Spacing.base01)
                        }
                    }
                }
            }
        }
        .navigationBarTitle(title)
        .onAppear {
            self.viewModel.fetchTokens(with: title)
        }
    }
    
    func getHeader(with title: String) -> Text {
        Text(title)
            .foregroundColor(Color(Colors.grayscale500.color))
    }
}
