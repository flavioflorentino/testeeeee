import SwiftUI
import UI

enum AlertType: String {
    case error, info, success, warning
    
    init(name: String) {
        guard let type = AlertType(rawValue: name) else {
            fatalError("Invalid header name:\(name)")
        }
        self = type
    }
}

struct AlertViewController: UIViewControllerRepresentable {
    private let type: AlertType
    
    init(type: String) {
        self.type = AlertType(name: type)
    }

    func makeUIViewController(context: Context) -> some AlertDetailViewController {
        return AlertDetailViewController(type: type)
    }
    
    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {}
}

final class AlertDetailViewController: UIViewController, ViewConfiguration {
    var type: AlertType
    
    private let primaryButtonAction = ApolloAlertAction(title: "Primary button", completion: {})
    private let linkButtonAction = ApolloAlertAction(title: "Link button", completion: {})
    
    private lazy var infoButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle("Icon", for: .normal)
        button.addTarget(self, action: #selector(didSelectIconButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var stickerButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle("Sticker", for: .normal)
        button.addTarget(self, action: #selector(didSelectStickerButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var stackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [infoButton, stickerButton])
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .vertical
        stack.spacing = Spacing.base02
        return stack
    }()
    
    init(type: AlertType) {
        self.type = type
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        buildLayout()
    }
    
    @objc private func didSelectIconButton() {
        switch type {
        case .error:
            showAlert(withType: .icon(style: .error))
        case .info:
            showAlert(withType: .icon(style: .info))
        case .success:
            showAlert(withType: .icon(style: .success))
        case .warning:
            showAlert(withType: .icon(style: .warning))
        }
    }
    
    @objc private func didSelectStickerButton() {
        switch type {
        case .error:
            showAlert(withType: .sticker(style: .error))
        case .info:
            showAlert(withType: .sticker(style: .info))
        case .success:
            showAlert(withType: .sticker(style: .success))
        case .warning:
            showAlert(withType: .sticker(style: .warning))
        }
    }
    
    private func showAlert(withType type: ApolloAlertType) {
        self.showApolloAlert(type: type,
                             title: "Title",
                             subtitle: "Subtitle",
                             primaryButtonAction: primaryButtonAction,
                             linkButtonAction: linkButtonAction)
    }
    
    func buildViewHierarchy() {
        view.addSubview(stackView)
    }
    
    func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.trailing.leading.equalToSuperview().inset(Spacing.base03)
        }
    }
}
