import SwiftUI
import UI

enum OpacityMapper {
    static func map(value: String) -> CGFloat {
        switch value {
        case "none":
            return Opacity.none
        case "ultraLight":
            return Opacity.ultraLight
        case "light":
            return Opacity.light
        case "medium":
            return Opacity.medium
        case "strong":
            return Opacity.strong
        case "full":
            return Opacity.full
        default:
            fatalError("Invalid opacity value: \(value)")
        }
    }
}
