import SwiftUI
import UI

struct OpacityPreview: View {
    let value: CGFloat
    
    var body: some View {
        RoundedRectangle(cornerRadius: CornerRadius.medium)
            .fill(Color(Colors.branding600.color))
            .opacity(Double(value))
            .frame(width: 50,
                   height: 50,
                   alignment: Alignment.center)
    }
}

struct OpacityPreview_Previews: PreviewProvider {
    static var previews: some View {
        OpacityPreview(value: OpacityMapper.map(value: "light"))
    }
}
