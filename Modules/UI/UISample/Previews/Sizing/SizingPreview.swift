import SwiftUI
import UI

struct SizingPreview: View {
    let value: CGFloat
    
    var body: some View {
        Rectangle()
            .fill(Color(Colors.branding500.color))
            .frame(width: value,
                   height: value,
                   alignment: Alignment.center)
    }
}

struct SizingPreview_Previews: PreviewProvider {
    static var previews: some View {
        SizingPreview(value: SizingMapper.map(value: "base01"))
    }
}
