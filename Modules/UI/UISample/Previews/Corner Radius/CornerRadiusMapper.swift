import SwiftUI
import UI

struct CornerRadiusMapper {
    static func map(from value: String) -> CGFloat {
        switch value {
        case "none":
            return CornerRadius.none
        case "light":
            return CornerRadius.light
        case "medium":
            return CornerRadius.medium
        case "strong":
            return CornerRadius.strong
        case "full":
            return 25.0
        default:
            fatalError("Invalid corner radius value: \(value)")
        }
    }
    
    private init() {}
}
