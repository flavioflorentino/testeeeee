import SwiftUI
import UI

struct CornerRadiusPreview: View {
    let value: CGFloat
    
    var body: some View {
        RoundedRectangle(cornerRadius: value)
            .fill(Color(Colors.branding600.color))
            .frame(width: 50,
                   height: 50,
                   alignment: Alignment.center)
    }
}

struct CornerRadiusPreview_Previews: PreviewProvider {
    static var previews: some View {
        CornerRadiusPreview(value: CornerRadiusMapper.map(from: "strong"))
    }
}
