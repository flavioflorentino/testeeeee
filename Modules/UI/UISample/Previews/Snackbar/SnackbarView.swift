import SwiftUI
import UI

//swiftlint:disable object_literal
struct SnackbarViewController: UIViewControllerRepresentable {
    enum SnackbarType: String {
        case lowInfo = "low.info"
        case lowWarning = "low.warning"
        case lowSuccess = "low.success"
        case lowError = "low.error"
        case highInfo = "high.info"
        case highWarning = "high.warning"
        case highSuccess = "high.success"
        case highError = "high.error"
        
        init(name: String) {
            guard let type = SnackbarType(rawValue: name) else {
                fatalError("Invalid snackbar name:\(name)")
            }
            self = type
        }
        
        var iconType: ApolloFeedbackCardViewIconType {
            switch self {
            case _ where self.rawValue.hasSuffix("error"):
                return .error
            case _ where self.rawValue.hasSuffix("info"):
                return .information
            case _ where self.rawValue.hasSuffix("warning"):
                return .warning
            case _ where self.rawValue.hasSuffix("success"):
                return .success
            default:
                fatalError("Invalid snackbar type suffix")
            }
        }
        
        var emphasys: ApolloSnackbarEmphasisStyle {
            self.rawValue.hasPrefix("high") ? .high : .low
        }
    }
    private let title: String
    private let type: SnackbarType
    
    init(title: String, type: String) {
        self.title = title
        self.type = SnackbarType(name: type)
    }

    func makeUIViewController(context: Context) -> some UIViewController {
        SnackbarView(type: type)
    }
    
    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {}
}
//swiftlint:enable object_literal
private final class SnackbarView: UIViewController, ViewConfiguration {
    private let snackbarType: SnackbarViewController.SnackbarType
    private let defaultText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
    
    private lazy var stackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [firstActionButton,
                                                   secondActionButton,
                                                   noActionButton])
        stack.axis = .vertical
        stack.spacing = Spacing.base02
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    private lazy var firstActionButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle("Botão à direita", for: .normal)
        button.addTarget(self, action: #selector(didHitFirstButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var secondActionButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle("Botão abaixo", for: .normal)
        button.addTarget(self, action: #selector(didHitSecondButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var noActionButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle("Sem botão", for: .normal)
        button.addTarget(self, action: #selector(didHitNoActionButton), for: .touchUpInside)
        return button
    }()
    
    init(type: SnackbarViewController.SnackbarType) {
        self.snackbarType = type
        super.init(nibName: nil, bundle: nil)
        buildLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        view.addSubview(stackView)
    }
    
    func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.trailing.leading.equalToSuperview().inset(Spacing.base03)
        }
    }
    
    func configureViews() {
        view.backgroundColor = Colors.groupedBackgroundPrimary.color
    }
    
    @objc
    private func didHitFirstButton() {
        let action = ApolloSnackbarButton(title: "Action", position: .right) {
            print("Action tapped")
        }
        self.showSnackbar(ApolloSnackbar(text: defaultText, iconType: snackbarType.iconType, button: action, emphasis: snackbarType.emphasys))
    }
    
    @objc
    private func didHitSecondButton() {
        let action = ApolloSnackbarButton(title: "Action with large text", position: .bottom) {
            print("Action with large text tapped")
        }
        self.showSnackbar(ApolloSnackbar(text: defaultText, iconType: snackbarType.iconType, button: action, emphasis: snackbarType.emphasys))
    }
    
    @objc
    private func didHitNoActionButton() {
        self.showSnackbar(ApolloSnackbar(text: defaultText, iconType: snackbarType.iconType, emphasis: snackbarType.emphasys))
    }
}
