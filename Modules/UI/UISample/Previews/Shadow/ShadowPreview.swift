import SwiftUI
import UI

struct ShadowPreview: View {
    struct Model {
        var radius: CGFloat = 0.0
        var offset = CGSize.zero
    }
    
    let value: Model
    
    var body: some View {
        RoundedRectangle(cornerRadius: CornerRadius.medium)
            .fill(Color(Colors.grayscale100.color))
            .shadow(color: .black,
                    radius: value.radius,
                    x: value.offset.width,
                    y: value.offset.height)
            .frame(width: 50,
                   height: 50,
                   alignment: Alignment.center)
    }
}

struct ShadowPreview_Previews: PreviewProvider {
    static var previews: some View {
        ShadowPreview(value: ShadowMapper.map(value: "none"))
    }
}
