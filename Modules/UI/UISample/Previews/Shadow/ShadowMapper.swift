import SwiftUI
import UI

struct ShadowMapper {
    static func map(value: String) -> ShadowPreview.Model {
        switch value {
        case "radius.none":
            return .init(radius: Shadow.none.radius)
        case "radius.light":
            return .init(radius: Shadow.light.radius)
        case "radius.medium":
            return .init(radius: Shadow.medium.radius)
        case "radius.strong":
            return .init(radius: Shadow.strong.radius)
        case "offset.none":
            return .init(offset: ShadowOffset.none.rawValue)
        case "offset.light":
            return .init(offset: ShadowOffset.light.rawValue)
        case "offset.medium":
            return .init(offset: ShadowOffset.medium.rawValue)
        default:
            fatalError("Invalid shadow value: \(value)")
        }
    }
    
    private init() {}
}
