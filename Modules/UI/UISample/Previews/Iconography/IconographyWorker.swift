import Combine
import SwiftUI
import UI

final class IconographyWorker: DetailWorkable {
    func fetchTokens(with fileName: String) -> AnyPublisher<[GroupedTokenModel], Error> {
        var tokens: [TokenModel] = []
        
        Iconography.allCases.forEach { icon in
            let token = TokenModel(parentId: 3,
                                   title: "\(icon.self)",
                                   subtitle: "-",
                                   value: icon.rawValue)
            tokens.append(token)
        }
        
        let groupedToken = GroupedTokenModel(key: "ASSETS",
                                             tokens: tokens)
        
        return Result.Publisher([groupedToken]).eraseToAnyPublisher()
    }
}
