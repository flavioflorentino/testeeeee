import SwiftUI
import UI

struct IconographyPreview: View {
    let value: String
    
    var body: some View {
        Text(value)
            .font(
                .custom(
                    Typography.icons(.large).font().fontName,
                    size: Typography.icons(.large).font().pointSize
                )
            )
    }
}

struct IconographyPreview_Previews: PreviewProvider {
    static var previews: some View {
        IconographyPreview(value: Iconography.user.rawValue)
    }
}
