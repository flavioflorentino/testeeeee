import SwiftUI

enum Element: Int {
    case border
    case cornerRadius
    case colors
    case iconography
    case opacity
    case shadow
    case sizing
    case spacing
    case typography
    case avatar
    case button
    case header
    case alert
    case feedbackCard
    case snackbar
    case loaderAlert
    
    init(id: Int) {
        guard let element = Element(rawValue: id) else {
            fatalError("Element with invalid id: \(id)")
        }
        self = element
    }
    
    func makePreview(value: String) -> AnyView {
        switch self {
        case .border:
            return AnyView(BorderPreview(value: BorderMapper.map(from: value)))
        case .cornerRadius:
            return AnyView(CornerRadiusPreview(value: CornerRadiusMapper.map(from: value)))
        case .colors:
            return AnyView(ColorPreview(value: Color(hex: value)))
        case .iconography:
            return AnyView(IconographyPreview(value: value))
        case .opacity:
            return AnyView(OpacityPreview(value: OpacityMapper.map(value: value)))
        case .shadow:
            return AnyView(ShadowPreview(value: ShadowMapper.map(value: value)))
        case .sizing:
            return AnyView(SizingPreview(value: SizingMapper.map(value: value)))
        case .spacing:
            return AnyView(SpacingPreview(value: SpacingMapper.map(value: value)))
        case .typography:
            return AnyView(TypographyPreview(value: TypographyMapper.map(value: value)))
        case .avatar:
            return AnyView(AvatarPreview(value: AvatarMapper.map(from: value)))
        case .button:
            return AnyView(ButtonPreview(value: ButtonMapper.map(from: value)))
        default:
            return AnyView(Rectangle())
        }
    }
    
    func makeWorker() -> DetailWorkable {
        switch self {
        case .iconography:
           return IconographyWorker()
        default:
           return DetailWorker()
        }
    }
    
    func makeDetailView<T: DetailViewModelable>(viewModel: T, title: String) -> AnyView {
        switch self {
        case .header:
            return AnyView(HeaderDetailView(viewModel: viewModel, title: title))
        case .alert:
            return AnyView(AlertDetailView(viewModel: viewModel, title: title))
        case .feedbackCard:
            return AnyView(FeedbackCardDetailView(viewModel: viewModel, title: title))
        case .snackbar:
            return AnyView(SnackbarDetailView(viewModel: viewModel, title: title))
        case .loaderAlert:
            return AnyView(LoaderAlertDetailView(viewModel: viewModel, title: title))
        default:
            return AnyView(DetailView(viewModel: viewModel, title: title))
        }
    }
}
