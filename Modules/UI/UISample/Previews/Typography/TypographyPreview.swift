import SwiftUI
import UI

struct TypographyPreview: View {
    struct Model {
        let font: UIFont
        var text: String = "Texto"
    }
    
    let value: Model
    
    var body: some View {
        Text(value.text)
            .font(Font(value.font))
            .foregroundColor(Color(Colors.branding600.color))
    }
}

struct TypographyPreview_Previews: PreviewProvider {
    static var previews: some View {
        TypographyPreview(value: TypographyMapper.map(value: "title.xLarge"))
    }
}
