import SwiftUI
import UI

struct TypographyMapper {
    static func map(value: String) -> TypographyPreview.Model {
        switch value {
        case "title.xLarge":
            return .init(font: Typography.title(.xLarge).font())
        case "title.large":
            return .init(font: Typography.title(.large).font())
        case "title.medium":
            return .init(font: Typography.title(.medium).font())
        case "title.small":
            return .init(font: Typography.title(.small).font())
        case "currency.large":
            return .init(font: Typography.currency(.large).font(), text: "2,00")
        case "currency.medium":
            return .init(font: Typography.currency(.medium).font(), text: "2,00")
        case "bodyPrimary.default":
            return .init(font: Typography.bodyPrimary(.default).font())
        case "bodyPrimary.highlight":
            return .init(font: Typography.bodyPrimary(.highlight).font())
        case "bodySecondary.default":
            return .init(font: Typography.bodySecondary(.default).font())
        case "bodySecondary.highlight":
            return .init(font: Typography.bodySecondary(.highlight).font())
        case "linkPrimary.default":
            return .init(font: Typography.linkPrimary(.default).font())
        case "linkSecondary.default":
            return .init(font: Typography.linkSecondary(.default).font())
        case "caption.default":
            return .init(font: Typography.caption(.default).font())
        case "caption.highlight":
            return .init(font: Typography.caption(.highlight).font())
        default:
            fatalError("Invalid shadow value: \(value)")
        }
    }
    
    private init() {}
}
