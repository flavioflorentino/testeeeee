import SwiftUI
import UI

struct SpacingMapper {
    static func map(value: String) -> CGFloat {
        switch value {
        case "none":
            return Spacing.none
        case "base00":
            return Spacing.base00
        case "base01":
            return Spacing.base01
        case "base02":
            return Spacing.base02
        case "base03":
            return Spacing.base03
        case "base04":
            return Spacing.base04
        case "base05":
            return Spacing.base05
        case "base06":
            return Spacing.base06
        case "base07":
            return Spacing.base07
        case "base08":
            return Spacing.base08
        case "base09":
            return Spacing.base09
        case "base10":
            return Spacing.base10
        case "base11":
            return Spacing.base11
        case "base12":
            return Spacing.base12
        default:
            fatalError("Invalid spacing value: \(value)")
        }
    }
    
    private init() {}
}
