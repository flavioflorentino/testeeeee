import SwiftUI
import UI

struct SpacingPreview: View {
    let value: CGFloat
    
    var body: some View {
        HStack(spacing: value) {
            Rectangle()
                .fill(Color(Colors.branding500.color))
            Rectangle()
                .fill(Color(Colors.business900.color))
        }
        .frame(width: 200,
               height: 50,
               alignment: Alignment.center)
    }
}

struct SpacingPreview_Previews: PreviewProvider {
    static var previews: some View {
        SpacingPreview(value: SpacingMapper.map(value: "base12"))
    }
}
