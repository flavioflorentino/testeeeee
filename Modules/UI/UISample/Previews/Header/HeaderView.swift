import SwiftUI
import UI

//swiftlint:disable object_literal
struct HeaderViewController: UIViewControllerRepresentable {
    enum HeaderType: String {
        case standard
        case large
        case userNavigation
        
        init(name: String) {
            guard let type = HeaderType(rawValue: name) else {
                fatalError("Invalid header name:\(name)")
            }
            self = type
        }
    }
    private let title: String
    private let type: HeaderType
    
    init(title: String, type: String) {
        self.title = title
        self.type = HeaderType(name: type)
    }

    func makeUIViewController(context: Context) -> some UIViewController {
        let navigationController: UINavigationController
        let rootViewController = UIViewController()
        rootViewController.view.backgroundColor = Colors.groupedBackgroundPrimary.color

        switch type {
        case .userNavigation:
            navigationController = UserNavigationController(rootViewController: rootViewController)
        default:
            navigationController = UINavigationController(rootViewController: rootViewController)
        }
        
        return navigationController
    }
    
    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {
        guard let navigation = uiViewController as? UINavigationController else { return }
        navigation.viewControllers.first?.title = title
        
        switch type {
        case .standard:
            navigation.navigationBar.navigationStyle(StandardNavigationStyle())
        case .large:
            navigation.navigationBar.navigationStyle(LargeNavigationStyle())
        default:
            guard let userNavigation = navigation as? UserNavigationController else { return }
            userNavigation.userNavigationBar?.setImage(UIImage(named: "thumb_avatar"))
        }
    }
}
//swiftlint:enable object_literal
