import SwiftUI
import UI

//swiftlint:disable cyclomatic_complexity
struct ButtonMapper {
    static func map(from value: String) -> ButtonView.Model {
        switch value {
        case "primary.default":
            return .init(type: .primary, size: .default)
        case "primary.small":
            return .init(type: .primary, size: .small)
        case "primary.disabled":
            return .init(type: .primary, size: .default, icon: (name: .search, alignment: .right), isEnabled: false)
        case "secondary.default":
            return .init(type: .secondary, size: .default)
        case "secondary.small":
            return .init(type: .secondary, size: .small)
        case "secondary.disabled":
            return .init(type: .secondary, size: .default, icon: (name: .search, alignment: .left), isEnabled: false)
        case "link.default":
            return .init(type: .link, size: .default)
        case "link.small":
            return .init(type: .link, size: .small)
        case "link.disabled":
            return .init(type: .link, size: .default, icon: (name: .search, alignment: .right), isEnabled: false)
        case _ where value.hasPrefix("danger"):
            let size: Sizing.Button = value.contains("small") ? .small : .default
            return .init(type: .danger, size: size)
        default:
            fatalError("Invalid border value: \(value)")
        }
    }
    //swiftlint:enable cyclomatic_complexity
    
    private init() {}
}
