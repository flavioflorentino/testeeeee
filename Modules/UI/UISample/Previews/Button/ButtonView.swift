import SwiftUI
import UI

struct ButtonView: View {
    enum ButtonType {
        case primary, secondary, link, danger
    }
    
    struct Model {
        let type: ButtonType
        var size: Sizing.Button
        var icon: (name: Iconography, alignment: IconAlignment)?
        var isEnabled = true
    }
    
    let model: Model
    
    var body: some View {
        ButtonUIView(model: model)
    }
}

private struct ButtonUIView: UIViewRepresentable {
    let model: ButtonView.Model
    
    func makeUIView(context: Context) -> UIButton {
        let button = UIButton(type: .custom)
        button.setTitle("Button", for: .normal)
        return button
    }
    
    func updateUIView(_ uiView: UIButton, context: Context) {
        uiView.isEnabled = model.isEnabled
        
        switch model.type {
        case .primary:
            uiView.buttonStyle(PrimaryButtonStyle(size: model.size,
                                                  icon: model.icon))
        case .secondary:
            uiView.buttonStyle(SecondaryButtonStyle(size: model.size,
                                                    icon: model.icon))
        case .link:
            uiView.buttonStyle(LinkButtonStyle(size: model.size,
                                               icon: model.icon))
        case .danger:
            uiView.buttonStyle(DangerButtonStyle(size: model.size,
                                                 icon: (name: .exclamationCircle,
                                                        alignment: .left)))
        }
    }
}
