import SwiftUI
import UI

struct ButtonPreview: View {
    let value: ButtonView.Model
    
    var body: some View {
        ButtonView(model: value)
            .frame(width: 120,
                   height: value.size.rawValue,
                   alignment: .center)
    }
}

struct ButtonPreview_Previews: PreviewProvider {
    static var previews: some View {
        ButtonPreview(value: ButtonView.Model(type: .secondary,
                                              size: .small,
                                              icon: (name: .search,
                                                     alignment: .left)))
    }
}
