import Combine

protocol HomeViewModelable: ObservableObject {
    var dataSource: [DSElementModel] { get set }
    func fetchNews()
}

final class HomeViewModel: HomeViewModelable {
    @Published var dataSource: [DSElementModel] = []
    
    private var disposables = Set<AnyCancellable>()
    private let worker: HomeWorkable
    
    init(worker: HomeWorkable = HomeWorker()) {
        self.worker = worker
    }
    
    func fetchNews() {
        worker.fetchNews()
            .map { $0 }
            .sink { [weak self] value in
                if case .failure = value {
                    self?.dataSource = []
                }
            } receiveValue: { [weak self] news in
                self?.dataSource = news
            }
            .store(in: &disposables)
    }
}
