import SwiftUI

enum HomeFactory {
    static func make() -> AnyView {
        let worker = HomeWorker()
        let viewModel = HomeViewModel(worker: worker)
        let view = HomeView(viewModel: viewModel)
        return AnyView(view)
    }
}
