import SwiftUI
import UI

struct HomeView<ViewModel>: View where ViewModel: HomeViewModelable {
    @ObservedObject var viewModel: ViewModel
    
    var body: some View {
        NavigationView {
            ScrollView(showsIndicators: false) {
                VStack(alignment: .leading, spacing: Spacing.base02) {
                    imageContainer
                    newsCollection
                }
            }
            .padding(Spacing.base02)
            .onAppear {
                self.viewModel.fetchNews()
            }
            .navigationBarHidden(true)
        }
    }
    
    var imageContainer: some View {
        ZStack(alignment: .bottomLeading) {
            Image("bg_apollo")
                .resizable()
                .aspectRatio(contentMode: .fill)
        }
        .clipShape(RoundedRectangle(cornerRadius: Spacing.base02))
    }
    
    var newsCollection: some View {
        VStack(alignment: .leading, spacing: Spacing.base01) {
            Text("Novidades")
                .font(.title)
                .fontWeight(.bold)
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(spacing: Spacing.base02) {
                    ForEach(viewModel.dataSource) { item in
                        let destination = DetailFactory.make(from: item)
                        NavigationLink(destination: destination) {
                            NewCell(element: item)
                        }
                        .accentColor(Color.black)
                    }
                }
            }
        }
    }
}
