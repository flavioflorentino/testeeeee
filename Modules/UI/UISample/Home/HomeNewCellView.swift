import SwiftUI
import UI

struct NewCell: View {
    let element: DSElementModel
    
    var body: some View {
        VStack(alignment: .leading, spacing: Spacing.base02) {
            Image(element.imageName)
                .clipShape(RoundedRectangle(cornerRadius: Spacing.base02))
            Text(element.name)
                .fontWeight(.bold)
        }
    }
}
