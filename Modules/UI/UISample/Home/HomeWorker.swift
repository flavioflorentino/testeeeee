import Combine

protocol HomeWorkable: AnyObject {
    func fetchNews() -> AnyPublisher<[DSElementModel], Error>
}

final  class HomeWorker: HomeWorkable {
    func fetchNews() -> AnyPublisher<[DSElementModel], Error> {
        Json<[DSElementModel]>().decode("news")
    }
}
