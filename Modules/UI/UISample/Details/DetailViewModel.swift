import Combine

protocol DetailViewModelable: ObservableObject {
    var dataSource: [GroupedTokenModel] { get set }
    func fetchTokens(with fileName: String)
}

final class DetailViewModel: DetailViewModelable {
    @Published var dataSource: [GroupedTokenModel] = []
    
    private var disposables = Set<AnyCancellable>()
    private let worker: DetailWorkable
    
    init(worker: DetailWorkable = DetailWorker()) {
        self.worker = worker
    }
    
    func fetchTokens(with fileName: String) {
        worker.fetchTokens(with: fileName.replacingOccurrences(of: " ", with: "-").lowercased())
            .map { $0 }
            .sink { [weak self] value in
                if case .failure = value {
                    self?.dataSource = []
                }
            } receiveValue: { [weak self] tokens in
                self?.dataSource = tokens
            }
            .store(in: &disposables)
    }
}
