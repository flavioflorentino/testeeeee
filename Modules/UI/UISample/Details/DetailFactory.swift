import SwiftUI

enum DetailFactory {
    static func make(from dsElement: DSElementModel) -> AnyView {
        let element = Element(id: dsElement.id)
        let worker: DetailWorkable = element.makeWorker()
        let viewModel = DetailViewModel(worker: worker)
        let view = element.makeDetailView(viewModel: viewModel, title: dsElement.name)
        return AnyView(view)
    }
}
