import SwiftUI
import UI

struct DetailCellView: View {
    let model: TokenModel
    
    var body: some View {
        HStack {
            LabelStack(title: model.title,
                       subtitle: model.subtitle)
            Spacer()
            Element(rawValue: model.parentId)?
                .makePreview(value: model.value)
        }.padding(.vertical, Spacing.base01)
    }
}

struct DetailCellView_Previews: PreviewProvider {
    static var previews: some View {
        DetailCellView(model: TokenModel(parentId: 0,
                                         title: "Title",
                                         subtitle: "Subtitle",
                                         value: "medium"))
    }
}
