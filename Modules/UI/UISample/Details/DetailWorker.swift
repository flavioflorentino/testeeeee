import Combine
import UI

protocol DetailWorkable: AnyObject {
    func fetchTokens(with fileName: String) -> AnyPublisher<[GroupedTokenModel], Error>
}

final class DetailWorker: DetailWorkable {
    func fetchTokens(with fileName: String) -> AnyPublisher<[GroupedTokenModel], Error> {
        Json<[GroupedTokenModel]>().decode(fileName)
    }
}
