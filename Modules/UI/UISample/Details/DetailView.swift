import SwiftUI
import UI

struct DetailView<ViewModel>: View where ViewModel: DetailViewModelable {
    @ObservedObject var viewModel: ViewModel
    let title: String
    
    var body: some View {
        List {
            ForEach(viewModel.dataSource) { item in
                Section(header: getHeader(with: item.key)) {
                    ForEach(item.tokens) { token in
                        DetailCellView(model: token)
                    }
                }
            }
        }
        .navigationBarTitle(title)
        .onAppear {
            self.viewModel.fetchTokens(with: title)
        }
    }
    
    func getHeader(with title: String) -> Text {
        Text(title)
            .foregroundColor(Color(Colors.grayscale500.color))
    }
}
