import Combine
protocol TokensWorkable: AnyObject {
    func fetchTokens() -> AnyPublisher<[DSElementModel], Error>
}

final  class TokensWorker: TokensWorkable {
    func fetchTokens() -> AnyPublisher<[DSElementModel], Error> {
       Json<[DSElementModel]>().decode("tokens")
    }
}
