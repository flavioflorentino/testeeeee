import SwiftUI

struct TokensView<ViewModel>: View where ViewModel: TokensViewModelable {
    @ObservedObject var viewModel: ViewModel
    
    var body: some View {
        NavigationView {
            List(viewModel.dataSource) { item in
                let destination = DetailFactory.make(from: item)
                NavigationLink(destination: destination) {
                    TokenCellView(model: item)
                }
            }
            .navigationBarTitle("Tokens")
        }.onAppear {
            self.viewModel.fetchNews()
        }
    }
}

struct TokensView_Previews: PreviewProvider {
    static var previews: some View {
        TokensView(viewModel: TokensViewModel())
    }
}
