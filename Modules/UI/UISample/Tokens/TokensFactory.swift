import SwiftUI

enum TokensFactory {
    static func make() -> AnyView {
        let worker = TokensWorker()
        let viewModel = TokensViewModel(worker: worker)
        let view = TokensView(viewModel: viewModel)
        return AnyView(view)
    }
}
