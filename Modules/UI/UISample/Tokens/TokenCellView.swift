import SwiftUI
import UI

struct TokenCellView: View {
    let model: DSElementModel
    
    init(model: DSElementModel) {
        self.model = model
    }
    
    var body: some View {
        HStack(spacing: Spacing.base02) {
            Image(model.imageName)
            LabelStack(title: model.name,
                       subtitle: model.status)
        }
        .padding(Spacing.base01)
    }
}

struct TokenCellView_Previews: PreviewProvider {
    static var previews: some View {
        TokenCellView(model: DSElementModel(id: 0,
                                            name: "Border",
                                            imageName: "thumb_border",
                                            status: "Disponível"))
    }
}
