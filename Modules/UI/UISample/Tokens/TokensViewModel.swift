import Combine

protocol TokensViewModelable: ObservableObject {
    var dataSource: [DSElementModel] { get set }
    func fetchNews()
}

final class TokensViewModel: TokensViewModelable {
    @Published var dataSource: [DSElementModel] = []
    
    private var disposables = Set<AnyCancellable>()
    private let worker: TokensWorkable
    
    init(worker: TokensWorkable = TokensWorker()) {
        self.worker = worker
    }
    
    func fetchNews() {
        worker.fetchTokens()
            .map { $0 }
            .sink { [weak self] value in
                if case .failure = value {
                    self?.dataSource = []
                }
            } receiveValue: { [weak self] news in
                self?.dataSource = news
            }
            .store(in: &disposables)
    }
}
