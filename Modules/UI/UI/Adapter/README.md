# Adapter

Uma biblioteca que permite uma abordagem declarativa para preencher  `UICollectionView` e  `UITableView`

Atualmente suportamos apenas  `UITableView`

## Requisitos

- iOS 10.3 or later
- Xcode 10.0 or later

## Motivação

Na nossa atual arquitetura MVVM-C (com elementos do VIPER), os métodos inputs do ViewModel devem possuir retorno Void, deixando toda comunicação seguir de maneira unidirecional. Mas Isso gerava incompatibilidade para implementar os métodos de `DataSource` tradicionais. Exemplo:

UITableViewDataSource:

```swift
func numberOfSections(in tableView: UITableView) -> Int {
    return viewModel.inputs.numberOfSections()
}

func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.inputs.numberOfRows(section: section)
}

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: defaultCard, for: indexPath) as? CardRechargeTableViewCell else {
        return UITableViewCell()
    }

    let row = indexPath.row
    let card = viewModel.inputs.cardRecharge(index: row)
    cell.configure(with: card)

    return cell
}
```

Alguns métodos como o `viewModel.inputs.numberOfSections()` apesar de inputs tinha que retornar um valor, ferindo nossa ideia de arquitetura.

## Soluçao para UITableView

Para usar a abordagem declarativa  temos que definir um `dataSource: TableViewHandler<Model, Cell>?` no escopo de classe, a fim de reter essa instância, caso contrário ela seria desalocada por `tableView.dataSource` ser weak.

Depois basta implementrar `TableViewHandler`

**Obs: Essa solução não exclui a nescessidade de implementar o `register` e outros metodos da TableView.** 

```swift

private var dataSource: TableViewHandler<Person, NameViewCell>?

...

extension ViewController: NameViewModelOutputs {
    func reload(data: [Person]) {
        dataSource = TableViewHandler(data: data, cellType: NameViewCell.self) { row, model, cell in
            cell.configure(with: model)
        }

        tableView.dataSource = dataSource
        tableView.reloadData()
    }
}
```

## License

Adapter é liberado para uso interno da [PicPay](https://www.picpay.com/site)
