import UIKit

@available(*, deprecated, message: "Use CollectionViewDataSource instead.")
public class CollectionViewHandler<T, Cell: UICollectionViewCell>: NSObject, UICollectionViewDataSource {
    public typealias ConfigureCellHandler = (Int, T, Cell) -> Void
    public typealias SupplementaryViewHandler = (String, IndexPath) -> UICollectionReusableView?
    
    private var data: [T]
    private let cellType: Cell.Type
    private let configureCellHandler: ConfigureCellHandler
    public var supplementaryView: SupplementaryViewHandler?
    
    public init(data: [T], cellType: Cell.Type = Cell.self, configureCell: @escaping ConfigureCellHandler) {
        self.data = data
        self.cellType = cellType
        self.configureCellHandler = configureCell
        
        super.init()
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        data.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellType.identifier, for: indexPath) as? Cell else {
            return UICollectionViewCell()
        }
        
        configureCellHandler(indexPath.row, data[indexPath.row], cell)
        
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        supplementaryView?(kind, indexPath) ?? UICollectionReusableView()
    }
    
    public func deleteItem(at indexPath: IndexPath) {
        data.remove(at: indexPath.row)
    }
}
