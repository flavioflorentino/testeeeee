import UIKit
import SkeletonView

public enum Header {
    case string(String)
    case view(UIView)
}

extension Header: ExpressibleByStringLiteral {
    public init(stringLiteral value: String) {
        self = Header.string(value)
    }
}

public struct Section<S, T> {
    public let header: S?
    public let items: [T]
    
    public init(header: S? = nil, items: [T]) {
        self.header = header
        self.items = items
    }
}

public enum RowHeightType {
    case custom(CGFloat = UITableView.automaticDimension)
    case full
}

@available(*, deprecated, message: "Use TableViewDataSource instead.")
public class TableViewHandler<S, T, Cell: UITableViewCell>: NSObject, UITableViewDataSource, UITableViewDelegate {
    public typealias ConfigureCellHandler = (Int, T, Cell) -> Void
    public typealias CellIdentifier = (IndexPath, T) -> String
    public typealias ConfigureMultiCellHandler = (Int, T, UITableViewCell) -> Void
    public typealias ConfigureSectionHeaderHandler = (Int, Section<S, T>) -> Header?
    public typealias ConfigureDidSelectRowHandler = (IndexPath, T) -> Void
    public typealias ConfigureLeadingSwipeActionHandler = (IndexPath, T) -> SwipeActionsConfiguration?
    public typealias ConfigureTrailingSwipeActionHandler = (IndexPath, T) -> SwipeActionsConfiguration?
    public typealias ConfigureCellHeight = (T) -> RowHeightType
    public typealias ScrollViewDidScrollHandler = () -> Void
    public typealias WillDisplayCellHandler = (IndexPath, T) -> Void

    private var data: [Section<S, T>]
    private let cellType: Cell.Type
    private let configureCellHandler: ConfigureCellHandler
    private let cellIdentifier: CellIdentifier?
    private let configureMultiCellHandler: ConfigureMultiCellHandler
    private var configureSectionHeader: ConfigureSectionHeaderHandler?
    private var configureDidSelectRowHandler: ConfigureDidSelectRowHandler?
    private var configureDidDeselectRowHandler: ConfigureDidSelectRowHandler?
    private var configureLeadingSwipeActionHandler: ConfigureLeadingSwipeActionHandler?
    private var configureTrailingSwipeActionHandler: ConfigureTrailingSwipeActionHandler?
    private var configureCellHeightHandler: ConfigureCellHeight?
    private var scrollViewDidScrollHandler: ScrollViewDidScrollHandler?
    private var willDisplayCellHandler: WillDisplayCellHandler?
    
    /// Initializes a table view handler
    /// - Parameters:
    ///   - data: The table view data
    ///   - cellType: The registered cell type
    ///   - configureCell: A configuration block to setup your cell with your data
    ///   - configureSection: A configuration block to setup a table view section header
    ///   - configureDidSelectRow: A call back block to handle the selection of a cell
    ///   - configureLeadingSwipeActionHandler: A configuration block to define leading swipe actions
    ///   - configureTrailingSwipeActionHandler: A configuration block to define trailing swipe actions
    /// - Note: The `configureLeadingSwipeActionHandler` is only available for iOS 11 and above
    public init(
        data: [Section<S, T>],
        cellType: Cell.Type = Cell.self,
        configureCell: @escaping ConfigureCellHandler,
        configureSection: ConfigureSectionHeaderHandler? = nil,
        configureDidSelectRow: ConfigureDidSelectRowHandler? = nil,
        configureDidDeselectRow: ConfigureDidSelectRowHandler? = nil,
        configureLeadingSwipeActionHandler: ConfigureLeadingSwipeActionHandler? = nil,
        configureTrailingSwipeActionHandler: ConfigureTrailingSwipeActionHandler? = nil,
        scrollViewDidScrollHandler: ScrollViewDidScrollHandler? = nil,
        willDisplayCellHandler: WillDisplayCellHandler? = nil
    ) {
        self.data = data
        self.cellType = cellType
        self.configureCellHandler = configureCell
        self.cellIdentifier = nil
        self.configureMultiCellHandler = { _, _, _ in }
        self.configureSectionHeader = configureSection
        self.configureDidSelectRowHandler = configureDidSelectRow
        self.configureDidDeselectRowHandler = configureDidDeselectRow
        self.configureLeadingSwipeActionHandler = configureLeadingSwipeActionHandler
        self.configureTrailingSwipeActionHandler = configureTrailingSwipeActionHandler
        self.scrollViewDidScrollHandler = scrollViewDidScrollHandler
        self.willDisplayCellHandler = willDisplayCellHandler
        
        super.init()
    }
    
    /// Initializes a table view handler for multiples cell types
    /// - Parameters:
    ///   - data: The table view data
    ///   - cellIndentifier: A closure block that should return cell identifier based on IndexPath and Data
    ///   - configureCell: A configuration block to setup your cell with your data
    ///   - configureSection: A configuration block to setup a table view section header
    ///   - configureDidSelectRow: A call back block to handle the selection of a cell
    ///   - configureLeadingSwipeActionHandler: A configuration block to define leading swipe actions
    ///   - configureTrailingSwipeActionHandler: A configuration block to define trailing swipe actions
    /// - Note: The `configureLeadingSwipeActionHandler` is only available for iOS 11 and above
    public init(
        data: [Section<S, T>],
        cellIdentifier: @escaping CellIdentifier,
        configureMultiCell: @escaping ConfigureMultiCellHandler,
        configureSection: ConfigureSectionHeaderHandler? = nil,
        configureDidSelectRow: ConfigureDidSelectRowHandler? = nil,
        configureDidDeselectRow: ConfigureDidSelectRowHandler? = nil,
        configureLeadingSwipeActionHandler: ConfigureLeadingSwipeActionHandler? = nil,
        configureTrailingSwipeActionHandler: ConfigureTrailingSwipeActionHandler? = nil,
        configureCellHeightHandler: ConfigureCellHeight? = nil,
        scrollViewDidScrollHandler: ScrollViewDidScrollHandler? = nil,
        willDisplayCellHandler: WillDisplayCellHandler? = nil
    ) {
        self.data = data
        self.cellType = Cell.self
        self.configureCellHandler = { _, _, _  in }
        self.cellIdentifier = cellIdentifier
        self.configureMultiCellHandler = configureMultiCell
        self.configureSectionHeader = configureSection
        self.configureDidSelectRowHandler = configureDidSelectRow
        self.configureDidDeselectRowHandler = configureDidDeselectRow
        self.configureLeadingSwipeActionHandler = configureLeadingSwipeActionHandler
        self.configureTrailingSwipeActionHandler = configureTrailingSwipeActionHandler
        self.configureCellHeightHandler = configureCellHeightHandler
        self.scrollViewDidScrollHandler = scrollViewDidScrollHandler
        self.willDisplayCellHandler = willDisplayCellHandler
        
        super.init()
    }
    
    public func sectionHeader(completion: @escaping ConfigureSectionHeaderHandler) {
        configureSectionHeader = completion
    }
    
    public func didSelectRow(completion: @escaping ConfigureDidSelectRowHandler) {
        configureDidSelectRowHandler = completion
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        data.count
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data[section].items.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cellIdentifier = cellIdentifier else {
            return singleCell(tableView, cellForRowAt: indexPath)
        }
        
        return multiCell(tableView, cellForRowAt: indexPath, identifier: cellIdentifier)
    }
    
    private func singleCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellType.identifier) as? Cell else {
            return UITableViewCell()
        }
        
        configureCellHandler(indexPath.row, data[indexPath.section].items[indexPath.row], cell)
        
        return cell
    }
    
    private func multiCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath, identifier: CellIdentifier) -> UITableViewCell {
        let cellData = data[indexPath.section].items[indexPath.row]
        let cellIdentifier = identifier(indexPath, cellData)
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) else {
            return UITableViewCell()
        }
        
        configureMultiCellHandler(indexPath.row, cellData, cell)
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard case let Header.string(title)? = configureSectionHeader?(section, data[section]) else {
            return nil
        }

        return title
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard case let Header.view(view)? = configureSectionHeader?(section, data[section]) else {
            return nil
        }
        
        return view
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard configureSectionHeader?(section, data[section]) != nil else {
            return .leastNonzeroMagnitude
        }

        return UITableView.automaticDimension
    }

    public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        tableView.sectionFooterHeight
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let model = data[indexPath.section].items[indexPath.row]
        guard let height = configureCellHeightHandler?(model) else {
            return tableView.rowHeight
        }
        switch height {
        case let .custom(size):
            return size
        case .full:
            var otherSizes = -tableView.contentOffset.y
            if let headerSize = tableView.tableHeaderView?.frame.height {
                otherSizes += headerSize
            }
            if let footerSize = tableView.tableFooterView?.frame.height {
                otherSizes += footerSize
            }
            return tableView.frame.size.height - otherSizes
        }
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        configureDidSelectRowHandler?(indexPath, data[indexPath.section].items[indexPath.row])
    }
    
    public func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        configureDidDeselectRowHandler?(indexPath, data[indexPath.section].items[indexPath.row])
    }
    
    // swiftlint:disable discouraged_optional_collection
    public func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        // swiftlint:enable discouraged_optional_collection
        let swipeActionConfiguration = configureLeadingSwipeActionHandler?(indexPath, data[indexPath.section].items[indexPath.row])
        return swipeActionConfiguration?.actions.map { $0.rowAction }
    }
    
    @available(iOS 11.0, *)
    public func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        configureLeadingSwipeActionHandler?(indexPath, data[indexPath.section].items[indexPath.row])?.swipeActionsConfiguration
    }
    
    @available(iOS 11.0, *)
    public func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        configureTrailingSwipeActionHandler?(indexPath, data[indexPath.section].items[indexPath.row])?.swipeActionsConfiguration
    }
    
    public func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        cellType.identifier
    }
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollViewDidScrollHandler?()
    }

    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        willDisplayCellHandler?(indexPath, data[indexPath.section].items[indexPath.row])
    }
}
