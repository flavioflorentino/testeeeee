import UIKit

/// Contextual Action compatible with older versions
public struct ContextualAction {
    /// Contextual action style
    /// - Note: Only available when running on iOS 11 or later
    public enum Style {
        case normal
        case destructive
        
        /// Fallback action style to prior iOS 11
        @available(iOS, deprecated: 11.0, message: "Use contextual action related APIs instead")
        var rowActionStyle: UITableViewRowAction.Style {
            switch self {
            case .normal:
                return .normal
            case .destructive:
                return .destructive
            }
        }
        
        @available(iOS, introduced: 11.0)
        var contextualActionStyle: UIContextualAction.Style {
            switch self {
            case .normal:
                return .normal
            case .destructive:
                return .destructive
            }
        }
    }
    
    public typealias Handler = (_ action: ContextualAction, _ completion: @escaping (Bool) -> Void) -> Void
    
    public let style: Style
    public let title: String?
    public let handler: Handler?
    
    public init(style: Style, title: String?, handler: Handler?) {
        self.style = style
        self.title = title
        self.handler = handler
    }
    
    @available(iOS, deprecated: 11.0, message: "Use contextual action related APIs instead")
    internal var rowAction: UITableViewRowAction {
        UITableViewRowAction(style: style.rowActionStyle, title: title) { _, _ in
            self.handler?(self, { _ in })
        }
    }
    @available(iOS 11.0, *)
    internal var contextualAction: UIContextualAction {
        UIContextualAction(style: style.contextualActionStyle, title: title, handler: { _, _, completion in
            self.handler?(self, completion)
        })
    }
}
