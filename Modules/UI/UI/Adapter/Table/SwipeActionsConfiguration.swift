import UIKit

/// Swipe action configuration compatible with iOS versions prior to iOS 11
public struct SwipeActionsConfiguration {
    public let actions: [ContextualAction]
    
    /// default YES, set to NO to prevent a full swipe from performing the first action
    /// - Note: Only available when running on iOS 11 or later
    public var performsFirstActionWithFullSwipe: Bool = true
    
    public init(actions: [ContextualAction]) {
        self.actions = actions
    }
    
    @available(iOS 11.0, *)
    /// UIKit swipe actions configuration
    internal var swipeActionsConfiguration: UISwipeActionsConfiguration {
        let swipeActionsConfiguration = UISwipeActionsConfiguration(actions: actions.map { $0.contextualAction })
        swipeActionsConfiguration.performsFirstActionWithFullSwipe = performsFirstActionWithFullSwipe
        return swipeActionsConfiguration
    }
}
