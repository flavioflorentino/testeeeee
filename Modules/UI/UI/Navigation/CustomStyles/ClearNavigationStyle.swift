import UIKit

public final class ClearNavigationStyle: DefaultNavigationStyle {
    let prefersLargeTitle: Bool
    
    public init(prefersLargeTitle: Bool = true) {
        self.prefersLargeTitle = prefersLargeTitle
    }
    
    override public func makeStyle(_ style: StyleCore<DefaultNavigationStyle.View>) {
        super.makeStyle(style)
        style.with(\.tintColor, Colors.brandingBase.color)
        style.with(\.barTintColor, Colors.backgroundPrimary.color)
        style.with(\.isTranslucent, false)
        
        if #available(iOS 13.0, *) {
            let defaultBarAppearance = UINavigationBarAppearance()
            defaultBarAppearance.configureWithTransparentBackground()
            defaultBarAppearance.shadowColor = .none
            defaultBarAppearance.backgroundColor = Colors.backgroundPrimary.color
            
            let standardBarAppearance = UINavigationBarAppearance()
            standardBarAppearance.configureWithDefaultBackground()
            standardBarAppearance.shadowColor = .none
            standardBarAppearance.backgroundColor = Colors.backgroundPrimary.color
            
            style.with(\.standardAppearance, standardBarAppearance)
            style.with(\.compactAppearance, standardBarAppearance)
            style.with(\.scrollEdgeAppearance, defaultBarAppearance)
        }
        
        if #available(iOS 11.0, *) {
            style.with(\.prefersLargeTitles, prefersLargeTitle)
        }
    }
}
