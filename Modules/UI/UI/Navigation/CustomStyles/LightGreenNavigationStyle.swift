import UIKit

public final class LightGreenNavigationStyle: DefaultNavigationStyle {
    override public init() {}
    
    override public func makeStyle(_ style: StyleCore<DefaultNavigationStyle.View>) {
        style.with(\.tintColor, Colors.white.lightColor)
        style.with(\.barTintColor, Colors.branding400.color)
        style.with(\.titleTextAttributes, [.foregroundColor: Colors.white.lightColor])
        style.with(\.isTranslucent, false)
        
        if #available(iOS 13.0, *) {
            let defaultBarAppearance = UINavigationBarAppearance()
            defaultBarAppearance.configureWithTransparentBackground()
            defaultBarAppearance.backgroundColor = Colors.branding400.color
            
            let standardBarAppearance = UINavigationBarAppearance()
            standardBarAppearance.configureWithDefaultBackground()
            standardBarAppearance.backgroundColor = Colors.branding400.color
            standardBarAppearance.titleTextAttributes = [.foregroundColor: Colors.white.lightColor]
            standardBarAppearance.shadowColor = .none
            
            style.with(\.standardAppearance, standardBarAppearance)
            style.with(\.compactAppearance, standardBarAppearance)
            style.with(\.scrollEdgeAppearance, defaultBarAppearance)
        } else {
            style.with(\.backgroundColor, Colors.branding400.color)
        }
        
        if #available(iOS 11.0, *) {
            style.with(\.prefersLargeTitles, false)
        }
    }
}
