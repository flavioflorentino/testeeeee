import UIKit

public final class CustomColorNavigationStyle: DefaultNavigationStyle {
    private let customColor: UIColor
    
    public init(customColor: UIColor) {
        self.customColor = customColor
    }
    
    override public func makeStyle(_ style: StyleCore<DefaultNavigationStyle.View>) {
        style.with(\.tintColor, Colors.white.lightColor)
        style.with(\.barTintColor, customColor)
        style.with(\.titleTextAttributes, [.foregroundColor: Colors.white.lightColor])
        style.with(\.isTranslucent, false)
        
        if #available(iOS 13.0, *) {
            let defaultBarAppearance = UINavigationBarAppearance()
            defaultBarAppearance.configureWithTransparentBackground()
            defaultBarAppearance.backgroundColor = customColor
            
            let standardBarAppearance = UINavigationBarAppearance()
            standardBarAppearance.configureWithDefaultBackground()
            standardBarAppearance.backgroundColor = customColor
            standardBarAppearance.titleTextAttributes = [.foregroundColor: Colors.white.lightColor]
            standardBarAppearance.shadowColor = .none
            
            style.with(\.standardAppearance, standardBarAppearance)
            style.with(\.compactAppearance, standardBarAppearance)
            style.with(\.scrollEdgeAppearance, defaultBarAppearance)
        } else {
            style.with(\.backgroundColor, customColor)
        }
        
        if #available(iOS 11.0, *) {
            style.with(\.prefersLargeTitles, false)
        }
    }
}
