import UIKit

public final class RedesignNavigationStyle: DefaultNavigationStyle {
    let prefersLargeTitle: Bool
    
    public init(prefersLargeTitle: Bool = false) {
        self.prefersLargeTitle = prefersLargeTitle
    }
    
    override public func makeStyle(_ style: StyleCore<DefaultNavigationStyle.View>) {
        super.makeStyle(style)
        style.with(\.tintColor, Colors.grayscale750.color)
        
        // This color will be added to the DS later
        // swiftlint:disable object_literal
        let backgroundColor = UIColor(red: 245, green: 245, blue: 245, alpha: 1)
        
        style.with(\.barTintColor, backgroundColor)
        style.with(\.isTranslucent, false)
        
        if #available(iOS 13.0, *) {
            let defaultBarAppearance = UINavigationBarAppearance()
            defaultBarAppearance.configureWithTransparentBackground()
            defaultBarAppearance.shadowColor = .none
            defaultBarAppearance.backgroundColor = backgroundColor
            
            let standardBarAppearance = UINavigationBarAppearance()
            standardBarAppearance.configureWithDefaultBackground()
            standardBarAppearance.shadowColor = .none
            standardBarAppearance.backgroundColor = backgroundColor
            
            style.with(\.standardAppearance, standardBarAppearance)
            style.with(\.compactAppearance, standardBarAppearance)
            style.with(\.scrollEdgeAppearance, defaultBarAppearance)
        }
        
        if #available(iOS 11.0, *) {
            style.with(\.prefersLargeTitles, prefersLargeTitle)
        }
    }
}
