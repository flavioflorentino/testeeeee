import UIKit

/// A UINavigationController that manages the status bar and navigation bar appearence accordingly to the screen being presented
public class StyledNavigationController: UINavigationController {
    // MARK: - Override
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        if let topVC = viewControllers.last {
            return topVC.preferredStatusBarStyle
        }
        return .default
    }
    
    // MARK: - Init
    override public func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        navigationBar.isTranslucent = false
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}

// MARK: - UINavigationControllerDelegate
extension StyledNavigationController: UINavigationControllerDelegate {
    public func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        guard let vc = viewController as? StyledNavigationDisplayable else { return }
        navigationBar.navigationStyle(vc.navigationBarStyle)
        
        if let coordinator = topViewController?.transitionCoordinator {
            coordinator.notifyWhenInteractionChanges { [weak self] context in
                guard context.isCancelled, let currentVC = context.viewController(forKey: .from) as? StyledNavigationDisplayable else {
                    return
                }
                self?.navigationBar.navigationStyle(currentVC.navigationBarStyle)
            }
        }
        setNeedsStatusBarAppearanceUpdate()
    }
}
