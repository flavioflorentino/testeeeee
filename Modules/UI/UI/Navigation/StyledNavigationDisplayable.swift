import UIKit

public protocol StyledNavigationDisplayable {
    var navigationBarStyle: DefaultNavigationStyle { get }
}

/// Default implementation of the StyledNavigationDisplayable protocol, which most child view controllers use
public extension StyledNavigationDisplayable where Self: UIViewController {
    var navigationBarStyle: DefaultNavigationStyle {
        ClearNavigationStyle()
    }

    var statusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return traitCollection.userInterfaceStyle == .dark ? .lightContent : .darkContent
        }
        return .default
    }
}
