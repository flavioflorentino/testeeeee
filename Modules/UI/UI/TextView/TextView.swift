import UIKit

extension TextView.Layout {
    enum Length {
        // MARK: Insets
        static let separatorBottomInset = Spacing.base00
        static let floatingLabelTopInset: CGFloat = 20
        
        // MARK: Height
        static let separatorHeight: CGFloat = 1
        static let floatingLabelHeight: CGFloat = 16
    }
}

public final class TextView: UITextView, Maskable {
    fileprivate enum Layout {}
    // MARK: Components
    
    private lazy var placeholderLabel: UILabel = {
        let label = UILabel()
        label.text = placeholderText
        label.isHidden = !text.isEmpty
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale300())
        return label
    }()
    
    private lazy var masker: TextViewMasker = {
        let masker = TextViewMasker(textMask: nil)
        masker.bind(to: self)
        return masker
    }()
    
    private lazy var separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = tintColor
        view.isHidden = !isSeparatorVisible
        return view
    }()
    
    // MARK: Options
    
    public var placeholderText: String? {
        didSet {
            placeholderLabel.text = placeholderText
        }
    }
        
    public var isFloatingPlaceholder: Bool = false {
        didSet {
            textContainerInset.top = isFloatingPlaceholder ? Layout.Length.floatingLabelTopInset : .zero
            invalidateIntrinsicContentSize()
        }
    }
    
    public var isSeparatorVisible: Bool = false {
        didSet {
            separatorView.isHidden = !isSeparatorVisible
            textContainerInset.bottom = isSeparatorVisible ? Layout.Length.separatorBottomInset : .zero
            invalidateIntrinsicContentSize()
        }
    }
    
    public var textMask: TextMask? {
        get {
            masker.textMask
        }

        set {
            masker.textMask = newValue
        }
    }

    private var rectForSeparator: CGRect {
        CGRect(x: 0, y: bounds.maxY - Layout.Length.separatorHeight, width: bounds.width, height: Layout.Length.separatorHeight)
    }
    
    private var rectForPlaceholder: CGRect {
        bounds.inset(by: textContainerInset)
    }
    
    private var rectForFloatingPlaceholder: CGRect {
        let insetBounds = bounds.inset(by: textContainerInset)
        let origin = CGPoint(x: insetBounds.minX, y: 0)
        let size = CGSize(width: bounds.width, height: Layout.Length.floatingLabelHeight)
        return CGRect(origin: origin, size: size)
    }
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        buildLayout()
        registerForTextDidChangeNotifications()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        placeholderLabel.frame = text.isEmpty ? rectForPlaceholder : rectForFloatingPlaceholder
        separatorView.frame = rectForSeparator
    }
    
    override public func tintColorDidChange() {
        super.tintColorDidChange()
        separatorView.backgroundColor = tintColor
    }
    
    public func setPlaceholderTextStyle<T: LabelStyle>(_ style: T) {
        placeholderLabel
            .labelStyle(style)
            .with(\.textColor, .grayscale300())
    }
}

extension TextView: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(placeholderLabel)
        addSubview(separatorView)
    }
    
    public func setupConstraints() {}
    
    public func configureViews() {
        textContainerInset = .zero
        textContainer.lineFragmentPadding = 0
        invalidateIntrinsicContentSize()
    }
}

// MARK: Text Notifications

@objc
private extension TextView {
    func registerForTextDidChangeNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(
            self,
            selector: #selector(textDidChangeNotification(_:)),
            name: UITextView.textDidChangeNotification,
            object: nil
        )
    }
    
    func textDidChangeNotification(_ notification: Notification) {
        guard isFloatingPlaceholder else {
            placeholderLabel.isHidden = !text.isEmpty
            return
        }
        if text.isEmpty {
            placeholderLabel.frame = rectForPlaceholder
            setPlaceholderTextStyle(BodyPrimaryLabelStyle())
        } else {
            placeholderLabel.frame = rectForFloatingPlaceholder
            placeholderLabel.labelStyle(CaptionLabelStyle(type: .highlight))
        }
    }
}
