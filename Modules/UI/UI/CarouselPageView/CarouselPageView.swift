public protocol CarouselPageViewDelegate: AnyObject {
    func didScrollToPage(at pageIndex: Int)
}

public final class CarouselPageView<T, Cell: UICollectionViewCell>: UICollectionView, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    private var data = [T]()
    private var configureCellHandler: ((Int, T, Cell) -> Void)?
    private var supplementaryView: ((String, IndexPath) -> UICollectionReusableView)?
    private var lastPage: Int = .zero
    
    private var currentPage: Int {
        guard frame.size.width != 0 else {
            return 0
        }
        let index = round(contentOffset.x / frame.size.width)
        return max(Int(index), .zero)
    }
    
    public var isOnLastPage: Bool {
        currentPage == data.count - 1
    }
    
    public var isOnFirstPage: Bool {
        currentPage == 0
    }
    
    public weak var carouselPageDelegate: CarouselPageViewDelegate?
    
    public init() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        super.init(frame: .zero, collectionViewLayout: layout)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setup(data: [T], configureCell: @escaping (Int, T, Cell) -> Void) {
        self.data = data
        self.configureCellHandler = configureCell
        configureView()
        reloadData()
    }
    
    private func configureView() {
        showsHorizontalScrollIndicator = false
        isPagingEnabled = true
        dataSource = self
        delegate = self
        backgroundColor = .clear
        translatesAutoresizingMaskIntoConstraints = false
        register(Cell.self, forCellWithReuseIdentifier: Cell.identifier)
    }
    
    public func scrollToNextPage() {
        guard !isOnLastPage else { return }
        scrollToPage(at: currentPage + 1)
    }
    
    public func scrollToPreviousPage() {
        guard !isOnFirstPage else { return }
        scrollToPage(at: currentPage - 1, at: .left)
    }
    
    public func scrollToLastPage() {
        scrollToPage(at: data.count - 1)
    }
    
    private func scrollToPage(at pageIndex: Int, at scrollPosition: UICollectionView.ScrollPosition = .right) {
        let nextPageIndexPath = IndexPath(item: pageIndex, section: .zero)
        scrollToItem(at: nextPageIndexPath, at: scrollPosition, animated: true)
    }
    
    // MARK: UICollectionViewDataSource

    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        data.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Cell.identifier, for: indexPath) as? Cell else {
            return UICollectionViewCell()
        }
        let model = data[indexPath.row]
        configureCellHandler?(indexPath.row, model, cell)
        return cell
    }
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentPageIndex = currentPage
        guard currentPageIndex != lastPage else {
            return
        }
        lastPage = currentPageIndex
        carouselPageDelegate?.didScrollToPage(at: currentPageIndex)
    }

    // MARK: UICollectionViewDelegateFlowLayout
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        frame.size
    }

    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        .zero
    }

    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        .zero
    }
}
