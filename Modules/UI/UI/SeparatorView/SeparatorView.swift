import UIKit

public final class SeparatorView: UIView {
    override public var intrinsicContentSize: CGSize {
        CGSize(width: .zero, height: 1)
    }
    
    public init<S>(style: S) where S: ViewStyle {
        super.init(frame: .zero)
        
        viewStyle(style)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
