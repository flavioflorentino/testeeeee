import UIKit

final class TabIndicatorScrollView: UIScrollView {
    // MARK: View Properties
    var pagedViews = [UIView]() {
        didSet {
            oldValue.forEach { $0.removeFromSuperview() }
            pagedViews.forEach { self.addSubview($0) }
        }
    }
    
    var mostVisibleIndex: Int? {
        let origins = self.pagedViews.map { $0.frame.origin.x }
        let diffs = origins.map { abs(self.contentOffset.x - $0) }
        guard let minimum = diffs.min() else {
            return nil
        }
        return diffs.firstIndex(of: minimum)
    }
    
    // MARK: Init
    init() {
        super.init(frame: .zero)
        self.isPagingEnabled = true
        self.showsHorizontalScrollIndicator = false
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Override Functions
    override func layoutSubviews() {
        super.layoutSubviews()
        pagedViews.enumerated().forEach { index, view in
            view.frame = CGRect(origin: CGPoint(x: CGFloat(index) * bounds.width, y: 0), size: bounds.size)
        }
        contentSize = CGSize(width: bounds.width * CGFloat(pagedViews.count), height: bounds.height)
    }
}
