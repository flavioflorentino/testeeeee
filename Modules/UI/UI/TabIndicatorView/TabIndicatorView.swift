import UIKit

// MARK: Layout
private extension TabIndicatorView.Layout {
    enum Size {
        static var lineIndicatorHeight: CGFloat = 3
        /// Ratio of how much of the line will take of the tab button width. Values between 0.0 and 1.0
        static var lineWidthRatio: CGFloat = 0.75
    }
    
    enum Animation {
        static let duration: TimeInterval = 0.25
    }
    
    enum Color {
        static var tintColor: UIColor = Colors.branding600.color
    }
}

final class TabIndicatorView: UIView {
    // MARK: Private Properties
    fileprivate enum Layout { }
    private let tabTitles: [String]
    
    // MARK: View Properties
    private lazy var lineIndicatorView: UIView = {
        let view = UIView()
        view.backgroundColor = Layout.Color.tintColor
        return view
    }()
    
    private lazy var containerStackView: UIStackView = {
        let stackView = UIStackView(frame: .zero)
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    private lazy var tabButtons: [UIButton] = {
        let buttons = tabTitles.enumerated().map { index, title -> UIButton in
            let button = UIButton(frame: .zero)
            button.setTitle(title, for: .normal)
            button.addTarget(self, action: #selector(didTapTabButton(sender:)), for: .touchUpInside)
            button.tag = index
            button.isSelected = index == 0
            button.titleLabel?.textAlignment = .center
            setButtonsAttributes(with: title, for: button)
            containerStackView.addArrangedSubview(button)
            return button
        }
        return buttons
    }()
    
    let scrollView: TabIndicatorScrollView
    
    // MARK: Delegate
    weak var delegate: TabIndicatorViewDelegate?

    // MARK: Init
    init(tabTitles: [String],
         scrollView: TabIndicatorScrollView,
         lineWidthRatio: CGFloat = 0.75,
         tintColor: UIColor = Colors.branding600.color) {
        self.scrollView = scrollView
        self.tabTitles = tabTitles
        Layout.Color.tintColor = tintColor
        Layout.Size.lineWidthRatio = lineWidthRatio
        super.init(frame: .zero)
        backgroundColor = Colors.backgroundPrimary.color
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Override
    override func layoutSubviews() {
        super.layoutSubviews()
        buildLayout()
        setupIndicatorLineInitialFrame()
    }
    
    // MARK: Internal Functions
    func updateIndicatorPosition(at contentOffset: CGPoint) {
        let width = bounds.width / CGFloat(tabButtons.count)
        let widthWithRatio = width * Layout.Size.lineWidthRatio
        let ratioOffset = (width - widthWithRatio) / 2
        
        let newXposition = contentOffset.x / CGFloat(tabButtons.count) + ratioOffset
        var newFrame = lineIndicatorView.frame
        newFrame.origin.x = newXposition
        lineIndicatorView.frame = newFrame
        
        guard
            let mostVisibleIndex = scrollView.mostVisibleIndex,
            let selectedButton = tabButtons[safe: mostVisibleIndex]
        else {
            return
        }
        focus(on: selectedButton)
    }
    
    // MARK: Private Functions
    private func focus(on selectedButton: UIButton, animated: Bool = true) {
        let duration: TimeInterval = animated ? Layout.Animation.duration : 0
        UIView.animate(withDuration: duration) {
            self.tabButtons.forEach { $0.isSelected = false }
            selectedButton.isSelected = true
        }
    }
    
    private func setupIndicatorLineInitialFrame() {
        let y = bounds.maxY - Layout.Size.lineIndicatorHeight
        let width = bounds.width / CGFloat(tabButtons.count)
        let widthWithRatio = width * Layout.Size.lineWidthRatio
        let ratioOffset = (width - widthWithRatio) / 2
        lineIndicatorView.frame = CGRect(x: ratioOffset,
                                         y: y,
                                         width: width * Layout.Size.lineWidthRatio,
                                         height: Layout.Size.lineIndicatorHeight)
    }
    
    private func setButtonsAttributes(with title: String, for button: UIButton) {
        let normalAttributes: [NSAttributedString.Key: Any] = [
            .font: Typography.bodySecondary().font(),
            .foregroundColor: Colors.grayscale300.color
        ]
        let normalAttributesString = NSMutableAttributedString(string: title, attributes: normalAttributes)
        button.setAttributedTitle(normalAttributesString, for: .normal)
        
        let selectedAttributes: [NSAttributedString.Key: Any] = [
            .font: Typography.bodySecondary().font(),
            .foregroundColor: Layout.Color.tintColor
        ]
        let selectedAttributesString = NSMutableAttributedString(string: title, attributes: selectedAttributes)
        button.setAttributedTitle(selectedAttributesString, for: .selected)
    }
    
    // MARK: Objc Functions
    @objc
    func didTapTabButton(sender: UIButton) {
        delegate?.didSelectButton(at: sender.tag)
    }
}

// MARK: ViewConfiguration
extension TabIndicatorView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(containerStackView)
        addSubview(lineIndicatorView)
    }

    func setupConstraints() {
        containerStackView.snp.makeConstraints {
            $0.leading.top.trailing.width.equalToSuperview()
            $0.height.equalToSuperview()
        }
    }
}

protocol TabIndicatorViewDelegate: AnyObject {
    func didSelectButton(at index: Int)
}
