import UIKit
import SnapKit

extension TabIndicatorViewController.Layout {
    enum Size {
        static var tabIndicatorViewHeight: CGFloat = 45
    }
}

public protocol TabIndicatorViewControllerDelegate: AnyObject {
    func didUpdateSelectedTab(on index: Int)
    func didSelectButton(on index: Int)
}

/// A ViewController that displays n* numbers of child view controllers on a scroll view, with a tab indicator on top that you can interact with
public final class TabIndicatorViewController: UIViewController {
    // MARK: View Properties
    fileprivate enum Layout { }
    private let tabIndicatorView: TabIndicatorView
    private let tabIndicatorScrollView: TabIndicatorScrollView
    public var delegates: [TabIndicatorViewControllerDelegate] = []
    
    // MARK: Init
    /**
    - Parameters:
        - titles: A collection of String that determines the title for each tab to be shown in the given order *Important to make sure it has the same size as the child view controllers*
        - tabIndicatorViewHeight: The height of the top indicator view, default is 45.
        - lineWidthRatio: The proportional width of the line indicator compared to the tab view. Use values between 0.0 and 1.0
        - tintColor: The color if the line indicator and the title label.
    */
    public init(with titles: [String],
                tabIndicatorViewHeight: CGFloat = 45,
                lineWidthRatio: CGFloat = 0.75,
                tintColor: UIColor = Colors.branding600.color) {
        Layout.Size.tabIndicatorViewHeight = tabIndicatorViewHeight
        tabIndicatorScrollView = TabIndicatorScrollView()
        tabIndicatorView = TabIndicatorView(tabTitles: titles,
                                            scrollView: tabIndicatorScrollView,
                                            lineWidthRatio: lineWidthRatio,
                                            tintColor: tintColor)
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Override Functions
    override public func viewDidLoad() {
        super.viewDidLoad()
        buildLayout()
    }
    
    // MARK: Public Functions
    public func setViewControllers(_ viewControllers: [UIViewController], animated: Bool) {
        children.forEach { $0.willMove(toParent: nil) }
        tabIndicatorScrollView.pagedViews = []
        children.forEach { $0.removeFromParent() }
        
        viewControllers.forEach { addChild($0) }
        tabIndicatorScrollView.pagedViews = viewControllers.map { $0.view }
        viewControllers.forEach { $0.didMove(toParent: self) }
        
        view.setNeedsLayout()
        view.layoutIfNeeded()
        
        let contentOffset = -tabIndicatorView.bounds.height
        viewControllers.forEach { ($0.view as? UIScrollView)?.contentOffset.y = contentOffset }
    }
}

// MARK: UIScrollViewDelegate
extension TabIndicatorViewController: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        tabIndicatorView.updateIndicatorPosition(at: scrollView.contentOffset)
    }
    
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        guard let mostVisibleIndex = tabIndicatorView.scrollView.mostVisibleIndex else { return }
        delegates.forEach { $0.didUpdateSelectedTab(on: mostVisibleIndex) }
    }
}

// MARK: TabIndicatorViewDelegate
extension TabIndicatorViewController: TabIndicatorViewDelegate {
    func didSelectButton(at index: Int) {
        guard let viewController = children[safe: index] else { return }
        tabIndicatorScrollView.scrollRectToVisible(viewController.view.frame, animated: true)
        delegates.forEach { $0.didSelectButton(on: index) } 
    }
}

// MARK: ViewConfiguration
extension TabIndicatorViewController: ViewConfiguration {
    public func buildViewHierarchy() {
        view.addSubview(tabIndicatorView)
        view.addSubview(tabIndicatorScrollView)
    }
    
    public func setupConstraints() {
        tabIndicatorView.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top)
            $0.leading.trailing.width.equalToSuperview()
            $0.height.equalTo(Layout.Size.tabIndicatorViewHeight)
        }
        
        tabIndicatorScrollView.snp.makeConstraints {
            $0.top.equalTo(tabIndicatorView.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    public func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        tabIndicatorScrollView.delegate = self
        tabIndicatorView.delegate = self
    }
}
