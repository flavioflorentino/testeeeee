import UIKit

public protocol NavigationContract {
    func getCurrentNavigation() -> UINavigationController?
    func updateCurrentCoordinating(_ coordinating: Coordinating?)
}
