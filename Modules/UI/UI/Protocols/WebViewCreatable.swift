import UIKit

public protocol WebViewCreatable {
    func createWebView(with url: URL) -> UIViewController
}
