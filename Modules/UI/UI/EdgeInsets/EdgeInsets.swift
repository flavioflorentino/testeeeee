import UIKit

public struct EdgeInsets {
    public var top, leading, bottom, trailing: CGFloat
    
    public static let zero = EdgeInsets()
    public static let rootView: EdgeInsets = {
        let deviceScreenWidth = UIScreen.main.bounds.width
        let horizontalSpacing = deviceScreenWidth > 375 ? 20 : Spacing.base02
        return EdgeInsets(
            top: Spacing.base02,
            leading: horizontalSpacing,
            bottom: Spacing.base02,
            trailing: horizontalSpacing
        )
    }()
    
    public init(
        top: CGFloat = 0,
        leading: CGFloat = 0,
        bottom: CGFloat = 0,
        trailing: CGFloat = 0
    ) {
        self.top = top
        self.leading = leading
        self.bottom = bottom
        self.trailing = trailing
    }
    
    public init(edgeInsets: UIEdgeInsets) {
        top = edgeInsets.top
        leading = edgeInsets.left
        bottom = edgeInsets.bottom
        trailing = edgeInsets.right
    }
    
    @available(iOS 11, *)
    public init(directionalEdgeInsets: NSDirectionalEdgeInsets) {
        top = directionalEdgeInsets.top
        leading = directionalEdgeInsets.leading
        bottom = directionalEdgeInsets.bottom
        trailing = directionalEdgeInsets.trailing
    }
}

@available(iOS 11, *)
extension EdgeInsets: NSDirectionalEdgeInsetsConvertible {
    public var asNSDirectionalEdgeInsets: NSDirectionalEdgeInsets {
        NSDirectionalEdgeInsets(
            top: top,
            leading: leading,
            bottom: bottom,
            trailing: trailing
        )
    }
}

extension EdgeInsets: UIEdgeInsetsConvertible {
    public var asUIEdgeInsets: UIEdgeInsets {
        UIEdgeInsets(
            top: top,
            left: leading,
            bottom: bottom,
            right: trailing
        )
    }
}
