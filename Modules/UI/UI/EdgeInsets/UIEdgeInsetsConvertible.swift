import UIKit

public protocol UIEdgeInsetsConvertible {
    var asUIEdgeInsets: UIEdgeInsets { get }
}
