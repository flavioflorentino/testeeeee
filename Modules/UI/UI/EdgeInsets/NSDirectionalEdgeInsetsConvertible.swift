import UIKit

@available(iOS 11, *)
public protocol NSDirectionalEdgeInsetsConvertible {
    var asNSDirectionalEdgeInsets: NSDirectionalEdgeInsets { get }
}
