import UIKit

public extension UIView {
    var compatibleLayoutMargins: EdgeInsets {
        get {
            if #available(iOS 11.0, *) {
                return EdgeInsets(directionalEdgeInsets: directionalLayoutMargins)
            } else {
                return EdgeInsets(edgeInsets: layoutMargins)
            }
        }

        set {
            if #available(iOS 11, *) {
                directionalLayoutMargins = newValue.asNSDirectionalEdgeInsets
            } else {
                layoutMargins = newValue.asUIEdgeInsets
            }
        }
    }
}
