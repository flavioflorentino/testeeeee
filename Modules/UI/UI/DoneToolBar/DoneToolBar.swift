import UIKit

/// It declares the interface that the DoneToolBarDelegate implements to handle touch on the done button.
@objc
public protocol DoneToolBarDelegate {
    /// Method called when the done button is touched.
    @objc
    optional func didTouchOnDone()
}

/// A control that displays one right button that resigns the application's current first responder.
public final class DoneToolBar: UIToolbar {
    private enum Layout {
        static let width: CGFloat = UIScreen.main.bounds.width
        static let height: CGFloat = 50
    }
    
    // MARK: - Visual Components
    private lazy var doneButton: UIBarButtonItem = {
        let button = UIBarButtonItem(title: self.doneText,
                                     style: .plain,
                                     target: self,
                                     action: #selector(resignApplicationFirstResponder))
        return button
    }()
    
    // MARK: - Variables
    private var doneText: String
    /// The DoneToolBar's delegate.
    public weak var doneDelegate: DoneToolBarDelegate?
    
    // MARK: - Life Cycle
    /// Create an instance of DoneToolBar with the desired done text.
    /// - Parameter doneText: The text attributed to the done button.
    public init(doneText: String) {
        self.doneText = doneText
        super.init(frame: CGRect(origin: .zero,
                                 size: CGSize(width: Layout.width,
                                              height: Layout.height)))
        let flexSpace = UIBarButtonItem(
            barButtonSystemItem: .flexibleSpace,
            target: nil,
            action: nil
        )
        self.items = [flexSpace, doneButton]
        self.sizeToFit()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Resign First Responder
    @objc
    private func resignApplicationFirstResponder() {
        doneDelegate?.didTouchOnDone?()
        UIApplication.shared.sendAction(#selector(resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
