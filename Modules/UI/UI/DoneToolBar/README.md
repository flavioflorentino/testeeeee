#  DoneToolBar

It's an UIToolBar with only one button of done if pressed it will resign the first responder of the application. It was created to be added as an input accessory view.

<p align="center" >
<img src="screenshot/keyboard.png" width="50%" height="50%" />
</p>

## How to use

To use it you just need to create a new instance of DoneToolBar and assign it to a view as an input accessory view or whatever you want.

```swift

let doneToolBar = DoneToolBar(doneText: "Ok")
textField.inputAccessoryView = doneToolBar

```


## Delegate

The delegate of this component is DoneToolBarDelegate.
This protocol have an optional method that is executed after the user touch on the button done.

```swift
optional func didTouchOnDone()
```