import UIKit

@available(*, deprecated, message: "Use ViewController")
open class LegacyViewController<ViewModel, Coordinator, V: UIView>: UIViewController, ViewConfiguration {
    public let coordinator: Coordinator
    public let viewModel: ViewModel
    public var rootView = V()
    
    public init(viewModel: ViewModel, coordinator: Coordinator) {
        self.coordinator = coordinator
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        buildViewHierarchy()
        configureViews()
        setupConstraints()
    }
    
    override open func loadView() {
        view = rootView
    }
    
    open func configureViews() { }
    
    open func buildViewHierarchy() { }
    
    open func setupConstraints() { }
}
