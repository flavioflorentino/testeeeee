import UIKit

/// Designated to handle scroll view content and scroll indicators insets when keyboard appears
public final class KeyboardScrollViewHandler: NSObject {
    public weak var scrollView: UIScrollView?
    public var keyboardMargin: CGFloat
    
    public init(scrollView: UIScrollView? = nil, keyboardMargin: CGFloat = 0) {
        self.keyboardMargin = keyboardMargin
        super.init()
        self.scrollView = scrollView
    }
    
    public func registerForKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(keyboardDidShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc
    private func keyboardDidShow(_ notification: Notification) {
        let keyboardInfo = notification.userInfo
        
        guard
            let keyboardFrame = keyboardInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect,
            let scrollView = scrollView else {
            return
        }
        var bottomInset = keyboardFrame.height
        if #available(iOS 11, *) {
            bottomInset -= scrollView.safeAreaInsets.bottom
        }
        bottomInset += keyboardMargin
        scrollView.contentInset.bottom = bottomInset
        scrollView.scrollIndicatorInsets = scrollView.contentInset
    }
    
    @objc
    private func keyboardWillHide(_ notification: Notification) {
        guard let scrollView = self.scrollView else {
            return
        }
        scrollView.contentInset.bottom = 0
        scrollView.scrollIndicatorInsets = scrollView.contentInset
    }
}
