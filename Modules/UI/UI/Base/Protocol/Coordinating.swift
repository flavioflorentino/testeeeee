import UIKit

public protocol Coordinating: AnyObject {
    var childViewController: [UIViewController] { get set }
    var viewController: UIViewController? { get set }
    func start()
}

public extension Coordinating {
    func start() { }
}
