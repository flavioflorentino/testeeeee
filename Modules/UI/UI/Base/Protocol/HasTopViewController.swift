import UIKit

public protocol HasTopViewController {
    func topViewController(controller: UIViewController?) -> UIViewController?
}

public extension HasTopViewController {
    func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        let topController: UIViewController?
        
        switch controller {
        case let controller as UINavigationController:
            topController = controller.visibleViewController
        case let controller as UITabBarController:
            topController = controller.selectedViewController
        default:
            if let controller = controller?.presentedViewController {
                topController = controller
            } else {
                return controller
            }
        }
        
        return topViewController(controller: topController)
    }
}
