import UIKit

extension NestedScrollView {
    final class ScrollableElementView: UIView {
        // MARK: - Properties
        private let embedView: UIView
        private let scrollView: UIScrollView

        private var heightConstraint: NSLayoutConstraint?

        private var embedViewTopConstraint: NSLayoutConstraint?
        private var embedViewHeightConstraint: NSLayoutConstraint?

        private var contentSizeObserver: NSKeyValueObservation?

        var isLastElement = false

        // MARK: - Initialization
        init(embedView: UIView, scrollView: UIScrollView) {
            self.embedView = embedView
            self.scrollView = scrollView

            super.init(frame: .zero)

            setup()
        }

        @available(*, unavailable)
        override init(frame: CGRect) {
            fatalError("init(frame:) has not been implemented")
        }

        @available(*, unavailable)
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }

        // MARK: - Layout
        override func layoutSubviews() {
            super.layoutSubviews()

            contentSizeDidChange()
        }
    }
}

// MARK: - NestedScrollViewElement extension
extension NestedScrollView.ScrollableElementView: NestedScrollViewScrollableElement {
    func updateFor(contentOffset: CGPoint) {
        guard contentOffset.y >= frame.minY else {
            reset()
            return
        }

        var scrollViewContentOffset = scrollView.contentOffset
        scrollViewContentOffset.y = max(contentOffset.y - frame.minY - scrollView.frame.minY, .zero)

        embedViewTopConstraint?.constant = scrollViewContentOffset.y
        scrollView.contentOffset = scrollViewContentOffset

        contentSizeDidChange()
    }
}

// MARK: - Private
private extension NestedScrollView.ScrollableElementView {
    var maxScreenHeight: CGFloat {
        UIScreen.main.bounds.height - UIApplication.shared.statusBarFrame.height
    }

    func setup() {
        addSubview(embedView)

        scrollView.isScrollEnabled = false
        scrollView.bounces = false

        setupConstraints()

        contentSizeObserver = scrollView.observe(\.contentSize, changeHandler: { [weak self] _, _ in
            self?.contentSizeDidChange()
        })
    }

    func setupConstraints() {
        setupEmbedViewConstraints()

        heightConstraint = heightAnchor.constraint(equalToConstant: scrollView.contentSize.height)
        heightConstraint?.isActive = true
    }

    func setupEmbedViewConstraints() {
        embedView.translatesAutoresizingMaskIntoConstraints = false

        let top = embedView.topAnchor.constraint(equalTo: topAnchor)
        embedViewTopConstraint = top

        let bottom = embedView.bottomAnchor.constraint(equalTo: bottomAnchor)
        bottom.priority = .init(999)

        NSLayoutConstraint.activate([
            top,
            embedView.leadingAnchor.constraint(equalTo: leadingAnchor),
            embedView.trailingAnchor.constraint(equalTo: trailingAnchor),
            bottom
        ])

        embedViewHeightConstraint = createEmbedViewHeightConstraint()
    }

    func createEmbedViewHeightConstraint() -> NSLayoutConstraint {
        embedView.setNeedsDisplay()
        embedView.setNeedsLayout()
        embedView.layoutIfNeeded()

        var embedViewPreferredHeight = scrollView.contentSize.height + scrollView.frame.minY

        if embedViewPreferredHeight > UIScreen.main.bounds.height {
            embedViewPreferredHeight = UIScreen.main.bounds.height + scrollView.frame.minY
        }

        let constraint = embedView.heightAnchor.constraint(equalToConstant: embedViewPreferredHeight)
        constraint.isActive = true

        return constraint
    }

    func contentSizeDidChange() {
        updateHeightConstraint()
        updateEmbedViewHeight()
    }

    func updateHeightConstraint() {
        var maxHeight = scrollView.contentSize.height + scrollView.frame.minY

        if isLastElement {
            maxHeight = max(maxScreenHeight - frame.minY, maxHeight)
        }

        guard heightConstraint?.constant != maxHeight else { return }

        heightConstraint?.constant = maxHeight
    }

    func updateEmbedViewHeight() {
        var maxHeight = min(
            maxScreenHeight + scrollView.frame.minY,
            scrollView.contentSize.height + scrollView.frame.minY
        )

        if isLastElement {
            maxHeight = max(maxScreenHeight - frame.minY - scrollView.frame.minY, maxHeight)
        }

        guard embedViewHeightConstraint?.constant != maxHeight else { return }

        embedViewHeightConstraint?.constant = maxHeight
    }

    func reset() {
        embedViewTopConstraint?.constant = .zero
        scrollView.contentOffset = .zero
    }
}
