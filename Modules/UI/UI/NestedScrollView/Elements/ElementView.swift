import UIKit

extension NestedScrollView {
    final class ElementView: UIView {
        // MARK: - Properties
        private let embedView: UIView

        // MARK: - Initialization
        init(embedView: UIView) {
            self.embedView = embedView

            super.init(frame: .zero)

            setup()
        }

        @available(*, unavailable)
        override init(frame: CGRect) {
            fatalError("init(frame:) has not been implemented")
        }

        @available(*, unavailable)
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

// MARK: - NestedScrollViewElement extension
extension NestedScrollView.ElementView: NestedScrollViewElement {
    func updateFor(contentOffset: CGPoint) { }
}

// MARK: - Private
private extension NestedScrollView.ElementView {
    func setup() {
        embedView.translatesAutoresizingMaskIntoConstraints = false

        addSubview(embedView)
        NSLayoutConstraint.activate([
            embedView.topAnchor.constraint(equalTo: topAnchor),
            embedView.leadingAnchor.constraint(equalTo: leadingAnchor),
            embedView.trailingAnchor.constraint(equalTo: trailingAnchor),
            embedView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}
