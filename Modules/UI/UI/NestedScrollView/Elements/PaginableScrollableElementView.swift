import UIKit

extension NestedScrollView {
    final class PaginableScrollableElementView: UIView {
        // MARK: - Properties
        private let embedView: UIView
        private let pinnedView: UIView?

        private let scrollViews: [UIScrollView]
        private let currentScrollView: () -> UIScrollView?
        private var contentOffsets: [NSObject: CGPoint] = [:]

        private var heightConstraint: NSLayoutConstraint?

        private var embedViewTopConstraint: NSLayoutConstraint?
        private var embedViewHeightConstraint: NSLayoutConstraint?

        private var contentSizeObservers: [NSKeyValueObservation] = []

        var isLastElement = false
        var changeParentContentOffset: ((CGPoint) -> Void)?

        // MARK: - Initialization
        init(
            embedView: UIView,
            pinnedView: UIView?,
            scrollViews: [UIScrollView],
            currentScrollView: @escaping () -> UIScrollView?
        ) {
            self.embedView = embedView
            self.pinnedView = pinnedView
            self.scrollViews = scrollViews
            self.currentScrollView = currentScrollView

            super.init(frame: .zero)

            setup()
        }

        @available(*, unavailable)
        override init(frame: CGRect) {
            fatalError("init(frame:) has not been implemented")
        }

        @available(*, unavailable)
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }

        // MARK: - Layout
        override func layoutSubviews() {
            super.layoutSubviews()

            contentSizeDidChange()
        }
    }
}

// MARK: - NestedScrollViewElement extension
extension NestedScrollView.PaginableScrollableElementView: NestedScrollViewScrollableElement {
    func updateFor(contentOffset: CGPoint) {
        guard contentOffset.y >= frame.minY else {
            reset()
            return
        }

        guard let scrollView = currentScrollView(), let embedViewTopConstraint = embedViewTopConstraint else { return }

        // This type of verification is necessary, because the floating point divergence
        // for equal values depends on the screen size.
        if !embedViewTopConstraint.constant.isEqual(to: scrollView.contentOffset.y, precise: 4) {
            let difference = embedViewTopConstraint.constant - scrollView.contentOffset.y
            changeParentContentOffset?(CGPoint(x: contentOffset.x, y: contentOffset.y - difference))

            embedViewTopConstraint.constant = scrollView.contentOffset.y
            contentSizeDidChange()

            return
        }

        var scrollViewContentOffset = scrollView.contentOffset
        scrollViewContentOffset.y = max(contentOffset.y - frame.minY, .zero)

        embedViewTopConstraint.constant = scrollViewContentOffset.y
        scrollView.contentOffset = scrollViewContentOffset

        contentSizeDidChange()
    }
}

// MARK: - Private
private extension NestedScrollView.PaginableScrollableElementView {
    var maxScreenHeight: CGFloat {
        UIScreen.main.bounds.height - UIApplication.shared.statusBarFrame.height
    }

    func setup() {
        addSubview(embedView)

        for scrollView in scrollViews {
            scrollView.isScrollEnabled = false
            scrollView.bounces = false

            contentOffsets[scrollView] = .zero

            contentSizeObservers.append(scrollView.observe(\.contentSize, changeHandler: { [weak self] _, _ in
                self?.contentSizeDidChange()
            }))
        }

        setupConstraints()
    }

    func setupConstraints() {
        setupEmbedViewConstraints()

        heightConstraint = heightAnchor.constraint(equalToConstant: currentScrollView()?.contentSize.height ?? .zero)
        heightConstraint?.isActive = true
    }

    func setupEmbedViewConstraints() {
        embedView.translatesAutoresizingMaskIntoConstraints = false

        let top = embedView.topAnchor.constraint(equalTo: topAnchor)
        embedViewTopConstraint = top

        let bottom = embedView.bottomAnchor.constraint(equalTo: bottomAnchor)
        bottom.priority = .init(999)

        NSLayoutConstraint.activate([
            top,
            embedView.leadingAnchor.constraint(equalTo: leadingAnchor),
            embedView.trailingAnchor.constraint(equalTo: trailingAnchor),
            bottom
        ])

        embedViewHeightConstraint = createEmbedViewHeightConstraint()
    }

    func createEmbedViewHeightConstraint() -> NSLayoutConstraint {
        embedView.setNeedsDisplay()
        embedView.setNeedsLayout()
        embedView.layoutIfNeeded()

        let scrollView = currentScrollView()
        var embedViewPreferredHeight = (scrollView?.contentSize.height ?? .zero) + (scrollView?.frame.minY ?? .zero)

        if embedViewPreferredHeight > UIScreen.main.bounds.height {
            embedViewPreferredHeight = UIScreen.main.bounds.height + (scrollView?.frame.minY ?? .zero)
        }

        let constraint = embedView.heightAnchor.constraint(equalToConstant: embedViewPreferredHeight)
        constraint.isActive = true

        return constraint
    }

    func contentSizeDidChange() {
        updateHeightConstraint()
        updateEmbedViewHeight()
    }

    func updateHeightConstraint() {
        guard let heightConstraint = heightConstraint else { return }

        var maxHeight = (currentScrollView()?.contentSize.height ?? .zero) + (pinnedView?.frame.height ?? .zero)
        if isLastElement {
            maxHeight = max(maxScreenHeight - frame.minY, maxHeight)
        }

        guard heightConstraint.constant != maxHeight else { return }

        heightConstraint.constant = maxHeight
    }

    func updateEmbedViewHeight() {
        guard let scrollView = currentScrollView(), let embedViewHeightConstraint = embedViewHeightConstraint else { return }

        var maxHeight = min(
            maxScreenHeight + scrollView.frame.minY,
            scrollView.contentSize.height + scrollView.frame.minY
        )

        if isLastElement {
            maxHeight = max(maxScreenHeight - scrollView.frame.minY, maxHeight)
        }

        guard embedViewHeightConstraint.constant != maxHeight else { return }
        embedViewHeightConstraint.constant = maxHeight
    }

    func reset() {
        embedViewTopConstraint?.constant = .zero

        for scrollView in scrollViews {
            scrollView.contentOffset = .zero
        }
    }
}
