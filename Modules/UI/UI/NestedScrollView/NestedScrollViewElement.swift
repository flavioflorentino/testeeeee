import Foundation

protocol NestedScrollViewElement: AnyObject {
    func updateFor(contentOffset: CGPoint)
}

protocol NestedScrollViewScrollableElement: NestedScrollViewElement {
    var isLastElement: Bool { get set }
}
