import UIKit

public final class NestedScrollView: UIScrollView {
    public enum Element {
        case fixed(view: UIView)
        case scrollable(container: UIView, scrollView: UIScrollView)
        case paginableScrollable(
                container: UIView,
                pinned: UIView?,
                scrollViews: [UIScrollView],
                currentScrollView: () -> UIScrollView?
             )
    }

    typealias NestedScrollElementView = NestedScrollViewElement & UIView

    // MARK: - Properties
    private var lastContentOffset: CGPoint = .zero
    private var elements: [NestedScrollElementView] = []

    private weak var lastElement: NestedScrollViewScrollableElement? {
        didSet {
            oldValue?.isLastElement = false
        }
    }

    private lazy var contentView: UIStackView = {
        let stackView = UIStackView()

        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.alignment = .fill
        stackView.spacing = .zero

        return stackView
    }()

    public var spacing: CGFloat {
        get { contentView.spacing }
        set { contentView.spacing = newValue }
    }

    // MARK: - Initialization
    override public init(frame: CGRect) {
        super.init(frame: frame)

        setup()
    }

    @available(*, unavailable)
    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Layout
    override public func layoutSubviews() {
        super.layoutSubviews()

        if let lastElement = elements.last as? NestedScrollViewScrollableElement, lastElement !== self.lastElement {
            self.lastElement = lastElement
            lastElement.isLastElement = true
        }

        for element in elements {
            guard lastContentOffset.y != contentOffset.y else { continue }

            element.updateFor(contentOffset: contentOffset)
        }

        lastContentOffset = contentOffset
    }

    // MARK: - Add elements
    public func add(element: Element) {
        let elementView = createElementView(from: element)

        contentView.addArrangedSubview(elementView)
        elements.append(elementView)
    }

    public func add(elements: Element...) {
        elements.forEach { add(element: $0) }
    }
}

// MARK: - Private
private extension NestedScrollView {
    func setup() {
        addSubview(contentView)

        contentView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: topAnchor),
            contentView.leadingAnchor.constraint(equalTo: leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: trailingAnchor),
            contentView.bottomAnchor.constraint(equalTo: bottomAnchor),
            contentView.widthAnchor.constraint(equalTo: widthAnchor)
        ])
    }

    func createElementView(from element: Element) -> NestedScrollElementView {
        switch element {
        case let .fixed(view):
            return ElementView(embedView: view)
        case let .scrollable(container, scrollView):
            return ScrollableElementView(embedView: container, scrollView: scrollView)
        case let .paginableScrollable(container, pinned, scrollViews, currentScrollView):
            let view = PaginableScrollableElementView(
                embedView: container,
                pinnedView: pinned,
                scrollViews: scrollViews,
                currentScrollView: currentScrollView
            )
            view.changeParentContentOffset = { [weak self] contentOffset in
                self?.contentOffset = contentOffset
            }

            return view
        }
    }
}
