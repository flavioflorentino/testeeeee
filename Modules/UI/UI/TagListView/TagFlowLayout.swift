import UIKit

final class CollectionViewRow {
    private var spacing: CGFloat = 0
    var attributes = [UICollectionViewLayoutAttributes]()
    
    private var rowWidth: CGFloat {
        attributes.reduce(0, { result, attribute -> CGFloat in
            result + attribute.frame.width
        }) + CGFloat(attributes.count - 1) * spacing
    }
    
    init(spacing: CGFloat) {
        self.spacing = spacing
    }
    
    func add(attribute: UICollectionViewLayoutAttributes) {
        attributes.append(attribute)
    }
    
    func centerLayout(collectionViewWidth: CGFloat) {
        let padding = (collectionViewWidth - rowWidth) / 2
        var offset = padding
        for attribute in attributes {
            attribute.frame.origin.x = offset
            offset += attribute.frame.width + spacing
        }
    }
}

public final class TagFlowLayout: UICollectionViewFlowLayout {
    // swiftlint:disable:next discouraged_optional_collection
    override public func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        guard let attributes = super.layoutAttributesForElements(in: rect) else {
            return nil
        }
        
        var rows = [CollectionViewRow]()
        var currentRowY: CGFloat = -1
        
        for attribute in attributes {
            if currentRowY != attribute.frame.origin.y {
                currentRowY = attribute.frame.origin.y
                rows.append(CollectionViewRow(spacing: Spacing.base02))
            }
            rows.last?.add(attribute: attribute)
        }
        
        rows.forEach { $0.centerLayout(collectionViewWidth: collectionView?.frame.width ?? 0) }
        return rows.flatMap { $0.attributes }
    }
}
