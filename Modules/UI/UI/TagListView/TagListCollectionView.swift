import UIKit

public final class TagListCollectionView: UICollectionView {
    public convenience init() {
        let layout = TagFlowLayout()
        layout.estimatedItemSize = CGSize(width: Spacing.base11, height: Spacing.base04)
        
        self.init(frame: .zero, collectionViewLayout: layout)
        register(TagListCell.self, forCellWithReuseIdentifier: TagListCell.identifier)
        viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        backgroundColor = .clear
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        if bounds.size != self.intrinsicContentSize {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override public var intrinsicContentSize: CGSize {
        contentSize
    }
}
