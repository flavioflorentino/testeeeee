import SnapKit
import UIKit

public final class TagListCell: UICollectionViewCell {
    fileprivate enum Layout { }
    
    public var didSelectItem: (() -> Void)?
    
    private lazy var itemButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(LinkButtonStyle(size: .small))
            .with(\.borderColor, (color: .grayscale100(), state: .normal))
            .with(\.borderColor, (color: .grayscale100(), state: .highlighted))
            .with(\.borderColor, (color: .branding700(), state: .selected))
            .with(\.backgroundColor, (color: .branding700(), state: .selected))
            .with(\.textColor, (color: .black(), state: .normal))
            .with(\.textColor, (color: .black(), state: .highlighted))
            .with(\.textColor, (color: .white(), state: .selected))
            .with(\.contentEdgeInsets, UIEdgeInsets(top: 0, left: 13, bottom: 0, right: 13))
        button.addTarget(self, action: #selector(didTapCellButton), for: .touchUpInside)
        
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setup(text: String, isSelected: Bool) {
        itemButton.setTitle(text, for: .normal)
        itemButton.isSelected = isSelected
    }
}

private extension TagListCell {
    @objc
    func didTapCellButton() {
        itemButton.isSelected.toggle()
        didSelectItem?()
    }
}

extension TagListCell: ViewConfiguration {
    public func buildViewHierarchy() {
        contentView.addSubview(itemButton)
    }
    
    public func setupConstraints() {
        itemButton.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.height.equalTo(Spacing.base04)
        }
    }
}
