import UIKit

@objc
public extension UIImageView {
    private enum AssociatedKeys {
        static var imageDownloaderKey = "picpay_image_downloader"
        static var activityIndicatorKey = "picpay_activity_indicator"
        static var showActivityIndicatorKey = "picpay_show_activity_indicator"
    }

    private var imageDownloader: ImageDownloadable? {
        get {
            objc_getAssociatedObject(self, &AssociatedKeys.imageDownloaderKey) as? ImageDownloadable
        }
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(
                    self,
                    &AssociatedKeys.imageDownloaderKey,
                    newValue as ImageDownloadable?,
                    .OBJC_ASSOCIATION_RETAIN_NONATOMIC
                )
            }
        }
    }

    private var activityIndicator: UIActivityIndicatorView? {
        get {
            objc_getAssociatedObject(self, &AssociatedKeys.activityIndicatorKey) as? UIActivityIndicatorView
        }

        set {
            if let newValue = newValue {
                objc_setAssociatedObject(
                    self,
                    &AssociatedKeys.activityIndicatorKey,
                    newValue as UIActivityIndicatorView?,
                    .OBJC_ASSOCIATION_RETAIN_NONATOMIC
                )
            }
        }
    }

    var showActivityIndicator: Bool {
        get {
            objc_getAssociatedObject(
                self,
                &AssociatedKeys.showActivityIndicatorKey
            ) as? Bool ?? (activityIndicator != nil)
        }
        set {
            objc_setAssociatedObject(
                self,
                &AssociatedKeys.showActivityIndicatorKey,
                newValue as Bool,
                .OBJC_ASSOCIATION_RETAIN_NONATOMIC
            )
            
            if newValue == true {
                guard activityIndicator == nil else {
                    return
                }
                let activity = UIActivityIndicatorView(style: .whiteLarge)
                activity.translatesAutoresizingMaskIntoConstraints = false
                activity.startAnimating()
                addSubview(activity)
                activity.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
                activity.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
                activity.isHidden = true
            } else {
                activityIndicator?.removeFromSuperview()
            }
        }
    }

    /// Request image and set the response result, if its valid
    func setImage(url path: URL?, placeholder: UIImage? = nil, completion: ((UIImage?) -> Void)? = nil) {
        if imageDownloader == nil {
            imageDownloader = ImageDownloader()
        }
        self.cancelRequest()
        
        fetchImage(url: path, placeholder: placeholder, completion: completion)
    }

    /// Call this when implementing Table Views and Collection Views in order to reuse cell
    func cancelRequest() {
        imageDownloader?.cancelRequest()
    }

    private func fetchImage(url path: URL?, placeholder: UIImage?, completion: ((UIImage?) -> Void)?) {
        guard let url = path else {
            completion?(nil)
            return
        }

        imageDownloader?.download(
            from: url,
            willDownloadImage: { [weak self] in
                if placeholder != nil {
                    self?.image = placeholder
                }
            },
            completion: { [weak self] image in
                self?.showActivityIndicator = false
                DispatchQueue.main.async {
                    if let completion = completion {
                        completion(image)
                    } else {
                        self?.image = image
                    }
                }
            }
        )
    }
}
