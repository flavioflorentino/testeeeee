import Core
import UIKit

@objc
protocol ImageDownloadable {
    func download(from url: URL, willDownloadImage: (@escaping () -> Void), completion: @escaping ((UIImage?) -> Void))
    func cancelRequest()
}

@objc
final class ImageDownloader: NSObject, ImageDownloadable {
    private var dataTask: URLSessionDataTask?
    private let session: URLSession
    private let imageCache: ImageCaching

    init(urlSession: URLSession = URLSession.shared, imageCache: ImageCaching = ImageCacher.shared) {
        self.session = urlSession
        self.imageCache = imageCache
    }
    
    func download(from url: URL, willDownloadImage: (@escaping () -> Void), completion: @escaping ((UIImage?) -> Void)) {
        if let cachedImage = imageCache.loadImage(for: url.absoluteString as NSString) {
            completion(cachedImage)
            return
        }
        
        willDownloadImage()
        dataTask = session.dataTask(with: url) { [imageCache] data, _, _ in
            guard let data = data, let image = UIImage(data: data) else { return }
            imageCache.cache(image: image, withKey: url.absoluteString as NSString)
            completion(image)
        }
        dataTask?.resume()
    }
    
    func cancelRequest() {
        dataTask?.cancel()
    }
}

// MARK: URL Session Task Delegate
@objc
extension ImageDownloader: URLSessionTaskDelegate {
    public func urlSession(
        _ session: URLSession,
        task: URLSessionTask,
        didReceive challenge: URLAuthenticationChallenge,
        completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void
    ) {
        let pinningHandler: PinningHandlerProvider = PinningHandler()
        pinningHandler.handle(challenge: challenge, completionHandler: completionHandler)
    }
}
