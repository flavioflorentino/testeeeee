import Foundation
import UIKit

public typealias StatefulButton = (image: UIImage?, title: String?)
public typealias StatefulContent = (title: String?, description: String?)

public protocol StatefulViewModeling {
    var image: UIImage? { get set }
    var content: StatefulContent? { get set }
    var button: StatefulButton? { get set }
    var presenter: StatefulPresenting? { get set }
    
    func configureView()
}

public extension StatefulViewModeling {
    func configureView() {
        presenter?.configureView(image: image, content: content, button: button)
    }
}
