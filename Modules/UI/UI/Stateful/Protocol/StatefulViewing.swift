import Foundation
import UIKit

public typealias StatefulTransitionViewing = StatefulProviding & StatefulDelegate

public protocol StatefulDelegate: AnyObject {
    func didTryAgain()
    func didTapSecondaryButton()
}

public extension StatefulDelegate {
    func didTryAgain() { }
    func didTapSecondaryButton() { }
}

public protocol StatefulViewing: StatefulPresenting, AnyObject {
    var presentedView: UIView { get }
    var viewModel: StatefulViewModeling? { get set }
    var delegate: StatefulDelegate? { get set }
}

public extension StatefulViewing where Self: UIView {
    var presentedView: UIView {
        self
    }
}
