import Foundation
import UIKit

public enum StatefulType: String {
    case content
    case loading
    case error
    
    public enum StateViewState {
        case loading
        case error
        
        public var stateView: StatefulViewing {
            switch self {
            case .loading:
                return StatefulLoadingView()
            case .error:
                return StatefulErrorView()
            }
        }
    }
}

public protocol StatefulProviding: AnyObject {
    var stateMachine: ViewStateMachining { get }
    
    func beginState(animated: Bool, model: StatefulViewModeling?, completion: (() -> Void)?)
    func endState(animated: Bool, model: StatefulViewModeling?, completion: (() -> Void)?)
  
    func statefulViewForLoading() -> StatefulViewing
    func statefulViewForError() -> StatefulViewing
}

extension StatefulProviding where Self: UIViewController {
    public var stateMachine: ViewStateMachining {
        ViewStateMachine.shared
    }
    
    public func beginState(animated: Bool = true, model: StatefulViewModeling? = nil, completion: (() -> Void)? = nil) {
        transitionView(
            state: .loading,
            model: model,
            animated: animated,
            completion: completion
        )
    }
    
    public func endState(animated: Bool = true, model: StatefulViewModeling? = nil, completion: (() -> Void)? = nil) {
        transitionView(
            state: model != nil ? .error : .content,
            model: model,
            animated: animated,
            completion: completion
        )
    }
  
    public func statefulViewForLoading() -> StatefulViewing {
        StatefulType.StateViewState.loading.stateView
    }
    
    public func statefulViewForError() -> StatefulViewing {
        StatefulType.StateViewState.error.stateView
    }
    
    // MARK: - Private Methods
    private func transitionView(state: StatefulType, model: StatefulViewModeling? = nil, animated: Bool = true, completion: (() -> Void)?) {
        let newView = state == .loading ? statefulViewForLoading() : statefulViewForError()
        
        if state == .loading || state == .error {
            newView.viewModel = model
            newView.viewModel?.presenter = newView
            newView.delegate = self as? StatefulDelegate
        }
        
        stateTransition(
            state == .content ? .none : .view,
            transitionView: newView,
            animated: animated,
            completion: completion
        )
    }
    
    private func stateTransition(
        _ state: MachineState,
        transitionView: StatefulViewing,
        animated: Bool,
        completion: (() -> Void)?
    ) {
        stateMachine.transitionToState(
            state,
            transitionView: transitionView.presentedView,
            superview: view,
            animated: animated,
            completion: completion
        )
    }
}
