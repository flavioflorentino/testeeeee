import Foundation
import UIKit

public protocol StatefulPresenting {
    func configureView(image: UIImage?, content: StatefulContent?, button: StatefulButton?)
}

public extension StatefulPresenting {
    func configureView(image: UIImage?, content: StatefulContent?, button: StatefulButton?) { }
}
