import Foundation
import UIKit

public final class StatefulErrorViewModel: StatefulViewModeling {
    public var image: UIImage?
    public var content: StatefulContent?
    public var button: StatefulButton?
    
    public var presenter: StatefulPresenting?
    
    public init(image: UIImage?, content: StatefulContent, button: StatefulButton) {
        self.image = image
        self.content = content
        self.button = button
    }
}

// Equatable
extension StatefulErrorViewModel: Equatable {
    public static func == (lhs: StatefulErrorViewModel, rhs: StatefulErrorViewModel) -> Bool {
        lhs.image == rhs.image &&
            lhs.content?.title == rhs.content?.title &&
            lhs.content?.description == rhs.content?.description &&
            lhs.button?.title == rhs.button?.title
    }
}
