import UIKit

public final class StatefulFeedbackViewModel: StatefulViewModeling {
    public var image: UIImage?
    public var content: StatefulContent?
    public var button: StatefulButton?
    public var secondaryButton: StatefulButton?
    public var presenter: StatefulPresenting?
    public var description: NSAttributedString?
    
    public init(
        image: UIImage?,
        content: StatefulContent,
        description: NSAttributedString? = nil,
        button: StatefulButton?,
        secondaryButton: StatefulButton? = nil
    ) {
        self.image = image
        self.content = content
        self.button = button
        self.secondaryButton = secondaryButton
        self.description = description
    }
}

// Equatable
extension StatefulFeedbackViewModel: Equatable {
    public static func == (lhs: StatefulFeedbackViewModel, rhs: StatefulFeedbackViewModel) -> Bool {
        lhs.image == rhs.image &&
            lhs.content?.title == rhs.content?.title &&
            lhs.content?.description == rhs.content?.description &&
            lhs.button?.title == rhs.button?.title &&
            lhs.secondaryButton?.title == rhs.secondaryButton?.title
    }
}
