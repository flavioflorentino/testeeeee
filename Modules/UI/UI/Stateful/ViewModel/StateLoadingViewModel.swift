import Foundation
import UIKit

public final class StateLoadingViewModel: StatefulViewModeling {
    public var image: UIImage?
    public var content: StatefulContent?
    public var button: StatefulButton?
    
    public var presenter: StatefulPresenting?
    
    public init(message: String) {
        self.content = (title: message, description: nil)
    }
}

// Equatable
extension StateLoadingViewModel: Equatable {
    public static func == (lhs: StateLoadingViewModel, rhs: StateLoadingViewModel) -> Bool {
        lhs.image == rhs.image &&
            lhs.content?.title == rhs.content?.title &&
            lhs.content?.description == rhs.content?.description &&
            lhs.button?.title == rhs.button?.title
    }
}
