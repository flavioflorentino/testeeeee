import SnapKit
import UIKit

protocol StatefulContainerViewing where Self: UIView { }

final class StatefulContainerView: UIView, StatefulContainerViewing { }

public enum MachineState {
    case none
    case view
}

public protocol ViewStateMachining {
    static var shared: ViewStateMachining { get }
    func transitionToState(_ state: MachineState, transitionView: UIView, superview: UIView, animated: Bool, completion: (() -> Void)?)
}

public final class ViewStateMachine: ViewStateMachining {
    private let queue = DispatchQueue(label: "com.picpay.stateful.machine.queue", qos: .background)
    
    public static var shared: ViewStateMachining = ViewStateMachine()
        
    public func transitionToState(
        _ state: MachineState,
        transitionView: UIView,
        superview: UIView,
        animated: Bool = true,
        completion: (() -> Void)? = nil
    ) {
        queue.async {
            self.queue.suspend()
            
            let callback: () -> Void = {
                self.queue.resume()
                completion?()
            }
            
            DispatchQueue.main.async { [weak self] in
                switch state {
                case .none:
                    self?.hideAllViews(superview: superview, animated: animated, completion: callback)
                case .view:
                    self?.showView(transitionView, superview: superview, animated: animated, completion: callback)
                }
            }
        }
    }
    
    // MARK: - Private Methods
    private func showView(
        _ transitionView: UIView,
        superview: UIView,
        animated: Bool,
        completion: (() -> Void)? = nil
    ) {
        let containerView: StatefulContainerViewing = {
            let view = StatefulContainerView()
            view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            view.backgroundColor = .clear
            return view
        }()
        
        if !superview.subviews.contains(where: { $0 is StatefulViewing }) {
            containerView.frame = superview.bounds
            superview.addSubview(containerView)
        }
        
        configureTransitionState(
            containerView,
            transitionView: transitionView,
            animated: animated,
            completion: completion
        )
    }
    
    private func configureTransitionState(
        _ containerView: UIView,
        transitionView: UIView,
        animated: Bool,
        completion: (() -> Void)?
    ) {
        transitionView.alpha = animated ? 0.0 : 1.0
        
        containerView.addSubview(transitionView)
        
        transitionView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        UIView.animate(
            withDuration: 0.3,
            animations: {
                transitionView.alpha = 1.0
            },
            completion: { _ in
                containerView.subviews.filter { $0 !== transitionView }.forEach { $0.removeFromSuperview() }
                completion?()
            }
        )
    }
    
    private func hideAllViews(superview: UIView, animated: Bool, completion: (() -> Void)? = nil) {
        UIView.animate(
            withDuration: 0.3,
            animations: {
                superview.subviews.forEach {
                    if $0 is StatefulContainerViewing {
                        $0.alpha = animated ? 0.0 : 1.0
                    }
                }
            }, completion: { _ in
                superview.subviews.first(where: { $0 is StatefulContainerViewing })?.removeFromSuperview()
                completion?()
            }
        )
    }
}
