# Stateful (State Machine)

The easiest state machine in Swift.

Stateful is a minimalistic and super easy to use state machine in Swift.

### Feature

- [X] Easy presentation
- [x] Custom view state based on UIView
- [X] Attributed image, title label, description label and title button
- [X] Pure Swift 5.1

### Usage

- Add stateful in ViewController (use _**StatefulTransitionViewing**_ protocol)

```swift
public class SampleViewController: UIViewController { 
    // ...
  
  	public override func viewDidLoad() { 
        super.viewDidLoad()
        // ....
  	}
  
  	private func loadData() { 
        beginState()
      	viewModel.loadData()
    }
}

extension SampleViewController: SampleDisplay { 
    public func didError(_ error: Error) { 
        endState(
          model: StatefulErrorViewModel(
            image: UIImage,
            content: (title: String, description: String),
            button: (image: UIImage?, title: String)
          )
        )	
    }
}

extension SampleViewController: StatefulTransitionViewing { 
    public func didTryAgain() { 
    		loadData()		
    }

  	public func statefulViewForLoading() -> StatefulViewing { 
    		return CustomView()
    }
  
    public func statefulViewForError() -> StatefulViewing { 
    		return CustomView()
    }
}
```

## Stateful Methods
-  Parameters in method **`beginState`** and **`endState`**:
    - _**animated**_ - Responsible for animating the views transition 
    - _**model**_ - Responsible for showing the information in the View (image, title, description)
    - _**completion**_ - Resposible for executing the block code after the animation finishes 

```swift
func beginState(animated: Bool, model: StatefulViewModeling?, completion: (() -> Void)?)
```

```swift
func endState(animated: Bool, model: StatefulViewModeling?, completion: (() -> Void)?)
```



### StatefulViewModeling

The StatefulViewModeling is a protocol used for configuring view presented in screen

```swift 
public protocol StatefulViewModeling {
    var image: UIImage? { get set }
    var content: StatefulContent? { get set }
    var button: StatefulButton? { get set }
    var presenter: StatefulPresenting? { get set }
    
    func configureView()
}
```

- _**StatefulViewModeling**_ for Loading

```swift
public func StateLoadingViewModel(message: String)
```

- _**StatefulViewModeling**_ for Error

```swift
public func StatefulErrorViewModel(
    image: UIImage?, 
    content: (title: String, description: String), 
    button: (image: UIImage?, title: String)
)
```



## Create Custom View 

There is a possibility of changing the loading  and error views 

- Set custom **`viewForLoading`** or / and **`viewForError`**

```swift
public class SampleViewController: StateProviderProtocol {
    // ...
}

extension SampleViewController: StatefulTransitionViewing { 
    public func statefulViewForLoading() -> StatefulViewing { 
    		return CustomView()
    }
  
    public func statefulViewForError() -> StatefulViewing { 
    		return CustomView()
    }
}

// Custom Loading View State
public class CustomLoadingView: UIView, StatefulViewing {
    public var viewModel: StatefulViewModeling?
  	public weak var delegate: StatefulDelegate?

    // ...
}

// Custom Error View State
public class CustomErrorView: UIView, StatefulViewing {
    public var viewModel: StatefulViewModeling?
  	public weak var delegate: StatefulDelegate?

    // ...
}
```

The **`CustomLoadingView`** must conform to the **`StatefulViewing`** protocol

- Required properties: 

    - `ViewModel` contains basic information for setting the `View`, for example: `image`, `title`, `description` and `presenter`
    - `Delegate` communication the `Stateful` for `UIViewController` 

```swift
public var viewModel: StatefulViewModeling?
public weak var delegate: StatefulDelegate?
```
