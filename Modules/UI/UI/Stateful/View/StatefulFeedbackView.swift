import UIKit

public final class StatefulFeedbackView: ApolloFeedbackView {
    public weak var delegate: StatefulDelegate?
    
    public init() {
        // Init with empty values since StatefulPresenting method configureView will handle the style using the StatefulFeedbackViewModel
        super.init(
            content: .init(
                image: nil,
                title: String(),
                description: NSAttributedString(),
                primaryButtonTitle: String(),
                secondaryButtonTitle: String()
            ),
            style: .custom)
    }
    
    public var viewModel: StatefulViewModeling? {
        didSet {
            update()
        }
    }
    
    override func pressPrimaryButton() {
        delegate?.didTryAgain()
    }
    
    override func pressSecondaryButton() {
        delegate?.didTapSecondaryButton()
    }
    
    // MARK: - Methods StatefulPresenting
    public func configureView(image: UIImage?, content: StatefulContent?, button: StatefulButton?) {
        contentView.configureContent(image: image, content: content)
        
        if let viewModel = viewModel as? StatefulFeedbackViewModel {
            let content = ApolloFeedbackViewContent(image: image,
                                                    title: content?.title ?? "",
                                                    description: viewModel.description ?? NSAttributedString(string: content?.description ?? ""),
                                                    primaryButtonTitle: button?.title ?? "",
                                                    secondaryButtonTitle: viewModel.secondaryButton?.title ?? "")
            
            super.configureView(content: content,
                                primaryButtonBackgroundColor: style.primaryButtonColors.backgroundColor,
                                primaryButtonTitleColor: style.primaryButtonColors.titleColor,
                                secondaryButtonBackgroundColor: style.secondaryButtonColors.backgroundColor,
                                secondaryButtonTitleColor: style.secondaryButtonColors.titleColor)
            
            if viewModel.secondaryButton == nil {
                secondaryButtonIsHidden(true)
            }
        }
    }
    
    // MARK: - Private Methods
    private func update() {
        viewModel?.configureView()
    }
}

extension StatefulFeedbackView: StatefulViewing {}
