import SnapKit
import UIKit

private extension StatefulErrorView.Layout {
    enum Size {
        static let imageView = CGSize(width: 160, height: 120)
    }
    enum Edges {
        static let retryButtonImage = UIEdgeInsets(top: 12, left: 0, bottom: 12, right: 8)
    }
}

public final class StatefulErrorView: UIView, StatefulViewing {
    fileprivate enum Layout { }
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textColor, .black())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var retryButton: UIButton = {
        let button = UIButton()
        button
            .buttonStyle(PrimaryButtonStyle())
            .with(\.typography, .bodyPrimary(.highlight))
        button.addTarget(self, action: #selector(pressTryAgain), for: .touchUpInside)
        button.imageEdgeInsets = Layout.Edges.retryButtonImage
        return button
    }()
    
    public var viewModel: StatefulViewModeling? {
        didSet {
            update()
        }
    }
    
    public weak var delegate: StatefulDelegate?
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Methods StatefulPresenting
    public func configureView(image: UIImage?, content: StatefulContent?, button: StatefulButton?) {
        imageView.image = image
        titleLabel.text = content?.title
        descriptionLabel.text = content?.description
        
        if let statefulButton = button, let title = button?.title {
            retryButton.setTitle(title, for: .normal)
            retryButton.setImage(statefulButton.image, for: .normal)
            retryButton.setImage(statefulButton.image, for: .highlighted)
            retryButton.isHidden = false
        } else {
            retryButton.isHidden = true
        }
    }
    
    // MARK: - Private Methods
    private func update() {
        viewModel?.configureView()
    }
    
    @objc
    private func pressTryAgain() {
        delegate?.didTryAgain()
    }
}

// MARK: - ViewConfiguration
extension StatefulErrorView: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(imageView)
        addSubview(titleLabel)
        addSubview(descriptionLabel)
        addSubview(retryButton)
    }
    
    public func setupConstraints() {
        imageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.imageView)
            $0.centerX.equalTo(compatibleSafeArea.centerX)
            $0.bottom.equalTo(titleLabel.compatibleSafeArea.top).offset(-Spacing.base01)
        }
        
        titleLabel.snp.makeConstraints {
            $0.centerY.equalTo(compatibleSafeArea.centerY)
            $0.centerX.equalTo(compatibleSafeArea.centerX)
            $0.top.equalTo(imageView.compatibleSafeArea.bottom).offset(Spacing.base01)
            $0.leading.equalTo(compatibleSafeArea.leading).offset(Spacing.base03)
            $0.trailing.equalTo(compatibleSafeArea.trailing).offset(-Spacing.base03)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.compatibleSafeArea.bottom).offset(Spacing.base00)
            $0.leading.equalTo(compatibleSafeArea.leading).offset(Spacing.base03)
            $0.trailing.equalTo(compatibleSafeArea.trailing).offset(-Spacing.base03)
        }
        
        retryButton.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.compatibleSafeArea.bottom).offset(Spacing.base02)
            $0.height.equalTo(Spacing.base06)
            $0.leading.equalTo(compatibleSafeArea.leading).offset(Spacing.base03)
            $0.trailing.equalTo(compatibleSafeArea.trailing).offset(-Spacing.base03)
        }
    }
    
    public func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
}
