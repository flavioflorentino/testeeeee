import UIKit

private extension StatefulEmptyStateView.Layout {
    enum Size {
        static let imageView: CGFloat = 144.0
    }
}

public final class StatefulEmptyStateView: UIView, StatefulViewing {
    fileprivate enum Layout { }
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textColor, .black())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        return label
    }()
    
    public var viewModel: StatefulViewModeling? {
        didSet {
            update()
        }
    }
    
    public weak var delegate: StatefulDelegate?
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Methods StatefulPresenting
    public func configureView(image: UIImage?, content: StatefulContent?, button: StatefulButton?) {
        imageView.image = image
        titleLabel.text = content?.title
        descriptionLabel.text = content?.description
    }
    
    // MARK: - Private Methods
    private func update() {
        viewModel?.configureView()
    }
    
    @objc
    private func pressTryAgain() {
        delegate?.didTryAgain()
    }
}

// MARK: - ViewConfiguration
extension StatefulEmptyStateView: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(imageView)
        addSubview(titleLabel)
        addSubview(descriptionLabel)
    }
    
    public func setupConstraints() {
        imageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.imageView)
            $0.centerX.equalTo(compatibleSafeArea.centerX)
            $0.bottom.equalTo(titleLabel.compatibleSafeArea.top).offset(-Spacing.base02)
        }
        
        titleLabel.snp.makeConstraints {
            $0.centerY.equalTo(compatibleSafeArea.centerY)
            $0.centerX.equalTo(compatibleSafeArea.centerX)
            $0.top.equalTo(imageView.compatibleSafeArea.bottom).offset(Spacing.base01)
            $0.leading.equalTo(compatibleSafeArea.leading).offset(Spacing.base03)
            $0.trailing.equalTo(compatibleSafeArea.trailing).offset(-Spacing.base03)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.compatibleSafeArea.bottom).offset(Spacing.base01)
            $0.leading.equalTo(compatibleSafeArea.leading).offset(Spacing.base03)
            $0.trailing.equalTo(compatibleSafeArea.trailing).offset(-Spacing.base03)
        }
    }
    
    public func configureViews() {
        backgroundColor = Colors.white.color
    }
    
    private func setupView() {
        buildViewHierarchy()
        setupConstraints()
        configureViews()
    }
}
