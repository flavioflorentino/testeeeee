import SnapKit
import UIKit

public final class StatefulLoadingView: UIView, StatefulViewing {
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activity = UIActivityIndicatorView(style: .whiteLarge)
        activity.color = Colors.brandingBase.color
        activity.startAnimating()
        return activity
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale750())
            .with(\.textAlignment, .center)
        return label
    }()
        
    public var viewModel: StatefulViewModeling? {
        didSet {
            update()
        }
    }
    
    public weak var delegate: StatefulDelegate?
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Methods StatefulPresenting
    public func configureView(image: UIImage?, content: StatefulContent?, button: StatefulButton?) {
        messageLabel.text = content?.title
    }
    
    // MARK: - Private Methods
    private func update() {
        viewModel?.configureView()
    }
}

extension StatefulLoadingView: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(activityIndicator)
        addSubview(messageLabel)
    }
    
    public func setupConstraints() {
        activityIndicator.snp.makeConstraints {
            $0.centerY.centerX.equalToSuperview()
        }
        
        messageLabel.snp.makeConstraints {
            $0.top.equalTo(activityIndicator.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    public func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
}
