# RadioButtonForm
Component for list of RadioButtons 

<p align="center" >
<img src="Screenshots/radioButtonExample.gif.gif" width="50%" height="50%" />
</p>

## Requirements
- iOS 10.3 or later
- Xcode 10.0 or later

## Motivation
On Credit Squad, we needed to create a radio component for selection and this same component will be used in other scenes 

## How to use
First of all, you have to instance the component

```swift

private lazy var radioButtonForm: RadioButtonForm = {
    let form = RadioButtonForm()
    form.translatesAutoresizingMaskIntoConstraints = false
    form.delegate = self // optional
    return form
}()

```

Then you need to set the data
The data is basicaly an Array of RadioButtonData
And a RadioButtonData is a tuple of (Any, String)

```swift

radioButtonForm.data = [
    (value: "0", description: "First Option"),
    (value: "Second", description: "Second Option"),
    (value: 2, description: "Third Option"),
    (value: UIColor.green, description: "Fourth Option")
]

```

Add it on your view.

```swift

private func buildViewHierarchy() {
    view.addSubview(radioButtonForm)
}

private func setupConstraints() {
    NSLayoutConstraint.leadingTrailing(equalTo: view, for: [radioButtonForm], constant: 16)
    NSLayoutConstraint.activate([
        radioButtonForm.topAnchor.constraint(equalTo: anotherLabel.bottomAnchor, constant: 32)
    ])
    
}

```

## Delegate (optional)
Implement  `RadioButtonFormDelegate`

```swift
radioButtonForm.delegate = self

...

extension CardBlockReasonViewController: RadioButtonFormDelegate {
    public func didChangeSelected() {
        // usage example
        confirmButton.isEnabled = true
    }
}

```

## Get the radio button selected
To recover the data you use this method

```swift
radioButtonForm.selectedValue()
```

## License
RadioButtonForm is available for intern use of [PicPay](https://www.picpay.com/site)
