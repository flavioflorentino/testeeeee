import UIKit

public typealias RadioButtonData<T> = (value: T, description: String)

protocol RadioButtonDelegate: AnyObject {
    func didSelect(radio: RadioButton)
}

final class RadioButton: UIView {
    private enum Layout {
        static let sizeButton = CGSize(width: 32, height: 32)
    }
    weak var delegate: RadioButtonDelegate?
    var selected: Bool = false {
        didSet {
            button.isSelected = selected
            button.isUserInteractionEnabled = !selected
        }
    }
    var data: RadioButtonData<Any>? {
        didSet {
            update()
        }
    }
    private lazy var button: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UI.Assets.grayCheckmark.image, for: .normal)
        button.setImage(UI.Assets.greenFilledCheckmark.image, for: .selected)
        button.addTarget(self, action: #selector(didTouchUpInside), for: .touchUpInside)
        button.contentEdgeInsets = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
        return button
    }()
    private lazy var label: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textAlignment, .left)
            .with(\.textColor, Colors.grayscale500.color)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension RadioButton {
    @objc
    func didTouchUpInside() {
        delegate?.didSelect(radio: self)
    }
    func update() {
        label.text = data?.description
    }
}

extension RadioButton: ViewConfiguration {
    func buildViewHierarchy() {
        addSubviews(button, label)
    }
    
    func setupConstraints() {
        button.snp.makeConstraints {
            $0.size.equalTo(Layout.sizeButton)
            $0.centerY.leading.equalToSuperview()
        }
        
        label.snp.makeConstraints {
            $0.top.bottom.trailing.equalToSuperview()
            $0.leading.equalTo(button.snp.trailing).offset(Spacing.base01)
        }
    }
}
