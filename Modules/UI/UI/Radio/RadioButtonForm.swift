import UIKit

public protocol RadioButtonFormDelegate: AnyObject {
    func didChangeSelected()
}

public final class RadioButtonForm: UIView {
    public weak var delegate: RadioButtonFormDelegate?
    
    public var data: [RadioButtonData<Any>] = [] {
        didSet {
            update()
        }
    }
    
    private lazy var stackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = Spacing.base04
        stack.alignment = .leading
        stack.distribution = .equalSpacing
        return stack
    }()
    
    private var selectedRadio: RadioButton? {
        didSet {
            delegate?.didChangeSelected()
        }
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func selectedValue() -> Any? {
        selectedRadio?.data?.value
    }
    
    private func update() {
        stackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
        data.forEach { value, description in
            let radioButton = RadioButton()
            radioButton.data = (value: value, description: description)
            radioButton.delegate = self
            stackView.addArrangedSubview(radioButton)
        }
    }
    
    public func selectFirstRadio() {
        guard stackView.subviews.isNotEmpty else { return }
        
        guard let radioButton = stackView.subviews.first as? RadioButton else { return }
        radioButton.selected = true
        didSelect(radio: radioButton)
    }
}

extension RadioButtonForm: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(stackView)
    }
    
    public func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}

extension RadioButtonForm: RadioButtonDelegate {
    func didSelect(radio: RadioButton) {
        selectedRadio?.selected = false
        selectedRadio = radio
        selectedRadio?.selected = true
    }
}
