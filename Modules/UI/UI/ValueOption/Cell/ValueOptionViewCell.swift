import UIKit

public class ValueOptionViewCell: UICollectionViewCell {
    public enum ValueOptionState {
        case selected
        case unselected
    }
    
    private lazy var circleView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 32
        view.layer.borderColor = Palette.ppColorGrayscale400.color.withAlphaComponent(0.5).cgColor
        view.layer.borderWidth = 1
        view.backgroundColor = Palette.ppColorGrayscale000.color
        return view
    }()
    
    private lazy var middleCircleView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 28
        view.backgroundColor = Palette.ppColorGrayscale100.color
        return view
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Palette.ppColorGrayscale400.color
        label.textAlignment = .center
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Palette.ppColorGrayscale400.color
        label.textAlignment = .center
        return label
    }()
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func prepareForReuse() {
        super.prepareForReuse()
        titleLabel.text = nil
        descriptionLabel.text = nil
        setState(.unselected)
    }
    
    public func setState(_ state: ValueOptionState) {
        switch state {
        case .selected:
            circleView.layer.borderColor = Palette.ppColorBranding300.color.withAlphaComponent(0.5).cgColor
            middleCircleView.backgroundColor = Palette.ppColorBranding300.color
            titleLabel.textColor = Palette.ppColorGrayscale000.color
            descriptionLabel.textColor = Palette.ppColorGrayscale000.color
        case .unselected:
            circleView.layer.borderColor = Palette.ppColorGrayscale400.color.withAlphaComponent(0.5).cgColor
            middleCircleView.backgroundColor = Palette.ppColorGrayscale100.color
            titleLabel.textColor = Palette.ppColorGrayscale400.color
            descriptionLabel.textColor = Palette.ppColorGrayscale400.color
        }
    }
    
    public func setupCell(_ presenter: ValueOptionViewPresenting, highlight: ValueOptionHighlightType) {
        titleLabel.text = presenter.title
        stackView.addArrangedSubview(titleLabel)
        
        if presenter.description?.isEmpty == false {
            descriptionLabel.text = presenter.description
            stackView.addArrangedSubview(descriptionLabel)
        }

        configureComponents(highlight)
    }
    
    private func configureComponents(_ highlight: ValueOptionHighlightType) {
        switch highlight {
        case .topValue:
            titleLabel.font = UIFont.systemFont(ofSize: 20.0, weight: .bold)
            descriptionLabel.font = UIFont.systemFont(ofSize: 10.0, weight: .regular)
        case .bottomValue:
            titleLabel.font = UIFont.systemFont(ofSize: 10.0, weight: .regular)
            descriptionLabel.font = UIFont.systemFont(ofSize: 20.0, weight: .bold)
        }
        
        if stackView.arrangedSubviews.count == 1 {
            titleLabel.font = UIFont.systemFont(ofSize: 16.0, weight: .medium)
        }
    }
}

extension ValueOptionViewCell: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(circleView)
        addSubview(middleCircleView)
        middleCircleView.addSubview(stackView)
    }
    
    public func setupConstraints() {
        NSLayoutConstraint.activate([
            circleView.topAnchor.constraint(equalTo: topAnchor),
            circleView.bottomAnchor.constraint(equalTo: bottomAnchor),
            circleView.heightAnchor.constraint(equalToConstant: 64),
            circleView.widthAnchor.constraint(equalTo: circleView.heightAnchor),
            circleView.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            middleCircleView.heightAnchor.constraint(equalToConstant: 56),
            middleCircleView.widthAnchor.constraint(equalToConstant: 56),
            middleCircleView.centerXAnchor.constraint(equalTo: circleView.centerXAnchor),
            middleCircleView.centerYAnchor.constraint(equalTo: circleView.centerYAnchor)
        ])
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: middleCircleView.topAnchor, constant: 8),
            stackView.leadingAnchor.constraint(equalTo: middleCircleView.leadingAnchor, constant: 8),
            stackView.trailingAnchor.constraint(equalTo: middleCircleView.trailingAnchor, constant: -8),
            stackView.bottomAnchor.constraint(equalTo: middleCircleView.bottomAnchor, constant: -8)
        ])
    }
    
    public func configureViews() { }
    
    private func setupView() {
        buildViewHierarchy()
        setupConstraints()
        configureViews()
    }
}
