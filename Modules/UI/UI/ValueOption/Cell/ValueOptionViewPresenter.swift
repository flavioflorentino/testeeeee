import Foundation

public protocol ValueOptionViewPresenting {
    var title: String { get set }
    var description: String? { get set }
}

public struct ValueOptionViewPresenter: ValueOptionViewPresenting {
    public var title: String
    public var description: String?
    
    public init(title: String, description: String? = nil) {
        self.title = title
        self.description = description
    }
}
