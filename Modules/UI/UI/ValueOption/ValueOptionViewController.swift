import UIKit

public protocol ValueOptionViewDelegate: AnyObject {
    func didSelectItem(_ presenter: ValueOptionViewPresenting)
}

public protocol ValueOptionViewDataSource: AnyObject {
    func numberOfItems(in valueOptionViewController: ValueOptionViewController) -> Int
    func valueOptionViewController(_ valueOptionViewController: ValueOptionViewController, valueOptionForItemAt item: Int) -> ValueOptionViewPresenting
}

public enum ValueOptionHighlightType {
    case topValue
    case bottomValue
}

public class ValueOptionViewController: UIViewController {
    private enum Layout {
        static let itemSize: CGFloat = 64.0
        static let itemPadding: CGFloat = 10.0
        static let insetSection = UIEdgeInsets(top: Layout.itemPadding, left: Layout.itemPadding, bottom: Layout.itemPadding, right: Layout.itemPadding)
    }
    
    public enum ValueOptionAlignment {
        case center
        case `default`
    }
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .clear
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(ValueOptionViewCell.self, forCellWithReuseIdentifier: ValueOptionViewCell.identifier)
        return collectionView
    }()
    
    private var alignment: ValueOptionAlignment
    private var highlight: ValueOptionHighlightType
    
    private var numberOfItems: CGFloat {
        CGFloat(dataSource?.numberOfItems(in: self) ?? Int())
    }
    
    public weak var delegate: ValueOptionViewDelegate?
    
    public weak var dataSource: ValueOptionViewDataSource? {
        didSet {
            configureCollectionView()
        }
    }
    
    public init(alignment: ValueOptionAlignment = .default, highlight: ValueOptionHighlightType = .bottomValue) {
        self.alignment = alignment
        self.highlight = highlight
        
        super.init(nibName: nil, bundle: nil)
        setupView()
        configureCollectionView()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        collectionView.collectionViewLayout.prepare()
        collectionView.collectionViewLayout.invalidateLayout()
        collectionView.collectionViewLayout.finalizeCollectionViewUpdates()
    }
    
    // MARK: - Helpers Methods
    public func selectValueOption(at item: Int) {
        collectionView.selectItem(at: IndexPath(item: item, section: 0), animated: true, scrollPosition: .centeredVertically)
        collectionView(collectionView, didSelectItemAt: IndexPath(item: item, section: 0))
    }
    
    public func reloadData() {
        collectionView.reloadData()
    }
    
    // MARK: - Private Methods
    private func configureCollectionView() {
        collectionView.reloadData()

        let totalOfItems = UIScreen.main.bounds.height / Layout.itemSize
        let collectionViewHeight = (CGFloat(numberOfItems) / totalOfItems).rounded(.up) * Layout.itemSize + Layout.insetSection.top + Layout.insetSection.bottom
        
        view.constraints.first { $0.firstAttribute == .height }?.constant = collectionViewHeight
        
        if collectionViewHeight > UIScreen.main.bounds.height {
            view.constraints.first { $0.firstAttribute == .height }?.isActive = false
        }
        
        view.layoutIfNeeded()
    }
}

extension ValueOptionViewController: ViewConfiguration {
    public func buildViewHierarchy() {
        view.addSubview(collectionView)
    }
    
    public func setupConstraints() {
        NSLayoutConstraint.constraintAllEdges(from: collectionView, to: view)
        
        NSLayoutConstraint.activate([
            view.heightAnchor.constraint(equalToConstant: 0)
        ])
    }
    
    public func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    private func setupView() {
        buildViewHierarchy()
        setupConstraints()
        configureViews()
    }
}

// MARK: - CollectionViewDataSource
extension ValueOptionViewController: UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        dataSource?.numberOfItems(in: self) ?? Int()
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ValueOptionViewCell.identifier, for: indexPath) as? ValueOptionViewCell,
            let presenter = dataSource?.valueOptionViewController(self, valueOptionForItemAt: indexPath.item) else {
            return UICollectionViewCell()
        }
        
        cell.setupCell(presenter, highlight: highlight)
        return cell
    }
}

// MARK: CollectionViewDelegate
extension ValueOptionViewController: UICollectionViewDelegate {
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let presenter = dataSource?.valueOptionViewController(self, valueOptionForItemAt: indexPath.item) else {
            return
        }
        
        let cell = collectionView.cellForItem(at: indexPath) as? ValueOptionViewCell
        cell?.setState(.selected)
        delegate?.didSelectItem(presenter)
    }
    
    public func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? ValueOptionViewCell
        cell?.setState(.unselected)
    }
}

// MARK: CollectionViewDelegateFlowLayout
extension ValueOptionViewController: UICollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch alignment {
        case .center:
            return CGSize(width: Layout.itemSize, height: Layout.itemSize)
        case .default:
            return CGSize(width: ((view.frame.width / numberOfItems) - (Layout.insetSection.top + Layout.insetSection.bottom)), height: Layout.itemSize)
        }
    }

    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        switch alignment {
        case .center:
            let limitCells = (collectionView.frame.width / Layout.itemSize).rounded(.down)
            let numberOfCells = numberOfItems >= limitCells ? limitCells : numberOfItems
            let insets = (collectionView.frame.width - (numberOfCells * (Layout.itemSize + Layout.itemPadding))) / numberOfCells
            return UIEdgeInsets(top: Layout.insetSection.top, left: insets, bottom: Layout.insetSection.bottom, right: insets)
        case .default:
            return Layout.insetSection
        }
    }
}
