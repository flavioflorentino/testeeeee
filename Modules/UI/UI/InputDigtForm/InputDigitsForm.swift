import UIKit

public protocol InputDigitsFormDelegate: AnyObject {
    func formIsFilled(_ isFill: Bool)
    func tapHiddenForm(isHidden: Bool)
}

public class InputDigitsForm: UIView {
    public weak var delegate: InputDigitsFormDelegate?
    public var showError = String()
    public var isSecureTextEntry = false {
        didSet {
            setToogleSecurityVisible(isSecureTextEntry)
            setSecurity(isSecureTextEntry)
        }
    }
    public var isInputVisible = true {
        didSet {
            didChangeInputVisibility(isInputVisible)
        }
    }
    private lazy var contentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private lazy var stackView: UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .horizontal
        stack.distribution = .fillEqually
        stack.spacing = 8
        stack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(inputTextFieldFirstResponder)))
        return stack
    }()
    private lazy var inputTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.keyboardType = .numberPad
        textField.autocorrectionType = .no
        textField.delegate = self
        textField.isHidden = true
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        return textField
    }()
    private lazy var toogleSecurityButton: UIButton = {
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(Assets.icoPasswordEyeClosed.image, for: .normal)
        button.addTarget(self, action: #selector(didToogleVisibility), for: .touchUpInside)
        return button
    }()
    private var limitPasswordCount: Int = 0
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        setupAccessibility()
        setStyleOnDigts(for: String())
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @discardableResult
    override public func becomeFirstResponder() -> Bool {
        inputTextField.becomeFirstResponder()
        return super.becomeFirstResponder()
    }
    
    @discardableResult
    override public func resignFirstResponder() -> Bool {
        inputTextField.resignFirstResponder()
        return super.resignFirstResponder()
    }
    
    public func isFilled() -> Bool {
        inputTextField.text?.count == limitPasswordCount
    }
    
    public func text() -> String? {
        inputTextField.text
    }
    
    public func resetText() {
        inputTextField.text = String()
        for i in 0..<stackView.arrangedSubviews.count {
            if let digit = stackView.arrangedSubviews[i] as? InputDigit {
                digit.text = String()
            }
        }
        setStyleOnDigts(for: String())
    }
    
    // MARK: - Private Methods
    
    private func addPassword(with limit: Int) {
        guard stackView.arrangedSubviews.count <= limit else {
            return
        }
        
        limitPasswordCount = limit
        
        for _ in 1...limit {
            let textField = InputDigit()
            textField.isSecureTextEntry = isSecureTextEntry
            stackView.addArrangedSubview(textField)
        }
    }
    
    private func reloadPasswordFields() {
        guard let value = inputTextField.text else {
            return
        }
            
        var index = 0
        for view in stackView.arrangedSubviews {
            guard let textField = view as? UITextField else {
                continue
            }
            
            var char = ""
            if index < value.count {
                char = String(Array(value)[index])
            }
            
            textField.text = char
            index += 1
        }
    }
    
    @objc
    private func inputTextFieldFirstResponder() {
        inputTextField.becomeFirstResponder()
    }
    
    @objc
    private func textFieldDidChange(_ textField: UITextField) {
        reloadPasswordFields()
    }
    
    private func didChangeInputVisibility(_ isVisible: Bool) {
        setSecurity(isVisible)
        toogleSecurityButton.setImage(isVisible ? Assets.icoPasswordEyeClosed.image : Assets.icoPasswordEyeOpened.image,
                                      for: .normal)
    }
    
    @objc
    private func didToogleVisibility() {
        isInputVisible.toggle()
        delegate?.tapHiddenForm(isHidden: isInputVisible)
    }
    
    private func setupAccessibility() {
        inputTextField.isAccessibilityElement = true
        contentView.accessibilityElements = [inputTextField]
    }
}

extension InputDigitsForm {
    private func setupView() {
        buildHierarchy()
        buildConstraints()
        configure()
    }
    
    private func buildHierarchy() {
        addSubview(contentView)
        contentView.addSubview(inputTextField)
        contentView.addSubview(stackView)
        contentView.addSubview(toogleSecurityButton)
    }
    
    private func buildConstraints() {
        NSLayoutConstraint.leadingTrailing(equalTo: self,
                                           for: [contentView],
                                           constant: 0)
        
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: topAnchor),
            contentView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
        
        NSLayoutConstraint.leadingTrailing(equalTo: contentView,
                                           for: [inputTextField],
                                           constant: 0)
        
        NSLayoutConstraint.activate([
            toogleSecurityButton.leadingAnchor.constraint(equalTo: stackView.trailingAnchor, constant: 6),
            toogleSecurityButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            toogleSecurityButton.centerYAnchor.constraint(equalTo: stackView.centerYAnchor),
            toogleSecurityButton.heightAnchor.constraint(equalToConstant: 32),
            toogleSecurityButton.widthAnchor.constraint(equalToConstant: 32)
        ])
        
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            stackView.topAnchor.constraint(equalTo: contentView.topAnchor),
            stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
        
        NSLayoutConstraint.activate([
            inputTextField.topAnchor.constraint(equalTo: contentView.topAnchor),
            inputTextField.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }
    
    private func configure() {
        addPassword(with: 4)
    }
    
    private func setStyleOnDigts(for string: String) {
        let digits = stackView.arrangedSubviews.compactMap { $0 as? InputDigit }
        for (index, digit) in digits.enumerated() {
            if string.count >= limitPasswordCount {
                digit.setStyle(.highlighted)
            } else if index < string.count {
                digit.setStyle(.filled)
            } else if index == string.count {
                digit.setStyle(.highlighted)
            } else if !showError.isEmpty {
                digit.setStyle(.error)
            } else {
                digit.setStyle(.unfilled)
            }
        }
    }
    private func setErrorSyleOnDigts() {
        let digits = stackView.arrangedSubviews.compactMap { $0 as? InputDigit }
        digits.forEach { $0.setStyle(.error) }
    }
    
    private func setSecurity(_ value: Bool) {
        stackView.arrangedSubviews.forEach { ($0 as? InputDigit)?.isSecureTextEntry = value }
    }
    private func setToogleSecurityVisible(_ value: Bool) {
        toogleSecurityButton.isHidden = !value
        toogleSecurityButton.isUserInteractionEnabled = value
    }
}

extension InputDigitsForm: UITextFieldDelegate {
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let value = textField.text else {
            return true
        }
        
        if string.isEmpty {
            let newString = String(value.dropLast())
            let filled = newString.count >= limitPasswordCount
            delegate?.formIsFilled(filled)
            setStyleOnDigts(for: newString)
            
            return newString.count <= limitPasswordCount
        } else {
            let newString = "\(value)\(string)"
            let filled = newString.count >= limitPasswordCount
            delegate?.formIsFilled(filled)
            setStyleOnDigts(for: newString)
            
            return newString.count <= limitPasswordCount
        } 
    }
}
