import UIKit

public class InputDigit: UITextField {
    public enum InputDigitStyle {
        case unfilled
        case filled
        case highlighted
        case error
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setStyle(_ style: InputDigitStyle) {
        switch style {
        case .unfilled:
            layer.borderWidth = 0
        case .filled:
            layer.borderWidth = 1
            layer.borderColor = Palette.ppColorGrayscale400.color.cgColor
        case .highlighted:
            layer.borderWidth = 1
            layer.borderColor = Palette.ppColorBranding300.color.cgColor
        case .error:
            layer.borderWidth = 1
            layer.borderColor = Palette.ppColorNegative400.color.cgColor
        }
    }
    
    private func configure() {
        isEnabled = false
        backgroundColor = Colors.backgroundSecondary.color
        layer.cornerRadius = 14
        layer.borderColor = Colors.branding300.color.cgColor
        layer.borderWidth = 0
        keyboardType = .numberPad
        isAccessibilityElement = false
        textAlignment = .center
    }
}
