import UIKit

public final class ConfirmButton: UIButton {
    private var tempTitle: String?
    private var tempAttributedTitle: NSAttributedString?
    private var tempImage: UIImage?
    private var tempState: UIControl.State?

    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activity = UIActivityIndicatorView(style: .white)
        activity.translatesAutoresizingMaskIntoConstraints = false
        activity.isHidden = true
        activity.hidesWhenStopped = true
        activity.tintColor = Palette.ppColorGrayscale000.color
        return activity
    }()
    
    override public var backgroundColor: UIColor? {
        didSet {
            guard let backgroundColor = backgroundColor else {
                return
            }
            setBackgroundColor(color: backgroundColor, forState: .normal)
        }
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupInitialState()
        addAndCenterActivityIndicator()
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension ConfirmButton {
    func setupInitialState() {
        cornerRadius(24)
        fontSize(16)
        
        setTitleColor(Palette.white.color, for: .normal)
        
        setBackgroundColor(color: Palette.ppColorBranding300.color, forState: .normal)
        setBackgroundColor(color: Palette.ppColorGrayscale400.color(withCustomDark: .ppColorGrayscale500), forState: .disabled)
    }
    
    func addAndCenterActivityIndicator() {
        addSubview(activityIndicator)
        activityIndicator.layout {
            $0.centerX == centerXAnchor
            $0.centerY == centerYAnchor
        }
    }
    
    func startActivityIndicatorWith(_ style: UIActivityIndicatorView.Style) {
        activityIndicator.style = style
        activityIndicator.startAnimating()
    }
    
    func checkAndUpdateTitleAndImagesWithTemp() {
        if let state = tempState {
            setTitle(tempTitle, for: state)
            setAttributedTitle(tempAttributedTitle, for: state)
            setImage(tempImage, for: state)
        }
    }
    
    func clearButtonForLoading() {
        if let state = tempState {
            setTitle("", for: state)
            setAttributedTitle(nil, for: state)
            setImage(nil, for: state)
        }
    }
    
    func updateTempVarsWith(_ state: UIControl.State) {
        if let title = title(for: state), !title.isEmpty {
            tempTitle = title
        }
        tempAttributedTitle = attributedTitle(for: state)
        tempImage = image(for: state)
        tempState = state
    }
}

public extension ConfirmButton {
    func cornerRadius(_ cornerRadius: CGFloat) {
        layer.cornerRadius = cornerRadius
    }
    
    func fontSize(_ fontSize: CGFloat) {
        titleLabel?.font = UIFont.boldSystemFont(ofSize: fontSize)
    }
    
    func startLoadingAnimating(style: UIActivityIndicatorView.Style = .white, isEnabled: Bool = false) {
        self.isEnabled = isEnabled
        startActivityIndicatorWith(style)
        updateTempVarsWith(isEnabled ? .normal : .disabled)
        clearButtonForLoading()
    }
    
    func stopLoadingAnimating(isEnabled: Bool = true) {
        activityIndicator.stopAnimating()
        checkAndUpdateTitleAndImagesWithTemp()
        self.isEnabled = isEnabled
    }
}

extension ConfirmButton {
    func setBackgroundColor(color: UIColor, forState: UIControl.State) {
        self.clipsToBounds = true  // add this to maintain corner radius
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        if let context = UIGraphicsGetCurrentContext() {
            context.setFillColor(color.cgColor)
            context.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
            let colorImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            self.setBackgroundImage(colorImage, for: forState)
        }
    }
}
