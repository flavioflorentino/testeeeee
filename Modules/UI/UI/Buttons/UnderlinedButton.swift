import Foundation
import UIKit

public class UnderlinedButton: UIButton {
    override public func setTitle(_ title: String?, for state: UIControl.State) {
        super.setTitle(title, for: state)
        setup()
    }

    private func setup(with color: UIColor? = nil, highlightedColor: UIColor? = nil, font: UIFont? = nil) {
        var title = String()
        if let titleFromState = self.title(for: .normal) {
            title = titleFromState
        }
        let normalAttributes: [NSAttributedString.Key: Any] = [
            .font: font ?? UIFont.boldSystemFont(ofSize: 16),
            .foregroundColor: color ?? Palette.ppColorBranding300.color,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        let normalAttributesString = NSMutableAttributedString(string: title, attributes: normalAttributes)
        self.setAttributedTitle(normalAttributesString, for: .normal)
        
        let highlightedAttributes: [NSAttributedString.Key: Any] = [
            .font: font ?? UIFont.boldSystemFont(ofSize: 16),
            .foregroundColor: highlightedColor ?? Palette.ppColorBranding400.color,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        let highlightedAttributesString = NSMutableAttributedString(string: title, attributes: highlightedAttributes)
        self.setAttributedTitle(highlightedAttributesString, for: .highlighted)
    }
    
    public func updateColor(_ color: UIColor, highlightedColor: UIColor, font: UIFont? = nil) {
        setup(with: color, highlightedColor: highlightedColor, font: font)
    }
}
