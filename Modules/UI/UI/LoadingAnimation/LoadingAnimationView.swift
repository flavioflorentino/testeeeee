import Foundation
import Lottie
import UIKit

public final class LoadingAnimationView: UIView {
    private lazy var animationView: AnimationView = {
        let view = AnimationView()
        view.contentMode = .scaleAspectFit
        view.loopMode = .loop
        return view
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .large))
            .with(\.textAlignment, .center)
            .with(\.textColor, Colors.black.color)
        return label
    }()
    
    private lazy var progressView: UIProgressView = {
        let progressView = UIProgressView()
        progressView.progressTintColor = Colors.branding600.color
        progressView.trackTintColor = Colors.grayscale300.color
        progressView.isHidden = true
        return progressView
    }()
    
    public var progressValue: Float = 0 {
        didSet {
            progressView.setProgress(progressValue, animated: true)
        }
    }

    public init(hideProgressView: Bool = true, frame: CGRect = .zero) {
        super.init(frame: frame)
        progressView.isHidden = hideProgressView
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension LoadingAnimationView: ViewConfiguration {
    public func configureViews() {
        backgroundColor = Colors.white.color
    }

    public func buildViewHierarchy() {
        addSubview(animationView)
        addSubview(progressView)
        addSubview(titleLabel)
    }

    public func setupConstraints() {
        animationView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base10)
            $0.height.equalTo(animationView.snp.width)
            $0.centerY.equalToSuperview()
        }
        
        progressView.snp.makeConstraints {
            $0.top.equalTo(animationView.snp.bottom)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        titleLabel.snp.makeConstraints {
            progressView.isHidden ?
                $0.top.equalTo(animationView.snp.bottom) :
                $0.top.equalTo(progressView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    public func startProgressBar(withDuration duration: TimeInterval, completion: (() -> Void)?) {
        DispatchQueue.main.async {
            UIView.animate(withDuration: duration) {
                self.progressValue = 1.0
            }
            DispatchQueue.global().asyncAfter(deadline: .now() + duration) {
                guard let completion = completion else { return }
                completion()
            }
        }
    }

    public func start(with title: String?) {
        titleLabel.text = title
        titleLabel.isHidden = title == nil
        animationView.stop()
        guard let animation = Animation.named("loading-animation", bundle: Bundle(for: LoadingAnimationView.self)) else {
            return
        }
        animationView.animation = animation
        animationView.play()
    }
}
