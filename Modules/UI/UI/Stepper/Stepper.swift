import UIKit

public final class Stepper: UIControl {
    // MARK: - Values
    public var value: Double = 0.0 {
        didSet {
            valueTextField.text = stepperTextMask.string(from: value)
        }
    }
    public var stepValue: Double = 1.0
    public var minimumValue: Double = 0.0 {
        didSet {
            stepperTextMask.minimumValue = minimumValue
            valueTextField.text = stepperTextMask.string(from: value)
        }
    }
    public var maximumValue: Double = 10.0 {
        didSet {
            stepperTextMask.maximumValue = maximumValue
            valueTextField.text = stepperTextMask.string(from: value)
        }
    }
    
    // MARK: - Options
    public var allowsTextEditing: Bool = false {
        didSet {
            valueTextField.isEnabled = allowsTextEditing
        }
    }
    public var numberOfFractionDigits: Int = 0 {
        didSet {
            stepperTextMask.numberOfFractionDigits = numberOfFractionDigits
            valueTextField.text = stepperTextMask.string(from: value)
        }
    }
    
    public var rightAccessoryText: String? {
        didSet {
            rightAccessoryLabel.text = rightAccessoryText
            rightAccessoryLabel.isHidden = rightAccessoryText?.isEmpty ?? true
        }
    }
    
    private lazy var stepperTextMask = StepperDecimalNumberTextMask(
        numberOfFractionDigits: numberOfFractionDigits,
        minimumValue: minimumValue,
        maximumValue: maximumValue
    )
    
    private lazy var valueTextFieldMasker = TextFieldMasker(textMask: stepperTextMask)
    
    private lazy var decreaseButton: UIButton = {
        let button = UIButton()
        button.setImage(Assets.icoStepperDecrease.image, for: .normal)
        button.addTarget(self, action: #selector(decreaseButtonTapped), for: .touchUpInside)
        return button
    }()
    
    private lazy var increaseButton: UIButton = {
        let button = UIButton()
        button.setImage(Assets.icoStepperIncrease.image, for: .normal)
        button.addTarget(self, action: #selector(increaseButtonTapped), for: .touchUpInside)
        return button
    }()
    
    private lazy var valueTextField: VariableWidthTextField = {
        let textField = VariableWidthTextField()
        textField.keyboardType = .numberPad
        textField.font = Typography.title(.medium).font()
        textField.textAlignment = .center
        textField.tintColor = Colors.branding400.color
        textField.isEnabled = allowsTextEditing
        textField.text = stepperTextMask.string(from: value)
        textField.textColor = Colors.grayscale700.color
        textField.inputAccessoryView = DoneToolBar(doneText: "OK")
        return textField
    }()
    
    private lazy var rightAccessoryLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.title(.medium).font()
        label.textColor = Colors.grayscale700.color
        return label
    }()
    
    private lazy var valueStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [valueTextField, rightAccessoryLabel])
        stackView.axis = .horizontal
        stackView.alignment = .center
        return stackView
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [decreaseButton, valueStackView, increaseButton])
        stackView.axis = .horizontal
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        buildLayout()
    }
}

extension Stepper: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(stackView)
    }
    
    public func setupConstraints() {
        stackView.layout {
            $0.top == topAnchor
            $0.trailing == trailingAnchor
            $0.bottom == bottomAnchor
            $0.leading == leadingAnchor
        }
        
        valueTextField.layout {
            $0.width >= 20
        }
    }
    
    public func configureViews() {
        valueTextFieldMasker.bind(to: valueTextField)
        valueTextField.addTarget(self, action: #selector(valueTextFieldEditingChanged(_:)), for: .editingChanged)
        valueTextField.addTarget(self, action: #selector(valueTextFieldDidBeginEditing(_:)), for: .editingDidBegin)
    }
}

@objc
private extension Stepper {
    func decreaseButtonTapped() {
        valueTextField.resignFirstResponder()
        value = max(minimumValue, value - stepValue)
        sendActions(for: .valueChanged)
    }
    
    func increaseButtonTapped() {
        valueTextField.resignFirstResponder()
        value = min(maximumValue, value + stepValue)
        sendActions(for: .valueChanged)
    }
    
    func valueTextFieldDidBeginEditing(_ textField: UITextField) {
        DispatchQueue.main.async {
            textField.selectedTextRange = textField.textRange(from: textField.endOfDocument, to: textField.endOfDocument)
        }
    }
    
    func valueTextFieldEditingChanged(_ textField: UITextField) {
        value = stepperTextMask.decimalNumber(from: textField.text)
        sendActions(for: .valueChanged)
    }
}
