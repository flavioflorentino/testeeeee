import Foundation

final class StepperDecimalNumberTextMask: TextMask {
    var numberOfFractionDigits: Int {
        didSet {
            numberFormatter.minimumFractionDigits = numberOfFractionDigits
            numberFormatter.maximumFractionDigits = numberOfFractionDigits
        }
    }
    
    var maximumValue: Double
    var minimumValue: Double
    
    private lazy var numberFormatter: NumberFormatter = {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        numberFormatter.minimumFractionDigits = numberOfFractionDigits
        numberFormatter.maximumFractionDigits = numberOfFractionDigits
        numberFormatter.locale = Locale(identifier: "pt_BR")
        return numberFormatter
    }()
    
    init(numberOfFractionDigits: Int, minimumValue: Double, maximumValue: Double) {
        self.numberOfFractionDigits = numberOfFractionDigits
        self.minimumValue = minimumValue
        self.maximumValue = maximumValue
    }
    
    func maskedText(from originalText: String?) -> String? {
        guard
            let string = originalText?.filter({ $0.unicodeScalars.allSatisfy(CharacterSet.decimalDigits.contains(_:)) }),
            var doubleValue = Double(string)
            else {
                return nil
        }
        if numberOfFractionDigits > 0 {
            doubleValue /= Double(numberOfFractionDigits) * 10.0
        }
        return numberFormatter.string(from: min(maximumValue, max(minimumValue, doubleValue)) as NSNumber)
    }
    
    func decimalNumber(from string: String?) -> Double {
        guard let string = string else {
            return 0.0
        }
        return numberFormatter.number(from: string)?.doubleValue ?? 0.0
    }
    
    func string(from double: Double?) -> String? {
        guard let value = double else {
            return nil
        }
        return numberFormatter.string(from: value as NSNumber)
    }
}
