import UIKit

final class VariableWidthTextField: UITextField {
    override var text: String? {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        guard
            let text = text,
            !text.isEmpty
            else {
                return super.intrinsicContentSize
        }
        let string = text as NSString
        var size = string.size(withAttributes: typingAttributes)
        size.width += Spacing.base00
        return size
    }
    
    private func setup() {
        addTarget(self, action: #selector(textEditingChanged(_:)), for: .editingChanged)
    }
    
    @objc
    private func textEditingChanged(_ textField: UITextField) {
        textField.invalidateIntrinsicContentSize()
    }
}
