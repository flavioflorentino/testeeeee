import UIKit

public final class CheckboxButton: UIButton {
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupImages()
        setupTarget()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupTarget() {
        addTarget(self, action: #selector(toggleSelection), for: .touchUpInside)
    }
    
    private func setupImages() {
        setImage(Assets.icoButtonGlyphUnchecked.image, for: .normal)
        setImage(Assets.icoButtonGlyphUnchecked.image, for: [.normal, .highlighted])
        setImage(Assets.icoButtonGlyphChecked.image, for: .selected)
        setImage(Assets.icoButtonGlyphChecked.image, for: [.selected, .highlighted])
    }
    
    @objc
    private func toggleSelection() {
        isSelected.toggle()
        sendActions(for: .valueChanged)
    }
}
