import UIKit

final class PresentPopupAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    private let duration = 0.3
    var isPresenting = true
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        let popupView = transitionContext.view(forKey: .to) ?? transitionContext.view(forKey: .from)
        
        guard let popup = popupView else {
            transitionContext.completeTransition(true)
            return
        }
        
        if isPresenting {
            containerView.addSubview(popup)
            popup.frame = containerView.frame
            popup.frame.origin.y += popup.frame.size.height
        }

        UIView.animate(withDuration: duration,
                       delay: 0,
                       options: .curveEaseIn,
                       animations: {
            if self.isPresenting {
                popup.alpha = 1
                popup.frame.origin.y -= popup.frame.size.height
                popup.layer.cornerRadius = CornerRadius.medium
                containerView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            } else {
                popup.alpha = 0
                popup.frame.origin.y += popup.frame.size.height
                popup.layer.cornerRadius = CornerRadius.light
                containerView.backgroundColor = .clear
            }
        }, completion: { _ in
            if !self.isPresenting {
                popup.removeFromSuperview()
            }
            transitionContext.completeTransition(true)
        })
    }
}
