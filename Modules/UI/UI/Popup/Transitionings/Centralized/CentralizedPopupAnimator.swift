import UIKit

class CentralizedPopupAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    let duration = 0.3
    var isPresenting = true
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        let popupView = transitionContext.view(forKey: .to) ?? transitionContext.view(forKey: .from)
        
        guard let popup = popupView else {
            transitionContext.completeTransition(true)
            return
        }
        
        if isPresenting {
            containerView.addSubview(popup)
            setupSizeAndPosition(for: popup, superview: containerView)
        }
        
        popup.transform = isPresenting ? CGAffineTransform(scaleX: 1.2, y: 1.2) : CGAffineTransform(scaleX: 1.0, y: 1.0)
        popup.alpha = isPresenting ? 0.0 : 1.0
        UIView.animate(withDuration: duration,
                       delay: 0,
                       options: .curveEaseInOut,
                       animations: {
            containerView.backgroundColor = self.isPresenting ? UIColor.black.withAlphaComponent(0.5) : UIColor.clear
            popup.alpha = self.isPresenting ? 1.0 : 0.0
            popup.transform = self.isPresenting ? CGAffineTransform(scaleX: 1.0, y: 1.0) : CGAffineTransform(scaleX: 0.8, y: 0.8)
            popup.layer.cornerRadius = self.isPresenting ? 10.0 : 8.0
        }, completion: { _ in
            if !self.isPresenting {
                popup.removeFromSuperview()
            }
            transitionContext.completeTransition(true)
        })
    }
    
    private func setupSizeAndPosition(for view: UIView, superview: UIView) {
        guard view.frame.size == .zero else {
            view.center = superview.center
            return
        }
        
        let viewWidth: CGFloat = min(UIScreen.main.bounds.width - (2 * 12), 300)
        view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            view.widthAnchor.constraint(equalToConstant: viewWidth),
            view.centerXAnchor.constraint(equalTo: superview.centerXAnchor),
            view.centerYAnchor.constraint(equalTo: superview.centerYAnchor)
        ])
    }
}
