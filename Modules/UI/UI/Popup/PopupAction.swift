import UIKit

public class PopupAction: UIButton {
    @objc
    public enum PopupActionStyle: Int {
        case fill
        case tryAgain
        case `default`
        case link
        case destructive
        case simpleText
        
        var backgroundColor: UIColor {
            switch self {
            case .fill, .tryAgain:
                return Colors.brandingBase.color
            case .destructive:
                return Colors.critical600.color
            default:
                return .clear
            }
        }
        
        var attributes: [NSAttributedString.Key: Any] {
            switch self {
            case .fill, .tryAgain:
                return [
                    .font: Typography.linkSecondary().font(),
                    .foregroundColor: Colors.white.lightColor
                ]
            case .default:
                return [
                    .font: Typography.linkSecondary().font(),
                    .foregroundColor: Colors.grayscale400.color,
                    .underlineStyle: NSUnderlineStyle.single.rawValue
                ]
            case .link:
                return [
                    .font: Typography.linkSecondary().font(),
                    .foregroundColor: Colors.brandingBase.color,
                    .underlineStyle: NSUnderlineStyle.single.rawValue
                ]
            case .destructive:
                return [
                    .font: Typography.linkSecondary().font(),
                    .foregroundColor: Colors.white.lightColor
                ]
            case .simpleText:
                return [
                    .font: Typography.bodySecondary().font(),
                    .foregroundColor: Colors.grayscale500.color
                ]
            }
        }
    }
    
    public private(set) var completion: (() -> Void)?
    
    @objc
    public init(title: String, style: PopupActionStyle, completion: (() -> Void)? = nil) {
        super.init(frame: .zero)
        setTitle(title, for: .normal)
        backgroundColor = style.backgroundColor
        setAttributedTitle(NSMutableAttributedString(string: title, attributes: style.attributes), for: .normal)
        applyTryagainImage(style)
        self.completion = completion
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func applyTryagainImage(_ style: PopupActionStyle) {
        if style == .tryAgain {
            setImage(Assets.icoRefresh.image, for: .normal)
            imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 16)
        }
    }
}
