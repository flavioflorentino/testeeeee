import UIKit

@objc
extension UIViewController: PopupPresentable, UIViewControllerTransitioningDelegate {
    /// Show a viewController in a popup like style
    ///
    /// - Parameter popup: The viewController to show
    public func showPopup(_ popup: UIViewController) {
        popup.modalPresentationStyle = .custom
        popup.transitioningDelegate = self
        popup.definesPresentationContext = true
        self.present(popup, animated: true, completion: nil)
    }
    
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = CentralizedPopupAnimator()
        transition.isPresenting = true
        return transition
    }
    
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = CentralizedPopupAnimator()
        transition.isPresenting = false
        return transition
    }
}
