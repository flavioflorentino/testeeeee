import UIKit

@objc
public protocol PopupPresentable {
    func showPopup(_ popup: UIViewController)
    @objc
    optional func showPopup(_ popup: UIViewController, fromViewController controller: UIViewController)
}

/// A class that presents viewController in popup like style.
@objc
public class PopupPresenter: NSObject, PopupPresentable {
    /// The transitioningDelegate for the presentation, The default object uses a centralized alpha/scale animation.
    private let transition: UIViewControllerTransitioningDelegate
    
    /// Initialize the Presentator with default transitioningDelegate
    ///
    /// - Parameter transitioningDelegate: The transitioningDelegate object
    @objc
    public init(transitioningDelegate: UIViewControllerTransitioningDelegate = CentralizedTransitioning()) {
        transition = transitioningDelegate
    }

    override public init() {
        transition = CentralizedTransitioning()
    }
    
    /// Present a popup viewcontroller from the topViewController in the hierarchy
    ///
    /// - Parameter popup: the popup to present
    @objc
    public func showPopup(_ popup: UIViewController) {
        guard let topMostViewController = topMostViewController else {
            debugPrint("Could not find the topMostViewController for the presentation.")
            return
        }
        popup.modalPresentationStyle = .custom
        popup.transitioningDelegate = transition
        popup.definesPresentationContext = true
        topMostViewController.present(popup, animated: true)
    }
    
    /// Present the PopupViewController from a viewController of your choice
    ///
    /// - Parameters:
    ///   - popup: The popup to present
    ///   - controller: The controller that will present the popup
    @objc
    public func showPopup(_ popup: UIViewController, fromViewController controller: UIViewController) {
        popup.modalPresentationStyle = .overCurrentContext
        popup.transitioningDelegate = transition
        popup.definesPresentationContext = true
        controller.present(popup, animated: true)
    }
    
    /// The topMostViewController in the viewHierarchy
    private var topMostViewController: UIViewController? {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            return topController
        }
        return nil
    }
}
