import UIKit

public enum PopupType {
    case business
    case image(UIImage)
    case custom(UIViewController)
    case customView(UIView, shouldRemoveCloseButton: Bool = false)
    case text
    case none
}

private extension PopupViewController.Layout {
    private static let alertSize = UIScreen.main.bounds.width - (2 * Spacing.base02)
    static let popupWidth: CGFloat = min(alertSize, 300)
    static let textBold = UIFont.systemFont(ofSize: 20, weight: .bold)
    static let textRegular = UIFont.systemFont(ofSize: 14, weight: .regular)
    static let buttonSize: CGFloat = 48.0
    static let stackViewSpacing: CGFloat = 16
    static let stackViewTop: CGFloat = 80
    static let imageSize: CGFloat = 88
    static let closeButtonSize = CGSize(width: 24, height: 24)
}

public final class PopupViewController: UIViewController {
    fileprivate enum Layout {}
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Colors.white.color
        view.layer.cornerRadius = CornerRadius.strong
        return view
    }()
    
    private lazy var closeButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(Assets.icoClose.image, for: .normal)
        button.setTitleColor(Palette.ppColorGrayscale400.color, for: .normal)
        button.tintColor = Palette.ppColorGrayscale400.color
        button.imageView?.tintColor = Palette.ppColorGrayscale400.color
        button.addTarget(self, action: #selector(tapClose), for: .touchUpInside)
        return button
    }()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .large))
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var descriptionLabel: UITextView = {
        let text = UITextView()
        text.translatesAutoresizingMaskIntoConstraints = false
        text.textAlignment = .center
        text.textColor = Colors.grayscale700.color
        text.isEditable = false
        text.isSelectable = true
        text.isScrollEnabled = false
        text.backgroundColor = .clear
        text.textContainerInset = .zero
        text.textContainer.lineFragmentPadding = 0
        return text
    }()
        
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = Layout.stackViewSpacing
        return stackView
    }()
    
    @objc public var didCloseDismiss: (() -> Void)?
    @objc public var hideCloseButton: Bool = false {
        didSet {
            closeButton.isHidden = hideCloseButton
        }
    }
    
    public init(title: String?, description: String? = nil, preferredType: PopupType = .none) {
        super.init(nibName: nil, bundle: nil)
        configureTitleAndDescriptionIfNeeded(title, description)
        setupView()
        configurePreferredType(preferredType)
    }
    
    @objc
    public init(title: String?, description: String? = nil, image: UIImage?) {
        super.init(nibName: nil, bundle: nil)
        configureTitleAndDescriptionIfNeeded(title, description)
        setupView()
        guard let image = image else {
            return
        }
        configurePreferredType(.image(image))
    }
    
    @objc
    public convenience init(customView: CustomViewPopupType) {
        self.init(title: nil, preferredType: .customView(customView.view))
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    public func addAction(_ action: PopupAction) {
        stackView.addArrangedSubview(action)
        action.addTarget(self, action: #selector(tapAction(_:)), for: .touchUpInside)
        action.heightAnchor.constraint(equalToConstant: Layout.buttonSize).isActive = true
        action.layer.cornerRadius = Layout.buttonSize / 2
    }

    @objc
    public func addActions(_ actions: [PopupAction]) {
        actions.forEach { addAction($0) }
    }
    
    @objc
    public func setAttributeDescription(
        _ attribute: NSMutableAttributedString?,
        linkAttributes: [NSAttributedString.Key: Any] = [:]
    ) {
        if let description = attribute {
            descriptionLabel.attributedText = description
            stackView.addArrangedSubview(descriptionLabel)
        }
        
        if !linkAttributes.isEmpty {
            descriptionLabel.linkTextAttributes = linkAttributes
            descriptionLabel.delegate = self
        }
    }
}

@objc
private extension PopupViewController {
    func tapAction( _ sender: PopupAction) {
        dismiss(animated: true) {
            sender.completion?()
        }
    }
    
    func tapClose() {
        dismiss(animated: true) { [weak self] in
            self?.didCloseDismiss?()
        }
    }
}

private extension PopupViewController {
    func configureTitleAndDescriptionIfNeeded(_ title: String?, _ description: String?) {
        if let title = title {
            titleLabel.text = title
            stackView.addArrangedSubview(titleLabel)
            if #available(iOS 11, *) {
                stackView.setCustomSpacing(Spacing.base01, after: titleLabel)
            }
        }
        
        if let description = description {
            descriptionLabel.text = description
            stackView.addArrangedSubview(descriptionLabel)
        }
    }
    
    func configurePreferredType(_ type: PopupType) {
        switch type {
        case .business:
            closeButton.removeFromSuperview()
            titleLabel.labelStyle(TitleLabelStyle(type: .small))
            descriptionLabel.font = Typography.bodySecondary().font()
            descriptionLabel.textColor = Colors.grayscale600.color
        case .image(let image):
            imageView.image = image
            stackView.insertArrangedSubview(imageView, at: 0)
            
            imageView.layout {
                $0.height == Layout.imageSize
            }
        case .custom(let controller):
            addChild(controller)
            stackView.addArrangedSubview(controller.view)
            controller.didMove(toParent: self)
        case let .customView(view, shouldRemoveCloseButton):
            if shouldRemoveCloseButton {
                closeButton.removeFromSuperview()
            }
            stackView.addArrangedSubview(view)
        case .text:
            closeButton.removeFromSuperview()
            stackView.layout {
                $0.top == containerView.topAnchor + Spacing.base03
            }
        case .none:
            break
        }
    }
}

extension PopupViewController: UITextViewDelegate {
    public func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        true
    }
}

extension PopupViewController: ViewConfiguration {
    public func buildViewHierarchy() {
        view.addSubview(containerView)
        containerView.addSubview(stackView)
        containerView.addSubview(closeButton)
    }
    
    public func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.centerX.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        closeButton.snp.makeConstraints {
            $0.top.equalTo(containerView).offset(Spacing.base03)
            $0.trailing.equalTo(containerView).offset(-Spacing.base03)
            $0.size.equalTo(Layout.closeButtonSize)
        }
        
        stackView.snp.makeConstraints {
            $0.top.equalTo(containerView).offset(Spacing.base04)
            $0.leading.equalTo(containerView).offset(Spacing.base03)
            $0.trailing.bottom.equalTo(containerView).offset(-Spacing.base03)
        }
    }
    
    public func configureViews() { }
    
    private func setupView() {
        buildViewHierarchy()
        configureViews()
        setupConstraints()
    }
}

public protocol PopupTypeConvertible {
    var popupType: PopupType { get }
}

@objc
public final class CustomViewPopupType: NSObject {
    public let view: UIView
    
    @objc
    public init(view: UIView) {
        self.view = view
        super.init()
    }
}

extension CustomViewPopupType: PopupTypeConvertible {
    public var popupType: PopupType {
        .customView(view)
    }
}
