import UIKit
import SkeletonView

public final class TableViewDataSource<Section: Hashable, Item>: ReloadableDataSource<UITableView, UITableViewCell, Section, Item>, SkeletonTableViewDataSource {
    // MARK: Aliases
    
    public typealias SectionTitleProvider = (_ tableView: UITableView, _ sectionIndex: Int, _ section: Section) -> String?
    public typealias SkeletonCellIdentifierProvider = (_ tableView: UITableView, _ indexPath: IndexPath) -> String?
    
    // MARK: - Providers
    
    public var headerSectionTitleProvider: SectionTitleProvider?
    public var footerSectionTitleProvider: SectionTitleProvider?
    public var skeletonCellIdentifierProvider: SkeletonCellIdentifierProvider?
    
    // MARK: - Table view data source
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        sections.count
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data[sections[section]]?.count ?? 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let item = self.item(at: indexPath), let cell = itemProvider?(tableView, indexPath, item) else {
            return UITableViewCell()
        }
        return cell
    }
    
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        headerSectionTitleProvider?(tableView, section, sections[section])
    }
    
    public func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        footerSectionTitleProvider?(tableView, section, sections[section])
    }
    
    public func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        skeletonCellIdentifierProvider?(skeletonView, indexPath) ?? ""
    }
}
