protocol ErrorInteracting {
    func loadInfo()
}

struct ErrorInteractor {
    private let errorInfo: ErrorInfoModel
    private let presenter: ErrorPresenting
    
    init(errorInfo: ErrorInfoModel, presenter: ErrorPresenting) {
        self.errorInfo = errorInfo
        self.presenter = presenter
    }
}

extension ErrorInteractor: ErrorInteracting {
    func loadInfo() {
        presenter.present(info: errorInfo)
    }
}
