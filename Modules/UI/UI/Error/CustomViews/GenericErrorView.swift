import UIKit

public class GenericErrorView: UIView {
    private enum Layout {
        static let leadingTrailingMargin: CGFloat = 32
        static let bottomMargingBottomButton: CGFloat = 22
    }
    private lazy var errorImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = Assets.icoWarningBad.image
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Oops!\nAlgo deu errado."
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = Palette.ppColorGrayscale500.color
        label.numberOfLines = 0
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var tryAgainAttributedLabel: UILabel = {
        let label = UILabel()
        label.text = "Ocorreu um erro ao carregar as informações. Verifique sua conexão e\ntoque aqui para tentar novamente"
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = Palette.ppColorGrayscale400.color
        label.numberOfLines = 0
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        label.translatesAutoresizingMaskIntoConstraints = false
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTouchOnTryAgain))
        label.addGestureRecognizer(tap)
        
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let stack = UIStackView()
        stack.distribution = UIStackView.Distribution.fill
        stack.alignment = UIStackView.Alignment.center
        stack.axis = .vertical
        stack.spacing = 18
        stack.isUserInteractionEnabled = true
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    private lazy var bottomButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(didTapBottomButton), for: .touchUpInside)
        button.isHidden = true
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    @objc public var tryAgainTapped: () -> Void = {}
    private var bottomButtonTapped: (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildViewHierarchy()
        setupConstraints()
        configureViews()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func buildViewHierarchy() {
        addSubview(bottomButton)
        addSubview(stackView)
        stackView.addArrangedSubview(errorImageView)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(tryAgainAttributedLabel)
    }
    
    private func setupConstraints() {
        var compatibleLayoutBottomAchor: NSLayoutYAxisAnchor
        if #available(iOS 11, *) {
            compatibleLayoutBottomAchor = safeAreaLayoutGuide.bottomAnchor
        } else {
            compatibleLayoutBottomAchor = bottomAnchor
        }
        
        NSLayoutConstraint.activate([
            stackView.centerXAnchor.constraint(equalTo: centerXAnchor),
            stackView.centerYAnchor.constraint(equalTo: centerYAnchor),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Layout.leadingTrailingMargin),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Layout.leadingTrailingMargin),
            
            bottomButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            bottomButton.bottomAnchor.constraint(equalTo: compatibleLayoutBottomAchor, constant: -Layout.bottomMargingBottomButton)
        ])
    }
    
    private func configureViews() {
        backgroundColor = Palette.ppColorGrayscale100.color
        configureAttributedLabel()
    }
    
    public func setupBottomButton(title: String, isHidden: Bool, onTap: @escaping (() -> Void)) {
        let attributedTitle = NSAttributedString(
            string: title,
            attributes: [
                .underlineStyle: NSUnderlineStyle.single.rawValue,
                .foregroundColor: Palette.ppColorGrayscale400.color,
                .font: UIFont.systemFont(ofSize: 14, weight: .semibold)
            ]
        )
        bottomButton.setAttributedTitle(attributedTitle, for: .normal)
        bottomButton.isHidden = isHidden
        bottomButtonTapped = onTap
    }
    
    private func configureAttributedLabel() {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        paragraphStyle.lineSpacing = 4
        
        let attributedText = NSMutableAttributedString(
            string: tryAgainAttributedLabel.text ?? "",
            attributes: [.paragraphStyle: paragraphStyle]
        )
        guard let range = (tryAgainAttributedLabel.text as NSString?)?.range(of: "toque aqui para tentar novamente") else {
            return
        }
        
        attributedText.addAttributes(
            [
                .underlineStyle: NSUnderlineStyle.single.rawValue,
                .font: UIFont.boldSystemFont(ofSize: 14)
            ],
            range: range
        )
        tryAgainAttributedLabel.attributedText = attributedText
    }
    
    @objc
    private func didTouchOnTryAgain() {
        tryAgainTapped()
    }
    
    @objc
    private func didTapBottomButton() {
        bottomButtonTapped?()
    }
}
