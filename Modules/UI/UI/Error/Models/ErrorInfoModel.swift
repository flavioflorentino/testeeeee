import Foundation
import UIKit

public struct ErrorAction {
    let title: String
    let completion: (UIViewController) -> Void
    
    public init(title: String, completion: @escaping (UIViewController) -> Void) {
        self.title = title
        self.completion = completion
    }
}
public struct ErrorInfoModel {
    let image: UIImage
    let title: String
    let message: String
    let primaryAction: ErrorAction
    let secondaryAction: ErrorAction?
    
    public init(
        image: UIImage,
        title: String,
        message: String,
        primaryAction: ErrorAction,
        secondaryAction: ErrorAction? = nil
    ) {
        self.image = image
        self.title = title
        self.message = message
        self.primaryAction = primaryAction
        self.secondaryAction = secondaryAction
    }
}
