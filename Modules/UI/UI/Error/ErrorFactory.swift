public enum ErrorFactory {
    public static func make(with errorInfo: ErrorInfoModel) -> UIViewController {
        let presenter: ErrorPresenting = ErrorPresenter()
        let interactor = ErrorInteractor(errorInfo: errorInfo, presenter: presenter)
        let viewController = ErrorViewController(interactor: interactor)
        
        presenter.viewController = viewController

        return viewController
    }
}
