import Foundation
import SnapKit
import UIKit

protocol ErrorDisplayable: AnyObject {
    func display(info: ErrorInfoModel)
}

private extension ErrorViewController.Layout {
    enum Size {
        static let image = CGSize(width: 80, height: 80)
        static let titleHeight = CGFloat(28)
    }
}

final class ErrorViewController: ViewController<ErrorInteracting, UIView> {
    fileprivate enum Layout { }
    
    private lazy var containerView = UIView()
    
    private lazy var imageView = UIImageView()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var primaryButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
            .with(\.cornerRadius, .full)
        button.addTarget(self, action: #selector(didTouchPrimaryButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var secondaryButton: UIButton = {
        let button = UIButton()
        button.isHidden = true
        button.buttonStyle(SecondaryButtonStyle())
            .with(\.cornerRadius, .full)
        button.addTarget(self, action: #selector(didTouchSecondaryButton), for: .touchUpInside)
        return button
    }()
    
    var primaryAction: ((UIViewController) -> Void)?
    var secondaryAction: ((UIViewController) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.loadInfo()
    }
 
    override func buildViewHierarchy() {
        containerView.addSubview(imageView)
        containerView.addSubview(titleLabel)
        containerView.addSubview(messageLabel)
        containerView.addSubview(primaryButton)
        containerView.addSubview(secondaryButton)
        view.addSubview(containerView)
    }
    
    override func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        imageView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.centerX.equalToSuperview()
            $0.size.equalTo(Layout.Size.image)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        messageLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        primaryButton.snp.makeConstraints {
            $0.top.greaterThanOrEqualTo(messageLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        secondaryButton.snp.makeConstraints {
            $0.top.greaterThanOrEqualTo(primaryButton.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.bottom.equalToSuperview().inset(Spacing.base03)
        }
    }
        
    override func configureViews() {
        navigationController?.setNavigationBarHidden(true, animated: true)
        navigationItem.setHidesBackButton(true, animated: true)
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}

// MARK: View Model Outputs
extension ErrorViewController: ErrorDisplayable {
    func display(info: ErrorInfoModel) {
        imageView.image = info.image
        titleLabel.text = info.title
        messageLabel.text = info.message
        
        primaryButton.setTitle(info.primaryAction.title, for: .normal)
        primaryAction = info.primaryAction.completion
        
        if let title = info.secondaryAction?.title {
            secondaryButton.setTitle(title, for: .normal)
            secondaryButton.isHidden = false
            secondaryAction = info.secondaryAction?.completion
        }
    }
}

@objc
private extension ErrorViewController {
    func didTouchPrimaryButton() {
        primaryAction?(self)
    }
    
    func didTouchSecondaryButton() {
        secondaryAction?(self)
    }
}
