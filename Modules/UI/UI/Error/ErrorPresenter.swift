protocol ErrorPresenting: AnyObject {
    var viewController: ErrorDisplayable? { get set }
    
    func present(info: ErrorInfoModel)
}

final class ErrorPresenter: ErrorPresenting {
    weak var viewController: ErrorDisplayable?
    
    func present(info: ErrorInfoModel) {
        viewController?.display(info: info)
    }
}
