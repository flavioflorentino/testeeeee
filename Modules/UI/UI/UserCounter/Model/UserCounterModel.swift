import Foundation

public struct UserCounterModel {
    let imgUrl: URL?
    
    public init(imgUrl: URL?) {
        self.imgUrl = imgUrl
    }
}
