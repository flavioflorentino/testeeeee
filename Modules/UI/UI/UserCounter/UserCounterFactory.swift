import UIKit

public enum UserCounterFactory {
    public static func make(users: [UserCounterModel], backgroundColor: UIColor, placeholder: UIImage?) -> UIView {
        let presenter = UserCounterPresenter(placeholder: placeholder)
        let viewModel = UserCounterViewModel(presenter: presenter, users: users)
        let view = UserCounterView(viewModel: viewModel, backgroundColor: backgroundColor)
        presenter.view = view
        view.loadData()
        return view
    }
}
