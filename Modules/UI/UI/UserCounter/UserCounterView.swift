import UIKit
import SnapKit

protocol UserCounterDisplay: AnyObject {
    func displayUsers(_ users: [UserCounterModel], placeholder: UIImage?)
    func displayCount(text: String)
    func hideCount()
}

extension UserCounterView.Layout {
    enum Size {
        static let borderWidth: CGFloat = 2
        static let userImage = CGSize(width: 48, height: 48)
        static let labelCounter = CGSize(width: 20, height: 20)
    }
}

final class UserCounterView: UIView {
    fileprivate enum Layout {}
    
    // MARK: - Visual Components
    private lazy var viewContent: UIView = {
        let view = UIView()
        view.backgroundColor = backgroundColor
        return view
    }()
    
    private lazy var labelCounter: UILabel = {
        let label = UILabel()
        label.layer.cornerRadius = Layout.Size.labelCounter.height / 2
        label.labelStyle(BodySecondaryLabelStyle(type: .highlight))
        label.backgroundColor = Colors.branding300.color
        label.textColor = .white
        label.layer.masksToBounds = true
        label.textAlignment = .center
        return label
    }()
    
    // MARK: - Variables
    private let viewModel: UserCounterViewModeling
    
    // MARK: - Life Cycle
    init(viewModel: UserCounterViewModeling, backgroundColor: UIColor) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        self.backgroundColor = backgroundColor
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func loadData() {
        viewModel.loadData()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        viewContent.subviews.forEach { view in
            view.layer.borderColor = backgroundColor?.cgColor
        }
    }
    
    // MARK: - UserImageView
    private func createUserImageView(imgUrl: URL?, placeholder: UIImage?) -> UIImageView {
        let imageView = UIImageView()
        imageView.backgroundColor = backgroundColor
        imageView.layer.cornerRadius = Layout.Size.userImage.height / 2
        imageView.layer.borderColor = backgroundColor?.cgColor
        imageView.layer.borderWidth = Layout.Size.borderWidth
        imageView.layer.masksToBounds = true
        imageView.setImage(url: imgUrl, placeholder: placeholder)

        imageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.userImage)
        }
        
        return imageView
    }
    
    private func imageViewConstraintsUnique(current: UIView) {
        current.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    private func imageViewConstraintsFirst(current: UIView) {
        current.snp.makeConstraints {
            $0.leading.top.bottom.equalToSuperview()
        }
    }
    
    private func imageViewConstraints(current: UIView, toPrevious previous: UIView) {
        current.snp.makeConstraints {
            $0.leading.equalTo(previous).offset(Spacing.base01)
            $0.centerY.equalTo(previous)
        }
    }
    
    private func imageViewConstraintsFinalTo(current: UIView, toPrevious previous: UIView) {
        imageViewConstraints(current: current, toPrevious: previous)
        current.snp.makeConstraints {
            $0.trailing.equalToSuperview()
        }
    }
}

// MARK: - ViewConfiguration
extension UserCounterView: ViewConfiguration {
    func setupConstraints() {
        viewContent.snp.makeConstraints {
            $0.leading.top.equalToSuperview().offset(Spacing.base02)
            $0.trailing.bottom.equalToSuperview().inset(Spacing.base02)
        }
        
        labelCounter.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.labelCounter)
            $0.trailing.bottom.equalTo(viewContent).offset(Spacing.base00)
        }
    }
    
    func buildViewHierarchy() {
        addSubview(viewContent)
        addSubview(labelCounter)
    }
}

// MARK: - UserCounterDisplay
extension UserCounterView: UserCounterDisplay {
    func displayUsers(_ users: [UserCounterModel], placeholder: UIImage?) {
        guard users.count != 1 else {
            displayOneUser(user: users.first, placeholder: placeholder)
            return
        }
        displayManyUsers(users: users, placeholder: placeholder)
    }
    
    private func displayOneUser(user: UserCounterModel?, placeholder: UIImage?) {
        let imageView = createUserImageView(imgUrl: user?.imgUrl, placeholder: placeholder)
        viewContent.addSubview(imageView)
        imageViewConstraintsUnique(current: imageView)
    }
    
    private func displayManyUsers(users: [UserCounterModel], placeholder: UIImage?) {
        let lastIndex = users.count - 1
        var previousView: UIView?
        for (index, user) in users.enumerated() {
            let imageView = createUserImageView(imgUrl: user.imgUrl, placeholder: placeholder)
            viewContent.addSubview(imageView)
            viewContent.sendSubviewToBack(imageView)
            if index == 0 {
                imageViewConstraintsFirst(current: imageView)
            } else {
                guard let previousView = previousView else {
                    return
                }
                if index == lastIndex {
                    imageViewConstraintsFinalTo(current: imageView, toPrevious: previousView)
                } else {
                    imageViewConstraints(current: imageView, toPrevious: previousView)
                }
            }
            previousView = imageView
        }
    }
    
    func displayCount(text: String) {
        labelCounter.text = text
    }
    
    func hideCount() {
        labelCounter.isHidden = true
    }
}
