import Foundation

protocol UserCounterViewModeling {
    func loadData()
}

final class UserCounterViewModel {
    // MARK: - Variables
    private let presenter: UserCounterPresenting
    private let users: [UserCounterModel]
    
    // MARK: - Life Cycle
    init(presenter: UserCounterPresenting, users: [UserCounterModel]) {
        self.presenter = presenter
        self.users = users
    }
}

// MARK: - UserCounterViewModeling
extension UserCounterViewModel: UserCounterViewModeling {
    func loadData() {
        presenter.presentUsers(users)
    }
}
