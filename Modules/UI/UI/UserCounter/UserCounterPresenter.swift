import UIKit

protocol UserCounterPresenting {
    func presentUsers(_ users: [UserCounterModel])
}

final class UserCounterPresenter {
    // MARK: - Variables
    weak var view: UserCounterDisplay?
    private let placeholder: UIImage?
    private let maxPresentedUsers = 5
    
    init(placeholder: UIImage?) {
        self.placeholder = placeholder
    }
}

// MARK: - UserCounterPresenting
extension UserCounterPresenter: UserCounterPresenting {
    func presentUsers(_ users: [UserCounterModel]) {
        handleCount(users.count)
        handleUsers(users)
    }
    
    private func handleCount(_ count: Int) {
        guard count > 1 else {
            view?.hideCount()
            return
        }
        view?.displayCount(text: "\(count)")
    }
    
    private func handleUsers(_ users: [UserCounterModel]) {
        let presentableUsers = Array(users.prefix(maxPresentedUsers))
        view?.displayUsers(presentableUsers, placeholder: placeholder)
    }
}
