public final class CharacterLimitMask: TextMask {
    public var maximumNumberOfCharacters: Int
    
    public init(maximumNumberOfCharacters: Int) {
        self.maximumNumberOfCharacters = maximumNumberOfCharacters
    }
    
    public func maskedText(from originalText: String?) -> String? {
        guard let string = originalText else {
            return originalText
        }
        return String(string.prefix(maximumNumberOfCharacters))
    }
}
