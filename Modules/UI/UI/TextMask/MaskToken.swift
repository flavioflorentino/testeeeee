import Foundation

public struct MaskToken: Hashable {
    public let character: Character
    public let characterSet: CharacterSet
    
    public init(character: Character, characterSet: CharacterSet) {
        self.character = character
        self.characterSet = characterSet
    }
    
    public static var letters = MaskToken(character: "A", characterSet: .letters)
    public static var decimalDigits = MaskToken(character: "0", characterSet: .decimalDigits)
    public static var alphanumerics = MaskToken(character: "X", characterSet: .alphanumerics)
    public static var hexadecimalDigits = MaskToken(character: "X", characterSet: CharacterSet(charactersIn: "0123456789abcdef"))
    public static var decimalDigitsWithPunctuation = MaskToken(character: "0", characterSet: CharacterSet.decimalDigits.union(CharacterSet.punctuationCharacters))
    
    public static func == (lhs: MaskToken, rhs: MaskToken) -> Bool {
        lhs.character == rhs.character
    }

    public func hash(into: inout Hasher) {
        into.combine(character)
    }
}
