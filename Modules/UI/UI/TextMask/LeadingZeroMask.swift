import Foundation

public final class LeadingZeroMask: TextMask {
    public var numberOfLeadingZeros: Int {
        didSet {
            numberFormatter.minimumIntegerDigits = numberOfLeadingZeros
        }
    }
    
    private lazy var numberFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .none
        formatter.alwaysShowsDecimalSeparator = false
        formatter.maximumFractionDigits = 0
        formatter.minimumFractionDigits = 0
        formatter.minimumIntegerDigits = numberOfLeadingZeros
        return formatter
    }()
    
    public init(numberOfLeadingZeros: Int) {
        self.numberOfLeadingZeros = numberOfLeadingZeros
    }
    
    public func maskedText(from originalText: String?) -> String? {
        let validCharacterSet = CharacterSet.decimalDigits
        guard
            let numberString = originalText?.filter({ $0.unicodeScalars.allSatisfy(validCharacterSet.contains) }),
            let number = numberFormatter.number(from: numberString),
            let numberFinal = numberFormatter.string(from: number)
            else {
                return nil
        }
        return String(numberFinal.prefix(numberOfLeadingZeros))
    }
}
