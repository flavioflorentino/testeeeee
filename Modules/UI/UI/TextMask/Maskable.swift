/// A type that supports text masking
public protocol Maskable {
    var textMask: TextMask? { get set }
}
