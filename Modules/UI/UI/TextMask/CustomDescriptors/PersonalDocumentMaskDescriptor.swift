public struct PersonalDocumentMaskDescriptor: MaskDescriptor {
    public let format: String
    public let maskTokenSet: MaskTokenSet
    
    public static let cpf = PersonalDocumentMaskDescriptor(format: "000.000.000-00", maskTokenSet: [.decimalDigits])
    public static let rg = PersonalDocumentMaskDescriptor(format: "0.000.000", maskTokenSet: [.decimalDigits])
    public static let cnpj = PersonalDocumentMaskDescriptor(format: "00.000.000/0000-00", maskTokenSet: [.decimalDigits])
    public static let sensitiveCNPJ = PersonalDocumentMaskDescriptor(format: "00.000.000/0000-00", maskTokenSet: [.decimalDigitsWithPunctuation])
    public static let date = PersonalDocumentMaskDescriptor(format: "00/00/0000", maskTokenSet: [.decimalDigits])
    public static let cellPhoneWithDDD = PersonalDocumentMaskDescriptor(format: "(00) 00000-0000", maskTokenSet: [.decimalDigits])
    public static let cellPhoneWithDDDAndDDI: PersonalDocumentMaskDescriptor = {
        let plusSignCharset = CharacterSet(charactersIn: "+").union(.decimalDigits)
        let plusSignToken = MaskToken(character: "+", characterSet: plusSignCharset)
        let format = "+\(String(repeating: "0", count: 71))"
        return PersonalDocumentMaskDescriptor(format: format, maskTokenSet: [.decimalDigits, plusSignToken])
    }()
    public static let ddd = PersonalDocumentMaskDescriptor(format: "(00)", maskTokenSet: [.decimalDigits])
    public static let cellPhone = PersonalDocumentMaskDescriptor(format: "00000-0000", maskTokenSet: [.decimalDigits])
    public static let phone = PersonalDocumentMaskDescriptor(format: "0000-0000", maskTokenSet: [.decimalDigits])
}
