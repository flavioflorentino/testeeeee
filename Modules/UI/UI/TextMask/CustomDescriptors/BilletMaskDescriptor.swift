public struct BilletMaskDescriptor: MaskDescriptor {
    public let format: String
    public let maskTokenSet: MaskTokenSet
    
    public static let dealership = BilletMaskDescriptor(format: "00000000000-0 00000000000-0 00000000000-0 00000000000-0",
                                                        maskTokenSet: [.decimalDigits])
    public static let other = BilletMaskDescriptor(format: "00000.00000 00000.000000 00000.000000 0 00000000000000",
                                                   maskTokenSet: [.decimalDigits])
}
