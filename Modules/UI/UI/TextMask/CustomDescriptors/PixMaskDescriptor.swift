import Foundation

public struct PixMaskDescriptor: MaskDescriptor {
    public let format: String
    public let maskTokenSet: MaskTokenSet
    
    public static let randomKey = PixMaskDescriptor(format: "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX", maskTokenSet: [.hexadecimalDigits])
}
