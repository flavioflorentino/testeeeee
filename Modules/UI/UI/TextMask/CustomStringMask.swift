import Foundation

public class CustomStringMask: TextMask {
    public var mask: String
    public var maskTokenSet: MaskTokenSet
    
    public init(descriptor: MaskDescriptor) {
        self.mask = descriptor.format
        self.maskTokenSet = descriptor.maskTokenSet
    }
    
    public init(mask: String, maskTokenSet: MaskTokenSet = [.letters, .decimalDigits, .alphanumerics]) {
        self.mask = mask
        self.maskTokenSet = maskTokenSet
    }
    
    public func maskedText(from originalText: String?) -> String? {
        guard let string = unmaskedText(from: originalText), !string.isEmpty else {
            return nil
        }
        var substring = Substring(string)
        var maskedString: String = ""
        for character in mask where !substring.isEmpty {
            guard let maskToken = maskTokenSet[character] else {
                maskedString.append(character)
                continue
            }
            guard let firstChar = substring.first, firstChar.unicodeScalars.allSatisfy(maskToken.characterSet.contains(_:)) else {
                return maskedString
            }
            maskedString.append(substring.removeFirst())
        }
        return maskedString
    }
    
    public func unmaskedText(from maskedText: String?) -> String? {
        maskedText?.filter({ c in c.unicodeScalars.allSatisfy(maskTokenSet.allowedCharacterSet.contains(_:)) })
    }
}
