import UIKit

/// This class is responsible for managing TextMask for a UITextField
/// ```
/// let masker = TextFieldMasker(textMask: CustomTextMask())
/// masker.bind(to: textField)
/// ```
/// - Important: The masker does not retain the `TextField`
public final class TextFieldMasker: Maskable {
    public var textMask: TextMask?
    
    public init(textMask: TextMask?) {
        self.textMask = textMask
    }
    
    public func bind(to textField: UITextField) {
        textField.addTarget(self, action: #selector(textFieldEditingChanged(_:)), for: .editingChanged)
    }
    
    public func unbind(from textField: UITextField) {
        textField.removeTarget(self, action: #selector(textFieldEditingChanged(_:)), for: .editingChanged)
    }
    
    @objc
    private func textFieldEditingChanged(_ textField: UITextField) {
        guard let textMask = textMask else {
            return
        }
        textField.text = textMask.maskedText(from: textField.text)
    }
}
