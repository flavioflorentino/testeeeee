/// A type that's able to mask a text string
public protocol TextMask {
    /// Returns a masked text from the original text
    /// - Parameter originalText: The text to be masked
    func maskedText(from originalText: String?) -> String?
}
