import Foundation

public class BankAccountMask: TextMask {
    private let minCount = 2
    private let maxCount = 14
    
    public init() {}
    
    public func maskedText(from originalText: String?) -> String? {
        let validCharacterSet = CharacterSet.decimalDigits
        guard let numberSubString = originalText?.filter({ $0.unicodeScalars.allSatisfy(validCharacterSet.contains) }).prefix(maxCount) else {
            return nil
        }
        var numberString = String(numberSubString)
        guard numberString.count >= minCount else {
            return numberString
        }
        numberString.insert("-", at: numberString.index(before: numberString.endIndex))
        return numberString
    }
}
