import Foundation

/// This class is responsible for add mask to obfuscate the information
public enum StringObfuscationMasker {
    /// This method apply an obfuscation mask in the word with the symbol that you want
    /// Returns a masked text from the original text
    /// - Parameter word: The word that you want to apply the obfuscation mask
    /// - Parameter prefix: Total of the initial characters that shouldn't be changed
    /// - Parameter suffix: Total of the final characters that shouldn't be changed
    /// - Parameter symbol: The character that you want to put instead of the original letters of the word, by default is *
    public static func mask(word: String, prefix: Int, suffix: Int, symbol: String = "*") -> String {
        let count = word.count - (prefix + suffix)
        guard (prefix + suffix) < word.count else {
            return word
        }
        let mask = String(repeating: symbol, count: count)
        return "\(word.prefix(prefix))\(mask)\(word.suffix(suffix))"
    }
    
    public static func mask(email: String?) -> String? {
        guard
            let rawEmail = email,
            let atIndex = rawEmail.firstIndex(of: "@")
            else {
                return nil
        }
        let rawEmailName = String(rawEmail.prefix(upTo: atIndex))
        
        var rawDomain = String(rawEmail.suffix(from: atIndex))
        rawDomain.removeFirst(1)
        
        guard let dotIndex = rawDomain.firstIndex(of: ".") else {
            return nil
        }
        
        let rawDomainName = String(rawDomain.prefix(upTo: dotIndex))
        let emailName = mask(word: rawEmailName, prefix: 1, suffix: 1)
        let domainName = mask(word: rawDomainName, prefix: 1, suffix: 1)
        let domainType = rawDomain[dotIndex...]
        return "\(emailName)@\(domainName)\(domainType)"
    }
    
    public static func mask(phone: String?) -> String? {
        guard let rawPhone = phone else {
            return nil
        }
        
        return StringObfuscationMasker.mask(word: rawPhone, prefix: 0, suffix: 4).cellphoneFormatting()
    }

    public static func mask(cnpj: String?) -> String? {
        guard let rawCNPJ = cnpj else {
            return nil
        }

        let document = StringObfuscationMasker.mask(word: rawCNPJ, prefix: 2, suffix: 6)
        return CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.sensitiveCNPJ).maskedText(from: document)
    }
}
