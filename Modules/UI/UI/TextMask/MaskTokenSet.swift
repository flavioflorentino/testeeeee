import Foundation

public struct MaskTokenSet: ExpressibleByArrayLiteral {
    public typealias ArrayLiteralElement = MaskToken
    
    public let values: Set<MaskToken>
    public let maskTokenSetTable: [Character: MaskToken]
    public let allowedCharacterSet: CharacterSet
    
    public init(values: Set<MaskToken>) {
        self.values = values
        maskTokenSetTable = values.reduce(into: [Character: MaskToken](), { result, maskToken in
            result[maskToken.character] = maskToken
        })
        allowedCharacterSet = values.reduce(into: CharacterSet(), { result, maskToken in
            result.formUnion(maskToken.characterSet)
        })
    }
    
    public init(arrayLiteral elements: MaskToken...) {
        self.init(values: Set<MaskToken>(elements))
    }
    
    public subscript(token: Character) -> MaskToken? {
        maskTokenSetTable[token]
    }
}
