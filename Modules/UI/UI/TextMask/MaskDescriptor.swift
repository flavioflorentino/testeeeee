public protocol MaskDescriptor {
    var format: String { get }
    var maskTokenSet: MaskTokenSet { get }
}
