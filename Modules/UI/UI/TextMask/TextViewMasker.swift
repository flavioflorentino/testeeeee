import UIKit

public final class TextViewMasker: Maskable {
    public var textMask: TextMask?
    
    var notificationCenter: NotificationCenter {
        .default
    }
    
    public init(textMask: TextMask?) {
        self.textMask = textMask
    }
    
    public func bind(to textView: UITextView) {
        notificationCenter.addObserver(
            self,
            selector: #selector(textViewEditingChanged(_:)),
            name: UITextView.textDidChangeNotification,
            object: textView
        )
    }
    
    public func unbind(from textView: UITextView) {
        notificationCenter.removeObserver(self, name: UITextView.textDidChangeNotification, object: textView)
    }
    
    @objc
    private func textViewEditingChanged(_ notification: Notification) {
        guard
            let textView = notification.object as? UITextView,
            let textMask = textMask
            else {
                return
        }
        textView.text = textMask.maskedText(from: textView.text)
    }
}
