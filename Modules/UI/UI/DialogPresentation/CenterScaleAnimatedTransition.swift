import UIKit

final class CenterScaleAnimatedTransition: NSObject, UIViewControllerAnimatedTransitioning {
    private var isPresentation: Bool
    
    init(isPresentation: Bool) {
        self.isPresentation = isPresentation
        super.init()
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        0.5
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let viewKey: UITransitionContextViewKey = isPresentation ? .to : .from
        let controllerKey: UITransitionContextViewControllerKey = isPresentation ? .to : .from
        let containerView = transitionContext.containerView
        
        guard
            let view = transitionContext.view(forKey: viewKey),
            let controller = transitionContext.viewController(forKey: controllerKey),
            let snapshot = view.snapshotView(afterScreenUpdates: true)
            else {
                return
        }
        
        let smallScale = CGAffineTransform(scaleX: 0.3, y: 0.3)
        let originalScale = CGAffineTransform(scaleX: 1.0, y: 1.0)
        let finalFrame = isPresentation ? transitionContext.finalFrame(for: controller) : transitionContext.initialFrame(for: controller)
        
        containerView.addSubview(snapshot)
        snapshot.transform = isPresentation ? smallScale : originalScale
        snapshot.center = CGPoint(x: finalFrame.midX, y: finalFrame.midY)
        snapshot.alpha = isPresentation ? 0 : 1
        view.alpha = 0
        let duration = transitionDuration(using: transitionContext)
        
        let animator = UIViewPropertyAnimator(duration: duration, dampingRatio: 0.6) {
            snapshot.transform = self.isPresentation ? originalScale : smallScale
            snapshot.alpha = self.isPresentation ? 1 : 0
        }
        animator.addCompletion { animationPosition in
            view.alpha = 1
            snapshot.removeFromSuperview()
            if self.isPresentation {
                containerView.addSubview(view)
            }
            transitionContext.completeTransition(animationPosition == .end)
        }
        
        animator.startAnimation()
    }
}
