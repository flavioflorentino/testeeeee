import UIKit

final class DialogPresentationController: UIPresentationController {
    private lazy var dimmingView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        view.alpha = 0
        return view
    }()
    
    override func presentationTransitionWillBegin() {
        containerView?.insertSubview(dimmingView, at: 0)
        
        dimmingView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        presentedView?.cornerRadius = .strong
        presentedViewController.transitionCoordinator?.animate(alongsideTransition: { _ in
            self.dimmingView.alpha = 1.0
        })
    }
    
    override func dismissalTransitionWillBegin() {
        presentedViewController.transitionCoordinator?.animate(alongsideTransition: { _ in
            self.dimmingView.alpha = 0.0
        })
    }
    
    override func containerViewWillLayoutSubviews() {
        presentedView?.frame = frameOfPresentedViewInContainerView
    }
    
    override func size(forChildContentContainer container: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {
        container.preferredContentSize
    }
    
    override var shouldPresentInFullscreen: Bool {
        false
    }
    
    override var frameOfPresentedViewInContainerView: CGRect {
        let containerViewSize = containerView?.frame.size ?? .zero
        let preferredContentSize = size(
            forChildContentContainer: presentedViewController,
            withParentContainerSize: containerViewSize
        )
        let originX = (containerViewSize.width - preferredContentSize.width) / 2
        let originY = (containerViewSize.height - preferredContentSize.height) / 2
        let origin = CGPoint(x: originX, y: originY)
        return CGRect(origin: origin, size: preferredContentSize)
    }
    
    override func preferredContentSizeDidChange(forChildContentContainer container: UIContentContainer) {
        presentedView?.frame = frameOfPresentedViewInContainerView
    }
}
