import UIKit

public final class LayoutProxy {
    // MARK: - Properties
    private let view: UIView

    public lazy var leading: LayoutProperty = property(with: view.leadingAnchor)
    public lazy var trailing = property(with: view.trailingAnchor)
    public lazy var top = property(with: view.topAnchor)
    public lazy var bottom = property(with: view.bottomAnchor)

    public lazy var width = property(with: view.widthAnchor)
    public lazy var height = property(with: view.heightAnchor)

    public lazy var centerX = property(with: view.centerXAnchor)
    public lazy var centerY = property(with: view.centerYAnchor)

    public lazy var firstBaseline = property(with: view.firstBaselineAnchor)
    public lazy var lastBaseline = property(with: view.lastBaselineAnchor)

    // MARK: - Initialization
    init(view: UIView) {
        self.view = view
    }
}

private extension LayoutProxy {
    func property<Anchor: LayoutAnchor>(with anchor: Anchor) -> LayoutProperty<Anchor> {
        LayoutProperty(anchor: anchor)
    }

    func property<Anchor: LayoutDimension>(witch anchor: Anchor) -> LayoutProperty<Anchor> {
        LayoutProperty(anchor: anchor)
    }
}
