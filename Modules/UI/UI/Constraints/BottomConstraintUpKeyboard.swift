import UIKit

public class BottomConstraintUpKeyboard: NSLayoutConstraint {
    public convenience init(item: UIView, toItem: UIView, respectSafeArea: Bool = true) {
        self.init(item: item, attribute: .bottom, relatedBy: .equal, toItem: toItem, attribute: .bottom, multiplier: 1, constant: 0)
        startListen()
    }
    public func startListen() {
        NotificationCenter.default
            .addObserver(self,
                         selector: #selector(keyboardWillShow(notification:)),
                         name: UIResponder.keyboardWillShowNotification,
                         object: nil)
        
        NotificationCenter.default
            .addObserver(self,
                         selector: #selector(keyboardWillHide(notification:)),
                         name: UIResponder.keyboardWillHideNotification,
                         object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

@objc
extension BottomConstraintUpKeyboard {
    private func keyboardWillShow(notification: Notification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        
        var bottomPadding: CGFloat = 0
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            bottomPadding = window?.safeAreaInsets.bottom ?? 0
        }
        self.constant = -(keyboardSize.height - bottomPadding)
        animateTransition(notification)
    }
    
    private func keyboardWillHide(notification: Notification) {
        self.constant = 0
        animateTransition(notification)
    }
    
    private func animateTransition(_ notification: Notification) {
        guard let view = self.firstItem as? UIView,
              let keyboardAnimationDuration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double else {
            return
        }
        
        UIView.animate(withDuration: keyboardAnimationDuration) {
            view.superview?.layoutIfNeeded()
        }
    }
}
