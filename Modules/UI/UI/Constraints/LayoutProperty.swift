import UIKit

public struct LayoutProperty<Anchor: LayoutAnchor> {
    let anchor: Anchor
}

public extension LayoutProperty {
    @discardableResult
    func equal(to otherAnchor: Anchor, offsetBy constant: CGFloat = 0) -> NSLayoutConstraint {
        let constraint = anchor.constraint(equalTo: otherAnchor, constant: constant)
        constraint.isActive = true

        return constraint
    }

    @discardableResult
    func greaterThanOrEqual(to otherAnchor: Anchor, offsetBy constant: CGFloat = 0) -> NSLayoutConstraint {
        let constraint = anchor.constraint(greaterThanOrEqualTo: otherAnchor, constant: constant)
        constraint.isActive = true

        return constraint
    }

    @discardableResult
    func lessThanOrEqual(to otherAnchor: Anchor, offsetBy constant: CGFloat = 0) -> NSLayoutConstraint {
        let constraint = anchor.constraint(lessThanOrEqualTo: otherAnchor, constant: constant)
        constraint.isActive = true

        return constraint
    }
}

public extension LayoutProperty where Anchor: LayoutDimension {
    @discardableResult
    func equal(to constant: CGFloat) -> NSLayoutConstraint {
        let constraint = anchor.constraint(equalToConstant: constant)
        constraint.isActive = true

        return constraint
    }
    
    @discardableResult
    func equal(to dimension: Anchor, multiplier: CGFloat) -> NSLayoutConstraint {
      let constraint = anchor.constraint(equalTo: dimension, multiplier: multiplier)
      constraint.isActive = true
      return constraint
    }

    @discardableResult
    func greaterThanOrEqual(to constant: CGFloat) -> NSLayoutConstraint {
        let constraint = anchor.constraint(greaterThanOrEqualToConstant: constant)
        constraint.isActive = true

        return constraint
    }

    @discardableResult
    func lessThanOrEqual(to constant: CGFloat) -> NSLayoutConstraint {
        let constraint = anchor.constraint(lessThanOrEqualToConstant: constant)
        constraint.isActive = true

        return constraint
    }
}

// swiftlint:disable static_operator
// MARK: - Operators
@discardableResult
public func == <Anchor: LayoutAnchor>(lhs: LayoutProperty<Anchor>, rhs: (anchor: Anchor, offset: CGFloat)) -> NSLayoutConstraint {
    lhs.equal(to: rhs.anchor, offsetBy: rhs.offset)
}

@discardableResult
public func == <Anchor: LayoutDimension>(lhs: LayoutProperty<Anchor>, rhs: (anchor: Anchor, multiplier: CGFloat)) -> NSLayoutConstraint {
    lhs.equal(to: rhs.anchor, multiplier: rhs.multiplier)
}

@discardableResult
public func == <Anchor: LayoutAnchor>(lhs: LayoutProperty<Anchor>, rhs: Anchor) -> NSLayoutConstraint {
    lhs.equal(to: rhs)
}

@discardableResult
public func >= <Anchor: LayoutAnchor>(lhs: LayoutProperty<Anchor>, rhs: (anchor: Anchor, offset: CGFloat)) -> NSLayoutConstraint {
    lhs.greaterThanOrEqual(to: rhs.anchor, offsetBy: rhs.offset)
}

@discardableResult
public func >= <Anchor: LayoutAnchor>(lhs: LayoutProperty<Anchor>, rhs: Anchor) -> NSLayoutConstraint {
    lhs.greaterThanOrEqual(to: rhs)
}

@discardableResult
public func <= <Anchor: LayoutAnchor>(lhs: LayoutProperty<Anchor>, rhs: (anchor: Anchor, offset: CGFloat)) -> NSLayoutConstraint {
    lhs.lessThanOrEqual(to: rhs.anchor, offsetBy: rhs.offset)
}

@discardableResult
public func <= <Anchor: LayoutAnchor>(lhs: LayoutProperty<Anchor>, rhs: Anchor) -> NSLayoutConstraint {
    lhs.lessThanOrEqual(to: rhs)
}

@discardableResult
public func == <Anchor: LayoutDimension>(lhs: LayoutProperty<Anchor>, rhs: CGFloat) -> NSLayoutConstraint {
    lhs.equal(to: rhs)
}

@discardableResult
public func <= <Anchor: LayoutDimension>(lhs: LayoutProperty<Anchor>, rhs: CGFloat) -> NSLayoutConstraint {
    lhs.lessThanOrEqual(to: rhs)
}

@discardableResult
public func >= <Anchor: LayoutDimension>(lhs: LayoutProperty<Anchor>, rhs: CGFloat) -> NSLayoutConstraint {
    lhs.greaterThanOrEqual(to: rhs)
}
// swiftlint:enable static_operator
