import UIKit

public extension UIView {
    func layout(using closure: (LayoutProxy) -> Void) {
        if translatesAutoresizingMaskIntoConstraints {
            translatesAutoresizingMaskIntoConstraints = false
        }

        closure(LayoutProxy(view: self))
    }
}
