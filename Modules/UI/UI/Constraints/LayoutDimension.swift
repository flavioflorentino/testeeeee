import UIKit

public protocol LayoutDimension: LayoutAnchor {
    func constraint(equalToConstant constant: CGFloat) -> NSLayoutConstraint
    func constraint(greaterThanOrEqualToConstant constant: CGFloat) -> NSLayoutConstraint
    func constraint(lessThanOrEqualToConstant constant: CGFloat) -> NSLayoutConstraint
    func constraint(equalTo anchor: Self, multiplier m: CGFloat) -> NSLayoutConstraint
}

extension NSLayoutDimension: LayoutDimension {
}

// swiftlint:disable static_operator
public func * <Anchor: LayoutDimension>(lhs: Anchor, rhs: CGFloat) -> (Anchor, CGFloat) {
  (lhs, rhs)
}
// swiftlint:enable static_operator
