import Foundation
import UIKit

public enum ShadowOffset {
    case none
    case light
    case medium
    
    public var rawValue: CGSize {
        switch self {
        case .none:
            return .zero
        case .light:
            return CGSize(width: 0, height: 1)
        case .medium:
            return CGSize(width: 0, height: 2)
        }
    }
}

public enum ShadowRadius: CGFloat {
    case none = 0.0
    case light = 1.0
    case medium = 2.0
    case strong = 5.0
}

public extension CALayer {
    @available(*, deprecated, message: "Use setShadow(radius:_, offSet:_, color:_, opacity:_)")
    func setShadow(radius: ShadowRadius, offset: ShadowOffset, color: Palette, opacity: Float) {
        shadowOffset = offset.rawValue
        shadowRadius = radius.rawValue
        shadowColor = color.cgColor
        shadowOpacity = opacity
        shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
    }
}
