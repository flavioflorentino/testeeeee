import SkyFloatingLabelTextField
import UIKit

// MARK: - Line Colors
public struct LineColors {
    let validLineColor: UIColor
    let invalidLineColor: UIColor
    let blankLineColor: UIColor
    
    public init(validLineColor: UIColor = Colors.branding400.color,
                invalidLineColor: UIColor = Colors.critical600.color,
                nilLineColor: UIColor = Colors.grayscale200.color) {
        self.validLineColor = validLineColor
        self.invalidLineColor = invalidLineColor
        self.blankLineColor = nilLineColor
    }
    
    func color(for state: ValidationTextFieldState) -> UIColor {
        switch state {
        case .blank:
            return blankLineColor
        case .valid:
            return validLineColor
        case .invalid:
            return invalidLineColor
        }
    }
}

// MARK: - Layout
private extension ValidationTextField.Layout {
    enum Size {
        static let underlineErrorMessageLabelRatio = 0.8
        static let exceededCharactersCountLabelRatio = 0.2
    }
    
    enum Spacing {
        static let topSpacing = 2
    }
}

// MARK: - ValidationTextFieldState
public enum ValidationTextFieldState {
    case valid
    case invalid
    case blank
}

public final class ValidationTextField: SkyFloatingLabelTextField, MaskTextField {
    // MARK: - Public Properties
    
    /// Determines if the text field has valid fields
    /// There are 3 possible values for this property: blank, valid, invalid. Each one has its own dependant text line color set on the LineColors struct
    public var textFieldState: ValidationTextFieldState {
        didSet {
            lineColor = lineColors.color(for: textFieldState)
            selectedLineColor = lineColor
        }
    }
    
    /// Shows a error label below the text field line if set to a value
    /// Set this to nil or empty string in order to hide the error label
    public var underlineErrorMessage: String? {
        didSet {
            configureUnderlineErrorMessage(with: underlineErrorMessage)
        }
    }
    
    /// Overrides the SkyFloatingLabelTextField error message that shows the label on top to display the custom one below the text field
    override public var errorMessage: String? {
        didSet {
            underlineErrorMessage = errorMessage
        }
    }
    
    /// Shows a loading indicator in the right icon view
    public var isLoading: Bool {
        didSet {
            handle(isLoading: isLoading)
        }
    }
    
    /// Shows a UIView on the right side of the text field
    public var rightIconView: UIView? {
        didSet {
            guard let rightIconView = rightIconView else {
                rightView = nil
                rightViewMode = .never
                return
            }
            rightViewMode = .always
            rightView = rightIconView
            guard !(rightIconView is UIActivityIndicatorView) else { return }
            formerRightIconView = rightIconView
        }
    }
    
    /// Determines the colors for each possible state of the textFieldState property
    public let lineColors: LineColors
    
    /// This property determines if the character count limit label should display when the user types more than the maxlength allowed
    public let hasCharacterLimitBehavior: Bool
    
    // MARK: - MaskTextField Conformance
    public var maskString: String?
    public var maskblock: ((_ oldText: String, _ newText: String) -> String)?
    public var maxlength: Int?
    
    // MARK: - Private Properties
    fileprivate enum Layout { }
    private var underlineErrorMessageLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.caption().font()
        label.textColor = Colors.critical600.color
        label.numberOfLines = 1
        label.textAlignment = .left
        return label
    }()
    
    private var exceededCharactersCountLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.caption().font()
        label.textColor = Colors.critical600.color
        label.numberOfLines = 1
        label.textAlignment = .right
        return label
    }()
    
    /// Stores the right icon view that was set programatically to restore after loading is finished
    private var formerRightIconView: UIView?
    
    // MARK: - Init
    public init(placeholder: String,
                maxLength: Int,
                lineColors: LineColors = LineColors(),
                placeholderColor: UIColor = Colors.grayscale300.color,
                maskString: String? = nil,
                hasCharacterLimitBehavior: Bool = false
    ) {
        self.lineColors = lineColors
        self.isLoading = false
        self.hasCharacterLimitBehavior = hasCharacterLimitBehavior
        self.textFieldState = .blank
        super.init(frame: .zero)
        self.placeholder = placeholder
        self.placeholderColor = placeholderColor
        self.maxlength = maxLength
        self.maskString = maskString
        lineColor = lineColors.blankLineColor
        selectedLineColor = lineColors.blankLineColor
        selectedTitleColor = placeholderColor
        titleColor = placeholderColor
        placeholderFont = Typography.bodySecondary().font()
        titleFont = Typography.caption().font()
        underlineErrorMessageLabel.textColor = lineColors.invalidLineColor
        textErrorColor = Colors.black.color
        font = Typography.bodyPrimary().font()
        tintColor = Colors.black.color
        
        buildLayout()
        
        guard hasCharacterLimitBehavior else { return }
        addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private Functions
    private func configureUnderlineErrorMessage(with value: String?) {
        if let value = value, !value.isEmpty {
            underlineErrorMessageLabel.isHidden = false
            underlineErrorMessageLabel.text = value
        } else {
            underlineErrorMessageLabel.isHidden = true
            underlineErrorMessageLabel.text = ""
        }
    }
    
    private func handle(isLoading: Bool) {
        if isLoading {
            let activityIndicator = UIActivityIndicatorView(style: .gray)
            rightIconView = activityIndicator
            isEnabled = false
            activityIndicator.startAnimating()
        } else {
            rightIconView = formerRightIconView
            isEnabled = true
        }
    }
    
    @objc
    private func textFieldDidChange(_ sender: UITextField) {
        guard let maxLenght = maxlength, let text = sender.text else { return }
        if text.count > maxLenght {
            underlineErrorMessage = "Limite de caracteres atingido."
            underlineErrorMessageLabel.isHidden = false
            exceededCharactersCountLabel.text = "\(maxLenght - text.count)"
            exceededCharactersCountLabel.isHidden = false
            textFieldState = .invalid
        } else {
            underlineErrorMessageLabel.isHidden = false
            exceededCharactersCountLabel.isHidden = false
            underlineErrorMessage = ""
            exceededCharactersCountLabel.text = ""
            textFieldState = text.isEmpty ? .blank : .valid
        }
    }
}

// MARK: ViewConfiguration
extension ValidationTextField: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(underlineErrorMessageLabel)
        addSubview(exceededCharactersCountLabel)
    }

    public func setupConstraints() {
        underlineErrorMessageLabel.snp.makeConstraints {
            $0.leading.equalToSuperview()
            $0.top.equalTo(self.snp.bottom).offset(Spacing.base00)
            $0.width.equalToSuperview().multipliedBy(Layout.Size.underlineErrorMessageLabelRatio)
        }
        
        exceededCharactersCountLabel.snp.makeConstraints {
            $0.trailing.equalToSuperview()
            $0.top.equalTo(self.snp.bottom).offset(Layout.Spacing.topSpacing)
            $0.width.equalToSuperview().multipliedBy(Layout.Size.exceededCharactersCountLabelRatio)
        }
    }
}
