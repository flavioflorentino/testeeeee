import SnapKit
import UIKit

public protocol StarsRatingViewDelegate: AnyObject {
    func didTapStar(rating: Int)
}

public final class StarsRatingView: UIView, ViewConfiguration {
    private let starsNumber = 5
    
    private lazy var starsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .fill
        stackView.distribution = .equalSpacing
        stackView.axis = .horizontal
        stackView.spacing = Spacing.base02
        stackView.isUserInteractionEnabled = true
        stackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(markStars(_:))))
        stackView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(markStars(_:))))
        return stackView
    }()
    
    public weak var delegate: StarsRatingViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func buildViewHierarchy() {
        addSubview(starsStackView)
        
        for _ in 1...starsNumber {
            let starView = StarView()
            starsStackView.addArrangedSubview(starView)
        }
    }
    
    public func setupConstraints() {
        starsStackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}

private extension StarsRatingView {
    func turnOnStarsTo(rating: Int) {
        guard let starViews = starsStackView.arrangedSubviews as? [StarView] else {
            return
        }
        for i in 0..<rating {
            starViews[i].isOn = true
        }
        for i in rating..<starsNumber {
            starViews[i].isOn = false
        }
    }
    
    func ratingForPosition(_ position: CGPoint) -> Int {
        starsStackView.arrangedSubviews.filter { position.x > $0.frame.minX }.count
    }
    
    @objc
    func markStars(_ gesture: UIGestureRecognizer) {
        let touchPoint = gesture.location(in: starsStackView)
        let rating = ratingForPosition(touchPoint)
        turnOnStarsTo(rating: rating)
        delegate?.didTapStar(rating: rating)
    }
}
