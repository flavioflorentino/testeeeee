import SnapKit
import UIKit

final class StarView: UIImageView {
    var isOn: Bool = false {
        didSet {
            image = isOn ? Assets.filledStart.image : Assets.emptyStar.image
        }
    }
    
    init() {
        super.init(image: Assets.emptyStar.image)
        contentMode = .center
        setupConstraints()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupConstraints() {
        self.snp.makeConstraints {
            $0.height.width.equalTo(Spacing.base04)
        }
    }
}
