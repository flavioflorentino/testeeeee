import UIKit

@objc
public protocol StatusAlertViewDelegate: AnyObject {
    @objc
    optional func didTouchOnLink(URL: URL)
    @objc
    optional func didTouchOnButton()
}

public struct StatusAlertData {
    let icon: UIImage?
    let title: String
    let text: String
    let buttonTitle: String
    let isCloseButtonHidden: Bool
    
    public init(icon: UIImage?, title: String, text: String, buttonTitle: String, isCloseButtonHidden: Bool = true) {
        self.icon = icon
        self.title = title
        self.text = text
        self.buttonTitle = buttonTitle
        self.isCloseButtonHidden = isCloseButtonHidden
    }
}

public struct StatusAlertAttributedData {
    let icon: UIImage?
    let title: String
    let text: NSAttributedString
    let buttonTitle: String
    let isCloseButtonHidden: Bool
    
    public init(icon: UIImage?, title: String, text: NSAttributedString, buttonTitle: String, isCloseButtonHidden: Bool = true) {
        self.icon = icon
        self.title = title
        self.text = text
        self.buttonTitle = buttonTitle
        self.isCloseButtonHidden = isCloseButtonHidden
    }
}

public class StatusAlertView: UIView {
    // MARK: - Layout
    private enum Layout {
        static let topMargin: CGFloat = 25
        static let leadingMargin: CGFloat = 25
        static let textMargin: CGFloat = 20
        
        static let spacingIcon: CGFloat = 20
        static let spacingTitle: CGFloat = 5
        static let spacingText: CGFloat = 30
        
        static let numberOfLines: Int = 0
        
        static let fontTitle: CGFloat = 28
        static let fontDescription: CGFloat = 14
        
        static let closeButtonSide: CGFloat = 30.0
    }
    
    // MARK: - Visual Components
    
    private lazy var closeButton: UIButton = {
        let button = UIButton()
        button.setImage(Assets.icoCloseBigGreen.image, for: .normal)
        button.tintColor = Palette.ppColorBranding300.color
        button.addTarget(self, action: #selector(didTouchOnCloseButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var stackViewContent: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.alignment = .center
        return stackView
    }()
    
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.numberOfLines = Layout.numberOfLines
        label.font = UIFont.systemFont(ofSize: Layout.fontTitle)
        label.textColor = Palette.ppColorGrayscale500.color
        label.setContentCompressionResistancePriority(.required, for: .vertical)
        return label
    }()
    
    private lazy var descriptionTextView: UITextView = {
        let textView = UITextView()
        textView.backgroundColor = .clear
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isEditable = false
        textView.isScrollEnabled = false
        textView.delegate = self
        textView.linkTextAttributes = [
            .foregroundColor: Palette.ppColorNeutral300.color,
            .font: UIFont.systemFont(ofSize: Layout.fontDescription),
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        textView.textAlignment = .center
        textView.font = UIFont.systemFont(ofSize: Layout.fontDescription)
        textView.textColor = Palette.ppColorGrayscale500.color
        return textView
    }()
    
    private lazy var brandingButton: BrandingButton = {
        let button = BrandingButton()
        button.addTarget(self, action: #selector(touchUpInsideBrandingButton), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Variables
    public weak var statusAlertDelegate: StatusAlertViewDelegate?
    
    // MARK: - Life Cycle
    private init() {
        super.init(frame: .zero)
        autoresizingMask = [.flexibleWidth, .flexibleHeight]
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Presentation
    @discardableResult
    public static func show(on view: UIView, data: StatusAlertData, isAnimated: Bool = false) -> StatusAlertView {
        let alert = createAlert(
            on: view,
            icon: data.icon,
            title: data.title,
            buttonTitle: data.buttonTitle,
            isCloseButtonHidden: data.isCloseButtonHidden
        )
        alert.setText(data.text)
        view.addSubview(alert)
        alert.present(isAnimated: isAnimated)
        return alert
    }
    
    @discardableResult
    public static func show(on view: UIView, data: StatusAlertAttributedData, isAnimated: Bool = false) -> StatusAlertView {
        let alert = createAlert(
            on: view,
            icon: data.icon,
            title: data.title,
            buttonTitle: data.buttonTitle,
            isCloseButtonHidden: data.isCloseButtonHidden
        )
        alert.setAttributedText(data.text)
        view.addSubview(alert)
        alert.present(isAnimated: isAnimated)
        return alert
    }
    
    private func present(isAnimated: Bool) {
        alpha = isAnimated ? 0 : 1
        if isAnimated {
            UIView.animate(withDuration: 0.25) {
                self.alpha = 1
            }
        }
    }
    
    private func hide(isAnimated: Bool) {
        if isAnimated {
            let animation = {
                self.alpha = 0
            }
            let completion: (Bool) -> Void = { _ in
                self.removeFromSuperview()
            }
            UIView.animate(
                withDuration: 0.25,
                animations: animation,
                completion: completion
            )
        } else {
            self.removeFromSuperview()
        }
    }
    
    private static func createAlert(
        on view: UIView,
        icon: UIImage?,
        title: String,
        buttonTitle: String,
        isCloseButtonHidden: Bool
    ) -> StatusAlertView {
        let alert = StatusAlertView()
        alert.frame = CGRect(origin: .zero, size: view.frame.size)
        alert.setIcon(icon)
        alert.setTitle(title)
        alert.setButtonTitle(buttonTitle)
        alert.closeButton.isHidden = isCloseButtonHidden
        return alert
    }
    
    // MARK: - Content
    private func setIcon(_ icon: UIImage?) {
        iconImageView.image = icon
    }
    
    private func setTitle(_ title: String) {
        titleLabel.text = title
    }
    
    private func setText(_ text: String) {
        descriptionTextView.text = text
        let height = descriptionTextView.sizeThatFits(
            CGSize(width: bounds.width - 2 * Layout.leadingMargin,
                   height: .greatestFiniteMagnitude)
        ).height
        NSLayoutConstraint.activate([
            descriptionTextView.heightAnchor.constraint(equalToConstant: height)
        ])
    }
    
    private func setAttributedText(_ text: NSAttributedString) {
        descriptionTextView.attributedText = text
        let height = descriptionTextView.sizeThatFits(
            CGSize(width: bounds.width - 2 * Layout.leadingMargin,
                   height: .greatestFiniteMagnitude)
        ).height
        NSLayoutConstraint.activate([
            descriptionTextView.heightAnchor.constraint(equalToConstant: height)
        ])
    }
    
    private func setButtonTitle(_ title: String) {
        brandingButton.setTitle(title, for: .normal)
    }
    
    // MARK: - Action
    @objc
    private func touchUpInsideBrandingButton() {
        statusAlertDelegate?.didTouchOnButton?()
        hide(isAnimated: true)
    }
    
    @objc
    private func didTouchOnCloseButton() {
        hide(isAnimated: true)
    }
}

// MARK: - ViewConfiguration
extension StatusAlertView: ViewConfiguration {
    public func configureViews() {
        backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    public func setupConstraints() {
        closeButton.layout {
            $0.top == compatibleSafeAreaLayoutGuide.topAnchor + Spacing.base02
            $0.leading == leadingAnchor + Spacing.base02
            $0.width == Layout.closeButtonSide
            $0.height == Layout.closeButtonSide
        }
        
        NSLayoutConstraint.activate([
            stackViewContent.centerXAnchor.constraint(equalTo: centerXAnchor),
            stackViewContent.centerYAnchor.constraint(equalTo: centerYAnchor),
            stackViewContent.topAnchor.constraint(greaterThanOrEqualTo: compatibleSafeAreaLayoutGuide.topAnchor, constant: Layout.topMargin),
            stackViewContent.leadingAnchor.constraint(equalTo: compatibleSafeAreaLayoutGuide.leadingAnchor, constant: Layout.leadingMargin)
        ])
        
        NSLayoutConstraint.activate([
            brandingButton.leadingAnchor.constraint(equalTo: stackViewContent.leadingAnchor),
            brandingButton.trailingAnchor.constraint(equalTo: stackViewContent.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            descriptionTextView.leadingAnchor.constraint(equalTo: stackViewContent.leadingAnchor),
            descriptionTextView.trailingAnchor.constraint(equalTo: stackViewContent.trailingAnchor)
        ])
    }
    
    public func buildViewHierarchy() {
        addSubview(stackViewContent)
        addSubview(closeButton)
        stackViewContent.addArrangedSubview(iconImageView)
        stackViewContent.addArrangedSubview(titleLabel)
        stackViewContent.addArrangedSubview(descriptionTextView)
        stackViewContent.addArrangedSubview(brandingButton)
        stackViewContent.setSpacing(Layout.spacingIcon, after: iconImageView)
        stackViewContent.setSpacing(Layout.spacingTitle, after: titleLabel)
        stackViewContent.setSpacing(Layout.spacingText, after: descriptionTextView)
    }
}

// MARK: - UITextViewDelegate
extension StatusAlertView: UITextViewDelegate {
    public func textView(
        _ textView: UITextView,
        shouldInteractWith URL: URL,
        in characterRange: NSRange,
        interaction: UITextItemInteraction
    ) -> Bool {
        statusAlertDelegate?.didTouchOnLink?(URL: URL)
        return false
    }
}
