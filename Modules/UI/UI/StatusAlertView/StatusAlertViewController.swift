import UIKit

public final class StatusAlertViewController: UIViewController {
    private let data: StatusAlertData
    
    public var didTouchOnLinkClosure: ((URL) -> Void)?
    public var didTouchOnButtonClosure: (() -> Void)?
    
    public init(data: StatusAlertData) {
        self.data = data
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        let alert = StatusAlertView.show(on: view, data: data)
        alert.statusAlertDelegate = self
    }
}

extension StatusAlertViewController: StatusAlertViewDelegate {
    public func didTouchOnLink(URL: URL) {
        dismiss(animated: true) {
            self.didTouchOnLinkClosure?(URL)
        }
    }
    
    public func didTouchOnButton() {
        dismiss(animated: true) {
            self.didTouchOnButtonClosure?()
        }
    }
}
