import UIKit

public final class BrandingButton: UIButton {
    // MARK: - Layout
    fileprivate enum Layout { }
    // MARK: - Life Cycle
    public init() {
        super.init(frame: .zero)
        translatesAutoresizingMaskIntoConstraints = false
        prepareLayout()
        setupConstraints()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func prepareLayout() {
        layer.cornerRadius = Layout.radius
        backgroundColor = Palette.ppColorBranding300.color
        setTitleColor(Palette.white.color, for: .normal)
        titleLabel?.font = UIFont.systemFont(ofSize: Layout.fontTitle)
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            heightAnchor.constraint(equalToConstant: Layout.height)
        ])
    }
}

private extension BrandingButton.Layout {
    static let height: CGFloat = 48
    static let radius: CGFloat = 24
    
    static let fontTitle: CGFloat = 16
}
