# Status Alert View

It's a simple status alert that shows an image, a title, a description text and a branding button. It has a delegate that can detect touches on its button or in links on its description text.

### Feature

- [X] Generic Alert View
- [X] Accepts and detects links on text

### Usage

To show a simple alert, you should call the show method and pass some parameters.

```swift
StatusAlertView.show(
  on: view,
  icon: UIImage(),
  title: "Título",
  text: "Text",
  buttonTitle: "Button Title"
)
```

To detect the touch on its button or in links your class must conform with StatusAlertViewDelegate.