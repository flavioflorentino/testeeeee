#if DEBUG
import UIKit

public final class UINavigationControllerSpy: UINavigationController {
    public private(set) var pushViewControllerCallCount = 0
    public private(set) var popViewControllerCallCount = 0
    public private(set) var presentViewControllerCallCount = 0
    public private(set) var dismissViewControllerCallCount = 0
    public private(set) var setViewControllersCallCount = 0
    public private(set) var popToRootViewControllerCallCount = 0
    
    public private(set) var pushedViewController: UIViewController?
    public private(set) var popedViewController: UIViewController?
    public private(set) var viewControllerPresented: UIViewController?

    override public var presentedViewController: UIViewController? {
        viewControllerPresented
    }
    
    override public func pushViewController(_ viewController: UIViewController, animated: Bool) {
        pushedViewController = viewController
        pushViewControllerCallCount += 1
        super.pushViewController(viewController, animated: false)
    }
    
    override public func popViewController(animated: Bool) -> UIViewController? {
        let viewController = super.popViewController(animated: false)
        popedViewController = viewController
        popViewControllerCallCount += 1
        return viewController
    }
    
    // swiftlint:disable:next discouraged_optional_collection
    override public func popToRootViewController(animated: Bool) -> [UIViewController]? {
        let viewControllersList = super.popToRootViewController(animated: false)
        popToRootViewControllerCallCount += 1
        return viewControllersList
    }
    
    override public func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        viewControllerPresented = viewControllerToPresent
        presentViewControllerCallCount += 1
        super.present(viewControllerToPresent, animated: false)
        completion?()
    }
    
    override public func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        dismissViewControllerCallCount += 1
        super.dismiss(animated: false)
        completion?()
    }
    
    override public func setViewControllers(_ viewControllers: [UIViewController], animated: Bool) {
        setViewControllersCallCount += 1
        super.setViewControllers(viewControllers, animated: false)
    }
}
#endif
