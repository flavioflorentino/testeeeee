import UIKit

internal typealias TypographyStylable = TypographySet & FontConvertible

internal protocol TypographySet {
    var size: CGFloat { get }
    var weight: UIFont.Weight { get }
}

internal protocol FontConvertible {
    var font: UIFont { get }
    var fontName: String? { get }
    var fontType: String? { get }
}

extension FontConvertible where Self: TypographySet {
    internal var fontName: String? {
        nil
    }
    
    internal var fontType: String? {
        nil
    }
    
    internal var font: UIFont {
        let systemFont = scale(for: UIFont.systemFont(ofSize: size, weight: weight))
        
        guard let fontName = fontName, let fontType = fontType else {
            return systemFont
        }
        
        if !UIFont.familyNames.contains(fontName) {
            try? UIFont.register(fontName, ofType: fontType)
        }
        
        guard let font = UIFont(name: fontName, size: size) else {
            return systemFont
        }
        
        return scale(for: font)
    }
    
    private func scale(for font: UIFont, maximumScale: CGFloat = 1.0) -> UIFont {
        if #available(iOS 11, *) {
            let metrics = UIFontMetrics.default
            return metrics.scaledFont(for: font, maximumPointSize: font.pointSize * maximumScale)
        }
        
        let scaler = UIFont.preferredFont(forTextStyle: .body).pointSize / 17.0
        return font.withSize(scaler * font.pointSize)
    }
}
