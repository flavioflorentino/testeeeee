import CoreGraphics
import Foundation

public protocol ShadowType {
    var radius: CGFloat { get }
    var offSet: CGSize { get }
}
