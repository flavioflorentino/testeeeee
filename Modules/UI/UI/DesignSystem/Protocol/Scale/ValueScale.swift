import Foundation

public protocol ValueScale {
    associatedtype ValueType
    static var light: ValueType { get }
    static var medium: ValueType { get }
    static var strong: ValueType { get }
}
