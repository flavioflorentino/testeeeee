import Foundation

public protocol AdditionalScale {
    associatedtype AdditionalType
    static var ultraLight: AdditionalType { get }
}
