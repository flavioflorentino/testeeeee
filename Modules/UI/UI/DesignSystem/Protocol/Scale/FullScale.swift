import Foundation

public protocol FullScale {
    associatedtype FullType
    static var full: FullType { get }
}
