import Foundation

public protocol NoneScale {
    associatedtype NoneType
    static var none: NoneType { get }
}
