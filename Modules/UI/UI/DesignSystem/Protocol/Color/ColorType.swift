import Foundation

public protocol BrandingColorType {
    associatedtype BrandingColor
    static var branding900: BrandingColor { get }
    static var branding800: BrandingColor { get }
    static var branding700: BrandingColor { get }
    static var branding600: BrandingColor { get }
    static var branding500: BrandingColor { get }
    static var branding400: BrandingColor { get }
    static var branding300: BrandingColor { get }
    static var branding200: BrandingColor { get }
    static var branding100: BrandingColor { get }
    static var branding050: BrandingColor { get }
    static var brandingBase: BrandingColor { get }
}

public protocol UniversityColorType {
    associatedtype UniversityColor
    static var university800: UniversityColor { get }
    static var university600: UniversityColor { get }
    static var university200: UniversityColor { get }
    static var university100: UniversityColor { get }
    static var university050: UniversityColor { get }
    static var universityBase: UniversityColor { get }
}

public protocol BusinessColorType {
    associatedtype BusinessColor
    static var business900: BusinessColor { get }
    static var business800: BusinessColor { get }
    static var business700: BusinessColor { get }
    static var business600: BusinessColor { get }
    static var business500: BusinessColor { get }
    static var business400: BusinessColor { get }
    static var business300: BusinessColor { get }
    static var business200: BusinessColor { get }
    static var business100: BusinessColor { get }
    static var business050: BusinessColor { get }
    static var businessBase: BusinessColor { get }
}

public protocol GrayScaleColorType {
    associatedtype GrayScaleColor
    static var black: GrayScaleColor { get }
    static var grayscale950: GrayScaleColor { get }
    static var grayscale900: GrayScaleColor { get }
    static var grayscale850: GrayScaleColor { get }
    static var grayscale800: GrayScaleColor { get }
    static var grayscale750: GrayScaleColor { get }
    static var grayscale700: GrayScaleColor { get }
    static var grayscale600: GrayScaleColor { get }
    static var grayscale500: GrayScaleColor { get }
    static var grayscale400: GrayScaleColor { get }
    static var grayscale300: GrayScaleColor { get }
    static var grayscale200: GrayScaleColor { get }
    static var grayscale100: GrayScaleColor { get }
    static var grayscale050: GrayScaleColor { get }
    static var white: GrayScaleColor { get }
}

public protocol WarningColorType {
    associatedtype WarningColor
    static var warning900: WarningColor { get }
    static var warning600: WarningColor { get }
    static var warning400: WarningColor { get }
    static var warning200: WarningColor { get }
    static var warning050: WarningColor { get }
    static var warningBase: WarningColor { get }
}

public protocol CriticalColorType {
    associatedtype CriticalColor
    static var critical900: CriticalColor { get }
    static var critical600: CriticalColor { get }
    static var critical400: CriticalColor { get }
    static var critical200: CriticalColor { get }
    static var critical050: CriticalColor { get }
    static var criticalBase: CriticalColor { get }
}

public protocol SuccessColorType {
    associatedtype SuccessColor
    static var success900: SuccessColor { get }
    static var success600: SuccessColor { get }
    static var success500: SuccessColor { get }
    static var success400: SuccessColor { get }
    static var success050: SuccessColor { get }
    static var successBase: SuccessColor { get }
}

public protocol NeutralColorType {
    associatedtype NeutralColor
    static var neutral800: NeutralColor { get }
    static var neutral600: NeutralColor { get }
    static var neutral400: NeutralColor { get }
    static var neutral200: NeutralColor { get }
    static var neutral050: NeutralColor { get }
    static var neutralBase: NeutralColor { get }
}

public protocol NotificationColorType {
    associatedtype NotificationColor
    static var notification600: NotificationColor { get }
}

public protocol BackgroundColorType {
    associatedtype BackgroundColor
    static var backgroundPrimary: BackgroundColor { get }
    static var backgroundSecondary: BackgroundColor { get }
    static var backgroundTertiary: BackgroundColor { get }
}

public protocol GroupedBackgroundColorType {
    associatedtype GroupedBackgroundColor
    static var groupedBackgroundPrimary: GroupedBackgroundColor { get }
    static var groupedBackgroundSecondary: GroupedBackgroundColor { get }
    static var groupedBackgroundTertiary: GroupedBackgroundColor { get }
}

public protocol ExternalColorType {
    associatedtype ExternalColor
    static var externalFacebook: ExternalColor { get }
    static var externalTwitter: ExternalColor { get }
    static var externalWhatsApp: ExternalColor { get }
    static var external24HourBank: ExternalColor { get }
}

public protocol AdditionalColorType {
    associatedtype AdditionalColor
    static var additional001: AdditionalColor { get }
    static var additional002: AdditionalColor { get }
}

public protocol TemporaryColorType {
    associatedtype TemporaryColor
}

public protocol SupportHexadecimalColor {
    associatedtype HexadecimalColor
    static func hexColor(_ hexString: String) -> HexadecimalColor
}
