import UIKit

public protocol ChangeColorSet {
    /// The method returns a dynamic color with interface style color changed
    func change(_ style: InterfaceStyle, to newColor: UIColor) -> DynamicColorType
}
