import UIKit

public protocol LightenColorSet {
    /// The variable return an only for light (or default) color
    var lightColor: UIColor { get }
}
