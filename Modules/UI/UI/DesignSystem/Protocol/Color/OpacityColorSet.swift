import UIKit

public protocol OpacityColorSet {
    /// The method returns a dynamic color with alpha applied
    func opacity(_ opacity: Opacity.Style) -> DynamicColorType
}
