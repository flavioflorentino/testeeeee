import UIKit

public protocol DarkenColorSet {
    /// The variable return an only color for dark mode
    var darkColor: UIColor { get }
}
