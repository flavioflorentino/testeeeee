import UIKit

public protocol ColorSet {
    /// The variable return a dynamic color system preferred
    var color: UIColor { get }
}

extension ColorSet where Self: LightenColorSet & DarkenColorSet & ChangeColorSet {
    public var color: UIColor {
        dynamicColor(
            defaultColor: lightColor,
            darkColor: darkColor
        )
    }
    
    public func change(_ style: InterfaceStyle, to newColor: UIColor) -> DynamicColorType {
        DynamicColor(
            lightColor: style != .light ? lightColor : newColor,
            darkColor: style != .dark ? darkColor : newColor
        )
    }
    
    public func opacity(_ opacity: Opacity.Style) -> DynamicColorType {
        DynamicColor(
            lightColor: lightColor.withAlphaComponent(opacity.rawValue),
            darkColor: darkColor.withAlphaComponent(opacity.rawValue)
        )
    }
    
    // MARK: Private Methods Helpers
    private func dynamicColor(defaultColor: UIColor, darkColor: UIColor) -> UIColor {
        if #available(iOS 13.0, *) {
            return UIColor { traitCollection -> UIColor in
                switch traitCollection.userInterfaceStyle {
                case .dark:
                    return darkColor
                default:
                    return defaultColor
                }
            }
        }
        return defaultColor
    }
}
