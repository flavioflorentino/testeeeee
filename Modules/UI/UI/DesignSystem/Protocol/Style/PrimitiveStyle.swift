import UIKit

public protocol PrimitiveStyleConfiguration {
    associatedtype View
    func makeStyle(_ style: StyleCore<View>)
    func applied<Configuration>(to object: StyleCore<Configuration>)
}

public extension PrimitiveStyleConfiguration {
    func applied<Configuration>(to object: StyleCore<Configuration>) {
        guard let newStyle = object as? StyleCore<View> else {
            return
        }
        
        makeStyle(newStyle)
    }
}

public protocol PrimitiveViewStyle {
    func viewStyle<S>(_ style: S) -> StyleCore<UIView> where S: ViewStyle
}

public extension PrimitiveViewStyle where Self: UIView {
    @discardableResult
    func viewStyle<S>(_ style: S) -> StyleCore<UIView> where S: ViewStyle {
        let configuration = StyleCore<UIView>(self)
        style.applied(to: configuration)
        return configuration
    }
}

public protocol PrimitiveLabelStyle {
    func labelStyle<S>(_ style: S) -> StyleCore<UILabel> where S: LabelStyle
}

public extension PrimitiveLabelStyle where Self: UILabel {
    @discardableResult
    func labelStyle<S>(_ style: S) -> StyleCore<UILabel> where S: LabelStyle {
        let configuration = StyleCore<UILabel>(self)
        style.applied(to: configuration)
        return configuration
    }
}

public protocol PrimitiveButtonStyle {
    func buttonStyle<S>(_ style: S) -> StyleCore<UIButton> where S: ButtonStyle
}

public extension PrimitiveButtonStyle where Self: UIButton {
    @discardableResult
    func buttonStyle<S>(_ style: S) -> StyleCore<UIButton> where S: ButtonStyle {
        let configuration = StyleCore<UIButton>(self)
        style.applied(to: configuration)
        return configuration
    }
}

public protocol PrimitiveTextFieldStyle {
    func textFieldStyle<S>(_ style: S) -> StyleCore<UITextField> where S: TextFieldStyle
}

public extension PrimitiveTextFieldStyle where Self: UITextField {
    @discardableResult
    func textFieldStyle<S>(_ style: S) -> StyleCore<UITextField> where S: TextFieldStyle {
        let configuration = StyleCore<UITextField>(self)
        style.applied(to: configuration)
        return configuration
    }
}

public protocol PrimitiveImageStyle {
    func imageStyle<S>(_ style: S) -> StyleCore<UIImageView> where S: ImageStyle
}

public extension PrimitiveImageStyle where Self: UIImageView {
    @discardableResult
    func imageStyle<S>(_ style: S) -> StyleCore<UIImageView> where S: ImageStyle {
        let configuration = StyleCore<UIImageView>(self)
        style.applied(to: configuration)
        return configuration
    }
}

public protocol PrimitiveNavigationStyle {
    func navigationStyle<S>(_ style: S) -> StyleCore<UINavigationBar> where S: NavigationStyle
}

public extension PrimitiveNavigationStyle where Self: UINavigationBar {
    @discardableResult
    func navigationStyle<S>(_ style: S) -> StyleCore<UINavigationBar> where S: NavigationStyle {
        let configuration = StyleCore<UINavigationBar>(self)
        style.applied(to: configuration)
        return configuration
    }
}

public protocol ViewStyle: PrimitiveStyleConfiguration { }

public protocol LabelStyle: PrimitiveStyleConfiguration { }

public protocol ButtonStyle: PrimitiveStyleConfiguration { }

public protocol TextFieldStyle: PrimitiveStyleConfiguration { }

public protocol ImageStyle: PrimitiveStyleConfiguration { }

public protocol NavigationStyle: PrimitiveStyleConfiguration { }

extension UIView: PrimitiveViewStyle { }

extension UILabel: PrimitiveLabelStyle { }

extension UIButton: PrimitiveButtonStyle { }

extension UITextField: PrimitiveTextFieldStyle { }

extension UIImageView: PrimitiveImageStyle { }

extension UINavigationBar: PrimitiveNavigationStyle { }
