import UIKit
import AssetsKit

public enum ApolloFeedbackViewType {
    case icon(style: Style), sticker(style: Style)
    
    public enum Style {
        case error, warning, info, success
        
        var icon: UIImage {
            switch self {
            case .error:
                return makeImage(from: .exclamationCircle, withColor: Colors.critical900.color)
            case .info:
                return makeImage(from: .infoCircle, withColor: Colors.neutral600.color)
            case .success:
                return makeImage(from: .checkCircle, withColor: Colors.branding600.color)
            case .warning:
                return makeImage(from: .exclamationTriangle, withColor: Colors.warning600.color)
            }
        }
        
        var sticker: UIImage {
            switch self {
            case .error:
                return Resources.Illustrations.iluError.image
            case .info:
                return Resources.Illustrations.iluPersonInfo.image
            case .success:
                return Resources.Illustrations.iluSuccess.image
            case .warning:
                return Resources.Illustrations.iluWarning.image
            }
        }
        
        private func makeImage(from icon: Iconography, withColor color: UIColor) -> UIImage {
            let label = UILabel()
            label.text = icon.rawValue
            label.labelStyle(IconLabelStyle(type: .large))
                .with(\.textAlignment, .center)
                .with(\.textColor, color)
            return label.toImage
        }
    }
    
    var image: UIImage {
        switch self {
        case .icon(let style):
            return style.icon
        case .sticker(let style):
            return style.sticker
        }
    }
}
