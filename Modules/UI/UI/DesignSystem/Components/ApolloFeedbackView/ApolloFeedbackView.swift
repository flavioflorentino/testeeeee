import UIKit
import SnapKit

// MARK: FeedbackViewStyle Enum
public enum ApolloFeedbackViewStyle {
    case danger
    case confirmation
    case custom

    var primaryButtonColors: (backgroundColor: UIColor,
                              titleColor: UIColor) {
        switch self {
        case .confirmation, .custom:
            return (backgroundColor: Colors.branding600.color,
                    titleColor: Colors.white.lightColor)
        case .danger:
            return (backgroundColor: Colors.critical600.color,
                    titleColor: Colors.white.lightColor)
        }
    }
    
    var secondaryButtonColors: (backgroundColor: UIColor,
                                titleColor: UIColor) {
        switch self {
        case .confirmation, .custom:
            return  (backgroundColor: .clear,
                    titleColor: Colors.branding600.color)
        case .danger:
            return (backgroundColor: .clear,
                    titleColor: Colors.branding600.color)
        }
    }
}

// MARK: FeedbackViewContent Struct
public struct ApolloFeedbackViewContent: Equatable {
    let image: UIImage?
    let title: String
    let description: NSAttributedString
    let primaryButtonTitle: String
    let secondaryButtonTitle: String
    
    public init(image: UIImage?,
                title: String,
                description: NSAttributedString,
                primaryButtonTitle: String,
                secondaryButtonTitle: String = "") {
        self.image = image
        self.title = title
        self.description = description
        self.primaryButtonTitle = primaryButtonTitle
        self.secondaryButtonTitle = secondaryButtonTitle
    }

    public init(type: ApolloFeedbackViewType,
                title: String,
                description: NSAttributedString,
                primaryButtonTitle: String,
                secondaryButtonTitle: String = "") {
        self.init(image: type.image,
                  title: title,
                  description: description,
                  primaryButtonTitle: primaryButtonTitle,
                  secondaryButtonTitle: secondaryButtonTitle)
    }
}

// MARK: FeedbackView
public class ApolloFeedbackView: UIView {
    // MARK: Properties
    let style: ApolloFeedbackViewStyle
    
    // MARK: Callbacks
    public var didTapPrimaryButton: (() -> Void)?
    public var didTapSecondaryButton: (() -> Void)?
    
    // MARK: View Properties
    lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.bounces = false
        scroll.showsHorizontalScrollIndicator = false
        return scroll
    }()
    
    private lazy var contentContainerView = UIView(frame: .zero)
    
    // Main view with Image, Title and Description
    lazy var contentView = ApolloFeedbackContentView(frame: .zero)
    
    lazy var primaryButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(pressPrimaryButton), for: .touchUpInside)
        return button
    }()
    
    lazy var secondaryButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(pressSecondaryButton), for: .touchUpInside)
        return button
    }()
    
    lazy var buttonsStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [primaryButton, secondaryButton])
        stackView.distribution = .fillProportionally
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    // MARK: Initializer
    
    /// Creates a FeedbackView with the given style. .confirmation = green background button background, .danger = red background button background.
    /// If you need a custom style use the second  initializer to set custom colors.
    public init(content: ApolloFeedbackViewContent, style: ApolloFeedbackViewStyle = .confirmation) {
        self.style = style
        super.init(frame: .zero)
        setupView()
        configureView(content: content,
                      primaryButtonBackgroundColor: style.primaryButtonColors.backgroundColor,
                      primaryButtonTitleColor: style.primaryButtonColors.titleColor,
                      secondaryButtonBackgroundColor: style.secondaryButtonColors.backgroundColor,
                      secondaryButtonTitleColor: style.secondaryButtonColors.titleColor)
    }
    
    /// Use this initializer to set custom colors, if you want to use a existing style use the other initializer.
    public init(content: ApolloFeedbackViewContent,
                primaryButtonBackgroundColor: UIColor = Colors.branding600.color,
                primaryButtonTitleColor: UIColor = Colors.white.lightColor,
                secondaryButtonBackgroundColor: UIColor = .clear,
                secondaryButtonTitleColor: UIColor = Colors.branding600.color) {
        style = .custom
        super.init(frame: .zero)
        setupView()
        configureView(content: content,
                      primaryButtonBackgroundColor: primaryButtonBackgroundColor,
                      primaryButtonTitleColor: primaryButtonTitleColor,
                      secondaryButtonBackgroundColor: secondaryButtonBackgroundColor,
                      secondaryButtonTitleColor: secondaryButtonTitleColor)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
        if contentView.height > contentContainerView.frame.height {
            setupContentInScrollView()
        }
    }
    
    private func setupContentInScrollView() {
        contentContainerView.removeFromSuperview()
        contentContainerView.snp.removeConstraints()
        contentView.snp.removeConstraints()
        
        scrollView.addSubview(contentContainerView)
        addSubview(scrollView)
        
        scrollView.snp.makeConstraints {
            $0.top.leading.trailing.equalTo(self)
            $0.bottom.equalTo(buttonsStackView.snp.top).inset(-Spacing.base02)
        }
        
        contentContainerView.snp.makeConstraints {
            $0.top.bottom.leading.trailing.equalTo(scrollView)
            $0.width.equalTo(self)
        }
        
        contentView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.greaterThanOrEqualToSuperview()
        }
        
        if #available(iOS 11.0, *) {
            scrollView.contentLayoutGuide.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        }
    }

	public func primaryButtonIsHidden(_ isHidden: Bool) {
        primaryButton.isHidden = isHidden
    }
    
    public func secondaryButtonIsHidden(_ isHidden: Bool) {
        secondaryButton.isHidden = isHidden
    }
    
    // MARK: - Private Functions
    func configureView(content: ApolloFeedbackViewContent,
                       primaryButtonBackgroundColor: UIColor = Colors.branding600.color,
                       primaryButtonTitleColor: UIColor = Colors.white.lightColor,
                       secondaryButtonBackgroundColor: UIColor = .clear,
                       secondaryButtonTitleColor: UIColor = Colors.branding600.color) {
        contentView.configureContent(content)
        primaryButton.setTitle(content.primaryButtonTitle, for: .normal)
        primaryButton.setTitleColor(primaryButtonTitleColor, for: .normal)
        secondaryButton.setTitle(content.secondaryButtonTitle, for: .normal)
        secondaryButton.backgroundColor = secondaryButtonBackgroundColor
        secondaryButton.setTitleColor(secondaryButtonTitleColor, for: .normal)
        // Style setado aqui por conta do bug do underline
        secondaryButton.buttonStyle(LinkButtonStyle())
        // Configurado por ultimo para sobrescrever o buttonStyle se necessário
        primaryButton.setBackgroundColor(primaryButtonBackgroundColor, for: .normal)
        primaryButton.setBackgroundColor(primaryButtonBackgroundColor, for: .highlighted)
        primaryButton.setBackgroundColor(primaryButtonBackgroundColor, for: .selected)
    }
    
    // MARK: - Selectors
    
    @objc
    func pressPrimaryButton() {
        didTapPrimaryButton?()
    }
    
    @objc
    func pressSecondaryButton() {
        didTapSecondaryButton?()
    }
}

// MARK: - ViewConfiguration
extension ApolloFeedbackView: ViewConfiguration {
    public func buildViewHierarchy() {
        contentContainerView.addSubview(contentView)
        addSubviews(contentContainerView, buttonsStackView)
    }
    
    public func setupConstraints() {
        primaryButton.snp.makeConstraints {
            $0.height.equalTo(Sizing.base06)
        }
        
        secondaryButton.snp.makeConstraints {
            $0.height.equalTo(Sizing.base06)
        }

        buttonsStackView.snp.makeConstraints {
            $0.leading.trailing.equalTo(self).inset(Spacing.base02)
            $0.bottom.equalTo(self).inset(Spacing.base03)
        }
        
        contentContainerView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(buttonsStackView.snp.top).inset(-Spacing.base02)
        }
        
        contentView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.trailing.equalToSuperview()
        }
    }
    
    public func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
    
    private func setupView() {
        buildViewHierarchy()
        setupConstraints()
        configureViews()
    }
}
