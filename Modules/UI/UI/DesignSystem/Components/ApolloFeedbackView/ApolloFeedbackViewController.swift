import SnapKit
import UIKit

public final class ApolloFeedbackViewController: UIViewController {
    // MARK: - Outlets
    
    private let feedbackView: ApolloFeedbackView
    
    // MARK: - Properties
    
    public var didTapPrimaryButton: (() -> Void)?
    public var didTapSecondaryButton: (() -> Void)?
    public var onViewDidAppear: (() -> Void)?
    public var onViewWillDisapear: ((Bool) -> Void)?

    // MARK: - Initialization
    
    public init(content: ApolloFeedbackViewContent, style: ApolloFeedbackViewStyle = .confirmation) {
        self.feedbackView = ApolloFeedbackView(content: content, style: style)
        
        super.init(nibName: nil, bundle: nil)
    }
    
    public init(type: ApolloFeedbackViewType,
                title: String,
                description: NSAttributedString,
                primaryButtonTitle: String,
                secondaryButtonTitle: String) {
        let content = ApolloFeedbackViewContent(image: type.image,
                                                title: title,
                                                description: description,
                                                primaryButtonTitle: primaryButtonTitle,
                                                secondaryButtonTitle: secondaryButtonTitle)
        self.feedbackView = ApolloFeedbackView(content: content)
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override public func loadView() {
        view = feedbackView
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        feedbackView.didTapPrimaryButton = didTapPrimaryButton
        feedbackView.didTapSecondaryButton = didTapSecondaryButton
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        onViewDidAppear?()
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        onViewWillDisapear?(isMovingFromParent)
    }
}
