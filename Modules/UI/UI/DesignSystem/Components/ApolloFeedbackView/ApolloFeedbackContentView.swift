import SnapKit
import UIKit

extension ApolloFeedbackContentView.Layout {
    enum Size: CGFloat {
        case regularImage = 144
        case smallImage = 100
    }
}

final class ApolloFeedbackContentView: UIView {
    fileprivate enum Layout {}
    
    private lazy var isSmallScreen = UIScreen.main.bounds.height < 570
    
    // Manually compute the height
    var height: CGFloat {
        let contentViewWidth = UIScreen.main.bounds.width
        let targetSize = CGSize(width: contentViewWidth, height: UIView.layoutFittingCompressedSize.height)
        return systemLayoutSizeFitting(targetSize).height
    }
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.textColor, Colors.grayscale500.color)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureContent(_ content: ApolloFeedbackViewContent) {
        imageView.image = content.image
        titleLabel.text = content.title
        descriptionLabel.attributedText = content.description
        // Por usar NSAtributeString na descrição, precisa depois reforçar o alinhamento centralizado
        descriptionLabel.textAlignment = .center
    }
    
    func configureContent(image: UIImage?, content: StatefulContent?) {
        imageView.image = image
        titleLabel.text = content?.title
        descriptionLabel.text = content?.description
    }
}

// MARK: - ViewConfiguration
extension ApolloFeedbackContentView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubviews(imageView, titleLabel, descriptionLabel)
    }
    
    func setupConstraints() {
        let imageSize: Layout.Size = isSmallScreen ?.smallImage : .regularImage
        
        imageView.snp.makeConstraints {
            $0.top.centerX.equalToSuperview()
            $0.size.lessThanOrEqualTo(imageSize.rawValue)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview()
        }
    }
    
    func configureViews() {
        backgroundColor = .clear
    }
}
