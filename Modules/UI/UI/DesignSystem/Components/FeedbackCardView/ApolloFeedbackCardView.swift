import Foundation
import UIKit

class ApolloFeedbackCardView: UIView {
    typealias ButtonAction = (() -> Void)
    
// MARK: - PRIVATE VARS -
    private lazy var icon: UILabel = {
        let label = UILabel()
        label.labelStyle(IconLabelStyle(type: .medium))
        label.text = Iconography.infoCircle.rawValue
        return label
    }()
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
        label.textColor = Colors.grayscale600.color
        label.numberOfLines = 3
        return label
    }()
    
    private lazy var primaryButton: UIButton = {
        let button = UIButton(type: .custom)
        button.addTarget(self, action: #selector(touchFirstAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var secondaryButton: UIButton = {
        let button = UIButton(type: .custom)
        button.addTarget(self, action: #selector(touchSecondAction), for: .touchUpInside)
        return button
    }()
    
    private var primaryAction: ButtonAction?
    private var secondaryAction: ButtonAction?
    
    private let emphasisStyle: ApolloFeedbackCardViewStyle
    
// MARK: - INIT -
    init(
        description: String,
        iconType: ApolloFeedbackCardViewIconType,
        layoutType: ApolloFeedbackCardViewLayoutType,
        emphasisStyle: ApolloFeedbackCardViewStyle
    ) {
        self.emphasisStyle = emphasisStyle
        super.init(frame: .zero)
        buildLayout()
        configureFeedbackView(description: description, iconType: iconType, layoutType: layoutType)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - LIFE CICLE -
extension ApolloFeedbackCardView: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubviews(icon, descriptionLabel)
    }
    
    public func setupConstraints() {
        icon.snp.makeConstraints {
            $0.top.leading.equalToSuperview().offset(Spacing.base02)
            $0.size.equalTo(Spacing.base03)
            $0.bottom.lessThanOrEqualToSuperview().offset(-Spacing.base02)
        }
    }
    
    public func configureViews() {
        viewStyle(RoundedViewStyle())
            .with(\.backgroundColor, emphasisStyle.backgroundColor)
            .with(\.border, .light(color: emphasisStyle.borderColor))
        descriptionLabel.textColor = emphasisStyle.descriptionColor
        primaryButton.setTitleColor(emphasisStyle.buttonsColor, for: .normal)
        secondaryButton.setTitleColor(emphasisStyle.buttonsColor, for: .normal)
    }
}

// MARK: - CONFIGURE VIEW FUNCTIONS -
private extension ApolloFeedbackCardView {
    func configureFeedbackView(
            description: String,
            iconType: ApolloFeedbackCardViewIconType,
            layoutType: ApolloFeedbackCardViewLayoutType
    ) {
        descriptionLabel.text = description
        configureIcon(iconType: iconType)
        configureLayoutType(layoutType: layoutType)
    }
    
    func configureIcon(iconType: ApolloFeedbackCardViewIconType) {
        switch iconType {
        case .information:
            icon.text = Iconography.infoCircle.rawValue
            icon.textColor = Colors.neutral600.color
        case .warning:
            icon.text = Iconography.exclamationTriangle.rawValue
            icon.textColor = Colors.warning600.color
        case .error:
            icon.text = Iconography.exclamationCircle.rawValue
            icon.textColor = Colors.critical600.color
        case .success:
            icon.text = Iconography.checkCircle.rawValue
            icon.textColor = Colors.success600.color
        }
    }
    
    func configureLayoutType(layoutType: ApolloFeedbackCardViewLayoutType) {
        switch layoutType {
        case .onlyText:
            configureFullTextConstraints()
        case .buttonOnRight(let configuration):
            primaryAction = configuration.action
            configureButton(buttons: [primaryButton], configurations: [configuration])
            configureButtonOnRightConstraints()
        case .oneAction(let configuration):
            primaryAction = configuration.action
            configureButton(buttons: [primaryButton], configurations: [configuration])
            configureOneActionConstraints()
        case let .twoActions(primaryConfiguration, secondaryConfiguration):
            primaryAction = primaryConfiguration.action
            secondaryAction = secondaryConfiguration.action
            configureButton(buttons: [primaryButton, secondaryButton], configurations: [primaryConfiguration, secondaryConfiguration])
            configureTwoActionsConstraints()
        }
    }
}

// MARK: - CONFIGURE CONSTRAINTS FUNCTIONS -
private extension ApolloFeedbackCardView {
    func configureFullTextConstraints() {
        descriptionLabel.snp.makeConstraints {
            $0.top.trailing.bottom.equalToSuperview().inset(Spacing.base02)
            $0.leading.equalTo(icon.snp.trailing).offset(Spacing.base01)
            $0.bottom.lessThanOrEqualToSuperview().offset(-Spacing.base02)
        }
    }
    
    func configureButtonOnRightConstraints() {
        descriptionLabel.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base02)
            $0.leading.equalTo(icon.snp.trailing).offset(Spacing.base01)
            $0.bottom.lessThanOrEqualToSuperview().offset(-Spacing.base02)
        }
        
        descriptionLabel.snp.contentHuggingHorizontalPriority = 0
        descriptionLabel.snp.contentCompressionResistanceHorizontalPriority = 0
        
        primaryButton.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalTo(descriptionLabel.snp.trailing).offset(Spacing.base01)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        primaryButton.snp.contentHuggingHorizontalPriority = 50
        primaryButton.snp.contentCompressionResistanceHorizontalPriority = 50
    }
    
    func configureOneActionConstraints() {
        descriptionLabel.snp.makeConstraints {
            $0.top.trailing.equalToSuperview().inset(Spacing.base02)
            $0.leading.equalTo(icon.snp.trailing).offset(Spacing.base01)
        }
        
        primaryButton.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base00)
            $0.leading.greaterThanOrEqualToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base00)
        }
    }
    
    func configureTwoActionsConstraints() {
        descriptionLabel.snp.makeConstraints {
            $0.top.trailing.equalToSuperview().inset(Spacing.base02)
            $0.leading.equalTo(icon.snp.trailing).offset(Spacing.base01)
        }
        
        secondaryButton.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base00)
            $0.leading.greaterThanOrEqualToSuperview().offset(Spacing.base02)
            $0.trailing.equalTo(primaryButton.snp.leading).offset(-Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base00)
        }

        primaryButton.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base00)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base00)
        }
    }
}

// MARK: - ACTIONS -
private extension ApolloFeedbackCardView {
    @objc
    func touchFirstAction() {
        primaryAction?()
    }
    
    @objc
    func touchSecondAction() {
        secondaryAction?()
    }
    
    func configureButton(buttons: [UIButton], configurations: [ApolloFeedbackCardButtonAction]) {
        for index in 0 ..< buttons.count {
            let button = buttons[index]
            let configuration = configurations[index]
            button.setTitle(configuration.title, for: .normal)
            button.contentHorizontalAlignment = .right
            button.buttonStyle(LinkButtonStyle()).with(\.typography, .bodySecondary())
            addSubview(button)
        }
    }
}
