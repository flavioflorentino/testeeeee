import Foundation

public enum ApolloFeedbackCardViewIconType: Equatable {
    case information
    case warning
    case error
    case success
}

public struct ApolloFeedbackCardButtonAction {
    let title: String
    let action: (() -> Void)
    
    public init (title: String, action: @escaping (() -> Void)) {
        self.title = title
        self.action = action
    }
}

public enum ApolloFeedbackCardViewLayoutType {
    case onlyText
    case buttonOnRight(ApolloFeedbackCardButtonAction)
    case oneAction(ApolloFeedbackCardButtonAction)
    case twoActions(ApolloFeedbackCardButtonAction, ApolloFeedbackCardButtonAction)
}

public enum ApolloFeedbackCardViewEmphasisType {
    case low
    case high
}
