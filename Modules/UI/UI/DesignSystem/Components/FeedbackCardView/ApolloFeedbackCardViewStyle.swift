import Foundation

protocol ApolloFeedbackCardViewStyle {
    var backgroundColor: UIColor { get }
    var descriptionColor: UIColor { get }
    var buttonsColor: UIColor { get }
    var borderColor: Colors.Style { get }
}

struct ApolloFeedbackCardHighEmphasisStyle: ApolloFeedbackCardViewStyle {
    var backgroundColor: UIColor = Colors.black.color
    
    var descriptionColor: UIColor = Colors.grayscale100.color
    
    var buttonsColor: UIColor = Colors.branding600.color
    
    var borderColor: Colors.Style = .grayscale700()
}

struct ApolloFeedbackCardLowEmphasisStyle: ApolloFeedbackCardViewStyle {
    var backgroundColor: UIColor = Colors.white.color
    
    var descriptionColor: UIColor = Colors.grayscale600.color
    
    var buttonsColor: UIColor = Colors.branding600.color
    
    var borderColor: Colors.Style = .grayscale100()
}
