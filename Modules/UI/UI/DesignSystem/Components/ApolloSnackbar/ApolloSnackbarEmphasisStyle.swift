import Foundation

public enum ApolloSnackbarEmphasisStyle: Equatable {
    case low
    case high
    
    var viewStyle: ApolloFeedbackCardViewStyle {
        switch self {
        case .low:
            return ApolloSnackbarLowEmphasisStyle()
        case .high:
            return ApolloSnackbarHighEmphasisStyle()
        }
    }
}

struct ApolloSnackbarLowEmphasisStyle: ApolloFeedbackCardViewStyle {
    var backgroundColor: UIColor = Colors.white.change(.dark, to: Colors.grayscale900.lightColor).color
    
    var descriptionColor: UIColor = Colors.grayscale600.color
    
    var buttonsColor: UIColor = Colors.branding600.color
    
    var borderColor: Colors.Style = .grayscale100()
}

struct ApolloSnackbarHighEmphasisStyle: ApolloFeedbackCardViewStyle {
    var backgroundColor: UIColor = Colors.black.color

    var descriptionColor: UIColor = Colors.grayscale100.color
    
    var buttonsColor: UIColor = Colors.branding600.color
    
    var borderColor: Colors.Style = .grayscale700()
}
