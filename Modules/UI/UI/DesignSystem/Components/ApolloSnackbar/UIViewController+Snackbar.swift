import Foundation
import UIKit

public extension UIViewController {
    /// Show a Apollo Snackbar view in a toast alert style
    ///
    /// - Parameter snackbar: The snackbar model to be displayed
    /// - Parameter duration: The amount of time the component will be visible
    func showSnackbar(_ snackbar: ApolloSnackbar, onTopOf topView: UIView? = nil, duration: TimeInterval = 5.0) {
        guard let topView = topView ?? view else {
            return
        }
        ApolloSnackbarManager().show(snackbar, onTopOf: topView, duration: duration)
    }
}
