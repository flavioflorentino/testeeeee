import Foundation
import UIKit
import SnapKit

final class ApolloSnackbarManager {
    func show(_ snackbar: ApolloSnackbar, onTopOf view: UIView, duration: TimeInterval) {
        let snackbarView = createSnackbarView(snackbar)
        configureView(snackbarView)
        layoutSnackbarView(snackbarView, superView: view)
        displaySnackbarView(snackbarView, forDuration: duration)
    }
    
    private func createSnackbarView(_ snackbar: ApolloSnackbar) -> ApolloFeedbackCardView {
         ApolloFeedbackCardView(
            description: snackbar.text,
            iconType: snackbar.iconType,
            layoutType: layoutType(for: snackbar),
            emphasisStyle: snackbar.emphasis.viewStyle
        )
    }
    
    private func configureView(_ snackbarView: ApolloFeedbackCardView) {
        snackbarView.layer.zPosition = 1
        
        if #available(iOS 13.0, *), UITraitCollection.current.userInterfaceStyle == .dark {
            return
        }
        
        snackbarView.layer.setShadow(
            radius: 4,
            offSet: CGSize(width: 0, height: 4),
            color: Colors.black.lightColor,
            opacity: 0.3
        )
    }
    
    private func layoutType(for snackbar: ApolloSnackbar) -> ApolloFeedbackCardViewLayoutType {
        guard let button = snackbar.button else {
            return .onlyText
        }
        
        let action = ApolloFeedbackCardButtonAction(title: button.title, action: button.action)
        
        if button.position == .right {
            return .buttonOnRight(action)
        }
        
        return .oneAction(action)
    }
    
    private func layoutSnackbarView(_ snackbarView: UIView, superView: UIView) {
        superView.addSubview(snackbarView)
        
        snackbarView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(superView.compatibleSafeArea.bottom).offset(-Spacing.base02)
        }
    }
    
    private func displaySnackbarView(_ snackbarView: UIView, forDuration displayDuration: TimeInterval) {
        snackbarView.alpha = 0.0
        
        let fadeInAnimation = UIViewPropertyAnimator(duration: 0.5, curve: .easeIn) {
            snackbarView.alpha = 1.0
        }
        
        fadeInAnimation.addCompletion { _ in
            let fadeOutAnimation = UIViewPropertyAnimator(duration: 0.5, curve: .easeOut) {
                snackbarView.alpha = 0.0
            }
            
            fadeOutAnimation.addCompletion { _ in
                snackbarView.removeFromSuperview()
            }
            
            fadeOutAnimation.startAnimation(afterDelay: displayDuration)
        }
        
        fadeInAnimation.startAnimation()
    }
}
