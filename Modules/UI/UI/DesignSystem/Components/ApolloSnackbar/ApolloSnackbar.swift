import Foundation

public struct ApolloSnackbar: Equatable {
    let text: String
    let iconType: ApolloFeedbackCardViewIconType
    let button: ApolloSnackbarButton?
    let emphasis: ApolloSnackbarEmphasisStyle
    
    public init(
        text: String,
        iconType: ApolloFeedbackCardViewIconType,
        button: ApolloSnackbarButton? = nil,
        emphasis: ApolloSnackbarEmphasisStyle = .low
    ) {
        self.text = text
        self.iconType = iconType
        self.button = button
        self.emphasis = emphasis
    }

    public static func == (lhs: ApolloSnackbar, rhs: ApolloSnackbar) -> Bool {
        lhs.text == rhs.text &&
            lhs.iconType == rhs.iconType &&
            lhs.button?.title == rhs.button?.title &&
            lhs.button?.position == rhs.button?.position &&
            lhs.emphasis == rhs.emphasis
    }
}

public struct ApolloSnackbarButton {
    public enum Position: Equatable {
        case right, bottom
    }
    
    let title: String
    let position: Position
    let action: (() -> Void)
    
    public init(title: String, position: Position, action: @escaping () -> Void) {
        self.title = title
        self.position = position
        self.action = action
    }
}
