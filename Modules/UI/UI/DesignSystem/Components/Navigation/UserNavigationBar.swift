import UIKit

private extension UserNavigationBar.Layout {
    static let idealBarSize: CGFloat = 72.0
    static let largeTitleBarSize: CGFloat = 96.0
}

public final class UserNavigationBar: UINavigationBar {
    fileprivate enum Layout { }
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.imageStyle(AvatarImageStyle(size: .small))
        return imageView
    }()
    
    private var observer: NSKeyValueObservation?
    
    public var isImageHidden: Bool = false {
        didSet {
            imageView.isHidden = isImageHidden
        }
    }
    
    public var imageOpacity: CGFloat = 1.0 {
        didSet {
            imageView.alpha = imageOpacity
        }
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
        configureValueObservation()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setImage(_ image: UIImage?) {
        imageView.image = image
    }
    
    public func setImage(_ url: URL?, placeholder: UIImage? = nil, completion: ((UIImage?) -> Void)? = nil) {
        imageView.setImage(url: url, placeholder: placeholder, completion: completion)
    }
    
    private func configureValueObservation() {
        observer = observe(\.frame, options: [.new], changeHandler: { _, change in
            guard let currentValue = change.newValue?.size.height else {
                return
            }
            
            let value = Layout.largeTitleBarSize - Layout.idealBarSize
            self.imageView.alpha = (currentValue - Layout.idealBarSize) / value
        })
    }
}

extension UserNavigationBar: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(imageView)
    }
    
    public func setupConstraints() {
        imageView.snp.makeConstraints {
            $0.trailing.equalTo(snp.trailing).offset(-compatibleLayoutMargins.trailing)
            $0.bottom.equalTo(snp.bottom).offset(-compatibleLayoutMargins.bottom)
        }
    }
    
    public func configureViews() {
        navigationStyle(LargeNavigationStyle())
    }
}
