import UIKit

public final class UserNavigationController: UINavigationController {
    public var userNavigationBar: UserNavigationBar? {
        navigationBar as? UserNavigationBar
    }
    
    override public init(rootViewController: UIViewController) {
        super.init(navigationBarClass: UserNavigationBar.self, toolbarClass: nil)
        viewControllers = [rootViewController]
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        interactivePopGestureRecognizer?.addTarget(self, action: #selector(popGestureRecognizer(_:)))
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    private func popGestureRecognizer(_ sender: UIGestureRecognizer) {
        let value = sender.location(in: view).x
        let currentAlpha = value / view.frame.width
        
        userNavigationBar?.isImageHidden = currentAlpha > 0.5 ? false : true
        userNavigationBar?.imageOpacity = currentAlpha > 0.5 ? currentAlpha * 2 - 1 : 0
    }
}

extension UserNavigationController: UINavigationBarDelegate {
    public func navigationBar(_ navigationBar: UINavigationBar, shouldPush item: UINavigationItem) -> Bool {
        userNavigationBar?.isImageHidden = true
        return true
    }
    
    public func navigationBar(_ navigationBar: UINavigationBar, didPush item: UINavigationItem) {
        userNavigationBar?.imageOpacity = 0.0
    }
    
    public func navigationBar(_ navigationBar: UINavigationBar, shouldPop item: UINavigationItem) -> Bool {
        userNavigationBar?.isImageHidden = false
        return true
    }
    
    public func navigationBar(_ navigationBar: UINavigationBar, didPop item: UINavigationItem) {
        userNavigationBar?.isImageHidden = false
    }
}
