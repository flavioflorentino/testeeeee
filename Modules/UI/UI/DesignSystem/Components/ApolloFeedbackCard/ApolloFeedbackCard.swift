import Foundation
import UIKit

public final class ApolloFeedbackCard: UIView {
    public typealias ButtonAction = (() -> Void)
    private let feedBack: ApolloFeedbackCardView
    private let emphasisStyle: ApolloFeedbackCardViewStyle

// MARK: - INIT -
    public init(
        description: String,
        iconType: ApolloFeedbackCardViewIconType,
        layoutType: ApolloFeedbackCardViewLayoutType,
        emphasisType: ApolloFeedbackCardViewEmphasisType = .low
    ) {
        emphasisStyle = emphasisType == .low ? ApolloFeedbackCardLowEmphasisStyle() : ApolloFeedbackCardHighEmphasisStyle()
        feedBack = ApolloFeedbackCardView(
            description: description,
            iconType: iconType,
            layoutType: layoutType,
            emphasisStyle: emphasisStyle
        )
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func createEmphasyStyle(emphasys: ApolloFeedbackCardViewEmphasisType) -> ApolloFeedbackCardViewStyle {
        emphasys == .low ? ApolloFeedbackCardLowEmphasisStyle() : ApolloFeedbackCardHighEmphasisStyle()
    }
}

extension ApolloFeedbackCard: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(feedBack)
    }
    
    public func setupConstraints() {
        feedBack.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    public func configureViews() {
        feedBack.viewStyle(RoundedViewStyle()).with(\.border, .light(color: emphasisStyle.borderColor))
    }
}
