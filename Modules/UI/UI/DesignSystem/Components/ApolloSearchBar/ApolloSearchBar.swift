import UIKit
import AssetsKit

public protocol ApolloSearchBarDelegate: NSObjectProtocol {
    func didChangeText(_ newText: String?)
    func didCancel()
    func didBeginEditing()
}

public extension ApolloSearchBarDelegate {
    func didBeginEditing() { }
}

public final class ApolloSearchBar: UIControl {
    fileprivate enum Layout { }

    public var placeholderText: String = Layout.Strings.placeholder {
        didSet {
            updatePlaceholder()
        }
    }

    public weak var delegate: ApolloSearchBarDelegate?

    private lazy var searchBarContainer = UIView()

    private lazy var magnifyingGlassImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Resources.Icons.icoSearchV2.image
        imageView.tintColor = Layout.Colors.tintColor
        return imageView
    }()

    private lazy var textField: UITextField = {
        let textField = UITextField()
        textField.delegate = self
        textField.tintColor = Layout.Colors.tintColor
        textField.font = Layout.Fonts.textField
        textField.addTarget(self, action: #selector(didChangeText), for: .editingChanged)
        return textField
    }()

    private lazy var clearButton: UIButton = {
        let button = UIButton()
        button.setImage(Resources.Icons.icoCloseV2.image, for: .normal)
        button.addTarget(self, action: #selector(clear), for: .touchUpInside)
        button.tintColor = Layout.Colors.clearButton
        return button
    }()

    private lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = Layout.Fonts.cancelButton
        button.setTitleColor(Layout.Colors.cancelButton, for: .normal)
        button.setTitle(Layout.Strings.cancel, for: .normal)
        button.addTarget(self, action: #selector(cancel), for: .touchUpInside)
        return button
    }()

    private var trailingConstraint: NSLayoutConstraint?

    public init() {
        super.init(frame: .zero)
        buildLayout()
    }

    @available(*, unavailable)
    public required init?(coder: NSCoder) {
        nil
    }

    @objc
    private func didChangeText() {
        clearButton.isHidden = textField.text?.isEmpty ?? true
        delegate?.didChangeText(textField.text)
    }

    @objc
    private func cancel() {
        toggleCancelButtonDisplay(.hide)
        clear()
        delegate?.didCancel()
        textField.resignFirstResponder()
    }

    @objc
    private func clear() {
        clearButton.isHidden = true
        textField.text = .init()
    }

    private func toggleCancelButtonDisplay(_ animation: Layout.Animation) {
        trailingConstraint?.constant = animation.trailingConstant

        UIView.animate(withDuration: animation.duration) {
            self.cancelButton.alpha = animation.alpha
            self.layoutIfNeeded()
        }
    }

    private func updatePlaceholder() {
        let attributes: [NSAttributedString.Key: Any] = [
            .foregroundColor: Layout.Colors.placeholder,
            .font: Layout.Fonts.textField
        ]
        textField.attributedPlaceholder = .init(string: placeholderText, attributes: attributes)
    }
}

extension ApolloSearchBar: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubviews(searchBarContainer, cancelButton)
        searchBarContainer.addSubviews(magnifyingGlassImageView, textField, clearButton)
    }

    public func setupConstraints() {
        searchBarContainer.snp.makeConstraints {
            $0.leading.top.bottom.equalToSuperview()
            $0.height.equalTo(Layout.Sizes.containerHeight)
        }

        trailingConstraint = searchBarContainer.trailingAnchor.constraint(equalTo: trailingAnchor)
        trailingConstraint?.isActive = true

        magnifyingGlassImageView.snp.makeConstraints {
            $0.size.equalTo(Spacing.base02)
            $0.leading.top.bottom.equalToSuperview().inset(Layout.Spacing.magnifyingGlassInset)
        }

        magnifyingGlassImageView.snp.contentHuggingHorizontalPriority = UILayoutPriority.required.rawValue

        textField.snp.makeConstraints {
            $0.height.equalTo(Layout.Sizes.textFieldHeight)
            $0.leading.equalTo(magnifyingGlassImageView.snp.trailing).offset(Layout.Spacing.textFieldLeading)
            $0.trailing.equalTo(clearButton.snp.leading).offset(-Layout.Spacing.textFieldTrailing)
            $0.centerY.equalToSuperview()
        }

        clearButton.snp.makeConstraints {
            $0.size.equalTo(Layout.Sizes.clearButton)
            $0.trailing.top.bottom.equalToSuperview().inset(Spacing.base01)
        }

        clearButton.snp.contentHuggingHorizontalPriority = UILayoutPriority.required.rawValue

        cancelButton.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.width.equalTo(Layout.Sizes.cancelButtonWidth)
            $0.leading.equalTo(searchBarContainer.snp.trailing).offset(Spacing.base02)
        }

        cancelButton.snp.contentHuggingHorizontalPriority = UILayoutPriority.required.rawValue
    }

    public func configureViews() {
        searchBarContainer.backgroundColor = Layout.Colors.container
        searchBarContainer.layer.cornerRadius = Layout.Radius.container
        clearButton.isHidden = true
        cancelButton.alpha = Layout.Animation.hide.alpha
        updatePlaceholder()
    }
}

extension ApolloSearchBar: UITextFieldDelegate {
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let text = textField.text, text.isEmpty else { return }
        toggleCancelButtonDisplay(.show)
        delegate?.didBeginEditing()
    }
}

private extension ApolloSearchBar.Layout {
    enum Fonts {
        static let textField = Typography.bodyPrimary().font()
        static let cancelButton = Typography.bodySecondary(.highlight).font()
    }

    enum Colors {
        static let tintColor = UI.Colors.branding800.color
        static let clearButton = UI.Colors.grayscale950.color
        static let cancelButton = UI.Colors.grayscale750.color
        static let placeholder = UI.Colors.grayscale500.color
        static let container = UI.Colors.grayscale050.color
    }

    enum Sizes {
        static let containerHeight = 40
        static let textFieldHeight = 20
        static let clearButton = 24
        static let cancelButtonWidth = 65
    }

    enum Radius {
        static let container: CGFloat = 12
    }

    enum Spacing {
        static let magnifyingGlassInset = 12
        static let textFieldLeading = 10
        static let textFieldTrailing = 6
    }

    enum Strings {
        static let placeholder = "Pesquisar"
        static let cancel = "Cancelar"
    }

    enum Animation {
        case hide
        case show

        var alpha: CGFloat {
            self == .hide ? 0 : 1
        }

        var trailingConstant: CGFloat {
            self == .hide ? 0 : -78
        }

        var duration: Double {
            0.3
        }
    }
}
