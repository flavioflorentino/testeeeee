import Foundation
import UIKit

fileprivate extension ApolloLoaderAlertView.Layout {
    enum Size {
        static let minContentSize = CGSize(width: Sizing.base09, height: Sizing.base09)
        static let activitySize = CGSize(width: 24, height: 24)
    }
}

final class ApolloLoaderAlertView: UIView {
    fileprivate enum Layout {}
    private let loadingText: String
    
    // MARK: - Private lazy vars
    private lazy var backgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.black.opacity(.medium).lightColor
        return view
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.viewStyle(RoundedViewStyle(cornerRadius: .strong))
            .with(\.backgroundColor, Colors.backgroundPrimary.color)
        return view
    }()
    
    private lazy var activityLoader: UIActivityIndicatorView = {
        let activity = UIActivityIndicatorView()
        activity.hidesWhenStopped = true
        activity.color = Colors.branding600.color
        return activity
    }()
    
    private lazy var loaderLabel: UILabel = {
        let label = UILabel()
        label.text = loadingText
        label.labelStyle(BodyPrimaryLabelStyle())
        label.textColor = Colors.black.color
        label.numberOfLines = 2
        return label
    }()
    
    // MARK: - Life Cicle
    init(with loadingText: String) {
        self.loadingText = loadingText
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) { nil }
}

// MARK: - View configuration functions
extension ApolloLoaderAlertView: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(activityLoader)
        contentView.addSubview(loaderLabel)
        addSubview(backgroundView)
        addSubview(contentView)
    }
    
    func setupConstraints() {
        backgroundView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        contentView.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.size.greaterThanOrEqualTo(Layout.Size.minContentSize)
            $0.width.lessThanOrEqualToSuperview().offset(-Spacing.base03)
        }

        activityLoader.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base03)
            $0.size.equalTo(Layout.Size.activitySize)
        }

        loaderLabel.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalTo(activityLoader.snp.trailing).offset(Spacing.base00)
            $0.trailing.equalToSuperview().offset(-Spacing.base03)
        }
    }
    
    func configureViews() {
        activityLoader.startAnimating()
    }
}
