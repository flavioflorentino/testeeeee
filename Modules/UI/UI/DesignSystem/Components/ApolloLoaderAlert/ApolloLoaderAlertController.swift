import Foundation
import UIKit

public protocol ApolloLoadable: AnyObject {
    func startApolloLoader(loadingText: String)
    func stopApolloLoader(completion: (() -> Void)?)
}

final class ApolloLoaderAlertController: UIViewController {
    private let loadingText: String
    init(with loadingText: String) {
        self.loadingText = loadingText
        super.init(nibName: nil, bundle: nil)
        
        modalPresentationStyle = .overFullScreen
        modalTransitionStyle = .crossDissolve
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) { nil }
    
    override func loadView() {
        self.view = ApolloLoaderAlertView(with: loadingText)
    }
}

public extension ApolloLoadable where Self: UIViewController {
    func startApolloLoader(loadingText: String = "") {
        let controller = ApolloLoaderAlertController(with: loadingText)
        self.present(controller, animated: true)
    }
    
    func stopApolloLoader(completion: (() -> Void)? = nil) {
        guard let viewController = presentingViewController as? ApolloLoaderAlertController else {
            completion?()
            return
        }
        viewController.dismiss(animated: true, completion: completion)
    }
}
