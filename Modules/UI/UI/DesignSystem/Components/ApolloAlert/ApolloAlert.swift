import UIKit
import AssetsKit

public struct ApolloAlertAction {
    var title: String
    var dismissOnCompletion: Bool
    var completion: (() -> Void)
    
    public init(title: String, dismissOnCompletion: Bool = true, completion: @escaping (() -> Void)) {
        self.title = title
        self.dismissOnCompletion = dismissOnCompletion
        self.completion = completion
    }
}

public enum ApolloAlertType {
    case icon(style: Style), sticker(style: Style)
    
    public enum Style {
        case error, warning, info, success
        
        var icon: UIImage {
            switch self {
            case .error:
                return makeImage(from: .exclamationCircle, withColor: Colors.critical900.color)
            case .info:
                return makeImage(from: .infoCircle, withColor: Colors.neutral600.color)
            case .success:
                return makeImage(from: .checkCircle, withColor: Colors.branding600.color)
            case .warning:
                return makeImage(from: .exclamationTriangle, withColor: Colors.warning600.color)
            }
        }
        
        var sticker: UIImage {
            switch self {
            case .error:
                return Resources.Illustrations.iluError.image
            case .info:
                return Resources.Illustrations.iluPersonInfo.image
            case .success:
                return Resources.Illustrations.iluSuccess.image
            case .warning:
                return Resources.Illustrations.iluWarning.image
            }
        }
        
        private func makeImage(from icon: Iconography, withColor color: UIColor) -> UIImage {
            let label = UILabel()
            label.text = icon.rawValue
            label.labelStyle(IconLabelStyle(type: .large))
                .with(\.textAlignment, .center)
                .with(\.textColor, color)
            return label.toImage
        }
    }
    
    fileprivate var image: UIImage {
        switch self {
        case .icon(let style):
            return style.icon
        case .sticker(let style):
            return style.sticker
        }
    }
}

public extension UIViewController {
    func showApolloAlert(type: ApolloAlertType,
                         title: String,
                         subtitle: String,
                         primaryButtonAction: ApolloAlertAction,
                         linkButtonAction: ApolloAlertAction? = nil,
                         dismissCompletion: (() -> Void)? = nil) {
        let viewModel = ApolloAlertPopupViewModel(image: type.image,
                                                  title: title,
                                                  subtitle: subtitle,
                                                  attributedSubtitle: nil,
                                                  primaryButtonAction: primaryButtonAction,
                                                  linkButtonAction: linkButtonAction,
                                                  dismissAction: dismissCompletion,
                                                  additionalContentView: nil)
        self.present(ApolloAlertPopupViewController(viewModel: viewModel), animated: true)
    }

    func showApolloAlert(type: ApolloAlertType,
                         title: String,
                         attributedSubtitle: NSAttributedString,
                         primaryButtonAction: ApolloAlertAction,
                         linkButtonAction: ApolloAlertAction? = nil,
                         dismissCompletion: (() -> Void)? = nil) {
        let viewModel = ApolloAlertPopupViewModel(image: type.image,
                                                  title: title,
                                                  subtitle: nil,
                                                  attributedSubtitle: attributedSubtitle,
                                                  primaryButtonAction: primaryButtonAction,
                                                  linkButtonAction: linkButtonAction,
                                                  dismissAction: dismissCompletion,
                                                  additionalContentView: nil)
        self.present(ApolloAlertPopupViewController(viewModel: viewModel), animated: true)
    }
    
    func showApolloAlert(image: UIImage? = nil,
                         title: String? = nil,
                         subtitle: String? = nil,
                         attributedSubtitle: NSAttributedString? = nil,
                         primaryButtonAction: ApolloAlertAction? = nil,
                         linkButtonAction: ApolloAlertAction? = nil,
                         additionalContentView: UIView? = nil,
                         dismissCompletion: (() -> Void)? = nil,
                         dismissOnTouchOutside: Bool = true) {
        let viewModel = ApolloAlertPopupViewModel(image: image,
                                                  title: title,
                                                  subtitle: subtitle,
                                                  attributedSubtitle: attributedSubtitle,
                                                  primaryButtonAction: primaryButtonAction,
                                                  linkButtonAction: linkButtonAction,
                                                  dismissAction: dismissCompletion,
                                                  additionalContentView: additionalContentView)
        self.present(ApolloAlertPopupViewController(viewModel: viewModel, dismissOnTouchOutside), animated: true)
    }
}
