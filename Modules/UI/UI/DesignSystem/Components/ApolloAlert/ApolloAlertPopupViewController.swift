import UIKit

struct ApolloAlertPopupViewModel {
    let image: UIImage?
    let title, subtitle: String?
    let attributedSubtitle: NSAttributedString?
    let primaryButtonAction, linkButtonAction: ApolloAlertAction?
    let dismissAction: (() -> Void)?
    let additionalContentView: UIView?
}

fileprivate extension ApolloAlertPopupViewController.Layout {
    static let maxWidth = 280
}

final class ApolloAlertPopupViewController: UIViewController {
    fileprivate enum Layout {}
    
    private let viewModel: ApolloAlertPopupViewModel
    private let dismissOnTouchOutside: Bool
    
    private lazy var backgroundView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Colors.black.opacity(.medium).lightColor
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(forceDismiss)))
        return view
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.viewStyle(RoundedViewStyle(cornerRadius: .strong))
            .with(\.backgroundColor, Colors.backgroundSecondary.color)
        return view
    }()
    
    private lazy var stackView: UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .vertical
        stack.distribution = .fillProportionally
        stack.spacing = Spacing.base01
        return stack
    }()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textColor, Colors.grayscale900.color)
            .with(\.textAlignment, .center)
        return label
    }()

    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale600.color)
            .with(\.textAlignment, .center)
        return label
    }()

    private lazy var attributedSubtitleLabel: UITextView = {
        let text = UITextView()
        text.translatesAutoresizingMaskIntoConstraints = false
        text.textAlignment = .center
        text.textColor = Colors.grayscale700.color
        text.isEditable = false
        text.isSelectable = false
        text.isScrollEnabled = false
        text.backgroundColor = .clear
        text.textContainerInset = .zero
        text.textContainer.lineFragmentPadding = 0
        return text
    }()
    
    private lazy var primaryButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didHitPrimaryButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var linkButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(didHitLinkButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var additionalContentView = UIView()
    
    init(viewModel: ApolloAlertPopupViewModel, _ dismissOnTouchOutside: Bool = true) {
        self.viewModel = viewModel
        self.dismissOnTouchOutside = dismissOnTouchOutside

        super.init(nibName: nil, bundle: nil)
        
        self.modalPresentationStyle = .overFullScreen
        self.modalTransitionStyle = .crossDissolve
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildLayout()
    }
}

private extension ApolloAlertPopupViewController {
    func configureImageIfNeeded() {
        guard let image = viewModel.image else { return }
        imageView.image = image
        stackView.addArrangedSubview(imageView)
    }
    
    func configureTitleLabelIfNeeded() {
        guard let title = viewModel.title else { return }
        titleLabel.text = title
        stackView.addArrangedSubview(titleLabel)
    }

    func configureSubtitleLabelIfNeeded() {
        guard let subtitle = viewModel.subtitle else { return }
        subtitleLabel.text = subtitle
        stackView.addArrangedSubview(subtitleLabel)

        if #available(iOS 11.0, *) {
            stackView.setCustomSpacing(Spacing.base02, after: subtitleLabel)
        }
    }

    func configureAttributedSubtitleLabelIfNeeded() {
        guard let subtitle = viewModel.attributedSubtitle else { return }
        attributedSubtitleLabel.attributedText = subtitle
        stackView.addArrangedSubview(attributedSubtitleLabel)
        
        if #available(iOS 11.0, *) {
            stackView.setCustomSpacing(Spacing.base02, after: attributedSubtitleLabel)
        }
    }
    
    func configureAdditionalContentViewIfNeeded() {
        guard let view = viewModel.additionalContentView else { return }
        additionalContentView = view
        stackView.addArrangedSubview(view)
        
        if #available(iOS 11.0, *) {
            stackView.setCustomSpacing(Spacing.base03, after: additionalContentView)
        }
    }
    
    func configurePrimaryButtonTitleIfNeeded() {
        guard let action = viewModel.primaryButtonAction else { return }
        primaryButton.setTitle(action.title, for: .normal)
        stackView.addArrangedSubview(primaryButton)
        
        if #available(iOS 11.0, *) {
            stackView.setCustomSpacing(.zero, after: primaryButton)
        }
    }
    
    func configureLinkButtonTitleIfNeeded() {
        guard let action = viewModel.linkButtonAction else { return }
        linkButton.setTitle(action.title, for: .normal)
        linkButton.buttonStyle(LinkButtonStyle())
        stackView.addArrangedSubview(linkButton)
    }
}

extension ApolloAlertPopupViewController: ViewConfiguration {
    func buildViewHierarchy() {
        view.addSubview(backgroundView)
        view.addSubview(contentView)
        contentView.addSubview(stackView)
    }
    
    func setupConstraints() {
        backgroundView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        contentView.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.width.equalTo(Layout.maxWidth)
        }
        
        stackView.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(Spacing.base03)
        }
    }
    
    func configureViews() {
        configureImageIfNeeded()
        configureTitleLabelIfNeeded()
        configureSubtitleLabelIfNeeded()
        configureAttributedSubtitleLabelIfNeeded()
        configureAdditionalContentViewIfNeeded()
        configurePrimaryButtonTitleIfNeeded()
        configureLinkButtonTitleIfNeeded()
    }
}

@objc
private extension ApolloAlertPopupViewController {
    func didHitPrimaryButton() {
        guard let action = viewModel.primaryButtonAction else { return }
        
        if action.dismissOnCompletion {
            dismiss(animated: true) {
                action.completion()
            }
        } else {
            action.completion()
        }
    }
    
    func didHitLinkButton() {
        guard let action = viewModel.linkButtonAction else { return }
        
        if action.dismissOnCompletion {
            dismiss(animated: true) {
                action.completion()
            }
        } else {
            action.completion()
        }
    }
    
    func forceDismiss() {
        guard dismissOnTouchOutside else { return }
        viewModel.dismissAction?()
        self.dismiss(animated: true)
    }
}
