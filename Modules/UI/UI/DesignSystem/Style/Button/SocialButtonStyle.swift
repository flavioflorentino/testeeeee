import UIKit

public struct SocialButtonStyle: ButtonStyle {
    public typealias View = UIButton
    
    private let iconName: Iconography
    
    public init(iconName: Iconography) {
        self.iconName = iconName
    }
    
    public func makeStyle(_ style: StyleCore<View>) {
        style.with(\.size, CGSize(width: .zero, height: Sizing.Button.small.rawValue))
        style.with(\.cornerRadius, .full)
        style.with(\.textAlignment, .center)
        style.with(\.typography, .bodySecondary())
        style.with(\.textColor, (color: .grayscale400(), state: .normal))
        style.with(\.textColor, (color: .grayscale400(), state: .disabled))
        style.with(\.iconColor, (icon: (name: iconName, alignment: .left), state: (color: .grayscale400(.light), state: .normal)))
        style.with(\.iconColor, (icon: (name: iconName, alignment: .left), state: (color: .grayscale400(.light), state: .highlighted)))
        style.with(\.iconColor, (icon: (name: iconName, alignment: .left), state: (color: .grayscale400(.light), state: .selected)))
        style.with(\.iconColor, (icon: (name: iconName, alignment: .left), state: (color: .grayscale400(), state: .disabled)))
    }
}
