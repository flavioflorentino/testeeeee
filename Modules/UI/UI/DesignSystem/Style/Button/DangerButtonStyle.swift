import UIKit

public struct DangerButtonStyle: ButtonStyle {
    public typealias View = UIButton
    
    private let size: Sizing.Button
    private let icon: (name: Iconography, alignment: IconAlignment)
    
    public init(size: Sizing.Button = .default, icon: (name: Iconography, alignment: IconAlignment)) {
        self.size = size
        self.icon = icon
    }
    
    public func makeStyle(_ style: StyleCore<View>) {
        style.with(\.size, CGSize(width: .zero, height: size.rawValue))
        style.with(\.cornerRadius, .full)
        style.with(\.textAlignment, .center)
        style.with(\.typography, size == .default ? .bodyPrimary() : .bodySecondary())
        style.with(\.backgroundColor, (color: .criticalBase(), state: .normal))
        style.with(\.backgroundColor, (color: .critical400(), state: .highlighted))
        style.with(\.backgroundColor, (color: .critical400(), state: .selected))
        style.with(\.backgroundColor, (color: .grayscale100(), state: .disabled))
        style.with(\.textColor, (color: .white(.light), state: .normal))
        style.with(\.textColor, (color: .grayscale400(), state: .disabled))
        style.with(\.iconColor, (icon: icon, state: (color: .white(.light), state: .normal)))
        style.with(\.iconColor, (icon: icon, state: (color: .white(.light), state: .highlighted)))
        style.with(\.iconColor, (icon: icon, state: (color: .white(.light), state: .selected)))
        style.with(\.iconColor, (icon: icon, state: (color: .grayscale400(), state: .disabled)))
    }
}
