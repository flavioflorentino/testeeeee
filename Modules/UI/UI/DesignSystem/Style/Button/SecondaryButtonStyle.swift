import UIKit

public struct SecondaryButtonStyle: ButtonStyle {
    public typealias View = UIButton
    
    private let size: Sizing.Button
    private let icon: (name: Iconography, alignment: IconAlignment)?
    
    public init(size: Sizing.Button = .default, icon: (name: Iconography, alignment: IconAlignment)? = nil) {
        self.size = size
        self.icon = icon
    }
    
    public func makeStyle(_ style: StyleCore<View>) {
        style.with(\.size, CGSize(width: .zero, height: size.rawValue))
        style.with(\.cornerRadius, .full)
        style.with(\.textAlignment, .center)
        style.with(\.typography, size == .default ? .bodyPrimary() : .bodySecondary())
        style.with(\.borderColor, (color: .brandingBase(), state: .normal))
        style.with(\.borderColor, (color: .branding700(), state: .highlighted))
        style.with(\.borderColor, (color: .branding700(), state: .selected))
        style.with(\.backgroundColor, (color: .grayscale100(), state: .disabled))
        style.with(\.textColor, (color: .brandingBase(), state: .normal))
        style.with(\.textColor, (color: .branding700(), state: .highlighted))
        style.with(\.textColor, (color: .branding700(), state: .selected))
        style.with(\.textColor, (color: .grayscale400(), state: .disabled))
        style.with(\.iconColor, (icon: icon, state: (color: .brandingBase(), state: .normal)))
        style.with(\.iconColor, (icon: icon, state: (color: .branding700(), state: .highlighted)))
        style.with(\.iconColor, (icon: icon, state: (color: .branding700(), state: .selected)))
        style.with(\.iconColor, (icon: icon, state: (color: .grayscale400(), state: .disabled)))
    }
}
