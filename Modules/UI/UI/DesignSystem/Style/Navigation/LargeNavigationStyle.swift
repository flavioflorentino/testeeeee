import UIKit

public final class LargeNavigationStyle: DefaultNavigationStyle {
    override public init() {}
    
    override public func makeStyle(_ style: StyleCore<DefaultNavigationStyle.View>) {
        super.makeStyle(style)
        
        if #available(iOS 11.0, *) {
            style.with(\.prefersLargeTitles, true)
        }
    }
}
