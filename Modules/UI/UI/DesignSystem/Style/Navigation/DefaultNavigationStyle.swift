import UIKit

public class DefaultNavigationStyle: NavigationStyle {
    public typealias View = UINavigationBar
    
    public func makeStyle(_ style: StyleCore<View>) {
        style.with(\.tintColor, Colors.brandingBase.color)
        style.with(\.barTintColor, Colors.backgroundPrimary.color)
        
        if #available(iOS 13.0, *) {
            let defaultBarAppearance = UINavigationBarAppearance()
            defaultBarAppearance.configureWithTransparentBackground()
            defaultBarAppearance.backgroundColor = Colors.backgroundPrimary.color
            
            let standardBarAppearance = UINavigationBarAppearance()
            standardBarAppearance.configureWithDefaultBackground()
            standardBarAppearance.backgroundColor = Colors.groupedBackgroundSecondary.color
            
            style.with(\.standardAppearance, standardBarAppearance)
            style.with(\.compactAppearance, standardBarAppearance)
            style.with(\.scrollEdgeAppearance, defaultBarAppearance)
        } else {
            style.with(\.backgroundColor, Colors.backgroundPrimary.color)
            style.with(\.titleTextAttributes, [.foregroundColor: Colors.black.color])
        }
    }
}
