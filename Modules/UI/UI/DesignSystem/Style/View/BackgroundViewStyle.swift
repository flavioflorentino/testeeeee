import UIKit

public struct BackgroundViewStyle: ViewStyle {
    public typealias View = UIView
    
    private let color: Colors.Style
    
    public init(color: Colors.Style) {
        self.color = color
    }
    
    public func makeStyle(_ style: StyleCore<View>) {
        style.with(\.backgroundColor, color)
    }
}
