import UIKit

public struct RoundedViewStyle: ViewStyle {
    public typealias View = UIView
    
    private let cornerRadius: CornerRadius.Style
    
    public init(cornerRadius: CornerRadius.Style = .medium) {
        self.cornerRadius = cornerRadius
    }
    
    public func makeStyle(_ style: StyleCore<View>) {
        style.with(\.cornerRadius, cornerRadius)
    }
}
