import UIKit

public struct CardViewStyle: ViewStyle {
    public typealias View = UIView
    
    private let cgPath: CGPath?
    
    public init(cgPath: CGPath? = nil) {
        self.cgPath = cgPath
    }
    
    public func makeStyle(_ style: StyleCore<View>) {
        style.with(\.cornerRadius, .medium)
        style.with(\.shadow, .medium(color: .black(.change(.dark, .unknown)), opacity: .ultraLight, cgPath: cgPath))
        style.with(\.backgroundColor, .backgroundTertiary())
        style.with(\.layer.borderWidth, 1.0)
        style.with(\.layer.borderColor, Colors.grayscale050.color.cgColor)
    }
}
