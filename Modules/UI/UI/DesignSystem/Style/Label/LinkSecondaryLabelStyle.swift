import UIKit

public struct LinkSecondaryLabelStyle: LabelStyle {
    public typealias View = UILabel
    
    private let type: Typography.Style.LinkSecondary
    
    public init(type: Typography.Style.LinkSecondary = .default) {
        self.type = type
    }
    
    public func makeStyle(_ style: StyleCore<View>) {
        style.with(\.numberOfLines, 0)
        style.with(\.typography, .linkSecondary(type))
        style.with(\.textColor, .brandingBase())
    }
}
