import UIKit

public struct TitleLabelStyle: LabelStyle {
    public typealias View = UILabel
    
    private let type: Typography.Style.Title
    
    public init(type: Typography.Style.Title) {
        self.type = type
    }
    
    public func makeStyle(_ style: StyleCore<View>) {
        style.with(\.numberOfLines, 0)
        style.with(\.typography, .title(type))
        style.with(\.textColor, .black())
    }
}
