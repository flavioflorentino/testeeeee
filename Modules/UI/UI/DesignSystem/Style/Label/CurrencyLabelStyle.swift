import UIKit

public struct CurrencyLabelStyle: LabelStyle {
    public typealias View = UILabel
    
    private let type: Typography.Style.Currency
    
    public init(type: Typography.Style.Currency) {
        self.type = type
    }
    
    public func makeStyle(_ style: StyleCore<View>) {
        style.with(\.numberOfLines, 0)
        style.with(\.typography, .currency(type))
        style.with(\.textColor, .brandingBase())
    }
}
