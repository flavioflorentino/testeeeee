import UIKit

public struct BodySecondaryLabelStyle: LabelStyle {
    public typealias View = UILabel
    
    private let type: Typography.Style.BodySecondary
    
    public init(type: Typography.Style.BodySecondary = .default) {
        self.type = type
    }
    
    public func makeStyle(_ style: StyleCore<View>) {
        style.with(\.numberOfLines, 0)
        style.with(\.typography, .bodySecondary(type))
        style.with(\.textColor, .black())
    }
}
