import UIKit

public struct IconLabelStyle: LabelStyle {
    public typealias View = UILabel
    
    private let type: Typography.Style.Iconography
    
    public init(type: Typography.Style.Iconography) {
        self.type = type
    }
    
    public func makeStyle(_ style: StyleCore<View>) {
        style.with(\.numberOfLines, 0)
        style.with(\.typography, .icons(type))
        style.with(\.textColor, .black())
    }
}
