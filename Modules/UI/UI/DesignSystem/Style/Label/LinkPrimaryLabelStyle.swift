import UIKit

public struct LinkPrimaryLabelStyle: LabelStyle {
    public typealias View = UILabel
    
    private let type: Typography.Style.LinkPrimary
    
    public init(type: Typography.Style.LinkPrimary = .default) {
        self.type = type
    }
    
    public func makeStyle(_ style: StyleCore<View>) {
        style.with(\.numberOfLines, 0)
        style.with(\.typography, .linkPrimary(type))
        style.with(\.textColor, .brandingBase())
    }
}
