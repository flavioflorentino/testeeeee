import UIKit

public struct CaptionLabelStyle: LabelStyle {
    public typealias View = UILabel
    
    private let type: Typography.Style.Caption
    
    public init(type: Typography.Style.Caption = .default) {
        self.type = type
    }
    
    public func makeStyle(_ style: StyleCore<View>) {
        style.with(\.numberOfLines, 0)
        style.with(\.typography, .caption(type))
        style.with(\.textColor, .grayscale600())
    }
}
