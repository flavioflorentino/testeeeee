import UIKit

public struct BodyPrimaryLabelStyle: LabelStyle {
    public typealias View = UILabel
    
    private let type: Typography.Style.BodyPrimary
    
    public init(type: Typography.Style.BodyPrimary = .default) {
        self.type = type
    }
    
    public func makeStyle(_ style: StyleCore<View>) {
        style.with(\.numberOfLines, 0)
        style.with(\.typography, .bodyPrimary(type))
        style.with(\.textColor, .black())
    }
}
