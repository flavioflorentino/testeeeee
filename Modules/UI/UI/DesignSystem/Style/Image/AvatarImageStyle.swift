import UIKit

public struct AvatarImageStyle: ImageStyle {
    public typealias View = UIImageView
    
    private let size: CGSize
    private let hasBorder: Bool
    
    public init(size: Sizing.ImageView = .medium, hasBorder: Bool = false) {
        self.size = size.rawValue
        self.hasBorder = hasBorder
    }
    
    public func makeStyle(_ style: StyleCore<View>) {
        style.with(\.size, size)
        style.with(\.cornerRadius, .full)
        style.with(\.layer.masksToBounds, true)
        
        if hasBorder {
            style.with(\.border, .light(color: .grayscale100()))
        }
    }
}
