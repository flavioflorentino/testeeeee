import UIKit

public struct RoundedImageStyle: ImageStyle {
    public typealias View = UIImageView
    
    private let size: CGSize
    private let cornerRadius: CornerRadius.Style
    
    public init(size: Sizing.ImageView = .medium, cornerRadius: CornerRadius.Style) {
        self.size = size.rawValue
        self.cornerRadius = cornerRadius
    }
    
    public func makeStyle(_ style: StyleCore<View>) {
        style.with(\.size, size)
        style.with(\.cornerRadius, cornerRadius)
        style.with(\.layer.masksToBounds, true)
    }
}
