import UIKit

public struct BorderedImageStyle: ImageStyle {
    public typealias View = UIImageView
    
    private let size: CGSize
    private let borderColor: Colors.Style
    
    public init(size: Sizing.ImageView = .medium, borderColor: Colors.Style) {
        self.size = size.rawValue
        self.borderColor = borderColor
    }
    
    public func makeStyle(_ style: StyleCore<View>) {
        style.with(\.size, size)
        style.with(\.border, .light(color: borderColor))
        style.with(\.layer.masksToBounds, true)
    }
}
