import CoreGraphics
import Foundation

public struct ShadowSet: ShadowType {
    public let radius: CGFloat
    public let offSet: CGSize
}
