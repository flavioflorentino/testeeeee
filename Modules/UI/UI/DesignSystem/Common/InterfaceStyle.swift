import Foundation

public enum InterfaceStyle {
    case `default`
    case light
    case dark
}
