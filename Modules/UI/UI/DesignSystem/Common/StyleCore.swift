import UIKit

public struct StyleCore<View> {
    private let object: View
    
    internal init (_ object: View) {
        self.object = object
    }
    
    @discardableResult
    public func with<Value>(_ keyPath: WritableKeyPath<View, Value>, _ value: Value) -> Self {
        addKeyPath(keyPath, value: value)
        return self
    }
    
    @discardableResult
    public func with<Value>(_ keyPath: WritableKeyPath<View, Value?>, _ value: Value) -> Self {
        addKeyPath(keyPath, value: value)
        return self
    }
    
    // MARK: - Internal Methods
    func addConfigurations(_ configurations: [(View) -> Void]) {
        configurations.forEach { config in
            config(object)
        }
    }
    
    // MARK: - Private Methods
    private func addKeyPath<Value, Key>(_ keyPath: Key, value: Value) where Key: WritableKeyPath<View, Value> {
        let closure: (View) -> Void = { target in
            var target = target
            target[keyPath: keyPath] = value
        }
        
        closure(object)
    }
}

extension StyleCore where View == LabelStylable {
    @discardableResult
    public func analyzeContrast() -> Self {
        guard
            let label = object as? UILabel,
            let backgroundColor = label.firstBackgroundColor
            else {
                Logger.log(.color, message: "🌈 Contrast calculation is not possible [\(String(describing: type(of: object)))]")
                return self
        }
        
        discoverContrast(
            forTextColor: label.textColor,
            onBackgroundColor: backgroundColor,
            font: label.typography.font()
        )
        
        return self
    }
    
    // MARK: - Private Methods
    private func discoverContrast(forTextColor textColor: UIColor, onBackgroundColor backgroundColor: UIColor, font: UIFont) {
        guard #available(iOS 13.0, *) else {
            return resolveConformanceLevel(style: "🌝", textColor: textColor, backgroundColor: backgroundColor, font: font)
        }
        
        let interfaceStyles: [UIUserInterfaceStyle] = [.light, .dark]
        
        interfaceStyles.forEach {
            let textColorInMode = textColor.resolvedColor(with: .init(userInterfaceStyle: $0))
            let backgroundColorInMode = backgroundColor.resolvedColor(with: .init(userInterfaceStyle: $0))
            
            resolveConformanceLevel(
                style: $0 == .dark ? "🌚" : "🌝",
                textColor: textColorInMode,
                backgroundColor: backgroundColorInMode,
                font: font
            )
        }
    }
    
    private func resolveConformanceLevel(style: String, textColor: UIColor, backgroundColor: UIColor, font: UIFont) {
        guard let contrastRatio = UIColor.contrastRatio(forTextColor: textColor, onBackgroundColor: backgroundColor) else {
            return Logger.log(.color, message: "🌈 Contrast calculation is not possible [\(String(describing: type(of: object)))]")
        }
        
        let conformanceLevel = ConformanceLevel(contrastRatio: contrastRatio, font: font)
        let messageContrastResult = "[\(String(describing: type(of: object)))] [\(String(format: "%.2f", contrastRatio))]"
        
        Logger.log(.color, message: "\(style) \(conformanceLevel.description) \(messageContrastResult)")
    }
}
