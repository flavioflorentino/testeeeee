import UIKit

public typealias DynamicColorType = ColorSet & LightenColorSet & DarkenColorSet & ChangeColorSet

public struct DynamicColor: DynamicColorType {
    public let lightColor: UIColor
    public let darkColor: UIColor
    
    public init(lightColor: UIColor, darkColor: UIColor) {
        self.lightColor = lightColor
        self.darkColor = darkColor
    }
}
