import UIKit

public struct BackgroundDynamicColor: DynamicColorType {
    private let nextColor: DynamicColorType
    
    public let lightColor: UIColor
    public let darkColor: UIColor

    public var color: UIColor {
        dynamicColor(
            defaultColor: lightColor,
            darkColor: darkColor,
            nextColor: nextColor.darkColor
        )
    }
    
    public init(lightColor: UIColor, darkColor: UIColor, nextColor: DynamicColorType) {
        self.lightColor = lightColor
        self.darkColor = darkColor
        self.nextColor = nextColor
    }
    
    // MARK: Private Methods Helpers
    private func dynamicColor(defaultColor: UIColor, darkColor: UIColor, nextColor: UIColor) -> UIColor {
        guard #available(iOS 13.0, *) else {
            return defaultColor
        }
        return UIColor { traitCollection -> UIColor in
            guard traitCollection.userInterfaceLevel == .base else {
                switch traitCollection.userInterfaceStyle {
                case .dark:
                    return nextColor
                default:
                    return defaultColor
                }
            }
            
            switch traitCollection.userInterfaceStyle {
            case .dark:
                return darkColor
            default:
                return defaultColor
            }
        }
    }
}
