import UIKit

/// An enum specifying all WCAG conformance levels.
public enum ConformanceLevel: String {
    /// The minimum level of conformance.
    case A

    /// The medium level of conformance including success criterias of level A.
    case AA

    /// The highest level of conformance including success criterias of level A and AA.
    case AAA

    /// Indicates that no level of conformance has been reached.
    case failed
}

extension ConformanceLevel {
    init(contrastRatio: CGFloat, font: UIFont) {
        let isFontBold = font.fontDescriptor.symbolicTraits.contains(.traitBold)
        let pointSize = font.pointSize
        
        // The original calculate is pointSize> = 18.0, but it was defined that pointSize> = 16.0 will be our default calculate
        let isFontAccessible = pointSize >= 16.0 || (pointSize >= 14.0 && isFontBold)
        
        switch contrastRatio {
        case let value where value >= 4.5 && isFontAccessible || value >= 7.0:
            self = .AAA
        case let value where value >= 3.0 && isFontAccessible || value >= 4.5:
            self = .AA
        default:
            self = .failed
        }
    }
}

extension ConformanceLevel: CustomStringConvertible {
    public var description: String {
        switch self {
        case .failed:
            return "There isn't a good contrast level 🆘"
        case .A:
            return "There's the minimum contrast level [\(rawValue)] ✅"
        case .AA:
            return "There's a good contrast level [\(rawValue)] ✅"
        case .AAA:
            return "There's a wonderful contrast level [\(rawValue)] ✅"
        }
    }
}
