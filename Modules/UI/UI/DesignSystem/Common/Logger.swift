import os.log

enum Logger {
    private static var `default` = OSLog(subsystem: "com.picpay.ui", category: "UI")

    enum CategoryType: String {
        case color
        
        fileprivate var log: OSLog {
            OSLog(subsystem: "com.picpay.ui", category: self.rawValue.capitalized)
        }
    }
    
    static func log(_ type: CategoryType, message: String) {
        #if DEBUG
        os_log("%@", log: type.log, type: .debug, message)
        #endif
    }
}
