import UIKit

extension UIFont {
    private final class BundleToken { }
    
    private enum FontRegisterError: Error {
        case registerFailed
    }
    
    public static func register(_ fileName: String, ofType: String? = nil) throws {
        let bundle = Bundle(for: BundleToken.self)
        var errorReference: Unmanaged<CFError>?
        
        guard
            let resourceUrl = bundle.path(forResource: fileName, ofType: ofType),
            let data = NSData(contentsOfFile: resourceUrl),
            let provider = CGDataProvider(data: data),
            let fontReference = CGFont(provider),
            CTFontManagerRegisterGraphicsFont(fontReference, &errorReference)
        else {
            throw FontRegisterError.registerFailed
        }
    }
}
