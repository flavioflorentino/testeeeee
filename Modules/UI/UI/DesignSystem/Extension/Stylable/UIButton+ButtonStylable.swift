import UIKit

/// The protocol necessary for configure UIButton
public protocol ButtonStylable: HasBorder & HasCornerRadius & HasTypography & HasTextAlignment & HasStateColor & HasSize & HasAttributedColor { }

public extension ButtonStylable where Self: UIButton {
    var typography: Typography {
        get {
            Typography.discover(titleLabel?.font)
        }
        set {
            titleLabel?.font = newValue.font()

            titleLabel?.adjustsFontSizeToFitWidth = true
            titleLabel?.baselineAdjustment = .alignCenters
            titleLabel?.textAlignment = .center
            titleLabel?.lineBreakMode = .byClipping
            
            titleLabel?.adjustsFontForContentSizeCategory = true
        }
    }
    
    var textAlignment: NSTextAlignment {
        get {
            titleLabel?.textAlignment ?? .natural
        }
        set {
            titleLabel?.textAlignment = newValue
        }
    }
    
    var textColor: (color: Colors.Style?, state: UIControl.State) {
        get {
            (color: Colors.Style(rawValue: titleColor(for: state) ?? .clear), state: state)
        }
        set {
            setTitleColor(newValue.color?.rawValue, for: newValue.state)
        }
    }
    
    var backgroundColor: (color: Colors.Style?, state: UIControl.State) {
        get {
            (color: nil, state: state)
        }
        set {
            setBackgroundColor(newValue.color?.rawValue ?? .clear, for: newValue.state)
        }
    }
    
    var borderColor: (color: Colors.Style?, state: UIControl.State) {
        get {
            (color: nil, state: state)
        }
        set {
            setBorderColor(newValue.color?.rawValue ?? .clear, for: newValue.state)
        }
    }
  
    var iconColor: (icon: (name: Iconography, alignment: IconAlignment)?, state: (color: Colors.Style?, state: UIControl.State)) {
        get {
            (icon: nil, state: (color: nil, state: state))
        }
        set {
            guard let iconography = newValue.icon else {
                return
            }
            
            imageEdgeInsets = iconography.alignment.edgeInsets
            semanticContentAttribute = iconography.alignment.semanticContentAttribute
            
            setIconImage(
                iconography.name,
                color: newValue.state.color?.rawValue,
                for: newValue.state.state,
                edgeInsets: UIEdgeInsets(top: Spacing.base00, left: .zero, bottom: .zero, right: .zero)
            )
        }
    }
    
    var textAttributedColor: (color: Colors.Style?, state: UIControl.State) {
        get {
            (color: nil, state: state)
        }
        set {
            let currentColor = newValue.color?.rawValue ?? .clear
            
            let attributedString = NSMutableAttributedString(
                string: title(for: newValue.state) ?? String(),
                attributes: [
                    .font: typography.font(),
                    .foregroundColor: currentColor,
                    .underlineStyle: NSUnderlineStyle.single.rawValue,
                    .underlineColor: currentColor
                ]
            )
            
            setAttributedTitle(attributedString, for: newValue.state)
        }
    }
}

extension UIButton: ButtonStylable { }

public enum IconAlignment {
    case left
    case right
    
    var edgeInsets: UIEdgeInsets {
        switch self {
        case .left:
            return UIEdgeInsets(top: .zero, left: -Spacing.base00, bottom: .zero, right: .zero)
        case .right:
            return UIEdgeInsets(top: .zero, left: .zero, bottom: .zero, right: -Spacing.base00)
        }
    }
    
    var semanticContentAttribute: UISemanticContentAttribute {
        switch self {
        case .left:
            return .forceLeftToRight
        case .right:
            return .forceRightToLeft
        }
    }
}
