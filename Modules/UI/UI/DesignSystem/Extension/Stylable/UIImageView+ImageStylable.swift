import UIKit

public protocol ImageStylable: HasBorder & HasCornerRadius & HasSize & HasBackgroundColor { }

extension ImageStylable where Self: UIImageView { }

extension UIImageView: ImageStylable { }
