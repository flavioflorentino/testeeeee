import UIKit

/// The protocol necessary for configure UIView
public protocol ViewStylable: HasBorder & HasCornerRadius & HasBackgroundColor & HasSize & HasShadow & HasOpacity { }

public extension ViewStylable where Self: UIView {
    var border: Border.Style {
        get {
            Border.Style(
                rawValue: (
                    width: layer.borderWidth,
                    color: UIColor(cgColor: layer.borderColor ?? UIColor.clear.cgColor)
                )
            )
        }
        set {
            layer.borderWidth = newValue.rawValue.width
            layer.borderColor = newValue.rawValue.color.cgColor
        }
    }
    
    var cornerRadius: CornerRadius.Style {
        get {
            guard layer.cornerRadius == (size.height / 2) else {
                return CornerRadius.Style(rawValue: layer.cornerRadius)
            }
            
            return CornerRadius.Style.full
        }
        set {
            switch newValue {
            case .none, .light, .medium, .strong:
                layer.cornerRadius = newValue.rawValue
            case .full:
                layer.cornerRadius = size.height / 2
            }
        }
    }
    
    var backgroundColor: Colors.Style {
        get {
            Colors.Style(rawValue: backgroundColor ?? .clear)
        }
        set {
            backgroundColor = newValue.rawValue
        }
    }
    
    var size: CGSize {
        get {
            frame.size
        }
        set {
            let oldConstraints = constraints.filter { $0.firstAttribute == .height || $0.firstAttribute == .width }
            removeConstraints(oldConstraints)
            
            if !newValue.height.isZero {
                heightAnchor.constraint(equalToConstant: newValue.height).isActive = true
            }
            
            if !newValue.width.isZero {
                widthAnchor.constraint(equalToConstant: newValue.width).isActive = true
            }
            
            frame.size = newValue
        }
    }
    
    var shadow: Shadow.Style {
        get {
            Shadow.Style(
                rawValue: (
                    shadow: (
                        type: ShadowSet(radius: layer.shadowRadius, offSet: layer.shadowOffset),
                        color: Colors.Style(rawValue: UIColor(cgColor: layer.shadowColor ?? UIColor.clear.cgColor))
                    ),
                    config: (opacity: Opacity.Style(rawValue: CGFloat(layer.shadowOpacity)), cgPath: layer.shadowPath)))
        }
        set {
            layer.setShadow(
                radius: newValue.rawValue.shadow.type.radius,
                offSet: newValue.rawValue.shadow.type.offSet,
                color: newValue.rawValue.shadow.color.rawValue,
                opacity: Float(newValue.rawValue.config.opacity.rawValue),
                cgPath: newValue.rawValue.config.cgPath)
        }
    }
    
    var opacity: Opacity.Style {
        get {
            Opacity.Style(rawValue: alpha)
        }
        set {
            alpha = newValue.rawValue
        }
    }
}

extension UIView: ViewStylable { }
