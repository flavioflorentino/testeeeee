import UIKit

/// The protocol necessary for configure UILabel
public protocol LabelStylable: HasBorder & HasCornerRadius & HasTextColor & HasTypography & HasTextAlignment & HasBackgroundColor { }

public extension LabelStylable where Self: UILabel {
    var textColor: Colors.Style {
        get {
            Colors.Style(rawValue: textColor ?? .clear)
        }
        set {
            textColor = newValue.rawValue
        }
    }
    
    var typography: Typography {
        get {
            Typography.discover(font)
        }
        set {
            font = newValue.font()
            adjustsFontForContentSizeCategory = true
        }
    }
    
    var textAlignment: NSTextAlignment {
        get {
            textAlignment
        }
        set {
            textAlignment = newValue
        }
    }
}

extension UILabel: LabelStylable { }
