import UIKit

public protocol NavigationStylable { }

extension NavigationStylable where Self: UINavigationBar { }

extension UINavigationBar: NavigationStylable { }
