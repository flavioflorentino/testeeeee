import UIKit
import CoreGraphics

public extension UIImage {
    convenience init?(
        iconName: String,
        font: UIFont = Typography.icons(.small).font(),
        color: UIColor?,
        size: CGSize = CGSize(width: CGFloat.infinity, height: CGFloat.infinity),
        edgeInsets: UIEdgeInsets = .zero
    ) {
        var rect = CGRect(origin: .zero, size: size)
        rect.origin.y = edgeInsets.top
        rect.size.width -= edgeInsets.left + edgeInsets.right
        rect.size.height -= edgeInsets.top + edgeInsets.bottom
        
        let renderer = UIGraphicsImageRenderer(size: size)
        
        let data = renderer.pngData { context in
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = .center
            
            let attributes: [NSAttributedString.Key: Any] = [
                .font: font,
                .foregroundColor: color ?? Colors.black.color,
                .paragraphStyle: paragraphStyle
            ]
            
            iconName.draw(
                with: rect,
                options: .usesLineFragmentOrigin,
                attributes: attributes,
                context: nil
            )
        }
        
        self.init(data: data, scale: UIScreen.main.scale)
    }
}
