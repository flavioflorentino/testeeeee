import UIKit
import Foundation
import QuartzCore

public extension CALayer {
    func setShadow(radius: CGFloat, offSet: CGSize, color: UIColor, opacity: Float, cgPath: CGPath? = nil) {
        shadowOffset = offSet
        shadowRadius = radius
        shadowColor = color.cgColor
        shadowOpacity = opacity
        shadowPath = cgPath
        
        if cgPath?.isEmpty == true {
            shouldRasterize = true
            rasterizationScale = UIScreen.main.scale
        }
    }
}
