import UIKit

extension UIImage {
    convenience init?(fill: UIColor, strokeColor: UIColor? = nil, lineWidth: CGFloat = 0.0, cornerRadius: CGFloat = 0.0) {
        let bezierPath = UIBezierPath(lineWidth: lineWidth, cornerRadius: cornerRadius)
        let renderer = UIGraphicsImageRenderer(size: bezierPath.bounds.size)

        let data = renderer.pngData { context in
            fill.setFill()
            context.cgContext.fillEllipse(in: bezierPath.bounds)

            if let strokeColor = strokeColor, lineWidth > .zero {
                let strokeRect = bezierPath.bounds
                    .insetBy(dx: lineWidth * 0.5, dy: lineWidth * 0.5)
                strokeColor.setStroke()
                context.cgContext.setLineWidth(lineWidth)
                context.cgContext.strokeEllipse(in: strokeRect)
            }
        }
        
        self.init(data: data, scale: UIScreen.main.scale)
    }
}
