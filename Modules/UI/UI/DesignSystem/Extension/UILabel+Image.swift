import Foundation

extension UILabel {
    var toImage: UIImage {
        self.sizeToFit()
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, 0)
        defer { UIGraphicsEndImageContext() }
        guard let graficsCurrenctContext = UIGraphicsGetCurrentContext() else { return UIImage() }
        self.layer.render(in: graficsCurrenctContext)
        return UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
    }
}
