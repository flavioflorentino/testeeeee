import UIKit

extension UIButton {
    public func setBackgroundColor(_ color: UIColor, for state: UIControl.State) {
        guard let image = UIImage(fill: color, cornerRadius: layer.cornerRadius) else {
            return
        }
        
        setBackgroundImageResizable(image, color: color, state: state)
    }
    
    public func setBorderColor(_ color: UIColor, for state: UIControl.State) {
        guard let image = UIImage(fill: .clear, strokeColor: color, lineWidth: 2.0, cornerRadius: layer.cornerRadius) else {
            return
        }
        
        setBackgroundImageResizable(image, color: color, state: state)
    }
    
    public func setIconImage(_ icon: Iconography, color: UIColor?, for state: UIControl.State, edgeInsets: UIEdgeInsets = .zero) {
        let image = UIImage(iconName: icon.rawValue, color: color, size: CGSize(width: 24.0, height: 24.0), edgeInsets: edgeInsets)
        setImage(image, for: state)
    }
    
    private func setBackgroundImageResizable(_ image: UIImage, color: UIColor, state: UIControl.State) {
        let resizableImage = image.resizableImage(
            withCapInsets: UIEdgeInsets(
                top: layer.cornerRadius,
                left: layer.cornerRadius,
                bottom: layer.cornerRadius,
                right: layer.cornerRadius
            ),
            resizingMode: .stretch
        )
        
        guard #available(iOS 13.0, *) else {
            return setBackgroundImage(resizableImage, for: state)
        }
        
        setBackgroundImage(resizableImage.withTintColor(color), for: state)
    }
}
