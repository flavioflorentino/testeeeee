import UIKit
import SnapKit

public extension UIButton {
    func startLoading() {
        guard !subviews.contains(where: { $0 is UIActivityIndicatorView }),
              let titleLabel = titleLabel else {
            return
        }
        isEnabled = false

        let activityIndicatorView = UIActivityIndicatorView(style: .white)
        activityIndicatorView.color = Colors.branding600.color

        addSubview(activityIndicatorView)

        activityIndicatorView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.trailing.equalTo(titleLabel.snp.leading).offset(-Spacing.base01)
        }

        layoutIfNeeded()
        activityIndicatorView.startAnimating()
    }

    func stopLoading() {
        guard let activityIndicatorView = subviews.first(where: { $0 is UIActivityIndicatorView }) as? UIActivityIndicatorView else {
            return
        }
        isEnabled = true

        activityIndicatorView.stopAnimating()
        activityIndicatorView.removeFromSuperview()
    }
}
