import UIKit

extension UIView {
    var firstBackgroundColor: UIColor? {
        var currentView: UIView? = self
        
        while currentView != nil, currentView?.backgroundColor == nil || currentView?.backgroundColor == .clear {
            currentView = currentView?.superview
        }
        
        return currentView?.backgroundColor
    }
}
