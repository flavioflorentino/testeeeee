import UIKit

public extension Colors {
    indirect enum ColorType {
        case `default`
        case dark
        case light
        case change(InterfaceStyle, Style)
    }
    
    enum Style {
        case unknown
        
        // Style Branding Color
        case branding900(_ type: ColorType = .default)
        case branding800(_ type: ColorType = .default)
        case branding700(_ type: ColorType = .default)
        case branding600(_ type: ColorType = .default)
        case branding500(_ type: ColorType = .default)
        case branding400(_ type: ColorType = .default)
        case branding300(_ type: ColorType = .default)
        case branding200(_ type: ColorType = .default)
        case branding100(_ type: ColorType = .default)
        case branding050(_ type: ColorType = .default)
        case brandingBase(_ type: ColorType = .default)
        
        // Style University Color
        case university800(_ type: ColorType = .default)
        case university600(_ type: ColorType = .default)
        case university200(_ type: ColorType = .default)
        case university100(_ type: ColorType = .default)
        case university050(_ type: ColorType = .default)
        case universityBase(_ type: ColorType = .default)
        
        // Style Business Color
        case business900(_ type: ColorType = .default)
        case business800(_ type: ColorType = .default)
        case business700(_ type: ColorType = .default)
        case business600(_ type: ColorType = .default)
        case business500(_ type: ColorType = .default)
        case business400(_ type: ColorType = .default)
        case business300(_ type: ColorType = .default)
        case business200(_ type: ColorType = .default)
        case business100(_ type: ColorType = .default)
        case business050(_ type: ColorType = .default)
        case businessBase(_ type: ColorType = .default)
        
        // Style Grayscale Color
        case black(_ type: ColorType = .default)
        case grayscale950(_ type: ColorType = .default)
        case grayscale900(_ type: ColorType = .default)
        case grayscale850(_ type: ColorType = .default)
        case grayscale800(_ type: ColorType = .default)
        case grayscale750(_ type: ColorType = .default)
        case grayscale700(_ type: ColorType = .default)
        case grayscale600(_ type: ColorType = .default)
        case grayscale500(_ type: ColorType = .default)
        case grayscale400(_ type: ColorType = .default)
        case grayscale300(_ type: ColorType = .default)
        case grayscale200(_ type: ColorType = .default)
        case grayscale100(_ type: ColorType = .default)
        case grayscale050(_ type: ColorType = .default)
        case white(_ type: ColorType = .default)
        
        // Style Warning Color
        case warning900(_ type: ColorType = .default)
        case warning600(_ type: ColorType = .default)
        case warning400(_ type: ColorType = .default)
        case warning200(_ type: ColorType = .default)
        case warning050(_ type: ColorType = .default)
        case warningBase(_ type: ColorType = .default)
        
        // Style Critical Color
        case critical900(_ type: ColorType = .default)
        case critical600(_ type: ColorType = .default)
        case critical400(_ type: ColorType = .default)
        case critical200(_ type: ColorType = .default)
        case critical050(_ type: ColorType = .default)
        case criticalBase(_ type: ColorType = .default)
        
        // Style Success Color
        case success900(_ type: ColorType = .default)
        case success600(_ type: ColorType = .default)
        case success500(_ type: ColorType = .default)
        case success400(_ type: ColorType = .default)
        case success050(_ type: ColorType = .default)
        case successBase(_ type: ColorType = .default)
        
        // Style Neutral Color
        case neutral800(_ type: ColorType = .default)
        case neutral600(_ type: ColorType = .default)
        case neutral400(_ type: ColorType = .default)
        case neutral200(_ type: ColorType = .default)
        case neutral050(_ type: ColorType = .default)
        case neutralBase(_ type: ColorType = .default)
        
        // Style Notification Color
        case notification600(_ type: ColorType = .default)
        
        // Style Background Color
        case backgroundPrimary(_ type: ColorType = .default)
        case backgroundSecondary(_ type: ColorType = .default)
        case backgroundTertiary(_ type: ColorType = .default)
        
        // Style Grouped Background Color
        case groupedBackgroundPrimary(_ type: ColorType = .default)
        case groupedBackgroundSecondary(_ type: ColorType = .default)
        case groupedBackgroundTertiary(_ type: ColorType = .default)
        
        // Style External Color
        case externalFacebook(_ type: ColorType = .default)
        case externalTwitter(_ type: ColorType = .default)
        case externalWhatsApp(_ type: ColorType = .default)
        case external24HourBank(_ type: ColorType = .default)
        
        // Style Additional Color
        case additional001(_ type: ColorType = .default)
        case additional002(_ type: ColorType = .default)
    }
}

extension Colors.Style: RawRepresentable {
    public var rawValue: UIColor {
        switch self {
        case .unknown:
            return .clear
            
        case .branding050(let colorType):
            return discoverColor(colorType, color: Colors.branding050)
        case .branding100(let colorType):
            return discoverColor(colorType, color: Colors.branding100)
        case .branding200(let colorType):
            return discoverColor(colorType, color: Colors.branding200)
        case .branding300(let colorType):
            return discoverColor(colorType, color: Colors.branding300)
        case .branding400(let colorType):
            return discoverColor(colorType, color: Colors.branding400)
        case .branding500(let colorType):
            return discoverColor(colorType, color: Colors.branding500)
        case .branding600(let colorType):
            return discoverColor(colorType, color: Colors.branding600)
        case .branding700(let colorType):
            return discoverColor(colorType, color: Colors.branding700)
        case .branding800(let colorType):
            return discoverColor(colorType, color: Colors.branding800)
        case .branding900(let colorType):
            return discoverColor(colorType, color: Colors.branding900)
        case .brandingBase(let colorType):
            return discoverColor(colorType, color: Colors.brandingBase)
            
        case .university800(let colorType):
            return discoverColor(colorType, color: Colors.university800)
        case .university600(let colorType):
            return discoverColor(colorType, color: Colors.university600)
        case .university200(let colorType):
            return discoverColor(colorType, color: Colors.university200)
        case .university100(let colorType):
            return discoverColor(colorType, color: Colors.university100)
        case .university050(let colorType):
            return discoverColor(colorType, color: Colors.university050)
        case .universityBase(let colorType):
            return discoverColor(colorType, color: Colors.universityBase)
            
        case .business900(let colorType):
            return discoverColor(colorType, color: Colors.business900)
        case .business800(let colorType):
            return discoverColor(colorType, color: Colors.business800)
        case .business700(let colorType):
            return discoverColor(colorType, color: Colors.business700)
        case .business600(let colorType):
            return discoverColor(colorType, color: Colors.business600)
        case .business500(let colorType):
            return discoverColor(colorType, color: Colors.business500)
        case .business400(let colorType):
            return discoverColor(colorType, color: Colors.business400)
        case .business300(let colorType):
            return discoverColor(colorType, color: Colors.business300)
        case .business200(let colorType):
            return discoverColor(colorType, color: Colors.business200)
        case .business100(let colorType):
            return discoverColor(colorType, color: Colors.business100)
        case .business050(let colorType):
            return discoverColor(colorType, color: Colors.business050)
        case .businessBase(let colorType):
            return discoverColor(colorType, color: Colors.businessBase)
            
        case .white(let colorType):
            return discoverColor(colorType, color: Colors.white)
        case .grayscale050(let colorType):
            return discoverColor(colorType, color: Colors.grayscale050)
        case .grayscale100(let colorType):
            return discoverColor(colorType, color: Colors.grayscale100)
        case .grayscale200(let colorType):
            return discoverColor(colorType, color: Colors.grayscale200)
        case .grayscale300(let colorType):
            return discoverColor(colorType, color: Colors.grayscale300)
        case .grayscale400(let colorType):
            return discoverColor(colorType, color: Colors.grayscale400)
        case .grayscale500(let colorType):
            return discoverColor(colorType, color: Colors.grayscale500)
        case .grayscale600(let colorType):
            return discoverColor(colorType, color: Colors.grayscale600)
        case .grayscale700(let colorType):
            return discoverColor(colorType, color: Colors.grayscale700)
        case .grayscale750(let colorType):
            return discoverColor(colorType, color: Colors.grayscale750)
        case .grayscale800(let colorType):
            return discoverColor(colorType, color: Colors.grayscale800)
        case .grayscale850(let colorType):
            return discoverColor(colorType, color: Colors.grayscale850)
        case .grayscale900(let colorType):
            return discoverColor(colorType, color: Colors.grayscale900)
        case .grayscale950(let colorType):
            return discoverColor(colorType, color: Colors.grayscale950)
        case .black(let colorType):
            return discoverColor(colorType, color: Colors.black)
            
        case .warning900(let colorType):
            return discoverColor(colorType, color: Colors.warning900)
        case .warning600(let colorType):
            return discoverColor(colorType, color: Colors.warning600)
        case .warning400(let colorType):
            return discoverColor(colorType, color: Colors.warning400)
        case .warning200(let colorType):
            return discoverColor(colorType, color: Colors.warning200)
        case .warning050(let colorType):
            return discoverColor(colorType, color: Colors.warning050)
        case .warningBase(let colorType):
            return discoverColor(colorType, color: Colors.warningBase)
            
        case .critical900(let colorType):
            return discoverColor(colorType, color: Colors.critical900)
        case .critical600(let colorType):
            return discoverColor(colorType, color: Colors.critical600)
        case .critical400(let colorType):
            return discoverColor(colorType, color: Colors.critical400)
        case .critical200(let colorType):
            return discoverColor(colorType, color: Colors.critical200)
        case .critical050(let colorType):
            return discoverColor(colorType, color: Colors.critical050)
        case .criticalBase(let colorType):
            return discoverColor(colorType, color: Colors.criticalBase)
            
        case .success900(let colorType):
            return discoverColor(colorType, color: Colors.success900)
        case .success600(let colorType):
            return discoverColor(colorType, color: Colors.success600)
        case .success500(let colorType):
            return discoverColor(colorType, color: Colors.success500)
        case .success400(let colorType):
            return discoverColor(colorType, color: Colors.success400)
        case .success050(let colorType):
            return discoverColor(colorType, color: Colors.success050)
        case .successBase(let colorType):
            return discoverColor(colorType, color: Colors.successBase)
            
        case .neutral800(let colorType):
            return discoverColor(colorType, color: Colors.neutral800)
        case .neutral600(let colorType):
            return discoverColor(colorType, color: Colors.neutral600)
        case .neutral400(let colorType):
            return discoverColor(colorType, color: Colors.neutral400)
        case .neutral200(let colorType):
            return discoverColor(colorType, color: Colors.neutral200)
        case .neutral050(let colorType):
            return discoverColor(colorType, color: Colors.neutral050)
        case .neutralBase(let colorType):
            return discoverColor(colorType, color: Colors.neutralBase)
            
        case .notification600(let colorType):
            return discoverColor(colorType, color: Colors.notification600)
            
        case .backgroundPrimary(let colorType):
            return discoverColor(colorType, color: Colors.backgroundPrimary)
        case .backgroundSecondary(let colorType):
            return discoverColor(colorType, color: Colors.backgroundSecondary)
        case .backgroundTertiary(let colorType):
            return discoverColor(colorType, color: Colors.backgroundTertiary)
        
        case .groupedBackgroundPrimary(let colorType):
            return discoverColor(colorType, color: Colors.groupedBackgroundPrimary)
        case .groupedBackgroundSecondary(let colorType):
            return discoverColor(colorType, color: Colors.groupedBackgroundSecondary)
        case .groupedBackgroundTertiary(let colorType):
            return discoverColor(colorType, color: Colors.groupedBackgroundTertiary)
            
        case .externalFacebook(let colorType):
            return discoverColor(colorType, color: Colors.externalFacebook)
        case .externalTwitter(let colorType):
            return discoverColor(colorType, color: Colors.externalTwitter)
        case .externalWhatsApp(let colorType):
            return discoverColor(colorType, color: Colors.externalWhatsApp)
        case .external24HourBank(let colorType):
            return discoverColor(colorType, color: Colors.external24HourBank)
            
        case .additional001(let colorType):
            return discoverColor(colorType, color: Colors.additional001)
        case .additional002(let colorType):
            return discoverColor(colorType, color: Colors.additional002)
        }
    }
    
    public init(rawValue: UIColor) {
        self = .unknown
    }
    
    private func discoverColor(_ type: Colors.ColorType, color: DynamicColorType) -> UIColor {
        switch type {
        case .default:
            return color.color
        case .light:
            return color.lightColor
        case .dark:
            return color.darkColor
        case let .change(style, newColor):
            return color.change(style, to: newColor.rawValue).color
        }
    }
}

extension Colors.Style: Equatable {
    public static func == (lhs: Colors.Style, rhs: Colors.Style) -> Bool {
        switch (lhs, rhs) {
        case (.backgroundPrimary, .backgroundPrimary),
             (.backgroundSecondary, .backgroundSecondary),
             (.backgroundTertiary, .backgroundTertiary):
            return true
        default:
            return false
        }
    }
}
