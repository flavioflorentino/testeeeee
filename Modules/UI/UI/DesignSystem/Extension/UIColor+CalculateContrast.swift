import UIKit

extension UIColor {
    static func contrastRatio(forTextColor textColor: UIColor, onBackgroundColor backgroundColor: UIColor) -> CGFloat? {
        guard
            let textColorRelativeLuminance = textColor.relativeLuminance,
            let backgroundColorRelativeLuminance = backgroundColor.relativeLuminance
        else {
            return nil
        }
        
        let relativeLuminanceLighter = (max(textColorRelativeLuminance, backgroundColorRelativeLuminance) + 0.05)
        let relativeLuminanceDarker = (min(textColorRelativeLuminance, backgroundColorRelativeLuminance) + 0.05)
        
        return relativeLuminanceLighter / relativeLuminanceDarker
    }
    
    private var relativeLuminance: CGFloat? {
        var red: CGFloat = 0, green: CGFloat = 0, blue: CGFloat = 0, white: CGFloat = 0, alpha: CGFloat = 0

        if getRed(&red, green: &green, blue: &blue, alpha: &alpha) {
            return relativeLuminance(red: normalize(red), green: normalize(green), blue: normalize(blue), alpha: alpha)
        } else if getWhite(&white, alpha: &alpha) {
            let white = normalize(white)
            return relativeLuminance(red: white, green: white, blue: white, alpha: alpha)
        } else {
            return nil
        }
    }
    
    private func normalize(_ component: CGFloat) -> CGFloat {
        let normalizedValue = component * 255.0
        return max(0, min(normalizedValue, 255.0))
    }
    
    // https://www.w3.org/TR/WCAG20-TECHS/G17.html
    private func relativeLuminance(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) -> CGFloat {
        let adjustedRed = ajustedColorComponent(red)
        let adjustedGreen = ajustedColorComponent(green)
        let adjustedBlue = ajustedColorComponent(blue)

        return (0.212_6 * adjustedRed) + (0.715_2 * adjustedGreen) + (0.072_2 * adjustedBlue)
    }
    
    private func ajustedColorComponent(_ component: CGFloat) -> CGFloat {
        let ajustedComponet = component / 255.0
        
        guard ajustedComponet <= 0.039_28 else {
            return pow((ajustedComponet + 0.055) / 1.055, 2.4)
        }
        
        return ajustedComponet / 12.92
    }
}
