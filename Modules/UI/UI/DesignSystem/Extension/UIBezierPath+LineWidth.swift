import UIKit

extension UIBezierPath {
    convenience init(lineWidth: CGFloat, cornerRadius: CGFloat) {
        guard cornerRadius > .zero else {
            let size = CGSize(width: 1, height: 1)
            let rect = CGRect(origin: .zero, size: size)
            self.init(rect: rect)
            return
        }
        
        let size = CGSize(width: cornerRadius * 2, height: cornerRadius * 2)
        let rect = CGRect(origin: .zero, size: size)
        
        self.init(roundedRect: rect, cornerRadius: cornerRadius)
    }
}
