import UIKit

public extension UINavigationController {
    enum NavigationBarStyleType {
        case large
        case standard
        case none
    }
    
    var navigationBarStyle: NavigationBarStyleType {
        get { .none }
        
        set {
            switch newValue {
            case .standard:
                navigationBar.navigationStyle(StandardNavigationStyle())
            case .large:
                navigationBar.navigationStyle(LargeNavigationStyle())
            default:
                navigationBar.navigationStyle(DefaultNavigationStyle())
            }
        }
    }
}
