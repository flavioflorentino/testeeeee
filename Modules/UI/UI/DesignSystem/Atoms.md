<p align="center">
  <img width="80" height="80" src="https://i.imgur.com/MMUbJuh.png">
</p>
<h3 align="center"><b>Design System</b></h3>
<h4 align="center">Átomos / Tokens</h4>
<p align="center">
    O Design System fantástico e extraordinário do PicPay 💚
    <br/>
    <a href="">App Demo</a> ·
    <a href="">Report Bug</a> ·
    <a href="">Request Feature</a>
</p>
<p align="center">
    <img src="https://img.shields.io/badge/Swift-5.0-brightgreen.svg" alt="Swift 5.0"/>
    <img src="https://img.shields.io/badge/platform-iOS-brightgreen.svg" alt="Platform: iOS"/>
    <img src="https://img.shields.io/badge/Xcode-11%2B-brightgreen.svg" alt="Xcode 11+"/>
    <img src="https://img.shields.io/badge/iOS-10.3%2B-brightgreen.svg" alt="iOS 10.3+"/>
</p>


## Conteúdo do Design System

* [Sobre o Design System Atômico](#sobre-o-design-system-atômico)
    * [O que é um átomo?](#o-que-é-um-átomo) 
    * [Apollo - App Sample](#apollo-app-sample)
* [Átomos](#átomos)
    * [Aplicação dos átomos nos componentes](#aplicação-dos-átomos-nos-componentes)
    * [Átomos disponíveis](#)    
        * [Border](#border)
        * [Corner Radius](#corner-radius)
        * [Spacing](#spacing)
* [Contribuição](#contribuição)

## Sobre o Design System Atômico

###  O que é um átomo? 

A palavra átomo quer dizer “aquilo que não pode ser dividido”, o fato é que eles se juntam para formar moléculas que se juntam para formar organismos que são… bem, tudo. Os átomos aqui são os blocos de construção do universo. Algo como peças de lego que você pode montar e combinar para criar elementos maiores.

### Apollo - App Sample 

Procurando uma forma de validar os átomos desenvolvidos, criamos um aplicativo de exemplo para demonstrar esses componentes visualmente, na prática.

<p align="center">
  <img width="470" height="820" src="https://i.imgur.com/fpcCjKI.png">
</p>


## Átomos

### **Aplicação dos átomos nos componentes**

Durante o desenvolvimento dos átomos sempre pensávamos em como suportar o código legado. Depois de muitos estudos e testes, chegamos à conclusão que precisaríamos de duas formas de utilizar os átomos:

- Acesso direto (para suportar o legado); 
- Acesso dentro do módulo de `UI` para novos componentes (moléculas) . 

Com isso, chegamos às seguintes conclusões:

- O exemplo abaixo ilustra como ficaria a aplicação dos átomos nos componentes dentro do módulo de
 `UI` 

```swift
class ViewController: UIViewController { 
    private lazy var containerView: UIView { 
        let view = UIView()
        return view
    }

    private func buildConstraints() { 
        containerView.layout { 
            $0.top == view.topAnchor + Spacing.base02
            $0.leading == view.leadingAnchor + Spacing.base02
            $0.trailing == view.trailingAnchor - Spacing.base02
            $0.bottom == view.bottomAnchor - Spacing.base02
        }
    }

    private func styles() { 
        containerView.style {
            // Basicamente, o que estamos fazendo aqui, por trás, é 
            // setar o containerView.layer.borderWidth = 2.0 
            // Com o tempo, vamos abstrair isso junto com as cores
            $0.border = .medium
            // Agora aqui, o que estamos fazendo, por trás, é setar
            // containerView.layer.cornerRadius = 2.0
            $0.cornerRadius = .light
        }
    }
}
```

- Case precise criar um novo componente fora do módulo de `UI`, deve-se proceder da seguinte forma:

```swift
class ViewController: UIViewController { 
    private lazy var containerView: UIView { 
        let view = UIView()
        view.layer.borderWidth = Border.medium
        view.layer.cornerRadius = CornerRadius.light
        return view
    }

    private func buildConstraints() { 
        containerView.layout { 
            $0.top == view.topAnchor + Spacing.base02
            $0.leading == view.leadingAnchor + Spacing.base02
            $0.trailing == view.trailingAnchor - Spacing.base02
            $0.bottom == view.bottomAnchor - Spacing.base02
        }
    }
}
```



### Átomos disponíveis

### Border

O átomo ***Border*** consiste na combinação de alguns protocolos de medidas: 

- `ValueScale` 
- `NoneScale`

Com isso vamos ganhar as seguintes opções: 

![Tabela de Bordas](https://i.imgur.com/jCmd1xC.png)

Para facilitar futuras manutenções e alterações, foi adicionado um novo protocolo chamado `BorderLevel`, que nada mais é que um protocolo que conforma com os 2 outros citados acima. 

Como já citado anteriormente, quase sempre teremos duas formas de usar os átomos: uma para o legado e outra para novos componentes dentro do módulo de `UI`.

- Nesse primeiro exemplo estamos usando o átomo fora do módulo de `UI`

```swift
class ViewController: UIViewController { 
    private lazy var containerView: UIView = { 
        let view = UIView()
        view.layer.borderWidth = Border.light
        return view
    }()
}
```

- Agora aqui, estamos simulando um novo componente dentro do módulo de `UI`. Pra melhor organização, foi adicionado um método `private` para deixar toda a configuração de estilo em um único lugar.

```swift
class ViewController: UIViewController { 
    private lazy var containerView: UIView = { 
        let view = UIView()
        return view
    }()

    private func style() { 
        containerView.style { 
            $0.border = .light
        }
    }
}
```


### Corner Radius 

O átomo de ***Corner Radius*** consiste na combinação de alguns protocolos de medidas: 

- `ValueScale`
- `NoneScale` 
- `FullScale`

Com isso vamos ganhar as seguintes opções:

![Tabela de Corner Radius](https://i.imgur.com/lamO5CU.png)

No caso do `CornerRadius.full`, podemos perceber que o mesmo tem o valor de `Void`. Isso significa que ele não pode ser acessado diretamente de fora do módulo de `UI`, somente por meio do método `style(_:)`. O método `style(_:)` calcula internamente o valor do _corner radius_ para definir uma curvatura perfeitamente circular. 

Por segurança, foi adicionada a seguinte mensagem caso ele seja acessado diretamente: 

```swift
    @available(*, deprecated, message: "The full property has been deprecated, as it cannot be accessed directly, but only by style method")
    public static let full: Void = ()
```

Para facilitar futuras manutenções e alterações foi adicionado um novo protocolo chamado `CornerRadiusLevel` que nada mais é um protocolo que conforma com os 2 outros citados acima. 

Como já citado anteriormente quase sempre vamos ter 2 formas de usar os átomos, tanto no legado quanto em um novo componente dentro do módulo de `UI`.

- Nesse primeiro exemplo estamos usando o átomo fora do módulo de `UI`

```swift
class ViewController: UIViewController { 
    private lazy var containerView: UIView = { 
        let view = UIView()
        view.layer.cornerRadius = CornerRadius.large
        return view
    }()
}
```

- Agora aqui estamos simulando um novo componente dentro do módulo de `UI`, a fim de uma melhor organização foi adicionado um método `private` para deixar toda a configuração de estilo em um único lugar

```swift
class ViewController: UIViewController { 
    private lazy var containerView: UIView = { 
        let view = UIView()
        return view
    }()

    private func style() { 
        containerView.style { 
            $0.cornerRadius = .full
        }
    }
}
```


### Spacing

O átomo de ***Spacing*** consiste na combinação de alguns protocolos de medidas:

- `BaseValue` 
- `NoneScale`

Com isso vamos ganhar as seguintes opções: 

![Tabela de Spacing](https://i.imgur.com/pitDXu4.png)

Para termos uma melhor compreensão do porquê da cada nome do `Spacing`, foi usada a seguinte lógica:

- `none`, define o spacing = `00`;
- `base00`, define o spacing = `04`;
- `baseXX`, onde `XX` representa um número `n`, com `n > 0`, define o spacing = `n * 8`.

Sendo assim, `base01` é 1 * 8 = 8 e `base07` é  7 * 8 = 56.

- No caso do `Spacing`, não o utilizamos por meio do método `style(_:)` já comentado anteriormente. Ele é acessado diretamente tanto de dentro quanto de fora do módulo de `UI`.

```swift
public class ViewController: UIViewController {
    private lazy var containerView: UIView = {
        let view = UIView()
        return view
    }()
  
    private func setupConstraints() {
        containerView.layout {
            $0.top == view.topAnchor + Spacing.base01
            $0.leading == view.leadingAnchor + Spacing.base02
            $0.trailing == view.trailingAnchor - Spacing.base03
            $0.bottom == view.bottomAnchor - Spacing.base04
        }
    }
}
```