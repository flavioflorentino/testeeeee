import CoreGraphics
import Foundation

public protocol CornerRadiusLevel: ValueScale & NoneScale & FullScale { }

/// CornerRadius represents corner radius scales
public enum CornerRadius: CornerRadiusLevel {
    /// None, default value 0.0
    public static let none: CGFloat = 0.0
    
    /// Light, default value 4.0
    public static let light: CGFloat = 4.0
    
    /// Medium, default value 8.0
    public static let medium: CGFloat = 8.0
    
    /// Strong, default value 16.0
    public static let strong: CGFloat = 16.0
    
    /// Full, default value 50% view size
    @available(*, deprecated, message: "The full property has been deprecated, as it cannot be accessed directly, but only by style method")
    public static let full: Void = ()
}

public extension CornerRadius {
    enum Style {
        case none
        case light
        case medium
        case strong
        case full
    }
}

extension CornerRadius.Style: RawRepresentable {
    public var rawValue: CGFloat {
        switch self {
        case .none, .full:
            return CornerRadius.none
        case .light:
            return CornerRadius.light
        case .medium:
            return CornerRadius.medium
        case .strong:
            return CornerRadius.strong
        }
    }
    
    public init(rawValue: CGFloat) {
        switch rawValue {
        case CornerRadius.light:
            self = .light
        case CornerRadius.medium:
            self = .medium
        case CornerRadius.strong:
            self = .strong
        default:
            self = .none
        }
    }
}

/// The protocol represents if has corner radius
public protocol HasCornerRadius: AnyObject {
    var cornerRadius: CornerRadius.Style { get set }
}
