import CoreGraphics
import Foundation

public protocol ShadowLevel: NoneScale & ValueScale { }

public enum Shadow: ShadowLevel {
    /// None, default value radius 0.0 and offset 0.0
    public static let none: ShadowType = ShadowSet(radius: .zero, offSet: .zero)
    
    /// Light, default value radius 1.0 and offset width 0.5 | height 1.0
    public static let light: ShadowType = ShadowSet(radius: 1.0, offSet: CGSize(width: 0.5, height: 1.0))
    
    /// Medium, default value radius 2.0 and offset width 1.0 | height 2.0
    public static let medium: ShadowType = ShadowSet(radius: 2.0, offSet: CGSize(width: 1.0, height: 2.0))
    
    /// Strong, default value radius 5.0 and offset 0.0
    public static let strong: ShadowType = ShadowSet(radius: 5.0, offSet: .zero)
}

public extension Shadow {
    enum Style {
        case none
        case light(color: Colors.Style, opacity: Opacity.Style, cgPath: CGPath? = nil)
        case medium(color: Colors.Style, opacity: Opacity.Style, cgPath: CGPath? = nil)
        case strong(color: Colors.Style, opacity: Opacity.Style, cgPath: CGPath? = nil)
    }
}

extension Shadow.Style: RawRepresentable {
    public var rawValue: (shadow: (type: ShadowType, color: Colors.Style), config: (opacity: Opacity.Style, cgPath: CGPath?)) {
        switch self {
        case .none:
            return (shadow: (type: Shadow.none, color: .unknown), config: (opacity: .none, cgPath: nil))
        case let .light(color, opacity, cgPath):
            return (shadow: (type: Shadow.light, color: color), config: (opacity: opacity, cgPath: cgPath))
        case let .medium(color, opacity, cgPath):
            return (shadow: (type: Shadow.medium, color: color), config: (opacity: opacity, cgPath: cgPath))
        case let .strong(color, opacity, cgPath):
            return (shadow: (type: Shadow.strong, color: color), config: (opacity: opacity, cgPath: cgPath))
        }
    }
    
    public init(rawValue: (shadow: (type: ShadowType, color: Colors.Style), config: (opacity: Opacity.Style, cgPath: CGPath?))) {
        switch rawValue.shadow.type.radius {
        case Shadow.light.radius:
            self = .light(color: .unknown, opacity: .none)
        case Shadow.medium.radius:
            self = .medium(color: .unknown, opacity: .none)
        case Shadow.strong.radius:
            self = .strong(color: .unknown, opacity: .none)
        default:
            self = .none
        }
    }
}

/// The protocol represents if has shadow
public protocol HasShadow: AnyObject {
    var shadow: Shadow.Style { get set }
}
