import CoreGraphics
import Foundation

public protocol OpacityLevel: ValueScale & NoneScale & FullScale & AdditionalScale { }

public enum Opacity: OpacityLevel {
    /// None, default value 0.0
    public static let none: CGFloat = 0.0
    
    /// UltraLight, default value 0.10
    public static let ultraLight: CGFloat = 0.10
    
    /// Light, default value 0.25
    public static let light: CGFloat = 0.25
    
    /// Medium, default value 0.50
    public static let medium: CGFloat = 0.50
    
    /// Strong, default value 0.75
    public static let strong: CGFloat = 0.75
    
    /// Full, default value 1.0
    public static let full: CGFloat = 1.0
}

public extension Opacity {
    enum Style {
        case none
        case ultraLight
        case light
        case medium
        case strong
        case full
    }
}

extension Opacity.Style: RawRepresentable {
    public var rawValue: CGFloat {
        switch self {
        case .none:
            return Opacity.none
        case .ultraLight:
            return Opacity.ultraLight
        case .light:
            return Opacity.light
        case .medium:
            return Opacity.medium
        case .strong:
            return Opacity.strong
        case .full:
            return Opacity.full
        }
    }
    
    public init(rawValue: CGFloat) {
        switch rawValue {
        case Opacity.ultraLight:
            self = .ultraLight
        case Opacity.light:
            self = .light
        case Opacity.medium:
            self = .medium
        case Opacity.strong:
            self = .strong
        case Opacity.full:
            self = .full
        default:
            self = .none
        }
    }
}

/// The protocol represents if has opacity
public protocol HasOpacity: AnyObject {
    var opacity: Opacity.Style { get set }
}
