import UIKit

public typealias PrimaryColorType = BrandingColorType & GrayScaleColorType
public typealias SecondaryColorType = UniversityColorType & BusinessColorType
public typealias FeedbackColorType = WarningColorType & CriticalColorType & SuccessColorType & NeutralColorType & NotificationColorType
public typealias SupportColorType = BackgroundColorType & GroupedBackgroundColorType & ExternalColorType & AdditionalColorType & TemporaryColorType

public protocol ColorsType: PrimaryColorType, SecondaryColorType, FeedbackColorType, SupportColorType { }

public enum Colors: ColorsType {
    /// Branding900, default value light color `#006532` and dark color `#006532`
    public static let branding900: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0, green: 0.3960784314, blue: 0.1960784314, alpha: 1), darkColor: #colorLiteral(red: 0, green: 0.3960784314, blue: 0.1960784314, alpha: 1))
    
    /// Branding800, default value light color `#008441` and dark color `#008441`
    public static let branding800: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0, green: 0.5176470588, blue: 0.2549019608, alpha: 1), darkColor: #colorLiteral(red: 0, green: 0.5176470588, blue: 0.2549019608, alpha: 1))
    
    /// Branding700, default value light color `#00984B` and dark color `#00984B`
    public static let branding700: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0, green: 0.5960784314, blue: 0.2941176471, alpha: 1), darkColor: #colorLiteral(red: 0, green: 0.5960784314, blue: 0.2941176471, alpha: 1))
    
    /// Branding600, default value light color `#00AC4A` and dark color `#00AC4A`
    public static let branding600: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0, green: 0.6745098039, blue: 0.2901960784, alpha: 1), darkColor: #colorLiteral(red: 0, green: 0.6745098039, blue: 0.2901960784, alpha: 1))
    
    /// Branding500, default value light color `#00BC54` and dark color `#00BC54`
    public static let branding500: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0, green: 0.737254902, blue: 0.3294117647, alpha: 1), darkColor: #colorLiteral(red: 0, green: 0.737254902, blue: 0.3294117647, alpha: 1))
    
    /// Branding400, default value light color `#11C76F` and dark color `#11C76F`
    public static let branding400: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.06666666667, green: 0.7803921569, blue: 0.4352941176, alpha: 1), darkColor: #colorLiteral(red: 0.06666666667, green: 0.7803921569, blue: 0.4352941176, alpha: 1))
    
    /// Branding300, default value light color `#58D289` and dark color `#58D289`
    public static let branding300: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.3450980392, green: 0.8235294118, blue: 0.537254902, alpha: 1), darkColor: #colorLiteral(red: 0.3450980392, green: 0.8235294118, blue: 0.537254902, alpha: 1))
    
    /// Branding200, default value light color `#8FDEAC` and dark color `#8FDEAC`
    public static let branding200: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.5607843137, green: 0.8705882353, blue: 0.6745098039, alpha: 1), darkColor: #colorLiteral(red: 0.5607843137, green: 0.8705882353, blue: 0.6745098039, alpha: 1))
    
    /// Branding100, default value light color `#BDEBCC` and dark color `#BDEBCC`
    public static let branding100: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.7411764706, green: 0.9215686275, blue: 0.8, alpha: 1), darkColor: #colorLiteral(red: 0.5607843137, green: 0.8705882353, blue: 0.6745098039, alpha: 1))
    
    /// Branding050, default value light color `#E3F7EA` and dark color `#E3F7EA`
    public static let branding050: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.8901960784, green: 0.968627451, blue: 0.9176470588, alpha: 1), darkColor: #colorLiteral(red: 0.8901960784, green: 0.968627451, blue: 0.9176470588, alpha: 1))
    
    /// BrandingBase, default value light color `#00AC4A` and dark color `#00AC4A`
    public static let brandingBase: DynamicColorType = Colors.branding600
    
    /// University800, default value light color `#192543` and dark color `#192543`
    public static let university800: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.09803921569, green: 0.1450980392, blue: 0.262745098, alpha: 1), darkColor: #colorLiteral(red: 0.09803921569, green: 0.1450980392, blue: 0.262745098, alpha: 1))
    
    /// University600, default value light color `#082468` and dark color `#6A84D6`
    public static let university600: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.03137254902, green: 0.1411764706, blue: 0.4078431373, alpha: 1), darkColor: #colorLiteral(red: 0.4156862745, green: 0.5176470588, blue: 0.8392156863, alpha: 1))
    
    /// University200, default value light color `#0050C5` and dark color `#0050C5`
    public static let university200: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0, green: 0.3137254902, blue: 0.7725490196, alpha: 1), darkColor: #colorLiteral(red: 0, green: 0.3137254902, blue: 0.7725490196, alpha: 1))
    
    /// University100, default value light color `#6A84D6` and dark color `#6A84D6`
    public static let university100: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.4156862745, green: 0.5176470588, blue: 0.8392156863, alpha: 1), darkColor: #colorLiteral(red: 0.4156862745, green: 0.5176470588, blue: 0.8392156863, alpha: 1))
    
    /// University050, default value light color `#C1C9ED` and dark color `#C1C9ED`
    public static let university050: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.7568627451, green: 0.7882352941, blue: 0.9294117647, alpha: 1), darkColor: #colorLiteral(red: 0.7568627451, green: 0.7882352941, blue: 0.9294117647, alpha: 1))
    
    /// UniversityBase, default value light color `#082468` and dark color `#6A84D6`
    public static let universityBase: DynamicColorType = Colors.university600
    
    /// Business900, default value light color `#0A353C` and dark color `#38727C`
    public static let business900: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.03921568627, green: 0.2078431373, blue: 0.2352941176, alpha: 1), darkColor: #colorLiteral(red: 0.2196078431, green: 0.4470588235, blue: 0.4862745098, alpha: 1))
    
    /// Business800, default value light color `#1C4B53` and dark color `#1C4B53`
    public static let business800: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.1098039216, green: 0.2941176471, blue: 0.3254901961, alpha: 1), darkColor: #colorLiteral(red: 0.1098039216, green: 0.2941176471, blue: 0.3254901961, alpha: 1))
    
    /// Business700, default value light color `#295E67` and dark color `#295E67`
    public static let business700: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.1607843137, green: 0.368627451, blue: 0.4039215686, alpha: 1), darkColor: #colorLiteral(red: 0.1607843137, green: 0.368627451, blue: 0.4039215686, alpha: 1))
    
    /// Business600, default value light color `#38727C` and dark color `#38727C`
    public static let business600: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.2196078431, green: 0.4470588235, blue: 0.4862745098, alpha: 1), darkColor: #colorLiteral(red: 0.2196078431, green: 0.4470588235, blue: 0.4862745098, alpha: 1))
    
    /// Business500, default value light color `#43828C` and dark color `#43828C`
    public static let business500: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.262745098, green: 0.5098039216, blue: 0.5490196078, alpha: 1), darkColor: #colorLiteral(red: 0.262745098, green: 0.5098039216, blue: 0.5490196078, alpha: 1))
    
    /// Business400, default value light color `#5E959E` and dark color `#5E959E`
    public static let business400: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.368627451, green: 0.5843137255, blue: 0.6196078431, alpha: 1), darkColor: #colorLiteral(red: 0.368627451, green: 0.5843137255, blue: 0.6196078431, alpha: 1))
    
    /// Business300, default value light color `#78A9B1` and dark color `#78A9B1`
    public static let business300: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.4705882353, green: 0.662745098, blue: 0.6941176471, alpha: 1), darkColor: #colorLiteral(red: 0.4705882353, green: 0.662745098, blue: 0.6941176471, alpha: 1))
    
    /// Business200, default value light color `#9AC3CA` and dark color `#9AC3CA`
    public static let business200: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.6039215686, green: 0.7647058824, blue: 0.7921568627, alpha: 1), darkColor: #colorLiteral(red: 0.6039215686, green: 0.7647058824, blue: 0.7921568627, alpha: 1))
    
    /// Business100, default value light color `#BBDDE1` and dark color `#BBDDE1`
    public static let business100: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.7333333333, green: 0.8666666667, blue: 0.8823529412, alpha: 1), darkColor: #colorLiteral(red: 0.7333333333, green: 0.8666666667, blue: 0.8823529412, alpha: 1))
    
    /// Business050, default value light color `#D9F3FA` and dark color `#D9F3FA`
    public static let business050: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.8509803922, green: 0.9529411765, blue: 0.9803921569, alpha: 1), darkColor: #colorLiteral(red: 0.8509803922, green: 0.9529411765, blue: 0.9803921569, alpha: 1))
    
    /// BusinessBase, default value light color `#0A353C` and dark color `#38727C`
    public static let businessBase: DynamicColorType = Colors.business900
    
    /// Black, default value light color `#000000` and dark color `#FFFFFF`
    public static let black: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), darkColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
    
    /// Grayscale950, default value light color `#060B0D` and dark color `#FFFFFF`
    public static let grayscale950: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.02352941176, green: 0.0431372549, blue: 0.05098039216, alpha: 1), darkColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
    
    /// Grayscale900, default value light color `#0E161A` and dark color `#FFFFFF`
    public static let grayscale900: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.05490196078, green: 0.0862745098, blue: 0.1019607843, alpha: 1), darkColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
    
    /// Grayscale850, default value light color `#172126` and dark color `#F2F2F2`
    public static let grayscale850: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.09019607843, green: 0.1294117647, blue: 0.1490196078, alpha: 1), darkColor: #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1))
    
    /// Grayscale800, default value light color `#212D33` and dark color `#F2F2F2`
    public static let grayscale800: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.1294117647, green: 0.1764705882, blue: 0.2, alpha: 1), darkColor: #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1))
    
    /// Grayscale750, default value light color `#2D3A40` and dark color `#E5E5E5`
    public static let grayscale750: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.1764705882, green: 0.2274509804, blue: 0.2509803922, alpha: 1), darkColor: #colorLiteral(red: 0.8980392157, green: 0.8980392157, blue: 0.8980392157, alpha: 1))
    
    /// Grayscale700, default value light color `#39464D` and dark color `#E5E5E5`
    public static let grayscale700: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.2235294118, green: 0.2745098039, blue: 0.3019607843, alpha: 1), darkColor: #colorLiteral(red: 0.8980392157, green: 0.8980392157, blue: 0.8980392157, alpha: 1))
    
    /// Grayscale600, default value light color `#525F66` and dark color `#CCCCCC`
    public static let grayscale600: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.3215686275, green: 0.3725490196, blue: 0.4, alpha: 1), darkColor: #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1))
    
    /// Grayscale500, default value light color `#6C7980` and dark color `#AAB0B2`
    public static let grayscale500: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.4235294118, green: 0.4745098039, blue: 0.5019607843, alpha: 1), darkColor: #colorLiteral(red: 0.6666666667, green: 0.6901960784, blue: 0.6980392157, alpha: 1))
    
    /// Grayscale400, default value light color `#8A9499` and dark color `#8A9499`
    public static let grayscale400: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.5411764706, green: 0.5803921569, blue: 0.6, alpha: 1), darkColor: #colorLiteral(red: 0.5411764706, green: 0.5803921569, blue: 0.6, alpha: 1))
    
    /// Grayscale300, default value light color `#AAB0B2` and dark color `#6C7980`
    public static let grayscale300: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.6666666667, green: 0.6901960784, blue: 0.6980392157, alpha: 1), darkColor: #colorLiteral(red: 0.4235294118, green: 0.4745098039, blue: 0.5019607843, alpha: 1))
    
    /// Grayscale200, default value light color `#CCCCCC` and dark color `#525F66`
    public static let grayscale200: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1), darkColor: #colorLiteral(red: 0.3215686275, green: 0.3725490196, blue: 0.4, alpha: 1))
    
    /// Grayscale100, default value light color `#E5E5E5` and dark color `#39464D`
    public static let grayscale100: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.8980392157, green: 0.8980392157, blue: 0.8980392157, alpha: 1), darkColor: #colorLiteral(red: 0.2235294118, green: 0.2745098039, blue: 0.3019607843, alpha: 1))
    
    /// Grayscale050, default value light color `#F2F2F2` and dark color `#39464D`
    public static let grayscale050: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1), darkColor: #colorLiteral(red: 0.2235294118, green: 0.2745098039, blue: 0.3019607843, alpha: 1))
    
    /// White, default value light color `#FFFFFF` and dark color `#39464D`
    public static let white: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), darkColor: #colorLiteral(red: 0.2235294118, green: 0.2745098039, blue: 0.3019607843, alpha: 1))
    
    /// Warning900, default value light color `#F37422` and dark color `#F37422`
    public static let warning900: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.9529411765, green: 0.4549019608, blue: 0.1333333333, alpha: 1), darkColor: #colorLiteral(red: 0.9529411765, green: 0.4549019608, blue: 0.1333333333, alpha: 1))
    
    /// Warning600, default value light color `#F8B330` and dark color `#F8B330`
    public static let warning600: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.9725490196, green: 0.7019607843, blue: 0.1882352941, alpha: 1), darkColor: #colorLiteral(red: 0.9725490196, green: 0.7019607843, blue: 0.1882352941, alpha: 1))
    
    /// Warning400, default value light color `#FAC942` and dark color `#FAC942`
    public static let warning400: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.9803921569, green: 0.7882352941, blue: 0.2588235294, alpha: 1), darkColor: #colorLiteral(red: 0.9803921569, green: 0.7882352941, blue: 0.2588235294, alpha: 1))
    
    /// Warning200, default value light color `#FCDF8B` and dark color `#FCDF8B`
    public static let warning200: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.9882352941, green: 0.8745098039, blue: 0.5450980392, alpha: 1), darkColor: #colorLiteral(red: 0.9882352941, green: 0.8745098039, blue: 0.5450980392, alpha: 1))
    
    /// Warning050, default value light color `#FEF8E3` and dark color `#FEF8E3`
    public static let warning050: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.9960784314, green: 0.9725490196, blue: 0.8901960784, alpha: 1), darkColor: #colorLiteral(red: 0.9960784314, green: 0.9725490196, blue: 0.8901960784, alpha: 1))
    
    /// WarningBase, default value light color `#F8B330` and dark color `#F8B330`
    public static let warningBase: DynamicColorType = Colors.warning600
    
    /// Critical900, default value light color `#FF0828` and dark color `#FF0828`
    public static let critical900: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 1, green: 0.03137254902, blue: 0.1568627451, alpha: 1), darkColor: #colorLiteral(red: 1, green: 0.03137254902, blue: 0.1568627451, alpha: 1))
    
    /// Critical600, default value light color `#FF2048` and dark color `#FF2048`
    public static let critical600: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 1, green: 0.1254901961, blue: 0.2823529412, alpha: 1), darkColor: #colorLiteral(red: 1, green: 0.1254901961, blue: 0.2823529412, alpha: 1))
    
    /// Critical400, default value light color `#FF4F75` and dark color `#FF4F75`
    public static let critical400: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 1, green: 0.3098039216, blue: 0.4588235294, alpha: 1), darkColor: #colorLiteral(red: 1, green: 0.3098039216, blue: 0.4588235294, alpha: 1))
    
    /// Critical200, default value light color `#FF96AE` and dark color `#FF96AE`
    public static let critical200: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 1, green: 0.5882352941, blue: 0.6823529412, alpha: 1), darkColor: #colorLiteral(red: 1, green: 0.5882352941, blue: 0.6823529412, alpha: 1))
    
    /// Critical050, default value light color `#FFE6EB` and dark color `#FFE6EB`
    public static let critical050: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 1, green: 0.9019607843, blue: 0.9215686275, alpha: 1), darkColor: #colorLiteral(red: 1, green: 0.9019607843, blue: 0.9215686275, alpha: 1))
    
    /// CriticalBase, default value light color `#FF2048` and dark color `#FF2048`
    public static let criticalBase: DynamicColorType = Colors.critical600
    
    /// Success900, default value light color `#00984B` and dark color `#00984B`
    public static let success900: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0, green: 0.5960784314, blue: 0.2941176471, alpha: 1), darkColor: #colorLiteral(red: 0, green: 0.5960784314, blue: 0.2941176471, alpha: 1))
    
    /// Success600, default value light color `#00AC4A` and dark color `#00AC4A`
    public static let success600: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0, green: 0.6745098039, blue: 0.2901960784, alpha: 1), darkColor: #colorLiteral(red: 0, green: 0.6745098039, blue: 0.2901960784, alpha: 1))
    
    /// Success500, default value light color `#00BC54` and dark color `#00BC54`
    public static let success500: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0, green: 0.737254902, blue: 0.3294117647, alpha: 1), darkColor: #colorLiteral(red: 0, green: 0.737254902, blue: 0.3294117647, alpha: 1))
    
    /// Success400, default value light color `#11C76F` and dark color `#11C76F`
    public static let success400: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.06666666667, green: 0.7803921569, blue: 0.4352941176, alpha: 1), darkColor: #colorLiteral(red: 0.06666666667, green: 0.7803921569, blue: 0.4352941176, alpha: 1))
    
    /// Success050, default value light color `#E3F7EA` and dark color `#E3F7EA`
    public static let success050: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.8901960784, green: 0.968627451, blue: 0.9176470588, alpha: 1), darkColor: #colorLiteral(red: 0.8901960784, green: 0.968627451, blue: 0.9176470588, alpha: 1))
    
    /// SuccessBase, default value light color `#00AC4A` and dark color `#00AC4A`
    public static let successBase: DynamicColorType = Colors.success600
    
    /// Neutral800, default value light color `#0073B5` and dark color `#0073B5`
    public static let neutral800: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0, green: 0.4509803922, blue: 0.7098039216, alpha: 1), darkColor: #colorLiteral(red: 0, green: 0.4509803922, blue: 0.7098039216, alpha: 1))
    
    /// Neutral600, default value light color `#0097DC` and dark color `#0097DC`
    public static let neutral600: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0, green: 0.5921568627, blue: 0.862745098, alpha: 1), darkColor: #colorLiteral(red: 0, green: 0.5921568627, blue: 0.862745098, alpha: 1))
    
    /// Neutral400, default value light color `#00B0ED` and dark color `#00B0ED`
    public static let neutral400: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0, green: 0.6901960784, blue: 0.9294117647, alpha: 1), darkColor: #colorLiteral(red: 0, green: 0.6901960784, blue: 0.9294117647, alpha: 1))
    
    /// Neutral200, default value light color `#7BD1F4` and dark color `#7BD1F4`
    public static let neutral200: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.4823529412, green: 0.8196078431, blue: 0.9568627451, alpha: 1), darkColor: #colorLiteral(red: 0.4823529412, green: 0.8196078431, blue: 0.9568627451, alpha: 1))
    
    /// Neutral050, default value light color `#E0F4FC` and dark color `#E0F4FC`
    public static let neutral050: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.8784313725, green: 0.9568627451, blue: 0.9882352941, alpha: 1), darkColor: #colorLiteral(red: 0.8784313725, green: 0.9568627451, blue: 0.9882352941, alpha: 1))
    
    /// NeutralBase, default value light color `#0097DC` and dark color `#0097DC`
    public static let neutralBase: DynamicColorType = Colors.neutral600
    
    /// Notification600, default value light color `#FF2048` and dark color `#FF2048`
    public static let notification600: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 1, green: 0.1254901961, blue: 0.2823529412, alpha: 1), darkColor: #colorLiteral(red: 1, green: 0.1254901961, blue: 0.2823529412, alpha: 1))
    
    /// The color for the main background of your interface, light color `#FFFFFF` and dark color `#000000`
    public static let backgroundPrimary: DynamicColorType = BackgroundDynamicColor(
        lightColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1),
        darkColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1),
        nextColor: backgroundSecondary
    )
    
    /// The color for content layered on top of the main background, light mode color `#F2F2F2` and dark mode color `#172126`
    public static let backgroundSecondary: DynamicColorType = BackgroundDynamicColor(
        lightColor: #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1),
        darkColor: #colorLiteral(red: 0.09019607843, green: 0.1294117647, blue: 0.1490196078, alpha: 1),
        nextColor: backgroundTertiary
    )
    
    /// The color for content layered on top of secondary backgrounds, light mode color `#FFFFFF` and dark mode color `#2D3A40`
    public static let backgroundTertiary: DynamicColorType = BackgroundDynamicColor(
        lightColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1),
        darkColor: #colorLiteral(red: 0.1764705882, green: 0.2274509804, blue: 0.2509803922, alpha: 1),
        nextColor: backgroundQuarternary
    )
    
    /// The color for content layered on top of tertiary backgrounds, light mode color `#FFFFFF` and dark mode color `#39464D`
    private static let backgroundQuarternary: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), darkColor: #colorLiteral(red: 0.2235294118, green: 0.2745098039, blue: 0.3019607843, alpha: 1))
    
    /// The color for the main background of your grouped interface, light mode color `#F2F2F2` and dark mode color `#000000`
    public static let groupedBackgroundPrimary: DynamicColorType = BackgroundDynamicColor(
        lightColor: #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1),
        darkColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1),
        nextColor: groupedBackgroundSecondary
    )
    
    /// The color for content layered on top of the main background of your grouped interface, light mode color `#FFFFFF` and dark mode color `#172126`
    public static let groupedBackgroundSecondary: DynamicColorType = BackgroundDynamicColor(
        lightColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1),
        darkColor: #colorLiteral(red: 0.09019607843, green: 0.1294117647, blue: 0.1490196078, alpha: 1),
        nextColor: groupedBackgroundTertiary
    )
    
    /// The color for content layred on top of secondary backgrounds of your grouped interface, light mode color `#F2F2F2` and dark mode color `#2D3A40`
    public static let groupedBackgroundTertiary: DynamicColorType = BackgroundDynamicColor(
        lightColor: #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1),
        darkColor: #colorLiteral(red: 0.1764705882, green: 0.2274509804, blue: 0.2509803922, alpha: 1),
        nextColor: groupedBackgroundQuarternary
    )
    
    /// The color for content layred on top of tertiary backgrounds of your grouped interface, light mode color `#F2F2F2` and dark mode color `#39464D`
    private static let groupedBackgroundQuarternary: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1), darkColor: #colorLiteral(red: 0.2235294118, green: 0.2745098039, blue: 0.3019607843, alpha: 1))
    
    /// ExternalFacebook, default value light color `#3B5998` and dark color `#3B5998`
    public static let externalFacebook: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.231372549, green: 0.3490196078, blue: 0.5960784314, alpha: 1), darkColor: #colorLiteral(red: 0.231372549, green: 0.3490196078, blue: 0.5960784314, alpha: 1))
    
    /// ExternalTwitter, default value light color `#55ACEE` and dark color `#55ACEE`
    public static let externalTwitter: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.3333333333, green: 0.6745098039, blue: 0.9333333333, alpha: 1), darkColor: #colorLiteral(red: 0.3333333333, green: 0.6745098039, blue: 0.9333333333, alpha: 1))
    
    /// ExternalWhatsApp, default value light color `#25D366` and dark color `#25D366`
    public static let externalWhatsApp: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.1450980392, green: 0.8274509804, blue: 0.4, alpha: 1), darkColor: #colorLiteral(red: 0.1450980392, green: 0.8274509804, blue: 0.4, alpha: 1))
    
    /// External24HourBank, default value light color `#DA251C` and dark color `#DA251C`
    public static let external24HourBank: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.8549019608, green: 0.1450980392, blue: 0.1098039216, alpha: 1), darkColor: #colorLiteral(red: 0.8549019608, green: 0.1450980392, blue: 0.1098039216, alpha: 1))
    
    /// Additional001, default value light color `#638CFF` and dark color `#638CFF`
    public static let additional001: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 0.3882352941, green: 0.5490196078, blue: 1, alpha: 1), darkColor: #colorLiteral(red: 0.3882352941, green: 0.5490196078, blue: 1, alpha: 1))
    
    /// Additional002, default value light color `#FF85FA` and dark color `#FF85FA`
    public static let additional002: DynamicColorType = DynamicColor(lightColor: #colorLiteral(red: 1, green: 0.5215686275, blue: 0.9803921569, alpha: 1), darkColor: #colorLiteral(red: 1, green: 0.5215686275, blue: 0.9803921569, alpha: 1))
    
    /*
     There aren't linked color him still to `TemporaryColorType` typealias this is necessary,
     more information access design system documentation.
     */
    public typealias TemporaryColor = DynamicColorType
}

extension Colors: SupportHexadecimalColor {
    public static func hexColor(_ hexString: String) -> DynamicColorType? {
        if #available(iOS 13.0, *) {
            return DynamicColor(
                lightColor: hexFromLight(hexString),
                darkColor: hexFromDark(hexString)
            )
        }
        
        guard !hexString.isEmpty, let hexadecimal = Int(hexString.replacingOccurrences(of: "#", with: ""), radix: 16) else {
            return nil
        }
        
        return DynamicColor(
            lightColor: convertColorFromHex(hexadecimal),
            darkColor: convertColorFromHex(hexadecimal)
        )
    }
    
    private static func hexFromLight(_ hexString: String) -> UIColor {
        switch hexString.uppercased() {
        case "#21C25E":
            return Colors.branding600.lightColor
        case "#1E232A":
            return Colors.white.lightColor
        case "#F4F7FA", "#000000":
            return Colors.grayscale050.lightColor
        case "#3D4451":
            return Colors.grayscale100.lightColor
        case "#8F929D":
            return Colors.grayscale400.lightColor
        case "#F4F4F6":
            return Colors.black.lightColor
        default:
            guard let hexadecimal = Int(hexString.replacingOccurrences(of: "#", with: ""), radix: 16) else {
                return Colors.white.lightColor
            }
            
            return convertColorFromHex(hexadecimal)
        }
    }
    
    private static func hexFromDark(_ hexString: String) -> UIColor {
        switch hexString.uppercased() {
        case "#BBBBBB":
            return Colors.branding600.darkColor
        case "#E9ECED":
            return Colors.grayscale050.darkColor
        case "#21C25E":
            return Colors.grayscale100.darkColor
        case "#D9DCE3", "#989898":
            return Colors.grayscale400.darkColor
        case "#666666", "#FFFFFF", "#F4F4F6", "#F4F7FA":
            return Colors.grayscale700.darkColor
        case "#000000", "#4A4A4A", "#8F9D9D":
            return Colors.grayscale800.darkColor
        default:
            guard let hexadecimal = Int(hexString.replacingOccurrences(of: "#", with: ""), radix: 16) else {
                return Colors.black.lightColor
            }
            
            return convertColorFromHex(hexadecimal)
        }
    }
    
    private static func convertColorFromHex(_ hex: Int) -> UIColor {
        UIColor(red: CGFloat((hex >> 16) & 0xFF) / 255.0,
                green: CGFloat((hex >> 8) & 0xFF) / 255.0,
                blue: CGFloat(hex & 0xFF) / 255.0,
                alpha: 1)
    }
}

/// The protocol represents if has background color
public protocol HasBackgroundColor: AnyObject {
    var backgroundColor: Colors.Style { get set }
}

/// The protocol represents if has text color
public protocol HasTextColor: AnyObject {
    var textColor: Colors.Style { get set }
}

/// The protocol represents if has state color
public protocol HasStateColor: AnyObject {
    var textColor: (color: Colors.Style?, state: UIControl.State) { get set }
    var backgroundColor: (color: Colors.Style?, state: UIControl.State) { get set }
    var borderColor: (color: Colors.Style?, state: UIControl.State) { get set }
    var iconColor: (icon: (name: Iconography, alignment: IconAlignment)?, state: (color: Colors.Style?, state: UIControl.State)) { get set }
}

/// The protocol represents if has attributed color
public protocol HasAttributedColor: AnyObject {
    var textAttributedColor: (color: Colors.Style?, state: UIControl.State) { get set }
}
