import CoreGraphics
import Foundation
import UIKit

public protocol BorderLevel: ValueScale & NoneScale { }

/// Border represents border levels
public enum Border: BorderLevel {
    /// None, default value 0.0
    public static let none: CGFloat = 0.0
    
    /// Light, default value 1.0
    public static let light: CGFloat = 1.0
    
    /// Medium, default value 2.0
    public static let medium: CGFloat = 2.0
    
    /// Strong, default value 3.0
    public static let strong: CGFloat = 3.0
}

public extension Border {
    enum Style {
        case none
        case light(color: Colors.Style = .black())
        case medium(color: Colors.Style = .black())
        case strong(color: Colors.Style = .black())
    }
}

extension Border.Style: RawRepresentable {
    public var rawValue: (width: CGFloat, color: UIColor) {
        switch self {
        case .none:
            return (width: Border.none, color: Colors.black.color)
        case .light(let color):
            return (width: Border.light, color: color.rawValue)
        case .medium(let color):
            return (width: Border.medium, color: color.rawValue)
        case .strong(let color):
            return (width: Border.strong, color: color.rawValue)
        }
    }
    
    public init(rawValue: (width: CGFloat, color: UIColor)) {
        switch rawValue.width {
        case Border.light:
            self = .light(color: Colors.Style(rawValue: rawValue.color))
        case Border.medium:
            self = .medium(color: Colors.Style(rawValue: rawValue.color))
        case Border.strong:
            self = .strong(color: Colors.Style(rawValue: rawValue.color))
        default:
            self = .none
        }
    }
}

/// The protocol represents if has border
public protocol HasBorder: AnyObject {
    var border: Border.Style { get set }
}
