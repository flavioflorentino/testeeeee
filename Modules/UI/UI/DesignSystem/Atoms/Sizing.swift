import CoreGraphics
import Foundation

public protocol SizeLevel: BaseValue { }

/// Sizing represents size types
public enum Sizing: SizeLevel {
    /// Base00, default value 4.0
    public static let base00: CGFloat = 4.0
    
    /// Base01, default value 8.0
    public static let base01: CGFloat = 8.0
    
    /// Base02, default value 16.0
    public static let base02: CGFloat = 16.0
    
    /// Base03, default value 32.0
    public static let base03: CGFloat = 32.0
    
    /// Base04, default value 36.0
    public static let base04: CGFloat = 36.0
    
    /// Base05, default value 40.0
    public static let base05: CGFloat = 40.0
    
    /// Base06, default value 48.0
    public static let base06: CGFloat = 48.0
    
    /// Base07, default value 56.0
    public static let base07: CGFloat = 56.0
    
    /// Base08, default value 64.0
    public static let base08: CGFloat = 64.0
    
    /// Base09, default value 72.0
    public static let base09: CGFloat = 72.0
    
    /// Base10, default value 80.0
    public static let base10: CGFloat = 80.0
    
    /// Base11, default value 88.0
    public static let base11: CGFloat = 88.0
    
    /// Base12, default value 96.0
    public static let base12: CGFloat = 96.0
}

public extension Sizing {
    enum ImageView {
        /// XSmall, default value 32.0
        case xSmall
        
        /// Small, default value 40.0
        case small
        
        /// Medium, default value 48.0
        case medium
        
        /// Large, default value 72.0
        case large
        
        /// XLarge, default value 96.0
        case xLarge
    }
    
    enum Button {
        /// Small, default value 40.0
        case small
        
        /// Default, default value 48.0
        case `default`
    }
}

extension Sizing.ImageView: RawRepresentable {
    public var rawValue: CGSize {
        switch self {
        case .xSmall:
            return CGSize(width: Sizing.base03, height: Sizing.base03)
        case .small:
            return CGSize(width: Sizing.base05, height: Sizing.base05)
        case .medium:
            return CGSize(width: Sizing.base06, height: Sizing.base06)
        case .large:
            return CGSize(width: Sizing.base09, height: Sizing.base09)
        case .xLarge:
            return CGSize(width: Sizing.base12, height: Sizing.base12)
        }
    }
    
    public init?(rawValue: CGSize) {
        switch (rawValue.width, rawValue.height) {
        case (Sizing.base03, Sizing.base03):
            self = .xSmall
        case (Sizing.base05, Sizing.base05):
            self = .small
        case (Sizing.base06, Sizing.base06):
            self = .medium
        case (Sizing.base09, Sizing.base09):
            self = .large
        case (Sizing.base12, Sizing.base12):
            self = .xLarge
        default:
            return nil
        }
    }
}

extension Sizing.Button: RawRepresentable {
    public var rawValue: CGFloat {
        switch self {
        case .small:
            return Sizing.base05
        case .default:
            return Sizing.base06
        }
    }
    
    public init?(rawValue: CGFloat) {
        switch rawValue {
        case Sizing.base05:
            self = .small
        case Sizing.base06:
            self = .default
        default:
            return nil
        }
    }
}

/// The protocol represents if has size
public protocol HasSize: AnyObject {
    var size: CGSize { get set }
}
