import CoreGraphics
import Foundation

public protocol SpaceLevel: NoneScale & BaseValue { }

/// Spacing represents space types
public enum Spacing: SpaceLevel {
    /// None, default value 0.0
    public static let none: CGFloat = 0.0
    
    /// Base00, default value 4.0
    public static let base00: CGFloat = 4.0
    
    /// Base01, default value 8.0
    public static let base01: CGFloat = 8.0
    
    /// Base02, default value 16.0
    public static let base02: CGFloat = 16.0
    
    /// Base03, default value 24.0
    public static let base03: CGFloat = 24.0
    
    /// Base04, default value 32.0
    public static let base04: CGFloat = 32.0
    
    /// Base05, default value 40.0
    public static let base05: CGFloat = 40.0
    
    /// Base06, default value 48.0
    public static let base06: CGFloat = 48.0
    
    /// Base07, default value 56.0
    public static let base07: CGFloat = 56.0
    
    /// Base08, default value 64.0
    public static let base08: CGFloat = 64.0
    
    /// Base09, default value 72.0
    public static let base09: CGFloat = 72.0
    
    /// Base10, default value 80.0
    public static let base10: CGFloat = 80.0
    
    /// Base11, default value 88.0
    public static let base11: CGFloat = 88.0
    
    /// Base12, default value 96.0
    public static let base12: CGFloat = 96.0
}
