import UIKit

public struct Typography {
    private let closure: () -> UIFont
    
    internal static func discover(_ font: UIFont?) -> Self {
        Typography {
            font ?? UIFont.systemFont(ofSize: 16.0)
        }
    }
    
    /**
     Returns an instance of the system font for the specified typography style, scaled for the users selected content size category
     
     - Returns: The system font associated with the specified typography style.
     */
    public func font() -> UIFont {
        closure()
    }
    
    /**
    The typography style for titles
     
     - Parameter:
        - style: The style title
     
     - Returns: Returns an instance of the `Typography`
     */
    public static func title(_ style: Style.Title) -> Self {
        Typography {
            style.font
        }
    }
    
    /**
    The typography style for currency
     
     - Parameter:
        - style: The style currency
    
    - Returns: Returns an instance of the `Typography`
    */
    public static func currency(_ style: Style.Currency) -> Self {
        Typography {
            style.font
        }
    }
    
    /**
    The typography style for icons
     
     - Parameter:
        - style: The style currency
    
    - Returns: Returns an instance of the `Typography`
    */
    public static func icons(_ style: Style.Iconography) -> Self {
        Typography {
            style.font
        }
    }
    
    /**
    The typography style for primary body
     
     - Parameter:
        - style: The style body primary, default value is `default`
    
    - Returns: Returns an instance of the `Typography`
    */
    public static func bodyPrimary(_ style: Style.BodyPrimary = .default) -> Self {
        Typography {
            style.font
        }
    }
    
    /**
    The typography style for secondary body
    
     - Parameter:
        - style: The style secondary body, default value is `default`
     
    - Returns: Returns an instance of the `Typography`
    */
    public static func bodySecondary(_ style: Style.BodySecondary = .default) -> Self {
        Typography {
            style.font
        }
    }
    
    /**
    The typography style for primary link
     
     - Parameter:
        - style: The style primary link, default value is `default`
    
    - Returns: Returns an instance of the `Typography`
    */
    public static func linkPrimary(_ style: Style.LinkPrimary = .default) -> Self {
        Typography {
            style.font
        }
    }
    
    /**
    The typography style for secondary link
    
     - Parameter:
        - style: The style secondary link, default value is `default`
     
    - Returns: Returns an instance of the `Typography`
    */
    public static func linkSecondary(_ style: Style.LinkSecondary = .default) -> Self {
        Typography {
            style.font
        }
    }
    
    /**
    The typography style for caption
     
     - Parameter:
        - style: The style caption, default value is `default`
    
    - Returns: Returns an instance of the `Typography`
    */
    public static func caption(_ style: Style.Caption = .default) -> Self {
        Typography {
            style.font
        }
    }
}

public extension Typography {
    enum Style { }
}

public extension Typography.Style {
    enum Title: TypographyStylable {
        /// The size (in points) `28.0` and weight `bold`
        case xLarge
        /// The size (in points) `24.0` and weight `bold`
        case large
        /// The size (in points) `20.0` and weight `bold`
        case medium
        /// The size (in points) `18.0` and weight `semibold`
        case small
    
        internal var size: CGFloat {
            switch self {
            case .xLarge:
                return 28.0
            case .large:
                return 24.0
            case .medium:
                return 20.0
            case .small:
                return 18.0
            }
        }
        
        internal var weight: UIFont.Weight {
            switch self {
            case .xLarge, .large, .medium:
                return .bold
            case .small:
                return .semibold
            }
        }
    }
    
    enum Currency: TypographyStylable {
        /// The size (in points) `52.0` and weight `semibold`
        case large
        /// The size (in points) `36.0` and weight `semibold`
        case medium
        
        internal var size: CGFloat {
            switch self {
            case .large:
                return 52.0
            case .medium:
                return 36.0
            }
        }
        
        internal var weight: UIFont.Weight {
            switch self {
            case .large, .medium:
                return .semibold
            }
        }
    }
    
    enum Iconography: TypographyStylable {
        /// The size (in points) `16.0`
        case small
        /// The size (in points) `24.0`
        case medium
        /// The size (in points) `32.0`
        case large
        
        internal var size: CGFloat {
            switch self {
            case .small:
                return 16.0
            case .medium:
                return 24.0
            case .large:
                return 32.0
            }
        }
        
        internal var weight: UIFont.Weight {
            .regular
        }
        
        internal var fontName: String? {
            "unicons-line"
        }
        
        internal var fontType: String? {
            "ttf"
        }
    }

    enum BodyPrimary: TypographyStylable {
        /// The size (in points) `16.0` and weight `regular`
        case `default`
        /// The size (in points) `16.0` and weight `bold`
        case highlight
        
        internal var size: CGFloat {
            switch self {
            case .default, .highlight:
                return 16.0
            }
        }
        
        internal var weight: UIFont.Weight {
            switch self {
            case .default:
                return .regular
            case .highlight:
                return .bold
            }
        }
    }

    enum BodySecondary: TypographyStylable {
        /// The size (in points) `14.0` and weight `medium`
        case `default`
        /// The size (in points) `14.0` and weight `heavy`
        case highlight
        
        internal var size: CGFloat {
            switch self {
            case .default, .highlight:
                return 14.0
            }
        }
        
        internal var weight: UIFont.Weight {
            switch self {
            case .default:
                return .medium
            case .highlight:
                return .heavy
            }
        }
    }

    enum LinkPrimary: TypographyStylable {
        /// The size (in points) `16.0` and weight `regular`
        case `default`
        
        internal var size: CGFloat {
            switch self {
            case .default:
                return 16.0
            }
        }
        
        internal var weight: UIFont.Weight {
            switch self {
            case .default:
                return .regular
            }
        }
    }

    enum LinkSecondary: TypographyStylable {
        /// The size (in points) `14.0` and weight `semibold`
        case `default`
        
        internal var size: CGFloat {
            switch self {
            case .default:
                return 14.0
            }
        }
        
        internal var weight: UIFont.Weight {
            switch self {
            case .default:
                return .semibold
            }
        }
    }

    enum Caption: TypographyStylable {
        /// The size (in points) `12.0` and weight `regular`
        case `default`
        /// The size (in points) `12.0` and weight `bold`
        case highlight
        
        internal var size: CGFloat {
            switch self {
            case .default, .highlight:
                return 12.0
            }
        }
        
        internal var weight: UIFont.Weight {
            switch self {
            case .default:
                return .regular
            case .highlight:
                return .bold
            }
        }
    }
}

/// The protocol represents if has background typography
public protocol HasTypography: AnyObject {
    var typography: Typography { get set }
}

/// The protocol represents if has background text alignment
public protocol HasTextAlignment: AnyObject {
    var textAlignment: NSTextAlignment { get set }
}
