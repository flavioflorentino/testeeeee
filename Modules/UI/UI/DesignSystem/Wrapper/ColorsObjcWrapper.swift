import UIKit

@objc
public class DynamicColorObjcWrapper: NSObject {
    private var dynamicColor: DynamicColorType
    
    @objc public var lightColor: UIColor {
        dynamicColor.lightColor
    }
    
    @objc public var darkColor: UIColor {
        dynamicColor.darkColor
    }
    
    @objc public var color: UIColor {
        dynamicColor.color
    }
    
    public init(dynamicColor: DynamicColorType) {
        self.dynamicColor = dynamicColor
    }
}

@objc(Colors)
public final class ColorsObjcWrapper: NSObject, ColorsType {
    override private init() { }
    
    @objc public static var branding900: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.branding900)
    }
    
    @objc public static var branding800: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.branding800)
    }
    
    @objc public static var branding700: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.branding700)
    }
    
    @objc public static var branding600: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.branding600)
    }
    
    @objc public static var branding500: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.branding500)
    }
    
    @objc public static var branding400: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.branding400)
    }
    
    @objc public static var branding300: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.branding300)
    }
    
    @objc public static var branding200: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.branding200)
    }
    
    @objc public static var branding100: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.branding100)
    }
    
    @objc public static var branding050: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.branding050)
    }
    
    @objc public static var brandingBase: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.brandingBase)
    }
    
    @objc public static var university800: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.university800)
    }
    
    @objc public static var university600: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.university600)
    }
    
    @objc public static var university200: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.university200)
    }
    
    @objc public static var university100: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.university100)
    }
    
    @objc public static var university050: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.university050)
    }
    
    @objc public static var universityBase: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.universityBase)
    }
    
    @objc public static var business900: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.business900)
    }
    
    @objc public static var business800: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.business800)
    }
    
    @objc public static var business700: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.business700)
    }
    
    @objc public static var business600: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.business600)
    }
    
    @objc public static var business500: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.business500)
    }
    
    @objc public static var business400: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.business400)
    }
    
    @objc public static var business300: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.business300)
    }
    
    @objc public static var business200: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.business200)
    }
    
    @objc public static var business100: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.business100)
    }
    
    @objc public static var business050: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.business050)
    }
    
    @objc public static var businessBase: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.businessBase)
    }
    
    @objc public static var black: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.black)
    }
    
    @objc public static var grayscale950: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.grayscale950)
    }
    
    @objc public static var grayscale900: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.grayscale900)
    }
    
    @objc public static var grayscale850: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.grayscale850)
    }
    
    @objc public static var grayscale800: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.grayscale800)
    }
    
    @objc public static var grayscale750: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.grayscale750)
    }
    
    @objc public static var grayscale700: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.grayscale700)
    }
    
    @objc public static var grayscale600: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.grayscale600)
    }
    
    @objc public static var grayscale500: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.grayscale500)
    }
    
    @objc public static var grayscale400: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.grayscale400)
    }
    
    @objc public static var grayscale300: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.grayscale300)
    }
    
    @objc public static var grayscale200: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.grayscale200)
    }
    
    @objc public static var grayscale100: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.grayscale100)
    }
    
    @objc public static var grayscale050: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.grayscale050)
    }
    
    @objc public static var white: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.white)
    }
    
    @objc public static var warning900: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.warning900)
    }
    
    @objc public static var warning600: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.warning600)
    }
       
    @objc public static var warning400: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.warning400)
    }
       
    @objc public static var warning200: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.warning200)
    }
       
    @objc public static var warning050: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.warning050)
    }
       
    @objc public static var warningBase: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.warningBase)
    }
       
    @objc public static var critical900: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.critical900)
    }
    
    @objc public static var critical600: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.critical600)
    }
    
    @objc public static var critical400: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.critical400)
    }
    
    @objc public static var critical200: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.critical200)
    }
    
    @objc public static var critical050: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.critical050)
    }
    
    @objc public static var criticalBase: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.criticalBase)
    }
    
    @objc public static var success900: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.success900)
    }
    
    @objc public static var success600: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.success600)
    }
    
    @objc public static var success500: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.success500)
    }
    
    @objc public static var success400: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.success400)
    }
    
    @objc public static var success050: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.success050)
    }
    
    @objc public static var successBase: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.successBase)
    }
    
    @objc public static var neutral800: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.neutral800)
    }
    
    @objc public static var neutral600: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.neutral600)
    }
    
    @objc public static var neutral400: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.neutral400)
    }
    
    @objc public static var neutral200: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.neutral200)
    }
    
    @objc public static var neutral050: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.neutral050)
    }
    
    @objc public static var neutralBase: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.neutralBase)
    }
    
    @objc public static var notification600: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.notification600)
    }
    
    @objc public static var backgroundPrimary: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.backgroundPrimary)
    }
    
    @objc public static var backgroundSecondary: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.backgroundSecondary)
    }
    
    @objc public static var backgroundTertiary: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.backgroundTertiary)
    }
    
    @objc public static var groupedBackgroundPrimary: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.groupedBackgroundPrimary)
    }
    
    @objc public static var groupedBackgroundSecondary: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.groupedBackgroundSecondary)
    }
    
    @objc public static var groupedBackgroundTertiary: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.groupedBackgroundTertiary)
    }
    
    @objc public static var externalFacebook: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.externalFacebook)
    }
    
    @objc public static var externalTwitter: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.externalTwitter)
    }
    
    @objc public static var externalWhatsApp: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.externalWhatsApp)
    }
    
    @objc public static var external24HourBank: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.external24HourBank)
    }
    
    @objc public static var additional001: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.additional001)
    }
    
    @objc public static var additional002: DynamicColorObjcWrapper {
        DynamicColorObjcWrapper(dynamicColor: Colors.additional002)
    }
    
    /*
     There aren't linked color him still to `TemporaryColorType` typealias this is necessary,
     more information access design system documentation.
     */
    public typealias TemporaryColor = DynamicColorObjcWrapper
}
