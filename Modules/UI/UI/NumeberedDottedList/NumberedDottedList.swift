import UIKit

public final class NumberedDottedList: UIView, ViewConfiguration {
    fileprivate struct Layout {}
    
    private let sections: [String]
    private let spacing: CGFloat
    
    private lazy var itemsStackView: UIStackView = {
        let items = UIStackView()
        items.axis = .vertical
        
        for (index, name) in sections.enumerated() {
            let item = createSection(with: name, and: index + 1)
            items.addArrangedSubview(item)
        }
        
        return items
    }()
    
    public init(with sections: [String], spacing: CGFloat) {
        self.sections = sections
        self.spacing = spacing
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func buildViewHierarchy() {
        addSubview(itemsStackView)
    }
    
    public func setupConstraints() {
        itemsStackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    public func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
    
    private func createSection(with name: String, and number: Int) -> NumberedItem {
        let order: OrderNumberedItem
        
        switch number {
        case 1:
            order = .first
        case sections.count:
            order = .last
        default:
            order = .middle
        }
        
        return NumberedItem(with: name, and: number, order: order, spacing: spacing)
    }
}
