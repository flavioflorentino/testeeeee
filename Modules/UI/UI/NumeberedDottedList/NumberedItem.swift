import UIKit

enum OrderNumberedItem {
    case first
    case middle
    case last
}

private extension NumberedItem.Layout {
    enum Size {
        static let circle = CGSize(width: 28, height: 28)
        static let circleCorner: CGFloat = 14
    }
}

final class NumberedItem: UIView, ViewConfiguration {
    fileprivate struct Layout {}
    
    private let name: String
    private let number: Int
    private let order: OrderNumberedItem
    private let spacing: CGFloat
    
    private lazy var spacingView: UIView = {
        let view = UIView()
        let topSpacingPoint = CGPoint(x: Layout.Size.circle.width / 2, y: 0)
        let bottomSpacingPoint = CGPoint(x: Layout.Size.circle.width / 2, y: spacing)
        view.drawDottedLine(start: topSpacingPoint, end: bottomSpacingPoint, view: view)
        return view
    }()
    
    private lazy var circle: UIView = {
        let view = UIView()
        view.layer.borderWidth = 1
        view.layer.borderColor = Colors.branding400.color.cgColor
        view.layer.cornerRadius = Layout.Size.circleCorner
        return view
    }()
    
    private lazy var numberLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .branding400())
        label.text = "\(number)"
        return label
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale600())
        label.text = name
        return label
    }()
    
    private lazy var elementsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 0
        return stackView
    }()
    
    private lazy var backgroundStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    init(with name: String, and number: Int, order: OrderNumberedItem = .middle, spacing: CGFloat = 20) {
        self.name = name
        self.number = number
        self.order = order
        self.spacing = spacing
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        circle.addSubview(numberLabel)
        elementsStackView.addArrangedSubview(circle)
        if order != .last {
            elementsStackView.addArrangedSubview(spacingView)
        }
        addSubview(elementsStackView)
        addSubview(titleLabel)
    }
    
    func setupConstraints() {
        elementsStackView.snp.makeConstraints {
            $0.top.leading.bottom.equalToSuperview()
            $0.width.equalTo(Layout.Size.circle.width)
        }
        
        circle.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.size.equalTo(Layout.Size.circle)
        }
        
        if order != .last {
            spacingView.snp.makeConstraints {
                $0.top.equalTo(circle.snp.bottom)
                $0.leading.trailing.bottom.equalToSuperview()
                $0.height.equalTo(spacing)
            }
        }
        
        numberLabel.snp.makeConstraints {
            $0.centerY.centerX.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints {
            $0.leading.equalTo(elementsStackView.snp.trailing).offset(Spacing.base01)
            $0.trailing.equalToSuperview()
            $0.centerY.equalTo(circle.snp.centerY)
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
}
