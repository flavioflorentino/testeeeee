import Foundation
import SnapKit
import UIKit

public final class UIPPFloatingTextView: UIControl, MaskTextField {
    // MARK: Private
    override public var isSelected: Bool {
        didSet {
            updateBottomLine()
        }
    }
    
    private var lineHeightContraint: Constraint?
    private let bottomLine = UIView()
    private let topLabel = UILabel()
    private let infoButton = UIButton(type: .infoLight)
    
    // MARK: Public
    public var selectedLineColor = Palette.ppColorBranding300.color
    public var lineColor = UIColor.lightGray
    public var topText = "" {
        didSet {
            updateTopLabel()
        }
    }
    public var errorMessage: String? = "" {
        didSet {
            guard hasError else {
                updateTopLabel()
                updateBottomLine()
                return
            }
            topLabel.text = errorMessage
            bottomLine.backgroundColor = .red
            topLabel.textColor = .red
        }
    }
    public var hasError: Bool {
        errorMessage != nil && errorMessage?.isNotEmpty ?? false
    }
    public var maskString: String?
    public var maskblock: ((String, String) -> String)?
    public var maxlength: Int?
    public let textView = UITextView()
    public var infoButtonAction: (() -> Void)? {
        didSet {
            infoButton.isHidden = infoButtonAction == nil
        }
    }

    public weak var delegate: UITextViewDelegate?
    
    // MARK: Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    // MARK: Setup
    private func commonInit() {
        setupViews()
        setupConstraints()
    }
    
    private func setupViews() {
        addSubview(textView)
        addSubview(bottomLine)
        addSubview(topLabel)
        addSubview(infoButton)
        
        backgroundColor = .clear
        
        infoButton.addTarget(self, action: #selector(didTapInfo), for: .touchUpInside)
        
        bottomLine.backgroundColor = .lightGray
        
        textView.backgroundColor = .clear
        textView.textContainerInset = UIEdgeInsets(top: 3, left: 0, bottom: 3, right: 0)
        textView.textContainer.lineFragmentPadding = 0
        textView.isScrollEnabled = false
        textView.font = UIFont.systemFont(ofSize: 16)
        textView.delegate = self
        textView.keyboardType = .numberPad
        
        topLabel.font = UIFont.systemFont(ofSize: 13)
        topLabel.textColor = .lightGray
    }
    
    private func setupConstraints() {
        topLabel.snp.makeConstraints { make in
            make.top.equalTo(0)
            make.left.equalToSuperview()
            make.height.equalTo(17)
        }
        
        textView.snp.makeConstraints { make in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalTo(bottomLine.snp.top)
            make.top.equalTo(topLabel.snp.bottom)
        }
        
        bottomLine.snp.makeConstraints { make in
            self.lineHeightContraint = make.height.equalTo(1).constraint
            make.bottom.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
        }
        
        infoButton.snp.makeConstraints { make in
            make.left.equalTo(topLabel.snp.right).offset(5)
            make.size.equalTo(CGSize(width: 20, height: 20))
            make.centerY.equalTo(topLabel)
        }
    }
    
    // MARK: Helpers
    public func setupBilletMask() {
        maskblock = { _, newValue in
            // Handling user input and programmatically updated text field
            let maskDescriptor: BilletMaskDescriptor = newValue.first == "8" ? .dealership : .other
            let mask = CustomStringMask(descriptor: maskDescriptor)
            
            let nValue = mask.unmaskedText(from: newValue)
            return mask.maskedText(from: nValue) ?? ""
        }
    }
    
    private func updateTopLabel() {
        topLabel.text = topText
        topLabel.textColor = .lightGray
    }
    
    private func updateBottomLine() {
        let lineThickness = isSelected ? 2 : 1
        lineHeightContraint?.update(offset: lineThickness)
        bottomLine.backgroundColor = isSelected ? selectedLineColor : lineColor
        if hasError {
            bottomLine.backgroundColor = .red
        }
    }
    
    // MARK: Actions
    @objc
    private func didTapInfo() {
        infoButtonAction?()
    }
}

extension UIPPFloatingTextView: UITextViewDelegate {
    public func textViewDidBeginEditing(_ textView: UITextView) {
        delegate?.textViewDidBeginEditing?(textView)
        isSelected = true
    }
    
    public func textViewDidEndEditing(_ textView: UITextView) {
        delegate?.textViewDidEndEditing?(textView)
        isSelected = false
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if !isEnabled {
            return false
        }
        
        errorMessage = nil
        
        return delegate?.textView?(textView, shouldChangeTextIn: range, replacementText: text) ?? true
    }
}
