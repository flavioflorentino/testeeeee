import SkyFloatingLabelTextField
import UIKit

public protocol MaskTextField {
    var maskString: String? { get set }
    var maskblock: ((_ oldText: String, _ newText: String) -> String)? { get set }
    var maxlength: Int? { get set }
}

@IBDesignable
public class UIPPFloatingTextField: SkyFloatingLabelTextField, MaskTextField {
    public var maskString: String?
    public var maskblock: ((String, String) -> String)?
    public var maxlength: Int?

    public var alwaysShowPlaceholder = false

    override public func isTitleVisible() -> Bool {
        alwaysShowPlaceholder ? alwaysShowPlaceholder : super.isTitleVisible()
    }

    /// float textfield title formatter
    var picpayTitleFormatter: ((String) -> String) = { (text: String) -> String in
        text
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        self.titleFormatter = picpayTitleFormatter
        setupColors()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        self.titleFormatter = picpayTitleFormatter
        setupColors()
    }
    
    public func setupColors() {
        tintColor = Palette.ppColorBranding300.color
        textColor = Palette.ppColorGrayscale600.color
        placeholderColor = Palette.ppColorGrayscale400.color
        lineColor = Palette.ppColorGrayscale400.color
        titleColor = Palette.ppColorGrayscale400.color
        selectedLineColor = Palette.ppColorPositive300.color
        disabledColor = Palette.ppColorGrayscale400.color
        selectedTitleColor = Palette.ppColorGrayscale400.color(withCustomDark: .ppColorGrayscale000)
    }
}

public extension UIPPFloatingTextField {
    func defaultForm() {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        disabledColor = Palette.ppColorGrayscale400.color
        selectedLineColor = Palette.ppColorBranding300.color
        selectedTitleColor = Palette.ppColorGrayscale400.color
        font = UIFont.systemFont(ofSize: 16.0)
        textColor = Palette.ppColorGrayscale600.color
    }
}
