import Foundation
import UIKit

public protocol LoadingViewProtocol {
    var loadingView: LoadingView { get }
    func startLoadingView()
    func stopLoadingView()
}

public protocol LoadingViewOverlayNavigationProtocol: LoadingViewProtocol { }

public extension LoadingViewOverlayNavigationProtocol where Self: UIViewController {
    func startLoadingView() {
        guard let view = navigationController?.view else {
            return
        }
        
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        loadingView.alpha = 0
        view.addSubview(loadingView)
        NSLayoutConstraint.constraintAllEdges(from: loadingView, to: view)
        UIView.animate(
            withDuration: 0.3,
            animations: {
                self.loadingView.alpha = 1
            },
            completion: nil
        )
    }
}

public extension LoadingViewProtocol where Self: UIViewController {
    func startLoadingView() {
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        loadingView.alpha = 0
        view.addSubview(loadingView)
        NSLayoutConstraint.constraintAllEdges(from: loadingView, to: view)
        UIView.animate(
            withDuration: 0.3,
            animations: { [weak self] in
                self?.loadingView.alpha = 1
            },
            completion: nil
        )
    }
    
    func stopLoadingView() {
        UIView.animate(
            withDuration: 0.3,
            animations: { [weak self] in
                self?.loadingView.alpha = 0
            }, completion: { [weak self] completed in
                guard completed else { return }
                self?.loadingView.removeFromSuperview()
            }
        )
    }
}
