import UIKit

public class LoadingView: UIView {
    public var text: String? {
        didSet {
            textLabel.text = text
            textLabel.isHidden = false
        }
    }
    private lazy var textLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = true
        label.textColor = Palette.ppColorGrayscale600.color
        label.text = text
        label.font = UIFont.systemFont(ofSize: 18.0)
        label.isHidden = true
        return label
    }()
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.axis = .vertical
        stackView.spacing = 8.0
        return stackView
    }()
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activity = UIActivityIndicatorView(style: .whiteLarge)
        activity.translatesAutoresizingMaskIntoConstraints = false
        activity.color = Palette.ppColorGrayscale400.color
        activity.startAnimating()
        return activity
    }()

    override public init(frame: CGRect = .zero) {
        super.init(frame: frame)
        addComponents()
        layoutComponents()
        configureView()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setActivityIndicatorStyle(_ style: UIActivityIndicatorView.Style) {
        activityIndicator.style = style
    }
    
    public func setActivityIndicatorColor(_ color: UIColor?) {
        activityIndicator.color = color
    }
    
    public func setTextColor(_ color: UIColor?) {
        textLabel.textColor = color
    }
    
    // MARK: private methods
    
    private func configureView() {
        backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    func addComponents() {
        stackView.addArrangedSubview(textLabel)
        stackView.addArrangedSubview(activityIndicator)
        addSubview(stackView)
    }
    
    func layoutComponents() {
        textLabel.isHidden = true
        NSLayoutConstraint.activate([
            stackView.centerXAnchor.constraint(equalTo: centerXAnchor),
            stackView.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }
}
