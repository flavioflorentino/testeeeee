import Foundation

public protocol VerticalCalendarInteracting: AnyObject {
    func loadCalendar(selectedDate: Date, numberOfMonths: Int?)
    func select(day: VerticalCalendarDay)
}

public final class VerticalCalendarInteractor {
    private let presenter: VerticalCalendarPresenting
    private var numberOfMonths: Int = 3
    
    private weak var validator: VerticalCalendarValidatorDelegate?
    private weak var delegate: VerticalCalendarDelegate?
    
    var selectedDate: Date
    
    private let calendar = Calendar(identifier: .gregorian)
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d"
        return dateFormatter
    }()
    
    init(
        selectedDate: Date,
        presenter: VerticalCalendarPresenting,
        delegate: VerticalCalendarDelegate?,
        validator: VerticalCalendarValidatorDelegate?
    ) {
        self.selectedDate = selectedDate
        self.presenter = presenter
        self.delegate = delegate
        self.validator = validator
    }
}

extension VerticalCalendarInteractor: VerticalCalendarInteracting {
    public func loadCalendar(selectedDate: Date, numberOfMonths: Int? = nil) {
        self.selectedDate = selectedDate
        
        if let numberOfMonths = numberOfMonths {
            self.numberOfMonths = numberOfMonths
        }
        
        var baseDates: [Date] = []
        for month in 0...self.numberOfMonths - 1 {
            if let date = calendar.date(byAdding: .month, value: month, to: Date()) {
                baseDates.append(date)
            }
        }
        
        var configs: [VerticalCalendarMonthConfigurator] = []
        baseDates.forEach {
            configs.append(VerticalCalendarMonthConfigurator(baseDate: $0, days: createMonthDays(for: $0)))
        }
        
        presenter.displayDates(configs: configs)
    }
    
    public func select(day: VerticalCalendarDay) {
        guard day.isAvailable else { return }
        selectedDate = day.date
        delegate?.didSelectDate(date: day.date)
        loadCalendar(selectedDate: selectedDate, numberOfMonths: numberOfMonths)
    }
    
    func createMonth(for baseDate: Date) throws -> VerticalCalendarMonth {
        guard
            let numberOfDaysInMonth = calendar.range(of: .day, in: .month, for: baseDate)?.count,
            let firstDayOfMonth = calendar.date(from: calendar.dateComponents([.year, .month], from: baseDate))
        else {
            throw CalendarDataError.verticalCalendarMonthError
        }
        
        let firstDayWeekday = calendar.component(.weekday, from: firstDayOfMonth)
        
        return VerticalCalendarMonth(numberOfDays: numberOfDaysInMonth, firstDay: firstDayOfMonth, firstDayWeekday: firstDayWeekday)
    }
    
    func createMonthDays(for baseDate: Date) -> [VerticalCalendarDay] {
        guard let metadata = try? createMonth(for: baseDate) else {
            preconditionFailure("An error occurred when generating the metadata for \(baseDate)")
        }
        
        let numberOfDaysInMonth = metadata.numberOfDays
        let offsetInInitialRow = metadata.firstDayWeekday
        let firstDayOfMonth = metadata.firstDay
        
        var days: [VerticalCalendarDay] = (1..<(numberOfDaysInMonth + offsetInInitialRow))
            .map { day in
                let isWithinDisplayedMonth = day >= offsetInInitialRow
                let dayOffset = isWithinDisplayedMonth
                    ? day - offsetInInitialRow
                    : -(offsetInInitialRow - day)
                
                return createDay(offsetBy: dayOffset, for: firstDayOfMonth, isWithinDisplayedMonth: isWithinDisplayedMonth)
            }
        
        days += createStartOfNextMonth(using: firstDayOfMonth)
        
        return days
    }
    
    func createDay(offsetBy dayOffset: Int, for baseDate: Date, isWithinDisplayedMonth: Bool) -> VerticalCalendarDay {
        let date = calendar.date(byAdding: .day, value: dayOffset, to: baseDate) ?? baseDate
        
        let isAvailable = validator?.isAvailable(date: date) ?? true
        var isSelected = calendar.isDate(date, inSameDayAs: selectedDate)
        
        if isSelected && !isAvailable, let newDate = Calendar.current.date(byAdding: .day, value: 1, to: selectedDate) {
            selectedDate = newDate
            isSelected = false
        }
        
        return VerticalCalendarDay(
            date: date,
            number: dateFormatter.string(from: date),
            isSelected: isSelected,
            isWithinDisplayedMonth: isWithinDisplayedMonth,
            isAvailable: isAvailable
        )
    }
    
    func createStartOfNextMonth(using firstDayOfDisplayedMonth: Date) -> [VerticalCalendarDay] {
        guard let lastDayInMonth = calendar.date(byAdding: DateComponents(month: 1, day: -1), to: firstDayOfDisplayedMonth)
        else {
            return []
        }
        
        let additionalDays = 7 - calendar.component(.weekday, from: lastDayInMonth)
        guard additionalDays > 0 else { return [] }
        
        let days: [VerticalCalendarDay] = (1...additionalDays)
            .map {
                createDay(offsetBy: $0, for: lastDayInMonth, isWithinDisplayedMonth: false)
            }
        
        return days
    }
    
    enum CalendarDataError: Error {
        case verticalCalendarMonthError
    }
}
