import Foundation
import UIKit

public struct VerticalCalendarSection {
    let baseDate: Date
    let days: [VerticalCalendarDay]
    let displayCurrentMonthOnly: Bool
}

public protocol VerticalCalendarDisplaying: AnyObject {
    func displaySections(sections: [VerticalCalendarSection])
}

extension VerticalCalendarView: VerticalCalendarDisplaying {
    public func displaySections(sections: [VerticalCalendarSection]) {
        self.sections = sections
        collectionView.reloadData()
    }
}

private extension VerticalCalendarView.Layout {
    enum HeaderView {
        static let height: CGFloat = 42.0
    }
    
    enum SectionHeaderView {
        static let height: CGFloat = 60
    }
}

public final class VerticalCalendarView: UIView {
    fileprivate enum Layout { }
    
    private lazy var headerView = VerticalCalendarHeaderView()
    
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.showsVerticalScrollIndicator = false
        collectionView.backgroundColor = .clear
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(VerticalCalendarDateCell.self, forCellWithReuseIdentifier: VerticalCalendarDateCell.identifier)
        collectionView.register(
            VerticalCalendarSectionView.self,
            forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
            withReuseIdentifier: String(describing: VerticalCalendarSectionView.self)
        )
        return collectionView
    }()
    
    private var interactor: VerticalCalendarInteracting
    
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d"
        return dateFormatter
    }()
    
    private lazy var sections: [VerticalCalendarSection] = []
    
    public init(interactor: VerticalCalendarInteracting) {
        self.interactor = interactor
        
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func loadData(selectedDate: Date, numberOfMonths: Int?) {
        interactor.loadCalendar(selectedDate: selectedDate, numberOfMonths: numberOfMonths)
        collectionView.reloadData()
    }
}

extension VerticalCalendarView: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(headerView)
        addSubview(collectionView)
    }
    
    public func setupConstraints() {
        headerView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(Layout.HeaderView.height)
        }
        
        collectionView.snp.makeConstraints {
            $0.top.equalTo(headerView.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    public func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
}

extension VerticalCalendarView: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        sections.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        sections[section].days.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        guard kind == UICollectionView.elementKindSectionHeader else {
            return UICollectionReusableView()
        }
        
        guard let view = collectionView.dequeueReusableSupplementaryView(
            ofKind: kind,
            withReuseIdentifier: VerticalCalendarSectionView.identifier,
            for: indexPath
        ) as? VerticalCalendarSectionView else {
            return UICollectionReusableView()
        }
        
        view.setup(date: sections[indexPath.section].baseDate, isSeparatorHidden: indexPath.section == 0)
        
        return view
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        CGSize(width: collectionView.frame.width, height: Layout.SectionHeaderView.height)
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let day = sections[indexPath.section].days[indexPath.row]
        guard
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: VerticalCalendarDateCell.identifier, for: indexPath) as? VerticalCalendarDateCell
        else {
            return UICollectionViewCell()
        }
        
        cell.setup(with: day)
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let day = sections[indexPath.section].days[indexPath.row]
        interactor.select(day: day)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = Int(collectionView.frame.width / CGFloat(Calendar.current.weekdaySymbols.count))
        let height = width
        return CGSize(width: width, height: height)
    }
}
