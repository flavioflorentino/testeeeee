import Foundation

public protocol VerticalCalendarValidatorDelegate: AnyObject {
    func isAvailable(date: Date) -> Bool
}
