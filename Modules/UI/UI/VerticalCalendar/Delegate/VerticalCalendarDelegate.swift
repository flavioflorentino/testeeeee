import Foundation

public protocol VerticalCalendarDelegate: AnyObject {
    func didSelectDate(date: Date)
}
