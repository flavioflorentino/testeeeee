import Foundation

public enum VerticalCalendarFactory {
    public static func make(
        selectedDate: Date,
        delegate: VerticalCalendarDelegate? = nil,
        validator: VerticalCalendarValidatorDelegate? = nil
    ) -> VerticalCalendarView {
        let presenter = VerticalCalendarPresenter()
        let interactor = VerticalCalendarInteractor(selectedDate: selectedDate, presenter: presenter, delegate: delegate, validator: validator)
        let view = VerticalCalendarView(interactor: interactor)
        presenter.view = view
        return view
    }
}
