import Foundation

public protocol VerticalCalendarPresenting: AnyObject {
    var view: VerticalCalendarDisplaying? { get set }
    func displayDates(configs: [VerticalCalendarMonthConfigurator])
}

public struct VerticalCalendarMonthConfigurator {
    let baseDate: Date
    let days: [VerticalCalendarDay]
}

public final class VerticalCalendarPresenter: VerticalCalendarPresenting {
    public weak var view: VerticalCalendarDisplaying?
    
    public func displayDates(configs: [VerticalCalendarMonthConfigurator]) {
        var sections: [VerticalCalendarSection] = []
        
        configs.forEach { config in
            sections.append(
                VerticalCalendarSection(
                    baseDate: config.baseDate,
                    days: config.days,
                    displayCurrentMonthOnly: true
                )
            )
        }
        
        view?.displaySections(sections: sections)
    }
}
