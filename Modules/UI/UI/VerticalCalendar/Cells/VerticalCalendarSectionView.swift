import UIKit

final class VerticalCalendarSectionView: UICollectionReusableView {
    private lazy var separatorView = SeparatorView(style: BackgroundViewStyle(color: .grayscale100()))
    
    private lazy var monthYearLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, Colors.black.color)
        return label
    }()
    
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.locale = Locale(identifier: "pt_BR")
        dateFormatter.dateFormat = "MMMM yyyy"
        return dateFormatter
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(date: Date, isSeparatorHidden: Bool = false) {
        monthYearLabel.text = dateFormatter.string(from: date).capitalized
        separatorView.isHidden = isSeparatorHidden
    }
}

extension VerticalCalendarSectionView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(separatorView)
        addSubview(monthYearLabel)
    }
    
    func setupConstraints() {
        separatorView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
        }
        
        monthYearLabel.snp.makeConstraints {
            $0.top.equalTo(separatorView).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
        }
    }
}
