import UIKit

private extension VerticalCalendarDateCell.Layout {
    enum Size {
        static let selectionBackgroundView = CGSize(width: 40, height: 40)
        static let currentDateOutlineCircleView = CGSize(width: 40, height: 40)
    }
    
    enum BorderWidth {
        static let currentDateBackgroundView: CGFloat = 1.0
    }
}

final class VerticalCalendarDateCell: UICollectionViewCell {
    fileprivate enum Layout { }
    
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.locale = Locale(identifier: "pt_BR")
        dateFormatter.dateFormat = "ddMMMM"
        return dateFormatter
    }()
    
    private lazy var currentDateOutlineCircleView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.layer.masksToBounds = true
        view.layer.cornerRadius = Layout.Size.selectionBackgroundView.height / 2
        view.layer.borderWidth = Layout.BorderWidth.currentDateBackgroundView
        view.layer.borderColor = Colors.grayscale300.color.cgColor
        view.isHidden = true
        return view
    }()
    
    private lazy var selectionBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.branding600.color
        view.layer.masksToBounds = true
        view.layer.cornerRadius = Layout.Size.selectionBackgroundView.height / 2
        view.isHidden = true
        return view
    }()
    
    private lazy var numberLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.textColor, Colors.grayscale700.color)
            .with(\.textAlignment, .center)
        return label
    }()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        numberLabel.text = ""
        selectionBackgroundView.isHidden = true
        currentDateOutlineCircleView.isHidden = true
        numberLabel.textColor = Colors.grayscale700.color
    }
    
    private var day: VerticalCalendarDay?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(with day: VerticalCalendarDay) {
        self.day = day
        numberLabel.text = day.isWithinDisplayedMonth ? day.number : String()
        numberLabel.accessibilityLabel = dateFormatter.string(from: day.date)
        numberLabel.isAccessibilityElement = day.isWithinDisplayedMonth && day.isAvailable
        setupAvailability()
        setupTodayIfNeeded()
        selectDateIfNeeded()
    }
}

extension VerticalCalendarDateCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(currentDateOutlineCircleView)
        contentView.addSubview(selectionBackgroundView)
        contentView.addSubview(numberLabel)
    }
    
    func setupConstraints() {
        currentDateOutlineCircleView.snp.makeConstraints {
            $0.center.equalTo(contentView)
            $0.size.equalTo(Layout.Size.currentDateOutlineCircleView)
        }
        
        selectionBackgroundView.snp.makeConstraints {
            $0.center.equalTo(contentView)
            $0.size.equalTo(Layout.Size.selectionBackgroundView)
        }
        
        numberLabel.snp.makeConstraints {
            $0.edges.equalTo(contentView)
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
}

private extension VerticalCalendarDateCell {
    func setupTodayIfNeeded() {
        if let day = day, day.isToday, day.isWithinDisplayedMonth {
            currentDateOutlineCircleView.isHidden = false
        }
    }
    
    func setupAvailability() {
        if let day = day, !day.isAvailable {
            numberLabel.textColor = Colors.grayscale300.color
        }
    }
    
    func selectDateIfNeeded() {
        if let day = day, day.isWithinDisplayedMonth, day.isSelected {
            selectionBackgroundView.isHidden = false
            numberLabel.textColor = Colors.white.color
        }
    }
}
