import UIKit

final class VerticalCalendarHeaderView: UIView {
    private lazy var dayOfWeekStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "pt_BR")
        return dateFormatter
    }()
    
    init() {
        super.init(frame: CGRect.zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension VerticalCalendarHeaderView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(dayOfWeekStackView)
    }
    
    func setupConstraints() {
        dayOfWeekStackView.snp.makeConstraints {
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    func configureViews() {
        dateFormatter.calendar.veryShortWeekdaySymbols.forEach({
            let dayLabel = UILabel()
            dayLabel.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
                .with(\.textColor, Colors.black.color)
                .with(\.textAlignment, .center)
            dayLabel.text = $0
            dayLabel.accessibilityElementsHidden = true
            dayOfWeekStackView.addArrangedSubview(dayLabel)
        })
    }
}
