import Foundation

public struct VerticalCalendarMonth {
    let numberOfDays: Int
    let firstDay: Date
    let firstDayWeekday: Int
}
