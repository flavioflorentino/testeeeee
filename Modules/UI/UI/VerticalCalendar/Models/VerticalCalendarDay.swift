import Foundation

public struct VerticalCalendarDay {
    let date: Date
    let number: String
    let isSelected: Bool
    let isWithinDisplayedMonth: Bool
    let isAvailable: Bool
    
    var isToday: Bool {
        Calendar.current.isDateInToday(date)
    }
}
