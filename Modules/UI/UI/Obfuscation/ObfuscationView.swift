import Foundation
import UIKit

public enum AppType {
    case biz
    case personal
}

extension ObfuscationView.Layout {
    enum PersonalLogoImage {
        static let imageWidth: CGFloat = 190.0
        static let widthHeightMultiplier: CGFloat = 0.3
        static let centerYMultiplier: CGFloat = 0.65
    }
    
    enum BizLogoImage {
        static let imageWidth: CGFloat = 132.5
        static let imageHeight: CGFloat = 80.5
    }
}

public final class ObfuscationView: UIView {
    fileprivate enum Layout { }
    
    private var logoImageView: UIImageView
    private var appType: AppType
    
    public init(imageView: UIImageView, appType: AppType, frame: CGRect = .zero) {
        logoImageView = imageView
        self.appType = appType
        super.init(frame: frame)
        self.accessibilityIdentifier = "obfuscationView"
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ObfuscationView: ViewConfiguration {
    public func setupConstraints() {
        appType == .personal ? personalSetup() : bizSetup()
    }
    
    public func buildViewHierarchy() {
        addSubview(logoImageView)
    }
    
    public func configureViews() {
        let color = appType == .personal ? Palette.ppColorBranding300.color : Palette.ppColorDarkGreen.color
        backgroundColor = color
    }
    
    private func personalSetup() {
        logoImageView.layout {
            $0.centerX == centerXAnchor
            $0.width == Layout.PersonalLogoImage.imageWidth
            $0.height == logoImageView.widthAnchor * Layout.PersonalLogoImage.widthHeightMultiplier
        }
        
        let centerYConstraint = NSLayoutConstraint(
            item: logoImageView,
            attribute: .centerY,
            relatedBy: .equal,
            toItem: self,
            attribute: .centerY,
            multiplier: Layout.PersonalLogoImage.centerYMultiplier,
            constant: 0
        )
        centerYConstraint.isActive = true
    }
    
    // todo: Quando implementar no biz ele será ajustado de acordo.
    private func bizSetup() {
        logoImageView.layout {
            $0.centerX == centerXAnchor
            $0.centerY == centerYAnchor
            $0.width == Layout.BizLogoImage.imageWidth
            $0.height == Layout.BizLogoImage.imageHeight
        }
    }
}
