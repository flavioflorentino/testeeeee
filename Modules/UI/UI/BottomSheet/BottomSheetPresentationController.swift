import UIKit

enum ModalScaleState {
    case presentation
    case interaction
}

public final class BottomSheetPresentationController: UIPresentationController {
    private var direction: CGFloat = 0
    private var state: ModalScaleState = .interaction
    private lazy var dimmingView: UIView = {
        let view = UIView(frame: containerView?.bounds ?? .zero)
        view.backgroundColor = UIColor.black.withAlphaComponent(0.65)
        view.addGestureRecognizer(
            UITapGestureRecognizer(target: self, action: #selector(didTap(_:)))
        )
        return view
    }()
    
    override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        
        presentedViewController.view.addGestureRecognizer(
            UIPanGestureRecognizer(target: self, action: #selector(didPan(_:)))
        )
    }
    
    @objc
    func didPan(_ gesture: UIPanGestureRecognizer) {
        guard
            let view = gesture.view,
            let superView = view.superview,
            let presented = presentedView,
            let container = containerView
            else {
                return
        }
        
        let location = gesture.translation(in: superView)
        
        switch gesture.state {
        case .began:
            presented.frame.size.height = container.frame.height
        case .changed:
            let velocity = gesture.velocity(in: superView)
            
            switch state {
            case .interaction:
                presented.frame.origin.y =
                    location.y + (container.bounds.height - presentedViewController.preferredContentSize.height)
            case .presentation:
                presented.frame.origin.y = location.y
            }
            direction = velocity.y
        case .ended:
            let maxPresentedY = container.frame.height - presentedViewController.preferredContentSize.height
            switch presented.frame.origin.y {
            case 0...maxPresentedY:
                changeScale(to: .interaction)
            default:
                presentedViewController.dismiss(animated: true)
            }
        default:
            break
        }
    }
    
    @objc
    func didTap(_ gesture: UITapGestureRecognizer) {
        presentedViewController.dismiss(animated: true, completion: nil)
    }
    
    private func changeScale(to state: ModalScaleState) {
        guard let presented = presentedView else {
            return
        }
        
        UIView.animate(
            withDuration: 0.5,
            delay: 0,
            usingSpringWithDamping: 0.8,
            initialSpringVelocity: 0.8,
            options: [.curveEaseInOut, .allowUserInteraction],
            animations: { [weak self] in
                presented.frame = self?.frameOfPresentedViewInContainerView ?? CGRect.zero
            }, completion: { _ in
                self.state = state
            }
        )
    }
    
    override public var frameOfPresentedViewInContainerView: CGRect {
        guard let container = containerView else {
            return .zero
        }
        let size = presentedViewController.preferredContentSize
        return CGRect(
            x: 0,
            y: container.bounds.height - size.height,
            width: container.bounds.width,
            height: container.bounds.height
        )
    }
    
    override public func presentationTransitionWillBegin() {
        guard
            let container = containerView,
            let coordinator = presentingViewController.transitionCoordinator
            else {
                return
        }
        
        dimmingView.alpha = 0
        container.addSubview(dimmingView)
        dimmingView.addSubview(presentedViewController.view)
        
        coordinator.animate(alongsideTransition: { [weak self] _ in
            self?.dimmingView.alpha = 1
        })
    }
    
    override public func dismissalTransitionWillBegin() {
        guard let coordinator = presentingViewController.transitionCoordinator else {
            return
        }
        
        coordinator.animate(alongsideTransition: { [weak self] _ in
            self?.dimmingView.alpha = 0
        })
    }
    
    override public func dismissalTransitionDidEnd(_ completed: Bool) {
        if completed {
            dimmingView.removeFromSuperview()
        }
    }
}
