import Foundation

// swiftlint:disable convenience_type
final class UIResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: UIResources.self).url(forResource: "UIResources", withExtension: "bundle") else {
            return Bundle(for: UIResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: UIResources.self)
    }()
}
