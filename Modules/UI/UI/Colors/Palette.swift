import UIKit

// swiftlint:disable:next superfluous_disable_command type_body_length
public enum Palette: String {
    case ppColorBranding100
    case ppColorBranding200
    case ppColorBranding300
    case ppColorBranding400
    case ppColorBranding500
    case ppColorBranding600

    case ppColorPositive100
    case ppColorPositive200
    case ppColorPositive300
    case ppColorPositive400
    case ppColorPositive500
    case ppColorPositive600
    case ppColorPositive700
    
    case ppColorNegative100
    case ppColorNegative200
    case ppColorNegative300
    case ppColorNegative400
    case ppColorNegative500
    
    case ppColorNeutral100
    case ppColorNeutral200
    case ppColorNeutral300
    case ppColorNeutral400
    case ppColorNeutral500
    
    case ppColorAttentive100
    case ppColorAttentive200
    case ppColorAttentive300
    case ppColorAttentive400
    case ppColorAttentive500
    
    case ppColorGrayscale000
    case ppColorGrayscale100
    case ppColorGrayscale200
    case ppColorGrayscale300
    case ppColorGrayscale400
    case ppColorGrayscale500
    case ppColorGrayscale550
    case ppColorGrayscale600
    case ppColorGrayscale700
    
    case ppColorBrandingPlus300
    case ppColorBrandingPlus400
    case ppColorGrayscalePlus
    
    case ppColorLightGreen
    case ppColorDarkGreen
    
    case ppColorCardGray500
    
    case animatedBorderPink
    case animatedBorderPurple
    
    case tooltip
    case black
    case white
    
    public typealias GradientBicolor = (from: UIColor, to: UIColor)
    private typealias ColorPair = (light: UIColor, dark: UIColor?)
    
    public var color: UIColor {
        let pair: ColorPair
        
        switch self {
        case .ppColorBranding100:
            pair = (light: #colorLiteral(red: 0.1019607843, green: 0.8901960784, blue: 0.6549019608, alpha: 1), dark: #colorLiteral(red: 0.1019607843, green: 0.8901960784, blue: 0.6549019608, alpha: 1)) // #1AE3A7 | #1AE3A7
        case .ppColorBranding200:
            pair = (light: #colorLiteral(red: 0.07450980392, green: 0.8078431373, blue: 0.4901960784, alpha: 1), dark: #colorLiteral(red: 0.07450980392, green: 0.8078431373, blue: 0.4901960784, alpha: 1)) // #13CE7D | #13CE7D
        case .ppColorBranding300:
            pair = (light: #colorLiteral(red: 0.06666666667, green: 0.7803921569, blue: 0.4352941176, alpha: 1), dark: #colorLiteral(red: 0.06666666667, green: 0.7803921569, blue: 0.4352941176, alpha: 1)) // #11C76F | #11C76F
        case .ppColorBranding400:
            pair = (light: #colorLiteral(red: 0, green: 0.6588235294, blue: 0.1960784314, alpha: 1), dark: #colorLiteral(red: 0, green: 0.6588235294, blue: 0.1960784314, alpha: 1)) // #00A832 | #00A832
        case .ppColorBranding500:
            pair = (light: #colorLiteral(red: 0, green: 0.5019607843, blue: 0, alpha: 1), dark: #colorLiteral(red: 0, green: 0.5019607843, blue: 0, alpha: 1)) // #008000 | #008000
        case .ppColorBranding600:
            pair = (light: #colorLiteral(red: 0.035, green: 0.247, blue: 0.196, alpha: 1), dark: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)) // #093F32 | #FFFFFF

        case .ppColorPositive100:
            pair = (light: #colorLiteral(red: 0.1019607843, green: 0.8901960784, blue: 0.6549019608, alpha: 1), dark: #colorLiteral(red: 0.1019607843, green: 0.8901960784, blue: 0.6549019608, alpha: 1)) // #1AE3A7 | #1AE3A7
        case .ppColorPositive200:
            pair = (light: #colorLiteral(red: 0.07450980392, green: 0.8078431373, blue: 0.4901960784, alpha: 1), dark: #colorLiteral(red: 0.07450980392, green: 0.8078431373, blue: 0.4901960784, alpha: 1)) // #13CE7D | #13CE7D
        case .ppColorPositive300:
            pair = (light: #colorLiteral(red: 0.06666666667, green: 0.7803921569, blue: 0.4352941176, alpha: 1), dark: #colorLiteral(red: 0.06666666667, green: 0.7803921569, blue: 0.4352941176, alpha: 1)) // #11C76F | #11C76F
        case .ppColorPositive400:
            pair = (light: #colorLiteral(red: 0, green: 0.6588235294, blue: 0.1960784314, alpha: 1), dark: #colorLiteral(red: 0, green: 0.6588235294, blue: 0.1960784314, alpha: 1)) // #00A832 | #00A832
        case .ppColorPositive500:
            pair = (light: #colorLiteral(red: 0, green: 0.5019607843, blue: 0, alpha: 1), dark: #colorLiteral(red: 0, green: 0.5019607843, blue: 0, alpha: 1)) // #008000 | #008000
        case .ppColorPositive600:
            pair = (light: #colorLiteral(red: 0.137254902, green: 0.2588235294, blue: 0.2274509804, alpha: 1), dark: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)) // #23423A | #FFFFFF
        case .ppColorPositive700:
            pair = (light: #colorLiteral(red: 0.6196078431, green: 0.6941176471, blue: 0.6352941176, alpha: 1), dark: #colorLiteral(red: 0.5607843137, green: 0.5725490196, blue: 0.6156862745, alpha: 1)) // #9EB1A2 | #8F929D
            
        case .ppColorNegative100:
            pair = (light: #colorLiteral(red: 1, green: 0.6705882353, blue: 0.8117647059, alpha: 1), dark: #colorLiteral(red: 1, green: 0.6705882353, blue: 0.8117647059, alpha: 1)) // #FFABCF | #FFABCF
        case .ppColorNegative200:
            pair = (light: #colorLiteral(red: 1, green: 0.4274509804, blue: 0.5843137255, alpha: 1), dark: #colorLiteral(red: 1, green: 0.4274509804, blue: 0.5843137255, alpha: 1)) // #FF6D95 | #FF6D95
        case .ppColorNegative300:
            pair = (light: #colorLiteral(red: 1, green: 0.3137254902, blue: 0.4588235294, alpha: 1), dark: #colorLiteral(red: 1, green: 0.3137254902, blue: 0.4588235294, alpha: 1)) // #FF5075 | #FF5075
        case .ppColorNegative400:
            pair = (light: #colorLiteral(red: 1, green: 0.1254901961, blue: 0.2823529412, alpha: 1), dark: #colorLiteral(red: 1, green: 0.1254901961, blue: 0.2823529412, alpha: 1)) // #FF2048 | #FF2048
        case .ppColorNegative500:
            pair = (light: #colorLiteral(red: 1, green: 0.03137254902, blue: 0.1568627451, alpha: 1), dark: #colorLiteral(red: 1, green: 0.03137254902, blue: 0.1568627451, alpha: 1)) // #FF0828 | #FF0828
            
        case .ppColorNeutral100:
            pair = (light: #colorLiteral(red: 0, green: 0.8431372549, blue: 0.9647058824, alpha: 1), dark: #colorLiteral(red: 0, green: 0.8431372549, blue: 0.9647058824, alpha: 1)) // #00D7F6 | #00D7F6
        case .ppColorNeutral200:
            pair = (light: #colorLiteral(red: 0, green: 0.7254901961, blue: 0.9411764706, alpha: 1), dark: #colorLiteral(red: 0, green: 0.7254901961, blue: 0.9411764706, alpha: 1)) // #00B9F0 | #00B9F0
        case .ppColorNeutral300:
            pair = (light: #colorLiteral(red: 0, green: 0.6862745098, blue: 0.9333333333, alpha: 1), dark: #colorLiteral(red: 0, green: 0.6862745098, blue: 0.9333333333, alpha: 1)) // #00AFEE | #00AFEE
        case .ppColorNeutral400:
            pair = (light: #colorLiteral(red: 0, green: 0.4509803922, blue: 0.8823529412, alpha: 1), dark: #colorLiteral(red: 0, green: 0.4509803922, blue: 0.8823529412, alpha: 1)) // #0073E1 | #0073E1
        case .ppColorNeutral500:
            pair = (light: #colorLiteral(red: 0, green: 0.1960784314, blue: 0.8039215686, alpha: 1), dark: #colorLiteral(red: 0, green: 0.1960784314, blue: 0.8039215686, alpha: 1)) // #0032CD | #0032CD
            
        case .ppColorAttentive100:
            pair = (light: #colorLiteral(red: 0.9960784314, green: 0.9490196078, blue: 0.4509803922, alpha: 1), dark: #colorLiteral(red: 0.9960784314, green: 0.9490196078, blue: 0.4509803922, alpha: 1)) // #FEF273 | #FEF273
        case .ppColorAttentive200:
            pair = (light: #colorLiteral(red: 0.9882352941, green: 0.8431372549, blue: 0.3215686275, alpha: 1), dark: #colorLiteral(red: 0.9882352941, green: 0.8431372549, blue: 0.3215686275, alpha: 1)) // #FCD752 | #FCD752
        case .ppColorAttentive300:
            pair = (light: #colorLiteral(red: 0.9843137255, green: 0.7921568627, blue: 0.2588235294, alpha: 1), dark: #colorLiteral(red: 0.9843137255, green: 0.7921568627, blue: 0.2588235294, alpha: 1)) // #FBCA42 | #FBCA42
        case .ppColorAttentive400:
            pair = (light: #colorLiteral(red: 0.968627451, green: 0.6784313725, blue: 0.03529411765, alpha: 1), dark: #colorLiteral(red: 0.968627451, green: 0.6784313725, blue: 0.03529411765, alpha: 1)) // #F7AD09 | #F7AD09
        case .ppColorAttentive500:
            pair = (light: #colorLiteral(red: 0.9607843137, green: 0.5254901961, blue: 0, alpha: 1), dark: #colorLiteral(red: 0.9607843137, green: 0.5254901961, blue: 0, alpha: 1)) // #F58600 | #F58600
            
        case .ppColorGrayscale000:
            pair = (light: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), dark: #colorLiteral(red: 0.1176470588, green: 0.137254902, blue: 0.1647058824, alpha: 1)) // #FFFFFF | #1E232A
        case .ppColorGrayscale100:
            pair = (light: #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9647058824, alpha: 1), dark: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)) // #F4F4F6 | #000000
        case .ppColorGrayscale200:
            pair = (light: #colorLiteral(red: 0.9137254902, green: 0.9254901961, blue: 0.9294117647, alpha: 1), dark: #colorLiteral(red: 0.2392156863, green: 0.2666666667, blue: 0.3176470588, alpha: 1)) // #E9ECED | #3D4451
        case .ppColorGrayscale300:
            pair = (light: #colorLiteral(red: 0.8509803922, green: 0.862745098, blue: 0.8901960784, alpha: 1), dark: #colorLiteral(red: 0.5607843137, green: 0.5725490196, blue: 0.6156862745, alpha: 1)) // #D9DCE3 | #8F929D
        case .ppColorGrayscale400:
            pair = (light: #colorLiteral(red: 0.5607843137, green: 0.5725490196, blue: 0.6156862745, alpha: 1), dark: #colorLiteral(red: 0.8509803922, green: 0.862745098, blue: 0.8901960784, alpha: 1)) // #8F929D | #D9DCE3
        case .ppColorGrayscale500:
            pair = (light: #colorLiteral(red: 0.2392156863, green: 0.2666666667, blue: 0.3176470588, alpha: 1), dark: #colorLiteral(red: 0.9137254902, green: 0.9254901961, blue: 0.9294117647, alpha: 1)) // #3D4451 | #E9ECED
        case .ppColorGrayscale600:
            pair = (light: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), dark: #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9647058824, alpha: 1)) // #000000 | #F4F4F6
        case .ppColorGrayscale700:
            pair = (light: #colorLiteral(red: 0.7228445411, green: 0.7437271476, blue: 0.7507414222, alpha: 1), dark: #colorLiteral(red: 0.7228445411, green: 0.7437271476, blue: 0.7507414222, alpha: 1)) // #AAB0B2 | #AAB0B2
            
        case .ppColorLightGreen:
            pair = (light: #colorLiteral(red: 0.6470588235, green: 0.937254902, blue: 0.4941176471, alpha: 1), dark: #colorLiteral(red: 0.6470588235, green: 0.937254902, blue: 0.4941176471, alpha: 1)) // #A5EF7E | #A5EF7E
        case .ppColorDarkGreen:
            pair = (light: #colorLiteral(red: 0.05490196078, green: 0.4823529412, blue: 0.2196078431, alpha: 1), dark: #colorLiteral(red: 0.05490196078, green: 0.4823529412, blue: 0.2196078431, alpha: 1)) // #0E7B38 | #0E7B38

        case .tooltip:
            pair = (light: #colorLiteral(red: 0, green: 0.6862745098, blue: 0.9333333333, alpha: 1), dark: #colorLiteral(red: 0.2392156863, green: 0.2666666667, blue: 0.3176470588, alpha: 1)) // #00AFEE | #3D4451
        case .ppColorCardGray500:
            pair = (light: #colorLiteral(red: 0.1764705882, green: 0.1843137255, blue: 0.168627451, alpha: 1), dark: #colorLiteral(red: 0.1764705882, green: 0.1843137255, blue: 0.168627451, alpha: 1)) // #2D2F2B | #2D2F2B
            
        case .animatedBorderPink:
            pair = (light: #colorLiteral(red: 1, green: 0.5215686275, blue: 0.9803921569, alpha: 1), dark: #colorLiteral(red: 1, green: 0.5215686275, blue: 0.9803921569, alpha: 1)) // #FF85FA | #FF85FA
        case .animatedBorderPurple:
            pair = (light: #colorLiteral(red: 0.3882352941, green: 0.5490196078, blue: 1, alpha: 1), dark: #colorLiteral(red: 0.3882352941, green: 0.5490196078, blue: 1, alpha: 1)) // #638CFF | #638CFF
            
        case .black:
            pair = (light: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), dark: nil)
        case .white:
            pair = (light: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), dark: nil)

        default:
            return .black
        }
        
        return interfaceStyleColor(pair)
    }
     
    public var gradientColor: GradientBicolor {
        let from: ColorPair
        let to: ColorPair
        
        switch self {
        case .ppColorBrandingPlus300:
            from = (light: #colorLiteral(red: 0.04705882353, green: 0.9098039216, blue: 0.4901960784, alpha: 1), dark: #colorLiteral(red: 0.04705882353, green: 0.9098039216, blue: 0.4901960784, alpha: 1)) // #0CE87D | #0CE87D
            to = (light: #colorLiteral(red: 0.05882352941, green: 0.6823529412, blue: 0.3803921569, alpha: 1), dark: #colorLiteral(red: 0.05882352941, green: 0.6823529412, blue: 0.3803921569, alpha: 1)) // #0FAE61 | #0FAE61
        case .ppColorBrandingPlus400:
            from = (light: #colorLiteral(red: 0.1647058824, green: 0.4431372549, blue: 0.431372549, alpha: 1), dark: #colorLiteral(red: 0.1647058824, green: 0.4431372549, blue: 0.431372549, alpha: 1)) // #2A716E | #2A716E
            to = (light: #colorLiteral(red: 0.0431372549, green: 0.168627451, blue: 0.1803921569, alpha: 1), dark: #colorLiteral(red: 0.0431372549, green: 0.168627451, blue: 0.1803921569, alpha: 1)) // #0B2B2E | #0B2B2E
        case .ppColorGrayscalePlus:
            from = (light: #colorLiteral(red: 0.7254901961, green: 0.7254901961, blue: 0.7254901961, alpha: 1), dark: #colorLiteral(red: 0.7254901961, green: 0.7254901961, blue: 0.7254901961, alpha: 1)) // #B9B9B9 | #B9B9B9
            to = (light: #colorLiteral(red: 0.5333333333, green: 0.5333333333, blue: 0.5333333333, alpha: 1), dark: #colorLiteral(red: 0.5333333333, green: 0.5333333333, blue: 0.5333333333, alpha: 1)) // #888888 | #888888
        default:
            return GradientBicolor(from: .black, to: .white)
        }
        
        return GradientBicolor(from: interfaceStyleColor(from), to: interfaceStyleColor(to))
    }
    
    public var cgColor: CGColor {
        color.cgColor
    }
    
    public func color(withCustomDark customDark: Dark? = nil) -> UIColor {
        if #available(iOS 13.0, *) {
            return UIColor { traitCollection in
                switch traitCollection.userInterfaceStyle {
                case .dark:
                    return customDark?.color ?? self.color
                default:
                    return self.color
                }
            }
        }
        return color
    }
    
    private func interfaceStyleColor(_ color: ColorPair) -> UIColor {
        if #available(iOS 13.0, *) {
            return UIColor(dynamicProvider: { (traitColletion: UITraitCollection) -> UIColor in
                switch traitColletion.userInterfaceStyle {
                case .dark:
                    return color.dark ?? color.light
                default:
                    return color.light
                }
            })
        }
        return color.light
    }
    
    public static func hexColor(with hexStr: String, mapToDarkMode: Bool = true) -> UIColor? {
        if #available(iOS 13.0, *), mapToDarkMode {
            return UIColor(dynamicProvider: { (traitColletion: UITraitCollection) -> UIColor in
                switch traitColletion.userInterfaceStyle {
                case .dark:
                    return self.hexFromDark(hexStr)
                case .light, .unspecified:
                    return hexFromLight(hexStr)
                @unknown default:
                    return hexFromLight(hexStr)
                }
            })
        }
        
        guard !hexStr.isEmpty, let rgb = Int(hexStr.replacingOccurrences(of: "#", with: ""), radix: 16) else {
            return nil
        }
        return colorFromRgbInt(rgb)
    }
    
    private static func hexFromLight(_ hexStr: String) -> UIColor {
        var finalColor = ""
        switch hexStr.uppercased() {
        case "#8F929D":
            finalColor = "#989898"
        case "#21C25E":
            finalColor = "#11C76F"
        case "#1E232A":
            finalColor = "#FFFFFF"
        case "#000000":
            finalColor = "#F4F4F6"
        case "#3D4451":
            finalColor = "#E9ECED"
        case "#F4F4F6":
            finalColor = "#00000"
        case "#F4F7FA":
            finalColor = "#F4F4F6"
        default:
            finalColor = hexStr
        }
        guard let rgb = Int(finalColor.replacingOccurrences(of: "#", with: ""), radix: 16) else {
            return Palette.ppColorGrayscale000.color
        }
        return colorFromRgbInt(rgb)
    }
    // swiftlint:disable:next cyclomatic_complexity
    private static func hexFromDark(_ hexStr: String) -> UIColor {
        var finalColor = ""
        switch hexStr.uppercased() {
        case "#666666":
            finalColor = "#E9ECED"
        case "#989898":
            finalColor = "#8F929D"
        case "#BBBBBB":
            finalColor = "#8F929D"
        case "#21C25E":
            finalColor = "#11C76F"
        case "#FFFFFF":
            finalColor = "#1E232A"
        case "#F4F4F6":
            finalColor = "#000000"
        case "#E9ECED":
            finalColor = "#3D4451"
        case "#D9DCE3":
            finalColor = "#8F929D"
        case "#000000", "#4A4A4A", "#8F9D9D":
            finalColor = "#F4F4F6"
        case "#F4F7FA":
            finalColor = "#000000"
        default:
            finalColor = hexStr
        }
        guard let rgb = Int(finalColor.replacingOccurrences(of: "#", with: ""), radix: 16) else {
            return Palette.ppColorGrayscale600.color
        }
        return colorFromRgbInt(rgb)
    }
    
    private static func colorFromRgbInt(_ rgb: Int) -> UIColor {
        UIColor(
            red: CGFloat((rgb >> 16) & 0xFF) / 255,
            green: CGFloat((rgb >> 8) & 0xFF) / 255,
            blue: CGFloat(rgb & 0xFF) / 255,
            alpha: 1
        )
    }
}

public extension Palette {
    enum Dark {
        case ppColorGrayscale000
        case ppColorGrayscale100
        case ppColorGrayscale200
        case ppColorGrayscale300
        case ppColorGrayscale400
        case ppColorGrayscale500
        case ppColorGrayscale550
        case ppColorGrayscale600
        case ppColorGrayscale000Dark
        case ppColorBranding200
        case ppColorBranding300
        
        case tabBar
        
        var color: UIColor {
            switch self {
            case .ppColorGrayscale000:
                return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) // #FFFFFF
            case .ppColorGrayscale100:
                return #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9647058824, alpha: 1) // #F4F4F6
            case .ppColorGrayscale200:
                return #colorLiteral(red: 0.9137254902, green: 0.9254901961, blue: 0.9294117647, alpha: 1) // #E9ECED
            case .ppColorGrayscale300:
                return #colorLiteral(red: 0.6284515858, green: 0.6405263543, blue: 0.6784256697, alpha: 1) //
            case .ppColorGrayscale400:
                return #colorLiteral(red: 0.5607843137, green: 0.5725490196, blue: 0.6156862745, alpha: 1) // #8F929D
            case .ppColorGrayscale500:
                return #colorLiteral(red: 0.2392156863, green: 0.2666666667, blue: 0.3176470588, alpha: 1) // #3D4451
            case .ppColorGrayscale550:
                return #colorLiteral(red: 0.1450980392, green: 0.1568627451, blue: 0.1607843137, alpha: 1) // 252829
            case .ppColorGrayscale600:
                return #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1) // #000000
            case .ppColorBranding200:
                return #colorLiteral(red: 0.07450980392, green: 0.8078431373, blue: 0.4901960784, alpha: 1) // #13CE7D
            case .ppColorBranding300:
                return #colorLiteral(red: 0.06666666667, green: 0.7803921569, blue: 0.4352941176, alpha: 1) // #11C76F
            case .ppColorGrayscale000Dark:
                return #colorLiteral(red: 0.1176470588, green: 0.137254902, blue: 0.1647058824, alpha: 1) // #3D4451
            case .tabBar:
                return #colorLiteral(red: 0.07058823529, green: 0.07843137255, blue: 0.09411764706, alpha: 1) // #121418
            }
        }
    }
}

public extension Palette {
    enum SocialMediaColor {
        case facebook
        case twitter
        case whatsapp
        
        public var color: UIColor {
            switch self {
            case .facebook:
                return #colorLiteral(red: 0.231372549, green: 0.3490196078, blue: 0.5960784314, alpha: 1) // #3B5998
            case .twitter:
                return #colorLiteral(red: 0.3333333333, green: 0.6745098039, blue: 0.9333333333, alpha: 1) // #55ACEE
            case .whatsapp:
                return #colorLiteral(red: 0.1450980392, green: 0.8274509804, blue: 0.4, alpha: 1) // #25D366
            }
        }
    }
    
    enum PartnersColor {
        public static var banco24horas: UIColor {
            #colorLiteral(red: 0.8549019608, green: 0.1450980392, blue: 0.1098039216, alpha: 1) // #DA251C
        }
    }
}

// class to support Objective-C code
public class PaletteObjc: NSObject {
    override private init() {}
    
    @objc
    public class func ppColorBranding100() -> UIColor {
        Palette.ppColorBranding100.color
    }
    @objc
    public class func ppColorBranding200() -> UIColor {
        Palette.ppColorBranding200.color
    }
    @objc
    public class func ppColorBranding300() -> UIColor {
        Palette.ppColorBranding300.color
    }
    @objc
    public class func ppColorBranding400() -> UIColor {
        Palette.ppColorBranding400.color
    }
    @objc
    public class func ppColorBranding500() -> UIColor {
        Palette.ppColorBranding500.color
    }
    @objc
    public class func ppColorBranding600() -> UIColor {
        Palette.ppColorBranding600.color
    }
    
    @objc
    public class func ppColorNeutral100() -> UIColor {
        Palette.ppColorNeutral100.color
    }
    @objc
    public class func ppColorNeutral200() -> UIColor {
        Palette.ppColorNeutral200.color
    }
    @objc
    public class func ppColorNeutral300() -> UIColor {
        Palette.ppColorNeutral300.color
    }
    @objc
    public class func ppColorNeutral400() -> UIColor {
        Palette.ppColorNeutral400.color
    }
    @objc
    public class func ppColorNeutral500() -> UIColor {
        Palette.ppColorNeutral500.color
    }
    
    @objc
    public class func ppColorPositive300() -> UIColor {
        Palette.ppColorPositive300.color
    }
    
    @objc
    public class func ppColorAttentive100() -> UIColor {
        Palette.ppColorAttentive100.color
    }
    @objc
    public class func ppColorAttentive200() -> UIColor {
        Palette.ppColorAttentive200.color
    }
    @objc
    public class func ppColorAttentive300() -> UIColor {
        Palette.ppColorAttentive300.color
    }
    @objc
    public class func ppColorAttentive400() -> UIColor {
        Palette.ppColorAttentive400.color
    }
    @objc
    public class func ppColorAttentive500() -> UIColor {
        Palette.ppColorAttentive500.color
    }
    
    @objc
    public class func ppColorNegative100() -> UIColor {
        Palette.ppColorNegative100.color
    }
    @objc
    public class func ppColorNegative200() -> UIColor {
        Palette.ppColorNegative200.color
    }
    @objc
    public class func ppColorNegative300() -> UIColor {
        Palette.ppColorNegative300.color
    }
    @objc
    public class func ppColorNegative400() -> UIColor {
        Palette.ppColorNegative400.color
    }
    @objc
    public class func ppColorNegative500() -> UIColor {
        Palette.ppColorNegative500.color
    }
    @objc
    public class func ppColorGrayscale000() -> UIColor {
        Palette.ppColorGrayscale000.color
    }
    @objc
    public class func ppColorGrayscale100() -> UIColor {
        Palette.ppColorGrayscale100.color
    }
    @objc
    public class func ppColorGrayscale200() -> UIColor {
        Palette.ppColorGrayscale200.color
    }
    @objc
    public class func ppColorGrayscale300() -> UIColor {
        Palette.ppColorGrayscale300.color
    }
    @objc
    public class func ppColorGrayscale400() -> UIColor {
        Palette.ppColorGrayscale400.color
    }
    @objc
    public class func ppColorGrayscale500() -> UIColor {
        Palette.ppColorGrayscale500.color
    }
    @objc
    public class func ppColorGrayscale600() -> UIColor {
        Palette.ppColorGrayscale600.color
    }
    
    @objc
    public class func alwaysBlack() -> UIColor {
        Palette.black.color
    }
    @objc
    public class func alwaysWhite() -> UIColor {
        Palette.white.color
    }
    
    @objc
    public class func ppColorBrandingPlus300() -> [UIColor] {
        [Palette.ppColorBrandingPlus300.gradientColor.from, Palette.ppColorBrandingPlus300.gradientColor.to]
    }
    @objc
    public class func ppColorBrandingPlus400() -> [UIColor] {
        [Palette.ppColorBrandingPlus400.gradientColor.from, Palette.ppColorBrandingPlus400.gradientColor.to]
    }
    @objc
    public class func ppColorGrayscalePlus() -> [UIColor] {
        [Palette.ppColorGrayscalePlus.gradientColor.from, Palette.ppColorGrayscalePlus.gradientColor.to]
    }
}
