import UIKit

public protocol CustomSegmentedControlDelegate: AnyObject {
    func changeToIndex(index: Int)
}

private extension CustomSegmentedControl.Layout {
    enum Separator {
        static let height: CGFloat = 1.0
    }
    
    enum Selector {
        static let height: CGFloat = 2.0
    }
    
    enum Font {
        static let defaultFontSize: CGFloat = 16.0
    }
}

public class CustomSegmentedControl: UIControl {
    fileprivate enum Layout { }
    
    // MARK: - Properties
    private var buttons: [UIButton] = []
    private var buttonTitles: [String] = []
    
    private var selectorView: UIView?

    private weak var selectorViewWidthConstraint: NSLayoutConstraint?
    private weak var selectorViewCenterConstraint: NSLayoutConstraint?

    public private(set) var selectedIndex: Int = 0

    public var textColor: UIColor = .white {
        didSet {
            buttons.forEach { $0.setTitleColor(textColor, for: .normal) }
        }
    }
    
    public var buttonStyle: Typography?
    
    public var buttonDistribution: UIStackView.Distribution = .fillProportionally
    
    public var selectorViewColor: UIColor = .white {
        didSet {
            selectorView?.backgroundColor = selectorViewColor
        }
    }

    public var selectorTextColor: UIColor = .white {
        didSet {
            buttons.forEach({ $0.setTitleColor(selectorTextColor, for: .selected) })
        }
    }
    
    public var selectorIsFullSize: Bool = true
    public var selectorBottomMargin: CGFloat = Spacing.base00
    
    public var showSeparator: Bool = false
    
    public weak var delegate: CustomSegmentedControlDelegate?

    override public init(frame: CGRect) {
        super.init(frame: frame)
    }

    public init(buttonTitles: [String]) {
        self.init()
        setButtonTitles(buttonTitles: buttonTitles)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    public func setButtonTitles(buttonTitles: [String]) {
        self.buttonTitles = buttonTitles
        updateView()
    }
    
    override public func draw(_ rect: CGRect) {
        super.draw(rect)
        updateView()
    }
    
    public func setIndex(index: Int) {
        guard index >= 0 && index < buttons.count else {
            return
        }

        selectedIndex = index
        buttons.forEach { $0.setTitleColor(textColor, for: .normal) }

        updateSelectorView()
        UIView.animate(withDuration: 0.3) {
            self.layoutIfNeeded()
        }

        buttons[index].setTitleColor(selectorTextColor, for: .normal)
    }
}

// MARK: - Private
private extension CustomSegmentedControl {
    @objc
    func selectorButtonTapped(_ sender: UIButton) {
        guard let (index, button) = buttons.enumerated().first(where: { $1 === sender }) else {
            return
        }
        
        delegate?.changeToIndex(index: index)
        selectedIndex = index
        sendActions(for: .valueChanged)

        updateSelectorView()

        UIView.animate(withDuration: 0.3) {
            self.layoutIfNeeded()
        }

        buttons.forEach { $0.setTitleColor(textColor, for: .normal) }
        button.setTitleColor(selectorTextColor, for: .normal)
    }

    func updateView() {
        subviews.forEach { $0.removeFromSuperview() }

        configureButton()
        configureStackView()
        configureSeparatorViewIfNeeded()
        configureSelectorView()
    }

    func configureButton() {
        buttons = (buttonTitles).compactMap { title in
            let button = UIButton(type: .system)
            button.setTitle(title, for: .normal)
            button.addTarget(self, action: #selector(selectorButtonTapped(_:)), for: .touchUpInside)
            button.setTitleColor(textColor, for: .normal)
            button.titleLabel?.font = buttonStyle?.font() ?? UIFont.boldSystemFont(ofSize: Layout.Font.defaultFontSize)
            return button
        }
        
        if buttons.count - 1 >= selectedIndex {
            buttons[selectedIndex].setTitleColor(selectorTextColor, for: .normal)
        } else {
          buttons.first?.setTitleColor(selectorTextColor, for: .normal)
        }
    }

    func configureStackView() {
        let stackView = UIStackView(arrangedSubviews: buttons)
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = buttonDistribution
        stackView.spacing = Spacing.base02

        addSubview(stackView)

        stackView.layout {
            $0.top == topAnchor
            $0.leading == leadingAnchor
            $0.trailing == trailingAnchor
            $0.bottom == bottomAnchor
        }
    }

    func configureSelectorView() {
        let selector = UIView()
        selector.backgroundColor = selectorViewColor
        selectorView = selector

        addSubview(selector)

        selectorView?.layout {
            $0.height == Layout.Selector.height
            $0.bottom == bottomAnchor - selectorBottomMargin
        }

        updateSelectorView()
    }
    
    func configureSeparatorViewIfNeeded() {
        guard showSeparator else { return }
        
        let separator = UIView()
        separator.backgroundColor = Colors.grayscale100.color
        
        addSubview(separator)
        
        separator.layout {
            $0.height == Layout.Separator.height
            $0.leading == leadingAnchor
            $0.trailing == trailingAnchor
            $0.bottom == bottomAnchor - selectorBottomMargin
        }
    }

    func updateSelectorView() {
        guard let selectorView = selectorView, selectedIndex >= 0 && selectedIndex < buttons.count else {
            return
        }

        if let widthConstraint = selectorViewWidthConstraint, let centerConstraint = selectorViewCenterConstraint {
            NSLayoutConstraint.deactivate([widthConstraint, centerConstraint])
        }

        let button = buttons[selectedIndex]

        selectorView.layout {
            selectorViewCenterConstraint = $0.centerX == button.centerXAnchor

            if selectorIsFullSize {
                selectorViewWidthConstraint = $0.width == button.widthAnchor
            } else {
                selectorViewWidthConstraint = $0.width == button.widthAnchor * 0.5
            }
        }
    }
}
