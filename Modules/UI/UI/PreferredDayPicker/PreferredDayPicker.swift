import UIKit

public final class PreferredDayPicker: UIControl {
    public private(set) var selectedDate: Date?
    
    public var availableDates: [Date] = [] {
        didSet {
            reloadAvailableDateButtons()
        }
    }
    
    private var availableDateButtons: [PreferredDayButton] = []
    
    private var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        stackView.alignment = .center
        return stackView
    }()
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension PreferredDayPicker: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(stackView)
    }
    
    public func setupConstraints() {
        stackView.layout {
            $0.top == topAnchor
            $0.trailing == trailingAnchor
            $0.bottom == bottomAnchor
            $0.leading == leadingAnchor
        }
    }
}

private extension PreferredDayPicker {
    func resetStackView() {
        stackView.arrangedSubviews.forEach { view in
            stackView.removeArrangedSubview(view)
            view.removeFromSuperview()
        }
    }
    
    func buildStackViewHierarchy() {
        let slice = ArraySlice(availableDateButtons)
        let numberOfItems = availableDateButtons.count
        let numberOfItemsPerLine = 3
        let indexes = stride(from: 0, to: numberOfItems, by: numberOfItemsPerLine)
        
        for index in indexes {
            let upperBound = min(index + numberOfItemsPerLine, numberOfItems)
            stackView.addArrangedSubview(preferredDayButtonRow(for: Array(slice[index..<upperBound])))
        }
    }
    
    func reloadAvailableDateButtons() {
        availableDateButtons = availableDates.map(preferredDayButton(for:))
        resetStackView()
        buildStackViewHierarchy()
    }
    
    func preferredDayButtonRow(for buttons: [PreferredDayButton]) -> UIStackView {
        let stackView = UIStackView(arrangedSubviews: buttons)
        stackView.distribution = .fillEqually
        stackView.spacing = Spacing.base02
        return stackView
    }
    
    func preferredDayButton(for date: Date) -> PreferredDayButton {
        let button = PreferredDayButton(type: .custom)
        button.day = Calendar.current.component(.day, from: date)
        button.addTarget(self, action: #selector(preferredDayButtonTapped(_:)), for: .touchUpInside)
        button.layout {
            $0.width == UIScreen.main.bounds.width * 0.2
            $0.height == button.widthAnchor
        }
        return button
    }
}

@objc
private extension PreferredDayPicker {
    func preferredDayButtonTapped(_ sender: PreferredDayButton) {
        let previouslySelectedButton = availableDateButtons.first(where: \.isSelected)
        guard previouslySelectedButton != sender else {
            return
        }
        previouslySelectedButton?.isSelected.toggle()
        sender.isSelected.toggle()
        selectedDate = availableDates.first(where: { date in
            Calendar.current.component(.day, from: date) == sender.day
        })
        sendActions(for: .valueChanged)
    }
}
