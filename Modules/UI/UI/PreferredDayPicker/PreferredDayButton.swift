import UIKit

private struct PreferredDayButtonBackgroundDescriptor {
    var bounds: CGRect
    let backgroundColor: Colors.Style
    let borderGap: CGFloat
    let borderStyle: Border.Style
    
    init(bounds: CGRect, backgroundColor: Colors.Style, borderGap: CGFloat = 3.0, borderStyle: Border.Style) {
        self.bounds = bounds
        self.backgroundColor = backgroundColor
        self.borderGap = borderGap
        self.borderStyle = borderStyle
    }
}

private final class PreferredDayBackgroundBuilder {
    private init() {}
    
    static func backgroundImage(using descriptor: PreferredDayButtonBackgroundDescriptor) -> UIImage {
        let backgroundFillOffsetLength = descriptor.borderGap + descriptor.borderStyle.rawValue.width
        let backgroundFillRect = descriptor.bounds.insetBy(dx: backgroundFillOffsetLength, dy: backgroundFillOffsetLength)
        let borderOffsetLength = descriptor.borderStyle.rawValue.width / 2
        let borderRect = descriptor.bounds.insetBy(dx: borderOffsetLength, dy: borderOffsetLength)
        let renderer = UIGraphicsImageRenderer(bounds: descriptor.bounds)
        return renderer.image { context in
            descriptor.borderStyle.rawValue.color.setStroke()
            context.cgContext.setLineWidth(descriptor.borderStyle.rawValue.width)
            context.cgContext.strokeEllipse(in: borderRect)
            
            descriptor.backgroundColor.rawValue.setFill()
            context.cgContext.fillEllipse(in: backgroundFillRect)
        }
    }
}

final class PreferredDayButton: UIButton {
    var day: Int? {
        didSet {
            setTitle(forDay: day)
        }
    }
    
    private lazy var unselectedBackgroundDescriptor = PreferredDayButtonBackgroundDescriptor(
        bounds: bounds,
        backgroundColor: .grayscale050(),
        borderStyle: .light(color: .grayscale200())
    )
    
    private lazy var selectedBackgroundDescriptor = PreferredDayButtonBackgroundDescriptor(
        bounds: bounds,
        backgroundColor: .branding400(),
        borderStyle: .light(color: .branding400())
    )
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateBackgroundImagesIfNeeded()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard
            #available(iOS 13.0, *),
            traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection)
            else {
                return
        }
        updateBackgroundImagesIfNeeded(force: true)
    }
    
    func setup() {
        updateBackgroundImagesIfNeeded(force: true)
        titleLabel?.numberOfLines = 2
        titleLabel?.textAlignment = .center
        clipsToBounds = false
    }
    
    private func updateBackgroundImagesIfNeeded(force: Bool = false) {
        guard unselectedBackgroundDescriptor.bounds != bounds || force else {
            return
        }
        unselectedBackgroundDescriptor.bounds = bounds
        selectedBackgroundDescriptor.bounds = bounds
        setBackgroundImage(PreferredDayBackgroundBuilder.backgroundImage(using: unselectedBackgroundDescriptor), for: .normal)
        setBackgroundImage(PreferredDayBackgroundBuilder.backgroundImage(using: selectedBackgroundDescriptor), for: .selected)
    }
    
    private func setTitle(forDay day: Int?) {
        guard let dayDescription = day?.description else {
            setTitle(nil, for: .normal)
            return
        }
        let numberAttributes: [NSAttributedString.Key: Any] = [.font: Typography.title(.medium).font()]
        let textAttributes: [NSAttributedString.Key: Any] = [.font: Typography.caption().font()]
        let selectedAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: Colors.white.lightColor]
        let normalAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: Colors.grayscale400.color]
        let titleText = "Dia\n\(dayDescription)"
        
        let normalAttributedString = NSMutableAttributedString(string: titleText)
        normalAttributedString.addAttributes(normalAttributes, range: NSRange(location: 0, length: titleText.count))
        normalAttributedString.addAttributes(textAttributes, range: NSRange(location: 0, length: 3))
        normalAttributedString.addAttributes(numberAttributes, range: NSRange(3..<titleText.count))
        
        let selectedAttributedString = NSMutableAttributedString(string: titleText)
        selectedAttributedString.addAttributes(selectedAttributes, range: NSRange(location: 0, length: titleText.count))
        selectedAttributedString.addAttributes(textAttributes, range: NSRange(location: 0, length: 3))
        selectedAttributedString.addAttributes(numberAttributes, range: NSRange(3..<titleText.count))
        
        setAttributedTitle(normalAttributedString, for: .normal)
        setAttributedTitle(selectedAttributedString, for: .selected)
        setAttributedTitle(selectedAttributedString, for: [.highlighted, .selected])
        setAttributedTitle(selectedAttributedString, for: [.highlighted, .normal])
        accessibilityLabel = "Dia \(dayDescription)"
    }
}
