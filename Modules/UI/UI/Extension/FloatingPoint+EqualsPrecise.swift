public extension FloatingPoint {
    func isEqual(to other: Self, precise value: UInt? = nil) -> Bool {
        guard let value = value else { return self == other }
        return precised(value) == other.precised(value)
    }
}

private extension FloatingPoint {
    func precised(_ value: UInt = 1) -> Self {
        let offset = Int(pow(10.0, Double(value)))
        return (self * Self(offset)).rounded() / Self(offset)
    }
}
