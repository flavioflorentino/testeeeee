import Foundation
import UIKit

public extension String {
    func attributedStringWithFont(
        primary: UIFont,
        secondary: UIFont,
        separator: String = "**",
        underline: Bool = false,
        textAlignment: NSTextAlignment? = nil,
        lineHeight: CGFloat? = nil
    ) -> NSMutableAttributedString {
        var boldStrings = [String]()
        let splitString = self.components(separatedBy: separator)
        for (index, string) in splitString.enumerated() where index % 2 == 1 {
            boldStrings.append(string)
        }
        let normalFontAttribute = [NSAttributedString.Key.font: primary]
        let boldFontAttribute = [NSAttributedString.Key.font: secondary]
        let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
        let cleanString = self.replacingOccurrences(of: separator, with: "")
        let normalString = NSMutableAttributedString(string: cleanString, attributes: normalFontAttribute)
        
        boldStrings.forEach { string in
            normalString.addAttributes(boldFontAttribute, range: (cleanString as NSString).range(of: string as String))
            if underline {
                normalString.addAttributes(underlineAttribute, range: (cleanString as NSString).range(of: string as String))
            }
        }
        
        self.setupStyle(with: lineHeight, alignment: textAlignment, for: normalString)
        
        return normalString
    }
    
    func attributedStringWith(
        normalFont: UIFont,
        highlightFont: UIFont,
        normalColor: UIColor,
        highlightColor: UIColor,
        textAlignment: NSTextAlignment? = nil,
        underline: Bool = true,
        separator: String = "**",
        lineHeight: CGFloat? = nil
    ) -> NSMutableAttributedString {
        var boldStrings = [String]()
        let splitString = self.components(separatedBy: separator)
        for (index, string) in splitString.enumerated() where index % 2 == 1 {
            boldStrings.append(string)
        }

        let normalFontAttribute = [NSAttributedString.Key.font: normalFont, NSAttributedString.Key.foregroundColor: normalColor]
        let boldFontAttribute = [NSAttributedString.Key.font: highlightFont, NSAttributedString.Key.foregroundColor: highlightColor]
        let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
        let cleanString = self.replacingOccurrences(of: separator, with: "")
        let normalString = NSMutableAttributedString(string: cleanString, attributes: normalFontAttribute)
        
        boldStrings.forEach { string in
            normalString.addAttributes(boldFontAttribute, range: (cleanString as NSString).range(of: string as String))
            if underline {
                normalString.addAttributes(underlineAttribute, range: (cleanString as NSString).range(of: string as String))
            }
        }

        self.setupStyle(with: lineHeight, alignment: textAlignment, for: normalString)
        
        return normalString
    }
    
    func attributedString(
        separated: String,
        attrs: [NSAttributedString.Key: Any]
    ) -> NSMutableAttributedString {
        let sanitize = sanitizeText(self, separated: separated)
        let highlights = highlightText(self, separated: separated)
        
        return applyAttributedString(text: sanitize, highlights: highlights, attrs: attrs)
    }
    
    func attributedString(
        highlights: [String],
        attrs: [NSAttributedString.Key: Any]
    ) -> NSMutableAttributedString {
        applyAttributedString(text: self, highlights: highlights, attrs: attrs)
    }
    
    func applyAttributedString(
        text: String,
        highlights: [String],
        attrs: [NSAttributedString.Key: Any]
    ) -> NSMutableAttributedString {
        let attributedTex = NSMutableAttributedString(string: text)
        for highlight in highlights {
            let range = NSString(string: text).range(of: highlight)
            attributedTex.addAttributes(attrs, range: range)
        }
        
        return attributedTex
    }
    
    func applyHtmlTags(with font: UIFont, textAlignment: NSTextAlignment? = nil) -> NSAttributedString? {
        var options = [NSAttributedString.DocumentReadingOptionKey: Any]()
        options[.documentType] = NSAttributedString.DocumentType.html
        options[.characterEncoding] = String.Encoding.utf8.rawValue
        
        guard let data = data(using: .utf8),
              let mutableString = try? NSMutableAttributedString(data: data,
                                                                 options: options,
                                                                 documentAttributes: nil) else {
            return nil
        }
        
        let stringRange = NSRange(location: 0, length: mutableString.length)
        mutableString.addAttribute(.font, value: font, range: stringRange)
        setupStyle(with: nil, alignment: textAlignment, for: mutableString)
        
        return mutableString
    }
}

private extension String {
    func highlightText(_ text: String, separated: String) -> [String] {
        let highlight = text.components(separatedBy: separated)
        return highlight.enumerated().filter({ $0.offset.isMultiple(of: 2) }).map { $0.element }
    }
    
    func sanitizeText(_ text: String, separated: String) -> String {
        text.replacingOccurrences(of: separated, with: "")
    }
    
    func setupStyle(with height: CGFloat?, alignment: NSTextAlignment?, for text: NSMutableAttributedString) {
        let style = NSMutableParagraphStyle()
        
        if let lineHeight = height {
            style.minimumLineHeight = lineHeight
        }
        
        if let textAlignment = alignment {
            style.alignment = textAlignment
        }
        
        let range = NSRange(location: 0, length: text.length)
        
        let lineHeightAttribute = [NSAttributedString.Key.paragraphStyle: style]
        
        text.addAttributes(lineHeightAttribute, range: range)
    }
}
