import UIKit

public extension UINavigationController {
    @available(iOS 11.0, *)
    func setLargeTitle(shouldBeLarge: Bool) {
        navigationItem.largeTitleDisplayMode = shouldBeLarge ? .always : .never
        navigationBar.prefersLargeTitles = shouldBeLarge
    }
}
