import Foundation
import UIKit

public extension UIView {
    func addSubviews(_ views: UIView...) {
        views.forEach { addSubview($0) }
    }
    
    func removeAllSubviews() {
        for view in subviews {
            view.removeFromSuperview()
        }
    }
    
    func show(_ views: UIView...) {
        for view in views {
            view.isHidden = false
        }
    }
    
    func hide(_ views: UIView...) {
        for view in views {
            view.isHidden = true
        }
    }
    
    func applyRadiusCorners(radius: CGFloat) {
        self.clipsToBounds = true
        self.layer.cornerRadius = radius
    }

    func applyRadiusTopCorners(radius: CGFloat = 8.0) {
        if #available(iOS 11.0, *) {
            self.layer.maskedCorners = CACornerMask(arrayLiteral: [.layerMinXMinYCorner, .layerMaxXMinYCorner])
            self.layer.cornerRadius = radius
        } else {
            let path = UIBezierPath(
                roundedRect: self.bounds,
                byRoundingCorners: [.topLeft, .topRight],
                cornerRadii: CGSize(width: radius, height: radius)
            )
            let maskLayer = CAShapeLayer()
            maskLayer.path = path.cgPath
            self.layer.mask = maskLayer
        }
        self.clipsToBounds = true
    }

    func roundCorners(_ corners: CACornerMask, radius: CGFloat) {
        if #available(iOS 11.0, *) {
            layer.cornerRadius = radius
            layer.maskedCorners = corners
        } else {
            var cornerMask = UIRectCorner()
            if corners.contains(.layerMinXMinYCorner) {
                cornerMask.insert(.topLeft)
            }

            if corners.contains(.layerMaxXMinYCorner) {
                cornerMask.insert(.topRight)
            }

            if corners.contains(.layerMinXMaxYCorner) {
                cornerMask.insert(.bottomLeft)
            }

            if corners.contains(.layerMaxXMaxYCorner) {
                cornerMask.insert(.bottomRight)
            }

            let path = UIBezierPath(
                roundedRect: bounds,
                byRoundingCorners: cornerMask,
                cornerRadii: CGSize(width: radius, height: radius)
            )

            let mask = CAShapeLayer()
            mask.path = path.cgPath
            layer.mask = mask
        }

        clipsToBounds = true
    }
    
    /// Configures the shadow on the layer of the view.
    /// - Parameters:
    ///   - color: The shadow's color.
    ///   - alpha: The shadow's opacity.
    ///   - radius: The radius to be applied to the shadow.
    ///   - offset: The offset of the shadow related to the view.
    func addShadow(with color: UIColor, alpha: Float = 0.1, radius: CGFloat = 6, offset: CGSize = CGSize(width: 0, height: 2)) {
        layer.shadowOpacity = alpha
        layer.shadowRadius = radius
        layer.shadowOffset = offset
        layer.shadowColor = color.cgColor
        layer.masksToBounds = false
    }
    
    func drawDottedLine(
        start p0: CGPoint,
        end p1: CGPoint,
        view: UIView,
        atSublayer: Int = 0,
        strokeColor: CGColor = UIColor.lightGray.cgColor,
        lineWidth: CGFloat = 1,
        lengthOfDash: NSNumber = 2,
        lengthOfGap: NSNumber = 2
    ) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = strokeColor
        shapeLayer.lineWidth = lineWidth
        shapeLayer.lineDashPattern = [lengthOfDash, lengthOfGap]

        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.insertSublayer(shapeLayer, at: UInt32(atSublayer))
    }

    @discardableResult
    func setCustomCircleBorderAnimation(withColors colors: [CGColor]) -> CALayer {
        let gradient = CAGradientLayer()
        gradient.startPoint = CGPoint(x: 0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.colors = colors
        gradient.locations = (0..<colors.count).map(NSNumber.init)
        gradient.type = .axial
        
        gradient.frame = self.bounds
        gradient.cornerRadius = self.layer.cornerRadius
        layer.insertSublayer(gradient, at: 0)
        
        let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotationAnimation.toValue = 2 * CGFloat.pi
        rotationAnimation.duration = 1
        rotationAnimation.isRemovedOnCompletion = false
        rotationAnimation.repeatDuration = .infinity
        gradient.add(rotationAnimation, forKey: nil)

        return gradient
    }
    
    func shakeView() {
        let midX = center.x
        let delta: CGFloat = 5
        center.x = midX - delta
        
        UIView.animate(
            withDuration: 0.05,
            animations: { [weak self] in
                UIView.setAnimationRepeatCount(2)
                UIView.setAnimationRepeatAutoreverses(true)
                self?.center.x = midX + delta
            }, completion: { [weak self] _ in
                self?.center.x = midX
            }
        )
    }

    var snapshot: UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.frame.size, false, 0)
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        layer.render(in: context)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}
