import UIKit

public extension UIBarButtonItem {
    convenience init(icon: Iconography,
                     target: AnyObject,
                     action: Selector,
                     fontStyle: Typography.Style.Iconography = .medium,
                     color: UIColor = Colors.brandingBase.color,
                     size: CGSize = .init(width: 24, height: 24)) {
        let image = UIImage(iconName: icon.rawValue,
                            font: Typography.icons(fontStyle).font(),
                            color: color,
                            size: size)
        self.init(image: image,
                  style: .plain,
                  target: target,
                  action: action)
    }
}
