import UIKit

public extension UIImage {
    convenience init?(base64: String) {
        if let imageData = Data(base64Encoded: base64, options: .init(rawValue: 0)) {
            self.init(data: imageData)
        } else {
            return nil
        }
    }
}
