import UIKit

public extension UIWindow {
    var lastPresentedViewController: UIViewController? {
        var topViewController = rootViewController
        
        while let presentedViewController = topViewController?.presentedViewController {
            topViewController = presentedViewController
        }
        
        return topViewController
    }
}
