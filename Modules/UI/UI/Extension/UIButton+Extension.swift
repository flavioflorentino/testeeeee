import Foundation
import UIKit

public extension UIButton {
    func underline() {
        guard let text = titleLabel?.text, let normalColor = titleColor(for: .normal) else { return }
        
        let attributedString = NSMutableAttributedString(string: text)
        let range = NSRange(location: 0, length: text.count)
        
        attributedString.addAttribute(.underlineColor, value: normalColor, range: range)
        attributedString.addAttribute(.foregroundColor, value: normalColor, range: range)
        attributedString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range)
        setAttributedTitle(attributedString, for: .normal)
    }
}
