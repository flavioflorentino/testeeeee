import Foundation
import UIKit

public extension NSMutableAttributedString {
    func addSpacing(of spacing: CGFloat) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = spacing
        addAttribute(.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: length))
    }
    
    func font(text: String, font: UIFont) {
        let attribute = [NSAttributedString.Key.font: font]
        let range = NSString(string: string).range(of: text)
        addAttributes(attribute, range: range)
    }
    
    func underline(text: String) {
        let attribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
        let range = NSString(string: string).range(of: text)
        addAttributes(attribute, range: range)
    }
    
    func textColor(text: String, color: UIColor) {
        let attribute = [NSAttributedString.Key.foregroundColor: color]
        let range = NSString(string: string).range(of: text)
        addAttributes(attribute, range: range)
    }
    
    func textBackgroundColor(text: String, color: UIColor) {
        let attribute = [NSAttributedString.Key.backgroundColor: color]
        let range = NSString(string: string).range(of: text)
        addAttributes(attribute, range: range)
    }
    
    func paragraphStyle(text: String, paragraphStyle: NSParagraphStyle) {
        let attribute = [NSAttributedString.Key.paragraphStyle: paragraphStyle]
        let range = NSString(string: string).range(of: text)
        addAttributes(attribute, range: range)
    }
    
    func paragraph(aligment: NSTextAlignment, lineSpace: CGFloat) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = aligment
        paragraphStyle.lineSpacing = lineSpace
        let attribute = [NSAttributedString.Key.paragraphStyle: paragraphStyle]
        let range = NSRange(location: 0, length: length)
        addAttributes(attribute, range: range)
    }
    
    func link(text: String, link: String) {
        guard let url = URL(string: link) else {
            return
        }
        let attribute = [NSAttributedString.Key.link: url]
        let range = NSString(string: string).range(of: text)
        addAttributes(attribute, range: range)
    }
}
