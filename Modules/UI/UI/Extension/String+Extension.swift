import Foundation

public extension String {
    // formatting text for currency textField
    func cellphoneFormatting() -> String {
        // Check count to avoid crashs
        guard count > 5 else { return "" }
        
        let cellphone = self
        
        let ddd = cellphone.prefix(2)
        
        let start = cellphone.index(cellphone.startIndex, offsetBy: 2)
        let end = cellphone.index(cellphone.endIndex, offsetBy: -4)
        let range = start..<end
        let prefixNumber = cellphone[range]
        
        let lastDigitsIndex = cellphone.index(cellphone.endIndex, offsetBy: -4)
        let lastDigits = cellphone[lastDigitsIndex...]
        
        return "(\(ddd)) \(prefixNumber)-\(lastDigits)"
    }
    
    /**
        Return substring range
    */
    func substringRange(of substring: String) -> NSRange {
        NSString(string: self).range(of: substring)
    }
    
    func hasContent() -> String? {
        self.isEmpty ? nil : self
    }
    
    func capitalizingFirstLetter() -> String {
        prefix(1).capitalized + dropFirst()
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
