import UIKit

public extension UIStackView {
    func addArrangedSubviews(_ views: UIView...) {
        views.forEach(addArrangedSubview(_:))
    }
    
    func setSpacing(
        _ spacing: CGFloat,
        after view: UIView,
        axis: NSLayoutConstraint.Axis = .vertical
    ) {
        let spacerView = SpacerView(axis: axis, size: spacing)
        insert(view: spacerView, afterView: view)
    }
    
    @discardableResult
    func insertLine(after view: UIView, color: UIColor, edgeInset: CGFloat = 0) -> UIView {
        let lineView = UIView()
        let lineHeight: CGFloat = 0.5
        lineView.backgroundColor = color
        insert(view: lineView, afterView: view)
        updateConstraintsInArrangedView(lineView, height: lineHeight, edgeInset: edgeInset)
        
        return lineView
    }
}

private extension UIStackView {
    func insert(view: UIView, afterView: UIView) {
        if let index = arrangedSubviews.firstIndex(of: afterView) {
            insertArrangedSubview(view, at: index + 1)
        }
    }
    
    func updateConstraintsInArrangedView(_ view: UIView, height: CGFloat, edgeInset: CGFloat) {
        NSLayoutConstraint.activate([
            view.trailingAnchor.constraint(equalTo: trailingAnchor, constant: edgeInset),
            view.leadingAnchor.constraint(equalTo: leadingAnchor, constant: edgeInset),
            view.heightAnchor.constraint(equalToConstant: height)
        ])
    }
}
