import Foundation
import UIKit

public extension NSLayoutConstraint {
    convenience init(
        item view1: Any,
        attribute attr1: NSLayoutConstraint.Attribute,
        relatedBy relation: NSLayoutConstraint.Relation = .equal,
        multiplier: CGFloat = 1.0,
        constant c: CGFloat = 0.0
    ) {
        var view2: UIView?

        if let view = view1 as? UIView,
            let superview = view.superview {
            view2 = superview
        }

        self.init(
            item: view1,
            attribute: attr1,
            relatedBy: relation,
            toItem: view2,
            attribute: attr1,
            multiplier: multiplier,
            constant: c
        )
    }

    convenience init(item view1: Any, attribute attr1: NSLayoutConstraint.Attribute, constant c: CGFloat = 0.0) {
        var view2: UIView?

        if let view = view1 as? UIView, let superview = view.superview {
            view2 = superview
        }

        self.init(
            item: view1,
            attribute: attr1,
            relatedBy: .equal,
            toItem: view2,
            attribute: attr1,
            multiplier: 1.0,
            constant: c
        )
    }

    static func constraintAllEdges(from view: UIView, to superView: UIView) {
        if view.translatesAutoresizingMaskIntoConstraints {
            view.translatesAutoresizingMaskIntoConstraints = false
        }

        NSLayoutConstraint.activate([
            view.leadingAnchor.constraint(equalTo: superView.leadingAnchor),
            view.topAnchor.constraint(equalTo: superView.topAnchor),
            view.trailingAnchor.constraint(equalTo: superView.trailingAnchor),
            view.bottomAnchor.constraint(equalTo: superView.bottomAnchor)
        ])
    }

    static func leadingTrailing(equalTo contentView: UIView, for views: [UIView], constant: CGFloat = 0) {
        for view in views {
            view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: constant).isActive = true
            view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -constant).isActive = true
        }
    }

    static func leadingTrailingCenterX(equalTo contentView: UIView, for views: [UIView], constant: CGFloat = 0) {
        for view in views {
            view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: constant).isActive = true
            view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -constant).isActive = true
            view.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        }
    }
}
