import UIKit

public extension UILabel {
    func set(lineHeight: CGFloat) {
        let text = self.text
        
        if let text = text,
            let font = font,
            let textColor = textColor {
            let style = NSMutableParagraphStyle()
            style.minimumLineHeight = lineHeight
            style.alignment = textAlignment
            
            let labelAttributes = [
                NSAttributedString.Key.font: font,
                NSAttributedString.Key.paragraphStyle: style,
                NSAttributedString.Key.foregroundColor: textColor
            ]
            
            let range = NSRange(location: 0, length: text.count)
            
            let attr = NSMutableAttributedString(string: text)
            attr.addAttributes(labelAttributes, range: range)
            
            self.attributedText = attr
        }
    }
}
