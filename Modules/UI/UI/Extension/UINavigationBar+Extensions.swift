import UIKit

public extension UINavigationBar {
    func setUpNavigationBarDefaultAppearance(
        backgroundColor: UIColor = Colors.backgroundPrimary.color,
        tintColor: UIColor = Colors.branding400.color,
        shadowColor: UIColor = .clear
    ) {
        self.tintColor = tintColor
        if #available(iOS 13.0, *) {
           setNavigationBarAppearance(with: backgroundColor, shadowColor: shadowColor)
        } else {
            setNavigationBarBackgroundColor(with: backgroundColor)
        }
    }

    @available(iOS 13.0, *)
    func setNavigationBarAppearance(with backgroundColor: UIColor, shadowColor: UIColor) {
        let appearance = UINavigationBarAppearance()
        appearance.backgroundColor = backgroundColor
        appearance.shadowColor = shadowColor
        appearance.titleTextAttributes = [.foregroundColor: Colors.grayscale700.color]

        setBackgroundImage(nil, for: .default)
        standardAppearance = appearance
        compactAppearance = appearance
        scrollEdgeAppearance = appearance
    }

    @available(iOS, deprecated: 13.0, message: "Use setNavigationBarAppearence() instead")
    func setNavigationBarBackgroundColor(with backgroundColor: UIColor) {
        isTranslucent = false
        barTintColor = backgroundColor
        setBackgroundImage(UIImage(), for: .default)
        shadowImage = UIImage()
    }
}
