import UIKit

public extension UITableView {
    func updateTableHeaderView(with view: UIView, and size: CGSize = UIView.layoutFittingCompressedSize) {
        guard let size = updateTableHeaderOrFooterHeight(with: view, and: size) else { return }
        tableHeaderView?.frame.size = size
    }
    
    func updateTableFooterView(with view: UIView, and size: CGSize = UIView.layoutFittingCompressedSize) {
        guard let size = updateTableHeaderOrFooterHeight(with: view, and: size) else { return }
        tableFooterView?.frame.size = size
    }
    
    func updateTableHeaderOrFooterHeight(with view: UIView, and size: CGSize) -> CGSize? {
        let newViewSize = view.systemLayoutSizeFitting(size)
    
        guard newViewSize.height != view.frame.height else { return nil }
        
        return newViewSize
    }

    func scrollToBottom(_ isAnimated: Bool = true) {
        guard numberOfSections > 0, numberOfRows(inSection: numberOfSections - 1) > 0 else { return }
        let lastSectionIndex = numberOfSections - 1
        let rowsInSection = numberOfRows(inSection: lastSectionIndex)
        let indexPath = IndexPath(row: rowsInSection - 1, section: lastSectionIndex)
        
        guard hasMoreRowsAfter(indexPath: indexPath) else { return }
        
        DispatchQueue.main.async {
            self.scrollToRow(at: indexPath, at: .bottom, animated: isAnimated)
        }
    }
    
    private func hasMoreRowsAfter(indexPath: IndexPath) -> Bool {
        let hasMoreSections = indexPath.section < numberOfSections
        let hasMoreRows = indexPath.row < numberOfRows(inSection: indexPath.section)
        return hasMoreSections && hasMoreRows
    }
}
