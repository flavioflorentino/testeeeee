import UIKit

public extension UIViewController {
    func setUpNavigationBarDefaultAppearance(
        backgroundColor: UIColor = Colors.backgroundPrimary.color,
        tintColor: UIColor = Colors.branding400.color,
        shadowColor: UIColor = .clear
    ) {
        guard let navigation = navigationController else { return }
        navigation.setNavigationBarHidden(false, animated: true)
        if #available(iOS 11.0, *) {
            navigation.setLargeTitle(shouldBeLarge: false)
        }
        navigation.navigationBar.setUpNavigationBarDefaultAppearance(backgroundColor: backgroundColor, tintColor: tintColor)
    }
}
