import UIKit

public protocol PickerInputViewDelegate: AnyObject {
    func didSelectOption(_ option: PickerOption?, on pickerInputView: PickerInputView)
}

public final class PickerInputView: UIView {
    public var options: [PickerOption] = []
    
    public weak var delegate: PickerInputViewDelegate?
    
    public var selectedOption: PickerOption? {
        didSet {
            delegate?.didSelectOption(selectedOption, on: self)
            inputInterfaceElement?.inputText = selectedOption?.rowTitle
        }
    }
    
    public weak var inputInterfaceElement: InputInterfaceElement? {
        didSet {
            self.inputInterfaceElement?.addTarget(self, action: #selector(inputDidBeginEditing), for: .editingDidBegin)
        }
    }
    
    public private(set) lazy var pickerView: UIPickerView = {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        return pickerView
    }()
    
    public convenience init(options: [PickerOption]) {
        self.init(frame: .zero)
        self.options = options
        buildLayout()
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    private func inputDidBeginEditing() {
        if selectedOption == nil {
            selectedOption = options.first
        }
    }
}

extension PickerInputView: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(pickerView)
    }
    
    public func setupConstraints() {
        pickerView.layout {
            $0.top == compatibleSafeAreaLayoutGuide.topAnchor
            $0.trailing == compatibleSafeAreaLayoutGuide.trailingAnchor
            $0.bottom == compatibleSafeAreaLayoutGuide.bottomAnchor
            $0.leading == compatibleSafeAreaLayoutGuide.leadingAnchor
        }
    }
    
    public func configureViews() {
        translatesAutoresizingMaskIntoConstraints = false
    }
}

extension PickerInputView: UIPickerViewDataSource {
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        options.count
    }
}

extension PickerInputView: UIPickerViewDelegate {
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        options[row].rowTitle
    }
    
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedOption = options[row]
    }
}
