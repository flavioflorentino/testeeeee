import UIKit

public protocol InputInterfaceElement: UIControl {
    var inputText: String? { get set }
}

extension UITextField: InputInterfaceElement {
    public var inputText: String? {
        get {
            text
        }
        set {
            text = newValue
        }
    }
}
