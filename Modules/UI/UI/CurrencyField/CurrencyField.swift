import UIKit

public protocol CurrencyFieldDelegate: AnyObject {
    func onValueChange(value: Double)
}

public class CurrencyField: UIView {
    private lazy var presenter: CurrencyFieldPresenterProtocol = {
        let presenter = CurrencyFieldPresenter()
        presenter.outputs = self
        
        return presenter
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private lazy var currencyLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private lazy var valueTextField: UITextField = {
        let textField = UITextField()
        textField.keyboardType = .numberPad
        textField.addTarget(self, action: #selector(textDidChange(_:)), for: .editingChanged)
        
        return textField
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .leading
        stackView.distribution = .fill
        stackView.spacing = 0
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        return stackView
    }()
    
    public var positionCurrency: Position = .center {
        didSet {
            switch positionCurrency {
            case .top:
                stackView.alignment = .top
            case .center:
                stackView.alignment = .center
            case .bottom:
                stackView.alignment = .bottom
            }
        }
    }
    
    public var fontCurrency = UIFont.systemFont(ofSize: 14) {
        didSet {
            currencyLabel.font = fontCurrency
        }
    }
    
    public var hiddenCurrency: Bool = false {
        didSet {
            currencyLabel.isHidden = hiddenCurrency
        }
    }
    
    public var fontValue = UIFont.systemFont(ofSize: 14) {
        didSet {
            valueTextField.font = fontValue
        }
    }
    
    public var spacing: CGFloat = 0.0 {
        didSet {
            stackView.spacing = spacing
        }
    }
    
    public var value: Double = 0.0 {
        didSet {
            presenter.inputs.changeInputValue(value: value)
        }
    }
    
    public var inputAccessory: UIView? = nil {
        didSet {
            valueTextField.inputAccessoryView = inputAccessory
        }
    }
    
    public var isEnabled: Bool = true {
        didSet {
            isUserInteractionEnabled = isEnabled
            valueTextField.isEnabled = isEnabled
        }
    }
    public var clearButtonMode: UITextField.ViewMode = .never {
        didSet {
            valueTextField.clearButtonMode = clearButtonMode
        }
    }
    
    public var placeholderColor: UIColor = .gray
    public var textColor: UIColor = .black
    public var limitValue = Int.max
    public var locale: String = "pt_BR"
    
    public weak var delegate: CurrencyFieldDelegate?
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @discardableResult
    override public func becomeFirstResponder() -> Bool {
        valueTextField.becomeFirstResponder()
    }
    
    override public func didMoveToSuperview() {
        super.didMoveToSuperview()
        setupView()
    }
    
    private func setupView() {
        backgroundColor = .clear
        let attributes: [NSAttributedString.Key: Any] = [.foregroundColor: placeholderColor]
        valueTextField.attributedPlaceholder = NSAttributedString(string: "0,00", attributes: attributes)
        presenter.inputs.setup(initialValue: value, limit: limitValue, locale: locale)
        adjustConstraintCurrencyLabel(position: positionCurrency)
    }
    
    private func adjustConstraintCurrencyLabel(position: Position) {
        switch position {
        case .top:
            topConstraintCurrencyLabel()
            
        case .center:
            centerConstraintCurrencyLabel()
            
        case .bottom:
            bottomConstraintCurrencyLabel()
        }
    }
    
    private func topConstraintCurrencyLabel() {
        guard let fieldFont = valueTextField.font,
            let labelFont = currencyLabel.font else {
                return
        }
        
        let sizeField = fieldFont.ascender - fieldFont.capHeight
        let sizeLabel = labelFont.ascender - labelFont.capHeight
        let offset = sizeField - sizeLabel
        
        currencyLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: offset).isActive = true
        currencyLabel.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
        layoutIfNeeded()
    }
    
    private func centerConstraintCurrencyLabel() {
       currencyLabel.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
       currencyLabel.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
       layoutIfNeeded()
    }
    
    private func bottomConstraintCurrencyLabel() {
        currencyLabel.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        currencyLabel.firstBaselineAnchor.constraint(equalTo: valueTextField.firstBaselineAnchor).isActive = true
        layoutIfNeeded()
    }
    
    private func addComponents() {
        containerView.addSubview(currencyLabel)
        stackView.addArrangedSubview(containerView)
        stackView.addArrangedSubview(valueTextField)
        addSubview(stackView)
    }
    
    private func layoutComponents() {
        currencyLabel.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        valueTextField.setContentHuggingPriority(.defaultLow, for: .horizontal)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
        
        NSLayoutConstraint.activate([
            currencyLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            currencyLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor)
        ])
    }
    
    @objc
    private func textDidChange(_ textField: UITextField) {
        guard let text = textField.text else {
            return
        }
        
        presenter.inputs.inputValue(value: text)
    }
    
    public func becomeResponder() {
        valueTextField.becomeFirstResponder()
    }
    
    public func resignResponder() {
        valueTextField.resignFirstResponder()
    }
}

extension CurrencyField: CurrencyFieldPresenterOutputs {
    func highlightColor() {
        currencyLabel.textColor = textColor
        valueTextField.textColor = textColor
    }
    
    func emptyColor() {
        currencyLabel.textColor = placeholderColor
        valueTextField.textColor = placeholderColor
    }
    
    func deleteBackward() {
        valueTextField.deleteBackward()
    }
    
    func updateTextValue(text: String?) {
        valueTextField.text = text
    }
    
    func updateValue(value: Double) {
        delegate?.onValueChange(value: value)
        self.value = value
    }
    
    func updateCurrencySymbol(text: String?) {
        currencyLabel.text = text
    }
}

public extension CurrencyField {
    enum Position {
        case top
        case center
        case bottom
    }
}
