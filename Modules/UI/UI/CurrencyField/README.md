# CurrencyField
Componente para Input de moeda no formato brasileiro. 

<p align="center" >
<img src="Screenshots/topGif.gif" width="50%" height="50%" />
</p>

<p align="center" >
<img src="Screenshots/centerGif.gif" width="50%" height="50%" />
</p>

<p align="center" >
<img src="Screenshots/bottomGif.gif" width="50%" height="50%" />
</p>

## Requisitos
- iOS 10.3 or later
- Xcode 10.0 or later

## Motivação
Atualmente existem diversos campos para input distribuidos pelo App, Recarga, P2P, PAV, DigitalGoods. Todos eles apresentam pouca variação visual e pouca variação de comportamento. Esse compoenete pretende unir
todas essa caraacteristicas em um só lugar.

## Como usar
Primeiro inicialize o componente, inserindo as costumizações necessárias. Note que os valores precisam ser customizados antes da view ser adicionada na hierarquia de views.

```swift

private lazy var valueView: CurrencyField = {
    let value = CurrencyField()
    value.fontCurrency = UIFont.systemFont(ofSize: 22, weight: .medium)
    value.fontValue = UIFont.systemFont(ofSize: 52, weight: .light)
    value.placeholderColor = #colorLiteral(red: 0.8509803922, green: 0.862745098, blue: 0.8901960784, alpha: 1)
    value.textColor = #colorLiteral(red: 0.04705882353, green: 0.9098039216, blue: 0.4901960784, alpha: 1)
    value.positionCurrency = .top
    value.limitValue = 7
    value.translatesAutoresizingMaskIntoConstraints = false
    
    return value
}()

```

Adicione a sua view.

```swift

private func buildViewHierarchy() {
    view.addSubview(valueView)
}

private func setupConstraints() {
    NSLayoutConstraint.activate([
        valueView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
        valueView.centerXAnchor.constraint(equalTo: view.centerXAnchor)
    ])
}

```

## Customização
Muitas propriedades já possuem valores padrão. Altere-os se eles não forem adequados para você.

```swift

valueView.fontCurrency = UIFont.systemFont(ofSize: 22, weight: .medium)
valueView.fontValue = UIFont.systemFont(ofSize: 52, weight: .light)
valueView.placeholderColor = #colorLiteral(red: 0.8509803922, green: 0.862745098, blue: 0.8901960784, alpha: 1)
valueView.textColor = #colorLiteral(red: 0.04705882353, green: 0.9098039216, blue: 0.4901960784, alpha: 1)
valueView.spacing = 10.0
valueView.positionCurrency = .top
valueView.limitValue = 7
valueView.hiddenCurrency = false
valueView.value = 25.50

```

## Posições
```swift
valueView.positionCurrency = .top
```

<p align="center" >
<img src="Screenshots/topImage.png" width="50%" height="50%" />
</p>

```swift
valueView.positionCurrency = .center
```

<p align="center" >
<img src="Screenshots/centerImage.png" width="50%" height="50%" />
</p>

```swift
valueView.positionCurrency = .bottom
```

<p align="center" >
<img src="Screenshots/bottomImage.png" width="50%" height="50%" />
</p>

## Delegate
Implemente  `CurrencyFieldDelegate`

```swift
valueView.delegate = self

...

extension ViewController: CurrencyFieldDelegate {
    func onValueChange(value: Double) {
    }
}

```

## Recuperar Valores
Para recuperar o valor você pode usar dois métodos:

-  O ``.value` serve tanto para definir um valor, qunaod prar pegar o valor mais atual do input.
```swift
valueView.value
```

- A função  `onValueChange` é chamada pelo `CurrencyFieldDelegate` toda vez que acontece alguma alteração no valor do input

```swift
valueView.delegate = self
```
## License
CurrencyField é liberado para uso interno da [PicPay](https://www.picpay.com/site)
