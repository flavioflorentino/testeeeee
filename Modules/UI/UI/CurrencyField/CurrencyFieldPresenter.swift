import Foundation

protocol CurrencyFieldPresenterInputs {
    func setup(initialValue: Double, limit: Int, locale: String)
    func changeInputValue(value: Double)
    func inputValue(value: String)
}

protocol CurrencyFieldPresenterOutputs: AnyObject {
    func highlightColor()
    func emptyColor()
    func deleteBackward()
    func updateTextValue(text: String?)
    func updateValue(value: Double)
    func updateCurrencySymbol(text: String?)
}

protocol CurrencyFieldPresenterProtocol: AnyObject {
    var inputs: CurrencyFieldPresenterInputs { get }
    var outputs: CurrencyFieldPresenterOutputs? { get set }
}

class CurrencyFieldPresenter: CurrencyFieldPresenterProtocol, CurrencyFieldPresenterInputs {
    private lazy var formatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currencyAccounting
        formatter.currencyDecimalSeparator = ","
        formatter.currencyGroupingSeparator = "."
        formatter.currencySymbol = String()
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        formatter.locale = Locale(identifier: locale)
        
        return formatter
    }()
    
    private var limit = Int.max
    private var locale: String = ""
    
    var inputs: CurrencyFieldPresenterInputs { self }
    weak var outputs: CurrencyFieldPresenterOutputs?
    
    func setup(initialValue: Double, limit: Int, locale: String) {
        self.limit = limit
        self.locale = locale
        changeInputValue(value: initialValue)
        setupCurrency()
    }
    
    func inputValue(value: String) {
        let text = value.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
        
        guard text.count <= limit else {
            outputs?.deleteBackward()
            return
        }
        
        formatterCurrency(text: text)
    }
    
    func changeInputValue(value: Double) {
        guard !value.isZero else {
            outputs?.updateTextValue(text: nil)
            outputs?.emptyColor()
            return
        }
        
        let number = NSNumber(value: value)
        let text = formatter.string(from: number)?.trimmingCharacters(in: .whitespaces)
        outputs?.updateTextValue(text: text)
        outputs?.highlightColor()
    }
    
    private func formatterCurrency(text: String) {
        guard let amount = formatter.number(from: text) else {
            outputs?.updateValue(value: 0.0)
            return
        }
        
        let number = NSNumber(value: (amount.doubleValue / 100))
        changeValue(value: number)
    }
    
    private func changeValue(value: NSNumber) {
        outputs?.updateValue(value: value.doubleValue)
    }
    
    private func setupCurrency() {
        let symbol = Locale(identifier: locale).currencySymbol
        outputs?.updateCurrencySymbol(text: symbol)
    }
}
