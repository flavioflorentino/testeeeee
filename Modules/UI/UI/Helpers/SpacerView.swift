import Foundation
import SnapKit
import UIKit

public final class SpacerView: UIView {
    private enum CodingKeys: String {
        case axis
        case size
    }

    private let axis: NSLayoutConstraint.Axis
    private let size: CGFloat

    public init(axis: NSLayoutConstraint.Axis = .vertical, size: CGFloat = 0.0) {
        self.axis = axis
        self.size = size
        super.init(frame: .zero)
        setup()
        setupConstraints()
    }

    public required init?(coder: NSCoder) {
        self.axis = NSLayoutConstraint.Axis(rawValue: coder.decodeInteger(forKey: CodingKeys.axis.rawValue)) ?? .vertical
        self.size = CGFloat(coder.decodeDouble(forKey: CodingKeys.size.rawValue))
        super.init(coder: coder)
        setup()
        setupConstraints()
    }

    override public func encode(with coder: NSCoder) {
        coder.encode(axis.rawValue, forKey: CodingKeys.axis.rawValue)
        coder.encode(Double(size), forKey: CodingKeys.size.rawValue)
        super.encode(with: coder)
    }

    private func setup() {
        backgroundColor = .clear
    }

    private func setupConstraints() {
        snp.makeConstraints {
            let constraint = axis == .vertical ? $0.height : $0.width
            constraint.equalTo(size)
        }
    }

    public func updateConstraint(size: CGFloat) {
        snp.updateConstraints {
            let constraint = axis == .vertical ? $0.height : $0.width
            constraint.equalTo(size)
        }
    }
}
