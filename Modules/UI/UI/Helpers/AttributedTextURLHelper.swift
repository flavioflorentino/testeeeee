import Foundation
import UIKit

/**
    AttributedTextURLInfo:
    Model with url address and text of Attributed Text
 */

public class AttributedTextURLInfo {
    var urlAddress: String
    var text: String
    
    public init(urlAddress: String, text: String) {
        self.urlAddress = urlAddress
        self.text = text
    }
}

/**
    AttributedTextURLDelegate:
    Delegate to be implemented by the class that will instantiate the Attributed Text with URL.
    This method should set the text which the url will be binded to and the url address.
 */

public protocol AttributedTextURLDelegate: AnyObject {
    func getUrlInfo(linkType: String) -> AttributedTextURLInfo
}

/**
    AttributedTextURLHelper:
    Helper to set link into the Attributed Text.
 */

public enum AttributedTextURLHelper {
    public static var linkAttributes: [NSAttributedString.Key: Any] {
        [
            .font: Typography.bodyPrimary().font(),
            .underlineStyle: NSUnderlineStyle.single.rawValue,
            .foregroundColor: Colors.branding400.color
        ]
    }
    
    public static func createAttributedTextWithLinks(
        text: String,
        attributedTextURLDelegate: AttributedTextURLDelegate,
        linkTypes: [String],
        descriptionAttributes: [NSAttributedString.Key: Any] = [:]
    ) -> NSMutableAttributedString {
        let attributedText = createAttributedText(text: text, descriptionAttributes: descriptionAttributes)
        
        for linkType in linkTypes {
            addUrlToTextSection(
                attributedText: attributedText,
                attributedTextURLDelegate: attributedTextURLDelegate,
                linkType: linkType
            )
        }
        return attributedText
    }
}

// MARK: Support Methods

private extension AttributedTextURLHelper {
    static var descriptionAttributes: [NSAttributedString.Key: Any] {
        let style = NSMutableParagraphStyle()
        style.lineHeightMultiple = 1.26
        style.alignment = .left
        
        return [
            .font: Typography.bodyPrimary().font(),
            .foregroundColor: Colors.grayscale700.color,
            .paragraphStyle: style
        ]
    }
    
    static func addUrlToTextSection(
        attributedText: NSMutableAttributedString,
        attributedTextURLDelegate: AttributedTextURLDelegate,
        linkType: String
    ) {
        let urlInfo = attributedTextURLDelegate.getUrlInfo(linkType: linkType)
        guard let url = URL(string: urlInfo.urlAddress) else {
            return
        }
        let linkRange = attributedText.string.substringRange(of: urlInfo.text)
        attributedText.addAttribute(.link, value: url, range: linkRange)
    }
    
    static func attributes(of descriptionAttributes: [NSAttributedString.Key: Any]) -> [NSAttributedString.Key: Any] {
        guard !descriptionAttributes.isEmpty else {
            return self.descriptionAttributes
        }
        return descriptionAttributes
    }
    
    static func createAttributedText(text: String, descriptionAttributes: [NSAttributedString.Key: Any]) -> NSMutableAttributedString {
        NSMutableAttributedString(
            string: text,
            attributes: attributes(of: descriptionAttributes)
        )
    }
}
