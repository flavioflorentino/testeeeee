import UIKit

protocol UserGrouperPresenting {
    func presentUserQuantity(current: Int, max: Int)
    func presentUsers(_ users: [GroupedUser])
    func presentRemoveUser(at indexPath: IndexPath)
}

final class UserGrouperPresenter {
    // MARK: - Variables
    private let quantityText: String
    private let placeholderImage: UIImage?
    weak var view: UserGrouperDisplay?
    
    // MARK: - Life Cycle
    init(quantityText: String, placeholderImage: UIImage?) {
        self.quantityText = quantityText
        self.placeholderImage = placeholderImage
    }
}

// MARK: - UserGrouperPresenting
extension UserGrouperPresenter: UserGrouperPresenting {
    func presentUserQuantity(current: Int, max: Int) {
        let text = String(format: quantityText, current, max)
        view?.displayUserQuantity(text: text)
    }
    
    func presentUsers(_ users: [GroupedUser]) {
        let formattedUsers = users.map { ($0.name, $0.imageUrl) }
        view?.displayUsers(formattedUsers, placeholderImage: placeholderImage)
    }
    
    func presentRemoveUser(at indexPath: IndexPath) {
        view?.removeUser(at: indexPath)
    }
}
