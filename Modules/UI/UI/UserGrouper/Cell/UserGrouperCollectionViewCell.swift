import UIKit

extension UserGrouperCollectionViewCell.Layout {
    enum Font {
        static let username = UIFont.systemFont(ofSize: 12)
    }
    
    enum Size {
        static let userImage = CGSize(width: 44, height: 44)
        static let close = CGSize(width: 14, height: 14)
    }
}

final class UserGrouperCollectionViewCell: UICollectionViewCell {
    fileprivate enum Layout {}
    
    // MARK: - Visual Components
    private lazy var userImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = Layout.Size.userImage.height / 2
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    private lazy var closeImageView = UIImageView(image: Assets.icoCloseFilled.image)
    
    private lazy var usernameLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.username
        label.textColor = Colors.grayscale700.color
        label.textAlignment = .center
        return label
    }()
    
    // MARK: - Life Cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        userImageView.cancelRequest()
    }
    
    // MARK: - Content
    func set(username: String, imageUrl: URL?, placeholderImage: UIImage?) {
        usernameLabel.text = username
        userImageView.setImage(url: imageUrl, placeholder: placeholderImage)
    }
}

// MARK: - ViewConfiguration
extension UserGrouperCollectionViewCell: ViewConfiguration {
    func setupConstraints() {
        userImageView.layout {
            $0.width == Layout.Size.userImage.width
            $0.height == Layout.Size.userImage.height
            $0.top == topAnchor + Spacing.base00
            $0.leading == leadingAnchor + Spacing.base01
            $0.trailing == trailingAnchor - Spacing.base01
            $0.bottom == usernameLabel.topAnchor - Spacing.base00
        }
        
        usernameLabel.layout {
            $0.leading == leadingAnchor
            $0.trailing == trailingAnchor
            $0.bottom == bottomAnchor
        }
        
        closeImageView.layout {
            $0.width == Layout.Size.close.width
            $0.height == Layout.Size.close.height
            $0.top == userImageView.topAnchor - Spacing.base00
            $0.trailing == userImageView.trailingAnchor + Spacing.base00
        }
    }
    
    func buildViewHierarchy() {
        addSubview(userImageView)
        addSubview(usernameLabel)
        addSubview(closeImageView)
    }
}
