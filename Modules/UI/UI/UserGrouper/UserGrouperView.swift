import UIKit

protocol UserGrouperDisplay: AnyObject {
    func displayUserQuantity(text: String)
    func displayUsers(_ users: [(String, URL?)], placeholderImage: UIImage?)
    func removeUser(at indexPath: IndexPath)
}

public protocol UserGrouping: UIView {
    var groupingDelegate: UserGrouperDelegate? { get set }
    @discardableResult
    func add(newUser: GroupedUser) -> Bool
    @discardableResult
    func removeUser(id: String) -> Bool
}

private extension UserGrouperView.Layout {
    enum Font {
        static let title = UIFont.systemFont(ofSize: 12, weight: .bold)
        static let quantity = UIFont.systemFont(ofSize: 12)
    }
    
    enum Size {
        static let cell = CGSize(width: 60, height: 64)
    }
}

final class UserGrouperView: UIView {
    fileprivate enum Layout {}
    
    // MARK: - Visual Components
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.title
        label.textColor = Colors.grayscale700.color
        return label
    }()
    
    private lazy var quantityLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.quantity
        label.textColor = Colors.grayscale300.color
        return label
    }()
    
    private lazy var userCollectionView: UICollectionView = {
        let collectionLayout = UICollectionViewFlowLayout()
        collectionLayout.scrollDirection = .horizontal
        collectionLayout.itemSize = Layout.Size.cell
        collectionLayout.minimumInteritemSpacing = Spacing.base00
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionLayout)
        collectionView.register(
            UserGrouperCollectionViewCell.self,
            forCellWithReuseIdentifier: UserGrouperCollectionViewCell.identifier
        )
        collectionView.backgroundColor = .clear
        collectionView.delegate = self
        return collectionView
    }()
    
    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .leading
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var textStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Spacing.base00
        return stackView
    }()
    
    // MARK: - Variables
    var groupingDelegate: UserGrouperDelegate? {
        get {
            viewModel.delegate
        }
        set {
            viewModel.delegate = newValue
        }
    }
    let viewModel: UserGrouperViewModelInputs
    private var dataSource: CollectionViewHandler<(username: String, imageUrl: URL?), UserGrouperCollectionViewCell>?
    
    // MARK: - Life Cycle
    init(viewModel: UserGrouperViewModelInputs, title: String) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        buildLayout()
        titleLabel.text = title
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func loadInitialData() {
        viewModel.loadInitialData()
    }
}

// MARK: - ViewConfiguration
extension UserGrouperView: ViewConfiguration {
    func setupConstraints() {
        contentStackView.layout {
            $0.top == topAnchor + Spacing.base02
            $0.bottom == bottomAnchor - Spacing.base02
            $0.leading == leadingAnchor + Spacing.base02
            $0.trailing == trailingAnchor - Spacing.base02
        }
        
        userCollectionView.layout {
            $0.height == Layout.Size.cell.height
            $0.leading == contentStackView.leadingAnchor
            $0.trailing == contentStackView.trailingAnchor
        }
    }
    
    func buildViewHierarchy() {
        addSubview(contentStackView)
        contentStackView.addArrangedSubview(textStackView)
        contentStackView.addArrangedSubview(userCollectionView)
        textStackView.addArrangedSubview(titleLabel)
        textStackView.addArrangedSubview(quantityLabel)
    }
}

// MARK: - UICollectionViewDelegate
extension UserGrouperView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.removeUser(at: indexPath)
    }
}

// MARK: - UserGrouperDisplay
extension UserGrouperView: UserGrouperDisplay {
    func displayUserQuantity(text: String) {
        quantityLabel.text = text
    }
    
    func displayUsers(_ users: [(String, URL?)], placeholderImage: UIImage?) {
        dataSource = CollectionViewHandler(data: users, cellType: UserGrouperCollectionViewCell.self) { _, data, cell in
            cell.set(username: data.username, imageUrl: data.imageUrl, placeholderImage: placeholderImage)
        }
        userCollectionView.dataSource = dataSource
    }
    
    func removeUser(at indexPath: IndexPath) {
        dataSource?.deleteItem(at: indexPath)
        self.userCollectionView.deleteItems(at: [indexPath])
    }
}

// MARK: - UserGrouping
extension UserGrouperView: UserGrouping {
    @discardableResult
    func add(newUser: GroupedUser) -> Bool {
        viewModel.add(newUser: newUser)
    }
    
    @discardableResult
    func removeUser(id: String) -> Bool {
        viewModel.removeUser(id: id)
    }
}
