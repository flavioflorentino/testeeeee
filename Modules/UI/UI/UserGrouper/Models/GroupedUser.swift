import Foundation

public struct GroupedUser {
    public let id: String
    let imageUrl: URL?
    let name: String
    
    public init(id: String, imageUrl: URL?, name: String) {
        self.id = id
        self.imageUrl = imageUrl
        self.name = name
    }
}

// MARK: - Equatable
extension GroupedUser: Equatable {
    public static func == (lhs: Self, rhs: Self) -> Bool {
        lhs.id == rhs.id
    }
}
