import Foundation

protocol UserGrouperViewModelInputs: AnyObject {
    var delegate: UserGrouperDelegate? { get set }
    func loadInitialData()
    func add(newUser: GroupedUser) -> Bool
    func removeUser(id: String) -> Bool
    func removeUser(at indexPath: IndexPath)
}

public protocol UserGrouperDelegate: AnyObject {
    func didRemove(user: GroupedUser)
}

final class UserGrouperViewModel {
    // MARK: - Variables
    private let presenter: UserGrouperPresenting
    private var users: [GroupedUser]
    private let maxUserQuantity: Int
    weak var delegate: UserGrouperDelegate?
    
    // MARK: - Life Cycle
    init(presenter: UserGrouperPresenting, maxUserQuantity: Int, initialUsers: [GroupedUser]) {
        self.presenter = presenter
        self.users = initialUsers
        self.maxUserQuantity = maxUserQuantity
    }
}

// MARK: - UserGrouperViewModelInputs
extension UserGrouperViewModel: UserGrouperViewModelInputs {
    func loadInitialData() {
        presenter.presentUserQuantity(current: users.count, max: maxUserQuantity)
        presenter.presentUsers(users)
    }
    
    func add(newUser: GroupedUser) -> Bool {
        guard
            users.count + 1 <= maxUserQuantity,
            !users.contains(where: { $0.id == newUser.id })
            else {
                return false
        }
        users.append(newUser)
        presenter.presentUserQuantity(current: users.count, max: maxUserQuantity)
        presenter.presentUsers(users)
        return true
    }
    
    func removeUser(id: String) -> Bool {
        let foundUser = users.enumerated().first { $1.id == id }
        guard let removedUserIndex = foundUser?.offset else {
            return false
        }
        let indexPath = IndexPath(row: removedUserIndex, section: 0)
        removeUser(at: indexPath)
        return true
    }
    
    func removeUser(at indexPath: IndexPath) {
        guard indexPath.row < users.count else {
            return
        }
        let removedUser = users.remove(at: indexPath.row)
        delegate?.didRemove(user: removedUser)
        presenter.presentUserQuantity(current: users.count, max: maxUserQuantity)
        presenter.presentRemoveUser(at: indexPath)
    }
}
