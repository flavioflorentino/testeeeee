import UIKit

public enum UserGrouperFactory {
    public static func make(
        title: String, 
        quantityText: String, 
        maxUserQuantity: Int, 
        initialUsers: [GroupedUser], 
        placeholderImage: UIImage?
    ) -> UserGrouping {
        let presenter = UserGrouperPresenter(quantityText: quantityText, placeholderImage: placeholderImage)
        let viewModel = UserGrouperViewModel(presenter: presenter, maxUserQuantity: maxUserQuantity, initialUsers: initialUsers)
        let view = UserGrouperView(viewModel: viewModel, title: title)
        presenter.view = view
        view.loadInitialData()
        return view
    }
}
