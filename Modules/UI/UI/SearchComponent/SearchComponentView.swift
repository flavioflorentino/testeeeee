import UIKit

extension SearchComponentView.Layout {
    static let height: CGFloat = 40.0
}

public final class SearchComponentView: UIView {
    fileprivate enum Layout {}
    
    public private(set) lazy var textField: SearchComponentTextField = {
        let view = SearchComponentTextFieldFactory.make()
        view.backgroundColor = Colors.backgroundPrimary.color
        return view
    }()
    
    public init(placeholder: String, frame: CGRect = .zero, color: UIColor = Colors.grayscale700.color) {
        super.init(frame: frame)
        buildLayout()
        configurePlaceholder(placeholder, color: color)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configurePlaceholder(_ placeholder: String, color: UIColor) {
        textField.attributedPlaceholder = NSAttributedString(
            string: placeholder,
            attributes: [.foregroundColor: color]
        )
    }
}

extension SearchComponentView: ViewConfiguration {
    public func setupConstraints() {
        textField.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.top.bottom.equalToSuperview()
            $0.height.equalTo(SearchComponentView.Layout.height)
        }
    }
    
    public func buildViewHierarchy() {
        addSubview(textField)
    }
}
