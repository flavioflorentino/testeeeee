import UIKit

private extension SearchComponentTextField.Layout {
    static let fontSize: CGFloat = 14.0
    static let cornerRadius: CGFloat = 20.0
    static let padding = UIEdgeInsets(top: 0, left: Spacing.base02, bottom: 0, right: Spacing.base03)
}

public final class SearchComponentTextField: UITextField {
    // MARK: - Variables
    private var viewModel: SearchComponentTextFieldViewModelInputs
    
    fileprivate enum Layout { }
    
    private lazy var magnifyingGlassImageView: UIImageView = {
        let image = Assets.magnifyingGlass.image
        let imageView = UIImageView(image: image)
        imageView.tintColor = Colors.grayscale300.color
        return imageView
    }()

    // swiftlint:disable:next implicitly_unwrapped_optional
    override public var tintColor: UIColor! {
        didSet {
            magnifyingGlassImageView.tintColor = tintColor
        }
    }
    
    // MARK: - Life Cycle
    init(viewModel: SearchComponentTextFieldViewModelInputs, frame: CGRect = .zero) {
        self.viewModel = viewModel
        super.init(frame: frame)
        leftViewMode = .always
        leftView = magnifyingGlassImageView
        autocapitalizationType = .none
        clearButtonMode = .whileEditing
        layer.cornerRadius = Layout.cornerRadius
        addTarget(self, action: #selector(editingChanged), for: .editingChanged)
    }
    
    @available (*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Functions
    override public func textRect(forBounds bounds: CGRect) -> CGRect {
        adjustRectWithWidthLeftView(rect: bounds)
    }
    
    override public func editingRect(forBounds bounds: CGRect) -> CGRect {
        adjustRectWithWidthLeftView(rect: bounds)
    }
    
    override public func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var rect = super.leftViewRect(forBounds: bounds)
        rect.origin.x += Spacing.base02
        return rect
    }

    override public func clearButtonRect(forBounds bounds: CGRect) -> CGRect {
        var rect = super.clearButtonRect(forBounds: bounds)
        rect.origin.x -= Spacing.base01

        return rect
    }
    
    private func adjustRectWithWidthLeftView(rect: CGRect) -> CGRect {
        let paddedRect = rect.inset(by: SearchComponentTextField.Layout.padding)
        guard let leftView = leftView else {
            return paddedRect
        }
        return CGRect(
            x: paddedRect.origin.x + leftView.frame.width + Spacing.base01,
            y: paddedRect.origin.y,
            width: paddedRect.width,
            height: paddedRect.height
        )
    }
    
    @objc
    private func editingChanged() {
        guard let text = text else {
            return
        }
        viewModel.updateText(text)
    }
}

extension SearchComponentTextField: SearchComponentTextFieldDisplay {
    func updateText(_ text: String) {
        self.text = text
    }
}
