import Foundation

protocol SearchComponentTextFieldViewModelInputs: AnyObject {
    func updateText(_ text: String)
}

final class SearchComponentTextFieldViewModel {
    private let presenter: SearchComponentTextFieldPresenting
    private let maxCharacters: Int

    init(presenter: SearchComponentTextFieldPresenting, maxCharacters: Int) {
        self.presenter = presenter
        self.maxCharacters = maxCharacters
    }
}

extension SearchComponentTextFieldViewModel: SearchComponentTextFieldViewModelInputs {
    func updateText(_ text: String) {
        let substring = text.prefix(maxCharacters)
        presenter.updateText(String(substring))
    }
}
