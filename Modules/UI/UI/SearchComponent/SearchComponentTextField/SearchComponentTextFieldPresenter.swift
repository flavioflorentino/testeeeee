import Foundation

protocol SearchComponentTextFieldPresenting: AnyObject {
    var view: SearchComponentTextFieldDisplay? { get set }
    func updateText(_ text: String)
}

final class SearchComponentTextFieldPresenter: SearchComponentTextFieldPresenting {
    weak var view: SearchComponentTextFieldDisplay?
    
    func updateText(_ text: String) {
        view?.updateText(text)
    }
}
