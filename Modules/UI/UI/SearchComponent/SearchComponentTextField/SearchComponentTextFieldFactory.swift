import Foundation

public enum SearchComponentTextFieldFactory {
    public static func make(maxCharacters: Int = 20) -> SearchComponentTextField {
        let presenter = SearchComponentTextFieldPresenter()
        let viewModel = SearchComponentTextFieldViewModel(presenter: presenter, maxCharacters: maxCharacters)
        let view = SearchComponentTextField(viewModel: viewModel)
        presenter.view = view
        return view
    }
}
