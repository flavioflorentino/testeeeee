import UIKit

protocol SearchComponentTextFieldDisplay: AnyObject {
    func updateText(_ text: String)
}
