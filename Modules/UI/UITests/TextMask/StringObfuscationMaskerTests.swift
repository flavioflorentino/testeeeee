import XCTest
@testable import UI

final class StringObfuscationMaskerTests: XCTestCase {
    func testStringObfuscationMaskerCNPJ_ShouldDoNothing() {
        let cnpjMask = StringObfuscationMasker.mask(cnpj: nil)
        XCTAssertNil(cnpjMask)
    }

    func testStringObfuscationMaskerCNPJ_ShouldObfuscateAndMask() {
        let cnpj1 = "53602019000174"
        let cnpjMask1 = StringObfuscationMasker.mask(cnpj: cnpj1)
        XCTAssertEqual(cnpjMask1, "53.***.***/0001-74")

        let cnpj2 = "71254911000193"
        let cnpjMask2 = StringObfuscationMasker.mask(cnpj: cnpj2)
        XCTAssertEqual(cnpjMask2, "71.***.***/0001-93")

        let cnpj3 = "22481865000150"
        let cnpjMask3 = StringObfuscationMasker.mask(cnpj: cnpj3)
        XCTAssertEqual(cnpjMask3, "22.***.***/0001-50")

        let cnpj4 = "84084657000174"
        let cnpjMask4 = StringObfuscationMasker.mask(cnpj: cnpj4)
        XCTAssertEqual(cnpjMask4, "84.***.***/0001-74")

        let cnpj5 = "97179047000104"
        let cnpjMask5 = StringObfuscationMasker.mask(cnpj: cnpj5)
        XCTAssertEqual(cnpjMask5, "97.***.***/0001-04")

        let cnpj6 = "44433253000124"
        let cnpjMask6 = StringObfuscationMasker.mask(cnpj: cnpj6)
        XCTAssertEqual(cnpjMask6, "44.***.***/0001-24")

        let cnpj7 = "52547605000109"
        let cnpjMask7 = StringObfuscationMasker.mask(cnpj: cnpj7)
        XCTAssertEqual(cnpjMask7, "52.***.***/0001-09")

        let cnpj8 = "72720329000138"
        let cnpjMask8 = StringObfuscationMasker.mask(cnpj: cnpj8)
        XCTAssertEqual(cnpjMask8, "72.***.***/0001-38")

        let cnpj9 = "04264115000144"
        let cnpjMask9 = StringObfuscationMasker.mask(cnpj: cnpj9)
        XCTAssertEqual(cnpjMask9, "04.***.***/0001-44")
    }
}
