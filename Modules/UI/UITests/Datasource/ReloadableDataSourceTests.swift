import XCTest
@testable import UI

struct DataSpy: Hashable {
    var index: Int = 0
    var title: String? = ""
}

struct CellStub {
    var cell: String
}

struct ItemStub: Hashable {
    var item: String

    init(_ item: String) {
        self.item = item
    }
}

final class ReloadableViewSpy: ReloadableView {
    var reloadDataCount = 0

    func reloadData() {
        reloadDataCount += 1
    }
}

final class ReloadableDataSourceTests: XCTestCase {
    private lazy var data = DataSpy()
    private lazy var viewSpy = ReloadableViewSpy()
    private lazy var sut = ReloadableDataSource<ReloadableViewSpy, CellStub, DataSpy, ItemStub>(view: viewSpy)

    // MARK: - add section
    func testAddSection_WhenHasNoSection_ShouldContainTheSection() {
        let section = DataSpy(index: 1, title: "Section")
        sut.add(section: section)

        XCTAssertTrue(sut.sections.contains(section))
        XCTAssertEqual(sut.sections.count, 1)
    }

    func testAddSection_WhenTheSectionAlereadyExists_ShouldContainTheSection() {
        let section = DataSpy(index: 1, title: "Section")
        sut.add(section: section)
        sut.add(section: section)

        XCTAssertTrue(sut.sections.contains(section))
        XCTAssertEqual(sut.sections.count, 1)
    }

    func testAddSection_WhenTheSectionIndexAlreadyExists_ShouldContainTheSection() {
        let section = DataSpy(index: 1, title: "Section")
        sut.add(section: section)

        let sectionWithSameIndex = DataSpy(index: 1, title: "Section With Same Index")
        sut.add(section: sectionWithSameIndex)

        XCTAssertTrue(sut.sections.contains(section))
        XCTAssertTrue(sut.sections.contains(sectionWithSameIndex))
        XCTAssertEqual(sut.sections.count, 2)
    }

    func testAddSection_WhenTheSectionHasTheSameIndexAndData_ShouldContainTheSections() {
        let section = DataSpy(index: 1, title: "Section")
        sut.add(section: section)

        let sectionWithSameIndex = DataSpy(index: 1, title: "Section")
        sut.add(section: sectionWithSameIndex)

        XCTAssertTrue(sut.sections.contains(section))
        XCTAssertTrue(sut.sections.contains(sectionWithSameIndex))
        XCTAssertEqual(sut.sections.count, 1)
    }

    // MARK: - add itens to section
    func testAddItems_WhenHasNoSection_ShouldContainTheSection() {
        let data = [
            ItemStub("A"),
            ItemStub("B"),
            ItemStub("C")
        ]
        let section = DataSpy(index: 1, title: "Section")

        sut.add(items: data, to: section)

        XCTAssertTrue(sut.sections.contains(section))
        XCTAssertTrue(sut.data.values.contains(data))
        XCTAssertEqual(sut.sections.count, 1)
    }

    func testAddItems_WhenTheSectionAlreadyExists_ShouldContainTheSection() {
        let data = [
            ItemStub("A"),
            ItemStub("B"),
            ItemStub("C")
        ]
        let section = DataSpy(index: 1, title: "Section")

        sut.add(section: section)
        sut.add(items: data, to: section)

        XCTAssertTrue(sut.sections.contains(section))
        XCTAssertTrue(sut.data.values.contains(data))
        XCTAssertEqual(sut.sections.count, 1)
    }

    func testAddItems_WhenTheSectionHasTheSameIndex_ShouldContainTheSections() {
        let section = DataSpy(index: 1, title: "Section")
        sut.add(section: section)

        let data = [
            ItemStub("I"),
            ItemStub("II"),
            ItemStub("III")
        ]
        let sectionWithSameIndex = DataSpy(index: 1, title: "Section With Same Index")
        sut.add(items: data, to: sectionWithSameIndex)

        XCTAssertTrue(sut.sections.contains(section))
        XCTAssertTrue(sut.sections.contains(sectionWithSameIndex))
        XCTAssertTrue(sut.data.contains(where: {$0.key == sectionWithSameIndex && $0.value == data}))
        XCTAssertEqual(sut.sections.count, 2)
    }

    func testAddItems_WhenTheSectionHasTheSameIndexAndData_ShouldContainTheSection() {
        let data = [
            ItemStub("A"),
            ItemStub("B"),
            ItemStub("C")
        ]
        let section = DataSpy(index: 1, title: "Section")
        sut.add(section: section)

        let sectionWithSameIndex = DataSpy(index: 1, title: "Section")
        sut.add(items: data, to: sectionWithSameIndex)

        XCTAssertEqual(section, sectionWithSameIndex)
        XCTAssertTrue(sut.sections.contains(section))
        XCTAssertTrue(sut.sections.contains(sectionWithSameIndex))
        XCTAssertEqual(sut.sections.count, 1)
    }

    // MARK: - update itens from section
    func testUpdateItems_WhenHasNoSection_ShouldContainTheSection() {
        let data = [
            ItemStub("A"),
            ItemStub("B"),
            ItemStub("C")
        ]
        let section = DataSpy(index: 1, title: "Section")
        sut.update(items: data, from: section)

        XCTAssertFalse(sut.sections.contains(section))
        XCTAssertFalse(sut.data.contains(where: { $0.key == section && $0.value == data }))
        XCTAssertEqual(sut.sections.count, 0)
    }

    func testUpdateItems_WhenTheSectionAlreadyExists_ShouldContainTheSection() {
        let data = [
            ItemStub("A"),
            ItemStub("B"),
            ItemStub("C")
        ]
        let section = DataSpy(index: 1, title: "Section")
        sut.add(section: section)

        sut.update(items: data, from: section)

        XCTAssertTrue(sut.sections.contains(section))
        XCTAssertTrue(sut.data.contains(where: { $0.key == section && $0.value == data }))
        XCTAssertEqual(sut.sections.count, 1)
    }

    func testUpdateItems_WhenTheSectionHasTheSameIndex_ShouldContainTheSection() {
        let section = DataSpy(index: 1, title: "Section")
        sut.add(section: section)

        let data = [
            ItemStub("I"),
            ItemStub("II"),
            ItemStub("III")
        ]
        let sectionWithSameIndex = DataSpy(index: 1, title: "Section With Same Index")
        sut.add(items: data, to: sectionWithSameIndex)

        XCTAssertTrue(sut.sections.contains(section))
        XCTAssertTrue(sut.sections.contains(sectionWithSameIndex))
        XCTAssertTrue(sut.data.contains(where: { $0.key == sectionWithSameIndex && $0.value == data }))
        XCTAssertEqual(sut.sections.count, 2)
    }

    func testUpdateItems_WhenTheSectionHasTheSameIndexAndData_ShouldContainTheSection() {
        let data = [
            ItemStub("A"),
            ItemStub("B"),
            ItemStub("C")
        ]
        let section = DataSpy(index: 1, title: "Section")
        sut.add(section: section)

        let sectionWithSameIndex = DataSpy(index: 1, title: "Section")
        sut.add(items: data, to: sectionWithSameIndex)

        XCTAssertEqual(section, sectionWithSameIndex)
        XCTAssertTrue(sut.sections.contains(section))
        XCTAssertTrue(sut.sections.contains(sectionWithSameIndex))
        XCTAssertTrue(sut.data.contains(where: { $0.key == sectionWithSameIndex && $0.value == data }))
        XCTAssertEqual(sut.sections.count, 1)
    }

    // MARK: - remove section
    func testRemoveSection_WhenHasNoSection_ShouldContainTheSection() {
        let section = DataSpy(index: 1, title: "Section")
        sut.remove(section: section)

        XCTAssertFalse(sut.sections.contains(section))
        XCTAssertEqual(sut.sections.count, 0)
    }

    func testRemoveSection_WhenTheSectionAlreadyExists_ShouldContainTheSection() {
        let section = DataSpy(index: 1, title: "Section")
        sut.add(section: section)
        sut.remove(section: section)

        XCTAssertFalse(sut.sections.contains(section))
        XCTAssertEqual(sut.sections.count, 0)
    }

    func testRemoveSection_WhenTheSectionHasTheSameIndex_ShouldContainTheSection() {
        let section = DataSpy(index: 1, title: "Section")
        sut.add(section: section)

        let data = [
            ItemStub("I"),
            ItemStub("II"),
            ItemStub("III")
        ]
        let sectionWithSameIndex = DataSpy(index: 1, title: "Section With Same Index")
        sut.add(items: data, to: sectionWithSameIndex)

        sut.remove(section: sectionWithSameIndex)

        XCTAssertTrue(sut.sections.contains(section))
        XCTAssertFalse(sut.sections.contains(sectionWithSameIndex))
        XCTAssertFalse(sut.data.contains(where: { $0.key == sectionWithSameIndex && $0.value == data }))
        XCTAssertEqual(sut.sections.count, 1)
    }

    func testRemoveSection_WhenTheSectionHasTheSameIndexAndData_ShouldContainTheSection() {
        let data = [
            ItemStub("A"),
            ItemStub("B"),
            ItemStub("C")
        ]
        let section = DataSpy(index: 1, title: "Section")
        sut.add(items: data, to: section)

        let sectionWithSameIndex = DataSpy(index: 1, title: "Section")
        sut.add(items: data, to: sectionWithSameIndex)

        sut.remove(section: sectionWithSameIndex)

        XCTAssertFalse(sut.sections.contains(section))
        XCTAssertFalse(sut.sections.contains(sectionWithSameIndex))
        XCTAssertFalse(sut.data.contains(where: { $0.key == sectionWithSameIndex && $0.value == data }))
        XCTAssertEqual(sut.sections.count, 0)
    }

    // MARK: - item at indexPath
    func testItemAtIndexPath_WhenHasNoSection_ShouldNotContainTheItem() {
        let indexPath = IndexPath(row: 1, section: 0)
        let item = sut.item(at: indexPath)

        XCTAssertNil(item)
        XCTAssertEqual(sut.data.count, 0)
    }

    func testItemAtIndexPath_WhenTheSectionAlreadyExists_ShouldContainTheItem() throws {
        let stubItem = ItemStub("An Item")
        let section = DataSpy(index: 0)
        sut.add(items: [stubItem], to: section)

        let indexPath = IndexPath(row: 0, section: 0)
        let item = try XCTUnwrap(sut.item(at: indexPath), "Item do not exist")

        XCTAssertEqual(item, stubItem)
    }

    func testItemAtIndexPath_WhenTheSectionAlreadyExistsButHasNotItem_ShouldNotContainTheItem() {
        let section = DataSpy(index: 1, title: "Section")
        sut.add(section: section)

        let indexPath = IndexPath(row: 0, section: 0)
        let item = sut.item(at: indexPath)

        XCTAssertNil(item)
    }

    func testItemAtIndexPath_WhenTheIndexPathIsOutOfBoundsInSection_ShouldNotContainTheItem() {
        let stubItem = ItemStub("The Second Item")
        let stubItens = [
            ItemStub("The First Item"),
            stubItem,
            ItemStub("The Third Item")
        ]
        let section = DataSpy(index: 0)
        sut.add(items: stubItens, to: section)

        let indexPath = IndexPath(row: 0, section: 10)
        let item = sut.item(at: indexPath)

        XCTAssertNil(item)
    }

    func testItemAtIndexPath_WhenTheIndexPathIsOutOfBoundsInItem_ShouldNotContainTheItem() {
        let stubItem = ItemStub("The Second Item")
        let stubItens = [
            ItemStub("The First Item"),
            stubItem,
            ItemStub("The Third Item")
        ]
        let section = DataSpy(index: 0)
        sut.add(items: stubItens, to: section)

        let indexPath = IndexPath(row: 10, section: 0)
        let item = sut.item(at: indexPath)

        XCTAssertNil(item)
    }


    func testItemAtIndexPath_WhenTheSectionHasTheSameIndex_Should___() throws {
        let section = DataSpy(index: 1, title: "Section")
        sut.add(section: section)

        let data2 = [
            ItemStub("I"),
            ItemStub("II"),
            ItemStub("III")
        ]
        let sectionWithSameIndex = DataSpy(index: 1, title: "Section With Same Index")
        sut.add(items: data2, to: sectionWithSameIndex)

        let indexPath = IndexPath(row: 1, section: 1)
        let item = try XCTUnwrap(sut.item(at: indexPath), "Item do not exist")

        XCTAssertTrue(sut.data.contains(where: { $0.key == sectionWithSameIndex && $0.value == data2}))
        XCTAssertTrue(sut.data.contains(where: { $0.key == sectionWithSameIndex && $0.value == data2 && $0.value.contains(item) }))
    }

    func testItemAtIndexPath_WhenTheSectionHasTheSameIndexAndData_Should___() throws {
        let data = [
            ItemStub("A"),
            ItemStub("B"),
            ItemStub("C")
        ]
        let section = DataSpy(index: 1, title: "Section")
        sut.add(items: data, to: section)

        let sectionWithSameIndex = DataSpy(index: 1, title: "Section")
        sut.add(items: data, to: sectionWithSameIndex)

        let indexPath = IndexPath(row: 1, section: 1)
        let item = sut.item(at: indexPath)

        XCTAssertNil(item)
    }
}
