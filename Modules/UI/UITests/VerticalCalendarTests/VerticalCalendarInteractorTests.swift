import XCTest
@testable import UI

private final class VerticalCalendarDelegateSpy: VerticalCalendarDelegate, VerticalCalendarValidatorDelegate {
    private(set) var callDidSelectDateCount = 0
    private(set) var callIsAvailableCount = 0
    
    var dateIsAvailableMock: Bool = true
    var availableDate: Date?
    
    func didSelectDate(date: Date) {
        callDidSelectDateCount += 1
    }
    
    func isAvailable(date: Date) -> Bool {
        callIsAvailableCount += 1
        
        if let availableDate = availableDate {
            return Calendar.current.isDate(date, inSameDayAs: availableDate)
        } else {
            return dateIsAvailableMock
        }
    }
}

private final class VerticalCalendarPresenterSpy: VerticalCalendarPresenting {
    private(set) var callPresentDatesCount = 0
    private(set) var configs: [VerticalCalendarMonthConfigurator]?
    
    var view: VerticalCalendarDisplaying?
    
    func displayDates(configs: [VerticalCalendarMonthConfigurator]) {
        callPresentDatesCount += 1
        self.configs = configs
    }
}

final class VerticalCalendarInteractorTests: XCTestCase {
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return dateFormatter
    }()
    
    private lazy var baseDate: Date? = {
        return dateFormatter.date(from: "16/12/2020")
    }()
    
    private lazy var presenterSpy = VerticalCalendarPresenterSpy()
    private let delegate = VerticalCalendarDelegateSpy()
    private lazy var sut = VerticalCalendarInteractor(selectedDate: Date(), presenter: presenterSpy, delegate: delegate, validator: delegate)
    
    func testLoadCalendar_ShouldPresentDates() {
        sut.loadCalendar(selectedDate: Date(), numberOfMonths: 3)
        XCTAssertEqual(presenterSpy.callPresentDatesCount, 1)
    }
    
    func testCreateMonth_WhenCalledWithABaseDate_ShouldReturnVerticalCalendarMonthCreated() throws {
        let date = try XCTUnwrap(baseDate, "'baseDate' variable cannot be nil")
        let month = try XCTUnwrap(sut.createMonth(for: date), "createMonth: month created cannot be nil")
        
        XCTAssertEqual(month.firstDayWeekday, 3)
        XCTAssertEqual(month.numberOfDays, 31)
        XCTAssertEqual(month.firstDay, dateFormatter.date(from: "1/12/2020"))
    }
    
    func testCreateMonthDays_WhenCalledWithADate_ShouldReturnAllDaysInBaseMonthCreated() throws {
        let date = try XCTUnwrap(baseDate, "'baseDate' variable cannot be nil")
        let days = sut.createMonthDays(for: date)
        
        XCTAssertEqual(days.count, 35)
        XCTAssertEqual(days[0].date, dateFormatter.date(from: "29/11/2020"))
        XCTAssertEqual(days[34].date, dateFormatter.date(from: "02/01/2021"))
    }
    
    func testDidSelectDate_WhenSelectADate_GivenAnDelegate_ItShouldCallDidSelectDateDelegateMethod() {
        sut.select(day: VerticalCalendarDay(date: Date(), number: "1", isSelected: false, isWithinDisplayedMonth: true, isAvailable: true))
        XCTAssertEqual(delegate.callDidSelectDateCount, 1)
    }
    
    func testCreateDay_WhenCalledWithAValidatorDelegate_GivenAnUnavailableDate_ItShouldReturnAnUnavailableDay() {
        delegate.dateIsAvailableMock = false
        let day = sut.createDay(offsetBy: 10, for: Date(), isWithinDisplayedMonth: true)
        XCTAssertFalse(day.isAvailable)
    }
    
    func testCreateDay_WhenCalledWithAValidatorDelegate_GivenAnAvailableDate_ItShouldReturnAnAvailableDay() {
        delegate.dateIsAvailableMock = true
        let day = sut.createDay(offsetBy: 10, for: Date(), isWithinDisplayedMonth: true)
        XCTAssertTrue(day.isAvailable)
    }
    
    func testCreateDay_WhenCalledWithoutAValidatorDelegate_ItShouldReturnAnAvailableDay() {
        let sut = VerticalCalendarInteractor(selectedDate: Date(), presenter: presenterSpy, delegate: nil, validator: nil)
        let day = sut.createDay(offsetBy: 10, for: Date(), isWithinDisplayedMonth: true)
        XCTAssertTrue(day.isAvailable)
    }
    
    func testCreateDay_WhenCurrentDateIsNotAvailable_ItShouldReturnCurrentDateIsNotSelected() throws {
        delegate.dateIsAvailableMock = false
        
        let currentDate = try XCTUnwrap(dateFormatter.date(from: "16/11/2020"), "'currentDate' variable cannot be nil")
        sut.selectedDate = currentDate
        
        let dayUnavailable = sut.createDay(offsetBy: 0, for: currentDate, isWithinDisplayedMonth: true)
        
        XCTAssertFalse(dayUnavailable.isSelected)
    }
    
    func testCreateDay_WhenCurrentDateIsAvailable_ItShouldReturnCurrentDateIsSelected() throws {
        delegate.dateIsAvailableMock = true
        
        let currentDate = try XCTUnwrap(dateFormatter.date(from: "16/11/2020"), "'currentDate' variable cannot be nil")
        sut.selectedDate = currentDate
        
        let dayAvailable = sut.createDay(offsetBy: 0, for: currentDate, isWithinDisplayedMonth: true)
        
        XCTAssertTrue(dayAvailable.isSelected)
    }
    
    func testCreateMonth_WhenSuggestedDateIsNotAvailable_ItShouldNotBeSelected() throws {
        delegate.availableDate = try XCTUnwrap(dateFormatter.date(from: "02/12/2020"), "'availableDate' variable cannot be nil")
        
        let firstBaseMonthDay = try XCTUnwrap(dateFormatter.date(from: "01/12/2020"), "'currentDate' variable cannot be nil")
        sut.selectedDate = firstBaseMonthDay
        
        let baseMonth = try XCTUnwrap(sut.createMonthDays(for: firstBaseMonthDay), "'VerticalCalendarMonth' should be created successfully")
        
        // 01/12/2020 not selected
        XCTAssertFalse(baseMonth[2].isSelected)
    }
    
    func testCreateMonth_WhenSuggestedDateIsNotAvailable_NextAvailableDateShouldBeSelected() throws {
        delegate.availableDate = try XCTUnwrap(dateFormatter.date(from: "02/12/2020"), "'availableDate' variable cannot be nil")
        
        let firstBaseMonthDay = try XCTUnwrap(dateFormatter.date(from: "01/12/2020"), "'currentDate' variable cannot be nil")
        sut.selectedDate = firstBaseMonthDay
        
        let baseMonth = try XCTUnwrap(sut.createMonthDays(for: firstBaseMonthDay), "'VerticalCalendarMonth' should be created successfully")
        
        // changed selected date
        XCTAssertEqual(sut.selectedDate, delegate.availableDate)
        
        // 02/12/2020 is now selected
        XCTAssertTrue(baseMonth[3].isSelected)
    }
}
