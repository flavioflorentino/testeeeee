import XCTest
@testable import UI

private final class VerticalCalendarViewSpy: VerticalCalendarDisplaying {
    private(set) var callDisplaySectionsCount = 0
    private(set) var sections: [VerticalCalendarSection]?
    
    func displaySections(sections: [VerticalCalendarSection]) {
        callDisplaySectionsCount += 1
        self.sections = sections
    }
}

final class VerticalCalendarPresenterTests: XCTestCase {
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return dateFormatter
    }()
    
    private lazy var baseDate: Date? = {
        return dateFormatter.date(from: "16/11/1985")
    }()
    
    private lazy var viewSpy = VerticalCalendarViewSpy()
    private lazy var sut: VerticalCalendarPresenter = {
        let sut = VerticalCalendarPresenter()
        sut.view = viewSpy
        return sut
    }()
    
    func testPresentDates_ShouldDisplaySections() {
        sut.displayDates(configs: [])
        XCTAssertEqual(viewSpy.callDisplaySectionsCount, 1)
    }
    
    func testPresentDates_ShouldCreateSections() throws {
        let config = try createVerticalCalendarConfigurator()
        
        sut.displayDates(configs: [config])
        
        let sections = try XCTUnwrap(viewSpy.sections)
        let days = try XCTUnwrap(sections[0].days)
        let date = try XCTUnwrap(baseDate)
        
        XCTAssertEqual(sections.count, 1)
        XCTAssertEqual(days.count, 1)
        
        XCTAssertEqual(sections[0].baseDate, date)
        XCTAssertEqual(days[0].number, "16")
    }
    
    private func createVerticalCalendarConfigurator() throws -> VerticalCalendarMonthConfigurator {
        let date = try XCTUnwrap(baseDate, "'baseDate' variable cannot be nil")
        
        return VerticalCalendarMonthConfigurator(
            baseDate: date,
            days: [
                VerticalCalendarDay(
                    date: date,
                    number: "16",
                    isSelected: false,
                    isWithinDisplayedMonth: false,
                    isAvailable: true
                )
            ]
        )
    }
}
