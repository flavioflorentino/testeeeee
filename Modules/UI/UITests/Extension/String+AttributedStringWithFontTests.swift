import XCTest
@testable import UI

final class AttributedStringWithFont: XCTestCase {
    var sut: NSAttributedString!
    var stringTest: String!
    var labelMock: UILabel!
    var normalFont: UIFont!
    var boldFont: UIFont!
    
    override func setUp() {
        super.setUp()
        
        normalFont = UIFont.systemFont(ofSize: 10, weight: .regular)
        boldFont = UIFont.systemFont(ofSize: 10, weight: .bold)
        
        labelMock = UILabel()
        stringTest = "My string with **bold text** !"
        labelMock.text = stringTest
        sut = stringTest.attributedStringWithFont(primary: normalFont, secondary: boldFont)
        labelMock.attributedText = sut
    }
    func testSutIsNotNil() {
        XCTAssertNotNil(sut)
    }
    func testRegularFontStartUntilBold() {
        XCTAssertTrue(sut.attributedSubstring(from: NSRange(location: 0, length: 15)).string == "My string with ")
        let fontBegining = sut.attributes(at: 0, effectiveRange: nil)[NSAttributedString.Key.font] as! UIFont
        XCTAssertTrue(fontBegining == normalFont)
        
        let fontBeforeBold = sut.attributes(at: 14, effectiveRange: nil)[NSAttributedString.Key.font] as! UIFont
        XCTAssertTrue(fontBeforeBold == normalFont)
        
        let fontStartBold = sut.attributes(at: 15, effectiveRange: nil)[NSAttributedString.Key.font] as! UIFont
        XCTAssertTrue(fontStartBold == boldFont)
    }
    func testBoldFontStartUntilRegular() {
        XCTAssertTrue(sut.attributedSubstring(from: NSRange(location: 15, length: 11)).string == "bold text !")
        let fontBoldBegining = sut.attributes(at: 15, effectiveRange: nil)[NSAttributedString.Key.font] as! UIFont
        XCTAssertTrue(fontBoldBegining == boldFont)
        
        let fontBoldEnding = sut.attributes(at: 23, effectiveRange: nil)[NSAttributedString.Key.font] as! UIFont
        XCTAssertTrue(fontBoldEnding == boldFont)
        
        let fontStartRegular = sut.attributes(at: 24, effectiveRange: nil)[NSAttributedString.Key.font] as! UIFont
        XCTAssertTrue(fontStartRegular == normalFont)
        
        let fontEndRegular = sut.attributes(at: 25, effectiveRange: nil)[NSAttributedString.Key.font] as! UIFont
        XCTAssertTrue(fontEndRegular == normalFont)
    }
}
