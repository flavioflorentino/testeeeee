import XCTest
@testable import UI

final class StringExtensionsTests: XCTestCase {
    func testCellPhoneFormatting_WhenLessThan6Characters_ShouldReturnEmptyString() {
        let sut = "12345"
        
        let result = sut.cellphoneFormatting()
        XCTAssertEqual(result, "")
    }
    
    func testCellPhoneFormatting_WhenMoreThan6Characters_ShouldNotReturnEmptyString() {
        let sut = "123456"
        
        let result = sut.cellphoneFormatting()
        XCTAssertNotEqual(result, "")
    }
    
    func testCellPhoneFormatting_WhenNumbersOnlyCellphone_ShouldReturnFormattedCellphone() {
        let sut = "11999999999"
        let result = sut.cellphoneFormatting()
        XCTAssertEqual(result, "(11) 99999-9999")
    }
    
    func testCellPhoneFormatting_WhenNumbersOnlyPhone_ShouldReturnFormattedPhone() {
        let sut = "1199999999"
        let result = sut.cellphoneFormatting()
        XCTAssertEqual(result, "(11) 9999-9999")
    }
    
    func testSubstringRange_WhenExists_ShouldReturnSubstringRange() {
        let sut = "Range"
        let result = sut.substringRange(of: "Ran")
        XCTAssertEqual(result, NSRange(location: 0, length: 3))
    }
    
    func testSubstringRange_WhenExistsButNotSameCase_ShouldReturnNotFoundRange() {
        let sut = "Range"
        let result = sut.substringRange(of: "ran")
        XCTAssertEqual(result, NSRange(location: NSNotFound, length: 0))
    }
    
    func testSubstringRange_WhenDoesNotExist_ShouldReturnNotFoundRange() {
        let sut = "Range"
        let result = sut.substringRange(of: "abc")
        XCTAssertEqual(result, NSRange(location: NSNotFound, length: 0))
    }
    
    func testHasContent_WhenReciveEmptyString_ShouldReturnNil() {
        let emptyString = ""
        XCTAssertNil(emptyString.hasContent())
    }
    
    func testHasContent_WhenReciveNil_ShouldRerturnNil() {
        let reciveValue:String? = nil
        XCTAssertNil(reciveValue?.hasContent())
    }
    
    func testHasContent_WhenReciveString_ShouldReturnString() {
        let reciveString = "PicPay"
        XCTAssertEqual(reciveString, reciveString.hasContent())
    }
}
