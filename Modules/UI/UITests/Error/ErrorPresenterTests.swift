import XCTest
@testable import UI

final class ErrorPresenterTests: XCTestCase {
    private let viewControllerSpy = ErrorDisplayableSpy()
    private let errorInfo = ErrorInfoModel(
        image: UIImage(),
        title: "Some title",
        message: "Some message",
        primaryAction: ErrorAction(
            title: "Some button title",
            completion: { _ in }
        )
    )
    private lazy var sut: ErrorPresenter = {
        let sut = ErrorPresenter()
        sut.viewController = viewControllerSpy
        return sut
    }()
    
    func testDisplayInformation_WhenCalledByViewModel_ShouldPresentTheCorrectInformation() {
        sut.present(info: errorInfo)
        
        XCTAssertEqual(viewControllerSpy.callDisplayInfoCount, 1)
        XCTAssertEqual(viewControllerSpy.errorInfo?.title, errorInfo.title)
        XCTAssertEqual(viewControllerSpy.errorInfo?.message, errorInfo.message)
        XCTAssertEqual(viewControllerSpy.errorInfo?.primaryAction.title, errorInfo.primaryAction.title)
        XCTAssertNil(viewControllerSpy.errorInfo?.secondaryAction)
    }
}

private final class ErrorDisplayableSpy: ErrorDisplayable {
    private(set) var callDisplayInfoCount = 0
    private(set) var errorInfo: ErrorInfoModel?
    
    func display(info: ErrorInfoModel) {
        callDisplayInfoCount += 1
        errorInfo = info
    }
}
