import XCTest
@testable import UI

final class ErrorViewModelTests: XCTestCase {
    private let presenterSpy = ErrorPresenterSpy()
    private let errorInfo = ErrorInfoModel(
        image: UIImage(),
        title: "Some title",
        message: "Some message",
        primaryAction: ErrorAction(
            title: "Some button title",
            completion: { _ in }
        )
    )
    private lazy var sut = ErrorInteractor(errorInfo: errorInfo, presenter: presenterSpy)
    
    func testLoadInformation_WhenInstantiateByFactory_ShouldDisplayTheCorrectInformation() {
        sut.loadInfo()
        
        XCTAssertEqual(presenterSpy.callDisplayInfoCount, 1)
        XCTAssertEqual(presenterSpy.errorInfo?.title, errorInfo.title)
        XCTAssertEqual(presenterSpy.errorInfo?.message, errorInfo.message)
        XCTAssertEqual(presenterSpy.errorInfo?.primaryAction.title, errorInfo.primaryAction.title)
        XCTAssertNil(presenterSpy.errorInfo?.secondaryAction)
    }
}

private final class ErrorPresenterSpy: ErrorPresenting {
    var viewController: ErrorDisplayable?
    
    private(set) var callDisplayInfoCount = 0
    private(set) var errorInfo: ErrorInfoModel?
    
    func present(info: ErrorInfoModel) {
        callDisplayInfoCount += 1
        errorInfo = info
    }
}
