import XCTest
@testable import UI

private final class UserGrouperPresenterSpy: UserGrouperPresenting {
    private(set) var current: Int?
    private(set) var max: Int?
    private(set) var users: [GroupedUser]?
    private(set) var indexPath: IndexPath?
    private(set) var presentUserQuantityCallCount = 0
    private(set) var presentUsersCallCount = 0
    private(set) var presentRemoveUserCallCount = 0
    
    func presentUserQuantity(current: Int, max: Int) {
        self.current = current
        self.max = max
        presentUserQuantityCallCount += 1
    }
    
    func presentUsers(_ users: [GroupedUser]) {
        self.users = users
        presentUsersCallCount += 1
    }
    
    func presentRemoveUser(at indexPath: IndexPath) {
        self.indexPath = indexPath
        presentRemoveUserCallCount += 1
    }
}

private final class UserGrouperDelegateSpy: UserGrouperDelegate {
    private(set) var removeCallCount = 0
    private(set) var removedUser: GroupedUser?
    
    func didRemove(user: GroupedUser) {
        removedUser = user
        removeCallCount += 1
    }
}

final class UserGrouperViewModelTests: XCTestCase {
    // MARK: - Variables
    private let presenter = UserGrouperPresenterSpy()
    private let delegate = UserGrouperDelegateSpy()
    private var maxUserQuantity = 0
    private var initialUsers: [GroupedUser] = []
    private lazy var viewModel = UserGrouperViewModel(
        presenter: presenter,
        maxUserQuantity: maxUserQuantity,
        initialUsers: initialUsers
    )
    
    private func createUsers(quantity: Int, initialId: Int? = 0) -> [GroupedUser] {
        var newUsers: [GroupedUser] = []
        let initialIndex = initialId ?? 0
        let maxCount = initialIndex + quantity
        for index in initialIndex..<maxCount {
            let id = String(index)
            let newUser = GroupedUser(id: id, imageUrl: nil, name: "Test\(index)")
            newUsers.append(newUser)
        }
        return newUsers
    }
    
    // MARK: - loadInitialData
    func testLoadInitialData_ShouldCallPresentUserQuantityAndPresentUsersWithGivenData() throws {
        let max = 5
        let current = 1
        maxUserQuantity = max
        initialUsers = createUsers(quantity: current)
        viewModel.loadInitialData()
        let finalCurrent = try XCTUnwrap(presenter.current)
        let finalMax = try XCTUnwrap(presenter.max)
        XCTAssertEqual(presenter.presentUserQuantityCallCount, 1)
        XCTAssertEqual(presenter.presentUsersCallCount, 1)
        XCTAssertEqual(finalCurrent, current)
        XCTAssertEqual(finalMax, max)
    }
    
    // MARK: - add
    func testAdd_WhenCurrentQuantityOfUserIsLessThanMaxAndUserIsNotDuplicate_ShouldAddNewUserAndCallpresentUserQuantityAndPresentUsers() throws {
        let max = 5
        let current = 1
        let newUser = createUsers(quantity: 1, initialId: 4)[0]
        maxUserQuantity = max
        initialUsers = createUsers(quantity: current)
        let result = viewModel.add(newUser: newUser)
        let finalCurrent = try XCTUnwrap(presenter.current)
        let finalMax = try XCTUnwrap(presenter.max)
        let finalUsers = try XCTUnwrap(presenter.users)
        XCTAssertTrue(result)
        XCTAssertEqual(presenter.presentUserQuantityCallCount, 1)
        XCTAssertEqual(presenter.presentUsersCallCount, 1)
        XCTAssertEqual(finalCurrent, current + 1)
        XCTAssertEqual(finalMax, max)
        XCTAssertEqual(finalCurrent, finalUsers.count)
    }
    
    func testAdd_WhenCurrentQuantityOfUserIsGreaterThanMaxAndUserIsNotDuplicate_ShouldNotAddNewUserAndNotCallpresentUserQuantityAndPresentUsers() {
        let newUser = createUsers(quantity: 1, initialId: 4)[0]
        maxUserQuantity = 1
        initialUsers = createUsers(quantity: 1)
        let result = viewModel.add(newUser: newUser)
        XCTAssertFalse(result)
        XCTAssertEqual(presenter.presentUserQuantityCallCount, 0)
        XCTAssertEqual(presenter.presentUsersCallCount, 0)
    }
    
    func testAdd_WhenCurrentQuantityOfUserIsLessThanMaxAndUserIsDuplicate_ShouldNotAddNewUserAndNotCallpresentUserQuantityAndPresentUsers() {
        let newUser = createUsers(quantity: 1, initialId: 0)[0]
        maxUserQuantity = 2
        initialUsers = createUsers(quantity: 1)
        let result = viewModel.add(newUser: newUser)
        XCTAssertFalse(result)
        XCTAssertEqual(presenter.presentUserQuantityCallCount, 0)
        XCTAssertEqual(presenter.presentUsersCallCount, 0)
    }
    
    // MARK: - removeUserId
    func testRemoveUserId_WhenTryToRemoveAnUserInsideTheList_ShouldRemoveTheUserAndCallDelegateRemoveAndPresentUserQuantityAndPresentRemoveUser() throws {
        let max = 3
        let current = 3
        maxUserQuantity = max
        initialUsers = createUsers(quantity: current)
        viewModel.delegate = delegate
        let result = viewModel.removeUser(id: "1")
        let finalCurrent = try XCTUnwrap(presenter.current)
        let finalMax = try XCTUnwrap(presenter.max)
        XCTAssertTrue(result)
        XCTAssertNotNil(delegate.removedUser)
        XCTAssertEqual(delegate.removeCallCount, 1)
        XCTAssertEqual(presenter.presentUserQuantityCallCount, 1)
        XCTAssertEqual(presenter.presentRemoveUserCallCount, 1)
        XCTAssertEqual(finalCurrent, current - 1)
        XCTAssertEqual(finalMax, max)
    }
    
    func testRemoveUserId_WhenTryToRemoveAnUserThatIsNotInsideTheList_ShouldNotRemoveTheUserAndNotCallDelegateRemoveAndPresentUserQuantityAndPresentRemoveUser() throws {
        maxUserQuantity = 3
        initialUsers = createUsers(quantity: 3)
        viewModel.delegate = delegate
        let result = viewModel.removeUser(id: "5")
        XCTAssertFalse(result)
        XCTAssertEqual(delegate.removeCallCount, 0)
        XCTAssertEqual(presenter.presentUserQuantityCallCount, 0)
        XCTAssertEqual(presenter.presentRemoveUserCallCount, 0)
    }
    
    // MARK: - removeUser
    func testRemoveUser_WhenRowIsLessThanUsersCount_ShouldCallDelegateRemoveAndPresentUserQuantityAndPresentRemoveUser() throws {
        let max = 2
        let current = 2
        let indexPath = IndexPath(row: 1, section: 0)
        maxUserQuantity = max
        initialUsers = createUsers(quantity: current)
        viewModel.delegate = delegate
        viewModel.removeUser(at: indexPath)
        let finalCurrent = try XCTUnwrap(presenter.current)
        let finalMax = try XCTUnwrap(presenter.max)
        let finalIndex = try XCTUnwrap(presenter.indexPath)
        XCTAssertNotNil(delegate.removedUser)
        XCTAssertEqual(delegate.removeCallCount, 1)
        XCTAssertEqual(presenter.presentUserQuantityCallCount, 1)
        XCTAssertEqual(presenter.presentRemoveUserCallCount, 1)
        XCTAssertEqual(finalCurrent, current - 1)
        XCTAssertEqual(finalMax, max)
        XCTAssertEqual(finalIndex, indexPath)
    }
    
    func testRemoveUser_WhenRowIsGreaterThanUsersCount_ShouldNotCallDelegateRemoveAndPresentUserQuantityAndPresentRemoveUser() {
        let indexPath = IndexPath(row: 3, section: 0)
        maxUserQuantity = 3
        initialUsers = createUsers(quantity: 2)
        viewModel.delegate = delegate
        viewModel.removeUser(at: indexPath)
        XCTAssertEqual(delegate.removeCallCount, 0)
        XCTAssertEqual(presenter.presentUserQuantityCallCount, 0)
        XCTAssertEqual(presenter.presentRemoveUserCallCount, 0)
    }
}
