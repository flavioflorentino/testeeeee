import XCTest
@testable import UI

private final class UserGroupedViewSpy: UserGrouperDisplay {
    private(set) var displayUserQuantityCallCount = 0
    private(set) var displayUsersCallCount = 0
    private(set) var removeUserCallCount = 0
    private(set) var quantityText: String?
    private(set) var users: [(String, URL?)]?
    private(set) var placeholderImage: UIImage?
    private(set) var indexPath: IndexPath?
    
    func displayUserQuantity(text: String) {
        quantityText = text
        displayUserQuantityCallCount += 1
    }
    
    func displayUsers(_ users: [(String, URL?)], placeholderImage: UIImage?) {
        self.users = users
        self.placeholderImage = placeholderImage
        displayUsersCallCount += 1
    }
    
    func removeUser(at indexPath: IndexPath) {
        self.indexPath = indexPath
        removeUserCallCount += 1
    }
}

final class UserGrouperPresenterTests: XCTestCase {
    // MARK: - Variables
    private var quantityText = "% e %"
    private var placeholderImage = UIImage()
    private let view = UserGroupedViewSpy()
    private lazy var presenter: UserGrouperPresenter = {
        let presenter = UserGrouperPresenter(quantityText: quantityText, placeholderImage: placeholderImage)
        presenter.view = view
        return presenter
    }()
    
    // MARK: - presentUserQuantity
    func testPresentUserQuantity_ShouldCallDisplayUserQuantityWithCorrectFormat() throws {
        let current = 1
        let max = 5
        let expectedFinal = String(format: quantityText, current, max)
        presenter.presentUserQuantity(current: current, max: max)
        let finalQuantityText = try XCTUnwrap(view.quantityText)
        XCTAssertEqual(view.displayUserQuantityCallCount, 1)
        XCTAssertEqual(finalQuantityText, expectedFinal)
    }
    
    // MARK: - presentUsers
    func testPresentUsers_ShouldCallDisplayUsers() throws {
        let users = [GroupedUser(id: "0", imageUrl: nil, name: "Test")]
        presenter.presentUsers(users)
        let finalUsers = try XCTUnwrap(view.users)
        XCTAssertEqual(view.displayUsersCallCount, 1)
        XCTAssertEqual(finalUsers.count, users.count)
        XCTAssertNotNil(view.placeholderImage)
    }
    
    // MARK: - presentRemoveUser
    func testPresentRemoveUser_ShouldCallRemoveUser() throws {
        let indexPath = IndexPath(row: 0, section: 0)
        presenter.presentRemoveUser(at: indexPath)
        let finalIndexPath = try XCTUnwrap(view.indexPath)
        XCTAssertEqual(view.removeUserCallCount, 1)
        XCTAssertEqual(finalIndexPath, indexPath)
    }
}
