import XCTest
@testable import UI

private final class UserCounterViewSpy: UserCounterDisplay {
    private(set) var displayUsersCallsCount = 0
    private(set) var users: [UserCounterModel]?
    private(set) var placeholder: UIImage?
    private(set) var displayCountCallsCount = 0
    private(set) var text: String?
    private(set) var hideCountCallsCount = 0
    
    func displayUsers(_ users: [UserCounterModel], placeholder: UIImage?) {
        displayUsersCallsCount += 1
        self.users = users
        self.placeholder = placeholder
    }
    
    func displayCount(text: String) {
        displayCountCallsCount += 1
        self.text = text
    }
    
    func hideCount() {
        hideCountCallsCount += 1
    }
}

final class UserCounterPresenterTests: XCTestCase {
    // MARK: - Variables
    private var placeHolder = UIImage()
    private let view = UserCounterViewSpy()
    private lazy var presenter: UserCounterPresenting = {
        let presenter = UserCounterPresenter(placeholder: placeHolder)
        presenter.view = view
        return presenter
    }()
    
    // MARK: - presentUsers
    func testPresentUsers_WhenUserCountGreaterThanOneAndLessThanFive_ShouldCallDisplayCountAndDisplayUsersWithAllUsers() throws {
        let users = [
            UserCounterModel(imgUrl: nil),
            UserCounterModel(imgUrl: nil)
        ]
        presenter.presentUsers(users)
        let finalUsers = try XCTUnwrap(view.users)
        let finalText = try XCTUnwrap(view.text)
        XCTAssertEqual(finalUsers.count, users.count)
        XCTAssertEqual(finalText, "\(users.count)")
        XCTAssertEqual(view.displayCountCallsCount, 1)
        XCTAssertEqual(view.displayUsersCallsCount, 1)
    }
    
    func testPresentUsers_WhenUserCountGreaterThanFive_ShouldCallDisplayCountAndDisplayUsersWithFiveUsers() throws {
        let users = [
            UserCounterModel(imgUrl: nil),
            UserCounterModel(imgUrl: nil),
            UserCounterModel(imgUrl: nil),
            UserCounterModel(imgUrl: nil),
            UserCounterModel(imgUrl: nil),
            UserCounterModel(imgUrl: nil)
        ]
        presenter.presentUsers(users)
        let finalUsers = try XCTUnwrap(view.users)
        let finalText = try XCTUnwrap(view.text)
        XCTAssertEqual(finalUsers.count, 5)
        XCTAssertEqual(finalText, "\(users.count)")
        XCTAssertEqual(view.displayCountCallsCount, 1)
        XCTAssertEqual(view.displayUsersCallsCount, 1)
    }
    
    func testPresentUsers_WhenUserCountLessThanOrEqualToOne_ShouldCallHideCountAndDisplayUsersWithAllUsers() throws {
        let users = [
            UserCounterModel(imgUrl: nil)
        ]
        presenter.presentUsers(users)
        let finalUsers = try XCTUnwrap(view.users)
        XCTAssertEqual(finalUsers.count, users.count)
        XCTAssertEqual(view.hideCountCallsCount, 1)
        XCTAssertEqual(view.displayUsersCallsCount, 1)
    }
}
