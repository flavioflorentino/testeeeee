import XCTest
@testable import UI

private final class UserCounterPresenterSpy: UserCounterPresenting {
    private(set) var presentUsersCallsCount = 0
    private(set) var users: [UserCounterModel]?
    
    func presentUsers(_ users: [UserCounterModel]) {
        presentUsersCallsCount += 1
        self.users = users
    }
}

final class UserCounterViewModelTests: XCTestCase {
    // MARK: - Variables
    private let presenter = UserCounterPresenterSpy()
    private var users: [UserCounterModel] = []
    private lazy var viewModel = UserCounterViewModel(presenter: presenter, users: users)
    
    // MARK: - loadData
    func testLoadData_ShouldCallPresentUsers() throws {
        users = [UserCounterModel(imgUrl: nil)]
        viewModel.loadData()
        let finalUsers = try XCTUnwrap(presenter.users)
        XCTAssertEqual(finalUsers.count, users.count)
        XCTAssertEqual(presenter.presentUsersCallsCount, 1)
    }
}
