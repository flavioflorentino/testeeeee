import CashIn
import SnapKit
import UI
import UIKit

class HomeViewController: UIViewController, ViewConfiguration {
    private lazy var stackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.contentMode = .center
        stack.spacing = Spacing.base01
        return stack
    }()

    private lazy var methodsButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle("Cash In Methods", for: .normal)
        return button
    }()

    private lazy var errorButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle("Cash In Error", for: .normal)
        return button
    }()
    
    private lazy var onboardingButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle("Cash In Onboarding", for: .normal)
        return button
    }()

    private lazy var tutorialButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle("Tutorial", for: .normal)
        return button
    }()
    
    private lazy var boletoButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle("Boleto", for: .normal)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        buildLayout()
    }

    func buildViewHierarchy() {
        view.addSubview(stackView)
        stackView.addArrangedSubview(methodsButton)
        stackView.addArrangedSubview(errorButton)
        stackView.addArrangedSubview(onboardingButton)
        stackView.addArrangedSubview(tutorialButton)
        stackView.addArrangedSubview(boletoButton)
    }

    func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.centerY.equalToSuperview()
        }
    }

    func configureViews() {
        let methodsGesture = UITapGestureRecognizer(target: self, action: #selector(showMethods))
        methodsButton.addGestureRecognizer(methodsGesture)

        let errorGesture = UITapGestureRecognizer(target: self, action: #selector(showError))
        errorButton.addGestureRecognizer(errorGesture)
        
        let onboardingGesture = UITapGestureRecognizer(target: self, action: #selector(showOnboarding))
        onboardingButton.addGestureRecognizer(onboardingGesture)

        let tutorialGesture = UITapGestureRecognizer(target: self, action: #selector(showTutorial))
        tutorialButton.addGestureRecognizer(tutorialGesture)
        
        let boletoGesture = UITapGestureRecognizer(target: self, action: #selector(showBoleto))
        boletoButton.addGestureRecognizer(boletoGesture)
    }
}

private extension HomeViewController {
    @objc
    func showMethods() {
        let methodsVC = CashInMethodsFactory.make()
        let navigation = UINavigationController(rootViewController: methodsVC)
        present(navigation, animated: true, completion: nil)
    }

    @objc
    func showError() {
        let methodsVC = CashInErrorFactory.make(for: nil)
        let navigation = UINavigationController(rootViewController: methodsVC)
        present(navigation, animated: true, completion: nil)
    }
    
    @objc
    func showOnboarding() {
        let methodsVC = OnboardingFactory.make(for: .wireTransfer)
        let navigation = UINavigationController(rootViewController: methodsVC)
        present(navigation, animated: true, completion: nil)
    }

    @objc
    func showTutorial() {
        let tutorialVC = TutorialFactory.make(for: .boleto, delegate: nil)
        let navigation = UINavigationController(rootViewController: tutorialVC)
        present(navigation, animated: true, completion: nil)
    }
    
    @objc
    func showBoleto() {
        let boletoVC = BoletoOptionsFactory.make()
        let navigation = UINavigationController(rootViewController: boletoVC)
        present(navigation, animated: true, completion: nil)
    }
}
