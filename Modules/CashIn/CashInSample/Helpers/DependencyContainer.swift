import AnalyticsModule
import Core
import FeatureFlag
import Foundation

typealias CashInDependencies = HasMainQueue & HasFeatureManager & HasAnalytics

final class DependencyContainer: CashInDependencies {
    lazy var mainQueue = DispatchQueue.main
    lazy var featureManager: FeatureManagerContract = FeatureManager.shared
    lazy var analytics: AnalyticsProtocol = Analytics.shared
}
