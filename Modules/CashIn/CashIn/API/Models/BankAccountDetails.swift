import Foundation

struct BankingInfo: Decodable {
    let bankId: String
    let accountAgency: String
    let accountNumber: String
    let consumerUsername: String
    let consumerName: String
    let consumerCpf: String
    let consumerAvatar: String?
}

struct BankAccountDetails {
    let userInfo: UserInfo
    let bankInfo: BankAccountInfo
}

struct BankAccountInfo {
    let bankNumber: String
    let agencyNumber: String
    let accountNumber: String
    let document: String
}

struct UserInfo {
    let avatar: URL?
    let avatarPlaceholder: UIImage
    let username: String
    let fullname: String
}

extension BankAccountDetails: Equatable {}

extension BankAccountInfo: Equatable {}

extension UserInfo: Equatable {}
