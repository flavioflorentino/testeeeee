import Foundation

enum MethodId: String, Decodable {
    case boleto = "1"
    case deposit = "2"
    case wireTransfer = "3"
    case original = "4"
    case debit = "5"
    case loan = "6"
    case lending = "7"
    case caixa = "8"
    case account = "9"
    case deeplink = "10"
}

enum Availability: String, Decodable {
    case new = "N"
    case unavailable = "I"
}

struct CashInMethodCard: Decodable {
    let methodId: MethodId
    let status: Availability?
    let name: String
    let description: String
    let iconUrlString: String?
    let deeplink: String?
    let eventName: String?
    
    var iconUrl: URL? {
        guard let url = iconUrlString else { return nil }
        return URL(string: url)
    }

    private enum CodingKeys: String, CodingKey {
        case status, name, description, deeplink, eventName
        case methodId = "id"
        case iconUrlString = "rechargeTypeIcon"
    }
}

struct CashInCategory: Decodable {
    let id: Int
    let name: String
    let methods: [CashInMethodCard]

    private enum CodingKeys: String, CodingKey {
        case id = "categoryId"
        case name = "categoryName"
        case methods = "categoryRechargeMethods"
    }
}

// Conforming to UI TableViewDataSource Section
extension CashInCategory: Hashable, Equatable {
    static func == (lhs: CashInCategory, rhs: CashInCategory) -> Bool {
        lhs.id == rhs.id
    }

    public func hash(into hasher: inout Hasher) {
        hasher.combine(name)
    }
}
