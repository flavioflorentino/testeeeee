import Core

enum CashInEndpoints {
    case cashInMethods
    case methodDetail(id: String)
}

extension CashInEndpoints: ApiEndpointExposable {
    var path: String {
        switch self {
        case .cashInMethods:
            return "api/getRechargeMethods"
        case .methodDetail(let id):
            return "api/getDetailsRechargeMethod/" + id
        }
    }
}
