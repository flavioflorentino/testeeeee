public protocol  CashInMethodsContract {
    func startAddValue(on viewController: UIViewController, methodId: Int)
    func startGovernmentRecharge(on viewController: UIViewController)
    func goHome()
}
