import UI
import UIKit

extension String {
    var byRemovingPunctuation: String {
        self.filter { !"_-.,:;?!".contains($0) }
    }
}

extension NSMutableAttributedString {
    func append(_ value: String, font: UIFont) -> NSMutableAttributedString {
        let attributes: [NSAttributedString.Key: Any] = [
            .font: font
        ]
        
        self.append(NSAttributedString(string: value, attributes: attributes))
        return self
    }
}
