import AssetsKit
import UI

extension StatefulErrorViewModel {
    static var cashInError: StatefulErrorViewModel {
        StatefulErrorViewModel(
            image: Resources.Illustrations.iluNotFoundCircledBackground.image,
            content: (title: Strings.Global.Default.Error.title,
                      description: Strings.Global.Default.RequestError.message),
            button: (image: nil, title: Strings.Global.tryAgain)
        )
    }
}
