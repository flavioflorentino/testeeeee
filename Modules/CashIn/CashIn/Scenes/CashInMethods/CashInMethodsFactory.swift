import UIKit

public enum CashInMethodsFactory {
    public static func make(fromDeeplink: Bool = false) -> UIViewController {
        let container = DependencyContainer()
        let service = CashInMethodsService(dependencies: container)
        let coordinator = CashInMethodsCoordinator(dependencies: container)
        let presenter = CashInMethodsPresenter(coordinator: coordinator)
        let interactor = CashInMethodsInteractor(dependencies: container,
                                                 service: service,
                                                 presenter: presenter,
                                                 originIsDeeplink: fromDeeplink)
        let viewController = CashInMethodsViewController(interactor: interactor, itemProvider: presenter)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
