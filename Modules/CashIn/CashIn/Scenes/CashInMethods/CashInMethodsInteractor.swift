import AnalyticsModule
import FeatureFlag
import Foundation

protocol CashInMethodsInteracting: AnyObject {
    func loadItems()
    func action(for indexPath: IndexPath)
    func getHelp()
    func moreInfo()
    func dismiss()
    func logScreenViewedEvent()
}

final class CashInMethodsInteractor {
    typealias Dependencies = HasFeatureManager & HasAnalytics
    private let dependencies: Dependencies
    
    private let service: CashInMethodsServicing
    private let presenter: CashInMethodsPresenting
    private var categories: [CashInCategory] = []
    private let originIsDeeplink: Bool
    private var didSendLastMethodEvent: Bool = false

    init(dependencies: Dependencies,
         service: CashInMethodsServicing,
         presenter: CashInMethodsPresenting,
         originIsDeeplink: Bool) {
        self.dependencies = dependencies
        self.service = service
        self.presenter = presenter
        self.originIsDeeplink = originIsDeeplink
    }
}

// MARK: - CashInMethodsInteracting
extension CashInMethodsInteractor: CashInMethodsInteracting {
    func logScreenViewedEvent() {
        dependencies.analytics.log(
            CashInEvent.screenViewed(name: .addMoney,
                                     context: .cashIn,
                                     action: originIsDeeplink ? .deeplink : .tap)
        )
    }
    
    func loadItems() {
        presenter.displayLoading()
        service.getCashInMethods { [weak self] result in
            switch result {
            case .success(let categories):
                guard categories.isNotEmpty else {
                    self?.presenter.displayError()
                    return
                }
                self?.categories = categories
                self?.presenter.display(categories)
            case .failure:
                self?.presenter.displayError()
            }
        }
    }
    
    func action(for indexPath: IndexPath) {
        guard let method = getMethod(for: indexPath) else { return presenter.displayError() }
        guard method.status != .unavailable else { return handleMethodUnderConstruction() }
        
        handleAction(for: method)
    }
    
    func getHelp() {
        helpDeeplink()
    }
    
    func moreInfo() {
        helpDeeplink()
    }
    
    func dismiss() {
        presenter.didNextStep(action: .dismiss)
    }
}

private extension CashInMethodsInteractor {
    func getMethod(for indexPath: IndexPath) -> CashInMethodCard? {
        guard indexPath.section < categories.count,
              indexPath.row < categories[indexPath.section].methods.count else { return nil }
        
        return categories[indexPath.section].methods[indexPath.row]
    }
    
    func helpDeeplink() {
        logButtonTapEvent(for: .learnMore)
        let urlString = dependencies.featureManager.text(.cashInMethodsMoreInfoUrl)
        guard let url = URL(string: urlString) else { return }
        presenter.didNextStep(action: .deeplink(url: url))
    }
    
    func handleAction(for method: CashInMethodCard) {
        switch method.methodId {
        case .boleto, .debit, .deposit:
            if case .boleto = method.methodId { logButtonTapEvent(for: .boleto) }
            guard let id = Int(method.methodId.rawValue) else { return }
            presenter.didNextStep(action: .legacyAddValue(methodId: id))
        
        case .loan:
            logButtonTapEvent(for: .loan)
            presenter.didNextStep(action: .loan)

        case .lending:
            logButtonTapEvent(for: .p2pLending)
            presenter.didNextStep(action: .lending)
            
        case .caixa:
            logButtonTapEvent(for: .virtualDebitCard)
            presenter.didNextStep(action: .legacyCaixa)
            
        case .account:
            logButtonTapEvent(for: .wireTransfer) // Account is the new way of making a wire transfer
            presenter.didNextStep(action: .account)
            
        case .deeplink:
            logButtonTapEvent(for: .pix)
            guard let deeplink = method.deeplink, let url = URL(string: deeplink) else { return }
            presenter.didNextStep(action: .deeplink(url: url))
            
        case .wireTransfer, .original: // Discontinued methods
            presenter.displayError()
        }
    }
    
    func handleMethodUnderConstruction() {
        dependencies.analytics.log(CashInEvent.screenViewed(name: .underConstruction, context: .cashIn, action: nil))
        presenter.didNextStepUnderConstruction()
    }
    
    func logButtonTapEvent(for name: CashInEvent.Name) {
        dependencies.analytics.log(CashInEvent.buttonClicked(buttonName: name, screenName: .addMoney, context: .cashIn))
    }
}
