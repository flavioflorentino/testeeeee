import AssetsKit
import UI
import UIKit
import SnapKit

protocol CashInMethodsDisplaying: AnyObject {
    func setTitle(as titleString: String)
    func display(_ sections: [CashInCategory])
    func display(headerImage: UIImage)
    func display(footerText: String)
    func beginLoading()
    func displayError(with viewModel: StatefulErrorViewModel)
}

protocol CashInMethodItemProvider: AnyObject {
    func header(for section: Int) -> UIView?
    func cell(for method: CashInMethodCard, with indexPath: IndexPath, and tableView: UITableView) -> UITableViewCell?
}

final class CashInMethodsViewController: ViewController<CashInMethodsInteracting, UIView> {
    private typealias TableDataSource = TableViewDataSource<CashInCategory, CashInMethodCard>
    private typealias ItemProvider = TableDataSource.ItemProvider

    private var methodItemProvider: CashInMethodItemProvider

    private lazy var tableViewDataSource: TableDataSource = {
        let dataSource = TableDataSource(view: tableView)
        dataSource.itemProvider = itemProvider
        return dataSource
    }()

    private lazy var itemProvider: ItemProvider = { tableView, indexPath, item -> UITableViewCell? in
        self.methodItemProvider.cell(for: item, with: indexPath, and: tableView)
    }

    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)

        tableView.backgroundColor = .clear
        tableView.rowHeight = UITableView.automaticDimension
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.sectionFooterHeight = CGFloat.leastNormalMagnitude
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.delegate = self

        return tableView
    }()

    private lazy var tableViewHeader: CashInMethodsTableViewHeader = {
        let header = CashInMethodsTableViewHeader()
        header.buildLayout()
        return header
    }()

    private lazy var tableViewFooter: CashInMethodsTableViewFooter = {
        let header = CashInMethodsTableViewFooter()
        header.buildLayout()
        return header
    }()
    
    private lazy var closeButton = UIBarButtonItem(title: Strings.Global.close,
                                                   style: .plain,
                                                   target: self,
                                                   action: #selector(didTapNavigationClose))
                                                 
    private lazy var helpButton = UIBarButtonItem(image: Resources.Icons.icoQuestion.image,
                                                  style: .plain,
                                                  target: self,
                                                  action: #selector(didTapNavigationAction))

    required init(interactor: CashInMethodsInteracting, itemProvider: CashInMethodItemProvider) {
        self.methodItemProvider = itemProvider
        super.init(interactor: interactor)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        buildLayout()
        interactor.logScreenViewedEvent()
        interactor.loadItems()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        if tableView.tableHeaderView == nil {
            displayTableViewHeader()
        }

        if tableView.tableFooterView == nil {
            displayTableViewFooter()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.navigationStyle(ClearNavigationStyle(prefersLargeTitle: false))
        navigationItem.leftBarButtonItem = modalPresentationStyle != .pageSheet ? closeButton : nil
        navigationItem.rightBarButtonItem = helpButton
    }

    override func buildViewHierarchy() {
        view.addSubview(tableView)
    }

    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(view.compatibleSafeArea.bottom)
        }
    }

    override func configureViews() {
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        tableView.register(CashInMethodCell.self, forCellReuseIdentifier: CashInMethodCell.identifier)
        tableView.dataSource = tableViewDataSource
    }
}

// MARK: - Private Methods
private extension CashInMethodsViewController {
    func displayTableViewFooter() {
        updateViewHeight(for: tableViewFooter)
        tableView.tableFooterView = tableViewFooter
        tableView.layoutIfNeeded()
    }

    func displayTableViewHeader() {
        updateViewHeight(for: tableViewHeader)
        tableView.tableHeaderView = tableViewHeader
        tableView.layoutIfNeeded()
    }

    func updateViewHeight(for view: UIView) {
        let expectedSize = view.systemLayoutSizeFitting(
            CGSize(width: tableView.frame.width, height: .leastNonzeroMagnitude),
            withHorizontalFittingPriority: .required,
            verticalFittingPriority: .defaultLow)
        view.frame.size.height = expectedSize.height
    }
    
    @objc
    func didTapNavigationAction() {
        interactor.getHelp()
    }
    
    @objc
    func didTapNavigationClose() {
        interactor.dismiss()
    }
}

// MARK: - CashInMethodsDisplaying
extension CashInMethodsViewController: CashInMethodsDisplaying {
    public func setTitle(as titleString: String) {
        title = titleString
    }
    
    public func display(_ sections: [CashInCategory]) {
        endState()
        tableViewDataSource.sections.forEach {
            tableViewDataSource.remove(section: $0)
        }

        for section in sections where section.methods.isNotEmpty {
            tableViewDataSource.add(items: section.methods, to: section)
        }
    }

    public func display(headerImage: UIImage) {
        tableViewHeader.display(image: headerImage)
        displayTableViewHeader()
    }
    
    public func display(footerText: String) {
        tableViewFooter.setUp(text: footerText, delegate: self)
        displayTableViewFooter()
    }
    
    func beginLoading() {
        beginState()
    }
    
    func displayError(with viewModel: StatefulErrorViewModel) {
        endState(model: viewModel)
    }
}

// MARK: - CashInMethodsTableViewDelegate
extension CashInMethodsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        interactor.action(for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        methodItemProvider.header(for: section)
    }
}

extension CashInMethodsViewController: CashInMethodsTableViewFooterDelegate {
    func didTapLink() {
        interactor.moreInfo()
    }
}

extension CashInMethodsViewController: StatefulTransitionViewing {
    func didTryAgain() {
        interactor.loadItems()
    }
    
    func statefulViewForLoading() -> StatefulViewing {
        CashInMethodsSkeletonView()
    }
    
    func statefulViewForError() -> StatefulViewing {
        let view = EmptyStateView()
        view.buildLayout()
        view.delegate = self
        return view
    }
}
