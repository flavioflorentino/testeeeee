import Loan
import LoanOffer
import P2PLending
import UI
import UIKit

enum CashInMethodsAction: Equatable {
    case account
    case loan
    case lending
    case deeplink(url: URL)
    case dismiss
    case legacyAddValue(methodId: Int)
    case underConstruction(content: ApolloFeedbackViewContent)
    case legacyCaixa
}

protocol CashInMethodsCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: CashInMethodsAction)
}

final class CashInMethodsCoordinator {
    typealias Dependencies = HasLegacy & HasDeeplink
    private let dependencies: Dependencies
    weak var viewController: UIViewController?
    private var coordinating: Coordinating?
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - CashInMethodsCoordinating
extension CashInMethodsCoordinator: CashInMethodsCoordinating {
    func perform(action: CashInMethodsAction) {
        switch action {
        case .account:
            startAccountFlow()
            
        case let .deeplink(url):
            dependencies.deeplink?.open(url: url)

        case .dismiss:
            viewController?.dismiss(animated: true)
            
        case .loan:
            startLoanFlow()
            
        case .lending:
            startLendingFlow()
            
        case let .legacyAddValue(methodId):
            startAddValueLegacyFlow(methodId: methodId)

        case let .underConstruction(content):
            startUnderConstructionFlow(with: content)
            
        case .legacyCaixa:
            startCaixaLegacyFlow()
        }
    }
}

private extension CashInMethodsCoordinator {
    func startAddValueLegacyFlow(methodId: Int) {
        guard let controller = viewController else { return }
        dependencies.legacy.methods?.startAddValue(on: controller, methodId: methodId)
    }
    
    func startLoanFlow() {
        guard let navigation = viewController?.navigationController else { return }
        let coordinator = LoanOfferCoordinator(with: navigation, from: .offer)
        coordinating = coordinator
        coordinator.start(flow: .hire) {
            self.coordinating = nil
        }
    }
    
    func startLendingFlow() {
        let controller = LendingGreetingsFactory.make()
        viewController?.navigationController?.show(controller, sender: nil)
    }
    
    func startCaixaLegacyFlow() {
        guard let controller = viewController else { return }
        dependencies.legacy.methods?.startGovernmentRecharge(on: controller)
    }
    
    func startAccountFlow() {
        let controller = BankAccountDetailsFactory.make()
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
    
    func startUnderConstructionFlow(with content: ApolloFeedbackViewContent) {
        let controller = ApolloFeedbackViewController(content: content)
        controller.didTapPrimaryButton = { self.viewController?.navigationController?.popViewController(animated: true) }
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
}
