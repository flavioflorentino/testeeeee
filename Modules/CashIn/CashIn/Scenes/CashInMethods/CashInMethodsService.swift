import Core
import Foundation

protocol CashInMethodsServicing {
    func getCashInMethods(completion: @escaping (Result<[CashInCategory], ApiError>) -> Void)
}

final class CashInMethodsService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - CashInMethodsServicing
extension CashInMethodsService: CashInMethodsServicing {
    func getCashInMethods(completion: @escaping (Result<[CashInCategory], ApiError>) -> Void) {
        let api = Api<[CashInCategory]>(endpoint: CashInEndpoints.cashInMethods)

        api.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
