import AssetsKit
import UI
import Core
import Foundation
import UIKit

protocol CashInMethodsPresenting: AnyObject {
    var viewController: CashInMethodsDisplaying? { get set }
    func display(_ sections: [CashInCategory])
    func displayLoading()
    func displayError()
    func didNextStepUnderConstruction()
    func didNextStep(action: CashInMethodsAction)
}

final class CashInMethodsPresenter {
    private let coordinator: CashInMethodsCoordinating
    private var sectionHeaderTitles: [String] = []
    weak var viewController: CashInMethodsDisplaying? {
        didSet {
            viewController?.setTitle(as: Strings.CashInMethods.screenTitle)
        }
    }

    init(coordinator: CashInMethodsCoordinating) {
        self.coordinator = coordinator
    }
    
    private var underConstructionContent: ApolloFeedbackViewContent {
        ApolloFeedbackViewContent(image: Resources.Illustrations.iluConstruction.image,
                                  title: Strings.CashInMethods.UnderConstruction.title,
                                  description: NSAttributedString(string: Strings.CashInMethods.UnderConstruction.message),
                                  primaryButtonTitle: Strings.Global.action)
    }
}

// MARK: - CashInMethodsPresenting
extension CashInMethodsPresenter: CashInMethodsPresenting {
    func display(_ sections: [CashInCategory]) {
        sectionHeaderTitles = sections.map { $0.name }
        viewController?.display(sections)
        viewController?.display(headerImage: Assets.iluCashInMethodHeader.image)
        viewController?.display(footerText: Strings.CashInMethods.TableFooter.text)
    }
    
    func displayLoading() {
        viewController?.beginLoading()
    }

    func displayError() {
        viewController?.displayError(with: StatefulErrorViewModel.cashInError)
    }

    func didNextStep(action: CashInMethodsAction) {
        coordinator.perform(action: action)
    }
    
    func didNextStepUnderConstruction() {
        coordinator.perform(action: .underConstruction(content: underConstructionContent))
    }
}

extension CashInMethodsPresenter: CashInMethodItemProvider {
    func header(for section: Int) -> UIView? {
        guard section < sectionHeaderTitles.count else { return nil }

        let sectionHeaderView = CashInMethodsTableSectionHeader()
        sectionHeaderView.buildLayout()
        sectionHeaderView.display(title: sectionHeaderTitles[section])
        return sectionHeaderView
    }

    func cell(for method: CashInMethodCard, with indexPath: IndexPath, and tableView: UITableView) -> UITableViewCell? {
        let cell = tableView.dequeueReusableCell(withIdentifier: CashInMethodCell.identifier, for: indexPath) as? CashInMethodCell
        cell?.setUp(with: method)
        return cell
    }
}
