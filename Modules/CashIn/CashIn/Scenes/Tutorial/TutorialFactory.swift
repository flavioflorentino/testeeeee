import UIKit

public enum TutorialFactory {
    public static func make(for context: TutorialContext, delegate: TutorialCoordinatorDelegate?) -> UIViewController {
        let container = DependencyContainer()
        let coordinator: TutorialCoordinating = TutorialCoordinator(dependencies: container)
        let presenter: TutorialPresenting = TutorialPresenter(coordinator: coordinator)
        let interactor = TutorialInteractor(presenter: presenter, dependencies: container, tutorialContext: context)
        let viewController = TutorialViewController(interactor: interactor)

        coordinator.delegate = delegate
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
