import Foundation

protocol TutorialPresenting: AnyObject {
    var viewController: TutorialDisplaying? { get set }
    
    func display(_ context: TutorialContext)
    func didNextStep(action: TutorialAction)
}

final class TutorialPresenter {
    private let coordinator: TutorialCoordinating
    weak var viewController: TutorialDisplaying?

    init(coordinator: TutorialCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - TutorialPresenting
extension TutorialPresenter: TutorialPresenting {
    func display(_ context: TutorialContext) {
        let descriptor = TutorialDescriptorFactory.make(for: context)
        viewController?.setUp(with: descriptor)
    }
    
    func didNextStep(action: TutorialAction) {
        coordinator.perform(action: action)
    }
}
