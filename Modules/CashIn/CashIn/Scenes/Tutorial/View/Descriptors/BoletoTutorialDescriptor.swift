import UI
import UIKit

struct BoletoTutorialDescriptor: TutorialDescriptor {
    private typealias Localizable = Strings.CashInMethods.Boleto.Tutorial
    
    var titleText: String? { Localizable.title }
    
    var descriptionText: NSAttributedString {
        let text = Localizable.info
        return text.attributedStringWith(normalFont: Typography.bodyPrimary().font(),
                                         highlightFont: Typography.bodyPrimary(.highlight).font(),
                                         normalColor: Colors.grayscale700.color,
                                         highlightColor: Colors.grayscale700.color,
                                         underline: false)
    }
    
    var buttonTitle: String? { Strings.Global.continue }
    
    var linkTitle: String? { nil }
}
