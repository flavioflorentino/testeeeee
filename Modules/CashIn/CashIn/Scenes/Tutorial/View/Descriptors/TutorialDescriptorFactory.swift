enum TutorialDescriptorFactory {
    static func make(for context: TutorialContext) -> TutorialDescriptor {
        switch context {
        case .boleto:
            return BoletoTutorialDescriptor()
        case .account:
            return AccountTutorialDescriptor()
        }
    }
}
