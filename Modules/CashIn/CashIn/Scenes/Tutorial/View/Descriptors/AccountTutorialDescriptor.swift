import UI
import UIKit

struct AccountTutorialDescriptor: TutorialDescriptor {
    private typealias Localizable = Strings.CashInMethods.BankAccount.Tutorial
    
    var titleText: String? { Localizable.title }
    
    var descriptionText: NSAttributedString {
        let text = Localizable.info
        return text.attributedStringWith(normalFont: Typography.bodyPrimary().font(),
                                         highlightFont: Typography.bodyPrimary(.highlight).font(),
                                         normalColor: Colors.grayscale700.color,
                                         highlightColor: Colors.grayscale700.color,
                                         underline: false)
    }
    
    var buttonTitle: String? { nil }
    
    var linkTitle: String? { Localizable.linkTitle }
}
