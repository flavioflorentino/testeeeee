import Foundation
import SnapKit
import UI
import UIKit

protocol TutorialDescriptor {
    var titleText: String? { get }
    var descriptionText: NSAttributedString { get }
    var buttonTitle: String? { get }
    var linkTitle: String? { get }
}

protocol TutorialViewDelegate: AnyObject {
    func didTapLink()
    func didTapButton()
}

class TutorialView: UIView, ViewConfiguration {
    weak var delegate: TutorialViewDelegate?

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))

        return label
    }()

    private lazy var descriptionTextLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
        return label
    }()

    private lazy var actionButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
        button.isHidden = true

        return button
    }()

    private lazy var linkButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(didTapLink), for: .touchUpInside)
        button.isHidden = true
        
        return button
    }()
    
    private lazy var rootStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .equalSpacing
        stack.spacing = Spacing.base06
        stack.compatibleLayoutMargins = .rootView
        stack.isLayoutMarginsRelativeArrangement = true
        
        return stack
    }()
    
    private lazy var topStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = Spacing.base05

        return stack
    }()
    
    private lazy var bottomStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = Spacing.base05
        
        return stack
    }()
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        topStackView.addArrangedSubview(titleLabel)
        topStackView.addArrangedSubview(descriptionTextLabel)
        bottomStackView.addArrangedSubview(actionButton)
        bottomStackView.addArrangedSubview(linkButton)
        rootStackView.addArrangedSubview(topStackView)
        rootStackView.addArrangedSubview(bottomStackView)
        
        addSubview(rootStackView)
    }
    
    func setupConstraints() {
        rootStackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    func setUp(with descriptor: TutorialDescriptor) {
        titleLabel.text = descriptor.titleText
        descriptionTextLabel.attributedText = descriptor.descriptionText
        
        actionButton.setTitle(descriptor.buttonTitle, for: .normal)
        actionButton.isHidden = descriptor.buttonTitle == nil
    
        linkButton.setTitle(descriptor.linkTitle, for: .normal)
        linkButton.isHidden = descriptor.linkTitle == nil
        linkButton.buttonStyle(LinkButtonStyle())
    }

    @objc
    private func didTapButton() {
        delegate?.didTapButton()
    }

    @objc
    private func didTapLink() {
        delegate?.didTapLink()
    }
}
