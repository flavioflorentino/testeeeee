import UIKit

enum TutorialAction {
    case didNextStep(context: TutorialContext)
    case deeplink(url: URL)
}

extension TutorialAction: Equatable {}

public protocol TutorialCoordinatorDelegate: AnyObject {
    func proceed(from context: TutorialContext)
}

protocol TutorialCoordinating: AnyObject {
    var delegate: TutorialCoordinatorDelegate? { get set }
    var viewController: UIViewController? { get set }
    func perform(action: TutorialAction)
}

final class TutorialCoordinator {
    typealias Dependencies = HasDeeplink
    private let dependencies: Dependencies
    
    weak var delegate: TutorialCoordinatorDelegate?
    weak var viewController: UIViewController?
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - TutorialCoordinating
extension TutorialCoordinator: TutorialCoordinating {
    func perform(action: TutorialAction) {
        switch action {
        case let .deeplink(url):
            dependencies.deeplink?.open(url: url)
            
        case let .didNextStep(context):
            delegate?.proceed(from: context)
        }
    }
}
