import AnalyticsModule
import FeatureFlag
import Foundation

public enum TutorialContext {
    case boleto
    case account
}

protocol TutorialInteracting: AnyObject {
    func sendTutorialOpenedEvent()
    func loadContext()
    func buttonAction()
    func linkAction()
}

final class TutorialInteractor {
    typealias Dependencies = HasAnalytics & HasFeatureManager
    private let dependencies: Dependencies
    
    private let presenter: TutorialPresenting
    private let tutorialContext: TutorialContext

    init(presenter: TutorialPresenting, dependencies: Dependencies, tutorialContext: TutorialContext) {
        self.presenter = presenter
        self.dependencies = dependencies
        self.tutorialContext = tutorialContext
    }
}

// MARK: - TutorialInteracting
extension TutorialInteractor: TutorialInteracting {
    func loadContext() {
        presenter.display(tutorialContext)
    }
    
    func sendTutorialOpenedEvent() {
        dependencies.analytics.log(analyticsEvent)
    }
    
    func buttonAction() {
        guard case .boleto = tutorialContext else { return }
        presenter.didNextStep(action: .didNextStep(context: tutorialContext))
    }
    
    func linkAction() {
        guard case .account = tutorialContext else { return }
        handleAccountLinkAction()
    }
}

private extension TutorialInteractor {
    var analyticsEvent: AnalyticsKeyProtocol {
        switch tutorialContext {
        case .account:
            return CashInLegacyEvent.wireTransfer(.howToAddMoney)
        case .boleto:
            return CashInLegacyEvent.recharge(.howToAddMoney)
        }
    }
    
    func handleAccountLinkAction() {
        let urlString = dependencies.featureManager.text(.opsCashInTutorialWireTransferString)
        guard let url = URL(string: urlString) else { return }
        
        presenter.didNextStep(action: .deeplink(url: url))
        let event = CashInLegacyEvent.faqDeeplink(.howToAddMoney)
        dependencies.analytics.log(event)
    }
}
