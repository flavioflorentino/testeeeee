import UI
import UIKit

protocol TutorialDisplaying: AnyObject {
    func setUp(with descriptor: TutorialDescriptor)
}

final class TutorialViewController: ViewController<TutorialInteracting, TutorialView> {
    override func viewDidLoad() {
        super.viewDidLoad()

        buildLayout()
        interactor.loadContext()
        interactor.sendTutorialOpenedEvent()
        rootView.delegate = self
    }

    override func buildViewHierarchy() { }
    
    override func setupConstraints() { }

    override func configureViews() {
        setUpNavigationBarDefaultAppearance()
        rootView.backgroundColor = Colors.backgroundPrimary.color
    }
}

// MARK: - TutorialDisplaying
extension TutorialViewController: TutorialDisplaying {
    func setUp(with descriptor: TutorialDescriptor) {
        rootView.setUp(with: descriptor)
    }
}

extension TutorialViewController: TutorialViewDelegate {
    func didTapLink() {
        interactor.linkAction()
    }
    
    func didTapButton() {
        interactor.buttonAction()
    }
}
