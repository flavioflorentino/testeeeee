import Foundation

protocol NewBoletoInteracting: AnyObject {
    func doSomething()
}

final class NewBoletoInteractor {
    private let service: NewBoletoServicing
    private let presenter: NewBoletoPresenting

    init(service: NewBoletoServicing, presenter: NewBoletoPresenting) {
        self.service = service
        self.presenter = presenter
    }
}

// MARK: - NewBoletoInteracting
extension NewBoletoInteractor: NewBoletoInteracting {
    func doSomething() {
        presenter.displaySomething()
    }
}
