import UIKit

enum NewBoletoFactory {
    static func make() -> NewBoletoViewController {
        let service: NewBoletoServicing = NewBoletoService()
        let coordinator: NewBoletoCoordinating = NewBoletoCoordinator()
        let presenter: NewBoletoPresenting = NewBoletoPresenter(coordinator: coordinator)
        let interactor = NewBoletoInteractor(service: service, presenter: presenter)
        let viewController = NewBoletoViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
