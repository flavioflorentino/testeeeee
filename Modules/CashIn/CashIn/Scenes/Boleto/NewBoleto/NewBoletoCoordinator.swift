import UIKit

enum NewBoletoAction {
}

protocol NewBoletoCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: NewBoletoAction)
}

final class NewBoletoCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - NewBoletoCoordinating
extension NewBoletoCoordinator: NewBoletoCoordinating {
    func perform(action: NewBoletoAction) {
    }
}
