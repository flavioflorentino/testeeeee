import UI
import UIKit

protocol NewBoletoDisplaying: AnyObject {
    func displaySomething()
}

final class NewBoletoViewController: ViewController<NewBoletoInteracting, UIView> {
    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.doSomething()
    }

    override func buildViewHierarchy() { }
    
    override func setupConstraints() { }

    override func configureViews() { }
}

// MARK: - NewBoletoDisplaying
extension NewBoletoViewController: NewBoletoDisplaying {
    func displaySomething() { }
}

extension NewBoletoViewController: BoletoTab {
    func didUpdateSelectedTab(on index: Int) { }
    func didSelectButton(on index: Int) { }
}
