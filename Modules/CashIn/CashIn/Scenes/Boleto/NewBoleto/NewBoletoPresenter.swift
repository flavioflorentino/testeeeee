import Foundation

protocol NewBoletoPresenting: AnyObject {
    var viewController: NewBoletoDisplaying? { get set }
    func displaySomething()
    func didNextStep(action: NewBoletoAction)
}

final class NewBoletoPresenter {
    private let coordinator: NewBoletoCoordinating
    weak var viewController: NewBoletoDisplaying?

    init(coordinator: NewBoletoCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - NewBoletoPresenting
extension NewBoletoPresenter: NewBoletoPresenting {
    func displaySomething() {
        viewController?.displaySomething()
    }
    
    func didNextStep(action: NewBoletoAction) {
        coordinator.perform(action: action)
    }
}
