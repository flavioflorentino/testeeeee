import Foundation

protocol BoletoListInteracting: AnyObject {
    func doSomething()
}

final class BoletoListInteractor {
    private let service: BoletoListServicing
    private let presenter: BoletoListPresenting

    init(service: BoletoListServicing, presenter: BoletoListPresenting) {
        self.service = service
        self.presenter = presenter
    }
}

// MARK: - BoletoListInteracting
extension BoletoListInteractor: BoletoListInteracting {
    func doSomething() {
        presenter.displaySomething()
    }
}
