import Foundation

protocol BoletoListPresenting: AnyObject {
    var viewController: BoletoListDisplaying? { get set }
    func displaySomething()
    func didNextStep(action: BoletoListAction)
}

final class BoletoListPresenter {
    private let coordinator: BoletoListCoordinating
    weak var viewController: BoletoListDisplaying?

    init(coordinator: BoletoListCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - BoletoListPresenting
extension BoletoListPresenter: BoletoListPresenting {
    func displaySomething() {
        viewController?.displaySomething()
    }
    
    func didNextStep(action: BoletoListAction) {
        coordinator.perform(action: action)
    }
}
