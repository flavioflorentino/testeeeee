import UIKit

enum BoletoListAction {
}

protocol BoletoListCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: BoletoListAction)
}

final class BoletoListCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - BoletoListCoordinating
extension BoletoListCoordinator: BoletoListCoordinating {
    func perform(action: BoletoListAction) {
    }
}
