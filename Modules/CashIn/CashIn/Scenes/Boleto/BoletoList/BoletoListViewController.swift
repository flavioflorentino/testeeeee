import UI
import UIKit

protocol BoletoListDisplaying: AnyObject {
    func displaySomething()
}

final class BoletoListViewController: ViewController<BoletoListInteracting, UIView> {
    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.doSomething()
    }

    override func buildViewHierarchy() { }
    
    override func setupConstraints() { }

    override func configureViews() { }
}

// MARK: - BoletoListDisplaying
extension BoletoListViewController: BoletoListDisplaying {
    func displaySomething() { }
}

extension BoletoListViewController: BoletoTab {
    func didUpdateSelectedTab(on index: Int) { }
    func didSelectButton(on index: Int) { }
}
