import UIKit

enum BoletoListFactory {
    static func make() -> BoletoListViewController {
        let service: BoletoListServicing = BoletoListService()
        let coordinator: BoletoListCoordinating = BoletoListCoordinator()
        let presenter: BoletoListPresenting = BoletoListPresenter(coordinator: coordinator)
        let interactor = BoletoListInteractor(service: service, presenter: presenter)
        let viewController = BoletoListViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
