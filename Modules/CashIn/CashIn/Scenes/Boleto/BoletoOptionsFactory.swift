import UI
import UIKit

protocol BoletoTab: UIViewController, TabIndicatorViewControllerDelegate {}

public enum BoletoOptionsFactory {
    public static func make() -> UIViewController {
        let titles = [
            Strings.CashInMethods.Boleto.NewBoleto.title,
            Strings.CashInMethods.Boleto.BoletoList.title
        ]
        let tabsViewControllers: [BoletoTab] = [
            NewBoletoFactory.make(),
            BoletoListFactory.make()
        ]
        
        let tabIndicatorViewController = TabIndicatorViewController(with: titles)
        tabIndicatorViewController.delegates = tabsViewControllers
        tabIndicatorViewController.setViewControllers(tabsViewControllers, animated: false)

        return tabIndicatorViewController
    }
}
