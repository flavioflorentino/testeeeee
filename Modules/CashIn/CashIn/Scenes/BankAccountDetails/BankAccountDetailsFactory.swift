import UIKit

enum BankAccountDetailsFactory {
    static func make() -> BankAccountDetailsViewController {
        let container = DependencyContainer()
        let service: BankAccountDetailsServicing = BankAccountDetailsService(dependencies: container)
        let coordinator: BankAccountDetailsCoordinating = BankAccountDetailsCoordinator(dependencies: container)
        let presenter: BankAccountDetailsPresenting = BankAccountDetailsPresenter(coordinator: coordinator)
        let interactor = BankAccountDetailsInteractor(service: service, presenter: presenter, dependencies: container)
        let viewController = BankAccountDetailsViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
