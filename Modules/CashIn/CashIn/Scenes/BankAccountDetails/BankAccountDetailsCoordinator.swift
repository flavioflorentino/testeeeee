import UIKit

enum BankAccountDetailsAction: Equatable {
    case back
    case startOnboarding
    case deeplink(url: URL)
}

protocol BankAccountDetailsCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: BankAccountDetailsAction)
}

final class BankAccountDetailsCoordinator {
    typealias Dependencies = HasDeeplink
    private let dependencies: Dependencies
    weak var viewController: UIViewController?
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    private func startOnboarding() {
        let onboardingController = OnboardingFactory.make(for: .wireTransfer)
        let navigationController = UINavigationController(rootViewController: onboardingController)
        viewController?.present(navigationController, animated: true)
    }
}

// MARK: - BankAccountDetailsCoordinating
extension BankAccountDetailsCoordinator: BankAccountDetailsCoordinating {
    func perform(action: BankAccountDetailsAction) {
        switch action {
        case .back:
            viewController?.navigationController?.popViewController(animated: true)
        case .startOnboarding:
            startOnboarding()
        case let .deeplink(url):
            dependencies.deeplink?.open(url: url)
        }
    }
}
