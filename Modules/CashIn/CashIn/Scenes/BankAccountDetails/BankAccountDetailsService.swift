import Core
import Foundation

protocol BankAccountDetailsServicing {
    func getBankingInfo(completion: @escaping (Result<BankingInfo, ApiError>) -> Void)
}

final class BankAccountDetailsService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - BankAccountDetailsServicing
extension BankAccountDetailsService: BankAccountDetailsServicing {
    func getBankingInfo(completion: @escaping (Result<BankingInfo, ApiError>) -> Void) {
        let methodId = MethodId.account.rawValue
        let api = Api<BankingInfo>(endpoint: CashInEndpoints.methodDetail(id: methodId))

        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
