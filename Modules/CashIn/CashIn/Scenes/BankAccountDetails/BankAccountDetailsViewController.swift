import AssetsKit
import UI
import UIKit

protocol BankAccountDetailsDisplaying: AnyObject {
    func shareInfo(_ info: [Any])
    func setup(userInfo: UserInfo, bankInfo: BankAccountInfo)
    func beginLoading()
    func displayError(with viewModel: StatefulErrorViewModel)
}

final class BankAccountDetailsViewController: ViewController<BankAccountDetailsInteracting, UIView> {
    fileprivate enum Layout { }

    private lazy var scrollView: UIScrollView = {
        let view = UIScrollView()
        view.alwaysBounceVertical = false
        return view
    }()

    private lazy var moreInfoBarButton: UIBarButtonItem = {
        let barButtonItem = UIBarButtonItem(image: Resources.Icons.icoQuestion.image,
                                            style: .done,
                                            target: self,
                                            action: #selector(didTapMoreInfo))
        return barButtonItem
    }()

    private lazy var accountView = BankAccountDetailsView(delegate: self)

    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.loadInfo()
    }

    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(accountView)
    }

    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        accountView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.width.equalTo(view)
        }
    }

    override func configureViews() {
        navigationItem.title = Strings.CashInMethods.BankAccount.navigationTitle
        view.backgroundColor = Colors.backgroundPrimary.color

        navigationItem.rightBarButtonItem = moreInfoBarButton
        setUpNavigationBarDefaultAppearance()
    }

    @objc
    private func didTapMoreInfo() {
        interactor.moreInfo()
    }
}

// MARK: - BankAccountDetailsDisplaying
extension BankAccountDetailsViewController: BankAccountDetailsDisplaying {
    func shareInfo(_ info: [Any]) {
        let controller = UIActivityViewController(activityItems: info, applicationActivities: nil)
        present(controller, animated: true)
    }
    
    func setup(userInfo: UserInfo, bankInfo: BankAccountInfo) {
        endState()
        accountView.setup(userInfo: userInfo, bankInfo: bankInfo)
    }
    
    func beginLoading() {
        beginState()
    }
    
    func displayError(with viewModel: StatefulErrorViewModel) {
        endState(model: viewModel)
    }
}

extension BankAccountDetailsViewController: BankInfoDelegate {
    func shareBankInfo() {
        interactor.shareBankInfo()
    }
}

extension BankAccountDetailsViewController: StatefulTransitionViewing {
    func didTryAgain() {
        interactor.loadInfo()
    }
    
    func statefulViewForError() -> StatefulViewing {
        let view = EmptyStateView()
        view.buildLayout()
        view.delegate = self
        return view
    }
}
