import AnalyticsModule
import AssetsKit
import Core
import FeatureFlag
import Foundation

protocol BankAccountDetailsInteracting: AnyObject {
    func loadInfo()
    func close()
    func moreInfo()
    func shareBankInfo()
}

final class BankAccountDetailsInteractor {
    typealias Dependencies = HasAnalytics & HasKVStore & HasFeatureManager
    private let dependencies: Dependencies
    
    private var accountDetails: BankAccountDetails?
    private let service: BankAccountDetailsServicing
    private let presenter: BankAccountDetailsPresenting

    init(service: BankAccountDetailsServicing, presenter: BankAccountDetailsPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

private extension BankAccountDetailsInteractor {
    var onboardinguUserDefaultsKey: String { "wire-transfer-onboarding-has-been-shown" }
    
    var onboardingHasBeenShown: Bool {
        dependencies.kvStore.getFirstTimeOnlyEvent(onboardinguUserDefaultsKey)
    }
    
    func presentOnboardingIfNeeded() {
        guard !onboardingHasBeenShown else { return }
        presenter.didNextStep(action: .startOnboarding)
        dependencies.kvStore.setFirstTimeOnlyEvent(onboardinguUserDefaultsKey)
    }

    func sendAnalyticsEvent() {
        let event = CashInLegacyEvent.wireTransfer(.picpayAccount)
        dependencies.analytics.log(event)
    }
    
    func mapBankingInfo(_ info: BankingInfo) -> BankAccountDetails {
        BankAccountDetails(userInfo: UserInfo(avatar: URL(string: info.consumerAvatar ?? ""),
                                              avatarPlaceholder: Resources.Placeholders.greenAvatarPlaceholder.image,
                                              username: info.consumerUsername,
                                              fullname: info.consumerName),
                           bankInfo: BankAccountInfo(bankNumber: info.bankId,
                                                     agencyNumber: info.accountAgency,
                                                     accountNumber: info.accountNumber,
                                                     document: info.consumerCpf))
    }
}

// MARK: - BankAccountDetailsInteracting
extension BankAccountDetailsInteractor: BankAccountDetailsInteracting {
    func loadInfo() {
        presenter.displayLoading()
        service.getBankingInfo { [weak self] result in
            guard let self = self else { return }
            switch result {
            case let .success(bankingInfo):
                let accountDetails = self.mapBankingInfo(bankingInfo)
                self.accountDetails = accountDetails
                self.presenter.display(accountDetails)
                self.presentOnboardingIfNeeded()
                self.sendAnalyticsEvent()
            case .failure:
                self.presenter.displayError()
            }
        }
    }
    
    func close() {
        presenter.didNextStep(action: .back)
    }

    func moreInfo() {
        let urlString = dependencies.featureManager.text(.cashInMethodsMoreInfoUrl)
        guard let url = URL(string: urlString) else { return }
        presenter.didNextStep(action: .deeplink(url: url))
    }

    func shareBankInfo() {
        guard let accountInfo = accountDetails else { return } // Fetch account details
        let accountString = accountInfo.bankInfo.accountNumber.byRemovingPunctuation
        let documentString = accountInfo.bankInfo.document.byRemovingPunctuation
        let infoString = Strings.CashInMethods.BankInfo.sharable(accountInfo.userInfo.fullname,
                                                                 accountInfo.bankInfo.bankNumber,
                                                                 accountInfo.bankInfo.agencyNumber,
                                                                 accountString,
                                                                 documentString)

        presenter.share(accountInfo: [infoString])
    }
}
