import SnapKit
import UI
import UIKit

final class UserInfoView: UIView, ViewConfiguration {
    private lazy var avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.imageStyle(AvatarImageStyle(size: .xLarge, hasBorder: false))
        return imageView
    }()

    private lazy var usernameLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
            .with(\.textAlignment, .center)
        return label
    }()

    private lazy var fullnameLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textAlignment, .center)
            .with(\.textColor, .grayscale500())
        return label
    }()

    func buildViewHierarchy() {
        addSubviews(avatarImageView,
                    usernameLabel,
                    fullnameLabel)
    }

    func setupConstraints() {
        avatarImageView.snp.makeConstraints {
            $0.top.centerX.equalToSuperview()
        }

        usernameLabel.snp.makeConstraints {
            $0.top.equalTo(avatarImageView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview()
        }

        fullnameLabel.snp.makeConstraints {
            $0.top.equalTo(usernameLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.bottom.trailing.equalToSuperview()
        }
    }
}

extension UserInfoView {
    /// Populates the texts and images in the view
    func setup(with userInfo: UserInfo) {
        buildLayout()

        usernameLabel.text = "@\(userInfo.username)"
        fullnameLabel.text = userInfo.fullname
        avatarImageView.setImage(url: userInfo.avatar,
                                 placeholder: userInfo.avatarPlaceholder)
    }
}
