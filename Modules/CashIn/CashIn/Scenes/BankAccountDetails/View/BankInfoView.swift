import SnapKit
import UI
import UIKit

protocol BankInfoDelegate: CopyableDelegate {
    func shareBankInfo()
}

final class BankInfoView: UIView, ViewConfiguration {
    fileprivate enum Layout { }

    private lazy var headerLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textAlignment, .center)
        return label
    }()

    private lazy var copiableStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        return stack
    }()

    private lazy var stackTitle: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
        return label
    }()

    private lazy var shareButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Strings.CashInMethods.BankInfo.share, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(shareBankInfo), for: .touchUpInside)
        return button
    }()

    private weak var delegate: BankInfoDelegate?

    init(delegate: BankInfoDelegate? = nil) {
        self.delegate = delegate
        super.init(frame: .zero)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc
    func shareBankInfo() {
        delegate?.shareBankInfo()
    }

    func buildViewHierarchy() {
       addSubviews(headerLabel,
                   copiableStack,
                   shareButton)
    }

    func setupConstraints() {
        headerLabel.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        copiableStack.snp.makeConstraints {
            $0.top.equalTo(headerLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        stackTitle.snp.makeConstraints {
            $0.height.equalTo(Spacing.base08)
        }

        shareButton.snp.makeConstraints {
            $0.top.equalTo(copiableStack.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview()
        }
    }

    func configureViews() {
        copiableStack.backgroundColor = Colors.backgroundPrimary.color
        headerLabel.text = Strings.CashInMethods.BankInfo.header
    }
}

extension BankInfoView {
    /// Populates the texts in the view
    func setup(with bankInfo: BankAccountInfo) {
        stackTitle.text = Strings.CashInMethods.BankInfo.title

        let bankView = CopyableInfoView(delegate: delegate)
        let agencyView = CopyableInfoView(delegate: delegate)
        let accountView = CopyableInfoView(delegate: delegate)
        let documentView = CopyableInfoView(delegate: delegate)

        bankView.setUp(title: Strings.CashInMethods.BankInfo.bank, info: bankInfo.bankNumber)
        agencyView.setUp(title: Strings.CashInMethods.BankInfo.agency, info: bankInfo.agencyNumber)
        accountView.setUp(title: Strings.CashInMethods.BankInfo.account, info: bankInfo.accountNumber)
        documentView.setUp(title: Strings.CashInMethods.BankInfo.document, info: bankInfo.document)

        copiableStack.addArrangedSubview(stackTitle)
        copiableStack.addArrangedSubview(bankView)
        copiableStack.addArrangedSubview(agencyView)
        copiableStack.addArrangedSubview(accountView)
        copiableStack.addArrangedSubview(documentView)

        copiableStack.insertLine(after: stackTitle, color: Colors.grayscale400.color)
        copiableStack.insertLine(after: bankView, color: Colors.grayscale400.color)
        copiableStack.insertLine(after: agencyView, color: Colors.grayscale400.color)
        copiableStack.insertLine(after: accountView, color: Colors.grayscale400.color)
    }
}
