import SnapKit
import UI
import UIKit

final class BankAccountDetailsView: UIView, ViewConfiguration {
    private lazy var userInfoView = UserInfoView()
    
    private lazy var bankInfoView = BankInfoView(delegate: delegate)
    
    private lazy var footerContainer: UIView = {
        let view = UIView()
        view.viewStyle(RoundedViewStyle(cornerRadius: .light))
            .with(\.backgroundColor, .grayscale050())
        return view
    }()
    
    private lazy var footerLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
        return label
    }()
    
    private var footerText: NSMutableAttributedString {
        let boldText = Strings.CashInMethods.BankAccount.footerRemind
        let defaultText = Strings.CashInMethods.BankAccount.footerInfo
        let mutableString = NSMutableAttributedString()
            .append(boldText, font: Typography.bodyPrimary(.highlight).font())
            .append(defaultText, font: Typography.bodyPrimary().font())

        return mutableString
    }
    
    private weak var delegate: BankInfoDelegate?

    init(delegate: BankInfoDelegate? = nil) {
        self.delegate = delegate
        super.init(frame: .zero)
        buildLayout()
    }

    override init(frame: CGRect) {
        self.delegate = nil
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubviews(userInfoView, bankInfoView, footerContainer)
        footerContainer.addSubview(footerLabel)
    }
    
    func setupConstraints() {
        userInfoView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
        }
        
        bankInfoView.snp.makeConstraints {
            $0.top.equalTo(userInfoView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
        }
        
        footerContainer.snp.makeConstraints {
            $0.top.equalTo(bankInfoView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.lessThanOrEqualToSuperview().inset(Spacing.base08)
        }
        
        footerLabel.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
        footerLabel.attributedText = footerText
    }
}

extension BankAccountDetailsView {
    func setup(userInfo: UserInfo, bankInfo: BankAccountInfo) {
        userInfoView.setup(with: userInfo)
        bankInfoView.setup(with: bankInfo)
    }
}
