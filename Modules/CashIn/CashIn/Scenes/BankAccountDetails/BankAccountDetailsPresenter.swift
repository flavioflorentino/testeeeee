import Core
import Foundation
import UIKit
import UI

protocol BankAccountDetailsPresenting: AnyObject {
    var viewController: BankAccountDetailsDisplaying? { get set }
    func display(_ accountInfo: BankAccountDetails)
    func share(accountInfo: [Any])
    func displayError()
    func displayLoading()
    func didNextStep(action: BankAccountDetailsAction)
}

final class BankAccountDetailsPresenter {
    private let coordinator: BankAccountDetailsCoordinating
    weak var viewController: BankAccountDetailsDisplaying?

    init(coordinator: BankAccountDetailsCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - BankAccountDetailsPresenting
extension BankAccountDetailsPresenter: BankAccountDetailsPresenting {
    func share(accountInfo: [Any]) {
        viewController?.shareInfo(accountInfo)
    }
    
    func display(_ accountInfo: BankAccountDetails) {
        viewController?.setup(userInfo: accountInfo.userInfo, bankInfo: formatInfo(from: accountInfo.bankInfo))
    }
    
    func displayLoading() {
        viewController?.beginLoading()
    }
    
    func displayError() {
        viewController?.displayError(with: StatefulErrorViewModel.cashInError)
    }
    
    func didNextStep(action: BankAccountDetailsAction) {
        coordinator.perform(action: action)
    }
}

private extension BankAccountDetailsPresenter {
    func formatInfo(from bankInfo: BankAccountInfo) -> BankAccountInfo {
        let mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cpf)
        let maskedCpf = mask.maskedText(from: bankInfo.document) ?? bankInfo.document
        
        return BankAccountInfo(bankNumber: bankInfo.bankNumber,
                               agencyNumber: bankInfo.agencyNumber,
                               accountNumber: bankInfo.accountNumber,
                               document: maskedCpf)
    }
}
