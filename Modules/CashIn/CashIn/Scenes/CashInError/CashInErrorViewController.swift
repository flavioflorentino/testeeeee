import AssetsKit
import Foundation
import UI
import UIKit

protocol CashInErrorDisplaying: AnyObject {
    func setUp(with viewModel: CashInErrorViewModel)
}

private extension CashInErrorViewController.Layout {
    enum Size {
        static let image: CGFloat = 145.0
        static let button: CGFloat = 28.0
    }
}

final class CashInErrorViewController: ViewController<CashInErrorInteracting, UIView> {
    fileprivate enum Layout { }

    private lazy var containerView = UIView()

    private lazy var closeButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = Typography.icons(.large).font()
        button.setTitleColor(Colors.branding600.color, for: .normal)
        button.setTitle(Iconography.multiply.rawValue, for: .normal)
        button.addTarget(self, action: #selector(didTapClose), for: .touchUpInside)
        return button
    }()

    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textAlignment, .center)
            .with(\.textColor, .black())

        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textAlignment, .center)
            .with(\.textColor, .grayscale700())

        return label
    }()

    private lazy var primaryButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTapPrimaryButton), for: .touchUpInside)

        return button
    }()

    private lazy var secondaryButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.Global.HelperCenter.action, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(didTapSecondaryButton), for: .touchUpInside)
        return button
    }()

    private lazy var footerNotesLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.textAlignment, .center)
            .with(\.textColor, .grayscale500())

        return label
    }()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.loadError()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }

    override func buildViewHierarchy() {
        containerView.addSubviews(imageView,
                                  titleLabel,
                                  descriptionLabel,
                                  primaryButton,
                                  secondaryButton,
                                  footerNotesLabel)
        
        view.addSubviews(containerView, closeButton)
    }
    
    override func setupConstraints() {
        containerView.setContentHuggingPriority(.defaultHigh, for: .vertical)
        containerView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.top.greaterThanOrEqualToSuperview().offset(Spacing.base02)
            $0.bottom.lessThanOrEqualToSuperview().offset(-Spacing.base02)
            $0.centerY.equalToSuperview().priority(.required)
        }
        
        closeButton.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.button)
            $0.top.equalTo(view.compatibleSafeArea.top).offset(Spacing.base01)
            $0.trailing.equalToSuperview().offset(-Spacing.base01)
        }

        imageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.image)
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.centerX.equalToSuperview()
        }

        titleLabel.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        primaryButton.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        secondaryButton.snp.makeConstraints {
            $0.top.equalTo(primaryButton.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        footerNotesLabel.snp.makeConstraints {
            $0.top.equalTo(secondaryButton.snp.bottom).offset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview()
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        
        addGestures()
    }

    private func addGestures() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapFooterNotes(_:)))
        tapGesture.numberOfTapsRequired = 1
        footerNotesLabel.isUserInteractionEnabled = true
        footerNotesLabel.addGestureRecognizer(tapGesture)
    }

    @objc
    private func didTapClose() {
        dismiss(animated: true, completion: nil)
    }

    @objc
    private func didTapPrimaryButton() {
        interactor.primaryAction()
    }

    @objc
    func didTapSecondaryButton() {
        interactor.secondaryAction()
    }

    @objc
    private func didTapFooterNotes(_ sender: Any) {
        interactor.footerAction()
    }
}

// MARK: - CashInErrorDisplaying
extension CashInErrorViewController: CashInErrorDisplaying {
    func setUp(with viewModel: CashInErrorViewModel) {
        titleLabel.text = viewModel.titleText
        descriptionLabel.attributedText = viewModel.descriptionText
        primaryButton.setTitle(viewModel.primaryButtonText, for: .normal)
        secondaryButton.setTitle(viewModel.secondaryButtonText, for: .normal)
        secondaryButton.buttonStyle(LinkButtonStyle())
        secondaryButton.isHidden = viewModel.isSecondaryButtonHidden
        footerNotesLabel.attributedText = viewModel.bottomText
        footerNotesLabel.isHidden = viewModel.isBottomTextHidden
        imageView.setImage(url: viewModel.imageURL, placeholder: Assets.iluCashInError.image)
    }
}
