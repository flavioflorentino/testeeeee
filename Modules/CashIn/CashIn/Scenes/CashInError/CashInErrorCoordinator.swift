import UIKit

enum CashInErrorAction {
    case home
    case dismiss
    case openDeeplink(url: URL)
}

extension CashInErrorAction: Equatable {}

protocol CashInErrorCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: CashInErrorAction)
}

final class CashInErrorCoordinator {
    typealias Dependencies = HasLegacy & HasDeeplink
    private let dependencies: Dependencies
    weak var viewController: UIViewController?
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - CashInErrorCoordinating
extension CashInErrorCoordinator: CashInErrorCoordinating {
    func perform(action: CashInErrorAction) {
        guard let controller = viewController else { return }

        switch action {
        case .home:
            dependencies.legacy.methods?.goHome()
            controller.dismiss(animated: true)
        case .dismiss:
            controller.dismiss(animated: true)
        case let .openDeeplink(url):
            dependencies.deeplink?.open(url: url)
        }
    }
}
