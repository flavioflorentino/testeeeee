import Foundation

public struct CashInErrorModel: Decodable {
    let code: Int
    let title: String
    let message: String
    let image: String
    let buttonText: String
    let buttonUrl: String
    let underlinedButtonText: String?
    let underlinedButtonUrl: String?
    let footerButtonText: String?
    let footerButtonUrl: String?
    
    public init(code: Int,
                title: String,
                message: String,
                image: String,
                buttonText: String,
                buttonUrl: String,
                underlinedButtonText: String? = nil,
                underlinedButtonUrl: String? = nil,
                footerButtonText: String? = nil,
                footerButtonUrl: String? = nil) {
        self.code = code
        self.title = title
        self.message = message
        self.image = image
        self.buttonText = buttonText
        self.buttonUrl = buttonUrl
        self.underlinedButtonText = underlinedButtonText
        self.underlinedButtonUrl = underlinedButtonUrl
        self.footerButtonText = footerButtonText
        self.footerButtonUrl = footerButtonUrl
    }
}

extension CashInErrorModel: Equatable {}
