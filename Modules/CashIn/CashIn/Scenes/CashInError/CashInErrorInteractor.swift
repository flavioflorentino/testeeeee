import Foundation

private enum PrimaryAction: String {
    case home = "HOME"
    case identityAnalysis = "IDENTITY_ANALYSIS"
}

protocol CashInErrorInteracting: AnyObject {
    func loadError()
    func primaryAction()
    func secondaryAction()
    func footerAction()
    func close()
}

final class CashInErrorInteractor {
    typealias Localizable = Strings.Global.Default.Error
    private let presenter: CashInErrorPresenting
    private var errorModel: CashInErrorModel?

    init(error: CashInErrorModel?, presenter: CashInErrorPresenting) {
        self.errorModel = error
        self.presenter = presenter
    }
    
    private var error: CashInErrorModel {
        errorModel ?? defaultErrorModel
    }
    
    private lazy var defaultErrorModel = CashInErrorModel(code: 0,
                                                          title: Localizable.title,
                                                          message: Localizable.message,
                                                          image: "",
                                                          buttonText: Localizable.buttonTitle,
                                                          buttonUrl: "")
}

// MARK: - CashInErrorInteracting
extension CashInErrorInteractor: CashInErrorInteracting {
    func loadError() {
        presenter.display(model: error)
    }
    
    func primaryAction() {
        switch error.buttonUrl {
        case PrimaryAction.identityAnalysis.rawValue:
            guard let url = URL(string: "picpay://picpay/identityanalysis")
                else { return }
            presenter.didNextStep(action: .openDeeplink(url: url))

        case PrimaryAction.home.rawValue:
            presenter.didNextStep(action: .home)

        default:
            presenter.didNextStep(action: .dismiss)
        }
    }
    
    func secondaryAction() {
        guard let urlString = error.underlinedButtonUrl,
            let url = URL(string: urlString) else { return }
        presenter.didNextStep(action: .openDeeplink(url: url))
    }
    
    func footerAction() {
        guard let urlString = error.footerButtonUrl,
            let url = URL(string: urlString) else { return }
        presenter.didNextStep(action: .openDeeplink(url: url))
    }
    
    func close() {
        presenter.didNextStep(action: .dismiss)
    }
}
