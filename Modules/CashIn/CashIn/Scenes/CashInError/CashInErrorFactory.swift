import UIKit

public enum CashInErrorFactory {
    public static func make(for error: CashInErrorModel?) -> UIViewController {
        let container = DependencyContainer()
        let coordinator: CashInErrorCoordinating = CashInErrorCoordinator(dependencies: container)
        let presenter: CashInErrorPresenting = CashInErrorPresenter(coordinator: coordinator)
        let interactor = CashInErrorInteractor(error: error, presenter: presenter)
        let viewController = CashInErrorViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
