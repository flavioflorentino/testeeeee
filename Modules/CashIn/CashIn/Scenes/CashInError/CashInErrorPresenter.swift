import Foundation
import UIKit
import UI

struct CashInErrorViewModel {
    let imageURL: URL?
    let titleText: String
    let descriptionText: NSAttributedString
    let primaryButtonText: String
    let secondaryButtonText: String?
    let isSecondaryButtonHidden: Bool
    let bottomText: NSAttributedString?
    let isBottomTextHidden: Bool
}

protocol CashInErrorPresenting: AnyObject {
    var viewController: CashInErrorDisplaying? { get set }
    func display(model: CashInErrorModel)
    func didNextStep(action: CashInErrorAction)
}

final class CashInErrorPresenter {
    private let coordinator: CashInErrorCoordinating
    weak var viewController: CashInErrorDisplaying?

    init(coordinator: CashInErrorCoordinating) {
        self.coordinator = coordinator
    }
}

private extension CashInErrorPresenter {
    func generateViewModel(model: CashInErrorModel) -> CashInErrorViewModel {
        let descriptionText = createDescriptionAttributedString(from: model.message)
        let bottomText = createBottomTextAttributedString(from: model.footerButtonText)

        return CashInErrorViewModel(
            imageURL: URL(string: model.image),
            titleText: model.title,
            descriptionText: descriptionText,
            primaryButtonText: model.buttonText,
            secondaryButtonText: model.underlinedButtonText,
            isSecondaryButtonHidden: model.underlinedButtonText == nil,
            bottomText: bottomText,
            isBottomTextHidden: model.footerButtonText == nil
        )
    }

    func createDescriptionAttributedString(from text: String) -> NSAttributedString {
        text.applyHtmlTags(with: Typography.bodyPrimary(.default).font(), textAlignment: .center) ??
            NSAttributedString(string: Strings.Global.Default.Error.message)
    }

    func createBottomTextAttributedString(from text: String?) -> NSAttributedString? {
        text?.applyHtmlTags(with: Typography.caption().font(), textAlignment: .center)
    }
}

// MARK: - CashInErrorPresenting
extension CashInErrorPresenter: CashInErrorPresenting {
    func didNextStep(action: CashInErrorAction) {
        coordinator.perform(action: action)
    }
    
    func display(model: CashInErrorModel) {
        let viewModel = generateViewModel(model: model)
        viewController?.setUp(with: viewModel)
    }
}
