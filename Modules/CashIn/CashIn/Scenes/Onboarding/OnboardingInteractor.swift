import AnalyticsModule
import FeatureFlag
import Foundation

public enum OnboardingGroup {
    case emergencyAid
    case wireTransfer
}

protocol OnboardingInteracting: AnyObject {
    func load()
    func scroll(to page: Int)
    func action()
    func close()
}

final class OnboardingInteractor {
    typealias Dependencies = HasAnalytics & HasFeatureManager
    
    private let onboardingGroup: OnboardingGroup
    private let dependencies: Dependencies
    private let presenter: OnboardingPresenting

    private lazy var panelStyles: [OnboardingPanel] = {
        switch onboardingGroup {
        case .emergencyAid:
            return [.bills, .phone, .establishments].shuffled()
            
        case .wireTransfer:
            return [.startUsing, .benefits, .enjoy]
        }
    }()
    
    private var currentPage: Int = 0
    
    init(for group: OnboardingGroup, presenter: OnboardingPresenting, dependencies: Dependencies) {
        self.onboardingGroup = group
        self.presenter = presenter
        self.dependencies = dependencies
    }
    
    private func sendAnalyticsEvent(for panel: OnboardingPanel) {
        let event = CashInLegacyEvent.onboarding(panel)
        dependencies.analytics.log(event)
    }
}

// MARK: - OnboardingInteracting
extension OnboardingInteractor: OnboardingInteracting {
    func load() {
        presenter.display(panels: panelStyles)
        presenter.displayFooter(currentPage: currentPage, numberOfPages: panelStyles.count)
        sendAnalyticsEvent(for: panelStyles[currentPage])
    }
    
    func scroll(to page: Int) {
        currentPage = page
        let numberOfPages = panelStyles.count
        guard currentPage >= 0 && currentPage < numberOfPages else {
            return
        }
        
        presenter.displayFooter(currentPage: currentPage, numberOfPages: numberOfPages)
        sendAnalyticsEvent(for: panelStyles[currentPage])
    }

    func action() {
        let nextStep = currentPage + 1
        guard nextStep < panelStyles.count else {
            presenter.didNextStep(action: .close)
            return
        }
        presenter.goTo(step: nextStep)
    }
    
    func close() {
        presenter.didNextStep(action: .close)
    }
}
