import Foundation

enum OnboardingPanelDescriptorFactory {
    static func make(for panel: OnboardingPanel) -> OnboardingPanelDescriptor {
        switch panel {
        case .bills:
            return OnboardingBillsDescriptor()
        case .phone:
            return OnboardingPhoneDescriptor()
        case .establishments:
            return OnboardingEstablishmentsDescriptor()
        case .startUsing:
            return OnboardingStartUsingDescriptor()
        case .benefits:
            return OnboardingBenefitsDescriptor()
        case .enjoy:
            return OnboardingEnjoyDescriptor()
        }
    }
}

enum OnboardingPanel: String, CaseIterable, CustomStringConvertible {
    case bills = "button_info_pay_bill"
    case phone = "button_info_cellphone_recharge"
    case establishments = "button_info_establishment"
    case startUsing = "Account"
    case benefits = "Benefits"
    case enjoy = "Enjoy"
    
    var description: String {
        rawValue
    }
}
