import UIKit

private typealias Localizable = Strings.Onboarding

protocol OnboardingPanelDescriptor {
    var ilustration: UIImage { get }
    var title: String { get }
    var description: String { get }
}

struct OnboardingBillsDescriptor: OnboardingPanelDescriptor {
    var ilustration: UIImage { Assets.iluRechargeOnboardingBills.image }
    
    var title: String { Localizable.Bills.title }
    
    var description: String { Localizable.Bills.description }
}

struct OnboardingPhoneDescriptor: OnboardingPanelDescriptor {
    var ilustration: UIImage { Assets.iluRechargeOnboardingPhone.image }
    
    var title: String { Localizable.PhoneCredit.title }
    
    var description: String { Localizable.PhoneCredit.description }
}

struct OnboardingEstablishmentsDescriptor: OnboardingPanelDescriptor {
    var ilustration: UIImage { Assets.iluRechargeOnboardingEstablishments.image }
    
    var title: String { Localizable.Establishments.title }
    
    var description: String { Localizable.Establishments.description }
}

struct OnboardingStartUsingDescriptor: OnboardingPanelDescriptor {
    var ilustration: UIImage { Assets.iluWireTransferOnboardingStartUsing.image }
    
    var title: String { Localizable.StartUsing.title }
    
    var description: String { Localizable.StartUsing.description }
}

struct OnboardingBenefitsDescriptor: OnboardingPanelDescriptor {
    var ilustration: UIImage { Assets.iluWireTransferOnboardingBenefits.image }
    
    var title: String { Localizable.Benefits.title }
    
    var description: String { Localizable.Benefits.description }
}

struct OnboardingEnjoyDescriptor: OnboardingPanelDescriptor {
    var ilustration: UIImage { Assets.iluWireTransferOnboardingEnjoy.image }
    
    var title: String { Localizable.Enjoy.title }
    
    var description: String { Localizable.Enjoy.description }
}
