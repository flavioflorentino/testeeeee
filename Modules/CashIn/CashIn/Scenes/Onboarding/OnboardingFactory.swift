import Foundation
import UIKit

public enum OnboardingFactory {
    public static func make(for onboardingGroup: OnboardingGroup) -> UIViewController {
        let dependencies = DependencyContainer()
        let coordinator: OnboardingCoordinating = OnboardingCoordinator(dependencies: dependencies)
        let presenter: OnboardingPresenting = OnboardingPresenter(coordinator: coordinator)
        let interactor = OnboardingInteractor(for: onboardingGroup, presenter: presenter, dependencies: dependencies)
        let viewController = OnboardingViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
