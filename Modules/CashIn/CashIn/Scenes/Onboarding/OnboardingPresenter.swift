import Foundation

protocol OnboardingPresenting: AnyObject {
    var viewController: OnboardingDisplay? { get set }
    func didNextStep(action: OnboardingAction)
    func display(panels: [OnboardingPanel])
    func displayFooter(currentPage: Int, numberOfPages: Int)
    func goTo(step: Int)
}

final class OnboardingPresenter {
    private let coordinator: OnboardingCoordinating
    weak var viewController: OnboardingDisplay?

    init(coordinator: OnboardingCoordinating) {
        self.coordinator = coordinator
    }
    
    private func createFooter(currentPage: Int, numberOfPages: Int) -> OnboardingFooterViewModel {
        let text = currentPage == numberOfPages - 1 ? Strings.Onboarding.ActionButton.gotIt : Strings.Onboarding.ActionButton.next
        return OnboardingFooterViewModel(numberOfPages: numberOfPages,
                                         currentPage: currentPage,
                                         actionButtonTitle: text)
    }
}

// MARK: - OnboardingPresenting
extension OnboardingPresenter: OnboardingPresenting {
    func didNextStep(action: OnboardingAction) {
        coordinator.perform(action: action)
    }
    
    /// The method will populate the tutorial page controller with screens based on the list of styles received.
    /// - Parameter panels: The array of Onboarding panels to be displayed in the tutorial. The array should contain at least 2 elements (it's not a tutorial if you only have 1 page right?!)
    func display(panels: [OnboardingPanel]) {
        guard panels.count > 1 else {
            return
        }
        
        var onboardingControllers: [UIViewController] = []
        for panel in panels {
            let descriptor = OnboardingPanelDescriptorFactory.make(for: panel)
            let controller = OnboardingPanelViewController(descriptor: descriptor)
            onboardingControllers.append(controller)
        }
        
        viewController?.setupOnboarding(with: onboardingControllers)
    }
    
    func displayFooter(currentPage: Int, numberOfPages: Int) {
        let footer = createFooter(currentPage: currentPage, numberOfPages: numberOfPages)
        
        viewController?.setupFooter(for: footer)
    }
    
    func goTo(step: Int) {
        viewController?.goTo(step: step, direction: .forward)
    }
}
