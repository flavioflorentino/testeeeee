import AssetsKit
import SnapKit
import UI
import UIKit

protocol OnboardingDisplay: AnyObject {
    func setupOnboarding(with panels: [UIViewController])
    func setupFooter(for footer: OnboardingFooterViewModel)
    func goTo(step: Int, direction: UIPageViewController.NavigationDirection)
}

final class OnboardingViewController: ViewController<OnboardingInteracting, UIView> {
    fileprivate enum Layout { }

    private var onboardingControllers: [UIViewController] = []
    
    private var nextPanel: UIViewController?
    
    private lazy var pageController: UIPageViewController = {
        let controller = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        controller.delegate = self
        controller.dataSource = self
        controller.view.setContentCompressionResistancePriority(.required, for: .vertical)
        return controller
    }()
    
    private lazy var footerView: OnboardingFooterView = {
        let view = OnboardingFooterView()
        view.delegate = self
        view.setContentHuggingPriority(.defaultHigh, for: .vertical)
        return view
    }()
    
    private lazy var closeBarButton: UIBarButtonItem = {
        let barButtonItem = UIBarButtonItem(image: Resources.Icons.icoClose.image,
                                            style: .plain,
                                            target: self,
                                            action: #selector(didTapClose))
        
        return barButtonItem
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.load()
    }
 
    override func buildViewHierarchy() {
        addChild(pageController)
        view.addSubviews(pageController.view, footerView)
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        
        navigationItem.hidesBackButton = true
        navigationItem.rightBarButtonItem = closeBarButton
        setUpNavigationBarDefaultAppearance()
    }
    
    override func setupConstraints() {
        pageController.view.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top).inset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
        }
        
        footerView.snp.makeConstraints {
            $0.top.equalTo(pageController.view.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(view.compatibleSafeArea.bottom)
        }
    }
    
    @objc
    private func didTapClose() {
        interactor.close()
    }
}

// MARK: OnboardingDisplay
extension OnboardingViewController: OnboardingDisplay {
    func setupOnboarding(with panels: [UIViewController]) {
        onboardingControllers = panels
        if let firstController = onboardingControllers.first {
            pageController.setViewControllers([firstController], direction: .forward, animated: true)
        }
    }
    
    func setupFooter(for footer: OnboardingFooterViewModel) {
        footerView.setup(with: footer)
    }
    
    func goTo(step: Int, direction: UIPageViewController.NavigationDirection) {
        guard onboardingControllers.indices.contains(step) else {
            return
        }
        
        pageController.setViewControllers([onboardingControllers[step]], direction: direction, animated: true)
        interactor.scroll(to: step)
    }
}

extension OnboardingViewController: OnboardingFooterViewDelegate {
    func didTapAction() {
        interactor.action()
    }
}

extension OnboardingViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard completed,
            let nextController = nextPanel,
            let previousController = previousViewControllers.first,
            let index = onboardingControllers.firstIndex(of: nextController),
            nextController != previousController else {
            return
        }
            
        interactor.scroll(to: index)
        nextPanel = nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        guard let nextController = pendingViewControllers.first,
            onboardingControllers.contains(nextController) else {
                nextPanel = nil
                return
        }
        
        self.nextPanel = nextController
    }
}

extension OnboardingViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let index = onboardingControllers.firstIndex(of: viewController),
            onboardingControllers.indices.contains(index - 1) else {
                return nil
        }
        
        let indexBefore = onboardingControllers.index(before: index)
        return onboardingControllers[indexBefore]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let index = onboardingControllers.firstIndex(of: viewController),
            onboardingControllers.indices.contains(index + 1) else {
                return nil
        }

        let indexAfter = onboardingControllers.index(after: index)
        return onboardingControllers[indexAfter]
    }
}
