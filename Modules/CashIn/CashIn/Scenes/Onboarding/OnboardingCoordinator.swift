import UIKit

enum OnboardingAction: Equatable {
    case close
    case deeplink(url: URL)
}

protocol OnboardingCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: OnboardingAction)
}

final class OnboardingCoordinator {
    typealias Dependencies = HasDeeplink
    private let dependencies: Dependencies
    
    weak var viewController: UIViewController?
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - OnboardingCoordinating
extension OnboardingCoordinator: OnboardingCoordinating {
    func perform(action: OnboardingAction) {
        switch action {
        case .close:
            viewController?.dismiss(animated: true)
        case .deeplink(let url):
            dependencies.deeplink?.open(url: url)
        }
    }
}
