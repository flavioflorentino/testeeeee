import UI
import UIKit

final class OnboardingPanelViewController: UIViewController {
    private var descriptor: OnboardingPanelDescriptor
    private let panelView = OnboardingPanelView()
    
    init(descriptor: OnboardingPanelDescriptor) {
        self.descriptor = descriptor
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = panelView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        panelView.setup(with: descriptor)
    }
}
