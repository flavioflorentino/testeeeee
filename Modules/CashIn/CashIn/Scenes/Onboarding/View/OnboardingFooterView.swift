import SnapKit
import UI
import UIKit

struct OnboardingFooterViewModel {
    let numberOfPages: Int
    let currentPage: Int
    let actionButtonTitle: String
}

protocol OnboardingFooterViewDelegate: AnyObject {
    func didTapAction()
}

final class OnboardingFooterView: UIView, ViewConfiguration {
    private lazy var pageControl: UIPageControl = {
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = Colors.success500.color
        pageControl.pageIndicatorTintColor = Colors.branding100.color
        pageControl.backgroundColor = Colors.backgroundPrimary.color
        pageControl.isUserInteractionEnabled = false
        return pageControl
    }()
    
    private lazy var actionButton: UIButton = {
        let button = UIButton(type: .custom)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTapAction), for: .touchUpInside)
        return button
    }()
    
    weak var delegate: OnboardingFooterViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubviews(pageControl,
                    actionButton)
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
    
    func setupConstraints() {
        pageControl.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
        }
        
        actionButton.snp.makeConstraints {
            $0.top.equalTo(pageControl.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    @objc
    private func didTapAction() {
        delegate?.didTapAction()
    }
}

extension OnboardingFooterView {
    /// Populates the button titles in the view and the page control
    /// - Parameter footer: OnboardingFooter is an object containing all the information in the panel
    func setup(with footer: OnboardingFooterViewModel) {
        pageControl.numberOfPages = footer.numberOfPages
        pageControl.currentPage = footer.currentPage
        pageControl.updateCurrentPageDisplay()
        
        actionButton.setTitle(footer.actionButtonTitle, for: .normal)
    }
}
