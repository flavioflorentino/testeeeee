import SnapKit
import UI
import UIKit

private extension OnboardingPanelView.Layout {
    enum Color {
        static let text: UIColor = Colors.grayscale700.color
    }
    
    enum Spacing {
        static let verticalMargin: CGFloat = 24
        static let textMargin: CGFloat = 40
    }
}

final class OnboardingPanelView: UIView, ViewConfiguration {
    fileprivate enum Layout { }
    
    private lazy var illustrationImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.setContentHuggingPriority(.defaultLow, for: .vertical)
        return imageView
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .large))
            .with(\.textAlignment, .center)
            .with(\.textColor, .grayscale700())
        label.setContentCompressionResistancePriority(.required, for: .vertical)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textAlignment, .center)
            .with(\.textColor, .grayscale700())
        label.setContentCompressionResistancePriority(.required, for: .vertical)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubviews(illustrationImageView,
                    titleLabel,
                    descriptionLabel)
    }
    
    func setupConstraints() {
        illustrationImageView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Layout.Spacing.verticalMargin)
            $0.centerX.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(illustrationImageView.snp.bottom).offset(Layout.Spacing.verticalMargin)
            $0.leading.equalToSuperview().offset(Layout.Spacing.textMargin)
            $0.trailing.equalToSuperview().offset(-Layout.Spacing.textMargin)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Layout.Spacing.verticalMargin)
            $0.leading.equalToSuperview().offset(Layout.Spacing.textMargin)
            $0.trailing.equalToSuperview().offset(-Layout.Spacing.textMargin)
            $0.bottom.lessThanOrEqualToSuperview()
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
}

extension OnboardingPanelView {
    /// Populates the texts and images in the view
    /// - Parameter panel: OnboardingPanelDescriptor is an object containing all the information in the panel
    func setup(with panel: OnboardingPanelDescriptor) {
        illustrationImageView.image = panel.ilustration
        titleLabel.text = panel.title
        descriptionLabel.text = panel.description
    }
}
