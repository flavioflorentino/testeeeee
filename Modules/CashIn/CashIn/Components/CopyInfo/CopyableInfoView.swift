import SnapKit
import UI
import UIKit

protocol CopyableDelegate: AnyObject {
    func didCopy(info: String?, title: String?)
}

extension CopyableDelegate {
    func didCopy(info: String?, title: String?) { }
}

protocol CopyableInfoViewProtocol {
    var title: String? { get }
    var info: String? { get }

    func setUp(title: String, info: String)
    func copyInfo()
}

final class CopyableInfoView: UIView, ViewConfiguration {
    fileprivate enum Layout { }

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale700())
        return label
    }()

    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .left)

        label.setContentHuggingPriority(.required, for: .horizontal)
        return label
    }()

    private lazy var copyButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Strings.Global.Copyable.copy, for: .normal)
        button.addTarget(self, action: #selector(copyInfo), for: .touchUpInside)
        button.buttonStyle(LinkButtonStyle())
        return button
    }()
    
    private lazy var copiedButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Strings.Global.Copyable.copied, for: .normal)
        button.addTarget(self, action: #selector(copyInfo), for: .touchUpInside)
        button.buttonStyle(LinkButtonStyle())
            .with(\.textAttributedColor, (color: .neutral400(), state: .normal))
            .with(\.textAttributedColor, (color: .neutral400(), state: .highlighted))

        return button
    }()

    private var copied: Bool = false {
        didSet {
            updateCopyButton()
            if copied { didCopy() }
        }
    }
    
    private var revertCopyStateWorkItem: DispatchWorkItem?

    private weak var delegate: CopyableDelegate?

    init(delegate: CopyableDelegate?) {
        self.delegate = delegate
        super.init(frame: .zero)
        buildLayout()
    }

    override init(frame: CGRect) {
        self.delegate = nil
        super.init(frame: frame)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func buildViewHierarchy() {
        addSubviews(titleLabel, infoLabel, copyButton, copiedButton)
    }

    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.leading.bottom.equalToSuperview()
        }

        infoLabel.snp.makeConstraints {
            $0.leading.equalTo(titleLabel.snp.trailing).offset(Spacing.base00)
            $0.top.bottom.equalToSuperview()
        }

        copyButton.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base01)
            $0.leading.greaterThanOrEqualTo(infoLabel.snp.trailing).offset(Spacing.base00)
            $0.trailing.equalToSuperview()
        }
        
        copiedButton.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base01)
            $0.leading.greaterThanOrEqualTo(infoLabel.snp.trailing).offset(Spacing.base00)
            $0.trailing.equalToSuperview()
        }
    }

    @objc
    func copyInfo() {
        let cleanString = infoLabel.text?.byRemovingPunctuation
        UIPasteboard.general.string = cleanString

        if copied, let dispatchWorkItem = revertCopyStateWorkItem {
            dispatchWorkItem.cancel()
        }
        let dispatchWorkItem = DispatchWorkItem { [weak self] in
            self?.copied = false
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: dispatchWorkItem)
        revertCopyStateWorkItem = dispatchWorkItem
        copied = true
    }

    private func updateCopyButton() {
        UIView.animate(withDuration: 0.15,
                       animations: {
            self.copyButton.alpha = 0
            self.copiedButton.alpha = 0
        }, completion: { _ in
            UIView.animate(withDuration: 0.15) {
                if self.copied {
                    self.copiedButton.alpha = 1
                } else {
                    self.copyButton.alpha = 1
                }
            }
        })
    }

    private func didCopy() {
        delegate?.didCopy(info: UIPasteboard.general.string, title: titleLabel.text)
    }
}

extension CopyableInfoView: CopyableInfoViewProtocol {
    var title: String? {
        titleLabel.text
    }

    var info: String? {
        infoLabel.text
    }

    func setUp(title: String, info: String) {
        titleLabel.text = title
        infoLabel.text = info
        copied = false
    }
}
