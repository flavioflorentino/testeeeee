import SnapKit
import SkeletonView
import UI
import UIKit

// MARK: - Layout
private extension CashInMethodsSkeletonView.Layout {
    enum Size {
        static let headerImage: CGFloat = 120
        static let line: CGFloat = 18.0
    }
}

final class CashInMethodsSkeletonView: UIView, ViewConfiguration, StatefulViewing {
    fileprivate enum Layout { }
    
    var viewModel: StatefulViewModeling?
    weak var delegate: StatefulDelegate?
    
    private lazy var headerImageView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = Layout.Size.headerImage / 2
        view.clipsToBounds = true
        view.isSkeletonable = true
        return view
    }()
    
    private lazy var sectionHeaderView: UIView = {
        let view = UIView()
        view.cornerRadius = .light
        view.clipsToBounds = true
        view.isSkeletonable = true
        return view
    }()
    
    private lazy var cellsContainer: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            CashInMethodsSkeletonCell(),
            CashInMethodsSkeletonCell()
        ])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        buildLayout()
    }
    
    func buildViewHierarchy() {
        addSubview(headerImageView)
        addSubview(sectionHeaderView)
        addSubview(cellsContainer)
    }
    
    func setupConstraints() {
        headerImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.headerImage)
            $0.top.equalTo(compatibleSafeArea.top).inset(Spacing.base03)
            $0.centerX.equalToSuperview()
        }
        
        sectionHeaderView.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.line)
            $0.top.equalTo(headerImageView.snp.bottom).offset(Spacing.base04)
            $0.leading.equalToSuperview().inset(Spacing.base02)
            $0.trailing.equalTo(snp.centerX).offset(-Spacing.base08)
        }
        
        cellsContainer.snp.makeConstraints {
            $0.top.equalTo(sectionHeaderView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureViews() {
        viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let gradient = SkeletonGradient(baseColor: Colors.grayscale200.color)
        showAnimatedGradientSkeleton(usingGradient: gradient)
    }
}
