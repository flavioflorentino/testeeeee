import SnapKit
import SkeletonView
import UI
import UIKit

private extension CashInMethodsSkeletonCell.Layout {
    enum Size {
        static let container: CGFloat = 96.0
        static let icon: CGFloat = 48.0
        static let line: CGFloat = 18.0
    }
    
    enum CornerRadius {
        static let icon: CGFloat = Size.icon / 2.0
    }
}

final class CashInMethodsSkeletonCell: UIView, ViewConfiguration {
    fileprivate enum Layout { }
    
    private lazy var iconView: UIView = {
        let view = UIImageView()
        view.imageStyle(AvatarImageStyle(size: .medium))
        view.isSkeletonable = true
        return view
    }()
    
    private lazy var firstLineView = lineView
    private lazy var secondLineView = lineView
    private lazy var thirdLineView = lineView
    
    private var lineView: UIView {
        let view = UIView()
        view.cornerRadius = .light
        view.clipsToBounds = true
        view.isSkeletonable = true
        return view
    }
    
    // MARK: - Initialization
    convenience init() {
        self.init(frame: .zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        buildLayout()
    }
    
    func buildViewHierarchy() {
        addSubview(iconView)
        addSubview(firstLineView)
        addSubview(secondLineView)
        addSubview(thirdLineView)
    }
    
    func setupConstraints() {
        iconView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.icon)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.centerY.equalToSuperview()
        }
        
        firstLineView.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.line)
            $0.top.equalToSuperview().inset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02 * 2)
            $0.leading.equalTo(iconView.snp.trailing).offset(Spacing.base02)
        }
        
        secondLineView.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.line)
            $0.top.equalTo(firstLineView.snp.bottom).offset(Spacing.base01)
            $0.trailing.equalToSuperview().inset(Spacing.base08 * 2)
            $0.leading.equalTo(firstLineView)
        }
        
        thirdLineView.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.line)
            $0.top.equalTo(secondLineView.snp.bottom).offset(Spacing.base01)
            $0.trailing.equalToSuperview().inset(Spacing.base04 * 2)
            $0.leading.equalTo(firstLineView)
            $0.bottom.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureViews() {
        cornerRadius = .medium
        backgroundColor = Colors.groupedBackgroundPrimary.color
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let gradient = SkeletonGradient(baseColor: Colors.grayscale200.color)
        showAnimatedGradientSkeleton(usingGradient: gradient)
    }
}
