import SnapKit
import UI
import UIKit

public final class EmptyStateView: UIView, ViewConfiguration, StatefulViewing {
    public weak var delegate: StatefulDelegate?
    
    public var viewModel: StatefulViewModeling? {
        didSet {
            updateView()
        }
    }
    
    private lazy var imageView = UIImageView()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textAlignment, .center)
            .with(\.textColor, Colors.grayscale600.color)
        return label
    }()
    
    private lazy var tryAgainButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(tryAgain), for: .touchUpInside)
        return button
    }()
    
    public func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }

    public func buildViewHierarchy() {
        addSubview(imageView)
        addSubview(titleLabel)
        addSubview(descriptionLabel)
        addSubview(tryAgainButton)
    }
    
    public func setupConstraints() {
        imageView.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.bottom.equalTo(titleLabel.snp.top).offset(-Spacing.base02)
        }
        
        titleLabel.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(descriptionLabel.snp.top).offset(-Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.top.equalTo(self.snp.centerY).offset(Spacing.base01)
        }
        
        tryAgainButton.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func updateView() {
        imageView.image = viewModel?.image
        titleLabel.text = viewModel?.content?.title
        descriptionLabel.text = viewModel?.content?.description
        tryAgainButton.setTitle(viewModel?.button?.title, for: .normal)
    }
    
    @objc
    private func tryAgain() {
        delegate?.didTryAgain()
    }
}
