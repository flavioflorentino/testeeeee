import SnapKit
import UI
import UIKit

protocol CashInMethodsTableViewFooterDelegate: AnyObject {
    func didTapLink()
}

final class CashInMethodsTableViewFooter: UIView, ViewConfiguration {
    private lazy var linkButton = UIButton()
    
    weak var delegate: CashInMethodsTableViewFooterDelegate?
    
    func buildViewHierarchy() {
        addSubview(linkButton)
    }
    
    func setupConstraints() {
        linkButton.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.top.bottom.greaterThanOrEqualToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureViews() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(didTapLink))
        linkButton.addGestureRecognizer(gesture)
    }
    
    @objc
    private func didTapLink() {
        delegate?.didTapLink()
    }
    
    func setUp(text: String, delegate: CashInMethodsTableViewFooterDelegate?) {
        linkButton.setTitle(text, for: .normal)
        linkButton.buttonStyle(LinkButtonStyle(size: .small))
        self.delegate = delegate
    }
}
