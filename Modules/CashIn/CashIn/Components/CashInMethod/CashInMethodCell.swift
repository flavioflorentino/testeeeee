import SnapKit
import UI
import UIKit

final class CashInMethodCell: UITableViewCell, ViewConfiguration {
    private lazy var methodView = CashInMethodView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubview(methodView)
    }
    
    func setupConstraints() {
        methodView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.top.bottom.equalToSuperview().inset(Spacing.base00)
        }
    }
    
    func configureViews() {
        selectionStyle = .none
        backgroundColor = .clear
    }
    
    func setUp(with method: CashInMethodCard) {
        var descriptor: AvailabilityDescriptor?
        if let availability = method.status {
            descriptor = AvailabilityDescriptorFactory.make(for: availability)
        }
        
        methodView.setUp(icon: method.iconUrl,
                         title: method.name,
                         message: method.description,
                         descriptor: descriptor)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        methodView.removeAvailabilityView()
    }
}
