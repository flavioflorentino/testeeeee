import SnapKit
import UI
import UIKit

final class CashInMethodsTableSectionHeader: UIView, ViewConfiguration {
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale750())
        return label
    }()
    
    func buildViewHierarchy() {
        addSubview(titleLabel)
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base01)
        }
    }
    
    func display(title: String) {
        titleLabel.text = title
    }
}
