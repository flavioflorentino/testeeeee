import UIKit
import UI

final class CashInMethodView: UIView, ViewConfiguration {
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()

    private lazy var arrowImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.forwardArrow.image
        return imageView
    }()

    private lazy var infoStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Spacing.base01
        stackView.axis = .vertical
        stackView.alignment = .leading
        stackView.contentMode = .left
        return stackView
    }()

    private lazy var availabilityView: AvailabilityView = {
        let view = AvailabilityView()
        view.buildLayout()
        return view
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
        return label
    }()

    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .default))
            .with(\.textColor, Colors.grayscale500.color)
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func buildViewHierarchy() {
        addSubview(iconImageView)
        addSubview(infoStackView)
        addSubview(arrowImageView)
        infoStackView.addArrangedSubview(titleLabel)
        infoStackView.addArrangedSubview(messageLabel)
    }

    func setupConstraints() {
        iconImageView.setContentHuggingPriority(.required, for: .horizontal)
        iconImageView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.width.height.equalTo(Spacing.base06)
            $0.leading.equalToSuperview().inset(Spacing.base02)
        }

        infoStackView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base02)
            $0.leading.equalTo(iconImageView.snp.trailing).offset(Spacing.base01)
        }

        arrowImageView.setContentHuggingPriority(.required, for: .horizontal)
        arrowImageView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalTo(infoStackView.snp.trailing).offset(Spacing.base01)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }

    func configureStyles() {
        viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        cornerRadius = .medium
        border = .light(color: .grayscale100())
    }

    func setUp(icon: URL?, title: String, message: String, descriptor: AvailabilityDescriptor?) {
        iconImageView.setImage(url: icon)
        titleLabel.text = title
        messageLabel.text = message

        if let descriptor = descriptor {
            availabilityView.setUp(with: descriptor)
            infoStackView.insertArrangedSubview(availabilityView, at: 0)
        }
    }

    func removeAvailabilityView() {
        availabilityView.removeFromSuperview()
    }
}
