import SnapKit
import UI
import UIKit

final class CashInMethodsTableViewHeader: UIView, ViewConfiguration {
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    func buildViewHierarchy() {
        addSubview(imageView)
    }
    
    func setupConstraints() {
        imageView.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.bottom.equalToSuperview().offset(-Spacing.base00)
        }
    }
    
    func display(image: UIImage) {
        imageView.image = image
    }
}
