import UI
import UIKit

struct AvailabilityUnavailableDescriptor: AvailabilityDescriptor {
    var message: String = Strings.Global.Availability.unavailable
    var textColor: UIColor = Colors.white.color
    var backgroundColor: UIColor = Colors.grayscale500.color
}
