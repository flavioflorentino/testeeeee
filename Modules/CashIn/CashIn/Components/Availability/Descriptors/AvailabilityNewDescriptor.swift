import UI
import UIKit

struct AvailabilityNewDescriptor: AvailabilityDescriptor {
    var message: String = Strings.Global.Availability.new
    var textColor: UIColor = Colors.white.lightColor
    var backgroundColor: UIColor = Colors.neutral600.color
}
