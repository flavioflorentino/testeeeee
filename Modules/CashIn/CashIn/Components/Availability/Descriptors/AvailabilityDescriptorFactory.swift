enum AvailabilityDescriptorFactory {
    static func make(for availability: Availability) -> AvailabilityDescriptor {
        switch availability {
        case .new:
            return AvailabilityNewDescriptor()
        case .unavailable:
            return AvailabilityUnavailableDescriptor()
        }
    }
}
