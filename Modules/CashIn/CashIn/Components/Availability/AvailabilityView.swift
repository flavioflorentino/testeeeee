import UIKit
import UI

protocol AvailabilityDescriptor {
    var message: String { get }
    var textColor: UIColor { get }
    var backgroundColor: UIColor { get }
}

final class AvailabilityView: UIView, ViewConfiguration {
    private lazy var textLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .default))
        return label
    }()

    func buildViewHierarchy() {
        addSubview(textLabel)
    }

    func configureStyles() {
        cornerRadius = .light
    }

    func setupConstraints() {
        setContentHuggingPriority(.required, for: .horizontal)
        textLabel.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base01)
        }
    }

    func setUp(with descriptor: AvailabilityDescriptor) {
        backgroundColor = descriptor.backgroundColor
        textLabel.textColor = descriptor.textColor
        textLabel.text = descriptor.message
    }
}
