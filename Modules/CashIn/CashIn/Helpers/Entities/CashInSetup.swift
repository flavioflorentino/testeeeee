import Core

public protocol CashInSettable: AnyObject {
    var deeplink: DeeplinkContract? { get set }
}

public final class CashInSetup: CashInSettable {
    public static let shared = CashInSetup()
    
    public var deeplink: DeeplinkContract?
    
    private init() {}
    
    public func inject(instance: Any) {
        switch instance {
        case let instance as DeeplinkContract:
            deeplink = instance
        default:
            assertionFailure("🚨\nAttempted to inject unexpected depedency: \(instance)\n")
        }
    }
}
