import Core

protocol HasLegacy {
    var legacy: CashInLegacySettable { get }
}

protocol HasDeeplink {
    var deeplink: DeeplinkContract? { get }
}
