import Core
import UI
import UIKit

public protocol CashInLegacySettable: AnyObject {
    var methods: CashInMethodsContract? { get set }
}

public final class  CashInLegacySetup: CashInLegacySettable {
    public static let shared = CashInLegacySetup()
    
    public var methods: CashInMethodsContract?
    
    private init() {}
    
    public func inject(instance: Any) {
        switch instance {
        case let instance as CashInMethodsContract:
            methods = instance
        default:
            assertionFailure("🚨\nAttempted to inject unexpected depedency: \(instance)\n")
        }
    }
}
