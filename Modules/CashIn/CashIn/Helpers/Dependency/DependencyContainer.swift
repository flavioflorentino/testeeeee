import AnalyticsModule
import Core
import FeatureFlag
import Foundation

typealias CashInDependencies = HasMainQueue &
    HasFeatureManager &
    HasAnalytics &
    HasKVStore &
    HasLegacy &
    HasDeeplink

final class DependencyContainer: CashInDependencies {
    lazy var mainQueue = DispatchQueue.main
    lazy var featureManager: FeatureManagerContract = FeatureManager.shared
    lazy var analytics: AnalyticsProtocol = Analytics.shared
    lazy var kvStore: KVStoreContract = KVStore()
    lazy var legacy: CashInLegacySettable = CashInLegacySetup.shared
    lazy var deeplink: DeeplinkContract? = CashInSetup.shared.deeplink
}
