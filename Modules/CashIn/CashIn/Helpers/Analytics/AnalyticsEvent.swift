import AnalyticsModule
import Foundation

enum CashInEvent: AnalyticsKeyProtocol {
    case screenViewed(name: Name, context: Context, action: Action?)
    case buttonClicked(buttonName: Name, screenName: Name, context: Context)
    
    private var name: String {
        switch self {
        case .screenViewed:
            return "Screen Viewed"
            
        case .buttonClicked:
            return "Button Clicked"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .screenViewed(name, context, action):
            var properties: [String: Any] = [
                KeyStrings.screenName: name,
                KeyStrings.context: context
            ]
            
            if let method = action {
                properties[KeyStrings.method] = method
            }
            
            return properties
            
        case let .buttonClicked(buttonName, screenName, context):
            return [
                KeyStrings.buttonName: buttonName,
                KeyStrings.screenName: screenName,
                KeyStrings.context: context
            ]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.mixPanel, .appsFlyer, .firebase])
    }
}

extension CashInEvent {
    private enum KeyStrings {
        static let screenName = "screen_name"
        static let buttonName = "button_name"
        static let context = "business_context"
        static let method = "method_call"
    }
    
    enum Name: String, CustomStringConvertible {
        case cashIn = "CASH_IN"
        case addMoney = "ADICIONE_DINHEIRO"
        case virtualDebitCard = "CARTAO_DEBITO_VIRTUAL"
        case wireTransfer = "TRANSFERENCIA_BANCARIA"
        case boleto = "BOLETO"
        case pix = "PIX"
        case loan = "EMPRESTIMO_PESSOAL"
        case p2pLending = "P2P_LENDING"
        case learnMore = "SAIBA_MAIS"
        case underConstruction = "ADICIONE_DINHEIRO_ERRO_INDISPONIVEL"
        case help = "AJUDA"
        
        var description: String {
            rawValue
        }
    }
    
    enum Context: String, CustomStringConvertible {
        case cashIn = "CASH_IN"
        
        var description: String {
            rawValue
        }
    }
    
    enum Action: String, CustomStringConvertible {
        case tap = "CLICK"
        case deeplink = "DEEPLINK"
        
        var description: String {
            rawValue
        }
    }
}
