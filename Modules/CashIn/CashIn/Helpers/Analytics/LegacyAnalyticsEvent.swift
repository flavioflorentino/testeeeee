import AnalyticsModule
import Foundation

enum CashInLegacyEvent: AnalyticsKeyProtocol {
    case recharge(_ name: CashInName)
    case wireTransfer(_ name: CashInName)
    case onboarding(_ panel: OnboardingPanel)
    case faqDeeplink(_ name: CashInName)
    
    private var name: String {
        switch self {
        case .recharge, .wireTransfer:
            return "Visualizacao de tela"

        case .onboarding:
            return "Onboarding Cash-In - Carousel Caixa Viewed"

        case .faqDeeplink:
            return "Cashin - tutorial"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .recharge(name),
             let .wireTransfer(name):
            return [KeyStrings.name: name.description]

        case let .onboarding(page):
            return [KeyStrings.page: page.description]
            
        case let .faqDeeplink(name):
            return [KeyStrings.faq: name.description]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties)
    }
}

extension CashInLegacyEvent {
    private enum KeyStrings {
        static let name = "nome"
        static let type = "type"
        static let page = "Page"
        static let faq = "FAQ"
    }
    
    enum CashInName: String, CustomStringConvertible {
        case cashIn = "CASH-IN"
        
        case picpayAccount = "Picpay Account - viewed"
        case howToAddMoney = "Picpay Account - tutorial"
        case moreAboutWireTransfer = "Saiba mais sobre transferência"
        
        var description: String {
            rawValue
        }
    }
}
