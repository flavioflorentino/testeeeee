import Foundation
import Core

public struct CashInDeeplinkResolver: DeeplinkResolver {
    public init() { }

    public func canHandle(url: URL, isAuthenticated: Bool) -> DeeplinkResolverResult {
        guard CashInDeeplinkType(url: url) != nil else {
            return .notHandleable
        }
        guard isAuthenticated else {
            return .onlyWithAuth
        }
        return .handleable
    }

    public func open(url: URL, isAuthenticated: Bool) -> Bool {
        guard let deeplinkType = CashInDeeplinkType(url: url),
              canHandle(url: url, isAuthenticated: isAuthenticated) == .handleable else {
            return false
        }
        return open(deeplink: deeplinkType)
    }
}

extension CashInDeeplinkResolver {
    private func open(deeplink: CashInDeeplinkType) -> Bool {
        switch deeplink {
        case .receipt:
            return false
        }
    }
}
