import Foundation

enum CashInDeeplinkType {
    static let cashInPath = "cashin"
    
    case receipt
    
    init?(url: URL) {
        guard CashInDeeplinkType.cashInPath == url.pathComponents.first(where: { $0 != "/" }) else {
            return nil
        }
        
        let params = url.pathComponents.filter { $0 != "/" && $0 != CashInDeeplinkType.cashInPath }
        
        guard let path = params.first else {
            return nil
        }
        
        switch path {
        case "wiretransfer":
            self = .receipt
        default:
            return nil
        }
    }
}
