import Foundation

// swiftlint:disable convenience_type
final class CashInResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: CashInResources.self).url(forResource: "CashInResources", withExtension: "bundle") else {
            return Bundle(for: CashInResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: CashInResources.self)
    }()
}
