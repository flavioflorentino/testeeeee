@testable import CashIn

enum CashInMethodsMock: String, JsonLoadable {
    case categories = "cashInMethods"
    
    func getMock() -> [CashInCategory]? {
        decodeFile(named: rawValue) as [CashInCategory]?
    }
}
