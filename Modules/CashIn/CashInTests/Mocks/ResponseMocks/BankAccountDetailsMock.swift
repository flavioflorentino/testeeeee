import AssetsKit
@testable import CashIn

enum BankAccountDetailsMock {
    static var bankingInfo: BankingInfo {
        BankingInfo(bankId: "111",
                    accountAgency: "11111",
                    accountNumber: "1111-1",
                    consumerUsername: "magpali",
                    consumerName: "Victor Magpali",
                    consumerCpf: "111.111.111-11",
                    consumerAvatar: nil)
    }
    
    static var accountInfoMock: BankAccountDetails {
        let userInfo = UserInfo(avatar: nil,
                                avatarPlaceholder: Resources.Placeholders.greenAvatarPlaceholder.image,
                                username: "magpali",
                                fullname: "Victor Magpali")
        
        let bankInfo = BankAccountInfo(bankNumber: "111",
                                       agencyNumber: "11111",
                                       accountNumber: "1111-1",
                                       document: "111.111.111-11")
        
        return BankAccountDetails(userInfo: userInfo, bankInfo: bankInfo)
    }
}
