@testable import CashIn

enum CashInErrorMock: String, JsonLoadable {
    case cashIn = "securityRiskError_cashin"
    case home = "securityRiskError_home"
    case identityAnalysis = "securityRiskError_identityAnalysis"
    case unknown = "securityRiskError_unknown"
    
    func getMock() -> CashInErrorModel? {
        decodeFile(named: rawValue) as CashInErrorModel?
    }
}
