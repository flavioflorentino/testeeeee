import AnalyticsModule
import Core
import FeatureFlag
@testable import CashIn
import XCTest

final class DependencyContainerMock: CashInDependencies {
    lazy var mainQueue: DispatchQueue = resolve()
    lazy var analytics: AnalyticsProtocol = resolve()
    lazy var legacy: CashInLegacySettable = resolve()
    lazy var featureManager: FeatureManagerContract = resolve()
    lazy var kvStore: KVStoreContract = resolve()
    lazy var deeplink: DeeplinkContract? = resolve()

    private let dependencies: [Any]

    init(_ dependencies: Any...) {
        self.dependencies = dependencies
    }
}

extension DependencyContainerMock {
    func resolve<T>() -> T {
        let resolved = dependencies.lazy.compactMap { $0 as? T }

        switch resolved.first {
        case .none:
            fatalError("DependencyContainerMock could not resolve dependency: \(T.self)\n")
        case .some where resolved.count > 1:
            fatalError("DependencyContainerMock resolved mutiple dependencies for: \(T.self)\n")
        case .some(let mock):
            return mock
        }
    }

    func resolve() -> DispatchQueue {
        let resolved = dependencies.lazy.compactMap { $0 as? DispatchQueue }.first
        return resolved ?? DispatchQueue(label: "DependencyContainerMock")
    }
}
