@testable import CashIn
import Foundation

protocol JsonLoadable {
    func decodeFile<T:Decodable>(named filename: String) -> T?
}

extension JsonLoadable {
    private func readLocalFile(named filename: String) -> Data? {
        if let bundlePath = Bundle(for: PathRetainer.self).path(forResource: filename,
                                                                ofType: "json"),
         let jsonData = try? String(contentsOfFile: bundlePath).data(using: .utf8) {
        return jsonData
      }
      
      return nil
   }
    
    func decodeFile<T:Decodable>(named filename: String) -> T? {
        let decoder = JSONDecoder()
        
        guard let data = readLocalFile(named: filename),
              let decodedData = try? decoder.decode(T.self, from: data) else {
            return nil
        }
        
        return decodedData
    }
}

private final class PathRetainer { }
