@testable import CashIn
import XCTest

final class TutorialFactoryTests: XCTestCase {
    private typealias Sut = TutorialFactory

    func testFactoryMake_wheneverCalled_shouldCreateATutorialViewController() {
        XCTAssertTrue(Sut.make(for: .account, delegate: nil) is TutorialViewController)
    }
}
