@testable import CashIn
import Foundation
import XCTest

final class TutorialDisplayableSpy: TutorialDisplaying {
    private(set) var setDescriptors = [TutorialDescriptor]()
    func setUp(with descriptor: TutorialDescriptor) {
        setDescriptors.append(descriptor)
    }
}

final class TutorialCoordinatorSpy: TutorialCoordinating {
    var delegate: TutorialCoordinatorDelegate?
    var viewController: UIViewController?
    
    private(set) var performedActions = [TutorialAction]()
    func perform(action: TutorialAction) {
        performedActions.append(action)
    }
}

final class TutorialPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = TutorialCoordinatorSpy()
    private lazy var viewControllerSpy = TutorialDisplayableSpy()
    private lazy var sut: TutorialPresenter = {
        let presenter = TutorialPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testDisplay_ShouldInvokeViewControllerSetUpWithCorrectDescriptor() {
        sut.display(.account)
        sut.display(.boleto)

        XCTAssertTrue(viewControllerSpy.setDescriptors[0] is AccountTutorialDescriptor)
        XCTAssertTrue(viewControllerSpy.setDescriptors[1] is BoletoTutorialDescriptor)
        XCTAssertEqual(viewControllerSpy.setDescriptors.count, 2)
    }
    
    func testDidNextStep_ShouldInvokeCoordinatorPerform() throws {
        let validUrl = try XCTUnwrap(URL(string: "www.picpay.com"))
        let validPerformedActions: [TutorialAction] = [.didNextStep(context: .account),
                                                       .didNextStep(context: .boleto),
                                                       .deeplink(url: validUrl)]
        
        sut.didNextStep(action: validPerformedActions[0])
        sut.didNextStep(action: validPerformedActions[1])
        sut.didNextStep(action: validPerformedActions[2])
        
        XCTAssertEqual(coordinatorSpy.performedActions, validPerformedActions)
    }
}
