@testable import CashIn
import XCTest

final class TutorialDescriptorTests: XCTestCase {
    private typealias AccountLocalizable = Strings.CashInMethods.BankAccount.Tutorial
    private typealias BoletoLocalizable = Strings.CashInMethods.Boleto.Tutorial
    
    func testDescriptorFactory_WheneverCalled_ShouldReturnCorrectDescriptor() {
        XCTAssertTrue(TutorialDescriptorFactory.make(for: .account) is AccountTutorialDescriptor)
        XCTAssertTrue(TutorialDescriptorFactory.make(for: .boleto) is BoletoTutorialDescriptor)
    }
    
    func testAccountDescriptor_WheneverAccessed_ShouldDeliverTheCorrectInformation() {
        let sut = AccountTutorialDescriptor()
        let validTitleText = AccountLocalizable.title
        let validDescription = AccountLocalizable.info.replacingOccurrences(of: "*", with: "")
        let validButtonTitle = AccountLocalizable.linkTitle
        
        
        XCTAssertEqual(sut.titleText, validTitleText)
        XCTAssertEqual(sut.descriptionText.string, validDescription)
        XCTAssertEqual(sut.linkTitle, validButtonTitle)
        XCTAssertNil(sut.buttonTitle)
    }
    
    func testBoletoDescriptor_WheneverAccessed_ShouldDeliverTheCorrectInformation() {
        let sut = BoletoTutorialDescriptor()
        let validTitleText = BoletoLocalizable.title
        let validDescription = BoletoLocalizable.info.replacingOccurrences(of: "*", with: "")
        let validButtonTitle = Strings.Global.continue
        
        
        XCTAssertEqual(sut.titleText, validTitleText)
        XCTAssertEqual(sut.descriptionText.string, validDescription)
        XCTAssertEqual(sut.buttonTitle, validButtonTitle)
        XCTAssertNil(sut.linkTitle)
    }
}
