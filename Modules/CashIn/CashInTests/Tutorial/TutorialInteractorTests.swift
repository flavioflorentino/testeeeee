@testable import CashIn
import AnalyticsModule
import FeatureFlag
import Foundation
import XCTest

final class TutorialPresenterSpy: TutorialPresenting {
    var viewController: TutorialDisplaying?
    
    private(set) var displayedContext: TutorialContext?
    private(set) var displayCount = 0
    func display(_ context: TutorialContext) {
        displayedContext = context
        displayCount += 1
    }
    
    private(set) var invokedActions = [TutorialAction]()
    func didNextStep(action: TutorialAction) {
        invokedActions.append(action)
    }
}

final class TutorialInteractorTests: XCTestCase {
    private lazy var analytics = AnalyticsSpy()
    private lazy var featureManager = FeatureManagerMock()
    private lazy var dependenciesMock = DependencyContainerMock(analytics, featureManager)
    private lazy var presenterSpy = TutorialPresenterSpy()

    func testLoadContext_WheneverCalled_ShouldInvokePresenterDisplay() {
        let sut = TutorialInteractor(presenter: presenterSpy,
                                     dependencies: dependenciesMock,
                                     tutorialContext: .account)
        
        sut.loadContext()
        
        XCTAssertEqual(presenterSpy.displayCount, 1)
        XCTAssertEqual(presenterSpy.displayedContext, .account)
    }
    
    func testSendEvent_WhenOnAccountContext_ShouldSendTheCorrectAnalyticsEvent() {
        let sut = TutorialInteractor(presenter: presenterSpy,
                                     dependencies: dependenciesMock,
                                     tutorialContext: .account)
        let validEvent = CashInLegacyEvent.wireTransfer(.howToAddMoney).event()
        
        sut.sendTutorialOpenedEvent()
        
        XCTAssertTrue(analytics.equals(to: validEvent))
    }
    
    func testSendEvent_WhenOnBoletoContext_ShouldSendTheCorrectAnalyticsEvent() {
        let sut = TutorialInteractor(presenter: presenterSpy,
                                     dependencies: dependenciesMock,
                                     tutorialContext: .boleto)
        let validEvent = CashInLegacyEvent.recharge(.howToAddMoney).event()
        
        sut.sendTutorialOpenedEvent()
        
        XCTAssertTrue(analytics.equals(to: validEvent))
    }
    
    func testButtonAction_WhenOnAccountContext_ShouldNotInvokePresenterNextStepAction() {
        let sut = TutorialInteractor(presenter: presenterSpy,
                                     dependencies: dependenciesMock,
                                     tutorialContext: .account)
        
        sut.buttonAction()
        
        XCTAssertEqual(presenterSpy.invokedActions, [])
    }
    
    func testButtonAction_WhenOnBoletoContext_ShouldInvokePresenterNextStepAction() {
        let sut = TutorialInteractor(presenter: presenterSpy,
                                     dependencies: dependenciesMock,
                                     tutorialContext: .boleto)
        
        sut.buttonAction()
        
        XCTAssertEqual(presenterSpy.invokedActions, [.didNextStep(context: .boleto)])
    }
    
    func testLinkAction_WhenOnAccountContext_ShouldInvokePresenterDeeplinkActionAndSendAnalyticsEvent() throws {
        let sut = TutorialInteractor(presenter: presenterSpy,
                                     dependencies: dependenciesMock,
                                     tutorialContext: .account)
        
        let validUrlString = "www.picpay.com"
        let validUrl = try XCTUnwrap(URL(string: validUrlString))
        let validEvent = CashInLegacyEvent.faqDeeplink(.howToAddMoney).event()
        featureManager.override(key: .opsCashInTutorialWireTransferString, with: validUrlString)
        
        sut.linkAction()
        
        XCTAssertEqual(presenterSpy.invokedActions, [.deeplink(url: validUrl)])
        XCTAssertTrue(analytics.equals(to: validEvent))
    }
    
    func testLinkAction_WhenOnBoletoContext_ShouldNotInvokePresenterDeeplinkActionAndSendAnalyticsEvent() {
        let sut = TutorialInteractor(presenter: presenterSpy,
                                     dependencies: dependenciesMock,
                                     tutorialContext: .boleto)
        
        sut.linkAction()
        
        XCTAssertEqual(presenterSpy.invokedActions, [])
        XCTAssertNil(analytics.event)
    }
}
