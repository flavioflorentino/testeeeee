import XCTest
@testable import CashIn

final class CashInErrorPresenterSpy: CashInErrorPresenting {
    var viewController: CashInErrorDisplaying?

    private(set) var displayedError: CashInErrorModel?
    private(set) var displayCount = 0
    func display(model: CashInErrorModel) {
        displayCount += 1
        displayedError = model
    }

    private(set) var didNextStepCount = 0
    private(set) var receivedAction: CashInErrorAction?
    func didNextStep(action: CashInErrorAction) {
        didNextStepCount += 1
        receivedAction = action
    }

    private(set) var presentLoadingCount = 0
    func presentLoading() {
        presentLoadingCount += 1
    }

    private(set) var hideLoadingCount = 0
    func hideLoading() {
        hideLoadingCount += 1
    }
}

final class CashInErrorInteractorTests: XCTestCase {
    private lazy var presenterSpy = CashInErrorPresenterSpy()
    
    func testLoadError_WhenDataIsValid_ShouldCallPresenterDisplay() throws {
        let errorMock = try XCTUnwrap(CashInErrorMock.home.getMock())
        let sut = CashInErrorInteractor(error: errorMock, presenter: presenterSpy)
        
        sut.loadError()

        XCTAssertEqual(presenterSpy.displayCount, 1)
        XCTAssertEqual(presenterSpy.displayedError, errorMock)
    }
    
    func testLoadError_WhenNoErrorsArePassed_ShouldCallPresenterDisplayWithDefaultError() throws {
        let errorMock = try XCTUnwrap(CashInErrorMock.unknown.getMock())
        let sut = CashInErrorInteractor(error: errorMock, presenter: presenterSpy)
        
        sut.loadError()

        XCTAssertEqual(presenterSpy.displayCount, 1)
        XCTAssertEqual(presenterSpy.displayedError, errorMock)
    }
    
    func testPrimaryAction_WhenErrorHasIdentityAnalysisButtonUrl_ShouldInvokeDeeplinkNextStep() throws {
        let errorMock = try XCTUnwrap(CashInErrorMock.identityAnalysis.getMock())
        let buttonUrl = try XCTUnwrap(URL(string: "picpay://picpay/identityanalysis"))
        
        let validAction = CashInErrorAction.openDeeplink(url: buttonUrl)
        let sut = CashInErrorInteractor(error: errorMock, presenter: presenterSpy)
        
        sut.primaryAction()
        
        XCTAssertEqual(presenterSpy.didNextStepCount, 1)
        XCTAssertEqual(presenterSpy.receivedAction, validAction)
    }
    
    func testPrimaryAction_WhenErrorHasHomeButtonUrl_ShouldInvokeHomeNextStep() throws {
        let errorMock = try XCTUnwrap(CashInErrorMock.home.getMock())
        let validAction = CashInErrorAction.home
        let sut = CashInErrorInteractor(error: errorMock, presenter: presenterSpy)
        
        sut.primaryAction()
        
        XCTAssertEqual(presenterSpy.didNextStepCount, 1)
        XCTAssertEqual(presenterSpy.receivedAction, validAction)
    }
    
    func testPrimaryAction_WhenErrorHasNoUrl_ShouldInvokeDismissNextStep() throws {
        let errorMock = try XCTUnwrap(CashInErrorMock.unknown.getMock())
        let validAction = CashInErrorAction.dismiss
        let sut = CashInErrorInteractor(error: errorMock, presenter: presenterSpy)
        
        sut.primaryAction()
        
        XCTAssertEqual(presenterSpy.didNextStepCount, 1)
        XCTAssertEqual(presenterSpy.receivedAction, validAction)
    }
    
    func testSecondaryAction_WhenErrorHasUnderlinedButtonUrl_ShouldInvokeDeeplinkNextStep() throws {
        let errorMock = try XCTUnwrap(CashInErrorMock.home.getMock())
        let buttonUrl = try XCTUnwrap(URL(string: errorMock.underlinedButtonUrl ?? ""))
        
        let validAction = CashInErrorAction.openDeeplink(url: buttonUrl)
        let sut = CashInErrorInteractor(error: errorMock, presenter: presenterSpy)
        
        sut.secondaryAction()

        XCTAssertEqual(presenterSpy.didNextStepCount, 1)
        XCTAssertEqual(presenterSpy.receivedAction, validAction)
    }
    
    func testSecondaryAction_WhenErrorHasNoUnderlinedButtonUrl_ShouldNotInvokeDeeplinkNextStep() throws {
        let errorMock = try XCTUnwrap(CashInErrorMock.unknown.getMock())
        
        let sut = CashInErrorInteractor(error: errorMock, presenter: presenterSpy)
        
        sut.secondaryAction()

        XCTAssertEqual(presenterSpy.didNextStepCount, 0)
        XCTAssertNil(presenterSpy.receivedAction)
    }
    
    func testFooterAction_WhenErrorHasFooterButtonUrl_ShouldInvokeDeeplinkNextStep() throws {
        let errorMock = try XCTUnwrap(CashInErrorMock.home.getMock())
        let buttonUrl = try XCTUnwrap(URL(string: errorMock.footerButtonUrl ?? ""))
        
        let validAction = CashInErrorAction.openDeeplink(url: buttonUrl)
        let sut = CashInErrorInteractor(error: errorMock, presenter: presenterSpy)
        
        sut.footerAction()

        XCTAssertEqual(presenterSpy.didNextStepCount, 1)
        XCTAssertEqual(presenterSpy.receivedAction, validAction)
    }
    
    func testFooterAction_WhenErrorHasNoFooterButtonUrl_ShouldNotInvokeDeeplinkNextStep() throws {
        let errorMock = try XCTUnwrap(CashInErrorMock.unknown.getMock())
        
        let sut = CashInErrorInteractor(error: errorMock, presenter: presenterSpy)
        
        sut.footerAction()

        XCTAssertEqual(presenterSpy.didNextStepCount, 0)
        XCTAssertNil(presenterSpy.receivedAction)
    }
    
    func testClose_WheneverCalled_ShouldInvokePresenterDismissStep() throws {
        let errorMock = try XCTUnwrap(CashInErrorMock.unknown.getMock())
        
        let sut = CashInErrorInteractor(error: errorMock, presenter: presenterSpy)
        
        sut.close()

        XCTAssertEqual(presenterSpy.didNextStepCount, 1)
        XCTAssertEqual(presenterSpy.receivedAction, .dismiss)
    }
}
