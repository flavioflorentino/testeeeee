
import XCTest
@testable import CashIn

final class CashInErrorFactoryTests: XCTestCase {
    private typealias Sut = CashInErrorFactory

    func testFactoryMake_wheneverCalled_shouldCreateACashInMethodsViewController() {
        XCTAssertTrue(CashInErrorFactory.make(for: nil) is CashInErrorViewController)
    }
}
