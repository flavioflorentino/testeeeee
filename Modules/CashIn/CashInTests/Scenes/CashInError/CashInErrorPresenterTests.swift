import UI
import XCTest
@testable import CashIn

final class CashInErrorCoordinatorSpy: CashInErrorCoordinating {
    var viewController: UIViewController?
    
    private(set) var performedActions = [CashInErrorAction]()
    func perform(action: CashInErrorAction) {
        performedActions.append(action)
    }
}

final class CashInErrorViewControllerSpy: CashInErrorDisplaying {
    private(set) var setUpViewModel: CashInErrorViewModel?
    private(set) var setUpCount = 0
    func setUp(with viewModel: CashInErrorViewModel) {
        setUpCount += 1
        setUpViewModel = viewModel
    }
}

final class CashInErrorPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = CashInErrorCoordinatorSpy()
    private lazy var viewControllerSpy = CashInErrorViewControllerSpy()
    
    private lazy var sut: CashInErrorPresenter = {
        let presenter = CashInErrorPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testDisplay_WheneverCalled_ShouldInvokeViewControllerSetUp() throws {
        let errorMock = try XCTUnwrap(CashInErrorMock.home.getMock())
        let validDescription = try XCTUnwrap(errorMock.message.applyHtmlTags(with: Typography.bodyPrimary(.default).font(),
                                                                             textAlignment: .center))
        let validBottomText = try XCTUnwrap(errorMock.footerButtonText?.applyHtmlTags(with: Typography.caption().font(),
                                                                                     textAlignment: .center))
        
        sut.display(model: errorMock)
        let spiedViewModel = try XCTUnwrap(viewControllerSpy.setUpViewModel)
        
        XCTAssertEqual(spiedViewModel.imageURL, URL(string: errorMock.image))
        XCTAssertEqual(spiedViewModel.titleText, errorMock.title)
        XCTAssertEqual(spiedViewModel.descriptionText, validDescription)
        XCTAssertEqual(spiedViewModel.primaryButtonText, errorMock.buttonText)
        XCTAssertEqual(spiedViewModel.secondaryButtonText, errorMock.underlinedButtonText)
        XCTAssertEqual(spiedViewModel.isSecondaryButtonHidden, errorMock.underlinedButtonText == nil)
        XCTAssertEqual(spiedViewModel.bottomText, validBottomText)
        XCTAssertEqual(spiedViewModel.isBottomTextHidden, errorMock.footerButtonText == nil)
    }
    
    func testDidNextStep_WheneverCalled_ShouldInvokeCoordinatorPerform() throws {
        let url = try XCTUnwrap(URL(string: "picpay.com.br"))
        let validActions: [CashInErrorAction] = [.home, .openDeeplink(url: url), .dismiss]
        
        sut.didNextStep(action: .home)
        sut.didNextStep(action: .openDeeplink(url: url))
        sut.didNextStep(action: .dismiss)
        
        XCTAssertEqual(coordinatorSpy.performedActions, validActions)
    }
}
