@testable import CashIn
import AnalyticsModule
import FeatureFlag
import Foundation
import XCTest

final class OnboardingPresenterSpy: OnboardingPresenting {
    var viewController: OnboardingDisplay?

    private(set) var didNextStepAction: OnboardingAction?
    func didNextStep(action: OnboardingAction) {
        self.didNextStepAction = action
    }
    
    private(set) var panels: [OnboardingPanel] = []
    private(set) var displayCount = 0
    func display(panels: [OnboardingPanel]) {
        self.panels = panels
        displayCount += 1
    }
    
    private(set) var currentPage: Int = 0
    private(set) var displayFooterCallCount: Int = 0
    func displayFooter(currentPage: Int, numberOfPages: Int) {
        self.currentPage = currentPage
        displayFooterCallCount += 1
    }
    
    private(set) var didGoTo: Int = 0
    private(set) var goToCount = 0
    func goTo(step: Int) {
        goToCount += 1
        didGoTo = step
    }
}


final class OnboardingInteractorTest: XCTestCase {
    private let analytics = AnalyticsSpy()
    private let featureManager = FeatureManagerMock()
    private lazy var dependenciesMock = DependencyContainerMock(analytics, featureManager)
    private lazy var presenterSpy = OnboardingPresenterSpy()
    private lazy var sut = OnboardingInteractor(for: .wireTransfer, presenter: presenterSpy, dependencies: dependenciesMock)
    
    func testLoad_wheneverCalled_shouldInvokePresenterDisplays() throws {
        sut.load()
        
        let validPanels: [OnboardingPanel] = [.startUsing, .benefits, .enjoy]
        let validEvent = CashInLegacyEvent.onboarding(.startUsing)
        
        XCTAssertEqual(presenterSpy.panels, validPanels)
        XCTAssertEqual(presenterSpy.displayCount, 1)
        XCTAssertEqual(presenterSpy.displayFooterCallCount, 1)
        XCTAssertTrue(analytics.equals(to: validEvent.event()))
    }
    
    func testScroll_WhenScrollingToInvalidPage_ShouldNotInvokePresenterDisplayFooter() {
        sut.scroll(to: -5)
        XCTAssertEqual(presenterSpy.displayFooterCallCount, 0)
        
        sut.scroll(to: 15)
        XCTAssertEqual(presenterSpy.displayFooterCallCount, 0)
    }

    func testScroll_WhenScrollingToValidPage_ShouldInvokePresenterDisplayFooter() {
        sut.scroll(to: 2)
        XCTAssertEqual(presenterSpy.displayFooterCallCount, 1)
        
        sut.scroll(to: 1)
        XCTAssertEqual(presenterSpy.displayFooterCallCount, 2)
        
        sut.scroll(to: 0)
        XCTAssertEqual(presenterSpy.displayFooterCallCount, 3)
    }
    
    func testAction_whenNotOnLastPage_ShouldInvokePresenterGoTo() {
        sut.action()
        
        XCTAssertEqual(presenterSpy.didGoTo, 1)
        XCTAssertEqual(presenterSpy.goToCount, 1)
        
        sut.scroll(to: 1)
        sut.action()
        
        XCTAssertEqual(presenterSpy.didGoTo, 2)
        XCTAssertEqual(presenterSpy.goToCount, 2)
        
        sut.scroll(to: 0)
        sut.action()
        
        XCTAssertEqual(presenterSpy.didGoTo, 1)
        XCTAssertEqual(presenterSpy.goToCount, 3)
    }
    
    func testAction_whenOnLastPage_ShouldInvokePresenterClose() {
        sut.scroll(to: 2)
        sut.action()
        
        var assertion = false
        if case .close = presenterSpy.didNextStepAction {
            assertion = true
        }
        
        XCTAssertTrue(assertion)
    }
    
    func testClose_shouldCallNextStep() {
        sut.close()
        
        XCTAssertEqual(presenterSpy.didNextStepAction, .close)
    }

}
