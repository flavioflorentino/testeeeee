@testable import CashIn
import XCTest

final class OnboardingCoordinatorSpy: OnboardingCoordinating {
    var viewController: UIViewController?
    
    private(set) var didPerformAction: OnboardingAction? = nil
    private(set) var performCount: Int = 0
    func perform(action: OnboardingAction) {
        didPerformAction = action
        performCount += 1
    }
}

final class OnboardingViewControllerSpy: OnboardingDisplay {
    private(set) var setupOnboardingCount = 0
    func setupOnboarding(with panels: [UIViewController]) {
        setupOnboardingCount += 1
    }
    
    private(set) var didSetupFooter: OnboardingFooterViewModel? = nil
    private(set) var setupFooterCount = 0
    func setupFooter(for footer: OnboardingFooterViewModel) {
        didSetupFooter = footer
        setupFooterCount += 1
    }
    
    private(set) var didGoToStep = -1
    private(set) var goToStepCount = 0
    func goTo(step: Int, direction: UIPageViewController.NavigationDirection) {
        didGoToStep = step
        goToStepCount += 1
    }
}

final class OnboardingPresenterTest: XCTestCase {
    
    private lazy var coordinatorSpy = OnboardingCoordinatorSpy()
    private lazy var controllerSpy = OnboardingViewControllerSpy()
    private lazy var sut:  OnboardingPresenter = {
        let presenter = OnboardingPresenter(coordinator: coordinatorSpy)
        presenter.viewController = controllerSpy
        return presenter
    }()
    
    func test_didNextStep_WhenPassingAnyAction_ShouldInvokeCoordinatorPerform() throws {
        sut.didNextStep(action: .close)
        XCTAssertEqual(coordinatorSpy.performCount, 1)
        XCTAssertEqual(coordinatorSpy.didPerformAction, .close)
        
        let validUrl = try XCTUnwrap(URL(string: "www.picpay.com"))
        let action: OnboardingAction = .deeplink(url: validUrl)
        sut.didNextStep(action: action)
        
        XCTAssertEqual(coordinatorSpy.performCount, 2)
        XCTAssertEqual(coordinatorSpy.didPerformAction, action)
    }
    
    
    func test_display_WhenPassingInvalidArray_ShouldNotInvokeControllerSetupOnboarding() {
        sut.display(panels: [])
        
        XCTAssertEqual(controllerSpy.setupOnboardingCount, 0)
        
        sut.display(panels: [.bills])
        
        XCTAssertEqual(controllerSpy.setupOnboardingCount, 0)
    }
    
    func test_display_WhenPassingValidArray_ShouldInvokeControllerSetupOnboarding() {
        sut.display(panels: [.bills, .bills])
        
        XCTAssertEqual(controllerSpy.setupOnboardingCount, 1)
        
        sut.display(panels: [.bills, .bills, .bills])
        
        XCTAssertEqual(controllerSpy.setupOnboardingCount, 2)
        
        sut.display(panels: [.bills, .establishments, .phone, .bills, .phone, .establishments])
        
        XCTAssertEqual(controllerSpy.setupOnboardingCount, 3)
    }
    
    func test_goTo_WhenPassingAnyInt_ShouldInvokeControllerGoTo() {
        sut.goTo(step: 1)
        
        XCTAssertEqual(controllerSpy.didGoToStep, 1)
        XCTAssertEqual(controllerSpy.goToStepCount, 1)
        
        sut.goTo(step: 5)
        
        XCTAssertEqual(controllerSpy.didGoToStep, 5)
        XCTAssertEqual(controllerSpy.goToStepCount, 2)
        
        sut.goTo(step: 9999)
        
        XCTAssertEqual(controllerSpy.didGoToStep, 9999)
        XCTAssertEqual(controllerSpy.goToStepCount, 3)
    }
    
}
