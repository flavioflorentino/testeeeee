import XCTest
@testable import CashIn

final class CashInMethodsFactoryTests: XCTestCase {
    func testFactoryMake_wheneverCalled_shouldCreateACashInMethodsViewController() {
        XCTAssertTrue(CashInMethodsFactory.make() is CashInMethodsViewController)
    }
}
