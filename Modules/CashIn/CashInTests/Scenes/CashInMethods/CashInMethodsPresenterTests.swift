import AssetsKit
import UI
import XCTest
@testable import CashIn

private final class CashInMethodsDisplayableSpy: CashInMethodsDisplaying {
    private(set) var setTitleCount = 0
    private(set) var titleSet: String?
    private(set) var displaySectionsCount = 0
    private(set) var displayedSections = [CashInCategory]()
    private(set) var displayHeaderCount = 0
    private(set) var displayedHeader: UIImage?
    private(set) var displayFooterCount = 0
    private(set) var displayedFooter: String?
    private(set) var beginLoadingCount = 0
    private(set) var displayErrorCount = 0
    private(set) var displayedError: StatefulErrorViewModel?
    
    func setTitle(as titleString: String) {
        setTitleCount += 1
        titleSet = titleString
    }
    
    func display(_ sections: [CashInCategory]) {
        displaySectionsCount += 1
        displayedSections = sections
    }
    
    func display(headerImage: UIImage) {
        displayHeaderCount += 1
        displayedHeader = headerImage
    }
    
    func display(footerText: String) {
        displayFooterCount += 1
        displayedFooter = footerText
    }
    
    func beginLoading() {
        beginLoadingCount += 1
    }
    
    func displayError(with viewModel: StatefulErrorViewModel) {
        displayErrorCount += 1
        displayedError = viewModel
    }
}

private final class CashInMethodsCoordinatorSpy: CashInMethodsCoordinating {
    var viewController: UIViewController?
    
    private(set) var performedActions: [CashInMethodsAction] = []
    func perform(action: CashInMethodsAction) {
        performedActions.append(action)
    }
}

final class CashInMethodsPresenterTests: XCTestCase {
    private lazy var viewControllerSpy = CashInMethodsDisplayableSpy()
    private lazy var coordinatorSpy = CashInMethodsCoordinatorSpy()
    
    private lazy var sut: CashInMethodsPresenter = {
        let presenter = CashInMethodsPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testDisplaySections_wheneverCalled_shouldInvokeControllerDisplayMethods() throws {
        let categories = try XCTUnwrap(CashInMethodsMock.categories.getMock())
        sut.display(categories)
        
        XCTAssertEqual(viewControllerSpy.setTitleCount, 1)
        XCTAssertEqual(viewControllerSpy.titleSet, Strings.CashInMethods.screenTitle)
        XCTAssertEqual(viewControllerSpy.displaySectionsCount, 1)
        XCTAssertEqual(viewControllerSpy.displayedSections, categories)
        XCTAssertEqual(viewControllerSpy.displayHeaderCount, 1)
        XCTAssertEqual(viewControllerSpy.displayedHeader, Assets.iluCashInMethodHeader.image)
        XCTAssertEqual(viewControllerSpy.displayFooterCount, 1)
        XCTAssertEqual(viewControllerSpy.displayedFooter, Strings.CashInMethods.TableFooter.text)
    }
    
    func testNextStep_wheneverCalled_shouldInvokeCoordinatorPerformAction() throws {
        let mockURL = try XCTUnwrap(URL(string: "www.picpay.com"))
        let mockActions: [CashInMethodsAction] = [
            .account,
            .legacyAddValue(methodId: 1),
            .legacyCaixa,
            .lending,
            .loan,
            .dismiss,
            .deeplink(url: mockURL)
        ]
        
        for action in mockActions { sut.didNextStep(action: action) }
        
        XCTAssertEqual(coordinatorSpy.performedActions, mockActions)
    }
    
    func testNextStepUnderConstruction_ShouldInvokeCoordinatorPerformAction() {
        let attributedMessage = NSAttributedString(string: Strings.CashInMethods.UnderConstruction.message)
        let validApolloFeedbackViewContent = ApolloFeedbackViewContent(image: Resources.Illustrations.iluConstruction.image,
                                                                       title: Strings.CashInMethods.UnderConstruction.title,
                                                                       description: attributedMessage,
                                                                       primaryButtonTitle: Strings.Global.action)
        
        let validAction = CashInMethodsAction.underConstruction(content: validApolloFeedbackViewContent)
        
        sut.didNextStepUnderConstruction()
        
        XCTAssertEqual(coordinatorSpy.performedActions, [validAction])
    }
    
    func testDisplayLoading_wheneverCalled_shouldInvokeControllerBeginLoading() {
        sut.displayLoading()
        
        XCTAssertEqual(viewControllerSpy.beginLoadingCount, 1)
    }
    
    func testDisplayError_wheneverCalled_shouldInvokeControllerDisplayError() {
        let validViewModel = StatefulErrorViewModel(
            image: Resources.Illustrations.iluNotFoundCircledBackground.image,
            content: (title: Strings.Global.Default.Error.title,
                      description: Strings.Global.Default.RequestError.message),
            button: (image: nil, title: Strings.Global.tryAgain)
        )
        
        sut.displayError()
        
        XCTAssertEqual(viewControllerSpy.displayErrorCount, 1)
        XCTAssertEqual(viewControllerSpy.displayedError, validViewModel)
        XCTAssertEqual(viewControllerSpy.displaySectionsCount, 0)
        XCTAssertEqual(viewControllerSpy.displayHeaderCount, 0)
        XCTAssertEqual(viewControllerSpy.displayFooterCount, 0)
    }
}
