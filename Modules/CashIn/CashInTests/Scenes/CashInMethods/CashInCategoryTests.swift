import Foundation
import XCTest
@testable import CashIn

final class CashInCategoryTests: XCTestCase {
    //This test validates the API contract
    //If this test is broken you should:
    //1) double check with the backend developers
    //2) update the contract in the Mock file
    func testDecode_whenPassingTheDefinedJson_shouldCreateTheObject() throws {
        let sut = try XCTUnwrap(CashInMethodsMock.categories.getMock())

        XCTAssertEqual(sut.first?.methods.first?.methodId, .boleto)
    }
}
