import XCTest
@testable import CashIn

class CashInMethodsCoordinatorTests: XCTestCase {
    private let mockController = UIViewController()
    private lazy var rootController = UIViewController()
    
    private lazy var sut: CashInMethodsCoordinator = {
        let container = DependencyContainerMock()
        let coordinator = CashInMethodsCoordinator(dependencies: container)
        return coordinator
    }()
    
    func testPerformAction_whenDismissing_shouldDismissMockController() {
        sut.viewController = mockController
        rootController.present(mockController, animated: true, completion: nil)
        
        sut.perform(action: .dismiss)
        
        XCTAssertNil(sut.viewController?.presentingViewController)
        // WIP: This assertion passes but validates nothing as
        // the presentingViewController property is nil even
        // after calling the present function from the rootController
    }
}
