import AnalyticsModule
import Core
import FeatureFlag
import XCTest
@testable import CashIn

private final class CashInMethodsServiceMock: CashInMethodsServicing {
    var getCashInMethodsExpectedResult: Result<[CashInCategory], ApiError>?
    
    func getCashInMethods(completion: @escaping (Result<[CashInCategory], ApiError>) -> Void) {
        guard let expectedResult = getCashInMethodsExpectedResult else {
            XCTFail("Expected result not defined")
            return
        }
        completion(expectedResult)
    }
}

private final class CashInMethodsPresenterSpy: CashInMethodsPresenting {
    var viewController: CashInMethodsDisplaying?
    
    private(set) var displayCount: Int = 0
    private(set) var displayedSections = [CashInCategory]()
    func display(_ sections: [CashInCategory]) {
        displayCount += 1
        displayedSections = sections
    }
    
    private(set) var displayLoadingCount = 0
    func displayLoading() {
        displayLoadingCount += 1
    }
    
    private(set) var displayErrorCount: Int = 0
    func displayError() {
        displayErrorCount += 1
    }
    
    private(set) var didNextStepCount: Int = 0
    private(set) var didNextStepAction: CashInMethodsAction?
    func didNextStep(action: CashInMethodsAction) {
        didNextStepCount += 1
        didNextStepAction = action
    }
    
    private(set) var didNextStepUnderConstructionCount: Int = 0
    func didNextStepUnderConstruction() {
        didNextStepUnderConstructionCount += 1
    }
}

final class CashInMethodsInteractorTests: XCTestCase {
    private let analytics = AnalyticsSpy()
    private lazy var featureManager = FeatureManagerMock()
    private lazy var dependenciesMock = DependencyContainerMock(analytics, featureManager)
    private lazy var serviceMock = CashInMethodsServiceMock()
    private lazy var presenterSpy = CashInMethodsPresenterSpy()
    
    private lazy var sut = CashInMethodsInteractor(dependencies: dependenciesMock,
                                                   service: serviceMock,
                                                   presenter: presenterSpy,
                                                   originIsDeeplink: false)
    
    func testLogScreenViewedEvent_ShouldLogTheCorrectEvent() {
        let validEvent = CashInEvent.screenViewed(name: .addMoney, context: .cashIn, action: .tap)
        
        sut.logScreenViewedEvent()
        
        XCTAssertTrue(analytics.equals(to: validEvent.event()))
    }
    
    func testLoadItems_whenFetchSucceeds_shouldInvokePresenterDisplay() throws {
        let categories = try XCTUnwrap(CashInMethodsMock.categories.getMock())
        serviceMock.getCashInMethodsExpectedResult = .success(categories)
        sut.loadItems()
        
        XCTAssertEqual(presenterSpy.displayLoadingCount, 1)
        XCTAssertEqual(presenterSpy.displayErrorCount, 0)
        XCTAssertEqual(presenterSpy.displayCount, 1)
        XCTAssertEqual(presenterSpy.displayedSections, categories)
    }
    
    func testLoadItems_whenFetchFails_shouldInvokePresenterDisplayError() {
        serviceMock.getCashInMethodsExpectedResult = .failure(.unknown(nil))
        sut.loadItems()
        
        XCTAssertEqual(presenterSpy.displayLoadingCount, 1)
        XCTAssertEqual(presenterSpy.displayErrorCount, 1)
        XCTAssertEqual(presenterSpy.displayCount, 0)
        XCTAssertEqual(presenterSpy.displayedSections, [])
    }
    
    func testAction_whenThereAreLoadedMethods_shouldInvokePresenterDidNextStep() throws {
        let categories = try XCTUnwrap(CashInMethodsMock.categories.getMock())
        let validEvent = CashInEvent.buttonClicked(buttonName: .boleto, screenName: .addMoney, context: .cashIn)
        
        serviceMock.getCashInMethodsExpectedResult = .success(categories)
        sut.loadItems()
        
        sut.action(for: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(presenterSpy.didNextStepCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepAction, .legacyAddValue(methodId: 1))
        XCTAssertTrue(analytics.equals(to: validEvent.event()))
    }
    
    func testAction_WhenMethodIsUnavailable_ShouldInvokePresenterDidNextStepUnderConstruction() throws {
        let categories = try XCTUnwrap(CashInMethodsMock.categories.getMock())
        let validEvent = CashInEvent.screenViewed(name: .underConstruction, context: .cashIn, action: nil)
        
        serviceMock.getCashInMethodsExpectedResult = .success(categories)
        sut.loadItems()
        
        sut.action(for: IndexPath(row: 1, section: 0))
        
        XCTAssertEqual(presenterSpy.didNextStepCount, 0)
        XCTAssertEqual(presenterSpy.didNextStepUnderConstructionCount, 1)
        XCTAssertTrue(analytics.equals(to: validEvent.event()))
    }
    
    func testAction_whenThereAreNoLoadedMethods_shouldInvokePresenterGetDetails() {
        sut.action(for: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(presenterSpy.displayErrorCount, 1)
    }
    
    func testGetHelp_wheneverCalled_shouldInvokePresenterGetHelp() throws {
        let validEvent = CashInEvent.buttonClicked(buttonName: .learnMore, screenName: .addMoney, context: .cashIn)
        featureManager.override(key: .cashInMethodsMoreInfoUrl, with: "www.picpay.com")
        let url = try XCTUnwrap(URL(string: featureManager.text(.cashInMethodsMoreInfoUrl)))
        sut.getHelp()
        
        XCTAssertEqual(presenterSpy.didNextStepAction, .deeplink(url: url))
        XCTAssertTrue(analytics.equals(to: validEvent.event()))
    }
    
    func testMoreInfo_wheneverCalled_shouldInvokePresenterMoreInfo() throws {
        let validEvent = CashInEvent.buttonClicked(buttonName: .learnMore, screenName: .addMoney, context: .cashIn)
        featureManager.override(key: .cashInMethodsMoreInfoUrl, with: "www.picpay.com")
        let url = try XCTUnwrap(URL(string: featureManager.text(.cashInMethodsMoreInfoUrl)))
        sut.moreInfo()
        
        XCTAssertEqual(presenterSpy.didNextStepAction, .deeplink(url: url))
        XCTAssertTrue(analytics.equals(to: validEvent.event()))
    }
    
    func testDismiss_wheneverCalled_shouldInvokePresenterDismiss() {
        sut.dismiss()
        
        XCTAssertEqual(presenterSpy.didNextStepAction, .dismiss)
    }
}
