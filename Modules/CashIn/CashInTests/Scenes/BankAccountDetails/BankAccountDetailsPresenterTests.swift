import UI
import XCTest
@testable import CashIn

private final class BankAccountDetailsDisplayableSpy: BankAccountDetailsDisplaying {
    private(set) var sharedInfo: [Any] = []
    private(set) var shareInfoCount = 0
    func shareInfo(_ info: [Any]) {
        sharedInfo = info
        shareInfoCount += 1
    }
    
    private(set) var setupParams: (userInfo: UserInfo, bankInfo: BankAccountInfo)?
    private(set) var setupCount = 0
    func setup(userInfo: UserInfo, bankInfo: BankAccountInfo) {
        setupParams = (userInfo, bankInfo)
        setupCount += 1
    }
    
    private(set) var beginLoadingCount = 0
    func beginLoading() {
        beginLoadingCount += 1
    }
    
    private(set) var displayErrorCount = 0
    private(set) var displayedError: StatefulErrorViewModel?
    func displayError(with viewModel: StatefulErrorViewModel) {
        displayErrorCount += 1
        displayedError = viewModel
    }
}

private final class BankAccountDetailsCoordinatorSpy: BankAccountDetailsCoordinating {
    var viewController: UIViewController?
    
    private(set) var performedActions: [BankAccountDetailsAction] = []
    func perform(action: BankAccountDetailsAction) {
        performedActions.append(action)
    }
}

final class BankAccountDetailsPresenterTests: XCTestCase {
    private lazy var viewControllerSpy = BankAccountDetailsDisplayableSpy()
    private lazy var coordinatorSpy = BankAccountDetailsCoordinatorSpy()
    
    private lazy var sut: BankAccountDetailsPresenter = {
        let presenter = BankAccountDetailsPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testDisplayInfo_wheneverCalled_shouldInvokeControllerSetup() {
        let mock = BankAccountDetailsMock.accountInfoMock
        sut.display(mock)
        
        XCTAssertEqual(viewControllerSpy.setupParams?.userInfo, mock.userInfo)
        XCTAssertEqual(viewControllerSpy.setupParams?.bankInfo, mock.bankInfo)
        XCTAssertEqual(viewControllerSpy.setupCount, 1)
    }
    
    func testShare_wheneverCalled_shouldInvokeControllerShare() throws {
        let shareableInfo = ["shareableInfo"]
        sut.share(accountInfo: shareableInfo)
        
        let sharedInfo = try XCTUnwrap(viewControllerSpy.sharedInfo as? [String])
        
        XCTAssertEqual(sharedInfo, shareableInfo)
        XCTAssertEqual(viewControllerSpy.shareInfoCount, 1)
    }
    
    func testNextStep_wheneverCalled_shouldInvokeCoordinatorPerformAction() throws {
        let url = try XCTUnwrap(URL(string: "www.picpay.com"))
        let actions: [BankAccountDetailsAction] = [.startOnboarding, .deeplink(url: url), .back]
        for action in actions {
            sut.didNextStep(action: action)
        }
        
        XCTAssertEqual(coordinatorSpy.performedActions, actions)
    }
    
    func testDisplayLoading_shouldInvokeControllerBeginLoading() {
        sut.displayLoading()
        
        XCTAssertEqual(viewControllerSpy.beginLoadingCount, 1)
    }
    
    func testDisplayError_shouldInvokeControllerDisplayError() {
        let validError = StatefulErrorViewModel.cashInError
        sut.displayError()
        
        XCTAssertEqual(viewControllerSpy.displayErrorCount, 1)
        XCTAssertEqual(viewControllerSpy.displayedError, validError)
    }
}
