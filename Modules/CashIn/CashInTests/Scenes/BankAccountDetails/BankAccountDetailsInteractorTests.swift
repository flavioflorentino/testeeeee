@testable import CashIn
import AnalyticsModule
import FeatureFlag
import Core
import Foundation
import XCTest

final class BankAccountDetailsServiceMock: BankAccountDetailsServicing {
    var getBankingInfoExpectedResult: Result<BankingInfo, ApiError>?
    
    func getBankingInfo(completion: @escaping (Result<BankingInfo, ApiError>) -> Void) {
        guard let expectedResult = getBankingInfoExpectedResult else {
            XCTFail("Expected result not defined")
            return
        }
        completion(expectedResult)
    }
}

final class BankAccountDetailsPresenterSpy: BankAccountDetailsPresenting {
    var viewController: BankAccountDetailsDisplaying?
    
    private(set) var displayedInfo: BankAccountDetails?
    private(set) var displayCount = 0
    func display(_ accountInfo: BankAccountDetails) {
        displayedInfo = accountInfo
        displayCount += 1
    }

    private(set) var sharedInfo = [Any]()
    private(set) var shareCount = 0
    func share(accountInfo: [Any]) {
        sharedInfo = accountInfo
        shareCount += 1
    }
    
    private(set) var triggeredActions: [BankAccountDetailsAction] = []
    func didNextStep(action: BankAccountDetailsAction) {
        triggeredActions.append(action)
    }
    
    private(set) var displayErrorCount = 0
    func displayError() {
        displayErrorCount += 1
    }
    
    private(set) var displayLoadingCount = 0
    func displayLoading() {
        displayLoadingCount += 1
    }
}

final class BankAccountDetailsInteractorTests: XCTestCase {
    private lazy var featureManager = FeatureManagerMock()
    private let analytics = AnalyticsSpy()
    private let kvStore = KVStoreMock()
    private lazy var dependenciesMock = DependencyContainerMock(analytics, kvStore, featureManager)
    private lazy var presenterSpy = BankAccountDetailsPresenterSpy()
    private lazy var serviceMock = BankAccountDetailsServiceMock()
    private lazy var sut = BankAccountDetailsInteractor(service: serviceMock,
                                                        presenter: presenterSpy,
                                                        dependencies: dependenciesMock)
    
    func testLoadInfo_onSuccess_shouldInvokePresenterDisplay() {
        serviceMock.getBankingInfoExpectedResult = .success(BankAccountDetailsMock.bankingInfo)
        sut.loadInfo()
        
        XCTAssertEqual(presenterSpy.displayedInfo, BankAccountDetailsMock.accountInfoMock)
        XCTAssertEqual(presenterSpy.displayCount, 1)
        XCTAssertEqual(presenterSpy.displayLoadingCount, 1)
        XCTAssertEqual(presenterSpy.displayErrorCount, 0)
    }
    
    func testLoadInfo_onApiError_shouldInvokePresenterDisplay() {
        let apiError = ApiError.unknown(nil)
        serviceMock.getBankingInfoExpectedResult = .failure(apiError)
        sut.loadInfo()
        
        XCTAssertEqual(presenterSpy.displayLoadingCount, 1)
        XCTAssertEqual(presenterSpy.displayErrorCount, 1)
        XCTAssertEqual(presenterSpy.displayCount, 0)
    }
    
    func testLoadInfo_wheneverCalled_shouldSendAnalyticsEvent() {
        let validEvent = CashInLegacyEvent.wireTransfer(.picpayAccount).event()
        serviceMock.getBankingInfoExpectedResult = .success(BankAccountDetailsMock.bankingInfo)
        sut.loadInfo()
        
        XCTAssertTrue(analytics.equals(to: validEvent))
    }
    
    func testLoadInfo_onUsersFirstAccess_shouldInvokePresenterOnboarding() {
        kvStore.set(value: false, with: "wire-transfer-onboarding-has-been-shown")
        serviceMock.getBankingInfoExpectedResult = .success(BankAccountDetailsMock.bankingInfo)
        sut.loadInfo()
        
        XCTAssertEqual(presenterSpy.triggeredActions, [.startOnboarding])
    }
    
    func testLoadInfo_afterUsersFirstAccess_shouldNotInvokePresenterOnboarding() {
        kvStore.set(value: true, with: "wire-transfer-onboarding-has-been-shown")
        serviceMock.getBankingInfoExpectedResult = .success(BankAccountDetailsMock.bankingInfo)
        sut.loadInfo()
        
        XCTAssertEqual(presenterSpy.triggeredActions, [])
    }
    
    func testClose_wheneverCalled_shouldInvokePresenterBack() {
        sut.close()
        
        XCTAssertEqual(presenterSpy.triggeredActions, [.back])
    }
    
    func testMoreInfo_wheneverCalled_shouldInvokePresenterMoreInfo() throws {
        featureManager.override(key: .cashInMethodsMoreInfoUrl, with: "www.picpay.com")
        let url = try XCTUnwrap(URL(string: featureManager.text(.cashInMethodsMoreInfoUrl)))
        sut.moreInfo()
        
        XCTAssertEqual(presenterSpy.triggeredActions, [.deeplink(url: url)])
    }
    
    func testShareBankInfo_wheneverCalled_shouldInvokePresenterShareWithFormattedInfo() {
        let validMock = BankAccountDetailsMock.accountInfoMock
        let validAccountString = validMock.bankInfo.accountNumber.byRemovingPunctuation
        let validDocumentString = validMock.bankInfo.document.byRemovingPunctuation
        
        let validInfoString = Strings.CashInMethods.BankInfo.sharable(validMock.userInfo.fullname,
                                                                      validMock.bankInfo.bankNumber,
                                                                      validMock.bankInfo.agencyNumber,
                                                                      validAccountString,
                                                                      validDocumentString)
        
        serviceMock.getBankingInfoExpectedResult = .success(BankAccountDetailsMock.bankingInfo)
        sut.loadInfo()
        sut.shareBankInfo()
        
        XCTAssertEqual(presenterSpy.sharedInfo as? [String], [validInfoString])
        XCTAssertEqual(presenterSpy.shareCount, 1)
    }
}
