#!/bin/sh

if [ -n "${IS_PROD_TARGET}" ]; then
     echo "Gerando credenciais para o ambiente de produção da aplicação Sendbird"
     sourcery --config Setup/Sourcery/sourcery-prod.yml
 elif [ -n "${IS_HOMOLOG_TARGET}" ]; then
     echo "Gerando credenciais para o ambiente de homologação/QA da aplicação Sendbird"
     sourcery --config Setup/Sourcery/sourcery-homolog.yml
 else
     echo "Gerando credenciais para o ambiente de desenvolvimento da aplicação Sendbird"
     sourcery --config Setup/Sourcery/sourcery-develop.yml
fi