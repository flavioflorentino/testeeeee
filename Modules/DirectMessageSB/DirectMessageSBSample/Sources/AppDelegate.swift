import UIKit
import SendBirdSDK
import DirectMessageSB

enum SendbirdConstants {
    static let devApiKey = "4F3C814D-328A-41FF-95F0-0CAB63CA06D0"
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    func application(_ application: UIApplication,
                     // swiftlint:disable:next discouraged_optional_collection
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        SBDMain.initWithApplicationId(SendbirdConstants.devApiKey)
        SendBirdThemeManager().setup()
        return true
    }

    func application(_ application: UIApplication,
                     configurationForConnecting connectingSceneSession: UISceneSession,
                     options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
}
