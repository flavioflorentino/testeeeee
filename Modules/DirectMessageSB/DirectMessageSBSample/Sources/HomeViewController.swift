import AssetsKit
import UI
import UIKit
import SendBirdSDK
import SendBirdUIKit
import DirectMessageSB

struct SBChatUser: ChatUser {
    let messagingId: Int
    let type: ChatUserType
    let username: String?
    let profileName: String?
    let avatarUrl: String?
    let token: String
}

enum Credentials {
    static let userRamones = SBChatUser(messagingId: 311,
                                        type: .consumer,
                                        username: "ramones",
                                        profileName: "Ramon Honorio",
                                        avatarUrl: nil,
                                        token: "a9d2fe11e416c7dd0061fba2ffb0bc08abc274dc")
    static let userAdriano = SBChatUser(messagingId: 113,
                                        type: .consumer,
                                        username: "adrianodias",
                                        profileName: "José Adriano",
                                        avatarUrl: nil,
                                        token: "e0cd0b5c2eb8c768959f110bf1b8ac180ef94d84")
}

final class HomeViewController: UIViewController {
    @available(*, unavailable)
    required init?(coder: NSCoder) { nil }
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 16
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    private lazy var ramonesButton: UIButton = {
        let button = UIButton()
        button.setTitle("open chat as [Ramones]", for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(startSendbirdChatWithRamones), for: .touchUpInside)
        button.titleEdgeInsets = .init(top: 0, left: 8, bottom: 0, right: 12)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var adrianosButton: UIButton = {
        let button = UIButton()
        button.setTitle("open chat as [Jose Adriano]", for: .normal)
        button.buttonStyle(SecondaryButtonStyle())
        button.addTarget(self, action: #selector(startSendbirdChatWithAdriano), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleEdgeInsets = .init(top: 0, left: 8, bottom: 0, right: 12)
        return button
    }()
    
    private lazy var chatNotificationButton: UIButton = {
        let button = UIButton()
        button.setTitle("show chat notification", for: .normal)
        button.buttonStyle(
            SecondaryButtonStyle(size: .default,
                                 icon: (name: Iconography.chat, alignment: IconAlignment.left))
        )
        button.addTarget(self, action: #selector(openChatNotification), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleEdgeInsets = .init(top: 0, left: 8, bottom: 0, right: 12)
        return button
    }()
    
    init() {
        super.init(nibName: nil, bundle: nil)
        view.addSubview(stackView)
        stackView.addArrangedSubview(ramonesButton)
        stackView.addArrangedSubview(adrianosButton)
        stackView.addArrangedSubview(chatNotificationButton)
        
        NSLayoutConstraint.activate([
            stackView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.8),
            stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            stackView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
        
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    @objc
    func startSendbirdChatWithRamones() {
        startSendbirdChat(withUser: Credentials.userRamones)
    }
    
    @objc
    func startSendbirdChatWithAdriano() {
        startSendbirdChat(withUser: Credentials.userAdriano)
    }
    
    @objc
    func openChatNotification() {
        ChatNotificationBannerView.present(
            onView: view,
            title: "@ramones 💬",
            message: "salve catchurro",
            imageURL: URL(string: "https://ca.slack-edge.com/T03C4AX6M-U01DQBJLY1F-9830a6825be8-512")
        )
    }
    
    func startSendbirdChat(withUser user: SBChatUser) {
        SBDMain.add(self, identifier: "HomeDelegate")
        SBDMain.connect(withUserId: user.clientId, accessToken: user.token) { [weak self] user, error in
            if let error = error {
                print("SDB Connection Error: \(error)")
                return
            }
            guard let self = self, let user = user else { return }
//            self.presentChat(forUser: user)
            self.presentChatList(forUser: user)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension HomeViewController: SBDChannelDelegate {
    func presentChat(forUser user: SBDUser) {
        SBUGlobals.CurrentUser = SBUUser(user: user)
        
        let otherChatter = user.userId == Credentials.userRamones.clientId
            ? Credentials.userAdriano
            : Credentials.userRamones
        
        let chatViewController = ChatSetupFactory.make(for: .distinctUser(otherChatter))
        navigationController?.pushViewController(chatViewController, animated: true)
    }
    
    func presentChatList(forUser user: SBDUser) {
        SBUGlobals.CurrentUser = SBUUser(user: user)
        
        let listViewController = ChatListFactory.make()
        navigationController?.pushViewController(listViewController, animated: true)
    }
}
