import Foundation

final class Mocker {
    private var jsonDecoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .custom { decoder in
            var container = try decoder.singleValueContainer()
            let timeInterval = TimeInterval(try container.decode(UInt.self))
            let date = Date(timeIntervalSince1970: timeInterval)
            return date
        }
        return decoder
    }()

    func decode<T: Decodable>(_ type: T.Type, from data: Data?) -> T? {
        guard let data = data, let model = try? jsonDecoder.decode(type, from: data) else { return nil }
        return model
    }

    func data(_ fileName: String, ofType fileType: String = "json") -> Data? {
        guard
            let file = Bundle(for: type(of: self)).path(forResource: fileName, ofType: fileType),
            let data = try? Data(contentsOf: URL(fileURLWithPath: file))
        else {
            return nil
        }
        return data
    }

    func decode<T: Decodable>(_ type: T.Type, fromResource resource: String, ofType fileType: String = "json") -> T? {
        decode(type, from: data(resource, ofType: fileType))
    }
}
