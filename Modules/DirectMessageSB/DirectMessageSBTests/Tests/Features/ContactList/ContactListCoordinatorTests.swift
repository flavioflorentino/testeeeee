import XCTest
@testable import DirectMessageSB

final class ContactListCoordinatorTests: XCTestCase {
    private lazy var viewController = UIViewController()
    private lazy var navControllerSpy = NavigationControllerSpy(rootViewController: viewController)
    private lazy var sut: ContactListCoordinator = {
        let coordinator = ContactListCoordinator()
        coordinator.viewController = navControllerSpy.topViewController
        return coordinator
    }()
    
    func testPerform_WhenActionIsBack_ShouldPopViewController() {
        sut.perform(action: .back)
        
        XCTAssertTrue(navControllerSpy.isPopViewControllerCalled)
    }
    
    func testPerform_WhenActionIsOpenChat_ShouldPushChatSetupViewController() {
        sut.perform(action: .openChat(user: Stubs.currentChatUser))
        
        XCTAssert(navControllerSpy.settedViewControllers.last is ChatSetupViewController)
    }
}
