import AnalyticsModule
import XCTest
@testable import DirectMessageSB

private final class ContactListPresenterSpy: ContactListPresenting {
    var viewController: ContactListDisplaying?
    
    lazy var didCallDidNextStepCount: Int = 0
    lazy var calledAction: ContactListAction? = nil
    
    lazy var didCallInitialSetup: Int = 0
    
    func didNextStep(action: ContactListAction) {
        didCallDidNextStepCount += 1
        calledAction = action
    }
    
    func initialSetup() {
        didCallInitialSetup += 1
    }
}

final class ContactListInteractorTests: XCTestCase {
    private lazy var analyticsSpy = AnalyticsSpy()
    private lazy var presenterSpy = ContactListPresenterSpy()
    private lazy var sut = ContactListInteractor(presenter: presenterSpy, dependencies: DependenciesContainerMock(analyticsSpy))
    
    func testInitialSetup_WhenInitiatingViewController_ShouldCallPresenterInitialSetup() {
        sut.initialSetup()
        XCTAssertEqual(presenterSpy.didCallInitialSetup, 1)
    }
    
    func testBack_WhenBackIsPressed_ShouldCallDidNextStep() {
        sut.back()
        
        XCTAssertEqual(presenterSpy.didCallDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.calledAction, .back)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        let expectedEvent = DirectMessageSBAnalytics.button(.clicked, .back(onScreen: .newChatSB)).event()
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testNewChat_WhenNewChatIsPressed_ShouldCallDidNextStep() {
        sut.openChat(with: Stubs.currentChatUser)
        
        XCTAssertEqual(presenterSpy.didCallDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.calledAction, .openChat(user: Stubs.currentChatUser))
    }
    
    func testSetScreenAsViewed() {
        sut.setScreenAsViewed()
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        let expectedEvent = DirectMessageSBAnalytics.screenViewed( .newChatSB).event()
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
    }
}
