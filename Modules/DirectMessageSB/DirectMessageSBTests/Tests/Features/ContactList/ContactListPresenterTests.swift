import XCTest
@testable import DirectMessageSB

private final class ContactListCoordinatorSpy: ContactListCoordinating {
    var viewController: UIViewController?
    
    lazy var didCallPerformCount: Int = 0
    lazy var calledAction: ContactListAction? = nil
    
    func perform(action: ContactListAction) {
        didCallPerformCount += 1
        calledAction = action
    }
}

private final class ContactListViewControllerSpy: ContactListDisplaying {
    lazy var didCallSetupNavigationbarCount: Int = 0
    
    func setupNavigationBar() {
        didCallSetupNavigationbarCount += 1
    }
}

final class ContactListPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = ContactListCoordinatorSpy()
    private lazy var viewControllerSpy = ContactListViewControllerSpy()
    private lazy var sut: ContactListPresenter = {
        let presenter = ContactListPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testDidNextStep_WhenActionIsBack_ShouldCallPerform() {
        sut.didNextStep(action: .back)
        
        XCTAssertEqual(coordinatorSpy.didCallPerformCount, 1)
        XCTAssertEqual(coordinatorSpy.calledAction, .back)
    }
    
    func testDidNextStep_WhenActionIsOpenChat_ShouldCallPerform() {
        sut.didNextStep(action: .openChat(user: Stubs.currentChatUser))
        
        XCTAssertEqual(coordinatorSpy.didCallPerformCount, 1)
        XCTAssertEqual(coordinatorSpy.calledAction, .openChat(user: Stubs.currentChatUser))
    }
}
