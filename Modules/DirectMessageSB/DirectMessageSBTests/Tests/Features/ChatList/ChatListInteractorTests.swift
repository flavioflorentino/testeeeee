import AnalyticsModule
import Core
import FeatureFlag
import XCTest
@testable import DirectMessageSB

private final class ChatListPresenterSpy: ChatListPresenting {
    var viewController: ChatListDisplaying?
    
    lazy var didCallDidNextStepCount: Int = 0
    lazy var calledAction: ChatListAction? = nil
    lazy var calledShowBetaAlertCount: Int = 0
    
    lazy var didCallInitialSetup: Int = 0
    
    func didNextStep(action: ChatListAction) {
        didCallDidNextStepCount += 1
        calledAction = action
    }
    
    func initialSetup() {
        didCallInitialSetup += 1
    }
    
    func showBetaAlert() {
        calledShowBetaAlertCount += 1
    }
}

final class ChatListInteractorTests: XCTestCase {
    private lazy var presenterSpy = ChatListPresenterSpy()
    private lazy var analyticsSpy = AnalyticsSpy()
    private lazy var featureManagerMock = FeatureManagerMock()
    private lazy var chatConnectionManagerMock = ChatApiManagerMock()
    private lazy var kvStoreMock = KVStoreMock()
    private lazy var dependencies = DependenciesContainerMock(chatConnectionManagerMock,
                                                              analyticsSpy,
                                                              featureManagerMock,
                                                              kvStoreMock)
    private lazy var sut = ChatListInteractor(presenter: presenterSpy, dependencies: dependencies)

    func testClientId_ShouldReturnClientIdFromConnectionManager() {
        XCTAssertEqual(sut.clientId, chatConnectionManagerMock.clientId)
    }
    
    func testInitialSetup_WhenInitiatingViewController_ShouldCallPresenterInitialSetup() {
        sut.initialSetup()
        
        XCTAssertEqual(presenterSpy.didCallInitialSetup, 1)
    }
    
    func testShowBetaAlertIfNeeded_WhenNeverShowned_ShouldShowAlertAndSetKeyValueToTrue() {
        let betaAlertKey = KVKey.hasPresentedBetaAlert
        
        dependencies.kvStore.setBool(false, with: betaAlertKey)
        sut.showBetaAlertIfNeeded()
        
        XCTAssertEqual(presenterSpy.calledShowBetaAlertCount, 1)
        XCTAssertTrue(dependencies.kvStore.boolFor(betaAlertKey))
    }
    
    func testShowBetaAlertIfNeeded_WhenAlreadyShowned_ShouldNotShowAlertAndShouldNotChangeKeyValue() {
        let betaAlertKey = KVKey.hasPresentedBetaAlert
        
        dependencies.kvStore.setBool(true, with: KVKey.hasPresentedBetaAlert)
        sut.showBetaAlertIfNeeded()
        
        XCTAssertEqual(presenterSpy.calledShowBetaAlertCount, 0)
        XCTAssertTrue(dependencies.kvStore.boolFor(betaAlertKey))
    }

    func testSetScreenAsViewed() {
        sut.setScreenAsViewed()
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        let expectedEvent = DirectMessageSBAnalytics.screenViewed( .chats).event()
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testTrackOpenChatEvent_WhenHasUnreadMessages() {
        sut.trackOpenChatEvent(unreadMessageCount: 1)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        let expectedEvent = DirectMessageSBAnalytics.button(.clicked, .openChat(status: .unread)).event()
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testTrackOpenChatEvent_WhenHasNoUnreadMessages() {
        sut.trackOpenChatEvent(unreadMessageCount: 0)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        let expectedEvent = DirectMessageSBAnalytics.button(.clicked, .openChat(status: .clear)).event()
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testNewChat_WhenNewChatViaSBIsPressed_ShouldCallDidNextStep() {
        featureManagerMock.override(key: .directMessageSBViaSearchEnabled, with: false)
        sut.newChat()
        
        XCTAssertEqual(presenterSpy.calledAction, .newChatViaSB)
        XCTAssertEqual(presenterSpy.didCallDidNextStepCount, 1)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        let expectedEvent = DirectMessageSBAnalytics.button(.clicked, .newChat).event()
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
    }

    func testNewChat_WhenNewChatViaSearchIsPressed_ShouldCallDidNextStep() {
        featureManagerMock.override(key: .directMessageSBViaSearchEnabled, with: true)
        sut.newChat()

        XCTAssertEqual(presenterSpy.calledAction, .newChatViaSearch)
        XCTAssertEqual(presenterSpy.didCallDidNextStepCount, 1)
        XCTAssertEqual(analyticsSpy.logCalledCount, 2)
        let expectedNewChatButtonEvent = DirectMessageSBAnalytics.button(.clicked, .newChat).event()
        let expectedNewChatScreenViewedEvent = DirectMessageSBAnalytics.screenViewed(.newChatSearch).event()
        XCTAssert(analyticsSpy.equals(to: expectedNewChatButtonEvent, expectedNewChatScreenViewedEvent))
    }
    
    func testCellViewModel_ShouldRetrieveTitleFromOtherUser() {
        let groupChannelMock = GroupChannelMock(channelMembers: [
            GroupChannelMemberMock(userId: "C1", profileName: "Current User"),
            GroupChannelMemberMock(userId: "C2", profileName: "Other User")
        ])
        
        let viewModel = sut.cellViewModel(for: groupChannelMock)
        
        XCTAssertEqual(viewModel.titleText, "Other User")
    }
    
    func testDidDismissScreen_WhenIsMovingFromParent_ShouldTrackAnalyticsEvent() {
        sut.didDismissScreen(isMovingFromParent: true)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        let expectedEvent = DirectMessageSBAnalytics.button(.clicked, .back(onScreen: .chats)).event()
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testDidDismissScreen_WhenIsMovingFromParent_ShouldNotTrackAnalyticsEvent() {
        sut.didDismissScreen(isMovingFromParent: false)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 0)
    }
}
