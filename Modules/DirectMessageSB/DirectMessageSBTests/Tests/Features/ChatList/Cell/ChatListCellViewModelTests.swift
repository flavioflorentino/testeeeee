import XCTest
@testable import DirectMessageSB

// MARK: - Doubles

final class UIViewSpy: UIView {
    private(set) var removeFromSuperviewCalledCount = 0
    
    override func removeFromSuperview() {
        super.removeFromSuperview()
        removeFromSuperviewCalledCount += 1
    }
}

final class ChatListCellSpy: ChatListCell {
    let titleLabel = UILabel()
    let separatorLine: UIView = UIViewSpy()
}

final class GroupChannelMock: ChatListGroupChannel {
    init() { }
    init(channelMembers: [ChatListGroupChannelMember]) {
        self.channelMembers = channelMembers
    }
    
    var channelMembers: [ChatListGroupChannelMember] = [
        GroupChannelMemberMock(userId: "1", profileName: "Current User"),
        GroupChannelMemberMock(userId: "2", profileName: "Other User")
    ]
}

final class GroupChannelMemberMock: ChatListGroupChannelMember {
    var userId: String
    var profileName: String?
    
    init(userId: String, profileName: String?) {
        self.userId = userId
        self.profileName = profileName
    }
}

// MARK: - Tests

final class ChatListCellViewModelTests: XCTestCase {
    private lazy var groupChannelMock = GroupChannelMock()
    private lazy var cellSpy = ChatListCellSpy()

    func testTitleText_ShouldRetrieveProfileNameFromTheOtherUser() {
        let sut = ChatListCellViewModel(from: groupChannelMock, with: "1")
        
        XCTAssertEqual(sut.titleText, "Other User")
    }
}
