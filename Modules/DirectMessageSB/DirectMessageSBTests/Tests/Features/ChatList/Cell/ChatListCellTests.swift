import XCTest
@testable import DirectMessageSB

// MARK: - Doubles

private final class ChatListCellViewModelMock: ChatListCellViewModeling {
    var titleText: String = Stubs.currentChatUser.profileName ?? ""
}

// MARK: - Tests

final class ChatListCellTests: XCTestCase {
    private lazy var viewModelMock = ChatListCellViewModelMock()
    private lazy var sut = ChatListCellSpy()

    func testSetup_ShouldSetValuesProperly() {
        sut.setup(with: viewModelMock)

        let separatorLineSpy = sut.separatorLine as? UIViewSpy
        XCTAssertEqual(separatorLineSpy?.removeFromSuperviewCalledCount, 1)
        XCTAssertEqual(sut.titleLabel.text, Stubs.currentChatUser.profileName)
    }
}
