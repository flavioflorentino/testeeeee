import UI
import XCTest
@testable import DirectMessageSB

private final class ChatListCoordinatorSpy: ChatListCoordinating {
    var viewController: UIViewController?
    
    lazy var didCallPerformCount: Int = 0
    lazy var calledAction: ChatListAction? = nil
    
    func perform(action: ChatListAction) {
        didCallPerformCount += 1
        calledAction = action
    }
}

private final class ChatListViewControllerSpy: ChatListDisplaying {
    lazy var didCallSetupNavigationbarCount: Int = 0
    lazy var didCallShowBetaAlertCount: Int = 0
    
    func setupNavigationBar() {
        didCallSetupNavigationbarCount += 1
    }
    
    func showBetaAlert(text: String, iconType: ApolloFeedbackCardViewIconType, emphasis: ApolloSnackbarEmphasisStyle, duration: TimeInterval) {
        didCallShowBetaAlertCount += 1
    }
}

final class ChatListPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = ChatListCoordinatorSpy()
    private lazy var viewControllerSpy = ChatListViewControllerSpy()
    private lazy var sut: ChatListPresenter = {
        let presenter = ChatListPresenter(coordinator: coordinatorSpy, dependencies: DependenciesContainerMock())
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testDidNextStep_WhenActionIsBack_ShouldCallPerform() {
        sut.didNextStep(action: .back)
        
        XCTAssertEqual(coordinatorSpy.didCallPerformCount, 1)
        XCTAssertEqual(coordinatorSpy.calledAction, .back)
    }
    
    func testDidNextStep_WhenActionIsNewChat_ShouldOpenChatScreen() {
        sut.didNextStep(action: .newChatViaSB)
        
        XCTAssertEqual(coordinatorSpy.didCallPerformCount, 1)
        XCTAssertEqual(coordinatorSpy.calledAction, .newChatViaSB)
    }
    
    func testInitialSetup_WhenInitiatingViewController_ShouldCallSetupNavigationBar() {
        sut.initialSetup()
        
        XCTAssertEqual(viewControllerSpy.didCallSetupNavigationbarCount, 1)
    }
    
    func testShowBetaAlert_WhenActionIsShowBetaAlert_ShouldShowBetaAlert() {
        sut.showBetaAlert()
        
        XCTAssertEqual(viewControllerSpy.didCallShowBetaAlertCount, 1)
    }
}
