import Search
import XCTest
@testable import DirectMessageSB

final class ChatListCoordinatorTests: XCTestCase {
    private lazy var viewController = UIViewController()
    private lazy var navControllerSpy = NavigationControllerSpy(rootViewController: viewController)
    private lazy var sut: ChatListCoordinator = {
        let coordinator = ChatListCoordinator(dependencies: DependenciesContainerMock())
        coordinator.viewController = navControllerSpy.topViewController
        return coordinator
    }()
    
    func testPerform_WhenActionIsBack_ShouldPopViewController() {
        sut.perform(action: .back)
        
        XCTAssertTrue(navControllerSpy.isPopViewControllerCalled)
    }
    
    func testPerform_WhenActionIsNewChatViaSB_ShouldPushContactListViewController() {
        sut.perform(action: .newChatViaSB)
        
        XCTAssert(navControllerSpy.pushedViewController is ContactListViewController)
    }

    func testPerform_WhenActionIsNewChatViaSearch_ShouldPushSearchListViewController() {
        sut.perform(action: .newChatViaSearch)

        XCTAssertEqual(navControllerSpy.pushedViewController?.title, Strings.Contactlist.title)
    }

    func testOpenChat_WhenSearchSelecionIsNil_ShouldNotPushController() {
        sut.openChat(for: nil, in: navControllerSpy)

        XCTAssertFalse(navControllerSpy.settedViewControllers.last is ChatSetupViewController)
    }

    func testOpenChat_WhenSearchSelecionHasNoConsumerData_ShouldNotPushController() {
        let result: SearchResult = .unknown(type: String())
        sut.openChat(for: .single(result), in: navControllerSpy)

        XCTAssertFalse(navControllerSpy.settedViewControllers.last is ChatSetupViewController)
    }

    func testOpenChat_WhenSearchedConsumerHasConsumerData_ShouldNotPushController() throws {
        let result = try XCTUnwrap(Mocker().decode(SearchResult.self, fromResource: "search-result-consumer"))
        sut.openChat(for: .single(result), in: navControllerSpy)
        
        XCTAssert(navControllerSpy.settedViewControllers.last is ChatSetupViewController)
    }

    func testOpenChat_WhenSearchedContactHasConsumerData_ShouldNotPushController() throws {
        let result = try XCTUnwrap(Mocker().decode(SearchResult.self, fromResource: "search-result-contact"))
        sut.openChat(for: .single(result), in: navControllerSpy)

        XCTAssert(navControllerSpy.settedViewControllers.last is ChatSetupViewController)
    }
}
