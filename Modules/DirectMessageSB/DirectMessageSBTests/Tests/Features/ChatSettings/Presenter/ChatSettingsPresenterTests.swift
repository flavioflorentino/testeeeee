import Foundation
import XCTest
import SendBirdSDK
import SendBirdUIKit
@testable import DirectMessageSB

// MARK: - Doubles

private final class ChatSettingsCoordinatingSpy: ChatSettingsCoordinating {
    var viewController: UIViewController?

    private(set) var performActionReceivedInvocations: [ChatSettingsAction] = []

    func perform(action: ChatSettingsAction) {
        performActionReceivedInvocations.append(action)
    }
}

private final class ChatSettingsDisplayingSpy: SBUChannelViewController, ChatSettingsDisplaying {
    private(set) var setupNavigationBarCallsCount = 0
    
    func setupNavigationBar() {
        setupNavigationBarCallsCount += 1
    }
}

// MARK: - Tests

final class ChatSettingsPresenterTests: XCTestCase {
    private let coordinatorSpy = ChatSettingsCoordinatingSpy()
    private let displayingSpy = ChatSettingsDisplayingSpy()
    private lazy var sut = createSut()
    
    func testSetupNavigationBar_ShouldSetupNavigationBar() {
        sut.setupNavigationBar()
        
        XCTAssertEqual(displayingSpy.setupNavigationBarCallsCount, 1)
    }
    
    func testOpenProfile_ShouldPerformCoordinatorOpenProfileAction() {
        sut.openProfile(withId: Stubs.otherChatUser.clientId)
        
        XCTAssertEqual(coordinatorSpy.performActionReceivedInvocations, [.openProfile(id: Stubs.otherChatUser.clientId)])
    }
}

private extension ChatSettingsPresenterTests {
    func createSut() -> ChatSettingsPresenter {
        let sut = ChatSettingsPresenter(coordinator: coordinatorSpy)
        sut.viewController = displayingSpy
        return sut
    }
}
