import AnalyticsModule
import SendBirdUIKit
import XCTest
@testable import DirectMessageSB

private final class ChatSettingsPresenterSpy: ChatSettingsPresenting {
    var viewController: ChatSettingsDisplaying?
    
    private(set) lazy var didCallDidNextStepCount: Int = 0
    private(set) var calledAction: ChatSettingsAction?
    private(set) lazy var didSetupNavigationBarCount: Int = 0
    private(set) lazy var didOpenProfileCount: Int = 0
    
    func setupNavigationBar() {
        didSetupNavigationBarCount += 1
    }
    
    func openProfile(withId id: String) {
        didOpenProfileCount += 1
    }
    
    func didNextStep(action: ChatSettingsAction) {
        didCallDidNextStepCount += 1
        calledAction = action
    }
}

final class ChatSettingsInteractorTests: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    private let presenterSpy = ChatSettingsPresenterSpy()
    private lazy var dependencies = DependenciesContainerMock(analyticsSpy)
    private lazy var sut = ChatSettingsInteractor(channel: Stubs.chat, presenter: presenterSpy, dependencies: dependencies)
    
    func testInitialSetup_WhenOtherUserIsAvailable_ShouldCallLayoutSetup() throws {
        sut.setupNavigationBar()

        XCTAssertEqual(presenterSpy.didSetupNavigationBarCount, 1)
    }
    
    func testOpenProfile_ShouldCallOpenProfile() {
        sut.openProfile()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        let expectedEvent =
            DirectMessageSBAnalytics.button(.clicked, .openProfile(onScreen: .chatSettings)).event()
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(presenterSpy.didOpenProfileCount, 1)
    }
    
    func testSetScreenAsViewed() {
        sut.setScreenAsViewed()
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        let expectedEvent = DirectMessageSBAnalytics.screenViewed(.chatSettings).event()
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
    }
}
