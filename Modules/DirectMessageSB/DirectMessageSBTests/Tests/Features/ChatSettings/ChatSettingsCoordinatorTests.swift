@testable import DirectMessageSB
import UI
import XCTest

final class ChatSettingsCoordinatorTests: XCTestCase {
    lazy var legacyMock = DMSBLegacySetupMock()
    lazy var dependenciesMock = DependenciesContainerMock(legacyMock)
    lazy var navigationControllerSpy = UINavigationControllerSpy()
    lazy var viewControllerMock = UIViewController()
    
    lazy var sut: ChatSettingsCoordinator = {
        let sut = ChatSettingsCoordinator(dependencies: dependenciesMock)
        navigationControllerSpy.setViewControllers([viewControllerMock], animated: false)
        sut.viewController = viewControllerMock
        return sut
    }()
    
    func testOpenProfile_ShouldOpenProfileViewController() {
        sut.perform(action: .openProfile(id: ""))
        
        XCTAssertEqual(navigationControllerSpy.pushViewControllerCallCount, 1)
        XCTAssertEqual(navigationControllerSpy.viewControllers.count, 2)
        XCTAssertTrue(navigationControllerSpy.viewControllers.last is FakeProfileViewController)
    }
}
