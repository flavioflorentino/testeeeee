import SendBirdSDK

@testable import DirectMessageSB

final class ChatSetupServicingMock: ChatSetupServicing, ConnectionStateServicingDouble {
    var expectedConnectionState: ConnectionState = .closed
    var setActionForConnectionStateCount: Int = 0
    var cancelActionForConnectionStateCount: Int = 0
    var reconnectCount: Int = 0

    private(set) var retrieveChatWithUserIdIsDistinctCompletionCallsCount = 0
    func retrieveChat(withUserId userId: String,
                      isDistinct: Bool,
                      completion: @escaping (Result<SBDGroupChannel, ChatSetupServicingError>) -> Void) {
        retrieveChatWithUserIdIsDistinctCompletionCallsCount += 1
    }

    private(set) var retrieveChatUrlCompletionCallsCount = 0

    func retrieveChat(withUrl url: String, completion: @escaping (Result<SBDGroupChannel, ChatSetupServicingError>) -> Void) {
        retrieveChatUrlCompletionCallsCount += 1
    }
}
