import SendBirdSDK

@testable import DirectMessageSB

final class ChatSetupPresentingSpy: ChatSetupPresenting, ConnectionStatePresentingDouble {
    private(set) var presentChatForChannelCallsCount = 0
    var startConnectionLoadingCount: Int = 0
    var stopConnectionLoadingCount: Int = 0
    var presentConnectionErrorCount: Int = 0
    var connectionStateDisplay: ConnectionStateDisplaying?
    var connectionErrorAlert: ChatConnectionErrorAlert = .standard
    
    func presentChat(forChannel channel: SBDGroupChannel) {
        presentChatForChannelCallsCount += 1
    }

    private(set) var closeChatCallsCount = 0
    func closeChat() {
        closeChatCallsCount += 1
    }
}
