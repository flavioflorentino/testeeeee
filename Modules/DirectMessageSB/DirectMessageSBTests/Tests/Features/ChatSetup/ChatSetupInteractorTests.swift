import Foundation
import XCTest
import SendBirdSDK
@testable import DirectMessageSB

final class ChatSetupInteractorTests: XCTestCase {
    private let userStub: ChatRetrievingMethod = .distinctUser(Stubs.otherChatUser)
    private let urlStub: ChatRetrievingMethod = .url(Stubs.currentChatUser.avatarUrl ?? String())
    private lazy var queueMock = DispatchQueue(label: "ChatSetupInteractor")
    private lazy var dependenciesMock = DependenciesContainerMock(queueMock)
    private lazy var presenterSpy = ChatSetupPresentingSpy()
    private lazy var serviceMock = ChatSetupServicingMock()
    private lazy var sut = createSut(with: .distinctUser(Stubs.otherChatUser))

    var connectionStateInteractor: ConnectionStateInteracting {
        sut
    }

    var connectionStatePresenterSpy: ConnectionStatePresentingDouble {
        presenterSpy
    }

    var connectionStateServiceMock: ConnectionStateServicingDouble {
        serviceMock
    }

    func testClose_ShouldCallCloseChatOnPresenter() {
        sut.close()

        XCTAssertEqual(presenterSpy.closeChatCallsCount, 1)
    }

    func testFetchChatInformation_WhenConnectionStateIsConnecting_ShouldNotRetrieveChat() {
        setConnection(state: .connecting)

        sut.fetchChatInformation()

        assertForCurrentState()
        XCTAssertEqual(serviceMock.retrieveChatWithUserIdIsDistinctCompletionCallsCount, 0)
    }

    func testFetchChatInformation_WhenConnectionStateIsConnectedAndRetrievingMethodIsUser_ShouldRetrieveChatOnServiceWithUser() {
        setConnection(state: .open)

        sut = createSut(with: userStub)
        sut.fetchChatInformation()

        assertForCurrentState()
        XCTAssertEqual(serviceMock.retrieveChatWithUserIdIsDistinctCompletionCallsCount, 1)
        XCTAssertEqual(serviceMock.retrieveChatUrlCompletionCallsCount, 0)
    }

    func testFetchChatInformation_WhenConnectionStateIsConnectedAndRetrievingMethodIsUrl_ShouldRetrieveChatOnServiceWithUrl() {
        setConnection(state: .open)

        sut = createSut(with: urlStub)
        sut.fetchChatInformation()

        assertForCurrentState()
        XCTAssertEqual(serviceMock.retrieveChatUrlCompletionCallsCount, 1)
        XCTAssertEqual(serviceMock.retrieveChatWithUserIdIsDistinctCompletionCallsCount, 0)
    }

    func testFetchChatInformation_WhenConnectionStateIsNotConnected__ShouldNotRetrieveChat() {
        setConnection(state: .failure)

        sut = createSut(with: urlStub)
        sut.fetchChatInformation()

        assertForCurrentState()
        XCTAssertEqual(serviceMock.retrieveChatUrlCompletionCallsCount, 0)
    }

    func testDidCancelAction_ShouldStopLoadingAndResetActions() {
        assertDidCancelAction()
    }

    func testReconnect_ShouldCallReconnectOnServerAndResetActions() {
        assertReconnect()
    }
}

extension ChatSetupInteractorTests: ConnectionStateStandardInteractorTestable {
    func createSut(with retrievingMethod: ChatRetrievingMethod) -> ChatSetupInteractor {
        ChatSetupInteractor(retrievingMethod: retrievingMethod,
                            presenter: presenterSpy,
                            service: serviceMock,
                            dependencies: DependenciesContainerMock())
    }
}
