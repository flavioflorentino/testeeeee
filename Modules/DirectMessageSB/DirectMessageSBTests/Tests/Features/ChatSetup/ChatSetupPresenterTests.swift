import Foundation
import XCTest

@testable import DirectMessageSB

fileprivate final class ChatSetupCoordinatorSpy: ChatSetupCoordinating {
    var viewController: UIViewController?

    private(set) var didCallPerformCount: Int = 0
    var calledAction: ChatSetupAction?

    func perform(action: ChatSetupAction) {
        didCallPerformCount += 1
        calledAction = action
    }
}

fileprivate final class ChatSetupDisplaySpy: ChatSetupDisplaying, ConnectionStateDisplayingDouble {
    var showConnectionErrorCount: Int = 0
    var startLoadingCount: Int = 0
    var stopLoadingCount: Int = 0
    var didRequestRetryConnectionCount: Int = 0
    var didRequestCancelConnectionCount: Int = 0
    var loadingText = String()
    var alert: ChatConnectionErrorAlert = .standard
    var connectionStateInteractor: ConnectionStateInteracting {
        ConnectionStateInteractorSpy()
    }
}

final class ChatSetupPresenterTests: XCTestCase, ConnectionStateStandardPresenterTestable {
    private lazy var displaySpy = ChatSetupDisplaySpy()
    private lazy var coordinatorSpy = ChatSetupCoordinatorSpy()
    private lazy var sut: ChatSetupPresenter = {
        let presenter = ChatSetupPresenter(coordinator: coordinatorSpy)
        presenter.viewController = displaySpy
        return presenter
    }()

    var connectionStateDisplaySpy: ConnectionStateDisplayingDouble? {
        displaySpy
    }
    var connectionStatePresenter: ConnectionStatePresenting {
        sut
    }

    func testCloseChat_ShouldCallPerformDismissOnCoordinator() {
        sut.closeChat()

        XCTAssertEqual(coordinatorSpy.didCallPerformCount, 1)
        switch coordinatorSpy.calledAction {
        case .dismiss:
            XCTAssert(true)
        default:
            XCTFail("Should call dismiss action")
        }
    }

    func testStartConnectionLoading_ShouldCallShowLoadingWithEmptyText() {
        assertStartConnectionLoading()
    }

    func testStopConnectionLoading_ShouldCallStopLoading() {
        assertStopConnectionLoading()
    }

    func testPresentConnectionError_ShouldCallShowConnectionErrorCount() {
        assertPresentConnectionError()
    }
}
