import AnalyticsModule
import SendBirdUIKit
import XCTest
@testable import DirectMessageSB

private struct MessageCellStub: MessageCell {
    var profileView = UIView()
    var userNameView = UIView()
    var messageTextView = UIView()
    var position: MessagePosition
    
    init(position: MessagePosition) {
        self.position = position
    }
}

private final class ChatPresenterSpy: ChatPresenting {
    var viewController: ChatDisplaying?

    private(set) lazy var didCallDidNextStepCount: Int = 0
    private(set) var calledAction: ChatAction?
    private(set) var user: ChatUser?
    private(set) var profileId = String()
    private(set) lazy var didCallSetupNavigationBarCount: Int = 0
    private(set) lazy var didCallSetupAdittionalButtonsCount: Int = 0
    private(set) lazy var didCallSetupEditionButtonsCount: Int = 0
    private(set) lazy var didDismissCount: Int = 0
    private(set) lazy var didOpenChatSettingsCount: Int = 0
    private(set) lazy var didOpenProfileCount: Int = 0

    func didNextStep(action: ChatAction) {
        didCallDidNextStepCount += 1
        calledAction = action
    }
    
    func setupNavigationBar(with otherUser: ChatUser) {
        self.user = otherUser
        didCallSetupNavigationBarCount += 1
    }

    func setupAdittionalButtons() {
        didCallSetupAdittionalButtonsCount += 1
    }
    
    func setupEditionButtons() {
        didCallSetupEditionButtonsCount += 1
    }

    func dismiss() {
        didDismissCount += 1
    }
    
    func openChatSettings(for channel: OneOnOneChatting) {
        didOpenChatSettingsCount += 1
    }
    
    func openProfile(withId id: String) {
        didOpenProfileCount += 1
        profileId = id
    }
}

final class ChatInteractorTests: XCTestCase {
    private lazy var chatApiManager = ChatApiManagerMock()
    private lazy var analyticsSpy = AnalyticsSpy()
    private lazy var presenterSpy = ChatPresenterSpy()
    private lazy var sut = createSut(for: Stubs.chat)

    func testInitialSetup_WhenOtherUserIsAvailable_ShouldCallLayoutSetup() throws {
        sut.initialSetup()

        XCTAssertEqual(presenterSpy.didCallSetupNavigationBarCount, 1)
    }

    func testInitialSetup_WhenOtherUserIsUnavailable_ShouldCallDismiss() {
        let chat = OneOnOneChat(channelUrl: "", currentUser: Stubs.currentChatUser, otherUser: nil)
        sut = createSut(for: chat)
        sut.initialSetup()

        XCTAssertEqual(presenterSpy.didCallSetupAdittionalButtonsCount, 0)
        XCTAssertEqual(presenterSpy.didCallSetupNavigationBarCount, 0)
        XCTAssertEqual(presenterSpy.didDismissCount, 1)
        XCTAssertNil(presenterSpy.user)
    }

    func testDismiss_ShouldCallDismiss() {
        sut.dismiss()

        XCTAssertEqual(presenterSpy.didDismissCount, 1)
    }
    
    func testOpenProfile_ShouldCallOpenProfile() {
        sut.openProfile()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        let expectedEvent =
            DirectMessageSBAnalytics.button(.clicked, .openProfile(onScreen: .privateChat)).event()
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(presenterSpy.didOpenProfileCount, 1)
    }
    
    func testOpenChatSettings_ShouldCallOpenChatSettings() {
        sut.openChatSettings()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        let expectedEvent =
            DirectMessageSBAnalytics.button(.clicked, .openSettings).event()
        XCTAssertEqual(presenterSpy.didOpenChatSettingsCount, 1)
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
    }

    func testSetScreenAsViewed_ShouldLogScreenViewedEvent() {
        sut.setScreenAsViewed()

        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        let expectedEvent = DirectMessageSBAnalytics.screenViewed(.privateChat).event()
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testLongPress_WhenLongPressedSentMessage_ShouldTrackSentMessageOptionsEvent() {
        let expectedEvent = DirectMessageSBAnalytics.dialogViewed(.sentMessageOptions).event()
        
        sut.didLongPressMessage(sentMessageCell)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testLongPress_WhenLongPressedReceivedMessage_ShouldTrackReceivedMessageOptionsEvent() {
        let expectedEvent = DirectMessageSBAnalytics.dialogViewed(.receivedMessageOptions).event()
        
        sut.didLongPressMessage(receivedMessageCell)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
    }

    func testSetSaveButtonAsTapped_ShouldLogSaveButtonEvent() {
        let expectedEvent = DirectMessageSBAnalytics.button(.clicked, .saveButton).event()

        sut.didTapSaveButton()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testSetCancelButtonAsTapped_ShouldLogCancelButtonEvent() {
        let expectedEvent = DirectMessageSBAnalytics.button(.clicked, .cancelButton).event()

        sut.didTapCancelButton()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
    }

    func testIsFromUrl_WhenChatMatchesUrl_ShouldReturnTrue() {
        XCTAssert(sut.isFromChannel(url: Stubs.chat.channelUrl))
    }

    func testIsFromUrl_WhenChatDoesNotMatchesUrl_ShouldReturnFalse() {
        XCTAssertFalse(sut.isFromChannel(url: String()))
    }

    func testCanOpenChat_WhenChatIsNotConnected_ShouldReturnFalse() {
        chatApiManager.connectionState = .closed

        XCTAssertFalse(sut.canOpenChat(url: Stubs.chat.channelUrl))
    }

    func testCanOpenChat_WhenChatIsConnected_ShouldReturnTrue() {
        chatApiManager.connectionState = .open

        XCTAssert(sut.canOpenChat(url: Stubs.chat.channelUrl))
    }

    func testOpenProfile_WhenOtherUserIdIsAvailable_ShouldCallOpenProfileOnPresenter() {
        sut.openProfile()

        XCTAssertEqual(presenterSpy.didOpenProfileCount, 1)
        XCTAssertEqual(presenterSpy.profileId, Stubs.chat.otherUser?.clientId.numbers)
    }

    func testOpenProfile_WhenOtherUserIdIsNotAvailable_ShouldNotCallOpenProfileOnPresenter() {
        let chat = OneOnOneChat(channelUrl: "", currentUser: nil, otherUser: nil)
        sut = createSut(for: chat)
        sut.openProfile()

        XCTAssertEqual(presenterSpy.didOpenProfileCount, 0)
        XCTAssert(presenterSpy.profileId.isEmpty)
    }
    
    func testDidTapSendButton_ShouldLogAnalytics() throws {
        // Given
        let chat = OneOnOneChat(channelUrl: "urlMock",
                                currentUser: Stubs.currentChatUser,
                                otherUser: Stubs.otherChatUser)
        let expectedEvent = DirectMessageSBAnalytics
            .interaction(.sendMessage(channelUrl: chat.channelUrl,
                                      senderUserId: Stubs.currentChatUser.clientId,
                                      targetUserId: Stubs.otherChatUser.clientId))
            .event()
        
        // When
        sut = createSut(for: chat)
        sut.didTapSendButton()
        
        // Then
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testDidTapCancelButton_ShouldLogAnalytics() throws {
        // Given
        let expectedEvent = DirectMessageSBAnalytics
            .button(.clicked, .cancelButton)
            .event()
        
        // When
        sut.didTapCancelButton()
        
        // Then
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testDidDismissScreen_WhenMovingFromParent_ShouldLogAnalytics() throws {
        // Given
        let expectedEvent = DirectMessageSBAnalytics
            .button(.clicked, .back(onScreen: .privateChat))
            .event()
        
        // When
        sut.didDismissScreen(isMovingFromParent: true)
        
        // Then
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testDidDismissScreen_WhenMovingFromParent_ShouldNotLogAnalytics() throws {
        sut.didDismissScreen(isMovingFromParent: false)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 0)
        XCTAssertNil(analyticsSpy.event)
    }
}

private extension ChatInteractorTests {
    func createSut(for chat: OneOnOneChatting) -> ChatInteractor {
        .init(channel: chat,
              presenter: presenterSpy,
              dependencies: DependenciesContainerMock(analyticsSpy, chatApiManager))
    }
}

private extension ChatInteractorTests {
    var sentMessageCell: MessageCellStub {
        .init(position: .right)
    }
    
    var receivedMessageCell: MessageCellStub {
        .init(position: .left)
    }
}
