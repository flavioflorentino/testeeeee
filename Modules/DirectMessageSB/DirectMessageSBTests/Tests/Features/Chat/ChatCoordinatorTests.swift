@testable import DirectMessageSB
import UI
import XCTest

final class ChatCoordinatorTests: XCTestCase {
    lazy var legacyMock = DMSBLegacySetupMock()
    lazy var dependenciesMock = DependenciesContainerMock(legacyMock)
    lazy var navigationControllerSpy = UINavigationControllerSpy()
    lazy var viewControllerMock = UIViewController()
    
    lazy var sut: ChatCoordinator = {
        let sut = ChatCoordinator(dependencies: dependenciesMock)
        navigationControllerSpy.setViewControllers([viewControllerMock], animated: false)
        sut.viewController = viewControllerMock
        return sut
    }()
    
    func testDismiss_ShouldPopNavigationController() {
        sut.perform(action: .dismiss)
        
        XCTAssertEqual(navigationControllerSpy.popViewControllerCallCount, 1)
        XCTAssertEqual(navigationControllerSpy.viewControllers, [viewControllerMock])
    }
    
    func testOpenProfile_ShouldOpenProfileViewController() {
        sut.perform(action: .openProfile(id: ""))
        
        XCTAssertEqual(navigationControllerSpy.pushViewControllerCallCount, 1)
        XCTAssertEqual(navigationControllerSpy.viewControllers.count, 2)
        XCTAssertTrue(navigationControllerSpy.viewControllers.last is FakeProfileViewController)
    }
    
    func testOpenSettings_ShouldOpenSettingsViewController() {
        sut.perform(action: .openChatSettings(channel: Stubs.chat))
        
        XCTAssertEqual(navigationControllerSpy.pushViewControllerCallCount, 1)
        XCTAssertEqual(navigationControllerSpy.viewControllers.count, 2)
        XCTAssertTrue(navigationControllerSpy.viewControllers.last is FakeChatSettingsViewController)
    }
}
