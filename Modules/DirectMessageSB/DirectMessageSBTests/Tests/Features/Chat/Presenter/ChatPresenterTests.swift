import Foundation
import XCTest
import SendBirdSDK
import SendBirdUIKit
@testable import DirectMessageSB

// MARK: - Doubles

private final class ChatCoordinatingSpy: ChatCoordinating {
    var viewController: UIViewController?

    private(set) var performActionReceivedInvocations: [ChatAction] = []

    func perform(action: ChatAction) {
        performActionReceivedInvocations.append(action)
    }
}

private final class ChatDisplayingSpy: SBUChannelViewController, ChatDisplaying { 
    // MARK: - SetupNavigationBar
    private(set) var setupNavigationBarTitleSubtitleImageURLCallsCount = 0
    typealias SetupNavigationBarParams = (title: String, subtitle: String, imageURL: String)
    private(set) var setupNavigationBarTitleSubtitleImageURLReceivedInvocations: SetupNavigationBarParams?

    func setupNavigationBar(title: String, subtitle: String, imageURL: String) {
        setupNavigationBarTitleSubtitleImageURLCallsCount += 1
        setupNavigationBarTitleSubtitleImageURLReceivedInvocations = (
            title: title,
            subtitle: subtitle,
            imageURL: imageURL
        )
    }

    // MARK: - SetupSaveButton
    private(set) var setupSaveButtonCallsCount = 0

    func setupSaveButton() {
        setupSaveButtonCallsCount += 1
    }

    // MARK: - SetupCancelButton
    private(set) var setupCancelButtonCallsCount = 0

    func setupCancelButton() {
        setupCancelButtonCallsCount += 1
    }

    // MARK: - RemoveAttachmentButton
    private(set) var removeAttachmentButtonCallsCount = 0

    func removeAttachmentButton() {
        removeAttachmentButtonCallsCount += 1
    }
}

// MARK: - Tests

final class ChatPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = ChatCoordinatingSpy()
    private lazy var displayingSpy = ChatDisplayingSpy()
    private lazy var sut = createSut()
    
    func testRemoveAttachmentButton_ShouldRemoveItFromViewController() {
        sut.setupAdittionalButtons()
        
        XCTAssertEqual(displayingSpy.removeAttachmentButtonCallsCount, 1)
    }
    
    func testSetupNavigationBar_ShouldSetupNavigationBarWithNicknameAndImage() throws {
        let expectedUser = Stubs.otherChatUser
        
        sut.setupNavigationBar(with: expectedUser)
        
        XCTAssertEqual(displayingSpy.setupNavigationBarTitleSubtitleImageURLCallsCount, 1)

        let settedNavigationBarUser = try XCTUnwrap(
            displayingSpy.setupNavigationBarTitleSubtitleImageURLReceivedInvocations
        )
        XCTAssertEqual(settedNavigationBarUser.title, try XCTUnwrap(expectedUser.profileName))
        XCTAssertEqual(settedNavigationBarUser.subtitle, try "@\(XCTUnwrap(expectedUser.username))")
        XCTAssertEqual(settedNavigationBarUser.imageURL, try XCTUnwrap(expectedUser.avatarUrl))
    }
    
    func testSetupEditionButtons_WhenInitiatingViewController_ShouldSetupSaveAndCancelButton() {
        sut.setupEditionButtons()
        
        XCTAssertEqual(displayingSpy.setupSaveButtonCallsCount, 1)
        XCTAssertEqual(displayingSpy.setupCancelButtonCallsCount, 1)
    }
    
    func testDismiss_ShouldPerformCoordinatorDismissAction() {
        sut.dismiss()
        
        XCTAssertEqual(coordinatorSpy.performActionReceivedInvocations, [.dismiss])
    }
    
    func testOpenProfile_ShouldPerformCoordinatorOpenProfileAction() {
        sut.openProfile(withId: Stubs.otherChatUser.clientId)
        
        XCTAssertEqual(coordinatorSpy.performActionReceivedInvocations, [.openProfile(id: Stubs.otherChatUser.clientId)])
    }
}

private extension ChatPresenterTests {
    func createSut() -> ChatPresenter {
        let sut = ChatPresenter(coordinator: coordinatorSpy)
        sut.viewController = displayingSpy
        return sut
    }
}
