import Foundation
import XCTest
@testable import DirectMessageSB

final class ConnectionStateServiceTests: XCTestCase {
    lazy var chatApiManagerMock = ChatApiManagerMock()
    private lazy var sut = ConnectionStateService(dependencies: DependenciesContainerMock(chatApiManagerMock))

    func testCancelActionForConnectionState_ShouldCallCancelActionForConnectionStateOnChatApiManager() {
        assertCancelActionForConnectionState()
    }

    func testSetActionForConnectionState_ShouldCallSetActionForConnectionStateOnChatApiManager() {
        assertSetActionForConnectionState()
    }

    func testReconnect_ShouldCallReconnectOnChatApiManager() {
        assertReconnect()
    }
}

extension ConnectionStateServiceTests: ConnectionStateStandardServiceTestable {
    var connectionStateService: ConnectionStateServicing {
        sut
    }
}
