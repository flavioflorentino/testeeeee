import AnalyticsModule
import Foundation
import XCTest
@testable import DirectMessageSB

private final class ConnectionStateSuccessDelegateSpy: ConnectionStateSuccessDelegate {
    var didSucceedConnectionCount: Int = 0

    func didSucceedConnection() {
        didSucceedConnectionCount += 1
    }
}

final class ConnectionStateInteractorTests: XCTestCase {
    private lazy var successDelegateSpy = ConnectionStateSuccessDelegateSpy()
    lazy var connectionStateServiceMock: ConnectionStateServicingDouble = ConnectionStateServiceMock()
    lazy var connectionStatePresenterSpy: ConnectionStatePresentingDouble = ConnectionStatePresenterSpy()
    private lazy var analyticsSpy = AnalyticsSpy()
    private lazy var dependenciesContainerMock = DependenciesContainerMock(analyticsSpy)
    private lazy var sut: ConnectionStateInteractor = {
        let interactor = ConnectionStateInteractor(service: connectionStateServiceMock,
                                                   presenter: connectionStatePresenterSpy,
                                                   dependencies: dependenciesContainerMock)
        interactor.delegate = successDelegateSpy
        return interactor
    }()

    func testSetConnectionActions_WhenConnectionStateIsConnecting_ShouldNotPerformSuccessAction() {
        setConnection(state: .connecting)

        sut.setConnectionActions()

        assertForCurrentState()
        XCTAssertEqual(successDelegateSpy.didSucceedConnectionCount, 0)
    }

    func testSetConnectionActions_WhenConnectionStateIsConnected_ShouldPerformSuccessAction() {
        setConnection(state: .open)

        sut.setConnectionActions()

        assertForCurrentState()
        XCTAssertEqual(successDelegateSpy.didSucceedConnectionCount, 1)
    }

    func testSetConnectionActions_WhenConnectionStateIsNotConnected_ShouldNotPerformSuccessAction() {
        setConnection(state: .failure)

        sut.setConnectionActions()

        assertForCurrentState()
        XCTAssertEqual(successDelegateSpy.didSucceedConnectionCount, 0)
    }

    func testDidCancelAction_ShouldStopLoadingAndResetActions() {
        assertDidCancelAction()
    }

    func testReconnect_ShouldCallReconnectOnServerAndResetActions() {
        assertReconnect()
    }
    
    func testDidFailConnection_WhenHavingTrackableScreen_ShouldLogAnalytics() throws {
        // Given
        setConnection(state: .closed)
        let expectedScreen: TrackedScreen = .home
        let expectedTrackedDialog: TrackedDialog = .connectionError(onScreen: expectedScreen, withMessage: "")
        let expectedEvent: DirectMessageSBAnalytics = .dialogViewed(expectedTrackedDialog)
        
        // When
        sut.trackableScreen = expectedScreen
        sut.setConnectionActions()
        
        // Then
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        
        let analyticsSpiedProperties = try XCTUnwrap(analyticsSpy.event?.properties as? [String: String])
        XCTAssertEqual(analyticsSpy.event?.name, expectedEvent.name)
        
        let screenName = try XCTUnwrap(analyticsSpiedProperties[TrackedScreen.Property.screenName.rawValue])
        XCTAssertEqual(screenName, expectedTrackedDialog.screen.name)
        
        let dialogName = try XCTUnwrap(analyticsSpiedProperties[TrackedDialog.Property.dialogName.rawValue])
        XCTAssertEqual(dialogName, expectedTrackedDialog.name)
        
        let businessContext = try XCTUnwrap(analyticsSpiedProperties[BusinessContext.Property.businessContext.rawValue])
        XCTAssertEqual(businessContext, expectedTrackedDialog.screen.context.rawValue)
    }
    
    func testDidFailConnection_WithoutHavingTrackableScreen_ShouldNotLogAnalytics() throws {
        // Given
        setConnection(state: .closed)
        sut.trackableScreen = nil
        
        // When
        sut.setConnectionActions()
        
        // Then
        XCTAssertEqual(analyticsSpy.logCalledCount, 0)
        XCTAssertNil(analyticsSpy.event)
    }
    
    func testLogConnectionErrorSelectedTryAgainOption_ShouldLogAnalytics() throws {
        // Given
        let expectedTrackedDialog: TrackedDialog = .connectionError(
            onScreen: .chatSetup,
            withMessage: connectionStatePresenterSpy.connectionErrorAlert.subtitle
        )
        
        let expectedOptionSelected: TrackedDialogOption = .tryAgain(onDialog: expectedTrackedDialog)
        
        let expectedEvent: DirectMessageSBAnalytics = .dialogOptionSelected(
            expectedOptionSelected
        )
        
        sut.trackableDialog = expectedTrackedDialog
        sut.logConnectionErrorDialog()
        
        // When
        sut.logConnectionErrorSelectedTryAgainOption()
        
        // Then
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        
        let analyticsSpiedProperties = try XCTUnwrap(analyticsSpy.event?.properties as? [String: String])
        XCTAssertEqual(analyticsSpy.event?.name, expectedEvent.name)
        
        let screenName = try XCTUnwrap(analyticsSpiedProperties[TrackedScreen.Property.screenName.rawValue])
        XCTAssertEqual(screenName, expectedTrackedDialog.screen.name)
        
        let dialogName = try XCTUnwrap(analyticsSpiedProperties[TrackedDialog.Property.dialogName.rawValue])
        XCTAssertEqual(dialogName, expectedTrackedDialog.name)
        
        let optionSelected = try XCTUnwrap(analyticsSpiedProperties[TrackedDialogOption.Property.optionSelected.rawValue])
        XCTAssertEqual(optionSelected, expectedOptionSelected.name)
    }
    
    func testLogConnectionErrorSelectedCancelOption_ShouldLogAnalytics() throws {
        // Given
        let expectedTrackedDialog: TrackedDialog = .connectionError(
            onScreen: .chatSetup,
            withMessage: connectionStatePresenterSpy.connectionErrorAlert.subtitle
        )
        
        let expectedOptionSelected: TrackedDialogOption = .cancel(onDialog: expectedTrackedDialog)
        
        let expectedEvent: DirectMessageSBAnalytics = .dialogOptionSelected(
            expectedOptionSelected
        )
        
        sut.trackableDialog = expectedTrackedDialog
        sut.logConnectionErrorDialog()
        
        // When
        sut.logConnectionErrorSelectedCancelOption()
        
        // Then
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        
        let analyticsSpiedProperties = try XCTUnwrap(analyticsSpy.event?.properties as? [String: String])
        XCTAssertEqual(analyticsSpy.event?.name, expectedEvent.name)
        
        let dialogName = try XCTUnwrap(analyticsSpiedProperties[TrackedDialog.Property.dialogName.rawValue])
        XCTAssertEqual(dialogName, expectedTrackedDialog.name)
        
        let optionSelected = try XCTUnwrap(analyticsSpiedProperties[TrackedDialogOption.Property.optionSelected.rawValue])
        XCTAssertEqual(optionSelected, expectedOptionSelected.name)
    }
}

extension ConnectionStateInteractorTests: ConnectionStateStandardInteractorTestable {
    var connectionStateInteractor: ConnectionStateInteracting {
        sut
    }
}
