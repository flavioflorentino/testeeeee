import Foundation
import XCTest
@testable import DirectMessageSB

final class ConnectionStatePresenterTests: XCTestCase {
    lazy var connectionStateDisplaySpy: ConnectionStateDisplayingDouble? = ConnectionStateDisplaySpy()
    private lazy var sut: ConnectionStatePresenter = {
        let presenter = ConnectionStatePresenter()
        presenter.connectionStateDisplay = connectionStateDisplaySpy
        return presenter
    }()

    func testStartConnectionLoading_ShouldCallShowLoadingWithEmptyText() {
        assertStartConnectionLoading()
    }

    func testStopConnectionLoading_ShouldCallStopLoading() {
        assertStopConnectionLoading()
    }

    func testPresentConnectionError_ShouldCallShowConnectionErrorCount() {
        assertPresentConnectionError()
    }
}

extension ConnectionStatePresenterTests: ConnectionStateStandardPresenterTestable {
    var connectionStatePresenter: ConnectionStatePresenting {
        sut
    }
}
