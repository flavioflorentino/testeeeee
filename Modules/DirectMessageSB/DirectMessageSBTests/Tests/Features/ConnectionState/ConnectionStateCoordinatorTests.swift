import Foundation
import XCTest
@testable import DirectMessageSB

private final class ChatConnectionStateViewController: ConnectionStateViewController {
    var loadingText = String()
    var alert: ChatConnectionErrorAlert = .standard
    var showConnectionErrorCount: Int = 0
    var startLoadingCount: Int = 0
    var stopLoadingCount: Int = 0

    func startApolloLoader(loadingText: String) {
        startLoadingCount += 1
        self.loadingText = loadingText
    }

    func stopApolloLoader(completion: (() -> Void)?) {
        stopLoadingCount += 1
        completion?()
    }
}

final class ConnectionStateCoordinatorTests: XCTestCase {
    private lazy var viewControllerSpy = ChatConnectionStateViewController()
    private lazy var interactor = ConnectionStateInteractorSpy()
    private lazy var sut: ConnectionStateCoordinator = {
        let coordinator = ConnectionStateCoordinator(interactor: interactor)
        coordinator.viewController = viewControllerSpy
        return coordinator
    }()

    func testStart_ShouldCallSetConnectionActionsOnInteractor() {
        sut.start()

        XCTAssertEqual(interactor.setConnectionActionsCount, 1)
    }

    func testStartLoading_ShouldCallStarLoadingOnViewController() {
        sut.startLoading()

        XCTAssertEqual(viewControllerSpy.startLoadingCount, 1)
    }

    func testStopLoading_ShouldCallStopLoadingOnViewController() {
        sut.stopLoading()

        XCTAssertEqual(viewControllerSpy.stopLoadingCount, 1)
    }

    func testDidRequestRetryConnection_ShouldCallRetryOnInteractor() {
        sut.didRequestRetryConnection()

        XCTAssertEqual(interactor.reconnectCount, 1)
    }
}
