import Foundation
import XCTest
@testable import DirectMessageSB

final class ChatNotificationDataMapperTests: XCTestCase {
    lazy var sut: ChatNotificationDataMapping = ChatNotificationDataMapper()
    lazy var mockChatNotificationData: Data? = Mocker().data("sendbird-chatnotification-101")
    
    func testIsChatNotification_WhenDictHaveSendbirdKey_ShouldReturnTrue() {
        let dictionary: [AnyHashable: Any] = [
            "sendbird": ""
        ]
        
        XCTAssertTrue(sut.isChatNotification(dictionary))
    }
    
    func testIsChatNotification_WhenDictDoesntHaveSendbirdKey_ShouldReturnFalse() {
        let dictionary: [AnyHashable: Any] = [
            "chat": ""
        ]
        
        XCTAssertFalse(sut.isChatNotification(dictionary))
    }
    
    func testDecodeFromData_ShouldDecodeValidData() throws {
        // Given
        let expectedChannel = ChatNotification.Channel(
            channelURL: "sendbird_group_channel_120244530_9eaed88b26e1cdd3733cca56ab247ec94402a1ed",
            name: "Group Channel",
            customType: ""
        )
        
        let expectedSender = ChatNotification.Sender(
            id: "C17",
            name: "joao",
            profileURL: "https://picpay-dev.s3.amazonaws.com/profiles/da69e6085cbd31f787c943036f8583d2.jpg"
        )
        
        let expectedRecipient = ChatNotification.Recipient(
            id: "C3",
            name: "anderson",
            pushTemplate: "default"
        )
        
        // When
        let mockData = try XCTUnwrap(mockChatNotificationData)
        let result = sut.decode(fromData: mockData)
        
        // Then
        XCTAssertEqual(result?.category, "messaging:offline_notification")
        XCTAssertEqual(result?.channelType, "messaging")
        XCTAssertEqual(result?.pushSound, "default")
        XCTAssertEqual(result?.appID, "FFCC64B1-2705-4751-95A0-89C2439DEBBE")
        XCTAssertEqual(result?.type, "MESG")
        XCTAssertEqual(result?.message, "12345")
        XCTAssertEqual(result?.messageID, 6_653_874_862)
        XCTAssertEqual(result?.audienceType, "only")
        XCTAssertEqual(result?.customType, "")
        XCTAssertEqual(result?.unreadMessageCount, 8)
        XCTAssertNotNil(result?.createdAt)
        XCTAssertEqual(result?.channel, expectedChannel)
        XCTAssertEqual(result?.sender, expectedSender)
        XCTAssertEqual(result?.recipient, expectedRecipient)
    }
    
    func testDecodeFromDictionary_ShouldDecodeValidData() throws {
        let dictionary: [AnyHashable: Any] = [
            "sendbird": [
                "type": "MESG",
                "message": "teste"
            ]
        ]
        
        let result = sut.decode(fromDictionary: dictionary)
        
        XCTAssertEqual(result?.type, "MESG")
        XCTAssertEqual(result?.message, "teste")
    }
}
