import Foundation
import XCTest
@testable import DirectMessageSB

final class ChatNotificationBannerViewModelTest: XCTestCase {
    func testInit_WhenViewModelHasBeenCreated_ShouldRetrieveValuesFormatted() throws {
        let sut = try createSut(from: "sendbird-chatnotification-101") { _ in }
        
        XCTAssertEqual(sut.title, "@joao💬")
        XCTAssertEqual(sut.message, "12345")
        XCTAssertEqual(sut.secondsToDismiss, 5)
        XCTAssertEqual(
            sut.photoUrl,
            URL(string: "https://picpay-dev.s3.amazonaws.com/profiles/da69e6085cbd31f787c943036f8583d2.jpg")
        )
    }
    
    func testOnTapAction_ShouldCallGivenAction() throws {
        // Given
        let tapExpected = expectation(description: "tappedOnBanner")
        var retrievedBannerView: ChatNotificationBannerView?
        let sut = try createSut(from: "sendbird-chatnotification-101") { bannerView in
            retrievedBannerView = bannerView
            tapExpected.fulfill()
        }
        let tappedBannerView = ChatNotificationBannerView(viewModel: sut)
        
        // When
        tappedBannerView.didTap()
        
        // Then
        wait(for: [tapExpected], timeout: 1)
        XCTAssertEqual(retrievedBannerView, tappedBannerView)
    }
}

private extension ChatNotificationBannerViewModelTest {
    func createSut(
        from jsonMockFilename: String,
        onTapAction: @escaping ((ChatNotificationBannerView) -> Void)
    ) throws -> ChatNotificationBannerViewModel {
        let mockNotificationData = try XCTUnwrap(
            Mocker().data(jsonMockFilename)
        )
        let chatNotification = try XCTUnwrap(
            ChatNotification(data: mockNotificationData)
        )
        return ChatNotificationBannerViewModel(chatNotification, onTapAction: onTapAction)
    }
}
