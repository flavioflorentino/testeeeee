// swiftlint:disable discouraged_optional_collection
@testable import DirectMessageSB
import XCTest

final class ChatNotifierTests: XCTestCase {
    private lazy var notificationCenterSpy = NotificationCenterSpy()
    private lazy var sut = ChatNotifier(notificationCenter: notificationCenterSpy)
    
    func testPostUpdateUnreadMessages_ShouldPostWithNotificationCenter() throws {
        sut.postUpdateUnreadMessages(count: 42)

        let postedNotification = try XCTUnwrap(notificationCenterSpy.postNameObjectUserInfoReceivedInvocations.first)
        let postedNotificationUserInfo = try XCTUnwrap(postedNotification.aUserInfo?.first)

        XCTAssertEqual(notificationCenterSpy.postNameObjectUserInfoCallsCount, 1)
        XCTAssertEqual(postedNotification.aName, ChatNotifier.NotificationName.didUpdateUnreadMessagesCount.notificationName)
        XCTAssertNil(postedNotification.anObject)
        XCTAssertEqual(postedNotificationUserInfo.key, ChatNotifier.NotificationUserInfoKey.totalCount)
        XCTAssertEqual(postedNotificationUserInfo.value as? Int, 42)
    }
}

private final class NotificationCenterSpy: NotificationCenterType {
    // MARK: - Post
    struct PostNotificationParams {
        let aName: NSNotification.Name
        let anObject: Any?
        let aUserInfo: [AnyHashable: Any]?
    }
    private(set) var postNameObjectUserInfoCallsCount = 0
    private(set) var postNameObjectUserInfoReceivedInvocations: [PostNotificationParams] = []

    func post(name aName: NSNotification.Name, object anObject: Any?, userInfo aUserInfo: [AnyHashable: Any]?) {
        postNameObjectUserInfoCallsCount += 1
        postNameObjectUserInfoReceivedInvocations.append(.init(aName: aName, anObject: anObject, aUserInfo: aUserInfo))
    }
}
// swiftlint:enable discouraged_optional_collection
