import XCTest
@testable import DirectMessageSB

final class MessageButtonViewModelDelegateSpy: MessageButtonViewModelDelegate {
    private(set) var isBadgeVisible: Bool = false
    private(set) var badgeContent: String = ""
    
    private(set) var didCallChangeBadgeVisibilityCount = 0
    private(set) var didCallChangeBadgeTextCount = 0
    
    func changeBadgeVisibility(isVisible: Bool) {
        self.isBadgeVisible = isVisible
        didCallChangeBadgeVisibilityCount += 1
    }
    
    func changeBadgeText(content: String) {
        self.badgeContent = content
        didCallChangeBadgeTextCount += 1
    }
}

final class MessageButtonViewModelTests: XCTestCase {
    let maximumUnreadMessages = 999
    
    private lazy var delegateSpy = MessageButtonViewModelDelegateSpy()
    private lazy var sut = MessageButtonViewModel(delegate: delegateSpy, maximumUnreadMessages: maximumUnreadMessages)
    
    func testSetupButton_WhenThereAreNoUnreadMessages_ShouldHideAndCleanBadge() {
        sut.setupButton(unreadMessages: 0)
        
        XCTAssertFalse(delegateSpy.isBadgeVisible)
        XCTAssertEqual(delegateSpy.badgeContent, "")
        
        XCTAssertEqual(delegateSpy.didCallChangeBadgeVisibilityCount, 1)
        XCTAssertEqual(delegateSpy.didCallChangeBadgeTextCount, 1)
    }
    
    func testSetupButton_WhenThereAreUnreadMessages_ShouldShowAndFillBadge() {
        let unreadMessages: Int = 10
        sut.setupButton(unreadMessages: unreadMessages)
        
        XCTAssertTrue(delegateSpy.isBadgeVisible)
        XCTAssertEqual(delegateSpy.badgeContent, String(unreadMessages))
        
        XCTAssertEqual(delegateSpy.didCallChangeBadgeVisibilityCount, 1)
        XCTAssertEqual(delegateSpy.didCallChangeBadgeTextCount, 1)
    }
    
    func testSetupButton_WhenThereAreMaximumUnreadMessages_ShouldShowAndFillBadge() {
        sut.setupButton(unreadMessages: maximumUnreadMessages)
        
        XCTAssertTrue(delegateSpy.isBadgeVisible)
        XCTAssertEqual(delegateSpy.badgeContent, String(maximumUnreadMessages))
        
        XCTAssertEqual(delegateSpy.didCallChangeBadgeVisibilityCount, 1)
        XCTAssertEqual(delegateSpy.didCallChangeBadgeTextCount, 1)
    }
    
    func testSetupButton_WhenThereAreMoreThanMaximumUnreadMessages_ShouldShowAndFillBadge() {
        sut.setupButton(unreadMessages: maximumUnreadMessages + 1)
        
        XCTAssertTrue(delegateSpy.isBadgeVisible)
        XCTAssertEqual(delegateSpy.badgeContent, "\(maximumUnreadMessages)+")
        
        XCTAssertEqual(delegateSpy.didCallChangeBadgeVisibilityCount, 1)
        XCTAssertEqual(delegateSpy.didCallChangeBadgeTextCount, 1)
    }
}
