import XCTest

@testable import DirectMessageSB

protocol ConnectionStateStandardServiceTestable {
    var connectionStateService: ConnectionStateServicing { get }
    var chatApiManagerMock: ChatApiManagerMock { get }
}

extension ConnectionStateStandardServiceTestable where Self: XCTestCase {
    func assertCancelActionForConnectionState() {
        connectionStateService.cancelActionForConnectionState()

        XCTAssertEqual(chatApiManagerMock.didCancelActionForConnectionStateCount, 1)
    }

    func assertSetActionForConnectionState() {
        let setActionExpectation = expectation(description: "Should retrieve expected unread messages count")

        connectionStateService.setActionForConnectionState { state in
            XCTAssertEqual(self.chatApiManagerMock.connectionState, state)
            setActionExpectation.fulfill()
        }
        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(chatApiManagerMock.didSetActionForConnectionStateCount, 1)
    }

    func assertReconnect() {
        connectionStateService.reconnect()

        XCTAssertEqual(chatApiManagerMock.didConnectCount, 1)
    }
}

protocol ConnectionStateStandardInteractorTestable {
    var connectionStateInteractor: ConnectionStateInteracting { get }
    var connectionStatePresenterSpy: ConnectionStatePresentingDouble { get }
    var connectionStateServiceMock: ConnectionStateServicingDouble { get }
}

extension ConnectionStateStandardInteractorTestable where Self: XCTestCase {
    func assertDidCancelAction() {
        connectionStateInteractor.didCancelAction()

        XCTAssertEqual(connectionStateServiceMock.cancelActionForConnectionStateCount, 1)
        XCTAssertEqual(connectionStatePresenterSpy.stopConnectionLoadingCount, 1)
    }

    func assertReconnect() {
        connectionStateInteractor.reconnect()

        XCTAssertEqual(connectionStateServiceMock.reconnectCount, 1)
        XCTAssertEqual(connectionStateServiceMock.setActionForConnectionStateCount, 1)
    }

    func setConnection(state: ConnectionState) {
        connectionStateServiceMock.expectedConnectionState = state
    }

    func assertForCurrentState() {
        XCTAssertEqual(connectionStateServiceMock.setActionForConnectionStateCount, 1)
        XCTAssertEqual(connectionStatePresenterSpy.startConnectionLoadingCount,
                       connectionStateServiceMock.expectedConnectionState == .connecting ? 1 : 0)
        XCTAssertEqual(connectionStatePresenterSpy.stopConnectionLoadingCount,
                       connectionStateServiceMock.expectedConnectionState != .connecting ? 1 : 0)
        XCTAssertEqual(connectionStatePresenterSpy.presentConnectionErrorCount,
                       connectionStateServiceMock.expectedConnectionState.shouldPresentError ? 1 : 0)
    }
}

protocol ConnectionStateStandardPresenterTestable {
    var connectionStatePresenter: ConnectionStatePresenting { get }
    var connectionStateDisplaySpy: ConnectionStateDisplayingDouble? { get }
}

extension ConnectionStateStandardPresenterTestable where Self: XCTestCase {
    func assertStartConnectionLoading() {
        connectionStatePresenter.startConnectionLoading()

        XCTAssertEqual(connectionStateDisplaySpy?.startLoadingCount, 1)
        XCTAssertEqual(connectionStateDisplaySpy?.loadingText, String())
    }

    func assertStopConnectionLoading() {
        let stopLoadingExpectation = expectation(description: "Should stop loading")
        connectionStatePresenter.stopConnectionLoading {
            stopLoadingExpectation.fulfill()
        }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(connectionStateDisplaySpy?.stopLoadingCount, 1)
    }

    func assertPresentConnectionError() {
        connectionStatePresenter.presentConnectionError()

        XCTAssertEqual(connectionStateDisplaySpy?.showConnectionErrorCount, 1)
        XCTAssertEqual(connectionStateDisplaySpy?.alert, .standard)
    }
}

fileprivate extension ConnectionState {
    var shouldPresentError: Bool {
        switch self {
        case .failure, .closed:
            return true
        default:
            return false
        }
    }
}
