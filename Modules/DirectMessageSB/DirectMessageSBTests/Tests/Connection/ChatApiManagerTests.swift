import XCTest
@testable import DirectMessageSB

final class ChatApiManagerTests: XCTestCase {
    private lazy var sendBirdClientMock = SendBirdClientMock()
    private lazy var configurationMock = ChatConnectionConfigurationMock()
    private lazy var dependenciesMock = DependenciesContainerMock()
    private lazy var sut = ChatApiManager(client: sendBirdClientMock,
                                          configuration: configurationMock,
                                          dependencies: dependenciesMock)

    func testSetup_ShouldCallSetupOnClient() {
        let setupExpectation = expectation(description: "Setup should be called")

        sut.setup { _ in
            setupExpectation.fulfill()
        }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(sendBirdClientMock.didSetupCount, 1)
    }
    
    func testConnection_WhenConnectionSucceed_ShouldCallUpdateCurrentUserInfo() {
        sendBirdClientMock.connectExpectedResult = .success(())
        let connectExpectation = expectation(description: "The connection should succeed")

        sut.connect { result in
            switch result {
            case .success:
                XCTAssert(true)
            case .failure:
                XCTFail("Connection is expected to succeed")
            }
            connectExpectation.fulfill()
        }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(sendBirdClientMock.didSetupCount, 0)
        XCTAssertEqual(sut.connectionState, sendBirdClientMock.connectionState)
        XCTAssertNil(sut.didFinishConnection)
    }

    func testConnection_WhenConnectionFails_ShouldCallUpdateCurrentUserInfo() {
        sendBirdClientMock.connectExpectedResult = .failure(MessagingClientError.nullUser)
        let connectExpectation = expectation(description: "The connection should fail")

        sut.connect { result in
            switch result {
            case .success:
                XCTFail("Connection is expected to fail")
            case .failure:
                XCTAssert(true)
            }
            connectExpectation.fulfill()
        }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(sendBirdClientMock.didSetupCount, 0)
        XCTAssertEqual(sut.connectionState, .failure)
        XCTAssertNil(sut.didFinishConnection)
    }
    
    func testRegisterDeviceToken_ShouldRegisterWithSendBirdClient() {
        sut.registerDeviceToken()
        
        XCTAssertEqual(self.sendBirdClientMock.didRegisterDeviceTokenCount, 1)
    }
    
    func testAddPushDeviceToken_ShouldPushDeviceTokenWithSendBirdClient() {
        sut.addPushDeviceToken(Data())
        
        XCTAssertEqual(sendBirdClientMock.didAddPushDeviceToken, 1)
    }
    
    func testGetPushNotificationOption_ShouldSuccessfullyGetCurrentNotificationOption() {
        let getPushNotificationOptionExpectation = expectation(description: "Should get notification option")
        let expectedOption: PushNotificationOption = .all
        
        sendBirdClientMock.expectedGetNotificationOptionResult = .success(expectedOption)
        
        sut.getPushNotificationOption { result in
            switch result {
            case let .success(option):
                XCTAssertEqual(option, expectedOption)
            case .failure:
                XCTFail("Get push notification option should succeed")
            }
            getPushNotificationOptionExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(sendBirdClientMock.didGetPushNotificationOptionCount, 1)
    }
    
    func testGetPushNotificationOption_ShouldFailToGetCurrentNotificationOption() {
        let getPushNotificationOptionExpectation = expectation(description: "Should fail to get notification option")
        
        sendBirdClientMock.expectedGetNotificationOptionResult = .failure(Stubs.genericMessagingClientError)
        
        sut.getPushNotificationOption { result in
            if case .success = result {
                XCTFail("Get push notification option should fail")
            }
            getPushNotificationOptionExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(sendBirdClientMock.didGetPushNotificationOptionCount, 1)
    }
    
    func testSetPushNotificationOption_ShouldSuccessfullySetNewNotificationOption() {
        let setPushNotificationOptionExpectation = expectation(description: "Should get notification option")
        
        sendBirdClientMock.expectedSetNotificationOptionResult = .success(())
        
        sut.setPushNotificationOption(.all) { result in
            if case .failure = result {
                XCTFail("Set push notification option should succeed")
            }
            setPushNotificationOptionExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(sendBirdClientMock.didSetPushNotificationOptionCount, 1)
    }
    
    func testSetPushNotificationOption_ShouldFailToSetNewNotificationOption() {
        let setPushNotificationOptionExpectation = expectation(description: "Should fail to get notification option")
        
        sendBirdClientMock.expectedSetNotificationOptionResult = .failure(Stubs.genericMessagingClientError)
        
        sut.setPushNotificationOption(.all) { result in
            if case .success = result {
                XCTFail("Set push notification option should fail")
            }
            setPushNotificationOptionExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(sendBirdClientMock.didSetPushNotificationOptionCount, 1)
    }
    
    func testDisconnect_ShouldDisconnectWithSendBirdClient() {
        let disconnectExpectation = expectation(description: "The disconnection should fail")
        sut.disconnect {
            disconnectExpectation.fulfill()
        }
        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(self.sendBirdClientMock.didDisconnectCount, 1)
    }
    
    func testFetchUnreadMessages_ShouldFetchUnreadMessagesCount() {
        let fetchUnreadMessagesExpectation = expectation(description: "Should fetch unread messages count")
        let expectedUnreadMessages: Int = 2
        var receivedUnreadMessages: Int = 0
        sendBirdClientMock.expectedUnreadMessagesCount = expectedUnreadMessages
        
        sut.fetchUnreadMessages { unreadMessages in
            receivedUnreadMessages = unreadMessages
            fetchUnreadMessagesExpectation.fulfill()
        }
        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(sendBirdClientMock.didFetchUnreadMessagesCalledCount, 1)
        XCTAssertEqual(sendBirdClientMock.expectedUnreadMessagesCount, receivedUnreadMessages)
    }
    
    func testConnectionState_ShouldRetrieveValueFromClient() {
        sendBirdClientMock.connectionState = .open
        XCTAssertEqual(sut.connectionState, .open)
    }

    func testClientId_ShouldRetrieveValueFromConfiguration() {
        XCTAssertEqual(sut.clientId, configurationMock.clientId)
    }

    func testCanReachUser_ShouldCallClientCorrespondingMethod() {
        let clientExpectation = expectation(description: "Should call client")
        sut.canReach(user: Stubs.currentChatUser) { _ in
            clientExpectation.fulfill()
        }
        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(sendBirdClientMock.didCanReachUserCount, 1)
    }

    func testCanReachUsers_ShouldCallClientCorrespondingMethod() {
        let clientExpectation = expectation(description: "Should call client")
        sut.canReach(users: [Stubs.currentChatUser]) { _ in
            clientExpectation.fulfill()
        }
        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(sendBirdClientMock.didCanReachUsersCount, 1)
    }

    func testSetConnection_WhenOpen_ShouldCompleteWithUpdatedCurrentState() {
        sendBirdClientMock.connectionState = .open
        let connectionStateExpectation = expectation(description: "Should complete with .open")
        sut.setActionForConnectionState { state in
            XCTAssertEqual(state, .open)
            connectionStateExpectation.fulfill()
        }
        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(sendBirdClientMock.didConnectionStateCount, 2)
        XCTAssertNil(sut.didFinishConnection)
    }

    func testSetConnection_WhenNotOpen_ShouldCompleteWithCurrentStateAndSetAction() {
        sendBirdClientMock.connectionState = .failure
        let connectionStateExpectation = expectation(description: "Should complete with current state and set action")
        sut.setActionForConnectionState { state in
            XCTAssertEqual(state, .failure)
            connectionStateExpectation.fulfill()
        }
        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(sendBirdClientMock.didConnectionStateCount, 1)
        XCTAssertNotNil(sut.didFinishConnection)
    }

    func testCancelActionForConnectionState_ShouldSetDidFinishConnectionAsNil() {
        sut.cancelActionForConnectionState()
        XCTAssertNil(sut.didFinishConnection)
    }

    func testGetCurrentConnectionState_WhenStateIsNotOpen_ShouldJustReturnCurrentStateFromApiManager() {
        sendBirdClientMock.connectionState = .closed
        sut = createSut()

        XCTAssertEqual(sut.getCurrentConnectionState(), .closed)
        XCTAssertEqual(sendBirdClientMock.didConnectionStateCount, 1)
    }

    func testGetCurrentConnectionState_WhenStateIsOpen_ShouldUpdateCurrentStateUsingClient() {
        sendBirdClientMock.connectionState = .open
        sut = createSut()

        XCTAssertEqual(sut.getCurrentConnectionState(), .open)
        XCTAssertEqual(sendBirdClientMock.didConnectionStateCount, 2)
    }

    func testDidFailReconnection_ShouldUpdateConnectionStateToFailure() {
        sut.didFailReconnection()

        XCTAssertEqual(sut.connectionState, .failure)
        XCTAssertNil(sut.didFinishConnection)
    }

    func testDidSucceedReconnection_ShouldUpdateConnectionStateToFailure() {
        sut.didSucceedReconnection()

        XCTAssertEqual(sut.connectionState, sendBirdClientMock.connectionState)
        XCTAssertNil(sut.didFinishConnection)
    }
}

extension ChatApiManagerTests {
    func createSut() -> ChatApiManager {
        ChatApiManager(client: sendBirdClientMock, configuration: configurationMock, dependencies: dependenciesMock)
    }
}
