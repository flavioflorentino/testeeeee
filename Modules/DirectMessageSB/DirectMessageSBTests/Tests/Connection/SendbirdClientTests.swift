import Core
@testable import DirectMessageSB
import FeatureFlag
import SendBirdSDK
import XCTest

// swiftlint:disable file_length
final class ChatCredentialsSpy: ChatCredentialing {
    private var _token = String()
    var token: String {
        get {
            didTokenCount += 1
            return _token
        }
        set {
            _token = newValue
            didTokenCount = 0
        }
    }
    private(set) var didTokenCount: Int = 0
}

final class SessionProviderSpy: SessionProvider {
    var expectedRetrieveAccessTokenResult: Result<ChatCredentialing, Error> = .failure(Stubs.sbdError)
    private(set) var didRetrieveAccessTokenCount = 0

    func retrieveAccessToken(forClientId clientId: String, completion: @escaping (Result<ChatCredentialing, Error>) -> Void) {
        didRetrieveAccessTokenCount += 1
        completion(expectedRetrieveAccessTokenResult)
    }
}

final class MessagingProviderSpy: MessagingProvider {
    static var messagingUserListQuerySpy: MessagingUserListQuerySpy?
    static var connectionState: ConnectionState = .closed
    static var expectedConnectWithUserIdResult: Result<SBDUser?, SBDError> = .success(Stubs.sbdUser)
    static var expectedUpdateCurrentUserInfoResult: Result<Void, SBDError> = .success(())
    static var expectedGetPushNotificationResult: Result<SBDPushTriggerOption, SBDError> = .success(.all)
    static var expectedSetPushNotificationError: SBDError? = Stubs.sbdError
    static var expectedUnreadMessageCount: Result<UInt, SBDError> = .success(14)
    static var expectedCurrentUser: ChatUser?
    static var addedUserEvent: (delegate: SBDUserEventDelegate?, identifier: String?) = (nil, nil)
    static var addedConnectionDelegate: (delegate: SBDConnectionDelegate?, identifier: String?) = (nil, nil)
    
    private(set) static var didInitWithApplicationIdCount = 0
    private(set) static var didConnectWithUserIdCount = 0
    private(set) static var didDisconnectCount = 0
    private(set) static var didUpdateCurrentUserInfoCount = 0
    private(set) static var didRegisterDeviceTokenCount = 0
    private(set) static var didAddPushDeviceToken = 0
    private(set) static var didUnregisterDeviceTokenCount = 0
    private(set) static var didGetPushNotificationOptionCount = 0
    private(set) static var didSetPushNotificationOptionCount = 0
    private(set) static var didGetTotalUnreadMessageCount = 0
    private(set) static var didCallCreateMessagingUserListQueryCount = 0
    private(set) static var didCallSetSessionDelegateCount = 0
    private(set) static var didCallGetCurrentChatUserCount = 0
    private(set) static var didCallAddUserEventCount = 0
    private(set) static var didCallRemoveAllDelegatesCount = 0
    private(set) static var didAddConnectionDelegateCount = 0

    static func initWithApplicationId(_ applicationId: String) -> Bool {
        didInitWithApplicationIdCount += 1
        return true
    }

    static func connect(withUserId userId: String, accessToken: String?, completionHandler: ((SBDUser?, SBDError?) -> Void)?) {
        didConnectWithUserIdCount += 1
        switch expectedConnectWithUserIdResult {
        case .success(let user):
            completionHandler?(user, nil)
        case .failure(let error):
            completionHandler?(nil, error)
        }
    }
    
    static func disconnect(completionHandler: (() -> Void)?) {
        didDisconnectCount += 1
        completionHandler?()
    }

    static func updateCurrentUserInfo(withNickname nickname: String?, profileUrl: String?, completionHandler: ((SBDError?) -> Void)?) {
        didUpdateCurrentUserInfoCount += 1
        switch expectedUpdateCurrentUserInfoResult {
        case .success:
            completionHandler?(nil)
        case .failure(let error):
            completionHandler?(error)
        }
    }
    
    static func registerDeviceToken() {
        didRegisterDeviceTokenCount += 1
    }
    
    static func addPushDeviceToken(_ data: Data) {
        didAddPushDeviceToken += 1
    }
    
    static func unregisterDeviceToken() {
        didUnregisterDeviceTokenCount += 1
    }
    
    static func getPushTriggerOption(completionHandler: @escaping SBDPushTriggerOptionHandler) {
        didGetPushNotificationOptionCount += 1
        switch expectedGetPushNotificationResult {
        case let .success(option):
            completionHandler(option, nil)
        case let .failure(error):
            completionHandler(.all, error)
        }
    }
    
    static func setPushTriggerOption(_ pushTriggerOption: SBDPushTriggerOption, completionHandler: ((SBDError?) -> Void)?) {
        didSetPushNotificationOptionCount += 1
        guard let completion = completionHandler else { return }
        completion(expectedSetPushNotificationError)
    }

    static func getTotalUnreadMessageCount(completionHandler: ((UInt, SBDError?) -> Void)?) {
        switch expectedUnreadMessageCount {
        case .success(let count):
            completionHandler?(count, nil)
        case .failure(let error):
            completionHandler?(.min, error)
        }
        didGetTotalUnreadMessageCount += 1
    }

    static func createMessagingUserListQuery() -> MessagingUserListQuery? {
        didCallCreateMessagingUserListQueryCount += 1
        return messagingUserListQuerySpy
    }

    static func setSessionDelegate(_ delegate: SBDSessionDelegate) {
        didCallSetSessionDelegateCount += 1
    }

    static func getCurrentChatUser() -> ChatUser? {
        didCallGetCurrentChatUserCount += 1
        return expectedCurrentUser
    }
    
    static func addUserEvent(delegate: SBDUserEventDelegate, identifier: String) {
        didCallAddUserEventCount += 1
        self.addedUserEvent = (delegate: delegate, identifier: identifier)
    }
    
    static func removeAllDelegates() {
        didCallRemoveAllDelegatesCount += 1
    }

    static func add(_ delegate: SBDConnectionDelegate, identifier: String) {
        didAddConnectionDelegateCount += 1
        addedConnectionDelegate = (delegate: delegate, identifier: identifier)
    }
    
    static func clear() {
        didInitWithApplicationIdCount = 0
        didConnectWithUserIdCount = 0
        didDisconnectCount = 0
        didUpdateCurrentUserInfoCount = 0
        didRegisterDeviceTokenCount = 0
        didAddPushDeviceToken = 0
        didUnregisterDeviceTokenCount = 0
        didGetPushNotificationOptionCount = 0
        didSetPushNotificationOptionCount = 0
        didGetTotalUnreadMessageCount = 0
        didCallCreateMessagingUserListQueryCount = 0
        didCallSetSessionDelegateCount = 0
        didCallGetCurrentChatUserCount = 0
        didCallAddUserEventCount = 0
        didAddConnectionDelegateCount = 0
        didCallRemoveAllDelegatesCount = 0
        expectedConnectWithUserIdResult = .success(Stubs.sbdUser)
        expectedUpdateCurrentUserInfoResult = .success(())
        expectedUnreadMessageCount = .success(14)
        messagingUserListQuerySpy = nil
        expectedCurrentUser = nil
        addedUserEvent = (delegate: nil, identifier: nil)
        addedConnectionDelegate = (delegate: nil, identifier: nil)
    }
}

final class MessagingUserListQuerySpy: MessagingUserListQuery {
    private var _userIdsFilter = [String]()
    // swiftlint:disable:next discouraged_optional_collection
    var userIdsFilter: [String]? {
        get {
            didCallUserIdsFilterCount += 1
            return _userIdsFilter
        }
        set {
            _userIdsFilter = newValue ?? []
            didCallUserIdsFilterCount = 0
        }
    }
    // swiftlint:disable:next discouraged_optional_collection
    var expectedLoadNextPageResult: Result<[SBDUser]?, SBDError> = .success([Stubs.sbdUser])

    private(set) var didCallUserIdsFilterCount: Int = 0
    private(set) var didCallLoadNextPageCount: Int = 0

    // swiftlint:disable:next discouraged_optional_collection
    func loadNextPage(completionHandler: (([SBDUser]?, SBDError?) -> Void)?) {
        didCallLoadNextPageCount += 1
        switch expectedLoadNextPageResult {
        case .success(let users):
            completionHandler?(users, nil)
        case .failure(let error):
            completionHandler?(nil, error)
        }
    }
}

// swiftlint:disable:next type_body_length
final class SendbirdClientTests: XCTestCase {
    private lazy var chatCredentialsSpy = ChatCredentialsSpy()
    private lazy var themeManagerSpy = SendBirdThemeManagingSpy()
    private lazy var chatConfigurationMock = ChatConnectionConfigurationMock()
    private lazy var featureManagerMock = defaultFeatureManagerMock()
    private lazy var chatNotifierSpy = ChatNotifierSpy()
    private lazy var dependencyContainerMock = DependenciesContainerMock(featureManagerMock, chatNotifierSpy)
    private lazy var sessionProviderSpy = SessionProviderSpy()
    private lazy var messagingUserListQuerySpy = MessagingUserListQuerySpy()
    private lazy var messagingProviderSpy: MessagingProviderSpy.Type = {
        let spy = MessagingProviderSpy.self
        spy.messagingUserListQuerySpy = messagingUserListQuerySpy
        return spy
    }()
    private lazy var sut = SendbirdClient(sessionProvider: sessionProviderSpy,
                                          messagingProvider: messagingProviderSpy,
                                          themeManager: themeManagerSpy,
                                          dependencies: dependencyContainerMock)

    override func tearDown() {
        super.tearDown()

        messagingProviderSpy.clear()
    }

    func testSetup_WhenSessionDelegateIsAvailable_ShouldCallCorrectInitializers() {
        featureManagerMock.override(key: .directMessageSBSessionDelegateAvailable, with: true)

        sut.setup()

        XCTAssertEqual(messagingProviderSpy.didInitWithApplicationIdCount, 1)
        XCTAssertEqual(messagingProviderSpy.didCallSetSessionDelegateCount, 1)
        XCTAssertEqual(themeManagerSpy.setupCallsCount, 1)
    }
    
    func testSetup_WhenSessionDelegateIsNotAvailable_ShouldCallCorrectInitializers() {
        featureManagerMock.override(key: .directMessageSBSessionDelegateAvailable, with: false)

        sut.setup()

        XCTAssertEqual(messagingProviderSpy.didInitWithApplicationIdCount, 1)
        XCTAssertEqual(messagingProviderSpy.didCallSetSessionDelegateCount, 0)
        XCTAssertEqual(themeManagerSpy.setupCallsCount, 1)
    }

    func testUpdateCurrentUserInfo_WhenUpdateSucceed_ShouldCompleteWithSuccess() {
        messagingProviderSpy.expectedUpdateCurrentUserInfoResult = .success(())
        let updateExpectation = expectation(description: "The update should succeed")

        sut.updateCurrentUserInfo(withNickname: chatConfigurationMock.username,
                                  profileUrl: chatConfigurationMock.avatarUrl) { result in
            switch result {
            case .success:
                XCTAssert(true)
            case .failure:
                XCTFail("Update is expected to succeed")
            }
            XCTAssertEqual(self.messagingProviderSpy.didUpdateCurrentUserInfoCount, 1)
            updateExpectation.fulfill()
        }

        waitForExpectations(timeout: 0.5, handler: nil)
    }

    func testUpdateCurrentUserInfo_WhenUpdateFail_ShouldCompleteWithError() {
        messagingProviderSpy.expectedUpdateCurrentUserInfoResult = .failure(Stubs.sbdError)
        let updateExpectation = expectation(description: "The update should fail")

        sut.updateCurrentUserInfo(withNickname: chatConfigurationMock.username,
                                  profileUrl: chatConfigurationMock.avatarUrl) { result in
            switch result {
            case .success:
                XCTFail("Update is expected to fail")
            case .failure:
                XCTAssert(true)
            }
            XCTAssertEqual(self.messagingProviderSpy.didUpdateCurrentUserInfoCount, 1)
            updateExpectation.fulfill()
        }

        waitForExpectations(timeout: 0.5, handler: nil)
    }

    func testConnect_WhenConnectionSucceed_ShouldCallUpdateUserAndSetCurrentUserOnTheme() {
        sessionProviderSpy.expectedRetrieveAccessTokenResult = .success(chatCredentialsSpy)
        messagingProviderSpy.expectedConnectWithUserIdResult = .success(Stubs.sbdUser)
        let connectExpectation = expectation(description: "The connect should succeed")

        sut.connect(withConfiguration: chatConfigurationMock) { result in
            switch result {
            case .success:
                XCTAssert(true)
            case .failure:
                XCTFail("Update is expected to succeed")
            }
            XCTAssertEqual(self.sessionProviderSpy.didRetrieveAccessTokenCount, 1)
            XCTAssertEqual(self.messagingProviderSpy.didConnectWithUserIdCount, 1)
            XCTAssertEqual(self.themeManagerSpy.setCurrentUserCallsCount, 1)
            XCTAssertEqual(self.messagingProviderSpy.didUpdateCurrentUserInfoCount, 1)

            connectExpectation.fulfill()
        }
        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(messagingProviderSpy.didRegisterDeviceTokenCount, 1)
    }

    func testConnect_WhenAccessTokenIsNotObtained_ShouldCompleteWithError() {
        sessionProviderSpy.expectedRetrieveAccessTokenResult = .failure(Stubs.sbdError)
        messagingProviderSpy.expectedConnectWithUserIdResult = .success(Stubs.sbdUser)
        let connectExpectation = expectation(description: "The connect should fail")
        sut.connect(withConfiguration: chatConfigurationMock) { result in
            switch result {
            case .success:
                XCTFail("Update is expected to fail")
            case .failure:
                XCTAssert(true)
            }
            XCTAssertEqual(self.sessionProviderSpy.didRetrieveAccessTokenCount, 1)
            XCTAssertEqual(self.messagingProviderSpy.didConnectWithUserIdCount, 0)
            XCTAssertEqual(self.themeManagerSpy.setCurrentUserCallsCount, 0)
            XCTAssertEqual(self.messagingProviderSpy.didUpdateCurrentUserInfoCount, 0)
            XCTAssertEqual(self.messagingProviderSpy.didRegisterDeviceTokenCount, 0)
            XCTAssertEqual(self.messagingProviderSpy.didAddPushDeviceToken, 0)
            
            connectExpectation.fulfill()
        }

        waitForExpectations(timeout: 0.5, handler: nil)
    }

    func testConnect_WhenConnectionFail_ShouldCompleteWithError() {
        sessionProviderSpy.expectedRetrieveAccessTokenResult = .success(chatCredentialsSpy)
        messagingProviderSpy.expectedConnectWithUserIdResult = .failure(Stubs.sbdError)
        let connectExpectation = expectation(description: "The connect should fail")

        sut.connect(withConfiguration: chatConfigurationMock) { result in
            switch result {
            case .success:
                XCTFail("Update is expected to fail")
            case .failure:
                XCTAssert(true)
            }
            XCTAssertEqual(self.sessionProviderSpy.didRetrieveAccessTokenCount, 1)
            XCTAssertEqual(self.messagingProviderSpy.didConnectWithUserIdCount, 1)
            XCTAssertEqual(self.themeManagerSpy.setCurrentUserCallsCount, 0)
            XCTAssertEqual(self.messagingProviderSpy.didUpdateCurrentUserInfoCount, 0)
            XCTAssertEqual(self.messagingProviderSpy.didRegisterDeviceTokenCount, 0)

            connectExpectation.fulfill()
        }

        waitForExpectations(timeout: 0.5, handler: nil)
    }

    func testConnect_WhenConnectionUserIsNil_ShouldCompleteWithError() {
        sessionProviderSpy.expectedRetrieveAccessTokenResult = .success(chatCredentialsSpy)
        messagingProviderSpy.expectedConnectWithUserIdResult = .success(nil)
        let connectExpectation = expectation(description: "The connect should fail")

        sut.connect(withConfiguration: chatConfigurationMock) { result in
            switch result {
            case .success:
                XCTFail("Update is expected to fail")
            case .failure:
                XCTAssert(true)
            }
            XCTAssertEqual(self.sessionProviderSpy.didRetrieveAccessTokenCount, 1)
            XCTAssertEqual(self.messagingProviderSpy.didConnectWithUserIdCount, 1)
            XCTAssertEqual(self.themeManagerSpy.setCurrentUserCallsCount, 0)
            XCTAssertEqual(self.messagingProviderSpy.didUpdateCurrentUserInfoCount, 0)
            XCTAssertEqual(self.messagingProviderSpy.didRegisterDeviceTokenCount, 0)

            connectExpectation.fulfill()
        }

        waitForExpectations(timeout: 0.5, handler: nil)
    }
    
    func testDisconnect_ShouldDisconnectAndUnregisterWithMessagingProvider() {
        let disconnectExpectation = expectation(description: "The disconnection should fail")
        sut.disconnect {
            disconnectExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 0.5, handler: nil)
        
        XCTAssertEqual(messagingProviderSpy.didDisconnectCount, 1)
        XCTAssertEqual(messagingProviderSpy.didUnregisterDeviceTokenCount, 1)
    }
    
    func testRegisterDeviceToken_ShouldRegisterWithMessagingProvider() {
        sut.registerDeviceToken()
        
        XCTAssertEqual(messagingProviderSpy.didRegisterDeviceTokenCount, 1)
    }
    
    func testAddPushDeviceToken_ShouldAddPushDeviceTokenWithMessagingProvider() {
        sut.addPushDeviceToken(Data())
        
        XCTAssertEqual(messagingProviderSpy.didAddPushDeviceToken, 1)
    }
    
    func testGetPushNotificationOption_ShouldSuccessfullyGetNotificationOption() {
        let getPushOptionExpectation = expectation(description: "Should get push notification option")
        let expectedPushNotificationOption: PushNotificationOption = .all
        let expectedGetPushNotificationResult: Result<SBDPushTriggerOption, SBDError> = .success(.all)
        messagingProviderSpy.expectedGetPushNotificationResult = expectedGetPushNotificationResult
        
        sut.getPushNotificationOption { result in
            switch result {
            case let .success(option):
                XCTAssertEqual(option, expectedPushNotificationOption)
            case .failure:
                XCTFail("Get push notification should succeed")
            }
            getPushOptionExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 0.5, handler: nil)
        
        XCTAssertEqual(messagingProviderSpy.didGetPushNotificationOptionCount, 1)
    }
    
    func testGetPushNotificationOption_ShouldFailToGetNotificationOption() {
        let getPushOptionExpectation = expectation(description: "Should fail to get push notification option")
        let expectedGetPushNotificationResult: Result<SBDPushTriggerOption, SBDError> = .failure(Stubs.sbdError)
        messagingProviderSpy.expectedGetPushNotificationResult = expectedGetPushNotificationResult
        
        sut.getPushNotificationOption { result in
            if case .success = result {
                XCTFail("Get push notification should fail")
            }
            getPushOptionExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(messagingProviderSpy.didGetPushNotificationOptionCount, 1)
    }
    
    func testSetPushNotificationOption_ShouldSuccessfullySetPushNotificationOption() {
        let setPushNotificationOptionExpectation = expectation(description: "Should set push notification option")
        messagingProviderSpy.expectedSetPushNotificationError = nil
        
        sut.setPushNotificationOption(.all) { result in
            if case .failure = result {
                XCTFail("Set push notification option should succeed")
            }
            setPushNotificationOptionExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(messagingProviderSpy.didSetPushNotificationOptionCount, 1)
    }
    
    func testSetPushNotificationOption_ShouldFailToSetPushNotificationOption() {
        let setPushNotificationOptionExpectation = expectation(description: "Should fail to set push notification option")
        messagingProviderSpy.expectedSetPushNotificationError = Stubs.sbdError
        
        sut.setPushNotificationOption(.all) { result in
            if case .success = result {
                XCTFail("Set push notification option should fail")
            }
            setPushNotificationOptionExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(messagingProviderSpy.didSetPushNotificationOptionCount, 1)
    }

    func testFetchUnreadMessageCount_WhenRequestSucceeded_ShouldReturnValidNumber() {
        let fetchExpectation = expectation(description: "The count should be fetched")
        sut.fetchUnreadMessages { count in
            XCTAssertEqual(count, 14)
            fetchExpectation.fulfill()
        }

        waitForExpectations(timeout: 0.5, handler: nil)

        XCTAssertEqual(messagingProviderSpy.didGetTotalUnreadMessageCount, 1)
    }

    func testFetchUnreadMessageCount_WhenRequestFailed_ShouldReturnZero() {
        messagingProviderSpy.expectedUnreadMessageCount = .failure(Stubs.sbdError)
        let fetchExpectation = expectation(description: "The count should not be fetched")
        sut.fetchUnreadMessages { count in
            XCTAssertEqual(count, 0)
            fetchExpectation.fulfill()
        }

        waitForExpectations(timeout: 0.5, handler: nil)

        XCTAssertEqual(messagingProviderSpy.didGetTotalUnreadMessageCount, 1)
    }

    func testConnectionState_ShouldReturnStateFromProvider() {
        messagingProviderSpy.connectionState = .open

        XCTAssertEqual(sut.connectionState, .open)
    }

    private func assertUserQuery(for chatUser: ChatUser) {
        XCTAssertEqual(chatUser.clientId, messagingUserListQuerySpy.userIdsFilter?.first)
        XCTAssertEqual(messagingProviderSpy.didCallCreateMessagingUserListQueryCount, 1)
    }

    func testCanReachUsers_WhenListQueryIsValid_ShouldSucceed() {
        let sbdUser = Stubs.sbdUser
        messagingUserListQuerySpy.expectedLoadNextPageResult = .success([sbdUser])

        let reachExpectation = expectation(description: "The users should be returned")
        let chatUser = Stubs.currentChatUser
        sut.canReach(users: [chatUser]) { result in
            switch result {
            case .success(let users):
                XCTAssertEqual(users.first?.username, sbdUser.nickname)
            case .failure:
                XCTFail("The request should succeed")
            }
            reachExpectation.fulfill()
        }

        waitForExpectations(timeout: 0.5, handler: nil)
        assertUserQuery(for: chatUser)
    }

    func testCanReachUsers_WhenListQueryIsNil_ShouldReturnError() {
        messagingProviderSpy.messagingUserListQuerySpy = nil

        let reachExpectation = expectation(description: "The request should fail")
        sut.canReach(users: [Stubs.currentChatUser]) { result in
            switch result {
            case .success:
                XCTFail("The request should fail")
            case .failure(let error):
                XCTAssertEqual(error.localizedDescription, MessagingClientError.nullUser.localizedDescription)
            }
            reachExpectation.fulfill()
        }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(messagingUserListQuerySpy.userIdsFilter?.count, 0)
        XCTAssertEqual(messagingProviderSpy.didCallCreateMessagingUserListQueryCount, 1)
    }

    func testCanReachUsers_WhenListQueryIsInvalid_ShouldReturnError() {
        let expectedError = SBDError(nsError: MessagingClientError.nullUser)
        messagingUserListQuerySpy.expectedLoadNextPageResult = .failure(expectedError)
        let reachExpectation = expectation(description: "The request should fail")
        let chatUser = Stubs.currentChatUser
        sut.canReach(users: [chatUser]) { result in
            switch result {
            case .success:
                XCTFail("The request should fail")
            case .failure(let error):
                XCTAssertEqual(error.localizedDescription, MessagingClientError.generic(expectedError).localizedDescription)
            }
            reachExpectation.fulfill()
        }

        waitForExpectations(timeout: 0.5, handler: nil)
        assertUserQuery(for: chatUser)
    }

    func testCanReachUsers_WhenListQueryIsEmpty_ShouldReturnError() {
        let expectedError = MessagingClientError.nullUser
        messagingUserListQuerySpy.expectedLoadNextPageResult = .success(nil)
        let reachExpectation = expectation(description: "The request should fail")
        let chatUser = Stubs.currentChatUser
        sut.canReach(users: [chatUser]) { result in
            switch result {
            case .success:
                XCTFail("The request should fail")
            case .failure(let error):
                XCTAssertEqual(error.localizedDescription, expectedError.localizedDescription)
            }
            reachExpectation.fulfill()
        }

        waitForExpectations(timeout: 0.5, handler: nil)
        assertUserQuery(for: chatUser)
    }

    func testCanReachUser_WhenListQueryIsInvalid_ShouldReturnError() {
        let expectedError = SBDError(nsError: MessagingClientError.nullUser)
        messagingUserListQuerySpy.expectedLoadNextPageResult = .failure(expectedError)
        let reachExpectation = expectation(description: "The request should fail")
        let chatUser = Stubs.currentChatUser
        sut.canReach(user: chatUser) { result in
            switch result {
            case .success:
                XCTFail("The request should fail")
            case .failure(let error):
                XCTAssertEqual(error.localizedDescription, MessagingClientError.generic(expectedError).localizedDescription)
            }
            reachExpectation.fulfill()
        }

        waitForExpectations(timeout: 0.5, handler: nil)
        assertUserQuery(for: chatUser)
    }

    func testCanReachUser_WhenUserIsNotReachable_ShouldSucceed() {
        let sbdUser = Stubs.sbdUser
        messagingUserListQuerySpy.expectedLoadNextPageResult = .success([sbdUser])

        let reachExpectation = expectation(description: "The users should be returned")
        let chatUser = Stubs.currentChatUser
        sut.canReach(user: chatUser) { result in
            switch result {
            case .success(let isUserReachable):
                XCTAssertFalse(isUserReachable)
            case .failure:
                XCTFail("The request should succeed")
            }
            reachExpectation.fulfill()
        }

        waitForExpectations(timeout: 0.5, handler: nil)
        assertUserQuery(for: chatUser)
    }

    func testSessionTokenDidRequire_WhenCurrentUserIsNotAvailable_ShouldFail() {
        messagingProviderSpy.expectedCurrentUser = nil

        sut.sessionTokenDidRequire { _ in } failCompletion: {}

        XCTAssertEqual(messagingProviderSpy.didCallGetCurrentChatUserCount, 1)
        XCTAssertEqual(sessionProviderSpy.didRetrieveAccessTokenCount, 0)
    }

    func testSessionTokenDidRequire_WhenCurrentUserIsAvailableAndTokenIsRetrieved_ShouldProvideToken() {
        sessionProviderSpy.expectedRetrieveAccessTokenResult = .success(chatCredentialsSpy)
        messagingProviderSpy.expectedCurrentUser = Stubs.currentChatUser
        sut.sessionTokenDidRequire { _ in } failCompletion: {}

        XCTAssertEqual(messagingProviderSpy.didCallGetCurrentChatUserCount, 1)
        XCTAssertEqual(sessionProviderSpy.didRetrieveAccessTokenCount, 1)
        XCTAssertEqual(chatCredentialsSpy.didTokenCount, 1)
    }

    func testSessionTokenDidRequire_WhenCurrentUserIsAvailableAndTokenIsNotRetrieved_ShouldCompleteWithError() {
        sessionProviderSpy.expectedRetrieveAccessTokenResult = .failure(Stubs.sbdError)
        messagingProviderSpy.expectedCurrentUser = Stubs.currentChatUser
        sut.sessionTokenDidRequire { _ in } failCompletion: {}

        XCTAssertEqual(messagingProviderSpy.didCallGetCurrentChatUserCount, 1)
        XCTAssertEqual(sessionProviderSpy.didRetrieveAccessTokenCount, 1)
        XCTAssertEqual(chatCredentialsSpy.didTokenCount, 0)
    }
    
    func testDidUpdateTotalUnreadMessageCount_ShouldPostMessageWithChatNotifier() {
        sut.didUpdateTotalUnreadMessageCount(10, totalCountByCustomType: nil)

        XCTAssertEqual(chatNotifierSpy.postUpdateUnreadMessagesCallsCount, 1)
        XCTAssertEqual(chatNotifierSpy.postUpdateUnreadMessagesReceivedInvocations, [10])
    }
}

private extension SendbirdClientTests {
    func defaultFeatureManagerMock() -> FeatureManagerMock {
        let featureManager = FeatureManagerMock()
        featureManager.override(key: .directMessageSBSessionDelegateAvailable, with: true)
        return featureManager
    }
}
