import XCTest
@testable import DirectMessageSB

final class ChatConnectionConfigurationTests: XCTestCase {
    private lazy var dependenciesMock = ChatDependencyContainerMock()
    private lazy var sut = ChatConnectionConfiguration(dependencies: dependenciesMock)

    func testClientId_WhenConsumerIdIsNotNumeric_ShouldReturnEmptyString() {
        dependenciesMock.legacy.chatUser = nil

        let id = sut.clientId

        XCTAssertEqual(id, String())
    }

    func testClientId_WhenConsumerIdIsNumeric_ShouldReturnedFormattedId() {
        let consumerId = dependenciesMock.legacy.chatUser?.messagingId ?? -1

        let id = sut.clientId

        XCTAssertEqual(id, "C\(consumerId)")
    }

    func testUsername_WhenConsumerIsNil_ShouldReturnEmptyString() {
        dependenciesMock.legacy.chatUser = nil

        let username = sut.username

        XCTAssertEqual(username, String())
    }

    func testUsername_WhenConsumerIsAvailable_ShouldReturnDependcyValue() {
        let consumerUsername = dependenciesMock.legacy.chatUser?.username

        let username = sut.username

        XCTAssertEqual(username, consumerUsername)
    }

    func testAvatar_WhenRetrieving_ShouldReturnDependcyValue() {
        let consumerAvatar = dependenciesMock.legacy.chatUser?.avatarUrl

        let avatar = sut.avatarUrl

        XCTAssertEqual(avatar, consumerAvatar)
    }
}
