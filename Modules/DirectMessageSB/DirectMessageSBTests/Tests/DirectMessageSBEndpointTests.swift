import Core
import XCTest
@testable import DirectMessageSB

final class DirectMessageSBEndpointTests: XCTestCase {
    private lazy var userId = Stubs.currentChatUser.messagingId.description
    private lazy var sut = DirectMessageSBEndpoint.accessToken(userId)

    func testDirectMessageSBEndpoint_WhenAccessToken_ShouldReturnCorrectContract() {
        XCTAssertEqual(sut.method, .post)
        XCTAssertEqual(sut.path, "direct-message/v1/profiles/\(userId)/session-tokens")
    }
}
