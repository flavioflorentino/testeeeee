#if DEBUG
protocol ConnectionStateServicingDouble: AnyObject, ConnectionStateServicing {
    var expectedConnectionState: ConnectionState { get set }
    var setActionForConnectionStateCount: Int { get set }
    var cancelActionForConnectionStateCount: Int { get set }
    var reconnectCount: Int { get set }
}

extension ConnectionStateServicingDouble {
    var chatApiManager: ChatApiManaging {
        ChatApiManagerMock()
    }

    func setActionForConnectionState(completion: @escaping ((ConnectionState) -> Void)) {
        setActionForConnectionStateCount += 1
        completion(expectedConnectionState)
    }

    func cancelActionForConnectionState() {
        cancelActionForConnectionStateCount += 1
    }

    func reconnect() {
        reconnectCount += 1
    }
}

protocol ConnectionStateInteractingDouble: ConnectionStateInteracting {
    var didSucceedConnectionCount: Int { get set }
    var setConnectionActionsCount: Int { get set }
    var reconnectCount: Int { get set }
    var didCancelActionCount: Int { get set }
    var didFailConnectionCount: Int { get set }
    var didLogConnectionErrorDialogCount: Int { get set }
    var didLogConnectionErrorSelectedTryAgainOptionCount: Int { get set }
    var didLogConnectionErrorSelectedCancelOptionCount: Int { get set }
}

extension ConnectionStateInteractingDouble {
    func didSucceedConnection() {
        didSucceedConnectionCount += 1
    }

    func setConnectionActions() {
        setConnectionActionsCount += 1
    }

    func reconnect() {
        reconnectCount += 1
    }

    func didCancelAction() {
        didCancelActionCount += 1
    }

    func didFailConnection() {
        didFailConnectionCount += 1
    }
    
    func logConnectionErrorDialog() {
        didLogConnectionErrorDialogCount += 1
    }
    
    func logConnectionErrorSelectedTryAgainOption() {
        didLogConnectionErrorSelectedTryAgainOptionCount += 1
    }
    
    func logConnectionErrorSelectedCancelOption() {
        didLogConnectionErrorSelectedCancelOptionCount += 1
    }
}

protocol ConnectionStatePresentingDouble: ConnectionStatePresenting {
    var startConnectionLoadingCount: Int { get set }
    var stopConnectionLoadingCount: Int { get set }
    var presentConnectionErrorCount: Int { get set }
}

extension ConnectionStatePresentingDouble {
    func startConnectionLoading() {
        startConnectionLoadingCount += 1
    }

    func stopConnectionLoading(completion: (() -> Void)? = nil) {
        stopConnectionLoadingCount += 1
        completion?()
    }

    func presentConnectionError() {
        presentConnectionErrorCount += 1
    }
}

protocol ConnectionStateDisplayingDouble: ConnectionStateDisplaying {
    var loadingText: String { get set }
    var alert: ChatConnectionErrorAlert { get set }
    var showConnectionErrorCount: Int { get set }
    var startLoadingCount: Int { get set }
    var stopLoadingCount: Int { get set }
    var didRequestRetryConnectionCount: Int { get set }
    var didRequestCancelConnectionCount: Int { get set }
}

extension ConnectionStateDisplayingDouble {
    func startLoading(loadingText: String) {
        startLoadingCount += 1
        self.loadingText = loadingText
    }

    func stopLoading(completion: (() -> Void)?) {
        stopLoadingCount += 1
        completion?()
    }

    func didRequestRetryConnection() {
        didRequestRetryConnectionCount += 1
    }

    func didRequestCancelConnection() {
        didRequestCancelConnectionCount += 1
    }

    func showConnectionError(alert: ChatConnectionErrorAlert) {
        showConnectionErrorCount += 1
        self.alert = alert
    }
}
#endif
