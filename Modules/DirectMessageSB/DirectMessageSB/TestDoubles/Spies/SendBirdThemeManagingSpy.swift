#if DEBUG
import Foundation
import SendBirdUIKit

final class SendBirdThemeManagingSpy: SendBirdThemeManaging {
    private(set) var setupCallsCount = 0

    func setup() {
        setupCallsCount += 1
    }

    private(set) var setCurrentUserCallsCount = 0
    private(set) var setCurrentUserReceivedInvocations: [SBUUser?] = []

    func set(currentUser: SBUUser?) {
        setCurrentUserCallsCount += 1
        setCurrentUserReceivedInvocations.append(currentUser)
    }
}
#endif
