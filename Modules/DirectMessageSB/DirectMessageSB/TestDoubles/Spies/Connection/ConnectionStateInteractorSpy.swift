#if DEBUG
import AnalyticsModule
final class ConnectionStateInteractorSpy: ConnectionStateInteractingDouble {
    // MARK: Dependencies
    let connectionStateService: ConnectionStateServicing
    let connectionStatePresenter: ConnectionStatePresenting
    var analytics: AnalyticsProtocol = AnalyticsSpy()
    var trackableScreen: TrackedScreen?
    var trackableDialog: TrackedDialog?
    
    // MARK: Properties
    var didSucceedConnectionCount: Int = 0
    var setConnectionActionsCount: Int = 0
    var reconnectCount: Int = 0
    var didCancelActionCount: Int = 0
    var didFailConnectionCount: Int = 0
    var didLogConnectionErrorDialogCount: Int = 0
    var didLogConnectionErrorSelectedTryAgainOptionCount: Int = 0
    var didLogConnectionErrorSelectedCancelOptionCount: Int = 0

    init(service: ConnectionStateServicing = ConnectionStateServiceMock(),
         presenter: ConnectionStatePresenting = ConnectionStatePresenterSpy()) {
        connectionStateService = service
        connectionStatePresenter = presenter
    }
}
#endif
