#if DEBUG
final class ConnectionStateDisplaySpy: ConnectionStateDisplayingDouble {
    let connectionStateInteractor: ConnectionStateInteracting
    var loadingText = String()
    var alert: ChatConnectionErrorAlert = .standard
    var showConnectionErrorCount: Int = 0
    var startLoadingCount: Int = 0
    var stopLoadingCount: Int = 0
    var didRequestRetryConnectionCount: Int = 0
    var didRequestCancelConnectionCount: Int = 0

    init(interactor: ConnectionStateInteracting = ConnectionStateInteractorSpy()) {
        connectionStateInteractor = interactor
    }
}
#endif
