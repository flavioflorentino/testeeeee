#if DEBUG
final class ConnectionStatePresenterSpy: ConnectionStatePresentingDouble {
    weak var connectionStateDisplay: ConnectionStateDisplaying?
    var startConnectionLoadingCount: Int = 0
    var stopConnectionLoadingCount: Int = 0
    var presentConnectionErrorCount: Int = 0
    var connectionErrorAlert: ChatConnectionErrorAlert = .standard
}
#endif
