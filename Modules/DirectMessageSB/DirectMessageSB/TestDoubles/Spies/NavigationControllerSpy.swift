#if DEBUG
import UIKit

public final class NavigationControllerSpy: UINavigationController {
    private(set) var pushedViewController: UIViewController?
    private(set) var settedViewControllers: [UIViewController] = []
    
    private(set) var isPushViewControllerCalled: Bool = false
    private(set) var isPopViewControllerCalled: Bool = false
    private(set) var isSetViewControllersCalled: Bool = false
    
    override public func pushViewController(_ viewController: UIViewController, animated: Bool) {
        pushedViewController = viewController
        isPushViewControllerCalled = true
        super.pushViewController(viewController, animated: false)
    }
    
    override public func popViewController(animated: Bool) -> UIViewController? {
        isPopViewControllerCalled = true
        return super.popViewController(animated: true)
    }
    
    override public func setViewControllers(_ viewControllers: [UIViewController], animated: Bool) {
        isSetViewControllersCalled = true
        settedViewControllers = viewControllers
        super.setViewControllers(viewControllers, animated: animated)
    }
}
#endif
