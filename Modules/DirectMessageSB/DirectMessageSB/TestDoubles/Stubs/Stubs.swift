#if DEBUG
import SendBirdSDK
import SendBirdUIKit

public enum Stubs {
    public static let currentChatUser = DMSBChatUser(
        messagingId: 1,
        type: .consumer,
        username: "drake-yes",
        profileName: "Drake Yes",
        avatarUrl: "https://i.pinimg.com/originals/e0/dd/95/e0dd9576defc8388403bb0a0a5cc1c7e.png"
    )
    
    public static let otherChatUser = DMSBChatUser(
        messagingId: 0,
        type: .consumer,
        username: "drake-no",
        profileName: "Drake No",
        avatarUrl: "https://i.pinimg.com/originals/55/07/fb/5507fb24890e7e9be70fcc8aa84a3f76.png"
    )
    
    static let sbdUser: SBDUser = {
        let user = SBDUser()
        user.nickname = "drake-yes"
        user.profileUrl = "https://i.pinimg.com/originals/e0/dd/95/e0dd9576defc8388403bb0a0a5cc1c7e.png"
        return user
    }()
    
    static let sbuUser = SBUUser(
        userId: "C0",
        nickname: "drake-no",
        profileUrl: "https://i.pinimg.com/originals/55/07/fb/5507fb24890e7e9be70fcc8aa84a3f76.png"
    )
    
    static let sbdError = SBDError(nsError: MessagingClientError.nullUser)
    public static let genericMessagingClientError = MessagingClientError.generic(sbdError)
    
    static let chat = OneOnOneChat(
        channelUrl: "https://i.ytimg.com/vi/oNBW6UY1UdI/maxresdefault.jpg",
        currentUser: Stubs.currentChatUser,
        otherUser: Stubs.otherChatUser
    )
}
#endif
