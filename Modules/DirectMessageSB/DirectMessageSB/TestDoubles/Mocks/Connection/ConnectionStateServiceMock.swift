#if DEBUG
final class ConnectionStateServiceMock: ConnectionStateServicingDouble {
    let chatApiManager: ChatApiManaging
    var expectedConnectionState: ConnectionState = .closed
    var setActionForConnectionStateCount: Int = 0
    var cancelActionForConnectionStateCount: Int = 0
    var reconnectCount: Int = 0

    init(chatApiManager: ChatApiManaging = ChatApiManagerMock()) {
        self.chatApiManager = chatApiManager
    }
}
#endif
