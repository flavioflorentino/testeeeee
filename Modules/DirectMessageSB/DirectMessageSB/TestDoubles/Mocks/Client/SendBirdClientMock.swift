#if DEBUG
import Foundation
import SendBirdSDK

final class SendBirdClientMock: MessagingClient {
    private var _connectionState: ConnectionState = .closed
    var connectionState: ConnectionState {
        get {
            didConnectionStateCount += 1
            return _connectionState
        }
        set {
            _connectionState = newValue
            didConnectionStateCount = 0
        }
    }

    var connectExpectedResult: Result<Void, Error> = .success(())
    var expectedGetNotificationOptionResult: Result<PushNotificationOption, MessagingClientError> = .success(.all)
    var expectedSetNotificationOptionResult: Result<Void, MessagingClientError> = .failure(MessagingClientError.generic(Stubs.sbdError))
    var expectedCanReachUserResult: Result<Bool, MessagingClientError> = .success(true)
    var expectedCanReachUsersResult: Result<[ChatUser], MessagingClientError> = .success([Stubs.currentChatUser])
    
    var expectedUnreadMessagesCount: Int = 0
    private(set) var didSetupCount: Int = 0
    private(set) var didConnectionStateCount: Int = 0
    private(set) var didDisconnectCount: Int = 0
    private(set) var didRegisterDeviceTokenCount: Int = 0
    private(set) var didAddPushDeviceToken: Int = 0
    private(set) var didGetPushNotificationOptionCount: Int = 0
    private(set) var didSetPushNotificationOptionCount: Int = 0
    private(set) var didFetchUnreadMessagesCalledCount: Int = 0
    private(set) var didCanReachUserCount: Int = 0
    private(set) var didCanReachUsersCount: Int = 0
    private(set) var didAddConnectionDelegateCount: Int = 0

    func setup() {
        didSetupCount += 1
    }

    func connect(withConfiguration configuration: ChatConnectionConfiguring, completion: @escaping (Result<Void, Error>) -> Void) {
        completion(connectExpectedResult)
    }
    
    func disconnect(completion: (() -> Void)?) {
        didDisconnectCount += 1
        completion?()
    }
    
    func registerDeviceToken() {
        didRegisterDeviceTokenCount += 1
    }
    
    func addPushDeviceToken(_ data: Data) {
        didAddPushDeviceToken += 1
    }
    
    func getPushNotificationOption(completion: @escaping (Result<PushNotificationOption, MessagingClientError>) -> Void) {
        didGetPushNotificationOptionCount += 1
        completion(expectedGetNotificationOptionResult)
    }
    
    func setPushNotificationOption(_ option: PushNotificationOption, completion: @escaping (Result<Void, MessagingClientError>) -> Void) {
        didSetPushNotificationOptionCount += 1
        completion(expectedSetNotificationOptionResult)
    }
    
    func fetchUnreadMessages(completion: @escaping (Int) -> Void) {
        didFetchUnreadMessagesCalledCount += 1
        completion(expectedUnreadMessagesCount)
    }

    func canReach(users: [ChatUser], completion: @escaping (Result<[ChatUser], MessagingClientError>) -> Void) {
        didCanReachUsersCount += 1
        completion(expectedCanReachUsersResult)
    }

    func canReach(user: ChatUser, completion: @escaping (Result<Bool, MessagingClientError>) -> Void) {
        didCanReachUserCount += 1
        completion(expectedCanReachUserResult)
    }

    func addConnection(delegate: SBDConnectionDelegate) {
        didAddConnectionDelegateCount += 1
    }
}
#endif
