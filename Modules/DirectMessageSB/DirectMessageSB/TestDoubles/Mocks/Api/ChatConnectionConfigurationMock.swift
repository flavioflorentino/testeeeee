#if DEBUG
final class ChatConnectionConfigurationMock: ChatConnectionConfiguring {
    var clientId: String {
        Stubs.currentChatUser.clientId
    }

    var username: String {
        Stubs.currentChatUser.username ?? String()
    }

    var avatarUrl: String? {
        Stubs.currentChatUser.avatarUrl
    }
}
#endif
