#if DEBUG
import Foundation
import Core

public final class ChatApiManagerMock: ChatApiManaging {
    var dependencies: Dependencies
    public var connectionState: ConnectionState
    public var clientId: String = Stubs.currentChatUser.clientId
    public var expectedGetPushNotificationOptionResult: Result<PushNotificationOption, MessagingClientError> = .success(.all)
    public var expectedSetPushNotificationOptionResult: Result<Void, MessagingClientError> = .success(())
    public var expectedUnreadMessagesCount: Int = 0
    public var expectedSetupResult: Result<Void, Error> = .failure(NSError())
    public var expectedConnectResult: Result<Void, Error> = .failure(NSError())
    public var expectedCheckConnectionInvocations: (attempts: Int, poolInterval: TimeInterval)?
    public var expectedCanReachUserResult: Result<Bool, MessagingClientError> = .success(true)
    public var expectedCanReachUsersResult: Result<[ChatUser], MessagingClientError> = .success([Stubs.currentChatUser])
    public private(set) var didSetupCount = 0
    public private(set) var didCheckConnectionCount = 0
    public private(set) var didDisconnectCount = 0
    public private(set) var didRegisterDeviceToken = 0
    public private(set) var didConnectCount = 0
    public private(set) var didAddPushDeviceToken = 0
    public private(set) var didGetPushNotificationOptionCount = 0
    public private(set) var didSetPushNotificationOptionCount = 0
    public private(set) var didGetPushNotificationOption = 0
    public private(set) var didFetchUnreadMessagesCount: Int = 0
    public private(set) var didCanReachUserCount: Int = 0
    public private(set) var didCanReachUsersCount: Int = 0
    public private(set) var didSetActionForConnectionStateCount: Int = 0
    public private(set) var didCancelActionForConnectionStateCount: Int = 0
    public private(set) var didGetCurrentConnectionStateCount: Int = 0
    
    init(dependencies: Dependencies,
         connectionState: ConnectionState = .open) {
        self.connectionState = connectionState
        self.dependencies = dependencies
    }

    public convenience init() {
        self.init(dependencies: DependenciesContainerMock())
    }

    public func setup(completion: @escaping (Result<Void, Error>) -> Void) {
        didSetupCount += 1
        completion(expectedSetupResult)
    }

    public func connect(completion: @escaping (Result<Void, Error>) -> Void) {
        didConnectCount += 1
        completion(expectedConnectResult)
    }
    
    public func disconnect(completion: (() -> Void)?) {
        didDisconnectCount += 1
        completion?()
    }
    
    public func registerDeviceToken() {
        didRegisterDeviceToken += 1
    }
    
    public func addPushDeviceToken(_ data: Data) {
        didAddPushDeviceToken += 1
    }
    
    public func getPushNotificationOption(completion: @escaping (Result<PushNotificationOption, MessagingClientError>) -> Void) {
        didGetPushNotificationOptionCount += 1
        completion(expectedGetPushNotificationOptionResult)
    }
    
    public func setPushNotificationOption(_ option: PushNotificationOption, completion: @escaping (Result<Void, MessagingClientError>) -> Void) {
        didSetPushNotificationOptionCount += 1
        completion(expectedSetPushNotificationOptionResult)
    }
    public func fetchUnreadMessages(completion: @escaping (Int) -> Void) {
        didFetchUnreadMessagesCount += 1
        completion(expectedUnreadMessagesCount)
    }

    public func canReach(user: ChatUser, completion: @escaping (Result<Bool, MessagingClientError>) -> Void) {
        didCanReachUserCount += 1
        completion(expectedCanReachUserResult)
    }

    public func canReach(users: [ChatUser], completion: @escaping (Result<[ChatUser], MessagingClientError>) -> Void) {
        didCanReachUsersCount += 1
        completion(expectedCanReachUsersResult)
    }

    public func setActionForConnectionState(withTimeout timeout: TimeInterval,
                                            completion: @escaping ((ConnectionState) -> Void)) {
        didSetActionForConnectionStateCount += 1
        completion(connectionState)
    }

    public func cancelActionForConnectionState() {
        didCancelActionForConnectionStateCount += 1
    }

    public func getCurrentConnectionState() -> ConnectionState {
        didGetCurrentConnectionStateCount += 1
        return connectionState
    }
}
#endif
