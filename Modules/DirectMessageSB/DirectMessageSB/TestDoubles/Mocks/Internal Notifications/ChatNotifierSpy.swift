#if DEBUG
import Foundation

public final class ChatNotifierSpy: ChatNotifying {
    public init() { }
    
    private(set) var postUpdateUnreadMessagesCallsCount = 0
    private(set) var postUpdateUnreadMessagesReceivedInvocations: [Int] = []
    
    public func postUpdateUnreadMessages(count: Int) {
        postUpdateUnreadMessagesCallsCount += 1
        postUpdateUnreadMessagesReceivedInvocations.append(count)
    }
}
#endif
