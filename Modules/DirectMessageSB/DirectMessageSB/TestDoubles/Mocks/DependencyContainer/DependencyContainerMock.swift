#if DEBUG
import AnalyticsModule
import Core
import FeatureFlag
import Foundation

final class DependenciesContainerMock: Dependencies {
    lazy var analytics: AnalyticsProtocol = resolve()
    lazy var chatUser: ChatUser? = resolve()
    lazy var chatApiManager: ChatApiManaging = resolve()
    lazy var chatNotifier: ChatNotifying = resolve()
    lazy var featureManager: FeatureManagerContract = resolve()
    lazy var kvStore: KVStoreContract = resolve()
    lazy var legacy: DMSBLegacyContract = resolve()
    lazy var mainQueue: DispatchQueue = resolve()
    lazy var chatSettingsFactory: ChatSettingsManufacturing = resolve()
    lazy var notificationCenter: NotificationCenter = resolve()
    
    private let dependencies: [Any]
    init(_ dependencies: Any...) {
        self.dependencies = dependencies
    }
}

extension DependenciesContainerMock {
    func resolve<T>(default: T) -> T {
        guard let mock = dependencies.first(where: { $0 is T }) as? T else {
            return `default`
        }
        return mock
    }

    func resolve() -> DispatchQueue {
        dependencies.compactMap { $0 as? DispatchQueue }.first ?? DispatchQueue(label: "DirectMessageSB")
    }

    func resolve() -> ChatUser {
        let resolved = dependencies.compactMap { $0 as? ChatUser }.first
        return resolved ?? Stubs.currentChatUser
    }

    func resolve() -> ChatApiManaging {
        let resolved = dependencies.compactMap { $0 as? ChatApiManaging }.first
        return resolved ?? ChatApiManagerMock()
    }

    func resolve() -> AnalyticsProtocol {
        let resolved = dependencies.compactMap { $0 as? AnalyticsProtocol }.first
        return resolved ?? AnalyticsSpy()
    }

    func resolve() -> FeatureManagerContract {
        let resolved = dependencies.compactMap { $0 as? FeatureManagerContract }.first
        return resolved ?? FeatureManagerMock()
    }
    
    func resolve() -> KVStoreContract {
        let resolved = dependencies.compactMap { $0 as? KVStoreContract }.first
        return resolved ?? KVStoreMock()
    }
    
    func resolve() -> DMSBLegacyContract {
        let resolved = dependencies.compactMap { $0 as? DMSBLegacyContract }.first
        return resolved ?? DMSBLegacySetupMock()
    }
    
    func resolve() -> ChatSettingsManufacturing {
        let resolved = dependencies.compactMap { $0 as? ChatSettingsManufacturing }.first
        return resolved ?? ChatSettingsFactoryMock()
    }
    
    func resolve() -> NotificationCenter {
        dependencies.compactMap { $0 as? NotificationCenter }
            .first ?? .default
    }
    
    func resolve() -> ChatNotifying {
        dependencies.compactMap { $0 as? ChatNotifying }
            .first ?? ChatNotifierSpy()
    }
}
#endif
