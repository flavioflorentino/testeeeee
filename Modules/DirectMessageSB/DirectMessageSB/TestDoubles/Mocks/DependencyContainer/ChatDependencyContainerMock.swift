#if DEBUG
final class ChatDependencyContainerMock: ChatConnectionConfiguration.Dependencies {
    // MARK: - Dependencies
    
    var legacy: DMSBLegacyContract
    
    // MARK: - Initialization
    
    init(legacy: DMSBLegacyContract = DMSBLegacySetupMock()) {
        self.legacy = legacy
    }
}
#endif
