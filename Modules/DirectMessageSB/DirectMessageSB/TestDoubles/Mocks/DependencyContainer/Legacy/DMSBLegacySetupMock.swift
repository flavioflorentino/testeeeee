#if DEBUG
public final class DMSBLegacySetupMock: DMSBLegacyContract {    
    public lazy var chatUser: ChatUser? = Stubs.currentChatUser
    public lazy var factory: DMSBLegacyFactory? = DMSBLegacyFactoryWrapperMock()
    
    public func inject(instance: Any) { }
}
#endif
