#if DEBUG
import UIKit

public final class DMSBLegacyFactoryWrapperMock: DMSBLegacyFactory {
    public func makeProfileViewController(withId id: String) -> UIViewController? {
        FakeProfileViewController(nibName: nil, bundle: nil)
    }
}

// MARK: - FAKES
// Should be used only on unit tests
final class FakeProfileViewController: UIViewController { }
#endif
