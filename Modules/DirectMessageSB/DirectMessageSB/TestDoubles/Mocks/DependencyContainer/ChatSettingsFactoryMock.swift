#if DEBUG
import UIKit

final class ChatSettingsFactoryMock: ChatSettingsManufacturing {
    func make(withChat chat: OneOnOneChatting) -> UIViewController? {
        FakeChatSettingsViewController()
    }
}

final class FakeChatSettingsViewController: UIViewController { }
#endif
