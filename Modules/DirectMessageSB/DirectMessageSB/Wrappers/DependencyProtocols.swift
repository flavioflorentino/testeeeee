import Foundation

protocol HasNoDependency { }

protocol HasChatUser {
    var chatUser: ChatUser? { get }
}

protocol HasLegacy {
    var legacy: DMSBLegacyContract { get }
}

protocol HasChatSettingsManufacturing {
    var chatSettingsFactory: ChatSettingsManufacturing { get }
}

protocol HasNotificationCenter {
    var notificationCenter: NotificationCenter { get }
}
