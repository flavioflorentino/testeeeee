public protocol DMSBLegacyFactory: AnyObject {
    func makeProfileViewController(withId id: String) -> UIViewController?
}
