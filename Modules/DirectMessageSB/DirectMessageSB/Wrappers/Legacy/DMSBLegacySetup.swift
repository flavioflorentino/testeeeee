import Core

public protocol DMSBLegacyContract {
    var chatUser: ChatUser? { get set }
    var factory: DMSBLegacyFactory? { get set }
    
    func inject(instance: Any)
}

public final class DMSBLegacySetup: DMSBLegacyContract {
    public static let shared: DMSBLegacyContract = DMSBLegacySetup()
    
    public var chatUser: ChatUser?
    public var factory: DMSBLegacyFactory?
    
    public func inject(instance: Any) {
        switch instance {
        case let instance as ChatUser:
            chatUser = instance
        case let instance as DMSBLegacyFactory:
            factory = instance
        default:
            assertionFailure("🚨\nAttempted to inject unexpected depedency: \(instance)\n")
        }
    }
}
