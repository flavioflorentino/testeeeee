import AnalyticsModule
import Core
import FeatureFlag
import Foundation

typealias Dependencies = HasNoDependency
    & HasAnalytics
    & HasChatApiManager
    & HasChatNotifier
    & HasFeatureManager
    & HasKVStore
    & HasLegacy
    & HasMainQueue
    & HasChatSettingsManufacturing
    & HasNotificationCenter

final class DependencyContainer: Dependencies {
    lazy var analytics: AnalyticsProtocol = Analytics.shared
    lazy var chatApiManager: ChatApiManaging = ChatApiManager.shared
    lazy var chatNotifier: ChatNotifying = ChatNotifier.shared
    lazy var featureManager: FeatureManagerContract = FeatureManager.shared
    lazy var kvStore: KVStoreContract = KVStore()
    lazy var chatSettingsFactory: ChatSettingsManufacturing = ChatSettingsFactory()
    lazy var legacy: DMSBLegacyContract = DMSBLegacySetup.shared
    lazy var mainQueue = DispatchQueue.main
    lazy var notificationCenter = NotificationCenter.default
}
