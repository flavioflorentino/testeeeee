import SendBirdUIKit

protocol ChatProvider {
    func hide(hidePreviousMessages: Bool, allowAutoUnhide: Bool, completion: ((Error?) -> Void)?)
    func unhideChannel(completion: ((Error?) -> Void)?)
}

extension SBDGroupChannel: ChatProvider {
    func hide(hidePreviousMessages: Bool,
              allowAutoUnhide: Bool,
              completion: ((Error?) -> Void)?) {
        hide(withHidePreviousMessages: hidePreviousMessages,
             allowAutoUnhide: allowAutoUnhide,
             completionHandler: completion)
    }
    
    func unhideChannel(completion: ((Error?) -> Void)?) {
        unhideChannel(completionHandler: completion)
    }
}
