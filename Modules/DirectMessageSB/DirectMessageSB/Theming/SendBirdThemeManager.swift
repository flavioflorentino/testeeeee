import UI
import AssetsKit
import SendBirdSDK
import SendBirdUIKit

public protocol SendBirdThemeManaging {
    func setup()
    func set(currentUser: SBUUser?)
}

private extension SendBirdThemeManager.Layout {
    enum IconSize {
        static let small = CGSize(width: 8, height: 8)
        static let medium = CGSize(width: 24, height: 24)
        static let large = CGSize(width: 32, height: 32)
    }
    
    enum FontSize {
        static let message: CGFloat = 17
    }
}

public final class SendBirdThemeManager: SendBirdThemeManaging {
    fileprivate enum Layout { }
    private let textManager = SendBirdTextManager()

    public init() {}
    
    public func setup() {
        setupIcons()
        setupGeneralColors()
        setupSpecificThemes()
        textManager.setup()
    }
    
    public func set(currentUser: SBUUser?) {
        SBUGlobals.CurrentUser = currentUser
    }
}

private extension SendBirdThemeManager {
    // MARK: - IconSet
    func setupIcons() {
        SBUIconSet.iconSend = UIImage(
            iconName: Iconography.message.rawValue,
            font: Typography.icons(.medium).font(),
            color: Colors.brandingBase.color,
            size: Layout.IconSize.medium
        ) ?? UIImage()
        
        SBUIconSet.iconSpinnerLarge = UIImage(
            iconName: Iconography.sync.rawValue,
            font: Typography.icons(.large).font(),
            color: Colors.brandingBase.color,
            size: Layout.IconSize.large
        ) ?? UIImage()
        
        SBUIconSet.iconNotifications = UIImage(
            iconName: Iconography.bell.rawValue,
            font: Typography.icons(.large).font(),
            color: Colors.black.color,
            size: Layout.IconSize.large
        ) ?? UIImage()
    }
    
    // MARK: - ColorSet
    func setupGeneralColors() {
        SBUColorSet.primary100 = Colors.branding100.color
        SBUColorSet.primary200 = Colors.branding200.color
        SBUColorSet.primary300 = Colors.branding300.color
        SBUColorSet.primary400 = Colors.branding400.color
        SBUColorSet.primary500 = Colors.branding500.color
        
        SBUColorSet.secondary100 = Colors.branding100.color
        SBUColorSet.secondary200 = Colors.branding200.color
        SBUColorSet.secondary300 = Colors.branding300.color
        SBUColorSet.secondary400 = Colors.branding400.color
        SBUColorSet.secondary500 = Colors.branding500.color
        
        SBUColorSet.background100 = Colors.backgroundPrimary.color
        SBUColorSet.background200 = Colors.backgroundPrimary.color
        SBUColorSet.background300 = Colors.backgroundPrimary.color
        SBUColorSet.background400 = Colors.backgroundPrimary.color
        SBUColorSet.background500 = Colors.backgroundPrimary.color
        SBUColorSet.background600 = Colors.backgroundPrimary.color
        SBUColorSet.background700 = Colors.backgroundPrimary.color
        
        SBUColorSet.onlight01 = Colors.black.color
        SBUColorSet.ondark01 = Colors.white.color
        
        SBUColorSet.error = Colors.criticalBase.color
        SBUColorSet.information = Colors.neutralBase.color
    }
    
    // MARK: - Specific themes
    func setupSpecificThemes() {
        let theme = SBUTheme(
            channelCellTheme: createChannelCellTheme(),
            channelTheme: createChannelTheme(),
            messageInputTheme: createMessageInputTheme(),
            messageCellTheme: createMessageCellTheme(),
            userCellTheme: createUserCellTheme(),
            channelSettingsTheme: createChannelSettingsTheme(),
            componentTheme: createComponentTheme()
        )
        SBUTheme.set(theme: theme)
    }
    
    func createChannelTheme() -> SBUChannelTheme {
        let channelTheme = SBUChannelTheme()
        channelTheme.leftBarButtonTintColor = Colors.branding600.color
        channelTheme.rightBarButtonTintColor = Colors.branding600.color
        
        channelTheme.menuTextColor = Colors.black.color
        channelTheme.navigationBarTintColor = Colors.backgroundPrimary.color
        channelTheme.navigationBarShadowColor = Colors.backgroundPrimary.color
        
        return channelTheme
    }
    
    func createChannelCellTheme() -> SBUChannelCellTheme {
        let channelCellTheme = SBUChannelCellTheme()
        channelCellTheme.titleFont = Typography.bodyPrimary(.highlight).font()
        channelCellTheme.messageFont = Typography.bodySecondary().font()
        channelCellTheme.unreadCountFont = Typography.caption(.default).font()
        channelCellTheme.lastUpdatedTimeFont = Typography.caption().font()
        
        channelCellTheme.unreadCountTextColor = Colors.white.lightColor
        channelCellTheme.unreadCountBackgroundColor = Colors.branding900.color
        channelCellTheme.messageTextColor = Colors.grayscale500.color
        channelCellTheme.lastUpdatedTimeTextColor = Colors.grayscale500.color
        
        return channelCellTheme
    }
    
    func createMessageCellTheme() -> SBUMessageCellTheme {
        let messageCellTheme = SBUMessageCellTheme()
        messageCellTheme.userMessageFont = Typography.bodyPrimary().font().withSize(Layout.FontSize.message)
        messageCellTheme.dateFont = Typography.caption().font()
        
        messageCellTheme.backgroundColor = Colors.backgroundPrimary.color
        messageCellTheme.dateBackgroundColor = Colors.grayscale100.color
        messageCellTheme.leftBackgroundColor = Colors.grayscale050.color
        messageCellTheme.rightBackgroundColor = Colors.grayscale100.color
        
        messageCellTheme.adminMessageTextColor = Colors.black.color
        messageCellTheme.userMessageLeftTextColor = Colors.black.color
        messageCellTheme.userMessageRightTextColor = Colors.black.color
        messageCellTheme.dateTextColor = Colors.grayscale800.color
        messageCellTheme.timeTextColor = Colors.grayscale800.color
        messageCellTheme.userNameTextColor = Colors.grayscale500.color
        
        messageCellTheme.deliveryReceiptStateColor = Colors.grayscale750.color
        messageCellTheme.readReceiptStateColor = Colors.neutral600.color
        
        messageCellTheme.leftPressedBackgroundColor = Colors.branding300.color
        messageCellTheme.rightPressedBackgroundColor = Colors.branding600.color

        messageCellTheme.userMessageLeftEditTextColor = Colors.grayscale750.color
        messageCellTheme.userMessageRightEditTextColor = Colors.grayscale750.color

        return messageCellTheme
    }
    
    func createMessageInputTheme() -> SBUMessageInputTheme {
        let inputTheme = SBUMessageInputTheme()
        inputTheme.buttonTintColor = Colors.branding600.color
        inputTheme.backgroundColor = Colors.backgroundPrimary.color
        
        inputTheme.textFieldTextColor = Colors.black.color
        inputTheme.textFieldBackgroundColor = Colors.backgroundSecondary.color
        inputTheme.textFieldBorderColor = Colors.backgroundSecondary.color
        inputTheme.textFieldPlaceholderColor = Colors.grayscale700.color
        inputTheme.textFieldPlaceholderFont = Typography.bodyPrimary().font()
        
        inputTheme.saveButtonFont = Typography.icons(.medium).font()
        inputTheme.saveButtonTextColor = Colors.white.lightColor
        
        inputTheme.cancelButtonFont = Typography.bodySecondary(.default).font()
        
        return inputTheme
    }
    
    func createComponentTheme() -> SBUComponentTheme {
        let componentTheme = SBUComponentTheme()
        componentTheme.titleFont = Typography.bodyPrimary(.highlight).font()

        componentTheme.titleStatusFont = Typography.caption(.default).font()
        componentTheme.titleStatusColor = Colors.grayscale500.color
        
        return componentTheme
    }
    
    func createUserCellTheme() -> SBUUserCellTheme {
        let userCellTheme = SBUUserCellTheme()
        userCellTheme.checkboxOnColor = .clear
        userCellTheme.checkboxOffColor = .clear
        
        return userCellTheme
    }
    
    func createChannelSettingsTheme() -> SBUChannelSettingsTheme {
        let channelSettingsTheme = SBUChannelSettingsTheme()
        
        channelSettingsTheme.userNameFont = Typography.title(.small).font()
        channelSettingsTheme.userNameTextColor = Colors.black.color
        
        channelSettingsTheme.cellSeparateColor = .clear
        channelSettingsTheme.cellTextFont = Typography.bodyPrimary(.default).font()
        channelSettingsTheme.cellTextColor = Colors.black.color
        channelSettingsTheme.cellTypeIconTintColor = Colors.black.color
        
        return channelSettingsTheme
    }
}
