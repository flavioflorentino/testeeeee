import SendBirdSDK
import SendBirdUIKit

final class SendBirdTextManager {
    func setup() {
        setupGeneralStrings()
        setupDateStrings()
        setupChannelListStrings()
        setupChannelStrings()
        setupMessageInputStrings()
        setupChannelSettingsStrings()
    }
}

private extension SendBirdTextManager {
    func setupGeneralStrings() {
        SBUStringSet.Cancel = Strings.Button.cancel
        SBUStringSet.Camera = Strings.Attachment.takePhoto
        SBUStringSet.PhotoVideoLibrary = Strings.Attachment.photoLibrary
        SBUStringSet.Document = Strings.Attachment.document
        SBUStringSet.Message_Edited = Strings.Edited.message
        SBUStringSet.Copy = Strings.Copy.message
        SBUStringSet.Save = Strings.Save.message
        SBUStringSet.Edit = Strings.Edit.message
        SBUStringSet.Delete = Strings.Delete.message
        SBUStringSet.MemberList_Me = Strings.UserList.you
        SBUStringSet.Empty_No_Messages = Strings.Empty.message
        SBUStringSet.Retry = Strings.Retry.conection
        SBUStringSet.User_No_Name = String()
        SBUStringSet.Empty_Wrong = Strings.Empty.somethingGoWrong
        SBUStringSet.Alert_Delete = Strings.Confirm.Delete.message
    }
    
    func setupChannelListStrings() {
        SBUStringSet.ChannelList_Header_Title = Strings.Chatlist.title
    }
    
    func setupChannelStrings() {
        SBUStringSet.Channel_Name_No_Members = String()
        SBUStringSet.Channel_Header_LastSeen = Strings.LastSeen.message
        SBUStringSet.Channel_Header_Typing = { members in
            switch members.count {
            case 1:
                return Strings.IsTyping.message(members[0].nickname ?? Strings.User.member)
            case 2:
                return Strings.IsTyping.twoUsers(members[0].nickname ?? Strings.User.member,
                                                 members[1].nickname ?? Strings.User.member)
            default:
                return Strings.IsTyping.manyUsers
            }
        }
        SBUStringSet.Empty_No_Channels = Strings.Chatlist.Empty.channels
        SBUStringSet.Channel_New_Message = { messages in
            messages > 1 ? Strings.New.messages(messages) : Strings.New.message(messages)
        }
    }
    
    func setupChannelSettingsStrings() {
        SBUStringSet.ChannelSettings_Notifications = Strings.ChatSettings.notifications
    }
    
    func setupDateStrings() {
        SBUStringSet.Date_Ago = String()
        SBUStringSet.Date_Yesterday = Strings.Date.yesterday
        SBUStringSet.Date_On = Strings.Date.on
        SBUStringSet.Date_Min = { minCount in
            minCount > 1 ? Strings.Date.minutes(minCount) : Strings.Date.minute
        }
        SBUStringSet.Date_Day = { daysCount in
            daysCount > 1 ? Strings.Date.days(daysCount) : Strings.Date.day
        }
        SBUStringSet.Date_Month = { monthCount in
            monthCount > 1 ? Strings.Date.months(monthCount) : Strings.Date.month
        }
        SBUStringSet.Date_Year = { yearsCount in
            yearsCount > 1 ? Strings.Date.years(yearsCount) : Strings.Date.year
        }
    }
    
    func setupMessageInputStrings() {
        SBUStringSet.MessageInput_Text_Placeholder = Strings.Placeholder.message
    }
}
