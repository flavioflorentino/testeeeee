import Core

enum DirectMessageSBEndpoint {
    case accessToken(_ userId: String)
}

extension DirectMessageSBEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case let .accessToken(userId):
            return "direct-message/v1/profiles/\(userId)/session-tokens"
        }
    }
    
    var method: HTTPMethod {
        .post
    }
}
