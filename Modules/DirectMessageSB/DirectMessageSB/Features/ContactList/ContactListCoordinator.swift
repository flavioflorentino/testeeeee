import UIKit

enum ContactListAction: Equatable {
    case back
    case openChat(user: ChatUser)
    
    static func == (lhs: ContactListAction, rhs: ContactListAction) -> Bool {
        switch (lhs, rhs) {
        case (.back, .back):
            return true
        case let (.openChat(userLhs), .openChat(userRhs)):
            return userLhs.isEqual(to: userRhs)
        default:
            return false
        }
    }
}

protocol ContactListCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ContactListAction)
}

final class ContactListCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - ContactListCoordinating
extension ContactListCoordinator: ContactListCoordinating {
    func perform(action: ContactListAction) {
        guard let navigationController = viewController?.navigationController else { return }
        
        switch action {
        case .back:
            navigationController.popViewController(animated: true)
        case let .openChat(user):
            let chatSetupController = ChatSetupFactory.make(for: .distinctUser(user))
            navigationController.pushReplacingLastController(chatSetupController, animated: true)
        }
    }
}
