import AnalyticsModule
import Foundation

protocol ContactListInteracting: AnyObject {
    func initialSetup()
    func back()
    func openChat(with user: ChatUser)
    func setScreenAsViewed()
}

final class ContactListInteractor {
    typealias Dependencies = HasAnalytics
    private let presenter: ContactListPresenting
    private let dependencies: Dependencies

    init(presenter: ContactListPresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - ContactListInteracting
extension ContactListInteractor: ContactListInteracting {
    func initialSetup() {
        presenter.initialSetup()
    }
    
    func back() {
        presenter.didNextStep(action: .back)
        dependencies.analytics.log(.button(.clicked, .back(onScreen: .newChatSB)))
    }
    
    func openChat(with user: ChatUser) {
        presenter.didNextStep(action: .openChat(user: user))
        dependencies.analytics.log(.button(.clicked, .createNewChatViaSB))
    }
    
    func setScreenAsViewed() {
        dependencies.analytics.log(.screenViewed(.newChatSB))
    }
}
