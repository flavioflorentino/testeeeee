import UIKit

enum ContactListFactory {
    static func make() -> ContactListViewController {
        let container = DependencyContainer()
        let coordinator: ContactListCoordinating = ContactListCoordinator()
        let presenter: ContactListPresenting = ContactListPresenter(coordinator: coordinator)
        let interactor = ContactListInteractor(presenter: presenter, dependencies: container)
        let viewController = ContactListViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
