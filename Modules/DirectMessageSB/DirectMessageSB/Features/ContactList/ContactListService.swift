import Foundation

protocol ContactListServicing {
}

final class ContactListService {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - ContactListServicing
extension ContactListService: ContactListServicing {
}
