import Foundation

protocol ContactListPresenting: AnyObject {
    var viewController: ContactListDisplaying? { get set }
    func initialSetup()
    func didNextStep(action: ContactListAction)
}

final class ContactListPresenter {
    private let coordinator: ContactListCoordinating
    weak var viewController: ContactListDisplaying?

    init(coordinator: ContactListCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - ContactListPresenting
extension ContactListPresenter: ContactListPresenting {
    func initialSetup() {
        viewController?.setupNavigationBar()
    }
    
    func didNextStep(action: ContactListAction) {
        coordinator.perform(action: action)
    }
}
