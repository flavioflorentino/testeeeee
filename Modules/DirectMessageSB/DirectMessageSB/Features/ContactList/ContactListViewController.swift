import AssetsKit
import SendBirdUIKit
import UI
import UIKit

protocol ContactListDisplaying: AnyObject {
    func setupNavigationBar()
}

private extension ContactListViewController.Layout {
    enum Fonts {
        static let navigationTitle = Typography.title(.small).font()
    }
    
    enum Colors {
        static let backButton = UI.Colors.brandingBase.color
        static let navigationTitle = UI.Colors.grayscale700.color
    }
    
    enum Images {
        static let backButton = Resources.Icons.icoGreenLeftArrow.image.withRenderingMode(.alwaysTemplate)
    }
}

final class ContactListViewController: SBUCreateChannelViewController {
    fileprivate enum Layout { }
    
    // MARK: - Properties
    
    private let interactor: ContactListInteracting
    
    // MARK: - Views
    
    private lazy var backButton: UIBarButtonItem = {
        let button = UIButton()
        button.setImage(Layout.Images.backButton, for: .normal)
        button.tintColor = Layout.Colors.backButton
        button.addTarget(self, action: #selector(didTapBack), for: .touchUpInside)
        button.sizeToFit()
        return UIBarButtonItem(customView: button)
    }()

    // MARK: - Initialization
    
    init(interactor: ContactListInteractor) {
        self.interactor = interactor
        super.init(users: nil, type: .group)
    }
    
    // MARK: - Overrides
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        interactor.initialSetup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        interactor.setScreenAsViewed()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        .default
    }

    // MARK: - TableView Overrides
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        interactor.openChat(with: DMSBChatUser(userList[indexPath.row]))
    }
    
    // MARK: - Private methods
    
    @objc
    func didTapBack() {
        interactor.back()
    }
}

// MARK: - ContactListDisplaying
extension ContactListViewController: ContactListDisplaying {
    func setupNavigationBar() {
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.navigationStyle(StandardNavigationStyle())
        navigationItem.leftBarButtonItems = [backButton]
        navigationItem.rightBarButtonItems = nil
        navigationItem.titleView = ChatNavigationTitleLabel(
            text: Strings.Contactlist.title
        )
    }
}
