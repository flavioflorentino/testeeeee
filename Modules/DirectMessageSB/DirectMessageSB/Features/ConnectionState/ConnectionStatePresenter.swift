import Foundation

final class ConnectionStatePresenter {
    weak var connectionStateDisplay: ConnectionStateDisplaying?
}

// MARK: - ConnectionStatePresenting
extension ConnectionStatePresenter: ConnectionStatePresenting, ConnectionStateSceneStandardFlow {}
