import AnalyticsModule
import Foundation

public protocol ConnectionStateSuccessDelegate: AnyObject {
    func didSucceedConnection()
}

final class ConnectionStateInteractor {
    typealias Dependencies = HasAnalytics
    
    let dependencies: Dependencies
    let connectionStateService: ConnectionStateServicing
    let connectionStatePresenter: ConnectionStatePresenting
    weak var delegate: ConnectionStateSuccessDelegate?
    var trackableScreen: TrackedScreen?
    var trackableDialog: TrackedDialog?

    init(service: ConnectionStateServicing,
         presenter: ConnectionStatePresenting,
         dependencies: Dependencies,
         screen: TrackedScreen? = nil) {
        connectionStateService = service
        connectionStatePresenter = presenter
        trackableScreen = screen
        self.dependencies = dependencies
    }
}

// MARK: - ConnectionStateInteracting
extension ConnectionStateInteractor: ConnectionStateInteracting, ConnectionStateSceneStandardFlow {
    var analytics: AnalyticsProtocol {
        dependencies.analytics
    }
    
    func didSucceedConnection() {
        delegate?.didSucceedConnection()
    }
}
