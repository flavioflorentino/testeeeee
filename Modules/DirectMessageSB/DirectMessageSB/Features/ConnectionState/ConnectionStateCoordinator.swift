import UI
import UIKit

public typealias ConnectionStateViewController = UIViewController & ApolloLoadable

public protocol ConnectionStateCoordinating: ConnectionStateDisplaying {
    var viewController: ConnectionStateViewController? { get }
    func start()
}

final class ConnectionStateCoordinator {
    let connectionStateInteractor: ConnectionStateInteracting
    weak var viewController: ConnectionStateViewController?

    init(interactor: ConnectionStateInteracting) {
        connectionStateInteractor = interactor
    }

    func start() {
        connectionStateInteractor.setConnectionActions()
    }
}

extension ConnectionStateCoordinator: ConnectionStateCoordinating, ConnectionStateSceneStandardFlow {
    func startLoading(loadingText: String = String()) {
        viewController?.startApolloLoader(loadingText: loadingText)
    }

    func stopLoading(completion: (() -> Void)? = nil) {
        viewController?.stopApolloLoader(completion: completion)
    }

    func showConnectionError(alert: ChatConnectionErrorAlert) {
        guard let viewController = viewController else { return }
        showConnectionError(alert: alert, in: viewController)
    }
}
