import Foundation

struct ConnectionStateService {
    typealias Dependencies = HasChatApiManager
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - ConnectionStateServicing
extension ConnectionStateService: ConnectionStateServicing, ConnectionStateSceneStandardFlow {
    var chatApiManager: ChatApiManaging {
        dependencies.chatApiManager
    }
}
