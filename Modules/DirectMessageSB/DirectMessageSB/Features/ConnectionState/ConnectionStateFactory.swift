import UIKit

public protocol HasConnectionStateManufacturing {
    var connectionStateFactory: ConnectionStateManufacturing { get }
}

public protocol ConnectionStateManufacturing {
    func makeInteractor(delegate: ConnectionStateSuccessDelegate?,
                        display: ConnectionStateDisplaying?,
                        screen: TrackedScreen?) -> ConnectionStateInteracting
    func makeCoordinator(viewController: ConnectionStateViewController?,
                         delegate: ConnectionStateSuccessDelegate?,
                         screen: TrackedScreen?) -> ConnectionStateCoordinating
}

public struct ConnectionStateFactory: ConnectionStateManufacturing {
    public init() {}

    public func makeInteractor(delegate: ConnectionStateSuccessDelegate?,
                               display: ConnectionStateDisplaying?,
                               screen: TrackedScreen? = nil) -> ConnectionStateInteracting {
        let container = DependencyContainer()

        let presenter = ConnectionStatePresenter()
        let service = ConnectionStateService(dependencies: container)
        let interactor = ConnectionStateInteractor(service: service,
                                                   presenter: presenter,
                                                   dependencies: container,
                                                   screen: screen)

        interactor.delegate = delegate
        presenter.connectionStateDisplay = display

        return interactor
    }

    public func makeCoordinator(viewController: ConnectionStateViewController?,
                                delegate: ConnectionStateSuccessDelegate?,
                                screen: TrackedScreen? = nil) -> ConnectionStateCoordinating {
        let container = DependencyContainer()

        let presenter = ConnectionStatePresenter()
        let service = ConnectionStateService(dependencies: container)
        let interactor = ConnectionStateInteractor(service: service,
                                                   presenter: presenter,
                                                   dependencies: container,
                                                   screen: screen)
        
        let coordinator = ConnectionStateCoordinator(interactor: interactor)

        interactor.delegate = delegate
        coordinator.viewController = viewController
        presenter.connectionStateDisplay = coordinator

        return coordinator
    }
}
