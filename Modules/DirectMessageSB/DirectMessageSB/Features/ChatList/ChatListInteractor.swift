import AnalyticsModule
import Core
import FeatureFlag
import Foundation
import SendBirdSDK

protocol ChatListInteracting: AnyObject {
    var clientId: String { get }
    
    func initialSetup()
    func showBetaAlertIfNeeded()
    func setScreenAsViewed()
    func newChat()
    func openChat(for channel: SBDGroupChannel)
    func didDismissScreen(isMovingFromParent: Bool)
    func cellViewModel(for channel: ChatListGroupChannel) -> ChatListCellViewModeling
}

final class ChatListInteractor {
    typealias Dependencies = HasChatApiManager &
        HasAnalytics &
        HasFeatureManager &
        HasKVStore
    private let dependencies: Dependencies
    private let presenter: ChatListPresenting
    
    var hasPresentedBetaAlert: Bool {
        dependencies.kvStore.boolFor(KVKey.hasPresentedBetaAlert)
    }

    init(presenter: ChatListPresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - ChatListInteracting
extension ChatListInteractor: ChatListInteracting {
    var clientId: String {
        dependencies.chatApiManager.clientId
    }
    
    func initialSetup() {
        presenter.initialSetup()
    }
    
    func showBetaAlertIfNeeded() {
        guard !hasPresentedBetaAlert else { return }
        presenter.showBetaAlert()
        dependencies.kvStore.setBool(true, with: KVKey.hasPresentedBetaAlert)
    }

    func setScreenAsViewed() {
        dependencies.analytics.log(.screenViewed(.chats))
    }
    
    func newChat() {
        let isSearchEnabled = dependencies.featureManager.isActive(.directMessageSBViaSearchEnabled)
        let action: ChatListAction = isSearchEnabled ? .newChatViaSearch : .newChatViaSB
        presenter.didNextStep(action: action)
        dependencies.analytics.log(.button(.clicked, .newChat))
        guard isSearchEnabled else { return }
        dependencies.analytics.log(.screenViewed(.newChatSearch))
    }
    
    func openChat(for channel: SBDGroupChannel) {
        presenter.didNextStep(action: .openChat(channel: channel))
        trackOpenChatEvent(unreadMessageCount: channel.unreadMessageCount)
    }
    
    func didDismissScreen(isMovingFromParent: Bool) {
        guard isMovingFromParent else { return }
        dependencies.analytics.log(.button(.clicked, .back(onScreen: .chats)))
    }

    func cellViewModel(for channel: ChatListGroupChannel) -> ChatListCellViewModeling {
        ChatListCellViewModel(from: channel, with: clientId)
    }
}

extension ChatListInteractor {
    func trackOpenChatEvent(unreadMessageCount: UInt) {
        let hasUnreadMessages = unreadMessageCount > 0
        let messageStatus: TrackedMessagesStatus = hasUnreadMessages ? .unread : .clear
        dependencies.analytics.log(.button(.clicked, .openChat(status: messageStatus)))
    }
}
