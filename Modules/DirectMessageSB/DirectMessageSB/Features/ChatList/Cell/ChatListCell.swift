import Foundation
import SendBirdSDK
import SendBirdUIKit
import UIKit

protocol ChatListCellViewModeling {
    var titleText: String { get }
}

final class ChatListCellViewModel: ChatListCellViewModeling {
    // MARK: Properties
    
    private let groupChannel: ChatListGroupChannel
    private let currentUserId: String
    private lazy var otherMember: ChatListGroupChannelMember? = {
        groupChannel.channelMembers.first(where: { $0.userId != currentUserId })
    }()
    
    // MARK: Initialization
    
    init(from groupChannel: ChatListGroupChannel,
         with currentUserId: String) {
        self.groupChannel = groupChannel
        self.currentUserId = currentUserId
    }
    
    var titleText: String {
        otherMember?.profileName ?? ""
    }
}

// MARK: - Internal Protocols

protocol ChatListCell: AnyObject {
    var titleLabel: UILabel { get }
    var separatorLine: UIView { get }
    
    func setup(with viewModel: ChatListCellViewModeling)
}

protocol ChatListGroupChannel: AnyObject {
    var channelMembers: [ChatListGroupChannelMember] { get }
}

protocol ChatListGroupChannelMember: AnyObject {
    var userId: String { get }
    var profileName: String? { get }
}

// MARK: - Internal Extensions

extension ChatListCell {
    func setup(with viewModel: ChatListCellViewModeling) {
        titleLabel.text = viewModel.titleText
        separatorLine.removeFromSuperview()
    }
}

// MARK: - SendBird Protocol Extensions

extension SBDMember: ChatListGroupChannelMember { }

extension SBDGroupChannel: ChatListGroupChannel {
    var channelMembers: [ChatListGroupChannelMember] {
        members as? [ChatListGroupChannelMember] ?? []
    }
}

extension SBUChannelCell: ChatListCell { }
