import AnalyticsModule
import Search
import SendBirdSDK
import UI
import UIKit

enum ChatListAction: Equatable {
    case back
    case newChatViaSearch
    case newChatViaSB
    case openChat(channel: SBDGroupChannel)
}

protocol ChatListCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ChatListAction)
}

final class ChatListCoordinator {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    weak var viewController: UIViewController?
    private var searchCoordinator: SearchCoordinating?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - ChatListCoordinating
extension ChatListCoordinator: ChatListCoordinating {
    func perform(action: ChatListAction) {
        guard let navigationController = viewController?.navigationController else { return }
        
        switch action {
        case .back:
            navigationController.popViewController(animated: true)
        case .newChatViaSearch:
            self.searchCoordinator = makeSearchCoordinator()
            searchCoordinator?.start()
        case .newChatViaSB:
            let contactListController = ContactListFactory.make()
            navigationController.pushViewController(contactListController, animated: true)
        case .openChat(let channel):
            let chatController = ChatFactory.make(withChannel: channel)
            navigationController.pushViewController(chatController, animated: true)
        }
    }
}

// MARK: - SearchCoordinating
extension ChatListCoordinator {
    func makeSearchCoordinator() -> SearchCoordinating? {
        guard let navigationController = viewController?.navigationController else { return nil }
        navigationController.navigationBar
            .setUpNavigationBarDefaultAppearance(backgroundColor: Colors.grayscale050.color)
        let searchCoordinator = SearchFactory(dependencies: DependencyContainer())
            .makeSearchCoordinator(
                title: Strings.Contactlist.title,
                searchType: .consumers,
                shouldShowContacts: true,
                navigationController: navigationController
            )
        searchCoordinator.didFinishFlow = { self.openChat(for: $0, in: navigationController) }
        return searchCoordinator
    }

    func openChat(for selection: SearchSelection?, in navigationController: UINavigationController) {
        guard case let .single(singleValue) = selection else { return }
        switch singleValue {
        case let .contact(consumerData),
             let .consumer(consumerData):
            let chatSetupViewController = ChatSetupFactory.make(for: .distinctUser(DMSBChatUser(consumerData)))
            navigationController.pushReplacingLastController(chatSetupViewController, animated: true)
        default:
            break
        }
        dependencies.analytics.log(.button(.clicked, .createNewChatViaSearch))
    }
}
