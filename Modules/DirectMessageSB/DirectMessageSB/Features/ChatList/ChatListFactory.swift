import UIKit

public enum ChatListFactory {
    public static func make() -> ChatListViewController {
        let container = DependencyContainer()
        let coordinator: ChatListCoordinating = ChatListCoordinator(dependencies: container)
        let presenter: ChatListPresenting = ChatListPresenter(coordinator: coordinator, dependencies: container)
        let interactor = ChatListInteractor(presenter: presenter, dependencies: container)
        let viewController = ChatListViewController(interactor: interactor)
        viewController.hidesBottomBarWhenPushed = true

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
