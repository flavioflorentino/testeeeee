import Foundation
import UI

protocol ChatListPresenting: AnyObject {
    var viewController: ChatListDisplaying? { get set }
    func didNextStep(action: ChatListAction)
    func initialSetup()
    func showBetaAlert()
}

final class ChatListPresenter {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let coordinator: ChatListCoordinating
    weak var viewController: ChatListDisplaying?
    
    private let betaAlertDuration: TimeInterval = 10

    init(coordinator: ChatListCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
}

// MARK: - ChatListPresenting
extension ChatListPresenter: ChatListPresenting {
    func didNextStep(action: ChatListAction) {
        coordinator.perform(action: action)
    }
    
    func initialSetup() {
        viewController?.setupNavigationBar()
    }
    
    func showBetaAlert() {
        viewController?.showBetaAlert(text: Strings.Chatlist.Alert.beta,
                                      iconType: .information,
                                      emphasis: .high,
                                      duration: betaAlertDuration)
    }
}
