import AssetsKit
import SendBirdUIKit
import UI
import UIKit

protocol ChatListDisplaying: AnyObject {
    func setupNavigationBar()
    func showBetaAlert(text: String,
                       iconType: ApolloFeedbackCardViewIconType,
                       emphasis: ApolloSnackbarEmphasisStyle,
                       duration: TimeInterval)
}

private extension ChatListViewController.Layout {
    enum Fonts {
        static let navigationTitle = Typography.title(.small).font()
    }
    
    enum Colors {
        static let newChatButton = UI.Colors.brandingBase.color
        static let navigationTitle = UI.Colors.grayscale700.color
    }
}

public final class ChatListViewController: SBUChannelListViewController {
    fileprivate enum Layout { }
    
    // MARK: - Properties
    
    private let interactor: ChatListInteracting
    
    // MARK: - Initialization
    
    init(interactor: ChatListInteracting) {
        self.interactor = interactor
        super.init(channelListQuery: nil)
    }
    
    // MARK: - Views
    
    private lazy var newChatButton: UIBarButtonItem = {
        UIBarButtonItem(icon: .commentPlus,
                        target: self,
                        action: #selector(didTapNewChat))
    }()
    
    // MARK: - Overrides
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        .default
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        interactor.showBetaAlertIfNeeded()
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        interactor.initialSetup()
    }

    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        interactor.setScreenAsViewed()
    }
    
    override public func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        interactor.didDismissScreen(isMovingFromParent: isMovingFromParent)
    }
    
    // MARK: - TableView Overrides
    
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let baseCell = super.tableView(tableView, cellForRowAt: indexPath)
        
        guard let cell = baseCell as? SBUChannelCell,
              let channel = cell.channel as? SBDGroupChannel
        else { return baseCell }
        
        cell.setup(with: interactor.cellViewModel(for: channel))
        
        return cell
    }
    
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let channel = channelList[indexPath.row]
        interactor.openChat(for: channel)
    }
    
    @available(iOS 11.0, *)
    override public func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        nil
    }
    
    // swiftlint:disable:next discouraged_optional_collection
    override public func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        []
    }
    
    // MARK: - Private methods
    
    @objc
    private func didTapNewChat() {
        interactor.newChat()
    }
}

// MARK: - ChatListDisplaying

extension ChatListViewController: ChatListDisplaying {
    func setupNavigationBar() {
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.navigationStyle(StandardNavigationStyle())
        navigationItem.leftItemsSupplementBackButton = true
        navigationItem.leftBarButtonItems = []
        navigationItem.rightBarButtonItems = [newChatButton]
        navigationItem.titleView = ChatNavigationTitleLabel(
            text: Strings.Chatlist.title
        )
    }
    
    func showBetaAlert(text: String,
                       iconType: ApolloFeedbackCardViewIconType,
                       emphasis: ApolloSnackbarEmphasisStyle,
                       duration: TimeInterval) {
        let snackbar = ApolloSnackbar(text: text,
                                      iconType: iconType,
                                      emphasis: emphasis)
        showSnackbar(snackbar, duration: duration)
    }
}
