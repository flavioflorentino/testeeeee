import Foundation
import UIKit
import SendBirdSDK

enum ChatSetupAction {
    case openChat(channel: SBDGroupChannel)
    case dismiss
}

protocol ChatSetupCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ChatSetupAction)
}

final class ChatSetupCoordinator {
    typealias Dependencies = HasNoDependency
        
    // MARK: - Properties
    private let dependencies: Dependencies
    weak var viewController: UIViewController?
    
    // MARK: - Initialization
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension ChatSetupCoordinator: ChatSetupCoordinating {
    func perform(action: ChatSetupAction) {
        guard let navigationController = viewController?.navigationController else { return }
        
        switch action {
        case .openChat(let channel):
            let chatViewController = ChatFactory.make(withChannel: channel)
            navigationController.pushReplacingLastController(chatViewController, animated: true)
            
        case .dismiss:
            navigationController.popViewController(animated: true)
        }
    }
}
