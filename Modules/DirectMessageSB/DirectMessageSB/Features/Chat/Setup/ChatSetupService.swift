import Foundation
import SendBirdSDK

enum ChatSetupServicingError: Error {
    case unexpectedNil
    case sdk(error: SBDError)
}

protocol ChatSetupServicing {
    func retrieveChat(withUserId userId: String,
                      isDistinct: Bool,
                      completion: @escaping (Result<SBDGroupChannel, ChatSetupServicingError>) -> Void)

    func retrieveChat(withUrl url: String,
                      completion: @escaping (Result<SBDGroupChannel, ChatSetupServicingError>) -> Void)
}

struct ChatSetupService: ChatSetupServicing {
    typealias Dependencies = HasChatApiManager
    private let dependencies: Dependencies

    // MARK: Initialization
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    func retrieveChat(withUserId userId: String,
                      isDistinct: Bool,
                      completion: @escaping (Result<SBDGroupChannel, ChatSetupServicingError>) -> Void) {
        SBDGroupChannel.createChannel(withUserIds: [userId], isDistinct: isDistinct) { channel, error in
            if let sbdError = error {
                completion(.failure(.sdk(error: sbdError)))
                return
            }
            
            guard let channel = channel else {
                completion(.failure(.unexpectedNil))
                return
            }
            
            completion(.success(channel))
        }
    }
    
    func retrieveChat(withUrl url: String,
                      completion: @escaping (Result<SBDGroupChannel, ChatSetupServicingError>) -> Void) {
        SBDGroupChannel.getWithUrl(url) { channel, error in
            if let sbdError = error {
                completion(.failure(.sdk(error: sbdError)))
                return
            }
            
            guard let channel = channel else {
                completion(.failure(.unexpectedNil))
                return
            }
            
            completion(.success(channel))
        }
    }
}

// MARK: - ConnectionStateServicing
extension ChatSetupService: ConnectionStateServicing, ConnectionStateSceneStandardFlow {
    var chatApiManager: ChatApiManaging {
        dependencies.chatApiManager
    }
}
