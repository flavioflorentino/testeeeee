import Foundation

public enum ChatSetupFactory {
    public static func make(for retrievingMethod: ChatRetrievingMethod) -> ChatSetupViewController {
        let container = DependencyContainer()
        let service = ChatSetupService(dependencies: container)
        let coordinator = ChatSetupCoordinator(dependencies: container)
        let presenter = ChatSetupPresenter(coordinator: coordinator)
        let interactor = ChatSetupInteractor(retrievingMethod: retrievingMethod,
                                             presenter: presenter,
                                             service: service,
                                             dependencies: container)
        let viewController = ChatSetupViewController(interactor: interactor)
        
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
