import Foundation
import SendBirdSDK

protocol ChatSetupPresenting: AnyObject {
    func presentChat(forChannel channel: SBDGroupChannel)
    func closeChat()
}

final class ChatSetupPresenter {
    typealias Displaying = ChatSetupDisplaying & ConnectionStateDisplaying
    private let coordinator: ChatSetupCoordinating
    weak var viewController: Displaying?
    
    init(coordinator: ChatSetupCoordinating) {
        self.coordinator = coordinator
    }
}

extension ChatSetupPresenter: ChatSetupPresenting {
    func presentChat(forChannel channel: SBDGroupChannel) {
        coordinator.perform(action: .openChat(channel: channel))
    }
    
    func closeChat() {
        coordinator.perform(action: .dismiss)
    }
}

// MARK: - ConnectionStatePresenting
extension ChatSetupPresenter: ConnectionStatePresenting, ConnectionStateSceneStandardFlow {
    var connectionStateDisplay: ConnectionStateDisplaying? {
        viewController
    }
}
