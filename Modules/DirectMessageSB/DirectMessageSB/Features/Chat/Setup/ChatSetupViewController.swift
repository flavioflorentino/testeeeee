import UI
import UIKit

protocol ChatSetupDisplaying: AnyObject {}

public final class ChatSetupViewController: UIViewController, ChatSetupDisplaying {
    fileprivate enum Layout { }
    
    // MARK: - Properties
    typealias Interacting = ChatSetupInteracting & ConnectionStateInteracting
    private let statefulLoadingView = StatefulLoadingView()
    private let interactor: ChatSetupInteracting & ConnectionStateInteracting
    
    // MARK: - Initialization
    required init?(coder: NSCoder) { nil }
    
    init(interactor: Interacting) {
        self.interactor = interactor
        super.init(nibName: nil, bundle: nil)
        self.hidesBottomBarWhenPushed = true
    }
    
    // MARK: - Methods
    override public func loadView() {
        view = statefulLoadingView
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        interactor.fetchChatInformation()
    }

    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        interactor.didCancelAction()
    }
}

// MARK: - ConnectionStateDisplaying
extension ChatSetupViewController: ConnectionStateDisplaying, ConnectionStateSceneStandardFlow {
    public var connectionStateInteractor: ConnectionStateInteracting {
        interactor
    }

    public func startLoading(loadingText: String) {
        // No additional loading is required
    }

    public func stopLoading(completion: (() -> Void)?) {
        completion?()
    }

    public func didRequestCancelConnection() {
        interactor.close()
    }
}
