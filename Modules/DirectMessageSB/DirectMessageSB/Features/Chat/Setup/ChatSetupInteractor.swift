import AnalyticsModule
import Core
import Foundation
import SendBirdSDK

protocol ChatSetupInteracting: AnyObject {
    func fetchChatInformation()
    func close()
}

final class ChatSetupInteractor {
    typealias Dependencies = HasMainQueue & HasAnalytics
    typealias Presenting = ChatSetupPresenting & ConnectionStatePresenting
    typealias Servicing = ChatSetupServicing & ConnectionStateServicing

    // MARK: - Properties
    private let dependencies: Dependencies
    private let presenter: Presenting
    private let service: Servicing
    private let retrievingMethod: ChatRetrievingMethod
    var trackableDialog: TrackedDialog?
    
    // MARK: - Initialization
    init(retrievingMethod: ChatRetrievingMethod,
         presenter: Presenting,
         service: Servicing,
         dependencies: Dependencies) {
        self.dependencies = dependencies
        self.presenter = presenter
        self.service = service
        self.retrievingMethod = retrievingMethod
    }
}

// MARK: - ChatSetupInteracting
extension ChatSetupInteractor: ChatSetupInteracting {
    func fetchChatInformation() {
        setConnectionActions()
    }

    func close() {
        presenter.closeChat()
    }
}

// MARK: - ConnectionStateInteracting
extension ChatSetupInteractor: ConnectionStateInteracting, ConnectionStateSceneStandardFlow {
    var analytics: AnalyticsProtocol {
        dependencies.analytics
    }

    var trackableScreen: TrackedScreen? {
        .chatSetup
    }
    
    var connectionStateService: ConnectionStateServicing {
        service
    }

    var connectionStatePresenter: ConnectionStatePresenting {
        presenter
    }

    func didSucceedConnection() {
        retrieveChat()
    }
}

private extension ChatSetupInteractor {
    func retrieveChat() {
        switch retrievingMethod {
        case let .distinctUser(user):
            service.retrieveChat(withUserId: user.clientId, isDistinct: true) { [weak self] result in
                self?.handle(result: result)
            }
        case let .url(chatUrl):
            service.retrieveChat(withUrl: chatUrl) { [weak self] result in
                self?.handle(result: result)
            }
        }
    }

    func handle(result: Result<SBDGroupChannel, ChatSetupServicingError>) {
        switch result {
        case .success(let channel):
            presenter.presentChat(forChannel: channel)
        case .failure:
            close()
        }
    }
}
