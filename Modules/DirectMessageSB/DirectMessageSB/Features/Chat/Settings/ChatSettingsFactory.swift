import SendBirdUIKit
import UIKit

protocol ChatSettingsManufacturing {
    func make(withChat chat: OneOnOneChatting) -> UIViewController?
}

struct ChatSettingsFactory: ChatSettingsManufacturing {
    func make(withChat chat: OneOnOneChatting) -> UIViewController? {
        guard let channel = chat as? SBDGroupChannel else { return nil }
        return make(withChannel: channel)
    }
    
    func make(withChannel channel: SBDGroupChannel) -> ChatSettingsViewController {
        let container = DependencyContainer()
        let coordinator: ChatSettingsCoordinating = ChatSettingsCoordinator(dependencies: container)
        let presenter: ChatSettingsPresenting = ChatSettingsPresenter(coordinator: coordinator)
        let interactor = ChatSettingsInteractor(channel: channel, presenter: presenter, dependencies: container)
        let viewController = ChatSettingsViewController(interactor: interactor, channel: channel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
