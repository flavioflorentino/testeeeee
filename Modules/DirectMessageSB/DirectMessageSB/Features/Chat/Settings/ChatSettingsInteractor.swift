import AnalyticsModule
import Foundation

protocol ChatSettingsInteracting: AnyObject {
    func setupNavigationBar()
    func openProfile()
    func setScreenAsViewed()
}

final class ChatSettingsInteractor {
    typealias Dependencies = HasAnalytics
    
    private let channel: OneOnOneChatting
    private let dependencies: Dependencies
    private let presenter: ChatSettingsPresenting

    init(channel: OneOnOneChatting, presenter: ChatSettingsPresenting, dependencies: Dependencies) {
        self.channel = channel
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - ChatSettingsInteracting
extension ChatSettingsInteractor: ChatSettingsInteracting {
    func setupNavigationBar() {
        presenter.setupNavigationBar()
    }
    
    func openProfile() {
        guard let otherUserId = channel.otherUser?.clientId.numbers else { return }
        presenter.openProfile(withId: otherUserId)
        
        dependencies.analytics.log(.button(.clicked, .openProfile(onScreen: .chatSettings)))
    }
    
    func setScreenAsViewed() {
        dependencies.analytics.log(.screenViewed(.chatSettings))
    }
}
