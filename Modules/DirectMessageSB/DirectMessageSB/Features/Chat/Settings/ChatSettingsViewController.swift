import AssetsKit
import SendBirdUIKit
import UI
import UIKit

protocol ChatSettingsDisplaying: AnyObject {
    func setupNavigationBar()
}

final class ChatSettingsViewController: SBUChannelSettingsViewController {
    // MARK: - Properties
    private let interactor: ChatSettingsInteracting
    
    // MARK: - Initialization
    init(interactor: ChatSettingsInteracting, channel: SBDGroupChannel) {
        self.interactor = interactor
        super.init(channel: channel)
    }
    
    // MARK: - Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        interactor.setupNavigationBar()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addUserTapGestureRecognizer()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        interactor.setScreenAsViewed()
    }
    
    // MARK: - TableView Overrides numberOfRowsInSection
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    @objc
    fileprivate func didTapUsername() {
        interactor.openProfile()
    }
    
    private func addUserTapGestureRecognizer() {
            userInfoView?.isUserInteractionEnabled = true
            userInfoView?.addGestureRecognizer(
                UITapGestureRecognizer(target: self, action: #selector(didTapUsername))
            )
        }
}

// MARK: - ChatSettingsDisplaying
extension ChatSettingsViewController: ChatSettingsDisplaying {
    func setupNavigationBar() {
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.navigationStyle(StandardNavigationStyle())
        navigationItem.leftBarButtonItems = []
        navigationItem.leftItemsSupplementBackButton = true
        navigationItem.rightBarButtonItems = nil
        navigationItem.titleView = ChatNavigationTitleLabel(
            text: Strings.ChatSettings.title
        )
    }
}
