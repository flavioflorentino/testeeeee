import Foundation

protocol ChatSettingsPresenting: AnyObject {
    var viewController: ChatSettingsDisplaying? { get set }
    func setupNavigationBar()
    func openProfile(withId id: String)
}

final class ChatSettingsPresenter {
    private let coordinator: ChatSettingsCoordinating
    weak var viewController: ChatSettingsDisplaying?

    init(coordinator: ChatSettingsCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - ChatSettingsPresenting
extension ChatSettingsPresenter: ChatSettingsPresenting {
    func setupNavigationBar() {
        viewController?.setupNavigationBar()
    }
    
    func openProfile(withId id: String) {
        coordinator.perform(action: .openProfile(id: id))
    }
}
