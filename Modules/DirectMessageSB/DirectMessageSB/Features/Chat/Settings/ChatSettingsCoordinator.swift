import SendBirdUIKit
import Foundation
import UIKit

enum ChatSettingsAction: Equatable {
    case openProfile(id: String)
    
    static func == (lhs: ChatSettingsAction, rhs: ChatSettingsAction) -> Bool {
        switch (lhs, rhs) {
        case let (.openProfile(idLhs), openProfile(idRhs)):
            return idLhs == idRhs
        }
    }
}

protocol ChatSettingsCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ChatSettingsAction)
}

final class ChatSettingsCoordinator {
    typealias Dependencies = HasLegacy
    private let dependencies: Dependencies

    weak var viewController: UIViewController?
    private var navigationController: UINavigationController? {
        viewController?.navigationController
    }

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - ChatSettingsCoordinating
extension ChatSettingsCoordinator: ChatSettingsCoordinating {
    func perform(action: ChatSettingsAction) {
        switch action {
        case .openProfile(let id):
            openProfile(withId: id)
        }
    }
}

private extension ChatSettingsCoordinator {
    func openProfile(withId id: String) {
        guard let factory = dependencies.legacy.factory,
              let profileViewController = factory.makeProfileViewController(withId: id)
        else { return }
        
        navigationController?.pushViewController(profileViewController, animated: true)
    }
}
