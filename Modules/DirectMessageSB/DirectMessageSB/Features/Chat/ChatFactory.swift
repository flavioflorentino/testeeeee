import Foundation
import SendBirdSDK

public enum ChatFactory {
    static func make(withChannel channel: SBDGroupChannel) -> ChatViewController {
        let container = DependencyContainer()
        let coordinator = ChatCoordinator(dependencies: container)
        let presenter = ChatPresenter(coordinator: coordinator)
        let interactor = ChatInteractor(channel: channel, presenter: presenter, dependencies: container)
        let chatViewController = ChatViewController(interactor: interactor, channel: channel)
        
        coordinator.viewController = chatViewController
        presenter.viewController = chatViewController
        
        return chatViewController
    }
}
