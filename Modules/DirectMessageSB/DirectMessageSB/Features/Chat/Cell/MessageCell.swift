import Foundation
import UIKit
import SendBirdSDK
import SendBirdUIKit

protocol MessageCell {
    var profileView: UIView { get }
    var userNameView: UIView { get }
    var messageTextView: UIView { get }
    var position: MessagePosition { get }
}

// MARK: - SendBird Protocol Extensions

extension SBUUserMessageCell: MessageCell {
    func setupCell() {
        profileView.removeFromSuperview()
        userNameView.removeFromSuperview()
        
        let cellBubble = messageTextView.superview?.superview
        cellBubble?.cornerRadius = .strong
        if #available(iOS 11.0, *) {
            if position == .right {
                cellBubble?.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner]
            } else if position == .left {
                cellBubble?.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner]
            }
        }
    }
}
