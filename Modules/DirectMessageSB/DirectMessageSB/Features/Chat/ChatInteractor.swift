import AnalyticsModule
import Foundation
import SendBirdSDK

protocol ChatInteracting {
    var defaultMessageParameters: SBDMessageListParams { get }
    
    func isFromChannel(url: String) -> Bool
    func canOpenChat(url: String) -> Bool
    
    // MARK: Actions
    func initialSetup()
    func setScreenAsViewed()
    func didTapSaveButton()
    func didTapCancelButton()
    func didLongPressMessage(_ messageCell: MessageCell)
    func didDismissScreen(isMovingFromParent: Bool)
    func didTapSendButton()
    
    // MARK: Flow
    func dismiss()
    func openChatSettings()
    func openProfile()
}

final class ChatInteractor: ChatInteracting {
    // MARK: - Dependencies
    typealias Dependencies = HasAnalytics & HasChatApiManager
    private let channel: OneOnOneChatting
    private let presenter: ChatPresenting
    private let dependencies: Dependencies
    
    // MARK: - Properties
    /// Default parameters to create a new conversation
    let defaultMessageParameters: SBDMessageListParams = {
        let params = SBDMessageListParams()
        params.includeMetaArray = false
        params.includeReactions = false
        params.includeReplies = false
        params.includeParentMessageText = false
        params.includeThreadInfo = false
        params.messageType = .user
        return params
    }()
    
    // MARK: - Initialization
    init(channel: OneOnOneChatting, presenter: ChatPresenting, dependencies: Dependencies) {
        self.channel = channel
        self.presenter = presenter
        self.dependencies = dependencies
    }
    
    // MARK: - Actions
    func initialSetup() {
        guard let otherUser = channel.otherUser else { return dismiss() }
        presenter.setupNavigationBar(with: otherUser)
        presenter.setupAdittionalButtons()
        presenter.setupEditionButtons()
    }

    func setScreenAsViewed() {
        dependencies.analytics.log(.screenViewed(.privateChat))
    }
    
    func didLongPressMessage(_ messageCell: MessageCell) {
        switch messageCell.position {
        case .right:
            dependencies.analytics.log(.dialogViewed(.sentMessageOptions))
        case .left:
            dependencies.analytics.log(.dialogViewed(.receivedMessageOptions))
        default:
            break
        }
    }
    
    func didTapSendButton() {
        guard
            let currentUserId = channel.currentUser?.clientId,
            let targetUserId = channel.otherUser?.clientId
        else { return }
        
        dependencies.analytics.log(
            .interaction(.sendMessage(channelUrl: channel.channelUrl,
                                      senderUserId: currentUserId,
                                      targetUserId: targetUserId))
        )
    }
    
    func didTapSaveButton() {
        dependencies.analytics.log(.button(.clicked, .saveButton))
    }
    
    func didTapCancelButton() {
        dependencies.analytics.log(.button(.clicked, .cancelButton))
    }
    
    func didDismissScreen(isMovingFromParent: Bool) {
        guard isMovingFromParent else { return }
        dependencies.analytics.log(.button(.clicked, .back(onScreen: .privateChat)))
    }
    
    func isFromChannel(url: String) -> Bool {
        channel.channelUrl == url
    }

    func canOpenChat(url: String) -> Bool {
        dependencies.chatApiManager.getCurrentConnectionState().isConnected
            && isFromChannel(url: url)
    }
    
    // MARK: - Flow
    func dismiss() {
        presenter.dismiss()
    }
    
    func openChatSettings() {
        presenter.openChatSettings(for: channel)
        
        dependencies.analytics.log(.button(.clicked, .openSettings))
    }
    
    func openProfile() {
        guard let otherUserId = channel.otherUser?.clientId.numbers else { return }
        presenter.openProfile(withId: otherUserId)
        
        dependencies.analytics.log(.button(.clicked, .openProfile(onScreen: .privateChat)))
    }
}
