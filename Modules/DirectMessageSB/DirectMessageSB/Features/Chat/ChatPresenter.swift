import Foundation
import SendBirdSDK

protocol ChatPresenting: AnyObject {
    var viewController: ChatDisplaying? { get set }
    
    func setupNavigationBar(with otherUser: ChatUser)
    func setupAdittionalButtons()
    func setupEditionButtons()
    func openChatSettings(for channel: OneOnOneChatting)
    func openProfile(withId id: String)
    func dismiss()
}

final class ChatPresenter {
    private let coordinator: ChatCoordinating
    weak var viewController: ChatDisplaying?
    
    init(coordinator: ChatCoordinating) {
        self.coordinator = coordinator
    }
}

extension ChatPresenter: ChatPresenting {
    func setupNavigationBar(with otherUser: ChatUser) {
        let profileName = otherUser.profileName ?? ""
        let username = otherUser.username ?? ""
        let imageURL = otherUser.avatarUrl ?? ""
        
        viewController?.setupNavigationBar(
            title: profileName,
            subtitle: Strings.User.formattedNickname(username),
            imageURL: imageURL
        )
    }
    
    func setupAdittionalButtons() {
        viewController?.removeAttachmentButton()
    }
    
    func setupEditionButtons() {
        viewController?.setupCancelButton()
        viewController?.setupSaveButton()
    }
    
    func openChatSettings(for channel: OneOnOneChatting) {
        coordinator.perform(action: .openChatSettings(channel: channel))
    }
    
    func openProfile(withId id: String) {
        coordinator.perform(action: .openProfile(id: id))
    }
    
    func dismiss() {
        coordinator.perform(action: .dismiss)
    }
}
