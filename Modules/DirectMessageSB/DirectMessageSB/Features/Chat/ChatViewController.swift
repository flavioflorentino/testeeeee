import SendBirdSDK
import SendBirdUIKit
import AssetsKit
import UI
import UIKit

extension ChatViewController.Layout {
    enum Size {
        static let saveButton = CGSize(width: Sizing.base03, height: Sizing.base03)
    }
    
    enum CornerRadius {
        static let saveButton = Size.saveButton.width / 2
    }
    
    enum Colors {
        static let titleText = UI.Colors.grayscale700.color
        static let navigationBarButtonItems = UI.Colors.brandingBase.color
        static let saveButton = UI.Colors.white.lightColor
    }
    
    enum Fonts {
        static let title = Typography.title(.small).font()
    }
}

protocol ChatDisplaying: SBUChannelViewController {
    func setupNavigationBar(title: String, subtitle: String, imageURL: String)
    func setupSaveButton()
    func setupCancelButton()
    func removeAttachmentButton()
}

public final class ChatViewController: SBUChannelViewController {
    fileprivate enum Layout { }
    
    // MARK: - Properties
    private let interactor: ChatInteracting
    
    // MARK: - Initialization
    init(interactor: ChatInteracting, channel: SBDGroupChannel) {
        self.interactor = interactor
        super.init(channel: channel,
                   messageListParams: interactor.defaultMessageParameters)
        self.hidesBottomBarWhenPushed = true
    }
    
    // MARK: - Views
    
    private lazy var avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView
            .imageStyle(AvatarImageStyle(size: .xSmall))
            .with(\.backgroundColor, .backgroundPrimary())
        return imageView
    }()
    
    private lazy var avatarButtonItem = UIBarButtonItem(customView: avatarImageView)
    
    private lazy var titleDescriptionView = ChatNavigationTitleDescriptionView()
    
    private lazy var titleDescriptionButtonItem = UIBarButtonItem(customView: titleDescriptionView)

    private lazy var infoButton: UIBarButtonItem = {
        UIBarButtonItem(icon: .infoCircle,
                        target: self,
                        action: #selector(didTapInfoButton))
    }()
    
    // MARK: - Overrides
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        .default
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        interactor.initialSetup()
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showNavigationBar()
    }

    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        interactor.setScreenAsViewed()
    }
    
    override public func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        interactor.didDismissScreen(isMovingFromParent: isMovingFromParent)
    }
    
    override public func setLongTapGestureHandler(_ cell: SBUBaseMessageCell, message: SBDBaseMessage, indexPath: IndexPath) {
        super.setLongTapGestureHandler(cell, message: message, indexPath: indexPath)
        
        guard let cell = cell as? MessageCell else { return }
        interactor.didLongPressMessage(cell)
    }
    
    override public func messageInputView(_ messageInputView: SBUMessageInputView, didSelectSend text: String) {
        interactor.didTapSendButton()
        super.messageInputView(messageInputView, didSelectSend: text)
    }
    
    // MARK: - TableView Overrides
    
    override public func tableView(_ tableView: UITableView,
                                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let baseCell = super.tableView(tableView, cellForRowAt: indexPath)
        guard let cell = baseCell as? SBUUserMessageCell else { return baseCell }
        cell.setupCell()
        return cell
    }

    @objc
    private func didTapInfoButton() {
        interactor.openChatSettings()
    }
    
    @objc
    fileprivate func didTapUsername() {
        interactor.openProfile()
    }
    
    @objc
    private func trackingSaveButton() {
        interactor.didTapSaveButton()
    }
    
    @objc
    private func trackingCancelButton() {
        interactor.didTapCancelButton()
    }
}

extension ChatViewController: ChatDisplaying {
    func setupNavigationBar(title: String, subtitle: String, imageURL: String) {
        setupNavigationStyle()
        
        titleDescriptionView.set(title: title, subtitle: subtitle)
        avatarImageView.setImage(
            url: URL(string: imageURL),
            placeholder: Resources.Placeholders.greenAvatarPlaceholder.image
        )
        
        setupNavigationBarButtonItems()
        addNavigationBarGestureRecognizers()
    }
    
    /// Removes attachment button (camera, photo library, documents)
    func removeAttachmentButton() {
        messageInputView.addButton?.removeFromSuperview()
    }
    
    func setupSaveButton() {
        guard let saveButton = messageInputView.saveButton else { return }
        saveButton.backgroundColor = .clear
        saveButton.size = Layout.Size.saveButton
        saveButton.layer.cornerRadius = Layout.CornerRadius.saveButton
        saveButton.setTitle(Iconography.check.rawValue, for: .normal)
        saveButton.addTarget(self, action: #selector(trackingSaveButton), for: .touchUpInside)
    }
    
    func setupCancelButton() {
        guard let cancelButton = messageInputView.cancelButton else { return }
        let attributes: [NSAttributedString.Key: Any] = [
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        let attributedText = NSAttributedString(string: Strings.Button.cancel, attributes: attributes)
        cancelButton.setAttributedTitle(attributedText, for: .normal)
        cancelButton.addTarget(self, action: #selector(trackingCancelButton), for: .touchUpInside)
    }
}

private extension ChatViewController {
    func removeSendBirdDefaultViews() {
        navigationItem.titleView = nil
        
        titleView?.removeFromSuperview()
        titleView = nil
        userProfileView?.removeFromSuperview()
        userProfileView = nil
    }
    
    func setupNavigationStyle() {
        removeSendBirdDefaultViews()
        
        navigationController?
            .navigationBar
            .navigationStyle(StandardNavigationStyle())
    }
    
    func setupNavigationBarButtonItems() {
        navigationItem.rightBarButtonItems = [infoButton]
        navigationItem.leftItemsSupplementBackButton = true
        navigationItem.leftBarButtonItems = [
            avatarButtonItem,
            titleDescriptionButtonItem
        ]
    }
    
    func addNavigationBarGestureRecognizers() {
        avatarImageView.addUserTapGestureRecognizer(target: self)
        titleDescriptionView.addUserTapGestureRecognizer(target: self)
    }
    
    func showNavigationBar() {
        navigationController?.isNavigationBarHidden = false
    }
}

public extension ChatViewController {
    func isFromChannel(url: String) -> Bool {
        interactor.isFromChannel(url: url)
    }

    func canOpenChat(url: String) -> Bool {
        interactor.canOpenChat(url: url)
    }
}

private extension UIView {
    func addUserTapGestureRecognizer<T: ChatViewController>(target viewController: T) {
        isUserInteractionEnabled = true
        addGestureRecognizer(
            UITapGestureRecognizer(target: viewController, action: #selector(T.didTapUsername))
        )
    }
}
