import Foundation
import SendBirdUIKit
import UIKit

enum ChatAction: Equatable {
    case dismiss
    case openChatSettings(channel: OneOnOneChatting)
    case openProfile(id: String)
    
    static func == (lhs: ChatAction, rhs: ChatAction) -> Bool {
        switch (lhs, rhs) {
        case (.dismiss, .dismiss):
            return true
        case let (.openChatSettings(userLhs), .openChatSettings(userRhs)):
            return userLhs.isEqual(to: userRhs)
        case let (.openProfile(idLhs), openProfile(idRhs)):
            return idLhs.isEqual(idRhs)
        default:
            return false
        }
    }
}

protocol ChatCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ChatAction)
}

final class ChatCoordinator {
    typealias Dependencies = HasLegacy & HasChatSettingsManufacturing
        
    // MARK: - Properties
    private let dependencies: Dependencies
    weak var viewController: UIViewController?
    private var navigationController: UINavigationController? {
        viewController?.navigationController
    }
    
    // MARK: - Initialization
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - ChatCoordinating
extension ChatCoordinator: ChatCoordinating {
    func perform(action: ChatAction) {
        switch action {
        case .dismiss:
            dismiss()
        case .openChatSettings(let channel):
            openChatSettings(for: channel)
        case .openProfile(let id):
            openProfile(withId: id)
        }
    }
}

private extension ChatCoordinator {
    func dismiss() {
        navigationController?.popViewController(animated: true)
    }
    
    func openChatSettings(for channel: OneOnOneChatting) {
        guard let navigationController = viewController?.navigationController,
              let chatSettingsViewController = dependencies.chatSettingsFactory.make(withChat: channel) else { return }
        
        navigationController.pushViewController(chatSettingsViewController, animated: true)
    }
    
    func openProfile(withId id: String) {
        guard let factory = dependencies.legacy.factory,
              let profileViewController = factory.makeProfileViewController(withId: id)
        else { return }
        
        navigationController?.pushViewController(profileViewController, animated: true)
    }
}
