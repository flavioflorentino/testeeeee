import Foundation
import SnapKit
import UI
import UIKit

extension ChatNotificationBannerView.Layout {
    static let cornerRadius: CGFloat = CornerRadius.medium
    
    enum Size {
        static let bannerHeight: CGFloat = Sizing.base09
        static let imageHeight: CGFloat = Sizing.base05
    }
    
    enum Margin {
        static let small: CGFloat = Spacing.base01
        static let `default`: CGFloat = Spacing.base02
    }
    
    enum Animation {
        static let overScreenPosition: CGAffineTransform = .init(translationX: 0, y: -200)
    }
    
    enum ShowAnimation {
        static let duration: TimeInterval = 0.6
        static let springDamping: CGFloat = 5
        static let springVelocity: CGFloat = 5
        static let options: UIView.AnimationOptions = .curveEaseIn
    }
    
    enum DismissAnimation {
        static let duration: TimeInterval = 0.2
        static let options: UIView.AnimationOptions = .curveEaseOut
    }
}

public final class ChatNotificationBannerView: UIView {
    fileprivate enum Layout { }
    
    // MARK: - Properties
    
    static var presentedView: ChatNotificationBannerView?
    
    var title: String {
        didSet {
            titleLabel.text = title
        }
    }
    var message: String {
        didSet {
            messageLabel.text = message
        }
    }
    var imageURL: URL? {
        didSet {
            imageView.setImage(url: imageURL)
        }
    }
    var onTapAction: ((ChatNotificationBannerView) -> Void)?
    
    private var dismissTimer: Timer?
    
    // MARK: - Initialization
    
    init(title: String, message: String, imageURL: URL?, onTap: ((ChatNotificationBannerView) -> Void)? = nil) {
        self.title = title
        self.message = message
        self.imageURL = imageURL
        self.onTapAction = onTap
        super.init(frame: .zero)
    }
    
    init(viewModel: ChatNotificationBannerViewModeling) {
        self.title = viewModel.title
        self.message = viewModel.message
        self.imageURL = viewModel.photoUrl
        self.onTapAction = viewModel.onTapAction
        super.init(frame: .zero)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) { nil }
    
    // MARK: - Views
    
    private lazy var blurEffect: UIBlurEffect = {
        if #available(iOS 13.0, *) {
            return UIBlurEffect(style: .systemThinMaterialDark)
        } else {
            return UIBlurEffect(style: .dark)
        }
    }()
    
    private lazy var blurEffectView: UIVisualEffectView = {
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        blurEffectView.layer.cornerRadius = Layout.cornerRadius
        blurEffectView.layer.masksToBounds = true
        blurEffectView.frame = self.bounds
        return blurEffectView
    }()
    
    lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .leading
        stackView.distribution = .equalCentering
        stackView.spacing = Spacing.base00
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        return stackView
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .white)
            .with(\.numberOfLines, 1)
        return label
    }()
    
    lazy var messageLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .white)
            .with(\.numberOfLines, 1)
        return label
    }()
    
    lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.imageStyle(RoundedImageStyle(size: .small, cornerRadius: .full))
        return imageView
    }()
    
    // MARK: - Internal Methods
    
    @objc
    func didTap() {
        onTapAction?(self)
    }
    
    func showAnimated(onView superview: UIView, secondsToDismiss: TimeInterval) {
        UINotificationFeedbackGenerator().notificationOccurred(.success)
        ChatNotificationBannerView.presentedView = self
        
        transform = Layout.Animation.overScreenPosition
        superview.addSubview(self)
        buildLayout()
        
        UIView.animate(
            withDuration: Layout.ShowAnimation.duration,
            delay: 0,
            usingSpringWithDamping: Layout.ShowAnimation.springDamping,
            initialSpringVelocity: Layout.ShowAnimation.springVelocity,
            options: Layout.ShowAnimation.options,
            animations: {
                self.transform = .identity
            }, completion: { _ in
                self.createDismissTimer(withInterval: secondsToDismiss)
            }
        )
    }
}

extension ChatNotificationBannerView: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(blurEffectView)
        addSubview(imageView)
        addSubview(stackView)
        
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(messageLabel)
    }
    
    public func setupConstraints() {
        guard let superview = self.superview else { return }
        
        snp.makeConstraints {
            $0.top.equalTo(superview.compatibleSafeArea.top).offset(Spacing.base02)
            $0.centerX.equalToSuperview()
            $0.height.greaterThanOrEqualTo(Layout.Size.bannerHeight)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base01)
        }
        
        imageView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(Layout.Margin.small)
            $0.width.height.equalTo(Layout.Size.imageHeight)
        }
        
        stackView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalTo(imageView.snp.trailing).offset(Layout.Margin.small)
            $0.trailing.equalToSuperview().offset(Layout.Margin.small)
        }
    }
    
    public func configureViews() {
        translatesAutoresizingMaskIntoConstraints = false
        isUserInteractionEnabled = true
        
        titleLabel.text = title
        messageLabel.text = message
        imageView.setImage(url: imageURL)
        addGestures()
    }
    
    public func configureStyles() {
        layer.cornerRadius = Layout.cornerRadius
    }
}

private extension ChatNotificationBannerView {
    private func createDismissTimer(withInterval interval: TimeInterval) {
        self.dismissTimer?.invalidate()
        self.dismissTimer = Timer.scheduledTimer(withTimeInterval: interval, repeats: false) { [weak self] _ in
            self?.dismiss()
        }
    }
    
    private func addGestures() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTap))
        addGestureRecognizer(tapGesture)
    }
}

public extension ChatNotificationBannerView {
    func dismiss() {
        UIView.animate(
            withDuration: Layout.DismissAnimation.duration,
            delay: 0,
            options: Layout.DismissAnimation.options,
            animations: {
                self.transform = Layout.Animation.overScreenPosition
            }, completion: { _ in
                self.removeFromSuperview()
                ChatNotificationBannerView.presentedView = nil
            }
        )
    }
    
    static func present(onView superview: UIView,
                        title: String,
                        message: String,
                        imageURL: URL?,
                        secondsToDismiss: TimeInterval = 5,
                        tapAction: ((ChatNotificationBannerView) -> Void)? = nil) {
        guard let presentedView = ChatNotificationBannerView.presentedView else {
            let notificationView = ChatNotificationBannerView(
                title: title,
                message: message,
                imageURL: imageURL,
                onTap: tapAction
            )
            
            notificationView.showAnimated(onView: superview, secondsToDismiss: secondsToDismiss)
            return
        }
        
        presentedView.title = title
        presentedView.message = message
        presentedView.imageURL = imageURL
        presentedView.createDismissTimer(withInterval: secondsToDismiss)
    }
    
    static func present(onView superview: UIView,
                        viewModel: ChatNotificationBannerViewModel) {
        present(onView: superview,
                title: viewModel.title,
                message: viewModel.message,
                imageURL: viewModel.photoUrl,
                secondsToDismiss: viewModel.secondsToDismiss,
                tapAction: viewModel.onTapAction)
    }
}
