import Foundation

public protocol ChatNotificationDataMapping {
    func isChatNotification(_ hashable: [AnyHashable: Any]) -> Bool
    
    func decode(fromData data: Data) -> ChatNotification?
    func decode(fromDictionary dictionary: [AnyHashable: Any]) -> ChatNotification?
}

final class ChatNotificationDataMapper {
    enum Constants {
        static let sendbirdRemoteNotificationKey = "sendbird"
    }
    
    let jsonDecoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .custom { decoder in
            var container = try decoder.singleValueContainer()
            let timeInterval = TimeInterval(try container.decode(UInt.self))
            let date = Date(timeIntervalSince1970: timeInterval)
            return date
        }
        return decoder
    }()
}

extension ChatNotificationDataMapper: ChatNotificationDataMapping {
    func isChatNotification(_ hashable: [AnyHashable: Any]) -> Bool {
        hashable[Constants.sendbirdRemoteNotificationKey] != nil
    }
    
    func decode(fromData data: Data) -> ChatNotification? {
        try? jsonDecoder.decode(ChatNotification.self, from: data)
    }
    
    func decode(fromDictionary dictionary: [AnyHashable: Any]) -> ChatNotification? {
        guard let sendbirdUserInfo = dictionary[Constants.sendbirdRemoteNotificationKey] as? [AnyHashable: Any],
              let sendbirdInfoData = sendbirdUserInfo.toData(),
              let notification = ChatNotification(data: sendbirdInfoData)
        else { return nil }
        return notification
    }
}
