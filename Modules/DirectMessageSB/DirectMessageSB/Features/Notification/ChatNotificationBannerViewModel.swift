import Foundation

public protocol ChatNotificationBannerViewModeling {
    var title: String { get }
    var message: String { get }
    var photoUrl: URL? { get }
    var secondsToDismiss: TimeInterval { get }
    var onTapAction: ((ChatNotificationBannerView) -> Void) { get }
}

public final class ChatNotificationBannerViewModel: ChatNotificationBannerViewModeling {
    // MARK: - Properties
    let notification: ChatNotification
    private var sender: ChatNotification.Sender? { notification.sender }
    
    public var title: String {
        Strings.Notification.titleWithUserNickname(sender?.name ?? "")
    }
    
    public var message: String {
        notification.message ?? ""
    }
    
    public var photoUrl: URL? {
        URL(string: sender?.profileURL ?? "")
    }
    
    public let secondsToDismiss: TimeInterval = 5
    
    public let onTapAction: ((ChatNotificationBannerView) -> Void)
    
    // MARK: - Initialization
    public init(_ notification: ChatNotification, onTapAction: @escaping (ChatNotificationBannerView) -> Void) {
        self.notification = notification
        self.onTapAction = onTapAction
    }
}
