import Foundation

public struct ChatNotification: Decodable, Equatable {
    // MARK: - Properties
    
    public let category: String?
    public let channelType: String?
    public let pushSound: String?
    public let appID: String?
    public let type: String?
    public let messageID: Int?
    public let message: String?
    public let audienceType: String?
    public let createdAt: Date?
    public let customType: String?
    public let unreadMessageCount: Int?
    public let channel: Channel?
    public let recipient: Recipient?
    public let sender: Sender?
    
    public static let dataMapper: ChatNotificationDataMapping = ChatNotificationDataMapper()
    
    // MARK: - Initialization
    
    public init?(data: Data) {
        guard let notification = ChatNotification.dataMapper.decode(fromData: data)
        else { return nil }
        self = notification
    }
    
    public init?(dictionary: [AnyHashable: Any]) {
        guard let notification = ChatNotification.dataMapper.decode(fromDictionary: dictionary)
        else { return nil }
        self = notification
    }
    
    // MARK: - Structs
    
    public struct Channel: Decodable, Equatable {
        public let channelURL: String?
        public let name: String?
        public let customType: String?
    }

    public struct Recipient: Decodable, Equatable {
        public let id: String?
        public let name: String?
        public let pushTemplate: String?
    }

    public struct Sender: Decodable, Equatable {
        public let id: String?
        public let name: String?
        public let profileURL: String?
    }
}

extension ChatNotification {
    enum CodingKeys: String, CodingKey {
        case category
        case channelType = "channel_type"
        case pushSound = "push_sound"
        case channel
        case appID = "app_id"
        case recipient
        case type
        case messageID = "message_id"
        case message
        case audienceType = "audience_type"
        case createdAt = "created_at"
        case customType = "custom_type"
        case sender
        case unreadMessageCount = "unread_message_count"
    }
}

extension ChatNotification.Channel {
    enum CodingKeys: String, CodingKey {
        case name
        case channelURL = "channel_url"
        case customType = "custom_type"
    }
}

extension ChatNotification.Recipient {
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case pushTemplate = "push_template"
    }
}

extension ChatNotification.Sender {
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case profileURL = "profile_url"
    }
}
