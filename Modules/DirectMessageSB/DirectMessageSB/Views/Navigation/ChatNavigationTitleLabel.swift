import UI
import UIKit

private extension ChatNavigationTitleLabel.Layout {
    enum Fonts {
        static let navigationTitle = Typography.title(.small).font()
    }
}

final class ChatNavigationTitleLabel: UILabel {
    fileprivate enum Layout { }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) { nil }
    
    init(text: String) {
        super.init(frame: .zero)
        self.labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, Colors.grayscale700.color)
            .with(\.font, Layout.Fonts.navigationTitle)
            .with(\.text, text)
        self.sizeToFit()
    }
    
    override var intrinsicContentSize: CGSize {
        UIView.layoutFittingExpandedSize
    }
}
