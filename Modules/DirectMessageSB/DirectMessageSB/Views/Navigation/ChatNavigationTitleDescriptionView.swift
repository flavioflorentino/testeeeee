import Foundation
import SnapKit
import UI
import UIKit

extension ChatNavigationTitleDescriptionView.Layout {
    enum Margin {
        static let small: CGFloat = Spacing.base01
        static let medium: CGFloat = Spacing.base02
    }
    
    enum Colors {
        static let title = UI.Colors.grayscale700.color
        static let subtitle = UI.Colors.grayscale500.color
    }
    
    enum Fonts {
        static let title = Typography.title(.small).font()
        static let subtitle = Typography.bodyPrimary(.default).font()
    }
}

public final class ChatNavigationTitleDescriptionView: UIView {
    fileprivate enum Layout { }
    
    // MARK: - Properties
    
    private(set) var title: String {
        didSet {
            titleLabel.updateText(with: title)
        }
    }
    
    private(set) var subtitle: String {
        didSet {
            subtitleLabel.updateText(with: subtitle)
        }
    }
    
    // MARK: - Initialization
    
    init(title: String = "", description: String = "") {
        self.title = title
        self.subtitle = description
        super.init(frame: .zero)
        self.buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) { nil }
    
    // MARK: - Views
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .leading
        stackView.distribution = .equalCentering
        stackView.spacing = Spacing.none
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.numberOfLines, 1)
            .with(\.textColor, Layout.Colors.title)
            .with(\.font, Layout.Fonts.title)
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(CaptionLabelStyle())
            .with(\.numberOfLines, 1)
            .with(\.textColor, Layout.Colors.subtitle)
            .with(\.font, Layout.Fonts.subtitle)
        return label
    }()
    
    // MARK: - Public Methods
    
    public func set(title: String, subtitle: String = "") {
        self.title = title
        self.subtitle = subtitle
    }
}

extension ChatNavigationTitleDescriptionView: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(stackView)
        
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(subtitleLabel)
    }
    
    public func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    public func configureViews() {
        translatesAutoresizingMaskIntoConstraints = false
        
        titleLabel.text = title
        subtitleLabel.text = subtitle
    }
}

private extension UILabel {
    func updateText(with text: String) {
        self.text = text
        self.isHidden = text.isEmpty
    }
}
