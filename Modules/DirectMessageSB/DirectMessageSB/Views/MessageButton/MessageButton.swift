import SnapKit
import UI
import UIKit

private extension MessageButton.Layout {
    enum Size {
        static let image = CGSize(width: 24.0, height: 24.0)
        static let badge: CGFloat = 20
    }
    
    enum CornerRadius {
        static let badge = Size.badge / 2
    }
    
    enum Spacing {
        static let badgeX: CGFloat = 10
        static let badgeY: CGFloat = 12
    }
}

protocol MessageButtonViewModelDelegate: AnyObject {
    func changeBadgeVisibility(isVisible: Bool)
    func changeBadgeText(content: String)
}

public final class MessageButton: UIButton {
    fileprivate enum Layout { }

    public var messagesStatus: TrackedMessagesStatus {
        badge.isHidden ? .clear : .unread
    }

    private lazy var viewModel: MessageButtonViewModeling = MessageButtonViewModel(delegate: self)
    
    // MARK: - Views
    
    private lazy var badge: UILabel = {
        let label = UILabel()
        
        label.labelStyle(CaptionLabelStyle(type: .highlight))
            .with(\.textColor, Colors.white.lightColor)
            .with(\.backgroundColor, Colors.critical600.color)
            .with(\.textAlignment, .center)
            .with(\.numberOfLines, 1)
            .with(\.lineBreakMode, .byClipping)
            .with(\.isHidden, true)
        
        label.layer.cornerRadius = Layout.CornerRadius.badge
        label.layer.masksToBounds = true

        return label
    }()
    
    // MARK: - Initialization

    override public init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) { nil }
    
    // MARK: - Functions

    public func changeUnreadMessagesCount(to count: Int) {
        viewModel.setupButton(unreadMessages: count)
    }
}

extension MessageButton: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(badge)
    }
    
    public func setupConstraints() {
        badge.snp.makeConstraints {
            $0.centerX.equalToSuperview().offset(Layout.Spacing.badgeX)
            $0.centerY.equalToSuperview().offset(-Layout.Spacing.badgeY)
            $0.width.greaterThanOrEqualTo(Layout.Size.badge)
            $0.height.equalTo(Layout.Size.badge)
        }
    }
    
    public func configureViews() {
        adjustsImageWhenDisabled = false
        
        let image = UIImage(
            iconName: Iconography.commentDots.rawValue,
            font: Typography.icons(.medium).font(),
            color: Colors.brandingBase.color,
            size: Layout.Size.image
        )
        
        setImage(image, for: .normal)
        tintColor = Colors.branding600.color
        translatesAutoresizingMaskIntoConstraints = false
    }
}

extension MessageButton: MessageButtonViewModelDelegate {
    func changeBadgeVisibility(isVisible: Bool) {
        badge.isHidden = !isVisible
    }
    
    func changeBadgeText(content: String) {
        badge.text = content
    }
}
