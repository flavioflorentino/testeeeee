import Foundation

protocol MessageButtonViewModeling {
    func setupButton(unreadMessages: Int)
}

final class MessageButtonViewModel: MessageButtonViewModeling {
    private weak var delegate: MessageButtonViewModelDelegate?
    
    private let maximumUnreadMessages: Int
    
    init(delegate: MessageButtonViewModelDelegate, maximumUnreadMessages: Int = 999) {
        self.maximumUnreadMessages = maximumUnreadMessages
        self.delegate = delegate
    }
    
    func setupButton(unreadMessages: Int) {
        guard let delegate = self.delegate else { return }
        if unreadMessages <= 0 {
            delegate.changeBadgeVisibility(isVisible: false)
            delegate.changeBadgeText(content: "")
        } else if unreadMessages > maximumUnreadMessages {
            delegate.changeBadgeVisibility(isVisible: true)
            delegate.changeBadgeText(content:
                                        Strings
                                        .MessageButton
                                        .Badge
                                        .maxUnreadMessagesCount(maximumUnreadMessages))
        } else {
            delegate.changeBadgeVisibility(isVisible: true)
            delegate.changeBadgeText(content: String(unreadMessages))
        }
    }
}
