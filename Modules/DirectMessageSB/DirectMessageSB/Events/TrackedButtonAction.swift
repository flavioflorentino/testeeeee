public enum TrackedButtonAction: TrackeableButtonAction {
    case back(onScreen: TrackedScreen)
    case newChat
    case openChats(status: TrackedMessagesStatus)
    case openChatViaProfile
    case openChat(status: TrackedMessagesStatus)
    case saveButton
    case cancelButton
    case createNewChatViaSearch
    case createNewChatViaSB
    case generalNotificationSettings(status: TrackedToggleStatus)
    case openProfile(onScreen: TrackedScreen)
    case openSettings

    enum Property: String, EventProperty {
        case buttonName = "button_name"

        var key: String {
            rawValue
        }
    }

    var name: String {
        switch self {
        case .back:
            return "VOLTAR"
        case .newChat:
            return "DM_NOVA_CONVERSA"                   
        case .openChats,
             .openChatViaProfile:
            return "DM"
        case .openChat:
            return "DM_CHAT_CONVERSA"
        case .saveButton:
            return "CONFIRMAR_EDICAO_MENSAGEM"
        case .cancelButton:
            return "CANCELAR_EDICAO_MENSAGEM"
        case .createNewChatViaSearch,
             .createNewChatViaSB:
            return "DM_USUARIO"
        case .generalNotificationSettings:
            return "NOTIFICACOES_DM"
        case .openProfile:
            return "PROFILE"
        case .openSettings:
            return "DM_INFO"
        }
    }

    var screen: TrackedScreen {
        switch self {
        case let .back(screen):
            return screen
        case .newChat:
            return .chats
        case .openChats:
            return .home
        case .openChatViaProfile:
            return .userProfile
        case .openChat:
            return .chats
        case .saveButton,
             .cancelButton,
             .openSettings:
            return .privateChat
        case .createNewChatViaSearch:
            return .newChatSearch
        case .createNewChatViaSB:
            return .newChatSB
        case .generalNotificationSettings:
            return .notificationSettings
        case let .openProfile(screen):
            return screen
        }
    }

    var properties: Properties {
        let properties = screen.properties.merging([Property.buttonName.key: name], uniquingKeysWith: { $1 })
        switch self {
        case let .openChats(status),
             let .openChat(status):
            return properties.merging(status.properties, uniquingKeysWith: { $1 })
        case let .generalNotificationSettings(status):
            return properties.merging(status.properties, uniquingKeysWith: { $1 })
        default:
            return properties
        }
    }
}
