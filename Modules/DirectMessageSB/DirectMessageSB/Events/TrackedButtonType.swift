public enum TrackedButtonInteraction: String, Trackeable {
    case clicked = "Button Clicked"
    case toggled = "Button Toggled"
    
    var name: String {
        rawValue
    }
}
