public enum TrackedDialog: TrackeableDialog {
    case sentMessageOptions
    case receivedMessageOptions
    case connectionError(onScreen: TrackedScreen, withMessage: String)
    
    enum Property: String, EventProperty {
        case dialogName = "dialog_name"
        case dialogText = "dialog_text"
        
        var key: String {
            rawValue
        }
    }
    
    var name: String {
        switch self {
        case .sentMessageOptions:
            return "DM_OPCOES_MENSAGEM_ENVIADA"
        case .receivedMessageOptions:
            return "DM_OPCOES_MENSAGEM_RECEBIDA"
        case .connectionError:
            return "DM_ERRO_CONEXAO"
        }
    }
    
    var text: String? {
        switch self {
        case let .connectionError(_, message):
            return message
        default:
            return nil
        }
    }
    
    var screen: TrackedScreen {
        switch self {
        case .sentMessageOptions, .receivedMessageOptions:
            return .privateChat
        case let .connectionError(screen, _):
            return screen
        }
    }
    
    var properties: Properties {
        let dialogProperties = [
            Property.dialogName.key: name,
            Property.dialogText.key: text
        ].compactMapValues { $0 }
        return screen.properties.merging(dialogProperties, uniquingKeysWith: { $1 })
    }
}
