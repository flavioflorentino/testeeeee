protocol Trackeable {
    var name: String { get }
}

protocol TrackeableScreen: Trackeable, PropertiesConvertible {
    var context: BusinessContext { get }
}

protocol TrackeableDialog: Trackeable, PropertiesConvertible {
    var screen: TrackedScreen { get }
}

protocol TrackeableDialogOption: Trackeable, PropertiesConvertible {
    var dialog: TrackedDialog { get }
}

protocol TrackeableButtonAction: Trackeable, PropertiesConvertible {
    var screen: TrackedScreen { get }
}

protocol TrackeableToggle: TrackeableButtonAction {
    var value: Bool { get }
}

protocol TrackeableInteraction: Trackeable, PropertiesConvertible {
    var context: BusinessContext { get }
}
