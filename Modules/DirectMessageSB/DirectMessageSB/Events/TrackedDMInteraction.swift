public enum TrackedDMInteraction: TrackeableInteraction {
    case sendMessage(channelUrl: String,
                     senderUserId: String,
                     targetUserId: String)
    
    enum Property: String, EventProperty {
        case interactionType = "interaction_type"
        case channelUrl = "channel_url"
        case senderUserId = "dm_user_id"
        case targetUserId = "target_dm_user_id"
        
        var key: String {
            rawValue
        }
    }
    
    var name: String {
        switch self {
        case .sendMessage:
            return "ENVIAR_MENSAGEM"
        }
    }
    
    var context: BusinessContext {
        .dm
    }
    
    var properties: Properties {
        var interactionProperties = [Property.interactionType.key: name]
        
        switch self {
        case let .sendMessage(channelUrl, senderUserId, targetUserId):
            interactionProperties[Property.channelUrl.rawValue] = channelUrl
            interactionProperties[Property.senderUserId.rawValue] = senderUserId
            interactionProperties[Property.targetUserId.rawValue] = targetUserId
        }
        
        return context.properties.merging(interactionProperties, uniquingKeysWith: { $1 })
    }
}
