public enum TrackedMessagesStatus: String, Trackeable, PropertiesConvertible {
    case clear = "SEM_NOTIFICACAO"
    case unread = "COM_NOTIFICACAO"

    enum Property: String, EventProperty {
        case buttonStatus = "button_status"

        var key: String {
            rawValue
        }
    }

    var name: String {
        rawValue
    }

    var properties: Properties {
        [Property.buttonStatus.key: name]
    }
}
