public enum TrackedScreen: TrackeableScreen {
    case chats
    case home
    case newChatSearch
    case newChatSB
    case privateChat
    case userProfile
    case chatSettings
    case chatSetup
    case notificationSettings

    enum Property: String, EventProperty {
        case screenName = "screen_name"

        var key: String {
            rawValue
        }
    }

    var name: String {
        switch self {
        case .chats:
            return "DM_CONVERSAS"
        case .home:
            return "INICIO"
        case .newChatSearch:
            return "DM_NOVA_CONVERSA_BUSCA"
        case .newChatSB:
            return "DM_NOVA_CONVERSA_LISTA"
        case .privateChat:
            return "DM_CHAT_CONVERSA"
        case .userProfile:
            return "PERFIL_USUARIO"
        case .chatSettings:
            return "DM_INFO_CONVERSA"
        case .chatSetup:
            return "CHATSETUP"
        case .notificationSettings:
            return "AJUSTES_NOTIFICACOES"
        }
    }

    var context: BusinessContext {
        .dm
    }

    var properties: Properties {
        context.properties.merging([Property.screenName.key: name], uniquingKeysWith: { $1 })
    }
}
