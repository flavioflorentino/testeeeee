public enum TrackedToggleStatus: PropertiesConvertible {
    case turnOn
    case turnOff

    enum Property: String, EventProperty {
        case toggleDidChange = "flag_changed_to"

        var key: String {
            rawValue
        }
    }
    
    var properties: Properties {
        [Property.toggleDidChange.key: value]
    }

    var value: Bool {
        self == .turnOn
    }
}
