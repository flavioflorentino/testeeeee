enum BusinessContext: String, PropertiesConvertible {
    case dm = "DM"
    case home = "INICIO"
    case notifications = "NOTIFICACOES"

    enum Property: String, EventProperty {
        case businessContext = "business_context"

        var key: String {
            rawValue
        }
    }

    var properties: Properties {
        [Property.businessContext.key: rawValue]
    }
}
