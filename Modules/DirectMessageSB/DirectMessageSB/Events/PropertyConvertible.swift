typealias Properties = [String: Any]

protocol EventProperty {
    var key: String { get }
}

protocol PropertiesConvertible {
    associatedtype Property: EventProperty

    var properties: Properties { get }
}

extension Properties {
    func filtering(properties: [EventProperty]) -> Self {
        var filteredProperties = self
        properties.forEach { filteredProperties.removeValue(forKey: $0.key) }
        return filteredProperties
    }
}
