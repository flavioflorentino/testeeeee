public enum TrackedDialogOption: TrackeableDialogOption {
    case tryAgain(onDialog: TrackedDialog)
    case cancel(onDialog: TrackedDialog)
    
    enum Property: String, EventProperty {
        case optionSelected = "option_selected"
        
        var key: String {
            rawValue
        }
    }
    
    var name: String {
        switch self {
        case .tryAgain:
            return "TENTAR_NOVAMENTE"
        case .cancel:
            return "CANCELAR"
        }
    }
    
    var dialog: TrackedDialog {
        switch self {
        case let .tryAgain(dialog), let .cancel(dialog):
            return dialog
        }
    }
    
    var properties: Properties {
        dialog.properties
            .merging([Property.optionSelected.key: name], uniquingKeysWith: { $1 })
            .filtering(properties: [TrackedDialog.Property.dialogText])
    }
}
