import AnalyticsModule
import Foundation

public enum DirectMessageSBAnalytics: AnalyticsKeyProtocol {
    case screenViewed(TrackedScreen)
    case dialogViewed(TrackedDialog)
    case dialogOptionSelected(TrackedDialogOption)
    case button(TrackedButtonInteraction, TrackedButtonAction)
    case interaction(TrackedDMInteraction)
    
    var name: String {
        switch self {
        case .screenViewed:
            return "Screen Viewed"
        case .dialogViewed:
            return "Dialog Viewed"
        case .dialogOptionSelected:
            return "Dialog Option Selected"
        case let .button(buttonType, _):
            return buttonType.name
        case .interaction:
            return "Direct Message Interacted"
        }
    }

    private var properties: Properties {
        switch self {
        case let .screenViewed(screen):
            return screen.properties
        case let .dialogViewed(dialog):
            return dialog.properties
        case let .dialogOptionSelected(dialog):
            return dialog.properties
        case let .button(_, action):
            return action.properties
        case let .interaction(interaction):
            return interaction.properties
        }
    }

    private var providers: [AnalyticsProvider] {
        [.eventTracker]
    }

    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}

public extension AnalyticsProtocol {
    func log(_ dmsbEventKey: DirectMessageSBAnalytics) {
        log(dmsbEventKey)
    }
}
