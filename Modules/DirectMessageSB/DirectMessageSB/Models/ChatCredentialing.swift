protocol ChatCredentialing {
    var token: String { get }
}

struct ChatCredentials: Codable, ChatCredentialing {
    let token: String
}
