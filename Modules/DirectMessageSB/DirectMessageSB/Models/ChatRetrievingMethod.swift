public enum ChatRetrievingMethod {
    case url(String)
    case distinctUser(ChatUser)
}
