import AssetsKit
import UIKit

public struct ChatConnectionErrorAlert: Equatable {
    public struct Options: Equatable {
        public let retryTitle: String
        public let cancelTitle: String

        public init(retryTitle: String, cancelTitle: String) {
            self.retryTitle = retryTitle
            self.cancelTitle = cancelTitle
        }
    }

    public let image: UIImage
    public let title: String
    public let subtitle: String
    public let options: Options

    public init(image: UIImage, title: String, subtitle: String, options: Options = .standard) {
        self.image = image
        self.title = title
        self.subtitle = subtitle
        self.options = options
    }
}

public extension ChatConnectionErrorAlert {
    static var standard: ChatConnectionErrorAlert {
        .init(image: Resources.Illustrations.iluError.image,
              title: Strings.ConnectionState.Error.title,
              subtitle: Strings.ConnectionState.Error.subtitle)
    }
}

public extension ChatConnectionErrorAlert.Options {
    static var standard: ChatConnectionErrorAlert.Options {
        .init(retryTitle: Strings.ConnectionState.Error.retry,
              cancelTitle: Strings.ConnectionState.Error.cancel)
    }
}
