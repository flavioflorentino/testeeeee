public enum MessagingClientError: LocalizedError {
    case nullUser
    case generic(Error)
}
