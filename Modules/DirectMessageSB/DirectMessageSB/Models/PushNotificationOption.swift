import SendBirdSDK

public enum PushNotificationOption {
    case all
    case off
    case mentionOnly
    
    init(option: SBDPushTriggerOption) {
        switch option {
        case .off:
            self = .off
        case .mentionOnly:
            self = .mentionOnly
        default:
            self = .all
        }
    }
}

extension PushNotificationOption {
    var asTriggerOption: SBDPushTriggerOption {
        switch self {
        case .all:
            return .all
        case .off:
            return .off
        case .mentionOnly:
            return .mentionOnly
        }
    }
}
