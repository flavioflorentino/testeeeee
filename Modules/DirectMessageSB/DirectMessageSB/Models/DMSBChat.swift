#if DEBUG
import SendBirdSDK

struct OneOnOneChat: OneOnOneChatting {
    let channelUrl: String    
    let currentUser: ChatUser?
    let otherUser: ChatUser?
}
#endif
