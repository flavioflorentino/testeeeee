import Foundation
import Search
import SendBirdSDK
import SendBirdUIKit

public struct DMSBChatUser: ChatUser, Equatable {
    public let type: ChatUserType
    public let messagingId: Int
    public let avatarUrl: String?
    public let username: String?
    public let profileName: String?

    public init(messagingId: Int,
                type: ChatUserType,
                username: String? = nil,
                profileName: String? = nil,
                avatarUrl: String? = nil) {
        self.messagingId = messagingId
        self.type = type
        self.username = username
        self.profileName = profileName
        self.avatarUrl = avatarUrl
    }

    public init(messagingId: String,
                type: ChatUserType,
                username: String? = nil,
                profileName: String? = nil,
                avatarUrl: String? = nil) {
        self.init(messagingId: Int(messagingId.numbers) ?? .min,
                  type: type,
                  username: username,
                  profileName: profileName,
                  avatarUrl: avatarUrl)
    }

    init(_ sbuUser: SBUUser) {
        self.init(messagingId: sbuUser.userId,
                  type: ChatUserType(rawCharacter: sbuUser.userId.first) ?? .unknown,
                  username: sbuUser.nickname,
                  avatarUrl: sbuUser.profileUrl)
    }

    init(_ sbdUser: SBDUser) {
        self.init(messagingId: sbdUser.userId,
                  type: ChatUserType(rawCharacter: sbdUser.userId.first) ?? .unknown,
                  username: sbdUser.nickname,
                  profileName: sbdUser.profileName,
                  avatarUrl: sbdUser.profileUrl)
    }

    init(_ consumerData: SearchResult.ConsumerData) {
        self.init(messagingId: consumerData.id,
                  type: .consumer,
                  username: consumerData.username,
                  profileName: consumerData.name,
                  avatarUrl: consumerData.smallImageUrl?.absoluteString)
    }
}

extension SBDUser {
    var profileName: String? {
        metaData?["profile_name"] ?? nickname
    }
}
