public enum ChatUserType: String {
    case consumer = "C"
    case seller = "S"
    case unknown

    init?(rawCharacter: Character?) {
        guard let character = rawCharacter else { return nil }
        self.init(rawValue: String(character))
    }
}

public protocol ChatUser {
    var messagingId: Int { get }
    var type: ChatUserType { get }
    var avatarUrl: String? { get }
    var username: String? { get }
    var profileName: String? { get }
}

public extension ChatUser {
    var clientId: String {
        "\(type.rawValue)\(messagingId)"
    }
}

extension ChatUser {
    func isEqual(to user: ChatUser) -> Bool {
        self.messagingId == user.messagingId &&
        self.type == user.type &&
        self.username == user.username &&
        self.profileName == user.profileName &&
        self.avatarUrl == user.avatarUrl &&
        self.clientId == user.clientId
    }
}
