import SendBirdSDK

public enum ConnectionState {
    case connecting
    case failure
    case open
    case closed

    init(_ state: SBDWebSocketConnectionState) {
        switch state {
        case .open:
            self = .open
        case .connecting:
            self = .connecting
        default:
            self = .closed
        }
    }

    public var isConnected: Bool {
        self == .open
    }
}
