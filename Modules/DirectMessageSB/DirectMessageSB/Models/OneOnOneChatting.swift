import SendBirdUIKit

protocol OneOnOneChatting {
    var channelUrl: String { get }
    var currentUser: ChatUser? { get }
    var otherUser: ChatUser? { get }
}

extension SBDGroupChannel: OneOnOneChatting {
    /// Current user of this chat
    var currentUser: ChatUser? {
        guard let sbdUser = SBDMain.getCurrentUser() else { return nil }
        return DMSBChatUser(sbdUser)
    }

    /// Other user for this chat
    var otherUser: ChatUser? {
        let channelMembers = self.members as? [SBDMember] ?? []
        guard let sbdMember = channelMembers.first(where: { $0.userId != currentUser?.clientId })
        else { return nil }
        return DMSBChatUser(sbdMember)
    }
}

extension OneOnOneChatting {
    func isEqual(to channel: OneOnOneChatting) -> Bool {
        self.channelUrl == channel.channelUrl
    }
}
