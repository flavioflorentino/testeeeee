public enum ChatRetriever {
    case url(String)
    case user(ChatUser)
}
