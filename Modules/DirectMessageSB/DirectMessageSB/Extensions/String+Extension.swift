import Foundation

extension String {
    var numbers: String {
        components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
    }
}
