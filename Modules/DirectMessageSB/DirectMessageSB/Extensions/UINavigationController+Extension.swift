import UIKit

public extension UINavigationController {
    func pushReplacingLastController(_ viewController: UIViewController, animated: Bool) {
        var controllers = viewControllers
        controllers.removeLast()
        controllers.append(viewController)
        
        setViewControllers(controllers, animated: animated)
    }
}
