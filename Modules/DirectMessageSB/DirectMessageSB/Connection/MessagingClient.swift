import Core
import FeatureFlag
import SendBirdSDK
import SendBirdUIKit

protocol MessagingClient {
    var connectionState: ConnectionState { get }
    func setup()
    func connect(withConfiguration configuration: ChatConnectionConfiguring, completion: @escaping (Result<Void, Error>) -> Void)
    func disconnect(completion: (() -> Void)?)
    func registerDeviceToken()
    func addPushDeviceToken(_ data: Data)
    func getPushNotificationOption(completion: @escaping (Result<PushNotificationOption, MessagingClientError>) -> Void)
    func setPushNotificationOption(_ option: PushNotificationOption, completion: @escaping (Result<Void, MessagingClientError>) -> Void)
    func fetchUnreadMessages(completion: @escaping (Int) -> Void)
    func canReach(user: ChatUser, completion: @escaping (Result<Bool, MessagingClientError>) -> Void)
    func canReach(users: [ChatUser], completion: @escaping (Result<[ChatUser], MessagingClientError>) -> Void)
    func addConnection(delegate: SBDConnectionDelegate)
}

final class SendbirdClient: NSObject, MessagingClient {
    typealias Dependencies = HasFeatureManager & HasChatNotifier
    
    // MARK: - Properties
    
    private let dependencies: Dependencies
    private let sessionProvider: SessionProvider
    private let messagingProvider: MessagingProvider.Type
    private let themeManager: SendBirdThemeManaging
    private let uniqueDelegateId = UUID().uuidString
    
    var connectionState: ConnectionState {
        messagingProvider.connectionState
    }

    // MARK: - Initialization
    // TODO: Precisamos decidir se faz sentido migrar essas dependências para o DependencyContainer
    init(sessionProvider: SessionProvider = PicPaySessionProvider(),
         messagingProvider: MessagingProvider.Type = SBDMain.self,
         themeManager: SendBirdThemeManaging = SendBirdThemeManager(),
         dependencies: Dependencies = DependencyContainer()) {
        self.sessionProvider = sessionProvider
        self.messagingProvider = messagingProvider
        self.themeManager = themeManager
        self.dependencies = dependencies
    }

    // MARK: - Methods
    
    func setup() {
        themeManager.setup()
        setupMessagingProvider()
    }

    func connect(withConfiguration configuration: ChatConnectionConfiguring, completion: @escaping (Result<Void, Error>) -> Void) {
        sessionProvider.retrieveAccessToken(forClientId: configuration.clientId) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case let .success(chatCredentials):
                self.connect(withConfiguration: configuration, accessToken: chatCredentials.token, completion: completion)
            case let .failure(error):
                completion(.failure(error))
            }
        }
    }

    func connect(withConfiguration configuration: ChatConnectionConfiguring,
                 accessToken: String,
                 completion: @escaping (Result<Void, Error>) -> Void) {
        messagingProvider.connect(withUserId: configuration.clientId, accessToken: accessToken) { user, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            guard let user = user else {
                let error = SBDError(nsError: MessagingClientError.nullUser)
                completion(.failure(error))
                return
            }
            self.themeManager.set(currentUser: SBUUser(user: user))
            self.updateCurrentUserInfo(withNickname: configuration.username,
                                       profileUrl: configuration.avatarUrl,
                                       completion: completion)
            self.registerDeviceToken()
        }
    }
    
    func disconnect(completion: (() -> Void)? = nil) {
        messagingProvider.removeAllDelegates()
        messagingProvider.unregisterDeviceToken()
        messagingProvider.disconnect(completionHandler: completion)
    }

    func updateCurrentUserInfo(withNickname nickname: String,
                               profileUrl: String?,
                               completion: @escaping (Result<Void, Error>) -> Void) {
        messagingProvider.updateCurrentUserInfo(withNickname: nickname, profileUrl: profileUrl) { error in
            guard let error = error else {
                completion(.success(()))
                return
            }
            completion(.failure(error))
        }
    }
    
    func registerDeviceToken() {
        messagingProvider.registerDeviceToken()
    }
    
    func addPushDeviceToken(_ data: Data) {
        messagingProvider.addPushDeviceToken(data)
    }
    
    func getPushNotificationOption(completion: @escaping (Result<PushNotificationOption, MessagingClientError>) -> Void) {
        messagingProvider.getPushTriggerOption { sbOption, error in
            if let error = error {
                completion(.failure(.generic(error)))
                return
            }
            let option = PushNotificationOption(option: sbOption)
            completion(.success(option))
        }
    }
    
    func setPushNotificationOption(_ option: PushNotificationOption, completion: @escaping (Result<Void, MessagingClientError>) -> Void) {
        messagingProvider.setPushTriggerOption(option.asTriggerOption) { error in
            if let error = error {
                completion(.failure(.generic(error)))
                return
            }
            completion(.success(()))
        }
    }

    func fetchUnreadMessages(completion: @escaping (Int) -> Void) {
        messagingProvider.getTotalUnreadMessageCount { count, error in
            guard error == nil else { return completion(0) }
            completion(Int(count))
        }
    }

    func canReach(user: ChatUser, completion: @escaping (Result<Bool, MessagingClientError>) -> Void) {
        canReach(users: [user]) { result in
            switch result {
            case .success(let users):
                let isUserReachable = users.contains(where: { $0.clientId == user.clientId })
                completion(.success(isUserReachable))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    func canReach(users: [ChatUser], completion: @escaping (Result<[ChatUser], MessagingClientError>) -> Void) {
        guard var listQuery = messagingProvider.createMessagingUserListQuery() else {
            return completion(.failure(.nullUser))
        }
        listQuery.userIdsFilter = users.map(\.clientId)
        listQuery.loadNextPage { users, error in
            if let error = error {
                return completion(.failure(.generic(error)))
            }
            guard let reachableUsers = users?.map(DMSBChatUser.init) else {
                return completion(.failure(.nullUser))
            }
            completion(.success(reachableUsers))
        }
    }

    func addConnection(delegate: SBDConnectionDelegate) {
        messagingProvider.add(delegate, identifier: uniqueDelegateId)
    }
}

private extension SendbirdClient {
    func setupMessagingProvider() {
        messagingProvider.setApplicationId(Credentials.Sendbird.appId)
        messagingProvider.addUserEvent(delegate: self, identifier: uniqueDelegateId)
        
        guard dependencies.featureManager.isActive(.directMessageSBSessionDelegateAvailable) else { return }
        messagingProvider.setSessionDelegate(self)
    }
}

extension SendbirdClient: SBDSessionDelegate {
    func sessionTokenDidRequire(successCompletion success: @escaping (String?) -> Void,
                                failCompletion fail: @escaping () -> Void) {
        guard let clientId = messagingProvider.getCurrentChatUser()?.clientId else { return fail() }
        sessionProvider.retrieveAccessToken(forClientId: clientId) { result in
            switch result {
            case .success(let chatCredentials):
                success(chatCredentials.token)
            case .failure:
                fail()
            }
        }
    }

    func sessionWasClosed() {
        // Intentionaly left blank since it is a non optional method from SBDSessionDelegate.
    }
}

extension SendbirdClient: SBDUserEventDelegate {
    // swiftlint:disable:next discouraged_optional_collection
    func didUpdateTotalUnreadMessageCount(_ totalCount: Int32, totalCountByCustomType: [String: NSNumber]?) {
        dependencies.chatNotifier.postUpdateUnreadMessages(count: Int(totalCount))
    }
}
