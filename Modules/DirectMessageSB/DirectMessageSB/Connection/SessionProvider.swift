import Core
import Foundation

protocol SessionProvider {
    func retrieveAccessToken(forClientId clientId: String, completion: @escaping (Result<ChatCredentialing, Error>) -> Void)
}

final class PicPaySessionProvider: SessionProvider {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }

    func retrieveAccessToken(forClientId clientId: String, completion: @escaping (Result<ChatCredentialing, Error>) -> Void) {
        Api<ChatCredentials>(endpoint: DirectMessageSBEndpoint.accessToken(clientId)).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case let .success((model, _)):
                    completion(.success(model))
                case let .failure(error):
                    completion(.failure(error))
                }
            }
        }
    }
}
