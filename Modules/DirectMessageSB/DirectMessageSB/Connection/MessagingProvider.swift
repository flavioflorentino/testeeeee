import SendBirdSDK

protocol MessagingProvider {
    static var connectionState: ConnectionState { get }
    
    static func initWithApplicationId(_ applicationId: String) -> Bool
    static func connect(withUserId userId: String, accessToken: String?, completionHandler: ((SBDUser?, SBDError?) -> Void)?)
    static func disconnect(completionHandler: (() -> Void)?)
    static func updateCurrentUserInfo(withNickname nickname: String?, profileUrl: String?, completionHandler: ((SBDError?) -> Void)?)
    static func registerDeviceToken()
    static func addPushDeviceToken(_ data: Data)
    static func getPushTriggerOption(completionHandler: @escaping SBDPushTriggerOptionHandler)
    static func setPushTriggerOption(_ pushTriggerOption: SBDPushTriggerOption, completionHandler: ((SBDError?) -> Void)?)
    static func unregisterDeviceToken()
    static func getTotalUnreadMessageCount(completionHandler: ((UInt, SBDError?) -> Void)?)
    static func createMessagingUserListQuery() -> MessagingUserListQuery?
    static func setSessionDelegate(_ delegate: SBDSessionDelegate)
    static func getCurrentChatUser() -> ChatUser?
    static func addUserEvent(delegate: SBDUserEventDelegate, identifier: String)
    static func removeAllDelegates()
    static func add(_ delegate: SBDConnectionDelegate, identifier: String)
}

extension MessagingProvider {
    static func setApplicationId(_ applicationId: String) {
        _ = initWithApplicationId(applicationId)
    }
}

extension SBDMain: MessagingProvider {
    static func removeAllDelegates() {
        removeAllChannelDelegates()
        removeAllUserEventDelegates()
        removeAllConnectionDelegates()
    }
    
    static var connectionState: ConnectionState {
        ConnectionState(getConnectState())
    }
    
    static func registerDeviceToken() {
        guard let tokenData = getPendingPushToken() else { return }
        addPushDeviceToken(tokenData)
    }
    
    static func unregisterDeviceToken() {
        guard let tokenData = getPendingPushToken() else { return }
        unregisterPushToken(tokenData) { _, error in
            if let error = error {
                NSLog("[DMSB] ⚠️ Push unregistration error: %@ ⚠️", error)
                return
            }
            NSLog("[DMSB] ✅ Push unregistration succeeded ✅")
        }
    }
    
    static func addPushDeviceToken(_ data: Data) {
        registerDevicePushToken(data, unique: false) { status, error in
            if let error = error {
                NSLog("[DMSB] ⚠️ Push registration error: %@ ⚠️", error)
                return
            }
            
            switch status {
            case .success:
                NSLog("[DMSB] ✅ Push registration succeeded ✅")
            case .pending:
                NSLog("[DMSB] ⚠️ Push registration is pending ⚠️")
            case .error:
                NSLog("[DMSB] ⚠️ Push registration error ⚠️")
            @unknown default:
                break
            }
        }
    }

    static func createMessagingUserListQuery() -> MessagingUserListQuery? {
        createApplicationUserListQuery()
    }

    static func getCurrentChatUser() -> ChatUser? {
        guard let sbdUser = getCurrentUser() else { return nil }
        return DMSBChatUser(sbdUser)
    }
    
    static func addUserEvent(delegate: SBDUserEventDelegate, identifier: String) {
        add(delegate, identifier: identifier)
    }
}

// swiftlint:disable discouraged_optional_collection
protocol MessagingUserListQuery {
    var userIdsFilter: [String]? { get set }
    func loadNextPage(completionHandler: (([SBDUser]?, SBDError?) -> Void)?)
}

extension SBDApplicationUserListQuery: MessagingUserListQuery {}
