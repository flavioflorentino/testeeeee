import Foundation

public protocol NotificationCenterType {
    // swiftlint:disable:next discouraged_optional_collection
    func post(name aName: NSNotification.Name, object anObject: Any?, userInfo aUserInfo: [AnyHashable: Any]?)
}

public protocol HasChatNotifier {
    var chatNotifier: ChatNotifying { get }
}

public protocol ChatNotifying {
    func postUpdateUnreadMessages(count: Int)
}

public final class ChatNotifier: ChatNotifying {
    typealias Dependencies = HasNotificationCenter
    
    // MARK: - Internal enums
    
    public enum NotificationName: String {
        case didUpdateUnreadMessagesCount
    }
    public enum NotificationUserInfoKey: String {
        case totalCount
    }
    
    // MARK: - Properties
    
    private let notificationCenter: NotificationCenterType
    public static let shared = ChatNotifier(notificationCenter: NotificationCenter.default)
    
    // MARK: - Initialization
    
    init(notificationCenter: NotificationCenterType) {
        self.notificationCenter = notificationCenter
    }
}

public extension ChatNotifier.NotificationName {
    var notificationName: Notification.Name {
        Notification.Name(rawValue)
    }
}

extension NotificationCenter: NotificationCenterType { }

// MARK: - ChatNotifying
public extension ChatNotifier {
    func postUpdateUnreadMessages(count: Int) {
        notificationCenter.post(
            name: NotificationName.didUpdateUnreadMessagesCount.notificationName,
            object: nil,
            userInfo: [
                NotificationUserInfoKey.totalCount: count
            ]
        )
    }
}
