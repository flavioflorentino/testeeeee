import AssetsKit
import AnalyticsModule
import UI
import UIKit

public protocol ConnectionStateSceneStandardFlow { }

public protocol ConnectionStateServicing: HasChatApiManager {
    func cancelActionForConnectionState()
    func setActionForConnectionState(completion: @escaping ((ConnectionState) -> Void))
    func reconnect()
}

public extension ConnectionStateServicing where Self: ConnectionStateSceneStandardFlow {
    func cancelActionForConnectionState() {
        chatApiManager.cancelActionForConnectionState()
    }

    func setActionForConnectionState(completion: @escaping ((ConnectionState) -> Void)) {
        chatApiManager.setActionForConnectionState(completion: completion)
    }

    func reconnect() {
        chatApiManager.connect()
    }
}

public protocol ConnectionStateInteractorTracking: AnyObject, HasAnalytics {
    var trackableScreen: TrackedScreen? { get }
    var trackableDialog: TrackedDialog? { get set }
    
    func logConnectionErrorDialog()
    func logConnectionErrorSelectedTryAgainOption()
    func logConnectionErrorSelectedCancelOption()
}

public protocol ConnectionStateInteracting: ConnectionStateInteractorTracking {
    var connectionStateService: ConnectionStateServicing { get }
    var connectionStatePresenter: ConnectionStatePresenting { get }
    func setConnectionActions()
    func didSucceedConnection()
    func didFailConnection()
    func didCancelAction()
    func reconnect()
}

public extension ConnectionStateInteracting where Self: ConnectionStateSceneStandardFlow {
    func setConnectionActions() {
        connectionStateService.setActionForConnectionState { [weak self] state in
            guard let self = self else { return }
            switch state {
            case .connecting:
                self.connectionStatePresenter.startConnectionLoading()
            case .open:
                self.connectionStatePresenter.stopConnectionLoading {
                    self.didSucceedConnection()
                }
            case .failure, .closed:
                self.connectionStatePresenter.stopConnectionLoading {
                    self.didFailConnection()
                }
            }
        }
    }

    func reconnect() {
        connectionStateService.reconnect()
        setConnectionActions()
    }

    func didCancelAction() {
        connectionStatePresenter.stopConnectionLoading(completion: nil)
        connectionStateService.cancelActionForConnectionState()
    }

    func didFailConnection() {
        connectionStatePresenter.presentConnectionError()
        self.logConnectionErrorDialog()
    }
    
    func logConnectionErrorDialog() {
        guard let trackableScreen = self.trackableScreen else { return }
        let trackedDialog: TrackedDialog = .connectionError(
            onScreen: trackableScreen,
            withMessage: connectionStatePresenter.connectionErrorAlert.subtitle
        )
        analytics.log(.dialogViewed(trackedDialog))
        trackableDialog = trackedDialog
    }
    
    func logConnectionErrorSelectedTryAgainOption() {
        guard let trackedDialog = self.trackableDialog else { return }
        analytics.log(.dialogOptionSelected(.tryAgain(onDialog: trackedDialog)))
    }
    
    func logConnectionErrorSelectedCancelOption() {
        guard let trackedDialog = self.trackableDialog else { return }
        analytics.log(.dialogOptionSelected(.cancel(onDialog: trackedDialog)))
    }
}

public protocol ConnectionStatePresenting: AnyObject {
    var connectionStateDisplay: ConnectionStateDisplaying? { get }
    var connectionErrorAlert: ChatConnectionErrorAlert { get }
    func startConnectionLoading()
    func stopConnectionLoading(completion: (() -> Void)?)
    func presentConnectionError()
}

public extension ConnectionStatePresenting where Self: ConnectionStateSceneStandardFlow {
    var connectionErrorAlert: ChatConnectionErrorAlert {
        .standard
    }
    
    func startConnectionLoading() {
        connectionStateDisplay?.startLoading(loadingText: String())
    }

    func stopConnectionLoading(completion: (() -> Void)? = nil) {
        connectionStateDisplay?.stopLoading(completion: completion)
    }

    func presentConnectionError() {
        connectionStateDisplay?.showConnectionError(alert: connectionErrorAlert)
    }
}

public protocol ConnectionStateDisplaying: AnyObject {
    var connectionStateInteractor: ConnectionStateInteracting { get }
    func startLoading(loadingText: String)
    func stopLoading(completion: (() -> Void)?)
    func didRequestRetryConnection()
    func didRequestCancelConnection()
    func showConnectionError(alert: ChatConnectionErrorAlert)
}

private extension ConnectionStateDisplaying where Self: ConnectionStateSceneStandardFlow {
    func primaryAlertButton(title: String) -> ApolloAlertAction {
        ApolloAlertAction(title: title, dismissOnCompletion: true) {
            self.connectionStateInteractor.logConnectionErrorSelectedTryAgainOption()
            self.didRequestRetryConnection()
        }
    }

    func secodaryAlertButton(title: String) -> ApolloAlertAction {
        ApolloAlertAction(title: title, dismissOnCompletion: true) {
            self.connectionStateInteractor.logConnectionErrorSelectedCancelOption()
            self.didRequestCancelConnection()
        }
    }
}

private extension UIViewController {
    func showChatConnectionError(alert: ChatConnectionErrorAlert,
                                 primaryAlertButton: ApolloAlertAction,
                                 secodaryAlertButton: ApolloAlertAction) {
        showApolloAlert(image: alert.image,
                        title: alert.title,
                        subtitle: alert.subtitle,
                        primaryButtonAction: primaryAlertButton,
                        linkButtonAction: secodaryAlertButton)
    }
}

public extension ConnectionStateDisplaying where Self: ConnectionStateSceneStandardFlow {
    func didRequestRetryConnection() {
        self.connectionStateInteractor.reconnect()
    }

    func didRequestCancelConnection() {
        // Not action is required
    }

    func showConnectionError(alert: ChatConnectionErrorAlert, in viewController: UIViewController) {
        viewController.showChatConnectionError(alert: alert,
                                               primaryAlertButton: primaryAlertButton(title: alert.options.retryTitle),
                                               secodaryAlertButton: secodaryAlertButton(title: alert.options.cancelTitle))
    }
}

public extension ConnectionStateDisplaying where Self: UIViewController & ConnectionStateSceneStandardFlow {
    func showConnectionError(alert: ChatConnectionErrorAlert) {
        showChatConnectionError(alert: alert,
                                primaryAlertButton: primaryAlertButton(title: alert.options.retryTitle),
                                secodaryAlertButton: secodaryAlertButton(title: alert.options.cancelTitle))
    }
}

public extension ConnectionStateDisplaying where Self: ApolloLoadable & ConnectionStateSceneStandardFlow {
    func startLoading(loadingText: String = String()) {
        startApolloLoader(loadingText: loadingText)
    }

    func stopLoading(completion: (() -> Void)? = nil) {
        stopApolloLoader(completion: completion)
    }
}
