import Foundation

protocol ChatConnectionConfiguring {
    var clientId: String { get }
    var username: String { get }
    var avatarUrl: String? { get }
}

struct ChatConnectionConfiguration: ChatConnectionConfiguring {
    typealias Dependencies = HasLegacy
    private let dependencies: Dependencies
    private var chatUser: ChatUser? {
        dependencies.legacy.chatUser
    }

    // MARK: - Initialization
    
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    // MARK: - ChatConnectionConfiguring
    
    var clientId: String {
        chatUser?.clientId ?? String()
    }

    var username: String {
        chatUser?.username ?? String()
    }
    
    var avatarUrl: String? {
        chatUser?.avatarUrl
    }
}
