import Core
import Foundation
import SendBirdSDK

public protocol HasChatApiManager {
    var chatApiManager: ChatApiManaging { get }
}

public protocol ChatApiManaging {
    // MARK: - Common Properties
    var clientId: String { get }
    
    // MARK: - Initial setup
    func setup(completion: @escaping (Result<Void, Error>) -> Void)
    
    // MARK: - Connection
    func connect(completion: @escaping (Result<Void, Error>) -> Void)
    func disconnect(completion: (() -> Void)?)
    
    // MARK: - Push
    func registerDeviceToken()
    func addPushDeviceToken(_ data: Data)
    func getPushNotificationOption(completion: @escaping (Result<PushNotificationOption, MessagingClientError>) -> Void)
    func setPushNotificationOption(_ option: PushNotificationOption, completion: @escaping (Result<Void, MessagingClientError>) -> Void)
    
    // MARK: - Fetches
    func fetchUnreadMessages(completion: @escaping (Int) -> Void)
    
    // MARK: - User Reachability
    func canReach(user: ChatUser, completion: @escaping (Result<Bool, MessagingClientError>) -> Void)
    func canReach(users: [ChatUser], completion: @escaping (Result<[ChatUser], MessagingClientError>) -> Void)
    
    // MARK: - ConnectionState
    func getCurrentConnectionState() -> ConnectionState
    func setActionForConnectionState(withTimeout timeout: TimeInterval, completion: @escaping ((ConnectionState) -> Void))
    func cancelActionForConnectionState()
}

public extension ChatApiManaging {
    func connect() {
        connect { _ in }
    }
    
    /// Set actions for connectionState with default timeout of 3 seconds
    func setActionForConnectionState(completion: @escaping ((ConnectionState) -> Void)) {
        setActionForConnectionState(withTimeout: 3.0, completion: completion)
    }
}
