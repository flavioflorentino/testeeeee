import Core
import Foundation
import SendBirdSDK

public final class ChatApiManager: NSObject, ChatApiManaging {
    public typealias Dependencies = HasMainQueue
    
    public static let shared = ChatApiManager()
    public private(set) lazy var connectionState: ConnectionState = client.connectionState
    private let configuration: ChatConnectionConfiguring
    private let client: MessagingClient
    private let dependencies: Dependencies
    private var lastConnectionTimer: Timer?
    private var connectionTimeOut: TimeInterval = 3.0
    private(set) var didFinishConnection: ((ConnectionState) -> Void)?
    
    init(client: MessagingClient = SendbirdClient(),
         configuration: ChatConnectionConfiguring = ChatConnectionConfiguration(),
         dependencies: Dependencies = DependencyContainer()) {
        self.configuration = configuration
        self.client = client
        self.dependencies = dependencies
    }
    
    public var clientId: String {
        configuration.clientId
    }

    public func setActionForConnectionState(withTimeout timeout: TimeInterval,
                                            completion: @escaping ((ConnectionState) -> Void)) {
        connectionTimeOut = timeout
        checkIfConnectionIsStillOpen()
        setActionOnlyIfConnectionIsNotOpen(completion: completion)
    }

    public func cancelActionForConnectionState() {
        self.didFinishConnection = nil
        self.lastConnectionTimer?.invalidate()
    }

    public func getCurrentConnectionState() -> ConnectionState {
        checkIfConnectionIsStillOpen()
        return connectionState
    }

    public func setup(completion: @escaping (Result<Void, Error>) -> Void) {
        client.addConnection(delegate: self)
        client.setup()
        connect(completion: completion)
    }
    
    public func connect(completion: @escaping (Result<Void, Error>) -> Void) {
        connectionState = .connecting
        startConnectionTimerIfNeeded()
        client.connect(withConfiguration: configuration) { [weak self] result in
            guard let self = self else { return }
            self.updateConnectionState(basedOn: result)
            completion(result)
        }
    }
    
    public func disconnect(completion: (() -> Void)?) {
        client.disconnect(completion: completion)
    }
    
    public func registerDeviceToken() {
        client.registerDeviceToken()
    }
    
    public func addPushDeviceToken(_ data: Data) {
        client.addPushDeviceToken(data)
    }
    
    public func getPushNotificationOption(completion: @escaping (Result<PushNotificationOption, MessagingClientError>) -> Void) {
        client.getPushNotificationOption(completion: completion)
    }
    
    public func setPushNotificationOption(_ option: PushNotificationOption, completion: @escaping (Result<Void, MessagingClientError>) -> Void) {
        client.setPushNotificationOption(option, completion: completion)
    }
    
    public func fetchUnreadMessages(completion: @escaping (Int) -> Void) {
        client.fetchUnreadMessages(completion: completion)
    }

    public func canReach(user: ChatUser, completion: @escaping (Result<Bool, MessagingClientError>) -> Void) {
        client.canReach(user: user, completion: completion)
    }

    public func canReach(users: [ChatUser], completion: @escaping (Result<[ChatUser], MessagingClientError>) -> Void) {
        client.canReach(users: users, completion: completion)
    }
}

// MARK: - ConnectionState Managing
private extension ChatApiManager {
    func updateConnectionState(basedOn result: Result<Void, Error>) {
        switch result {
        case .success:
            updateConnectionStateToCurrent()
        case .failure:
            updateConnectionStateToFailure()
        }
        didFinishConnection = nil
    }

    func updateConnectionStateToCurrent() {
        lastConnectionTimer?.invalidate()
        
        connectionState = client.connectionState
        didFinishConnection?(connectionState)
    }

    func updateConnectionStateToFailure() {
        lastConnectionTimer?.invalidate()

        guard connectionState != .failure else { return }
        connectionState = .failure
        didFinishConnection?(.failure)
    }
    
    func checkIfConnectionIsStillOpen() {
        guard connectionState == .open else { return }
        connectionState = client.connectionState
    }

    func setActionOnlyIfConnectionIsNotOpen(completion: @escaping ((ConnectionState) -> Void)) {
        if connectionState != .open {
            didFinishConnection = completion
        }
        if connectionState == .connecting {
            startConnectionTimerIfNeeded()
        }
        completion(connectionState)
    }
}

// MARK: - SendBird Delegates
extension ChatApiManager: SBDConnectionDelegate {
    public func didSucceedReconnection() {
        updateConnectionStateToCurrent()
    }

    public func didFailReconnection() {
        updateConnectionStateToFailure()
    }
}

private extension ChatApiManager {
    func startConnectionTimerIfNeeded() {
        guard didFinishConnection != nil else { return }

        lastConnectionTimer = Timer.scheduledTimer(
            withTimeInterval: connectionTimeOut,
            repeats: false
        ) { [weak self] _ in
            self?.updateConnectionStateToFailure()
        }
    }
}
