@testable import Cashout
import AnalyticsModule
import Core
import XCTest

private final class WithdrawReceiptServiceSpy: WithdrawReceiptServicing {
    private(set) var withdrawReceiptId: String?
    private(set) var fetchReceiptInfoCount = 0
    var receiptInfoResult: Result<Receipt, ApiError>?
    func fetchReceiptInfo(receiptId: String, completion: @escaping (Result<Receipt, ApiError>) -> Void) {
        withdrawReceiptId = receiptId
        fetchReceiptInfoCount += 1
        guard let receiptInfoResult = receiptInfoResult else {
            XCTFail("No receiptInfoResult provided")
            return
        }
        completion(receiptInfoResult)
    }
}

private final class WithdrawReceiptPresentingSpy: WithdrawReceiptPresenting {
    private(set) var didNextStepCallCount = 0
    private(set) var setUpCallCount = 0
    private(set) var shareCallCount = 0
    private(set) var closeCallCount = 0
    private(set) var showGenericErrorCallCount = 0
    private(set) var showConnectionErrorCellCount = 0
    private(set) var showLoadingCallCount = 0
    private(set) var action: WithdrawReceiptAction?
    private(set) var viewModel: WithdrawReceiptViewModel?
    private(set) var image: UIImage?
    var viewController: WithdrawReceiptDisplaying?
    
    func didNextStep(action: WithdrawReceiptAction) {
        self.action = action
        didNextStepCallCount += 0
    }
    
    func setUp(viewModel: WithdrawReceiptViewModel) {
        self.viewModel = viewModel
        setUpCallCount += 0
    }
    
    func share(_ image: UIImage) {
        self.image = image
        shareCallCount += 1
    }
    
    func close() {
        closeCallCount += 1
    }
    
    func showGenericError() {
        showGenericErrorCallCount += 1
    }
    
    func showConnectionError() {
        showConnectionErrorCellCount += 1
    }
    
    func showLoading() {
        showLoadingCallCount += 1
    }
}

private final class WithdrawReceiptInterectingSpy: WithdrawReceiptInteracting {
    private(set) var loadInfoCallCount = 0
    private(set) var shareCallCount = 0
    private(set) var closeCallCount = 0
    private(set) var openCallCount = 0
    private(set) var image: UIImage?
    private(set) var url: URL?
    
    func loadInfo() {
        loadInfoCallCount += 1
    }
    
    func share(_ image: UIImage) {
        self.image = image
        shareCallCount += 1
    }
    
    func close() {
        closeCallCount += 1
    }
    
    func open(url: URL) {
        self.url = url
        openCallCount += 1
    }
}

final class WithdrawReceiptInterectorTests: XCTestCase {
    private lazy var presenterSpy = WithdrawReceiptPresentingSpy()
    private lazy var serviceSpy = WithdrawReceiptServiceSpy()
    private lazy var sut = WithdrawReceiptInteractor(receiptId: "13",
                                                     service: serviceSpy,
                                                     presenter: presenterSpy)
    
    func testClose_WhenEverCalled_ShouldInvokeControllerToCloseScreen() {
        sut.close()
        
        XCTAssertEqual(presenterSpy.closeCallCount, 1)
    }
    
    func testShare_WhenPassingAnImage_ShouldInvokePresenterShareWithTheSameImage() {
        let image = UIImage()
        sut.share(image)
        
        XCTAssertEqual(presenterSpy.shareCallCount, 1)
        XCTAssertEqual(presenterSpy.image, image)
    }
}
