@testable import Cashout
import UI
import XCTest

private final class WithdrawReceiptCoordinatingSpy: WithdrawReceiptCoordinating {
    private(set) var performCallCount = 0
    private(set) var action: WithdrawReceiptAction?
    var viewController: UIViewController?
    
    func perform(action: WithdrawReceiptAction) {
        self.action = action
        performCallCount += 1
    }
}

private final class WithdrawReceiptDisplaySpy: WithdrawReceiptDisplaying {
    private(set) var displayCallCount = 0
    private(set) var state: WithdrawReceiptState?
    
    func display(state: WithdrawReceiptState) {
        self.state = state
        displayCallCount += 1
    }
}

class WithdrawReceiptPresenterTests: XCTestCase {
    private let coordinatorSpy = WithdrawReceiptCoordinatingSpy()
    private let viewControllerSpy = WithdrawReceiptDisplaySpy()
    
    private lazy var sut: WithdrawReceiptPresenting = {
        let presenter = WithdrawReceiptPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testDidNextStep_whenEverCalled_ShouldMakeTheScreenTransaction() {
        sut.didNextStep(action: .close)
        
        XCTAssertEqual(coordinatorSpy.performCallCount, 1)
    }
    
    func testSetup_whenEverCalled_ShouldShowTheCorrectsDatas() {
        let receiptHeader = WithdrawReceiptHeaderViewModel(depositId: "", depositDate: "05-SET-2020")
        let info = [AccountInfo(label: "", value: ""), AccountInfo(label: "", value: "")]
        let accounts = Accounts(origin: info, destination: info)
        let viewModel = WithdrawReceiptViewModel(header: receiptHeader, value: "", info: accounts)
        sut.setUp(viewModel: viewModel)
        
        XCTAssertEqual(viewControllerSpy.displayCallCount, 1)
        XCTAssertEqual(viewControllerSpy.state, .loaded(viewModel: viewModel))
    }
    
    func testShowGenericError_shouldCallDisplayWithError() {
        sut.showGenericError()
        
        XCTAssertEqual(viewControllerSpy.displayCallCount, 1)
    }
    
    func testShowConnectionError_shouldCallDisplayWithError() {
        sut.showConnectionError()
        
        XCTAssertEqual(viewControllerSpy.displayCallCount, 1)
    }
    
    func testShowLoading_shouldCallDisplayWithLoading() {
        sut.showLoading()
        
        XCTAssertEqual(viewControllerSpy.displayCallCount, 1)
        XCTAssertEqual(viewControllerSpy.state, .loading)
    }
    
    func testShare_whenEverCalled_shouldInvokeCoordinatorShareWithImage() {
        let image = UIImage()
        sut.share(image)
        
        XCTAssertEqual(coordinatorSpy.performCallCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .share(image: image))
    }
    
    func testClose_whenEverCalled_shouldInvokeCloseInViewController() {
        sut.close()
        
        XCTAssertEqual(coordinatorSpy.performCallCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .close)
    }
}
