import UIKit

enum BankAccountsFactory {
    static func make() -> BankAccountsViewController {
        let container = DependencyContainer()
        let service: BankAccountsServicing = BankAccountsService(dependencies: container)
        let coordinator: BankAccountsCoordinating = BankAccountsCoordinator()
        let presenter: BankAccountsPresenting = BankAccountsPresenter(coordinator: coordinator)
        let interactor = BankAccountsInteractor(service: service, presenter: presenter)
        let viewController = BankAccountsViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
