import Foundation

protocol BankAccountsPresenting: AnyObject {
    var viewController: BankAccountsDisplaying? { get set }
    func didNextStep(action: BankAccountsAction)
}

final class BankAccountsPresenter {
    private let coordinator: BankAccountsCoordinating
    weak var viewController: BankAccountsDisplaying?

    init(coordinator: BankAccountsCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - BankAccountsPresenting
extension BankAccountsPresenter: BankAccountsPresenting {
    func didNextStep(action: BankAccountsAction) {
        coordinator.perform(action: action)
    }
}
