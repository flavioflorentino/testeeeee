import Foundation

protocol BankAccountsInteracting: AnyObject {}

final class BankAccountsInteractor {
    private let service: BankAccountsServicing
    private let presenter: BankAccountsPresenting

    init(service: BankAccountsServicing, presenter: BankAccountsPresenting) {
        self.service = service
        self.presenter = presenter
    }
}

// MARK: - BankAccountsInteracting
extension BankAccountsInteractor: BankAccountsInteracting {
}
