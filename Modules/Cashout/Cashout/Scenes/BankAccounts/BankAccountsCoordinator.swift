import UIKit

enum BankAccountsAction {
}

protocol BankAccountsCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: BankAccountsAction)
}

final class BankAccountsCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - BankAccountsCoordinating
extension BankAccountsCoordinator: BankAccountsCoordinating {
    func perform(action: BankAccountsAction) {
    }
}
