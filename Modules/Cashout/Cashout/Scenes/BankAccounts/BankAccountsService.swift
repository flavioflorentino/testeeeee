import Core
import Foundation

protocol BankAccountsServicing {
}

final class BankAccountsService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - BankAccountsServicing
extension BankAccountsService: BankAccountsServicing {
}
