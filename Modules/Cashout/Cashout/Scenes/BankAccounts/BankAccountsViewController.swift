import UI
import UIKit

protocol BankAccountsDisplaying: AnyObject {
}

private extension BankAccountsViewController.Layout {
}

final class BankAccountsViewController: ViewController<BankAccountsInteracting, UIView> {
    fileprivate enum Layout { }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func buildViewHierarchy() { }
    
    override func setupConstraints() { }

    override func configureViews() { }
}

// MARK: - BankAccountsDisplaying
extension BankAccountsViewController: BankAccountsDisplaying {
    
}
