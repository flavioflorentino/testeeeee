import Core
import Foundation

protocol WithdrawReceiptInteracting: AnyObject {
    func loadInfo()
    func share(_ image: UIImage)
    func close()
    func open(url: URL)
}

final class WithdrawReceiptInteractor {
    private let service: WithdrawReceiptServicing
    private let presenter: WithdrawReceiptPresenting

    private let receiptId: String

    init(receiptId: String, service: WithdrawReceiptServicing, presenter: WithdrawReceiptPresenting) {
        self.service = service
        self.presenter = presenter
        self.receiptId = receiptId
    }
}

// MARK: - WithdrawReceiptInteracting
extension WithdrawReceiptInteractor: WithdrawReceiptInteracting {
    func loadInfo() {
        presenter.showLoading()
        service.fetchReceiptInfo(receiptId: receiptId) { [weak self] result in
            switch result {
            case let .success(receipt):
                guard let viewModel = self?.map(receipt) else { return }
                self?.presenter.setUp(viewModel: viewModel)
            case let .failure(error):
                self?.handleError(error: error)
            }
        }
    }

    func close() {
        presenter.close()
    }
    
    func share(_ image: UIImage) {
        presenter.share(image)
    }

    func open(url: URL) {
        presenter.didNextStep(action: .open(url: url))
    }
}

private extension WithdrawReceiptInteractor {
    func map(_ receipt: Receipt) -> WithdrawReceiptViewModel {
        let header = WithdrawReceiptHeaderViewModel(depositId: String(receipt.id), depositDate: receipt.completedAt)
        return WithdrawReceiptViewModel(header: header,
                                        value: receipt.value,
                                        info: receipt.accounts)
    }

    func handleError(error: ApiError) {
        if case .connectionFailure = error {
            presenter.showConnectionError()
        } else {
            presenter.showGenericError()
        }
    }
}
