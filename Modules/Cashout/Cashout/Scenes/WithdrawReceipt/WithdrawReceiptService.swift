import Foundation
import Core

protocol WithdrawReceiptServicing {
    func fetchReceiptInfo(receiptId: String, completion: @escaping (Result<Receipt, ApiError>) -> Void)
}

final class WithdrawReceiptService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - WithdrawReceiptServicing
extension WithdrawReceiptService: WithdrawReceiptServicing {
    func fetchReceiptInfo(receiptId: String, completion: @escaping (Result<Receipt, ApiError>) -> Void) {
        let endpoint = WireTransferReceiptEndpoint.getWireTransferReceipt(receiptId: receiptId)
        let api = Api<Receipt>(endpoint: endpoint)
        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] response in
            self?.dependencies.mainQueue.async {
                completion(response.map(\.model))
            }
        }
    }
}

enum WireTransferReceiptEndpoint {
    case getWireTransferReceipt(receiptId: String)
}

extension WireTransferReceiptEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case let .getWireTransferReceipt(receiptId):
            return "cashout/banktransfer/\(receiptId)/receipt"
        }
    }
}
