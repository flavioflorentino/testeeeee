import UIKit

public enum WithdrawReceiptFactory {
    public static func make(receiptId: String) -> UIViewController {
        let container = DependencyContainer()
        let service: WithdrawReceiptServicing = WithdrawReceiptService(dependencies: container)
        let coordinator: WithdrawReceiptCoordinating = WithdrawReceiptCoordinator()
        let presenter: WithdrawReceiptPresenting = WithdrawReceiptPresenter(coordinator: coordinator)
        let interactor = WithdrawReceiptInteractor(receiptId: receiptId, service: service, presenter: presenter)
        let viewController = WithdrawReceiptViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
