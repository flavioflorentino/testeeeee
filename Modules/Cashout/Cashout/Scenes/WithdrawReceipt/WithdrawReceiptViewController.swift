import AssetsKit
import Core
import SnapKit
import UI
import UIKit

enum WithdrawReceiptState: Equatable {
    case loading
    case error(viewModel: StatefulErrorViewModel)
    case loaded(viewModel: WithdrawReceiptViewModel)
    
    static func == (lhs: WithdrawReceiptState, rhs: WithdrawReceiptState) -> Bool {
        switch (lhs, rhs) {
        case (.loading, .loading):
            return true
        case let (.error(lhs), .error(rhs)):
            return lhs.button?.image == rhs.button?.image &&
                lhs.button?.title == rhs.button?.title &&
                lhs.content?.title == rhs.content?.title &&
                lhs.content?.description == rhs.content?.description
        case let (.loaded(lhs), .loaded(rhs)):
            return lhs == rhs
        default:
            return false
        }
    }
}

protocol WithdrawReceiptDisplaying: AnyObject {
    func display(state: WithdrawReceiptState)
}

final class WithdrawReceiptViewController: ViewController<WithdrawReceiptInteracting, WithdrawReceiptView> {
    private lazy var optionsBarButton: UIBarButtonItem = {
        let shareImage = Resources.Icons.icoShare.image
        let barButtonItem = UIBarButtonItem(image: shareImage,
                                            style: .plain,
                                            target: self,
                                            action: #selector(shareScreenSnapshot))
        return barButtonItem
    }()
    
    private lazy var closeBarButtonItem: UIBarButtonItem = {
        let barButtonItem = UIBarButtonItem(title: Strings.btClose,
                                            style: .plain,
                                            target: self,
                                            action: #selector(didTapClose))
        barButtonItem.tintColor = Colors.branding700.color
        return barButtonItem
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.loadInfo()
        setUpNavigation()
    }
    
    override func configureViews() {
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        rootView.buildViewHierarchy()
        rootView.setupConstraints()
        rootView.configureViews(textViewDelegate: self)
        rootView.configureStyles()
    }
    
    private func setUpNavigation() {
        setUpNavigationBarDefaultAppearance()
        navigationItem.title = Strings.receiptNavigationTitle
        navigationItem.leftBarButtonItem = closeBarButtonItem
    }
    
    private func setUpShareButton() {
        navigationItem.rightBarButtonItem = optionsBarButton
    }
    
    @objc
    private func didTapClose() {
        interactor.close()
    }
    
    @objc
    func shareScreenSnapshot() {
        guard let snapshot = rootView.snapshotAll() else { return }
        if #available(iOS 13.0, *) {
            let currentStyle = traitCollection.userInterfaceStyle
            overrideUserInterfaceStyle = .light
            rootView.updateLogo()
            interactor.share(snapshot)
            overrideUserInterfaceStyle = currentStyle
            rootView.updateLogo()
        } else {
            interactor.share(snapshot)
        }
    }
    
    private func showLoadedView(with viewModel: WithdrawReceiptViewModel) {
        endState {
            self.setUpShareButton()
            self.rootView.setUp(with: viewModel)
        }
    }
}

// MARK: - WithdrawReceiptDisplaying
extension WithdrawReceiptViewController: WithdrawReceiptDisplaying {
    func display(state: WithdrawReceiptState) {
        switch state {
        case .loading:
            beginState()
        case .error(let error):
            endState(model: error)
        case .loaded(let viewModel):
            showLoadedView(with: viewModel)
        }
    }
}

extension WithdrawReceiptViewController: StatefulTransitionViewing {
    func didTryAgain() {
        interactor.loadInfo()
    }
    
    func statefulViewForLoading() -> StatefulViewing {
        WithdrawReceiptLoadingStateView()
    }
}

extension WithdrawReceiptViewController: UITextViewDelegate {
    func textView(
        _ textView: UITextView,
        shouldInteractWith URL: URL,
        in characterRange: NSRange,
        interaction: UITextItemInteraction
    ) -> Bool {
        interactor.open(url: URL)
        return false
    }
}
