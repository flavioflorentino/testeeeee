import AssetsKit
import Foundation
import UI

protocol WithdrawReceiptPresenting: AnyObject {
    var viewController: WithdrawReceiptDisplaying? { get set }
    func didNextStep(action: WithdrawReceiptAction)
    
    func setUp(viewModel: WithdrawReceiptViewModel)
    func share(_ image: UIImage)
    func close()
    func showGenericError()
    func showConnectionError()
    func showLoading()
}

final class WithdrawReceiptPresenter {
    private let coordinator: WithdrawReceiptCoordinating
    weak var viewController: WithdrawReceiptDisplaying?

    init(coordinator: WithdrawReceiptCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - WithdrawReceiptPresenting
extension WithdrawReceiptPresenter: WithdrawReceiptPresenting {
    func didNextStep(action: WithdrawReceiptAction) {
        coordinator.perform(action: action)
    }
    
    func setUp(viewModel: WithdrawReceiptViewModel) {
        viewController?.display(state: .loaded(viewModel: viewModel))
    }
    
    func showGenericError() {
        let viewModel = StatefulErrorViewModel(
            image: Resources.Illustrations.iluConstruction.image,
            content: (title: Strings.genericErrorTitle,
                      description: Strings.genericErrorDescription),
            button: (image: nil, title: Strings.buttonErrorTitle)
        )
        viewController?.display(state: .error(viewModel: viewModel))
    }
    
    func showConnectionError() {
        let viewModel = StatefulErrorViewModel(
            image: Resources.Illustrations.iluNoConnectionTransparent.image,
            content: (title: Strings.noConnectionTitle,
                      description: Strings.noConnectionDescription),
            button: (image: nil, title: Strings.buttonErrorTitle)
        )
        viewController?.display(state: .error(viewModel: viewModel))
    }
    
    func showLoading() {
        viewController?.display(state: .loading)
    }
    
    func share(_ image: UIImage) {
        coordinator.perform(action: .share(image: image))
    }
    
    func close() {
        coordinator.perform(action: .close)
    }
}
