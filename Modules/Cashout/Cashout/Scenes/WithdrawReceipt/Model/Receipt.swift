import Foundation

struct Receipt: Decodable {
    let id: Int
    let value: String
    let completedAt: String
    let accounts: Accounts
}

struct AccountInfo: Decodable, Equatable {
    let label: String
    let value: String
}

struct Accounts: Decodable, Equatable {
   let origin: [AccountInfo]
   let destination: [AccountInfo]
    
    enum CodingKeys: String, CodingKey {
        case origin = "source"
        case destination = "target"
    }
}
