import Foundation

struct WithdrawReceiptViewModel: Equatable {
    let header: WithdrawReceiptHeaderViewModel
    let value: String
    let info: Accounts
}

struct WithdrawReceiptHeaderViewModel: Equatable {
    let depositId: String
    let depositDate: String
}
