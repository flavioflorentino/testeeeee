import UIKit
import SafariServices

enum WithdrawReceiptAction: Equatable {
    case close
    case share(image: UIImage)
    case open(url: URL)
}

protocol WithdrawReceiptCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: WithdrawReceiptAction)
}

final class WithdrawReceiptCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - WithdrawReceiptCoordinating
extension WithdrawReceiptCoordinator: WithdrawReceiptCoordinating {
    func perform(action: WithdrawReceiptAction) {
        switch action {
        case .close:
            viewController?.dismiss(animated: true)
        case let .share(image):
            let controller = UIActivityViewController(activityItems: [image], applicationActivities: nil)
            viewController?.present(controller, animated: true)
        case let .open(url):
            let safariViewController = SFSafariViewController(url: url)
            viewController?.navigationController?.present(safariViewController, animated: true)
        }
    }
}
