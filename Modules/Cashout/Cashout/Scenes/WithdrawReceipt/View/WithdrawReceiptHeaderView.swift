import AssetsKit
import SnapKit
import UI
import UIKit

final class WithdrawReceiptHeaderView: UIView, ViewConfiguration {
    private typealias Texts = Strings
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.grayscale700.color)
        return label
    }()
    
    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .default))
            .with(\.textColor, Colors.grayscale700.color)
        return label
    }()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Resources.Icons.icoTransferReceipt.image
        return imageView
    }()
    
    init() {
        super.init(frame: .zero)
        buildLayout()
    }

    @available(*, unavailable)
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubviews(titleLabel,
                    infoLabel,
                    imageView)
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.trailing.lessThanOrEqualToSuperview().offset(-Spacing.base02)
        }
        
        imageView.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.top)
            $0.leading.equalToSuperview()
            $0.bottom.lessThanOrEqualToSuperview().offset(-Spacing.base01)
            $0.trailing.equalTo(titleLabel.snp.leading).offset(-Spacing.base02)
        }
        
        infoLabel.snp.makeConstraints {
            $0.leading.trailing.equalTo(titleLabel)
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.bottom.equalToSuperview()
        }
    }
    
    func setUp(with viewModel: WithdrawReceiptHeaderViewModel) {
        titleLabel.text = Texts.withdrawHeaderTitle
        infoLabel.text = Texts.withdrawHeaderInfo(viewModel.depositId, viewModel.depositDate)
    }
}
