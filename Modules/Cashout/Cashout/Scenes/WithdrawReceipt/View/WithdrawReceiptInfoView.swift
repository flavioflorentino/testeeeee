import SnapKit
import UI
import UIKit

final class WithdrawReceiptInfoView: UIStackView {
    private typealias Texts = Strings
    
    private lazy var originSectionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.grayscale700.color)
        label.text = Texts.originTitle
        return label
    }()
    
    private lazy var destinationSectionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.grayscale700.color)
        label.text = Texts.destinyTitle
        return label
    }()
    
    private func buildStack(sectionHeader: UILabel, infoArray: [AccountInfo]) {
        addArrangedSubview(sectionHeader)
        for info in infoArray {
            let component = WithdrawReceiptInfoComponent(title: info.label, info: info.value)
            addArrangedSubview(component)
        }
    }
    
    func setUp(with info: Accounts) {
        axis = .vertical
        spacing = Spacing.base02
        removeAllSubviews()
        buildStack(sectionHeader: originSectionLabel, infoArray: info.origin)
        buildStack(sectionHeader: destinationSectionLabel, infoArray: info.destination)
    }
}
