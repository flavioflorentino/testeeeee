import SnapKit
import UI
import UIKit

final class WithdrawReceiptValueView: UIView, ViewConfiguration {
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.grayscale700.color)
        label.text = Strings.withdrawValue
        return label
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.grayscale700.color)
        return label
    }()
    
    init() {
        super.init(frame: .zero)
        buildLayout()
    }

    @available(*, unavailable)
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubviews(titleLabel,
                    valueLabel)
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.equalToSuperview()
        }
        
        valueLabel.snp.makeConstraints {
            $0.leading.greaterThanOrEqualTo(titleLabel.snp.trailing).offset(Spacing.base01)
            $0.top.bottom.equalToSuperview()
            $0.trailing.equalToSuperview()
        }
    }
    
    func setUp(value: String) {
        valueLabel.text = value
    }
}
