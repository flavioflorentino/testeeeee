import SnapKit
import UI
import UIKit

final class WithdrawReceiptContactComponent: UIView, ViewConfiguration {
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.grayscale600.color)
        return label
    }()
    
    private lazy var phoneLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .default))
            .with(\.textColor, Colors.grayscale600.color)
        return label
    }()
    
    init() {
        super.init(frame: .zero)
        buildLayout()
    }

    @available(*, unavailable)
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubviews(nameLabel,
                    phoneLabel)
    }
    
    func setupConstraints() {
        nameLabel.snp.makeConstraints {
            $0.top.leading.bottom.equalToSuperview()
        }
        
        phoneLabel.snp.makeConstraints {
            $0.leading.equalTo(nameLabel.snp.trailing).offset(Spacing.base01)
            $0.top.trailing.bottom.equalToSuperview()
        }
    }
    
    func setUp(name: String, phone: String) {
        nameLabel.text = name
        phoneLabel.text = phone
    }
}
