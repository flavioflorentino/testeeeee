import UI
import UIKit

final class WithdrawReceiptLoadingStateView: UIView, ViewConfiguration, StatefulViewing {
    private typealias Texts = Strings
    var viewModel: StatefulViewModeling?
    weak var delegate: StatefulDelegate?
    
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.tintColor = Colors.grayscale700.color
        return indicator
    }()
    
    private lazy var activityLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.textColor, .grayscale500())
        label.text = Texts.loading
        return label
    }()
    
    init() {
        super.init(frame: .zero)
        buildLayout()
        
        activityIndicator.startAnimating()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubviews(activityIndicator, activityLabel)
    }
    
    func setupConstraints() {
        activityIndicator.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
        
        activityLabel.snp.makeConstraints {
            $0.centerX.equalTo(activityIndicator)
            $0.bottom.equalTo(activityIndicator.snp.top).offset(-Spacing.base01)
        }
    }
}
