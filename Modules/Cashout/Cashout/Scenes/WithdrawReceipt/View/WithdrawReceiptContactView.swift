import AssetsKit
import SnapKit
import UI
import UIKit

final class WithdrawReceiptContactView: UIStackView {
    private typealias Texts = Strings
    
    private lazy var helpCenterTextView: UITextView = {
        let text = UITextView()
        text.textColor = Colors.grayscale700.color
        text.isEditable = false
        text.isSelectable = true
        text.isScrollEnabled = false
        text.backgroundColor = .clear
        text.textContainerInset = .zero
        text.textContainer.lineFragmentPadding = 0
        return text
    }()
    
    private lazy var sacLabel: WithdrawReceiptContactComponent = {
        let component = WithdrawReceiptContactComponent()
        component.setUp(name: Texts.contactTitle, phone: Texts.contactPhone)
        return component
    }()
    
    private lazy var ouvidoriaLabel: WithdrawReceiptContactComponent = {
        let component = WithdrawReceiptContactComponent()
        component.setUp(name: Texts.ombudsmanTitle, phone: Texts.ombudsmanPhone)
        return component
    }()
    
    private lazy var helpSacStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [helpCenterTextView, sacLabel])
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.alignment = .center
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var ouvidoriaLogoStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [ouvidoriaLabel, logoImageView])
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.alignment = .center
        stackView.spacing = Spacing.base04
        return stackView
    }()
    
    private lazy var logoCnpjStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [ouvidoriaLogoStack, cnpjLabel])
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.alignment = .center
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var logoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var cnpjLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .default))
            .with(\.textColor, Colors.grayscale400.color)
        label.text = Texts.cnpjLabel
        return label
    }()
    
    private var logoImage: UIImage {
        Resources.Logos.consumerLogoWhite.image.withRenderingMode(.alwaysTemplate)
    }
    
    init() {
        super.init(frame: .zero)
        setUp()
        setUpConstraint()
    }

    @available(*, unavailable)
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setUp() {
        axis = .vertical
        alignment = .center
        spacing = Spacing.base01
        addArrangedSubview(helpSacStack)
        addArrangedSubview(logoCnpjStack)
        updateLogo()
        setupLinks()
    }
    
    private func setUpConstraint() {
        logoImageView.snp.makeConstraints {
            $0.width.equalTo(Sizing.base07)
            $0.height.equalTo(Spacing.base03)
        }
        
        helpSacStack.snp.makeConstraints {
            $0.centerX.equalToSuperview()
        }
        
        logoCnpjStack.snp.makeConstraints {
            $0.centerX.equalToSuperview()
        }
    }
    
    func configureViews(textViewDelegate: UITextViewDelegate) {
        helpCenterTextView.delegate = textViewDelegate
    }
    
    func updateLogo() {
        logoImageView.tintColor = Colors.grayscale400.color
        if #available(iOS 12.0, *), traitCollection.userInterfaceStyle == .dark {
            logoImageView.tintColor = Colors.black.color
        }
        logoImageView.image = logoImage
    }
}

extension WithdrawReceiptContactView {
    func setupLinks() {
        let attributedText = AttributedTextURLHelper.createAttributedTextWithLinks(
            text: Texts.titleHelperDescription,
            attributedTextURLDelegate: self,
            linkTypes: [Texts.helperText]
        )
        setHelpCenterTextView(linkText: attributedText, linkAttributes: AttributedTextURLHelper.linkAttributes)
    }
    
    func setHelpCenterTextView(
        linkText: NSAttributedString,
        linkAttributes: [NSAttributedString.Key: Any]
    ) {
        helpCenterTextView.attributedText = linkText
        helpCenterTextView.linkTextAttributes = linkAttributes
        helpCenterTextView.font = Typography.bodySecondary().font()
    }
}

extension WithdrawReceiptContactView: AttributedTextURLDelegate {
    func getUrlInfo(linkType: String) -> AttributedTextURLInfo {
        AttributedTextURLInfo(
            urlAddress: Texts.helperURL,
            text: Texts.helperText
        )
    }
}
