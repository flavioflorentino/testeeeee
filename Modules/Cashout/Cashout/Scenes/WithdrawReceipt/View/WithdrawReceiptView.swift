import SnapKit
import UI
import UIKit

final class WithdrawReceiptView: UIView, ViewConfiguration {
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.isHidden = true
        return scrollView
    }()
    
    private lazy var containerView = UIView()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var headerView = WithdrawReceiptHeaderView()
    private lazy var valueView = WithdrawReceiptValueView()
    private lazy var infoView = WithdrawReceiptInfoView()
    private lazy var footerView = WithdrawReceiptContactView()
    
    func buildViewHierarchy() {
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(stackView)
        stackView.addArrangedSubview(headerView)
        stackView.addArrangedSubview(valueView)
        stackView.addArrangedSubview(infoView)
        stackView.addArrangedSubview(footerView)
    }
    
    func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.width.equalToSuperview()
        }
        
        containerView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.height.equalToSuperview().priority(.low)
            $0.width.equalToSuperview()
            $0.centerX.equalToSuperview()
        }
        
        stackView.snp.makeConstraints {
            $0.leading.top.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().inset(Spacing.base04)
            $0.width.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureViews(textViewDelegate: UITextViewDelegate) {
        footerView.configureViews(textViewDelegate: textViewDelegate)
        containerView.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        stackView.insertLine(after: headerView, color: Colors.grayscale100.color)
        stackView.insertLine(after: valueView, color: Colors.grayscale200.color)
        stackView.insertLine(after: infoView, color: Colors.grayscale100.color)
    }
    
    func setUp(with viewModel: WithdrawReceiptViewModel) {
        scrollView.isHidden = false
        headerView.setUp(with: viewModel.header)
        valueView.setUp(value: viewModel.value)
        infoView.setUp(with: viewModel.info)
    }
    
    func updateLogo() {
        footerView.updateLogo()
    }
    
    func snapshotAll() -> UIImage? {
        containerView.snapshot
    }
}
