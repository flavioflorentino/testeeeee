import Foundation

// swiftlint:disable convenience_type
final class CashoutResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: CashoutResources.self).url(forResource: "CashoutResources", withExtension: "bundle") else {
            return Bundle(for: CashoutResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: CashoutResources.self)
    }()
}
