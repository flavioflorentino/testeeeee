import Core
import Foundation

typealias CashoutDependencies = HasMainQueue

final class DependencyContainer: CashoutDependencies {
    lazy var mainQueue = DispatchQueue.main
}
