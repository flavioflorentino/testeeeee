import Billet
import Core
import UIKit

final class HomeViewController: UIViewController {
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.rowHeight = 80
        tableView.dataSource = self
        tableView.delegate = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        return tableView
    }()
    
    private let features = ["Selection", "Transaction", "FollowUp", "Hub", "Scanner", "Form"]

    init() {
        super.init(nibName: nil, bundle: nil)
        
        title = "Features"
        
        view.addSubview(tableView)
        
        let constraints = [
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
        
        view.backgroundColor = .white
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        features.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = features[indexPath.row]
        cell.textLabel?.textColor = .systemGreen
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
        case 0:
            openSelection()
        case 1:
            openTransaction()
        case 2:
            openFollowUp()
        case 3:
            openHub()
        case 4:
            openScanner()
        case 5:
            openForm()
        case 4:
            let viewController = FollowUpFactory.make(withBillId: "1001356")
            navigationController?.pushViewController(viewController, animated: true)
        default:
            break
        }
    }
}

private extension HomeViewController {
    func openSelection() {
        let billets = [
            BilletSelectionListModel(linecode: "816200000007832947842015903194784091310761100789",
                                     description: "Cota única",
                                     dueDate: "2019-03-19",
                                     amount: 166.58),
            BilletSelectionListModel(linecode: "816200000007832947842015903194784091310761100789",
                                     description: "Cota 1",
                                     dueDate: "2019-03-19",
                                     amount: 83.29),
            BilletSelectionListModel(linecode: "816200000007832947842015903194784091310761100789",
                                     description: "Cota 2",
                                     dueDate: "2019-03-19",
                                     amount: 83.29)
        ]
        
        let billetSelection = BilletSelectionModel(title: "IPTU", description: nil, billets: billets)
        let viewController = BilletSelectionFactory.make(model: billetSelection)
        let navigation = UINavigationController(rootViewController: viewController)
        present(navigation, animated: true)
    }
    
    func openTransaction() {
        let viewController = BilletFeedDetailFactory.make(transactionId: "846293", externalId: "bill-10200")
        navigationController?.pushViewController(viewController, animated: true)
    }
  
    func openFollowUp() {
        let viewController = FollowUpFactory.make(withBillId: "1001356")
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func openHub() {
        let viewController = BilletHubFactory.make(with: .unknown)
        let navigation = UINavigationController(rootViewController: viewController)
        present(navigation, animated: true)
    }
    
    func openScanner() {
        let viewController = BilletScannerFactory.make(with: .unknown)
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func openForm() {
        let viewController = BilletFormFactory.make(with: .unknown, linecode: "34191090163019294067103922160001383720000216122")
        navigationController?.pushViewController(viewController, animated: true)
    }
}
