import UIKit

struct GestureAutomator {
    static func tap(view: UIView) {
        view.gestureRecognizers?
            .compactMap {
                $0 as? UITapGestureRecognizer
            }
            .forEach {
                $0.sendActions()
            }
    }
}

fileprivate extension UIGestureRecognizer {
    typealias TargetActionInfo = [(target: AnyObject, action: Selector)]
    
    func getTargetInfo() -> TargetActionInfo {
        guard let targets = value(forKeyPath: "_targets") as? [NSObject] else {
            return []
        }
        
        var targetsInfo: TargetActionInfo = []
        
        targets.forEach { target in
            let characterSet = CharacterSet(charactersIn: "()")
            let description = String(describing: target).trimmingCharacters(in: characterSet)
            var selectorString = description.components(separatedBy: ", ").first ?? ""
            selectorString = selectorString.components(separatedBy: "=").last ?? ""
            let selector = NSSelectorFromString(selectorString)

            if let targetActionPairClass = NSClassFromString("UIGestureRecognizerTarget"),
                let targetIvar = class_getInstanceVariable(targetActionPairClass, "_target"),
                let targetObject = object_getIvar(target, targetIvar) {
                targetsInfo.append((target: targetObject as AnyObject, action: selector))
            }
        }

        return targetsInfo
    }

    func sendActions() {
        let targetsInfo = getTargetInfo()
    
        targetsInfo.forEach { info in
            info.target.performSelector(onMainThread: info.action, with: self, waitUntilDone: true)
        }
    }
}
