@testable import Billet
import Core
import UI
import XCTest

final class FollowUpCoordinatorTests: XCTestCase {
    private let dependenciesMock = DependencyContainerMock()
    private let viewControllerSpy = ViewControllerMock()
    private lazy var navControllerSpy = NavigationControllerMock(rootViewController: viewControllerSpy)
    private lazy var sut: FollowUpCoordinating = {
        let coordinator = FollowUpCoordinator(dependencies: dependenciesMock)
        coordinator.viewController = navControllerSpy.topViewController
        return coordinator
    }()
    
    func testPerform_WhenActionIsReceipt_ShouldOpenTheReceiptScene() {
        sut.perform(action: .receipt(id: "1234", isScheduling: false))
        
        XCTAssertEqual(viewControllerSpy.didPresentControllerCount, 1)
    }
    
    func testPerform_WhenActionIsReceiptAndIsAScheduling_ShouldOpenTheReceiptScene() {
        sut.perform(action: .receipt(id: "1234", isScheduling: true))
        
        XCTAssertEqual(viewControllerSpy.didPresentControllerCount, 1)
    }
    
    func testPerform_WhenActionIsAccelerateAnalysis_ShouldOpenADeeplink() throws {
        let url = "https://app.picpay.com"
        sut.perform(action: .helpCenter(url: url))
        
        let deeplinkSpy = try XCTUnwrap(dependenciesMock.legacy.deeplink as? DeeplinkSpy)
        XCTAssertEqual(deeplinkSpy.callOpenCount, 1)
    }
    
    func testPerform_WhenActionIsHelpCenter_ShouldOpenADeeplink() throws {
        let url = "https://app.picpay.com/helpcenter?query=pagamento-de-conta-boletos"
        sut.perform(action: .helpCenter(url: url))
        
        let deeplinkSpy = try XCTUnwrap(dependenciesMock.legacy.deeplink as? DeeplinkSpy)
        XCTAssertEqual(deeplinkSpy.callOpenCount, 1)
    }
    
    func testPerform_WhenActionIsClose_ShouldOpenTheReceiptsScene() {
        sut.perform(action: .close)
        
        XCTAssertEqual(navControllerSpy.callPopViewControllerCount, 1)
    }
}
