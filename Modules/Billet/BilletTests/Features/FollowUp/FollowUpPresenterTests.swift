@testable import Billet
import UI
import XCTest

private final class FollowUpDisplaySpy: FollowUpDisplaying {
    private(set) var callDisplayLoadingStateCount = 0
    private(set) var callDisplayCancelLoadingCount = 0
    private(set) var callHideCancelLoadingCount = 0
    private(set) var callDisplayHeaderCount = 0
    private(set) var headerViewModel: FollowUpHeaderViewModeling?
    private(set) var callDisplaySuccessStateCount = 0
    private(set) var timelineViewModels: [FollowUpTimelineCellViewModeling]?
    private(set) var messageViewModel: FollowUpMessageViewModeling?
    private(set) var callDisplayErrorStateCount = 0
    private(set) var errorViewModel: StatefulFeedbackViewModel?
    private(set) var callDisplayAlertCount = 0
    private(set) var alert: BilletErrorAlert?

    func displayLoadingState() {
        callDisplayLoadingStateCount += 1
    }
    
    func displayCancelLoading() {
        callDisplayCancelLoadingCount += 1
    }
    
    func hideCancelLoading() {
        callHideCancelLoadingCount += 1
    }
    
    func displayHeader(with viewModel: FollowUpHeaderViewModeling) {
        callDisplayHeaderCount += 1
        headerViewModel = viewModel
    }
    
    func displaySuccessState(with viewModels: [FollowUpTimelineCellViewModeling]) {
        callDisplaySuccessStateCount += 1
        timelineViewModels = viewModels
    }
    
    func displaySuccessState(with viewModel: FollowUpMessageViewModeling) {
        callDisplaySuccessStateCount += 1
        messageViewModel = viewModel
    }
    
    func displayErrorState(with viewModel: StatefulFeedbackViewModel) {
        callDisplayErrorStateCount += 1
        errorViewModel = viewModel
    }
    
    func display(alert: BilletErrorAlert) {
        callDisplayAlertCount += 1
        self.alert = alert
    }
}

private final class FollowUpCoordinatorSpy: FollowUpCoordinating {
    var viewController: UIViewController?
    
    private(set) var callPerformCount = 0
    private(set) var action: FollowUpAction?
    
    func perform(action: FollowUpAction) {
        callPerformCount += 1
        self.action = action
    }
}

final class FollowUpPresenterTests: XCTestCase {
    private typealias Localizable = Strings.FollowUp
    
    private let viewControllerSpy = FollowUpDisplaySpy()
    private let coordinatorSpy = FollowUpCoordinatorSpy()
    private lazy var sut: FollowUpPresenting = {
        let sut = FollowUpPresenter(coordinator: coordinatorSpy)
        sut.viewController = viewControllerSpy
        return sut
    }()
    
    func testPresentLoading_ShouldCallViewController() {
        sut.presentLoading()
     
        XCTAssertEqual(viewControllerSpy.callDisplayLoadingStateCount, 1)
    }
    
    func testPresentCancelLoading_ShouldCallViewController() {
        sut.presentCancelLoading()
     
        XCTAssertEqual(viewControllerSpy.callDisplayCancelLoadingCount, 1)
    }
    
    func testHideCancelLoading_ShouldCallViewController() {
        sut.hideCancelLoading()
     
        XCTAssertEqual(viewControllerSpy.callHideCancelLoadingCount, 1)
    }
    
    func testPresentHeader_WhenIsAWarningStatus_ShouldCallViewControllerWithTheRightValues() {
        let followUpStatus = FollowUpStatusMock.accelerateAnalysis
        sut.presentHeader(with: followUpStatus)
        
        XCTAssertEqual(viewControllerSpy.callDisplayHeaderCount, 1)
        XCTAssertEqual(viewControllerSpy.headerViewModel?.title, followUpStatus.title)
        XCTAssertEqual(viewControllerSpy.headerViewModel?.status, followUpStatus.currentStatusTitle)
        XCTAssertEqual(viewControllerSpy.headerViewModel?.icon, Iconography.clockThree.rawValue)
    }
    
    func testPresentHeader_WhenIsADangerStatus_ShouldCallViewControllerWithTheRightValues() {
        let followUpStatus = FollowUpStatusMock.refused
        sut.presentHeader(with: followUpStatus)
        
        XCTAssertEqual(viewControllerSpy.callDisplayHeaderCount, 1)
        XCTAssertEqual(viewControllerSpy.headerViewModel?.title, followUpStatus.title)
        XCTAssertEqual(viewControllerSpy.headerViewModel?.status, followUpStatus.currentStatusTitle)
        XCTAssertEqual(viewControllerSpy.headerViewModel?.icon, Iconography.timesCircle.rawValue)
    }
    
    func testPresentHeader_WhenIsATimelineStatus_ShouldCallViewControllerWithTheRightValues() {
        let followUpStatus = FollowUpStatusMock.completed
        sut.presentHeader(with: followUpStatus)
        
        XCTAssertEqual(viewControllerSpy.callDisplayHeaderCount, 1)
        XCTAssertEqual(viewControllerSpy.headerViewModel?.title, followUpStatus.title)
        XCTAssertEqual(viewControllerSpy.headerViewModel?.status, followUpStatus.currentStatusTitle)
        XCTAssertNil(viewControllerSpy.headerViewModel?.icon)
    }

    func testPresentMessage_ShouldCallViewControllerWithTheRightValues() throws {
        let followUpStatus = FollowUpStatusMock.accelerateAnalysis
        let messageStatus = followUpStatus.timeline.first { $0.status == followUpStatus.currentStatusType }
        
        sut.presentMessage(with: followUpStatus)
     
        XCTAssertEqual(viewControllerSpy.callDisplaySuccessStateCount, 1)
        XCTAssertEqual(viewControllerSpy.messageViewModel?.date, messageStatus?.date)
        XCTAssertEqual(viewControllerSpy.messageViewModel?.actions.count, messageStatus?.actions.count)
    }
    
    func testPresentTimeline_ShouldCallViewControllerWithTheRightValues() {
        let followUpStatus = FollowUpStatusMock.completed
        
        sut.presentTimeline(with: followUpStatus)
        
        XCTAssertEqual(viewControllerSpy.callDisplaySuccessStateCount, 1)
        XCTAssertEqual(viewControllerSpy.timelineViewModels?.count, followUpStatus.timeline.count)
    }
    
    func testPresentError_ShouldCallViewControllerWithTheRightValues() {
        sut.presentError()
     
        XCTAssertEqual(viewControllerSpy.callDisplayErrorStateCount, 1)
        XCTAssertEqual(viewControllerSpy.errorViewModel?.content?.title, Localizable.Error.title)
        XCTAssertEqual(viewControllerSpy.errorViewModel?.content?.description, Localizable.Error.message)
    }
    
    func testPresentAlert_ShouldCallViewControllerWithTheRightValues() {
        let alert = BilletErrorAlert(icon: .none,
                                     title: Strings.cancelPaymentErrorTitle,
                                     description: Strings.cancelPaymentErrorMessage,
                                     actions: [.init(title: Strings.Common.back)])
        sut.present(alert: alert)
        
        XCTAssertEqual(viewControllerSpy.callDisplayAlertCount, 1)
        XCTAssertEqual(viewControllerSpy.alert?.title, alert.title)
        XCTAssertEqual(viewControllerSpy.alert?.description, alert.description)
        XCTAssertEqual(viewControllerSpy.alert?.actions.count, 1)
    }
    
    func testPresentAction_ShouldCallTheCoordinatorWithTheRightAction() {
        sut.present(action: .close)
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .close)
    }
}
