import AnalyticsModule
@testable import Billet
import Core
import FeatureFlag
import UI
import XCTest

private final class FollowUpServiceMock: FollowUpServicing {
    var fetchFollowUpStatusExpectedResult: Result<FollowUpStatus, ApiError> = .failure(ApiError.connectionFailure)
    var cancelPaymentExpectedResult: Result<NoContent, ApiError> = .failure(ApiError.connectionFailure)
    
    func fetchFollowUpStatus(withBillId billId: String, _ completion: @escaping FetchFollowUpStatusCompletionBlock) {
        completion(fetchFollowUpStatusExpectedResult)
    }
    
    func cancelPayment(withBillId billId: String, isScheduling: Bool, _ completion: @escaping CancelPaymentCompletionBlock) {
        completion(cancelPaymentExpectedResult)
    }
}

private final class FollowUpPresenterSpy: FollowUpPresenting {
    var viewController: FollowUpDisplaying?
    
    private(set) var callPresentLoadingCount = 0
    private(set) var callPresentCancelLoadingCount = 0
    private(set) var callHideCancelLoadingCount = 0
    private(set) var callPresentHeaderCount = 0
    private(set) var callPresentTimelineCount = 0
    private(set) var callPresentMessageCount = 0
    private(set) var followUpStatus: FollowUpStatus?
    private(set) var callPresentErrorCount = 0
    private(set) var callPresentAlertCount = 0
    private(set) var alert: BilletErrorAlert?
    private(set) var callPresentActionCount = 0
    private(set) var action: FollowUpAction?
    
    func presentLoading() {
        callPresentLoadingCount += 1
    }
    
    func presentCancelLoading() {
        callPresentCancelLoadingCount += 1
    }
    
    func hideCancelLoading() {
        callHideCancelLoadingCount += 1
    }

    func presentHeader(with followUpStatus: FollowUpStatus) {
        callPresentHeaderCount += 1
        self.followUpStatus = followUpStatus
    }

    func presentMessage(with followUpStatus: FollowUpStatus) {
        callPresentMessageCount += 1
        self.followUpStatus = followUpStatus
    }
    
    func presentTimeline(with followUpStatus: FollowUpStatus) {
        callPresentTimelineCount += 1
        self.followUpStatus = followUpStatus
    }
    
    func presentError() {
        callPresentErrorCount += 1
    }
    
    func present(alert: BilletErrorAlert) {
        callPresentAlertCount += 1
        self.alert = alert
    }
    
    func present(action: FollowUpAction) {
        callPresentActionCount += 1
        self.action = action
    }
}

final class FollowUpInteractorTests: XCTestCase {
    private let serviceMock = FollowUpServiceMock()
    private let presenterSpy = FollowUpPresenterSpy()
    private let billId = "1234"
    private let featureManagerMock = FeatureManagerMock()
    private let analyticsSpy = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(featureManagerMock, analyticsSpy)
    private lazy var sut = FollowUpInteractor(service: serviceMock,
                                              presenter: presenterSpy,
                                              billId: billId,
                                              dependencies: dependencies)
    
    func testFetchFollowUpStatus_WhenServiceReturnSuccessAndIsAMessageStatus_ShouldCallPresenter() {
        serviceMock.fetchFollowUpStatusExpectedResult = .success(FollowUpStatusMock.refused)
        
        sut.fetchFollowUpStatus()
    
        XCTAssertEqual(presenterSpy.callPresentLoadingCount, 1)
        XCTAssertEqual(presenterSpy.callPresentHeaderCount, 1)
        XCTAssertEqual(presenterSpy.callPresentMessageCount, 1)
        XCTAssertNotNil(presenterSpy.followUpStatus)
        XCTAssertEqual(analyticsSpy.event?.name, FollowUpEvent.viewed(status: .refused).event().name)
    }
    
    func testFetchFollowUpStatus_WhenServiceReturnSuccessAndIsATimelineStatus_ShouldCallPresenter() {
        serviceMock.fetchFollowUpStatusExpectedResult = .success(FollowUpStatusMock.completed)
        
        sut.fetchFollowUpStatus()
    
        XCTAssertEqual(presenterSpy.callPresentLoadingCount, 1)
        XCTAssertEqual(presenterSpy.callPresentHeaderCount, 1)
        XCTAssertEqual(presenterSpy.callPresentTimelineCount, 1)
        XCTAssertEqual(presenterSpy.followUpStatus?.timeline.count, 3)
        XCTAssertEqual(analyticsSpy.event?.name, FollowUpEvent.viewed(status: .completed).event().name)
    }
    
    func testFetchFollowUpStatus_WhenServiceReturnFailure_ShouldCallPresenter() {
        serviceMock.fetchFollowUpStatusExpectedResult = .failure(.timeout)
        
        sut.fetchFollowUpStatus()
    
        XCTAssertEqual(presenterSpy.callPresentLoadingCount, 1)
        XCTAssertEqual(presenterSpy.callPresentErrorCount, 1)
    }
    
    func testCancelPayment_WhenServiceReturnSuccessAndIsSheduling_ShouldReloadInformations() {
        serviceMock.cancelPaymentExpectedResult = .success(NoContent())
        serviceMock.fetchFollowUpStatusExpectedResult = .success(FollowUpStatusMock.completed)
        
        sut.cancelPayment(withId: "1234", isScheduling: true)
    
        XCTAssertEqual(presenterSpy.callPresentCancelLoadingCount, 1)
        XCTAssertEqual(presenterSpy.callHideCancelLoadingCount, 1)
        XCTAssertEqual(presenterSpy.callPresentLoadingCount, 1)
        
        XCTAssertEqual(presenterSpy.callPresentHeaderCount, 1)
        XCTAssertEqual(presenterSpy.callPresentTimelineCount, 1)
        XCTAssertEqual(presenterSpy.followUpStatus?.timeline.count, 3)
        XCTAssertEqual(analyticsSpy.event?.name, FollowUpEvent.viewed(status: .completed).event().name)
    }
    
    func testCancelPayment_WhenServiceReturnSuccess_ShouldReloadInformations() {
        serviceMock.cancelPaymentExpectedResult = .success(NoContent())
        serviceMock.fetchFollowUpStatusExpectedResult = .success(FollowUpStatusMock.completed)
        
        sut.cancelPayment(withId: "1234", isScheduling: false)
    
        XCTAssertEqual(presenterSpy.callPresentCancelLoadingCount, 1)
        XCTAssertEqual(presenterSpy.callHideCancelLoadingCount, 1)
        XCTAssertEqual(presenterSpy.callPresentLoadingCount, 1)
        
        XCTAssertEqual(presenterSpy.callPresentHeaderCount, 1)
        XCTAssertEqual(presenterSpy.callPresentTimelineCount, 1)
        XCTAssertEqual(presenterSpy.followUpStatus?.timeline.count, 3)
        XCTAssertEqual(analyticsSpy.event?.name, FollowUpEvent.viewed(status: .completed).event().name)
    }
    
    func testCancelPayment_WhenServiceReturnFailure_ShouldCallPresenter() {
        serviceMock.cancelPaymentExpectedResult = .failure(.timeout)
        
        sut.cancelPayment(withId: "1234", isScheduling: false)
    
        XCTAssertEqual(presenterSpy.callPresentCancelLoadingCount, 1)
        XCTAssertEqual(presenterSpy.callHideCancelLoadingCount, 1)
        
        XCTAssertEqual(presenterSpy.callPresentAlertCount, 1)
        XCTAssertEqual(presenterSpy.alert?.title, Strings.CancelPaymentPopup.Error.title)
        XCTAssertEqual(presenterSpy.alert?.description, Strings.CancelPaymentPopup.Error.message)
        XCTAssertEqual(presenterSpy.alert?.actions.count, 1)
    }
    
    func testHandleAction_WhenActionIsReceipt_ShouldCallPresenterWithTheRightAction() throws {
        let action = FollowUpStatusActionMock.receipt
        
        sut.handle(action: action)
        
        XCTAssertEqual(presenterSpy.callPresentActionCount, 1)
        let actionId = try XCTUnwrap(action.id)
        XCTAssertEqual(presenterSpy.action, .receipt(id: actionId, isScheduling: false))
        XCTAssertEqual(analyticsSpy.event?.name, FollowUpEvent.actionSelected(.receipt).event().name)
    }
    
    func testHandleAction_WhenActionIsReceiptButTheIdIsNil_ShouldNotCallPresenter() throws {
        let action = FollowUpStatusActionMock.emptyReceipt
        
        sut.handle(action: action)
        
        XCTAssertEqual(presenterSpy.callPresentActionCount, 0)
        XCTAssertNil(presenterSpy.action)
    }
    
    func testHandleAction_WhenActionIsHelpCenter_ShouldCallPresenterWithTheRightAction() throws {
        let action = FollowUpStatusActionMock.helpCenter
        
        sut.handle(action: action)
        
        XCTAssertEqual(presenterSpy.callPresentActionCount, 1)
        let actionUrl = try XCTUnwrap(action.url)
        XCTAssertEqual(presenterSpy.action, .helpCenter(url: actionUrl))
        XCTAssertEqual(analyticsSpy.event?.name, FollowUpEvent.actionSelected(.knowMore).event().name)
    }
    
    func testHandleAction_WhenActionIsHelpCenterButTheUrlIsNil_ShouldNotCallPresenter() throws {
        let action = FollowUpStatusActionMock.emptyHelpCenter
        
        sut.handle(action: action)
        
        XCTAssertEqual(presenterSpy.callPresentActionCount, 0)
        XCTAssertNil(presenterSpy.action)
    }
    
    func testHandleAction_WhenActionIsAccelerateAnalysis_ShouldCallPresenterWithTheRightAction() throws {
        let action = FollowUpStatusActionMock.accelerateAnalysis
        
        sut.handle(action: action)
        
        XCTAssertEqual(presenterSpy.callPresentActionCount, 1)
        let actionUrl = try XCTUnwrap(action.url)
        XCTAssertEqual(presenterSpy.action, .accelerateAnalysis(url: actionUrl))
        XCTAssertEqual(analyticsSpy.event?.name, FollowUpEvent.actionSelected(.accelerateAnalysis).event().name)
    }
    
    func testHandleAction_WhenActionIsAccelerateAnalysisButTheUrlIsNil_ShouldNotCallPresenter() throws {
        let action = FollowUpStatusActionMock.emptyAccelerateAnalysis
        
        sut.handle(action: action)
        
        XCTAssertEqual(presenterSpy.callPresentActionCount, 0)
        XCTAssertNil(presenterSpy.action)
    }
    
    func testHandleAction_WhenActionIsCancel_ShouldCallPresenterWithTheRightAction() throws {
        let action = FollowUpStatusActionMock.cancelPayment
        
        sut.handle(action: action)
        
        XCTAssertEqual(presenterSpy.callPresentAlertCount, 1)
        XCTAssertEqual(presenterSpy.alert?.title, Strings.CancelPaymentPopup.Warning.title)
        let actionMessage = try XCTUnwrap(action.message)
        XCTAssertEqual(presenterSpy.alert?.description, actionMessage)
        XCTAssertEqual(presenterSpy.alert?.actions.count, 2)
        XCTAssertEqual(analyticsSpy.event?.name, FollowUpEvent.actionSelected(.cancelPayment).event().name)
    }
    
    func testHandleAction_WhenActionIsCancelButTheIdIsNil_ShouldNotCallPresenter() throws {
        let action = FollowUpStatusActionMock.emptyCancelPayment
        
        sut.handle(action: action)
        
        XCTAssertEqual(presenterSpy.callPresentActionCount, 0)
        XCTAssertNil(presenterSpy.action)
    }
    
    func testHandleAction_WhenActionIsNotMapped_ShouldNotCallPresenter() throws {
        let action = FollowUpStatusActionMock.generic
        
        sut.handle(action: action)
        
        XCTAssertEqual(presenterSpy.callPresentActionCount, 0)
        XCTAssertNil(presenterSpy.action)
    }
    
    func testOpenFaq_ShouldCallPresenter() {
        featureManagerMock.override(key: .billetFollowUpFaqUrl, with: "")
        
        sut.openFaq()
    
        XCTAssertEqual(presenterSpy.callPresentActionCount, 1)
        XCTAssertEqual(presenterSpy.action, .helpCenter(url: ""))
        XCTAssertEqual(analyticsSpy.event?.name, FollowUpEvent.faqAccessed.event().name)
    }
    
    func testClose_ShouldCallPresenter() {
        sut.close()
        
        XCTAssertEqual(presenterSpy.callPresentActionCount, 1)
        XCTAssertEqual(presenterSpy.action, .close)
    }
}
