@testable import Billet
import XCTest

final class FollowUpFactoryTests: XCTestCase {
    func testMake_ShouldReturnTheRightViewController() {
        let viewController = FollowUpFactory.make(withBillId: "1234")
        
        XCTAssertTrue(viewController is FollowUpViewController)
    }
}
