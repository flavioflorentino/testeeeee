import AnalyticsModule
@testable import Billet
import Core
import CorePayment
import FeatureFlag
import XCTest
import UI

private final class BilletAccessoryPresenterSpy: BilletAccessoryPresenting {
    var viewController: BilletAccessoryDisplaying?
    
    private(set) var callPresentFillablePopupCount = 0
    private(set) var fillableInfo: (amount: Double, limit: Double)?
    private(set) var callPresentBilletInfoCount = 0
    private(set) var billetInfo: (cardFee: Double, details: [AccessoryDetailViewModeling], instructions: String)?
    private(set) var callOpenCalendarCount = 0
    private(set) var calendarDates: (minDate: Date?, maxDate: Date?)?
    private(set) var callPresentSchedulingDateCount = 0
    private(set) var date: Date?
    private(set) var callPresentAlertCount = 0
    private(set) var alert: BilletAlert?
    private(set) var callPresentPaymentExtraParamsCount = 0
    private(set) var callPresentUpdatedDetailsCount = 0
    private(set) var details: [AccessoryDetailViewModeling]?
    private(set) var callDidNextStepCount = 0
    private(set) var action: BilletAccessoryAction?
    
    func presentFillablePopup(withAmount amount: Double, andLimit limit: Double) {
        callPresentFillablePopupCount += 1
        fillableInfo = (amount: amount, limit: limit)
    }
    
    func presentBilletInfo(withCardFee cardFee: Double, details: [AccessoryDetailViewModeling], instructions: String) {
        callPresentBilletInfoCount += 1
        billetInfo = (cardFee: cardFee, details: details, instructions: instructions)
    }
    
    func openCalendar(from minDate: Date?, to maxDate: Date?) {
        callOpenCalendarCount += 1
        calendarDates = (minDate: minDate, maxDate: maxDate)
    }
    
    func present(schedulingDate date: Date) {
        callPresentSchedulingDateCount += 1
        self.date = date
    }
    
    func present(alert: BilletAlert) {
        callPresentAlertCount += 1
        self.alert = alert
    }
    
    func present(paymentExtraParams params: BilletPaymentExtraParams) {
        callPresentPaymentExtraParamsCount += 1
    }
    
    func presentUpdatedDetails(_ details: [AccessoryDetailViewModeling]) {
        callPresentUpdatedDetailsCount += 1
        self.details = details
    }

    func didNextStep(action: BilletAccessoryAction) {
        callDidNextStepCount += 1
        self.action = action
    }
}

final class BilletAccessoryInteractorTests: XCTestCase {
    private let analytcsSpy = AnalyticsSpy()
    private let featureManagerMock = FeatureManagerMock()
    private lazy var dependenciesMock = DependencyContainerMock(analytcsSpy, featureManagerMock)
    private let presenterSpy = BilletAccessoryPresenterSpy()
    private lazy var sut = BilletAccessoryInteractor(presenter: presenterSpy,
                                                     dependencies: dependenciesMock,
                                                     billet: BilletResponseMock.default)
    
    func testFetchBilletInfo_ShouldCallPresenterWithTheRightValues() {
        featureManagerMock.override(key: .opsBilletPaymentInstructionsString, with: "")
        
        sut.fetchBilletInfo()
        
        XCTAssertEqual(presenterSpy.callPresentPaymentExtraParamsCount, 1)
        XCTAssertEqual(presenterSpy.callPresentBilletInfoCount, 1)
        XCTAssertEqual(presenterSpy.billetInfo?.cardFee, 2.99)
        XCTAssertEqual(presenterSpy.billetInfo?.details.count, 2)
        XCTAssertNotNil(presenterSpy.billetInfo?.instructions)
    }
    
    func testFetchBilletInfo_WhenIsAFillableBillet_ShouldCallPresenterWithTheRightValues() {
        let sut = BilletAccessoryInteractor(presenter: presenterSpy,
                                            dependencies: dependenciesMock,
                                            billet: BilletResponseMock.fillable)
        
        featureManagerMock.override(key: .opsBilletPaymentInstructionsString, with: "")
        
        sut.fetchBilletInfo()
        
        XCTAssertEqual(presenterSpy.callPresentFillablePopupCount, 1)
        XCTAssertEqual(presenterSpy.fillableInfo?.amount, 1000)
        XCTAssertEqual(presenterSpy.fillableInfo?.limit, 1000)
        XCTAssertEqual(presenterSpy.callPresentPaymentExtraParamsCount, 1)
        XCTAssertEqual(presenterSpy.callPresentBilletInfoCount, 1)
        XCTAssertEqual(presenterSpy.billetInfo?.cardFee, 2.99)
        XCTAssertEqual(presenterSpy.billetInfo?.details.count, 2)
        XCTAssertNotNil(presenterSpy.billetInfo?.instructions)
    }
    
    func testUpdateDetails_WhenCardFeeAndInstallmentAreBiggerThanZero_ShouldCallPresenterWithTheRightValues() {
        featureManagerMock.override(key: .opsBilletPaymentInstructionsString, with: "")
        
        sut.fetchBilletInfo()
        
        let values = AccessoryValue(total: 1140.20,
                                    cardFeeValue: 29.90,
                                    installment: 5,
                                    installmentValue: 228.04,
                                    installmentFeeValue: 110.30)
        sut.updateDetails(with: values)
        
        XCTAssertEqual(presenterSpy.callPresentUpdatedDetailsCount, 1)
        XCTAssertEqual(presenterSpy.details?.count, 5)
    }
    
    func testUpdateDetails_WhenCardFeeAndInstallmentAreZero_ShouldCallPresenterWithTheRightValues() {
        featureManagerMock.override(key: .opsBilletPaymentInstructionsString, with: "")
        
        sut.fetchBilletInfo()
        
        let values = AccessoryValue(total: 1000,
                                    cardFeeValue: 0,
                                    installment: 0,
                                    installmentValue: 0,
                                    installmentFeeValue: 0)
        sut.updateDetails(with: values)
        
        XCTAssertEqual(presenterSpy.callPresentUpdatedDetailsCount, 1)
        XCTAssertEqual(presenterSpy.details?.count, 2)
    }
    
    func testScheduleAPayment_WhenBilletIsntOverdue_ShouldCallPresenterWithTheRightValues() {
        featureManagerMock.override(key: .opsBilletPaymentInstructionsString, with: "")
        
        sut.fetchBilletInfo()
        
        sut.scheduleAPayment()
        
        XCTAssertEqual(presenterSpy.callOpenCalendarCount, 1)
        XCTAssertNotNil(presenterSpy.calendarDates?.minDate)
        XCTAssertNotNil(presenterSpy.calendarDates?.maxDate)
    }
    
    func testScheduleAPayment_WhenBilletIsOverdueAndCanAcceptOverduePayment_ShouldCallPresenterWithTheRightValues() {
        let sut = BilletAccessoryInteractor(presenter: presenterSpy,
                                            dependencies: dependenciesMock,
                                            billet: BilletResponseMock.overduePaymentAccepted)
        
        featureManagerMock.override(key: .opsBilletPaymentInstructionsString, with: "")
        
        sut.fetchBilletInfo()
        
        sut.scheduleAPayment()
        
        XCTAssertEqual(presenterSpy.callOpenCalendarCount, 0)
        XCTAssertEqual(presenterSpy.callPresentAlertCount, 1)
        XCTAssertEqual(presenterSpy.alert?.title, Strings.OverdueSchedulingPopup.title)
        XCTAssertEqual(presenterSpy.alert?.message, Strings.OverdueSchedulingPopup.message)
    }
    
    func testScheduleAPaymentToNextBusinessDay_ShouldCallValidateWithANewDate() {
        let sut = BilletAccessoryInteractor(presenter: presenterSpy,
                                            dependencies: dependenciesMock,
                                            billet: BilletResponseMock.paymentTimeLimitExceeded)
        
        featureManagerMock.override(key: .opsBilletPaymentInstructionsString, with: "")
        
        sut.fetchBilletInfo()
        
        sut.scheduleAPaymentToNextBusinessDay()
        
        XCTAssertEqual(presenterSpy.callPresentPaymentExtraParamsCount, 2)
        XCTAssertEqual(presenterSpy.callPresentSchedulingDateCount, 1)
        XCTAssertNotNil(presenterSpy.date)
    }
    
    func testValidateDate_WhenDateIsTodayDate_ShouldUpdatePaymentParams() throws {
        featureManagerMock.override(key: .opsBilletPaymentInstructionsString, with: "")
        
        sut.fetchBilletInfo()
        
        let today = Date()
        
        sut.validate(schedulingDate: today)
        
        XCTAssertEqual(presenterSpy.callPresentPaymentExtraParamsCount, 2)
        XCTAssertEqual(presenterSpy.callPresentSchedulingDateCount, 0)
        XCTAssertNil(presenterSpy.date)
        XCTAssertEqual(presenterSpy.callPresentAlertCount, 0)
        XCTAssertNil(presenterSpy.alert)
    }
    
    func testValidateDate_WhenIsAWeekDay_ShouldPresentDate() throws {
        featureManagerMock.override(key: .opsBilletPaymentInstructionsString, with: "")
        
        sut.fetchBilletInfo()
        
        let calendar = Calendar.current

        var dateComponents = DateComponents()
        dateComponents.year = 2020
        dateComponents.month = 10
        dateComponents.day = 15
    
        let day = try XCTUnwrap(calendar.date(from: dateComponents))
        
        sut.validate(schedulingDate: day)
        
        XCTAssertEqual(presenterSpy.callPresentPaymentExtraParamsCount, 2)
        XCTAssertEqual(presenterSpy.callPresentSchedulingDateCount, 1)
        XCTAssertNotNil(presenterSpy.date)
    }
    
    func testDismiss_ShouldCallPresenterWithTheRightEvent() {
        sut.dismiss()
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.action, .dismiss)
    }
    
    func testTrackFeeTypeViewed_ShouldLogTheRightEvent() {
        sut.trackFeeTypeViewed(.convenience)
        
        XCTAssertEqual(analytcsSpy.event?.name, PaymentEvent.billetCardFeeInformation.event().name)
    }
}
