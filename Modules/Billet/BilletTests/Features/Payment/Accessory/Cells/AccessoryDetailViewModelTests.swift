@testable import Billet
import XCTest

final class AccessoryDetailViewModelTests: XCTestCase {
    func testCreateAccessoryDetail_WhenActionIsCommonAndValueIsValid_ShouldHaveTheRightValue() {
        let sut = AccessoryDetailViewModel(description: "Beneficiary:", type: .common("Bradesco"))
        XCTAssertEqual(sut?.value, "Bradesco")
    }
    
    func testCreateAccessoryDetail_WhenActionIsCommonAndValueIsNil_ShouldBeNil() {
        let sut = AccessoryDetailViewModel(description: "Beneficiary:", type: .common(nil))
        XCTAssertNil(sut)
    }
    
    func testCreateAccessoryDetail_WhenActionIsDateAndValueIsValid_ShouldHaveTheRightValue() {
        let sut = AccessoryDetailViewModel(description: "Date:", type: .date("10/10/2020", false))
        XCTAssertEqual(sut?.value, "10/10/2020")
    }
    
    func testCreateAccessoryDetail_WhenActionIsDateAndValueIsNil_ShouldBeNil() {
        let sut = AccessoryDetailViewModel(description: "Date:", type: .date(nil, false))
        XCTAssertNil(sut)
    }
    
    func testCreateAccessoryDetail_WhenActionIsInstallemntsAndValueIsValid_ShouldHaveTheRightValue() throws {
        let sut = AccessoryDetailViewModel(description: "Installments:", type: .installments(2, 165.34))
        let value = try XCTUnwrap(sut?.value)
        XCTAssertEqual(value, "2x R$ 165,34")
    }
    
    func testCreateAccessoryDetail_WhenActionIsInstallemntsAndValueIsNil_ShouldBeNil() {
        let sut = AccessoryDetailViewModel(description: "Installments:", type: .installments(2, nil))
        XCTAssertNil(sut)
    }
    
    func testCreateAccessoryDetail_WhenActionIsTaxAndValueIsValid_ShouldHaveTheRightValue() throws {
        let sut = AccessoryDetailViewModel(description: "Tax:", type: .fee(102.22, .interest))
        let value = try XCTUnwrap(sut?.value)
        XCTAssertEqual(value, "R$ 102,22")
    }
    
    func testCreateAccessoryDetail_WhenActionIsTaxAndValueIsNil_ShouldBeNil() {
        let sut = AccessoryDetailViewModel(description: "Tax:", type: .fee(nil, .interest))
        XCTAssertNil(sut)
    }
    
    func testCreateAccessoryDetail_WhenActionIsValueAndTotalValueIsValid_ShouldHaveTheRightValue() throws {
        let sut = AccessoryDetailViewModel(description: "Total value:", type: .totalValue(1029.90))
        let value = try XCTUnwrap(sut?.value)
        XCTAssertEqual(value, "R$ 1.029,90")
    }
    
    func testCreateAccessoryDetail_WhenActionIsValueAndTotalValueIsNil_ShouldBeNil() {
        let sut = AccessoryDetailViewModel(description: "Total value:", type: .totalValue(nil))
        XCTAssertNil(sut)
    }
}
