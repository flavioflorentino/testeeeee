@testable import Billet
import Core
import UI
import XCTest

final class BilletAccessoryCoordinatorTests: XCTestCase {
    private let viewControllerSpy = ViewControllerMock()
    private lazy var navControllerSpy = NavigationControllerMock(rootViewController: viewControllerSpy)
    private lazy var sut: BilletAccessoryCoordinator = {
        let coordinator = BilletAccessoryCoordinator()
        coordinator.viewController = navControllerSpy.topViewController
        return coordinator
    }()
    
    func testPerform_WhenActionIsOverduePaymentUnaccepted_ShouldOpenTheErrorScene() {
        sut.perform(action: .dismiss)
        
        XCTAssertEqual(viewControllerSpy.didDismissControllerCount, 1)
    }
}
