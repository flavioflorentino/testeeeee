import AssetsKit
@testable import Billet
import UI
import UIKit
import XCTest

private final class BilletAccessoryDisplayingSpy: BilletAccessoryDisplaying {
    private(set) var callDisplayFillablePopupCount = 0
    private(set) var fillableInfo: (amount: Double, limit: Double)?
    private(set) var callDisplayBilletInfoCount = 0
    private(set) var billetInfo: (details: [AccessoryDetailViewModeling], instructions: NSAttributedString)?
    private(set) var callDisplayCalendarCount = 0
    private(set) var calendarDates: (minDate: Date?, maxDate: Date?)?
    private(set) var callDisplaySchedulingDateCount = 0
    private(set) var date: String?
    private(set) var callDisplayOverdueSchedulingAlertCount = 0
    private(set) var callDisplayPaymentTimeExceededAlertCount = 0
    private(set) var alert: BilletAlert?
    private(set) var callUpdateCardFeeCount = 0
    private(set) var cardFee: Double?
    private(set) var callSendExtraParamsToContainerPaymentCount = 0
    private(set) var callDisplayUpdatedDetailsCount = 0
    private(set) var details: [AccessoryDetailViewModeling]?
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0

    func displayFillablePopup(amount: Double, limit: Double) {
        callDisplayFillablePopupCount += 1
        fillableInfo = (amount: amount, limit: limit)
    }
    
    func displayBilletInfo(details: [AccessoryDetailViewModeling], instructions: NSAttributedString) {
        callDisplayBilletInfoCount += 1
        billetInfo = (details: details, instructions: instructions)
    }
    
    func displayCalendar(from minDate: Date?, to maxDate: Date?) {
        callDisplayCalendarCount += 1
        calendarDates = (minDate: minDate, maxDate: maxDate)
    }
    
    func display(schedulingDate date: String) {
        callDisplaySchedulingDateCount += 1
        self.date = date
    }
    
    func displayOverdueSchedulingAlert(_ alert: BilletAlert) {
        callDisplayOverdueSchedulingAlertCount += 1
        self.alert = alert
    }
    
    func displayPaymentTimeExceededAlert(_ alert: BilletAlert) {
        callDisplayPaymentTimeExceededAlertCount += 1
        self.alert = alert
    }
    
    func updateCardFee(_ cardFee: Double) {
        callUpdateCardFeeCount += 1
        self.cardFee = cardFee
    }
    
    func sendExtraParamsToContainerPayment(_ params: Decodable) {
        callSendExtraParamsToContainerPaymentCount += 1
    }
    
    func displayUpdatedDetails(_ details: [AccessoryDetailViewModeling]) {
        callDisplayUpdatedDetailsCount += 1
        self.details = details
    }
    
    func startLoading() {
        callStartLoadingCount += 1
    }
    
    func stopLoading() {
        callStopLoadingCount += 1
    }
}

private final class BilletAccessoryCoordinatorSpy: BilletAccessoryCoordinating {
    var viewController: UIViewController?
    
    private(set) var callPerformCount = 0
    private(set) var action: BilletAccessoryAction?
    
    func perform(action: BilletAccessoryAction) {
        callPerformCount += 1
        self.action = action
    }
}

final class BilletAccessoryPresenterTests: XCTestCase {
    private let viewControllerSpy = BilletAccessoryDisplayingSpy()
    private let coordinatorSpy = BilletAccessoryCoordinatorSpy()
    private lazy var sut: BilletAccessoryPresenter = {
        let sut = BilletAccessoryPresenter(coordinator: coordinatorSpy)
        sut.viewController = viewControllerSpy
        return sut
    }()
    
    func testDidNextStep_ShouldCallTheCoordinatorWithTheRightAction() {
        sut.didNextStep(action: .dismiss)
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .dismiss)
    }
    
    func testPresentFillablePopup_ShouldCallTheViewControllerWithTheRightValues() {
        sut.presentFillablePopup(withAmount: 1000, andLimit: 1000)
        
        XCTAssertEqual(viewControllerSpy.callDisplayFillablePopupCount, 1)
        XCTAssertEqual(viewControllerSpy.fillableInfo?.amount, 1000)
        XCTAssertEqual(viewControllerSpy.fillableInfo?.limit, 1000)
    }
    
    func testPresentBilletInfo_ShouldCallTheViewControllerWithTheRightValues() {
        sut.presentBilletInfo(withCardFee: 2.99, details: [], instructions: "**attention**")
        
        XCTAssertEqual(viewControllerSpy.callDisplayBilletInfoCount, 1)
        XCTAssertEqual(viewControllerSpy.billetInfo?.details.count, 0)
        XCTAssertEqual(viewControllerSpy.billetInfo?.instructions.string, "attention")
    }
    
    func testOpenCalendar_ShouldCallTheViewControllerWithTheRightValues() {
        let minDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())
        let maxDate = Calendar.current.date(byAdding: .month, value: 1, to: Date())
        
        sut.openCalendar(from: minDate, to: maxDate)
        
        XCTAssertEqual(viewControllerSpy.callDisplayCalendarCount, 1)
        XCTAssertEqual(viewControllerSpy.calendarDates?.minDate, minDate)
        XCTAssertEqual(viewControllerSpy.calendarDates?.maxDate, maxDate)
    }
    
    func testPresentSchedulingDate_ShouldCallTheViewControllerWithTheRightValues() {
        let date = Date()
        sut.present(schedulingDate: date)
        
        XCTAssertEqual(viewControllerSpy.callDisplaySchedulingDateCount, 1)
        XCTAssertEqual(viewControllerSpy.date, Date.ddMMyyyy.string(from: date))
    }
    
    func testPresentAlert_WhenIsAOverdueSchedulingAlert_ShouldCallTheViewControllerWithTheRightValues() {
        let alert: BilletAlert = .overdueScheduling
        sut.present(alert: alert)
        
        XCTAssertEqual(viewControllerSpy.callDisplayOverdueSchedulingAlertCount, 1)
        XCTAssertEqual(viewControllerSpy.alert?.title, Strings.OverdueSchedulingPopup.title)
        XCTAssertEqual(viewControllerSpy.alert?.formattedMessage.string, Strings.OverdueSchedulingPopup.message)
    }
    
    func testPresentAlert_WhenIsAPaymentTimeLimitExceededAlert_ShouldCallTheViewControllerWithTheRightValues() {
        let alert: BilletAlert = .paymentTimeLimitExceeded(timeLimit: "19:00")
        sut.present(alert: alert)
        
        XCTAssertEqual(viewControllerSpy.callDisplayPaymentTimeExceededAlertCount, 1)
        XCTAssertEqual(viewControllerSpy.alert?.title, Strings.PaymentTimeExceededPopup.title)
        XCTAssertEqual(viewControllerSpy.alert?.formattedMessage.string, Strings.PaymentTimeExceededPopup.message("19:00"))
    }
    
    func testPresentPaymentExtraParams_ShouldCallTheViewControllerWithTheRightValues() {
        sut.present(paymentExtraParams: BilletPaymentExtraParamsMock.default)
        
        XCTAssertEqual(viewControllerSpy.callSendExtraParamsToContainerPaymentCount, 1)
    }
    
    func testPresentUpdatedDetails_ShouldCallTheViewControllerWithTheRightValues() {
        sut.presentUpdatedDetails([])
        
        XCTAssertEqual(viewControllerSpy.callDisplayUpdatedDetailsCount, 1)
    }
}
