@testable import Billet
import Core
import UI
import XCTest

final class BilletScannerCoordinatorTests: XCTestCase {
    private let dependenciesMock = DependencyContainerMock()
    private let viewControllerSpy = ViewControllerMock()
    private lazy var navControllerSpy = NavigationControllerMock(rootViewController: viewControllerSpy)
    private lazy var sut: BilletScannerCoordinating = {
        let coordinator = BilletScannerCoordinator(dependencies: dependenciesMock)
        coordinator.viewController = navControllerSpy.topViewController
        return coordinator
    }()
    
    func testPerform_WhenActionIsForm_ShouldOpenTheBilletFormScene() throws {
        sut.perform(action: .form(barcode: "292939409540950595059540940402", origin: .unknown))
        
        XCTAssertTrue(navControllerSpy.pushedViewController is BilletFormViewController)
    }

    func testPerform_WhenActionIsClose_ShouldPopScannerScene() throws {
        sut.perform(action: .close)
        
        XCTAssertEqual(navControllerSpy.callPopViewControllerCount, 1)
    }
}
