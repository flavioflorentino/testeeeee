import XCTest
@testable import Billet

private final class BilletScannerPresenterSpy: BilletScannerPresenting {
    var viewController: BilletScannerDisplaying?
    
    private(set) var callPresentActionCount = 0
    private(set) var action: BilletScannerAction?
    
    func present(action: BilletScannerAction) {
        callPresentActionCount += 1
        self.action = action
    }
}

final class BilletScannerInteractorTests: XCTestCase {
    private let presenterSpy = BilletScannerPresenterSpy()
    private lazy var sut = BilletScannerInteractor(presenter: presenterSpy, origin: .unknown)
    
    func testOpenForm_WhenScannerGetsBarcode_ShouldCallPresenter() {
        sut.openForm(withBarcode: "292939409540950595059540940402")
        
        XCTAssertEqual(presenterSpy.callPresentActionCount, 1)
        XCTAssertEqual(presenterSpy.action, .form(barcode: "292939409540950595059540940402", origin: .unknown))
    }
    
    func testOpenForm_WithoutBarcode_ShouldCallPresenter() {
        sut.openForm()
        
        XCTAssertEqual(presenterSpy.callPresentActionCount, 1)
        
        if case let .form(barcode, origin) = presenterSpy.action {
            XCTAssertNil(barcode)
            XCTAssertNotNil(origin)
        } else {
            XCTFail("PresenterSpy.action should be a form action")
        }
    }
    
    func testClose_ShouldCallPresenter() {
        sut.close()
        
        XCTAssertEqual(presenterSpy.callPresentActionCount, 1)
        XCTAssertEqual(presenterSpy.action, .close)
    }
}
