import XCTest
@testable import Billet

final class BilletScannerFactoryTests: XCTestCase {
    func testMake_ShouldReturnTheRightViewController() {
        let viewController = BilletScannerFactory.make(with: .unknown)
        
        XCTAssertTrue(viewController is BilletScannerViewController)
    }
}
