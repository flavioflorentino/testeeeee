import UI
import XCTest
@testable import Billet

private final class BilletScannerCoordinatorSpy: BilletScannerCoordinating {
    var viewController: UIViewController?
    
    private(set) var callPerformCount = 0
    private(set) var action: BilletScannerAction?
    
    func perform(action: BilletScannerAction) {
        callPerformCount += 1
        self.action = action
    }
}

final class BilletScannerPresenterTests: XCTestCase {
    private let coordinatorSpy = BilletScannerCoordinatorSpy()
    private lazy var sut = BilletScannerPresenter(coordinator: coordinatorSpy)
    
    func testPresentAction_ShouldCallTheCoordinatorWithTheRightAction() {
        sut.present(action: .form(barcode: "292939409540950595059540940402", origin: .unknown))
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .form(barcode: "292939409540950595059540940402", origin: .unknown))
    }
}
