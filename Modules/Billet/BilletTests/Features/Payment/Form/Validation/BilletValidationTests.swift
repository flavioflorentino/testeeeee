@testable import Billet
import XCTest

final class BilletValidationTests: XCTestCase {
    
    func testValidate_WhenLinecodeIsAnAgreementFilledAndIsCorrect_ShouldBeValid() {
        let linecode = "816200000007832947842015903194784091310761100786"
        let isLinecodeValid = BilletPartialValidation.validate(input: linecode.onlyNumbers)
        
        XCTAssertTrue(isLinecodeValid, "When we input a correct agreement linecode, this linecode should be valid")
    }
    
    func testValidate_WhenLinecodeIsAnAgreementFilledAndIsIncorrect_ShouldBeInvalid() {
        let linecode = "816200000007832947842015903194784091310761100789"
        let isLinecodeValid = BilletPartialValidation.validate(input: linecode.onlyNumbers)
        
        XCTAssertFalse(isLinecodeValid, "When we input an incorrect agreement linecode, this linecode should be invalid")
    }
    
    func testValidate_WhenLinecodeIsAnAgreementButNotFilledAndIsCorrect_ShouldBeValid() {
        let linecode = "816200000007"
        let isLinecodeValid = BilletPartialValidation.validate(input: linecode.onlyNumbers)
        
        XCTAssertTrue(isLinecodeValid, "When we input a correct agreement linecode that is not filled, this linecode should be valid")
    }
    
    func testValidate_WhenLinecodeIsAnAgreementButNotFilledAndIsIncorrect_ShouldBeInvalid() {
        let linecode = "816200000107"
        let isLinecodeValid = BilletPartialValidation.validate(input: linecode.onlyNumbers)
        
        XCTAssertFalse(isLinecodeValid, "When we input a incorrect agreement linecode that is not filled, this linecode should be invalid")
    }
    
    func testValidate_WhenLinecodeHasSufficientCharactersAndIsCorrect_ShouldBeValid() {
        let linecode = "10495320528500017004400032263212683550000004990"
        let isLinecodeValid = BilletPartialValidation.validate(input: linecode.onlyNumbers)
        
        XCTAssertTrue(isLinecodeValid, "When we input a correct linecode that has over 46 characters, this linecode should be valid")
    }
    
    func testValidate_WhenLinecodeHasSufficientCharactersAndIsIncorrect_ShouldBeInvalid() {
        let linecode = "104953205285000170044000322632126835500000049901"
        let isLinecodeValid = BilletPartialValidation.validate(input: linecode.onlyNumbers)
        
        XCTAssertFalse(isLinecodeValid, "When we input an incorrect linecode that has over 46 characters, this linecode should be invalid")
    }
    
    func testValidate_WhenLinecodeIsNotFilledButIsCorrect_ShouldBeValid() {
        let linecode = "104953205285"
        let isLinecodeValid = BilletPartialValidation.validate(input: linecode.onlyNumbers)
        
        XCTAssertTrue(isLinecodeValid, "When we input a correct linecode that is not filled, this linecode should be valid")
    }
    
    func testValidate_WhenLinecodeIsNotFilledAndIsIncorrect_ShouldBeInvalid() {
        let linecode = "104953205385"
        let isLinecodeValid = BilletPartialValidation.validate(input: linecode.onlyNumbers)
        
        XCTAssertFalse(isLinecodeValid, "When we input a incorrect linecode that is not filled, this linecode should be invalid")
    }
}
