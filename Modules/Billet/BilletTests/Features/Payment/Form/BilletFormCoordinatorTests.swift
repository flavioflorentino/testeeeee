@testable import Billet
import Core
import FeatureFlag
import UI
import XCTest

final class BilletFormCoordinatorTests: XCTestCase {
    private let featureManagerMock = FeatureManagerMock()
    private lazy var dependenciesMock = DependencyContainerMock(featureManagerMock)
    private let viewControllerSpy = ViewControllerMock()
    private lazy var navControllerSpy = NavigationControllerMock(rootViewController: viewControllerSpy)
    private lazy var sut: BilletFormCoordinator = {
        let coordinator = BilletFormCoordinator(dependencies: dependenciesMock)
        coordinator.viewController = navControllerSpy.topViewController
        return coordinator
    }()
    
    func testPerform_WhenActionIsPayment_ShouldOpenTheBilletPaymentScene() throws {
        sut.perform(action: .payment(model: BilletResponseMock.default, origin: .unknown))
        
        let billetSpy = try XCTUnwrap(dependenciesMock.legacy.billet as? BilletFlowSpy)
        XCTAssertEqual(billetSpy.callOpenBilletPaymentCount, 1)
    }

    func testPerform_WhenActionIsTimelineAndFeatureFlagIsOn_ShouldOpenTheFollowUpScene() {
        featureManagerMock.override(key: .isFollowUpAvailable, with: true)
        
        sut.perform(action: .timeline(transactionId: "1234", externalId: "bill-1234"))
        
        XCTAssertTrue(navControllerSpy.pushedViewController is FollowUpViewController)
    }
    
    func testPerform_WhenActionIsTimelineAndFeatureFlagIsOff_ShouldOpenTheFeedDetailScene() {
        featureManagerMock.override(key: .isFollowUpAvailable, with: false)
        
        sut.perform(action: .timeline(transactionId: "1234", externalId: "bill-1234"))
        
        XCTAssertTrue(navControllerSpy.pushedViewController is BilletFeedDetailViewController)
    }
    
    func testPerform_WhenActionIsReceipt_ShouldOpenTheReceiptScene() throws {
        sut.perform(action: .receipt(id: "123"))
        
        let receiptSpy = try XCTUnwrap(dependenciesMock.legacy.receipt as? ReceiptSpy)
        XCTAssertEqual(receiptSpy.callOpenCount, 1)
    }
    
    func testPerform_WhenActionIsFaq_ShouldOpenTheFaqScene() throws {
        let url = "https://meajuda.picpay.com/hc/pt-br/articles/360044239952-N%C3%A3o-t%C3%B4-conseguindo-pagar-meu-boleto-Me-ajuda-"
        sut.perform(action: .faq(url: url))
        
        let faqSpy = try XCTUnwrap(dependenciesMock.legacy.deeplink as? DeeplinkSpy)
        XCTAssertEqual(faqSpy.callOpenCount, 1)
    }
    
    func testPerform_WhenActionIsClose_ShouldCloseBilletFlow() {
        sut.perform(action: .close)
        
        XCTAssertEqual(viewControllerSpy.didDismissControllerCount, 1)
    }
    
}
