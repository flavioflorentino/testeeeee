import AnalyticsModule
import Core
import XCTest
import UI
@testable import Billet

private final class BilletFormServiceMock: BilletFormServicing {
    var expectedResult: Result<BilletResponse, Error> = .failure(ApiError.connectionFailure)
    
    func fetchBillet(withCode code: String, completion: @escaping FetchBilletCompletionBlock) {
        completion(expectedResult)
    }
}

private final class BilletFormPresenterSpy: BilletFormPresenting {
    var viewController: BilletFormDisplaying?
    
    private(set) var callPresentFilledBarcodeCount = 0
    private(set) var barcode: String?
    private(set) var callPresentFilledLinecodeCount = 0
    private(set) var linecode: String?
    private(set) var callPresentValidLinecodeCount = 0
    private(set) var callPresentInvalidLinecodeCount = 0
    private(set) var callPresentNextActionAvailabilityCount = 0
    private(set) var isNextActionAvailable = false
    private(set) var callPresentBilletBlockMaskCount = 0
    private(set) var blockMask: String?
    private(set) var callPresentLoadingCount = 0
    private(set) var callHideLoadingCount = 0
    private(set) var callPresentAlertCount = 0
    private(set) var alert: BilletErrorAlert?
    private(set) var callDidNextStepCount = 0
    private(set) var action: BilletFormAction?
    
    func presentFilled(barcode: String) {
        callPresentFilledBarcodeCount += 1
        self.barcode = barcode
    }
    
    func presentFilled(linecode: String, description: String?) {
        callPresentFilledLinecodeCount += 1
        self.linecode = linecode
    }
    
    func presentValidLinecode() {
        callPresentValidLinecodeCount += 1
    }
    
    func presentInvalidLinecode() {
        callPresentInvalidLinecodeCount += 1
    }
    
    func presentNextActionAvailability(_ isNextActionAvailable: Bool) {
        callPresentNextActionAvailabilityCount += 1
        self.isNextActionAvailable = isNextActionAvailable
    }
    
    func presentBilletBlockMask(_ blockMask: String) {
        callPresentBilletBlockMaskCount += 1
        self.blockMask = blockMask
    }
    
    func presentLoading() {
        callPresentLoadingCount += 1
    }
    
    func hideLoading() {
        callHideLoadingCount += 1
    }
    
    func present(alert: BilletErrorAlert) {
        callPresentAlertCount += 1
        self.alert = alert
    }
    
    func didNextStep(action: BilletFormAction) {
        callDidNextStepCount += 1
        self.action = action
    }
}

final class BilletFormInteractortTests: XCTestCase {
    private let analytcsSpy = AnalyticsSpy()
    private lazy var dependenciesMock = DependencyContainerMock(analytcsSpy)
    private let serviceMock = BilletFormServiceMock()
    private let presenterSpy = BilletFormPresenterSpy()
    private let linecode = "816200000007832947842015903194784091310761100786"
    private lazy var sut = BilletFormInteractor(dependencies: dependenciesMock,
                                                service: serviceMock,
                                                presenter: presenterSpy,
                                                origin: .homeSugestions,
                                                linecode: linecode)
    
    func testGetFilledBarcodeIfNeeded_WhenBarcodeIsFilled_ShouldCallPresenter() {
        let barcode = "8162000000078329478420159"
        let sut = BilletFormInteractor(dependencies: dependenciesMock,
                                       service: serviceMock,
                                       presenter: presenterSpy,
                                       origin: .homeSugestions,
                                       barcode: barcode)
        sut.getFilledCodeIfNeeded()
        
        XCTAssertEqual(presenterSpy.callPresentFilledBarcodeCount, 1)
        XCTAssertEqual(presenterSpy.barcode, barcode)
        XCTAssertEqual(analytcsSpy.event?.name, PaymentEvent.billetFormViewed.event().name)
    }
    
    func testGetFilledLinecodeIfNeeded_WhenLinecodeIsFilled_ShouldCallPresenter() {
        sut.getFilledCodeIfNeeded()
        
        XCTAssertEqual(presenterSpy.callPresentFilledLinecodeCount, 1)
        XCTAssertEqual(presenterSpy.linecode, linecode)
        XCTAssertEqual(analytcsSpy.event?.name, PaymentEvent.billetFormViewed.event().name)
    }
    
    func testSkipValidation_WhenBarcodeIsFilled_ShouldCallPresenter() {
        sut.skipValidation()
        
        XCTAssertEqual(presenterSpy.callPresentValidLinecodeCount, 1)
        XCTAssertEqual(presenterSpy.callPresentNextActionAvailabilityCount, 1)
    }
    
    func testValidate_WhenLinecodeIsFilledAndIsValid_ShouldCallPresenter() throws {
        let previousLinecode = "81620000000783294784201590319478409131076110078"
        let linecode = "816200000007832947842015903194784091310761100786"
        
        sut.validate(previousLinecode: previousLinecode, linecode: linecode)
        
        XCTAssertEqual(presenterSpy.callPresentValidLinecodeCount, 1)
        XCTAssertEqual(presenterSpy.callPresentNextActionAvailabilityCount, 1)
        let isNextActionAvailable = try XCTUnwrap(presenterSpy.isNextActionAvailable)
        XCTAssertTrue(isNextActionAvailable)
    }
    
    func testValidate_WhenLinecodeIsFilledAndIsInvalid_ShouldCallPresenter() throws {
        let previousLinecode = "81620000000783294784201590319478409131076110078"
        let linecode = "816200000007832947842015903194784091310761100789"
        
        sut.validate(previousLinecode: previousLinecode, linecode: linecode)
        
        XCTAssertEqual(presenterSpy.callPresentInvalidLinecodeCount, 1)
        XCTAssertEqual(presenterSpy.callPresentNextActionAvailabilityCount, 1)
        let isNextActionAvailable = try XCTUnwrap(presenterSpy.isNextActionAvailable)
        XCTAssertFalse(isNextActionAvailable)
    }
    
    func testValidate_WhenLinecodeisntFilledAndIsValid_ShouldCallPresenter() throws {
        let previousLinecode = ""
        let linecode = "816200000007"
        
        sut.validate(previousLinecode: previousLinecode, linecode: linecode)
        
        XCTAssertEqual(presenterSpy.callPresentValidLinecodeCount, 1)
        XCTAssertEqual(presenterSpy.callPresentNextActionAvailabilityCount, 1)
        let isNextActionAvailable = try XCTUnwrap(presenterSpy.isNextActionAvailable)
        XCTAssertFalse(isNextActionAvailable)
    }
    
    func testValidate_WhenLinecodeisntFilledAndIsInvalid_ShouldCallPresenter() throws {
        let previousLinecode = ""
        let linecode = "816200000107"
        
        sut.validate(previousLinecode: previousLinecode, linecode: linecode)
        
        XCTAssertEqual(presenterSpy.callPresentInvalidLinecodeCount, 1)
        XCTAssertEqual(presenterSpy.callPresentNextActionAvailabilityCount, 1)
        let isNextActionAvailable = try XCTUnwrap(presenterSpy.isNextActionAvailable)
        XCTAssertFalse(isNextActionAvailable)
    }
    
    func testMask_WhenSuperviewIsMaskTextField_ShouldCallPresenter() throws {
        let blockMask = "81620000000-7 83294784201-5 90319478409-1 31076110078-6"
        
        let superview = UIPPFloatingTextView()
        superview.setupBilletMask()
        
        sut.mask(withText: linecode, superview: superview)
        
        XCTAssertEqual(presenterSpy.callPresentBilletBlockMaskCount, 1)
        XCTAssertEqual(presenterSpy.blockMask, blockMask)
    }
    
    func testFetchBillet_WhenServiceReturnSuccess_ShouldCallPresenterWithTheRightValues() {
        serviceMock.expectedResult = .success(BilletResponseMock.default)
        
        sut.fetchBillet(withCode: linecode, description: nil)
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.action, .payment(model: BilletResponseMock.default, description: nil, origin: .homeSugestions))
        XCTAssertEqual(analytcsSpy.event?.name, PaymentEvent.billetFormNextSelected(false).event().name)
    }
    
    func testFetchBillet_WhenServiceReturnSuccessAndHasDescription_ShouldCallPresenterWithTheRightValues() {
        serviceMock.expectedResult = .success(BilletResponseMock.default)
        
        sut.fetchBillet(withCode: linecode, description: "random description")
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.action, .payment(model: BilletResponseMock.default, description: "random description", origin: .homeSugestions))
        XCTAssertEqual(analytcsSpy.event?.name, PaymentEvent.billetFormNextSelected(true).event().name)
    }
    
    func testFetchBilletInfo_WhenServiceReturnSuccessAndCantReceiveOverdue_ShouldCallPresenterWithTheRightValues() {
        serviceMock.expectedResult = .success(BilletResponseMock.overduePaymentUnnaccepted)
        
        sut.fetchBillet(withCode: linecode, description: nil)
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.action, .overduePaymentUnaccepted)
    }
    
    func testFetchBilletInfo_WhenServiceReturnSuccessAndBillIdIsNotEmpty_ShouldCallPresenterWithTheRightValues() {
        serviceMock.expectedResult = .success(BilletResponseMock.duplicationScheduling)
        
        sut.fetchBillet(withCode: linecode, description: nil)
        
        XCTAssertEqual(presenterSpy.callPresentAlertCount, 1)
    }
    
    func testFetchBilletInfo_WhenServiceReturnFailureAndTheErrorIsAnApiError_ShouldCallPresenterWithAnError() {
        serviceMock.expectedResult = .failure(ApiError.connectionFailure)
        
        sut.fetchBillet(withCode: linecode, description: nil)
        
        XCTAssertEqual(presenterSpy.callPresentAlertCount, 1)
        XCTAssertNotNil(presenterSpy.alert)
    }
    
    func testFetchBilletInfo_WhenServiceReturnFailureAndTheErrorIsABilletError_ShouldCallPresenterWithAnError() {
        let billetError = BilletError(title: "random title",
                                      description: "random description",
                                      image: .success,
                                      type: .alreadyPayed,
                                      actions: [])
        
        serviceMock.expectedResult = .failure(billetError)
        
        sut.fetchBillet(withCode: linecode, description: nil)
        
        XCTAssertEqual(presenterSpy.callPresentAlertCount, 1)
        XCTAssertNotNil(presenterSpy.alert)
    }
    
    func testOpenReceipt_ShouldCallCoordinatorWithTheRightAction() {
        sut.openReceipt(withId: 123)
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.action, .receipt(id: "123"))
    }
    
    func testOpenFaq_ShouldCallCoordinatorWithTheRightAction() {
        let url = "https://meajuda.picpay.com/hc/pt-br/articles/360044239952-N%C3%A3o-t%C3%B4-conseguindo-pagar-meu-boleto-Me-ajuda-"
        sut.openFaq(withUrl: url)
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.action, .faq(url: url))
    }
    
    func testClose_ShouldCallPresenter() {
        sut.close()
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.action, .close)
    }
}
