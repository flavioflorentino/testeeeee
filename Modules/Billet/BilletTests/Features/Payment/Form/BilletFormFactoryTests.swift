import XCTest
@testable import Billet

final class BilletFormFactoryTests: XCTestCase {
    func testMake_ShouldReturnTheRightViewController() {
        let viewController = BilletFormFactory.make(with: .scanner)
        
        XCTAssertTrue(viewController is BilletFormViewController)
    }
    
    func testMake_WhenLinecoseIsSetted_ShouldReturnTheRightViewController() {
        let viewController = BilletFormFactory.make(with: .deeplink, linecode: "816200000007832947842015903194784091310761100786")
        
        XCTAssertTrue(viewController is BilletFormViewController)
    }
    
    func testMake_WhenLinecoseAndDescriptionAreSetted_ShouldReturnTheRightViewController() {
        let viewController = BilletFormFactory.make(with: .deeplink,
                                                    linecode: "816200000007832947842015903194784091310761100786",
                                                    description: "Pagamento via deeplink")
        
        XCTAssertTrue(viewController is BilletFormViewController)
    }
    
    func testMake_WhenBarcodeIsSetted_ShouldReturnTheRightViewController() {
        let viewController = BilletFormFactory.make(with: .deeplink, barcode: "34191855300002151741090187406130670392216000")
        
        XCTAssertTrue(viewController is BilletFormViewController)
    }
}
