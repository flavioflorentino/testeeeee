import UI
import XCTest
@testable import Billet

private final class BilletFormDisplayingSpy: BilletFormDisplaying {
    private(set) var callDisplayBarcodeCount = 0
    private(set) var barcode: String?
    private(set) var callDisplayLinecodeCount = 0
    private(set) var linecode: String?
    private(set) var callDisplayMaskedBlockCount = 0
    private(set) var maskedBlock: String?
    private(set) var callHideLinecodeErrorCount = 0
    private(set) var callDisplayLinecodeErrorCount = 0
    private(set) var message: String?
    private(set) var callDisplayNextButtonAvailabilityCount = 0
    private(set) var isNextButtonAvailable = false
    private(set) var callDisplayLoadingCount = 0
    private(set) var callHideLoadingCount = 0
    private(set) var callDisplayAlertCount = 0
    private(set) var errorAlert: BilletErrorAlert?

    func display(barcode: String) {
        callDisplayBarcodeCount += 1
        self.barcode = barcode
    }
    
    func display(linecode: String, description: String?) {
        callDisplayLinecodeCount += 1
        self.linecode = linecode
    }
    
    func display(maskedBlock: String) {
        callDisplayMaskedBlockCount += 1
        self.maskedBlock = maskedBlock
    }
    
    func hideLinecodeError() {
        callHideLinecodeErrorCount += 1
    }
    
    func displayLinecodeError(withMessage message: String) {
        callDisplayLinecodeErrorCount += 1
        self.message = message
    }
    
    func displayNextButtonAvailability(_ isNextButtonAvailable: Bool) {
        callDisplayNextButtonAvailabilityCount += 1
        self.isNextButtonAvailable = isNextButtonAvailable
    }
    
    func displayLoading() {
        callDisplayLoadingCount += 1
    }
    
    func hideLoading() {
        callHideLoadingCount += 1
    }
    
    func display(alert: BilletErrorAlert) {
        callDisplayAlertCount += 1
        self.errorAlert = alert
    }
}

private final class BilletFormCoordinatorSpy: BilletFormCoordinating {
    var viewController: UIViewController?
    
    private(set) var callPerformCount = 0
    private(set) var action: BilletFormAction?
    
    func perform(action: BilletFormAction) {
        callPerformCount += 1
        self.action = action
    }
}

final class BilletFormPresenterTests: XCTestCase {
    private let viewControllerSpy = BilletFormDisplayingSpy()
    private let coordinatorSpy = BilletFormCoordinatorSpy()
    private lazy var sut: BilletFormPresenting = {
        let sut = BilletFormPresenter(coordinator: coordinatorSpy)
        sut.viewController = viewControllerSpy
        return sut
    }()
    
    func testPresentFilledBarcode_WhenViewModelCallsHim_ShouldCallTheViewControllerWithTheRightValues() {
        let barcode = "8162000000078329478420159"
        
        sut.presentFilled(barcode: barcode)
        
        XCTAssertEqual(viewControllerSpy.callDisplayBarcodeCount, 1)
        XCTAssertEqual(viewControllerSpy.barcode, barcode)
    }
    
    func testPresentFilledLinecode_WhenViewModelCallsHim_ShouldCallTheViewControllerWithTheRightValues() {
        let linecode = "816200000007832947842015903194784091310761100786"
        
        sut.presentFilled(linecode: linecode, description: nil)
        
        XCTAssertEqual(viewControllerSpy.callDisplayLinecodeCount, 1)
        XCTAssertEqual(viewControllerSpy.linecode, linecode)
    }
    
    func testPresentBilletBlockMask_WhenViewModelCallsHim__ShouldCallTheViewControllerWithTheRightValues() {
        let blockMask = "81620000000-7 83294784201-5 90319478409-1 31076110078-6"
        
        sut.presentBilletBlockMask(blockMask)
        
        XCTAssertEqual(viewControllerSpy.callDisplayMaskedBlockCount, 1)
        XCTAssertEqual(viewControllerSpy.maskedBlock, blockMask)
    }
    
    func testPresentValidLinecode_ShouldCallTheViewControllerWithTheRightValues() {
        sut.presentValidLinecode()
        XCTAssertEqual(viewControllerSpy.callHideLinecodeErrorCount, 1)
    }
    
    func testPresentInvalidLinecode_ShouldCallTheViewControllerWithTheRightValues() {
        sut.presentInvalidLinecode()
        
        XCTAssertEqual(viewControllerSpy.callDisplayLinecodeErrorCount, 1)
        XCTAssertEqual(viewControllerSpy.message, Strings.BilletForm.invalidLinecodeMessage)
    }
    
    func testPresentNextActionAvailability_WhenAvailabilityIsTrue_ShouldCallTheViewControllerWithTheRightValues() {
        sut.presentNextActionAvailability(true)
        
        XCTAssertEqual(viewControllerSpy.callDisplayNextButtonAvailabilityCount, 1)
        XCTAssertTrue(viewControllerSpy.isNextButtonAvailable)
    }
    
    func testPresentNextActionAvailability_WhenAvailabilityIsFalse_ShouldCallTheViewControllerWithTheRightValues() {
        sut.presentNextActionAvailability(false)
        
        XCTAssertEqual(viewControllerSpy.callDisplayNextButtonAvailabilityCount, 1)
        XCTAssertFalse(viewControllerSpy.isNextButtonAvailable)
    }
    
    func testPresentLoading_ShouldCallViewController() {
        sut.presentLoading()
        
        XCTAssertEqual(viewControllerSpy.callDisplayLoadingCount, 1)
    }
    
    func testHideLoading_ShouldCallViewController() {
        sut.hideLoading()
        
        XCTAssertEqual(viewControllerSpy.callHideLoadingCount, 1)
    }
    
    func testPresentAlert_WhenIsAGenericAlert_ShouldCallTheViewControllerWithTheRightValues() {
        let actions = [
            BilletErrorAlert.Action(title: "ok, entendi", action: {}),
            BilletErrorAlert.Action(title: "saiba mais", action: {})
        ]
        
        let alert: BilletErrorAlert = .init(icon: .image(UIImage()),
                                            title: "random title",
                                            description: "random description",
                                            actions: actions)
        sut.present(alert: alert)
        
        XCTAssertEqual(viewControllerSpy.callDisplayAlertCount, 1)
        XCTAssertEqual(viewControllerSpy.errorAlert?.title, "random title")
        XCTAssertEqual(viewControllerSpy.errorAlert?.description, "random description")
        XCTAssertEqual(viewControllerSpy.errorAlert?.actions.count, 2)
    }
    
    func testDidNextStep_ShouldCallTheCoordinatorWithTheRightAction() {
        sut.didNextStep(action: .close)
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .close)
    }
}
