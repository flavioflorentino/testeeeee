import AnalyticsModule
import Core
import XCTest
import UI
import FeatureFlag
@testable import Billet

private final class BilletHubPresenterSpy: BilletHubPresenting {
    var viewController: BilletHubDisplaying?
    
    private(set) var callPresentModelsCount = 0
    private(set) var models: [HubModel]?
    private(set) var callDidNextStepCount = 0
    private(set) var action: BilletHubAction?
    
    func present(models: [HubModel]) {
        callPresentModelsCount += 1
        self.models = models
    }
    
    func didNextStep(action: BilletHubAction) {
        callDidNextStepCount += 1
        self.action = action
    }
    
}

private final class BilletHubInteractorTests: XCTestCase {
    private let presenterSpy = BilletHubPresenterSpy()
    private let featureManagerMock = FeatureManagerMock()
    private let analyticsSpy = AnalyticsSpy()
    private lazy var dependenciesMock = DependencyContainerMock(featureManagerMock, analyticsSpy)
    private lazy var sut = BilletHubInteractor(presenter: presenterSpy,
                                               dependencies: dependenciesMock,
                                               origin: .unknown)
    
    func testLoadInformation_ShouldCallPresenterWithTheRightModels() {
        featureManagerMock.override(key: .opsFaqHowToPayBilletUrl, with: "")
        
        sut.loadInformation()
        
        XCTAssertEqual(presenterSpy.callPresentModelsCount, 1)
        XCTAssertEqual(presenterSpy.models?.count, 2)
    }
    
    func testSelectedItem_WhenItemIsScanner_ShouldCallPresenterWithTheRightAction() {
        sut.selectedItem(at: [0, 0])
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.action, .scanner(origin: .unknown))
        XCTAssertEqual(analyticsSpy.event?.name, PaymentEvent.hubActionSelected(.scanner).event().name)
    }
    
    func testSelectedItem_WhenItemIsLinecode_ShouldCallPresenterWithTheRightAction() {
        sut.selectedItem(at: [0, 2])
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.action, .linecode(origin: .unknown))
        XCTAssertEqual(analyticsSpy.event?.name, PaymentEvent.hubActionSelected(.enterManually).event().name)
    }
    
    func testSelectedItem_WhenItemIsFaq_ShouldCallPresenterWithTheRightAction() {
        featureManagerMock.override(key: .opsFaqHowToPayBilletUrl, with: "")
        
        sut.loadInformation()
        sut.selectedItem(at: [1, 0])
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.action, .faq(url: ""))
        XCTAssertEqual(analyticsSpy.event?.name, PaymentEvent.hubActionSelected(.howToPayCard).event().name)
    }
    
    func testOpenDeeplink_WhenDeeplinkIsAccessible_ShouldCallPresenterWithTheRightAction() {
        featureManagerMock.override(key: .opsFaqHowToPayBilletUrl, with: "")
        
        sut.loadInformation()
        sut.openDeeplink(with: [1, 0])
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.action, .faq(url: ""))
        XCTAssertEqual(analyticsSpy.event?.name, PaymentEvent.hubActionSelected(.howToPayCard).event().name)
    }
    
    func testOpenDeeplink_WhenDeeplinkIsNotAccessible_ShouldNotCallPresenter() {
        sut.openDeeplink(with: [1, 0])
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 0)
        XCTAssertNil(presenterSpy.action)
    }
    
    func testOpenFaq_ShouldCallPresenter() {
        featureManagerMock.override(key: .billetHubFaqUrl, with: "")
        
        sut.openFaq()
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.action, .faq(url: ""))
        XCTAssertEqual(analyticsSpy.event?.name, PaymentEvent.hubActionSelected(.knowMore).event().name)
    }
    
    func testClose_ShouldCallPresenterWithDismissAction() {
        sut.close()
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.action, .dismiss)
    }
}
