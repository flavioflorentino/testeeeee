import UI
import XCTest
@testable import Billet

private final class BilletHubDisplayingSpy: BilletHubDisplaying {
    
    private(set) var callDisplayInfosCount = 0
    private(set) var infosCount: Int?
    
    func display(infos: [(section: String, cells: [HubCellType])]) {
        callDisplayInfosCount += 1
        infosCount = infos.count
    }
}

private final class BilletHubCoordinatorSpy: BilletHubCoordinating {
    var viewController: UIViewController?
    
    private(set) var callPerformCount = 0
    private(set) var action: BilletHubAction?
    
    func perform(action: BilletHubAction) {
        callPerformCount += 1
        self.action = action
    }
}

final class BilletHubPresenterTests: XCTestCase {
    private let viewControllerSpy = BilletHubDisplayingSpy()
    private let coordinatorSpy = BilletHubCoordinatorSpy()
    private lazy var sut: BilletHubPresenting = {
        let sut = BilletHubPresenter(coordinator: coordinatorSpy)
        sut.viewController = viewControllerSpy
        return sut
    }()
    
    func testPresent_ShouldDisplayViewControllerWithInfos() {
        let models = [HubModelMock.options, HubModelMock.banners]
        sut.present(models: models)
        
        XCTAssertEqual(viewControllerSpy.callDisplayInfosCount, 1)
        XCTAssertEqual(viewControllerSpy.infosCount, models.count)
    }
    
    func testDidNextStep_WhenItemIsFaq_ShouldCallPerform() {
        sut.didNextStep(action: .faq(url: ""))
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .faq(url: ""))
    }
}
