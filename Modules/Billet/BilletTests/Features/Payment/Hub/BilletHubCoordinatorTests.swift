@testable import Billet
import Core
import UI
import XCTest

final class BilletHubCoordinatorTests: XCTestCase {
    private let dependenciesMock = DependencyContainerMock()
    private let viewControllerSpy = ViewControllerMock()
    private lazy var navControllerSpy = NavigationControllerMock(rootViewController: viewControllerSpy)
    private lazy var sut: BilletHubCoordinator = {
        let coordinator = BilletHubCoordinator(dependencies: dependenciesMock)
        coordinator.viewController = navControllerSpy.topViewController
        return coordinator
    }()
    
    func testPerform_WhenActionIsLinecode_ShouldOpenBilletFormScene() {
        sut.perform(action: .linecode(origin: .unknown))
        XCTAssertTrue(navControllerSpy.pushedViewController is BilletFormViewController)
    }
    
    func testPerform_WhenActionIsScanner_ShouldOpenBilletScannerScene() {
        sut.perform(action: .scanner(origin: .unknown))
        XCTAssertTrue(navControllerSpy.pushedViewController is BilletScannerViewController)
    }
    
    func testPerform_WhenActionIsFaq_ShouldOpenDeeplink() throws {
        let url = "https://meajuda.picpay.com/"
        sut.perform(action: .faq(url: url))
        
        let webViewSpy = try XCTUnwrap(dependenciesMock.legacy.webview as? WebViewSpy)
        XCTAssertEqual(webViewSpy.callCreateWebviewCount, 1)
        XCTAssertEqual(viewControllerSpy.didPresentControllerCount, 1)
    }
}
