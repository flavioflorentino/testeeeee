import XCTest
@testable import Billet

final class BilletHubFactoryTests: XCTestCase {
    func testMake_ShouldReturnTheRightViewController() {
        let viewController = BilletHubFactory.make(with: .feed)
        
        XCTAssertTrue(viewController is BilletHubViewController)
    }
}
