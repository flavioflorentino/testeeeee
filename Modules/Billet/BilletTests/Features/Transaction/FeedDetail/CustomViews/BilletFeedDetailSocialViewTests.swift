import XCTest
@testable import Billet

final class BilletFeedDetailSocialViewTests: XCTestCase {
    private lazy var sut = BilletFeedDetailSocialView()
    
    func testSetup_WhenValueIsntHidden_ShouldPresentTheRightValues() throws {
        let presenter = BilletFeedDetailSocialViewPresenter(
            isValueHidden: false,
            value: "<font color='#ED1846'><b>R$ 5,00</b></font>",
            date: "2 dias atrás",
            isPublic: true,
            liked: false,
            likeText: "Curtir"
        )
    
        sut.setup(with: presenter)
        
        XCTAssertFalse(sut.valueLabel.isHidden)
        XCTAssertEqual(sut.valueLabel.attributedText, presenter.value)
        XCTAssertEqual(sut.dateLabel.text, presenter.date)
        XCTAssertEqual(sut.likeTextLabel.text, presenter.likeText)
    }
    
    func testSetup_WhenValueIsHidden_ShouldPresentTheRightValues() throws {
        let presenter = BilletFeedDetailSocialViewPresenter(
            isValueHidden: true,
            value: "<font color='#ED1846'><b>R$ 5,00</b></font>",
            date: "2 dias atrás",
            isPublic: true,
            liked: false,
            likeText: "Curtir"
        )
    
        sut.setup(with: presenter)
        
        XCTAssertTrue(sut.valueLabel.isHidden)
        XCTAssertNil(sut.valueLabel.text)
        XCTAssertEqual(sut.dateLabel.text, presenter.date)
        XCTAssertEqual(sut.likeTextLabel.text, presenter.likeText)
    }
    
    func testUpdateLikeInfo_ShouldUpdateTheRightValues() {
        sut.updateLikeInfo(withLikeText: "Você curtiu", and: true)
        
        XCTAssertEqual(sut.likeTextLabel.text, "Você curtiu")
    }
}
