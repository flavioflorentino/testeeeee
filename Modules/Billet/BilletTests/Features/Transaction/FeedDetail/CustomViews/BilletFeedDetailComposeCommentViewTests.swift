import XCTest
@testable import Billet

private final class BilletFeedDetailComposeCommentViewDelegateSpy: BilletFeedDetailComposeCommentViewDelegate {
    private(set) var callSendCommentCount = 0
    private(set) var comment = ""

    func sendComment(_ comment: String) {
        callSendCommentCount += 1
        self.comment = comment
    }
}

final class BilletFeedDetailComposeCommentViewTests: XCTestCase {
    private let delegate = BilletFeedDetailComposeCommentViewDelegateSpy()
    private lazy var sut = BilletFeedDetailComposeCommentView(delegate: delegate)
    
    
    func testSendComment_ShouldCallTheDelegate() {
        sut.commentTextField.text = "Random comment"
        
        sut.sendComment()
        
        XCTAssertEqual(delegate.callSendCommentCount, 1)
        XCTAssertEqual(delegate.comment, "Random comment")
    }
}
