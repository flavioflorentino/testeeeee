import XCTest
import Core
@testable import Billet

final class BilletFeedDetailMessageViewTests: XCTestCase {
    private lazy var sut = BilletFeedDetailMessageView()
    
    func testSetup_WhenAccelerateAnalysisIsntEnabled_ShouldPresentTheRightValues() throws {
        let actions = [
            TransactionActionMock.helpCenter,
            TransactionActionMock.cancelPayment
        ]
        let presenter = BilletFeedDetailMessageViewPresenter(date: "10 Jan 2020", description: "Message", actions: actions)
        
        sut.setup(with: presenter)
        
        XCTAssertEqual(sut.dateLabel.text, presenter.date)
        XCTAssertEqual(sut.descriptionLabel.attributedText, presenter.description)
        
        let linkAction = try XCTUnwrap(presenter.actions[safe: 0])
        XCTAssertEqual(sut.linkButton.titleLabel?.text, linkAction.title)
        XCTAssertFalse(sut.linkButton.isHidden)
        
        XCTAssertTrue(sut.primaryButton.isHidden)
        
        let secondaryAction = try XCTUnwrap(presenter.actions[safe: 1])
        XCTAssertEqual(sut.secondaryButton.titleLabel?.text, secondaryAction.title)
        XCTAssertFalse(sut.secondaryButton.isHidden)
    }
    
    func testSetup_WhenAccelerateAnalysisIsEnabled_ShouldPresentTheRightValues() throws {
        let actions = [
            TransactionActionMock.helpCenter,
            TransactionActionMock.accelerateAnalysis,
            TransactionActionMock.cancelPayment
        ]
        let presenter = BilletFeedDetailMessageViewPresenter(date: "10 Jan 2020", description: "Message", actions: actions)
        
        sut.setup(with: presenter)
        
        XCTAssertEqual(sut.dateLabel.text, presenter.date)
        XCTAssertEqual(sut.descriptionLabel.attributedText, presenter.description)
        
        let linkAction = try XCTUnwrap(presenter.actions[safe: 0])
        XCTAssertEqual(sut.linkButton.titleLabel?.text, linkAction.title)
        XCTAssertFalse(sut.linkButton.isHidden)
        
        let primaryAction = try XCTUnwrap(presenter.actions[safe: 1])
        XCTAssertEqual(sut.primaryButton.titleLabel?.text, primaryAction.title)
        XCTAssertFalse(sut.primaryButton.isHidden)
        
        let secondaryAction = try XCTUnwrap(presenter.actions[safe: 2])
        XCTAssertEqual(sut.secondaryButton.titleLabel?.text, secondaryAction.title)
        XCTAssertFalse(sut.secondaryButton.isHidden)
    }
}
