import XCTest
@testable import Billet

final class BilletDetailTimelineViewTests: XCTestCase {
    private lazy var sut = BilletDetailTimelineView()
    
    func testSetup_ShouldPresentTheRightValues() {
        let statusList = [
            BilletFeedDetailTimelineCellPresenter(status: "Aguardando pagamento",
                                                  isCurrentStatus: false,
                                                  isCompleted: true,
                                                  isNextStatusCompleted: false,
                                                  isFirstStatus: true,
                                                  isLastStatus: false),
            BilletFeedDetailTimelineCellPresenter(status: "Em processamento",
                                                  isCurrentStatus: true,
                                                  isCompleted: false,
                                                  isNextStatusCompleted: false,
                                                  isFirstStatus: false,
                                                  isLastStatus: false),
            BilletFeedDetailTimelineCellPresenter(status: "Análise realizada",
                                                  isCurrentStatus: false,
                                                  isCompleted: false,
                                                  isNextStatusCompleted: false,
                                                  isFirstStatus: false,
                                                  isLastStatus: false),
            BilletFeedDetailTimelineCellPresenter(status: "Pagamento aprovado",
                                                  isCurrentStatus: false,
                                                  isCompleted: false,
                                                  isNextStatusCompleted: false,
                                                  isFirstStatus: false,
                                                  isLastStatus: true)
        ]
        sut.setup(statusList: statusList)
        
        XCTAssertEqual(sut.collectionView.numberOfItems(inSection: 0), statusList.count)
    }
}
