import XCTest
@testable import Billet

private final class BilletFeedDetailHeaderViewDelegateSpy: BilletFeedDetailHeaderViewDelegate {
    private(set) var callDidPressFollowCount = 0
    private(set) var callDidPressMessageButtonCount = 0
    private(set) var callDidPressLikeCount = 0
    private(set) var callDidPressLikedListCount = 0
    
    func didPressFollow() {
        callDidPressFollowCount += 1
    }
    
    func didPressMessageButton(action: TransactionAction) {
        callDidPressMessageButtonCount += 1
    }
    
    func didPressLike(liked: Bool) {
        callDidPressLikeCount += 1
    }
    
    func didPressLikedList() {
        callDidPressLikedListCount += 1
    }
}

final class BilletFeedDetailHeaderViewTests: XCTestCase {
    private let delegate = BilletFeedDetailHeaderViewDelegateSpy()
    private lazy var sut = BilletFeedDetailHeaderView(delegate: delegate)
    
    @discardableResult
    private func setupHeaderPresenter(with headerType: BilletFeedDetailHeaderType, isOwner: Bool) -> BilletFeedDetailHeaderPresenter {
        let presenter = BilletFeedDetailHeaderPresenter(
            headerType: headerType,
            title: "Pagamento de boleto",
            status: "Realizado",
            isOwner: isOwner,
            imageUrl: nil,
            value: "<font color='#ED1846'><b>R$ 5,00</b></font>",
            date: "2 dias atrás",
            isPublic: true,
            liked: false,
            likeText: "Curtir"
        )
        
        sut.setup(with: presenter)
        
        return presenter
    }
    
    func testSetup_WhenTimelineIsTheHeaderType_ShouldPresentTheRightValues() {
        let statusList = [
            BilletFeedDetailTimelineCellPresenter(status: "Realizado",
                                                  isCurrentStatus: true,
                                                  isCompleted: true,
                                                  isNextStatusCompleted: false,
                                                  isFirstStatus: false,
                                                  isLastStatus: true)
        ]
        let presenter = setupHeaderPresenter(with: .timeline(statusList: statusList), isOwner: true)
        
        XCTAssertEqual(sut.titleLabel.text, presenter.title)
        XCTAssertEqual(sut.statusLabel.text, presenter.status)
        XCTAssertFalse(sut.timelineView.isHidden)
        XCTAssertTrue(sut.messageView.isHidden)
    }
    
    func testSetup_WhenMessageIsTheHeaderType_ShouldPresentTheRightValues() {
        let messageViewPresenter = BilletFeedDetailMessageViewPresenter(date: "10 Jan 2020", description: "Message")
        let presenter = setupHeaderPresenter(with: .analyze(messageViewPresenter: messageViewPresenter), isOwner: true)
        
        XCTAssertEqual(sut.titleLabel.text, presenter.title)
        XCTAssertEqual(sut.statusLabel.text, presenter.status)
        XCTAssertTrue(sut.timelineView.isHidden)
        XCTAssertFalse(sut.messageView.isHidden)
    }
    
    func testSetup_WhenUserIsntTheOwnerOfBilletAndTimelineIsTheHeaderType_ShouldPresentTheRightValues() {
        let statusList = [
            BilletFeedDetailTimelineCellPresenter(status: "Realizado",
                                                  isCurrentStatus: true,
                                                  isCompleted: true,
                                                  isNextStatusCompleted: false,
                                                  isFirstStatus: false,
                                                  isLastStatus: true)
        ]
        let presenter = setupHeaderPresenter(with: .timeline(statusList: statusList), isOwner: false)
        
        XCTAssertEqual(sut.titleLabel.text, presenter.title)
        XCTAssertEqual(sut.statusLabel.text, "")
        XCTAssertTrue(sut.timelineView.isHidden)
        XCTAssertTrue(sut.messageView.isHidden)
    }
    
    func testSetup_WhenUserIsntTheOwnerOfBilletAndMessageIsTheHeaderType_ShouldPresentTheRightValues() {
        let messageViewPresenter = BilletFeedDetailMessageViewPresenter(date: "10 Jan 2020", description: "Message")
        let presenter = setupHeaderPresenter(with: .analyze(messageViewPresenter: messageViewPresenter), isOwner: false)
        
        XCTAssertEqual(sut.titleLabel.text, presenter.title)
        XCTAssertEqual(sut.statusLabel.text, "")
        XCTAssertTrue(sut.timelineView.isHidden)
        XCTAssertTrue(sut.messageView.isHidden)
    }
    
    func testFollowHandler_WhenFollowButtonWasTapped_ShouldCallDelegate() {
        let statusList = [
            BilletFeedDetailTimelineCellPresenter(status: "Realizado",
                                                  isCurrentStatus: true,
                                                  isCompleted: true,
                                                  isNextStatusCompleted: false,
                                                  isFirstStatus: false,
                                                  isLastStatus: true)
        ]
        setupHeaderPresenter(with: .timeline(statusList: statusList), isOwner: true)
        
        sut.timelineView.followHandler?()
        
        XCTAssertEqual(delegate.callDidPressFollowCount, 1)
    }
    
    func testMessageButtonHandler_WhenMessageButtonWasTapped_ShouldCallDelegate() {
        let messageViewPresenter = BilletFeedDetailMessageViewPresenter(date: "10 Jan 2020", description: "Message")
        setupHeaderPresenter(with: .analyze(messageViewPresenter: messageViewPresenter), isOwner: true)
        
        sut.messageView.buttonHandler?(TransactionActionMock.helpCenter)
        
        XCTAssertEqual(delegate.callDidPressMessageButtonCount, 1)
    }
    
    func testLikeHandler_WhenLikeButtonWasTapped_ShouldCallDelegate() {
        let statusList = [
            BilletFeedDetailTimelineCellPresenter(status: "Realizado",
                                                  isCurrentStatus: true,
                                                  isCompleted: true,
                                                  isNextStatusCompleted: false,
                                                  isFirstStatus: false,
                                                  isLastStatus: true)
        ]
        setupHeaderPresenter(with: .timeline(statusList: statusList), isOwner: true)
        
        sut.socialView.likeHandler?(true)
        
        XCTAssertEqual(delegate.callDidPressLikeCount, 1)
    }
    
    func testLikedListHandler_WhenLikedListWasTapped_ShouldCallDelegate() {
        let statusList = [
            BilletFeedDetailTimelineCellPresenter(status: "Realizado",
                                                  isCurrentStatus: true,
                                                  isCompleted: true,
                                                  isNextStatusCompleted: false,
                                                  isFirstStatus: false,
                                                  isLastStatus: true)
        ]
        setupHeaderPresenter(with: .timeline(statusList: statusList), isOwner: true)
        
        sut.socialView.likedListHandler?()
        
        XCTAssertEqual(delegate.callDidPressLikedListCount, 1)
    }
}
