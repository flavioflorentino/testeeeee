import UI
import Core
import XCTest
@testable import Billet

private final class DeeplinkWrapperSpy: DeeplinkContract {
    private(set) var callOpenDeeplinkCount = 0
    
    func open(url: URL) -> Bool {
        callOpenDeeplinkCount += 1
        return true
    }
}

private final class ContactListWrapperSpy: ContactListContract {
    private(set) var callOpenContactListCount = 0
    
    func open(withFeedId id: String) {
        callOpenContactListCount += 1
    }
}

private final class ProfileWrapperSpy: ProfileContract {
    private(set) var callOpenProfileCount = 0
    
    func open(withId id: Int, image: String, isPro: Bool, username: String) {
        callOpenProfileCount += 1
    }
}

private final class ReceiptWrapperSpy: ReceiptContract {
    private(set) var callOpenReceiptCount = 0
    
    func open(withId id: String, receiptType: String, feedItemId: String?) {
        callOpenReceiptCount += 1
    }
}

final class BilletFeedDetailCoordinatorTests: XCTestCase {
    private let navController = NavigationControllerMock(rootViewController: ViewControllerMock())
    private lazy var sut: BilletFeedDetailCoordinating = {
        let coordinator = BilletFeedDetailCoordinator()
        coordinator.viewController = navController.topViewController
        return coordinator
    }()
    
    func testPerform_WhenActionIsTimeline_ShouldOpenTheTimelinesScene() {
        sut.perform(action: .timeline(billetInfo: BilletInfoDetailMock.completed))
        
        XCTAssertTrue(navController.pushedViewController is BilletTimelineViewController)
    }
    
    func testPerform_WhenActionIsHelpCenter_ShouldOpenTheHelpCentersWebView() {
        let deeplinkWrapperSpy = DeeplinkWrapperSpy()
        BilletSetup.shared.inject(instance: deeplinkWrapperSpy)
        
        let url = "https://app.picpay.com/helpcenter?query=pagamento-de-conta-boletos"
        sut.perform(action: .helpCenter(url: url))
        
        XCTAssertEqual(deeplinkWrapperSpy.callOpenDeeplinkCount, 1)
    }
    
    func testPerform_WhenActionIsLikedList_ShouldOpenTheLikedListScene() {
        let contactListWrapperSpy = ContactListWrapperSpy()
        BilletSetup.shared.inject(instance: contactListWrapperSpy)
        
        sut.perform(action: .likedList(feedId: "0"))
        
        XCTAssertEqual(contactListWrapperSpy.callOpenContactListCount, 1)
    }
    
    func testPerform_WhenActionIsProfile_ShouldOpenTheProfilesScene() {
        let profileWrapperSpy = ProfileWrapperSpy()
        BilletSetup.shared.inject(instance: profileWrapperSpy)
        
        sut.perform(action: .profile(profile: ProfileResponseMock.generic))
        
        XCTAssertEqual(profileWrapperSpy.callOpenProfileCount, 1)
    }
    
    func testPerform_WhenActionIsOptions_ShouldOpenTheRightAlertView() throws {
        let alert = UIAlertController(title: "Random title", message: "Random message", preferredStyle: .alert)
        
        sut.perform(action: .optionsAlert(alert: alert))
        
        let topController = try XCTUnwrap(navController.topViewController as? ViewControllerMock)
        XCTAssertNotNil(topController.viewControllerPresented is UIAlertController)
    }
    
    func testPerform_WhenActionIsPopup_ShouldOpenTheRightPopup() throws {
        let alert = PopupViewController(title: "Random title", description: "Random message", preferredType: .none)
        
        sut.perform(action: .popup(alert))
        
        let topController = try XCTUnwrap(navController.topViewController as? ViewControllerMock)
        XCTAssertNotNil(topController.viewControllerPresented is PopupViewController)
    }
    
    func testPerform_WhenActionIsReceipt_ShouldOpenTheReceiptsScene() {
        let receiptWrapperSpy = ReceiptWrapperSpy()
        BilletSetup.shared.inject(instance: receiptWrapperSpy)
        
        sut.perform(action: .receipt(id: "0"))
        
        XCTAssertEqual(receiptWrapperSpy.callOpenReceiptCount, 1)
    }
}
