import XCTest
import Core
import AnalyticsModule
@testable import Billet

private final class BilletFeedDetailPresenterSpy: BilletFeedDetailPresenting {
    var viewController: BilletFeedDetailDisplay?
    
    private(set) var callHandleStartLoadingCount = 0
    private(set) var callHandleStopLoadingCount = 0
    private(set) var callPresentOptionsCount = 0
    private(set) var callHandleBilletInfoDetailCount = 0
    private(set) var headerInfo: BilletFeedDetailHeaderPresenting?
    private(set) var callHandleTryAgainCount = 0
    private(set) var callHandleCommentsCount = 0
    private(set) var commentsInfo: (comments: [BilletFeedDetailCommentCellPresenting], isNewComment: Bool)?
    private(set) var callHandleLikeCount = 0
    private(set) var likeInfo: (likeText: String, isChecked: Bool)?
    private(set) var callHandlePrivacyCount = 0
    private(set) var isPrivate: Bool?
    private(set) var callDidNextStepCount = 0
    
    func handleStartLoading() {
        callHandleStartLoadingCount += 1
    }
    
    func handleStopLoading() {
        callHandleStopLoadingCount += 1
    }
    
    func presentOptions() {
        callPresentOptionsCount += 1
    }
    
    func handleBilletInfoDetail(_ headerInfo: BilletFeedDetailHeaderPresenting) {
        callHandleBilletInfoDetailCount += 1
        self.headerInfo = headerInfo
    }
    
    func handleTryAgain() {
        callHandleTryAgainCount += 1
    }
    
    func handleComments(_ comments: [BilletFeedDetailCommentCellPresenting], isNewComment: Bool) {
        callHandleCommentsCount += 1
        self.commentsInfo = (comments: comments, isNewComment: isNewComment)
    }
    
    func handleLike(_ likeText: String, isChecked: Bool) {
        callHandleLikeCount += 1
        self.likeInfo = (likeText: likeText, isChecked: isChecked)
    }
    
    func handlePrivacy(isPrivate: Bool) {
        callHandlePrivacyCount += 1
        self.isPrivate = isPrivate
    }
    
    func didNextStep(action: BilletFeedDetailAction) {
        callDidNextStepCount += 1
    }
}

private final class BilletFeedDetailServiceMock: BilletFeedDetailServicing {
    var statusHistoryExpectedResult: Result<BilletInfoDetail, ApiError> = .failure(.serverError)
    var cancelPaymentExpectedResult: Result<NoContent, ApiError> = .failure(.serverError)
    var commentsExpectedResult: Result<CommentsResponse, ApiError> = .failure(.serverError)
    var addCommentExpectedResult: Result<CommentsResponse, ApiError> = .failure(.serverError)
    var likeExpectedResult: Result<LikeResponse, ApiError> = .failure(.serverError)
    var makePrivateExpectedResult: Result<NoContent, ApiError> = .failure(.serverError)
    var genericBehaviorExpectedResult: Result<GenericBehaviorResponse, ApiError> = .failure(.serverError)
    
    func getStatusHistory(withTransactionId transactionId: String, andExternalId externalId: String, _ completion: @escaping StatusHistoryCompletionBlock) {
        completion(statusHistoryExpectedResult)
    }
    
    func cancelPayment(withBillId billId: String, type: TransactionActionType, _ completion: @escaping CancelPaymentCompletionBlock) {
        completion(cancelPaymentExpectedResult)
    }
    
    func getComments(withFeedId feedId: String, _ completion: @escaping CommentsCompletionBlock) {
        completion(commentsExpectedResult)
    }
    
    func addComment(withComment comment: String, andFeedId feedId: String, _ completion: @escaping CommentsCompletionBlock) {
        completion(addCommentExpectedResult)
    }
    
    func like(with liked: Bool, andFeedId feedId: String, _ completion: @escaping LikeCompletionBlock) {
        completion(likeExpectedResult)
    }
    
    func makePrivate(withFeedId feedId: String, _ completion: @escaping ChangePrivacyCompletionBlock) {
        completion(makePrivateExpectedResult)
    }
    
    func genericBehavior(with data: AnyCodable, _ completion: @escaping GenericBehaviorCompletionBlock) {
        completion(genericBehaviorExpectedResult)
    }
}

final class BilletFeedDetailViewModelTests: XCTestCase {
    private let presenterSpy = BilletFeedDetailPresenterSpy()
    private let serviceMock = BilletFeedDetailServiceMock()
    private let analytics = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(analytics)
    
    private lazy var sut: BilletFeedDetailViewModelInputs = {
        return BilletFeedDetailViewModel(
            service: serviceMock,
            presenter: presenterSpy,
            dependencies: dependencies,
            transactionId: "1234",
            externalId: "bill-1234"
        )
    }()
    
    func testLoadBilletInfo_WhenServiceReturnSuccessAndIsATimeline_ShouldPresentTheRightValues() {
        serviceMock.statusHistoryExpectedResult = .success(BilletInfoDetailMock.completed)
        
        sut.loadBilletInfo()
        
        XCTAssertEqual(presenterSpy.callHandleBilletInfoDetailCount, 1)
        XCTAssertEqual(presenterSpy.headerInfo?.status, "Pagamento Realizado")
    }
    
    func testLoadBilletInfo_WhenServiceReturnSuccessAndIsInAnalysis_ShouldPresentTheRightValues() {
        serviceMock.statusHistoryExpectedResult = .success(BilletInfoDetailMock.inAnalysis)

        sut.loadBilletInfo()

        XCTAssertEqual(presenterSpy.callHandleBilletInfoDetailCount, 1)
        XCTAssertEqual(presenterSpy.headerInfo?.status, "Em análise")
    }

    func testLoadBilletInfo_WhenServiceReturnSuccessAndIsRefused_ShouldPresentTheRightValues() {
        serviceMock.statusHistoryExpectedResult = .success(BilletInfoDetailMock.refused)

        sut.loadBilletInfo()

        XCTAssertEqual(presenterSpy.callHandleBilletInfoDetailCount, 1)
        XCTAssertEqual(presenterSpy.headerInfo?.status, "Não Realizado")
    }
    
    func testLoadBilletInfo_WhenServiceReturnSuccessAndActionsArrayIsFilled_ShouldPresentTheOptions() {
        serviceMock.statusHistoryExpectedResult = .success(BilletInfoDetailMock.completed)
        
        sut.loadBilletInfo()
        
        XCTAssertEqual(presenterSpy.callPresentOptionsCount, 1)
    }
    
    func testLoadBilletInfo_WhenServiceReturnSuccessAndActionsArrayIsEmpty_ShouldPresentTheOptions() {
        serviceMock.statusHistoryExpectedResult = .success(BilletInfoDetailMock.emptyActions)
        
        sut.loadBilletInfo()
        
        XCTAssertEqual(presenterSpy.callPresentOptionsCount, 0)
    }
    
    func testLoadBilletInfo_WhenServiceReturnFailure_ShouldPresentTryAgain() {
        serviceMock.statusHistoryExpectedResult = .failure(.serverError)

        sut.loadBilletInfo()

        XCTAssertEqual(presenterSpy.callHandleTryAgainCount, 1)
    }
    
    func testCancelPayment_WhenServiceReturnFailure_ShouldPresentError() throws {
        serviceMock.cancelPaymentExpectedResult = .failure(.serverError)

        sut.cancelPayment(withId: "123", type: .cancel)
        
        XCTAssertEqual(presenterSpy.callHandleStopLoadingCount, 1)
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
    }
    
    func testLoadComments_WhenServiceReturnSuccess_ShouldHaveTheRightValues() throws {
        serviceMock.statusHistoryExpectedResult = .success(BilletInfoDetailMock.completed)
        serviceMock.commentsExpectedResult = .success(CommentsResponseMock.generic)

        sut.loadBilletInfo()
        sut.loadComments()
        
        XCTAssertEqual(presenterSpy.callHandleCommentsCount, 1)
        let commentsInfo = try XCTUnwrap(presenterSpy.commentsInfo)
        XCTAssertGreaterThan(commentsInfo.comments.count, 0)
        XCTAssertFalse(commentsInfo.isNewComment)
    }
    
    func testAddComment_WhenServiceReturnSuccess_ShouldHaveTheRightValues() throws {
        serviceMock.statusHistoryExpectedResult = .success(BilletInfoDetailMock.completed)
        serviceMock.addCommentExpectedResult = .success(CommentsResponseMock.generic)

        sut.loadBilletInfo()
        sut.addComment("Random comment")
        
        XCTAssertEqual(presenterSpy.callHandleCommentsCount, 1)
        let commentsInfo = try XCTUnwrap(presenterSpy.commentsInfo)
        XCTAssertGreaterThan(commentsInfo.comments.count, 0)
        XCTAssertTrue(commentsInfo.isNewComment)
    }
    
    func testLike_WhenServiceReturnSuccess_ShouldHaveTheRightValues() throws {
        serviceMock.statusHistoryExpectedResult = .success(BilletInfoDetailMock.completed)
        serviceMock.likeExpectedResult = .success(LikeResponseMock.checked)

        sut.loadBilletInfo()
        sut.like(true)
        
        XCTAssertEqual(presenterSpy.callHandleLikeCount, 1)
        let likeInfo = try XCTUnwrap(presenterSpy.likeInfo)
        XCTAssertEqual(likeInfo.likeText, "Você curtiu")
        XCTAssertTrue(likeInfo.isChecked)
    }
    
    func testMakePrivate_WhenServiceReturnSuccess_ShouldHaveTheRightValues() throws {
        serviceMock.statusHistoryExpectedResult = .success(BilletInfoDetailMock.completed)
        serviceMock.makePrivateExpectedResult = .success(NoContent())

        sut.loadBilletInfo()
        sut.makePrivate()
        
        XCTAssertEqual(presenterSpy.callHandlePrivacyCount, 1)
        let isPrivate = try XCTUnwrap(presenterSpy.isPrivate)
        XCTAssertTrue(isPrivate)
    }
    
    func testGenericBehavior_WhenServiceReturnSuccess_ShouldHaveTheRightValues() {
        serviceMock.genericBehaviorExpectedResult = .success(GenericBehaviorResponseMock.generic)

        let transactionAction = TransactionActionMock.disableNotification
        sut.genericBehavior(with: transactionAction)
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
    }
    
    func testGenerateOptions_ShouldTryOpenTheAlert() {
        serviceMock.statusHistoryExpectedResult = .success(BilletInfoDetailMock.completed)
        
        sut.loadBilletInfo()
        sut.generateOptions()
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
    }
    
    func testOpenTimeline_ShouldTryOpenTheTimeline() {
        serviceMock.statusHistoryExpectedResult = .success(BilletInfoDetailMock.completed)
        
        sut.loadBilletInfo()
        sut.openTimeline()
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
    }
    
    func testPerformMessageAction_WhenActionIsCancelPayment_ShouldTryOpenPopup() {
        serviceMock.statusHistoryExpectedResult = .success(BilletInfoDetailMock.completed)
        
        sut.loadBilletInfo()
        sut.performMessageAction(TransactionActionMock.cancelPayment)
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
    }
    
    func testPerformMessageAction_WhenActionIsAccelerateAnalysis_ShouldTryOpenTheAccelerateAnalysis() {
        sut.performMessageAction(TransactionActionMock.accelerateAnalysis)
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
    }
    
    func testPerformMessageAction_WhenActionIsHelpCenter_ShouldTryOpenTheHelpCenter() {
        sut.performMessageAction(TransactionActionMock.helpCenter)
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
    }
    
    func testPerformMessageAction_WhenActionHasntUrl_ShouldntOpenAnyView() {
        sut.performMessageAction(TransactionActionMock.receipt)
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 0)
    }
    
    func testOpenLikedList_ShouldTryOpenLikedList() {
        serviceMock.statusHistoryExpectedResult = .success(BilletInfoDetailMock.completed)
        
        sut.loadBilletInfo()
        sut.openLikedList()
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
    }
    
    func testOpenProfile_ShouldTryOpenProfile() {
        serviceMock.statusHistoryExpectedResult = .success(BilletInfoDetailMock.completed)
        serviceMock.commentsExpectedResult = .success(CommentsResponseMock.generic)
        
        sut.loadBilletInfo()
        sut.loadComments()
        sut.openProfile(withIndex: 0)
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
    }
}
