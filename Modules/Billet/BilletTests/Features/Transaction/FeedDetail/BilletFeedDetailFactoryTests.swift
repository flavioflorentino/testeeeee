import XCTest
@testable import Billet

final class BilletFeedDetailFactoryTests: XCTestCase {
    func testMake_ShouldReturnTheRightViewController() {
        let viewController = BilletFeedDetailFactory.make(transactionId: "1234", externalId: "1234")
        
        XCTAssertTrue(viewController is BilletFeedDetailViewController)
    }
}
