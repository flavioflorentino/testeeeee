import XCTest
@testable import Billet

private final class BilletFeedDetailDisplaySpy: BilletFeedDetailDisplay {
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    private(set) var callDisplayOptionsCount = 0
    private(set) var callDisplayHeaderInfoCount = 0
    private(set) var headerInfo: BilletFeedDetailHeaderPresenting?
    private(set) var callDisplayTryAgainCount = 0
    private(set) var callDisplayCommentsCount = 0
    private(set) var commentsInfo: (cells: [BilletFeedDetailCommentCellPresenting], isNewComment: Bool)?
    private(set) var callDisplayLikeCount = 0
    private(set) var likeInfo: (likeText: String, isChecked: Bool)?
    private(set) var callUpdatePrivacyCount = 0
    private(set) var isPrivate: Bool?
    
    func startLoading() {
        callStartLoadingCount += 1
    }
    
    func stopLoading() {
        callStopLoadingCount += 1
    }
    
    func displayOptions() {
        callDisplayOptionsCount += 1
    }
    
    func displayHeaderInfo(_ headerInfo: BilletFeedDetailHeaderPresenting) {
        callDisplayHeaderInfoCount += 1
        self.headerInfo = headerInfo
    }
    
    func displayTryAgain() {
        callDisplayTryAgainCount += 1
    }
    
    func displayComments(_ cells: [BilletFeedDetailCommentCellPresenting], isNewComment: Bool) {
        callDisplayCommentsCount += 1
        commentsInfo = (cells: cells, isNewComment: isNewComment)
    }
    
    func displayLike(_ likeText: String, isChecked: Bool) {
        callDisplayLikeCount += 1
        likeInfo = (likeText: likeText, isChecked: isChecked)
    }
    
    func updatePrivacy(isPrivate: Bool) {
        callUpdatePrivacyCount += 1
        self.isPrivate = isPrivate
    }
}

private final class BilletFeedDetailCoordinatorSpy: BilletFeedDetailCoordinating {
    var viewController: UIViewController?
    
    private(set) var callPerformCount = 0
    private(set) var action: BilletFeedDetailAction?
    
    func perform(action: BilletFeedDetailAction) {
        callPerformCount += 1
        self.action = action
    }
}

final class BilletFeedDetailPresenterTests: XCTestCase {
    private let controllerSpy = BilletFeedDetailDisplaySpy()
    private let coordinatorSpy = BilletFeedDetailCoordinatorSpy()
    
    private lazy var sut: BilletFeedDetailPresenting = {
        let presenter = BilletFeedDetailPresenter(coordinator: coordinatorSpy)
        presenter.viewController = controllerSpy
        return presenter
    }()
    
    func testHandleBilletInfoDetail_ShouldDisplayTheRightValues() {
        let statusList = [
            BilletFeedDetailTimelineCellPresenter(status: "Pagamento Solicitado",
                                                  isCurrentStatus: false,
                                                  isCompleted: true,
                                                  isNextStatusCompleted: false,
                                                  isFirstStatus: true,
                                                  isLastStatus: false),
            BilletFeedDetailTimelineCellPresenter(status: "Processando",
                                                  isCurrentStatus: true,
                                                  isCompleted: false,
                                                  isNextStatusCompleted: false,
                                                  isFirstStatus: false,
                                                  isLastStatus: false),
            BilletFeedDetailTimelineCellPresenter(status: "Aguardando Realização",
                                                  isCurrentStatus: false,
                                                  isCompleted: false,
                                                  isNextStatusCompleted: false,
                                                  isFirstStatus: false,
                                                  isLastStatus: true)
        ]
        let headerInfo = BilletFeedDetailHeaderPresenter(
            headerType: .timeline(statusList: statusList),
            title: "Estamos processando seu boleto",
            status: "Em Processamento",
            isOwner: true,
            imageUrl: nil,
            value: "<font color='#ED1846'><b>R$ 5,00</b></font>",
            date: "2 dias atrás",
            isPublic: true,
            liked: false,
            likeText: "Curtir"
        )
        
        sut.handleBilletInfoDetail(headerInfo)
        
        XCTAssertEqual(controllerSpy.callDisplayHeaderInfoCount, 1)
        XCTAssertEqual(controllerSpy.headerInfo?.title, headerInfo.title)
        XCTAssertEqual(controllerSpy.headerInfo?.status, headerInfo.status)
    }
    
    func testPresentOptions_ShouldDisplayTheOptions() {
        sut.presentOptions()
        
        XCTAssertEqual(controllerSpy.callDisplayOptionsCount, 1)
    }
    
    func testHandleTryAgain_ShouldDisplayTheRightView() {
        sut.handleTryAgain()
        
        XCTAssertEqual(controllerSpy.callDisplayTryAgainCount, 1)
    }
    
    func testHandleStartLoading_ShouldDisplayTheLoading() {
        sut.handleStartLoading()
        
        XCTAssertEqual(controllerSpy.callStartLoadingCount, 1)
    }
    
    func testHandleStopLoading_ShouldHideTheLoading() {
        sut.handleStopLoading()
        
        XCTAssertEqual(controllerSpy.callStopLoadingCount, 1)
    }
    
    func testHandleComments_ShouldDisplayTheRightValues() throws {
        let comments = [
            BilletFeedDetailCommentCellPresenter(
                profileImageUrl: nil,
                profileName: "User 1",
                comment: "Random comment",
                datetime: "2 horas atrás"
            ),
            BilletFeedDetailCommentCellPresenter(
                profileImageUrl: nil,
                profileName: "User 2",
                comment: "Random comment",
                datetime: "3 horas atrás"
            )
        ]
        
        sut.handleComments(comments, isNewComment: false)
        
        XCTAssertEqual(controllerSpy.callDisplayCommentsCount, 1)
        let commentsInfo = try XCTUnwrap(controllerSpy.commentsInfo)
        XCTAssertEqual(commentsInfo.cells.count, comments.count)
        XCTAssertFalse(commentsInfo.isNewComment)
    }
    
    func testHandleLike_ShouldDisplayTheRightValues() throws {
        sut.handleLike("Você curtiu", isChecked: true)
        
        XCTAssertEqual(controllerSpy.callDisplayLikeCount, 1)
        let likeInfo = try XCTUnwrap(controllerSpy.likeInfo)
        XCTAssertEqual(likeInfo.likeText, "Você curtiu")
        XCTAssertTrue(likeInfo.isChecked)
    }
    
    func testHandlePrivacy_ShouldDisplayTheRightValues() throws {
        sut.handlePrivacy(isPrivate: true)
        
        XCTAssertEqual(controllerSpy.callUpdatePrivacyCount, 1)
        let isPrivate = try XCTUnwrap(controllerSpy.isPrivate)
        XCTAssertTrue(isPrivate)
    }
    
    func testDidNextStep_ShouldDisplayTheRightValues() throws {
        sut.didNextStep(action: .receipt(id: "0"))
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssertNotNil(coordinatorSpy.action)
    }
}
