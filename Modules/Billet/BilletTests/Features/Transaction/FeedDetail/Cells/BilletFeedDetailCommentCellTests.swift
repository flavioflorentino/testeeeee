import XCTest
@testable import Billet

private final class BilletFeedDetailCommentCellDelegateSpy: BilletFeedDetailCommentCellDelegate {
    private(set) var callDidTouchProfileCount = 0

    func didTouchProfile(at index: Int) {
        callDidTouchProfileCount += 1
    }
}

final class BilletFeedDetailCommentCellTests: XCTestCase {
    private let presenter = BilletFeedDetailCommentCellPresenter(
        profileImageUrl: nil,
        profileName: "PicPay",
        comment: "Comment Test",
        datetime: "10 Jan 2020"
    )
    private let delegate = BilletFeedDetailCommentCellDelegateSpy()
    private let sut = BilletFeedDetailCommentCell()
    
    func testSetupCell_ShouldPresentTheRightValues() {
        sut.configure(with: presenter, index: 0, and: delegate)
        
        XCTAssertEqual(sut.profileNameLabel.text, presenter.profileName)
        XCTAssertEqual(sut.commentLabel.text, presenter.comment)
        XCTAssertEqual(sut.datetimeLabel.text, presenter.datetime)
    }
    
    func testTouchIconView_ShouldCallDelegate() {
        sut.configure(with: presenter, index: 0, and: delegate)
        
        GestureAutomator.tap(view: sut.iconView)
        
        XCTAssertEqual(delegate.callDidTouchProfileCount, 1)
    }
}
