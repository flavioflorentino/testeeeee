import XCTest
@testable import Billet

final class BilletFeedDetailTimelineCellTests: XCTestCase {
    private let presenter = BilletFeedDetailTimelineCellPresenter(status: "Processing",
                                                                  isCurrentStatus: true,
                                                                  isCompleted: false,
                                                                  isNextStatusCompleted: false,
                                                                  isFirstStatus: false,
                                                                  isLastStatus: false)
    private let sut = BilletFeedDetailTimelineCell()
    
    func testSetupCell_ShouldPresentTheRightValues() {
        sut.setup(with: presenter)
        
        XCTAssertEqual(sut.statusLabel.text, presenter.status)
    }
}
