import XCTest
@testable import Billet

private final class BilletTimelineFooterViewDelegateSpy: BilletTimelineFooterViewDelegate {
    private(set) var callDidTapHelpLinkCount = 0
    
    func didTapHelpLink() {
        callDidTapHelpLinkCount += 1
    }
}

final class BilletTimelineFooterViewTests: XCTestCase {
    private let delegateSpy = BilletTimelineFooterViewDelegateSpy()
    private lazy var sut = BilletTimelineFooterView(delegate: delegateSpy)
    
    func testLinkButtonTapped_ShouldCallTheRightDelegate() {
        sut.perform(Selector("linkButtonTapped"))

        
        XCTAssertEqual(delegateSpy.callDidTapHelpLinkCount, 1)
    }
}
 
