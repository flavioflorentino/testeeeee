import XCTest
@testable import Billet

final class BilletTimelineHeaderViewTests: XCTestCase {
    private let sut = BilletTimelineHeaderView()
    
    func testLinkButtonTapped_ShouldCallTheRightDelegate() {
        sut.setup(with: "Pagamento de boleto", and: "Pagamento realizado")
        
        XCTAssertEqual(sut.titleLabel.text, "Pagamento de boleto")
        XCTAssertEqual(sut.statusLabel.text, "Pagamento realizado")
    }
}
