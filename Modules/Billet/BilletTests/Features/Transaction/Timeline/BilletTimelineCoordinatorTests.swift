import Core
import XCTest
@testable import Billet

private final class ReceiptWrapperSpy: ReceiptContract {
    private(set) var callOpenReceiptCount = 0
    
    func open(withId id: String, receiptType: String, feedItemId: String?) {
        callOpenReceiptCount += 1
    }
}

private final class DeeplinkWrapperSpy: DeeplinkContract {
    private(set) var callOpenDeeplinkCount = 0
    
    func open(url: URL) -> Bool {
        callOpenDeeplinkCount += 1
        return true
    }
}

final class BilletTimelineCoordinatorTests: XCTestCase {
    private let viewController = UIViewController()
    private lazy var sut: BilletTimelineCoordinator = {
        let coordinator = BilletTimelineCoordinator()
        coordinator.viewController = viewController
        return coordinator
    }()
    
    func testPerform_WhenActionIsOfTheTypeReceipt_ShouldOpenTheReceiptsScene() {
        let receiptWrapperSpy = ReceiptWrapperSpy()
        BilletSetup.shared.inject(instance: receiptWrapperSpy)
        
        sut.perform(action: .receipt(id: "0"))
        
        XCTAssertEqual(receiptWrapperSpy.callOpenReceiptCount, 1)
    }
    
    func testPerform_WhenActionIsOfTheTypeHelpCenter_ShouldOpenTheHelpCentersWebView() {
        let deeplinkWrapperSpy = DeeplinkWrapperSpy()
        BilletSetup.shared.inject(instance: deeplinkWrapperSpy)
        
        let url = "https://app.picpay.com/helpcenter?query=pagamento-de-conta-boletos"
        sut.perform(action: .helpCenter(url: url))
        
        XCTAssertEqual(deeplinkWrapperSpy.callOpenDeeplinkCount, 1)
    }
}
