import UI
import XCTest
@testable import Billet

private final class BilletTimelineDisplaySpy: BilletTimelineDisplay {
    private(set) var callDisplayTimelineInfoCount = 0
    private(set) var timelineInfo: (title: String, statusTitle: String?, cells: [BilletTimelineCellPresenting])?

    func displayTimelineInfo(with title: String, statusTitle: String, and cells: [BilletTimelineCellPresenting]) {
        callDisplayTimelineInfoCount += 1
        timelineInfo = (title: title, statusTitle: statusTitle, cells: cells)
    }
}

private final class BilletTimelineCoordinatorSpy: BilletTimelineCoordinating {
    var viewController: UIViewController?
    
    private(set) var callPerformCount = 0
    private(set) var action: BilletTimelineAction?
    
    func perform(action: BilletTimelineAction) {
        callPerformCount += 1
        self.action = action
    }
}

final class BilletTimelinePresenterTests: XCTestCase {
    private let viewControllerSpy = BilletTimelineDisplaySpy()
    private let coordinatorSpy = BilletTimelineCoordinatorSpy()
    private lazy var sut: BilletTimelinePresenter = {
        let sut = BilletTimelinePresenter(coordinator: coordinatorSpy)
        sut.viewController = viewControllerSpy
        return sut
    }()
    
    func testPresentTimelineInfo_ShouldDisplayTheRightValues() {
        let title = "some title"
        let statusTitle = "some status title"
        
        sut.presentTimelineInfo(with: title, statusTitle: statusTitle, and: [])
        
        XCTAssertEqual(viewControllerSpy.callDisplayTimelineInfoCount, 1)
        XCTAssertEqual(viewControllerSpy.timelineInfo?.title, title)
        XCTAssertEqual(viewControllerSpy.timelineInfo?.statusTitle, statusTitle)
        XCTAssertTrue(viewControllerSpy.timelineInfo?.cells.isEmpty ?? false)
    }
    
    func testDidNextStep_WhenReceivesAnAction_ShouldRedirectToTheCoordinator() {
        sut.didNextStep(action: .receipt(id: "0"))
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssertNotNil(coordinatorSpy.action)
    }
}
