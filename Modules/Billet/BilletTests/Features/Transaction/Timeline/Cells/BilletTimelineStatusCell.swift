import XCTest
@testable import Billet

private final class BilletTimelineCellDelegateSpy: BilletTimelineCellDelegate {
    private(set) var callDidTouchActionCount = 0
    
    func didTouchAction(_ action: TransactionAction) {
        callDidTouchActionCount += 1
    }
}

final class BilletTimelineCellTests: XCTestCase {
    private lazy var presenter = BilletTimelineCellPresenter(
        title: "Pagamento realizado",
        message: "Seu pagamento foi realizado com sucesso! Você já pode acessar seu código de autenticação.",
        isCurrentStatus: true,
        isCompleted: true,
        isNextStatusCompleted: false,
        isFirstStatus: false,
        isLastStatus: false,
        date: "10 Jan 2020",
        actions: [
            TransactionActionMock.receipt,
            TransactionActionMock.helpCenter
        ]
    )
    
    func testSetupCell_WhenThePresenterHasntActions_ShouldPresentTheRightValues() {
        let delegate = BilletTimelineCellDelegateSpy()
        let sut = BilletTimelineCell()
        sut.setup(with: presenter, and:  delegate)
        
        XCTAssertEqual(sut.dateLabel.text, presenter.date)
        XCTAssertEqual(sut.titleLabel.text, presenter.title)
        XCTAssertEqual(sut.messageLabel.text, presenter.message)
    }
    
    func testTouchAction_WhenPrimaryButtonWasTapped_ShouldCallDelegate() {
        let delegate = BilletTimelineCellDelegateSpy()
        let sut = BilletTimelineCell()
        sut.setup(with: presenter, and:  delegate)

        sut.perform(Selector("primaryButtonTapped"))

        XCTAssertEqual(delegate.callDidTouchActionCount, 1)
    }
    
    func testTouchAction_WhenSecondaryButtonWasTapped_ShouldCallDelegate() {
        let delegate = BilletTimelineCellDelegateSpy()
        let sut = BilletTimelineCell()
        sut.setup(with: presenter, and:  delegate)
        
        sut.perform(Selector("secondaryButtonTapped"))
        
        XCTAssertEqual(delegate.callDidTouchActionCount, 1)
    }
}
