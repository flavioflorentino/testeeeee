import XCTest
import AnalyticsModule
@testable import Billet

private final class BilletTimelinePresenterSpy: BilletTimelinePresenting {
    var viewController: BilletTimelineDisplay?
    
    private(set) var callPresentTimelineInfoCount = 0
    private(set) var timelineInfo: (title: String, statusTitle: String?, cells: [BilletTimelineCellPresenting])?
    
    private(set) var calldidNextStepCount = 0
    
    func presentTimelineInfo(with title: String, statusTitle: String, and cells: [BilletTimelineCellPresenting]) {
        callPresentTimelineInfoCount += 1
        timelineInfo = (title: title, statusTitle: statusTitle, cells: cells)
    }
    
    func didNextStep(action: BilletTimelineAction) {
        calldidNextStepCount += 1
    }
}

final class BilletTimelineViewModelTests: XCTestCase {
    private let presenterSpy = BilletTimelinePresenterSpy()
    private let analytics = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(analytics)
    private let billetInfo = BilletInfoDetailMock.completed
    
    private lazy var sut = BilletTimelineViewModel(
        presenter: presenterSpy,
        dependencies: dependencies,
        billetInfo: billetInfo
    )
    
    func testLoadTimelineInfo_ShouldPresentTheRightValues() {
        sut.loadTimelineInfo()
        
        XCTAssertEqual(presenterSpy.callPresentTimelineInfoCount, 1)
        XCTAssertEqual(presenterSpy.timelineInfo?.title, billetInfo.title)
        XCTAssertEqual(presenterSpy.timelineInfo?.statusTitle, billetInfo.currentStatusTitle)
        XCTAssertEqual(presenterSpy.timelineInfo?.cells.count, billetInfo.timeline.count)
    }
    
    // This test will fail if the JSON (JsonMock.billetInfoDetail) first timeline status hasn't an action 
    func testPerformAction_WhenTheViewModelReceivesAnAction_ShouldRedirectToCoordinator() throws {
        let timelineStatus = try XCTUnwrap(billetInfo.timeline.first)
        let action = try XCTUnwrap(timelineStatus.actions.first)
        sut.performAction(action)
        
        XCTAssertEqual(presenterSpy.calldidNextStepCount, 1)
    }
    
    func testOpenFooterHelpLink_ShouldRedirectToCoordinator() {
        sut.openFooterHelpLink()
        
        XCTAssertEqual(presenterSpy.calldidNextStepCount, 1)
    }
}
