import XCTest
@testable import Billet

final class BilletSelectionFactoryTests: XCTestCase {
    func testMake_ShouldReturnTheRightViewController() {
        let viewController = BilletSelectionFactory.make(model: BilletSelectionModelMock.generic)
        
        XCTAssertTrue(viewController is BilletSelectionViewController)
    }
}
