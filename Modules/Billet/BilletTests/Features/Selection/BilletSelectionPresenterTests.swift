import UI
import XCTest
@testable import Billet

private final class BilletSelectionDisplayableSpy: BilletSelectionDisplayable {    
    private(set) var callPresentViewElementsCount = 0
    private(set) var items: (section: String, cells: [BilletSelectionCellPresenting])?
    private(set) var callRemoveLoadingCount = 0

    func presentViewElements(withTitle title: String, section: String, and cells: [BilletSelectionCellPresenting]) {
        callPresentViewElementsCount += 1
        self.items = (section: section, cells: cells)
    }
}

private final class BilletSelectionCoordinatorSpy: BilletSelectionCoordinating {
    var viewController: UIViewController? = nil
    
    private(set) var callPerformCount = 0
    private(set) var action: BilletSelectionAction?
    
    func perform(action: BilletSelectionAction) {
        callPerformCount += 1
        self.action = action
    }
}

final class BilletSelectionPresenterTests: XCTestCase {
    private let viewControllerSpy = BilletSelectionDisplayableSpy()
    private let coordinatorSpy = BilletSelectionCoordinatorSpy()
    private lazy var sut: BilletSelectionPresenter = {
        let sut = BilletSelectionPresenter(coordinator: coordinatorSpy)
        sut.viewController = viewControllerSpy
        return sut
    }()
    
    func testHandleGetBillets_WhenViewModelCallsHim_ShouldCallViewController() {
        let billetSelection = BilletSelectionModelMock.generic
        
        let presenters = billetSelection.billets.map {
            BilletSelectionCellPresenter(isDealership: true,
                                         description: $0.description,
                                         linecode: $0.linecode,
                                         amount: $0.amount,
                                         dueDate: $0.dueDate)
        }
        
        sut.handleGetBillets(withTitle: billetSelection.title, description: "", and: presenters)
        
        XCTAssertEqual(viewControllerSpy.callPresentViewElementsCount, 1)
        XCTAssertEqual(viewControllerSpy.items?.cells.count, billetSelection.billets.count)
    }
    
    func testDidNextStep_ShouldCallTheCoordinatorWithTheRightAction() {
        sut.handleNextStep(action: .close)
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .close)
    }
}
