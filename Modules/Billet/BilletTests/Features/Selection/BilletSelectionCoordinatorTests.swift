import UI
import Core
import XCTest
@testable import Billet

private final class DeeplinkWrapperSpy: DeeplinkContract {
    private(set) var callOpenDeeplinkCount = 0
    
    func open(url: URL) -> Bool {
        callOpenDeeplinkCount += 1
        return true
    }
}

final class BilletSelectionCoordinatorTests: XCTestCase {
    private let viewControllerSpy = ViewControllerMock()
    private lazy var navControllerSpy = NavigationControllerMock(rootViewController: viewControllerSpy)
    private lazy var sut: BilletSelectionCoordinating = {
        let coordinator = BilletSelectionCoordinator()
        coordinator.viewController = navControllerSpy.topViewController
        return coordinator
    }()
    
    func testPerform_WhenActionIsMakePayment_ShouldOpenTheBilletFormsScene() {
        sut.perform(action: .makePayment(linecode: "000", placeholder: "IPTU - cota única"))
        
        XCTAssertTrue(navControllerSpy.pushedViewController is BilletFormViewController)
    }
    
    func testPerform_WhenActionIsHelpCenter_ShouldOpenTheHelpCentersWebView() {
        let deeplinkWrapperSpy = DeeplinkWrapperSpy()
        BilletSetup.shared.inject(instance: deeplinkWrapperSpy)
        
        let url = "https://app.picpay.com/helpcenter?query=pagamento-de-conta-boletos"
        sut.perform(action: .helpCenter(url: url))
        
        XCTAssertEqual(deeplinkWrapperSpy.callOpenDeeplinkCount, 1)
    }
    
    func testPerform_WhenActionIsClose_ShouldDismissTheCurrentScene() {
        sut.perform(action: .close)
        
        XCTAssertEqual(viewControllerSpy.didDismissControllerCount, 1)
    }
}
