import FeatureFlag
import XCTest
@testable import Billet

private final class BilletSelectionPresenterSpy: BilletSelectionPresenting {
    var viewController: BilletSelectionDisplayable? = nil
    
    private(set) var callHandleGetBilletsCount = 0
    private(set) var viewElements: (title: String, description: String?, presenters: [BilletSelectionCellPresenting])?
    
    private(set) var callHandleNextStepCount = 0
    private(set) var action: BilletSelectionAction?
    
    func handleGetBillets(withTitle title: String, description: String?, and presenters: [BilletSelectionCellPresenting]) {
        callHandleGetBilletsCount += 1
        self.viewElements = (title: title, description: description, presenters: presenters)
    }
    
    func handleNextStep(action: BilletSelectionAction) {
        callHandleNextStepCount += 1
        self.action = action
    }
}

final class BilletSelectionInteractorTests: XCTestCase {
    private let featureManagerMock = FeatureManagerMock()
    private lazy var dependenciesMock = DependencyContainerMock(featureManagerMock)
    private let presenterSpy = BilletSelectionPresenterSpy()
    private lazy var billetSelection = BilletSelectionModelMock.generic
    private lazy var sut = BilletSelectionInteractor(dependencies: dependenciesMock,
                                                     presenter: presenterSpy,
                                                     billetSelection: billetSelection)
    
    func testGetBillets_ShouldCallPresenterWithTheRightValues() {
        sut.getBillets()
        
        XCTAssertEqual(presenterSpy.callHandleGetBilletsCount, 1)
        XCTAssertEqual(presenterSpy.viewElements?.title, billetSelection.title)
        XCTAssertEqual(presenterSpy.viewElements?.presenters.count, billetSelection.billets.count)
    }
    
    func testSelectedItem_WhenABilletIsSelected_ShouldCallPresenterWithTheRightValues() {
        sut.selectedItem(at: 0)
        
        XCTAssertEqual(presenterSpy.callHandleNextStepCount, 1)
        let billet = billetSelection.billets[0]
        let placeholder = "\(billetSelection.title) - \(billet.description)"
        XCTAssertEqual(presenterSpy.action, .makePayment(linecode: billet.linecode, placeholder: placeholder))
    }
    
    func testClose_ShouldCallPresenterWithTheRightValues() {
        sut.close()
        
        XCTAssertEqual(presenterSpy.callHandleNextStepCount, 1)
        XCTAssertEqual(presenterSpy.action, .close)
    }
    
    func testHelp_ShouldCallPresenterWithTheRightValues() {
        let url = "https://app.picpay.com/helpcenter?query=pagamento-de-conta-boletos"
        featureManagerMock.override(key: .opsBilletSelectionHelpUrl, with: url)
        
        sut.help()
        
        XCTAssertEqual(presenterSpy.callHandleNextStepCount, 1)
        XCTAssertEqual(presenterSpy.action, .helpCenter(url: url))
    }
}
