import XCTest
@testable import Billet

final class BilletSelectionCellTests: XCTestCase {
    
    private let presenter = BilletSelectionCellPresenter(isDealership: true,
                                                         description: "Cota única",
                                                         linecode: "816200000007832947842015903194784091310761100789",
                                                         amount: 900.0,
                                                         dueDate: "2020-10-20")
    
    private let sut = BilletSelectionCell()
    
    func testConfigure_WhenPresenterIsInjected_ShouldHaveTheRightValues() {
        sut.configure(with: presenter)
        
        XCTAssertEqual(sut.descriptionLabel.text, presenter.description)
        XCTAssertEqual(sut.linecodeLabel.text, "81620000000-7 83294784201-5 90319478409-1 31076110078-9")
        XCTAssertEqual(sut.valueLabel.text, presenter.amount)
        XCTAssertEqual(sut.dueDateLabel.text, presenter.dueDate)
    }
}
