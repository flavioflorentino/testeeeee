import AnalyticsModule
@testable import Billet
import Core
import FeatureFlag
import XCTest

final class DependencyContainerMock: BillsDependencies {
    lazy var mainQueue: DispatchQueue = resolve(default: .main)
    lazy var analytics: AnalyticsProtocol = resolve(default: AnalyticsSpy())
    lazy var featureManager: FeatureManagerContract = resolve(default: FeatureManagerMock())
    lazy var legacy: BilletSetupable = resolve(default: BilletSetupMock())
    
    private let dependencies: [Any]
    
    init(_ dependencies: Any...) {
        self.dependencies = dependencies
    }
}

extension DependencyContainerMock {
    func resolve<T>(default: T) -> T {
        guard let mock = dependencies.first(where: { $0 is T }) as? T else {
            return `default`
        }
        return mock
    }
}
