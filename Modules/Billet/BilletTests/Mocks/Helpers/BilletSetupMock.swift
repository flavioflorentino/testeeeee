@testable import Billet
import Core
import UI
import UIKit

final class BilletFlowSpy: BilletFlowContract {
    private(set) var callOpenBilletPaymentCount = 0
    
    func openBilletPayment(with model: BilletResponse, description: String?, origin: BilletOrigin, from viewController: UIViewController) {
        callOpenBilletPaymentCount += 1
    }
}

final class ContactListSpy: ContactListContract {
    private(set) var callOpenCount = 0
    
    func open(withFeedId id: String) {
        callOpenCount += 1
    }
}

final class DeeplinkSpy: DeeplinkContract {
    private(set) var callOpenCount = 0
    
    func open(url: URL) -> Bool {
        callOpenCount += 1
        return true
    }
}

final class ProfileSpy: ProfileContract {
    private(set) var callOpenCount = 0
    
    func open(withId id: Int, image: String, isPro: Bool, username: String) {
        callOpenCount += 1
    }
}

final class ReceiptSpy: ReceiptContract {
    private(set) var callOpenCount = 0
    
    func open(withId id: String, receiptType: String, feedItemId: String?) {
        callOpenCount += 1
    }
}

final class WebViewSpy: WebViewCreatable {
    private(set) var callCreateWebviewCount = 0
    
    func createWebView(with url: URL) -> UIViewController {
        callCreateWebviewCount += 1
        return UIViewController()
    }
}

final class BilletSetupMock: BilletSetupable {
    var billet: BilletFlowContract? = BilletFlowSpy()
    
    var contactList: ContactListContract? = ContactListSpy()
    
    var deeplink: DeeplinkContract? = DeeplinkSpy()
    
    var profile: ProfileContract? = ProfileSpy()
    
    var receipt: ReceiptContract? = ReceiptSpy()
    
    var webview: WebViewCreatable? = WebViewSpy()
}
