@testable import Billet

enum BilletSelectionListModelMock {
    static var generic: BilletSelectionListModel {
        BilletSelectionListModel(linecode: "816200000007832947842015903194784091310761100789",
                                 description: "Cota 3",
                                 dueDate: "2019-03-19",
                                 amount: 83.29)
    }
}
