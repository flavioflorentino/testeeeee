@testable import Billet
import Core

enum CIPResponseMock {
    static var `default`: CIPResponse {
        CIPResponse(isOverdue: false,
                    canEditValue: false,
                    canReceiveOverdue: true,
                    isRegisterValid: true,
                    amount: 1000,
                    paymentValue: 1000)
    }
    
    static var overduePaymentAccepted: CIPResponse {
        CIPResponse(isOverdue: true,
                    canEditValue: false,
                    canReceiveOverdue: true,
                    isRegisterValid: true,
                    amount: 1000,
                    paymentValue: 1000)
    }
    
    static var overduePaymentUnnaccepted: CIPResponse {
        CIPResponse(isOverdue: true,
                    canEditValue: false,
                    canReceiveOverdue: false,
                    isRegisterValid: true,
                    amount: 1000,
                    paymentValue: 1000)
    }
    
    static var discount: CIPResponse {
        CIPResponse(isOverdue: false,
                    canEditValue: false,
                    canReceiveOverdue: true,
                    isRegisterValid: true,
                    amount: 1000,
                    paymentValue: 1000,
                    discount: 30.0)
    }
}
