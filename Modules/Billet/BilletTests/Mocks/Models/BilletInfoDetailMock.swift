@testable import Billet

enum BilletInfoDetailMock {
    static var completed: BilletInfoDetail {
        BilletInfoDetail(
            feedId: "5e82058b0e1b240001e7d4d0",
            isOwner: true,
            title: "Pagamento de boleto",
            currentStatusTitle: "Pagamento Realizado",
            currentStatus: .completed,
            userImage: "",
            value: "<font color='#ED1846'><b>R$ 5,00</b></font>",
            date: "2 dias atrás",
            actions: [
                TransactionActionMock.receipt,
                TransactionActionMock.switchPrivacy,
                TransactionActionMock.disableNotification,
                TransactionActionMock.hideTransaction
            ],
            isPublic: true,
            liked: false,
            likeText: "Curtir",
            timeline: [
                TimelineStatusMock.pendingCompleted,
                TimelineStatusMock.processingCompleted,
                TimelineStatusMock.completed
            ]
        )
    }
    
    static var emptyActions: BilletInfoDetail {
        BilletInfoDetail(
            feedId: "5e82058b0e1b240001e7d4d0",
            isOwner: true,
            title: "Pagamento de boleto",
            currentStatusTitle: "Pagamento Realizado",
            currentStatus: .completed,
            userImage: "",
            value: "<font color='#ED1846'><b>R$ 5,00</b></font>",
            date: "2 dias atrás",
            actions: [],
            isPublic: true,
            liked: false,
            likeText: "Curtir",
            timeline: [
                TimelineStatusMock.pendingCompleted,
                TimelineStatusMock.processingCompleted,
                TimelineStatusMock.completed
            ]
        )
    }
    
    static var analysisCompleted: BilletInfoDetail {
        BilletInfoDetail(
            feedId: "5e82058b0e1b240001e7d4d0",
            isOwner: true,
            title: "Pagamento de boleto",
            currentStatusTitle: "Pagamento Realizado",
            currentStatus: .completed,
            userImage: "",
            value: "<font color='#ED1846'><b>R$ 5,00</b></font>",
            date: "2 dias atrás",
            actions: [
                TransactionActionMock.receipt,
                TransactionActionMock.switchPrivacy,
                TransactionActionMock.disableNotification,
                TransactionActionMock.hideTransaction
            ],
            isPublic: true,
            liked: false,
            likeText: "Curtir",
            timeline: [
                TimelineStatusMock.pendingCompleted,
                TimelineStatusMock.analysisCompleted,
                TimelineStatusMock.processingCompleted,
                TimelineStatusMock.completed
            ]
        )
    }
    
    static var accelerateAnalysis: BilletInfoDetail {
        BilletInfoDetail(
            feedId: "5e82058b0e1b240001e7d4d0",
            isOwner: true,
            title: "Pagamento de boleto",
            currentStatusTitle: "Em análise",
            currentStatus: .accelerateAnalysis,
            userImage: "",
            value: "<font color='#ED1846'><b>R$ 5,00</b></font>",
            date: "2 dias atrás",
            actions: [
                TransactionActionMock.receipt,
                TransactionActionMock.switchPrivacy,
                TransactionActionMock.disableNotification,
                TransactionActionMock.hideTransaction
            ],
            isPublic: true,
            liked: false,
            likeText: "Curtir",
            timeline: [
                TimelineStatusMock.pendingCompleted,
                TimelineStatusMock.accelerateAnalysis,
                TimelineStatusMock.processing,
                TimelineStatusMock.notCompleted
            ]
        )
    }
    
    static var inAnalysis: BilletInfoDetail {
        BilletInfoDetail(
            feedId: "5e82058b0e1b240001e7d4d0",
            isOwner: true,
            title: "Pagamento de boleto",
            currentStatusTitle: "Em análise",
            currentStatus: .inAnalysis,
            userImage: "",
            value: "<font color='#ED1846'><b>R$ 5,00</b></font>",
            date: "2 dias atrás",
            actions: [
                TransactionActionMock.receipt,
                TransactionActionMock.switchPrivacy,
                TransactionActionMock.disableNotification,
                TransactionActionMock.hideTransaction
            ],
            isPublic: true,
            liked: false,
            likeText: "Curtir",
            timeline: [
                TimelineStatusMock.pendingCompleted,
                TimelineStatusMock.inAnalysis,
                TimelineStatusMock.processing,
                TimelineStatusMock.notCompleted
            ]
        )
    }
    
    static var processing: BilletInfoDetail {
        BilletInfoDetail(
            feedId: "5e82058b0e1b240001e7d4d0",
            isOwner: true,
            title: "Estamos processando seu boleto",
            currentStatusTitle: "Em Processamento",
            currentStatus: .processing,
            userImage: "",
            value: "<font color='#ED1846'><b>R$ 5,00</b></font>",
            date: "2 dias atrás",
            actions: [
                TransactionActionMock.receipt,
                TransactionActionMock.switchPrivacy,
                TransactionActionMock.disableNotification,
                TransactionActionMock.hideTransaction
            ],
            isPublic: true,
            liked: false,
            likeText: "Curtir",
            timeline: [
                TimelineStatusMock.pendingCompleted,
                TimelineStatusMock.processing,
                TimelineStatusMock.notCompleted
            ]
        )
    }
    
    static var refused: BilletInfoDetail {
        BilletInfoDetail(
            feedId: "5e82058b0e1b240001e7d4d0",
            isOwner: true,
            title: "Pagamento de boleto",
            currentStatusTitle: "Não Realizado",
            currentStatus: .refused,
            userImage: "",
            value: "<font color='#ED1846'><b>R$ 5,00</b></font>",
            date: "2 dias atrás",
            actions: [
                TransactionActionMock.receipt,
                TransactionActionMock.switchPrivacy,
                TransactionActionMock.disableNotification,
                TransactionActionMock.hideTransaction
            ],
            isPublic: true,
            liked: false,
            likeText: "Curtir",
            timeline: [
                TimelineStatusMock.refused
            ]
        )
    }
}
