@testable import Billet

enum GenericBehaviorResponseMock {
    static var generic: GenericBehaviorResponse {
        GenericBehaviorResponse(type: .reloadItem, alert: .init(title: "Random title", message: "Random message"))
    }
}
