@testable import Billet

enum ProfileResponseMock {
    static var generic: ProfileResponse {
        ProfileResponse(id: "1", name: "PicPay", username: "pp", img: "", isPro: false)
    }
}
