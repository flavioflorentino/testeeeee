@testable import Billet

enum CommentsResponseMock {
    static var generic: CommentsResponse {
        CommentsResponse(
            profiles: [
                ProfileResponseMock.generic,
                ProfileResponseMock.generic,
                ProfileResponseMock.generic
            ],
            comments: [
                CommentResponse(id: "1", text: "Random comment", date: "2 horas atrás", consumerId: "1", profile: nil),
                CommentResponse(id: "12", text: "Random comment 2", date: "3 horas atrás", consumerId: "1", profile: nil),
                CommentResponse(id: "123", text: "Random comment 3", date: "4 horas atrás", consumerId: "1", profile: nil)
            ],
            likes: LikeResponseMock.notChecked
        )
    }
}

