@testable import Billet
import Core

enum BilletResponseMock {
    static var `default`: BilletResponse {
        BilletResponse(sellerId: "102030",
                       beneficiary: "Itaú Unibanco",
                       amount: "1000",
                       linecode: "816200000007832947842015903194784091310761100786",
                       barcode: "816200000007832947842015903194784091310761100786",
                       isFillable: false,
                       isExpired: false,
                       surcharge: 2.99,
                       isPaymentTimeLimitExceeded: false,
                       dueDate: "12/10/2020",
                       paymentDate: "10/10/2020",
                       instructions: "random instruction",
                       cip: CIPResponseMock.default,
                       cipObj: AnyCodable(value: CIPResponseMock.default))
    }
    
    static var overduePaymentAccepted: BilletResponse {
        BilletResponse(sellerId: "102030",
                       beneficiary: "Itaú Unibanco",
                       amount: "1000",
                       linecode: "816200000007832947842015903194784091310761100786",
                       barcode: "816200000007832947842015903194784091310761100786",
                       isFillable: false,
                       isExpired: true,
                       surcharge: 2.99,
                       isPaymentTimeLimitExceeded: false,
                       dueDate: "12/10/2020",
                       paymentDate: "10/10/2020",
                       instructions: "random instruction",
                       cip: CIPResponseMock.overduePaymentAccepted,
                       cipObj: AnyCodable(value: CIPResponseMock.overduePaymentAccepted))
    }
    
    static var overduePaymentUnnaccepted: BilletResponse {
        BilletResponse(sellerId: "102030",
                       beneficiary: "Itaú Unibanco",
                       amount: "1000",
                       linecode: "816200000007832947842015903194784091310761100786",
                       barcode: "816200000007832947842015903194784091310761100786",
                       isFillable: false,
                       isExpired: true,
                       surcharge: 2.99,
                       isPaymentTimeLimitExceeded: false,
                       dueDate: "12/10/2020",
                       paymentDate: "10/10/2020",
                       instructions: "random instruction",
                       cip: CIPResponseMock.overduePaymentUnnaccepted,
                       cipObj: AnyCodable(value: CIPResponseMock.overduePaymentUnnaccepted))
    }
    
    static var paymentTimeLimitExceeded: BilletResponse {
        BilletResponse(sellerId: "102030",
                       beneficiary: "Itaú Unibanco",
                       amount: "1000",
                       linecode: "816200000007832947842015903194784091310761100786",
                       barcode: "816200000007832947842015903194784091310761100786",
                       isFillable: false,
                       isExpired: true,
                       surcharge: 2.99,
                       isPaymentTimeLimitExceeded: true,
                       paymentTimeLimit: "19:00",
                       dueDate: "12/10/2020",
                       paymentDate: "10/10/2020",
                       instructions: "random instruction",
                       cip: CIPResponseMock.default,
                       cipObj: AnyCodable(value: CIPResponseMock.overduePaymentAccepted))
    }

    static var duplicationScheduling: BilletResponse {
        BilletResponse(sellerId: "102030",
                       beneficiary: "Itaú Unibanco",
                       amount: "1000",
                       linecode: "816200000007832947842015903194784091310761100786",
                       barcode: "816200000007832947842015903194784091310761100786",
                       isFillable: false,
                       isExpired: true,
                       surcharge: 2.99,
                       isPaymentTimeLimitExceeded: true,
                       paymentTimeLimit: "19:00",
                       billId: "1234",
                       dueDate: "12/10/2020",
                       paymentDate: "10/10/2020",
                       instructions: "random instruction",
                       cip: CIPResponseMock.default,
                       cipObj: AnyCodable(value: CIPResponseMock.overduePaymentAccepted))
    }
    
    static var fillable: BilletResponse {
        BilletResponse(sellerId: "102030",
                       beneficiary: "Itaú Unibanco",
                       amount: "1000",
                       linecode: "816200000007832947842015903194784091310761100786",
                       barcode: "816200000007832947842015903194784091310761100786",
                       isFillable: true,
                       isExpired: false,
                       surcharge: 2.99,
                       isPaymentTimeLimitExceeded: false,
                       dueDate: "12/10/2020",
                       paymentDate: "10/10/2020",
                       instructions: "random instruction",
                       cip: CIPResponseMock.default,
                       cipObj: AnyCodable(value: CIPResponseMock.default))
    }
    
    static var discount: BilletResponse {
        BilletResponse(sellerId: "102030",
                       beneficiary: "Itaú Unibanco",
                       amount: "1000",
                       linecode: "816200000007832947842015903194784091310761100786",
                       barcode: "816200000007832947842015903194784091310761100786",
                       isFillable: false,
                       isExpired: false,
                       surcharge: 2.99,
                       isPaymentTimeLimitExceeded: false,
                       dueDate: "12/10/2020",
                       paymentDate: "10/10/2020",
                       instructions: "random instruction",
                       cip: CIPResponseMock.discount,
                       cipObj: AnyCodable(value: CIPResponseMock.default))
    }
}
