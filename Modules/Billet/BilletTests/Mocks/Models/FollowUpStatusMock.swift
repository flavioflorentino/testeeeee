@testable import Billet

enum FollowUpStatusMock {
    static var completed: FollowUpStatus {
        FollowUpStatus(
            title: "Pagamento de boleto",
            currentStatusTitle: "Pagamento Realizado",
            currentStatusType: .completed,
            timeline: [
                FollowUpTimelineStatusMock.pendingCompleted,
                FollowUpTimelineStatusMock.processingCompleted,
                FollowUpTimelineStatusMock.completed
            ]
        )
    }
    
    static var analysisCompleted: FollowUpStatus {
        FollowUpStatus(
            title: "Pagamento de boleto",
            currentStatusTitle: "Pagamento Realizado",
            currentStatusType: .completed,
            timeline: [
                FollowUpTimelineStatusMock.pendingCompleted,
                FollowUpTimelineStatusMock.analysisCompleted,
                FollowUpTimelineStatusMock.processingCompleted,
                FollowUpTimelineStatusMock.completed
            ]
        )
    }
    
    static var accelerateAnalysis: FollowUpStatus {
        FollowUpStatus(
            title: "Pagamento de boleto",
            currentStatusTitle: "Em análise",
            currentStatusType: .accelerateAnalysis,
            timeline: [
                FollowUpTimelineStatusMock.pendingCompleted,
                FollowUpTimelineStatusMock.accelerateAnalysis,
                FollowUpTimelineStatusMock.processing,
                FollowUpTimelineStatusMock.notCompleted
            ]
        )
    }
    
    static var inAnalysis: FollowUpStatus {
        FollowUpStatus(
            title: "Pagamento de boleto",
            currentStatusTitle: "Em análise",
            currentStatusType: .inAnalysis,
            timeline: [
                FollowUpTimelineStatusMock.pendingCompleted,
                FollowUpTimelineStatusMock.inAnalysis,
                FollowUpTimelineStatusMock.processing,
                FollowUpTimelineStatusMock.notCompleted
            ]
        )
    }
    
    static var processing: FollowUpStatus {
        FollowUpStatus(
            title: "Pagamento de boleto",
            currentStatusTitle: "Em Processamento",
            currentStatusType: .processing,
            timeline: [
                FollowUpTimelineStatusMock.pendingCompleted,
                FollowUpTimelineStatusMock.processing,
                FollowUpTimelineStatusMock.notCompleted
            ]
        )
    }
    
    static var refused: FollowUpStatus {
        FollowUpStatus(
            title: "Pagamento de boleto",
            currentStatusTitle: "Não Realizado",
            currentStatusType: .refused,
            timeline: [
                FollowUpTimelineStatusMock.refused
            ]
        )
    }
}
