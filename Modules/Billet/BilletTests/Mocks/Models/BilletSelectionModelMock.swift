@testable import Billet

enum BilletSelectionModelMock {
    static var generic: BilletSelectionModel {
        BilletSelectionModel(title: "IPTU",
                             description: nil,
                             billets: [BilletSelectionListModelMock.generic, BilletSelectionListModelMock.generic])
    }
}
