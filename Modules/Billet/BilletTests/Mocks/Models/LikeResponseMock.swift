@testable import Billet

enum LikeResponseMock {
    static var notChecked: LikeResponse {
        LikeResponse(text: "Curtir", isChecked: false)
    }
    
    static var checked: LikeResponse {
        LikeResponse(text: "Você curtiu", isChecked: true)
    }
}
