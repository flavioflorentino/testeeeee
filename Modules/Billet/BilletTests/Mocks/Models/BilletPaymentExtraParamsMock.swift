@testable import Billet
import Core

enum BilletPaymentExtraParamsMock {
    static var `default`: BilletPaymentExtraParams {
        BilletPaymentExtraParams(type: .default, date: "2020/10/10")
    }

    static var scheduling: BilletPaymentExtraParams {
        BilletPaymentExtraParams(type: .scheduling, date: "2020/11/11")
    }
}
