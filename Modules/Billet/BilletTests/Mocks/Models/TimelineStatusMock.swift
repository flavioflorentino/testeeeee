@testable import Billet

enum TimelineStatusMock {
    static var refused: TimelineStatus {
        TimelineStatus(
            status: .refused,
            title: "Não Realizado",
            label: "Não Realizado",
            message: "Sentimos muito que seu pagamento não foi realizado!<br/><br/>O valor de R$ 230,00 foi devolvido para sua carteira. Você pode conferir no seu feed de atividades ok?",
            date: "01 Jan 00:00",
            isCompleted: true,
            actions: [
                TransactionActionMock.helpCenter
            ]
        )
    }
    
    static var pendingCompleted: TimelineStatus {
        TimelineStatus(
            status: .pending,
            title: "Recebemos sua solicitação",
            label: "Pagamento Solicitado",
            message: "Pronto, recebemos sua solicitação de pagamento.",
            date: "01 Jan 00:00",
            isCompleted: true,
            actions: [
                TransactionActionMock.helpCenter
            ]
        )
    }
    
    static var processing: TimelineStatus {
        TimelineStatus(
            status: .processing,
            title: "Estamos processando seu boleto",
            label: "Em Processamento",
            message: "Seu boleto será processado até as 19h do dia do pagamento. Mas fique tranquilo, seu pagamento está feito e eventuais juros ou encargos não devem ser cobrados.",
            date: "01 Jan 00:00",
            isCompleted: false,
            actions: []
        )
    }
    
    static var processingCompleted: TimelineStatus {
        TimelineStatus(
            status: .processing,
            title: "Processamos seu pagamento com sucesso",
            label: "Pagamento Processado",
            message: "Seu pagamento foi processado com sucesso! Eventuais juros ou encargos não devem ser cobrados.",
            date: "01 Jan 00:00",
            isCompleted: true,
            actions: [
                TransactionActionMock.helpCenter
            ]
        )
    }
    
    static var inAnalysis: TimelineStatus {
        TimelineStatus(
            status: .inAnalysis,
            title: "Em análise",
            label: "Em análise",
            message: "Hey, seu pagamento entrou em análise. Isso acontece, pois as vezes, precisamos validar alguns dados para garantir que o pagamento ocorra corretamente.<br/><br/><b>Fique de olho, talvez a gente entre em contato por telefone.</b>",
            date: "01 Jan 00:00",
            isCompleted: false,
            actions: [
                TransactionActionMock.helpCenter
            ]
        )
    }
    
    static var accelerateAnalysis: TimelineStatus {
        TimelineStatus(
            status: .accelerateAnalysis,
            title: "Em análise",
            label: "Em análise",
            message: "Hey, seu pagamento entrou em análise. Isso acontece, pois as vezes, precisamos validar alguns dados para garantir que o pagamento ocorra corretamente.<br/><br/><b>Fique de olho, talvez a gente entre em contato por telefone.</b>",
            date: "01 Jan 00:00",
            isCompleted: false,
            actions: [
                TransactionActionMock.accelerateAnalysis,
                TransactionActionMock.helpCenter
            ]
        )
    }
    
    static var analysisCompleted: TimelineStatus {
        TimelineStatus(
            status: .accelerateAnalysis,
            title: "Análise aprovada",
            label: "Análise realizada",
            message: "Seu pagamento foi aprovado na análise. Alguns dados foram validados para garantir que o pagamento ocorra corretamente.",
            date: "01 Jan 00:00",
            isCompleted: true,
            actions: [
                TransactionActionMock.helpCenter
            ]
        )
    }
    
    static var notCompleted: TimelineStatus {
        TimelineStatus(
            status: .completed,
            title: "Aguardando a realização do pagamento",
            label: "Aguardando Realização",
            message: "Quando seu pagamento for aprovado você receberá uma notificação e poderá acessar seu código de autenticaão, que comprova que o pagamento foi efetuado com sucesso.",
            date: "01 Jan 00:00",
            isCompleted: false,
            actions: []
        )
    }
    
    static var completed: TimelineStatus {
        TimelineStatus(
            status: .completed,
            title: "Pagamento Realizado",
            label: "Pagamento Realizado",
            message: "Seu pagamento foi realizado com sucesso! Você já pode acessar seu código de autenticação.",
            date: "01 Jan 00:00",
            isCompleted: true,
            actions: [
                TransactionActionMock.receipt,
                TransactionActionMock.helpCenter
            ]
        )
    }
}
