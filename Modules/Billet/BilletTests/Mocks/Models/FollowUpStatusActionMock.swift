import Core
@testable import Billet

enum FollowUpStatusActionMock {
    static var generic: FollowUpStatusAction {
        FollowUpStatusAction(
            title: "Empty",
            message: nil,
            type: .generic,
            id: nil,
            url: nil
        )
    }
    
    static var cancelPayment: FollowUpStatusAction {
        FollowUpStatusAction(
            title: "Cancelar pagamento",
            message: "Ao cancelar, seu pagamento não será efetuado. O valor pago será devolvido.",
            type: .cancel,
            id: "12345678",
            url: nil
        )
    }
    
    static var emptyCancelPayment: FollowUpStatusAction {
        FollowUpStatusAction(
            title: "Cancelar pagamento",
            message: nil,
            type: .cancel,
            id: nil,
            url: nil
        )
    }
    
    static var receipt: FollowUpStatusAction {
        FollowUpStatusAction(
            title: "Ver recibo",
            message: nil,
            type: .receipt,
            id: "12345678",
            url: nil
        )
    }
    
    static var emptyReceipt: FollowUpStatusAction {
        FollowUpStatusAction(
            title: "Ver recibo",
            message: nil,
            type: .receipt,
            id: nil,
            url: nil
        )
    }
    
    static var helpCenter: FollowUpStatusAction {
        FollowUpStatusAction(
            title: "Saiba mais",
            message: nil,
            type: .helpCenter,
            id: nil,
            url: "picpay://picpay/helpcenter?query=card-feed-do-pagador&analytics=14537bec96e8507ab80c7c68812f2485"
        )
    }
    
    static var emptyHelpCenter: FollowUpStatusAction {
        FollowUpStatusAction(
            title: "Saiba mais",
            message: nil,
            type: .helpCenter,
            id: nil,
            url: nil
        )
    }
    
    static var accelerateAnalysis: FollowUpStatusAction {
        FollowUpStatusAction(
            title: "Acelerar análise",
            message: nil,
            type: .accelerateAnalysis,
            id: "7",
            url: "picpay://picpay/accelerateAnalysis?transactionId=64&transactionType=pav_transaction&analytics=8269021b259b5634e71fd32b8905f8a9"
        )
    }
    
    static var emptyAccelerateAnalysis: FollowUpStatusAction {
        FollowUpStatusAction(
            title: "Acelerar análise",
            message: nil,
            type: .accelerateAnalysis,
            id: nil,
            url: nil
        )
    }
}
