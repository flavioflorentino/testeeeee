import Core
@testable import Billet

enum TransactionActionMock {
    static var generic: TransactionAction {
        TransactionAction(
            title: "Empty",
            message: nil,
            type: .generic,
            data: nil,
            id: nil,
            url: nil,
            value: false,
            action: nil
        )
    }
    
    static var cancelPayment: TransactionAction {
        TransactionAction(
            title: "Cancelar pagamento",
            message: "Ao cancelar, seu pagamento não será efetuado. O valor pago será devolvido.",
            type: .cancel,
            data: nil,
            id: "12345678",
            url: nil,
            value: false,
            action: nil
        )
    }
    
    static var emptyCancelPayment: TransactionAction {
        TransactionAction(
            title: "Cancelar pagamento",
            message: nil,
            type: .cancel,
            data: nil,
            id: nil,
            url: nil,
            value: false,
            action: nil
        )
    }
    
    static var receipt: TransactionAction {
        TransactionAction(
            title: "Ver recibo",
            message: nil,
            type: .receipt,
            data: nil,
            id: "12345678",
            url: nil,
            value: false,
            action: nil
        )
    }
    
    static var emptyReceipt: TransactionAction {
        TransactionAction(
            title: "Ver recibo",
            message: nil,
            type: .receipt,
            data: nil,
            id: nil,
            url: nil,
            value: false,
            action: nil
        )
    }
    
    static var helpCenter: TransactionAction {
        TransactionAction(
            title: "Saiba mais",
            message: nil,
            type: .helpCenter,
            data: nil,
            id: nil,
            url: "picpay://picpay/helpcenter?query=card-feed-do-pagador&analytics=14537bec96e8507ab80c7c68812f2485",
            value: false,
            action: nil
        )
    }
    
    static var emptyHelpCenter: TransactionAction {
        TransactionAction(
            title: "Saiba mais",
            message: nil,
            type: .helpCenter,
            data: nil,
            id: nil,
            url: nil,
            value: false,
            action: nil
        )
    }
    
    static var accelerateAnalysis: TransactionAction {
        TransactionAction(
            title: "Acelerar análise",
            message: nil,
            type: .accelerateAnalysis,
            data: AnyCodable(value: ["id": "7"]),
            id: nil,
            url: "picpay://picpay/accelerateAnalysis?transactionId=64&transactionType=pav_transaction&analytics=8269021b259b5634e71fd32b8905f8a9",
            value: false,
            action: nil
        )
    }
    
    static var emptyAccelerateAnalysis: TransactionAction {
        TransactionAction(
            title: "Acelerar análise",
            message: nil,
            type: .accelerateAnalysis,
            data: nil,
            id: nil,
            url: nil,
            value: false,
            action: nil
        )
    }
    
    static var switchPrivacy: TransactionAction {
        TransactionAction(
            title: "Tornar privado",
            message: nil,
            type: .switchPrivacy,
            data: AnyCodable(value: [
                "id": "bill-1885412"
            ]),
            id: "bill-1885412",
            url: nil,
            value: false,
            action: nil
        )
    }
    
    static var disableNotification: TransactionAction {
        TransactionAction(
            title: "Desativar notificações",
            message: "Você não receberá mais notificações de novos comentários neste feed. Desativar as notificações?",
            type: .generic,
            data: AnyCodable(value: [
                "id": "8226820",
                "action": "enable_comment_updates",
                "value": false
            ]),
            id: "8226820",
            url: nil,
            value: false,
            action: .disableNotification
        )
    }
    
    static var hideTransaction: TransactionAction {
        TransactionAction(
            title: "Esconder transação",
            message: "A transação ficará privada e deixará de ser visível para você. Somente o outro participante da transação poderá vê-la. Deseja realmente esconder essa transação?",
            type: .generic,
            data: AnyCodable(value: [
                "id": "8226820",
                "action": "hide_feed_item"
            ]),
            id: "8226820",
            url: nil,
            value: false,
            action: .hideItem
        )
    }
}
