@testable import Billet
import UIKit

enum HubModelMock {
    static var options: HubModel {
        HubModel(
            title: "Como você deseja pagar sua conta?",
            contentType: .options([
                HubOptionMock.scanner,
                HubOptionMock.linecode
            ])
        )
    }
    static var banners: HubModel {
        HubModel(
            title: "Dúvidas",
            contentType: .banners([HubBannerMock.faq])
        )
    }
}

enum HubBannerMock {
    static var faq: HubBanner {
        HubBanner(title: "Ajuda",
                  description: "Descrição",
                  actionTitle: "Ação",
                  deeplink: "Faq")
    }
}

enum HubOptionMock {
    static var scanner: HubOption {
        HubOption(icon: UIImage(), title: "Scanner")
    }
    static var linecode: HubOption {
        HubOption(icon: UIImage(), title: "Linecode")
    }
}
