import Foundation

// swiftlint:disable convenience_type
final class BilletResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: BilletResources.self).url(forResource: "BilletResources", withExtension: "bundle") else {
            return Bundle(for: BilletResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: BilletResources.self)
    }()
}
