import AnalyticsModule

enum PaymentEvent: AnalyticsKeyProtocol, Equatable {
    case billetFormViewed
    case billetFormNextSelected(_ hasDescription: Bool)
    case billetCardFeeInformation
    case billetInstallmentFeeInformation(installment: Int)
    case billetInterestInformationViewed
    case billetDiscountInformationViewed
    case billetSchedulingCalendarViewed
    case hubActionSelected(HubActionSelected)
    
    private var name: String {
        switch self {
        case .billetFormViewed:
            return "Bar Code Screen"
        case .billetFormNextSelected:
            return "Bar Code Screen Next Selected"
        case .billetCardFeeInformation:
            return "Card Fee Information"
        case .billetInstallmentFeeInformation:
            return "Split Fee Information"
        case .billetInterestInformationViewed:
            return "Interest Information Viewed"
        case .billetDiscountInformationViewed:
            return "Discount Information Viewed"
        case .billetSchedulingCalendarViewed:
            return "Scheduling Calendar Viewed"
        case .hubActionSelected:
            return "Card On Bills"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .billetFormNextSelected(let hasDescription):
            return [ "description_added": hasDescription ]
            
        case .billetInstallmentFeeInformation(let installment):
            return [ "split_viewd": "\(installment)" ]
        
        case .hubActionSelected(let actionSelected):
            return [
                "option_selected": actionSelected.rawValue
            ]
            
        default:
            return [:]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .eventTracker]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("Bills - \(name)", properties: properties, providers: [.mixPanel])
    }
}

enum HubActionSelected: String {
    case scanner = "Usar Leitor"
    case enterManually = "Digitar"
    case knowMore = "Saiba mais"
    case howToPayCard = "Card como pagar"
}
