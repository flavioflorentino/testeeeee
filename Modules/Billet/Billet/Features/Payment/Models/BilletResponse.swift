import Core
import CorePayment
import Foundation

public struct BilletResponse: Decodable, Equatable {
    let sellerId: String
    let billId: String?
    let beneficiary: String
    public let amount: String
    public let linecode: String
    let barcode: String
    let isFillable: Bool
    let isExpired: Bool
    let dueDate: String?
    let paymentDate: String?
    let surcharge: Double
    let instructions: String?
    let paymentType: PaymentType
    let minSchedulePaymentDate: String?
    let schedulePaymentDate: String?
    let cip: CIPResponse?
    public let cipObj: AnyCodable?
    let installmentFee: Double = 3.49
    let isPaymentTimeLimitExceeded: Bool
    let paymentTimeLimit: String?
    
    enum CodingKeys: String, CodingKey {
        case sellerId
        case billId
        case beneficiary
        case beneficiaryName = "name"
        case beneficiaryFantasyName = "fantasyName"
        case amount
        case linecode = "lineCode"
        case barcode = "barCode"
        case isFillable = "fillable"
        case isExpired = "expired"
        case dueDate
        case paymentDate
        case surcharge
        case instructions
        case paymentType
        case minSchedulePaymentDate = "min_scheduling_date"
        case schedulePaymentDate
        case cip
        case isPaymentTimeLimitExceeded = "exceededPaymentTimeLimit"
        case paymentTimeLimit
    }
    
    init(sellerId: String,
         beneficiary: String,
         amount: String,
         linecode: String,
         barcode: String,
         isFillable: Bool,
         isExpired: Bool,
         surcharge: Double,
         isPaymentTimeLimitExceeded: Bool,
         paymentTimeLimit: String? = nil,
         billId: String? = nil,
         dueDate: String? = nil,
         paymentDate: String? = nil,
         instructions: String? = nil,
         paymentType: PaymentType = .default,
         minSchedulePaymentDate: String? = nil,
         schedulePaymentDate: String? = nil,
         cip: CIPResponse? = nil,
         cipObj: AnyCodable? = nil) {
        self.sellerId = sellerId
        self.beneficiary = beneficiary
        self.amount = amount
        self.linecode = linecode
        self.barcode = barcode
        self.isFillable = isFillable
        self.isExpired = isExpired
        self.dueDate = dueDate
        self.paymentDate = paymentDate
        self.surcharge = surcharge
        self.isPaymentTimeLimitExceeded = isPaymentTimeLimitExceeded
        self.paymentTimeLimit = paymentTimeLimit
        self.billId = billId
        self.instructions = instructions
        self.paymentType = paymentType
        self.minSchedulePaymentDate = minSchedulePaymentDate
        self.schedulePaymentDate = schedulePaymentDate
        self.cip = cip
        self.cipObj = cipObj
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let sellerId = try container.decode(Int.self, forKey: .sellerId)
        self.sellerId = String(describing: sellerId)
        
        self.billId = try container.decodeIfPresent(String.self, forKey: .billId)
        
        let beneficiaryDict = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .beneficiary)
        let beneficiaryKey: CodingKeys = beneficiaryDict.contains(.beneficiaryName) ? .beneficiaryName : .beneficiaryFantasyName
        beneficiary = try beneficiaryDict.decode(String.self, forKey: beneficiaryKey)
        
        amount = try container.decode(String.self, forKey: .amount)
        linecode = try container.decode(String.self, forKey: .linecode)
        barcode = try container.decode(String.self, forKey: .barcode)
        isFillable = try container.decode(Bool.self, forKey: .isFillable)
        isExpired = try container.decode(Bool.self, forKey: .isExpired)
        dueDate = try container.decodeIfPresent(String.self, forKey: .dueDate)
        paymentDate = try container.decodeIfPresent(String.self, forKey: .paymentDate)
        
        let surcharge = try container.decodeIfPresent(String.self, forKey: .surcharge) ?? ""
        self.surcharge = Double(surcharge) ?? 0
        instructions = try container.decodeIfPresent(String.self, forKey: .instructions)
        
        paymentType = try container.decodeIfPresent(PaymentType.self, forKey: .paymentType) ?? .default
        minSchedulePaymentDate = try container.decodeIfPresent(String.self, forKey: .minSchedulePaymentDate)
        schedulePaymentDate = try container.decodeIfPresent(String.self, forKey: .schedulePaymentDate)
        
        cip = try container.decodeIfPresent(CIPResponse.self, forKey: .cip)
        cipObj = try container.decodeIfPresent(AnyCodable.self, forKey: .cip)
        
        isPaymentTimeLimitExceeded = try container.decodeIfPresent(Bool.self, forKey: .isPaymentTimeLimitExceeded) ?? false
        paymentTimeLimit = try container.decodeIfPresent(String.self, forKey: .paymentTimeLimit)
    }
    
    public static func == (lhs: BilletResponse, rhs: BilletResponse) -> Bool {
        lhs.amount == rhs.amount && lhs.linecode == rhs.linecode && lhs.barcode == rhs.barcode && lhs.dueDate == rhs.dueDate
    }
}
