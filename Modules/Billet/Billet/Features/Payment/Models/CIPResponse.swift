public struct CIPResponse: Decodable, Equatable {
    let isOverdue: Bool
    let canEditValue: Bool
    let canReceiveOverdue: Bool
    let isRegisterValid: Bool
    let amount: Double?
    let paymentValue: Double?
    let interest: Double?
    let discount: Double?
    let dateSearch: String?
    
    enum CodingKeys: String, CodingKey {
        case amount
        case paymentValue
        case interest
        case discount
        case isOverdue
        case canEditValue
        case canReceiveOverdue
        case isRegisterValid
        case dateSearch
    }
    
    init(
        isOverdue: Bool,
        canEditValue: Bool,
        canReceiveOverdue: Bool,
        isRegisterValid: Bool,
        amount: Double? = nil,
        paymentValue: Double? = nil,
        interest: Double? = nil,
        discount: Double? = nil,
        dateSearch: String? = nil) {
        self.amount = amount
        self.paymentValue = paymentValue
        self.interest = interest
        self.discount = discount
        self.isOverdue = isOverdue
        self.canEditValue = canEditValue
        self.canReceiveOverdue = canReceiveOverdue
        self.isRegisterValid = isRegisterValid
        self.dateSearch = dateSearch
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        isOverdue = try container.decodeIfPresent(Bool.self, forKey: .isOverdue) ?? false
        canEditValue = try container.decodeIfPresent(Bool.self, forKey: .canEditValue) ?? false
        canReceiveOverdue = try container.decodeIfPresent(Bool.self, forKey: .canReceiveOverdue) ?? false
        isRegisterValid = try container.decodeIfPresent(Bool.self, forKey: .isRegisterValid) ?? false
        amount = try container.decodeIfPresent(Double.self, forKey: .amount)
        paymentValue = try container.decodeIfPresent(Double.self, forKey: .paymentValue)
        interest = try container.decodeIfPresent(Double.self, forKey: .interest)
        discount = try container.decodeIfPresent(Double.self, forKey: .discount)
        dateSearch = try container.decodeIfPresent(String.self, forKey: .dateSearch)
    }
}
