import Core
import CorePayment

public struct BilletPaymentExtraParams: Decodable {
    public let type: PaymentType
    public let date: String
}
