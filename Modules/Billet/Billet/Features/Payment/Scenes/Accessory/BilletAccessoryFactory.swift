import Foundation
import UIKit

public enum BilletAccessoryFactory {
    public static func make(with model: BilletResponse, andDescription description: String? = nil) -> BilletAccessoryViewController {
        let container = DependencyContainer()
        let coordinator: BilletAccessoryCoordinating = BilletAccessoryCoordinator()
        let presenter: BilletAccessoryPresenting = BilletAccessoryPresenter(coordinator: coordinator)
        let interactor = BilletAccessoryInteractor(presenter: presenter,
                                                   dependencies: container,
                                                   billet: model,
                                                   description: description)
        let viewController = BilletAccessoryViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
