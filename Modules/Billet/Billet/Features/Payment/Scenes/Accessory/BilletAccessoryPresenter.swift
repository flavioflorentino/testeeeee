import Core
import Foundation
import UI
import UIKit

public protocol BilletAccessoryPresenting: AnyObject {
    var viewController: BilletAccessoryDisplaying? { get set }
    func didNextStep(action: BilletAccessoryAction)
    func presentFillablePopup(withAmount amount: Double, andLimit limit: Double)
    func presentBilletInfo(withCardFee cardFee: Double, details: [AccessoryDetailViewModeling], instructions: String)
    func openCalendar(from minDate: Date?, to maxDate: Date?)
    func present(schedulingDate date: Date)
    func present(alert: BilletAlert)
    func present(paymentExtraParams params: BilletPaymentExtraParams)
    func presentUpdatedDetails(_ details: [AccessoryDetailViewModeling])
}

final class BilletAccessoryPresenter {
    private let coordinator: BilletAccessoryCoordinating
    weak var viewController: BilletAccessoryDisplaying?

    init(coordinator: BilletAccessoryCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - BilletAccessoryPresenting
extension BilletAccessoryPresenter: BilletAccessoryPresenting {
    func didNextStep(action: BilletAccessoryAction) {
        coordinator.perform(action: action)
    }
    
    func presentFillablePopup(withAmount amount: Double, andLimit limit: Double) {
        viewController?.displayFillablePopup(amount: amount, limit: limit)
    }
    
    func presentBilletInfo(withCardFee cardFee: Double, details: [AccessoryDetailViewModeling], instructions: String) {
        let instructions = format(text: instructions,
                                  defaultFont: .caption(),
                                  highlightedFont: .caption(.highlight),
                                  defaultColor: Colors.grayscale700.color,
                                  highlightedColor: Colors.grayscale700.color,
                                  textAlignment: .left)
        
        viewController?.displayBilletInfo(details: details, instructions: instructions)
        viewController?.updateCardFee(cardFee)
    }
    
    func openCalendar(from minDate: Date?, to maxDate: Date?) {
        viewController?.displayCalendar(from: minDate, to: maxDate)
    }
    
    func present(schedulingDate date: Date) {
        let date = Date.ddMMyyyy.string(from: date)
        viewController?.display(schedulingDate: date)
    }
    
    func present(alert: BilletAlert) {
        switch alert {    
        case .overdueScheduling:
            viewController?.displayOverdueSchedulingAlert(alert)
            
        case .paymentTimeLimitExceeded:
            viewController?.displayPaymentTimeExceededAlert(alert)
            
        default:
            break
        }
    }
    
    func present(paymentExtraParams params: BilletPaymentExtraParams) {
        viewController?.sendExtraParamsToContainerPayment(params)
    }
    
    func presentUpdatedDetails(_ details: [AccessoryDetailViewModeling]) {
        viewController?.displayUpdatedDetails(details)
    }
}

private extension BilletAccessoryPresenter {
    private func format(text: String,
                        defaultFont: Typography,
                        highlightedFont: Typography,
                        defaultColor: UIColor,
                        highlightedColor: UIColor,
                        textAlignment: NSTextAlignment = .center,
                        hasUnderline: Bool = false) -> NSAttributedString {
        let formattedString = text
            .replacingOccurrences(of: "<br>", with: "\n")
            .replacingOccurrences(of: "**", with: "[b]")
            .replacingOccurrences(of: "<strong>", with: "[b]")
            .replacingOccurrences(of: "</strong>", with: "[b]")
        
        return formattedString.attributedStringWith(
            normalFont: defaultFont.font(),
            highlightFont: highlightedFont.font(),
            normalColor: defaultColor,
            highlightColor: highlightedColor,
            textAlignment: textAlignment,
            underline: hasUnderline,
            separator: "[b]"
        )
    }
}
