import UI

public protocol AccessoryDetailViewModeling {
    var description: String { get }
    var value: String { get }
    var type: AccessoryDetailType { get }
}

public enum AccessoryDetailType {
    case common(String?)
    case date(String?, Bool)
    case installments(Int, Double?)
    case fee(Double?, FeeType)
    case totalValue(Double?)
    
    var value: String? {
        switch self {
        case .common(let value):
            return value
        
        case .date(let value, _):
            return value
        
        case let .installments(installments, value):
            guard let currencyValue = value?.toCurrencyString() else { return nil }
            return String("\(installments)x \(currencyValue)")
        
        case .fee(let value, _), .totalValue(let value):
            return value?.toCurrencyString()
        }
    }
}

public struct AccessoryDetailViewModel: AccessoryDetailViewModeling {
    public private(set) var description: String
    public private(set) var value: String
    public private(set) var type: AccessoryDetailType
    
    init?(description: String, type: AccessoryDetailType) {
        guard let value = type.value else { return nil }
        self.description = description
        self.value = value
        self.type = type
    }
}
