import AssetsKit
import SnapKit
import UI
import UIKit

protocol AccessoryDetailCellDelegate: AnyObject {
    func didTapInfoButton(type: AccessoryDetailType)
}

final class AccessoryDetailCell: UITableViewCell {
    // MARK: - Outlets
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textAlignment, .left)
        label.setContentHuggingPriority(.defaultLow, for: .horizontal)
        return label
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textAlignment, .right)
        label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        return label
    }()
    
    private lazy var infoButton: UIButton = {
        let button = UIButton()
        button.isHidden = true
        button.setImage(Resources.Icons.icoCircleInfo.image, for: .normal)
        button.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return button
    }()
        
    // MARK: - Properties
    
    private weak var delegate: AccessoryDetailCellDelegate?
    private var viewModel: AccessoryDetailViewModeling?
    
    // MARK: - Initialization
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
     
        buildLayout()
    }
    
    func configure(with viewModel: AccessoryDetailViewModeling, and delegate: AccessoryDetailCellDelegate) {
        self.viewModel = viewModel
        self.delegate = delegate
        
        buildStyle()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setup
    
    func buildStyle() {
        guard let viewModel = viewModel else { return }
        
        descriptionLabel.text = viewModel.description
        valueLabel.text = viewModel.value
        
        switch viewModel.type {
        case .common, .installments:
            descriptionLabel.textColor = Colors.grayscale500.color
            valueLabel.textColor = Colors.grayscale500.color
            
        case .date(_, let isOverdue):
            descriptionLabel.textColor = Colors.grayscale500.color
            valueLabel.textColor = isOverdue ? Colors.critical900.color : Colors.grayscale500.color
            
        case .fee:
            infoButton.isHidden = false
            descriptionLabel.textColor = Colors.grayscale500.color
            valueLabel.textColor = Colors.grayscale500.color
            
        case .totalValue:
            descriptionLabel.textColor = Colors.grayscale750.color
            valueLabel.textColor = Colors.grayscale700.color
            viewStyle(BackgroundViewStyle(color: .backgroundSecondary()))
        }
    }
    
    @objc
    private func buttonTapped() {
        guard let viewModel = viewModel else { return }
        delegate?.didTapInfoButton(type: viewModel.type)
    }
}

// MARK: - ViewConfiguration

extension AccessoryDetailCell: ViewConfiguration {
    func buildViewHierarchy() {
        stackView.addArrangedSubview(descriptionLabel)
        stackView.addArrangedSubview(valueLabel)
        stackView.addArrangedSubview(infoButton)
        contentView.addSubview(stackView)
    }
    
    func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        infoButton.snp.makeConstraints {
            $0.size.equalTo(Sizing.base02)
        }
    }
    
    func configureViews() {
        selectionStyle = .none
        backgroundColor = .clear
    }
}
