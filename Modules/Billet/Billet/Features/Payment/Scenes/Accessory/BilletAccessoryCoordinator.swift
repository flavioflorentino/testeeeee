import Foundation
import UI
import UIKit

public enum BilletAccessoryAction: Equatable {
    case dismiss
}

protocol BilletAccessoryCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: BilletAccessoryAction)
}

final class BilletAccessoryCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - BilletAccessoryCoordinating

extension BilletAccessoryCoordinator: BilletAccessoryCoordinating {
    func perform(action: BilletAccessoryAction) {
        guard case .dismiss = action else { return }
        viewController?.dismiss(animated: true)
    }
}
