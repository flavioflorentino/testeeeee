import CorePayment
import Foundation
import SnapKit
import UI
import UIKit

public protocol BilletAccessoryDisplaying: AnyObject {
    func displayFillablePopup(amount: Double, limit: Double)
    func displayBilletInfo(details: [AccessoryDetailViewModeling], instructions: NSAttributedString)
    func displayCalendar(from minDate: Date?, to maxDate: Date?)
    func display(schedulingDate date: String)
    func displayOverdueSchedulingAlert(_ alert: BilletAlert)
    func displayPaymentTimeExceededAlert(_ alert: BilletAlert)
    func updateCardFee(_ cardFee: Double)
    func sendExtraParamsToContainerPayment(_ params: Decodable)
    func displayUpdatedDetails(_ details: [AccessoryDetailViewModeling])
}

private extension BilletAccessoryViewController.Layout {
    enum Content {
        static let estimatedRowHeight: CGFloat = 20
        static let contentInset = UIEdgeInsets.zero
    }
}

public final class BilletAccessoryViewController: ViewController<BilletAccessoryInteracting, UIView>, PaymentAccessoryInput {
    fileprivate struct Layout { }
    
    // MARK: - Outlets
    
    private lazy var schedulingView = AccessorySchedulingView(date: Date(), delegate: self)
    
    private lazy var separatorView = SeparatorView(style: BackgroundViewStyle(color: .grayscale050()))
    
    private lazy var detailsTableView: UITableView = {
        let tableView = UITableView()
        tableView.register(AccessoryDetailCell.self, forCellReuseIdentifier: AccessoryDetailCell.identifier)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = Layout.Content.estimatedRowHeight
        tableView.contentInset = Layout.Content.contentInset
        tableView.separatorStyle = .none
        tableView.bounces = false
        return tableView
    }()
    
    private lazy var dataSource: TableViewDataSource<Int, AccessoryDetailViewModeling> = {
        let dataSource = TableViewDataSource<Int, AccessoryDetailViewModeling>(view: detailsTableView)
        dataSource.itemProvider = { tableView, indexPath, viewModel -> UITableViewCell? in
            let cellIdentifier = AccessoryDetailCell.identifier
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? AccessoryDetailCell
            cell?.configure(with: viewModel, and: self)
            
            return cell
        }
        
        dataSource.add(items: [], to: 0)
        
        return dataSource
    }()
    
    private lazy var separatorView2 = SeparatorView(style: BackgroundViewStyle(color: .grayscale050()))
    
    private lazy var instructionsView = AccessoryInstructionsView()
    
    // MARK: - Properties

    public weak var delegate: PaymentAccessoryOutput?
    
    private var method: PaymentMethod?
    private var cardFee: Double = 0
    private let transitioning = PresentTransitioning()

    // MARK: - Lifecycle
    
    // swiftlint:disable modifier_order
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate?.showLoadingView()
        interactor.fetchBilletInfo()
    }
    
    // MARK: - ViewConfiguration

    public override func buildViewHierarchy() {
        view.addSubview(schedulingView)
        view.addSubview(separatorView)
        view.addSubview(detailsTableView)
        view.addSubview(separatorView2)
        view.addSubview(instructionsView)
    }
    
    public override func setupConstraints() {
        schedulingView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        separatorView.snp.makeConstraints {
            $0.top.equalTo(schedulingView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        detailsTableView.snp.makeConstraints {
            $0.top.equalTo(separatorView.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview()
        }
        
        separatorView2.snp.makeConstraints {
            $0.top.equalTo(detailsTableView.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        instructionsView.snp.makeConstraints {
            $0.top.equalTo(separatorView2.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.greaterThanOrEqualToSuperview().inset(Spacing.base02)
        }
    }

    public override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        detailsTableView.backgroundColor = .clear
        detailsTableView.dataSource = dataSource
    }
    // swiftlint:enable modifier_order
}

// MARK: - PaymentAccessoryInput

public extension BilletAccessoryViewController {
    func changePaymentMethod(method: PaymentMethod) {
        self.method = method
        delegate?.accessoryUpdateCardFee(cardFee: method != .accountBalance ? cardFee : 0)
    }
    
    func changeAccessory(with values: AccessoryValue) {
        interactor.updateDetails(with: values)
    }
}

// MARK: - BilletAccessoryDisplaying

extension BilletAccessoryViewController: BilletAccessoryDisplaying {
    public func displayFillablePopup(amount: Double, limit: Double) {
        let popup = FillableAmountPopup(amount: amount, limit: limit) { amount in
            self.delegate?.accessoryUpdateValue(value: amount)
        }
    
        let popupPresenter = PopupPresenter(transitioningDelegate: transitioning)
        popupPresenter.showPopup(popup, fromViewController: self)
    }
    
    public func displayBilletInfo(details: [AccessoryDetailViewModeling], instructions: NSAttributedString) {
        dataSource.update(items: details, from: 0)
        
        instructionsView.setup(withInstructions: instructions)
        
        delegate?.hideLoadingView()
    }
    
    public func displayCalendar(from minDate: Date?, to maxDate: Date?) {
        let popup = DatePickerPopup(title: Strings.DatePickerPopup.title,
                                    message: Strings.DatePickerPopup.message,
                                    minDate: minDate,
                                    maxDate: maxDate) { date in
            self.interactor.validate(schedulingDate: date)
        }
        
        let popupPresenter = PopupPresenter(transitioningDelegate: PresentTransitioning())
        popupPresenter.showPopup(popup, fromViewController: self)
    }
    
    public func display(schedulingDate date: String) {
        schedulingView.update(date: date)
        delegate?.update(paymentType: .scheduling)
    }
    
    public func displayOverdueSchedulingAlert(_ alert: BilletAlert) {
        let understoodAction = PopupAction(title: Strings.Common.understood, style: .fill) {
            self.schedulingView.resetCheckbox()
        }
        presentPopup(with: alert, actions: [understoodAction])
    }
    
    public func displayPaymentTimeExceededAlert(_ alert: BilletAlert) {
        let continueAction = PopupAction(title: Strings.Common.continue, style: .fill) {
            self.schedulingView.disableCurrentDate()
            self.interactor.scheduleAPaymentToNextBusinessDay()
        }
        presentPopup(with: alert, actions: [continueAction])
    }
    
    public func updateCardFee(_ cardFee: Double) {
        self.cardFee = cardFee
        
        guard let method = method else { return }
        delegate?.accessoryUpdateCardFee(cardFee: method != .accountBalance ? cardFee : 0)
    }
    
    public func sendExtraParamsToContainerPayment(_ params: Decodable) {
        delegate?.accessoryUpdatePaymentParams(params)
    }
    
    public func displayUpdatedDetails(_ details: [AccessoryDetailViewModeling]) {
        dataSource.update(items: details, from: 0)
    }
}

// MARK: - AccessorySchedulingViewDelegate

extension BilletAccessoryViewController: AccessorySchedulingViewDelegate {
    func didTapCurrentDate(_ date: Date) {
        delegate?.update(paymentType: .default)
        interactor.validate(schedulingDate: date)
    }
    
    func didTapChooseDate() {
        interactor.scheduleAPayment()
    }
}

// MARK: - AccessoryDetailViewDelegate

extension BilletAccessoryViewController: AccessoryDetailCellDelegate {
    func didTapInfoButton(type: AccessoryDetailType) {
        guard case .fee(_, let feeType) = type else { return }

        let popup = FeePopup(value: type.value, type: feeType)
        PopupPresenter().showPopup(popup)
        
        interactor.trackFeeTypeViewed(feeType)
    }
}

// MARK: - Private methods

private extension BilletAccessoryViewController {
    func presentPopup(with alert: BilletAlert,
                      actions: [PopupAction]) {
        let popup = PopupViewController(title: alert.title, preferredType: .image(alert.icon))
        popup.hideCloseButton = true
        
        popup.setAttributeDescription(NSMutableAttributedString(attributedString: alert.formattedMessage))
        
        actions.forEach {
            popup.addAction($0)
        }
    
        PopupPresenter().showPopup(popup)
    }
}
