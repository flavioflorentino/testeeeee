import AnalyticsModule
import AssetsKit
import Core
import CorePayment
import FeatureFlag
import Foundation

public protocol BilletAccessoryInteracting: AnyObject {
    func fetchBilletInfo()
    func updateDetails(with values: AccessoryValue)
    func scheduleAPayment()
    func scheduleAPaymentToNextBusinessDay()
    func validate(schedulingDate: Date)
    func dismiss()
    func trackFeeTypeViewed(_ feeType: FeeType)
}

final class BilletAccessoryInteractor {
    typealias Localizable = Strings.BilletAccessory
    
    typealias Dependencies = HasAnalytics & HasFeatureManager
    private let dependencies: Dependencies

    private let presenter: BilletAccessoryPresenting
    private let billet: BilletResponse
    private let description: String?
    
    private var dueDate: Date?
    private var installment = 1

    init(presenter: BilletAccessoryPresenting,
         dependencies: Dependencies,
         billet: BilletResponse,
         description: String? = nil) {
        self.presenter = presenter
        self.dependencies = dependencies
        self.billet = billet
        self.description = description
    }
}

// MARK: - BilletAccessoryInteracting
extension BilletAccessoryInteractor: BilletAccessoryInteracting {
    func fetchBilletInfo() {
        dueDate = Date.ddMMyyyy.date(from: billet.dueDate ?? "")
        
        presentFillablePopupIfNeeded()
        presentPaymentTimeExceededPopupIfNeeded()
        updatePaymentWithExtraParamsIfNeeded(type: .default, date: Date().yyyyMMdd())

        let instructions = billet.instructions ?? dependencies.featureManager.text(.opsBilletPaymentInstructionsString)
        
        presenter.presentBilletInfo(withCardFee: billet.surcharge,
                                    details: createViewModels(),
                                    instructions: instructions)
    }
   
    func updateDetails(with values: AccessoryValue) {
        installment = values.installment

        var viewModels = [AccessoryDetailViewModeling?]()
    
        if !values.cardFeeValue.isZero {
            let cardFeeViewModel = AccessoryDetailViewModel(description: Localizable.Details.cardFee,
                                                            type: .fee(values.cardFeeValue, .convenience))
            viewModels.append(cardFeeViewModel)
        }
        
        if values.installment > 1 {
            let installmentFeeViewModel = AccessoryDetailViewModel(description: Localizable.Details.installmentFee,
                                                                   type: .fee(values.installmentFeeValue, .installment))
            viewModels.append(installmentFeeViewModel)
            
            let installmentValueViewModel = AccessoryDetailViewModel(description: Localizable.Details.installments,
                                                                     type: .installments(values.installment, values.installmentValue))
            viewModels.append(installmentValueViewModel)
        }
        
        viewModels = createViewModels(otherViewModels: viewModels)
        
        presenter.presentUpdatedDetails(viewModels.compactMap { $0 })
    }
    
    func scheduleAPayment() {
        if let cip = billet.cip, cip.isOverdue, cip.canReceiveOverdue || cip.discount != nil || cip.interest != nil {
            presenter.present(alert: .overdueScheduling)
            return
        }
        
        let minDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())
        let maxDate = dueDate
        
        presenter.openCalendar(from: minDate, to: maxDate)
        
        dependencies.analytics.log(PaymentEvent.billetSchedulingCalendarViewed)
    }
    
    func scheduleAPaymentToNextBusinessDay() {
        guard let date = billet.minSchedulePaymentDate, let schedulingDate = Date.ddMMyyyy.date(from: date) else {
            return validate(schedulingDate: Date.getNextBusinessDay)
        }
    
        validate(schedulingDate: schedulingDate)
    }
    
    func validate(schedulingDate: Date) {
        if Calendar.current.isDateInToday(schedulingDate) {
            updatePaymentWithExtraParamsIfNeeded(type: .default, date: schedulingDate.yyyyMMdd())
        } else {
            updatePaymentWithExtraParamsIfNeeded(type: .scheduling, date: schedulingDate.yyyyMMdd())
            presenter.present(schedulingDate: schedulingDate)
        }
    }
    
    func dismiss() {
        presenter.didNextStep(action: .dismiss)
    }
    
    func trackFeeTypeViewed(_ feeType: FeeType) {
        let event: PaymentEvent
        
        switch feeType {
        case .convenience:
            event = .billetCardFeeInformation
        case .discount:
            event = .billetDiscountInformationViewed
        case .interest:
            event = .billetInterestInformationViewed
        case .installment:
            event = .billetInstallmentFeeInformation(installment: installment)
        }
        
        dependencies.analytics.log(event)
    }
}

private extension BilletAccessoryInteractor {
    func presentFillablePopupIfNeeded() {
        guard billet.isFillable, let amount = Double(billet.amount), let limitAmount = billet.cip?.amount else { return }
        presenter.presentFillablePopup(withAmount: amount, andLimit: limitAmount)
    }
    
    func presentPaymentTimeExceededPopupIfNeeded() {
        guard billet.isPaymentTimeLimitExceeded, let paymentTimeLimit = billet.paymentTimeLimit else { return }
        presenter.present(alert: .paymentTimeLimitExceeded(timeLimit: paymentTimeLimit))
    }
    
    func updatePaymentWithExtraParamsIfNeeded(type: PaymentType, date: String) {
        let params = BilletPaymentExtraParams(type: type, date: date)
        presenter.present(paymentExtraParams: params)
    }
    
    func createViewModels(otherViewModels: [AccessoryDetailViewModeling?] = []) -> [AccessoryDetailViewModeling] {
        guard let cip = billet.cip else { return [] }
        
        var viewModels = [AccessoryDetailViewModeling?]()
        
        let dueDateViewModel = AccessoryDetailViewModel(description: Localizable.Details.dueDate, type: .date(billet.dueDate, cip.isOverdue))
        viewModels.append(dueDateViewModel)
        
        let descriptionViewModel = AccessoryDetailViewModel(description: Localizable.Details.description, type: .common(description))
        viewModels.append(descriptionViewModel)
        
        let beneficiaryViewModel = AccessoryDetailViewModel(description: Localizable.Details.beneficiary, type: .common(billet.beneficiary))
        viewModels.append(beneficiaryViewModel)
        
        let interestViewModel = AccessoryDetailViewModel(description: Localizable.Details.interest, type: .fee(cip.interest, .interest))
        viewModels.append(interestViewModel)
        
        let discountViewModel = AccessoryDetailViewModel(description: Localizable.Details.discount, type: .fee(cip.discount, .discount))
        viewModels.append(discountViewModel)

        viewModels.append(contentsOf: otherViewModels)
    
        return viewModels.compactMap { $0 }
    }
}
