import FeatureFlag
import Foundation
import SnapKit
import UI
import UIKit

protocol AccessorySchedulingViewDelegate: AnyObject {
    func didTapCurrentDate(_ date: Date)
    func didTapChooseDate()
}

extension AccessorySchedulingView.Layout {
    enum Size {
        static let checkbox = CGSize(width: 24, height: 24)
    }
}

final class AccessorySchedulingView: UIView {
    fileprivate struct Layout { }
    
    typealias Localizable = Strings.BilletAccessory.Scheduling
    
    // MARK: - Outlets
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var currentDateStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var currentDateCheckbox: CheckboxButton = {
        let checkbox = CheckboxButton()
        checkbox.isSelected = true
        checkbox.tintColor = Colors.branding600.color
        checkbox.addTarget(self, action: #selector(checkboxTapped(_:)), for: .valueChanged)
        return checkbox
    }()
    
    private lazy var currentDateDescriptionLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.payToday
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale500())
            .with(\.textAlignment, .left)
        return label
    }()
    
    private lazy var currentDateLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale500())
            .with(\.textAlignment, .right)
        return label
    }()
    
    private lazy var chooseDateStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var chooseDateCheckbox: CheckboxButton = {
        let checkbox = CheckboxButton()
        checkbox.tintColor = Colors.branding600.color
        checkbox.addTarget(self, action: #selector(checkboxTapped(_:)), for: .valueChanged)
        return checkbox
    }()
    
    private lazy var chooseDateDescriptionLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.schedule
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale500())
            .with(\.textAlignment, .left)
        return label
    }()
    
    private lazy var chooseDateButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.chooseDate, for: .normal)
        button.buttonStyle(LinkButtonStyle())
            .with(\.textColor, (.branding600(), .normal))
            .with(\.contentHorizontalAlignment, .right)
        button.addTarget(self, action: #selector(chooseDateTapped), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Properties
    
    private let date: Date
    
    private unowned let delegate: AccessorySchedulingViewDelegate
    
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    // MARK: - Initialization
    
    init(date: Date, delegate: AccessorySchedulingViewDelegate, dependencies: Dependencies = DependencyContainer()) {
        self.date = date
        self.delegate = delegate
        self.dependencies = dependencies
        
        super.init(frame: .zero)
        
        currentDateLabel.text = Date.ddMMyyyy.string(from: date)
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(date: String) {
        chooseDateButton.setTitle(date, for: .normal)
        chooseDateButton.buttonStyle(LinkButtonStyle())
            .with(\.textColor, (.branding600(), .normal))
            .with(\.contentHorizontalAlignment, .right)
    }
    
    func disableCurrentDate() {
        currentDateStackView.isHidden = true
        chooseDateCheckbox.isHidden = true
    }
    
    func resetCheckbox() {
        checkboxTapped(currentDateCheckbox)
    }
}

@objc
private extension AccessorySchedulingView {
    func checkboxTapped(_ sender: CheckboxButton) {
        guard dependencies.featureManager.isActive(.releaseBilletPaymentSchedulePresentingBool) else { return }

        switch sender {
        case currentDateCheckbox:
            if chooseDateCheckbox.isSelected {
                update(date: Localizable.chooseDate)
                delegate.didTapCurrentDate(date)
            }
            
            chooseDateCheckbox.isSelected = false
            currentDateCheckbox.isSelected = !chooseDateCheckbox.isSelected
            
        case chooseDateCheckbox:
            currentDateCheckbox.isSelected = false
            chooseDateCheckbox.isSelected = !currentDateCheckbox.isSelected
            
        default:
            break
        }
    }
    
    func chooseDateTapped() {
        checkboxTapped(chooseDateCheckbox)
        delegate.didTapChooseDate()
    }
}

// MARK: - ViewConfiguration

extension AccessorySchedulingView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(stackView)
        stackView.addArrangedSubview(currentDateStackView)

        if dependencies.featureManager.isActive(.releaseBilletPaymentSchedulePresentingBool) {
            stackView.addArrangedSubview(chooseDateStackView)
            currentDateStackView.addArrangedSubview(currentDateCheckbox)
            chooseDateStackView.addArrangedSubview(chooseDateCheckbox)
            chooseDateStackView.addArrangedSubview(chooseDateDescriptionLabel)
            chooseDateStackView.addArrangedSubview(chooseDateButton)
        }
        
        currentDateStackView.addArrangedSubview(currentDateDescriptionLabel)
        currentDateStackView.addArrangedSubview(currentDateLabel)
    }
    
    func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        guard dependencies.featureManager.isActive(.releaseBilletPaymentSchedulePresentingBool) else {
            return
        }
        
        currentDateCheckbox.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.checkbox)
        }
        
        chooseDateCheckbox.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.checkbox)
        }
    }
}
