import Foundation
import SnapKit
import UI
import UIKit

final class AccessoryInstructionsView: UIView {
    // MARK: - Outlets
    
    private lazy var instructionsLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
        return label
    }()
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(withInstructions instructions: NSAttributedString) {
        instructionsLabel.attributedText = instructions
    }
}

// MARK: - ViewConfiguration

extension AccessoryInstructionsView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(instructionsLabel)
    }
    
    func setupConstraints() {
        instructionsLabel.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(Spacing.base01)
        }
    }
    
    func configureViews() {
        viewStyle(BackgroundViewStyle(color: .backgroundSecondary()))
            .with(\.cornerRadius, .light)
    }
}
