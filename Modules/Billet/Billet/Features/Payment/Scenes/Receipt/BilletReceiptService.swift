import Core
import ReceiptKit

public struct BilletReceiptService: DefaultReceiptServicing {
    public let entryPoint: ReceiptEntryPoint
    
    public typealias Dependencies = HasMainQueue
    public let dependencies: Dependencies
    
    public init(entryPoint: ReceiptEntryPoint, dependencies: Dependencies) {
        self.entryPoint = entryPoint
        self.dependencies = dependencies
    }
    
    public func fetchReceipt(completion: @escaping (Result<ReceiptModel, ApiError>) -> Void) {
        let endpoint = BilletReceiptEndpoint.fetchReceipt(billId: entryPoint.transactionId)
        let api = Api<ReceiptModel>(endpoint: endpoint)
        
        api.execute { result in
            self.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
