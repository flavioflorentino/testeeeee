import Core

enum BilletReceiptEndpoint {
    case fetchReceipt(billId: String)
}

extension BilletReceiptEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .fetchReceipt(let billId):
            return "/bills/receipts/\(billId)"
        }
    }
    
    var method: HTTPMethod {
        .post
    }
}
