import UI
import UIKit
import SnapKit

final class HubSectionView: UITableViewHeaderFooterView {
    // MARK: - Views
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
        return label
    }()

    // MARK: - Initialization
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(withTitle title: String) {
        titleLabel.text = title
    }
}

// MARK: - ViewConfiguration

extension HubSectionView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(titleLabel)
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
        }
    }
    
    func configureViews() {
        tintColor = Colors.backgroundPrimary.color
    }
}
