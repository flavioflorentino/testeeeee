import Foundation

protocol BilletHubPresenting: AnyObject {
    var viewController: BilletHubDisplaying? { get set }
    func present(models: [HubModel])
    func didNextStep(action: BilletHubAction)
}

final class BilletHubPresenter {
    private let coordinator: BilletHubCoordinating
    weak var viewController: BilletHubDisplaying?
    
    init(coordinator: BilletHubCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - BilletHubPresenting
extension BilletHubPresenter: BilletHubPresenting {
    func present(models: [HubModel]) {
        var infos: [(section: String, cells: [HubCellType])] = []
        
        models.forEach { model in
            let info: (section: String, cells: [HubCellType])
            
            switch model.contentType {
            case .banners(let banners):
                info = (section: model.title, cells: format(banners: banners))
            case .options(let options):
                info = (section: model.title, cells: format(options: options))
            }
            
            infos.append(info)
        }
        
        viewController?.display(infos: infos)
    }
    
    private func format(options: [HubOption]) -> [HubCellType] {
        let options: [HubCellType] = options
            .map {
                .option(HubOptionCellViewModel(icon: $0.icon, title: $0.title))
            }
        
        let cells: [HubCellType] = (0 ..< 2 * options.count - 1).map { $0.isMultiple(of: 2) ? options[$0 / 2] : .spacing }
        
        return cells
    }
    
    private func format(banners: [HubBanner]) -> [HubCellType] {
        let banners: [HubCellType] = banners
            .map {
                .banner(HubBannerCellViewModel(title: $0.title, description: $0.description, actionTitle: $0.actionTitle))
            }
        
        let cells: [HubCellType] = (0 ..< 2 * banners.count - 1).map { $0.isMultiple(of: 2) ? banners[$0 / 2] : .spacing }
        
        return cells
    }
    
    func didNextStep(action: BilletHubAction) {
        coordinator.perform(action: action)
    }
}
