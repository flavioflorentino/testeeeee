import AnalyticsModule
import AssetsKit
import FeatureFlag
import Foundation

protocol BilletHubInteracting: AnyObject {
    func loadInformation()
    func selectedItem(at indexPath: IndexPath)
    func openDeeplink(with indexPath: IndexPath)
    func openFaq()
    func close()
}

final class BilletHubInteractor {
    private typealias Localizable = Strings.BilletHub

    typealias Dependencies = HasFeatureManager & HasAnalytics
    private let dependencies: Dependencies
    private let presenter: BilletHubPresenting
    private let origin: BilletOrigin
    
    private var models: [HubModel] = []

    init(presenter: BilletHubPresenting, dependencies: Dependencies, origin: BilletOrigin) {
        self.presenter = presenter
        self.dependencies = dependencies
        self.origin = origin
    }
}

// MARK: - BilletHubInteracting
extension BilletHubInteractor: BilletHubInteracting {
    func loadInformation() {
        let options = [
            HubOption(icon: Resources.Icons.icoScanner.image, title: Localizable.Options.scannerTitle),
            HubOption(icon: Resources.Icons.icoLinecode.image, title: Localizable.Options.linecodeTitle)
        ]
      
        let banners = [
            HubBanner(title: Localizable.Banner.title,
                      description: Localizable.Banner.description,
                      actionTitle: Localizable.Banner.actionTitle,
                      deeplink: dependencies.featureManager.text(.opsFaqHowToPayBilletUrl))
        ]

        let models = [
            HubModel(title: Localizable.Options.section, contentType: .options(options)),
            HubModel(title: Localizable.Banner.section, contentType: .banners(banners))
        ]
        
        self.models = models
        presenter.present(models: models)
    }
    
    func selectedItem(at indexPath: IndexPath) {
        switch (indexPath.section, indexPath.row) {
        case (0, 0):
            presenter.didNextStep(action: .scanner(origin: origin))
            trackActionSelected(action: .scanner)
        case (0, 2):
            presenter.didNextStep(action: .linecode(origin: origin))
            trackActionSelected(action: .enterManually)
        case (1, _):
            openDeeplink(with: indexPath)
        default:
            break
        }
    }
    
    func openDeeplink(with indexPath: IndexPath) {
        guard
            let model = models[safe: indexPath.section],
            case .banners(let banners) = model.contentType,
            let banner = banners[safe: indexPath.row]
        else { return }

        presenter.didNextStep(action: .faq(url: banner.deeplink))
        trackActionSelected(action: .howToPayCard)
    }
    
    func openFaq() {
        let url = dependencies.featureManager.text(.billetHubFaqUrl)
        presenter.didNextStep(action: .faq(url: url))
        trackActionSelected(action: .knowMore)
    }
    
    func close() {
        presenter.didNextStep(action: .dismiss)
    }
}

private extension BilletHubInteractor {
    func trackActionSelected(action: HubActionSelected) {
        let event = PaymentEvent.hubActionSelected(action)
        dependencies.analytics.log(event)
    }
}
