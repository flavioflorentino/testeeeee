import UIKit

enum HubContentType {
    case options([HubOption])
    case banners([HubBanner])
}

struct HubModel {
    let title: String
    let contentType: HubContentType
}

struct HubOption {
    let icon: UIImage
    let title: String
}

struct HubBanner {
    let title: String
    let description: String
    let actionTitle: String
    let deeplink: String
}
