import UIKit

public enum BilletHubFactory {
    public static func make(with origin: BilletOrigin) -> UIViewController {
        let container = DependencyContainer()
        let coordinator: BilletHubCoordinating = BilletHubCoordinator(dependencies: container)
        let presenter: BilletHubPresenting = BilletHubPresenter(coordinator: coordinator)
        let interactor = BilletHubInteractor(presenter: presenter, dependencies: container, origin: origin)
        let viewController = BilletHubViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
