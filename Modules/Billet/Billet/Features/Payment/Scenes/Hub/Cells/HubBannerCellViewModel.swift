import UIKit

protocol HubBannerCellViewModeling {
    var title: String { get }
    var description: String { get }
    var actionTitle: String { get }
}

struct HubBannerCellViewModel: HubBannerCellViewModeling {
    let title: String
    let description: String
    let actionTitle: String
}
