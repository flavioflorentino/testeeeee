enum HubCellType {
    case option(HubOptionCellViewModeling)
    case banner(HubBannerCellViewModeling)
    case spacing
}
