import UIKit

protocol HubOptionCellViewModeling {
    var icon: UIImage { get }
    var title: String { get }
}

struct HubOptionCellViewModel: HubOptionCellViewModeling {
    let icon: UIImage
    let title: String
}
