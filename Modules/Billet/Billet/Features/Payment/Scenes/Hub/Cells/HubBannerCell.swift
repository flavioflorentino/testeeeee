import UI
import UIKit
import AssetsKit
import SnapKit

protocol HubBannerCellDelegate: AnyObject {
    func didTouchDeeplink(_ indexPath: IndexPath)
}

final class HubBannerCell: UITableViewCell {
    // MARK: - Views
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
        return label
    }()
    
    private lazy var deeplinkButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(didTouchDeeplink), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Properties
    
    private var indexPath: IndexPath?
    private weak var delegate: HubBannerCellDelegate?

    // MARK: - Initialization
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
     
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(with viewModel: HubBannerCellViewModeling, indexPath: IndexPath, and delegate: HubBannerCellDelegate) {
        titleLabel.text = viewModel.title
        descriptionLabel.text = viewModel.description
        deeplinkButton.setTitle(viewModel.actionTitle, for: .normal)
        deeplinkButton.buttonStyle(LinkButtonStyle())
        self.indexPath = indexPath
        self.delegate = delegate
    }
    
    @objc
    private func didTouchDeeplink() {
        guard let indexPath = indexPath else { return }
        delegate?.didTouchDeeplink(indexPath)
    }
}

// MARK: - ViewConfiguration

extension HubBannerCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(deeplinkButton)
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.leading.equalToSuperview().inset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base03)
        }

        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base05)
        }

        deeplinkButton.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base02)
        }
    }
    
    func configureViews() {
        viewStyle(CardViewStyle())
            .with(\.backgroundColor, .backgroundPrimary())
        selectionStyle = .none
    }
}
