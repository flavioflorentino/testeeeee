import UI
import UIKit
import SnapKit

final class HubSpacingCell: UITableViewCell {
    fileprivate enum Layout { }
    
    // MARK: - Views
    
    private lazy var spacingView = UIView()

    // MARK: - Initialization
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
     
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - ViewConfiguration

extension HubSpacingCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(spacingView)
    }
    
    func setupConstraints() {
        spacingView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.height.equalTo(Sizing.base01)
        }
    }
    
    func configureViews() {
        spacingView.backgroundColor = .clear
        contentView.backgroundColor = .clear
        backgroundColor = .clear
        selectionStyle = .none
    }
}
