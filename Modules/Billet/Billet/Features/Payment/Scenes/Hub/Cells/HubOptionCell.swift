import UI
import UIKit
import AssetsKit
import SnapKit

private extension HubOptionCell.Layout {
    enum Size {
        static let disclosureIndicator = CGSize(width: 20, height: 20)
    }
}

final class HubOptionCell: UITableViewCell {
    fileprivate enum Layout { }
    
    // MARK: - Views
    
    private lazy var iconImage: UIImageView = {
       let imageView = UIImageView()
        imageView.imageStyle(RoundedImageStyle(size: .medium, cornerRadius: .full))
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
        return label
    }()
    
    private lazy var disclosureIndicatorImage: UILabel = {
        let label = UILabel()
        label.labelStyle(IconLabelStyle(type: .medium))
            .with(\.textColor, .branding600())
        label.text = Iconography.angleRightB.rawValue
        return label
    }()

    // MARK: - Initialization
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
     
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(with viewModel: HubOptionCellViewModeling) {
        iconImage.image = viewModel.icon
        titleLabel.text = viewModel.title
    }
}

// MARK: - ViewConfiguration

extension HubOptionCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubviews(iconImage, titleLabel, disclosureIndicatorImage)
    }
    
    func setupConstraints() {
        iconImage.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base04)
            $0.leading.equalTo(iconImage.snp.trailing).offset(Spacing.base02)
        }

        disclosureIndicatorImage.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.disclosureIndicator)
            $0.centerY.equalToSuperview()
            $0.leading.equalTo(titleLabel.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
    }
    
    func configureViews() {
        viewStyle(CardViewStyle())
            .with(\.backgroundColor, .backgroundPrimary())
        selectionStyle = .none
    }
}
