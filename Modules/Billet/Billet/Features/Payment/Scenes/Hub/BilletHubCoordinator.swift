import UIKit

enum BilletHubAction: Equatable {
    case scanner(origin: BilletOrigin)
    case linecode(origin: BilletOrigin)
    case faq(url: String)
    case dismiss
}

protocol BilletHubCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: BilletHubAction)
}

final class BilletHubCoordinator {
    typealias Dependencies = HasLegacy
    private let dependencies: Dependencies

    weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - BilletHubCoordinating
extension BilletHubCoordinator: BilletHubCoordinating {
    func perform(action: BilletHubAction) {
        switch action {
        case .linecode(let origin):
            guard let viewController = viewController else { return }
            
            let formController = BilletFormFactory.make(with: origin)
            viewController.navigationController?.pushViewController(formController, animated: true)
            
        case .scanner(let origin):
            guard let viewController = viewController else { return }
            
            let scannerController = BilletScannerFactory.make(with: origin)
            viewController.navigationController?.pushViewController(scannerController, animated: true)
            
        case .faq(let url):
            guard
                let url = URL(string: url),
                let controller = dependencies.legacy.webview?.createWebView(with: url)
            else { return }
            
            viewController?.present(controller, animated: true)
            
        case .dismiss:
            viewController?.navigationController?.dismiss(animated: true)
        }
    }
}
