import AssetsKit
import SnapKit
import UI
import UIKit

protocol BilletHubDisplaying: AnyObject {
    func display(infos: [(section: String, cells: [HubCellType])])
}

private extension BilletHubViewController.Layout {
    enum Content {
        static let estimatedRowHeight: CGFloat = 200
        static let contentInset = UIEdgeInsets.zero
    }
}

final class BilletHubViewController: ViewController<BilletHubInteracting, UIView> {
    fileprivate enum Layout { }
    
    private typealias Localizable = Strings.BilletHub
    
    // MARK: - Views
    
    private lazy var iconImg: UIImageView = {
        let imageView = UIImageView(image: Resources.Illustrations.iluScanBillet.image)
        imageView.contentMode = .scaleAspectFit
        imageView.imageStyle(RoundedImageStyle(size: .xLarge, cornerRadius: .none))
        return imageView
    }()

    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(headerFooterViewType: HubSectionView.self)
        tableView.register(cellType: HubOptionCell.self)
        tableView.register(cellType: HubSpacingCell.self)
        tableView.register(cellType: HubBannerCell.self)
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.estimatedSectionHeaderHeight = Sizing.base06
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = Layout.Content.estimatedRowHeight
        tableView.contentInset = Layout.Content.contentInset
        tableView.separatorStyle = .none
        tableView.bounces = false
        tableView.backgroundColor = .clear
        return tableView
    }()
    
    private lazy var dataSource: TableViewDataSource<String, HubCellType> = {
        let dataSource = TableViewDataSource<String, HubCellType>(view: tableView)
        dataSource.itemProvider = bindCell(_:_:_:)
        return dataSource
    }()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.loadInformation()
    }
    
    // MARK: - ViewConfiguration

    override func buildViewHierarchy() {
        view.addSubview(iconImg)
        view.addSubview(tableView)
    }
    
    override func setupConstraints() {
        iconImg.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top).offset(Spacing.base03)
            $0.centerX.equalToSuperview()
        }
        
        tableView.snp.makeConstraints {
            $0.top.equalTo(iconImg.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.bottom.equalToSuperview().inset(Spacing.base02)
        }
    }

    override func configureViews() {
        title = Localizable.title
        
        let infoBarButton = UIBarButtonItem(image: Resources.Icons.icoInformation.image,
                                            style: .plain,
                                            target: self,
                                            action: #selector(didTapInfoButton))
        
        let closeBarButton = UIBarButtonItem(image: Resources.Icons.icoClose.image, style: .plain, target: self, action: #selector(didTapCloseButton))
        
        navigationItem.setRightBarButton(infoBarButton, animated: true)
        navigationItem.setLeftBarButton(closeBarButton, animated: true)
        
        view.backgroundColor = Colors.backgroundPrimary.color
        tableView.dataSource = dataSource
        tableView.delegate = self
    }
    
    @objc
    private func didTapInfoButton() {
        interactor.openFaq()
    }
    
    @objc
    private func didTapCloseButton() {
        interactor.close()
    }
}

// MARK: - Private Methods

extension BilletHubViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(HubSectionView.self)
        let title = dataSource.sections[section]
        header?.configure(withTitle: title)
        return header
    }
    
    private func bindCell(_ tableView: UITableView, _ indexPath: IndexPath, _ cellType: HubCellType) -> UITableViewCell {
        switch cellType {
        case .option(let viewModel):
            let cell = tableView.dequeueReusableCell(for: indexPath, cellType: HubOptionCell.self)
            cell.configure(with: viewModel)
            return cell
            
        case .spacing:
            let cell = tableView.dequeueReusableCell(for: indexPath, cellType: HubSpacingCell.self)
            return cell
            
        case .banner(let viewModel):
            let cell = tableView.dequeueReusableCell(for: indexPath, cellType: HubBannerCell.self)
            cell.configure(with: viewModel, indexPath: indexPath, and: self)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        interactor.selectedItem(at: indexPath)
    }
}

// MARK: - BilletHubDisplaying

extension BilletHubViewController: BilletHubDisplaying {
    func display(infos: [(section: String, cells: [HubCellType])]) {
        infos.forEach { info in
            dataSource.add(items: info.cells, to: info.section)
        }
    }
}

// MARK: - HubBannerCellDelegate

extension BilletHubViewController: HubBannerCellDelegate {
    func didTouchDeeplink(_ indexPath: IndexPath) {
        interactor.openDeeplink(with: indexPath)
    }
}
