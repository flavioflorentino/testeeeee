import UIKit

public enum BilletFormFactory {
    public static func make(with origin: BilletOrigin,
                            linecode: String? = nil,
                            description: String? = nil) -> UIViewController {
        let container = DependencyContainer()
        let coordinator: BilletFormCoordinating = BilletFormCoordinator(dependencies: container)
        let service: BilletFormServicing = BilletFormService(dependencies: container)
        let presenter: BilletFormPresenting = BilletFormPresenter(coordinator: coordinator)
        let interactor = BilletFormInteractor(dependencies: container,
                                              service: service,
                                              presenter: presenter,
                                              origin: origin,
                                              linecode: linecode,
                                              description: description)
        let viewController = BilletFormViewController(interactor: interactor)
        
        coordinator.viewController = viewController
        presenter.viewController = viewController
        
        return viewController
    }
    
    static func make(with origin: BilletOrigin, barcode: String) -> UIViewController {
        let container = DependencyContainer()
        let coordinator: BilletFormCoordinating = BilletFormCoordinator(dependencies: container)
        let service: BilletFormServicing = BilletFormService(dependencies: container)
        let presenter: BilletFormPresenting = BilletFormPresenter(coordinator: coordinator)
        let interactor = BilletFormInteractor(dependencies: container,
                                              service: service,
                                              presenter: presenter,
                                              origin: origin,
                                              barcode: barcode)

        let viewController = BilletFormViewController(interactor: interactor)
        
        coordinator.viewController = viewController
        presenter.viewController = viewController
        
        return viewController
    }
}
