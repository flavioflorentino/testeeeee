import Core

enum BilletFormEndpoint {
    case fetchBillet(code: String)
    case fetchLegacyBillet(code: String)
}

extension BilletFormEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .fetchBillet(let code):
            return "bills/v2/barcode?code=\(code)"
        case .fetchLegacyBillet(let code):
            return "bills/barcode?code=\(code)"
        }
    }
}
