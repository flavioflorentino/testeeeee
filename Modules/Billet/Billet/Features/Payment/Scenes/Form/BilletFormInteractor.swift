import AnalyticsModule
import AssetsKit
import UI
import Core
import Foundation

protocol BilletFormInteracting: AnyObject {
    func getFilledCodeIfNeeded()
    func skipValidation()
    func validate(previousLinecode: String, linecode: String)
    func mask(withText text: String, superview: MaskTextField)
    func fetchBillet(withCode code: String, description: String?)
    func openReceipt(withId id: Int?)
    func openFaq(withUrl url: String?)
    func close()
}

final class BilletFormInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let service: BilletFormServicing
    private let presenter: BilletFormPresenting
    private let origin: BilletOrigin
    private let barcode: String?
    private let linecode: String?
    private let description: String?
    
    private var billet: BilletResponse?

    init(dependencies: Dependencies,
         service: BilletFormServicing,
         presenter: BilletFormPresenting,
         origin: BilletOrigin,
         barcode: String? = nil,
         linecode: String? = nil,
         description: String? = nil) {
        self.dependencies = dependencies
        self.service = service
        self.presenter = presenter
        self.origin = origin
        self.barcode = barcode
        self.linecode = linecode
        self.description = description
    }
}

// MARK: - BilletFormInteracting
extension BilletFormInteractor: BilletFormInteracting {
    func getFilledCodeIfNeeded() {
        dependencies.analytics.log(PaymentEvent.billetFormViewed)
        
        if let barcode = barcode {
            return presenter.presentFilled(barcode: barcode)
        }
        
        if let linecode = linecode {
            return presenter.presentFilled(linecode: linecode, description: description)
        }
    }
    
    func skipValidation() {
        presenter.presentValidLinecode()
        presenter.presentNextActionAvailability(true)
    }
    
    func validate(previousLinecode: String, linecode: String) {
        let linecodeMinLength = 46
        let isLinecodeValid = BilletPartialValidation.validate(input: linecode.onlyNumbers)
        let isNextActionAvailable = linecode.onlyNumbers.count >= linecodeMinLength && isLinecodeValid
        
        isLinecodeValid ? presenter.presentValidLinecode() : presenter.presentInvalidLinecode()
        presenter.presentNextActionAvailability(isNextActionAvailable)
    }
    
    func mask(withText text: String, superview: MaskTextField) {
        guard let maskABlock = superview.maskblock else { return }
    
        let billetBlockMask = maskABlock(text, text)
        presenter.presentBilletBlockMask(billetBlockMask)
    }
    
    func fetchBillet(withCode code: String, description: String?) {
        let hasDescription = description?.isNotEmpty ?? false
        
        presenter.presentLoading()
        
        service.fetchBillet(withCode: code.onlyNumbers) { result in
            self.presenter.hideLoading()
            
            switch result {
            case .success(let billet) where !self.canBePaidAfterDueDate(billet):
                self.presenter.didNextStep(action: .overduePaymentUnaccepted)
                
            case .success(let billet) where billet.billId?.isNotEmpty ?? false:
                self.presentSchedulingDuplicationAlert(billet)
                
            case .success(let billet):
                self.billet = billet
                
                self.presenter.didNextStep(action: .payment(model: billet,
                                                            description: hasDescription ? description: nil,
                                                            origin: self.origin))
                
                self.dependencies.analytics.log(PaymentEvent.billetFormNextSelected(hasDescription))
                
            case .failure(let error):
                self.handleErrors(error)
            }
        }
    }
    
    func openReceipt(withId id: Int?) {
        guard let id = id else { return }
        presenter.didNextStep(action: .receipt(id: String(id)))
    }
    
    func openFaq(withUrl url: String?) {
        guard let url = url else { return }
        presenter.didNextStep(action: .faq(url: url))
    }
    
    func close() {
        presenter.didNextStep(action: .close)
    }
}

private extension BilletFormInteractor {
    func canBePaidAfterDueDate(_ billet: BilletResponse) -> Bool {
        guard
            let isOverdue = billet.cip?.isOverdue,
            let canReceiveOverdue = billet.cip?.canReceiveOverdue
            else { return !billet.isExpired }

        return canReceiveOverdue || !isOverdue
    }
    
    func presentSchedulingDuplicationAlert(_ billet: BilletResponse) {
        let actions = [
            BilletErrorAlert.Action(title: Strings.SchedulingDuplicationPopup.primaryAction) {
                let transactionId = billet.billId ?? ""
                let externalId = "bill-\(transactionId)"
                self.presenter.didNextStep(action: .timeline(transactionId: transactionId, externalId: externalId))
            },
            BilletErrorAlert.Action(title: Strings.SchedulingDuplicationPopup.secondaryAction) {
                self.presenter.didNextStep(action: .payment(model: billet,
                                                            description: self.description,
                                                            origin: self.origin))
            }
        ]
        
        let alert = BilletErrorAlert(icon: .image(Resources.Illustrations.iluPersonInfo.image),
                                     title: Strings.SchedulingDuplicationPopup.title,
                                     description: Strings.SchedulingDuplicationPopup.message(billet.schedulePaymentDate ?? ""),
                                     actions: actions)
        presenter.present(alert: alert)
    }

    func handleErrors(_ error: Error) {
        if let billetError = error as? BilletError {
            self.handle(billetError: billetError)
            return
        }
        if let apiError = error as? ApiError {
            self.handle(apiError: apiError)
            return
        }
    }
    
    func handle(billetError: BilletError) {
        let actions: [BilletErrorAlert.Action] = billetError.actions.map { action in
            switch action.type {
            case .receipt:
                return BilletErrorAlert.Action(title: action.title) { self.openReceipt(withId: action.id) }
                
            case .faq:
                return BilletErrorAlert.Action(title: action.title) { self.openFaq(withUrl: action.url) }
                
            case .dismiss:
                return BilletErrorAlert.Action(title: action.title)
            }
        }
        
        let alert = BilletErrorAlert(icon: .image(billetError.image?.icon),
                                     title: billetError.title,
                                     description: billetError.description,
                                     actions: actions)
        presenter.present(alert: alert)
    }
    
    func handle(apiError: ApiError) {
        let requestError = apiError.requestError
        
        let alert = BilletErrorAlert(icon: .image(Resources.Illustrations.iluNotFoundCircledBackground.image),
                                     title: requestError?.title ?? "",
                                     description: requestError?.message ?? "",
                                     actions: [BilletErrorAlert.Action(title: Strings.Common.understood)])
        presenter.present(alert: alert)
    }
}
