import Core
import Foundation
import FeatureFlag

typealias FetchBilletCompletionBlock = (Result<BilletResponse, Error>) -> Void

protocol BilletFormServicing {
    func fetchBillet(withCode code: String, completion: @escaping FetchBilletCompletionBlock)
}

struct BilletFormService {
    typealias Dependencies = HasMainQueue & HasFeatureManager
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - BilletAccessoryServicing

extension BilletFormService: BilletFormServicing {
    func fetchBillet(withCode code: String, completion: @escaping FetchBilletCompletionBlock) {
        let endpoint: BilletFormEndpoint = dependencies.featureManager.isActive(.isBarcodeV2Available) 
            ? .fetchBillet(code: code)
            : .fetchLegacyBillet(code: code)
        
        let api = Api<BilletResponse>(endpoint: endpoint)
        
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        api.execute(jsonDecoder: decoder) { result in
            self.dependencies.mainQueue.async {
                let mappedResult = result
                    .map { $0.model }
                    .mapError { self.parseError($0) }
                
                completion(mappedResult)
            }
        }
    }
    
    private func parseError(_ apiError: ApiError) -> Error {
        switch apiError {
        case .unauthorized(let responseBody),
             .badRequest(let responseBody),
             .otherErrors(let responseBody),
             .tooManyRequests(let responseBody):
            let dictData = AnyCodable(value: responseBody.data)
            guard let data = try? JSONEncoder().encode(dictData), let billetError: BilletError = data.decoded() else {
                return apiError
            }
            return billetError
            
        default:
            return apiError
        }
    }
}
