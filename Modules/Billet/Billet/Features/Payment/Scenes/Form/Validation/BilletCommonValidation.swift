import Foundation

/// This class validate if a billet has the right logic numbers in the right order
/// Agreement is the billets that starts with 8 and has 48 digits
final class BilletCommonValidation: NSObject {
    var input = ""
    
    private let agreementVerificationCode = "8"
    
    var agreementBlockRanges = [
        0..<11,
        12..<23,
        24..<35,
        36..<47
    ]
    
    var billetBlockRanges = [
        0..<9,
        10..<20,
        21..<31
    ]
    
    var isAgreement: Bool {
        input.starts(with: agreementVerificationCode)
    }
    
    // The verifier param is only used by agreements
    func validateBlock(range: CountableRange<Int>?, verifier: String?, code: String? = nil, dv externalDv: String? = nil) -> Bool {
        var field = ""
        
        if let code = code {
            field = code
        } else if let range = range {
            field = input[range]
        } else {
            return false
        }
        
        var digitVerifier = ""
        
        if let range = range {
            digitVerifier = input[min(input.count, range.upperBound)] ?? ""
        } else if let externalDigitVerifier = externalDv {
            digitVerifier = externalDigitVerifier
        } else {
            return false
        }
        
        var mod: String?
        
        if !isAgreement {
            mod = calculateMod10Dv(field)
        } else if verifier == "6" || verifier == "7" || verifier == nil {
            mod = calculateMod10Dv(field)
        } else if verifier == "8" || verifier == "9" {
            mod = calculateMod11Dv(field, zeroOnSpecialCase: true)
        }
        
        return digitVerifier == mod
    }
    
    func calculateMod10Dv(_ code: String) -> String {
        let reversed = code.reversed()
        var sum = 0
        
        for (index, char) in reversed.enumerated() {
            guard let intChar = Int(String(char)) else { continue }
            let multiplier = index.isMultiple(of: 2) ? 2 : 1
            let result = String(intChar * multiplier)
       
            result.compactMap { Int(String($0)) }.forEach { sum += $0 }
        }
        
        let digitVerifier = 10 - sum % 10
        
        return (digitVerifier == 10 ? "0" : String(digitVerifier)[0]) ?? ""
    }
    
    func calculateMod11Dv(_ code: String, zeroOnSpecialCase: Bool = false) -> String {
        let reversed = code.reversed()
        var sum = 0
        var multiplier = 2
        
        for char in reversed {
            guard let value = Int(String(char)) else { continue }
            sum += value * multiplier
            multiplier = multiplier == 9 ? 2 : multiplier + 1
        }
        
        let digitVerifier = String(11 - sum % 11)
        let isSpecial = digitVerifier == "10" || digitVerifier == "11"
        
        if zeroOnSpecialCase, isSpecial {
            return "0"
        }
        
        return isSpecial ? "1" : digitVerifier
    }
}
