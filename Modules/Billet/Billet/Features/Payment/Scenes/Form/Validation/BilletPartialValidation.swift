import Foundation

/// This class validate if a billet has the right logic numbers in the right order
/// Agreement is the billets that starts with 8 and has 48 digits
enum BilletPartialValidation {
    static var input = ""
    private static let common = BilletCommonValidation()
    private static let minimumBilletLength = 46
    private static let billetAgreementLength = 48
    
    static func validate(input: String) -> Bool {
        self.input = input.onlyNumbers
        common.input = self.input
        
        return validate()
    }
    
    private static func validate() -> Bool {
        if (common.isAgreement && input.count == billetAgreementLength) || (!common.isAgreement && input.count > minimumBilletLength) {
            return BilletCompleteValidation().validate(input: input)
        }
        
        let ranges = common.isAgreement ? common.agreementBlockRanges : common.billetBlockRanges
        for range in ranges {
            guard input.count > range.upperBound else {
                return true
            }
            
            let validationMethodIdentifier = common.isAgreement ? input[common.agreementBlockRanges[0]][2] : nil
            guard common.validateBlock(range: range, verifier: validationMethodIdentifier) else {
                return false
            }
        }
        
        return true
    }
}
