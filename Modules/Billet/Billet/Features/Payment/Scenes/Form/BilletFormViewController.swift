import AssetsKit
import SnapKit
import UI
import UIKit

protocol BilletFormDisplaying: AnyObject {
    func display(barcode: String)
    func display(linecode: String, description: String?)
    func display(maskedBlock: String)
    func displayNextButtonAvailability(_ isNextButtonAvailable: Bool)
    func hideLinecodeError()
    func displayLinecodeError(withMessage message: String)
    func displayLoading()
    func hideLoading()
    func display(alert: BilletErrorAlert)
}

final class BilletFormViewController: ViewController<BilletFormInteracting, UIView> {
    private typealias Localizable = Strings.BilletForm
    
    // MARK: - Outlets
    
    private lazy var codeTextView: UIPPFloatingTextView = {
        let textView = UIPPFloatingTextView()
        textView.topText = Localizable.linecodePlaceholder
        textView.infoButtonAction = nil
        textView.setupBilletMask()
        textView.delegate = self
        return textView
    }()
    
    private lazy var descriptionTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = Localizable.linecodeDescriptionExample
        textField.title = Localizable.linecodeDescription
        textField.alwaysShowPlaceholder = true
        textField.returnKeyType = .done
        textField.delegate = self
        textField.defaultForm()
        return textField
    }()
    
    private lazy var nextButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(nextButtonTapped), for: .touchUpInside)
        button.setTitle(Strings.Common.next, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.isEnabled = false
        return button
    }()
    
    private lazy var closeButton = UIBarButtonItem(image: Resources.Icons.icoClose.image,
                                                   style: .plain,
                                                   target: self,
                                                   action: #selector(closeButtonTapped))
    
    // MARK: - Properties
    
    private let descriptionCharLimit = 40
    private var shouldChangeText = true
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        interactor.getFilledCodeIfNeeded()
    }
    
    // MARK: - ViewConfiguration

    override func buildViewHierarchy() {
        view.addSubview(codeTextView)
        view.addSubview(descriptionTextField)
        view.addSubview(nextButton)
    }
    
    override func setupConstraints() {
        codeTextView.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top).offset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        descriptionTextField.snp.makeConstraints {
            $0.top.equalTo(codeTextView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        nextButton.snp.makeConstraints {
            $0.top.equalTo(descriptionTextField.snp.bottom).offset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }

    override func configureViews() {
        title = Localizable.title
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        
        navigationItem.setRightBarButton(closeButton, animated: true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        codeTextView.resignFirstResponder()
        descriptionTextField.resignFirstResponder()
    }
    
    // MARK: - Private methods
    
    @objc
    private func closeButtonTapped() {
        interactor.close()
    }
    
    @objc
    private func nextButtonTapped() {
        guard let code = codeTextView.textView.text else { return }
        interactor.fetchBillet(withCode: code, description: descriptionTextField.text)
    }
    
    func present(alert: BilletAlert, with actions: [PopupAction]) {
        let popup = PopupViewController(title: alert.title, preferredType: .image(alert.icon))
        popup.hideCloseButton = true
        
        popup.setAttributeDescription(NSMutableAttributedString(attributedString: alert.formattedMessage))
        
        actions.forEach {
            popup.addAction($0)
        }
        
        PopupPresenter().showPopup(popup)
    }
}

// MARK: - BilletFormDisplaying
extension BilletFormViewController: BilletFormDisplaying {
    func display(barcode: String) {
        codeTextView.textView.text = barcode
        
        interactor.skipValidation()
    }
    
    func display(linecode: String, description: String?) {
        codeTextView.textView.text = linecode
        descriptionTextField.text = description
        
        interactor.validate(previousLinecode: codeTextView.textView.text, linecode: linecode)
        interactor.mask(withText: linecode, superview: codeTextView)
    }
    
    func display(maskedBlock: String) {
        codeTextView.textView.text = maskedBlock
        shouldChangeText = false
    }
    
    func displayNextButtonAvailability(_ isNextButtonAvailable: Bool) {
        nextButton.isEnabled = isNextButtonAvailable
    }
    
    func hideLinecodeError() {
        codeTextView.errorMessage = nil
    }
    
    func displayLinecodeError(withMessage message: String) {
        codeTextView.errorMessage = message
    }
    
    func displayLoading() {
        nextButton.startLoading()
    }
    
    func hideLoading() {
        nextButton.stopLoading()
    }
    
    func display(alert: BilletErrorAlert) {
        let actions = alert.actions.map {
            ApolloAlertAction(title: $0.title, dismissOnCompletion: true, completion: $0.action)
        }
    
        showApolloAlert(image: alert.icon,
                        title: alert.title,
                        subtitle: alert.description,
                        primaryButtonAction: actions.first,
                        linkButtonAction: actions.dropFirst().first,
                        dismissOnTouchOutside: true)
    }
}

// MARK: - UITextViewDelegate

extension BilletFormViewController: UITextViewDelegate, UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard string.isNotEmpty else {
            return true
        }
        
        guard textField == descriptionTextField else {
            return true
        }
        
        return textField.text?.count ?? 0 <= descriptionCharLimit
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard let codeTextView = textView.superview as? MaskTextField else {
            return shouldChangeText
        }
        
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        
        interactor.validate(previousLinecode: textView.text, linecode: newText)
        interactor.mask(withText: newText, superview: codeTextView)
        
        return shouldChangeText
    }
}
