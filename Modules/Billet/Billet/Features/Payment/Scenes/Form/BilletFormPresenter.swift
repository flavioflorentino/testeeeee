protocol BilletFormPresenting: AnyObject {
    var viewController: BilletFormDisplaying? { get set }
    func presentFilled(barcode: String)
    func presentFilled(linecode: String, description: String?)
    func presentValidLinecode()
    func presentInvalidLinecode()
    func presentNextActionAvailability(_ isNextActionAvailable: Bool)
    func presentBilletBlockMask(_ blockMask: String)
    func presentLoading()
    func hideLoading()
    func present(alert: BilletErrorAlert)
    func didNextStep(action: BilletFormAction)
}

final class BilletFormPresenter {
    private let coordinator: BilletFormCoordinating
    weak var viewController: BilletFormDisplaying?

    init(coordinator: BilletFormCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - BilletFormPresenting
extension BilletFormPresenter: BilletFormPresenting {
    func presentFilled(barcode: String) {
        viewController?.display(barcode: barcode)
    }
    
    func presentFilled(linecode: String, description: String?) {
        viewController?.display(linecode: linecode, description: description)
    }
    
    func presentValidLinecode() {
        viewController?.hideLinecodeError()
    }
    
    func presentInvalidLinecode() {
        viewController?.displayLinecodeError(withMessage: Strings.BilletForm.invalidLinecodeMessage)
    }
    
    func presentNextActionAvailability(_ isNextActionAvailable: Bool) {
        viewController?.displayNextButtonAvailability(isNextActionAvailable)
    }
    
    func presentBilletBlockMask(_ blockMask: String) {
        viewController?.display(maskedBlock: blockMask)
    }
    
    func presentLoading() {
        viewController?.displayLoading()
    }
    
    func hideLoading() {
        viewController?.hideLoading()
    }
    
    func present(alert: BilletErrorAlert) {
        viewController?.display(alert: alert)
    }
    
    func didNextStep(action: BilletFormAction) {
        coordinator.perform(action: action)
    }
}
