import FeatureFlag
import Foundation
import UI
import UIKit

enum BilletFormAction: Equatable {
    case payment(model: BilletResponse, description: String? = nil, origin: BilletOrigin)
    case overduePaymentUnaccepted
    case timeline(transactionId: String, externalId: String)
    case receipt(id: String)
    case faq(url: String)
    case close
}

protocol BilletFormCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: BilletFormAction)
}

final class BilletFormCoordinator {
    typealias Dependencies = HasLegacy & HasFeatureManager
    private let dependencies: Dependencies
    
    typealias Localizable = Strings.BilletAccessory
    weak var viewController: UIViewController?
    
    private let receiptType = "pav"
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - BilletFormCoordinating
extension BilletFormCoordinator: BilletFormCoordinating {
    func perform(action: BilletFormAction) {
        switch action {
        case let .payment(model, description, origin):
            guard let viewController = viewController else { return }
            
            dependencies.legacy.billet?.openBilletPayment(with: model,
                                                          description: description,
                                                          origin: origin,
                                                          from: viewController)
        case .overduePaymentUnaccepted:
            let errorController = ErrorFactory.make(with: createOverduePaymentUnacceptedError())
            viewController?.navigationController?.pushViewController(errorController, animated: true)
            
        case let .timeline(transactionId, externalId):
            let controller: UIViewController = {
                if dependencies.featureManager.isActive(.isFollowUpAvailable) {
                    return FollowUpFactory.make(withBillId: transactionId)
                } else {
                    return BilletFeedDetailFactory.make(transactionId: transactionId, externalId: externalId)
                }
            }()
            
            viewController?.navigationController?.pushViewController(controller, animated: true)
            
        case .receipt(let id):
            viewController?.dismiss(animated: true) { [weak self] in
                guard let self = self else { return }
                self.dependencies.legacy.receipt?.open(withId: id, receiptType: self.receiptType)
            }
            
        case .faq(let url):
            guard let url = URL(string: url) else { return }
            dependencies.legacy.deeplink?.open(url: url)
            
        case .close:
            viewController?.dismiss(animated: true)
        }
    }
}

// MARK: - Private methods

private extension BilletFormCoordinator {
    func createOverduePaymentUnacceptedError() -> ErrorInfoModel {
        ErrorInfoModel(
            image: Assets.Icons.warningBig.image,
            title: Localizable.Errors.overdueTitle,
            message: Localizable.Errors.overdueMessage,
            primaryAction: ErrorAction(title: Strings.Common.understood) { _ in
                self.viewController?.dismiss(animated: true)
            }
        )
    }
}
