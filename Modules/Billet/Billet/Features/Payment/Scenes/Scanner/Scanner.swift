import AVFoundation
import Foundation
import UIKit

protocol ScannerDelegate: AnyObject {
    func scanningSucceeded(with code: String?)
    func scanningDidFail()
}

final class Scanner: UIView {
    // MARK: - Properties
    
    override class var layerClass: AnyClass {
        AVCaptureVideoPreviewLayer.self
    }
    
    override var layer: AVCaptureVideoPreviewLayer {
        guard let layer = super.layer as? AVCaptureVideoPreviewLayer else {
            return AVCaptureVideoPreviewLayer()
        }
        return layer
    }
    
    private var captureSession: AVCaptureSession?
    
    private var isRunning: Bool {
        captureSession?.isRunning ?? false
    }
    
    private var isFlashOn = false
    
    private unowned let delegate: ScannerDelegate
    
    // MARK: - Initialization
    
    init(delegate: ScannerDelegate) {
        self.delegate = delegate
        
        super.init(frame: .zero)
        
        initialSetup()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Accessible Methods
    
    func resetSetup() {
        initialSetup()
    }
    
    func toggleFlash() -> Bool {
        guard let videoCaptureDevice = AVCaptureDevice.default(for: AVMediaType.video), videoCaptureDevice.hasTorch == true else {
            scanningDidFail()
            return isFlashOn
        }
        
        do {
            try videoCaptureDevice.lockForConfiguration()
            
            if videoCaptureDevice.torchMode == .on {
                videoCaptureDevice.torchMode = .off
            } else {
                try videoCaptureDevice.setTorchModeOn(level: 1.0)
            }
            
            isFlashOn.toggle()
            
            videoCaptureDevice.unlockForConfiguration()
        } catch {
            scanningDidFail()
        }
        
        return isFlashOn
    }
}

// MARK: - Private methods

private extension Scanner {
    func initialSetup() {
        clipsToBounds = true
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else {
            return scanningDidFail()
        }
        
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return scanningDidFail()
        }
        
        guard captureSession?.canAddInput(videoInput) ?? false else {
            return scanningDidFail()
        }
        
        captureSession?.addInput(videoInput)
        
        let metaDataOutput = AVCaptureMetadataOutput()
        
        guard captureSession?.canAddOutput(metaDataOutput) ?? false else {
            return scanningDidFail()
        }
        
        captureSession?.addOutput(metaDataOutput)
        
        metaDataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        metaDataOutput.metadataObjectTypes = [.itf14, .interleaved2of5]
        
        layer.session = captureSession
        layer.videoGravity = .resizeAspectFill
        
        captureSession?.startRunning()
    }
    
    func startScanning() {
        captureSession?.startRunning()
    }
    
    func stopScanning() {
        captureSession?.stopRunning()
    }
    
    func found(code: String) {
        delegate.scanningSucceeded(with: code)
    }
    
    func scanningDidFail() {
        delegate.scanningDidFail()
        
        captureSession?.stopRunning()
        captureSession = nil
    }
}

// MARK: - AVCaptureMetadataOutputObjectsDelegate

extension Scanner: AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(
        _ output: AVCaptureMetadataOutput,
        didOutput metadataObjects: [AVMetadataObject],
        from connection: AVCaptureConnection
    ) {
        stopScanning()
        
        guard
            let metadataObject = metadataObjects.first,
            let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject,
            let code = readableObject.stringValue
        else {
            return scanningDidFail()
        }
        
        AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
        
        found(code: code)
    }
}
