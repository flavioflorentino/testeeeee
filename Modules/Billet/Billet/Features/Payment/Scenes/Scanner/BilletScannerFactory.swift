import UIKit

public enum BilletScannerFactory {
    public static func make(with origin: BilletOrigin) -> UIViewController {
        let container = DependencyContainer()
        let coordinator: BilletScannerCoordinating = BilletScannerCoordinator(dependencies: container)
        let presenter: BilletScannerPresenting = BilletScannerPresenter(coordinator: coordinator)
        let interactor = BilletScannerInteractor(presenter: presenter, origin: origin)
        let viewController = BilletScannerViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
