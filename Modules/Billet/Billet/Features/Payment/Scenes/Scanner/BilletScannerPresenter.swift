import Foundation

protocol BilletScannerPresenting: AnyObject {
    var viewController: BilletScannerDisplaying? { get set }
    func present(action: BilletScannerAction)
}

final class BilletScannerPresenter {
    private let coordinator: BilletScannerCoordinating
    weak var viewController: BilletScannerDisplaying?

    init(coordinator: BilletScannerCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - BilletScannerPresenting

extension BilletScannerPresenter: BilletScannerPresenting {
    func present(action: BilletScannerAction) {
        coordinator.perform(action: action)
    }
}
