import UIKit

enum BilletScannerAction: Equatable {
    case form(barcode: String? = nil, origin: BilletOrigin)
    case close
}

protocol BilletScannerCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: BilletScannerAction)
}

final class BilletScannerCoordinator {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - BilletScannerCoordinating
extension BilletScannerCoordinator: BilletScannerCoordinating {
    func perform(action: BilletScannerAction) {
        switch action {
        case let .form(barcode, origin):
            let formController: UIViewController
            
            if let barcode = barcode {
                formController = BilletFormFactory.make(with: origin, barcode: barcode)
            } else {
                formController = BilletFormFactory.make(with: origin)
            }
            
            viewController?.navigationController?.pushViewController(formController, animated: true)
            
        case .close:
            viewController?.navigationController?.popViewController(animated: true)
        }
    }
}
