import Foundation

protocol BilletScannerInteracting: AnyObject {
    func openForm(withBarcode barcode: String?)
    func close()
}

extension BilletScannerInteracting {
    func openForm() {
        openForm(withBarcode: nil)
    }
}

final class BilletScannerInteractor {
    private let presenter: BilletScannerPresenting
    private let origin: BilletOrigin

    init(presenter: BilletScannerPresenting, origin: BilletOrigin) {
        self.presenter = presenter
        self.origin = origin
    }
}

// MARK: - BilletScannerInteracting

extension BilletScannerInteractor: BilletScannerInteracting {
    func openForm(withBarcode barcode: String?) {
        presenter.present(action: .form(barcode: barcode, origin: origin))
    }
    
    func close() {
        presenter.present(action: .close)
    }
}
