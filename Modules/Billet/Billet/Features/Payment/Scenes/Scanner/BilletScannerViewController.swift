import AssetsKit
import UI
import UIKit

protocol BilletScannerDisplaying: AnyObject { }

private extension BilletScannerViewController.Layout {
    enum Size {
        static let flashButtonSize = CGSize(width: 20, height: 20)
        static let lineViewWidth: CGFloat = 2
        static let sideMultiplier: CGFloat = 0.325
        static let codeManuallyButtonWidth: CGFloat = 330
    }
    
    enum Angle {
        static let right: CGFloat = .pi / 2
    }
    
    enum Alpha {
        static let multiplier: CGFloat = 0.8
    }
}

final class BilletScannerViewController: ViewController<BilletScannerInteracting, UIView> {
    fileprivate enum Layout { }
    
    private typealias Localizable = Strings.BilletScanner

    // MARK: - Views
    
    private lazy var scanner = Scanner(delegate: self)
    
    private lazy var topView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.backgroundPrimary.color.withAlphaComponent(Layout.Alpha.multiplier)
        return view
    }()
    
    private lazy var backButton: UIButton = {
        let button = UIButton()
        button.transform = CGAffineTransform(rotationAngle: Layout.Angle.right)
        button.setTitle(Strings.Common.back, for: .normal)
        button.setTitleColor(Colors.black.color, for: .normal)
        button.setImage(Resources.Icons.icoGreenLeftArrow.image.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = Colors.black.color
        button.addTarget(self, action: #selector(didTapCloseButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var flashButton: UIButton = {
        let button = UIButton()
        button.setImage(Resources.Icons.icoFlash.image.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = Colors.black.color
        button.addTarget(self, action: #selector(didTapFlashButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, .black())
        label.text = Localizable.title
        label.transform = CGAffineTransform(rotationAngle: Layout.Angle.right)
        return label
    }()
    
    private lazy var lineView = SeparatorView(style: BackgroundViewStyle(color: .branding600()))
    
    private lazy var bottomView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.backgroundPrimary.color.withAlphaComponent(Layout.Alpha.multiplier)
        return view
    }()
    
    private lazy var codeManuallyButton: UIButton = {
        let button = UIButton()
        button.transform = CGAffineTransform(rotationAngle: Layout.Angle.right)
        button.buttonStyle(SecondaryButtonStyle(size: .small))
        button.addTarget(self, action: #selector(didTapCodeManuallyButton), for: .touchUpInside)
        button.setTitle(Localizable.codeManuallyButtonTitle, for: .normal)
        return button
    }()
    
    // MARK: - Properties
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 12.0, *) {
            return traitCollection.userInterfaceStyle == .dark ? .lightContent : .default
        } else {
            return .default
        }
    }
    
    // MARK: - Lifecycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
        scanner.resetSetup()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    // MARK: - View Layout

    override func buildViewHierarchy() {
        view.addSubview(scanner)
        scanner.addSubviews(topView, lineView, bottomView)
        topView.addSubviews(backButton, flashButton, descriptionLabel)
        bottomView.addSubview(codeManuallyButton)
    }
    
    override func setupConstraints() {
        scanner.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        topView.snp.makeConstraints {
            $0.top.trailing.bottom.equalToSuperview()
            $0.width.equalToSuperview().multipliedBy(Layout.Size.sideMultiplier)
        }
        
        backButton.snp.makeConstraints {
            $0.top.equalTo(view.snp.topMargin).offset(Spacing.base02)
            $0.trailing.equalToSuperview()
        }
        
        flashButton.snp.makeConstraints {
            $0.trailing.equalToSuperview().offset(-Spacing.base03)
            $0.bottom.equalTo(view.snp.bottomMargin).offset(-Spacing.base01)
            $0.size.equalTo(Layout.Size.flashButtonSize)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.centerX.equalToSuperview().offset(-Spacing.base02)
        }
        
        lineView.snp.makeConstraints {
            $0.top.bottom.centerX.equalToSuperview()
            $0.width.equalTo(Layout.Size.lineViewWidth)
            $0.height.equalToSuperview()
        }
        
        bottomView.snp.makeConstraints {
            $0.top.leading.bottom.equalToSuperview()
            $0.width.equalToSuperview().multipliedBy(Layout.Size.sideMultiplier)
        }
        
        codeManuallyButton.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.width.equalTo(Layout.Size.codeManuallyButtonWidth)
        }
    }
}

// MARK: - BilletScannerDisplaying

extension BilletScannerViewController: BilletScannerDisplaying { }

// MARK: - ScannerDelegate

extension BilletScannerViewController: ScannerDelegate {
    func scanningSucceeded(with code: String?) {
        interactor.openForm(withBarcode: code)
    }
    
    func scanningDidFail() {
        displayErrorAlert()
    }
}

// MARK: - Private Methods

private extension BilletScannerViewController {
    @objc
    func didTapFlashButton() {
        let isFlashOn = scanner.toggleFlash()
        flashButton.tintColor = isFlashOn ? Colors.warning600.color : Colors.black.color
    }
    
    @objc
    func didTapCodeManuallyButton() {
        interactor.openForm()
    }
    
    @objc
    func didTapCloseButton() {
        interactor.close()
    }
    
    func displayErrorAlert() {
        let actions: [ApolloAlertAction] = [
            ApolloAlertAction(title: Strings.Common.understood, dismissOnCompletion: true, completion: scanner.resetSetup),
            ApolloAlertAction(title: Strings.Common.close, dismissOnCompletion: true, completion: scanner.resetSetup)
        ]
    
        showApolloAlert(image: Resources.Illustrations.iluError.image,
                        title: Localizable.Error.title,
                        subtitle: Localizable.Error.message,
                        primaryButtonAction: actions.first,
                        linkButtonAction: actions.dropFirst().first,
                        dismissOnTouchOutside: false)
    }
}
