import Foundation
import SnapKit
import UI
import UIKit

final class DatePickerPopup: UIViewController {
    // MARK: - Outlets
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.viewStyle(RoundedViewStyle(cornerRadius: .strong))
            .with(\.backgroundColor, .groupedBackgroundSecondary())
        return view
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var datePicker: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.tintColor = Colors.branding600.color
        
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        
        return datePicker
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Strings.Common.confirm, for: .normal)
        button.addTarget(self, action: #selector(confirmButtonTapped), for: .touchUpInside)
        return button
    }()
    
    private lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.Common.cancel, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(cancelButtonTapped), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Properties
    
    private let actionHandler: ((Date) -> Void)?
    
    // MARK: - Initialization
    
    init(title: String, message: String, minDate: Date? = Date(), maxDate: Date? = nil, action: ((Date) -> Void)? = nil) {
        actionHandler = action
        
        super.init(nibName: nil, bundle: nil)
        
        titleLabel.text = title
        messageLabel.text = message
        datePicker.minimumDate = minDate
        datePicker.maximumDate = maxDate
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

@objc
private extension DatePickerPopup {
    func confirmButtonTapped() {
        dismiss(animated: true) {
            self.actionHandler?(self.datePicker.date)
        }
    }
    
    func cancelButtonTapped() {
        dismiss(animated: true)
    }
}

extension DatePickerPopup: ViewConfiguration {
    func buildViewHierarchy() {
        view.addSubview(containerView)
        containerView.addSubview(stackView)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(messageLabel)
        stackView.setSpacing(Spacing.base01, after: messageLabel)
        stackView.addArrangedSubview(datePicker)
        containerView.addSubview(confirmButton)
        containerView.addSubview(cancelButton)
    }
    
    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        stackView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        confirmButton.snp.makeConstraints {
            $0.top.equalTo(stackView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        cancelButton.snp.makeConstraints {
            $0.top.equalTo(confirmButton.snp.bottom)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.bottom.equalToSuperview().inset(Spacing.base01)
        }
    }
}
