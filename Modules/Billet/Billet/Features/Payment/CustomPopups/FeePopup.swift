import Foundation
import SnapKit
import UI
import UIKit

public enum FeeType {
    case convenience
    case discount
    case installment
    case interest
    
    var title: String {
        switch self {
        case .convenience:
            return Strings.FeePopup.Convenience.title
        case .discount:
            return Strings.FeePopup.Discount.title
        case .installment:
            return Strings.FeePopup.Installment.title
        case .interest:
            return Strings.FeePopup.Interest.title
        }
    }
    
    var message: String {
        switch self {
        case .convenience:
            return Strings.FeePopup.Convenience.message
        case .discount:
            return Strings.FeePopup.Discount.message
        case .installment:
            return Strings.FeePopup.Installment.message
        case .interest:
            return Strings.FeePopup.Interest.message
        }
    }
    
    var fee: String {
        switch self {
        case .convenience:
            return Strings.FeePopup.Convenience.fee
        case .discount:
            return Strings.FeePopup.Discount.fee
        case .installment:
            return Strings.FeePopup.Installment.fee
        case .interest:
            return Strings.FeePopup.Interest.fee
        }
    }
}

final class FeePopup: UIViewController {
    // MARK: - Outlets
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.viewStyle(RoundedViewStyle(cornerRadius: .strong))
            .with(\.backgroundColor, .groupedBackgroundSecondary())
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .small))
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textAlignment, .center)
            .with(\.textColor, .grayscale700())
        return label
    }()
    
    private lazy var valueContainerView = UIView()
    
    private lazy var valueDescriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textAlignment, .right)
            .with(\.textColor, .grayscale700())
        label.setContentHuggingPriority(.defaultLow, for: .horizontal)
        return label
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textAlignment, .left)
            .with(\.textColor, .branding600())
        label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        return label
    }()
    
    private lazy var understoodButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Strings.Common.understood, for: .normal)
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Properties
    
    private let buttonHandler: (() -> Void)?
    
    // MARK: - Initialization
    
    init(value: String?, type: FeeType, completion: (() -> Void)? = nil) {
        buttonHandler = completion
        
        super.init(nibName: nil, bundle: nil)
        
        titleLabel.text = type.title
        descriptionLabel.text = type.message
        valueDescriptionLabel.text = type.fee
        valueLabel.text = value
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Actions
    
    @objc
    private func buttonTapped() {
        buttonHandler?()
        dismiss(animated: true)
    }
}

// MARK: - ViewConfiguration

extension FeePopup: ViewConfiguration {
    func buildViewHierarchy() {
        view.addSubview(containerView)
        containerView.addSubview(titleLabel)
        containerView.addSubview(descriptionLabel)
        containerView.addSubview(valueContainerView)
        valueContainerView.addSubview(valueDescriptionLabel)
        valueContainerView.addSubview(valueLabel)
        containerView.addSubview(understoodButton)
    }
    
    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base05)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        valueContainerView.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base02)
            $0.centerX.equalToSuperview()
        }
        
        valueDescriptionLabel.snp.makeConstraints {
            $0.top.leading.bottom.equalToSuperview()
        }
        
        valueLabel.snp.makeConstraints {
            $0.top.trailing.bottom.equalToSuperview()
            $0.leading.equalTo(valueDescriptionLabel.snp.trailing).offset(Spacing.base00)
        }
        
        understoodButton.snp.makeConstraints {
            $0.top.equalTo(valueContainerView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().inset(Spacing.base05)
        }
    }
}
