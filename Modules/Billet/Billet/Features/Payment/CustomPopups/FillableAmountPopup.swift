import Foundation
import SnapKit
import UI
import UIKit

final class FillableAmountPopup: UIViewController {
    // MARK: - Outlets
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        view.roundCorners([.layerMaxXMinYCorner, .layerMinXMinYCorner], radius: CornerRadius.medium)
        return view
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .small))
            .with(\.textAlignment, .center)
        label.text = Strings.FillableAmountPopup.description
        return label
    }()
    
    private lazy var amountTextField: CurrencyField = {
        let textField = CurrencyField()
        textField.fontCurrency = Typography.caption().font()
        textField.fontValue = Typography.currency(.large).font()
        textField.textColor = Colors.branding400.color
        textField.tintColor = Colors.branding400.color
        textField.positionCurrency = .top
        textField.limitValue = 10
        textField.delegate = self
        return textField
    }()
    
    private lazy var totalAmountLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var nextButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Strings.Common.next, for: .normal)
        button.addTarget(self, action: #selector(nextButtonTapped), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Properties
    
    private var buttonHandler: (Double) -> Void
    
    private var totalAmount: Double
    private var amount: Double
    private var limit: Double
    
    // MARK: - Initialization
    
    init(amount: Double, limit: Double, completion: @escaping (Double) -> Void) {
        self.totalAmount = amount
        self.amount = amount
        self.limit = limit
        buttonHandler = completion
        
        super.init(nibName: nil, bundle: nil)
        
        amountTextField.value = totalAmount
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        amountTextField.becomeResponder()
    }
    
    // MARK: - Actions
    
    @objc
    private func nextButtonTapped() {
        guard amount > 0, amount <= limit else { return }
        
        buttonHandler(amount)
        dismiss(animated: true)
    }
}

// MARK: - CurrencyFieldDelegate

extension FillableAmountPopup: CurrencyFieldDelegate {
    func onValueChange(value: Double) {
        amount = value
        
        if value > limit {
            totalAmountLabel.text = Strings.FillableAmountPopup.limit(limit.toCurrencyString() ?? "")
            totalAmountLabel.textColor = Colors.critical600.color
            amountTextField.textColor = Colors.critical600.color
            nextButton.isEnabled = false
        } else {
            let currencyTotalAmount = totalAmount.toCurrencyString() ?? ""
            totalAmountLabel.text = totalAmount != value ? "\(Strings.Common.totalValue) \(currencyTotalAmount)" : ""
            totalAmountLabel.textColor = Colors.black.color
            amountTextField.textColor = Colors.branding400.color
            nextButton.isEnabled = value != 0
        }
    }
}

// MARK: - ViewConfiguration

extension FillableAmountPopup: ViewConfiguration {
    func buildViewHierarchy() {
        view.addSubview(containerView)
        containerView.addSubview(descriptionLabel)
        containerView.addSubview(amountTextField)
        containerView.addSubview(totalAmountLabel)
        containerView.addSubview(nextButton)
    }
    
    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.top.lessThanOrEqualToSuperview().offset(Spacing.base07)
            $0.leading.trailing.bottom.equalToSuperview()
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview().inset(Spacing.base04)
        }
        
        amountTextField.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base04)
            $0.centerX.equalToSuperview()
        }
        
        totalAmountLabel.snp.makeConstraints {
            $0.top.equalTo(amountTextField.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Sizing.base02)
        }
        
        nextButton.snp.makeConstraints {
            $0.top.equalTo(totalAmountLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
}
