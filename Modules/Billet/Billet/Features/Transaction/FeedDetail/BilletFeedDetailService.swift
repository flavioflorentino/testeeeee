import Foundation
import Core

typealias StatusHistoryCompletionBlock = (Result<BilletInfoDetail, ApiError>) -> Void
typealias CommentsCompletionBlock = (Result<CommentsResponse, ApiError>) -> Void
typealias LikeCompletionBlock = (Result<LikeResponse, ApiError>) -> Void
typealias ChangePrivacyCompletionBlock = (Result<NoContent, ApiError>) -> Void
typealias GenericBehaviorCompletionBlock = (Result<GenericBehaviorResponse, ApiError>) -> Void

protocol BilletFeedDetailServicing {
    func getStatusHistory(
        withTransactionId transactionId: String,
        andExternalId externalId: String,
        _ completion: @escaping StatusHistoryCompletionBlock
    )
    func cancelPayment(withBillId billId: String, type: TransactionActionType, _ completion: @escaping CancelPaymentCompletionBlock)
    func getComments(withFeedId feedId: String, _ completion: @escaping CommentsCompletionBlock)
    func addComment(withComment comment: String, andFeedId feedId: String, _ completion: @escaping CommentsCompletionBlock)
    func like(with liked: Bool, andFeedId feedId: String, _ completion: @escaping LikeCompletionBlock)
    func makePrivate(withFeedId feedId: String, _ completion: @escaping ChangePrivacyCompletionBlock)
    func genericBehavior(with data: AnyCodable, _ completion: @escaping GenericBehaviorCompletionBlock)
}

final class BilletFeedDetailService: BilletFeedDetailServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func getStatusHistory(
        withTransactionId transactionId: String, 
        andExternalId externalId: String,
        _ completion: @escaping StatusHistoryCompletionBlock
    ) {
        let endpoint = BilletFeedDetailServiceEndpoint.statusHistory(transactionId: transactionId, externalId: externalId)
        let api = Api<BilletInfoDetail>(endpoint: endpoint)
        
        execute(api, then: completion)
    }
    
    func cancelPayment(withBillId billId: String, type: TransactionActionType, _ completion: @escaping CancelPaymentCompletionBlock) {
        var endpoint: BilletFeedDetailServiceEndpoint = .cancelPayment(billId: billId)
        
        if case .scheduledCancel = type {
            endpoint = .cancelScheduledPayment(billId: billId)
        }
        
        let api = Api<NoContent>(endpoint: endpoint)
        execute(api, then: completion)
    }
    
    func getComments(withFeedId feedId: String, _ completion: @escaping CommentsCompletionBlock) {
        let endpoint = BilletFeedDetailServiceEndpoint.loadItemComments(feedId: feedId)
        let api = Api<CommentsResponse>(endpoint: endpoint)
        
        execute(api, then: completion)
    }
    
    func addComment(withComment comment: String, andFeedId feedId: String, _ completion: @escaping CommentsCompletionBlock) {
        let endpoint = BilletFeedDetailServiceEndpoint.addItemComment(text: comment, feedId: feedId)
        let api = Api<CommentsResponse>(endpoint: endpoint)
        
        execute(api, then: completion)
    }
    
    func like(with liked: Bool, andFeedId feedId: String, _ completion: @escaping LikeCompletionBlock) {
        let endpoint = BilletFeedDetailServiceEndpoint.likeItem(liked: liked, feedId: feedId)
        let api = Api<LikeResponse>(endpoint: endpoint)
        
        execute(api, then: completion)
    }
    
    func makePrivate(withFeedId feedId: String, _ completion: @escaping ChangePrivacyCompletionBlock) {
        let endpoint = BilletFeedDetailServiceEndpoint.changeItemPrivacy(feedId: feedId)
        let api = Api<NoContent>(endpoint: endpoint)
        
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .lowercased
        
        execute(api, decoder: decoder, then: completion)
    }
    
    func genericBehavior(with data: AnyCodable, _ completion: @escaping GenericBehaviorCompletionBlock) {
        let endpoint = BilletFeedDetailServiceEndpoint.genericBehavior(data: data)
        let api = Api<GenericBehaviorResponse>(endpoint: endpoint)
        
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .lowercased
        
        execute(api, decoder: decoder, then: completion)
    }
    
    private func execute<T>(
        _ api: Api<T>,
        decoder: JSONDecoder = JSONDecoder(),
        then completion: @escaping (Result<T, ApiError>) -> Void
    ) {
        api.execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
