import Foundation
import UI

protocol BilletFeedDetailMessageViewPresenting {
    var date: String { get }
    var description: NSAttributedString? { get }
    var actions: [TransactionAction] { get }
}

struct BilletFeedDetailMessageViewPresenter: BilletFeedDetailMessageViewPresenting {
    var date: String
    var description: NSAttributedString?
    var actions: [TransactionAction] = []
    
    init(date: String, description: String, actions: [TransactionAction] = []) {
        self.date = date
        self.actions = actions
        self.description = format(description: description)
    }
    
    private func format(description: String?) -> NSAttributedString? {
        let formattedString = description?
            .replacingOccurrences(of: "<br/>", with: "\n")
            .replacingOccurrences(of: "<br />", with: "\n")
            .replacingOccurrences(of: "<b>", with: "[b]")
            .replacingOccurrences(of: "</b>", with: "[b]")
        
        guard
            let preColorString = formattedString?.attributedStringWithFont(
                primary: Typography.bodySecondary().font(),
                secondary: Typography.bodySecondary(.highlight).font(),
                separator: "[b]"
            )
            else {
                return nil
        }
        
        let coloredString = NSMutableAttributedString(attributedString: preColorString)
        let range = NSRange(location: 0, length: coloredString.length)
        
        coloredString.addAttribute(.foregroundColor, value: Colors.grayscale700.color, range: range)
        
        return coloredString
    }
}
