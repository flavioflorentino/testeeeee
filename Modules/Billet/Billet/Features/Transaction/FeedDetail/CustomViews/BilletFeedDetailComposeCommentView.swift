import UI
import UIKit
import SnapKit

protocol BilletFeedDetailComposeCommentViewDelegate: AnyObject {
    func sendComment(_ comment: String)
}

private extension BilletFeedDetailComposeCommentView.Layout {
    enum Size {
        static let button = CGSize(width: 60, height: 36)
        static let textFieldLeftPadding = CGFloat(10)
    }
}

final class BilletFeedDetailComposeCommentView: UIView {
    fileprivate enum Layout { }
    
    private(set) lazy var commentTextField: UITextField = {
        let textField = UITextField()
        let leftViewFrame = CGRect(x: 0, y: 0, width: Layout.Size.textFieldLeftPadding, height: textField.frame.height)
        textField.leftView = UIView(frame: leftViewFrame)
        textField.leftViewMode = .always
        textField.cornerRadius = .strong
        textField.backgroundColor = Colors.backgroundPrimary.color
        return textField
    }()
    
    private lazy var sendButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle(size: .small))
        button.setTitle(Strings.send, for: .normal)
        button.addTarget(self, action: #selector(sendComment), for: .touchUpInside)
        return button
    }()
    
    private weak var delegate: BilletFeedDetailComposeCommentViewDelegate?
    
    init(delegate: BilletFeedDetailComposeCommentViewDelegate, frame: CGRect = .zero) {
        self.delegate = delegate
        
        super.init(frame: frame)
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    func sendComment() {
        guard
            let comment = commentTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines),
            comment.isNotEmpty
            else {
                return
        }
        
        delegate?.sendComment(comment)
        
        commentTextField.text?.removeAll()
        commentTextField.resignFirstResponder()
    }
}

extension BilletFeedDetailComposeCommentView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(commentTextField)
        addSubview(sendButton)
    }
    
    func setupConstraints() {
        commentTextField.snp.makeConstraints {
            $0.top.leading.bottom.equalToSuperview().inset(Spacing.base01)
        }
        
        sendButton.snp.makeConstraints {
            $0.top.trailing.bottom.equalToSuperview().inset(Spacing.base01)
            $0.leading.equalTo(commentTextField.snp.trailing).offset(Spacing.base01)
            $0.size.equalTo(Layout.Size.button)
        }
    }
    
    func configureViews() {
        isUserInteractionEnabled = true
        backgroundColor = .backgroundSecondary()
    }
}
