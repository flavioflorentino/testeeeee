import UI
import UIKit
import SnapKit

final class BilletFeedDetailMessageView: UIView {
    // MARK: - Outlets
    
    private(set) lazy var dateLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(CaptionLabelStyle())
            .with(\.textColor, .grayscale300())
        return label
    }()
    
    private(set) lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = .zero
        return label
    }()
    
    private lazy var containerButton: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.spacing = Spacing.base01
        return view
    }()
    
    private(set) lazy var linkButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(linkButtonTapped), for: .touchUpInside)
        button.buttonStyle(LinkButtonStyle(size: .small))
        button.contentHorizontalAlignment = .left
        button.isHidden = true
        return button
    }()
    
    private(set) lazy var primaryButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(primaryButtonTapped), for: .touchUpInside)
        button.isHidden = true
        button.buttonStyle(PrimaryButtonStyle(size: .small))
        return button
    }()
    
    private(set) lazy var secondaryButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(secondaryButtonTapped), for: .touchUpInside)
        button.isHidden = true
        button.buttonStyle(DangerButtonStyle(size: .small, icon: (name: .infoCircle, alignment: .left)))
        return button
    }()
    
    // MARK: - Properties
    
    private var linkAction: TransactionAction?
    private var primaryAction: TransactionAction?
    private var secondaryAction: TransactionAction?
    
    var buttonHandler: ((_ action: TransactionAction) -> Void)?
    
    // MARK: - Initialization
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Setup

extension BilletFeedDetailMessageView {
    func setup(with presenter: BilletFeedDetailMessageViewPresenting) {
        dateLabel.text = presenter.date
        descriptionLabel.attributedText = presenter.description
        
        hideButtons()
        
        presenter.actions.forEach { action in
            setupButton(with: action)
        }
    }
    
    private func setupButton(with action: TransactionAction) {
        switch action.type {
        case .helpCenter:
            linkAction = action
            linkButton.setTitle(action.title, for: .normal)
            linkButton.isHidden = false
            linkButton.buttonStyle(LinkButtonStyle(size: .small))
            
        case .accelerateAnalysis:
            primaryAction = action
            primaryButton.setTitle(action.title, for: .normal)
            primaryButton.isHidden = false
            
        case .cancel, .scheduledCancel:
            secondaryAction = action
            secondaryButton.setTitle(action.title, for: .normal)
            secondaryButton.isHidden = false
            
        default:
            break
        }
    }
    
    private func hideButtons() {
        linkButton.isHidden = true
        primaryButton.isHidden = true
        secondaryButton.isHidden = true
    }
}

// MARK: - Actions

@objc
private extension BilletFeedDetailMessageView {
    func linkButtonTapped() {
        guard let linkAction = linkAction else {
            return
        }
        
        buttonHandler?(linkAction)
    }
    
    func primaryButtonTapped() {
        guard let primaryAction = primaryAction else {
            return
        }
        
        buttonHandler?(primaryAction)
    }
    
    func secondaryButtonTapped() {
        guard let secondaryAction = secondaryAction else {
            return
        }
        
        buttonHandler?(secondaryAction)
    }
}

// MARK: - ViewConfiguration

extension BilletFeedDetailMessageView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(dateLabel)
        addSubview(descriptionLabel)
        containerButton.addArrangedSubview(linkButton)
        containerButton.addArrangedSubview(primaryButton)
        containerButton.setSpacing(Spacing.base01, after: linkButton)
        containerButton.addArrangedSubview(secondaryButton)
        addSubviews(containerButton)
    }
    
    func setupConstraints() {
        dateLabel.snp.makeConstraints {
            $0.top.equalToSuperview().inset(Spacing.base01)
            $0.leading.trailing.equalToSuperview()
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(dateLabel.snp.bottom).offset(Spacing.base00)
            $0.leading.trailing.equalToSuperview()
        }
        
        containerButton.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalToSuperview().inset(Spacing.base01)
        }
    }
}
