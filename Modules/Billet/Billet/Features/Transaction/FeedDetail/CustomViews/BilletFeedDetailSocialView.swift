import UI
import UIKit
import SnapKit

private extension BilletFeedDetailSocialView.Layout {
    enum Size {
        static let image = CGSize(width: 25, height: 25)
        static let valueSeparator = CGSize(width: 1, height: 10)
        static let privacyIcon = CGSize(width: 18, height: 18)
        static let separatorHeight = CGFloat(1)
    }
}

final class BilletFeedDetailSocialView: UIView {
    fileprivate enum Layout { }
    
    // MARK: Outlets
    
    private lazy var headerContainerView = UIView()
    private(set) lazy var valueLabel = UILabel()
    private lazy var valueSeparator = UIView()
    private lazy var privacyImageView = UIImageView()
    private(set) lazy var dateLabel = UILabel()
    private lazy var footerContainerView = UIView()
    private lazy var topSeparator = UIView()
    private lazy var likeButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(likePressed), for: .touchUpInside)
        return button
    }()
    private(set) lazy var likeTextLabel: UILabel = {
        let label = UILabel()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(likedListPressed))
        label.addGestureRecognizer(tapGesture)
        label.isUserInteractionEnabled = true
        return label
    }()
    private lazy var bottomSeparator = UIView()
    
    // MARK: - Properties
    
    private var liked = false
    var likeHandler: ((_ liked: Bool) -> Void)?
    var likedListHandler: (() -> Void)?
    
    // MARK: - Initialization
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Setup & Action

extension BilletFeedDetailSocialView {
    func setup(with presenter: BilletFeedDetailSocialViewPresenting) {
        if !presenter.isValueHidden {
            valueLabel.attributedText = presenter.value
        } else {
            valueLabel.isHidden = true
            valueSeparator.isHidden = true
            updateHiddenValueConstraints()
        }
        
        dateLabel.text = presenter.date
        
        let privacyAsset = presenter.isPublic ? Assets.Icons.privacyFriends : Assets.Icons.privacyPrivate
        privacyImageView.image = privacyAsset.image
        
        likeTextLabel.text = presenter.likeText
        
        let likeAsset = presenter.liked ? Assets.Icons.likeFilled : Assets.Icons.like
        likeButton.setImage(likeAsset.image, for: .normal)
        
        self.liked = presenter.liked
    }
    
    func updateLikeInfo(withLikeText likeText: String, and isChecked: Bool) {
        self.likeTextLabel.text = likeText
        
        let likeAsset = isChecked ? Assets.Icons.likeFilled : Assets.Icons.like
        likeButton.setImage(likeAsset.image, for: .normal)
    }
    
    func updatePrivacyInfo(isPrivate: Bool) {
        let privacyAsset = isPrivate ? Assets.Icons.privacyPrivate : Assets.Icons.privacyFriends
        privacyImageView.image = privacyAsset.image
    }
    
    @objc
    private func likePressed() {
        liked.toggle()
        likeHandler?(liked)
    }
    
    @objc
    private func likedListPressed() {
        likedListHandler?()
    }
}

// MARK: ViewConfiguration

extension BilletFeedDetailSocialView: ViewConfiguration {
    func buildViewHierarchy() {
        headerContainerView.addSubview(valueLabel)
        headerContainerView.addSubview(valueSeparator)
        headerContainerView.addSubview(privacyImageView)
        headerContainerView.addSubview(dateLabel)
        addSubview(headerContainerView)
        footerContainerView.addSubview(topSeparator)
        footerContainerView.addSubview(likeButton)
        footerContainerView.addSubview(likeTextLabel)
        footerContainerView.addSubview(bottomSeparator)
        addSubview(footerContainerView)
    }
    
    func setupConstraints() {
        headerContainerView.snp.makeConstraints {
            $0.top.equalToSuperview().inset(Spacing.base01)
            $0.leading.trailing.equalToSuperview()
        }
        
        valueLabel.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.equalToSuperview().inset(Spacing.base03)
        }
        
        valueSeparator.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalTo(valueLabel.snp.trailing).offset(Spacing.base00)
            $0.size.equalTo(Layout.Size.valueSeparator)
        }
        
        privacyImageView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalTo(valueSeparator.snp.trailing).offset(Spacing.base00)
            $0.size.equalTo(Layout.Size.privacyIcon)
        }
        
        dateLabel.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.equalTo(privacyImageView.snp.trailing).offset(Spacing.base00)
            $0.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        footerContainerView.snp.makeConstraints {
            $0.top.equalTo(headerContainerView.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalToSuperview().inset(Spacing.base01)
        }
        
        topSeparator.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.height.equalTo(Layout.Size.separatorHeight)
        }
        
        likeButton.snp.makeConstraints {
            $0.top.equalTo(topSeparator.snp.bottom).offset(Spacing.base00)
            $0.leading.equalToSuperview().inset(Spacing.base03)
            $0.size.equalTo(Layout.Size.image)
        }
        
        likeTextLabel.snp.makeConstraints {
            $0.top.equalTo(topSeparator.snp.bottom).offset(Spacing.base00)
            $0.leading.equalTo(likeButton.snp.trailing).offset(Spacing.base01)
            $0.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        bottomSeparator.snp.makeConstraints {
            $0.top.equalTo(likeButton.snp.bottom).offset(Spacing.base00)
            $0.top.equalTo(likeTextLabel.snp.bottom).offset(Spacing.base00)
            $0.leading.trailing.bottom.equalToSuperview()
            $0.height.equalTo(Layout.Size.separatorHeight)
        }
    }
    
    private func updateHiddenValueConstraints() {
        valueSeparator.snp.updateConstraints {
            $0.leading.equalTo(valueLabel.snp.trailing)
        }
    }
    
    func configureStyles() {
        dateLabel
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale300())
        
        valueSeparator.backgroundColor = .grayscale300()
        
        topSeparator.backgroundColor = .grayscale100()
        
        likeTextLabel
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale300())
        
        bottomSeparator.backgroundColor = .grayscale100()
    }
    
    func configureViews() {
        valueLabel.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        dateLabel.setContentHuggingPriority(.defaultLow, for: .horizontal)
    }
}
