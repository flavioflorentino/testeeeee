import Foundation
import UI

protocol BilletFeedDetailSocialViewPresenting {
    var isValueHidden: Bool { get }
    var value: NSAttributedString? { get }
    var date: String { get }
    var isPublic: Bool { get }
    var liked: Bool { get }
    var likeText: String { get }
}

struct BilletFeedDetailSocialViewPresenter: BilletFeedDetailSocialViewPresenting {
    var isValueHidden: Bool
    var value: NSAttributedString?
    var date: String
    var isPublic: Bool
    var liked: Bool
    var likeText: String
    
    init(
        isValueHidden: Bool,
        value: String,
        date: String,
        isPublic: Bool,
        liked: Bool,
        likeText: String
    ) {
        self.isValueHidden = isValueHidden
        self.date = date
        self.isPublic = isPublic
        self.liked = liked
        self.likeText = likeText
        self.value = format(value: value)
    }
    
    private func format(value: String) -> NSAttributedString? {
        guard
            let data = value.data(using: .utf8),
            let attrString = try? NSAttributedString(
                data: data,
                options: [.documentType: NSAttributedString.DocumentType.html],
                documentAttributes: nil
            )
            else {
                return nil
        }
        
        let formattedString = NSMutableAttributedString(attributedString: attrString)
        let range = NSRange(location: 0, length: formattedString.length)
        
        formattedString.addAttribute(
            .font,
            value: Typography.bodySecondary(.highlight).font(),
            range: range
        )
        
        return formattedString
    }
}
