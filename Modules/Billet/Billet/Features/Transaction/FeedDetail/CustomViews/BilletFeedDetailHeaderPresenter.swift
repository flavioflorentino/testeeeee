import UI

enum BilletFeedDetailHeaderType {
    case timeline(statusList: [BilletFeedDetailTimelineCellPresenting])
    case analyze(messageViewPresenter: BilletFeedDetailMessageViewPresenting)
    case refused(messageViewPresenter: BilletFeedDetailMessageViewPresenting)
}

protocol BilletFeedDetailHeaderPresenting {
    var headerType: BilletFeedDetailHeaderType { get }
    var image: URL? { get }
    var title: String { get }
    var status: String { get }
    var isContentHidden: Bool { get }
    var socialPresenter: BilletFeedDetailSocialViewPresenting { get }
}

struct BilletFeedDetailHeaderPresenter: BilletFeedDetailHeaderPresenting {
    var headerType: BilletFeedDetailHeaderType
    var image: URL?
    var title: String
    var status: String
    var isContentHidden: Bool
    var socialPresenter: BilletFeedDetailSocialViewPresenting
    
    init(
        headerType: BilletFeedDetailHeaderType,
        title: String,
        status: String,
        isOwner: Bool,
        imageUrl: String?,
        value: String,
        date: String,
        isPublic: Bool,
        liked: Bool,
        likeText: String
    ) {
        self.headerType = headerType
        self.image = URL(string: imageUrl ?? "")
        self.title = title
        self.status = status
        self.isContentHidden = !isOwner
        self.socialPresenter = BilletFeedDetailSocialViewPresenter(
            isValueHidden: !isOwner,
            value: value,
            date: date,
            isPublic: isPublic,
            liked: liked,
            likeText: likeText
        )
    }
}
