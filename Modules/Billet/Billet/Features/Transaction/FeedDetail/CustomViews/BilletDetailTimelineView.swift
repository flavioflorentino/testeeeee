import UI
import UIKit
import SnapKit

private extension BilletDetailTimelineView.Layout {
    enum Size {
        static let collectionViewItem = CGSize(width: 110, height: 91)
        static let collectionView = CGSize(width: 328, height: 91)
        static let buttonHeight = CGFloat(36)
    }
}

final class BilletDetailTimelineView: UIView {
    fileprivate enum Layout { }
    
    // MARK: Outlets
    
    private lazy var topContainerView = UIView()
    
    private(set) lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.itemSize = Layout.Size.collectionViewItem
        
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.showsHorizontalScrollIndicator = true
        view.register(BilletFeedDetailTimelineCell.self, forCellWithReuseIdentifier: BilletFeedDetailTimelineCell.identifier)
        return view
    }()
    
    private lazy var bottomContainerView = UIView()
    
    private lazy var followButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(followPressed), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Properties
    
    private lazy var dataSource: CollectionViewDataSource<Int, BilletFeedDetailTimelineCellPresenting> = {
        let dataSource = CollectionViewDataSource<Int, BilletFeedDetailTimelineCellPresenting>(view: collectionView)
        dataSource.itemProvider = { collectionView, indexPath, presenter -> UICollectionViewCell? in
            let cellIdentifier = BilletFeedDetailTimelineCell.identifier
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? BilletFeedDetailTimelineCell
            cell?.setup(with: presenter)
            
            return cell
        }
        return dataSource
    }()
    
    var followHandler: (() -> Void)?
    
    // MARK: - Initialization
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Setup & Action

extension BilletDetailTimelineView {
    func setup(statusList: [BilletFeedDetailTimelineCellPresenting]) {
        dataSource.add(items: statusList, to: 0)
        
        followButton.setTitle(Strings.followPayment, for: .normal)
    }
    
    @objc
    func followPressed() {
        followHandler?()
    }
}

extension BilletDetailTimelineView: ViewConfiguration {
    func buildViewHierarchy() {
        topContainerView.addSubview(collectionView)
        addSubview(topContainerView)
        bottomContainerView.addSubview(followButton)
        addSubview(bottomContainerView)
    }
    
    func setupConstraints() {
        topContainerView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
        }
        
        collectionView.snp.makeConstraints {
            $0.top.centerX.bottom.equalToSuperview()
            $0.size.equalTo(Layout.Size.collectionView)
        }
        
        bottomContainerView.snp.makeConstraints {
            $0.top.equalTo(topContainerView.snp.bottom).offset(Spacing.base00)
            $0.centerX.bottom.equalToSuperview()
            $0.width.equalTo(collectionView.snp.width)
        }
        
        followButton.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.height.equalTo(Layout.Size.buttonHeight)
        }
    }
    
    func configureStyles() {
        collectionView.backgroundColor = Colors.backgroundSecondary.color
        collectionView.dataSource = dataSource
        
        bottomContainerView.backgroundColor = Colors.backgroundSecondary.color
        
        followButton.buttonStyle(LinkButtonStyle(size: .small))
            
        if #available(iOS 11.0, *) {
            collectionView.cornerRadius = .light
            collectionView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            
            bottomContainerView.cornerRadius = .light
            bottomContainerView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        }
        
        backgroundColor = .clear
    }
}
