import UI
import UIKit
import SnapKit

protocol BilletFeedDetailHeaderViewDelegate: AnyObject {
    func didPressFollow()
    func didPressMessageButton(action: TransactionAction)
    func didPressLike(liked: Bool)
    func didPressLikedList()
}

private extension BilletFeedDetailHeaderView.Layout {
    enum Size {
        static let image = CGSize(width: 48, height: 48)
        static let iconImage = CGSize(width: 16, height: 16)
    }
}

final class BilletFeedDetailHeaderView: UIView {
    fileprivate enum Layout { }
    
    // MARK: Outlets
    
    private lazy var imageView = UIImageView()
        
    private(set) lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
        return label
    }()
    
    private lazy var iconImageView = UIImageView()
    
    private(set) lazy var statusLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale300())
        return label
    }()
    
    private lazy var containerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        return stackView
    }()
    
    private(set) lazy var timelineView = BilletDetailTimelineView()
    private(set) lazy var messageView = BilletFeedDetailMessageView()
    private(set) lazy var socialView = BilletFeedDetailSocialView()
    
    // MARK: - Properties
    
    private weak var delegate: BilletFeedDetailHeaderViewDelegate?
    
    // MARK: - Initialization
    
    init(delegate: BilletFeedDetailHeaderViewDelegate, frame: CGRect = .zero) {
        self.delegate = delegate
        
        super.init(frame: frame)
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Setup

extension BilletFeedDetailHeaderView {
    func setup(with presenter: BilletFeedDetailHeaderPresenting) {
        imageView.setImage(url: presenter.image, placeholder: Assets.Icons.barcode.image)
        imageView.cornerRadius = .full
        imageView.layer.masksToBounds = true
        
        titleLabel.text = presenter.title
        statusLabel.text = presenter.status
        
        switch (presenter.headerType, presenter.isContentHidden) {
        case (let .timeline(statusList), false):
            updateIconStatusConstraints()
            setupTimelineView(with: statusList)
            
        case (let .analyze(messagePresenter), false):
            iconImageView.image = Assets.Icons.warning.image.withRenderingMode(.alwaysTemplate)
            iconImageView.tintColor = Colors.warning600.color

            setupMessageView(with: messagePresenter)
            
        case (let .refused(messagePresenter), false):
            iconImageView.image = Assets.Icons.error.image
            setupMessageView(with: messagePresenter)
            
        default:
            statusLabel.text = ""
            updateIconStatusConstraints()
        }
        
        setupSocialView(with: presenter)
    }
    
    private func setupTimelineView(with statusList: [BilletFeedDetailTimelineCellPresenting]) {
        timelineView.isHidden = false
        timelineView.setup(statusList: statusList)
        
        timelineView.followHandler = { [weak self] in
            self?.delegate?.didPressFollow()
        }
    }
    
    private func setupMessageView(with presenter: BilletFeedDetailMessageViewPresenting) {
        messageView.isHidden = false
        messageView.setup(with: presenter)
        
        messageView.buttonHandler = { [weak self] action in
            self?.delegate?.didPressMessageButton(action: action)
        }
    }
    
    private func setupSocialView(with presenter: BilletFeedDetailHeaderPresenting) {
        socialView.setup(with: presenter.socialPresenter)
        
        socialView.likeHandler = { [weak self] liked in
            self?.delegate?.didPressLike(liked: liked)
        }
        
        socialView.likedListHandler = { [weak self] in
            self?.delegate?.didPressLikedList()
        }
    }
    
    func updateLikeInfo(withLikeText likeText: String, and isChecked: Bool) {
        socialView.updateLikeInfo(withLikeText: likeText, and: isChecked)
    }
    
    func updatePrivacyInfo(isPrivate: Bool) {
        socialView.updatePrivacyInfo(isPrivate: isPrivate)
    }
}

// MARK: - ViewConfiguration

extension BilletFeedDetailHeaderView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(imageView)
        addSubview(titleLabel)
        addSubview(iconImageView)
        addSubview(statusLabel)
        containerStackView.addArrangedSubview(timelineView)
        containerStackView.addArrangedSubview(messageView)
        addSubview(containerStackView)
        addSubview(socialView)
    }
    
    func setupConstraints() {
        imageView.snp.makeConstraints {
            $0.top.leading.equalToSuperview().inset(Spacing.base03)
            $0.size.equalTo(Layout.Size.image)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base04)
            $0.leading.equalTo(imageView.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        iconImageView.snp.makeConstraints {
            $0.centerY.equalTo(statusLabel.snp.centerY)
            $0.leading.equalTo(imageView.snp.trailing).offset(Spacing.base02)
            $0.size.equalTo(Layout.Size.iconImage)
        }
        
        statusLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base00)
            $0.leading.equalTo(iconImageView.snp.trailing).offset(Spacing.base00)
            $0.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        containerStackView.snp.makeConstraints {
            $0.top.equalTo(statusLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        socialView.snp.makeConstraints {
            $0.top.equalTo(containerStackView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    private func updateIconStatusConstraints() {
        iconImageView.snp.updateConstraints {
            $0.size.equalTo(CGSize.zero)
        }

        statusLabel.snp.updateConstraints {
            $0.leading.equalTo(iconImageView.snp.trailing)
        }
    }
    
    func configureViews() {
        timelineView.isHidden = true
        messageView.isHidden = true
        
        backgroundColor = .backgroundPrimary()
    }
}
