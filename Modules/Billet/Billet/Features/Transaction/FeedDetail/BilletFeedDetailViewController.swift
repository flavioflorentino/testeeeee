import UI
import UIKit
import SnapKit

private extension BilletFeedDetailViewController.Layout {
    enum Content {
        static let estimatedRowHeight = CGFloat(64)
        static let contentInset = UIEdgeInsets.zero
    }
}

final class BilletFeedDetailViewController: ViewController<BilletFeedDetailViewModelInputs, UIView> {
    fileprivate enum Layout { }
    
    // MARK: - Outlets
    
    private lazy var headerContainer = UIView()
    private lazy var header = BilletFeedDetailHeaderView(delegate: self)
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(BilletFeedDetailCommentCell.self, forCellReuseIdentifier: BilletFeedDetailCommentCell.identifier)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = Layout.Content.estimatedRowHeight
        tableView.contentInset = Layout.Content.contentInset
        tableView.separatorStyle = .none
        headerContainer.addSubview(header)
        tableView.tableHeaderView = headerContainer
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(sender:)))
        tableView.addGestureRecognizer(tapGesture)
        return tableView
    }()
    private lazy var composeCommentView = BilletFeedDetailComposeCommentView(delegate: self)
    private lazy var paddingView = UIView()
    internal lazy var loadingView = LoadingView()
    private lazy var errorView = GenericErrorView()
    
    // MARK: - Properties
    
    private lazy var dataSource: TableViewDataSource<Int, BilletFeedDetailCommentCellPresenting> = {
        let dataSource = TableViewDataSource<Int, BilletFeedDetailCommentCellPresenting>(view: tableView)
        dataSource.itemProvider = { tableView, indexPath, presenter -> UITableViewCell? in
            let cellIdentifier = BilletFeedDetailCommentCell.identifier
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? BilletFeedDetailCommentCell
            cell?.configure(with: presenter, index: indexPath.row, and: self)
            
            return cell
        }
        
        return dataSource
    }()
    
    private var headerHeight = CGFloat(0)
    
    private var composeCommentViewBottomConstraint: Constraint?
    
    // MARK: - Lifecycle & ViewCode
    
    override func viewDidLoad() {
        super.viewDidLoad()

        startLoadingView()
        viewModel.loadBilletInfo()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        let headerSize = header.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
    
        guard headerSize.height > headerHeight else {
            return
        }
        
        headerHeight = headerSize.height
        tableView.tableHeaderView?.frame.size = CGSize(width: view.frame.width, height: headerHeight)
    }
 
    override func buildViewHierarchy() {
        view.addSubview(errorView)
        view.addSubview(tableView)
        view.addSubview(composeCommentView)
        view.addSubview(paddingView)
    }
    
    override func setupConstraints() {
        errorView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        header.snp.makeConstraints {
            $0.width.equalToSuperview()
        }
        
        tableView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
        }
        
        composeCommentView.snp.makeConstraints {
            $0.top.equalTo(tableView.snp.bottom)
            $0.leading.trailing.equalToSuperview()
            composeCommentViewBottomConstraint = $0.bottom.equalTo(view.compatibleSafeArea.bottom).constraint
        }
        
        paddingView.snp.makeConstraints {
            $0.top.equalTo(composeCommentView.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    override func configureStyles() {
        paddingView.backgroundColor = .backgroundSecondary()
        loadingView.backgroundColor = .backgroundPrimary()
        view.backgroundColor = .backgroundPrimary()
    }
    
    override func configureViews() {
        title = Strings.tracking
        errorView.isHidden = true
        composeCommentView.isHidden = true
        
        tableView.dataSource = dataSource
    }
}

// MARK: - LoadingViewOverlayNavigationProtocol

extension BilletFeedDetailViewController: LoadingViewProtocol { }

// MARK: - Presenter Outputs

extension BilletFeedDetailViewController: BilletFeedDetailDisplay {
    func startLoading() {
        startLoadingView()
    }
    
    func stopLoading() {
        stopLoadingView()
    }

    func displayOptions() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            title: Strings.options,
            style: .plain,
            target: self,
            action: #selector(openOptions)
        )
    }
    
    func displayHeaderInfo(_ headerInfo: BilletFeedDetailHeaderPresenting) {
        stopLoadingView()
        errorView.isHidden = true
        
        header.setup(with: headerInfo)
        
        viewModel.loadComments()
    }
    
    func displayTryAgain() {
        stopLoadingView()
        view.bringSubviewToFront(errorView)
        errorView.isHidden = false
        
        errorView.tryAgainTapped = { [weak self] in
            self?.startLoadingView()
            self?.viewModel.loadBilletInfo()
        }
    }
    
    func displayComments(_ cells: [BilletFeedDetailCommentCellPresenting], isNewComment: Bool) {
        dataSource.add(items: cells, to: 0)
       
        composeCommentView.isHidden = false
        
        if isNewComment {
            let lastRow = tableView.numberOfRows(inSection: 0) - 1
            let lastIndex = IndexPath(row: lastRow, section: 0)
            tableView.scrollToRow(at: lastIndex, at: .none, animated: true)
        }
    }
    
    func displayLike(_ likeText: String, isChecked: Bool) {
        header.updateLikeInfo(withLikeText: likeText, and: isChecked)
    }
    
    func updatePrivacy(isPrivate: Bool) {
        header.updatePrivacyInfo(isPrivate: isPrivate)
    }
}

// MARK: - Actions

@objc
private extension BilletFeedDetailViewController {
    func openOptions() {
        viewModel.generateOptions()
    }
    
    func dismissKeyboard(sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            view.endEditing(true)
        }
        
        sender.cancelsTouchesInView = false
    }
    
    func keyboardWillShow(notification: NSNotification) {
        guard
            let userInfo = notification.userInfo,
            let keyboardRectValue = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
            let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue
            else {
                return
        }
        
        let offset = -keyboardRectValue.height + paddingView.frame.height
        composeCommentViewBottomConstraint?.update(offset: offset)
        
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        guard
            let userInfo = notification.userInfo,
            let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue
            else {
                return
        }
        
        composeCommentViewBottomConstraint?.update(offset: 0)
        
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
}

// MARK: - BilletFeedDetailHeaderViewDelegate

extension BilletFeedDetailViewController: BilletFeedDetailHeaderViewDelegate {
    func didPressFollow() {
        viewModel.openTimeline()
    }
    
    func didPressMessageButton(action: TransactionAction) {
        viewModel.performMessageAction(action)
    }
    
    func didPressLike(liked: Bool) {
        viewModel.like(liked)
    }
    
    func didPressLikedList() {
        viewModel.openLikedList()
    }
}

// MARK: - BilletFeedDetailComposeCommentViewDelegate

extension BilletFeedDetailViewController: BilletFeedDetailComposeCommentViewDelegate {
    func sendComment(_ comment: String) {
        viewModel.addComment(comment)
    }
}

// MARK: - BilletFeedDetailCommentCellDelegate

extension BilletFeedDetailViewController: BilletFeedDetailCommentCellDelegate {
    func didTouchProfile(at index: Int) {
        viewModel.openProfile(withIndex: index)
    }
}
