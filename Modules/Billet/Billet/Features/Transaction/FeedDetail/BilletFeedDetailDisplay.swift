import UIKit

protocol BilletFeedDetailDisplay: AnyObject {
    func startLoading()
    func stopLoading()
    func displayOptions()
    func displayHeaderInfo(_ headerInfo: BilletFeedDetailHeaderPresenting)
    func displayTryAgain()
    func displayComments(_ cells: [BilletFeedDetailCommentCellPresenting], isNewComment: Bool)
    func displayLike(_ likeText: String, isChecked: Bool)
    func updatePrivacy(isPrivate: Bool)
}
