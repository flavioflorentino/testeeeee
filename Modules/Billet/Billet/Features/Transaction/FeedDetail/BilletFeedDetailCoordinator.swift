import UIKit

enum BilletFeedDetailAction {
    case timeline(billetInfo: BilletInfoDetail)
    case accelerateAnalysis(url: String)
    case helpCenter(url: String)
    case likedList(feedId: String)
    case profile(profile: ProfileResponse)
    case optionsAlert(alert: UIAlertController)
    case popup(_ popup: UIViewController)
    case receipt(id: String)
}

protocol BilletFeedDetailCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: BilletFeedDetailAction)
}

final class BilletFeedDetailCoordinator: BilletFeedDetailCoordinating {
    weak var viewController: UIViewController?
    
    private let receiptType = "pav"
    
    func perform(action: BilletFeedDetailAction) {
        switch action {
        case .timeline(let billetInfo):
            presentTimeline(with: billetInfo)
            
        case .helpCenter(let urlString), .accelerateAnalysis(let urlString):
            presentDeepLink(withUrl: urlString)
            
        case .likedList(let feedId):
            presentLikedList(withFeedId: feedId)
            
        case .profile(let profile):
            present(profile: profile)
            
        case .optionsAlert(let alert):
            viewController?.present(alert, animated: true)
            
        case .popup(let popup):
            viewController?.showPopup(popup)
            
        case .receipt(let id):
            presentReceipt(withId: id)
        }
    }
    
    private func presentTimeline(with billetInfo: BilletInfoDetail) {
        let timelineController = BilletTimelineFactory.make(billetInfo: billetInfo)
        viewController?.navigationController?.pushViewController(timelineController, animated: true)
    }
    
    private func presentDeepLink(withUrl urlString: String) {
        guard let url = URL(string: urlString) else {
            return
        }
        BilletSetup.shared.deeplink?.open(url: url)
    }
    
    private func presentLikedList(withFeedId id: String) {
        BilletSetup.shared.contactList?.open(withFeedId: id)
    }
    
    private func present(profile: ProfileResponse) {
        guard let profileId = Int(profile.id) else { return }
        
        BilletSetup.shared.profile?.open(
            withId: profileId,
            image: profile.img,
            isPro: profile.isPro,
            username: profile.username
        )
    }
    
    private func presentReceipt(withId id: String) {
        BilletSetup.shared.receipt?.open(withId: id, receiptType: receiptType)
    }
}
