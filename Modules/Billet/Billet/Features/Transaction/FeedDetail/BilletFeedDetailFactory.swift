import Foundation
import UIKit

@objcMembers
public final class BilletFeedDetailFactory: NSObject {
    public static func make(transactionId: String, externalId: String) -> UIViewController {
        let container = DependencyContainer()
        let service: BilletFeedDetailServicing = BilletFeedDetailService(dependencies: container)
        let coordinator: BilletFeedDetailCoordinating = BilletFeedDetailCoordinator()
        let presenter: BilletFeedDetailPresenting = BilletFeedDetailPresenter(coordinator: coordinator)
        let viewModel = BilletFeedDetailViewModel(
            service: service,
            presenter: presenter,
            dependencies: container,
            transactionId: transactionId,
            externalId: externalId
        )
        let viewController = BilletFeedDetailViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
