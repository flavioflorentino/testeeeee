import UIKit

protocol BilletFeedDetailTimelineCellPresenting {
    var iconImage: UIImage { get }
    var status: String { get }
    var isCurrentStatus: Bool { get }
    var isCompleted: Bool { get }
    var isNextStatusCompleted: Bool { get }
    var isFirstStatus: Bool { get }
    var isLastStatus: Bool { get }
}

struct BilletFeedDetailTimelineCellPresenter: BilletFeedDetailTimelineCellPresenting {
    var iconImage: UIImage
    var status: String
    var isCurrentStatus: Bool
    var isCompleted: Bool
    var isNextStatusCompleted: Bool
    var isFirstStatus: Bool
    var isLastStatus: Bool
    
    init(
        status: String,
        isCurrentStatus: Bool,
        isCompleted: Bool,
        isNextStatusCompleted: Bool,
        isFirstStatus: Bool,
        isLastStatus: Bool
    ) {
        let asset = isCompleted ? Assets.Icons.approved : Assets.Icons.processing
        self.iconImage = asset.image
        self.status = status
        self.isCurrentStatus = isCurrentStatus
        self.isCompleted = isCompleted
        self.isNextStatusCompleted = isNextStatusCompleted
        self.isFirstStatus = isFirstStatus
        self.isLastStatus = isLastStatus
    }
}
