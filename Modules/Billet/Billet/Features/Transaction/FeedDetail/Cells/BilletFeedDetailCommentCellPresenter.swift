import UIKit

protocol BilletFeedDetailCommentCellPresenting {
    var profileImage: URL? { get }
    var profileName: String? { get }
    var comment: String { get }
    var datetime: String { get }
}

struct BilletFeedDetailCommentCellPresenter: BilletFeedDetailCommentCellPresenting {
    var profileImage: URL?
    var profileName: String?
    var comment: String
    var datetime: String
    
    init(
        profileImageUrl: String?,
        profileName: String?,
        comment: String,
        datetime: String
    ) {
        self.profileImage = URL(string: profileImageUrl ?? "")
        self.profileName = profileName
        self.comment = comment
        self.datetime = datetime
    }
}
