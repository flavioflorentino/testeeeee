import UI
import UIKit
import SnapKit

protocol BilletFeedDetailCommentCellDelegate: AnyObject {
    func didTouchProfile(at index: Int)
}

private extension BilletFeedDetailCommentCell.Layout {
    enum Length {
        static let icon = CGSize(width: 38, height: 38)
        static let iconCornerRadius = CGFloat(icon.width / 2)
    }
}

final class BilletFeedDetailCommentCell: UITableViewCell {
    fileprivate enum Layout { }
    
    // MARK: - Outlets
    
    private(set) lazy var iconView: UIImageView = {
        let imageView = UIImageView()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTouchImage))
        imageView.addGestureRecognizer(tapGesture)
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    private(set) lazy var profileNameLabel = UILabel()
    private(set) lazy var commentLabel = UILabel()
    private(set) lazy var datetimeLabel = UILabel()
    
    // MARK: - Properties
    
    static let identifier = String(describing: BilletFeedDetailCommentCell.self)
    
    private weak var delegate: BilletFeedDetailCommentCellDelegate?
    
    private var index: Int?
    
    override func prepareForReuse() {
        super.prepareForReuse()
        iconView.cancelRequest()
    }
}

// MARK: - Setup & Actions

extension BilletFeedDetailCommentCell {
    func configure(
        with presenter: BilletFeedDetailCommentCellPresenting,
        index: Int,
        and delegate: BilletFeedDetailCommentCellDelegate
    ) {
        self.delegate = delegate
        self.index = index
        
        iconView.setImage(url: presenter.profileImage)
        profileNameLabel.text = presenter.profileName
        commentLabel.text = presenter.comment
        datetimeLabel.text = presenter.datetime
        
        buildLayout()
    }

    @objc
    private func didTouchImage() {
        guard let index = index else {
            return
        }
        delegate?.didTouchProfile(at: index)
    }
}

// MARK: - ViewConfiguration

extension BilletFeedDetailCommentCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(iconView)
        contentView.addSubview(profileNameLabel)
        contentView.addSubview(commentLabel)
        contentView.addSubview(datetimeLabel)
    }
    
    func setupConstraints() {
        iconView.snp.makeConstraints {
            $0.top.equalToSuperview().inset(Spacing.base02)
            $0.leading.equalToSuperview().inset(Spacing.base03)
            $0.size.equalTo(Layout.Length.icon)
        }
        
        profileNameLabel.snp.makeConstraints {
            $0.top.equalToSuperview().inset(Spacing.base02)
            $0.leading.equalTo(iconView.snp.trailing).offset(Spacing.base01)
            $0.trailing.equalToSuperview().inset(Spacing.base03)
        }

        commentLabel.snp.makeConstraints {
            $0.top.equalTo(profileNameLabel.snp.bottom).offset(Spacing.base00)
            $0.leading.equalTo(iconView.snp.trailing).offset(Spacing.base01)
            $0.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        datetimeLabel.snp.makeConstraints {
            $0.top.equalTo(commentLabel.snp.bottom).offset(Spacing.base00)
            $0.leading.equalTo(iconView.snp.trailing).offset(Spacing.base01)
            $0.trailing.equalToSuperview().inset(Spacing.base03)
            $0.bottom.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureStyles() {
        iconView.layer.cornerRadius = Layout.Length.iconCornerRadius
        iconView.layer.masksToBounds = true
        
        profileNameLabel
            .labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
        
        commentLabel
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
        
        datetimeLabel.labelStyle(CaptionLabelStyle())
        
        backgroundColor = .backgroundPrimary()
    }
    
    func configureViews() {
        selectionStyle = .none
    }
}
