import UI
import UIKit
import SnapKit

private extension BilletFeedDetailTimelineCell.Layout {
    enum Size {
        static let icon = CGSize(width: 20, height: 20)
        static let lineHeight = CGFloat(1)
    }
}

final class BilletFeedDetailTimelineCell: UICollectionViewCell {
    fileprivate enum Layout { }
    
    // MARK: - Outlets
    
    private lazy var leftLine = UIView()
    private lazy var iconView = UIImageView()
    private lazy var rightLine = UIView()
    private(set) lazy var statusLabel = UILabel()

    // MARK: - Properties
    
    static let identifier = String(describing: BilletFeedDetailTimelineCell.self)
    
    // MARK: - Properties
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Setup

extension BilletFeedDetailTimelineCell {
    func setup(with presenter: BilletFeedDetailTimelineCellPresenting) {
        iconView.image = presenter.iconImage
        statusLabel.text = presenter.status
        
        leftLine.isHidden = presenter.isFirstStatus
        rightLine.isHidden = presenter.isLastStatus
        
        setupGraphicState(isCompleted: presenter.isCompleted, isNextStatusCompleted: presenter.isNextStatusCompleted)
    }
    
    func setupGraphicState(isCompleted: Bool, isNextStatusCompleted: Bool) {
        statusLabel.textColor = .grayscale300()
        leftLine.backgroundColor = .grayscale300()
        rightLine.backgroundColor = .grayscale300()
        
        if isCompleted {
            statusLabel.textColor = .success400()
            leftLine.backgroundColor = .branding800()
        
            if isNextStatusCompleted {
                rightLine.backgroundColor = .branding800()
            }
        }
    }
}

extension BilletFeedDetailTimelineCell: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(leftLine)
        addSubview(iconView)
        addSubview(rightLine)
        addSubview(statusLabel)
    }
    
    func setupConstraints() {
        leftLine.snp.makeConstraints {
            $0.centerY.equalTo(iconView.snp.centerY)
            $0.leading.equalToSuperview()
            $0.height.equalTo(Layout.Size.lineHeight)
        }
        
        iconView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.leading.equalTo(leftLine.snp.trailing).offset(Spacing.base00)
            $0.centerX.equalToSuperview()
            $0.size.equalTo(Layout.Size.icon)
        }
        
        rightLine.snp.makeConstraints {
            $0.centerY.equalTo(iconView.snp.centerY)
            $0.leading.equalTo(iconView.snp.trailing).offset(Spacing.base00)
            $0.trailing.equalToSuperview()
            $0.height.equalTo(Layout.Size.lineHeight)
        }
        
        statusLabel.snp.makeConstraints {
            $0.top.equalTo(iconView.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base01)
            $0.bottom.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureStyles() {
        statusLabel
            .labelStyle(CaptionLabelStyle())
            .with(\.textAlignment, .center)
        
        statusLabel.numberOfLines = 0
    }
}
