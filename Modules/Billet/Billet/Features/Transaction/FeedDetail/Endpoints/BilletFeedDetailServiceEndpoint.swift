import Core
import Foundation

enum BilletFeedDetailServiceEndpoint {
    case statusHistory(transactionId: String, externalId: String)
    case cancelPayment(billId: String)
    case cancelScheduledPayment(billId: String)
    case loadItemComments(feedId: String)
    case addItemComment(text: String, feedId: String)
    case likeItem(liked: Bool, feedId: String)
    case changeItemPrivacy(feedId: String)
    case genericBehavior(data: AnyCodable)
}

extension BilletFeedDetailServiceEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case let .statusHistory(transactionId, externalId):
            return "bills/transaction/\(transactionId)/statusHistory?external_id=\(externalId)"
        case .cancelPayment(let billId):
            return "bills/\(billId)/cancelation/"
        case .cancelScheduledPayment(let billId):
            return "bills/scheduled/\(billId)/cancelation"
        case .loadItemComments:
            return "api/getFeedComments.json"
        case .addItemComment:
            return "api/addFeedComment.json"
        case .likeItem:
            return "api/likeFeedItem.json"
        case .changeItemPrivacy:
            return "api/changeFeedItemPrivacy.json"
        case .genericBehavior:
            return "api/feedItemAction.json"
        }
    }
    
    var method: HTTPMethod {
        guard case .statusHistory = self else {
            return .post
        }

        return .get
    }
    
    var body: Data? {
        switch self {
        case let .loadItemComments(feedId):
            return [
                "feed_common_data_id": feedId
            ].toData()
            
        case let .addItemComment(text, feedId):
            return [
                "text": text,
                "feed_common_data_id": feedId
            ].toData()
            
        case let .likeItem(liked, feedId):
            return [
                "like_value": (liked ? "1" : "0"),
                "feed_common_data_id": feedId
            ].toData()
            
        case let .changeItemPrivacy(feedId):
            return [
                "privacy": "2",
                "feed_common_data_id": feedId
            ].toData()
            
        case let .genericBehavior(data):
            guard let dict = data.value as? [String: Any] else {
                return nil
            }
            
            let keys = ["id", "action"]
            
            return capitalizeKeyIfNeeded(with: dict, and: keys).toData()
        
        default:
            return nil
        }
    }
    
    private func capitalizeKeyIfNeeded(with dict: [String: Any], and keys: [String]) -> [String: Any] {
        var dict = dict
        
        for key in dict.keys {
            for matchKey in keys where key == matchKey {
                dict[key.capitalized] = dict.removeValue(forKey: key)
            }
        }
        
        return dict
    }
}
