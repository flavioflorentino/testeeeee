import UI
import UIKit
import AnalyticsModule

protocol BilletFeedDetailViewModelInputs: AnyObject {
    func loadBilletInfo()
    func cancelPayment(withId id: String, type: TransactionActionType)
    func loadComments()
    func addComment(_ comment: String)
    func like(_ liked: Bool)
    func makePrivate()
    func genericBehavior(with action: TransactionAction)
    
    func generateOptions()
    func openTimeline()
    func performMessageAction(_ action: TransactionAction)
    func openLikedList()
    func openProfile(withIndex index: Int)
}

final class BilletFeedDetailViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    private let service: BilletFeedDetailServicing
    private let presenter: BilletFeedDetailPresenting
    private let transactionId: String
    private let externalId: String
    
    private var billetInfo: BilletInfoDetail?
    private var comments: [CommentResponse] = []
    
    private let feedNotification = "FeedNeedReload"
    
    init(
        service: BilletFeedDetailServicing,
        presenter: BilletFeedDetailPresenting,
        dependencies: Dependencies,
        transactionId: String,
        externalId: String
    ) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.transactionId = transactionId
        self.externalId = externalId
    }
}

extension BilletFeedDetailViewModel: BilletFeedDetailViewModelInputs {
    // MARK: - Services
    
    func loadBilletInfo() {
        service.getStatusHistory(withTransactionId: transactionId, andExternalId: externalId) { [weak self] result in
            switch result {
            case .success(let billetInfo):
                self?.billetInfo = billetInfo

                if billetInfo.actions.isNotEmpty { self?.presenter.presentOptions() }
                
                guard let headerType = self?.makeHeaderType(with: billetInfo) else { self?.presenter.handleTryAgain(); return }
                
                let headerInfo = BilletFeedDetailHeaderPresenter(
                    headerType: headerType,
                    title: billetInfo.title,
                    status: billetInfo.currentStatusTitle,
                    isOwner: billetInfo.isOwner,
                    imageUrl: billetInfo.userImage,
                    value: billetInfo.value,
                    date: billetInfo.date,
                    isPublic: billetInfo.isPublic,
                    liked: billetInfo.liked,
                    likeText: billetInfo.likeText
                )

                self?.presenter.handleBilletInfoDetail(headerInfo)
                
                self?.dependencies.analytics.log(TransactionEvent.billetFeedDetailViewed(billetInfo.currentStatus))

            case .failure:
                self?.presenter.handleTryAgain()
            }
        }
    }
    
    private func makeHeaderType(with billetInfo: BilletInfoDetail) -> BilletFeedDetailHeaderType {
        let messsageStatus: [FollowUpStatusType] = [.inAnalysis, .accelerateAnalysis, .scheduled, .refused, .canceled, .scheduledCancelled, .scheduledRefused]
        
        if messsageStatus.contains(billetInfo.currentStatus) {
            let status = billetInfo.timeline.first { $0.status == billetInfo.currentStatus }
            
            let presenter = BilletFeedDetailMessageViewPresenter(
                date: status?.date ?? "",
                description: status?.message ?? "",
                actions: status?.actions ?? []
            )
            
            guard billetInfo.currentStatus == .refused
                    || billetInfo.currentStatus == .canceled
                    || billetInfo.currentStatus == .scheduledCancelled
                    || billetInfo.currentStatus == .scheduledRefused else {
                return .analyze(messageViewPresenter: presenter)
            }
            
            return .refused(messageViewPresenter: presenter)
        }
        
        let reversedTimeline: [TimelineStatus] = billetInfo.timeline.reversed()
        var list = [BilletFeedDetailTimelineCellPresenter]()
        
        for (index, status) in reversedTimeline.enumerated() {
            let nextStatus = reversedTimeline[safe: index + 1]
            let isNextStatusCompleted = nextStatus?.isCompleted ?? false
            
            let isFirstStatus = index == reversedTimeline.startIndex
            let isLastStatus = index == reversedTimeline.endIndex - 1
            
            let status = BilletFeedDetailTimelineCellPresenter(
                status: status.label,
                isCurrentStatus: billetInfo.currentStatus == status.status,
                isCompleted: status.isCompleted,
                isNextStatusCompleted: isNextStatusCompleted,
                isFirstStatus: isFirstStatus,
                isLastStatus: isLastStatus
            )
            list.append(status)
        }
        
        return .timeline(statusList: list)
    }
    
    func cancelPayment(withId id: String, type: TransactionActionType) {
        presenter.handleStartLoading()
        
        service.cancelPayment(withBillId: id, type: type) { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .success:
                self.loadBilletInfo()
            case .failure:
                self.presenter.handleStopLoading()
                
                let action = PopupAction(title: Strings.back, style: .fill)
                
                let popup = self.makePopup(withTitle: Strings.cancelPaymentErrorTitle,
                                           message: Strings.cancelPaymentErrorMessage,
                                           primaryAction: action,
                                           isCloseButtonHidden: true)
                
                self.presenter.didNextStep(action: .popup(popup))
            }
        }
    }
    
    func loadComments() {
        guard let feedId = billetInfo?.feedId else { return }
        
        service.getComments(withFeedId: feedId) { [weak self] result in
            guard let self = self, case .success(let commentsResponse) = result else { return }
            
            self.comments.append(contentsOf: commentsResponse.comments)
            
            let cells = self.comments.map {
                BilletFeedDetailCommentCellPresenter(
                    profileImageUrl: $0.profile?.img,
                    profileName: $0.profile?.name,
                    comment: $0.text.decodingHTMLEntities(),
                    datetime: $0.date
                )
            }
            
            self.presenter.handleComments(cells, isNewComment: false)
        }
    }
    
    func addComment(_ comment: String) {
        guard let feedId = billetInfo?.feedId else { return }
        
        service.addComment(withComment: comment, andFeedId: feedId) { [weak self] result in
            guard let self = self, case .success(let commentsResponse) = result else { return }
            
            self.comments.append(contentsOf: commentsResponse.comments)
            
            let cells = self.comments.map {
                BilletFeedDetailCommentCellPresenter(
                    profileImageUrl: $0.profile?.img,
                    profileName: $0.profile?.name,
                    comment: $0.text.decodingHTMLEntities(),
                    datetime: $0.date
                )
            }
            
            self.presenter.handleComments(cells, isNewComment: true)
            self.presenter.handleLike(commentsResponse.likes.text, isChecked: commentsResponse.likes.isChecked)
            self.notifyFeed()
        }
    }
    
    func like(_ liked: Bool) {
        guard let feedId = billetInfo?.feedId else { return }
        
        service.like(with: liked, andFeedId: feedId) { [weak self] result in
            guard case .success(let likeResponse) = result else { return }

            self?.presenter.handleLike(likeResponse.text, isChecked: likeResponse.isChecked)
            self?.notifyFeed()
        }
    }
    
    func makePrivate() {
        guard let feedId = billetInfo?.feedId else { return }
        
        service.makePrivate(withFeedId: feedId) { [weak self] result in
            guard case .success = result else { return }
        
            self?.billetInfo?.actions.removeAll {
                $0.title.contains(Strings.private)
            }
            
            self?.presenter.handlePrivacy(isPrivate: true)
            self?.notifyFeed()
        }
    }
    
    func genericBehavior(with action: TransactionAction) {
        guard let data = action.data else { return }
        
        service.genericBehavior(with: data) { [weak self] result in
            guard case .success(let behaviorResponse) = result else { return }
            
            if let alert = behaviorResponse.alert {
                self?.presenter.didNextStep(action: .optionsAlert(alert: alert.generate()))
            }
            
            self?.billetInfo?.actions.removeAll {
                $0.title.contains(Strings.notifications)
            }
            
            self?.notifyFeed()
        }
    }
    
    // MARK: - Actions
    
    func generateOptions() {
        guard let billetInfo = billetInfo, billetInfo.actions.isNotEmpty else { return }
        
        let optionsAlert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        billetInfo.actions.forEach { action in
            let buttonAction = UIAlertAction(title: action.title, style: .default) { _ in
                self.performOptionsAction(action)
            }
            optionsAlert.addAction(buttonAction)
        }
        
        let cancelAction = UIAlertAction(title: Strings.cancel, style: .cancel)
        optionsAlert.addAction(cancelAction)
        
        presenter.didNextStep(action: .optionsAlert(alert: optionsAlert))
    }
    
    func openTimeline() {
        guard let billetInfo = billetInfo else { return }
        
        presenter.didNextStep(action: .timeline(billetInfo: billetInfo))
        
        let event = TransactionEvent.billetFeedDetailActionSelected(.followPayment)
        dependencies.analytics.log(event)
    }

    func performMessageAction(_ action: TransactionAction) {
        switch action.type {
        case .accelerateAnalysis:
            guard let url = action.url else { return }
            presenter.didNextStep(action: .accelerateAnalysis(url: url))
            
            let event = TransactionEvent.billetFeedDetailActionSelected(.accelerateAnalysis)
            dependencies.analytics.log(event)
            
        case .helpCenter:
            guard let url = action.url else { return }
            presenter.didNextStep(action: .helpCenter(url: url))
            
            let event = TransactionEvent.billetFeedDetailActionSelected(.knowMore)
            dependencies.analytics.log(event)
            
        case .cancel, .scheduledCancel:
            guard let id = action.id, let billetInfo = billetInfo else { return }
            
            let primaryAction = PopupAction(title: Strings.cancelPaymentWarningAction, style: .destructive) {
                self.cancelPayment(withId: id, type: action.type)
                
                var event: TransactionEvent = .billetCancelPaymentModalViewed(.cancelPayment, billetInfo.currentStatus)
                
                if case .scheduledCancel = action.type {
                    event = .billetScheduledCancelPaymentModalViewed
                }
                
                self.dependencies.analytics.log(event)
            }
            
            let secondaryAction = PopupAction(title: Strings.notNow, style: .link) {
                let event = TransactionEvent.billetCancelPaymentModalViewed(nil, billetInfo.currentStatus)
                self.dependencies.analytics.log(event)
            }
            
            let formattedString = action.message?.replacingOccurrences(of: "<br/>", with: "\n")
            
            let popup = makePopup(withTitle: Strings.cancelPaymentWarningTitle,
                                  message: formattedString ?? Strings.cancelPaymentWarningMessage,
                                  type: .image(Assets.Icons.warningBig.image),
                                  primaryAction: primaryAction,
                                  secondaryAction: secondaryAction)
            
            presenter.didNextStep(action: .popup(popup))
            
            let event = TransactionEvent.billetFeedDetailActionSelected(.cancelPayment)
            dependencies.analytics.log(event)
            
        default:
            break
        }
    }
    
    func openLikedList() {
        guard let feedId = billetInfo?.feedId else { return }
        
        presenter.didNextStep(action: .likedList(feedId: feedId))
    }
    
    func openProfile(withIndex index: Int) {
        guard let currentComment = comments[safe: index], let profile = currentComment.profile else { return }
        
        presenter.didNextStep(action: .profile(profile: profile))
    }
}

// MARK: - Private Auxilary Methods

private extension BilletFeedDetailViewModel {
    func notifyFeed() {
        let notification = NSNotification.Name(rawValue: feedNotification)
        NotificationCenter.default.post(name: notification, object: nil)
    }
    
    func performOptionsAction(_ action: TransactionAction) {
        switch action.type {
        case .receipt:
            guard let id = action.id else { return }
            presenter.didNextStep(action: .receipt(id: id))
            
        case .switchPrivacy:
            let title = Strings.switchPrivacyAlertTitle
            let message = Strings.switchPrivacyAlertMessage
            
            let alert = makeAlert(withTitle: title, message: message, style: .alert) { [weak self] in
                self?.makePrivate()
            }
            
            presenter.didNextStep(action: .optionsAlert(alert: alert))
            
        case .generic:
            guard let message = action.message else {
                genericBehavior(with: action); return
            }
            
            let alert = makeAlert(withTitle: action.title, message: message, style: .alert) { [weak self] in
                self?.genericBehavior(with: action)
            }
            
            presenter.didNextStep(action: .optionsAlert(alert: alert))
            
        default:
            break
        }
    }
    
    func makeAlert(withTitle title: String?,
                   message: String?,
                   style: UIAlertController.Style,
                   action: @escaping () -> Void) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: style)
        
        let cancelAction = UIAlertAction(title: Strings.no, style: .cancel)
        alert.addAction(cancelAction)
        
        let defaultAction = UIAlertAction(title: Strings.yes, style: .default) { _ in
            action()
        }
        alert.addAction(defaultAction)
        
        return alert
    }
    
    func makePopup(withTitle title: String,
                   message: String,
                   type: PopupType = .none,
                   primaryAction: PopupAction? = nil,
                   secondaryAction: PopupAction? = nil,
                   isCloseButtonHidden: Bool = false) -> UIViewController {
        let popup = PopupViewController(title: title, description: message, preferredType: type)
        popup.hideCloseButton = isCloseButtonHidden
        
        if let action = primaryAction {
            popup.addAction(action)
        }
        
        if let action = secondaryAction {
            popup.addAction(action)
        }
        
        return popup
    }
}
