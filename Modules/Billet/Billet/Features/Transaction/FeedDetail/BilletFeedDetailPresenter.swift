import Core
import Foundation

protocol BilletFeedDetailPresenting: AnyObject {
    var viewController: BilletFeedDetailDisplay? { get set }
    
    func handleBilletInfoDetail(_ headerInfo: BilletFeedDetailHeaderPresenting)
    func presentOptions()
    func handleTryAgain()
    func handleStartLoading()
    func handleStopLoading()
    func handleComments(_ comments: [BilletFeedDetailCommentCellPresenting], isNewComment: Bool)
    func handleLike(_ likeText: String, isChecked: Bool)
    func handlePrivacy(isPrivate: Bool)
    func didNextStep(action: BilletFeedDetailAction)
}

final class BilletFeedDetailPresenter: BilletFeedDetailPresenting {
    private let coordinator: BilletFeedDetailCoordinating
    weak var viewController: BilletFeedDetailDisplay?

    init(coordinator: BilletFeedDetailCoordinating) {
        self.coordinator = coordinator
    }
    
    func handleBilletInfoDetail(_ headerInfo: BilletFeedDetailHeaderPresenting) {
        viewController?.displayHeaderInfo(headerInfo)
    }

    func presentOptions() {
        viewController?.displayOptions()
    }
    
    func handleTryAgain() {
        viewController?.displayTryAgain()
    }
    
    func handleStartLoading() {
        viewController?.startLoading()
    }
    
    func handleStopLoading() {
        viewController?.stopLoading()
    }
    
    func handleComments(_ comments: [BilletFeedDetailCommentCellPresenting], isNewComment: Bool) {
        viewController?.displayComments(comments, isNewComment: isNewComment)
    }
    
    func handleLike(_ likeText: String, isChecked: Bool) {
        viewController?.displayLike(likeText, isChecked: isChecked)
    }
    
    func handlePrivacy(isPrivate: Bool) {
        viewController?.updatePrivacy(isPrivate: isPrivate)
    }
    
    func didNextStep(action: BilletFeedDetailAction) {
        coordinator.perform(action: action)
    }
}
