struct CommentsResponse: Decodable {
    let profiles: [ProfileResponse]
    var comments: [CommentResponse]
    let likes: LikeResponse
    
    private enum CodingKeys: String, CodingKey {
        case data
        case members
        case comments
        case likes
    }
    
    init(profiles: [ProfileResponse], comments: [CommentResponse], likes: LikeResponse) {
        self.profiles = profiles
        self.likes = likes
        
        self.comments = []
        
        for var comment in comments {
            if let profile = profiles.first(where: { $0.id == comment.consumerId }) {
                comment.profile = profile
                self.comments.append(comment)
            }
        }
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let data = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        let members = try data.decode([String: ProfileResponse].self, forKey: .members)
        
        profiles = members.map(\.value)
        likes = try data.decode(LikeResponse.self, forKey: .likes)
        
        let auxComments = try data.decodeIfPresent([CommentResponse].self, forKey: .comments) ?? []
        
        comments = []
        
        for var comment in auxComments {
            if let profile = profiles.first(where: { $0.id == comment.consumerId }) {
                comment.profile = profile
                comments.append(comment)
            }
        }
    }
}

struct CommentResponse: Decodable {
    let id: String
    let text: String
    let date: String
    let consumerId: String
    var profile: ProfileResponse?
    
    private enum CodingKeys: String, CodingKey {
        case id = "Id"
        case text = "Text"
        case date = "TimeAgo"
        case consumerId = "ConsumerId"
        case profile
    }
    
    init(id: String, text: String, date: String, consumerId: String, profile: ProfileResponse?) {
        self.id = id
        self.text = text
        self.date = date
        self.consumerId = consumerId
        self.profile = profile
    }
}
