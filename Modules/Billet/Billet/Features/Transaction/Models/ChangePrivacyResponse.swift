import Core

struct ChangePrivacyResponse: Decodable {
    let actions: [TransactionAction]
    let config: AnyCodable
    
    private enum CodingKeys: String, CodingKey {
        case data
        case actions
        case config
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let data = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        
        actions = try data.decode([TransactionAction].self, forKey: .actions)
        config = try data.decode(AnyCodable.self, forKey: .config)
    }
}
