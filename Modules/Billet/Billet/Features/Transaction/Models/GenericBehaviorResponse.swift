import Core
import UIKit

enum GenericBehaviorType: String, Decodable {
    case reload
    case reloadItem = "reload_item"
    case undefined = ""
}

struct GenericBehaviorResponse: Decodable {
    let type: GenericBehaviorType
    let alert: Alert?
    
    struct Alert: Decodable {
        let title: String
        let message: String
        var buttonTitle: String = "Ok"
        
        func generate() -> UIAlertController {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let action = UIAlertAction(title: buttonTitle, style: .default)
            alert.addAction(action)
            return alert
        }
    }
    
    private enum CodingKeys: String, CodingKey {
        case data
        case behavior
        case type
        case alert
    }
    
    init(type: GenericBehaviorType, alert: Alert) {
        self.type = type
        self.alert = alert
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let data = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        let behavior = try data.nestedContainer(keyedBy: CodingKeys.self, forKey: .behavior)
        
        type = try behavior.decode(GenericBehaviorType.self, forKey: .type)
        alert = try behavior.decode(Alert.self, forKey: .alert)
    }
}
