import Core

enum TransactionActionType: String, Decodable {
    case history = "bill_history"
    case receipt = "pav_receipt"
    case debitReceipt = "card_debit_receipt"
    case helpCenter = "faq_more"
    case accelerateAnalysis = "accelerate_analysis"
    case switchPrivacy = "make_private"
    case disableNotification = "enable_comment_updates"
    case hideItem = "hide_feed_item"
    case generic
    case cancel = "payment_cancel"
    case scheduledReceipt = "pav_receipt_scheduled"
    case scheduledCancel = "scheduled_cancel"
    case scheduledCancelConfirm = "scheduled_cancel_confirm"
}

struct TransactionAction: Decodable {
    let title: String
    let message: String?
    let type: TransactionActionType
    let data: AnyCodable?
    let id: String?
    let url: String?
    let value: Bool
    let action: TransactionActionType?
    
    private enum CodingKeys: String, CodingKey {
        case title = "label"
        case message
        case type
        case data
        case id
        case url
        case value
        case action
    }
    
    init(
        title: String,
        message: String?,
        type: TransactionActionType,
        data: AnyCodable?,
        id: String?,
        url: String?,
        value: Bool,
        action: TransactionActionType?
    ) {
        self.title = title
        self.message = message
        self.type = type
        self.data = data
        self.id = id
        self.url = url
        self.value = value
        self.action = action
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let data = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        
        self.data = try container.decodeIfPresent(AnyCodable.self, forKey: .data)
        
        title = try container.decode(String.self, forKey: .title)
        message = try container.decodeIfPresent(String.self, forKey: .message)
        type = try container.decode(TransactionActionType.self, forKey: .type)
        id = try data.decodeIfPresent(String.self, forKey: .id)
        url = try data.decodeIfPresent(String.self, forKey: .url)
        value = try data.decodeIfPresent(Bool.self, forKey: .value) ?? false
        action = try data.decodeIfPresent(TransactionActionType.self, forKey: .action)
    }
}
