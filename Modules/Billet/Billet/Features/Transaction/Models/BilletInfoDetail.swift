public struct BilletInfoDetail: Decodable {
    let feedId: String
    let isOwner: Bool
    let title: String
    let currentStatusTitle: String
    let currentStatus: FollowUpStatusType
    let userImage: String?
    let value: String
    let date: String
    let isPublic: Bool
    let liked: Bool
    let likeText: String
    let timeline: [TimelineStatus]
    var actions: [TransactionAction]

    private enum CodingKeys: String, CodingKey {
        case feedId = "feed_item_id"
        case isOwner = "is_owner"
        case header
        case title
        case currentStatusTitle = "status_current_label"
        case currentStatus = "status_value"
        case config
        case privacyType = "privacy_type"
        case userImage = "image_url"
        case value = "money_value"
        case date = "time_ago"
        case actions
        case isPublic
        case liked
        case likeText = "like_text"
        case timeline
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let header = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .header)
        let config = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .config)
        
        feedId = try container.decode(String.self, forKey: .feedId)
        isOwner = try container.decode(Bool.self, forKey: .isOwner)
        
        title = try header.decode(String.self, forKey: .title)
        currentStatusTitle = try header.decode(String.self, forKey: .currentStatusTitle)
        currentStatus = try header.decode(FollowUpStatusType.self, forKey: .currentStatus)
        
        userImage = try config.decodeIfPresent(String.self, forKey: .userImage)
        value = try config.decode(String.self, forKey: .value)
        date = try config.decode(String.self, forKey: .date)
        actions = try config.decode([TransactionAction].self, forKey: .actions)
        
        let privacyType = try config.decode(String.self, forKey: .privacyType)
        isPublic = privacyType == "friends"
        
        liked = try config.decode(Bool.self, forKey: .liked)
        likeText = try config.decode(String.self, forKey: .likeText)
        
        timeline = try container.decode([TimelineStatus].self, forKey: .timeline)
    }
    
    init(
        feedId: String,
        isOwner: Bool,
        title: String,
        currentStatusTitle: String,
        currentStatus: FollowUpStatusType,
        userImage: String,
        value: String,
        date: String,
        actions: [TransactionAction],
        isPublic: Bool,
        liked: Bool,
        likeText: String,
        timeline: [TimelineStatus]
    ) {
        self.feedId = feedId
        self.isOwner = isOwner
        self.title = title
        self.currentStatusTitle = currentStatusTitle
        self.currentStatus = currentStatus
        self.userImage = userImage
        self.value = value
        self.date = date
        self.actions = actions
        self.isPublic = isPublic
        self.liked = liked
        self.likeText = likeText
        self.timeline = timeline
    }
}
