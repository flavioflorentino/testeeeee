struct LikeResponse: Decodable {
    let text: String
    let isChecked: Bool
    
    private enum CodingKeys: String, CodingKey {
        case data
        case text = "Text"
        case isChecked = "Checked"
    }
    
    init(text: String, isChecked: Bool) {
        self.text = text
        self.isChecked = isChecked
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if container.contains(.data) {
            let data = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
            text = try data.decode(String.self, forKey: .text)
            isChecked = try data.decode(Bool.self, forKey: .isChecked)
        } else {
            text = try container.decode(String.self, forKey: .text)
            isChecked = try container.decode(Bool.self, forKey: .isChecked)
        }
    }
}
