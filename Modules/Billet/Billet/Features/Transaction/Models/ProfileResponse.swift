struct ProfileResponse: Decodable {
    let id: String
    let name: String
    let username: String
    let img: String
    let isPro: Bool
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case username
        case img = "img_url"
        case isPro = "is_pro"
    }
    
    init(id: String, name: String, username: String, img: String, isPro: Bool) {
        self.id = id
        self.name = name
        self.username = username
        self.img = img
        self.isPro = isPro
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        name = try container.decode(String.self, forKey: .name)
        username = try container.decode(String.self, forKey: .username)
        img = try container.decode(String.self, forKey: .img)
        let isProString = try container.decode(String.self, forKey: .isPro)
        isPro = isProString != "0"
    }
}
