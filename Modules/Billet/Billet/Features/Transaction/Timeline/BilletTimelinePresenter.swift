import Core
import Foundation
import UI

protocol BilletTimelinePresenting: AnyObject {
    var viewController: BilletTimelineDisplay? { get set }
    
    func presentTimelineInfo(with title: String, statusTitle: String, and cells: [BilletTimelineCellPresenting])
    func didNextStep(action: BilletTimelineAction)
}

final class BilletTimelinePresenter: BilletTimelinePresenting {
    private let coordinator: BilletTimelineCoordinating
    weak var viewController: BilletTimelineDisplay?

    init(coordinator: BilletTimelineCoordinating) {
        self.coordinator = coordinator
    }
    
    func presentTimelineInfo(with title: String, statusTitle: String, and cells: [BilletTimelineCellPresenting]) {
        viewController?.displayTimelineInfo(with: title, statusTitle: statusTitle, and: cells)
    }
    
    func didNextStep(action: BilletTimelineAction) {
        coordinator.perform(action: action)
    }
}
