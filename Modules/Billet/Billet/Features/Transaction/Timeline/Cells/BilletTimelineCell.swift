import UI
import UIKit

protocol BilletTimelineCellDelegate: AnyObject {
    func didTouchAction(_ action: TransactionAction)
}

private extension BilletTimelineCell.Layout {
    enum Font {
        static let size00 = UIFont.systemFont(ofSize: 12, weight: .regular)
        static let size01 = UIFont.systemFont(ofSize: 14, weight: .regular)
        static let size02 = UIFont.systemFont(ofSize: 14, weight: .bold)
    }
    
    enum Length {
        static let iconWidth = CGFloat(20)
        static let iconHeight = CGFloat(20)
        static let lineWidth = CGFloat(1)
    }
    
    enum Spacing {
        static let base00 = CGFloat(34)
    }
}

final class BilletTimelineCell: UITableViewCell {
    fileprivate enum Layout { }
    
    // MARK: Outlets
    
    private lazy var topline = UIView()
    
    private lazy var iconView = UIImageView()
    
    private lazy var bottomline = UIView()

    private(set) lazy var dateLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.size00
        return label
    }()
    
    private(set) lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = Layout.Font.size02
        return label
    }()
    
    private(set) lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = Layout.Font.size01
        return label
    }()
    
    private lazy var containerButton: UIStackView = {
        let view = UIStackView()
        view.axis = .horizontal
        view.spacing = Spacing.base01
        return view
    }()
    
    private(set) lazy var primaryButton: UIButton = {
        let button = UIButton(type: .custom)
        button.isHidden = true
        button.addTarget(self, action: #selector(primaryButtonTapped), for: .touchUpInside)
        return button
    }()
    
    private(set) lazy var secondaryButton: UIButton = {
        let button = UIButton(type: .custom)
        button.isHidden = true
        button.contentHorizontalAlignment = .left
        button.addTarget(self, action: #selector(secondaryButtonTapped), for: .touchUpInside)
        return button
    }()
    
    static let identifier = String(describing: BilletTimelineCell.self)
    
    private weak var delegate: BilletTimelineCellDelegate?
    
    private var hasActions = false
    private var primaryAction: TransactionAction?
    private var secondaryAction: TransactionAction?
}

// MARK: - Setups

extension BilletTimelineCell {
    func setup(with presenter: BilletTimelineCellPresenting, and delegate: BilletTimelineCellDelegate) {
        self.delegate = delegate
        
        iconView.image = presenter.isCompleted ? Assets.Icons.approved.image : Assets.Icons.processing.image
        dateLabel.text = presenter.date
        titleLabel.text = presenter.title
        messageLabel.text = presenter.message
        
        hasActions = presenter.actions.isNotEmpty
        
        presenter.actions.forEach { action in
            setupButton(with: action)
        }
        
        topline.isHidden = presenter.isFirstStatus
        bottomline.isHidden = presenter.isLastStatus
        
        setupGraphicState(isCompleted: presenter.isCompleted, isNextStatusCompleted: presenter.isNextStatusCompleted)
        
        buildLayout()
    }
    
    private func setupButton(with action: TransactionAction) {
        switch action.type {
        case .receipt:
            primaryAction = action
            primaryButton.setTitle(action.title, for: .normal)
            primaryButton.isHidden = false
            primaryButton.buttonStyle(PrimaryButtonStyle(size: .small))
            
        case .helpCenter:
            secondaryAction = action
            secondaryButton.setTitle(action.title, for: .normal)
            secondaryButton.isHidden = false
            secondaryButton.buttonStyle(LinkButtonStyle(size: .small))
            
        default:
            break
        }
    }
    
    private func setupGraphicState(isCompleted: Bool, isNextStatusCompleted: Bool) {
        dateLabel.textColor = Colors.grayscale300.color
        titleLabel.textColor = Colors.grayscale300.color
        messageLabel.textColor = Colors.grayscale300.color
        
        topline.backgroundColor = Colors.grayscale300.color
        bottomline.backgroundColor = Colors.grayscale300.color
        
        if isCompleted {
            dateLabel.textColor = Colors.grayscale700.color
            titleLabel.textColor = Colors.grayscale700.color
            messageLabel.textColor = Colors.grayscale700.color
            
            bottomline.backgroundColor = Colors.branding800.color
            
            if isNextStatusCompleted {
                topline.backgroundColor = Colors.branding800.color
                bottomline.backgroundColor = Colors.branding800.color
            }
        }
    }
}

// MARK: - Actions

@objc
private extension BilletTimelineCell {
    func primaryButtonTapped() {
        guard let primaryAction = primaryAction else {
            return
        }
        delegate?.didTouchAction(primaryAction)
    }
    
    func secondaryButtonTapped() {
        guard let secondaryAction = secondaryAction else {
            return
        }
        delegate?.didTouchAction(secondaryAction)
    }
}

// MARK: - ViewConfiguration

extension BilletTimelineCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(topline)
        contentView.addSubview(iconView)
        contentView.addSubview(bottomline)
        contentView.addSubview(dateLabel)
        contentView.addSubview(titleLabel)
        contentView.addSubview(messageLabel)
        
        guard hasActions else {
            return
        }
        
        containerButton.addArrangedSubview(primaryButton)
        containerButton.addArrangedSubview(secondaryButton)
        contentView.addSubview(containerButton)
    }
    
    func setupConstraints() {
        dateLabel.layout {
            $0.top == contentView.topAnchor + Spacing.base02
            $0.leading == iconView.trailingAnchor + Spacing.base02
            $0.trailing == contentView.trailingAnchor + Spacing.base03
        }
        
        topline.layout {
            $0.width == Layout.Length.lineWidth
            $0.top == contentView.topAnchor
            $0.leading == contentView.leadingAnchor + Layout.Spacing.base00
            $0.bottom == iconView.topAnchor - Spacing.base00
        }

        iconView.layout {
            $0.width == Layout.Length.iconWidth
            $0.height == Layout.Length.iconHeight
            $0.top == dateLabel.bottomAnchor + Spacing.base00
            $0.leading == contentView.leadingAnchor + Spacing.base03
        }
        
        bottomline.layout {
            $0.width == Layout.Length.lineWidth
            $0.top == iconView.bottomAnchor + Spacing.base00
            $0.leading == contentView.leadingAnchor + Layout.Spacing.base00
            $0.bottom == contentView.bottomAnchor
        }
        
        titleLabel.layout {
            $0.top == dateLabel.bottomAnchor + Spacing.base00
            $0.leading == iconView.trailingAnchor + Spacing.base02
            $0.trailing == contentView.trailingAnchor - Spacing.base03
        }
        
        messageLabel.layout {
            $0.top == titleLabel.bottomAnchor + Spacing.base01
            $0.leading == iconView.trailingAnchor + Spacing.base02
            $0.trailing == contentView.trailingAnchor - Spacing.base03
           
            if !hasActions {
                $0.bottom == contentView.bottomAnchor - Spacing.base02
            }
        }
        
        setupActionConstraintsIfNeeded()
    }
    
    private func setupActionConstraintsIfNeeded() {
        guard hasActions else {
            return
        }
        
        containerButton.layout {
            $0.top == messageLabel.bottomAnchor + Spacing.base02
            $0.leading == iconView.trailingAnchor + Spacing.base02
            $0.trailing == contentView.trailingAnchor - Spacing.base03
            $0.bottom == contentView.bottomAnchor - Spacing.base02
        }

        secondaryButton.layout {
            $0.width == primaryButton.widthAnchor
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
        selectionStyle = .none
    }
}
