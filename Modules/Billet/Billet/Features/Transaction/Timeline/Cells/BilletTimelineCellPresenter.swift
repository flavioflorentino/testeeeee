protocol BilletTimelineCellPresenting {
    var date: String? { get }
    var title: String { get }
    var message: String { get }
    var actions: [TransactionAction] { get }
    var isCurrentStatus: Bool { get }
    var isCompleted: Bool { get }
    var isNextStatusCompleted: Bool { get }
    var isFirstStatus: Bool { get }
    var isLastStatus: Bool { get }
}

struct BilletTimelineCellPresenter: BilletTimelineCellPresenting {
    var date: String?
    var title: String
    var message: String
    var actions: [TransactionAction]
    var isCurrentStatus: Bool
    var isCompleted: Bool
    var isNextStatusCompleted: Bool
    var isFirstStatus: Bool
    var isLastStatus: Bool
    
    init(
        title: String,
        message: String,
        isCurrentStatus: Bool,
        isCompleted: Bool,
        isNextStatusCompleted: Bool,
        isFirstStatus: Bool,
        isLastStatus: Bool,
        date: String? = nil,
        actions: [TransactionAction] = []
    ) {
        self.date = date
        self.title = title
        self.message = message
        self.actions = actions
        self.isCurrentStatus = isCurrentStatus
        self.isCompleted = isCompleted
        self.isNextStatusCompleted = isNextStatusCompleted
        self.isFirstStatus = isFirstStatus
        self.isLastStatus = isLastStatus
    }
}
