import Foundation

enum BilletTimelineFactory {
    static func make(billetInfo: BilletInfoDetail) -> BilletTimelineViewController {
        let container = DependencyContainer()
        let coordinator: BilletTimelineCoordinating = BilletTimelineCoordinator()
        let presenter: BilletTimelinePresenting = BilletTimelinePresenter(coordinator: coordinator)
        let viewModel = BilletTimelineViewModel(
            presenter: presenter,
            dependencies: container,
            billetInfo: billetInfo
        )
        let viewController = BilletTimelineViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
