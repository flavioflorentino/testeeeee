import UI
import UIKit

private extension BilletTimelineViewController.Layout {
    enum Content {
        static let estimatedRowHeight: CGFloat = 64
        static let contentInset = UIEdgeInsets.zero
    }
    
    enum Length {
        static let headerHeight = CGFloat(106)
        static let footerHeight = CGFloat(106)
    }
}

final class BilletTimelineViewController: ViewController<BilletTimelineViewModelInputs, UIView> {
    fileprivate enum Layout { }
    
    private lazy var headerContainer = UIView()
    private lazy var header = BilletTimelineHeaderView()
    private lazy var footerContainer = UIView()
    private lazy var footer = BilletTimelineFooterView(delegate: self)
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(BilletTimelineCell.self, forCellReuseIdentifier: BilletTimelineCell.identifier)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = Layout.Content.estimatedRowHeight
        tableView.contentInset = Layout.Content.contentInset
        tableView.separatorStyle = .none
        headerContainer.addSubview(header)
        tableView.tableHeaderView = headerContainer
        footerContainer.addSubview(footer)
        tableView.tableFooterView = footerContainer
        return tableView
    }()
    
    private lazy var dataSource: TableViewDataSource<Int, BilletTimelineCellPresenting> = {
        let dataSource = TableViewDataSource<Int, BilletTimelineCellPresenting>(view: tableView)
        dataSource.itemProvider = { tableView, indexPath, presenter -> UITableViewCell? in
            let cellIdentifier = BilletTimelineCell.identifier
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? BilletTimelineCell
            cell?.setup(with: presenter, and: self)
            
            return cell
        }
        return dataSource
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.loadTimelineInfo()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        tableView.tableHeaderView?.frame.size = CGSize(width: view.frame.width, height: Layout.Length.headerHeight)
        tableView.tableFooterView?.frame.size = CGSize(width: view.frame.width, height: Layout.Length.footerHeight)
    }
 
    override func buildViewHierarchy() {
        view.addSubview(tableView)
    }
    
    override func configureViews() {
        title = Strings.tracking
        tableView.dataSource = dataSource
        tableView.backgroundColor = Colors.backgroundPrimary.color
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.constraintAllEdges(from: tableView, to: view)
    
        header.layout {
            $0.width == view.widthAnchor
            $0.height >= Layout.Length.headerHeight
        }
        
        footer.layout {
            $0.width == view.widthAnchor
            $0.height >= Layout.Length.footerHeight
        }
    }
}

// MARK: - Presenter Outputs

extension BilletTimelineViewController: BilletTimelineDisplay {
    func displayTimelineInfo(with title: String, statusTitle: String, and cells: [BilletTimelineCellPresenting]) {
        header.setup(with: title, and: statusTitle)
        dataSource.add(items: cells, to: 0)
    }
}

// MARK: - BilletTimelineCellDelegate

extension BilletTimelineViewController: BilletTimelineCellDelegate {
    func didTouchAction(_ action: TransactionAction) {
        viewModel.performAction(action)
    }
}

// MARK: - BilletTimelineFooterViewDelegate

extension BilletTimelineViewController: BilletTimelineFooterViewDelegate {
    func didTapHelpLink() {
        viewModel.openFooterHelpLink()
    }
}
