import UI
import UIKit

protocol BilletTimelineDisplay: AnyObject {
    func displayTimelineInfo(with title: String, statusTitle: String, and cells: [BilletTimelineCellPresenting])
}
