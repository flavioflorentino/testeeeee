import UIKit

enum BilletTimelineAction {
    case receipt(id: String)
    case helpCenter(url: String)
}

protocol BilletTimelineCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: BilletTimelineAction)
}

final class BilletTimelineCoordinator: BilletTimelineCoordinating {
    weak var viewController: UIViewController?
    
    private let receiptType = "pav"
    
    func perform(action: BilletTimelineAction) {
        switch action {
        case .receipt(let id):
            BilletSetup.shared.receipt?.open(withId: id, receiptType: receiptType)
            
        case .helpCenter(let urlString):
            guard let url = URL(string: urlString) else {
                return
            }
            BilletSetup.shared.deeplink?.open(url: url)
        }
    }
}
