import AnalyticsModule

protocol BilletTimelineViewModelInputs: AnyObject {
    func loadTimelineInfo()
    func performAction(_ action: TransactionAction)
    func openFooterHelpLink()
}

final class BilletTimelineViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let presenter: BilletTimelinePresenting
    private let billetInfo: BilletInfoDetail
    
    private let helpCenterURL = "http://app.picpay.com/helpcenter?query=ja-paguei-o-boleto"

    init(
        presenter: BilletTimelinePresenting,
        dependencies: Dependencies,
        billetInfo: BilletInfoDetail
    ) {
        self.presenter = presenter
        self.dependencies = dependencies
        self.billetInfo = billetInfo
    }
}

extension BilletTimelineViewModel: BilletTimelineViewModelInputs {
    func loadTimelineInfo() {
        var cells = [BilletTimelineCellPresenter]()
        
        for (index, status) in billetInfo.timeline.enumerated() {
            let nextStatus = billetInfo.timeline[safe: index + 1]
            let isNextStatusCompleted = nextStatus?.isCompleted ?? false

            let isFirstStatus = index == billetInfo.timeline.startIndex
            let isLastStatus = index == billetInfo.timeline.endIndex - 1
            
            let cell = BilletTimelineCellPresenter(
                title: status.title,
                message: status.message,
                isCurrentStatus: billetInfo.currentStatus == status.status,
                isCompleted: status.isCompleted,
                isNextStatusCompleted: isNextStatusCompleted,
                isFirstStatus: isFirstStatus,
                isLastStatus: isLastStatus,
                date: status.date,
                actions: status.actions
            )
            cells.append(cell)
        }
        
        dependencies.analytics.log(TransactionEvent.billetTimelineViewed(billetInfo.currentStatus))
        presenter.presentTimelineInfo(with: billetInfo.title, statusTitle: billetInfo.currentStatusTitle, and: cells)
    }
    
    func performAction(_ action: TransactionAction) {
        switch action.type {
        case .receipt:
            guard let id = action.id else {
                return
            }
            
            dependencies.analytics.log(TransactionEvent.billetTimelineActionSelected(.receipt))
            presenter.didNextStep(action: .receipt(id: id))
            
        case .helpCenter:
            guard let url = action.url else {
                return
            }
            
            dependencies.analytics.log(TransactionEvent.billetTimelineFaqAccessed(billetInfo.currentStatus))
            dependencies.analytics.log(TransactionEvent.billetTimelineActionSelected(.knowMore))
            presenter.didNextStep(action: .helpCenter(url: url))
            
        default:
            break
        }
    }
    
    func openFooterHelpLink() {
        dependencies.analytics.log(TransactionEvent.billetTimelineActionSelected(.understoodPayments))
        presenter.didNextStep(action: .helpCenter(url: helpCenterURL))
    }
}
