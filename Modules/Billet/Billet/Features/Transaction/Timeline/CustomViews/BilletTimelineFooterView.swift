import UI
import UIKit

protocol BilletTimelineFooterViewDelegate: AnyObject {
    func didTapHelpLink()
}

private extension BilletTimelineFooterView.Layout {
    enum Font {
        static let size00 = UIFont.systemFont(ofSize: 14, weight: .regular)
    }
    
    enum Length {
        static let separatorHeight = CGFloat(1)
    }
}

final class BilletTimelineFooterView: UIView {
    fileprivate enum Layout { }
    
    // MARK: Outlets
    
    private lazy var separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale700.color
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.stillInDoubt
        label.textAlignment = .center
        label.textColor = Colors.grayscale700.color
        label.font = Layout.Font.size00
        return label
    }()
    
    private(set) lazy var linkButton: UnderlinedButton = {
        let button = UnderlinedButton()
        button.setTitle(Strings.billetPaymentProcessHelpLink, for: .normal)
        button.updateColor(
            Colors.branding600.color,
            highlightedColor: Colors.branding300.color,
            font: Layout.Font.size00
        )
        button.titleLabel?.textAlignment = .center
        button.titleLabel?.numberOfLines = 0
        button.addTarget(self, action: #selector(linkButtonTapped), for: .touchUpInside)
        return button
    }()
    
    weak var delegate: BilletTimelineFooterViewDelegate?
    
    // MARK: - Initialization
    
    init(delegate: BilletTimelineFooterViewDelegate, frame: CGRect = .zero) {
        self.delegate = delegate
        
        super.init(frame: frame)
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Actions

private extension BilletTimelineFooterView {
    @objc
    func linkButtonTapped() {
        delegate?.didTapHelpLink()
    }
}

// MARK: - ViewConfiguration

extension BilletTimelineFooterView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(separatorView)
        addSubview(titleLabel)
        addSubview(linkButton)
    }
    
    func setupConstraints() {
        separatorView.layout {
            $0.height == Layout.Length.separatorHeight
            $0.top == topAnchor + Spacing.base02
            $0.leading == leadingAnchor + Spacing.base02
            $0.trailing == trailingAnchor - Spacing.base02
        }
        
        titleLabel.layout {
            $0.top == separatorView.topAnchor + Spacing.base02
            $0.leading == leadingAnchor + Spacing.base06
            $0.trailing == trailingAnchor - Spacing.base06
        }
        
        linkButton.layout {
            $0.top == titleLabel.bottomAnchor + Spacing.base00
            $0.leading == leadingAnchor + Spacing.base06
            $0.trailing == trailingAnchor - Spacing.base06
            $0.bottom == bottomAnchor - Spacing.base02
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
}
