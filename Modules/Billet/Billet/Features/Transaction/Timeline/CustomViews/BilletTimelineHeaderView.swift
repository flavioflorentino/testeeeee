import UI
import UIKit

private extension BilletTimelineHeaderView.Layout {
    enum Font {
        static let size00 = UIFont.systemFont(ofSize: 14, weight: .regular)
        static let size01 = UIFont.systemFont(ofSize: 16, weight: .bold)
    }
    
    enum Length {
        static let imageWidth = CGFloat(48)
        static let imageHeight = CGFloat(48)
        static let titleHeight = CGFloat(24)
        static let statusHeight = CGFloat(24)
        static let separatorHeight = CGFloat(1)
    }
}

final class BilletTimelineHeaderView: UIView {
    fileprivate enum Layout { }
    
    // MARK: Outlets
    
    private lazy var imageView = UIImageView(image: Assets.Icons.barcode.image)
    
    private(set) lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = Colors.grayscale700.color
        label.font = Layout.Font.size01
        return label
    }()
    
    private(set) lazy var statusLabel: UILabel = {
        let label = UILabel()
        label.textColor = Colors.grayscale300.color
        label.font = Layout.Font.size00
        return label
    }()
    
    private lazy var separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale700.color
        return view
    }()
    
    // MARK: - Initialization
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Setup

extension BilletTimelineHeaderView {
    func setup(with title: String, and status: String) {
        titleLabel.text = title
        statusLabel.text = status
    }
}

// MARK: - ViewConfiguration

extension BilletTimelineHeaderView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(imageView)
        addSubview(titleLabel)
        addSubview(statusLabel)
        addSubview(separatorView)
    }
    
    func setupConstraints() {
        imageView.layout {
            $0.width == Layout.Length.imageWidth
            $0.height == Layout.Length.imageHeight
            $0.top == topAnchor + Spacing.base03
            $0.leading == leadingAnchor + Spacing.base03
        }
        
        titleLabel.layout {
            $0.height == Layout.Length.titleHeight
            $0.top == topAnchor + Spacing.base03
            $0.leading == imageView.trailingAnchor + Spacing.base02
            $0.trailing == trailingAnchor - Spacing.base03
        }
        
        statusLabel.layout {
            $0.height == Layout.Length.statusHeight
            $0.top == titleLabel.bottomAnchor
            $0.leading == imageView.trailingAnchor + Spacing.base02
            $0.trailing == trailingAnchor - Spacing.base03
        }
        
        separatorView.layout {
            $0.height == Layout.Length.separatorHeight
            $0.leading == leadingAnchor + Spacing.base02
            $0.trailing == trailingAnchor - Spacing.base02
            $0.bottom == bottomAnchor - Spacing.base02
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
}
