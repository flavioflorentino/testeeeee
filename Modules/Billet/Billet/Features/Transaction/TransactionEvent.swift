import AnalyticsModule

enum TransactionEvent: AnalyticsKeyProtocol, Equatable {
    case billetFeedDetailViewed(FollowUpStatusType)
    case billetFeedDetailActionSelected(TransactionActionSelectedType)
    case billetTimelineViewed(FollowUpStatusType)
    case billetTimelineActionSelected(TransactionActionSelectedType)
    case billetTimelineFaqAccessed(FollowUpStatusType)
    case billetCancelPaymentModalViewed(TransactionActionSelectedType? = nil, FollowUpStatusType)
    case billetScheduledCancelPaymentModalViewed
    
    private var name: String {
        switch self {
        case .billetFeedDetailViewed:
            return "Status Transaction Viewed"
        case .billetFeedDetailActionSelected:
            return "Status Transaction Option Selected"
        case .billetTimelineViewed:
            return "Status Detail Viewed"
        case .billetTimelineActionSelected:
            return "Status Detail Option Selected"
        case .billetTimelineFaqAccessed:
            return "Status Detail Page Faq Accessed"
        case .billetCancelPaymentModalViewed:
            return "Status Modal Analysis"
        case .billetScheduledCancelPaymentModalViewed:
            return "Cancel Scheduled Payment Viewed"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .billetFeedDetailViewed(status):
            return ["status": status.rawValue]
        case let .billetFeedDetailActionSelected(action):
            return ["option_selected": action.rawValue]
        case let .billetTimelineViewed(status):
            return ["status": status.rawValue]
        case let .billetTimelineActionSelected(action):
            return ["option_selected": action.rawValue]
        case let .billetTimelineFaqAccessed(status):
            return ["status_origin": status.rawValue]
        case let .billetCancelPaymentModalViewed(action, status):
            guard case .cancelPayment = action, let action = action else {
                return [
                    "option_selected": "VOLTAR",
                    "status_origin": status.rawValue
                ]
            }
            return [
                "option_selected": action.rawValue,
                "status_origin": status.rawValue
            ]
        default:
            return [:]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .eventTracker]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("Bills - \(name)", properties: properties, providers: providers)
    }
}

enum TransactionActionSelectedType: String {
    case receipt = "VER RECIBO"
    case knowMore = "SAIBA MAIS"
    case understoodPayments = "ENTENDA SOBRE PAGAMENTOS"
    case followPayment = "ACOMPANHAR PAGAMENTO"
    case accelerateAnalysis = "ACELERAR ANÁLISE"
    case cancelPayment = "CANCELAR PAGAMENTO"
}
