import UI

protocol BilletSelectionPresenting: AnyObject {
    var viewController: BilletSelectionDisplayable? { get set }

    func handleGetBillets(withTitle title: String, description: String?, and presenters: [BilletSelectionCellPresenting])
    func handleNextStep(action: BilletSelectionAction)
}

final class BilletSelectionPresenter: BilletSelectionPresenting {
    private let coordinator: BilletSelectionCoordinating
    weak var viewController: BilletSelectionDisplayable?

    init(coordinator: BilletSelectionCoordinating) {
        self.coordinator = coordinator
    }
   
    func handleGetBillets(withTitle title: String, description: String?, and presenters: [BilletSelectionCellPresenting]) {
        let section = description ?? Strings.BilletSelection.selectBilletToPay
        viewController?.presentViewElements(withTitle: title, section: section, and: presenters)
    }
    
    func handleNextStep(action: BilletSelectionAction) {
        coordinator.perform(action: action)
    }
}
