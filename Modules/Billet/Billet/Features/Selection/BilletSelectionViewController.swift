import SnapKit
import UI
import UIKit

protocol BilletSelectionDisplayable: AnyObject {
    func presentViewElements(withTitle title: String, section: String, and cells: [BilletSelectionCellPresenting])
}

private extension BilletSelectionViewController.Layout {
    enum Content {
        static let estimatedRowHeight: CGFloat = 64
        static let contentInset = UIEdgeInsets.zero
    }
}

final class BilletSelectionViewController: ViewController<BilletSelectionInteracting, UIView> {
    fileprivate struct Layout { }
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(BilletSelectionCell.self, forCellReuseIdentifier: BilletSelectionCell.identifier)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = Layout.Content.estimatedRowHeight
        tableView.contentInset = Layout.Content.contentInset
        tableView.separatorStyle = .none
        return tableView
    }()
    
    private lazy var dataSource: TableViewDataSource<String, BilletSelectionCellPresenting> = {
        let dataSource = TableViewDataSource<String, BilletSelectionCellPresenting>(view: tableView)
        dataSource.itemProvider = { tableView, indexPath, presenter -> UITableViewCell? in
            let cellIdentifier = BilletSelectionCell.identifier
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? BilletSelectionCell
            cell?.configure(with: presenter)
            
            return cell
        }
        
        return dataSource
    }()

    private lazy var closeButton = UIBarButtonItem(
        title: Strings.Common.close,
        style: .plain,
        target: self,
        action: #selector(didClickClose)
    )
    
    private lazy var helpCenterButton: UIBarButtonItem = {
        UIBarButtonItem(
            image: Assets.Icons.help.image,
            style: .plain,
            target: self,
            action: #selector(didClickHelp)
        )
    }()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.setLeftBarButton(closeButton, animated: true)
        navigationItem.setRightBarButton(helpCenterButton, animated: true)
       
        interactor.getBillets()
    }
    
    // MARK: - ViewCode
    
    override func buildViewHierarchy() {
        view.addSubview(tableView)
    }
    
    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    override func configureViews() {
        navigationController?.navigationBar.tintColor = Colors.white.color
        navigationController?.navigationBar.barTintColor = Colors.branding400.color
        navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: Colors.white.color
        ]
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        
        tableView.dataSource = dataSource
        tableView.delegate = self
    }
    
    @objc
    private func didClickClose() {
        interactor.close()
    }
    
    @objc
    private func didClickHelp() {
        interactor.help()
    }
}

// MARK: - Presenter Outputs

extension BilletSelectionViewController: BilletSelectionDisplayable {
    func presentViewElements(withTitle title: String, section: String, and cells: [BilletSelectionCellPresenting]) {
        self.title = title
        dataSource.add(items: cells, to: section)
    }
}

// MARK: - TableViewDelegate

extension BilletSelectionViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        interactor.selectedItem(at: indexPath.row)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
