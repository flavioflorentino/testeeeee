import SnapKit
import UI
import UIKit

private extension BilletSelectionCell.Layout {
    enum Size {
        static let separatorHeight: CGFloat = 1
    }
}

final class BilletSelectionCell: UITableViewCell {
    fileprivate struct Layout { }
    
    static let identifier = String(describing: BilletSelectionCell.self)
    
    private(set) lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textAlignment, .left)
        return label
    }()
    
    private(set) lazy var valueDescriptionLabel: UILabel = {
        let label = UILabel()
        label.text = "\(Strings.Common.value):"
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale400())
            .with(\.textAlignment, .left)
        return label
    }()
    
    private(set) lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textAlignment, .right)
        return label
    }()
    
    private(set) lazy var dueDateDescriptionLabel: UILabel = {
        let label = UILabel()
        label.text = "\(Strings.Common.dueDate):"
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale400())
            .with(\.textAlignment, .left)
        return label
    }()
    
    private(set) lazy var dueDateLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textAlignment, .right)
        return label
    }()
    
    private(set) lazy var linecodeLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
            .with(\.textColor, .grayscale400())
            .with(\.textAlignment, .left)
        return label
    }()
    
    private(set) lazy var separator: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale200.color
        return view
    }()
    
    func configure(with presenter: BilletSelectionCellPresenting) {
        descriptionLabel.text = presenter.description
        valueLabel.text = presenter.amount
        dueDateLabel.text = presenter.dueDate
        linecodeLabel.text = presenter.linecode
        
        buildLayout()
    }
}

extension BilletSelectionCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(valueDescriptionLabel)
        contentView.addSubview(valueLabel)
        contentView.addSubview(dueDateDescriptionLabel)
        contentView.addSubview(dueDateLabel)
        contentView.addSubview(linecodeLabel)
        contentView.addSubview(separator)
    }
    
    func setupConstraints() {
        descriptionLabel.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        valueDescriptionLabel.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base02)
        }
        
        valueLabel.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.greaterThanOrEqualTo(valueDescriptionLabel.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        dueDateDescriptionLabel.snp.makeConstraints {
            $0.top.equalTo(valueDescriptionLabel.snp.bottom).offset(Spacing.base00)
            $0.leading.equalToSuperview().offset(Spacing.base02)
        }
        
        dueDateLabel.snp.makeConstraints {
            $0.top.equalTo(valueLabel.snp.bottom).offset(Spacing.base00)
            $0.leading.greaterThanOrEqualTo(dueDateDescriptionLabel.snp.trailing).offset(Spacing.base01)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        linecodeLabel.snp.makeConstraints {
            $0.top.equalTo(dueDateDescriptionLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        separator.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.separatorHeight)
            $0.top.equalTo(linecodeLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview()
        }
    }
}
