import Core
import Foundation
import UI

protocol BilletSelectionCellPresenting {
    var description: String { get }
    var linecode: String? { get }
    var amount: String? { get }
    var dueDate: String? { get }
}

struct BilletSelectionCellPresenter: BilletSelectionCellPresenting {
    var description: String
    var linecode: String?
    var amount: String?
    var dueDate: String?
    
    init(isDealership: Bool, description: String, linecode: String, amount: Double, dueDate: String) {
        self.description = description
        
        let maskDescriptor: BilletMaskDescriptor = isDealership ? .dealership : .other
        let mask = CustomStringMask(descriptor: maskDescriptor)
        self.linecode = mask.maskedText(from: linecode)
        
        self.amount = amount.toCurrencyString()
        
        let date = Date.fifthFormatter.date(from: dueDate) ?? Date()
        self.dueDate = Date.forthFormatter.string(from: date)
    }
}
