public struct BilletSelectionModel: Decodable {
    enum CodingKeys: String, CodingKey {
        case title
        case description
        case billets = "payments"
    }
    
    let title: String
    let description: String?
    public let billets: [BilletSelectionListModel]
    
    public init(title: String, description: String?, billets: [BilletSelectionListModel]) {
        self.title = title
        self.description = description
        self.billets = billets
    }
    
    public var hasOnlyOneValue: Bool {
        billets.count == 1
    }
    
    public var isNotEmpty: Bool {
        !billets.isEmpty
    }
}
