public struct BilletSelectionListModel: Decodable {
    enum CodingKeys: String, CodingKey {
        case linecode = "barcode"
        case description
        case dueDate = "due_date"
        case amount
    }
    
    public let linecode: String
    let description: String
    let dueDate: String
    let amount: Double
    
    public init(linecode: String, description: String, dueDate: String, amount: Double) {
        self.linecode = linecode
        self.description = description
        self.dueDate = dueDate
        self.amount = amount
    }
}
