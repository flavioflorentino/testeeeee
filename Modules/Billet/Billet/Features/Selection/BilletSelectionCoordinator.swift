import Foundation
import UIKit

enum BilletSelectionAction: Equatable {
    case makePayment(linecode: String, placeholder: String)
    case helpCenter(url: String)
    case close
}

protocol BilletSelectionCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: BilletSelectionAction)
}

final class BilletSelectionCoordinator: BilletSelectionCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: BilletSelectionAction) {
        switch action {
        case let .makePayment(linecode, placeholder):
            guard let viewController = viewController else { return }
            
            let formController = BilletFormFactory.make(with: .qrCodeIptu, linecode: linecode, description: placeholder)
            viewController.navigationController?.pushViewController(formController, animated: true)
            
        case .helpCenter(let urlString):
            guard let url = URL(string: urlString) else { return }
            BilletSetup.shared.deeplink?.open(url: url)
            
        case .close:
            viewController?.dismiss(animated: true)
        }
    }
}
