import FeatureFlag

protocol BilletSelectionInteracting: AnyObject {
    func getBillets()
    func selectedItem(at index: Int)
    func close()
    func help()
}

final class BilletSelectionInteractor {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    private let presenter: BilletSelectionPresenting
    private let billetSelection: BilletSelectionModel
    
    init(dependencies: Dependencies, presenter: BilletSelectionPresenting, billetSelection: BilletSelectionModel) {
        self.dependencies = dependencies
        self.presenter = presenter
        self.billetSelection = billetSelection
    }
}

extension BilletSelectionInteractor: BilletSelectionInteracting {
    func getBillets() {
        let presenters = billetSelection.billets.map {
            BilletSelectionCellPresenter(
                isDealership: isDealership(linecode: $0.linecode),
                description: $0.description,
                linecode: $0.linecode,
                amount: $0.amount,
                dueDate: $0.dueDate
            )
        }
        
        presenter.handleGetBillets(withTitle: billetSelection.title,
                                   description: billetSelection.description,
                                   and: presenters)
    }
    
    func selectedItem(at index: Int) {
        guard let billet = billetSelection.billets[safe: index] else { return }
        
        let placeholder = "\(billetSelection.title) - \(billet.description)"
        let action: BilletSelectionAction = .makePayment(linecode: billet.linecode, placeholder: placeholder)
        presenter.handleNextStep(action: action)
    }
    
    func close() {
        presenter.handleNextStep(action: .close)
    }
    
    func help() {
        let urlString = dependencies.featureManager.text(.opsBilletSelectionHelpUrl)
        presenter.handleNextStep(action: .helpCenter(url: urlString))
    }
    
    private func isDealership(linecode: String) -> Bool {
        linecode.first == "8"
    }
}
