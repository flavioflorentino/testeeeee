import UIKit

public enum BilletSelectionFactory {
    public static func make(model: BilletSelectionModel) -> UIViewController {
        let dependencies = DependencyContainer()
        let coordinator: BilletSelectionCoordinating = BilletSelectionCoordinator()
        let presenter: BilletSelectionPresenting = BilletSelectionPresenter(coordinator: coordinator)
        let interactor = BilletSelectionInteractor(dependencies: dependencies, presenter: presenter, billetSelection: model)
        let viewController = BilletSelectionViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
