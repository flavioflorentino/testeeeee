import Core

enum FollowUpEndpoint {
    case followUpStatus(billId: String)
    case cancelPayment(billId: String)
    case cancelScheduledPayment(billId: String)
}

extension FollowUpEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .followUpStatus(let billId):
            return "/bills/transaction/\(billId)/followUp"
        case .cancelPayment(let billId):
            return "bills/\(billId)/cancelation/"
        case .cancelScheduledPayment(let billId):
            return "bills/scheduled/\(billId)/cancelation"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .followUpStatus:
            return .get
        case .cancelPayment, .cancelScheduledPayment:
            return .post
        }
    }
}
