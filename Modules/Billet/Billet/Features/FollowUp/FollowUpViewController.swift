import SnapKit
import UI
import UIKit

protocol FollowUpDisplaying: AnyObject {
    func displayLoadingState()
    func displayCancelLoading()
    func hideCancelLoading()
    func displayHeader(with viewModel: FollowUpHeaderViewModeling)
    func displaySuccessState(with viewModels: [FollowUpTimelineCellViewModeling])
    func displaySuccessState(with viewModel: FollowUpMessageViewModeling)
    func displayErrorState(with viewModel: StatefulFeedbackViewModel)
    func display(alert: BilletErrorAlert)
}

private extension FollowUpViewController.Layout {
    enum TableView {
        static let rowHeight = UITableView.automaticDimension
        static let estimatedRowHeight: CGFloat = 64
        static let contentInset = UIEdgeInsets.zero
    }
}

final class FollowUpViewController: ViewController<FollowUpInteracting, UIView> {
    fileprivate enum Layout { }
    
    private typealias Localizable = Strings.FollowUp
    
    // MARK: - Views
    
    private lazy var headerView = FollowUpHeaderView()
    
    private lazy var footerContainerView = UIView()
    private lazy var footerView = FollowUpTimelineFooterView(delegate: self)
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(cellType: FollowUpTimelineCell.self)
        tableView.rowHeight = Layout.TableView.rowHeight
        tableView.estimatedRowHeight = Layout.TableView.estimatedRowHeight
        tableView.contentInset = Layout.TableView.contentInset
        tableView.separatorStyle = .none
        footerContainerView.addSubview(footerView)
        tableView.tableFooterView = footerContainerView
        tableView.isHidden = true
        return tableView
    }()
    
    private lazy var dataSource: TableViewDataSource<Int, FollowUpTimelineCellViewModeling> = {
        let dataSource = TableViewDataSource<Int, FollowUpTimelineCellViewModeling>(view: tableView)
        dataSource.itemProvider = { tableView, indexPath, viewModel -> UITableViewCell? in
            let cell = tableView.dequeueReusableCell(for: indexPath, cellType: FollowUpTimelineCell.self)
            cell.configure(with: viewModel, and: self)
            return cell
        }
        return dataSource
    }()
    
    private lazy var messageView: FollowUpMessageView = {
        let view = FollowUpMessageView(delegate: self)
        view.isHidden = true
        return view
    }()
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.fetchFollowUpStatus()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        tableView.updateTableFooterView(with: footerView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    // MARK: - ViewConfiguration

    override func buildViewHierarchy() {
        view.addSubviews(headerView, tableView, messageView)
    }
    
    override func setupConstraints() {
        headerView.snp.makeConstraints {
            $0.top.equalTo(self.view.compatibleSafeArea.top)
            $0.leading.trailing.equalToSuperview()
        }
        
        tableView.snp.makeConstraints {
            $0.top.equalTo(headerView.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
        
        footerView.snp.makeConstraints {
            $0.width.equalTo(tableView)
        }
        
        messageView.snp.makeConstraints {
            $0.top.equalTo(headerView.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }

    override func configureViews() {
        title = Localizable.title
        
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        
        tableView.dataSource = dataSource
    }
}

// MARK: - FollowUpDisplaying

extension FollowUpViewController: FollowUpDisplaying {
    func displayLoadingState() {
        beginState()
    }
    
    func displayCancelLoading() {
        messageView.showSecondaryButtonLoading()
    }
    
    func hideCancelLoading() {
        messageView.hideSecondaryButtonLoading()
    }
    
    func displayHeader(with viewModel: FollowUpHeaderViewModeling) {
        headerView.configure(with: viewModel)
    }
    
    func displaySuccessState(with viewModels: [FollowUpTimelineCellViewModeling]) {
        dataSource.add(items: viewModels, to: 0)
        tableView.isHidden = false
        endState()
    }
    
    func displaySuccessState(with viewModel: FollowUpMessageViewModeling) {
        messageView.configure(with: viewModel)
        messageView.isHidden = false
        endState()
    }
    
    func displayErrorState(with viewModel: StatefulFeedbackViewModel) {
        endState(model: viewModel)
    }
    
    func display(alert: BilletErrorAlert) {
        let actions = alert.actions.map {
            ApolloAlertAction(title: $0.title, dismissOnCompletion: true, completion: $0.action)
        }
    
        showApolloAlert(image: alert.icon,
                        title: alert.title,
                        subtitle: alert.description,
                        primaryButtonAction: actions.first,
                        linkButtonAction: actions.dropFirst().first,
                        dismissOnTouchOutside: true)
    }
}

// MARK: - FollowUpTimelineCellDelegate

extension FollowUpViewController: FollowUpTimelineCellDelegate, FollowUpMessageViewDelegate {
    func actionTapped(_ action: FollowUpStatusAction) {
        interactor.handle(action: action)
    }
}

// MARK: - FollowUpTimelineFooterViewDelegate

extension FollowUpViewController: FollowUpTimelineFooterViewDelegate {
    func helpLinkTapped() {
        interactor.openFaq()
    }
}

// MARK: - StatefulTransitionViewing

extension FollowUpViewController: StatefulTransitionViewing {
    func didTryAgain() {
        interactor.fetchFollowUpStatus()
    }
    
    func didTapSecondaryButton() {
        interactor.close()
    }
}
