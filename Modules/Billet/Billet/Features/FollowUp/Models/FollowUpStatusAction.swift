import Core

enum FollowUpStatusActionType: String, Decodable {
    case receipt = "pav_receipt"
    case helpCenter = "faq_more"
    case accelerateAnalysis = "accelerate_analysis"
    case cancel = "payment_cancel"
    case scheduledReceipt = "pav_receipt_scheduled"
    case scheduledCancel = "scheduled_cancel"
    case generic
}

struct FollowUpStatusAction: Decodable {
    let title: String
    let message: String?
    let type: FollowUpStatusActionType
    let id: String?
    let url: String?
    
    private enum CodingKeys: String, CodingKey {
        case title = "label"
        case message
        case type
        case data
        case id
        case url
        case value
        case action
    }
    
    init(title: String, message: String?, type: FollowUpStatusActionType, id: String?, url: String?) {
        self.title = title
        self.message = message
        self.type = type
        self.id = id
        self.url = url
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let data = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        
        title = try container.decode(String.self, forKey: .title)
        message = try data.decodeIfPresent(String.self, forKey: .message)
        type = try container.decode(FollowUpStatusActionType.self, forKey: .type)
        id = try data.decodeIfPresent(String.self, forKey: .id)
        url = try data.decodeIfPresent(String.self, forKey: .url)
    }
}
