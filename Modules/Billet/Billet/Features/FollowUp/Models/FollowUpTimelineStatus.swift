public struct FollowUpTimelineStatus: Decodable {
    let status: FollowUpStatusType
    let title: String
    let label: String
    let message: String
    let date: String?
    let isCompleted: Bool
    let actions: [FollowUpStatusAction]
    
    private enum CodingKeys: String, CodingKey {
        case status = "status_value"
        case title = "status_title"
        case label = "status_label"
        case message = "status_description"
        case date = "status_date"
        case isCompleted = "is_completed"
        case actions
    }
    
    init(
        status: FollowUpStatusType,
        title: String,
        label: String,
        message: String,
        date: String?,
        isCompleted: Bool,
        actions: [FollowUpStatusAction] = []
    ) {
        self.status = status
        self.title = title
        self.label = label
        self.message = message
        self.date = date
        self.isCompleted = isCompleted
        self.actions = actions
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        status = try container.decode(FollowUpStatusType.self, forKey: .status)
        title = try container.decode(String.self, forKey: .title)
        label = try container.decodeIfPresent(String.self, forKey: .label) ?? ""
        message = try container.decode(String.self, forKey: .message)
        date = try container.decodeIfPresent(String.self, forKey: .date)
        isCompleted = try container.decode(Bool.self, forKey: .isCompleted)
        actions = try container.decodeIfPresent([FollowUpStatusAction].self, forKey: .actions) ?? []
    }
}
