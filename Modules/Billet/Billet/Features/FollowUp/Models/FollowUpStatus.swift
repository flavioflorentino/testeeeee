public struct FollowUpStatus: Decodable {
    let title: String
    let currentStatusTitle: String
    let currentStatusType: FollowUpStatusType
    let timeline: [FollowUpTimelineStatus]

    private enum CodingKeys: String, CodingKey {
        case header
        case title
        case currentStatusTitle = "status_title"
        case currentStatusType = "status_value"
        case timeline
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let header = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .header)
        
        title = try header.decode(String.self, forKey: .title)
        currentStatusTitle = try header.decode(String.self, forKey: .currentStatusTitle)
        currentStatusType = try header.decode(FollowUpStatusType.self, forKey: .currentStatusType)
        
        timeline = try container.decode([FollowUpTimelineStatus].self, forKey: .timeline)
    }
    
    init(
        title: String,
        currentStatusTitle: String,
        currentStatusType: FollowUpStatusType,
        timeline: [FollowUpTimelineStatus]
    ) {
        self.title = title
        self.currentStatusTitle = currentStatusTitle
        self.currentStatusType = currentStatusType
        self.timeline = timeline
    }
}
