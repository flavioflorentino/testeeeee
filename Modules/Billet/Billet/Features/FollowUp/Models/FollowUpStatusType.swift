public enum FollowUpStatusType: String, Decodable {
    case refused
    case processing
    case inAnalysis = "in_analysis"
    case accelerateAnalysis = "accelerate_analysis"
    case pending
    case completed
    case scheduled
    case scheduledCancelled = "scheduled_cancel"
    case scheduledRefused = "scheduled_refused"
    case canceled
}
