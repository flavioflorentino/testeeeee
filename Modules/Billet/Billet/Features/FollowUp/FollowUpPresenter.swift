import AssetsKit
import UI

protocol FollowUpPresenting: AnyObject {
    var viewController: FollowUpDisplaying? { get set }
    
    func presentLoading()
    func presentCancelLoading()
    func hideCancelLoading()
    func presentHeader(with followUpStatus: FollowUpStatus)
    func presentMessage(with followUpStatus: FollowUpStatus)
    func presentTimeline(with followUpStatus: FollowUpStatus)
    func presentError()
    func present(alert: BilletErrorAlert)
    func present(action: FollowUpAction)
}

final class FollowUpPresenter {
    private let coordinator: FollowUpCoordinating
    weak var viewController: FollowUpDisplaying?

    init(coordinator: FollowUpCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - FollowUpPresenting

extension FollowUpPresenter: FollowUpPresenting {
    func presentLoading() {
        viewController?.displayLoadingState()
    }
    
    func presentCancelLoading() {
        viewController?.displayCancelLoading()
    }
    
    func hideCancelLoading() {
        viewController?.hideCancelLoading()
    }
    
    func presentHeader(with followUpStatus: FollowUpStatus) {
        let viewModel = formatHeader(with: followUpStatus)
        viewController?.displayHeader(with: viewModel)
    }
   
    func presentMessage(with followUpStatus: FollowUpStatus) {
        let viewModel = formatMessage(with: followUpStatus)
        viewController?.displaySuccessState(with: viewModel)
    }
    
    func presentTimeline(with followUpStatus: FollowUpStatus) {
        let viewModels = formatTimeline(with: followUpStatus)
        viewController?.displaySuccessState(with: viewModels)
    }
    
    func presentError() {
        let viewModel = StatefulFeedbackViewModel(
            image: Resources.Illustrations.iluFalling.image,
            content: (
                title: Strings.FollowUp.Error.title,
                description: Strings.FollowUp.Error.message
            ),
            button: (
                image: Resources.Icons.icoRefresh.image,
                title: Strings.Common.tryAgain
            )
        )
        
        viewController?.displayErrorState(with: viewModel)
    }
    
    func present(alert: BilletErrorAlert) {
        viewController?.display(alert: alert)
    }
    
    func present(action: FollowUpAction) {
        coordinator.perform(action: action)
    }
}

// MARK: - Formatters

private extension FollowUpPresenter {
    func formatHeader(with followUpStatus: FollowUpStatus) -> FollowUpHeaderViewModeling {
        let statusIcon: FollowUpHeaderViewModel.IconType = {
            switch followUpStatus.currentStatusType {
            case .inAnalysis, .accelerateAnalysis, .scheduled:
                return .warning
            case .refused, .canceled, .scheduledRefused, .scheduledCancelled:
                return .danger
            default:
                return .none
            }
        }()
        
        return FollowUpHeaderViewModel(title: followUpStatus.title,
                                       status: followUpStatus.currentStatusTitle,
                                       statusIcon: statusIcon)
    }

    func formatMessage(with followUpStatus: FollowUpStatus) -> FollowUpMessageViewModeling {
        let messageStatus = followUpStatus.timeline.first { $0.status == followUpStatus.currentStatusType }
        let actions = messageStatus?.actions ?? []
        return FollowUpMessageViewModel(date: messageStatus?.date,
                                        message: messageStatus?.message,
                                        actions: actions)
    }
    
    func formatTimeline(with followUpStatus: FollowUpStatus) -> [FollowUpTimelineCellViewModeling] {
        var viewModels = [FollowUpTimelineCellViewModeling]()
        
        for (index, status) in followUpStatus.timeline.enumerated() {
            let nextStatus = followUpStatus.timeline[safe: index + 1]
            let isNextStatusCompleted = nextStatus?.isCompleted ?? false

            let isFirstStatus = index == followUpStatus.timeline.startIndex
            let isLastStatus = index == followUpStatus.timeline.endIndex - 1
            
            let viewModel = FollowUpTimelineCellViewModel(
                date: status.date,
                title: status.title,
                message: status.message,
                actions: status.actions,
                isCurrentStatus: followUpStatus.currentStatusType == status.status,
                isCompleted: status.isCompleted,
                isNextStatusCompleted: isNextStatusCompleted,
                isFirstStatus: isFirstStatus,
                isLastStatus: isLastStatus
            )
            viewModels.append(viewModel)
        }
        
        return viewModels
    }
}
