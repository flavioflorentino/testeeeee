import UIKit

public enum FollowUpFactory {
    public static func make(withBillId billId: String) -> UIViewController {
        let container = DependencyContainer()
        let service: FollowUpServicing = FollowUpService(dependencies: container)
        let coordinator: FollowUpCoordinating = FollowUpCoordinator(dependencies: container)
        let presenter: FollowUpPresenting = FollowUpPresenter(coordinator: coordinator)
        let interactor = FollowUpInteractor(service: service, presenter: presenter, billId: billId, dependencies: container)
        let viewController = FollowUpViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
