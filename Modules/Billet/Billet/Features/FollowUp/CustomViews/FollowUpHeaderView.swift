import AssetsKit
import SnapKit
import UI
import UIKit

final class FollowUpHeaderView: UIView {
    // MARK: Views
    
    private lazy var iconImageView: UIImageView = {
        let imageview = UIImageView(image: Resources.Icons.icoBill.image)
        imageview.imageStyle(RoundedImageStyle(size: .small, cornerRadius: .none))
        return imageview
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
        return label
    }()
    
    private lazy var statusStackView: UIStackView = {
        let view = UIStackView()
        view.spacing = Spacing.base01
        return view
    }()
    
    private lazy var iconStatusLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(IconLabelStyle(type: .small))
            .with(\.isHidden, true)
        return label
    }()
    
    private lazy var statusLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale600())
        return label
    }()
    
    private lazy var separatorView = SeparatorView(style: BackgroundViewStyle(color: .grayscale100()))
    
    // MARK: - Initialization
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Setup

    func configure(with viewModel: FollowUpHeaderViewModeling) {
        titleLabel.text = viewModel.title
        statusLabel.text = viewModel.status
        iconStatusLabel.text = viewModel.icon
        iconStatusLabel.textColor = viewModel.iconColor
        iconStatusLabel.isHidden = viewModel.icon?.isEmpty ?? true
    }
}

// MARK: - ViewConfiguration

extension FollowUpHeaderView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubviews(iconImageView, titleLabel, statusStackView, separatorView)
        statusStackView.addArrangedSubviews(iconStatusLabel, statusLabel)
    }
    
    func setupConstraints() {
        iconImageView.snp.makeConstraints {
            $0.top.leading.equalToSuperview().offset(Spacing.base03)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.trailing.equalToSuperview().inset(Spacing.base03)
            $0.leading.equalTo(iconImageView.snp.trailing).offset(Spacing.base02)
        }
        
        statusStackView.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom)
            $0.leading.equalTo(iconImageView.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base03)
        }
        
        iconStatusLabel.snp.makeConstraints {
            $0.size.equalTo(Sizing.base02)
        }
        
        separatorView.snp.makeConstraints {
            $0.top.equalTo(iconImageView.snp.bottom).offset(Spacing.base03)
            $0.top.equalTo(statusStackView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview()
        }
    }
}
