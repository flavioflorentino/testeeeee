import Foundation
import SnapKit
import UI
import UIKit

protocol FollowUpMessageViewDelegate: AnyObject {
    func actionTapped(_ action: FollowUpStatusAction)
}

final class FollowUpMessageView: UIView {
    // MARK: - Views
    
    private lazy var dateLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale500())
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = .zero
        return label
    }()
    
    private lazy var buttonsContainer: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.spacing = Spacing.base01
        return view
    }()
    
    private lazy var primaryButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(primaryButtonTapped), for: .touchUpInside)
        button.buttonStyle(PrimaryButtonStyle(size: .small))
            .with(\.isHidden, true)
        return button
    }()
    
    private lazy var secondaryButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(secondaryButtonTapped), for: .touchUpInside)
        button.buttonStyle(DangerButtonStyle(size: .small, icon: (name: .infoCircle, alignment: .left)))
            .with(\.isHidden, true)
        return button
    }()
    
    private lazy var linkButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(linkButtonTapped), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    
    private lazy var spacingView = UIView()
    
    // MARK: - Properties
    
    private weak var delegate: FollowUpMessageViewDelegate?
    
    private var primaryAction: FollowUpStatusAction?
    private var secondaryAction: FollowUpStatusAction?
    private var linkAction: FollowUpStatusAction?
    
    // MARK: - Initialization
    
    init(delegate: FollowUpMessageViewDelegate) {
        self.delegate = delegate
        
        super.init(frame: .zero)
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Setup

extension FollowUpMessageView {
    func configure(with viewModel: FollowUpMessageViewModeling) {
        dateLabel.text = viewModel.date
        descriptionLabel.attributedText = viewModel.message
        
        viewModel.actions.forEach { action in
            setupButton(with: action)
        }
    }
    
    func showSecondaryButtonLoading() {
        secondaryButton.startLoading()
    }
    
    func hideSecondaryButtonLoading() {
        secondaryButton.stopLoading()
    }
    
    private func setupButton(with action: FollowUpStatusAction) {
        switch action.type {
        case .scheduledReceipt, .accelerateAnalysis:
            primaryAction = action
            primaryButton.setTitle(action.title, for: .normal)
            primaryButton.isHidden = false
            
        case .cancel, .scheduledCancel:
            secondaryAction = action
            secondaryButton.setTitle(action.title, for: .normal)
            secondaryButton.isHidden = false
            
        case .helpCenter:
            linkAction = action
            linkButton.setTitle(action.title, for: .normal)
            linkButton.isHidden = false
            linkButton.buttonStyle(LinkButtonStyle(size: .small))
            
        default:
            break
        }
    }
}

// MARK: - Actions

@objc
private extension FollowUpMessageView {
    func primaryButtonTapped() {
        guard let action = primaryAction else { return }
        delegate?.actionTapped(action)
    }
    
    func secondaryButtonTapped() {
        guard let action = secondaryAction else { return }
        delegate?.actionTapped(action)
    }
    
    func linkButtonTapped() {
        guard let action = linkAction else { return }
        delegate?.actionTapped(action)
    }
}

// MARK: - ViewConfiguration

extension FollowUpMessageView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubviews(dateLabel, descriptionLabel, buttonsContainer, spacingView)
        buttonsContainer.addArrangedSubviews(primaryButton, secondaryButton, linkButton)
    }
    
    func setupConstraints() {
        dateLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(dateLabel.snp.bottom).offset(Spacing.base00)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        buttonsContainer.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        spacingView.snp.makeConstraints {
            $0.top.equalTo(buttonsContainer.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
}
