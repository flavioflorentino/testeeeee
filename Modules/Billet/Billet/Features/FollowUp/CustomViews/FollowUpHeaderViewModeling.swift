import Foundation
import UI
import UIKit

protocol FollowUpHeaderViewModeling {
    var title: String { get }
    var status: String { get }
    var icon: String? { get }
    var iconColor: UIColor? { get }
}

struct FollowUpHeaderViewModel: FollowUpHeaderViewModeling {
    enum IconType {
        case warning, danger, none
    }
    
    let title: String
    let status: String
    let icon: String?
    let iconColor: UIColor?
    
    init(title: String, status: String, statusIcon: IconType) {
        self.title = title
        self.status = status
        self.icon = {
            switch statusIcon {
            case .warning:
                return Iconography.clockThree.rawValue
            case .danger:
                return Iconography.timesCircle.rawValue
            case .none:
                return nil
            }
        }()
        
        self.iconColor = {
            switch statusIcon {
            case .warning:
                return Colors.warning600.color
            case .danger:
                return Colors.critical600.color
            case .none:
                return nil
            }
        }()
    }
}
