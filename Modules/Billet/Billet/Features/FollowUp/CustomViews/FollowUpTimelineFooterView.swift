import SnapKit
import UI
import UIKit

protocol FollowUpTimelineFooterViewDelegate: AnyObject {
    func helpLinkTapped()
}

final class FollowUpTimelineFooterView: UIView {
    private typealias Localizable = Strings.FollowUp
    
    // MARK: Views
    
    private lazy var separatorView = SeparatorView(style: BackgroundViewStyle(color: .grayscale100()))
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.text, Localizable.Timeline.footerTitle)
            .with(\.textColor, .grayscale600())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var linkButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.Timeline.footerLink, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(linkButtonTapped), for: .touchUpInside)
        return button
    }()
    
    weak var delegate: FollowUpTimelineFooterViewDelegate?
    
    // MARK: - Initialization
    
    init(delegate: FollowUpTimelineFooterViewDelegate, frame: CGRect = .zero) {
        self.delegate = delegate
        
        super.init(frame: frame)
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Actions
    
    @objc
    func linkButtonTapped() {
        delegate?.helpLinkTapped()
    }
}

// MARK: - ViewConfiguration

extension FollowUpTimelineFooterView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubviews(separatorView, titleLabel, linkButton)
    }
    
    func setupConstraints() {
        separatorView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(separatorView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base06)
        }
        
        linkButton.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base06)
            $0.bottom.equalToSuperview().offset(-Spacing.base03)
        }
    }
}
