import Foundation
import UI

protocol FollowUpMessageViewModeling {
    var date: String { get }
    var message: NSAttributedString? { get }
    var actions: [FollowUpStatusAction] { get }
}

struct FollowUpMessageViewModel: FollowUpMessageViewModeling {
    let date: String
    let message: NSAttributedString?
    let actions: [FollowUpStatusAction]
    
    init(date: String?, message: String?, actions: [FollowUpStatusAction]) {
        self.date = date ?? ""
        self.message = FollowUpMessageViewModel.format(message: message)
        self.actions = actions
    }
    
    private static func format(message: String?) -> NSAttributedString? {
        let formattedMessage = message?
            .replacingOccurrences(of: "<br/>", with: "\n")
            .replacingOccurrences(of: "<br />", with: "\n")
            .replacingOccurrences(of: "<b>", with: "[b]")
            .replacingOccurrences(of: "</b>", with: "[b]")
        
        return formattedMessage?.attributedStringWith(
            normalFont: Typography.bodyPrimary().font(),
            highlightFont: Typography.bodyPrimary(.highlight).font(),
            normalColor: Colors.grayscale700.color,
            highlightColor: Colors.grayscale700.color,
            textAlignment: .left,
            underline: false,
            separator: "[b]"
        )
    }
}
