import Core
import Foundation

typealias FetchFollowUpStatusCompletionBlock = (Result<FollowUpStatus, ApiError>) -> Void
typealias CancelPaymentCompletionBlock = (Result<NoContent, ApiError>) -> Void

protocol FollowUpServicing {
    func fetchFollowUpStatus(withBillId billId: String, _ completion: @escaping FetchFollowUpStatusCompletionBlock)
    func cancelPayment(withBillId billId: String, isScheduling: Bool, _ completion: @escaping CancelPaymentCompletionBlock)
}

struct FollowUpService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - FollowUpServicing

extension FollowUpService: FollowUpServicing {
    func fetchFollowUpStatus(withBillId billId: String, _ completion: @escaping FetchFollowUpStatusCompletionBlock) {
        let endpoint = FollowUpEndpoint.followUpStatus(billId: billId)
        let api = Api<FollowUpStatus>(endpoint: endpoint)
        
        execute(api, then: completion)
    }
    
    func cancelPayment(withBillId billId: String, isScheduling: Bool, _ completion: @escaping CancelPaymentCompletionBlock) {
        let endpoint: FollowUpEndpoint = isScheduling
            ? .cancelScheduledPayment(billId: billId)
            : .cancelPayment(billId: billId)
        
        let api = Api<NoContent>(endpoint: endpoint)
        execute(api, then: completion)
    }
    
    private func execute<T>(
        _ api: Api<T>,
        decoder: JSONDecoder = JSONDecoder(),
        then completion: @escaping (Result<T, ApiError>) -> Void
    ) {
        api.execute(jsonDecoder: decoder) { result in
            self.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
