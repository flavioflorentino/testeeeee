import Core
import UIKit
import ReceiptKit

enum FollowUpAction: Equatable {
    case receipt(id: String, isScheduling: Bool)
    case accelerateAnalysis(url: String)
    case helpCenter(url: String)
    case close
}

protocol FollowUpCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: FollowUpAction)
}

final class FollowUpCoordinator {
    typealias Dependencies = HasLegacy & HasMainQueue
    private let dependencies: Dependencies

    weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - FollowUpCoordinating
extension FollowUpCoordinator: FollowUpCoordinating {
    func perform(action: FollowUpAction) {
        switch action {
        case let .receipt(id, isScheduling) where isScheduling:
            let entryPoint = ReceiptEntryPoint(id: id, type: .pav)
            let service = BilletReceiptService(entryPoint: entryPoint, dependencies: dependencies)
            let receiptController = DefaultReceiptFactory.make(with: service)
            
            viewController?.present(receiptController, animated: true)
            
        case .receipt(let id, _):
            let entryPoint = ReceiptEntryPoint(id: id, type: .pav)
            let receiptController = DefaultReceiptFactory.make(with: entryPoint)
            
            viewController?.present(receiptController, animated: true)
        
        case .helpCenter(let urlString), .accelerateAnalysis(let urlString):
            guard let url = URL(string: urlString) else { return }
        
            dependencies.legacy.deeplink?.open(url: url)
            
        case .close:
            if let navigationController = viewController?.navigationController {
                navigationController.popViewController(animated: true)
            } else {
                viewController?.dismiss(animated: true)
            }
        }
    }
}
