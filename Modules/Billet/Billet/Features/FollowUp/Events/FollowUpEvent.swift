import AnalyticsModule

enum FollowUpEvent: AnalyticsKeyProtocol, Equatable {
    case viewed(status: FollowUpStatusType)
    case actionSelected(ActionSelected)
    case faqAccessed
    case cancelPaymentPopup(isScheduling: Bool)
    
    private var name: String {
        switch self {
        case .viewed:
            return "Follow Up Viewed"
        case .actionSelected:
            return "Follow Up Option Selected"
        case .faqAccessed:
            return "Follow Up Faq Accessed"
        case .cancelPaymentPopup:
            return "Cancel Payment Modal Viewed"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .viewed(status):
            return ["status": status.rawValue]
        case let .actionSelected(action):
            return ["option_selected": action.rawValue]
        case let .cancelPaymentPopup(isScheduling):
            return ["is_scheduling": isScheduling]
        default:
            return [:]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .eventTracker]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("Bills - \(name)", properties: properties, providers: providers)
    }
}
