import UI
import UIKit

protocol FollowUpTimelineCellDelegate: AnyObject {
    func actionTapped(_ action: FollowUpStatusAction)
}

private extension FollowUpTimelineCell.Layout {
    enum Size {
        static let lineWidth = 1
        static let icon = CGSize(width: 24, height: 24)
    }
    
    enum Spacing {
        static let customIconLeading = 29
    }
}

final class FollowUpTimelineCell: UITableViewCell {
    fileprivate enum Layout { }
    
    // MARK: Views
    
    private lazy var topline = UIView()
    
    private lazy var iconLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(IconLabelStyle(type: .medium))
        return label
    }()
    
    private lazy var bottomline = UIView()

    private lazy var dateLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
        return label
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
        return label
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
        return label
    }()
    
    private lazy var containerButton: UIStackView = {
        let view = UIStackView()
        view.axis = .horizontal
        view.spacing = Spacing.base01
        return view
    }()
    
    private lazy var primaryButton: UIButton = {
        let button = UIButton(type: .custom)
        button.addTarget(self, action: #selector(primaryButtonTapped), for: .touchUpInside)
        button.buttonStyle(PrimaryButtonStyle(size: .small))
            .with(\.isHidden, true)
        return button
    }()
    
    private lazy var secondaryButton: UIButton = {
        let button = UIButton(type: .custom)
        button.addTarget(self, action: #selector(secondaryButtonTapped), for: .touchUpInside)
        button.buttonStyle(SecondaryButtonStyle(size: .small))
            .with(\.isHidden, true)
        return button
    }()
    
    // MARK: Properties
    
    private weak var delegate: FollowUpTimelineCellDelegate?
    
    private var hasActions = false
    private var primaryAction: FollowUpStatusAction?
    private var secondaryAction: FollowUpStatusAction?
}

// MARK: - Setups

extension FollowUpTimelineCell {
    func configure(with viewModel: FollowUpTimelineCellViewModeling, and delegate: FollowUpTimelineCellDelegate) {
        self.delegate = delegate
        
        iconLabel.text = viewModel.isCompleted ? Iconography.checkCircle.rawValue : Iconography.clockThree.rawValue
        dateLabel.text = viewModel.date
        titleLabel.text = viewModel.title
        messageLabel.text = viewModel.message
        
        hasActions = viewModel.actions.isNotEmpty
        
        viewModel.actions.forEach { action in
            setupButton(with: action)
        }
        
        topline.isHidden = viewModel.isFirstStatus
        bottomline.isHidden = viewModel.isLastStatus

        viewModel.isCompleted ? setupCompleteGraphicState() : setupIncompleteGraphicState()
        
        buildLayout()
    }
    
    private func setupButton(with action: FollowUpStatusAction) {
        switch action.type {
        case .receipt:
            primaryAction = action
            primaryButton.isHidden = false
            primaryButton.setTitle(action.title, for: .normal)
            
        case .helpCenter:
            secondaryAction = action
            secondaryButton.setTitle(action.title, for: .normal)
            secondaryButton.isHidden = false
            
        default:
            break
        }
    }
    
    private func setupCompleteGraphicState() {
        dateLabel.textColor = .grayscale700()
        titleLabel.textColor = .grayscale700()
        messageLabel.textColor = .grayscale700()
        
        topline.backgroundColor = .branding800()
        iconLabel.textColor = .branding800()
        bottomline.backgroundColor = .branding800()
    }
    
    private func setupIncompleteGraphicState() {
        dateLabel.textColor = .grayscale300()
        titleLabel.textColor = .grayscale300()
        messageLabel.textColor = .grayscale300()
        
        topline.backgroundColor = .grayscale300()
        iconLabel.textColor = .grayscale300()
        bottomline.backgroundColor = .grayscale300()
    }
}

// MARK: - Actions

@objc
private extension FollowUpTimelineCell {
    func primaryButtonTapped() {
        guard let action = primaryAction else { return }
        delegate?.actionTapped(action)
    }
    
    func secondaryButtonTapped() {
        guard let action = secondaryAction else { return }
        delegate?.actionTapped(action)
    }
}

// MARK: - ViewConfiguration

extension FollowUpTimelineCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubviews(topline, iconLabel, bottomline, dateLabel, titleLabel, messageLabel)
        
        guard hasActions else { return }
       
        containerButton.addArrangedSubviews(primaryButton, secondaryButton)
        contentView.addSubview(containerButton)
    }
    
    func setupConstraints() {
        dateLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.leading.equalTo(iconLabel.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base03)
        }
        
        topline.snp.makeConstraints {
            $0.width.equalTo(Layout.Size.lineWidth)
            $0.top.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base05)
        }
        
        iconLabel.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.icon)
            $0.top.equalTo(dateLabel.snp.bottom).offset(Spacing.base00)
            $0.top.equalTo(topline.snp.bottom).offset(Spacing.base00)
            $0.leading.equalToSuperview().offset(Layout.Spacing.customIconLeading)
        }
        
        bottomline.snp.makeConstraints {
            $0.width.equalTo(Layout.Size.lineWidth)
            $0.top.equalTo(iconLabel.snp.bottom).offset(Spacing.base00)
            $0.leading.equalToSuperview().offset(Spacing.base05)
            $0.bottom.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(dateLabel.snp.bottom).offset(Spacing.base00)
            $0.leading.equalTo(iconLabel.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base03)
        }
        
        messageLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalTo(iconLabel.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base03)
            
            if !hasActions {
                $0.bottom.equalToSuperview().offset(-Spacing.base02)
            }
        }
        
        setupActionConstraintsIfNeeded()
    }
    
    private func setupActionConstraintsIfNeeded() {
        guard hasActions else { return }
        
        containerButton.snp.makeConstraints {
            $0.top.equalTo(messageLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.equalTo(iconLabel.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base03)
            $0.bottom.equalToSuperview().offset(-Spacing.base02)
        }
        
        secondaryButton.snp.makeConstraints {
            $0.width.equalTo(primaryButton.snp.width)
        }
    }
    
    func configureViews() {
        selectionStyle = .none
    }
}
