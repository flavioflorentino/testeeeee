protocol FollowUpTimelineCellViewModeling {
    var date: String? { get }
    var title: String { get }
    var message: String { get }
    var actions: [FollowUpStatusAction] { get }
    var isCurrentStatus: Bool { get }
    var isCompleted: Bool { get }
    var isNextStatusCompleted: Bool { get }
    var isFirstStatus: Bool { get }
    var isLastStatus: Bool { get }
}

struct FollowUpTimelineCellViewModel: FollowUpTimelineCellViewModeling {
    let date: String?
    let title: String
    let message: String
    let actions: [FollowUpStatusAction]
    let isCurrentStatus: Bool
    let isCompleted: Bool
    let isNextStatusCompleted: Bool
    let isFirstStatus: Bool
    let isLastStatus: Bool
}
