import AnalyticsModule
import AssetsKit
import FeatureFlag
import Foundation

protocol FollowUpInteracting: AnyObject {
    func fetchFollowUpStatus()
    func cancelPayment(withId id: String, isScheduling: Bool)
    func handle(action: FollowUpStatusAction)
    func openFaq()
    func close()
}

final class FollowUpInteractor {
    private let service: FollowUpServicing
    private let presenter: FollowUpPresenting
    private let billId: String
    
    typealias Dependencies = HasFeatureManager & HasAnalytics
    private let dependencies: Dependencies
    
    private let timelineStatuses: [FollowUpStatusType] = [
        .processing,
        .pending,
        .completed
    ]

    init(service: FollowUpServicing, presenter: FollowUpPresenting, billId: String, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.billId = billId
        self.dependencies = dependencies
    }
}

// MARK: - FollowUpInteracting

extension FollowUpInteractor: FollowUpInteracting {
    func fetchFollowUpStatus() {
        presenter.presentLoading()
        
        service.fetchFollowUpStatus(withBillId: billId) { result in
            switch result {
            case .success(let followUpStatus) where self.timelineStatuses.contains(followUpStatus.currentStatusType):
                self.trackScreenViewed(statusType: followUpStatus.currentStatusType)
                self.presenter.presentHeader(with: followUpStatus)
                self.presenter.presentTimeline(with: followUpStatus)
                
            case .success(let followUpStatus):
                self.trackScreenViewed(statusType: followUpStatus.currentStatusType)
                self.presenter.presentHeader(with: followUpStatus)
                self.presenter.presentMessage(with: followUpStatus)
                
            case .failure:
                self.presenter.presentError()
            }
        }
    }
    
    func cancelPayment(withId id: String, isScheduling: Bool) {
        presenter.presentCancelLoading()
        
        service.cancelPayment(withBillId: id, isScheduling: isScheduling) { result in
            self.presenter.hideCancelLoading()
            
            switch result {
            case .success:
                self.fetchFollowUpStatus()
            case .failure:
                let alert = BilletErrorAlert(icon: .none,
                                             title: Strings.CancelPaymentPopup.Error.title,
                                             description: Strings.CancelPaymentPopup.Error.message,
                                             actions: [.init(title: Strings.Common.back)])
                
                self.presenter.present(alert: alert)
            }
        }
    }
    
    func handle(action: FollowUpStatusAction) {
        switch action.type {
        case .receipt, .scheduledReceipt:
            guard let id = action.id else { return }
            trackSelectedAction(action: .receipt)
            presenter.present(action: .receipt(id: id, isScheduling: action.type == .scheduledReceipt))
            
        case .helpCenter:
            guard let url = action.url else { return }
            trackSelectedAction(action: .knowMore)
            presenter.present(action: .helpCenter(url: url))
            
        case .accelerateAnalysis:
            guard let url = action.url else { return }
            trackSelectedAction(action: .accelerateAnalysis)
            presenter.present(action: .accelerateAnalysis(url: url))
            
        case .cancel, .scheduledCancel:
            guard let alert = formatCancelPaymentAlert(with: action) else { return }
            trackSelectedAction(action: .cancelPayment)
            presenter.present(alert: alert)
            
        default:
            break
        }
    }
    
    func openFaq() {
        trackFaqAcessed()
        let url = dependencies.featureManager.text(.billetFollowUpFaqUrl)
        presenter.present(action: .helpCenter(url: url))
    }
    
    func close() {
        presenter.present(action: .close)
    }

    private func formatCancelPaymentAlert(with action: FollowUpStatusAction) -> BilletErrorAlert? {
        guard let id = action.id else { return nil }
        
        let message = action.message?.replacingOccurrences(of: "<br/>", with: "\n")
        
        let actions: [BilletErrorAlert.Action] = [
            .init(title: Strings.CancelPaymentPopup.Warning.actionTitle) {
                let isScheduling = action.type == .scheduledCancel
                self.trackCancelPopup(isScheduling: isScheduling)
                self.cancelPayment(withId: id, isScheduling: isScheduling)
            },
            .init(title: Strings.Common.back)
        ]
        
        return BilletErrorAlert(icon: .image(Resources.Icons.icoWarningBig.image),
                                title: Strings.CancelPaymentPopup.Warning.title,
                                description: message ?? Strings.CancelPaymentPopup.Warning.message,
                                actions: actions)
    }
}

// MARK: - Tracking

private extension FollowUpInteractor {
    func trackScreenViewed(statusType: FollowUpStatusType) {
        let event = FollowUpEvent.viewed(status: statusType)
        dependencies.analytics.log(event)
    }
    
    func trackSelectedAction(action: ActionSelected) {
        let event = FollowUpEvent.actionSelected(action)
        dependencies.analytics.log(event)
    }
    
    func trackFaqAcessed() {
        let event = FollowUpEvent.faqAccessed
        dependencies.analytics.log(event)
    }
    
    func trackCancelPopup(isScheduling: Bool) {
        let event = FollowUpEvent.cancelPaymentPopup(isScheduling: isScheduling)
        dependencies.analytics.log(event)
    }
}
