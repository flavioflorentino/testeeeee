import UIKit

public protocol BilletFlowContract {
    func openBilletPayment(with model: BilletResponse,
                           description: String?,
                           origin: BilletOrigin,
                           from viewController: UIViewController)
}
