extension Date {
    /// "dd/MM/yyyy"
    static var ddMMyyyy: DateFormatter {
        let df = DateFormatter()
        df.dateFormat = "dd/MM/yyyy"
        df.locale = Locale(identifier: "pt_BR")
        return df
    }
    
    /// "yyyy-MM-dd"
    static var yyyyMMdd: DateFormatter {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        df.locale = Locale(identifier: "pt_BR")
        return df
    }
    
    /// "yyyy/MM/dd", e.g.: "2020/11/11"
    func yyyyMMdd() -> String {
        Date.yyyyMMdd.string(from: self)
    }
    
    static var getNextBusinessDay: Date {
        var date = Date()
        
        while Calendar.current.isDateInToday(date) || Calendar.current.isDateInWeekend(date) {
            date = Calendar.current.date(byAdding: .day, value: 1, to: date) ?? date
        }
        
        return date
    }
}
