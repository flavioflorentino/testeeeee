protocol HasNoDependency {}

protocol HasLegacy {
    var legacy: BilletSetupable { get }
}
