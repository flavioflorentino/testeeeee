import Foundation
import AnalyticsModule
import Core
import FeatureFlag

typealias BillsDependencies = HasNoDependency & HasAnalytics & HasFeatureManager & HasMainQueue & HasLegacy

final class DependencyContainer: BillsDependencies {
    lazy var mainQueue = DispatchQueue.main
    lazy var analytics: AnalyticsProtocol = Analytics.shared
    lazy var featureManager: FeatureManagerContract = FeatureManager.shared
    lazy var legacy: BilletSetupable = BilletSetup.shared
}
