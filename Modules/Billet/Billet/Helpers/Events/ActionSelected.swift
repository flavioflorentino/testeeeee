enum ActionSelected: String {
    case receipt = "VER RECIBO"
    case knowMore = "SAIBA MAIS"
    case understoodPayments = "ENTENDA SOBRE PAGAMENTOS"
    case followPayment = "ACOMPANHAR PAGAMENTO"
    case accelerateAnalysis = "ACELERAR ANÁLISE"
    case cancelPayment = "CANCELAR PAGAMENTO"
    case back = "VOLTAR"
}
