import AssetsKit
import UIKit

enum BilletErrorImageType: String, Decodable {
    case beneficiary = "man_shrugging"
    case pending_info = "info"
    case not_found = "ufo"
    case success
    case error
    
    var icon: UIImage {
        switch self {
        case .beneficiary:
            return Resources.Illustrations.iluEmptyCircledBackground.image
            
        case .pending_info:
            return Resources.Illustrations.iluPendingReview.image
            
        case .not_found:
            return Resources.Illustrations.iluNotFoundCircledBackground.image
            
        case .success:
            return Resources.Illustrations.iluSuccess.image
            
        case .error:
            return Resources.Illustrations.iluError.image
        }
    }
}

enum BilletErrorType: String, Decodable {
    case generic = "generic_error"
    case alreadyPayed = "already_payed_codes"
    case notRegistred = "billet_not_registered"
    case cpiNotRegistered = "billet_not_registered_to_cip"
    case notReceived = "billet_not_received"
    case invalidBarcode = "invalid_barcode"
    case notAccepted = "beneficiary_not_accept"
    case genericNotAccepted = "generic_beneficiary_not_accept"
    case issuingBeneficiary = "issuing_beneficiary_not_accept"
    case cashInPicPay = "billet_cash_in_picpay"
    case instability = "instability_error"
    case paid = "billet_paid"
    case expiredToday = "expired_billet_today"
    case expired = "expired_billet"
}

enum BilletErrorActionType: String, Decodable {
    case dismiss
    case faq = "faq_more"
    case receipt = "pav_receipt"
}

struct BilletErrorAction: Decodable {
    let type: BilletErrorActionType
    let title: String
    let url: String?
    let id: Int?
    
    enum CodingKeys: String, CodingKey {
        case type
        case title = "label"
        case data
        case url
        case id
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        type = try container.decode(BilletErrorActionType.self, forKey: .type)
        title = try container.decode(String.self, forKey: .title)
        
        let data = try? container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        url = try data?.decodeIfPresent(String.self, forKey: .url)
        id = try data?.decodeIfPresent(Int.self, forKey: .id)
    }
}

struct BilletError: Error, Decodable {
    let title: String
    let description: String
    let image: BilletErrorImageType?
    let type: BilletErrorType
    let actions: [BilletErrorAction]
    
    enum CodingKeys: String, CodingKey {
        case title
        case description
        case image
        case type
        case actions
    }
}
