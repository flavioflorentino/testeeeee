public enum BilletOrigin: String, Equatable {
    case homeSugestions = "home_sugestions"
    case scanner
    case personalCredit = "personal_credit"
    case p2pLending = "p2p_lending"
    case qrCodeIptu = "qr_code"
    case clipboard
    case pdfFile = "pdf_file"
    case deeplink
    case notification
    case homePayButton = "home_pay_button"
    case feed
    case receipt
    case search
    case unknown
    
    // swiftlint:disable cyclomatic_complexity
    public init(value: String) {
        switch value {
        case "home_sugestions":
            self = .homeSugestions
        case "scanner":
            self = .scanner
        case "personal_credit":
            self = .personalCredit
        case "p2p_lending":
            self = .p2pLending
        case "qr_code_iptu":
            self = .qrCodeIptu
        case "clipboard":
            self = .clipboard
        case "pdf_file":
            self = .pdfFile
        case "deeplink":
            self = .deeplink
        case "notification":
            self = .notification
        case "home_pay_button":
            self = .homePayButton
        case "feed":
            self = .feed
        case "receipt":
            self = .receipt
        case "search":
            self = .search
        case "unknown":
            self = .unknown
        default:
            self = .unknown
        }
    }
}
