import UI
import UIKit
import AssetsKit

struct BilletErrorAlert {
    enum Icon {
        case image(UIImage?)
        case none
    }
    
    struct Action {
        let title: String
        let action: (() -> Void)
        
        init(title: String, action: @escaping (() -> Void) = {}) {
            self.title = title
            self.action = action
        }
    }
    
    let icon: UIImage?
    let title: String
    let description: String
    let actions: [Action]
    
    init(icon: Icon, title: String, description: String, actions: [Action]) {
        self.icon = {
            guard case .image(let image) = icon else { return nil }
            return image
        }()
        self.title = title
        self.description = description
        self.actions = actions
    }
}
