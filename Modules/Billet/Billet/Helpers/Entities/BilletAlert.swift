import AssetsKit
import Core
import Foundation
import UI
import UIKit

public enum BilletAlert {
    case businessDay
    case overdueScheduling
    case paymentTimeLimitExceeded(timeLimit: String)
    case generic(icon: UIImage, title: String?, message: String?)
    
    var icon: UIImage {
        switch self {
        case .businessDay:
            return Resources.Illustrations.iluPersonInfo.image
            
        case .overdueScheduling:
            return Resources.Illustrations.iluPersonInfo.image
            
        case .paymentTimeLimitExceeded:
            return Resources.Illustrations.iluPersonInfo.image
            
        case .generic(let icon, _, _):
            return icon
        }
    }
    
    var title: String {
        switch self {
        case .businessDay:
            return Strings.BusinessDayPopup.title
            
        case .overdueScheduling:
            return Strings.OverdueSchedulingPopup.title
            
        case .paymentTimeLimitExceeded:
            return Strings.PaymentTimeExceededPopup.title
            
        case .generic(_, let title, _):
            return title ?? ""
        }
    }
    
    var message: String {
        switch self {
        case .businessDay:
            return Strings.BusinessDayPopup.message
                        
        case .overdueScheduling:
            return Strings.OverdueSchedulingPopup.message
            
        case .paymentTimeLimitExceeded(let timeLimit):
            return Strings.PaymentTimeExceededPopup.message(timeLimit)
            
        case .generic(_, _, let message):
            return message ?? ""
        }
    }
    
    var formattedMessage: NSAttributedString {
        format(text: message,
               defaultFont: .bodySecondary(),
               highlightedFont: .bodySecondary(.highlight),
               defaultColor: Colors.grayscale500.color,
               highlightedColor: Colors.grayscale500.color)
    }
}

private extension BilletAlert {
    func format(text: String,
                defaultFont: Typography,
                highlightedFont: Typography,
                defaultColor: UIColor,
                highlightedColor: UIColor,
                textAlignment: NSTextAlignment = .center,
                hasUnderline: Bool = false) -> NSAttributedString {
        let formattedString = text
            .replacingOccurrences(of: "<br>", with: "\n")
            .replacingOccurrences(of: "**", with: "[b]")
            .replacingOccurrences(of: "<strong>", with: "[b]")
            .replacingOccurrences(of: "</strong>", with: "[b]")
        
        return formattedString.attributedStringWith(
            normalFont: defaultFont.font(),
            highlightFont: highlightedFont.font(),
            normalColor: defaultColor,
            highlightColor: highlightedColor,
            textAlignment: textAlignment,
            underline: hasUnderline,
            separator: "[b]"
        )
    }
}
