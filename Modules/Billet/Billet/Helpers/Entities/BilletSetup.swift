import Core
import UI

public protocol BilletSetupable: AnyObject {
    var billet: BilletFlowContract? { get set }
    var contactList: ContactListContract? { get set }
    var deeplink: DeeplinkContract? { get set }
    var profile: ProfileContract? { get set }
    var receipt: ReceiptContract? { get set }
    var webview: WebViewCreatable? { get set }
}

public final class BilletSetup: BilletSetupable {
    public static let shared = BilletSetup()
    
    public var billet: BilletFlowContract?
    public var contactList: ContactListContract?
    public var deeplink: DeeplinkContract?
    public var profile: ProfileContract?
    public var receipt: ReceiptContract?
    public var webview: WebViewCreatable?
    
    private init() {}
    
    public func inject(instance: Any) {
        switch instance {
        case let instance as BilletFlowContract:
            billet = instance
        case let instance as ContactListContract:
            contactList = instance
        case let instance as DeeplinkContract:
            deeplink = instance
        case let instance as ProfileContract:
            profile = instance
        case let instance as ReceiptContract:
            receipt = instance
        case let instance as WebViewCreatable:
            webview = instance
        default:
            assertionFailure("🚨\nAttempted to inject unexpected depedency: \(instance)\n")
        }
    }
}
