@testable import Statement
import UI
import XCTest
import Core

private class StatementContainerCoordinatingSpy: StatementContainerCoordinating {
    var viewController: UIViewController?
    var deeplink: DeeplinkContract?
    
    // MARK: - perform
    private(set) var performActionCallsCount = 0
    private(set) var action: StatementContainerAction?

    func perform(action: StatementContainerAction) {
        performActionCallsCount += 1
        self.action = action
    }
}

private class StatementContainerDisplaySpy: StatementContainerDisplay {
}

final class StatementContainerPresenterTests: XCTestCase {
    private let coordinatorSpy = StatementContainerCoordinatingSpy()
    private let viewControllerSpy = StatementContainerDisplaySpy()
    private lazy var sut: StatementContainerPresenting = {
        let presenter = StatementContainerPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testDidNextStep_WhenActionIsClose_ShouldPerformCloseAction() {
        sut.didNextStep(action: .close)
        
        XCTAssertEqual(coordinatorSpy.action, .close)
    }
}
