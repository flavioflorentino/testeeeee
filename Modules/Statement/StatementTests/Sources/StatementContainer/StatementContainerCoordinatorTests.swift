@testable import Statement
import XCTest
import Core

private final class DeeplinkWrapperSpy: DeeplinkContract {
    private(set) var callOpenDeeplinkCount = 0
    
    func open(url: URL) -> Bool {
        callOpenDeeplinkCount += 1
        return true
    }
}

final class StatementContainerCoordinatorTests: XCTestCase {
    private let navControllerSpy = NavigationControllerSpy(rootViewController: UIViewController())
    private let deeplinkWrapperSpy = DeeplinkWrapperSpy()
    
    private lazy var sut: StatementContainerCoordinating = {
        let coordinator = StatementContainerCoordinator()
        coordinator.viewController = navControllerSpy.topViewController
        coordinator.deeplink = deeplinkWrapperSpy
        return coordinator
    }()
    
    func testPerformAction_WhenActionIsClose_ShouldDismisNavigationController() {
        sut.perform(action: .close)
        
        XCTAssertEqual(navControllerSpy.dismissCallsCount, 1)
    }
    
    func testPerformAction_WhenActionIsOpenCustomerSupport_ShouldOpenCustomerSupport() {
        sut.perform(action: .openCustomerSupport)
        
        XCTAssertEqual(deeplinkWrapperSpy.callOpenDeeplinkCount, 1)
    }
}
