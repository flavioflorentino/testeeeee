import AnalyticsModule
import Core
@testable import Statement
import XCTest

private final class StatementContainerServicingMock: StatementContainerServicing {
}

private final class StatementContainerPresentingSpy: StatementContainerPresenting {
    var viewController: StatementContainerDisplay?
    
    // MARK: - didNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var action: StatementContainerAction?
    func didNextStep(action: StatementContainerAction) {
        didNextStepActionCallsCount += 1
        self.action = action
    }
}

final class StatementContainerViewModelTests: XCTestCase {
    private let serviceMock = StatementContainerServicingMock()
    private let presenterSpy = StatementContainerPresentingSpy()
    private let analyticsSpy = AnalyticsSpy()
    private lazy var sut = StatementContainerInteractor(
        service: serviceMock,
        presenter: presenterSpy,
        dependencies: DependencyContainerMock(analyticsSpy),
        origin: .wallet
    )
    
    func testDidClose_ShouldClose() {
        sut.didClose()
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .close)
    }
    
    func testTrackView_ShouldSendMainPageViewedEvent() {
        let expectedEvent = StatementEvent.mainPageViewed(origin: .wallet).event()
        
        sut.trackView()
        
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testDidOpenCustomerSupport_ShouldOpenCustomerSupport() {
        sut.didOpenCustomerSupport()
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .openCustomerSupport)
    }
}
