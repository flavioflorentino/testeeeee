@testable import Statement
import Core
import UI
import XCTest

final class StatementListCoordinatorSpy: StatementListCoordinating {
    var viewController: UIViewController?
    var incomeContract: IncomeContract?
    var wireTransferReceiptContract: WireTransferReceiptContract?
    var receiptContract: ReceiptContract?
    
    // MARK: - perform
    private(set) var performCallsCount = 0
    private(set) var performAction: StatementListAction?
    
    func perform(action: StatementListAction) {
        performCallsCount += 1
        performAction = action
    }
}

final class StatementListViewControllerSpy: StatementListDisplay {
    private(set) var fromSections: [StatementSection] = []
    
    // MARK: - displayNewItems
    private(set) var presentViewItemsCallsCount = 0
    
    func displayNewItems(fromSections sections: [StatementSection]) {
        presentViewItemsCallsCount += 1
        fromSections = sections
    }
    
    // MARK: - updateItems
    private(set) var updateItemsCallsCount = 0
    
    func updateItems(fromSections sections: [StatementSection]) {
        updateItemsCallsCount += 1
        fromSections = sections
    }
    
    // MARK: - removeItems
    private(set) var removeItemsCallsCount = 0
    
    func removeItems(fromSections sections: [StatementSection]) {
        removeItemsCallsCount += 1
        fromSections = sections
    }
    
    // MARK: - showAnimatedSkeletonView
    private(set) var showAnimatedSkeletonViewCallsCount = 0
    
    func showAnimatedSkeletonView() {
        showAnimatedSkeletonViewCallsCount += 1
    }

    // MARK: - hideAnimatedSkeletonView
    private(set) var hideAnimatedSkeletonViewCallsCount = 0
    
    func hideAnimatedSkeletonView() {
        hideAnimatedSkeletonViewCallsCount += 1
    }
    
    // MARK: - hideRefreshControl
    private(set) var hideRefreshControlCallsCount = 0
    
    func hideRefreshControl() {
        hideRefreshControlCallsCount += 1
    }
    
    // MARK: - displayTableFooterView
    private(set) var displayTableFooterViewCallsCount = 0
    private(set) var footerView: UIView?
    
    func displayTableFooterView(_ footerView: UIView) {
        displayTableFooterViewCallsCount += 1
        self.footerView = footerView
    }
  
    // MARK: - displayError
    private(set) var displayErrorCallsCount = 0
    private(set) var displayErrorTitle: String?
    private(set) var displayErrorText: String?
    private(set) var displayErrorButtonText: String?
    private(set) var displayErrorImage: UIImage?
    
    func displayError(title: String, text: String, buttonText: String?, image: UIImage) {
        displayErrorCallsCount += 1
        displayErrorTitle = title
        displayErrorText = text
        displayErrorButtonText = buttonText
        displayErrorImage = image
    }
    
    // MARK: - removeStatusViewIfNeeded
    private(set) var removeStatusViewIfNeededCallsCount = 0
    
    func removeStatusViewIfNeeded() {
        removeStatusViewIfNeededCallsCount += 1
    }
    
    // MARK: - displayEmptyView
    private(set) var displayEmptyViewCallsCount = 0
    private(set) var displayEmptyViewTitle: String?
    private(set) var displayEmptyViewText: String?
    private(set) var displayEmptyViewImage: UIImage?
    
    func displayEmptyView(title: String, text: String, image: UIImage) {
        displayEmptyViewCallsCount += 1
        displayEmptyViewTitle = title
        displayEmptyViewText = text
        displayEmptyViewImage = image
    }
}

final class StatementListPresenterTests: XCTestCase {
    private let coordinatorSpy = StatementListCoordinatorSpy()
    private let viewControllerSpy = StatementListViewControllerSpy()
    
    private lazy var sut: StatementListPresenting = {
        let presenter = StatementListPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testPresentViewItems_ShouldDisplayNewItems() {
        let sections = StatementListMock.sections
        sut.presentNewItems(fromSections: sections)
        
        XCTAssertEqual(viewControllerSpy.presentViewItemsCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.fromSections, sections)
    }
    
    func testUpdateItems_ShoulUpdateItems() {
        let sections = StatementListMock.sections
        sut.updateItems(fromSections: sections)
        
        XCTAssertEqual(viewControllerSpy.updateItemsCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.fromSections, sections)
    }
    
    func testShowAnimatedSkeletonViews_ShoulShowAnimatedSkeletonView() {
        sut.showAnimatedSkeletonView()
        
        XCTAssertEqual(viewControllerSpy.showAnimatedSkeletonViewCallsCount, 1)
    }
    
    func testHideAnimatedSkeletonViews_ShoulHideAnimatedSkeletonView() {
        sut.hideAnimatedSkeletonView()
        
        XCTAssertEqual(viewControllerSpy.hideAnimatedSkeletonViewCallsCount, 1)
    }
    
    func testPresentPaginationState_WhenPassedLoadingState_ShouldDisplayLoadingFooterView() {
        sut.presentPaginationState(.loading)
        
        XCTAssertEqual(viewControllerSpy.displayTableFooterViewCallsCount, 1)
        XCTAssert(viewControllerSpy.footerView is StatementListFooterLoadingView)
    }
    
    func testPresentPaginationState_WhenPassedFailureState_ShouldDisplayFailureFooterView() {
        sut.presentPaginationState(.failure)
        
        XCTAssertEqual(viewControllerSpy.displayTableFooterViewCallsCount, 1)
        XCTAssert(viewControllerSpy.footerView is StatementListFooterFailureView)
    }
    
    func testPresentPaginationState_WhenPassedEndState_ShouldDisplayLoadingEndOfListView() {
        sut.presentPaginationState(.end)
        
        XCTAssertEqual(viewControllerSpy.displayTableFooterViewCallsCount, 1)
        XCTAssert(viewControllerSpy.footerView is StatementListWelcomeView)
    }
    
    func testPresentConnectionFailureError_ShouldDisplayConnectionFailureError() {
        sut.presentConnectionFailureError()
        
        XCTAssertEqual(viewControllerSpy.displayErrorCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.displayErrorTitle, Strings.StatementList.ConnectionFailure.title)
        XCTAssertEqual(viewControllerSpy.displayErrorText, Strings.StatementList.ConnectionFailure.text)
        XCTAssertEqual(viewControllerSpy.displayErrorButtonText, Strings.StatementList.ConnectionFailure.buttonText)
        XCTAssertEqual(viewControllerSpy.displayErrorImage, Assets.noConnection.image)
    }
    
    func testPresentGenericError_ShouldDisplayGenericError() {
        sut.presentGenericError()
        
        XCTAssertEqual(viewControllerSpy.displayErrorCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.displayErrorTitle, Strings.StatementList.GenericError.title)
        XCTAssertEqual(viewControllerSpy.displayErrorText, Strings.StatementList.GenericError.text)
        XCTAssertEqual(viewControllerSpy.displayErrorButtonText, Strings.StatementList.GenericError.buttonText)
        XCTAssertEqual(viewControllerSpy.displayErrorImage, Assets.genericError.image)
    }
    
    func testRemoveStatusViewIfNeeded_ShouldRemoveStatusViewIfNeeded() {
        sut.removeStatusViewIfNeeded()
        
        XCTAssertEqual(viewControllerSpy.removeStatusViewIfNeededCallsCount, 1)
    }
    
    func testPresentEmptyView_ShouldDisplayEmptyView() {
        sut.presentEmptyView()
        
        XCTAssertEqual(viewControllerSpy.displayEmptyViewCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.displayEmptyViewTitle, Strings.StatementList.EmptyView.title)
        XCTAssertEqual(viewControllerSpy.displayEmptyViewText, Strings.StatementList.EmptyView.text)
        XCTAssertEqual(viewControllerSpy.displayEmptyViewImage, Assets.emptyState.image)
    }
    
    func testRemoveItems_ShouldCallRemoveItems() {
        let sections = StatementListMock.sections
        
        sut.removeItems(fromSections: sections)
        
        XCTAssertEqual(viewControllerSpy.removeItemsCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.fromSections, sections)
    }
    
    func testHideRefreshControl_ShouldHideRefreshControl() {
        sut.hideRefreshControl()
        
        XCTAssertEqual(viewControllerSpy.hideRefreshControlCallsCount, 1)
    }
    
    func testDidNextStep_WhenActionIsOpenIncome_ShouldOpenIncome() {
        sut.didNextStep(action: .openIncome)
        
        XCTAssertEqual(coordinatorSpy.performCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.performAction, .openIncome)
    }
    
    func testDidNextStep_WhenActionIsOpenReceipt_ShouldOpenReceipt() {
        let action: StatementListAction = .openReceipt(transactionId: "", receiptType: "")
        sut.didNextStep(action: action)
        
        XCTAssertEqual(coordinatorSpy.performCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.performAction, action)
    }
    
    func testDidNextStep_WhenActionIsOpenCashin_ShouldOpenCashin() {
        let action: StatementListAction = .openCashin(transactionId: "")
        sut.didNextStep(action: action)
        
        XCTAssertEqual(coordinatorSpy.performCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.performAction, action)
    }
    
    func testDidNextStep_WhenActionIsOpenCashout_ShouldOpenCashout() {
        let action: StatementListAction = .openCashout(transactionId: "")
        sut.didNextStep(action: action)
        
        XCTAssertEqual(coordinatorSpy.performCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.performAction, action)
    }
}
