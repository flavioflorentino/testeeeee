import Foundation
@testable import Statement

enum StatementListMock {
    static func mock(isLastPage: Bool = false, sections: [StatementSection] = sections, isEmpty: Bool = false) -> StatementList {
        StatementList(
            pagination: StatementPagination(
                pageIndex: 0,
                pageSize: 0,
                isLastPage: isLastPage
            ), statement: isEmpty ? [] : sections)
    }
    
    static let sections = [
        StatementSection(
            header: StatementHeader(date: "16/Nov"),
            statementItems: [
                StatementItem(
                    title: "Playstation Store",
                    time: "20:00",
                    date: "",
                    description: nil,
                    image: "",
                    type: .digitalGoods,
                    status: .default,
                    operationDetails: [
                        OperationDetail(
                            description: "Saldo",
                            value: "-R$ 30,00",
                            style: .debit
                        )
                    ],
                    transactionId: "1234",
                    flow: .inflow
                ),
                StatementItem(
                    title: "Playstation Store",
                    time: "20:00",
                    date: "",
                    description: nil,
                    image: "",
                    type: .digitalGoods,
                    status: .default,
                    operationDetails: [
                        OperationDetail(
                            description: "Saldo",
                            value: "-R$ 30,00",
                            style: .debit
                        )
                    ],
                    transactionId: "1234",
                    flow: .inflow
                )
            ]
        ),
        StatementSection(
            header: StatementHeader(date: "15/Nov"),
            statementItems: [
                StatementItem(
                    title: "Playstation Store",
                    time: "20:00",
                    date: "",
                    description: nil,
                    image: "",
                    type: .digitalGoods,
                    status: .default,
                    operationDetails: [
                        OperationDetail(
                            description: "Saldo",
                            value: "-R$ 30,00",
                            style: .debit
                        )
                    ],
                    transactionId: "1234",
                    flow: .inflow
                )
            ]
        )
    ]
    
    static let sectionsNewPage = [
        StatementSection(
            header: StatementHeader(date: "14/Nov"),
            statementItems: [
                StatementItem(
                    title: "Playstation Store",
                    time: "20:00",
                    date: "",
                    description: nil,
                    image: "",
                    type: .digitalGoods,
                    status: .default,
                    operationDetails: [
                        OperationDetail(
                            description: "Saldo",
                            value: "-R$ 30,00",
                            style: .debit
                        )
                    ],
                    transactionId: "1234",
                    flow: .inflow
                ),
                StatementItem(
                    title: "Playstation Store",
                    time: "20:00",
                    date: "",
                    description: nil,
                    image: "",
                    type: .digitalGoods,
                    status: .default,
                    operationDetails: [
                        OperationDetail(
                            description: "Saldo",
                            value: "-R$ 30,00",
                            style: .debit
                        )
                    ],
                    transactionId: "1234",
                    flow: .inflow
                )
            ]
        ),
        StatementSection(
            header: StatementHeader(date: "13/Nov"),
            statementItems: [
                StatementItem(
                    title: "Playstation Store",
                    time: "20:00",
                    date: "",
                    description: nil,
                    image: "",
                    type: .digitalGoods,
                    status: .default,
                    operationDetails: [
                        OperationDetail(
                            description: "Saldo",
                            value: "-R$ 30,00",
                            style: .debit
                        )
                    ],
                    transactionId: "1234",
                    flow: .inflow
                )
            ]
        )
    ]
    
    static let sectionsNewPageMergeGroup = [
        StatementSection(
            header: StatementHeader(date: "15/Nov"),
            statementItems: [
                StatementItem(
                    title: "Playstation Store",
                    time: "20:00",
                    date: "",
                    description: nil,
                    image: "",
                    type: .digitalGoods,
                    status: .default,
                    operationDetails: [
                        OperationDetail(
                            description: "Saldo",
                            value: "-R$ 30,00",
                            style: .debit
                        )
                    ],
                    transactionId: "1234",
                    flow: .inflow
                )
            ]
        ),
        StatementSection(
            header: StatementHeader(date: "14/Nov"),
            statementItems: [
                StatementItem(
                    title: "Playstation Store",
                    time: "20:00",
                    date: "",
                    description: nil,
                    image: "",
                    type: .digitalGoods,
                    status: .default,
                    operationDetails: [
                        OperationDetail(
                            description: "Saldo",
                            value: "-R$ 30,00",
                            style: .debit
                        )
                    ],
                    transactionId: "1234",
                    flow: .inflow
                )
            ]
        )
    ]
    
    static func item(
        withType type: StatementItem.ItemType,
        transactionId: String? = "1234",
        status: StatementItem.Status = .default,
        flow: StatementItem.OperationFlow = .inflow
    ) -> StatementItem {
        StatementItem(
            title: "P2P",
            time: "20:00",
            date: "11/11/2020",
            description: "",
            image: "",
            type: type,
            status: status,
            operationDetails: [],
            transactionId: transactionId,
            flow: flow
        )
    }
}
