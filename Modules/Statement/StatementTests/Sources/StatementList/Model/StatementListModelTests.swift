@testable import Statement
import XCTest

final class StatementListModelTests: XCTestCase {
    func testDecodeStateList_WhenIsFirstPage_ShouldDecodeSuccefully() throws {
        let model: StatementList = try JSONFileDecoder().load(resource: "statementListFirstPageMock", typeDecoder: .convertFromSnakeCase)
        XCTAssertEqual(model, firstPageMockedModel)
    }
    
    func testDecodeStateList_WhenIsLastPage_ShouldDecodeSuccefully() throws {
        let model: StatementList = try JSONFileDecoder().load(resource: "statementListLastPageMock", typeDecoder: .convertFromSnakeCase)
        XCTAssertEqual(model, lastPageMockedModel)
    }
}

extension StatementListModelTests {
    private var firstPageMockedModel: StatementList {
        StatementList(
            pagination: StatementPagination(pageIndex: 1, pageSize: 15, isLastPage: false),
            statement: [
                StatementSection(
                    header: StatementHeader(date: "17 Mai/20"),
                    statementItems: [
                        StatementItem(
                            title: "Pagamento recebido",
                            time: "18:30",
                            date: "",
                            description: "@alinesilva",
                            image: "https://picpay-dev.s3.amazonaws.com/profiles-processed-images/522f362ec08b70d5f79ad3a45a2fba5b.500.jpg",
                            type: .p2p,
                            status: .default,
                            operationDetails: [
                                OperationDetail(description: "Cartão de final 5643", value: "+R$ 60,00", style: .credit)
                            ],
                            transactionId: "1234",
                            flow: .inflow
                        ),
                        StatementItem(
                            title: "Pagamento enviado",
                            time: "12:51",
                            date: "",
                            description: "@marcoassis",
                            image: nil,
                            type: .p2p,
                            status: .default,
                            operationDetails: [
                                OperationDetail(description: "Saldo", value: "-R$ 100,00", style: .default),
                                OperationDetail(description: "Cartão Master 9821", value: "-R$ 50,00", style: .default),
                                OperationDetail(description: "Total", value: "-R$ 150,00", style: .debit)
                            ],
                            transactionId: "1234",
                            flow: .inflow
                        ),
                        StatementItem(
                            title: "Magazine Luiza",
                            time: "07:23",
                            date: "",
                            description: "Pagamento estornado",
                            image: nil,
                            type: .pav,
                            status: .canceled,
                            operationDetails: [
                                OperationDetail(description: "Cartão Nubank 7633", value: "+R$ 60,00", style: .default)
                            ],
                            transactionId: "1234",
                            flow: .inflow
                        )
                    ]
                ),
                StatementSection(
                    header: StatementHeader(date: "16 Mai/20"),
                    statementItems: [
                        StatementItem(
                            title: "Playstation Store",
                            time: "20:00",
                            date: "",
                            description: nil,
                            image: nil,
                            type: .generic,
                            status: .default,
                            operationDetails: [
                                OperationDetail(description: "Saldo", value: "-R$ 30,00", style: .default)
                            ],
                            transactionId: "1234",
                            flow: .inflow
                        )
                    ]
                )
            ]
        )
    }
    
    private var lastPageMockedModel: StatementList {
        StatementList(
            pagination: StatementPagination(pageIndex: 3, pageSize: 15, isLastPage: true),
            statement: [
                StatementSection(
                    header: StatementHeader(date: "16 Mai/20"),
                    statementItems: [
                        StatementItem(
                            title: "Playstation Store",
                            time: "20:00",
                            date: "",
                            description: nil,
                            image: nil,
                            type: .generic,
                            status: .default,
                            operationDetails: [
                                OperationDetail(description: "Saldo", value: "-R$ 30,00", style: .default)
                            ],
                            transactionId: "1234",
                            flow: .inflow
                        )
                    ]
                )
            ]
        )
    }
}
