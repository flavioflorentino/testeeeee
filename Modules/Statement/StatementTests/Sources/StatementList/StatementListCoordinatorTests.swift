import XCTest
import Core
@testable import Statement

private final class IncomeContractSpy: IncomeContract {
    // MARK: - open
    private(set) var openCallsCount = 0
    
    func open() {
        openCallsCount += 1
    }
}

private final class ReceiptContractSpy: ReceiptContract {
    // MARK: - open
    private(set) var openCallsCount = 0
    private(set) var id: String?
    private(set) var receiptType: String?
    private(set) var feedItemId: String?
    
    func open(withId id: String, receiptType: String, feedItemId: String?) {
        openCallsCount += 1
        self.id = id
        self.receiptType = receiptType
        self.feedItemId = feedItemId
    }
}

private final class WireTransferReceiptContractSpy: WireTransferReceiptContract {
    // MARK: - open
    private(set) var openCallsCount = 0
    private(set) var id: String?
    
    func open(withId id: String) {
        openCallsCount += 1
        self.id = id
    }
}

final class StatementListCoordinatorTests: XCTestCase {
    private let navControllerSpy = NavigationControllerSpy(rootViewController: UIViewController())
    private let incomeContractSpy = IncomeContractSpy()
    private let receiptContractSpy = ReceiptContractSpy()
    private let wireTransferReceiptContractSpy = WireTransferReceiptContractSpy()
    
    private lazy var sut: StatementListCoordinating = {
        let coordinator = StatementListCoordinator()
        coordinator.viewController = navControllerSpy
        coordinator.incomeContract = incomeContractSpy
        coordinator.receiptContract = receiptContractSpy
        coordinator.wireTransferReceiptContract = wireTransferReceiptContractSpy
        return coordinator
    }()
    
    func testPerfom_WhenActionIsOpenIncome_ShouldOpenIncome() {
        sut.perform(action: .openIncome)
        
        XCTAssertEqual(incomeContractSpy.openCallsCount, 1)
    }
    
    func testPerfom_WhenActionIsOpenReceipt_ShouldOpenReceipt() {
        sut.perform(action: .openReceipt(transactionId: "id", receiptType: "type"))
        
        XCTAssertEqual(receiptContractSpy.openCallsCount, 1)
        XCTAssertEqual(receiptContractSpy.id, "id")
        XCTAssertEqual(receiptContractSpy.receiptType, "type")
        XCTAssertNil(receiptContractSpy.feedItemId)
    }
    
    func testPerfom_WhenActionIsOpenCashin_ShouldOpenCashin() {
        sut.perform(action: .openCashin(transactionId: "id"))
        
        XCTAssertEqual(wireTransferReceiptContractSpy.openCallsCount, 1)
        XCTAssertEqual(wireTransferReceiptContractSpy.id, "id")
    }
    
    func testPerfom_WhenActionIsOpenCashout_ShouldOpenCashoutViewController() {
        sut.perform(action: .openCashout(transactionId: "id"))
        
        XCTAssertEqual(navControllerSpy.presentViewControllerCallsCount, 1)
    }
}
