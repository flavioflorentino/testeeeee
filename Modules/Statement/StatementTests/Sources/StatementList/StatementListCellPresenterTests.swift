import FeatureFlag
@testable import Statement
import UI
import XCTest

private class StatementListCellDisplaySpy: StatementListCellDisplay {
    // MARK: - displayTime
    private(set) var displayTimeCallsCount = 0
    private(set) var time: String?

    func displayTime(_ time: String) {
        displayTimeCallsCount += 1
        self.time = time
    }

    // MARK: - displayTitle
    private(set) var displayTitleCallsCount = 0
    private(set) var title: String?

    func displayTitle(_ title: String) {
        displayTitleCallsCount += 1
        self.title = title
    }

    // MARK: - displayDescription
    private(set) var displayDescriptionCallsCount = 0
    private(set) var description: String?

    func displayDescription(_ description: String) {
        displayDescriptionCallsCount += 1
        self.description = description
    }

    // MARK: - hideDescription
    private(set) var hideDescriptionCallsCount = 0

    func hideDescription() {
        hideDescriptionCallsCount += 1
    }

    // MARK: - displayOperationDetailItems
    private(set) var displayOperationDetailItemsCallsCount = 0
    private(set) var operationDetailItems = [OperationDetail]()
    private(set) var isCanceled  = false
    private(set) var actionIndicator = false
    
    func displayOperationDetailItems(_ operationDetailItems: [OperationDetail], isCanceled: Bool, actionIndicator: Bool) {
        displayOperationDetailItemsCallsCount += 1
        self.operationDetailItems = operationDetailItems
        self.isCanceled = isCanceled
        self.actionIndicator = actionIndicator
    }

    // MARK: - displayItemPlaceholderIcon
    private(set) var displayItemPlaceholderIconCallsCount = 0
    private(set) var icon: Iconography?

    func displayItemPlaceholderIcon(from icon: Iconography) {
        displayItemPlaceholderIconCallsCount += 1
        self.icon = icon
    }

    // MARK: - displayItemImage
    private(set) var displayItemImageCallsCount = 0
    private(set) var imageUrl: URL?

    func displayItemImage(from imageUrl: URL) {
        displayItemImageCallsCount += 1
        self.imageUrl = imageUrl
    }
    
    // MARK: - setupAccessibilityLabel
    private(set) var setupAccessibilityLabelCallsCount = 0
    private(set) var accessibilityText: String?
    func setupAccessibilityLabel(with accessibilityText: String) {
        self.accessibilityText = accessibilityText
        setupAccessibilityLabelCallsCount += 1
    }
}

final class StatementListCellPresenterTests: XCTestCase {
    private let viewSpy = StatementListCellDisplaySpy()
    
    private func mockItem(
        description: String? = nil,
        imageUrl: String? = nil,
        status: StatementItem.Status = .default,
        type: StatementItem.ItemType = .p2p,
        flow: StatementItem.OperationFlow = .inflow
    ) -> StatementItem {
        StatementItem(
            title: "Pagamento enviado",
            time: "12:51",
            date: "",
            description: description,
            image: imageUrl,
            type: type,
            status: status,
            operationDetails: [
                OperationDetail(description: "Saldo", value: "-R$ 10,00", style: .default),
                OperationDetail(description: "Cartão Master 9821", value: "-R$ 50,00", style: .default),
                OperationDetail(description: "Total", value: "-R$ 150,00", style: .debit)
            ],
            transactionId: "1234",
            flow: flow
        )
    }
    
    private let featureManagerMock: FeatureManagerMock = {
        let mock = FeatureManagerMock()
        mock.override(key: .isStatementDetailEnabled, with: false)
        return mock
    }()
    
    private lazy var sut: StatementListCellPresenting = {
        let sut = StatementListCellPresenter(
            dependencies: DependencyContainerMock(featureManagerMock)
        )
        sut.view = viewSpy
        return sut
    }()
    
    func testSetupModel_ShouldDisplayTitle() {
        let mockedModel = mockItem(status: .default)
        
        sut.setup(model: mockedModel)
        
        XCTAssertEqual(viewSpy.displayTitleCallsCount, 1)
        XCTAssertEqual(viewSpy.title, mockedModel.title)
    }
    
    func testSetupModel_ShouldDisplayTime() {
        let mockedModel = mockItem()
        
        sut.setup(model: mockedModel)
        
        XCTAssertEqual(viewSpy.displayTimeCallsCount, 1)
        XCTAssertEqual(viewSpy.time, mockedModel.time)
    }
    
    func testSetupModel_WhenItemHasDescription_ShouldDisplayDescription() {
        let mockedModel = mockItem(description: "@eduardo.m.o")
        
        sut.setup(model: mockedModel)
        
        XCTAssertEqual(viewSpy.displayDescriptionCallsCount, 1)
        XCTAssertEqual(viewSpy.description, mockedModel.description)
    }
    
    func testSetupModel_WhenItemHasNotDescription_ShouldHideDescription() {
        let mockedModel = mockItem(description: nil)
        
        sut.setup(model: mockedModel)
        
        XCTAssertEqual(viewSpy.hideDescriptionCallsCount, 1)
        XCTAssertEqual(viewSpy.displayDescriptionCallsCount, 0)
    }
    
    func testSetupModel_WhenItemWasNotCanceled_ShouldDisplayDescriptionWithNoSpecialFormating() {
        let mockedModel = mockItem(status: .default)
        
        sut.setup(model: mockedModel)
        
        XCTAssertEqual(viewSpy.displayOperationDetailItemsCallsCount, 1)
        XCTAssertEqual(viewSpy.operationDetailItems, mockedModel.operationDetails)
        XCTAssertFalse(viewSpy.isCanceled)
    }
    
    func testSetupModel_WhenItemWasCanceled_ShouldDisplayStrikethroughDescriptionAndCanceledIcon() {
        let mockedModel = mockItem(status: .canceled)
        
        sut.setup(model: mockedModel)
        
        XCTAssertEqual(viewSpy.displayOperationDetailItemsCallsCount, 1)
        XCTAssertEqual(viewSpy.operationDetailItems, mockedModel.operationDetails)
        
        XCTAssertEqual(viewSpy.displayItemPlaceholderIconCallsCount, 1)
        XCTAssertEqual(viewSpy.icon, .cornerUpLeft)
        
        XCTAssert(viewSpy.isCanceled)
    }
    
    func testSetupModel_WhenItemHasActionAndActionsAreEnabled_ShouldDisplayActionIndicator() {
        featureManagerMock.override(key: .isStatementDetailEnabled, with: true)
        
        let itensWithAction: [StatementItem] = [
            mockItem(type: .income),
            mockItem(type: .cashout),
            mockItem(type: .pav),
            mockItem(type: .phoneRecharge),
            mockItem(type: .digitalGoods),
            mockItem(type: .mobileTickets),
            mockItem(type: .picpayCard),
            mockItem(type: .p2pLending),
            mockItem(type: .p2p, flow: .outflow)
        ]
        
        itensWithAction.forEach {
            sut.setup(model: $0)
            XCTAssertTrue(viewSpy.actionIndicator)
        }
        
        XCTAssertEqual(viewSpy.displayOperationDetailItemsCallsCount, itensWithAction.count)
    }
    
    func testSetupModel_WhenItemHasNotActionAndActionsAreEnabled_ShouldNotDisplayActionIndicator() {
        featureManagerMock.override(key: .isStatementDetailEnabled, with: true)
        
        let itensWithoutAction: [StatementItem] = [
            mockItem(type: .cashback),
            mockItem(type: .cashin),
            mockItem(type: .withdraw),
            mockItem(type: .generic),
            mockItem(type: .accountAdjustment),
            mockItem(type: .b2p),
            mockItem(type: .fees),
            mockItem(type: .loan),
            mockItem(type: .store),
            mockItem(type: .personalLoan),
            mockItem(type: .pix),
            mockItem(type: .p2p, flow: .inflow)
        ]
        
        itensWithoutAction.forEach {
            sut.setup(model: $0)
            XCTAssertFalse(viewSpy.actionIndicator)
        }
        
        XCTAssertEqual(viewSpy.displayOperationDetailItemsCallsCount, itensWithoutAction.count)
    }
    
    func testSetupModel_WhenImageUrlIsNil_ShouldOnlyDisplayItemPlaceholdIcon() {
        let mockedModel = mockItem(imageUrl: nil)
        
        sut.setup(model: mockedModel)
        
        XCTAssertEqual(viewSpy.displayItemPlaceholderIconCallsCount, 1)
        XCTAssertEqual(viewSpy.displayItemImageCallsCount, 0)
    }
    
    func testSetupModel_WhenImageUrlIsInvalid_ShouldOnlyDisplayItemPlaceholdIcon() {
        let mockedModel = mockItem(imageUrl: "")
        
        sut.setup(model: mockedModel)
        
        XCTAssertEqual(viewSpy.displayItemPlaceholderIconCallsCount, 1)
        XCTAssertEqual(viewSpy.displayItemImageCallsCount, 0)
    }
    
    func testSetupModel_WhenImageUrlIsValid_ShouldOnlyDisplayItemPlaceholdIconAndImage() {
        let mockedModel = mockItem(imageUrl:
            "https://picpay-dev.s3.amazonaws.com/profiles-processed-images/bad46633784b40b9e0f74c1c496ee599.200.jpg"
        )
        
        sut.setup(model: mockedModel)
        
        XCTAssertEqual(viewSpy.displayItemPlaceholderIconCallsCount, 1)
        XCTAssertEqual(viewSpy.displayItemImageCallsCount, 1)
    }
    
    func testSetupModel_WhenItemTypeIsCashback_ShouldDisplayCashbackIcon() {
        let mockedModel = mockItem(type: .cashback)
        
        sut.setup(model: mockedModel)
        
        XCTAssertEqual(viewSpy.icon, .usdCircle)
    }
    
    func testSetupModel_WhenItemTypeIsIncome_ShouldDisplayIncomeIcon() {
        let mockedModel = mockItem(type: .income)
        
        sut.setup(model: mockedModel)
        
        XCTAssertEqual(viewSpy.icon, .arrowGrowth)
    }
    
    func testSetupModel_WhenItemTypeIsPhoneRecharge_ShouldDisplayPhoneRechargeIcon() {
        let mockedModel = mockItem(type: .phoneRecharge)
        
        sut.setup(model: mockedModel)
        
        XCTAssertEqual(viewSpy.icon, .mobileAndroid)
    }
    
    func testSetupModel_WhenItemTypeIsWithdraw_ShouldDisplayWithdrawIcon() {
        let mockedModel = mockItem(type: .withdraw)
        
        sut.setup(model: mockedModel)
        
        XCTAssertEqual(viewSpy.icon, .moneyWithdrawal)
    }
    
    func testSetupModel_WhenItemTypeIsCashout_ShouldDisplayCashoutIcon() {
        let mockedModel = mockItem(type: .cashout)
        
        sut.setup(model: mockedModel)
        
        XCTAssertEqual(viewSpy.icon, .moneyInsert)
    }
    
    func testSetupModel_WhenItemTypeIsPav_ShouldDisplayPavIcon() {
        let mockedModel = mockItem(type: .pav)
        
        sut.setup(model: mockedModel)
        
        XCTAssertEqual(viewSpy.icon, .store)
    }
    
    func testSetupModel_WhenItemTypeIsDigitalGoods_ShouldDisplayDigitalGoodsIcon() {
        let mockedModel = mockItem(type: .digitalGoods)
        
        sut.setup(model: mockedModel)
        
        XCTAssertEqual(viewSpy.icon, .shoppingBag)
    }
    
    func testSetupModel_WhenItemTypeIsStore_ShouldDisplayStoreIcon() {
        let mockedModel = mockItem(type: .store)
        
        sut.setup(model: mockedModel)
        
        XCTAssertEqual(viewSpy.icon, .shoppingBag)
    }
    
    func testSetupModel_WhenItemTypeIsP2pLendingAndIsInflow_ShouldDisplayP2pLendingInflowIcon() {
        let mockedModel = mockItem(type: .p2pLending, flow: .inflow)
        
        sut.setup(model: mockedModel)
        
        XCTAssertEqual(viewSpy.icon, .moneyWithdraw)
    }
    
    func testSetupModel_WhenItemTypeIsP2pLendingAndIOutflow_ShouldDisplayP2pLendingOutflowIcon() {
        let mockedModel = mockItem(type: .p2pLending, flow: .outflow)
        
        sut.setup(model: mockedModel)
        
        XCTAssertEqual(viewSpy.icon, .moneyInsert)
    }
    
    func testSetupModel_WhenItemTypeIsMobileTickets_ShouldDisplayMobileTicketsIcon() {
        let mockedModel = mockItem(type: .mobileTickets)
        
        sut.setup(model: mockedModel)
        
        XCTAssertEqual(viewSpy.icon, .bus)
    }
    
    func testSetupModel_WhenItemTypeIsLoan_ShouldDisplayLoanIcon() {
        let mockedModel = mockItem(type: .loan)
        
        sut.setup(model: mockedModel)
        
        XCTAssertEqual(viewSpy.icon, .moneyWithdraw)
    }
    
    func testSetupModel_WhenItemTypeIsPersonalLoan_ShouldDisplayPersonalLoanIcon() {
        let mockedModel = mockItem(type: .personalLoan)
        
        sut.setup(model: mockedModel)
        
        XCTAssertEqual(viewSpy.icon, .moneyWithdraw)
    }
    
    func testSetupModel_WhenItemTypeIsCashin_ShouldDisplayCashinIcon() {
        let mockedModel = mockItem(type: .cashin)
        
        sut.setup(model: mockedModel)
        
        XCTAssertEqual(viewSpy.icon, .moneyWithdraw)
    }
    
    func testSetupModel_WhenItemTypeIsP2pAndFlowIsInflow_ShouldDisplayP2pInflowIcon() {
        let mockedModel = mockItem(type: .p2p, flow: .inflow)
        
        sut.setup(model: mockedModel)
        
        XCTAssertEqual(viewSpy.icon, .moneyWithdraw)
    }
    
    func testSetupModel_WhenItemTypeIsP2pAndFlowIsOutflow_ShouldDisplayOutflowIcon() {
        let mockedModel = mockItem(type: .p2p, flow: .outflow)
        
        sut.setup(model: mockedModel)
        
        XCTAssertEqual(viewSpy.icon, .moneyInsert)
    }
    
    func testSetupModel_WhenItemTypeIsPix_ShouldDisplayPixIcon() {
        let mockedModel = mockItem(type: .pix)
        
        sut.setup(model: mockedModel)
        
        XCTAssertEqual(viewSpy.icon, .receiptAlt)
    }
    
    func testSetupModel_WhenItemTypeIsFees_ShouldDisplayFeesIcon() {
        let mockedModel = mockItem(type: .fees)
        
        sut.setup(model: mockedModel)
        
        XCTAssertEqual(viewSpy.icon, .percentage)
    }
    
    func testSetupModel_WhenItemTypeIsAccountAdjustments_ShouldDisplayAccountAdjustmentsIcon() {
        let mockedModel = mockItem(type: .accountAdjustment)
        
        sut.setup(model: mockedModel)
        
        XCTAssertEqual(viewSpy.icon, .cornerUpLeft)
    }
    
    func testSetupModel_WhenItemTypeIsPicPayCard_ShouldDisplayPicPayCardIcon() {
        let mockedModel = mockItem(type: .picpayCard)
        
        sut.setup(model: mockedModel)
        
        XCTAssertEqual(viewSpy.icon, .creditCard)
    }
    
    func testSetupModel_WhenItemTypeIsGeneric_ShouldDisplayGenericIcon() {
        let mockedModel = mockItem(type: .generic)
        
        sut.setup(model: mockedModel)
        
        XCTAssertEqual(viewSpy.icon, .receiptAlt)
    }
    
    func testSetupModel_ShouldDisplayCorrectAccessibilityLabel() {
        let mockedModel = mockItem(type: .p2p)
        
        sut.setup(model: mockedModel)
        
        XCTAssertEqual(viewSpy.setupAccessibilityLabelCallsCount, 1)
        let expectedAccessibilityText = "Pagamento enviado, doze horas e cinquenta e um minutos, Saldo -R$ 10,00 Cartão Master 9821 -R$ 50,00 Total -R$ 150,00 "
        XCTAssertEqual(viewSpy.accessibilityText, expectedAccessibilityText)
    }
}
