import AnalyticsModule
import Core
import FeatureFlag
@testable import Statement
import UI
import XCTest

final class StatementListServiceMock: StatementListServicing {
    var result: Result<StatementList, ApiError>?
    
    private(set) var pageIndex: Int?
    
    func getStatementData(pageIndex: Int, completion: @escaping (Result<StatementList, ApiError>) -> Void) {
        self.pageIndex = pageIndex
        
        guard let result = result else {
            XCTFail("Mocked retult for getStatementData method is nil")
            return
        }
        
        completion(result)
    }
}

final class StatementListPresenterSpy: StatementListPresenting {
    var viewController: StatementListDisplay?
    
    // MARK: - presentNewItems
    private(set) var presentNewItemsCallsCount = 0
    private(set) var presentNewItemsSections = [StatementSection]()
    
    func presentNewItems(fromSections sections: [StatementSection]) {
        presentNewItemsCallsCount += 1
        presentNewItemsSections = sections
    }
    
    // MARK: - updateItems
    private(set) var updateItemsCallCounts = 0
    private(set) var updateItemsSections = [StatementSection]()
    
    func updateItems(fromSections sections: [StatementSection]) {
        updateItemsCallCounts += 1
        updateItemsSections = sections
    }
    
    // MARK: - removeItems
    private(set) var removeItemsCallCounts = 0
    private(set) var removeItemsSections = [StatementSection]()
    
    func removeItems(fromSections sections: [StatementSection]) {
        removeItemsCallCounts += 1
        removeItemsSections = sections
    }
    
    // MARK: - didNextStep
    private(set) var didNextStepCallCounts = 0
    private(set) var didNextStepAction: StatementListAction?
    
    func didNextStep(action: StatementListAction) {
        didNextStepCallCounts += 1
        didNextStepAction = action
    }
    
    // MARK: - showAnimatedSkeletonView
    private(set) var showAnimatedSkeletonViewCallsCount = 0
    
    func showAnimatedSkeletonView() {
        showAnimatedSkeletonViewCallsCount += 1
    }

    // MARK: - hideAnimatedSkeletonView
    private(set) var hideAnimatedSkeletonViewCallsCount = 0
    
    func hideAnimatedSkeletonView() {
        hideAnimatedSkeletonViewCallsCount += 1
    }
    
    // MARK: - hideRefreshControl
    private(set) var hideRefreshControlCallsCount = 0
    
    func hideRefreshControl() {
        hideRefreshControlCallsCount += 1
    }
    
    // MARK: - presentPaginationState
    private(set) var presentPaginationStateCallsCount = 0
    private(set) var paginationState = [StatementListVisiblePaginationState]()
    
    func presentPaginationState(_ state: StatementListVisiblePaginationState) {
        presentPaginationStateCallsCount += 1
        paginationState.append(state)
    }
  
    // MARK: - presentConnectionFailureError
    private(set) var presentConnectionFailureErrorCallsCount = 0
    
    func presentConnectionFailureError() {
        presentConnectionFailureErrorCallsCount += 1
    }
    
    // MARK: - presentGenericError
    private(set) var presentGenericErrorCallsCount = 0
    
    func presentGenericError() {
        presentGenericErrorCallsCount += 1
    }
    
    // MARK: - removeStatusViewIfNeeded
    private(set) var removeStatusViewIfNeededCallsCount = 0
    
    func removeStatusViewIfNeeded() {
        removeStatusViewIfNeededCallsCount += 1
    }
    
    // MARK: - presentEmptyView
    private(set) var presentEmptyViewCallsCount = 0
    
    func presentEmptyView() {
        presentEmptyViewCallsCount += 1
    }
}

final class StatementListInteractorTests: XCTestCase {
    private var serviceMock = StatementListServiceMock()
    private var presenterSpy = StatementListPresenterSpy()
    private let analyticsSpy = AnalyticsSpy()
    private let featureManagerMock = FeatureManagerMock()
    private lazy var sut: StatementListInteracting = StatementListInteractor(
        service: serviceMock,
        presenter: presenterSpy,
        dependencies: DependencyContainerMock(analyticsSpy, featureManagerMock)
    )

    func testFetchFirstPage_WhenSuccessResponse_ShouldPresentNewItems() {
        let mock = StatementListMock.mock()
        serviceMock.result = .success(mock)
        sut.fetchFirstPage()
        
        XCTAssertEqual(presenterSpy.presentNewItemsCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentNewItemsSections, mock.statement)
        XCTAssertEqual(presenterSpy.removeStatusViewIfNeededCallsCount, 1)
    }
    
    func testFetchFirstPage_WhenSuccessResponse_ShouldShowAndHideSkeletonView() {
        serviceMock.result = .success(StatementListMock.mock())
        sut.fetchFirstPage()
        
        XCTAssertEqual(presenterSpy.showAnimatedSkeletonViewCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideAnimatedSkeletonViewCallsCount, 1)
        XCTAssertEqual(presenterSpy.removeStatusViewIfNeededCallsCount, 1)
    }
    
    func testFetchFirstPage_WhenSuccessResponseButResultIsEmpty_ShouldPresentEmptyView() {
        let expectedEventPaginationLoaded = StatementEvent.paginationLoaded(tab: .all, page: 0).event()
        let expectedEventNoTransactions = StatementEvent.error(errorType: .noTransactions).event()
        
        serviceMock.result = .success(StatementListMock.mock(isEmpty: true))
        sut.fetchFirstPage()
        
        XCTAssertEqual(presenterSpy.presentEmptyViewCallsCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEventNoTransactions, expectedEventPaginationLoaded))
    }
    
    func testFetchFirstPage_WhenFailureResponse_ShouldShowAndHideSkeletonView() {
        serviceMock.result = .failure(.connectionFailure)
        sut.fetchFirstPage()
        
        XCTAssertEqual(presenterSpy.showAnimatedSkeletonViewCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideAnimatedSkeletonViewCallsCount, 1)
        XCTAssertEqual(presenterSpy.removeStatusViewIfNeededCallsCount, 1)
    }
    
    func testFetchStatementData_WhenConnectionFailureResponse_ShouldConnectionFailureError() {
        serviceMock.result = .failure(.connectionFailure)
        sut.fetchFirstPage()
        
        XCTAssertEqual(presenterSpy.removeStatusViewIfNeededCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentConnectionFailureErrorCallsCount, 1)
    }
    
    func testFetchStatementData_WhenGenericFailureResponse_ShouldPresentGenericError() {
        serviceMock.result = .failure(.badRequest(body: RequestError()))
        sut.fetchFirstPage()
        
        XCTAssertEqual(presenterSpy.removeStatusViewIfNeededCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentGenericErrorCallsCount, 1)
    }
    
    func testFetchNextPage_WhenSuccessResponse_ShouldPresentLoadingPaginationState() {
        let morePage = StatementListMock.mock(isLastPage: true, sections: StatementListMock.sectionsNewPage)
        serviceMock.result = .success(morePage)
        sut.fetchNextPage()
        
        XCTAssertEqual(presenterSpy.presentPaginationStateCallsCount, 1)
        XCTAssertEqual(presenterSpy.paginationState.last, .loading)
    }
    
    func testFetchNextPage_WhenFailureResponse_ShouldPresentLoadingAndFalingPaginationState() {
        serviceMock.result = .failure(.connectionFailure)
        sut.fetchNextPage()
        
        XCTAssertEqual(presenterSpy.presentPaginationStateCallsCount, 2)
        XCTAssertEqual(presenterSpy.paginationState, [.loading, .failure])
    }
    
    func testFetchMoreItemsIfNeeded_WhenNoMorePageAndIsLastCell_ShouldDoNotUpdate() {
        let mock = StatementListMock.mock(isLastPage: true)
        serviceMock.result = .success(mock)
        sut.fetchFirstPage()
        
        sut.fetchMoreItemsIfNeeded(IndexPath(item: 0, section: 1))
        
        XCTAssertEqual(presenterSpy.presentNewItemsCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentNewItemsSections, mock.statement)
        XCTAssertEqual(presenterSpy.updateItemsCallCounts, 0)
        XCTAssertEqual(presenterSpy.updateItemsSections, [])
    }
    
    func testFetchMoreItemsIfNeeded_WhenHaveMorePageAndIsLastCell_ShouldPresentNewItems() {
        let expectedEvent = StatementEvent.paginationLoaded(tab: .all, page: 0).event()
        let mock = StatementListMock.mock(isLastPage: false)
        serviceMock.result = .success(mock)
        sut.fetchFirstPage()
        
        let morePage = StatementListMock.mock(isLastPage: true, sections: StatementListMock.sectionsNewPage)
        serviceMock.result = .success(morePage)
        sut.fetchMoreItemsIfNeeded(IndexPath(item: 0, section: 1))
        
        XCTAssertEqual(presenterSpy.presentNewItemsCallsCount, 2)
        XCTAssertEqual(presenterSpy.presentNewItemsSections, morePage.statement)
        XCTAssertEqual(presenterSpy.updateItemsCallCounts, 0)
        XCTAssertEqual(presenterSpy.updateItemsSections, [])
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testFetchMoreItemsIfNeeded_WhenHaveMorePageAndIsLastCellAndMergeGroupSection_ShouldPresentNewItemsAndUpdateItems() throws {
        let expectedEvent = StatementEvent.paginationLoaded(tab: .all, page: 0).event()
        let mock = StatementListMock.mock(isLastPage: false)
        serviceMock.result = .success(mock)
        sut.fetchFirstPage()
        
        let morePage = StatementListMock.mock(isLastPage: true, sections: StatementListMock.sectionsNewPageMergeGroup)
        serviceMock.result = .success(morePage)
        sut.fetchMoreItemsIfNeeded(IndexPath(item: 0, section: 1))
        
        let newItems = [try XCTUnwrap(morePage.statement.last)]
        let updatedItems = [
            StatementSection(header: StatementHeader(date: "15/Nov"), statementItems: [
                try XCTUnwrap(mock.statement.last?.statementItems.first),
                try XCTUnwrap(morePage.statement.first?.statementItems.first)
            ])
        ]
        
        XCTAssertEqual(presenterSpy.presentNewItemsCallsCount, 2)
        XCTAssertEqual(presenterSpy.presentNewItemsSections, newItems)
        XCTAssertEqual(presenterSpy.updateItemsCallCounts, 1)
        XCTAssertEqual(presenterSpy.updateItemsSections, updatedItems)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testFetchMoreItemsIfNeeded_WhenIsNotLastCell_ShouldDoNothing() {
        let mock = StatementListMock.mock(isLastPage: false)
        serviceMock.result = .success(mock)
        sut.fetchFirstPage()
        
        sut.fetchMoreItemsIfNeeded(IndexPath(item: 1, section: 0))
        
        XCTAssertEqual(presenterSpy.presentNewItemsCallsCount, 1)
        XCTAssertEqual(presenterSpy.updateItemsCallCounts, 0)
        XCTAssertEqual(presenterSpy.presentPaginationStateCallsCount, 0)
    }
    
    func testFetchMoreItemsIfNeeded_WhenNoMorePageAndIsLastCell_ShouldPresentEndPaginationState() {
        let mock = StatementListMock.mock(isLastPage: true)
        serviceMock.result = .success(mock)
        sut.fetchFirstPage()
        
        sut.fetchMoreItemsIfNeeded(IndexPath(item: 0, section: 1))
        
        XCTAssertEqual(presenterSpy.presentPaginationStateCallsCount, 1)
        XCTAssertEqual(presenterSpy.paginationState.last, .end)
    }
    
    func testDidSelectItem_ShouldTrackInformations() throws {
        featureManagerMock.override(key: .isStatementDetailEnabled, with: true)
        let item = try XCTUnwrap(StatementListMock.mock().statement.first?.statementItems.first)
        let expectedEvent = StatementEvent.itemSelected(interaction: .card, itemType: item.type, transactionStatus: item.status).event()
        
        sut.didSelectItem(item)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testDidSelectItem_WhenItemOpenAPavReceiptType_ShouldCallDidNextStep() {
        featureManagerMock.override(key: .isStatementDetailEnabled, with: true)
        let items: [StatementItem] = [
            StatementListMock.item(withType: .pav),
            StatementListMock.item(withType: .phoneRecharge),
            StatementListMock.item(withType: .digitalGoods),
            StatementListMock.item(withType: .picpayCard),
        ]
        
        items.forEach {
            sut.didSelectItem($0)
            XCTAssertEqual(presenterSpy.didNextStepAction, .openReceipt(transactionId: "1234", receiptType: "pav"))
        }
        
        XCTAssertEqual(presenterSpy.didNextStepCallCounts, 4)
    }
    
    func testDidSelectItem_WhenItemHasActionAssociatedButHasNoTransactionId_ShouldCallDidNextStep() {
        featureManagerMock.override(key: .isStatementDetailEnabled, with: true)
        let item = StatementListMock.item(withType: .pav, transactionId: nil)
        
        sut.didSelectItem(item)
        
        XCTAssertEqual(presenterSpy.didNextStepCallCounts, 0)
        XCTAssertNil(presenterSpy.didNextStepAction)
    }
    
    func testDidSelectItem_WhenItemIsPhoneRecharge_ShouldCallDidNextStep() {
        featureManagerMock.override(key: .isStatementDetailEnabled, with: true)
        let item = StatementListMock.item(withType: .phoneRecharge)
        
        sut.didSelectItem(item)
        
        XCTAssertEqual(presenterSpy.didNextStepCallCounts, 1)
        XCTAssertEqual(presenterSpy.didNextStepAction, .openReceipt(transactionId: "1234", receiptType: "pav"))
    }
    
    func testDidSelectItem_WhenItemIsP2PSent_ShouldCallDidNextStep() {
        featureManagerMock.override(key: .isStatementDetailEnabled, with: true)
        let item = StatementListMock.item(withType: .p2p, flow: .outflow)
        
        sut.didSelectItem(item)
        
        XCTAssertEqual(presenterSpy.didNextStepCallCounts, 1)
        XCTAssertEqual(presenterSpy.didNextStepAction, .openReceipt(transactionId: "1234", receiptType: "p2p"))
    }
    
    func testDidSelectItem_WhenItemIsP2pLending_ShouldCallDidNextStep() {
        featureManagerMock.override(key: .isStatementDetailEnabled, with: true)
        let item = StatementListMock.item(withType: .p2pLending)
        
        sut.didSelectItem(item)
        
        XCTAssertEqual(presenterSpy.didNextStepCallCounts, 1)
        XCTAssertEqual(presenterSpy.didNextStepAction, .openReceipt(transactionId: "1234", receiptType: "p2pLending"))
    }
    
    func testDidSelectItem_WhenItemIsMobileTickets_ShouldNotCallDidNextStep() {
        featureManagerMock.override(key: .isStatementDetailEnabled, with: true)
        let item = StatementListMock.item(withType: .mobileTickets)
        
        sut.didSelectItem(item)
        
        XCTAssertEqual(presenterSpy.didNextStepCallCounts, 1)
        XCTAssertEqual(presenterSpy.didNextStepAction, .openReceipt(transactionId: "1234", receiptType: "pav"))
    }
    
    func testDidSelectItem_WhenItemIsCashout_ShouldCallDidNextStep() {
        featureManagerMock.override(key: .isStatementDetailEnabled, with: true)
        let item = StatementListMock.item(withType: .cashout)
        
        sut.didSelectItem(item)
        
        XCTAssertEqual(presenterSpy.didNextStepCallCounts, 1)
        XCTAssertEqual(presenterSpy.didNextStepAction, .openCashout(transactionId: "1234"))
    }
    
    func testDidSelectItem_WhenItemIsIncome_ShouldCallDidNextStep() {
        featureManagerMock.override(key: .isStatementDetailEnabled, with: true)
        let item = StatementListMock.item(withType: .income)
        
        sut.didSelectItem(item)
        
        XCTAssertEqual(presenterSpy.didNextStepCallCounts, 1)
        XCTAssertEqual(presenterSpy.didNextStepAction, .openIncome)
    }
    
    func testDidSelectItem_WhenItemDoesNotHaveAction_ShouldNotCallDidNextStep() {
        featureManagerMock.override(key: .isStatementDetailEnabled, with: true)
        let itemsWithoutAction: [StatementItem] = [
            StatementListMock.item(withType: .generic),
            StatementListMock.item(withType: .cashback),
            StatementListMock.item(withType: .cashin),
            StatementListMock.item(withType: .withdraw),
            StatementListMock.item(withType: .accountAdjustment),
            StatementListMock.item(withType: .b2p),
            StatementListMock.item(withType: .fees),
            StatementListMock.item(withType: .loan),
            StatementListMock.item(withType: .store),
            StatementListMock.item(withType: .personalLoan),
            StatementListMock.item(withType: .pix),
            StatementListMock.item(withType: .p2p, flow: .inflow),
            StatementListMock.item(withType: .generic, status: .canceled)
        ]
        
        itemsWithoutAction.forEach {
            sut.didSelectItem($0)
        }
        
        XCTAssertEqual(presenterSpy.didNextStepCallCounts, 0)
        XCTAssertNil(presenterSpy.didNextStepAction)
    }
    
    func testDidSelectItem_WhenFlagIsStatementDetailEnabledIsFalse_ShouldNotCallDidNextStep() {
        featureManagerMock.override(key: .isStatementDetailEnabled, with: false)
        let item = StatementListMock.item(withType: .pav)
        
        sut.didSelectItem(item)
        
        XCTAssertEqual(presenterSpy.didNextStepCallCounts, 0)
        XCTAssertNil(presenterSpy.didNextStepAction)
    }
    
    func testRefreshItems_WhenSuccessResponse_ShouldPresentNewItemsSection() {
        let mock = StatementListMock.mock(isLastPage: false)
        serviceMock.result = .success(mock)
        
        sut.fetchFirstPage()
        sut.refreshItems()
        
        XCTAssertEqual(presenterSpy.presentNewItemsCallsCount, 2)
        XCTAssertEqual(presenterSpy.hideRefreshControlCallsCount, 1)
        XCTAssertEqual(presenterSpy.removeItemsCallCounts, 1)
        XCTAssertEqual(presenterSpy.removeItemsSections, mock.statement)
        XCTAssertEqual(presenterSpy.presentNewItemsSections, presenterSpy.removeItemsSections)
    }
    
    func testRefreshItems_WhenGenericFailureResponse_ShouldPresentGenericError() {
        let mock = StatementListMock.mock(isLastPage: false)
        
        serviceMock.result = .success(mock)
        sut.fetchFirstPage()
        
        serviceMock.result = .failure(.badRequest(body: RequestError()))
        sut.refreshItems()
        
        XCTAssertEqual(presenterSpy.presentNewItemsCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideRefreshControlCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentGenericErrorCallsCount, 1)
    }
}
