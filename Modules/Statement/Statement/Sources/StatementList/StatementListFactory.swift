import UIKit

enum StatementListFactory {
    static func make() -> StatementListViewController {
        let container = DependencyContainer()
        let service: StatementListServicing = StatementListService(dependencies: container)
        let coordinator: StatementListCoordinating = StatementListCoordinator()
        let presenter: StatementListPresenting = StatementListPresenter(coordinator: coordinator)
        let interactor = StatementListInteractor(
            service: service,
            presenter: presenter,
            dependencies: container
        )
        let viewController = StatementListViewController(interactor: interactor)

        coordinator.viewController = viewController
        coordinator.incomeContract = StatementSetup.income
        coordinator.receiptContract = StatementSetup.receipt
        coordinator.wireTransferReceiptContract = StatementSetup.wireTransferReceipt
        presenter.viewController = viewController

        return viewController
    }
}
