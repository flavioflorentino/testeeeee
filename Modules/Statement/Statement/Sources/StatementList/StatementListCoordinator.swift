import Cashout
import Core
import Foundation
import UIKit

enum StatementListAction: Equatable {
    case openIncome
    case openReceipt(transactionId: String, receiptType: String)
    case openCashin(transactionId: String)
    case openCashout(transactionId: String)
}

protocol StatementListCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var receiptContract: ReceiptContract? { get set }
    var incomeContract: IncomeContract? { get set }
    var wireTransferReceiptContract: WireTransferReceiptContract? { get set }
    func perform(action: StatementListAction)
}

final class StatementListCoordinator {
    weak var viewController: UIViewController?
    var receiptContract: ReceiptContract?
    var incomeContract: IncomeContract?
    var wireTransferReceiptContract: WireTransferReceiptContract?
}

// MARK: - StatementListCoordinating
extension StatementListCoordinator: StatementListCoordinating {
    func perform(action: StatementListAction) {
        switch action {
        case .openIncome:
            incomeContract?.open()
        case let .openReceipt(transactionId, receiptType):
            receiptContract?.open(withId: transactionId, receiptType: receiptType)
        case let .openCashin(transactionId):
            wireTransferReceiptContract?.open(withId: transactionId)
        case let .openCashout(transactionId):
            let target = WithdrawReceiptFactory.make(receiptId: transactionId)
            viewController?.present(target, animated: true)
        }
    }
}
