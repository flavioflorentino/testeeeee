import UI
import UIKit

protocol StatementListFooterFailureDelegate: AnyObject {
    func didTapTryAgain()
}

private extension StatementListFooterFailureView.Layout {
    enum Size {
        static let icon = CGSize(width: 20, height: 20)
    }
}

final class StatementListFooterFailureView: UIView, ViewConfiguration {
    fileprivate enum Layout {}

    weak var delegate: StatementListFooterFailureDelegate?
    
    private lazy var containterView: UIView = {
        let view = UIView()
        view.viewStyle(RoundedViewStyle(cornerRadius: .light))
            .with(\.backgroundColor, .critical600())
        view.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapContainer))
        view.addGestureRecognizer(tapGesture)
        return view
    }()
    
    private lazy var iconImageView = UIImageView(image: Assets.icoExclamationCircle.image)
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
        .with(\.textColor, .white())
        let attributedTitle = NSMutableAttributedString(string: Strings.StatementList.Footer.failureMessage)
        let underlinedRange = attributedTitle.mutableString.range(of: Strings.StatementList.Footer.failureMessageUnderlined)
        attributedTitle.addAttribute(.underlineStyle, value: 1, range: underlinedRange)
        label.attributedText = attributedTitle
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        containterView.addSubview(iconImageView)
        containterView.addSubview(messageLabel)
        addSubview(containterView)
    }
    
    func setupConstraints() {
        containterView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.top.bottom.equalToSuperview().inset(Spacing.base01)
        }
        
        iconImageView.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.size.equalTo(Layout.Size.icon)
            $0.centerY.equalToSuperview()
            $0.top.greaterThanOrEqualToSuperview().inset(Spacing.base02)
            $0.bottom.lessThanOrEqualToSuperview().inset(Spacing.base02)
        }
        
        messageLabel.snp.makeConstraints {
            $0.top.bottom.trailing.equalToSuperview().inset(Spacing.base02)
            $0.leading.equalTo(iconImageView.snp.trailing).offset(Spacing.base02)
        }
    }
    
    func configureViews() {
        viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }
    
    @objc
    private func didTapContainer() {
        delegate?.didTapTryAgain()
    }
}
