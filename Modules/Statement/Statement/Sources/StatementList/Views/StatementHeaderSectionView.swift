import SnapKit
import UI
import UIKit

final class StatementHeaderSectionView: UITableViewHeaderFooterView {
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
        return label
    }()
    
    var sectionTitle: String? {
        didSet {
            titleLabel.text = sectionTitle
            titleLabel.accessibilityLabel = formatDateAccessibilityText(from: sectionTitle)
        }
    }
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func formatDateAccessibilityText(from input: String?) -> String? {
        let dateFormater = DateFormatter()
        dateFormater.locale = Locale(identifier: "pt_BR")
        dateFormater.dateFormat = "d MMM/yy"
        guard let inputText = input, let date = dateFormater.date(from: inputText) else {
            return input
        }
        return DateFormatter.localizedString(from: date, dateStyle: .medium, timeStyle: .none)
    }
}

extension StatementHeaderSectionView: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(titleLabel)
        contentView.backgroundColor = Colors.backgroundPrimary.color
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
}
