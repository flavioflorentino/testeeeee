import SnapKit
import UI
import UIKit

protocol StatementStatusViewDelegate: AnyObject {
    func didTapActionButton()
}

final class StatementStatusView: UIView {
    private let title: String
    private let text: String
    private let buttonText: String?
    private let image: UIImage
    
    private lazy var containerImageView = UIView()
    
    weak var delegate: StatementStatusViewDelegate?
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = title
        label
            .labelStyle(TitleLabelStyle(type: .large))
            .with(\.textColor, .black())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var textLabel: UILabel = {
        let label = UILabel()
        label.text = text
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var actionButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(buttonText, for: .normal)
        button.isHidden = buttonText == nil
        button.addTarget(self, action: #selector(didTapActionButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        imageView.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
        return imageView
    }()
    
    init(image: UIImage, title: String, text: String, buttonText: String? = nil) {
        self.image = image
        self.title = title
        self.text = text
        self.buttonText = buttonText
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    private func didTapActionButton() {
        delegate?.didTapActionButton()
    }
}

extension StatementStatusView: ViewConfiguration {
    func buildViewHierarchy() {
        containerImageView.addSubview(imageView)
        addSubview(containerImageView)
        addSubview(titleLabel)
        addSubview(textLabel)
        addSubview(actionButton)
    }
    
    func setupConstraints() {
        containerImageView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview().inset(Spacing.base08)
        }
        
        imageView.snp.makeConstraints {
            $0.centerX.centerY.equalToSuperview()
            $0.height.lessThanOrEqualToSuperview()
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(containerImageView.snp.bottom).offset(Spacing.base06)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        textLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(actionButton.snp.top).offset(-Spacing.base04)
        }
        
        actionButton.snp.makeConstraints {
            $0.bottom.equalToSuperview().offset(-Spacing.base05)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
}
