import SnapKit
import UI
import UIKit

private extension StatementListWelcomeView.Layout {
    static let iconSize = CGSize(width: 17, height: 17)
    static let lineWidth: CGFloat = 1
}

final class StatementListWelcomeView: UIView {
    private let title = Strings.StatementList.StatementListWelcome.title
    private let message = Strings.StatementList.StatementListWelcome.description
    
    fileprivate enum Layout {}
    
    private lazy var containerView = UIView()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.viewStyle(BackgroundViewStyle(color: .grayscale200()))
        return view
    }()
    
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView(image: Assets.icoStatementWelcome.image)
        imageView
            .imageStyle(RoundedImageStyle(size: .xSmall, cornerRadius: .full))
            .with(\.border, .light(color: .grayscale200()))
            .with(\.backgroundColor, .backgroundPrimary())
        imageView.contentMode = .center
        imageView.accessibilityElementsHidden = true
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
        label.text = title
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale500())
        label.text = message
        return label
    }()
    
    init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension StatementListWelcomeView: ViewConfiguration {
    func buildViewHierarchy() {
        containerView.addSubview(iconImageView)
        containerView.addSubview(titleLabel)
        containerView.addSubview(descriptionLabel)
        
        addSubview(lineView)
        addSubview(containerView)
    }
    
    func setupConstraints() {
        lineView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.bottom.equalTo(iconImageView.snp.centerY)
            $0.centerX.equalTo(iconImageView)
            $0.width.equalTo(1)
        }
        
        containerView.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(Spacing.base02)
        }
        
        iconImageView.snp.makeConstraints {
            $0.top.leading.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.trailing.equalToSuperview()
            $0.leading.equalTo(iconImageView.snp.trailing).offset(Spacing.base01)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalTo(titleLabel)
            $0.bottom.equalToSuperview()
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
        isAccessibilityElement = true
        accessibilityLabel = title + ", " + message
    }
}
