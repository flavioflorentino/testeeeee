import SnapKit
import UI
import UIKit

final class StatementOperationDetailView: UIStackView {
    private let title: String
    private let value: String
    private let style: OperationDetail.Style
    private let isCanceled: Bool
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        let attributes: [NSAttributedString.Key: Any] = isCanceled ? [.strikethroughStyle: 1] : [:]
        label.attributedText = NSAttributedString(string: title, attributes: attributes)
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale500())
        return label
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        let attributes: [NSAttributedString.Key: Any] = isCanceled ? [.strikethroughStyle: 1] : [:]
        label.attributedText = NSAttributedString(string: value, attributes: attributes)
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, style.color)
            .with(\.textAlignment, .right)
        label.setContentHuggingPriority(.required, for: .horizontal)
        label.setContentHuggingPriority(.required, for: .vertical)
        return label
    }()
    
    init(operationDetail: OperationDetail, isCanceled: Bool = false) {
        title = operationDetail.description
        value = operationDetail.value
        style = operationDetail.style
        self.isCanceled = isCanceled
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension StatementOperationDetailView: ViewConfiguration {
    func setupConstraints() {}
    
    func buildViewHierarchy() {
        addArrangedSubview(descriptionLabel)
        addArrangedSubview(valueLabel)
    }
    
    func configureViews() {
        spacing = Spacing.base01
        shouldGroupAccessibilityChildren = true
    }
}

private extension OperationDetail.Style {
    var color: Colors.Style {
        switch self {
        case .credit, .income:
            return .branding600()
        case .debit:
            return .critical900()
        case .default:
            return .grayscale500()
        }
    }
}
