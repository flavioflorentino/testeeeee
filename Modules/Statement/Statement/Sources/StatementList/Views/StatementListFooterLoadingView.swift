import UI
import UIKit

private extension StatementListFooterLoadingView.Layout {
    enum Size {
        static let activityIndicator = CGSize(width: 32, height: 32)
    }
}

final class StatementListFooterLoadingView: UIView, ViewConfiguration {
    fileprivate enum Layout {}
    
    private lazy var activityIndicatorView: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView(style: .gray)
        view.color = Colors.branding400.color
        return view
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale500())
        label.text = Strings.StatementList.Footer.loadingMessage
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubview(activityIndicatorView)
        addSubview(messageLabel)
    }
    
    func setupConstraints() {
        activityIndicatorView.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.top.bottom.equalToSuperview().inset(Spacing.base03)
            $0.size.equalTo(Layout.Size.activityIndicator)
        }
        
        messageLabel.snp.makeConstraints {
            $0.leading.equalTo(activityIndicatorView.snp.trailing).offset(Spacing.base01)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.centerY.equalTo(activityIndicatorView)
        }
    }
    
    func configureViews() {
        viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        activityIndicatorView.startAnimating()
    }
}
