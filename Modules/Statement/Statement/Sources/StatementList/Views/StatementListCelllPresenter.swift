import FeatureFlag
import UI

protocol StatementListCellPresenting: AnyObject {
    var model: StatementItem? { get }
    var view: StatementListCellDisplay? { get set }
    func setup(model: StatementItem)
}

final class StatementListCellPresenter: StatementListCellPresenting {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    weak var view: StatementListCellDisplay?
    
    private(set) var model: StatementItem?
    
    private var isActionEnabled: Bool {
        dependencies.featureManager.isActive(.isStatementDetailEnabled)
    }
    
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func setup(model: StatementItem) {
        self.model = model
        
        let isCanceled = model.status == .canceled
        view?.displayTime(model.time)
        view?.displayTitle(model.title)
        configureImageView(withUrl: model.image, type: model.type, isCanceled: isCanceled, flow: model.flow)
        configureDescriptionText(model.description)

        view?.displayOperationDetailItems(
            model.operationDetails,
            isCanceled: isCanceled,
            actionIndicator: isActionEnabled && model.action != nil
        )
        
        configureAccessibilityText(for: model)
    }
    
    private func configureDescriptionText(_ description: String?) {
        if let descriptionText = description {
            view?.displayDescription(descriptionText)
        } else {
            view?.hideDescription()
        }
    }
    
    private func configureImageView(
        withUrl imageUrl: String?,
        type: StatementItem.ItemType,
        isCanceled: Bool,
        flow: StatementItem.OperationFlow?
    ) {
        let icon = getIcon(for: type, isCanceled: isCanceled, flow: flow)
        view?.displayItemPlaceholderIcon(from: icon)
        
        guard let urlString = imageUrl, let url = URL(string: urlString) else {
            return
        }
        view?.displayItemImage(from: url)
    }
    
    private func configureAccessibilityText(for model: StatementItem) {
        let timeText = getAccessibilityTimeText(for: model.time)
        var accessibilityText = "\(model.title), \(timeText), "

        if let modelDescription = model.description {
            accessibilityText.append("\(modelDescription), ")
        }
        
        model.operationDetails.forEach { operation in
            accessibilityText.append("\(operation.description) \(operation.value) ")
        }
        view?.setupAccessibilityLabel(with: accessibilityText)
    }
    
    private func getAccessibilityTimeText(for timeString: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        guard let time = dateFormatter.date(from: timeString) else {
            return timeString
        }
        
        var calendar = Calendar.current
        calendar.locale = Locale(identifier: "pt_BR")
        let hourComponents = calendar.dateComponents([.hour, .minute], from: time)
        
        let dateComponentsFormatter = DateComponentsFormatter()
        dateComponentsFormatter.calendar = calendar
        dateComponentsFormatter.unitsStyle = .spellOut
        return dateComponentsFormatter.string(from: hourComponents) ?? timeString
    }
    
    // swiftlint:disable:next cyclomatic_complexity
    private func getIcon(for itemType: StatementItem.ItemType, isCanceled: Bool, flow: StatementItem.OperationFlow?) -> Iconography {
        if isCanceled {
            return .cornerUpLeft
        }
        
        switch itemType {
        case .cashback:
            return .usdCircle
        case .income:
            return .arrowGrowth
        case .phoneRecharge:
            return .mobileAndroid
        case .withdraw:
            return .moneyWithdrawal
        case .cashout:
            return .moneyInsert
        case .pav:
            return .store
        case .digitalGoods, .store:
            return .shoppingBag
        case .mobileTickets:
            return .bus
        case .cashin, .loan, .personalLoan:
            return .moneyWithdraw
        case .p2p, .p2pLending :
            return flow == .inflow ? .moneyWithdraw : .moneyInsert
        case .fees:
            return .percentage
        case .accountAdjustment:
            return .cornerUpLeft
        case .picpayCard:
            return .creditCard
        default:
            return .receiptAlt
        }
    }
}
