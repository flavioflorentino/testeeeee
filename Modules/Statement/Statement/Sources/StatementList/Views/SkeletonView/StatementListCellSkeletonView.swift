import SkeletonView
import SnapKit
import UI

private extension StatementListCellSkeletonView.Layout {
    static let containerViewInset = UIEdgeInsets(top: Spacing.base04,
                                                 left: Spacing.base02,
                                                 bottom: Spacing.base01,
                                                 right: Spacing.base03)
    static let labelWidthMultiplier = 0.7
    static let labelCornerRadius = 8
    enum Width {
        static let line: CGFloat = 1
        static let titleLabel: CGFloat = 100
        static let descriptionLabel: CGFloat = 150
        static let hourLabel: CGFloat = 40
        static let operationLabel: CGFloat = 120
        static let valueLabel: CGFloat = 80
    }
}

final class StatementListCellSkeletonView: UIView {
    fileprivate enum Layout {}
    
    enum CellLayout {
        case compact
        case large
    }
    
    private let cellLayout: CellLayout
    private let nonEmptyString = "-"
    
    private lazy var containerView = UIView()
    private lazy var containerTextsView = UIView()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale200.color
        view.isSkeletonable = true
        return view
    }()
    
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.imageStyle(RoundedImageStyle(size: .xSmall, cornerRadius: .full))
        imageView.isSkeletonable = true
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = nonEmptyString
        label.isSkeletonable = true
        return label
    }()
    
    private lazy var hourLabel: UILabel = {
        let label = UILabel()
        label.text = nonEmptyString
        label.isSkeletonable = true
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = nonEmptyString
        label.isSkeletonable = true
        return label
    }()
    
    private lazy var operationLabel: UILabel = {
        let label = UILabel()
        label.text = nonEmptyString
        label.isSkeletonable = true
        return label
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.text = nonEmptyString
        label.isSkeletonable = true
        return label
    }()
    
    private let gradient = SkeletonGradient(baseColor: Colors.grayscale100.color)
    
    init(_ layout: CellLayout) {
        cellLayout = layout
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        showSkeletonAnimation()
    }
    
    func showSkeletonAnimation() {
        titleLabel.linesCornerRadius = Layout.labelCornerRadius
        titleLabel.showAnimatedGradientSkeleton(usingGradient: gradient)
        
        if cellLayout == .large {
            descriptionLabel.linesCornerRadius = Layout.labelCornerRadius
            descriptionLabel.showAnimatedGradientSkeleton(usingGradient: gradient)
        }
        
        hourLabel.linesCornerRadius = Layout.labelCornerRadius
        hourLabel.showAnimatedGradientSkeleton(usingGradient: gradient)
        
        iconImageView.showAnimatedGradientSkeleton(usingGradient: gradient)
        iconImageView.layer.borderWidth = .zero
        
        lineView.showAnimatedGradientSkeleton(usingGradient: gradient)
        
        operationLabel.linesCornerRadius = Layout.labelCornerRadius
        operationLabel.showAnimatedGradientSkeleton(usingGradient: gradient)
        
        valueLabel.linesCornerRadius = Layout.labelCornerRadius
        valueLabel.showAnimatedGradientSkeleton(usingGradient: gradient)
    }
}

extension StatementListCellSkeletonView: ViewConfiguration {
    func buildViewHierarchy() {
        containerTextsView.addSubview(titleLabel)
        
        if cellLayout == .large {
            containerTextsView.addSubview(descriptionLabel)
        }
        
        containerTextsView.addSubview(hourLabel)
        containerTextsView.addSubview(operationLabel)
        containerTextsView.addSubview(valueLabel)
        
        containerView.addSubview(iconImageView)
        containerView.addSubview(containerTextsView)
        
        addSubview(lineView)
        addSubview(containerView)
    }
    
    func setupConstraints() {
        lineView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.centerX.equalTo(iconImageView)
            $0.width.equalTo(Layout.Width.line)
        }
        
        iconImageView.snp.makeConstraints {
            $0.top.leading.equalToSuperview()
        }
        
        containerTextsView.snp.makeConstraints {
            $0.top.trailing.bottom.equalToSuperview()
            $0.leading.equalTo(iconImageView.snp.trailing).offset(Spacing.base01)
        }
        
        containerView.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(Layout.containerViewInset)
        }
        
        titleLabel.snp.makeConstraints {
            $0.leading.top.equalToSuperview()
            $0.width.equalTo(Layout.Width.titleLabel)
        }
        
        var labelOverOperationLabel = titleLabel
        if cellLayout == .large {
            descriptionLabel.snp.makeConstraints {
                $0.leading.equalToSuperview()
                $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base00)
                $0.width.equalTo(Layout.Width.descriptionLabel)
            }
            labelOverOperationLabel = descriptionLabel
        }

        hourLabel.snp.makeConstraints {
            $0.top.trailing.equalToSuperview()
            $0.width.equalTo(Layout.Width.hourLabel)
        }
        
        operationLabel.snp.makeConstraints {
            $0.leading.bottom.equalToSuperview()
            $0.top.equalTo(labelOverOperationLabel.snp.bottom).offset(Spacing.base01)
            $0.width.equalTo(Layout.Width.operationLabel)
        }
        
        valueLabel.snp.makeConstraints {
            $0.trailing.equalToSuperview()
            $0.centerY.equalTo(operationLabel)
            $0.width.equalTo(Layout.Width.valueLabel)
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
}
