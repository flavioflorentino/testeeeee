import SkeletonView
import UI
import UIKit

final class StatementListSkeletonView: UIView, StatefulViewing, ViewConfiguration {
    var viewModel: StatefulViewModeling?
    weak var delegate: StatefulDelegate?
    
    private lazy var containerStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            StatementHeaderSectionSkeletonView(),
            StatementListCellSkeletonView(.compact),
            StatementListCellSkeletonView(.large),
            StatementListCellSkeletonView(.compact),
            StatementListCellSkeletonView(.compact),
            StatementListCellSkeletonView(.large),
            StatementListCellSkeletonView(.compact),
            StatementListCellSkeletonView(.large)
        ])
        stackView.axis = .vertical
        stackView.spacing = .zero
        return stackView
    }()
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubview(containerStackView)
    }
    
    func setupConstraints() {
        containerStackView.snp.makeConstraints {
            $0.topMargin.equalToSuperview().offset(-Spacing.base01)
            $0.leading.trailing.equalToSuperview()
        }
    }
    
    func configureViews() {
        viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }
}
