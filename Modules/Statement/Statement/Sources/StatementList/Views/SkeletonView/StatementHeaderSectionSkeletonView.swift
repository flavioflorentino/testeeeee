import SkeletonView
import SnapKit
import UI
import UIKit

private extension StatementHeaderSectionSkeletonView.Layout {
    enum Size {
        static let title = CGSize(width: 80, height: 20)
    }
}

final class StatementHeaderSectionSkeletonView: UIView, ViewConfiguration {
    fileprivate enum Layout {}
    
    private lazy var titleView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = Layout.Size.title.height / 2
        view.clipsToBounds = true
        return view
    }()
    
    private let gradient = SkeletonGradient(baseColor: Colors.grayscale100.color)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        titleView.showAnimatedGradientSkeleton(usingGradient: gradient)
    }

    func buildViewHierarchy() {
        addSubview(titleView)
    }
    
    func setupConstraints() {
        titleView.snp.makeConstraints {
            $0.leading.top.bottom.equalToSuperview().inset(Spacing.base02)
            $0.size.equalTo(Layout.Size.title)
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
}
