import SnapKit
import UI

protocol StatementListCellDelegate: AnyObject {
    func showActionMenu(withItem item: StatementItem?)
}

protocol StatementListCellDisplay: AnyObject {
    func setupAccessibilityLabel(with accessibilityText: String)
    func displayTime(_ time: String)
    func displayTitle(_ title: String)
    func displayDescription(_ description: String)
    func hideDescription()
    func displayOperationDetailItems(_ operationDetailItems: [OperationDetail], isCanceled: Bool, actionIndicator: Bool)
    func displayItemPlaceholderIcon(from icon: Iconography)
    func displayItemImage(from imageUrl: URL)
}

private extension StatementListCell.Layout {
    static let lineWidth: CGFloat = 1
    static let actionMenuButtonSize: CGFloat = 30.0
    static let disclosureIndicatorSize: CGFloat = 17.0
}

final class StatementListCell: UITableViewCell {
    fileprivate enum Layout {}
    
    weak var delegate: StatementListCellDelegate?
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.viewStyle(BackgroundViewStyle(color: .grayscale200()))
        return view
    }()
    
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView
            .imageStyle(RoundedImageStyle(size: .xSmall, cornerRadius: .full))
            .with(\.border, .light(color: .grayscale200()))
            .with(\.backgroundColor, .backgroundPrimary())
        imageView.contentMode = .scaleAspectFill
        imageView.accessibilityElementsHidden = true
        return imageView
    }()
    
    private lazy var iconLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(IconLabelStyle(type: .small))
            .with(\.textAlignment, .center)
            .with(\.textColor, .grayscale500())
        label.accessibilityElementsHidden = true
        return label
    }()
    
    private lazy var actionMenuButton: UIButton = {
        let button = UIButton()
        button.setImage(Assets.icoMoreMenu.image, for: .normal)
        button.addTarget(self, action: #selector(didTapMoreButton), for: .touchUpInside)
        button.accessibilityElementsHidden = true
        button.isHidden = true // Desabilitado enquanto a ação não é implementada
        return button
    }()
    
    private lazy var containerTextsView = UIView()
    
    private lazy var titleAndHourStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var titleHourAndDescriptionStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var hourLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(CaptionLabelStyle())
            .with(\.textColor, .grayscale400())
            .with(\.textAlignment, .right)
        label.setContentHuggingPriority(.required, for: .horizontal)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale500())
        return label
    }()
    
    private lazy var operationDetailStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var disclosureIndicator: UILabel = {
        let label = UILabel()
        label.labelStyle(IconLabelStyle(type: .small))
            .with(\.text, Iconography.angleRightB.rawValue)
            .with(\.textColor, Colors.grayscale600.color)
            .with(\.textAlignment, .right)
        label.accessibilityElementsHidden = true
        return label
    }()
    
    private lazy var presenter: StatementListCellPresenting = {
        let presenter = StatementListCellPresenter()
        presenter.view = self
        return presenter
    }()
    
    private lazy var customSelectedBackgroundView: UIView = {
        let view = UIView()
        view.viewStyle(BackgroundViewStyle(color: .backgroundSecondary()))
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView(model: StatementItem) {
        presenter.setup(model: model)
    }
    
    @objc
    private func didTapMoreButton() {
        delegate?.showActionMenu(withItem: presenter.model)
    }
    
    private func updateDisclosureIndicator(isVisible: Bool) {
        disclosureIndicator.isHidden = !isVisible
        
        disclosureIndicator.snp.updateConstraints {
            $0.width.height.equalTo(isVisible ? Layout.disclosureIndicatorSize : .zero)
        }
    }
}

extension StatementListCell: ViewConfiguration {
    func buildViewHierarchy() {
        titleAndHourStackView.addArrangedSubview(titleLabel)
        titleAndHourStackView.addArrangedSubview(hourLabel)
        
        titleHourAndDescriptionStackView.addArrangedSubview(titleAndHourStackView)
        titleHourAndDescriptionStackView.addArrangedSubview(descriptionLabel)
        
        containerTextsView.addSubviews(titleHourAndDescriptionStackView, operationDetailStackView)
        
        contentView.addSubviews(lineView, iconImageView, iconLabel, containerTextsView, actionMenuButton, disclosureIndicator)
    }
    
    func setupConstraints() {
        lineView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.centerX.equalTo(iconImageView)
            $0.width.equalTo(Layout.lineWidth)
        }

        iconImageView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base04)
            $0.leading.equalToSuperview().offset(Spacing.base02)
        }
        
        iconLabel.snp.makeConstraints {
            $0.width.height.centerX.centerY.equalTo(iconImageView)
        }
        
        containerTextsView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base04)
            $0.trailing.equalToSuperview().offset(-Spacing.base03)
            $0.bottom.equalToSuperview().offset(-Spacing.base01)
            $0.leading.equalTo(iconImageView.snp.trailing).offset(Spacing.base01)
        }
        
        titleHourAndDescriptionStackView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
        }
        
        operationDetailStackView.snp.makeConstraints {
            $0.top.equalTo(titleHourAndDescriptionStackView.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview()
            $0.bottom.equalToSuperview().priority(.low)
        }
        
        disclosureIndicator.snp.makeConstraints {
            $0.leading.equalTo(operationDetailStackView.snp.trailing)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.width.height.equalTo(Layout.disclosureIndicatorSize)
            $0.bottom.equalTo(operationDetailStackView)
        }
        
        actionMenuButton.snp.makeConstraints {
            $0.top.equalTo(containerTextsView.snp.top).offset(-Spacing.base00)
            $0.leading.equalTo(containerTextsView.snp.trailing).offset(Spacing.base00)
            $0.width.height.equalTo(Layout.actionMenuButtonSize)
        }
    }
    
    func configureViews() {
        selectedBackgroundView = customSelectedBackgroundView
        backgroundColor = Colors.backgroundPrimary.color
        selectionStyle = .none
    }
}

extension StatementListCell: StatementListCellDisplay {
    func setupAccessibilityLabel(with accessibilityText: String) {
        accessibilityLabel = accessibilityText
        isAccessibilityElement = true
    }
    
    func displayTime(_ time: String) {
        hourLabel.text = time
    }
    
    func displayTitle(_ title: String) {
        titleLabel.text = title
    }
    
    func displayDescription(_ description: String) {
        descriptionLabel.text = description
        descriptionLabel.isHidden = false
    }
    
    func hideDescription() {
        descriptionLabel.isHidden = true
    }
    
    func displayOperationDetailItems(_ operationDetailItems: [OperationDetail], isCanceled: Bool, actionIndicator: Bool) {
        operationDetailStackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
        operationDetailItems.forEach {
            let subview = StatementOperationDetailView(operationDetail: $0, isCanceled: isCanceled)
            operationDetailStackView.addArrangedSubview(subview)
        }
        updateDisclosureIndicator(isVisible: actionIndicator)
    }
    
    func displayItemPlaceholderIcon(from icon: Iconography) {
        iconImageView.image = nil
        iconLabel.isHidden = false
        iconLabel.text = icon.rawValue
    }
    
    func displayItemImage(from imageUrl: URL) {
        iconImageView.setImage(url: imageUrl, placeholder: nil) { [weak self] downloadedImage in
            guard let image = downloadedImage else {
                return
            }
            self?.iconLabel.isHidden = true
            self?.iconImageView.image = image
        }
    }
}
