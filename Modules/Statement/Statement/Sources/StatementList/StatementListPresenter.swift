import UIKit

enum StatementListVisiblePaginationState {
    case loading
    case failure
    case end
}

protocol StatementListPresenting: AnyObject {
    var viewController: StatementListDisplay? { get set }
    func presentNewItems(fromSections sections: [StatementSection])
    func updateItems(fromSections sections: [StatementSection])
    func removeItems(fromSections sections: [StatementSection])
    func didNextStep(action: StatementListAction)
    func showAnimatedSkeletonView()
    func hideAnimatedSkeletonView()
    func hideRefreshControl()
    func presentPaginationState(_ state: StatementListVisiblePaginationState)
    func presentConnectionFailureError()
    func presentGenericError()
    func presentEmptyView()
    func removeStatusViewIfNeeded()
}

final class StatementListPresenter {
    private typealias Localizable = Strings.StatementList
    private let coordinator: StatementListCoordinating
    weak var viewController: StatementListDisplay?

    init(coordinator: StatementListCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - StatementListPresenting
extension StatementListPresenter: StatementListPresenting {
    func presentNewItems(fromSections sections: [StatementSection]) {
        viewController?.displayNewItems(fromSections: sections)
    }
    
    func updateItems(fromSections sections: [StatementSection]) {
        viewController?.updateItems(fromSections: sections)
    }
    
    func removeItems(fromSections sections: [StatementSection]) {
        viewController?.removeItems(fromSections: sections)
    }
    
    func didNextStep(action: StatementListAction) {
        coordinator.perform(action: action)
    }
    
    func showAnimatedSkeletonView() {
        viewController?.showAnimatedSkeletonView()
    }
    
    func hideAnimatedSkeletonView() {
        viewController?.hideAnimatedSkeletonView()
	}
    
    func hideRefreshControl() {
        viewController?.hideRefreshControl()
    }
	
    func presentPaginationState(_ state: StatementListVisiblePaginationState) {
        let footerView: UIView
        switch state {
        case .loading:
            footerView = StatementListFooterLoadingView()
        case .failure:
            footerView = StatementListFooterFailureView()
        case .end:
            footerView = StatementListWelcomeView()
        }
        viewController?.displayTableFooterView(footerView)
    }
    
    func presentConnectionFailureError() {
        viewController?.displayError(
            title: Localizable.ConnectionFailure.title,
            text: Localizable.ConnectionFailure.text,
            buttonText: Localizable.ConnectionFailure.buttonText,
            image: Assets.noConnection.image
        )
    }
    
    func presentGenericError() {
        viewController?.displayError(
            title: Localizable.GenericError.title,
            text: Localizable.GenericError.text,
            buttonText: Localizable.GenericError.buttonText,
            image: Assets.genericError.image
        )
    }
    
    func presentEmptyView() {
        viewController?.displayEmptyView(
            title: Localizable.EmptyView.title,
            text: Localizable.EmptyView.text,
            image: Assets.emptyState.image
        )
    }
    
    func removeStatusViewIfNeeded() {
        viewController?.removeStatusViewIfNeeded()
    }
}
