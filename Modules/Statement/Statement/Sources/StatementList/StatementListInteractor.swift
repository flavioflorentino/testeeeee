import AnalyticsModule
import Core
import FeatureFlag
import Foundation

protocol StatementListInteracting: AnyObject {
    func fetchFirstPage()
    func fetchNextPage()
    func fetchMoreItemsIfNeeded(_ indexPath: IndexPath)
    func didSelectItem(_ item: StatementItem?)
    func refreshItems()
}

final class StatementListInteractor {
    typealias Dependencies = HasAnalytics & HasFeatureManager
    private let dependencies: Dependencies
    
    private let service: StatementListServicing
    private let presenter: StatementListPresenting
    private var statementSections: [StatementSection] = []
    private var pagination: StatementPagination?
    private var fetchingNewStatements = false
    
    private var nextPage: Int {
        guard let pagination = pagination else {
            return .zero
        }
        return pagination.pageIndex + 1
    }

    init(
        service: StatementListServicing,
        presenter: StatementListPresenting,
        dependencies: Dependencies
    ) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - StatementListInteracting
extension StatementListInteractor: StatementListInteracting {
    func fetchMoreItemsIfNeeded(_ indexPath: IndexPath) {
        guard isLastItem(indexPath: indexPath), !fetchingNewStatements else {
            return
        }
        
        if pagination?.isLastPage == true {
            presenter.presentPaginationState(.end)
            return
        }
        
        fetchNextPage()
    }
    
    func fetchFirstPage() {
        fetchingNewStatements = true
        presenter.showAnimatedSkeletonView()
        presenter.removeStatusViewIfNeeded()
      
        service.getStatementData(pageIndex: nextPage) { [weak self] result in
            self?.fetchingNewStatements = false
            self?.presenter.hideAnimatedSkeletonView()
                                                          
            switch result {
            case .success(let statementList):
                self?.pagination = statementList.pagination
                self?.statementSections = statementList.statement
                self?.showSuccessfulResult()
                self?.trackPageIndex()
            case .failure(let error):
                self?.presentError(error)
            }
        }
    }
    
    func fetchNextPage() {
        fetchingNewStatements = true
        presenter.presentPaginationState(.loading)
        
        service.getStatementData(pageIndex: nextPage) { [weak self] result in
            self?.fetchingNewStatements = false
            switch result {
            case .success(let statementList):
                self?.pagination = statementList.pagination
                self?.updateSections(statementList.statement)
                self?.trackPageIndex()
            case .failure:
                self?.presenter.presentPaginationState(.failure)
                self?.dependencies.analytics.log(StatementEvent.error(errorType: .errorLoading))
            }
        }
    }
    
    func refreshItems() {
        fetchingNewStatements = true
        pagination = .none
        
        service.getStatementData(pageIndex: nextPage) { [weak self] result in
            self?.fetchingNewStatements = false
            self?.presenter.hideRefreshControl()
            
            switch result {
            case .success(let statementList):
                self?.updateStatementFromRefresh(result: statementList)
                self?.trackPageIndex()
            case .failure(let error):
                self?.presentError(error)
            }
        }
    }
    
    private func updateStatementFromRefresh(result: StatementList) {
        presenter.removeItems(fromSections: statementSections)
        pagination = result.pagination
        statementSections = result.statement
        presenter.presentNewItems(fromSections: statementSections)
    }
    
    func didSelectItem(_ item: StatementItem?) {
        guard let item = item else {
            return
        }
        
        trackTapItem(item)
        executeAction(for: item)
    }
}

// MARK: - Private methods
private extension StatementListInteractor {
    func showSuccessfulResult() {
        if statementSections.isEmpty {
            presenter.presentEmptyView()
            dependencies.analytics.log(StatementEvent.error(errorType: .noTransactions))
        } else {
            presenter.presentNewItems(fromSections: statementSections)
        }
    }
    
    func presentError(_ error: ApiError) {
        switch error {
        case .connectionFailure:
            presenter.presentConnectionFailureError()
            dependencies.analytics.log(StatementEvent.error(errorType: .offline))
        default:
            presenter.presentGenericError()
            dependencies.analytics.log(StatementEvent.error(errorType: .errorLoading))
        }
    }
    
    func isLastItem(indexPath: IndexPath) -> Bool {
        guard
            indexPath.section == statementSections.count - 1,
            indexPath.row == statementSections[indexPath.section].statementItems.count - 1
            else {
                return false
        }
        
        return true
    }
    
    func updateSections(_ newSections: [StatementSection]) {
        guard
            let firstHeaderNewSections = newSections.first?.header,
            let lastHeaderCurrentSections = statementSections.last?.header,
            firstHeaderNewSections == lastHeaderCurrentSections
            else {
                statementSections += newSections
                presenter.presentNewItems(fromSections: newSections)
                return
        }
        
        var currentSections = statementSections
        guard let lastFromCurrentSections = currentSections.popLast(), !newSections.isEmpty else {
            return
        }

        var newSections = newSections
        let firstFromNewSections = newSections.removeFirst()
        let newItens = lastFromCurrentSections.statementItems + firstFromNewSections.statementItems
        let mergedSection = [StatementSection(header: lastFromCurrentSections.header, statementItems: newItens)]
        
        statementSections = currentSections + mergedSection + newSections
        
        presenter.updateItems(fromSections: mergedSection)
        presenter.presentNewItems(fromSections: newSections)
    }
    
    func trackPageIndex() {
        dependencies.analytics.log(StatementEvent.paginationLoaded(tab: .all, page: pagination?.pageIndex ?? 0))
    }
    
    func trackTapItem(_ item: StatementItem) {
        dependencies.analytics.log(
            StatementEvent.itemSelected(interaction: .card, itemType: item.type, transactionStatus: item.status)
        )
    }
    
    func executeAction(for item: StatementItem) {
        guard dependencies.featureManager.isActive(.isStatementDetailEnabled) else {
            return
        }
        
        guard let action = item.action else { return }
        presenter.didNextStep(action: action)
    }
}
