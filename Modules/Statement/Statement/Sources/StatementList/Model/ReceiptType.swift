import Foundation

enum ReceiptType: String {
    case pav = "pav"
    case p2p = "p2p"
    case p2pExternalTransfer = "P2pExternalTransfer"
    case pix = "pix"
    case pixReceiver = "pixReceiver"
    case p2pLending = "p2pLending"
    
    init?(_ itemType: StatementItem.ItemType, flow: StatementItem.OperationFlow?) {
        switch itemType {
        case .pav, .phoneRecharge, .digitalGoods, .picpayCard, .mobileTickets:
            self = .pav
        case .p2p:
            guard flow == .outflow else { return nil }
            self = .p2p
        case .p2pLending:
            self = .p2pLending
        default:
            return nil
        }
    }
}
