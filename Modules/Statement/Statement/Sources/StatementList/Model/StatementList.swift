import Foundation

struct StatementList: Decodable, Equatable {
    let pagination: StatementPagination
    let statement: [StatementSection]
}

struct StatementPagination: Decodable, Equatable {
    let pageIndex, pageSize: Int
    let isLastPage: Bool
}

struct StatementSection: Decodable, Equatable {
    let header: StatementHeader
    let statementItems: [StatementItem]
}

struct StatementHeader: Decodable, Hashable, Equatable {
    let date: String
}

struct StatementItem: Decodable, Equatable {
    let title, time, date: String
    let description, image: String?
    let type: ItemType
    let status: Status
    let operationDetails: [OperationDetail]
    let transactionId: String?
    let flow: OperationFlow?
}

struct OperationDetail: Decodable, Equatable {
    let description, value: String
    let style: Style
}

// Enumerated types

extension StatementItem {
    enum ItemType: String, Decodable {
        case p2p = "P2P"
        case p2pLending = "P2P_LENDING"
        case phoneRecharge = "PHONE_RECHARGE"
        case cashin = "CASHIN"
        case cashout = "CASHOUT"
        case cashback = "CASHBACK"
        case pav = "PAV"
        case digitalGoods = "DIGITAL_GOODS"
        case income = "INCOME"
        case mobileTickets = "MOBILE_TICKETS"
        case withdraw = "WITHDRAW"
        case pix = "PIX"
        case generic = "GENERIC"
        case accountAdjustment = "ACCOUNT_ADJUSTMENT"
        case b2p = "B2P"
        case fees = "FEES"
        case loan = "LOAN"
        case personalLoan = "PERSONAL_LOAN"
        case picpayCard = "PICPAY_CARD"
        case store = "STORE"
        
        init(from decoder: Decoder) throws {
            let rawValue = try decoder.singleValueContainer().decode(RawValue.self)
            self = ItemType(rawValue: rawValue) ?? .generic
        }
    }
    
    enum Status: String, Decodable {
        case `default` = "DEFAULT"
        case canceled = "CANCELED"
        
        init(from decoder: Decoder) throws {
            let rawValue = try decoder.singleValueContainer().decode(RawValue.self)
            self = Status(rawValue: rawValue) ?? .`default`
        }
    }
    
    enum OperationFlow: String, Decodable {
        case inflow = "INFLOW"
        case outflow = "OUTFLOW"
    }
}

extension OperationDetail {
    enum Style: String, Decodable {
        case `default` = "DEFAULT"
        case credit = "CREDIT"
        case debit = "DEBIT"
        case income = "INCOME"
        
        init(from decoder: Decoder) throws {
            let rawValue = try decoder.singleValueContainer().decode(RawValue.self)
            self = Style(rawValue: rawValue) ?? .`default`
        }
    }
}

extension StatementItem {
    var action: StatementListAction? {
        if type == .income {
            return .openIncome
        }
        
        guard let transactionId = transactionId, status != .canceled else {
            return nil
        }
        
        switch type {
        case .cashin:
            // Todo: Separar o cashin TED do cashin boleto. Habilitar recibo para o cashin TED e desabilitar para o cashin boleto.
            return nil
        case .cashout:
            return .openCashout(transactionId: transactionId)
        default:
            guard let receiptType = ReceiptType(type, flow: flow) else {
                return nil
            }
            return .openReceipt(transactionId: transactionId, receiptType: receiptType.rawValue)
        }
    }
}
