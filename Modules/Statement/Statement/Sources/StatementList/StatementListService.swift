import Core
import Foundation

protocol StatementListServicing {
    func getStatementData(pageIndex: Int, completion: @escaping (Result<StatementList, ApiError>) -> Void)
}

final class StatementListService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - StatementListServicing
extension StatementListService: StatementListServicing {
    func getStatementData(pageIndex: Int, completion: @escaping (Result<StatementList, ApiError>) -> Void) {
        let endpoint = StatementEndpoint.statementList(pageIndex)
        let decoder = JSONDecoder(.convertFromSnakeCase)
        Api<StatementList>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
