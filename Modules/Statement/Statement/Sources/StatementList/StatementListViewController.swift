import UI
import UIKit

protocol StatementListDisplay: AnyObject {
    func displayNewItems(fromSections sections: [StatementSection])
    func updateItems(fromSections sections: [StatementSection])
    func removeItems(fromSections sections: [StatementSection])
    func showAnimatedSkeletonView()
    func hideAnimatedSkeletonView()
    func hideRefreshControl()
    func displayTableFooterView(_ footerView: UIView)
    func displayError(title: String, text: String, buttonText: String?, image: UIImage)
    func displayEmptyView(title: String, text: String, image: UIImage)
    func removeStatusViewIfNeeded()
}

private extension StatementListViewController.Layout {
    enum Size {
        static let estimatedRowHeight: CGFloat = 115.0
        static let estimatedSectionHeaderViewHeight: CGFloat = 50.0
    }
}

final class StatementListViewController: ViewController<StatementListInteracting, UIView> {
    fileprivate enum Layout { }
    
    private typealias ItemProviderType = TableViewDataSource<StatementHeader, StatementItem>.ItemProvider
    
    private lazy var statementCellIdentifier = String(describing: StatementListCell.self)
    private lazy var statementSectionHeaderViewIdentifier = String(describing: StatementHeaderSectionView.self)
    
    private lazy var itemProvider: ItemProviderType = { tableView, indexPath, item -> UITableViewCell? in
        let cell = tableView.dequeueReusableCell(withIdentifier: self.statementCellIdentifier, for: indexPath)
        let statementListCell = cell as? StatementListCell
        statementListCell?.setupView(model: item)
        statementListCell?.delegate = self
        return statementListCell
    }
    
    private lazy var tableViewDataSource: TableViewDataSource<StatementHeader, StatementItem> = {
        let dataSource = TableViewDataSource<StatementHeader, StatementItem>(view: tableView)
        dataSource.itemProvider = itemProvider
        return dataSource
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = Layout.Size.estimatedRowHeight
        tableView.estimatedSectionHeaderHeight = Layout.Size.estimatedSectionHeaderViewHeight
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.delegate = self
        tableView.register(StatementListCell.self, forCellReuseIdentifier: statementCellIdentifier)
        tableView.register(
            StatementHeaderSectionView.self,
            forHeaderFooterViewReuseIdentifier: statementSectionHeaderViewIdentifier
        )
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.addTarget(self, action: #selector(handleRefreshControl), for: .valueChanged)
        return tableView
    }()
    
    private var statusView: StatementStatusView?

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = tableViewDataSource
        interactor.fetchFirstPage()
    }

    override func buildViewHierarchy() {
        view.addSubview(tableView)
    }
    
    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }

    override func configureViews() {
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }

    private func updateViewHeight(for view: UIView) {
        let expectedSize = view.systemLayoutSizeFitting(
            CGSize(width: tableView.frame.width, height: .leastNonzeroMagnitude),
            withHorizontalFittingPriority: .required,
            verticalFittingPriority: .defaultLow)
        if view.frame.size.height != expectedSize.height {
            view.frame.size.height = expectedSize.height
        }
    }
    
    private func displayStatusView(title: String, text: String, buttonText: String?, image: UIImage) {
        tableView.isHidden = true
        
        let statusView = StatementStatusView(image: image, title: title, text: text, buttonText: buttonText)
        view.addSubview(statusView)
        
        statusView.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom)
            $0.leading.trailing.equalToSuperview()
        }
        
        statusView.delegate = self
        
        self.statusView = statusView
    }
    
    @objc
    private func handleRefreshControl() {
        interactor.refreshItems()
    }
}

extension StatementListViewController: StatementListCellDelegate {
    func showActionMenu(withItem item: StatementItem?) {
        interactor.didSelectItem(item)
    }
}

extension StatementListViewController: StatementListFooterFailureDelegate {
    func didTapTryAgain() {
        interactor.fetchNextPage()
    }
}

// MARK: UITableViewDelegate
extension StatementListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        interactor.fetchMoreItemsIfNeeded(indexPath)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let title = tableViewDataSource.sections[section].date
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: statementSectionHeaderViewIdentifier)
        
        guard let statementHeaderSectionView = headerView as? StatementHeaderSectionView else {
            let currentView = StatementHeaderSectionView(reuseIdentifier: statementSectionHeaderViewIdentifier)
            currentView.sectionTitle = title
            return currentView
        }
        
        statementHeaderSectionView.sectionTitle = title
        return statementHeaderSectionView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        interactor.didSelectItem(tableViewDataSource.item(at: indexPath))
    }
}

// MARK: StatementListDisplay
extension StatementListViewController: StatementListDisplay {
    func displayNewItems(fromSections sections: [StatementSection]) {
        for section in sections {
            tableViewDataSource.add(items: section.statementItems, to: section.header)
        }
    }
    
    func updateItems(fromSections sections: [StatementSection]) {
        for section in sections {
            tableViewDataSource.update(items: section.statementItems, from: section.header)
        }
	}
    
    func removeItems(fromSections sections: [StatementSection]) {
        for section in sections {
            tableViewDataSource.remove(section: section.header)
        }
    }
	
    func displayTableFooterView(_ footerView: UIView) {
        if let failureFooterView = footerView as? StatementListFooterFailureView {
            failureFooterView.delegate = self
        }

        updateViewHeight(for: footerView)
        tableView.tableFooterView = footerView
        tableView.layoutIfNeeded()
    }
    
    func showAnimatedSkeletonView() {
        beginState()
    }
    
    func hideAnimatedSkeletonView() {
        endState()
    }
    
    func hideRefreshControl() {
        tableView.refreshControl?.endRefreshing()
    }
    
    func displayError(title: String, text: String, buttonText: String?, image: UIImage) {
        displayStatusView(title: title, text: text, buttonText: buttonText, image: image)
    }
    
    func displayEmptyView(title: String, text: String, image: UIImage) {
        displayStatusView(title: title, text: text, buttonText: nil, image: image)
    }
    
    func removeStatusViewIfNeeded() {
        statusView?.removeFromSuperview()
        tableView.isHidden = false
    }
}

extension StatementListViewController: StatefulTransitionViewing {
    func statefulViewForLoading() -> StatefulViewing {
        StatementListSkeletonView()
    }
}

// MARK: StatementStatusViewDelegate
extension StatementListViewController: StatementStatusViewDelegate {
    func didTapActionButton() {
        interactor.fetchFirstPage()
    }
}
