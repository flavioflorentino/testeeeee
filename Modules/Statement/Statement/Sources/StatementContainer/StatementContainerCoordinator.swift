import Foundation
import Core

enum StatementContainerAction: Equatable {
    case close
    case openCustomerSupport
}

protocol StatementContainerCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var deeplink: DeeplinkContract? { get set }
    func perform(action: StatementContainerAction)
}

final class StatementContainerCoordinator {
    weak var viewController: UIViewController?
    var deeplink: DeeplinkContract?
    
    private var customerSupportURL: URL? {
        URL(string: "picpay://picpay/helpcenter/section/360011869332")
    }
}

// MARK: - StatementContainerCoordinating
extension StatementContainerCoordinator: StatementContainerCoordinating {
    func perform(action: StatementContainerAction) {
        switch action {
        case .close:
            viewController?.navigationController?.dismiss(animated: true)
        case .openCustomerSupport:
            guard let customerSupportURL = customerSupportURL else { return }
            deeplink?.open(url: customerSupportURL)
        }
    }
}
