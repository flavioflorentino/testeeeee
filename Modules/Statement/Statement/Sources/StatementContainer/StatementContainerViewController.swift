import SnapKit
import UI
import UIKit

protocol StatementContainerDisplay: AnyObject {
}

private extension StatementContainerViewController.Layout {
}

final class StatementContainerViewController: ViewController<StatementContainerInteracting, UIView> {
    fileprivate enum Layout { }
    
    private lazy var closeBarButtonItem = UIBarButtonItem(
        title: Strings.StatementContainer.closeButton,
        style: .plain,
        target: self,
        action: #selector(tapClose)
    )
    
    private lazy var customerSupportBarButtonItem: UIBarButtonItem = {
        let label = UILabel()
        label
            .labelStyle(IconLabelStyle(type: .medium))
            .with(\.textColor, .branding600())
            .with(\.text, Iconography.questionCircle.rawValue)
            .with(\.isUserInteractionEnabled, true)
            
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapCustomerSupportButton))
        label.addGestureRecognizer(tapGesture)
        let barButton = UIBarButtonItem(customView: label)
        barButton.accessibilityLabel = Strings.Accessibility.helpCenterButton
        return barButton
    }()
    
    private lazy var fullStatementTableView = StatementListFactory.make()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        interactor.trackView()
    }

    override func buildViewHierarchy() {
        addChild(fullStatementTableView)
        fullStatementTableView.didMove(toParent: self)
        view.addSubview(fullStatementTableView.view)
    }
    
    override func setupConstraints() {
        fullStatementTableView.view.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }

    override func configureViews() {
        title = Strings.StatementContainer.title
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        setupNavigationBar()
    }
    
    private func setupNavigationBar() {
        navigationController?.navigationBar.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        navigationController?.navigationBar.barTintColor = Colors.backgroundPrimary.color
        navigationItem.leftBarButtonItem = closeBarButtonItem
        navigationItem.rightBarButtonItem = customerSupportBarButtonItem
    }
    
    @objc
    private func tapClose() {
        interactor.didClose()
    }
    
    @objc
    private func tapCustomerSupportButton() {
        interactor.didOpenCustomerSupport()
    }
}

// MARK: StatementContainerDisplay
extension StatementContainerViewController: StatementContainerDisplay {
}
