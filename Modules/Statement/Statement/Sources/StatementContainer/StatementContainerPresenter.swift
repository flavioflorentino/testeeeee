import Foundation

protocol StatementContainerPresenting: AnyObject {
    var viewController: StatementContainerDisplay? { get set }
    func didNextStep(action: StatementContainerAction)
}

final class StatementContainerPresenter {
    private let coordinator: StatementContainerCoordinating
    weak var viewController: StatementContainerDisplay?

    init(coordinator: StatementContainerCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - StatementContainerPresenting
extension StatementContainerPresenter: StatementContainerPresenting {
    func didNextStep(action: StatementContainerAction) {
        coordinator.perform(action: action)
    }
}
