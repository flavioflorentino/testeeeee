import Foundation

public enum StatementOrigin {
    case wallet, notification, feed, email
}

public enum StatementContainerFactory {
    public static func make(origin: StatementOrigin = .wallet) -> UIViewController {
        let container = DependencyContainer()
        let service: StatementContainerServicing = StatementContainerService()
        let coordinator: StatementContainerCoordinating = StatementContainerCoordinator()
        let presenter: StatementContainerPresenting = StatementContainerPresenter(coordinator: coordinator)
        let interactor = StatementContainerInteractor(
            service: service,
            presenter: presenter,
            dependencies:
            container,
            origin: origin
        )
        let viewController = StatementContainerViewController(interactor: interactor)

        coordinator.viewController = viewController
        coordinator.deeplink = StatementSetup.deeplinkInstance
        presenter.viewController = viewController

        return viewController
    }
}
