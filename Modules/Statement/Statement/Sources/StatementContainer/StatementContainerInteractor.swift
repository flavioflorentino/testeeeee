import AnalyticsModule
import Foundation

protocol StatementContainerInteracting: AnyObject {
    func didClose()
    func didOpenCustomerSupport()
    func trackView()
}

final class StatementContainerInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let service: StatementContainerServicing
    private let presenter: StatementContainerPresenting
    private let origin: StatementOrigin

    init(
        service: StatementContainerServicing,
        presenter: StatementContainerPresenting,
        dependencies: Dependencies,
        origin: StatementOrigin
    ) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.origin = origin
    }
}

// MARK: - StatementContainerInteracting
extension StatementContainerInteractor: StatementContainerInteracting {
    func didClose() {
        presenter.didNextStep(action: .close)
    }
    
    func didOpenCustomerSupport() {
        presenter.didNextStep(action: .openCustomerSupport)
    }
    
    func trackView() {
        dependencies.analytics.log(StatementEvent.mainPageViewed(origin: .init(statementOrigin: origin)))
    }
}
