import Core
import Foundation

public protocol StatementEnabledServicing {
    func isStatementEnabled(completion: @escaping (Result<Bool, ApiError>) -> Void)
}

public final class StatementEnabledService {
    public typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    public init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension StatementEnabledService: StatementEnabledServicing {
    public func isStatementEnabled(completion: @escaping (Result<Bool, ApiError>) -> Void) {
        Api<[String: Bool]>(endpoint: StatementEndpoint.isStatementEnabled).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model).flatMap { dic -> Result<Bool, ApiError> in
                    guard let isStatementEnabled = dic["isStatementEnabled"] else {
                        return .failure(ApiError.bodyNotFound)
                    }
                    
                    return .success(isStatementEnabled)
                }
                
                completion(mappedResult)
            }
        }
    }
}
