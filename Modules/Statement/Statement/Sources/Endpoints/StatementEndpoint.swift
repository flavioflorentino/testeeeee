import Core

enum StatementEndpoint {
    case statementList(_ pageIndex: Int)
    case isStatementEnabled
}

// Todo: Acrescentar todos os endpoints usados e corrigir seus paths e configurações
extension StatementEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .statementList:
            return "account-statement/statement"
        case .isStatementEnabled:
            return "walle/statement/enabled"
        }
    }
    
    var parameters: [String: Any] {
        guard case StatementEndpoint.statementList(let pageIndex) = self else {
            return [:]
        }
        return ["page": pageIndex]
    }
    
    var method: HTTPMethod {
        .get
    }
}
