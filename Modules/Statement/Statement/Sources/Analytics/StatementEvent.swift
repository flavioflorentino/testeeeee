import AnalyticsModule
import Foundation

enum StatementEvent: AnalyticsKeyProtocol {
    case mainPageViewed(origin: Origin)
    case paginationLoaded(tab: Tab, page: Int)
    case itemSelected(interaction: Interaction, itemType: StatementItem.ItemType, transactionStatus: StatementItem.Status)
    case error(errorType: ErrorType)
    
    private var name: String {
        switch self {
        case .mainPageViewed:
            return "Main Page Viewed"
        case .paginationLoaded:
            return "Pagination Loaded"
        case .itemSelected:
            return "Item Selected"
        case .error:
            return "Error"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .mainPageViewed(let origin):
            return [Keys.origin.rawValue: origin.rawValue]
        case let .paginationLoaded(tab, page):
            return [Keys.tabSelected.rawValue: tab.rawValue, Keys.scroll.rawValue: page]
        case let .itemSelected(interaction, itemType, transactionStatus):
            return [
                Keys.interactionType.rawValue: interaction.rawValue,
                Keys.transactionType.rawValue: itemType.rawValue,
                Keys.transactionStatus.rawValue: transactionStatus.rawValue
            ]
        case .error(let errorType):
            return [Keys.errorType.rawValue: errorType.rawValue]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.firebase, .mixPanel]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("Statement - \(name)", properties: properties, providers: providers)
    }
    
    enum Keys: String {
        case origin, scroll
        case tabSelected = "tab_selected"
        case interactionType = "interaction_type"
        case menuType = "menu_type"
        case transactionType = "transaction_type"
        case transactionStatus = "transaction_status"
        case errorType = "error_type"
    }
    
    enum Origin: String {
        case wallet, notification, feed, email
        
        init(statementOrigin origin: StatementOrigin) {
            switch origin {
            case .wallet:
                self = .wallet
            case .notification:
                self = .notification
            case .feed:
                self = .notification
            case .email:
                self = .email
            }
        }
    }
    
    enum Tab: String {
        case all, balance, card
    }
    
    enum Interaction: String {
        case card = "CARD"
        case menu = "MENU"
    }
    
    enum Menu: String {
        case returnPayment = "return_payment"
        case openReceipt = "open_receipt"
        case openProfit = "open_profit"
        case close, share
    }
    
    enum ErrorType: String {
        case offline
        case errorLoading = "error_loading"
        case noTransactions = "no_transactions"
    }
}
