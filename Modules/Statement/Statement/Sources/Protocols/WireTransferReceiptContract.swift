import Foundation

public protocol WireTransferReceiptContract {
    func open(withId id: String)
}
