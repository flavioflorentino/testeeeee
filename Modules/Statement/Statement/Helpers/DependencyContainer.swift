import AnalyticsModule
import Core
import FeatureFlag

typealias RegistrationDependencies = HasMainQueue & HasAnalytics & HasFeatureManager

final class DependencyContainer: RegistrationDependencies {
    lazy var mainQueue = DispatchQueue.main
    lazy var analytics: AnalyticsProtocol = Analytics.shared
    lazy var featureManager: FeatureManagerContract = FeatureManager.shared
}
