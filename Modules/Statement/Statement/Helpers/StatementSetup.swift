import Foundation
import Core

public enum StatementSetup {
    public static var receipt: ReceiptContract?
    public static var income: IncomeContract?
    public static var wireTransferReceipt: WireTransferReceiptContract?
    public static var deeplinkInstance: DeeplinkContract?

    public static func inject(instance: ReceiptContract) {
        receipt = instance
    }
    
    public static func inject(instance: IncomeContract) {
        income = instance
    }
    
    public static func inject(instance: WireTransferReceiptContract) {
        wireTransferReceipt = instance
    }
    
    public static func inject(instance: DeeplinkContract) {
        deeplinkInstance = instance
    }
}
