import Foundation

// swiftlint:disable convenience_type
final class StatementResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: StatementResources.self).url(forResource: "StatementResources", withExtension: "bundle") else {
            return Bundle(for: StatementResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: StatementResources.self)
    }()
}
