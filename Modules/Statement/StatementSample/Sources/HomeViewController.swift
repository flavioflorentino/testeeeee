import SnapKit
import Statement
import UI
import UIKit

class HomeViewController: UIViewController {
    private lazy var registrationRenewalButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle("Extrato", for: .normal)
        button.addTarget(self, action: #selector(tapStatementButton), for: .touchUpInside)
        return button
    }()
    
    init() {
        super.init(nibName: nil, bundle: nil)
        buildViewHierarchy()
        setupConstraints()
        configureViews()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    private func tapStatementButton() {
        let statementController = StatementContainerFactory.make()
        present(UINavigationController(rootViewController: statementController), animated: true)
    }
}

extension HomeViewController: ViewConfiguration {
    func buildViewHierarchy() {
        view.addSubview(registrationRenewalButton)
    }
    
    func setupConstraints() {
        registrationRenewalButton.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
    }
    
    func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}
