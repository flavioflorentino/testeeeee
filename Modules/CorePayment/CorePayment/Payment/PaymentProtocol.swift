public protocol PaymentHeaderInput: AnyObject {
    var delegate: PaymentHeaderOutput? { get set }
    func changePayment(value: Double)
    func dismissKeyboard()
    func appearKeyboard()
}

public extension PaymentHeaderInput {
    func changePayment(value: Double) {}
    func dismissKeyboard() {}
    func appearKeyboard() {}
}

public protocol PaymentHeaderOutput: AnyObject {
    func headerUpdateValue(value: Double)
    func headerUpdateInstallment(installment: Int, forceCreditCard: Bool)
    func didTapPaymentMethodSelection()
    func didTapInstallments()
}

public protocol PaymentAccessoryInput: AnyObject {
    var delegate: PaymentAccessoryOutput? { get set }
    func changePaymentMethod(method: PaymentMethod)
    func changeAccessory(with values: AccessoryValue)
}

public protocol PaymentAccessoryOutput: AnyObject {
    func accessoryUpdateValue(value: Double)
    func accessoryUpdateCardFee(cardFee: Double)
    func accessoryUpdateMessage(message: String)
    func accessoryUpdatePaymentParams(_ param: Decodable)
    func update(paymentType: PaymentType)
    func showLoadingView()
    func hideLoadingView()
}

public protocol PaymentToolbarInput: AnyObject {
    var delegate: PaymentToolbartOutput? { get set }
    var paymentMethod: PaymentMethod { get set }
    func showPaymentMethodSelection()
    func updateToolbar(value: Double, installment: Int, forceCreditCard: Bool)
    func changePayButtonTitle(with type: PaymentType)
}

public protocol PaymentToolbartOutput: AnyObject {
    func didTapPayment()
    func changePrivacy(privacy: PaymentPrivacy)
    func completionPaymentMethods()
}

public extension PaymentToolbarInput {
    func showPaymentMethodSelection() { }
    func updateToolbar(value: Double, installment: Int, forceCreditCard: Bool) { }
    func changePayButtonTitle(with type: PaymentType) { }
}

public extension PaymentAccessoryInput {
    func changePaymentMethod(method: PaymentMethod) { }
    func changeAccessory(with values: AccessoryValue) { }
}
