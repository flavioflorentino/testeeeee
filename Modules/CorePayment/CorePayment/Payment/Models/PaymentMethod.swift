public enum PaymentMethod {
    case accountBalance
    case balanceAndCard
    case card
}
