import Foundation

@objc
public enum PaymentPrivacy: Int {
    case `private` = 2
    case `public` = 3
    
    public var stringValue: String {
        return "\(self.rawValue)"
    }
}
