public enum PaymentType: Int, Decodable, Equatable {
    case `default` = 1
    case scheduling
}
