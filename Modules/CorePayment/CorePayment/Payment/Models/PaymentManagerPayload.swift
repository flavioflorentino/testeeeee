public struct PaymentManagerPayload {
    public let value: Double
    public let cardFee: Double
    public let installment: Int
    
    public init(value: Double, cardFee: Double, installment: Int) {
        self.value = value
        self.cardFee = cardFee
        self.installment = installment
    }
}
