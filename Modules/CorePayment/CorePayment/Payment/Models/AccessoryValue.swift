public struct AccessoryValue {
    public let total: Double
    public let cardFeeValue: Double
    public let installment: Int
    public let installmentValue: Double
    public let installmentFeeValue: Double
    
    public init(total: Double, cardFeeValue: Double, installment: Int, installmentValue: Double, installmentFeeValue: Double) {
        self.total = total
        self.cardFeeValue = cardFeeValue
        self.installment = installment
        self.installmentValue = installmentValue
        self.installmentFeeValue = installmentFeeValue
    }
}
