public enum PaymentValue {
    case balance
    case card
    case cardWithoutInterest
    case cardFee
    case installment
    case installmentFee
}
