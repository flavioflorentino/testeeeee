public struct PaymentInstallment {
    public let payeeId: String
    public let value: Double
    public let installment: Int
    
    public init(payeeId: String, value: Double, installment: Int) {
        self.payeeId = payeeId
        self.value = value
        self.installment = installment
    }
}

public struct PaymentBusinessInstallment {
    public let sellerId: String
    public let value: Double
    public let installment: Int
    
    public init(sellerId: String, value: Double, installment: Int) {
        self.sellerId = sellerId
        self.value = value
        self.installment = installment
    }
}
