public struct Payment {
    public let paymentType: PaymentType
    public let password: String
    public let biometry: Bool
    public let value: Double
    public let balanceValue: Double
    public let cardValueWithoutInterest: Double
    public let cvv: String?
    public let surcharge: Double?
    public let cardId: String
    public let installment: Int
    public let privacyConfig: PaymentPrivacy
    public let message: String
    public let someErrorOccurred: Bool
    public let extra: Decodable?
    
    public init(
        paymentType: PaymentType,
        password: String,
        biometry: Bool,
        value: Double,
        balanceValue: Double,
        cardValueWithoutInterest: Double,
        cardId: String,
        installment: Int,
        privacyConfig: PaymentPrivacy,
        message: String,
        someErrorOccurred: Bool,
        cvv: String? = nil,
        surcharge: Double? = nil,
        extra: Decodable? = nil
    ) {
        self.paymentType = paymentType
        self.password = password
        self.biometry = biometry
        self.value = value
        self.balanceValue = balanceValue
        self.cardValueWithoutInterest = cardValueWithoutInterest
        self.cvv = cvv
        self.surcharge = surcharge
        self.cardId = cardId
        self.installment = installment
        self.privacyConfig = privacyConfig
        self.message = message
        self.someErrorOccurred = someErrorOccurred
        self.extra = extra
    }
}
