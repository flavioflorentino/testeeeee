public protocol SuccessPayment {
    var transactionId: String { get }
}

public protocol FailurePayment: Error {
    var message: String { get }
}

public protocol LegacyPaymentServicing {
    associatedtype Success: SuccessPayment
    associatedtype Failure: FailurePayment
    func createTransaction(payment: Payment, completion: @escaping (Result<Success, Failure>) -> Void)
}
