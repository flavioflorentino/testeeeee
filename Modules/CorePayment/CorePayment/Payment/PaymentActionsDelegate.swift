public struct PaymentActionErrorViewModel {
    public let title: String
    public let message: String
    public let code: Int
    
    public init (title: String, message: String, code: Int) {
        self.title = title
        self.message = message
        self.code = code
    }
}

public protocol PaymentActionsDelegate: AnyObject {
    var paymentMethod: PaymentMethod? { get set }
    var installment: Int? { get set }
    var extraParams: Decodable? { get set }
    
    func viewWillAppear()
    func didTapPay(privacyType: PaymentPrivacy, hasMessage: Bool, requestValue: String)
    func didTapInstallments()
    func didTapChangePaymentMethod()
    func didTapPayment(wentToCards: Bool, wentToInstallments: Bool)
    func paymentSuccess(transactionId: String, value: Double)
    func paymentFailure(error: PaymentActionErrorViewModel)
    func paymentCancel()
}

public extension PaymentActionsDelegate {
    func viewWillAppear() {}
    func didTapPay(privacyType: PaymentPrivacy, hasMessage: Bool, requestValue: String) {}
    func didTapInstallments() {}
    func didTapChangePaymentMethod() {}
    func didTapPayment(wentToCards: Bool, wentToInstallments: Bool) {}
    func paymentSuccess(transactionId: String, value: Double) {}
    func paymentFailure(error: PaymentActionErrorViewModel) {}
    func paymentCancel() {}
}
