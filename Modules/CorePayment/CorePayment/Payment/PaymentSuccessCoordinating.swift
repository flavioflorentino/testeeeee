import UIKit

public protocol PaymentSuccessCoordinating: AnyObject {
    associatedtype Success
    func showPaymentSuccess(model: Success, paymentType: PaymentType, viewController: UIViewController?)
}

public extension PaymentSuccessCoordinating {
    func showPaymentSuccess(model: Success, viewController: UIViewController?) {
        showPaymentSuccess(model: model, paymentType: .default, viewController: viewController)
    }
}
