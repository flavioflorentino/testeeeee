import Foundation
import Core
import FeatureFlag
import AnalyticsModule
import UIKit

typealias AppDependencies = HasNoDependency
    & HasMainQueue
    & HasFeatureManager
    & HasAnalytics
    & HasURLOpener

final class DependencyContainer: AppDependencies {
    lazy var mainQueue: DispatchQueue = .main
    lazy var featureManager: FeatureManagerContract = FeatureManager.shared
    lazy var analytics: AnalyticsProtocol = Analytics.shared
    lazy var urlOpener: URLOpener? = UIApplication.shared
}
