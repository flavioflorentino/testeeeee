import Foundation
import Core

protocol HasNoDependency {}

protocol HasURLOpener {
    var urlOpener: URLOpener? { get }
}
