import Foundation
import Core

protocol CollectiveLendingCreditScoreInteracting: AnyObject {
    func fetchCreditScore()
    func close()
}

final class CollectiveLendingCreditScoreInteractor {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let service: CollectiveLendingCreditScoreServicing
    private let presenter: CollectiveLendingCreditScorePresenting

    init(service: CollectiveLendingCreditScoreServicing, presenter: CollectiveLendingCreditScorePresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
    
    private func handle(error: ApiError) {
        guard
            let requestError = error.requestError,
            let identifiableError = IdentifiableCreditScoreError(rawValue: requestError.code)
        else {
            presenter.displayError()
            return
        }
        
        switch identifiableError {
        case .nonExistentScore:
            presenter.didNextStep(action: .nonExistentScore(identifiableError))
        case .unavailableLimit:
            presenter.didNextStep(action: .unavailableLimit(identifiableError))
        }
    }
}

// MARK: - CollectiveLendingCreditScoreInteracting
extension CollectiveLendingCreditScoreInteractor: CollectiveLendingCreditScoreInteracting {
    func fetchCreditScore() {
        presenter.startLoading(title: Strings.CollectiveLending.CreditScore.loadingTitle)

        let group = DispatchGroup()
        group.enter()
        self.presenter.startProgressBar(withDuration: 3.0) {
            group.leave()
        }
        
        service.fetchCreditScore { [weak self] result in
            guard let strongSelf = self else { return }
            group.notify(queue: .main) {
                strongSelf.presenter.stopLoading()
                switch result {
                case .success(let creditScoreResponse):
                    return
                    // TODO: Implementar sucesso
                case .failure(let error):
                    strongSelf.handle(error: error)
                }
            }
        }
    }
    
    func close() {
        presenter.close()
    }
}
