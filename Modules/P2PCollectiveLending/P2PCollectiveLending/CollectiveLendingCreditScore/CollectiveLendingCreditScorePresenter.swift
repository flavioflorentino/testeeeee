import Foundation

protocol CollectiveLendingCreditScorePresenting: AnyObject {
    var viewController: CollectiveLendingCreditScoreDisplaying? { get set }
    func didNextStep(action: CollectiveLendingCreditScoreAction)
    func startLoading(title: String)
    func startProgressBar(withDuration: TimeInterval, completion: (() -> Void)?)
    func stopLoading()
    func displayError()
    func close()
}

final class CollectiveLendingCreditScorePresenter {
    private let coordinator: CollectiveLendingCreditScoreCoordinating
    weak var viewController: CollectiveLendingCreditScoreDisplaying?

    init(coordinator: CollectiveLendingCreditScoreCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - CollectiveLendingCreditScorePresenting
extension CollectiveLendingCreditScorePresenter: CollectiveLendingCreditScorePresenting {
    func startLoading(title: String) {
        viewController?.startLoading(title: title)
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
    
    func displayError() {
        viewController?.displayError()
    }
    
    func didNextStep(action: CollectiveLendingCreditScoreAction) {
        coordinator.perform(action: action)
    }
    
    func startProgressBar(withDuration: TimeInterval, completion: (() -> Void)?) {
        viewController?.startProgressBar(withDuration: withDuration, completion: completion)
    }
    
    func close() {
        coordinator.perform(action: .close)
    }
}
