import UIKit
import UI
import SafariServices

enum CollectiveLendingCreditScoreAction {
    case close
    case nonExistentScore(_ detailedError: IdentifiableCreditScoreError)
    case unavailableLimit(_ detailedError: IdentifiableCreditScoreError)
}

protocol CollectiveLendingCreditScoreCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: CollectiveLendingCreditScoreAction)
}

final class CollectiveLendingCreditScoreCoordinator {
    typealias Dependencies = HasURLOpener
    private let dependencies: Dependencies

    weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - CollectiveLendingCreditScoreCoordinating
extension CollectiveLendingCreditScoreCoordinator: CollectiveLendingCreditScoreCoordinating {
    func perform(action: CollectiveLendingCreditScoreAction) {
        switch action {
        case .close:
            viewController?.dismiss(animated: true, completion: nil)
        case let .nonExistentScore(error):
            showNonExistentScoreFeedbackController(error)
        case let .unavailableLimit(error):
            showUnavailableLimitFeedbackController(error)
        }
    }
}

private extension CollectiveLendingCreditScoreCoordinator {
    func openUrl(_ url: URL) {
        guard dependencies.urlOpener?.canOpenURL(url) == true else { return }
        dependencies.urlOpener?.open(url, options: [:], completionHandler: nil)
    }
    
    func makeApolloFeedbackViewController(error: IdentifiableCreditScoreError) -> ApolloFeedbackViewController {
        let content = error.asFeedbackViewContent
        viewController?.navigationController?.setNavigationBarHidden(true, animated: true)
        return ApolloFeedbackViewController(content: content)
    }
    
    func showNonExistentScoreFeedbackController(_ error: IdentifiableCreditScoreError) {
        let apolloFeedbackController = makeApolloFeedbackViewController(error: error)
        
        apolloFeedbackController.didTapPrimaryButton = {
            apolloFeedbackController.dismiss(animated: true, completion: nil)
        }
        
        apolloFeedbackController.didTapSecondaryButton = { [weak self] in
            guard let self = self else { return }
            guard let url = error.asDetailedError.url else { return }
            self.openUrl(url)
        }
        
        viewController?.show(apolloFeedbackController, sender: nil)
    }
    
    func showUnavailableLimitFeedbackController(_ error: IdentifiableCreditScoreError) {
        let apolloFeedbackController = makeApolloFeedbackViewController(error: error)
        
        apolloFeedbackController.didTapPrimaryButton = {
            // TODO: Quando colocado o link correto, verificar se será necessário essa lógica
            self.viewController?.dismiss(animated: true) { [weak self] in
                guard let self = self else { return }
                guard let url = error.asDetailedError.url else { return }
                self.openUrl(url)
            }
        }
        
        apolloFeedbackController.didTapSecondaryButton = {
            apolloFeedbackController.dismiss(animated: true, completion: nil)
        }
        
        viewController?.show(apolloFeedbackController, sender: nil)
    }
}
