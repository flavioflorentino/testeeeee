import UIKit

enum CollectiveLendingCreditScoreFactory {
    static func make() -> CollectiveLendingCreditScoreViewController {
        let container = DependencyContainer()
        let service: CollectiveLendingCreditScoreServicing = CollectiveLendingCreditScoreService(dependencies: container)
        let coordinator: CollectiveLendingCreditScoreCoordinating = CollectiveLendingCreditScoreCoordinator(dependencies: container)
        let presenter: CollectiveLendingCreditScorePresenting = CollectiveLendingCreditScorePresenter(coordinator: coordinator)
        let interactor = CollectiveLendingCreditScoreInteractor(service: service, presenter: presenter, dependencies: container)
        let viewController = CollectiveLendingCreditScoreViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
