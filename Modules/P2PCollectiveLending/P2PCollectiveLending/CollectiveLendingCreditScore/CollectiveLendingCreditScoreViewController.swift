import UI
import UIKit

protocol CollectiveLendingCreditScoreDisplaying: AnyObject {
    func startLoading(title: String)
    func startProgressBar(withDuration: TimeInterval, completion: (() -> Void)?)
    func stopLoading()
    func displayError()
}

private extension CollectiveLendingCreditScoreViewController.Layout { }

final class CollectiveLendingCreditScoreViewController: ViewController<CollectiveLendingCreditScoreInteracting, UIView> {
    fileprivate enum Layout { }
    
    private typealias Localizable = Strings.CollectiveLending.CreditScore
    
    lazy var loadingView: LoadingAnimationView = {
        let loading = LoadingAnimationView(hideProgressView: false)
        loading.isHidden = true
        return loading
    }()
    
    lazy var errorView: UIView = {
        let content = ApolloFeedbackViewContent(image: Assets.collectiveLendingFigureError.image,
                                                title: Localizable.genericErrorTitle,
                                                description: NSAttributedString(string: Localizable.genericErrorDescription),
                                                primaryButtonTitle: Localizable.genericErrorTryAgainButton,
                                                secondaryButtonTitle: Localizable.genericErrorDismissButton)
        let controller = ApolloFeedbackViewController(content: content)
        
        controller.didTapPrimaryButton = { [weak self] in
            self?.errorView.isHidden = true
            self?.interactor.fetchCreditScore()
        }
        
        controller.didTapSecondaryButton = { [weak self] in
            self?.interactor.close()
        }
        
        controller.view.isHidden = true
        return controller.view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.fetchCreditScore()
    }

    override func buildViewHierarchy() {
        view.addSubview(loadingView)
        view.addSubview(errorView)
    }
    
    override func setupConstraints() {
        loadingView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.size.equalToSuperview()
        }
        
        errorView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.size.equalToSuperview()
        }
    }

    override func configureViews() { }
}

// MARK: - CollectiveLendingCreditScoreDisplaying
extension CollectiveLendingCreditScoreViewController: CollectiveLendingCreditScoreDisplaying {
    func startLoading(title: String) {
        loadingView.isHidden = false
        loadingView.start(with: title)
        navigationController?.isNavigationBarHidden = true
    }
    
    func startProgressBar(withDuration: TimeInterval, completion: (() -> Void)?) {
        loadingView.startProgressBar(withDuration: withDuration, completion: completion)
    }
    
    func stopLoading() {
        loadingView.isHidden = true
        loadingView.progressValue = 0
        navigationController?.isNavigationBarHidden = false
    }
    
    func displayError() {
        errorView.isHidden = false
        navigationController?.isNavigationBarHidden = true
    }
}
