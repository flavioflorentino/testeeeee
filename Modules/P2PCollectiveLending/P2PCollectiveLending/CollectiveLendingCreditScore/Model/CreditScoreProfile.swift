struct CreditScoreProfile: Decodable {
    let riskGroupCharacter: String
    let limitGranted: Double
    let monthlyInterestRate: Double
}
