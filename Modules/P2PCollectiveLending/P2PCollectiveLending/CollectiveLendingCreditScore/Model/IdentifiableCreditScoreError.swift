enum IdentifiableCreditScoreError: String {
    case nonExistentScore = "8001"
    case unavailableLimit = "8002"
}

extension IdentifiableCreditScoreError: DetailedErrorConvertible {
    private typealias Localizable = Strings.CollectiveLending.DetailedError
    
    var asDetailedError: DetailedError {
        var image: UIImage
        var title: String
        var primaryButtonTitle: String
        var message: String
        var linkButtonTitle: String
        var url: URL?
        switch self {
        case .nonExistentScore:
            image = Assets.collectiveLendingFigureNonexistentScoreError.image
            title = Localizable.nonexistentScoreTitle
            message = Localizable.nonexistentScoreBody
            primaryButtonTitle = Localizable.nonexistentScorePrimaryButtonTitle
            linkButtonTitle = Localizable.nonexistentScoreLinkButtonTitle
            url = URL(string: "picpay://picpay") // TODO: Trocar pelo link correto
        case .unavailableLimit:
            image = Assets.collectiveLendingFigureUnavailableLimitError.image
            title = Localizable.unavailableLimitTitle
            message = Localizable.unavailableLimitBody
            primaryButtonTitle = Localizable.unavailableLimitPrimaryButtonTitle
            linkButtonTitle = Localizable.unavailableLimitLinkButtonTitle
            url = URL(string: "picpay://picpay") // TODO: Trocar pelo link correto
        }
        return DetailedError(
            image: image,
            title: title,
            message: message,
            primaryButtonTitle: primaryButtonTitle,
            linkButtonTitle: linkButtonTitle,
            url: url
        )
    }
}
