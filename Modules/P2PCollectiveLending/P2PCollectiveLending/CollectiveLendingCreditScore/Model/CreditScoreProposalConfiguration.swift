struct CreditScoreProposalConfiguration: Decodable {
    let minimumAmount: Double
    let maximumInstallment: Int
}
