import Foundation

struct CreditScoreResponse: Decodable {
    let profile: CreditScoreProfile
    let proposalConfiguration: CreditScoreProposalConfiguration
}
