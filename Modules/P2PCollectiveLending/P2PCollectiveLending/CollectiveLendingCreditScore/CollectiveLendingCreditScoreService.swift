import Core
import Foundation

protocol CollectiveLendingCreditScoreServicing: Servicing {
    func fetchCreditScore(completion: @escaping ModelCompletionBlock<CreditScoreResponse>)
}

final class CollectiveLendingCreditScoreService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - CollectiveLendingCreditScoreServicing
extension CollectiveLendingCreditScoreService: CollectiveLendingCreditScoreServicing {
    func fetchCreditScore(completion: @escaping ModelCompletionBlock<CreditScoreResponse>) {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        let request = Api<CreditScoreResponse>(endpoint: P2PCollectiveLendingEndpoint.creditScore)
        request.execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
