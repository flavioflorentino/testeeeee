import Core
import Foundation

protocol OffersListServicing {
}

final class OffersListService {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - OffersListServicing
extension OffersListService: OffersListServicing {
}
