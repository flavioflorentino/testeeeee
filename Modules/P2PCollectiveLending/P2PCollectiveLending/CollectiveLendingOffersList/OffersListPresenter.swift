import Foundation
import UI

protocol OffersListPresenting: AnyObject {
    var viewController: OffersListDisplaying? { get set }
    func updateOfferSummary(with summary: OfferSummaryModel)
    func presentSummaryEmptyState()
}

final class OffersListPresenter {
    private typealias Localizable = Strings.CollectiveLending.OffersList

    private let coordinator: OffersListCoordinating
    weak var viewController: OffersListDisplaying?

    init(coordinator: OffersListCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - OffersListPresenting
extension OffersListPresenter: OffersListPresenting {
    func updateOfferSummary(with summary: OfferSummaryModel) {
        let model = OffersSummaryDisplayingModel(model: summary)
        viewController?.diaplaySummaryInfo(with: model.summaryStringFormated)
        viewController?.setContinueButtonEnabled(isEnabled: true)
    }
    
    func presentSummaryEmptyState() {
        viewController?.setSummaryEmptyState()
        viewController?.setContinueButtonEnabled(isEnabled: false)
    }
}
