import UIKit

enum OffersListAction {
}

protocol OffersListCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: OffersListAction)
}

final class OffersListCoordinator {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - OffersListCoordinating
extension OffersListCoordinator: OffersListCoordinating {
    func perform(action: OffersListAction) { }
}
