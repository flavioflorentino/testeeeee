import Foundation
import UI

struct OffersSummaryDisplayingModel {
    private typealias Localizable = Strings.CollectiveLending.OffersList
    
    let model: OfferSummaryModel
    
    private var titleStyle: [NSAttributedString.Key: Any] {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        return [
            .font: Typography.title(.medium).font(),
            .foregroundColor: Colors.grayscale700.color,
            .paragraphStyle: paragraphStyle
        ]
    }
    
    private var highlightedStyle: [NSAttributedString.Key: Any] {
        var style = titleStyle
        style[NSAttributedString.Key.foregroundColor] = Colors.neutral600.color
        return style
    }
    
    var summaryStringFormated: NSMutableAttributedString {
        let summaryQuantity = Localizable.summaryObjectives(model.quantity)
        let valueString = model.value.toCurrencyString() ?? ""
        let fullString = Localizable.summaryInfo(valueString, summaryQuantity)
        
        let attributedString = NSMutableAttributedString(string: fullString, attributes: titleStyle)
        attributedString.addAttributes(highlightedStyle,
                                       range: (fullString as NSString).range(of: summaryQuantity))
        attributedString.addAttributes(highlightedStyle,
                                       range: (fullString as NSString).range(of: valueString))
        return attributedString
    }
}
