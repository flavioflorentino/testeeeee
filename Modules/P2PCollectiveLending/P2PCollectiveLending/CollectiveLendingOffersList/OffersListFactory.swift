import UIKit

enum OffersListFactory {
    static func make() -> OffersListViewController {
        let container = DependencyContainer()
        let service: OffersListServicing = OffersListService(dependencies: container)
        let coordinator: OffersListCoordinating = OffersListCoordinator(dependencies: container)
        let presenter: OffersListPresenting = OffersListPresenter(coordinator: coordinator)
        let interactor = OffersListInteractor(service: service, presenter: presenter, dependencies: container)
        let viewController = OffersListViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
