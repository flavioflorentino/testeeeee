import UI
import UIKit

protocol OffersListDisplaying: AnyObject {
    func diaplaySummaryInfo(with attributedString: NSAttributedString)
    func setContinueButtonEnabled(isEnabled: Bool)
    func setSummaryEmptyState()
}

private extension OffersListViewController.Layout {
    enum Size {
        static let imageHeight: CGFloat = 90.0
    }
    
    enum ShadowView {
        static let height: CGFloat = 10.0
        static let alpha: CGFloat = 0.1
        static let colorLocations: [NSNumber] = [0.3, 0.95]
    }
}

final class OffersListViewController: ViewController<OffersListInteracting, UIView> {
    private typealias Localizable = Strings.CollectiveLending.OffersList
    fileprivate enum Layout { }
    
    private lazy var offersTableView: UITableView = {
        let tableView = UITableView()
        return tableView
    }()
    
    private lazy var offerSummaryView: OffersSummaryView = {
        let offer = OffersSummaryView()
        offer.emptyLabel.text = Localizable.summaryEmptyText
        offer.continueButton.setTitle(Localizable.continueButtonTitle,
                                      for: .normal)
        offer.continueButton.addTarget(self,
                                       action: #selector(didTouchContinueButton),
                                       for: .touchUpInside)
        return offer
    }()
    
    private lazy var shadowView: UIView = {
        let shadowView = UIView()
        shadowView.alpha = Layout.ShadowView.alpha
        return shadowView
    }()
    
    private lazy var gradientShadow: CAGradientLayer = {
        let gradient = CAGradientLayer()
        gradient.frame = shadowView.bounds
        gradient.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
        gradient.locations = Layout.ShadowView.colorLocations
        return gradient
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func buildViewHierarchy() {
        view.addSubview(offersTableView)
        view.addSubview(shadowView)
        view.addSubview(offerSummaryView)
    }
    
    override func setupConstraints() {
        offerSummaryView.snp.makeConstraints {
            $0.leading.trailing.bottom.equalToSuperview()
        }
        
        offersTableView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(offerSummaryView.snp.top)
        }
        
        shadowView.snp.makeConstraints {
            $0.bottom.leading.trailing.equalTo(offersTableView)
            $0.height.equalTo(Layout.ShadowView.height)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        setupNavigationAppearance()
    }
    
    private func setupNavigationAppearance() {
        navigationController?.view.backgroundColor = Colors.backgroundPrimary.color
        navigationItem.title = Localizable.title
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .automatic
            navigationController?.navigationBar.prefersLargeTitles = true
            navigationController?.navigationBar.isTranslucent = true
            navigationController?.navigationBar.largeTitleTextAttributes = [.font: Typography.title(.large).font()]
        }
        if #available(iOS 13.0, *) {
            let scrollEdgeAppearance = UINavigationBarAppearance()
            let standardAppearance = UINavigationBarAppearance()
            scrollEdgeAppearance.configureWithTransparentBackground()
            standardAppearance.configureWithDefaultBackground()
            navigationController?.navigationBar.scrollEdgeAppearance = scrollEdgeAppearance
            navigationController?.navigationBar.standardAppearance = standardAppearance
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        shadowView.layer.addSublayer(gradientShadow)
    }
}

@objc private extension OffersListViewController {
    func didTouchContinueButton() { }
}

// MARK: - OffersListDisplaying
extension OffersListViewController: OffersListDisplaying {
    func diaplaySummaryInfo(with attributedString: NSAttributedString) {
        offerSummaryView.setSummaryValue(with: attributedString)
    }
    
    func setContinueButtonEnabled(isEnabled: Bool) {
        offerSummaryView.continueButton.isEnabled = isEnabled
    }
    
    func setSummaryEmptyState() {
        offerSummaryView.setEmptyState()
    }
}
