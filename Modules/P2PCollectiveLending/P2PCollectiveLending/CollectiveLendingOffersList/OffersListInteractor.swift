import Foundation

protocol OffersListInteracting: AnyObject { }

final class OffersListInteractor {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let service: OffersListServicing
    private let presenter: OffersListPresenting

    init(service: OffersListServicing, presenter: OffersListPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - OffersListInteracting
extension OffersListInteractor: OffersListInteracting { }
