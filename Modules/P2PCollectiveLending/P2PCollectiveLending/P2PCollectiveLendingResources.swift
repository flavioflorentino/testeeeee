import Foundation

// swiftlint:disable convenience_type
final class P2PCollectiveLendingResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: P2PCollectiveLendingResources.self).url(forResource: "P2PCollectiveLendingResources", withExtension: "bundle") else {
            return Bundle(for: P2PCollectiveLendingResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: P2PCollectiveLendingResources.self)
    }()
}
