import Foundation
import Core

enum P2PCollectiveLendingEndpoint {
    case creditScore
}

extension P2PCollectiveLendingEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .creditScore:
            return "collective-lending/borrower/config"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .creditScore:
            return .get
        }
    }
    
    var parameters: [String: Any] {
        switch self {
        default:
            return [:]
        }
    }
    
    var body: Data? {
        switch self {
        default:
            return nil
        }
    }
    
    var customHeaders: [String: String] {
        switch self {
        default:
            return [:]
        }
    }
}
