import Foundation
import AssetsKit
import UI

struct OfferSummaryModel {
    var quantity: Int
    var value: Double
}

private extension OffersSummaryView.Layout {
    enum Size {
        static let warningImageSize: CGFloat = 20.0
    }
    
    enum Insets {
        static let stackviewLayoutMargins = EdgeInsets(
            top: Spacing.base02,
            leading: Spacing.base02,
            bottom: Spacing.base03,
            trailing: Spacing.base02
        )
    }
}

final class OffersSummaryView: UIView {
    fileprivate enum Layout {}
            
    private lazy var summaryValueLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.isHidden = true
        return label
    }()
    
    private lazy var warningImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Resources.Icons.icoBlueInfo.image
        return imageView
    }()
    
    private(set) lazy var emptyLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.grayscale700.color)
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var emptyStackView: UIStackView = {
        let stackview = UIStackView(arrangedSubviews: [warningImage, emptyLabel])
        stackview.axis = .vertical
        stackview.spacing = Spacing.base01
        stackview.alignment = .center
        stackview.distribution = .equalSpacing
        return stackview
    }()
    
    private(set) lazy var continueButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.isEnabled = false
        return button
    }()
    
    private lazy var stackview: UIStackView = {
        let stackview = UIStackView(arrangedSubviews: [summaryValueLabel, emptyStackView, continueButton])
        stackview.axis = .vertical
        stackview.spacing = Spacing.base02
        stackview.compatibleLayoutMargins = Layout.Insets.stackviewLayoutMargins
        stackview.isLayoutMarginsRelativeArrangement = true
        return stackview
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension OffersSummaryView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(stackview)
    }
    
    func setupConstraints() {
        stackview.snp.makeConstraints {
            $0.directionalEdges.equalToSuperview()
        }
        
        warningImage.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.warningImageSize)
        }
    }
}

extension OffersSummaryView {
    func setSummaryValue(with attributedString: NSAttributedString) {
        summaryValueLabel.attributedText = attributedString
        summaryValueLabel.isHidden = false
        emptyStackView.isHidden = true
    }
    
    func setEmptyState() {
        summaryValueLabel.attributedText = nil
        summaryValueLabel.isHidden = true
        emptyStackView.isHidden = false
    }
}
