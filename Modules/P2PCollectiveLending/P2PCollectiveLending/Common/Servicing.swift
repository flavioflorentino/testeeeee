import Core

protocol Servicing {
    typealias ModelCompletionBlock<T: Decodable> = (Result<T, ApiError>) -> Void
}
