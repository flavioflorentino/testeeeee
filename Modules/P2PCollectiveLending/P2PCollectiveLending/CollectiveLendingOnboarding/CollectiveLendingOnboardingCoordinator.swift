import SafariServices
import UIKit

enum CollectiveLendingOnboardingAction: Equatable {
    case creditScore
    case close
}

protocol CollectiveLendingOnboardingCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: CollectiveLendingOnboardingAction)
    func showHelp(_ url: URL)
    func showTermsOfUse(for url: URL)
}

final class CollectiveLendingOnboardingCoordinator {
    typealias Dependencies = HasURLOpener
    private let dependencies: Dependencies

    weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - CollectiveLendingOnboardingCoordinating
extension CollectiveLendingOnboardingCoordinator: CollectiveLendingOnboardingCoordinating {
    func perform(action: CollectiveLendingOnboardingAction) {
        switch action {
        case .creditScore:
            let controller = CollectiveLendingCreditScoreFactory.make()
            viewController?.show(controller, sender: self)
        case .close:
            viewController?.dismiss(animated: true, completion: nil)
        }
    }
    
    func showHelp(_ url: URL) {
        guard dependencies.urlOpener?.canOpenURL(url) == true else {
            return
        }
        dependencies.urlOpener?.open(url, options: [:], completionHandler: nil)
    }
    
    func showTermsOfUse(for url: URL) {
        let safariViewController = SFSafariViewController(url: url)
        viewController?.present(safariViewController, animated: true, completion: nil)
    }
}
