import UIKit
import UI

class LendingCustomPageControl: UIControl {
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fillProportionally
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    var currentPage: Int = 0 {
        didSet {
            updateDots()
        }
    }
    
    var numOfPages: Int = 0 {
        didSet {
            setupStackView()
        }
    }
    
    var pageIndicatorTintColor: UIColor? = Colors.grayscale200.color
    var currentPageIndicatorTintColor: UIColor? = Colors.branding600.color
    private var previousPage: Int = 0
}

extension LendingCustomPageControl: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(stackView)
    }
    
    func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }
}

private extension LendingCustomPageControl {
    func setupStackView() {
        for index in 0..<numOfPages {
            let view = getDotView(index)
            stackView.addArrangedSubview(view)
            view.snp.makeConstraints {
                $0.width.equalTo(index == currentPage ? Sizing.base02 : Sizing.base01)
                $0.height.equalTo(Sizing.base01)
            }
        }
        buildLayout()
    }
    
    func getDotView(_ index: Int) -> UIView {
        let dot = UIView()
        dot.transform = .identity
        dot.layer.cornerRadius = Sizing.base00
        dot.layer.masksToBounds = true
        dot.backgroundColor = currentPage == index ? currentPageIndicatorTintColor : pageIndicatorTintColor
        return dot
    }
    
    func updateDots() {
        let previousPageDot = stackView.arrangedSubviews[previousPage]
        let currentPageDot = stackView.arrangedSubviews[currentPage]
        UIView.animate(withDuration: 0.3) {
            previousPageDot.backgroundColor = self.pageIndicatorTintColor
            previousPageDot.snp.updateConstraints {
                $0.width.equalTo(Sizing.base01)
            }
            currentPageDot.backgroundColor = self.currentPageIndicatorTintColor
            currentPageDot.snp.updateConstraints {
                $0.width.equalTo(Sizing.base02)
            }
            self.layoutIfNeeded()
        }
        previousPage = currentPage
    }
}
