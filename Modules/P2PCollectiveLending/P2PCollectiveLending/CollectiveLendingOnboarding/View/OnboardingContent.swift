import UI
import UIKit

private extension OnboardingContent.Layout {
    enum Size {
        static let imageSize: CGFloat = 144.0
    }
}

final class OnboardingContent: UICollectionViewCell {
    fileprivate enum Layout {}
    
    private lazy var onboardingImage: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, Colors.grayscale900.color)
            .with(\.textAlignment, .center)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale600.color)
            .with(\.textAlignment, .center)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var stackView: UIStackView = {
       let stackView = UIStackView(arrangedSubviews: [
                                    onboardingImage,
                                    titleLabel,
                                    subtitleLabel
       ])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        stackView.alignment = .center
        stackView.distribution = .equalSpacing
        return stackView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension OnboardingContent {
    func setup(_ data: OnboardingContentModel) {
        onboardingImage.image = data.image
        titleLabel.text = data.title
        subtitleLabel.text = data.subtitle
    }
}

extension OnboardingContent: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(stackView)
    }
    
    func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.directionalEdges.equalToSuperview()
        }
        
        onboardingImage.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.imageSize)
        }
    }
}
