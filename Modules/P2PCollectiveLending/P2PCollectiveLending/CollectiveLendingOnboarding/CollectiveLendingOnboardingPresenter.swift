import Foundation
import UI

protocol CollectiveLendingOnboardingPresenting: AnyObject {
    var viewController: CollectiveLendingOnboardingDisplaying? { get set }
    func didNextStep(action: CollectiveLendingOnboardingAction)
    func presentOnboardingContent(with pages: [OnboardingContentModel])
    func setupButtons(isOnLastPage: Bool, isOnFirstPage: Bool)
    func showHelp(_ url: URL)
    func showTermsOfUse(for url: URL)
    func presentTermsOfServiceText(privacyPolicyLink: String, termsAndConditionsLink: String)
}

final class CollectiveLendingOnboardingPresenter {
    private typealias Localizable = Strings.CollectiveLending.Onboarding

    private let coordinator: CollectiveLendingOnboardingCoordinating
    weak var viewController: CollectiveLendingOnboardingDisplaying?

    init(coordinator: CollectiveLendingOnboardingCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - CollectiveLendingOnboardingPresenting
extension CollectiveLendingOnboardingPresenter: CollectiveLendingOnboardingPresenting {
    func presentOnboardingContent(with pages: [OnboardingContentModel]) {
        viewController?.displayOnboardingContent(with: pages)
    }
    
    func setupButtons(isOnLastPage: Bool, isOnFirstPage: Bool) {
        viewController?.displayButtonsSettings(isOnLastPage: isOnLastPage, isOnFirstPage: isOnFirstPage)
    }
    
    func showHelp(_ url: URL) {
        coordinator.showHelp(url)
    }
    
    func showTermsOfUse(for url: URL) {
        coordinator.showTermsOfUse(for: url)
    }
    
    func presentTermsOfServiceText(privacyPolicyLink: String, termsAndConditionsLink: String) {
        let text = Localizable.checkboxText as NSString
        let privacyPolicyRange = text.range(of: Localizable.privacyPolicies)
        let termsAndConditionsRange = text.range(of: Localizable.termsOfUse)
        let attributes: [NSAttributedString.Key: Any] = [
            .font: Typography.bodySecondary().font(),
            .foregroundColor: Colors.grayscale600.color
        ]
        let attributedString = NSMutableAttributedString(string: Localizable.checkboxText, attributes: attributes)
        attributedString.addAttribute(.link, value: privacyPolicyLink, range: privacyPolicyRange)
        attributedString.addAttribute(.link, value: termsAndConditionsLink, range: termsAndConditionsRange)
        viewController?.displayTermsOfConditions(using: attributedString)
    }
    
    func didNextStep(action: CollectiveLendingOnboardingAction) {
        coordinator.perform(action: action)
    }
}
