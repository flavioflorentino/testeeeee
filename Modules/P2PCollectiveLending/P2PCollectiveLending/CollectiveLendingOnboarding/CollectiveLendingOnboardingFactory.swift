import UIKit

public enum CollectiveLendingOnboardingFactory {
    public static func make(shouldDisplayCloseButton: Bool = false) -> UIViewController {
        let container = DependencyContainer()
        let coordinator: CollectiveLendingOnboardingCoordinating = CollectiveLendingOnboardingCoordinator(dependencies: container)
        let presenter: CollectiveLendingOnboardingPresenting = CollectiveLendingOnboardingPresenter(coordinator: coordinator)
        let interactor = CollectiveLendingOnboardingInteractor(presenter: presenter)
        let viewController = CollectiveLendingOnboardingViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController
        
        viewController.shouldDisplayCloseButton = shouldDisplayCloseButton
        
        return viewController
    }
}
