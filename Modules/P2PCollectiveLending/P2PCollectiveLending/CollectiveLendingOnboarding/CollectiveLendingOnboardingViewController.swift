import UI
import UIKit
import LendingComponents
import AssetsKit

protocol CollectiveLendingOnboardingDisplaying: AnyObject {
    func displayOnboardingContent(with pages: [OnboardingContentModel])
    func displayTermsOfConditions(using text: NSAttributedString)
    func displayButtonsSettings(isOnLastPage: Bool, isOnFirstPage: Bool)
}

private extension CollectiveLendingOnboardingViewController.Layout {
    enum Size {
        static let CarouselEstimatedHeight: CGFloat = 300.0
    }
}

final class CollectiveLendingOnboardingViewController: ViewController<CollectiveLendingOnboardingInteracting, UIView> {
    private typealias Localizable = Strings.CollectiveLending.Onboarding
    fileprivate enum Layout {}
    
    private lazy var onboardingCarousel: CarouselPageView<OnboardingContentModel, OnboardingContent> = {
        let caroselView = CarouselPageView<OnboardingContentModel, OnboardingContent>()
        caroselView.carouselPageDelegate = self
        return caroselView
    }()
    
    private lazy var pageControl: LendingCustomPageControl = {
        let pageControl = LendingCustomPageControl()
        pageControl.backgroundColor = .clear
        pageControl.isUserInteractionEnabled = false
        return pageControl
    }()
    
    private lazy var rightArrow: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = Typography.icons(.medium).font()
        button.setTitleColor(Colors.branding600.color, for: .normal)
        button.setTitle(Iconography.arrowRight.rawValue, for: .normal)
        button.addTarget(self, action: #selector(nextPage(_:)), for: .touchUpInside)
        return button
    }()
    
    private lazy var leftArrow: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = Typography.icons(.medium).font()
        button.setTitleColor(Colors.branding600.color, for: .normal)
        button.setTitle(Iconography.arrowLeft.rawValue, for: .normal)
        button.addTarget(self, action: #selector(previousPage(_:)), for: .touchUpInside)
        button.alpha = 0.0
        return button
    }()
    
    private lazy var pageControlStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [leftArrow, pageControl, rightArrow])
        stackView.axis = .horizontal
        stackView.spacing = Spacing.base01
        stackView.alignment = .center
        stackView.distribution = .equalCentering
        return stackView
    }()
    
    private lazy var topStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [onboardingCarousel, pageControlStackView])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        stackView.alignment = .fill
        return stackView
    }()
    
    private lazy var termsOfUseCheckbox: CheckboxView = {
        let checkboxView = CheckboxView()
        checkboxView.addTarget(self, action: #selector(checkboxButtonValueChanged), for: .valueChanged)
        checkboxView.checkboxButton.tintColor = Colors.branding600.color
        checkboxView.textView.delegate = self
        return checkboxView
    }()
    
    private lazy var continueButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.buttonTitle, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(continueButtonTapped), for: .touchUpInside)
        button.isEnabled = false
        return button
    }()
    
    private lazy var bottomStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [termsOfUseCheckbox, continueButton])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base03
        stackView.alpha = 0.0
        return stackView
    }()
    
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [topStackView, bottomStackView])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        stackView.distribution = .equalSpacing
        stackView.compatibleLayoutMargins = .rootView
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    
    private lazy var helpButtonItem: UIBarButtonItem = {
        let buttonItem = UIBarButtonItem(image: Resources.Icons.icoQuestion.image,
                                         style: .plain,
                                         target: self,
                                         action: #selector(helpButtonTapped))
        buttonItem.tintColor = Colors.branding600.color
        return buttonItem
    }()
    
    private lazy var skipButtonItem: UIBarButtonItem = {
        let buttonItem = UIBarButtonItem(title: Localizable.skipButtonTitle,
                                         style: .plain,
                                         target: self,
                                         action: #selector(skipButtonTapped))
        buttonItem.tintColor = Colors.branding600.color
        return buttonItem
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsVerticalScrollIndicator = false
        return scrollView
    }()
    
    private lazy var closeButton = UIBarButtonItem(title: Localizable.closeButtonTitle,
                                                   style: .plain,
                                                   target: self,
                                                   action: #selector(close))
    
    var shouldDisplayCloseButton = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.setupOnboardingViews()
        interactor.getTermsOfServiceText()
    }
	
    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(rootStackView)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        onboardingCarousel.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.CarouselEstimatedHeight)
        }
        
        pageControlStackView.snp.makeConstraints {
            $0.height.equalTo(Sizing.base05)
        }
        
        rootStackView.snp.makeConstraints {
            $0.edges.width.equalToSuperview()
            $0.height.greaterThanOrEqualTo(view.layoutMarginsGuide)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        navigationController?.view.backgroundColor = Colors.backgroundPrimary.color
        if shouldDisplayCloseButton {
            navigationItem.leftBarButtonItem = closeButton
        }
        setupNavigationAppearance()
    }
    
    private func setupNavigationAppearance() {
        navigationItem.rightBarButtonItems = [skipButtonItem]
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .automatic
            navigationController?.navigationBar.prefersLargeTitles = true
            navigationController?.navigationBar.isTranslucent = true
        }
        if #available(iOS 13.0, *) {
            let scrollEdgeAppearance = UINavigationBarAppearance()
            let standardAppearance = UINavigationBarAppearance()
            scrollEdgeAppearance.configureWithTransparentBackground()
            standardAppearance.configureWithDefaultBackground()
            navigationController?.navigationBar.scrollEdgeAppearance = scrollEdgeAppearance
            navigationController?.navigationBar.standardAppearance = standardAppearance
        }
    }
}

// MARK: - CollectiveLendingOnboardingDisplaying
extension CollectiveLendingOnboardingViewController: CollectiveLendingOnboardingDisplaying {
    func displayOnboardingContent(with pages: [OnboardingContentModel]) {
        pageControl.numOfPages = pages.count
        
        onboardingCarousel.setup(data: pages) {  _, model, cell in
            cell.setup(model)
        }
    }
    
    func displayTermsOfConditions(using text: NSAttributedString) {
        termsOfUseCheckbox.textView.attributedText = text
    }
    
    func displayButtonsSettings(isOnLastPage: Bool, isOnFirstPage: Bool) {
        hidePreviousPageButton(isHidden: isOnFirstPage)
        hideNextPageButton(isHidden: isOnLastPage)
    }
}

// MARK: - UITextViewDelegate
extension CollectiveLendingOnboardingViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        interactor.showTermsOfUse(for: URL)
        return false
    }
}

// MARK: - @objc Private Methods
@objc private extension CollectiveLendingOnboardingViewController {
    func nextPage(_ sender: UIButton) {
        onboardingCarousel.scrollToNextPage()
    }
    
    func previousPage(_ sender: UIButton) {
        onboardingCarousel.scrollToPreviousPage()
    }
    
    func checkboxButtonValueChanged(_ checkboxView: CheckboxView) {
        continueButton.isEnabled = checkboxView.isSelected
    }
    
    func continueButtonTapped() {
        interactor.showCreditScore()
    }
    
    func helpButtonTapped() {
        interactor.showHelp()
    }
    
    func skipButtonTapped() {
        onboardingCarousel.scrollToLastPage()
    }
    
    func close() {
        interactor.close()
    }
}

// MARK: - Private Methods
private extension CollectiveLendingOnboardingViewController {
    func hidePreviousPageButton(isHidden: Bool) {
        UIView.animate(withDuration: 0.2) {
            self.leftArrow.alpha = isHidden ? 0.0 : 1.0
            self.leftArrow.isUserInteractionEnabled = !isHidden
        }
    }
    
    func hideNextPageButton(isHidden: Bool) {
        UIView.animate(withDuration: 0.2) {
            self.rightArrow.alpha = isHidden ? 0.0 : 1.0
            self.rightArrow.isUserInteractionEnabled = !isHidden
            self.showCheckboxButton(isHidden)
        }
    }
    
    func showCheckboxButton(_ isHidden: Bool) {
        bottomStackView.alpha = isHidden ? 1.0 : 0.0
        let navButton = isHidden ? self.helpButtonItem : self.skipButtonItem
        navigationItem.setRightBarButton(navButton, animated: false)
    }
}

extension CollectiveLendingOnboardingViewController: CarouselPageViewDelegate {
    func didScrollToPage(at pageIndex: Int) {
        pageControl.currentPage = pageIndex
        interactor.didScrollToPage(at: pageIndex)
    }
}
