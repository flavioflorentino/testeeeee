import AssetsKit

protocol OnboardingSectionContent {
    var onboardingContent: OnboardingContentModel { get }
}

enum OnboardingSection {
    case help
    case installments
    case contract
    case ending
}

extension OnboardingSection: OnboardingSectionContent {
    private typealias Localizable = Strings.CollectiveLending.Onboarding
    
    var onboardingContent: OnboardingContentModel {
        switch self {
        case .help:
            return OnboardingContentModel(image: Resources.Illustrations.iluSocialInteractionWithBackgorund.image,
                                          title: Localizable.helpTitle,
                                          subtitle: Localizable.helpSubtitle)
        case .installments:
            return OnboardingContentModel(image: Resources.Illustrations.iluGirlCoinSquareBackground.image,
                                          title: Localizable.installmentsTitle,
                                          subtitle: Localizable.installmentsSubtitle)
        case .contract:
            return OnboardingContentModel(image: Resources.Illustrations.iluFillingContract.image,
                                          title: Localizable.contractTitle,
                                          subtitle: Localizable.contractSubtitle)
        case .ending:
            return OnboardingContentModel(image: Resources.Illustrations.iluTakingSelfieBlueBackground.image,
                                          title: Localizable.endingTitle,
                                          subtitle: Localizable.endingSubtitle)
        }
    }
}
