struct OnboardingViewModel {
    static let privacyPolicyLink = "https://picpay.s3-sa-east-1.amazonaws.com/p2p_lending/Crednovo+-+Poli%CC%81tica+de+Privacidade.pdf"
    static let termsAndConditionsLink = "https://picpay.s3-sa-east-1.amazonaws.com/p2p_lending/Crednovo+-+Termos+e++Condic%CC%A7o%CC%83es+de+Uso+da+Plataforma.pdf"
}
