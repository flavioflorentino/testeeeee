struct OnboardingContentModel {
    let image: UIImage
    let title: String
    let subtitle: String
}
