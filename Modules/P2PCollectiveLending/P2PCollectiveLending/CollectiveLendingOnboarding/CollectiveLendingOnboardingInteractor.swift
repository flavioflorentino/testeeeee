import Foundation

protocol CollectiveLendingOnboardingInteracting: AnyObject {
    func setupOnboardingViews()
    func didScrollToPage(at pageIndex: Int)
    func showCreditScore()
    func getTermsOfServiceText()
    func showHelp()
    func showTermsOfUse(for url: URL)
    func close()
}

final class CollectiveLendingOnboardingInteractor {
    private let presenter: CollectiveLendingOnboardingPresenting
    
    private let pages: [OnboardingContentModel] = [
        OnboardingSection.help.onboardingContent,
        OnboardingSection.installments.onboardingContent,
        OnboardingSection.contract.onboardingContent,
        OnboardingSection.ending.onboardingContent
    ]
    
    init(presenter: CollectiveLendingOnboardingPresenting) {
        self.presenter = presenter
    }
}

// MARK: - CollectiveLendingOnboardingInteracting
extension CollectiveLendingOnboardingInteractor: CollectiveLendingOnboardingInteracting {
    func showCreditScore() {
        presenter.didNextStep(action: .creditScore)
    }
    
    func setupOnboardingViews() {
        presenter.presentOnboardingContent(with: pages)
    }
    
    func didScrollToPage(at pageIndex: Int) {
        presenter.setupButtons(isOnLastPage: pageIndex == pages.count - 1,
                               isOnFirstPage: pageIndex == 0)
    }
    
    func getTermsOfServiceText() {
        presenter.presentTermsOfServiceText(privacyPolicyLink: OnboardingViewModel.privacyPolicyLink,
                                            termsAndConditionsLink: OnboardingViewModel.termsAndConditionsLink)
    }
    
    func showHelp() {
        guard let url = URL(string: "picpay://picpay/helpcenter/article/360049387652") else {
            return
        }
        presenter.showHelp(url)
    }
    
    func showTermsOfUse(for url: URL) {
        presenter.showTermsOfUse(for: url)
    }
    
    func close() {
        presenter.didNextStep(action: .close)
    }
}
