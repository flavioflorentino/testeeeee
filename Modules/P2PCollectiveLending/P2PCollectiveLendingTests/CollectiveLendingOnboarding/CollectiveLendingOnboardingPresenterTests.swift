import XCTest
@testable import P2PCollectiveLending

private final class CollectiveLendingOnboardDisplaySpy: CollectiveLendingOnboardingDisplaying {
    private(set) var callDisplayTermsOfConditionsCount = 0
    private(set) var callDisplayOnboardingContent = 0
    private(set) var callDisplayButtonSettings = 0
    
    private(set) var termsOfConditionsText: NSAttributedString?
    private(set) var contentPages = [OnboardingContentModel]()
    private(set) var isOnLastPage = false
    private(set) var isOnFirstPage = false
    
    func displayTermsOfConditions(using text: NSAttributedString) {
        callDisplayTermsOfConditionsCount += 1
        termsOfConditionsText = text
    }
    
    func displayOnboardingContent(with pages: [OnboardingContentModel]) {
        callDisplayOnboardingContent += 1
        contentPages = pages
    }
    
    func displayButtonsSettings(isOnLastPage: Bool, isOnFirstPage: Bool) {
        callDisplayButtonSettings += 1
        self.isOnFirstPage = isOnFirstPage
        self.isOnLastPage = isOnLastPage
    }
}

private final class CollectiveLendingOnboardCoordinatorSpy: CollectiveLendingOnboardingCoordinating {
    var viewController: UIViewController?
    
    private(set) var callShowHelpCount = 0
    private(set) var callShowTermsOfUseCount = 0
    private(set) var callPerformCount = 0
    
    private(set) var helpURL: URL?
    private(set) var termsOfUseURL: URL?
    private(set) var calledAction: CollectiveLendingOnboardingAction?
    
    func showHelp(_ url: URL) {
        callShowHelpCount += 1
        helpURL = url
    }
    
    func showTermsOfUse(for url: URL) {
        callShowTermsOfUseCount += 1
        termsOfUseURL = url
    }
    
    func perform(action: CollectiveLendingOnboardingAction) {
        callPerformCount += 1
        calledAction = action
    }
}

final class CollectiveLendingOnboardPresenterTests: XCTestCase {
    private lazy var viewControllerSpy = CollectiveLendingOnboardDisplaySpy()
    private lazy var coordinatorSpy = CollectiveLendingOnboardCoordinatorSpy()
    private lazy var sut: CollectiveLendingOnboardingPresenter = {
        let presenter = CollectiveLendingOnboardingPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testDidNextStep_WhenCalledFromInteractor_ShouldCallPerform() {
        sut.didNextStep(action: .creditScore)
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssertEqual(coordinatorSpy.calledAction, .creditScore)
    }
    
    func testPresentTermsOfServiceText_WhenCalledFromInteractor_ShouldCallDisplayTermsOfConditions() {
        sut.presentTermsOfServiceText(privacyPolicyLink: "", termsAndConditionsLink: "")
        
        XCTAssertEqual(viewControllerSpy.callDisplayTermsOfConditionsCount, 1)
    }
    
    func testPresentOnboardingContent_WhenCalledFromInteractor_ShouldCallDisplayOnboardingContent() {
        let pagesMock: [OnboardingContentModel] = [
            OnboardingSection.help.onboardingContent,
            OnboardingSection.installments.onboardingContent
        ]
        
        sut.presentOnboardingContent(with: pagesMock)
        
        XCTAssertEqual(viewControllerSpy.callDisplayOnboardingContent, 1)
    }
    
    func testSetupButtons_WhenCalledFromInteractor_ShouldCallDisplayButtonsSettings() {
        sut.setupButtons(isOnLastPage: true, isOnFirstPage: false)
        
        XCTAssertEqual(viewControllerSpy.callDisplayButtonSettings, 1)
    }
    
    func testShowHelp_WhenCalledFromInteractor_ShouldCallShowHelp() throws {
        let url = try XCTUnwrap(URL(string: "picpay://picpay/helpcenter/article/360049387652"))
        
        sut.showHelp(url)
        
        XCTAssertEqual(coordinatorSpy.callShowHelpCount, 1)
        XCTAssertEqual(coordinatorSpy.helpURL, url)
    }
    
    func testPresentTermsOfUse_WhenCalledFromInteractor_ShouldCallShowTersOfUse() throws {
        let url = try XCTUnwrap(URL(string: "https://www.picpay.com.br/"))
        
        sut.showTermsOfUse(for: url)
        
        XCTAssertEqual(coordinatorSpy.callShowTermsOfUseCount, 1)
        XCTAssertEqual(coordinatorSpy.termsOfUseURL, url)
    }
}
