import XCTest
@testable import P2PCollectiveLending

private final class CollectiveLendingOnboardingPresenterSpy: CollectiveLendingOnboardingPresenting {
    var viewController: CollectiveLendingOnboardingDisplaying?
    
    private(set) var callShowHelpCount = 0
    private(set) var callShowTermsOfUseCount = 0
    private(set) var callPresentOnboardingContent = 0
    private(set) var callSetupButtons = 0
    private(set) var callPresentTermsOfServiceTextCount = 0
    private(set) var callDidNextStepCount = 0
    
    private(set) var helpURL: URL?
    private(set) var termsOfUseURL: URL?
    private(set) var contentPages = [OnboardingContentModel]()
    private(set) var isOnLastPage = false
    private(set) var isOnFirstPage = false
    private(set) var privacyLink: String?
    private(set) var termsLink: String?
    private(set) var calledAction: CollectiveLendingOnboardingAction?
    
    func showHelp(_ url: URL) {
        callShowHelpCount += 1
        helpURL = url
    }
    
    func showTermsOfUse(for url: URL) {
        callShowTermsOfUseCount += 1
        termsOfUseURL = url
    }
    
    func presentOnboardingContent(with pages: [OnboardingContentModel]) {
        callPresentOnboardingContent += 1
        contentPages = pages
    }
    
    func setupButtons(isOnLastPage: Bool, isOnFirstPage: Bool) {
        callSetupButtons += 1
        self.isOnLastPage = isOnLastPage
        self.isOnFirstPage = isOnFirstPage
    }
    
    func presentTermsOfServiceText(privacyPolicyLink: String, termsAndConditionsLink: String) {
        callPresentTermsOfServiceTextCount += 1
        privacyLink = privacyPolicyLink
        termsLink = termsAndConditionsLink
    }
    
    func didNextStep(action: CollectiveLendingOnboardingAction) {
        callDidNextStepCount += 1
        calledAction = action
    }
}

final class CollectiveLendingOnboardInteractorTests: XCTestCase {
    private lazy var presenterSpy = CollectiveLendingOnboardingPresenterSpy()
    private lazy var sut = CollectiveLendingOnboardingInteractor(presenter: presenterSpy)
    
    func testShowCreditScore_WhenCalledFromViewController_ShouldCallDidNextStep() throws {
        sut.showCreditScore()
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.calledAction, .creditScore)
    }
    
    func testShowHelp_WhenCalledFromViewController_ShouldCallShowHelp() throws {
        let url = try XCTUnwrap(URL(string: "picpay://picpay/helpcenter/article/360049387652"))
        
        sut.showHelp()
        
        XCTAssertEqual(presenterSpy.callShowHelpCount, 1)
        XCTAssertEqual(presenterSpy.helpURL, url)
    }
    
    func testPresentTermsOfUse_WhenCalledFromViewController_ShouldCallShowTersOfUse() throws {
        let url = try XCTUnwrap(URL(string: "https://www.picpay.com.br/"))
        
        sut.showTermsOfUse(for: url)
        
        XCTAssertEqual(presenterSpy.callShowTermsOfUseCount, 1)
        XCTAssertEqual(presenterSpy.termsOfUseURL, url)
    }
    
    func testPresentOnboardingContent_WhenCalledFromViewController_ShouldCallDisplayOnboardingContent() {
        sut.setupOnboardingViews()
        
        XCTAssertEqual(presenterSpy.callPresentOnboardingContent, 1)
    }
    
    func testDidScrollToPage_WhenCalledFromViewController_ShouldCallSetupButtons() {
        let firstPageIndex = 0
        let lastPageIndex = 3
        
        sut.didScrollToPage(at: firstPageIndex)
        
        XCTAssertEqual(presenterSpy.callSetupButtons, 1)
        XCTAssertTrue(presenterSpy.isOnFirstPage)
        XCTAssertFalse(presenterSpy.isOnLastPage)
        
        sut.didScrollToPage(at: lastPageIndex)
        
        XCTAssertEqual(presenterSpy.callSetupButtons, 2)
        XCTAssertFalse(presenterSpy.isOnFirstPage)
        XCTAssertTrue(presenterSpy.isOnLastPage)
    }
    
    func testGetTermsOfServiceText_WhenCalledFromViewController_ShouldCallpresentTermsOfServiceText() {
        let expectedPrivacyLink = OnboardingViewModel.privacyPolicyLink
        let expectedTermsLink = OnboardingViewModel.termsAndConditionsLink
        
        sut.getTermsOfServiceText()
        
        XCTAssertEqual(presenterSpy.callPresentTermsOfServiceTextCount, 1)
        XCTAssertEqual(presenterSpy.privacyLink, expectedPrivacyLink)
        XCTAssertEqual(presenterSpy.termsLink, expectedTermsLink)
    }
}
