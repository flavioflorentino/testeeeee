import XCTest
@testable import P2PCollectiveLending

private final class DeeplinkSpy: URLOpener {
    private(set) var callOpenDeeplinkCount = 0
    
    func canOpenURL(_ url: URL) -> Bool { true }
    
    func open(_ url: URL,
              options: [UIApplication.OpenExternalURLOptionsKey: Any],
              completionHandler: ((Bool) -> Void)?) {
        callOpenDeeplinkCount += 1
    }
}

final class CollectiveLendingOnboardCoordinatorTests: XCTestCase {
    private lazy var deeplinkSpy = DeeplinkSpy()
    private lazy var viewControllerSpy = ViewControllerSpy()
    lazy var sut: CollectiveLendingOnboardingCoordinator = {
        let coordinator = CollectiveLendingOnboardingCoordinator(
            dependencies: CollectiveLendingDependencyContainerMock(deeplinkSpy))
        coordinator.viewController = viewControllerSpy
        return coordinator
    }()
    
    func testShowHelp_WhenCalledFromPresenter_ShouldOpenFAQArticle() throws {
        let url = try XCTUnwrap(URL(string: "picpay://picpay/helpcenter/article/360049387652"))
        
        sut.showHelp(url)
        
        XCTAssertEqual(deeplinkSpy.callOpenDeeplinkCount, 1)
    }
    
    func testShowTermsOfService_WhenCalledFromPresenter_ShouldPresentSafariWebview() throws {
        let url = try XCTUnwrap(URL(string: "https://www.picpay.com.br/"))
        
        sut.showTermsOfUse(for: url)
        
        XCTAssertEqual(viewControllerSpy.callPresentViewControllerCount, 1)
    }
}
