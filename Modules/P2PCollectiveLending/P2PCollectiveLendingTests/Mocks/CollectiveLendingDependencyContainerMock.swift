import AnalyticsModule
import FeatureFlag
@testable import P2PCollectiveLending

final class CollectiveLendingDependencyContainerMock: AppDependencies {
    lazy var urlOpener: URLOpener? = resolve()
    lazy var mainQueue: DispatchQueue = resolve()
    lazy var analytics: AnalyticsProtocol = resolve()
    lazy var featureManager: FeatureManagerContract = resolve()

    private let dependencies: [Any]

    init(_ dependencies: Any...) {
        self.dependencies = dependencies
    }
}

extension CollectiveLendingDependencyContainerMock {
    func resolve<T>() -> T {
        let resolved = dependencies.compactMap { $0 as? T }

        switch resolved.first {
        case .none:
            fatalError("CollectiveLendingDependencyContainerMock could not resolve dependency: \(T.self)\n")
        case .some where resolved.count > 1:
            fatalError("CollectiveLendingDependencyContainerMock resolved mutiple dependencies for: \(T.self)\n")
        case .some(let mock):
            return mock
        }
    }

    func resolve() -> DispatchQueue {
        dependencies.compactMap { $0 as? DispatchQueue }.first ?? DispatchQueue(label: "CollectiveLendingDependencyContainerMock")
    }
}
