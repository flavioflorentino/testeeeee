import XCTest
@testable import P2PCollectiveLending

private final class OffersListDisplaySpy: OffersListDisplaying {
    private(set) var callDisplaySummaryInfoCount = 0
    private(set) var callSetContinueButtonEnabledCount = 0
    private(set) var callSetSummaryEmptyState = 0
    
    private(set) var summaryInfoAttributedString: NSAttributedString?
    private(set) var isContinueButtonEnabled = false
    
    func diaplaySummaryInfo(with attributedString: NSAttributedString) {
        callDisplaySummaryInfoCount += 1
        summaryInfoAttributedString = attributedString
    }
    
    func setContinueButtonEnabled(isEnabled: Bool) {
        callSetContinueButtonEnabledCount += 1
        isContinueButtonEnabled = isEnabled
    }
    
    func setSummaryEmptyState() {
        callSetSummaryEmptyState += 1
    }
}

private final class OffersListCoordinatorSpy: OffersListCoordinating {
    var viewController: UIViewController?
    
    private(set) var callPerformAction = 0
    
    private(set) var action: OffersListAction?
    
    func perform(action: OffersListAction) {
        callPerformAction += 1
        self.action = action
    }
}

final class OffersListPresenterTests: XCTestCase {
    private lazy var viewControllerSpy = OffersListDisplaySpy()
    private lazy var coordinatorSpy = OffersListCoordinatorSpy()
    private lazy var sut: OffersListPresenter = {
        let presenter = OffersListPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testUpdateOfferSummary_WhenCalledFromInteractor_ShouldSetupSummaryInfo() {
        let summaryModel = OfferSummaryModel(quantity: 2, value: 350)
        let expectedString = "Você vai investir\nR$ 350,00 em 2 objetivos"
        
        sut.updateOfferSummary(with: summaryModel)
        
        XCTAssertEqual(viewControllerSpy.callDisplaySummaryInfoCount, 1)
        XCTAssertEqual(viewControllerSpy.callSetContinueButtonEnabledCount, 1)
        XCTAssertTrue(viewControllerSpy.isContinueButtonEnabled)
        XCTAssertEqual(viewControllerSpy.summaryInfoAttributedString?.string, expectedString)
    }
    
    func testPresentSummaryEmptyState_WhenCalledFromInteractor_ShouldSetupSummaryEmptyState() {
        sut.presentSummaryEmptyState()
        
        XCTAssertEqual(viewControllerSpy.callSetSummaryEmptyState, 1)
        XCTAssertEqual(viewControllerSpy.callSetContinueButtonEnabledCount, 1)
        XCTAssertFalse(viewControllerSpy.isContinueButtonEnabled)
    }
}
