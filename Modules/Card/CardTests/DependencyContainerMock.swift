import AnalyticsModule
import Core
import FeatureFlag
import XCTest
@testable import Card

/// Don't use this class for testing, use DependencyContainerMock
final class DependencyContainer {
    init() {
        fatalError("Use a DependencyContainerMock on test target")
    }
}

final class DependencyContainerMock: AppDependencies {
    lazy var authManager: AuthContract = resolve()
    lazy var debitAPIManager: DebitAPIManagerContract = resolve()
    lazy var mainQueue: DispatchQueue = resolve()
    lazy var featureManager: FeatureManagerContract = resolve()
    lazy var analytics: AnalyticsProtocol = resolve()
    lazy var consumer: CardConsumerContract = resolve()
    lazy var ppauth: PPAuthSwiftContract = resolve()
    lazy var keychain: KeychainManagerContract = resolve()

    private let dependencies: [Any]
    
    init(_ dependencies: Any...) {
        self.dependencies = dependencies
    }
}

extension DependencyContainerMock {
    func resolve<T>() -> T {
        let resolved = dependencies.compactMap { $0 as? T }
                
        switch resolved.first {
        case .none:
            fatalError("DependencyContainerMock could not resolve dependency: \(T.self)\n")
        case .some where resolved.count > 1:
            fatalError("DependencyContainerMock resolved mutiple dependencies for: \(T.self)\n")
        case .some(let mock):
            return mock
        }
    }
    
    func resolve() -> DispatchQueue {
        let resolved = dependencies.compactMap { $0 as? DispatchQueue }.first
        return resolved ?? DispatchQueue(label: "DependencyContainerMock")
    }
    
    func resolve() -> DispatchGroup {
        let resolved = dependencies.compactMap { $0 as? DispatchGroup }.first
        return resolved ?? DispatchGroup()
    }
    
    func resolve() -> KeychainManagerContract {
        let resolved = dependencies.compactMap { $0 as? KeychainManagerContract }.first
        return resolved ?? KeychainManagerMock()
    }
}
