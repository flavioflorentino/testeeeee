import AnalyticsModule
import Core
import FeatureFlag
import UI
import XCTest
@testable import Card

final class CustomerSupportFeedbackViewControllerSpy {
    private(set) var primaryButtonCallBackCount = 0
    private(set) var secondaryButtonCallBackCount = 0
    private(set) var urls: [URL] = []
    
    func primaryButtonCallBack() {
        primaryButtonCallBackCount += 1
    }
    
    func secondaryButtonCallBack() {
        secondaryButtonCallBackCount += 1
    }
    
    func helpCenterCallBack(url: URL) {
        urls.append(url)
    }
}

final class CustomerSupportFeedbackViewControllerTests: XCTestCase {
    private lazy var customerSupportFeedbackSpy = CustomerSupportFeedbackViewControllerSpy()
    private lazy var urlString = "picpay://picpay/helpcenter/test"

    private lazy var sut: CustomerSupportFeedbackViewController? = {
      guard let url = URL(string: urlString) else { return nil }
      let viewController = CustomerSupportFeedbackViewController(model: StatefulFeedbackViewModel(.bodyNotFound), url: url)
      viewController.primaryButtonCallBack = customerSupportFeedbackSpy.primaryButtonCallBack
      viewController.secondaryButtonCallBack = customerSupportFeedbackSpy.secondaryButtonCallBack
      viewController.helpCenterCallBack = customerSupportFeedbackSpy.helpCenterCallBack
      return viewController
    }()
    
    func testDidTryAgain_WhenDidCall_ShouldCallPrimaryButtonCallBack() {
        sut?.didTryAgain()
        XCTAssertEqual(customerSupportFeedbackSpy.primaryButtonCallBackCount, 1)
        XCTAssertEqual(customerSupportFeedbackSpy.secondaryButtonCallBackCount, 0)
        XCTAssertEqual(customerSupportFeedbackSpy.urls, [])
    }
    
    func testDidTapSecondaryButton_WhenDidCall_ShouldCallSecondaryButtonCallBack() {
        sut?.didTapSecondaryButton()
        XCTAssertEqual(customerSupportFeedbackSpy.secondaryButtonCallBackCount, 1)
        XCTAssertEqual(customerSupportFeedbackSpy.urls, [])
    }

    func testHelpCenterCallBack_WhenDidCall_ShouldCallHelpCenterCallBack() throws {
        let expectedURL = try XCTUnwrap(URL(string: urlString))
        sut?.helpCenterCallBack?(expectedURL)
        XCTAssertEqual(customerSupportFeedbackSpy.urls, [expectedURL])
    }
}
