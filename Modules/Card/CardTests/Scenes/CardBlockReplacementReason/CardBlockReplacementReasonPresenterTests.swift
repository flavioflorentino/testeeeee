import UI
import XCTest
@testable import Card

private final class CardBlockReplacementReasonViewControllerSpy: CardBlockReplacementReasonDisplay {
    // MARK: - Variables
    private(set) var didReasonCount = 0
    private(set) var didReasonData: [CardBlockReplacementReasonData] = []
    private(set) var didDisplayTextCount = 0
    private(set) var didDisplayTextDescription: String?
    private(set) var didDisplayConfirmButtonTitle: String?

    func displayReasons(data: [CardBlockReplacementReasonData]) {
        didReasonCount += 1
        didReasonData = data
    }

    func displayText(description: String, confirmButtonTitle: String) {
        didDisplayTextCount += 1
        didDisplayTextDescription = description
        didDisplayConfirmButtonTitle = confirmButtonTitle
    }
}

private final class CardBlockReplacementReasonCoordinatingSpy: CardBlockReplacementReasonCoordinating {
    // MARK: - Variables
    var delegate: CardBlockReplacementReasonCoordinatorDelegate?
    private(set) var didPerfomCount = 0
    private(set) var didPerfomAction: CardBlockReplacementReasonAction?

    func perform(action: CardBlockReplacementReasonAction) {
        didPerfomCount += 1
        didPerfomAction = action
    }
}

final class CardBlockReplacementReasonPresenterTests: XCTestCase {
    // MARK: - Variables
    private var viewControllerSpy = CardBlockReplacementReasonViewControllerSpy()
    private let coordinatorSpy = CardBlockReplacementReasonCoordinatingSpy()
    private let container = DependencyContainerMock()

    private lazy var sut: CardBlockReplacementReasonPresenter = {
        let sut = CardBlockReplacementReasonPresenter(coordinator: coordinatorSpy)
        sut.viewController = viewControllerSpy
        return sut
    }()

    // MARK: - Tests
    func testDidNextStepControllerWithCancel_WhenCalledFromPresenter_ShouldPassTheActionToCoordinator() {
        sut.didNextStep(action: .cancel)
        XCTAssertEqual(coordinatorSpy.didPerfomCount, 1)
        XCTAssertEqual(coordinatorSpy.didPerfomAction, .cancel)
    }
    func testDidNextStepControllerWithConfirmDamaged_WhenCalledFromPresenter_ShouldPassTheActionToCoordinator() {
        sut.didNextStep(action: .confirm(reason: .damaged))
        XCTAssertEqual(coordinatorSpy.didPerfomCount, 1)
        XCTAssertEqual(coordinatorSpy.didPerfomAction, .confirm(reason: .damaged))
    }
    func testDidNextStepControllerWithConfirmLoss_WhenCalledFromPresenter_ShouldPassTheActionToCoordinator() {
        sut.didNextStep(action: .confirm(reason: .loss))
        XCTAssertEqual(coordinatorSpy.didPerfomCount, 1)
        XCTAssertEqual(coordinatorSpy.didPerfomAction, .confirm(reason: .loss))
    }
    func testDidNextStepControllerWithConfirmStolen_WhenCalledFromPresenter_ShouldPassTheActionToCoordinator() {
        sut.didNextStep(action: .confirm(reason: .stolen))
        XCTAssertEqual(coordinatorSpy.didPerfomCount, 1)
        XCTAssertEqual(coordinatorSpy.didPerfomAction, .confirm(reason: .stolen))
    }
    func testSetReasons_WhenCalledFromPresenter_ShouldPassTheDataToController() {
        sut.displayReasons(data: [CardBlockReplacementReasonData(value: .damaged, description: "Danificado")])
        XCTAssertEqual(viewControllerSpy.didReasonCount, 1)
        XCTAssertEqual(viewControllerSpy.didReasonData[0].value, .damaged)
        XCTAssertEqual(viewControllerSpy.didReasonData[0].description, "Danificado")
    }
    func testDisplayText_WhenCalledFromPresenterPassingBlockFlow_ShouldPassTheseButtonTitleAndColorToController() {
        sut.displayTextDescriptionConfirmButtonTitle(for: .block)
        XCTAssertEqual(viewControllerSpy.didDisplayTextCount, 1)
        XCTAssertEqual(viewControllerSpy.didDisplayTextDescription, "Uma vez feita a solicitação do bloqueio, o cartão atual não poderá ser mais utilizado.")
        XCTAssertEqual(viewControllerSpy.didDisplayConfirmButtonTitle, "Continuar")
    }
    func testDisplayText_WhenCalledFromPresenterPassingReplacementFlow_ShouldPassTheseButtonTitleAndColorToController() {
        sut.displayTextDescriptionConfirmButtonTitle(for: .replacement)
        XCTAssertEqual(viewControllerSpy.didDisplayTextCount, 1)
        XCTAssertEqual(viewControllerSpy.didDisplayTextDescription, "Informe qual o motivo para solicitar a **nova via** do seu cartão.")
        XCTAssertEqual(viewControllerSpy.didDisplayConfirmButtonTitle, "Solicitar nova via")
    }
}
