import Core
import UI
import XCTest
@testable import Card

private class CardBlockReplacementReasonViewModelPresentingSpy: CardBlockReplacementReasonPresenting {
    var viewController: CardBlockReplacementReasonDisplay?

    private(set) var didNextStepCount = 0
    private(set) var didNextStepAction: CardBlockReplacementReasonAction?
    private(set) var didReasonsCount = 0
    private(set) var didReasonsData: [CardBlockReplacementReasonData] = []
    private(set) var didConfirmButtonCount = 0

    func didNextStep(action: CardBlockReplacementReasonAction) {
        didNextStepCount += 1
        didNextStepAction = action
    }

    func displayReasons(data: [CardBlockReplacementReasonData]) {
        didReasonsCount += 1
        didReasonsData = data
    }

    func displayTextDescriptionConfirmButtonTitle(for kindFlow: CardBlockReplacementReasonTypeFlow) {
        didConfirmButtonCount += 1
    }
}

final class CardBlockReplacementReasonViewModelTests: XCTestCase {
    // MARK: - Variables
    private var presenterSpyBlock = CardBlockReplacementReasonViewModelPresentingSpy()
    private var presenterSpyReplacement = CardBlockReplacementReasonViewModelPresentingSpy()

    private lazy var sutBlock =
        CardBlockReplacementReasonViewModel(
            presenter: presenterSpyBlock,
            typeFlow: .block
    )
    private lazy var sutReplacement =
        CardBlockReplacementReasonViewModel(
            presenter: presenterSpyReplacement,
            typeFlow: .replacement
    )
    // MARK: - Tests SutBlock
    func testSutBlockDidCancel_WhenCalledFromViewModel_ShouldPassAnActionCancelToPresenter() {
        sutBlock.cancel()
        XCTAssertEqual(presenterSpyBlock.didNextStepCount, 1)
        XCTAssertEqual(presenterSpyBlock.didNextStepAction, .cancel)
    }
    func testSutUpdateViewDidCancel_WhenCalledFromViewModel_ShouldCallDisplayReasonAndDisplayConfirmButton() {
        sutBlock.updateView()
        XCTAssertEqual(presenterSpyBlock.didReasonsCount, 1)
        XCTAssertEqual(presenterSpyBlock.didConfirmButtonCount, 1)
    }
    func testSutBlockDidConfirmWithDamagedReason_WhenCalledFromViewModel_ShouldPassAnActionConfirmToPresenter() {
        sutBlock.confirm(reason: .damaged)
        XCTAssertEqual(presenterSpyBlock.didNextStepCount, 1)
        XCTAssertEqual(presenterSpyBlock.didNextStepAction, .confirm(reason: .damaged))
    }
    func testSutBlockDidConfirmWithLossReason_WhenCalledFromViewModel_ShouldPassAnActionConfirmToPresenter() {
        sutBlock.confirm(reason: .loss)
        XCTAssertEqual(presenterSpyBlock.didNextStepCount, 1)
        XCTAssertEqual(presenterSpyBlock.didNextStepAction, .confirm(reason: .loss))
    }
    func testSutBlockDidConfirmWithStolenReason_WhenCalledFromViewModel_ShouldPassAnActionConfirmToPresenter() {
        sutBlock.confirm(reason: .stolen)
        XCTAssertEqual(presenterSpyBlock.didNextStepCount, 1)
        XCTAssertEqual(presenterSpyBlock.didNextStepAction, .confirm(reason: .stolen))
    }
    func testSutBlockDidGetReasons_WhenCalledFromViewModel_ShouldPassAnListOfReasonsToPresenter() {
        sutBlock.getReasons()
        XCTAssertEqual(presenterSpyBlock.didReasonsCount, 1)
        XCTAssertEqual(presenterSpyBlock.didReasonsData[0].value, CardBlockReplacementReason.loss)
        XCTAssertEqual(presenterSpyBlock.didReasonsData[1].value, CardBlockReplacementReason.stolen)
    }
    func testSutBlockDidGetConfirmButtonTilte_WhenCalledFromViewModel_ShouldPassATitleAndBackgroundColorToPresenter() {
        sutBlock.getTextDescriptionConfirmButtonTitle()
        XCTAssertEqual(presenterSpyBlock.didConfirmButtonCount, 1)
    }
    // MARK: - Tests SutReplacement
    func testSutReplacementDidCancel_WhenCalledFromViewModel_ShouldPassAnActionCancelToPresenter() {
        sutReplacement.cancel()
        XCTAssertEqual(presenterSpyReplacement.didNextStepCount, 1)
        XCTAssertEqual(presenterSpyReplacement.didNextStepAction, .cancel)
    }
    func testSutReplacementDidConfirmWithDamagedReason_WhenCalledFromViewModel_ShouldPassAnActionConfirmToPresenter() {
        sutReplacement.confirm(reason: .damaged)
        XCTAssertEqual(presenterSpyReplacement.didNextStepCount, 1)
        XCTAssertEqual(presenterSpyReplacement.didNextStepAction, .confirm(reason: .damaged))
    }
    func testSutReplacementDidConfirmWithLossReason_WhenCalledFromViewModel_ShouldPassAnActionConfirmToPresenter() {
        sutReplacement.confirm(reason: .loss)
        XCTAssertEqual(presenterSpyReplacement.didNextStepCount, 1)
        XCTAssertEqual(presenterSpyReplacement.didNextStepAction, .confirm(reason: .loss))
    }
    func testSutReplacementDidConfirmWithStolenReason_WhenCalledFromViewModel_ShouldPassAnActionConfirmToPresenter() {
        sutReplacement.confirm(reason: .stolen)
        XCTAssertEqual(presenterSpyReplacement.didNextStepCount, 1)
        XCTAssertEqual(presenterSpyReplacement.didNextStepAction, .confirm(reason: .stolen))
    }
    func testSutReplacementDidGetReasons_WhenCalledFromViewModel_ShouldPassAnListOfReasonsToPresenter() {
        sutReplacement.getReasons()
        XCTAssertEqual(presenterSpyReplacement.didReasonsCount, 1)
        XCTAssertEqual(presenterSpyReplacement.didReasonsData[0].value, CardBlockReplacementReason.damaged)
        XCTAssertEqual(presenterSpyReplacement.didReasonsData[1].value, CardBlockReplacementReason.loss)
        XCTAssertEqual(presenterSpyReplacement.didReasonsData[2].value, CardBlockReplacementReason.stolen)
    }
    func testSutReplacementDidGetConfirmButtonTilte_WhenCalledFromViewModel_ShouldPassATitleAndBackgroundColorToPresenter() {
        sutReplacement.getTextDescriptionConfirmButtonTitle()
        XCTAssertEqual(presenterSpyReplacement.didConfirmButtonCount, 1)
    }
}
