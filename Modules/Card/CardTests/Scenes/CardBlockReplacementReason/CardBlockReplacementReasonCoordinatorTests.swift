import XCTest
@testable import Card

private final class CardBlockReplacementReasonCoordinatorDelegateSpy: CardBlockReplacementReasonCoordinatorDelegate {

    // MARK: - Variables
    private(set) var cancelBlockReasonCount = 0
    private(set) var confirmBlockCount = 0
    private(set) var confirmBlockReason: CardBlockReplacementReason?

    func cancelBlockReason() {
        cancelBlockReasonCount += 1
    }

    func confirmBlock(with reason: CardBlockReplacementReason) {
        confirmBlockCount += 1
        confirmBlockReason = reason
    }
}

final class CardBlockReplacementReasonCoordinatorTests: XCTestCase {
    // MARK: - Variables
    private let delegateSpy = CardBlockReplacementReasonCoordinatorDelegateSpy()

    private lazy var sut: CardBlockReplacementReasonCoordinator = {
        let sut = CardBlockReplacementReasonCoordinator()
        sut.delegate = delegateSpy
        return sut
    }()

    // MARK: - Tests
    func testDidPerformCancel_WhenCalledFromPresenter_ShouldCallCancelBlockReasonOnDelegate() {
        sut.perform(action: .cancel)
        XCTAssertEqual(delegateSpy.cancelBlockReasonCount, 1)
    }
    func testDidPerformWithConfirmWithDamagedReason_WhenCalledFromPresenter_ShouldCallConfirmBlockReasonOnDelegate() {
        sut.perform(action: .confirm(reason: .damaged))
        XCTAssertEqual(delegateSpy.confirmBlockCount, 1)
        XCTAssertEqual(delegateSpy.confirmBlockReason, .damaged)
    }
    func testDidPerformWithConfirmWithLossReason_WhenCalledFromPresenter_ShouldCallConfirmBlockReasonOnDelegate() {
        sut.perform(action: .confirm(reason: .loss))
        XCTAssertEqual(delegateSpy.confirmBlockCount, 1)
        XCTAssertEqual(delegateSpy.confirmBlockReason, .loss)
    }
    func testDidPerformWithConfirmWithStolenReason_WhenCalledFromPresenter_ShouldCallConfirmBlockReasonOnDelegate() {
        sut.perform(action: .confirm(reason: .stolen))
        XCTAssertEqual(delegateSpy.confirmBlockCount, 1)
        XCTAssertEqual(delegateSpy.confirmBlockReason, .stolen)
    }
}
