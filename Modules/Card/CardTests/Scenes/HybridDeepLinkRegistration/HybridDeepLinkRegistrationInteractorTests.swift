import XCTest
import AnalyticsModule
import Core
import FeatureFlag
@testable import Card

final class HybridDeepLinkRegistrationServicingSpy: HybridDeepLinkRegistrationServicing {
    var expectedResult: (Result<HybridDeepLink, ApiError>) = .failure(.bodyNotFound)

    func loadStateRegistration(completion: @escaping HybridDeepLinkResult) {
        completion(expectedResult)
    }

}

final class HybridDeepLinkRegistrationPresentingSpy: HybridDeepLinkRegistrationPresenting {
    var viewController: HybridDeepLinkRegistrationDisplaying? = nil

    private(set) var flows: [HybridDeepLinkEnum] = []
    private(set) var errors: [ApiError] = []
    private(set) var presentLoadStateCount = 0
    private(set) var actions: [HybridDeepLinkRegistrationAction] = []
    
    func presentIdealScenario(_ flow: HybridDeepLinkEnum) {
        flows.append(flow)
    }
    func presentError(_ error: ApiError) {
        errors.append(error)
    }
    
    func presentLoadState() {
        presentLoadStateCount += 1
    }
    
    func didNextStep(action: HybridDeepLinkRegistrationAction) {
        actions.append(action)
    }
}

final class HybridDeepLinkRegistrationInteractorTests: XCTestCase {
    lazy var presenterSpy = HybridDeepLinkRegistrationPresentingSpy()
    lazy var serviceSpy = HybridDeepLinkRegistrationServicingSpy()
    private lazy var analytics = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(analytics, FeatureManager.shared)

    lazy var sut: HybridDeepLinkRegistrationInteractor = {
        HybridDeepLinkRegistrationInteractor(service: serviceSpy, presenter: presenterSpy, dependencies: dependencies)
    }()
    
    func testLoadData_WhenSuccessFlowOfferNotFound_ShouldCallPresentIdealState() throws {
        let expectedEvent = HybridDeepLinkRegistrationEvent.unavailableOffer.event()
        let expectedCaseFlow: HybridDeepLinkEnum = .offerNotFound
        serviceSpy.expectedResult = .success(.init(cardOfferFlow: expectedCaseFlow))
        sut.loadData()
        XCTAssertEqual(expectedCaseFlow, .offerNotFound)
        XCTAssertEqual(presenterSpy.presentLoadStateCount, 1)
        XCTAssertEqual([expectedCaseFlow], presenterSpy.flows)
        XCTAssertEqual(presenterSpy.errors.count, 0)
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testLoadData_WhenSuccessFlowUserAlReadyHasDebit_ShouldCallPresentIdealState() throws {
        let expectedEvent = HybridDeepLinkRegistrationEvent.hasDebitCard.event()
        let expectedCaseFlow: HybridDeepLinkEnum = .userHasDebit
        serviceSpy.expectedResult = .success(.init(cardOfferFlow: expectedCaseFlow))
        sut.loadData()
        
        XCTAssertEqual(expectedCaseFlow, .userHasDebit)
        XCTAssertEqual(presenterSpy.presentLoadStateCount, 1)
        XCTAssertEqual([expectedCaseFlow], presenterSpy.flows)
        XCTAssertEqual(presenterSpy.errors.count, 0)
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testLoadData_WhenSuccessFlowUserAlReadyHasCredit_ShouldCallPresentIdealState() throws {
        let expectedEvent = HybridDeepLinkRegistrationEvent.hasMultipleCard.event()
        let expectedCaseFlow: HybridDeepLinkEnum = .userHasCredit
        serviceSpy.expectedResult = .success(.init(cardOfferFlow: expectedCaseFlow))
        sut.loadData()
        XCTAssertEqual(expectedCaseFlow, .userHasCredit)
        XCTAssertEqual(presenterSpy.presentLoadStateCount, 1)
        XCTAssertEqual([expectedCaseFlow], presenterSpy.flows)
        XCTAssertEqual(presenterSpy.errors.count, 0)
        XCTAssertTrue(analytics.equals(to: expectedEvent))

    }
    
    func testLoadData_WhenSuccessFlowUserSimplifiedDebit_ShouldCallPresentIdealState() throws {
        let expectedEvent = HybridDeepLinkRegistrationEvent.progress(.inital).event()
        let expectedCaseFlow: HybridDeepLinkEnum = .simplifiedDebit
        serviceSpy.expectedResult = .success(.init(cardOfferFlow: expectedCaseFlow))
        sut.loadData()
        XCTAssertEqual(expectedCaseFlow, sut.state)
        XCTAssertEqual(presenterSpy.presentLoadStateCount, 1)
        XCTAssertEqual([expectedCaseFlow], presenterSpy.flows)
        XCTAssertEqual(presenterSpy.errors.count, 0)
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testLoadData_WhenSuccessFlowUserCreditInProgress_ShouldCallPresentIdealState() throws {
        let expectedEvent = HybridDeepLinkRegistrationEvent.progress(.inital).event()
        let expectedCaseFlow: HybridDeepLinkEnum = .creditInProgress
        serviceSpy.expectedResult = .success(.init(cardOfferFlow: expectedCaseFlow))
        sut.loadData()
        XCTAssertEqual(expectedCaseFlow, sut.state)
        XCTAssertEqual(presenterSpy.presentLoadStateCount, 1)
        XCTAssertEqual([expectedCaseFlow], presenterSpy.flows)
        XCTAssertEqual(presenterSpy.errors.count, 0)
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testLoadData_WhenSuccessFlowUserDebitInProgress_ShouldCallPresentIdealState() throws {
        let expectedEvent = HybridDeepLinkRegistrationEvent.progress(.inital).event()
        let expectedCaseFlow: HybridDeepLinkEnum = .debitInProgress
        serviceSpy.expectedResult = .success(.init(cardOfferFlow: expectedCaseFlow))
        sut.loadData()
        XCTAssertEqual(expectedCaseFlow, sut.state)
        XCTAssertEqual(presenterSpy.presentLoadStateCount, 1)
        XCTAssertEqual([expectedCaseFlow], presenterSpy.flows)
        XCTAssertEqual(presenterSpy.errors.count, 0)
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testLoadData_WhenSuccessFlowDebit_ShouldNotCallPresentIdealState() throws {
        let expectedEvent = HybridDeepLinkRegistrationEvent.hasDebitCard.event()
        let expectedCaseFlow: HybridDeepLinkEnum = .debit
        serviceSpy.expectedResult = .success(.init(cardOfferFlow: expectedCaseFlow))
        sut.loadData()
        XCTAssertEqual(expectedCaseFlow, .debit)
        XCTAssertEqual(presenterSpy.presentLoadStateCount, 1)
        XCTAssertEqual([], presenterSpy.flows)
        XCTAssertEqual(presenterSpy.errors.count, 0)
        XCTAssertFalse(analytics.equals(to: expectedEvent))
    }
    
    func testLoadData_WhenSuccessFlowCredit_ShouldNotCallPresentIdealState() throws {
        let expectedCaseFlow: HybridDeepLinkEnum = .credit
        serviceSpy.expectedResult = .success(.init(cardOfferFlow: expectedCaseFlow))
        sut.loadData()
        XCTAssertEqual(expectedCaseFlow, .credit)
        XCTAssertEqual(presenterSpy.presentLoadStateCount, 1)
        XCTAssertEqual([], presenterSpy.flows)
        XCTAssertEqual(presenterSpy.errors.count, 0)
    }
    
    func testLoadData_WhenFailure_ShouldCallPresentError() {
        let expectedError = ApiError.connectionFailure
        serviceSpy.expectedResult = .failure(expectedError)
        sut.loadData()
        XCTAssertNil(sut.state)
        XCTAssertEqual([], presenterSpy.flows)
        XCTAssertEqual([expectedError], presenterSpy.errors)
    }
    
    func testSecondaryButtonAction_WhenFailure_ShouldCallLoadData() {
        serviceSpy.expectedResult = .failure(.connectionFailure)
        sut.loadData()
        XCTAssertNil(sut.state)
        sut.secondaryButtonAction()
        XCTAssertEqual(presenterSpy.presentLoadStateCount, 1)
        XCTAssertNil(sut.state)
    }

    func testSecondaryButtonAction_WhenSuccessFlowOfferNotFound_ShouldCallHelpCenter() throws {
        serviceSpy.expectedResult = .success(.init(cardOfferFlow: .offerNotFound))
        sut.loadData()
        sut.secondaryButtonAction()
        let expectedUrlstring = try XCTUnwrap(sut.takeDeeplinkHelpCenter())
        XCTAssertEqual(presenterSpy.presentLoadStateCount, 1)
        XCTAssertEqual(presenterSpy.actions, [.helpCenter(expectedUrlstring)])
    }
    
    func testSecondaryButtonAction_WhenSuccessFlowUserAlReadyHasCredit_ShouldCallHelpCenter() throws {
        serviceSpy.expectedResult = .success(.init(cardOfferFlow: .userHasCredit))
        sut.loadData()
        sut.secondaryButtonAction()
        let expectedUrlstring = try XCTUnwrap(sut.takeDeeplinkHelpCenter())
        XCTAssertEqual(presenterSpy.presentLoadStateCount, 1)
        XCTAssertEqual(presenterSpy.actions, [.helpCenter(expectedUrlstring)])
    }
    
    func testSecondaryButtonAction_WhenSuccessFlowUserAlReadyHasDebit_ShouldCallHelpCenter() throws {
        serviceSpy.expectedResult = .success(.init(cardOfferFlow: .userHasDebit))
        sut.loadData()
        sut.secondaryButtonAction()
        let expectedUrlstring = try XCTUnwrap(sut.takeDeeplinkHelpCenter())
        XCTAssertEqual(presenterSpy.presentLoadStateCount, 1)
        XCTAssertEqual(presenterSpy.actions, [.helpCenter(expectedUrlstring)])
    }
    
    func testTakeDeeplinkHelpCenter_WhendDidCall_ShouldReturnNil() {
        let expectedNilThatFlows: [HybridDeepLinkEnum] = [.debit, .credit, .simplifiedDebit]
        expectedNilThatFlows.forEach { flow in
            serviceSpy.expectedResult = .success(.init(cardOfferFlow: flow))
            sut.loadData()
            XCTAssertNil(sut.takeDeeplinkHelpCenter())
        }
    }
    
    func testDidTapConfirmButton_WhenUserAlreadyHasCredit_ShouldCallPresenter() {
        serviceSpy.expectedResult = .success(.init(cardOfferFlow: .userHasCredit))
        sut.loadData()
        sut.didTapConfirmButton()
        XCTAssertEqual(presenterSpy.actions.last, .cardHome)
    }
    
    func testDidTapConfirmButton_WhenUserDebitInProgress_ShouldCallPresenter() {
        let expectedEvent = HybridDeepLinkRegistrationEvent.progress(.confirm).event()
        serviceSpy.expectedResult = .success(.init(cardOfferFlow: .debitInProgress))
        sut.loadData()
        sut.didTapConfirmButton()
        XCTAssertEqual(presenterSpy.actions.last, .debitFlow)
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testDidTapConfirmButton_WhenUserCreditInProgress_ShouldCallPresenter() {
        let expectedEvent = HybridDeepLinkRegistrationEvent.progress(.confirm).event()
        serviceSpy.expectedResult = .success(.init(cardOfferFlow: .creditInProgress))
        sut.loadData()
        sut.didTapConfirmButton()
        XCTAssertEqual(presenterSpy.actions.last, .creditFlow)
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testDidTapConfirmButton_WhenUserSimplifiedDebit_ShouldCallPresenter() {
        let expectedEvent = HybridDeepLinkRegistrationEvent.progress(.confirm).event()
        serviceSpy.expectedResult = .success(.init(cardOfferFlow: .simplifiedDebit))
        sut.loadData()
        sut.didTapConfirmButton()
        XCTAssertEqual(presenterSpy.actions.last, .simplifiedDebitFlow)
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testDidTapConfirmButton_WhenUserHasNotCredit_ShouldDismiss() {
        serviceSpy.expectedResult = .success(.init(cardOfferFlow: .debit))
        sut.loadData()
        sut.didTapConfirmButton()
        XCTAssertEqual(presenterSpy.actions.last, .dismiss)
    }
    
    func testDidTapConfirmButton_WhenUserPressSecondaryButtonProgressRegistration_ShouldDismiss() {
        let expectedEvent = HybridDeepLinkRegistrationEvent.progress(.cancel).event()
        let actionsWhithSameFlow =  [HybridDeepLinkEnum.creditInProgress, .debitInProgress, .simplifiedDebit]
        actionsWhithSameFlow.forEach { action in
            serviceSpy.expectedResult = .success(.init(cardOfferFlow: .creditInProgress))
            sut.loadData()
            sut.secondaryButtonAction()
            XCTAssertEqual(presenterSpy.actions.last, .dismiss)
            XCTAssertTrue(analytics.equals(to: expectedEvent))
        }
    }
}
