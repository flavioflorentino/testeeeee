import XCTest
import AnalyticsModule
import Core
import UI
@testable import Card

final class HybridDeepLinkRegistrationCoordinatingSpy: HybridDeepLinkRegistrationCoordinating {
    var viewController: UIViewController? = nil
    var coordinatorOutput: (HybridDeeplinkFlowCoordinatorOutput) -> Void = { _ in }
    private(set) var actions: [HybridDeepLinkRegistrationAction] = []
    private(set) var coordinatorOutputs: [HybridDeeplinkFlowCoordinatorOutput] = []
    
    func perform(action: HybridDeepLinkRegistrationAction) {
        actions.append(action)
    }
}

final class ViewControllerSpy: HybridDeepLinkRegistrationDisplaying {
    private(set) var displayLoadViewCount = 0
    private(set) var models: [StatefulFeedbackViewModel] = []
    private(set) var displayError: [StatefulFeedbackViewModel] = []
    func displayLoadView() {
        displayLoadViewCount += 1
    }
    
    func displayIdealScenario(_ model: StatefulFeedbackViewModel) {
        models.append(model)
    }
    
    func displayError(_ modelError: StatefulFeedbackViewModel) {
        displayError.append(modelError)
    }
}

class HybridDeepLinkRegistrationPresenterTests: XCTestCase {
    lazy var coordinatingSpy = HybridDeepLinkRegistrationCoordinatingSpy()
    lazy var viewControllerSpy = ViewControllerSpy()
    private lazy var dependencies = DependencyContainerMock()
    
    lazy var sut: HybridDeepLinkRegistrationPresenter = {
        let present = HybridDeepLinkRegistrationPresenter(coordinator: coordinatingSpy, dependencies: dependencies)
        present.viewController = viewControllerSpy
        return present
    }()

    func testPresentIdealScenario_WhenReciveSucessInRequest_ShouldDisplayIdealScenario() throws {
        let expectedCaseFlow: HybridDeepLinkEnum = .offerNotFound
        sut.presentIdealScenario(expectedCaseFlow)
        XCTAssertEqual(viewControllerSpy.models.count, 1)
        XCTAssertEqual(viewControllerSpy.displayError.count, 0)
        XCTAssertEqual(viewControllerSpy.displayLoadViewCount, 0)
    }
    
    func testPresentLoadView_WhenInteractorDidCall_ShouldDisplayLoad() {
        sut.presentLoadState()
        XCTAssertEqual(viewControllerSpy.displayLoadViewCount, 1)
        XCTAssertEqual(viewControllerSpy.displayError.count, 0)
        XCTAssertEqual(viewControllerSpy.models.count, 0)
    }
    
    func testPresentError_WhenReciveFalureInRequest_ShouldDisplayError() {
        let expectedErrorModel = StatefulFeedbackViewModel(ApiError.connectionFailure)
        viewControllerSpy.displayError(expectedErrorModel)
        XCTAssertEqual(viewControllerSpy.displayError.count, 1)
        XCTAssertEqual(viewControllerSpy.displayLoadViewCount, 0)
        XCTAssertEqual(viewControllerSpy.models.count, 0)
    }
    
    func testPresentDidNextStep_WhenIsCalledFromInteractor_ShouldCallCoordinator() {
        sut.didNextStep(action: .creditFlow)
        XCTAssertEqual(coordinatingSpy.actions.last, .creditFlow)
    }
}
