import XCTest
@testable import Card

private final class PasswordSecurityTipsInteractorSpy: PasswordSecurityTipsInteracting {
    private(set) var callConfirm = 0
    private(set) var callClose = 0
    private(set) var callUpdateView = 0
    
    func updateView() {
        callUpdateView += 1
    }
    
    func confirm() {
        callConfirm += 1
    }
    
    func close() {
        callClose += 1
    }
}

class PasswordSecurityTipsViewControllerTests: XCTestCase {
    private lazy var interactorSpy = PasswordSecurityTipsInteractorSpy()
    private lazy var sut = PasswordSecurityTipsViewController(viewModel: interactorSpy)
    
    override func setUp() {
        super.setUp()
        
        let tips = [StepPresenter(position: .initial, number: "1", description: "first")]
        sut.rootView.tipsModel = PasswordSecurityTipsModel(description: "Test Description",
                                                           securityTip: "Test Security Tip",
                                                           passwordTitle: "title button",
                                                           tips: tips)
        sut.beginAppearanceTransition(true, animated: false)
        sut.endAppearanceTransition()
    }
    
    func testRootView_WhenUpdateView_ShouldConfigureRootView() {
        XCTAssertNotNil(sut.rootView.tipsModel)
        XCTAssertEqual(sut.rootView.tipsModel?.description, "Test Description")
        XCTAssertEqual(sut.rootView.tipsModel?.securityTip, "Test Security Tip")
        XCTAssertEqual(sut.rootView.tipsModel?.passwordTitle, "title button")
        XCTAssert(sut.rootView.tipsModel?.tips.count == 1)
        
        XCTAssertEqual(sut.title, "Senha do cartão")
    }
    
    func testDidTapCreatePassword_WhenClickConfirm_ShouldReturnCloseAction() {
        sut.didTapCreatePassword()
        XCTAssertEqual(interactorSpy.callConfirm, 1)
    }
    
    func testTapClose_WhenTapConfirm_ShouldConfirm() {
        sut.tapClose()
        XCTAssertEqual(interactorSpy.callConfirm, 1)
    }
}
