import XCTest
@testable import Card

private final class PasswordSecurityTipsCoordinatorSpy: PasswordSecurityTipsCoordinating {
    var delegate: PasswordSecurityTipsCoordinatorDelegate?
    var viewController: UIViewController?
    
    //MARK: - perform
    private(set) var performActionCallsCount = 0
    private(set) var selectedAction: PasswordSecurityTipsAction?
    
    func perform(action: PasswordSecurityTipsAction) {
        performActionCallsCount += 1
        selectedAction = action
    }
}

private final class PasswordSecurityTipsViewControllerSpy: PasswordSecurityTipsDisplay {
    //MARK: - perform
    private(set) var callUpdateCount = 0
    private(set) var tipsModel: PasswordSecurityTipsModel?
    
    func update(with tipsModel: PasswordSecurityTipsModel) {
        callUpdateCount += 1
        self.tipsModel = tipsModel
    }
}

final class PasswordSecurityTipsPresenterTests: XCTestCase {
    private let coordinatorSpy = PasswordSecurityTipsCoordinatorSpy()
    private let viewControllerSpy = PasswordSecurityTipsViewControllerSpy()
    
    private lazy var sut: PasswordSecurityTipsPresenting = {
        let sut = PasswordSecurityTipsPresenter(coordinator: coordinatorSpy)
        sut.viewController = viewControllerSpy
        return sut
    }()

    func testUpdateView() {
        sut.updateView()
        let  tips = [
            StepPresenter(position: .initial, number: "1", description: "Não use números sequencias"),
            StepPresenter(position: .middle, number: "2", description: "Evite usar a senha de outros cartões."),
            StepPresenter(position: .final, number: "3", description: "Não use informações pessoais, como: data de nascimento, telefone e parte do CPF.")
        ]
        let tipsModel = PasswordSecurityTipsModel(
            description: "Crie uma senha para confirmar as movimentações do seu cartão.",
            securityTip: "Dicas de segurança:",
            passwordTitle: "Ok, entendi",
            tips: tips
        )
        XCTAssertEqual(viewControllerSpy.callUpdateCount, 1)
        XCTAssertEqual(viewControllerSpy.tipsModel, tipsModel)
    }

    func testDidNextStep_WhenCreatePassword_ShouldSelectAction() {
        sut.didNextStep(action: .createPassword)
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.selectedAction, .createPassword)
    }
    
    func testDidNextStep_WhenCloseScreen_ShouldSelectAction() {
        sut.didNextStep(action: .close)
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.selectedAction, .close)
    }
}
