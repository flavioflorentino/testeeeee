import XCTest
@testable import Card

final class MockPasswordSecurityTipsViewDelegate: PasswordSecurityTipsViewDelegate {
    private (set) var isDidTapCreatePasswordCalled = false
    private (set) var callCreatePassword = 0
    
    func didTapCreatePassword() {
        isDidTapCreatePasswordCalled = true
        callCreatePassword += 1
    }
}

final class PasswordSecurityTipsViewTests: XCTestCase {
    var sut: PasswordSecurityTipsView!
    var fakeDelegate = MockPasswordSecurityTipsViewDelegate()
    var tispModel: PasswordSecurityTipsModel!
    
    override func setUp() {
        super.setUp()
        let tips = [StepPresenter(position: .initial, number: "1", description: "First Description")]
        tispModel = PasswordSecurityTipsModel(description: "Description",
                                              securityTip: "SecurityTip",
                                              passwordTitle: "PasswordTitle",
                                              tips: tips)
        sut = PasswordSecurityTipsView(frame: UIScreen.main.bounds)
        sut.delegate = fakeDelegate
    }
    
    func testTapCreatePassword_WhenTapConfirm_ShouldReturnDelegateTrue() {
        sut.tapCreatePassword()
        XCTAssertTrue(fakeDelegate.isDidTapCreatePasswordCalled)
        XCTAssertEqual(fakeDelegate.callCreatePassword, 1)
    }
}
