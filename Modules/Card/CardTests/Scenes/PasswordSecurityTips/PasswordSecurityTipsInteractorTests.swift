import XCTest
@testable import Card

private final class PasswordSecurityTipsPresentingSpy: PasswordSecurityTipsPresenting {
    var viewController: PasswordSecurityTipsDisplay?
    
    private(set) var callUpdateView = 0
    private(set) var callAction = 0
    private(set) var selectedAction: PasswordSecurityTipsAction?
    
    func updateView() {
        callUpdateView += 1
    }
    
    func didNextStep(action: PasswordSecurityTipsAction) {
        selectedAction = action
        callAction += 1
    }
}

final class PasswordSecurityTipsInteractorTests: XCTestCase {
    private var presenterSpy = PasswordSecurityTipsPresentingSpy()
    private lazy var sut = PasswordSecurityTipsInteractor(presenter: presenterSpy)
    
    func testViewDidLoadShouldReturnPresenter() {
        sut.updateView()
        XCTAssertEqual(presenterSpy.callUpdateView, 1)
    }
    
    func testKDDKKD() {
        sut.confirm()
        XCTAssertEqual(presenterSpy.callAction, 1)
        XCTAssertEqual(presenterSpy.selectedAction, .createPassword)
    }
    
    func testKDDKiKD() {
        sut.close()
        XCTAssertEqual(presenterSpy.callAction, 1)
        XCTAssertEqual(presenterSpy.selectedAction, .close)
    }
}
