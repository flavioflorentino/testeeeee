import Foundation
@testable import Card
import XCTest
import UI

final class UnsuccessfulDeliveryCardViewControllerSpy: UnsuccessfulDeliveryCardDisplaying {
   private(set) var viewsReceived: [UIView] = []
    private(set) var displayLoadStateCount = 0
    private(set) var displayErrorStateModel: [StatefulFeedbackViewModel] = []
    private(set) var models: [StatefulFeedbackViewModel] = []
    private(set) var hasFooters: [Bool] = []
   
    func displayLoadState() {
        displayLoadStateCount += 1
    }
    
    func displayErrorState(model: StatefulFeedbackViewModel) {
        displayErrorStateModel.append(model)
    }
    
    func displayInfo(model: StatefulFeedbackViewModel, hasFooter: Bool) {
        models.append(model)
        hasFooters.append(hasFooter)
    }
}

final class UnsuccessfulDeliveryCardCoordinatorSpy: UnsuccessfulDeliveryCardCoordinating {
    var viewController: UIViewController?
    private(set) var actions: [UnsuccessfulDeliveryCardAction] = []
    
    func perform(action: UnsuccessfulDeliveryCardAction) {
        actions.append(action)
    }
}

final class UnsuccessfulDeliveryCardPresenterTests: XCTestCase {
    lazy var coordinatorSpy = UnsuccessfulDeliveryCardCoordinatorSpy()
    lazy var viewControllerSpy = UnsuccessfulDeliveryCardViewControllerSpy()
    
    let address = HomeAddress(
        zipCode: "60811110",
        addressComplement: nil,
        city: "Fortaleza",
        cityId: nil,
        neighborhood: "Eng.Luciano Cavalcante",
        state: "Ceara",
        stateId: nil,
        streetName: "Rua Luiza Miranda Coelho",
        streetNumber: "1355",
        streetType: nil,
        street: "Rua Luiza Miranda Coelho"
    )
    
    lazy var sut: UnsuccessfulDeliveryCardPresenter = {
        let present = UnsuccessfulDeliveryCardPresenter(coordinator: coordinatorSpy)
        present.viewController = viewControllerSpy
        return present
    }()
    
    func testPresentHelpCenter_WhenReceivedUrl_ShouldCallCoordinator() throws {
        let expectedUrl = try XCTUnwrap(URL(string: "picpay//Test"))
        sut.presentNextStep(action: .link(url: expectedUrl))
        XCTAssertEqual(coordinatorSpy.actions, [.link(url: expectedUrl)])
    }
    
    func testPresentInfoAddress_WhenReiceive_ShouldCallDisplayInfo() throws {
        let expectedUrl = try XCTUnwrap(URL(string: "picpay//Test"))
        let completeAddress = try XCTUnwrap(address.completeAddress)
        let model = UnsuccessfulDeliveryTypeCardModel.typeModel(type: .debit).getModel(deliveryAddress: completeAddress, url: expectedUrl)
        sut.presentInfoAddress(model, hasFooter: true)
        XCTAssertEqual(viewControllerSpy.models , [model])
    }
    
    func testPresentLoadState_WhenDidCall_ShouldCallDisplayLoadState() {
        sut.presentLoadState()
        XCTAssertEqual(viewControllerSpy.displayLoadStateCount, 1)
    }
    
    func testPresentErrorState_WhenReceiveError_ShouldCallDisplayErroState() {
        sut.presentErroState(.connectionFailure)
        XCTAssertEqual(viewControllerSpy.displayErrorStateModel.count, 1)
        XCTAssertEqual(viewControllerSpy.displayLoadStateCount, 0)
    }
}
