import Foundation
@testable import Card
import XCTest
import UI
import AnalyticsModule
import FeatureFlag
import Core

final class UnsuccessfulDeliveryCardPresentingSpy: UnsuccessfulDeliveryCardPresenting {
    var viewController: UnsuccessfulDeliveryCardDisplaying?
    private(set) var presentInfoAddressReceived: [StatefulFeedbackViewModel] = []
    private(set) var receivedActions: [UnsuccessfulDeliveryCardAction] = []
    private(set) var presentLoadStateCount = 0
    private(set) var presentErroState: [ApiError] = []
    private(set) var hasFooterReceiveds: [Bool] = []
    
    func presentInfoAddress(_ model: StatefulFeedbackViewModel, hasFooter: Bool) {
        presentInfoAddressReceived.append(model)
        hasFooterReceiveds.append(hasFooter)
    }

    func presentLoadState() {
        presentLoadStateCount += 1
    }
    
    func presentErroState(_ error: ApiError) {
        presentErroState.append(error)
    }
    
    func presentNextStep(action: UnsuccessfulDeliveryCardAction) {
        receivedActions.append(action)
    }
}

final class UnsuccessfulDeliveryCardServiceSpy: RequestPhysicalCardLoadingServicing {
    var receivedResult: Result<HomeAddress?, ApiError> = .failure(.bodyNotFound)
    func getDeliveryAddress(completion: @escaping CompletionAddress) {
        completion(receivedResult)
    }
}

final class UnsuccessfulDeliveryCardInteractorTests: XCTestCase {
    typealias Dependencies = HasFeatureManager & HasAnalytics
    lazy var presentSpy = UnsuccessfulDeliveryCardPresentingSpy()
    private lazy var  featureManagerMock = FeatureManagerMock()
    private lazy var analytics = AnalyticsSpy()
    private lazy var dependencies: Dependencies = DependencyContainerMock(analytics, featureManagerMock)
    lazy var serviceSpy = UnsuccessfulDeliveryCardServiceSpy()
    
    let address = HomeAddress(
        zipCode: "60811110",
        city: "Fortaleza",
        cityId: nil,
        neighborhood: "Eng.Luciano Cavalcante",
        state: "Ceara",
        stateId: nil,
        streetName: "Rua Luiza Miranda Coelho",
        streetNumber: "1355",
        streetType: nil,
        street: "Rua Luiza Miranda Coelho"
    )

    lazy var sut = UnsuccessfulDeliveryCardInteractor(presenter: presentSpy, cardType: .multiple, dependencies: dependencies, service: serviceSpy)
    
    func testLoadInfo_WhenSuccess_ShouldCallpresentInfoAddress() throws {
        let faqChangeAddress = "faqDeliveryFailedValueToTest"
        let urlExpected = try XCTUnwrap(URL(string: faqChangeAddress))
        let deliveryAddress = try XCTUnwrap(address.neighborhood)
        let model = UnsuccessfulDeliveryTypeCardModel.typeModel(type: .multiple).getModel(deliveryAddress: deliveryAddress, url: urlExpected)
        featureManagerMock.override(key: .faqChangeCardAddress, with: faqChangeAddress)
        serviceSpy.receivedResult = .success(address)
        sut.loadInfo()
        XCTAssertEqual(presentSpy.presentLoadStateCount, 1)
        XCTAssertEqual(presentSpy.presentInfoAddressReceived, [model])
    }
    
    func testLoadInfo_WhenFailure_ShouldCallPresentError() {
        let expectedError = ApiError.connectionFailure
        serviceSpy.receivedResult = .failure(expectedError)
        sut.loadInfo()
        XCTAssertEqual(presentSpy.presentLoadStateCount, 1)
        XCTAssertEqual(presentSpy.presentInfoAddressReceived, [])
        XCTAssertEqual(presentSpy.receivedActions, [])
        XCTAssertEqual(presentSpy.presentErroState, [expectedError])
    }
    
    func testOpenFaqChangeAddress_WhenDidCall_ShouldCallHelpCenter() throws {
        let faqChangeAddress = "faqDeliveryFailedValueToTest"
        let expectedEvent = UnsuccessfulDeliveryCardEvent.unsuccessAttempt(action: .changeAddress).event()
        let urlExpected = try XCTUnwrap(URL(string: faqChangeAddress))
        featureManagerMock.override(key: .faqChangeCardAddress, with: faqChangeAddress)
        sut.openFaqChangeAddress()
        XCTAssertEqual(presentSpy.receivedActions, [.link(url: urlExpected)])
        XCTAssertEqual(presentSpy.presentInfoAddressReceived, [])
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testOpenFaqDeliveryFaileds_WhenDidCall_ShouldCallHelpCenter() throws {
        let faqDeliveryFaileds = "faqDeliveryFailedValueToTest"
        let urlExpected = try XCTUnwrap(URL(string: faqDeliveryFaileds))
        let expectedEvent = UnsuccessfulDeliveryCardEvent.unsuccessAttempt(action: .helpCenter).event()

        featureManagerMock.override(key: .faqDeliveryCardFailed, with: faqDeliveryFaileds)
        sut.openFaqDeliveryFailed()
        XCTAssertEqual(presentSpy.presentInfoAddressReceived, [])
        XCTAssertEqual(presentSpy.receivedActions, [UnsuccessfulDeliveryCardAction.link(url: urlExpected)])

        XCTAssertEqual(presentSpy.presentLoadStateCount, 0)
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testDismiss_WhenDidCall_ShouldCallPresentNextStep() {
        sut.dismiss()
        XCTAssertEqual(presentSpy.receivedActions, [.close])
    }
    
    func testOpenHelpcenter_WhenDid_ShouldOpenCallHelpCenter() throws {
        sut = UnsuccessfulDeliveryCardInteractor(presenter: presentSpy, cardType: .debit, dependencies: dependencies, service: serviceSpy)
        let faqDeliveryFaileds = "faqDeliveryFailedValueToTest"
        let urlExpected = try XCTUnwrap(URL(string: faqDeliveryFaileds))
        let expectedEvent = UnsuccessfulDeliveryCardEvent.unsuccessAttemptDebit(action: .helpCenter).event()
        featureManagerMock.override(key: .faqDeliveryCardFailed, with: faqDeliveryFaileds)

        sut.openHelpcenter()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
        XCTAssertEqual(presentSpy.receivedActions, [UnsuccessfulDeliveryCardAction.link(url: urlExpected)])
    }
}
