import XCTest
import AnalyticsModule
import Core
import UI
@testable import Card

final class BillNotificationCoordinatorPresenterSpy: BillNotificationCoordinating {
    var viewController: UIViewController?
    var actions:[BillNotificationAction] = []
    func perform(action: BillNotificationAction) {
        actions.append(action)
    }
}

final class BillNotificationDisplaySpy: BillNotificationDisplaying {

    private(set) var models: [StatefulFeedbackViewModel] = []
    private(set) var erros: [ApiError] = []
    private(set) var displayLoadStateCount = 0
    
    func displayLoadState() {
        displayLoadStateCount += 1
    }

    func displaydReciveError(_ error: ApiError) {
        erros.append(error)
    }
    
    func displayIdealState(_ model: StatefulFeedbackViewModel) {
        models.append(model)
    }
    
}

final class BillNotificationPresenterTests: XCTestCase {
    private lazy var container = DependencyContainerMock()
    private lazy var coordinatorSpy = BillNotificationCoordinatorPresenterSpy()
    private lazy var viewControllerSpy = BillNotificationDisplaySpy()
   
    private lazy var sut: BillNotificationPresenter = {
        let presenter = BillNotificationPresenter(coordinator: coordinatorSpy, dependencies: container)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testPresentIdealState_WhenSuccessInNotification_ShouldDisplayIdealState() {
        sut.presentIdealState()
        XCTAssertEqual(viewControllerSpy.models.count, 1)
    }
    
    func testDidNextStep_WhenActionDismiss_ShouldPerformCoordinator() {
        sut.didNextStep(action: .dismiss)
        XCTAssertEqual(coordinatorSpy.actions, [.dismiss])
    }
    
    func testPresentErrorState_WhenReciveError_ShouldDisplayReciveError() {
        let expectedError = ApiError.connectionFailure
        sut.presentErrorState(error: expectedError)
        XCTAssertEqual(viewControllerSpy.erros, [.connectionFailure])
    }
   
    func testPresentLoadState_WhenDidCallLoadState_ShouldDislplayLoadState() {
        sut.presentLoadState()
        XCTAssertEqual(viewControllerSpy.displayLoadStateCount, 1)
    }
}
