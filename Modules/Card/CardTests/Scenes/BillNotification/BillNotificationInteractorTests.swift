import XCTest
import AnalyticsModule
import Core
@testable import Card

final class BillNotificationServicingSpy: BillNotificationServicing {
    var expectedResult:  (Result<NoContent, ApiError>) = .failure(.bodyNotFound)
    
    func putBillNotification(invoiceId: String, completion: @escaping CompletionBillNotification) {
        completion(expectedResult)
    }
}

final class BillNotificationPresentingSpy: BillNotificationPresenting {
    private(set) var presentLoadStateCount = 0
    private(set) var presentIdealStateCount = 0
    private(set) var erros: [ApiError] = []
    var viewController: BillNotificationDisplaying?
    var actions: [BillNotificationAction] = []
    
    func didNextStep(action: BillNotificationAction) {
        actions.append(action)
    }
    
    func presentIdealState() {
        presentIdealStateCount += 1
    }
    
    func presentErrorState(error: ApiError) {
        erros.append(error)
    }
    
    func presentLoadState() {
        presentLoadStateCount += 1
    }
}

extension ApiError: Equatable {
    public static func == (lhs: ApiError, rhs: ApiError) -> Bool {
        lhs.requestError?.code == rhs.requestError?.code
    }
}

// MARK: - Tests
final class BillNotificationInteractorTests: XCTestCase {
    lazy var serviceSpy = BillNotificationServicingSpy()
    lazy var presentSpy = BillNotificationPresentingSpy()
    private lazy var analytics = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(analytics)
    
    lazy var sut: BillNotificationInteractor = {
        let interactor = BillNotificationInteractor(service: serviceSpy, presenter: presentSpy, dependencies: dependencies, invoiceId: "idBolado")
        return interactor
    }()
    
    func testRequestNotification_WhenSuccess_ShouldCallPresentIdealState() {
        serviceSpy.expectedResult = .success(NoContent())
        sut.requestNotificationRegister()
        XCTAssertEqual(presentSpy.presentIdealStateCount, 1)
        XCTAssertEqual(presentSpy.presentLoadStateCount, 1)
        XCTAssertNil(sut.currentError)
    }
    
    func testRequestNotification_WhenConnectionFailure_ShouldCallPresentErroState() {
        let expectedEvent = BillNotificationEvent.connectionError(action: .conectionError).event()
        let error = ApiError.connectionFailure
        serviceSpy.expectedResult = .failure(error)
        sut.requestNotificationRegister()
        XCTAssertEqual(presentSpy.erros, [error])
        XCTAssertNotEqual(presentSpy.erros, [ApiError.serverError])
        XCTAssertTrue(analytics.equals(to: expectedEvent))
        XCTAssertEqual(presentSpy.presentLoadStateCount, 1)
    }
     
    func testRequestNotification_WhenGenericError_ShouldCallPresentErroState() {
        let expectedEvent = BillNotificationEvent.genericError(action: .genericError).event()
        let error = ApiError.serverError
        serviceSpy.expectedResult = .failure(error)
        sut.requestNotificationRegister()
        XCTAssertEqual(presentSpy.erros, [error])
        XCTAssertTrue(analytics.equals(to: expectedEvent))
        XCTAssertEqual(presentSpy.presentLoadStateCount, 1)

    }
    
    func testTrackingAlertStateError_WhenConnectionFailure_ShouldTrackConnectionErrorRetryEvent() {
        let expectedEvent = BillNotificationEvent.connectionError(action: .retry).event()
        let error = ApiError.connectionFailure
        serviceSpy.expectedResult = .failure(error)
        sut.requestNotificationRegister()
        sut.trackingAlertStateError(tryAgain: true)
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testTrackingAlertStateError_WhenConnectionFailure_ShouldTrackConnectionErrorCancelEvent() {
        let expectedEvent = BillNotificationEvent.connectionError(action: .cancel).event()
        let error = ApiError.connectionFailure
        serviceSpy.expectedResult = .failure(error)
        sut.requestNotificationRegister()
        sut.trackingAlertStateError(tryAgain: false)
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testTrackingAlertStateError_WhenGenericError_ShouldTrackGenericErrorrRetryEvent() {
        let expectedEvent = BillNotificationEvent.genericError(action: .retry).event()
        let error = ApiError.bodyNotFound
        serviceSpy.expectedResult = .failure(error)
        sut.requestNotificationRegister()
        sut.trackingAlertStateError(tryAgain: true)
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testTrackingAlertStateError_WhenGenericError_ShouldTrackGenericErrorrCancelEvent() {
        let expectedEvent = BillNotificationEvent.genericError(action: .cancel).event()
        let error = ApiError.bodyNotFound
        serviceSpy.expectedResult = .failure(error)
        sut.requestNotificationRegister()
        sut.trackingAlertStateError(tryAgain: false)
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testIGotIt_WhenDidCall_ShouldTrackNotifiedEvent() {
        let expectedEvent = BillNotificationEvent.notified.event()
        sut.iGotIt()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testIGotIt_WhenDidCall_ShouldperformDismiss() {
        sut.iGotIt()
        XCTAssertEqual(presentSpy.actions, [.dismiss])
    }
    
    func testDismiss_WhenDidCall_ShouldSendActionToPresent(){
        sut.dismiss()
        XCTAssertEqual(presentSpy.actions, [.dismiss])
    }
   
    func testFeedbackPagePrimaryButtonDidTap_WhenSuccess_ShouldDismiss() {
        let expectedEvent = BillNotificationEvent.notified.event()
        serviceSpy.expectedResult = .success(NoContent())
        sut.requestNotificationRegister()
        sut.feedbackPagePrimaryButtonDidTap()
        XCTAssertEqual(presentSpy.actions, [.dismiss])
        XCTAssertTrue(analytics.equals(to: expectedEvent))
        XCTAssertNil(sut.currentError)
    }
    
    func testFeedbackPagePrimaryButtonDidTap_WhenConnectionFailure_ShouldTryAgainRequest() {
        let expectedEvent = BillNotificationEvent.connectionError(action: .conectionError).event()
        let error = ApiError.connectionFailure
        serviceSpy.expectedResult = .failure(error)
        sut.requestNotificationRegister()
        sut.feedbackPagePrimaryButtonDidTap()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
        XCTAssertNotNil(sut.currentError)
    }
}
