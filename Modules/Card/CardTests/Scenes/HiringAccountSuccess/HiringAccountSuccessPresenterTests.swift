import XCTest
@testable import Card
import UI

private final class HiringAccountSuccessCoordinatingSpy: HiringAccountSuccessCoordinating {
    //MARK: - perform
    private(set) var action: [HiringAccountSuccessAction] = []

    func perform(action: HiringAccountSuccessAction) {
        self.action.append(action)
    }
}

final class HiringAccountSuccessPresenterTests: XCTestCase {
    // MARK: - Variables
    private let coordinatorSpy = HiringAccountSuccessCoordinatingSpy()
    private lazy var sut = HiringAccountSuccessPresenter(coordinator: coordinatorSpy)

    // MARK: - Tests
    func testDidNextStep_WhenConfirmIsCalled_ShouldCallCoordinator() {
        sut.didNextStep(action: .confirm)
        XCTAssertEqual(coordinatorSpy.action, [.confirm])
    }
    
    func testDidNextStep_WhenCloseIsCalled_ShouldCallCoordinator() {
        sut.didNextStep(action: .close)
        XCTAssertEqual(coordinatorSpy.action, [.close])
    }
}
