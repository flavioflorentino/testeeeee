import AnalyticsModule
import XCTest
@testable import Card

private final class HiringAccountSuccessPresentingSpy: HiringAccountSuccessPresenting {
    // MARK: - Variables
    private(set) var action: [HiringAccountSuccessAction] = []
    
    // MARK: - Public Methods
    func didNextStep(action: HiringAccountSuccessAction) {
        self.action.append(action)
    }
}

final class HiringAccountSuccessInteractorTests: XCTestCase {
    // MARK: - Variables
    typealias Dependencies = HasAnalytics
    private let analytics = AnalyticsSpy()
    private let presenterSpy = HiringAccountSuccessPresentingSpy()
    private lazy var container: Dependencies = DependencyContainerMock(analytics)
    
    private lazy var sut = HiringAccountSuccessInteractor(presenter: presenterSpy, dependencies: container)

    // MARK: - Public Methods
    func testPressConfirm_WhenPressConfirm_ShouldSendEvent() {
        sut.pressConfirm()
        XCTAssertEqual(presenterSpy.action, [.confirm])
        let event = analytics.events.last
        let action = event?.properties["action"] as? String
        XCTAssertEqual(analytics.logCalledCount, 1)
        XCTAssertEqual(event?.name, "PicPay Card - Credit - Activated Success")
        XCTAssertNil(action)
    }
    
    func testPressConfirm_WhenPressClose_ShouldSendEvent() {
        sut.pressClose()
        XCTAssertEqual(presenterSpy.action, [.close])
    }
}
