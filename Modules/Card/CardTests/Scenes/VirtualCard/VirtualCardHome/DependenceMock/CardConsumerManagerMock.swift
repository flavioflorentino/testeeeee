import Foundation
import XCTest

@testable import Card

public class CardConsumerManagerMock: NSObject, CardConsumerContract {
    public var tokenMock = ""
    public var token: String {
        tokenMock
    }

    public var consumerIdMock = 0
    public var consumerId: Int {
        consumerIdMock
    }

    public static let shared = CardConsumerManager()
}
