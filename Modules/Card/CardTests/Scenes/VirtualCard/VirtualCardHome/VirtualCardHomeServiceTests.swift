import Core
import XCTest
@testable import Card

private final class VirtualCardHomeServiceTests: XCTestCase {
    private let consumerManagerMock = CardConsumerManagerMock()

    private lazy var sut: VirtualCardHomeService = {
        let button = VirtualCardButton(action: .cardInfo, newFeature: false)
        let flow = VirtualCardFlowInput(button: button)
        return VirtualCardHomeService(dependencies: DependencyContainerMock(consumerManagerMock), input: flow)
    }()

    func testbuildVirtualCard_WhenPassValidData_ShouldReturnAnValidObject() {
        consumerManagerMock.tokenMock = "5a88eb2f1b5c4252ac8866bea11654cf"
        consumerManagerMock.consumerIdMock = 8599172
        let encryptedInf = "WcK91AbTu5AE23xk5Rtv3A== /_s_/ JIie85vuiHMKf17vJxU5ggu0hBHbifHlnj7MWP3SvrTfEnSx4m9AwA/OeioJWcT6MseFxzSAQUIFQP83RKS+kDpNmJ9nDw9vfhQiNf4+6TRHxX45AEYJpzzXrHZWSe4Mp7ojpnEGWp9UowCZCuHUVA=="

        let virtualCardResult = VirtualCardResult(info: encryptedInf, blocked: false, blockEnabled: true)
        sut.buildVirtualCard(with: virtualCardResult) { result in
            let criptoResult = try? result.get()
            XCTAssertEqual(criptoResult?.card.cvv, "828")
            XCTAssertEqual(criptoResult?.card.expiration, "2028-10-08")
            XCTAssertEqual(criptoResult?.card.name, "RENAN MACHADO")
            XCTAssertEqual(criptoResult?.card.number, "2227630000101110")
        }
    }

    func testbuildVirtualCard_WhenPassInvalidData_ShouldReturnNil() {
        let virtualCardResult = VirtualCardResult(info: "0", blocked: false, blockEnabled: true)
        sut.buildVirtualCard(with: virtualCardResult) { result in
            XCTAssertNil(try? result.get())
        }
    }
}
