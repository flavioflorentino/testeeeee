import Core
import UI
import XCTest
@testable import Card

private final class VirtualCardHomeDisplaySpy: VirtualCardHomeDisplaying {
    private(set) var clipboardFeedbackCount = 0
    private(set) var loadingStateCount = 0
    private(set) var errorStateCount = 0
    private(set) var idealStateCount = 0
    private(set) var customErrorStateCount = 0
    private(set) var errorState: StatefulFeedbackViewModel?
    private(set) var modelSpy: VirtualCardDisplayModel?
    private(set) var displayDelete = false
    private(set) var displayBlock = false
    private(set) var displayBlockedCard = false
    private(set) var displayStatusWarningCount = 0
    private(set) var displayStatusWarningSheetSpy: UIAlertController?
    private(set) var displayStatusWarningActionSpy = ""
    private(set) var displayStatusErrorCount = 0
    private(set) var displayStatusErrorPopupSpy: PopupViewController?
    private(set) var displayStatusCount = 0
    private(set) var blockedStatusSpy = false
    private(set) var clipboardIsHiddenSpy = false
    
    func displayClipboardFeedback() {
        clipboardFeedbackCount += 1
    }
    
    func displayLoadingState() {
        loadingStateCount += 1
    }
    
    func displayErrorState(error: StatefulFeedbackViewModel) {
        errorStateCount += 1
        errorState = error
    }
    
    func displayCustomErrorView(_ customErrorView: ApolloFeedbackView) {
        customErrorStateCount += 1
    }
    
    func displayIdealState(model: VirtualCardDisplayModel) {
        idealStateCount += 1
        modelSpy = model
        displayDelete = model.isDeleteHidden
        displayBlock = model.isBlockHidden
        displayBlockedCard = model.isBlocked
    }
    
    func displayStatusWarning(actionSheet: UIAlertController, action: String) {
        displayStatusWarningCount += 1
        displayStatusWarningSheetSpy = actionSheet
        displayStatusWarningActionSpy = action
    }
    
    func displayStatusError(popup: PopupViewController) {
        displayStatusErrorCount += 1
        displayStatusErrorPopupSpy = popup
    }
    
    func displayStatus(isBlocked: Bool) {
        displayStatusCount += 1
        blockedStatusSpy = isBlocked
        clipboardIsHiddenSpy = isBlocked
    }
}

private final class VirtualCardHomeCoordinatorSpy: VirtualCardHomeCoordinating {
    var viewController: UIViewController?
    var delegate: VirtualCardHomeCoordinatorDelegate?
    
    private(set) var actionSpy: [VirtualCardHomeAction] = []
    
    func perform(action: VirtualCardHomeAction) {
        actionSpy.append(action)
    }
}

final class VirtualCardHomePresenterTests: XCTestCase {
    
    private let faqURLMock = "https://meajuda.picpay.com/hc/pt-br/categories/360003297112-PicPay-Card"
    private let container = DependencyContainerMock()
    private lazy var coordinatorSpy = VirtualCardHomeCoordinatorSpy()
    private lazy var viewControllerSpy = VirtualCardHomeDisplaySpy()
    private lazy var sut: VirtualCardHomePresenter = {
        let presenter = VirtualCardHomePresenter(coordinator: coordinatorSpy, dependencies: container)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testDidNextStep_WhenActionFaq_ShouldPerformCoordinator() throws {
        let url = try XCTUnwrap(URL(string: faqURLMock))
        sut.didNextStep(action: .faq(url: url))
        XCTAssertEqual(coordinatorSpy.actionSpy, [.faq(url: url)])
    }
    
    func testDidNextStep_WhenActionDismiss_ShouldPerformCoordinator() {
        sut.didNextStep(action: .dismiss)
        XCTAssertEqual(coordinatorSpy.actionSpy, [.dismiss])
    }
    
    func testDidNextStep_WhenActionSuccessed_ShouldPerformCoordinator() {
        sut.didNextStep(action: .succeeded)
        XCTAssertEqual(coordinatorSpy.actionSpy, [.succeeded])
    }
    
    func testDidNextStep_WhenActionDelete_ShouldPerformCoordinator() {
        sut.didNextStep(action: .delete)
        XCTAssertEqual(coordinatorSpy.actionSpy, [.delete])
    }
    
    func testDidNextStepFaq_WhenTapped_ShouldPerformCoordinator() throws {
        let url = try XCTUnwrap(URL(string: faqURLMock))
        sut.didNextStep(action: .faq(url: url))
        XCTAssertEqual(coordinatorSpy.actionSpy, [VirtualCardHomeAction.faq(url: url)])
    }
    
    func testPresentClipboardFeedback_WhenTapped_ShouldDisplayInViewController() {
        sut.presentClipboardFeedback()
        XCTAssertEqual(viewControllerSpy.clipboardFeedbackCount, 1)
    }
    
    func testPresentLoadingState_WhenInteractorCall_ShouldUpdateViewController() {
        sut.presentLoadingState()
        XCTAssertEqual(viewControllerSpy.loadingStateCount, 1)
    }
    
    func testPresentFeatureErrorState_ShouldDisplayError() {
        sut.presentFeatureErrorState()
        XCTAssertEqual(viewControllerSpy.errorStateCount, 1)
    }
    
    func testPresentCustomErrorState_ShouldDisplayCustomError() {
        sut.presentCustomErrorState()
        XCTAssertEqual(viewControllerSpy.customErrorStateCount, 1)
    }
    
    func testPresentErrorState_WhenConnectionFailure_ShouldDisplayConnectionError() {
        let errorMock = ApiError.connectionFailure
        sut.presentErrorState(error: errorMock)
        XCTAssertEqual(viewControllerSpy.errorStateCount, 1)
        XCTAssertNotNil(viewControllerSpy.errorState)
        XCTAssertEqual(viewControllerSpy.errorState?.content?.title, Strings.Default.Connection.title)
    }
    
    func testPresentErrorState_WhenNotAvailable_ShouldDisplayConnectionError() {
        let errorMock = ApiError.timeout
        sut.presentErrorState(error: errorMock)
        XCTAssertEqual(viewControllerSpy.errorStateCount, 1)
        XCTAssertNotNil(viewControllerSpy.errorState)
        XCTAssertEqual(viewControllerSpy.errorState?.content?.title, Strings.ApiError.NotAvailable.title)
    }
    
    func testPresentErrorState_WhenServiceGotError_ShouldDisplayConnectionError() {
        let errorMock = ApiError.serverError
        sut.presentErrorState(error: errorMock)
        XCTAssertEqual(viewControllerSpy.errorStateCount, 1)
        XCTAssertNotNil(viewControllerSpy.errorState)
        XCTAssertEqual(viewControllerSpy.errorState?.content?.title, Strings.VirtualCard.Home.Error.title)
    }
    
    func testPresentIdealState_WhenCardIsEmpty_ShouldDisplayFeatureError() {
        let cardMock = VirtualCard(number: "", cvv: "", expiration: "", name: "")
        let resultMock = VirtualCardCriptoResult(card: cardMock, blocked: true, blockEnabled: true)
        let modelMock = VirtualCardPresenterModel(delete: true, block: true, result: resultMock)
        sut.presentIdealState(model: modelMock)
        XCTAssertEqual(viewControllerSpy.errorStateCount, 1)
        XCTAssertNotNil(viewControllerSpy.errorState)
        XCTAssertEqual(viewControllerSpy.errorState?.content?.title, Strings.VirtualCard.Home.Error.title)
    }
    
    func testPresentIdealState_WhenCardIsInconsistent_ShouldDisplayFeatureError() {
        let cardMock = VirtualCard(number: "215123", cvv: "12223", expiration: "02-50-122", name: "21521gff12")
        let resultMock = VirtualCardCriptoResult(card: cardMock, blocked: true, blockEnabled: true)
        let modelMock = VirtualCardPresenterModel(delete: true, block: true, result: resultMock)
        sut.presentIdealState(model: modelMock)
        XCTAssertEqual(viewControllerSpy.errorStateCount, 1)
        XCTAssertNotNil(viewControllerSpy.errorState)
        XCTAssertEqual(viewControllerSpy.errorState?.content?.title, Strings.VirtualCard.Home.Error.title)
    }
    
    func testPresentIdealState_WhenCardIsRight_ShouldDisplayIdealState() {
        let cardMock = VirtualCard(number: "1234567887654321", cvv: "922", expiration: "2028-02-23", name: "SILVIO SANTOS")
        let resultMock = VirtualCardCriptoResult(card: cardMock, blocked: false, blockEnabled: true)
        let modelMock = VirtualCardPresenterModel(delete: true, block: true, result: resultMock)
        sut.presentIdealState(model: modelMock)
        XCTAssertEqual(viewControllerSpy.idealStateCount, 1)
        XCTAssertNotNil(viewControllerSpy.modelSpy)
        XCTAssertEqual(viewControllerSpy.modelSpy?.card.expiration, "02/28")
        XCTAssertEqual(viewControllerSpy.modelSpy?.card.number, "1234  5678  8765  4321")
        XCTAssertEqual(viewControllerSpy.modelSpy?.isClipboardHidden, false)
        XCTAssertEqual(viewControllerSpy.modelSpy?.isDeleteHidden, false)
        XCTAssertEqual(viewControllerSpy.modelSpy?.isBlockHidden, false)
        XCTAssertEqual(viewControllerSpy.modelSpy?.isBlocked, false)
        XCTAssertEqual(coordinatorSpy.actionSpy, [.succeeded])
        XCTAssertFalse(viewControllerSpy.displayDelete)
        XCTAssertFalse(viewControllerSpy.displayBlock)
        XCTAssertFalse(viewControllerSpy.displayBlockedCard)
    }
    
    func testPresentIdealState_WhenCardIsBlocked_ShouldDisplayIdealState() {
        let cardMock = VirtualCard(number: "1234567887654321", cvv: "922", expiration: "2028-02-23", name: "SILVIO SANTOS")
        let resultMock = VirtualCardCriptoResult(card: cardMock, blocked: true, blockEnabled: true)
        let modelMock = VirtualCardPresenterModel(delete: true, block: true, result: resultMock)
        sut.presentIdealState(model: modelMock)
        XCTAssertEqual(viewControllerSpy.idealStateCount, 1)
        XCTAssertNotNil(viewControllerSpy.modelSpy)
        XCTAssertEqual(viewControllerSpy.modelSpy?.card.expiration, "02/28")
        XCTAssertEqual(viewControllerSpy.modelSpy?.card.number, "1234  5678  8765  4321")
        XCTAssertEqual(viewControllerSpy.modelSpy?.isClipboardHidden, true)
        XCTAssertEqual(viewControllerSpy.modelSpy?.isDeleteHidden, false)
        XCTAssertEqual(viewControllerSpy.modelSpy?.isBlockHidden, false)
        XCTAssertEqual(viewControllerSpy.modelSpy?.isBlocked, true)
        XCTAssertEqual(coordinatorSpy.actionSpy, [.succeeded])
        XCTAssertFalse(viewControllerSpy.displayDelete)
        XCTAssertFalse(viewControllerSpy.displayBlock)
        XCTAssertTrue(viewControllerSpy.displayBlockedCard)
    }
    
    func testPresentStatusWarning_WhenIsUnlocked_ShouldDisplayStatusWarning() {
        sut.presentStatusWarning(isBlocked: false)
        
        XCTAssertEqual(viewControllerSpy.displayStatusWarningCount, 1)
        XCTAssertEqual(viewControllerSpy.displayStatusWarningSheetSpy?.title, Strings.VirtualCard.Block.ActionSheet.title)
        XCTAssertEqual(viewControllerSpy.displayStatusWarningSheetSpy?.message, Strings.VirtualCard.Block.ActionSheet.message)
        XCTAssertEqual(viewControllerSpy.displayStatusWarningActionSpy, Strings.VirtualCard.Block.ActionSheet.action)
    }
    
    func testPresentStatusWarning_WhenIsBlocked_ShouldDisplayStatusWarning() {
        sut.presentStatusWarning(isBlocked: true)
        
        XCTAssertEqual(viewControllerSpy.displayStatusWarningCount, 1)
        XCTAssertEqual(viewControllerSpy.displayStatusWarningSheetSpy?.title, Strings.VirtualCard.Unlock.ActionSheet.title)
        XCTAssertEqual(viewControllerSpy.displayStatusWarningSheetSpy?.message, Strings.VirtualCard.Unlock.ActionSheet.message)
        XCTAssertEqual(viewControllerSpy.displayStatusWarningActionSpy, Strings.VirtualCard.Unlock.ActionSheet.action)
    }
    
    func testPresentStatusError_WhenIsUnlocked_ShouldDisplayStatusError() throws {
        sut.presentStatusError(isBlocked: false)

        XCTAssertEqual(viewControllerSpy.displayStatusErrorCount, 1)
        XCTAssertNotNil(viewControllerSpy.displayStatusErrorPopupSpy)
    }
    
    func testPresentStatusError_WhenIsBlocked_ShouldDisplayStatusError() throws {
        sut.presentStatusError(isBlocked: true)
        
        XCTAssertEqual(viewControllerSpy.displayStatusErrorCount, 1)
        XCTAssertNotNil(viewControllerSpy.displayStatusErrorPopupSpy)
    }
    
    func testPresentStatus_WhenIsUnlocked_ShouldDisplayStatus() {
        sut.presentStatus(isBlocked: false)
        
        XCTAssertEqual(viewControllerSpy.displayStatusCount, 1)
        XCTAssertFalse(viewControllerSpy.blockedStatusSpy)
        XCTAssertFalse(viewControllerSpy.clipboardIsHiddenSpy)
    }
    
    func testPresentStatus_WhenIsBlocked_ShouldDisplayStatus() {
        sut.presentStatus(isBlocked: true)
        
        XCTAssertEqual(viewControllerSpy.displayStatusCount, 1)
        XCTAssertTrue(viewControllerSpy.blockedStatusSpy)
        XCTAssertTrue(viewControllerSpy.clipboardIsHiddenSpy)
    }
}
