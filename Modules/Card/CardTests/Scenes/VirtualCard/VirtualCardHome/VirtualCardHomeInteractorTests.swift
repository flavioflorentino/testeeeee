import AnalyticsModule
import Core
import FeatureFlag
import UI
import XCTest
@testable import Card

private final class VirtualCardHomeServicingSpy: VirtualCardHomeServicing {
    private(set) var blockCount = 0
    private(set) var unlockCount = 0

    private(set) var loadDataCount = 0
    private(set) var passwordSpy: String?
    
    var expectedResult: VirtualCardLoadDataCompletion?
    
    func loadData(password: String?, completion: @escaping VirtualCardLoadDataCompletion) {
        passwordSpy = password
        loadDataCount += 1
    }
    
    func changeStatus(isBlocked: Bool, completion: @escaping VirtualCardServiceCompletion) {
        isBlocked ? (unlockCount += 1) : (blockCount += 1)
    }
}

private final class VirtualCardHomePresentingSpy: VirtualCardHomePresenting {
    
    var viewController: VirtualCardHomeDisplaying?
    
    private(set) var clipboardFeedbackCount = 0
    private(set) var nextStepCount = 0
    private(set) var nextStepAction: VirtualCardHomeAction?
    private(set) var loadingStateCount = 0
    private(set) var featureErrorStateCount = 0
    private(set) var errorStateCount = 0
    private(set) var errorApiSpy: ApiError?
    private(set) var idealStateCount = 0
    private(set) var deleteEnabled: Bool = false
    private(set) var presentStatusWarningCount = 0
    private(set) var presentStatusErrorCount = 0
    private(set) var presentCustomErrorCount = 0
    private(set) var presentStatusCount = 0
    private(set) var isBlockedSpy: Bool = false
    
    func presentClipboardFeedback() {
        clipboardFeedbackCount += 1
    }
    
    func didNextStep(action: VirtualCardHomeAction) {
        nextStepCount += 1
        nextStepAction = action
    }
    
    func presentLoadingState() {
        loadingStateCount += 1
    }
    
    func presentFeatureErrorState() {
        featureErrorStateCount += 1
    }
    
    func presentErrorState(error: ApiError) {
        errorStateCount += 1
        errorApiSpy = error
    }
    
    func presentIdealState(model: VirtualCardPresenterModel) {
        idealStateCount += 1
    }
    
    func presentStatusWarning(isBlocked: Bool) {
        presentStatusWarningCount += 1
        isBlockedSpy = isBlocked
    }
    
    func presentStatusError(isBlocked: Bool) {
        presentStatusErrorCount += 1
        isBlockedSpy = isBlocked
    }
    
    func presentCustomErrorState() {
        presentCustomErrorCount += 1
    }
    
    func presentStatus(isBlocked: Bool) {
        presentStatusCount += 1
        isBlockedSpy = isBlocked
    }
}

private final class AuthManagerSpy: AuthContract {
    var isSuccess = true
    var isBiometric = true
    
    func authenticate(sucessHandler: @escaping (String, Bool) -> Void, cancelHandler: (() -> Void)?) {
        guard isSuccess else {
            cancelHandler?()
            return
        }
        sucessHandler("1234", isBiometric)
    }
}

final class VirtualCardHomeInteractorTests: XCTestCase {
    
    typealias Dependencies = HasAnalytics & HasFeatureManager & HasAuthManagerDependency
    private let analytics = AnalyticsSpy()
    private let featureManagerMock = FeatureManagerMock()
    private let authManagerMock = AuthManagerSpy()
    private let faqURLMock = "https://meajuda.picpay.com/hc/pt-br/categories/360003297112-PicPay-Card"
    private let serviceSpy = VirtualCardHomeServicingSpy()
    private let presenterSpy = VirtualCardHomePresentingSpy()
    private lazy var container: Dependencies = DependencyContainerMock(analytics, featureManagerMock, authManagerMock)
    
    private lazy var sut = VirtualCardHomeInteractor(service: serviceSpy, presenter: presenterSpy, dependencies: container)
    
    func testClose_ShouldTrackEvent() {
        let expectedEvent = VirtualCardHomeEvent.generated(name: .generated, action: .close).event()
        sut.close()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }

    func testClipboardAction_WhenCardNumberIsNull_ShouldCallPresenterAndTrackEvent() {
        sut.clipboardAction()
        XCTAssertEqual(presenterSpy.featureErrorStateCount, 1)
    }
    
    func testClipboardAction_WhenCardNumberIsEmpty_ShouldCallPresenterAndTrackEvent() {
        sut.virtualCardModel = VirtualCard(number: "", cvv: "123", expiration: "2028-07", name: "SILVIO SANTOS")
        sut.clipboardAction()
        XCTAssertEqual(presenterSpy.featureErrorStateCount, 1)
    }
    
    func testClipboardAction_WhenCardNumberIsValid_ShouldCallPresenterAndTrackEvent() {
        let expectedEvent = VirtualCardHomeEvent.generated(name: .generated, action: .clipboard).event()
        sut.virtualCardModel = VirtualCard(number: "12345678", cvv: "123", expiration: "2028-07", name: "SILVIO SANTOS")
        sut.clipboardAction()
        XCTAssertEqual(presenterSpy.clipboardFeedbackCount, 1)
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }

    func testDeleteAction_ShouldPresentNextStepAndTrackEvent() {
        let expectedEvent = VirtualCardHomeEvent.generated(name: .generated, action: .delete).event()
        sut.deleteAction()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
        XCTAssertEqual(presenterSpy.nextStepCount, 1)
        XCTAssertEqual(presenterSpy.nextStepAction, VirtualCardHomeAction.delete)
    }
    
    func testOpenFaq_WhenHasNoValue_ShouldPresentError() {
        featureManagerMock.override(keys: .opsVirtualCardFaq, with: "")
        sut.openFaq()
        XCTAssertEqual(presenterSpy.featureErrorStateCount, 1)
    }

    func testOpenFaq_WhenHasURL_ShouldPresentToNextStepAndTrackEvent() throws {
        let expectedEvent = VirtualCardHomeEvent.generated(name: .generated, action: .faq).event()
        let url = try XCTUnwrap(URL(string: faqURLMock))
        featureManagerMock.override(keys: .opsVirtualCardFaq, with: faqURLMock)
        sut.openFaq()

        XCTAssertEqual(presenterSpy.nextStepCount, 1)
        XCTAssertNotNil(presenterSpy.nextStepAction)
        XCTAssertEqual(presenterSpy.nextStepAction, VirtualCardHomeAction.faq(url: url))
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testLoadData_WhenPasswordFailed_ShouldTrackEvent() {
        let expectedEvent = VirtualCardHomeEvent.auth(action: .fingerprintCancel).event()
        authManagerMock.isSuccess = false
        featureManagerMock.override(keys: .opsVirtualCardDelete, with: true)
        sut.loadData()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testLoadData_WhenPasswordNumeralSuccessed_ShouldLoadServiceAndTrackEvent() {
        let expectedEvent = VirtualCardHomeEvent.auth(action: .numeralPassword).event()
        authManagerMock.isSuccess = true
        authManagerMock.isBiometric = false
        featureManagerMock.override(keys: .opsVirtualCardDelete, with: true)
        sut.loadData()
        
        XCTAssertTrue(analytics.equals(to: expectedEvent))
        XCTAssertEqual(presenterSpy.loadingStateCount, 1)
        XCTAssertEqual(serviceSpy.loadDataCount, 1)
    }
    
    func testLoadData_WhenPasswordBiometricSuccessed_ShouldLoadServiceAndTrackEvent() {
        let expectedEvent = VirtualCardHomeEvent.auth(action: .fingerprint).event()
        authManagerMock.isSuccess = true
        authManagerMock.isBiometric = true
        featureManagerMock.override(keys: .opsVirtualCardDelete, with: true)
        sut.loadData()
        
        XCTAssertTrue(analytics.equals(to: expectedEvent))
        XCTAssertEqual(presenterSpy.loadingStateCount, 1)
        XCTAssertEqual(serviceSpy.loadDataCount, 1)
    }
    
    // MARK: - Block and Unlock
    func testStatusWarningDidOpen_WhenIsUnlocked_ShouldTrackEventAndPresentStatusWarning() {
        let expectedEvent = VirtualCardHomeEvent.generated(name: .generated, action: .blockWarning).event()
        sut.virtualCardIsBlocked = false
        sut.statusWarningDidOpen()
        
        XCTAssertTrue(analytics.equals(to: expectedEvent))
        XCTAssertEqual(presenterSpy.presentStatusWarningCount, 1)
    }
    
    func testStatusWarningDidOpen_WhenIsBlocked_ShouldTrackEventAndPresentStatusWarning() {
        let expectedEvent = VirtualCardHomeEvent.generated(name: .blocked, action: .blockWarning).event()
        sut.virtualCardIsBlocked = true
        sut.statusWarningDidOpen()
        
        XCTAssertTrue(analytics.equals(to: expectedEvent))
        XCTAssertEqual(presenterSpy.presentStatusWarningCount, 1)
    }
    
    func testStatusWarningDidCancel_WhenIsUnlocked_ShouldTrackEvent() {
        let expectedEvent = VirtualCardHomeEvent.dialogFreeze(action: .cancel).event()
        sut.virtualCardIsBlocked = false
        sut.statusWarningDidCancel()
        
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testStatusWarningDidCancel_WhenIsBlocked_ShouldTrackEvent() {
        let expectedEvent = VirtualCardHomeEvent.dialogUnfreeze(action: .cancel).event()
        sut.virtualCardIsBlocked = true
        sut.statusWarningDidCancel()
        
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
 
    func testStatusErrorDidRetry_WhenIsUnlocked_ShouldTrackEventAndCallBlockService() {
        let expectedEvent = VirtualCardHomeEvent.errorAlertAction(name: .unlock, action: .retry).event()
        sut.virtualCardIsBlocked = false
        sut.statusErrorDidRetry()
        
        XCTAssertTrue(analytics.equals(to: expectedEvent))
        XCTAssertEqual(serviceSpy.blockCount, 1)
    }
    
    func testStatusErrorDidRetry_WhenIsBlocked_ShouldTrackEventAndCallBlockService() {
        let expectedEvent = VirtualCardHomeEvent.errorAlertAction(name: .block, action: .retry).event()
        sut.virtualCardIsBlocked = true
        sut.statusErrorDidRetry()
        
        XCTAssertTrue(analytics.equals(to: expectedEvent))
        XCTAssertEqual(serviceSpy.unlockCount, 1)
    }
    
    func testStatusErrorDidCancel_WhenIsUnlocked_ShouldTrackEvent() {
        let expectedEvent = VirtualCardHomeEvent.errorAlertAction(name: .unlock, action: .cancel).event()
        sut.virtualCardIsBlocked = false
        sut.statusErrorDidCancel()
        
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testStatusErrorDidCancel_WhenIsBlocked_ShouldTrackEvent() {
        let expectedEvent = VirtualCardHomeEvent.errorAlertAction(name: .block, action: .cancel).event()
        sut.virtualCardIsBlocked = true
        sut.statusErrorDidCancel()
        
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testStatusChangeRequested_WhenIsUnlocked_ShouldTrackEventAndCallBlockService() {
        let expectedEvent = VirtualCardHomeEvent.dialogFreeze(action: .block).event()
        sut.virtualCardIsBlocked = false
        sut.statusChangeRequested()
        
        XCTAssertTrue(analytics.equals(to: expectedEvent))
        XCTAssertEqual(serviceSpy.blockCount, 1)
    }
    
    func testStatusChangeRequested_WhenIsBlocked_ShouldTrackEventAndCallUnlockService() {
        let expectedEvent = VirtualCardHomeEvent.dialogUnfreeze(action: .unlock).event()
        sut.virtualCardIsBlocked = true
        sut.statusChangeRequested()
        
        XCTAssertTrue(analytics.equals(to: expectedEvent))
        XCTAssertEqual(serviceSpy.unlockCount, 1)
    }
    
    // MARK: - Custom Error
    
    func testCustomError_WhenOpenChatTapped_ShouldTrackEventAndCallPresenterCorrectAction() {
        let expectedEvent = VirtualCardHomeEvent.customErrorAlertAction(name: .invalidStatus, action: .callCenterChat).event()
        sut.openHelpChat()
        
        XCTAssertTrue(analytics.equals(to: expectedEvent))
        XCTAssertEqual(presenterSpy.nextStepAction, .openHelpChat)
    }
    
    func testCustomError_WhenClosed_ShouldTrackEventAndCallPresenterWithDismiss() {
        let expectedEvent = VirtualCardHomeEvent.customErrorAlertAction(name: .invalidStatus, action: .close).event()
        sut.closeCustomError()
        
        XCTAssertTrue(analytics.equals(to: expectedEvent))
        XCTAssertEqual(presenterSpy.nextStepAction, .dismiss)
    }
}
