import Foundation
import UIKit
import XCTest
@testable import Card

final class VirtualCardHomeCoordinatoSpy: VirtualCardHomeCoordinatorDelegate {
    private(set) var actionSpy: [VirtualCardHomeAction] = []
    
    func homeDidNextStep(action: VirtualCardHomeAction) {
        actionSpy.append(action)
    }
}

final class VirtualCardHomeCoordinatorTests: XCTestCase {
    private let viewController = UIViewController()
    private let container = DependencyContainerMock()
    private let delegateSpy = VirtualCardHomeCoordinatoSpy()
    
    private lazy var sut: VirtualCardHomeCoordinator = {
        let coordinator = VirtualCardHomeCoordinator(dependencies: container)
        coordinator.viewController = viewController
        coordinator.delegate = delegateSpy
        return coordinator
    }()
    
    func testPerform_WhenSuccessed_ShouldDelegate(){
        sut.perform(action: .succeeded)
        XCTAssertEqual(delegateSpy.actionSpy, [.succeeded])
    }
    
    func testPerform_WhenDelete_ShouldDelegate(){
        sut.perform(action: .delete)
        XCTAssertEqual(delegateSpy.actionSpy, [.delete])
    }
    
    func testPerform_WhenDismiss_ShouldDelegate(){
        sut.perform(action: .dismiss)
        XCTAssertEqual(delegateSpy.actionSpy, [.dismiss])
    }
}
