import Foundation
import XCTest
@testable import Card


final class VirtualCardHomeDecryptTest: XCTestCase {
    lazy var sut = makeSut(token: "5a88eb2f1b5c4252ac8866bea11654cf", consumerId: 3123125)

    private func makeSut(token: String, consumerId: Int) -> VirtualCardHomeDecrypt {
        return VirtualCardHomeDecrypt(token: token, consumerId: consumerId)
    }
    
    func testDecrypt_WhenPassValidDataEncrypted_ShouldResultValidObjectStringScenarioOne() {
        let encryptedInfoString = "qQMBHIc3a0RMf2uEQ6rhUg== /_s_/ 6U5UHVaTJcIIjWPmX/w2isHs+J1WT+8f+F5oM2ki8YDRYVbtdNE1ZtGZj5zAgSQoM2BaTAS8TOpxKQvgBvSAdJRfXYtDl0yC+wZO6JLB8znCrx5gi0rg4uqBbXDABUpv"
        let encryptedData = VirtualCardResult(info: encryptedInfoString, blocked: false, blockEnabled: true)
        let expectedResultString = "{\"cvv\":\"616\",\"expire_date\":\"10/16\",\"card_number\":\"5126054966110566\",\"name\":\"Miles Morales\"}"
        XCTAssertEqual(sut.decryptInfo(with: encryptedData), expectedResultString)
    }
    
    func testDecrypt_WhenPassInvalidEncrypted_ShouldResultNil() {
        let encryptedInfoString = "qQMBHIc3a0RMf2uEQ6rhUg== 6U5UHVaTJcIIjWPmX/w2isHs+J1WT+8f+F5oM2ki8YDRYVbtdNE1ZtGZj5zAgSQoM2BaTAS8TOpxKQvgBvSAdJRfXYtDl0yC+wZO6JLB8znCrx5gi0rg4uqBbXDABUpv" + "ivalid String"
        let encryptedData = VirtualCardResult(info: encryptedInfoString, blocked: false, blockEnabled: true)
        XCTAssertNil(sut.decryptInfo(with: encryptedData))
    }
    
    func testDecrypt_WhenPassNil_ShouldResultNil() {
        let encryptedInfoString:String? = nil
        let encryptedData = VirtualCardResult(info: encryptedInfoString, blocked: false, blockEnabled: true)
        XCTAssertNil(sut.decryptInfo(with: encryptedData))
    }
    
    func testDecrypt_WhenPassValidDataEncrypted_ShouldResultValidObjectStringScenarioTwo() {
        let sut = makeSut(token: "5a88eb2f1b5c4252ac8866bea11654cf", consumerId: 8599172)
        let encryptedInfoString = "WcK91AbTu5AE23xk5Rtv3A== /_s_/ JIie85vuiHMKf17vJxU5ggu0hBHbifHlnj7MWP3SvrTfEnSx4m9AwA/OeioJWcT6MseFxzSAQUIFQP83RKS+kDpNmJ9nDw9vfhQiNf4+6TRHxX45AEYJpzzXrHZWSe4Mp7ojpnEGWp9UowCZCuHUVA=="
        let encryptedData = VirtualCardResult(info: encryptedInfoString, blocked: false, blockEnabled: true)
        let expectedResultString =  #"{"name":"RENAN MACHADO","cvv":"828","expiration_date":"2028-10-08","card_number":"2227630000101110"}"#
        XCTAssertEqual(sut.decryptInfo(with: encryptedData), expectedResultString)
    }
}

