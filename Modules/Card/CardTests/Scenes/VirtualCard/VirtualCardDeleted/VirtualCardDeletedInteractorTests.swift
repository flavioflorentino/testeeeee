import AnalyticsModule
import Core
import FeatureFlag
import UI
import XCTest
@testable import Card

private final class VirtualCardDeletedServicingSpy: VirtualCardDeletedServicing {
    private(set) var deleteCount = 0
    private(set) var passwordSpy: String?
    
    var expectedResult: (Result<NoContent, ApiError>) = .failure(ApiError.connectionFailure)
    
    func delete(password: String?, completion: @escaping VirtualCardDeleteCompletion) {
        passwordSpy = password
        deleteCount += 1
        completion(expectedResult)
    }
}

private final class VirtualCardDeletedPresentingSpy: VirtualCardDeletedPresenting {
    var viewController: VirtualCardDeletedDisplaying?
    
    private(set) var nextStepAction: [VirtualCardDeletedAction] = []
    private(set) var loadingStateCount = 0
    private(set) var errorStateCount = 0
    private(set) var errorApiSpy: ApiError?
    private(set) var idealStateCount = 0

    func didNextStep(action: VirtualCardDeletedAction) {
        nextStepAction.append(action)
    }
    
    func presentLoadingState() {
        loadingStateCount += 1
    }
    
    func presentErrorState(error: ApiError) {
        errorStateCount += 1
        errorApiSpy = error
    }
    
    func presentIdealState() {
        idealStateCount += 1
    }
}

private final class AuthManagerSpy: AuthContract {
    var isSuccess = true
    var isBiometric = true
    
    func authenticate(sucessHandler: @escaping (String, Bool) -> Void, cancelHandler: (() -> Void)?) {
        guard isSuccess else {
            cancelHandler?()
            return
        }
        sucessHandler("1234", isBiometric)
    }
}

final class VirtualCardDeletedInteractorTests: XCTestCase {
    
    typealias Dependencies = HasAnalytics & HasFeatureManager & HasAuthManagerDependency
    private let analytics = AnalyticsSpy()
    private let featureManagerMock = FeatureManagerMock()
    private let authManagerMock = AuthManagerSpy()
    private let faqURLMock = "https://meajuda.picpay.com/hc/pt-br/categories/360003297112-PicPay-Card"
    private let serviceSpy = VirtualCardDeletedServicingSpy()
    private let presenterSpy = VirtualCardDeletedPresentingSpy()
    private lazy var container: Dependencies = DependencyContainerMock(analytics, featureManagerMock, authManagerMock)
    
    private let inputMock = VirtualCardFlowInput(button: VirtualCardButton(action: .onboarding, newFeature: true))
    
    private lazy var sut = VirtualCardDeletedInteractor(service: serviceSpy, presenter: presenterSpy, dependencies: container, input: inputMock)
    
    func testViewDidLoad_WhenAuthFailed_ShouldTrackingAuth() {
        authManagerMock.isSuccess = false
        let expectedEvent = VirtualCardDeletedEvent.auth(action: .fingerprintCancel).event()
        
        sut.viewDidLoad()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
        XCTAssertEqual(presenterSpy.nextStepAction, [.dismiss])
    }
    
    func testViewDidLoad_WhenAuthSuccessWithBiometric_ShouldTrackingAuthAndCallService() {
        authManagerMock.isSuccess = true
        authManagerMock.isBiometric = true
        let expectedEvent = VirtualCardDeletedEvent.auth(action: .fingerprint).event()
        
        serviceSpy.expectedResult = .success(.init())
        sut.viewDidLoad()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
        XCTAssertEqual(serviceSpy.deleteCount, 1)
    }
    
    func testViewDidLoad_WhenAuthSuccessWithNumeral_ShouldTrackingAuthAndCallService() {
        authManagerMock.isSuccess = true
        authManagerMock.isBiometric = false
        let expectedEvent = VirtualCardDeletedEvent.auth(action: .numeralPassword).event()
        
        serviceSpy.expectedResult = .success(.init())
        sut.viewDidLoad()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
        XCTAssertEqual(serviceSpy.deleteCount, 1)
    }
    
    func testRequest_ShouldPresentNextStepAndTrackEvent() {
        let expectedEvent = VirtualCardDeletedEvent.deleted(action: .request).event()
        sut.request()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
        XCTAssertEqual(presenterSpy.nextStepAction, [.request])
    }
    
    func testDismiss_ShouldPresentNextStepAndTrackEvent() {
        let expectedEvent = VirtualCardDeletedEvent.deleted(action: .close).event()
        sut.dismiss()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
        XCTAssertEqual(presenterSpy.nextStepAction, [.dismiss])
    }
    
    func testTryAgain_WhenConnectionError_CallsAnalyticsCorrectly() {
        sut.tryAgain()
        let expectedEvent = VirtualCardDeletedEvent.errorAction(name: .connectionFailure, action: .tryAgain).event()
        XCTAssert(analytics.equals(to: expectedEvent))
    }
    
    func testTryAgain_WhenServerError_CallsAnalyticsCorrectly() {
        serviceSpy.expectedResult = .failure(.serverError)
        sut.tryAgain()
        let expectedEvent = VirtualCardDeletedEvent.errorAction(name: .serviceError, action: .tryAgain).event()
        XCTAssert(analytics.equals(to: expectedEvent))
    }
    
    func testDismiss_WhenServerError_CallsAnalyticsCorrectly() {
        serviceSpy.expectedResult = .failure(.serverError)
        sut.viewDidLoad()
        let expectedEvent = VirtualCardDeletedEvent.errorAction(name: .serviceError, action: .cancel).event()
        sut.dismiss()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testDismiss_WhenConnectionError_CallsAnalyticsCorrectly() {
        serviceSpy.expectedResult = .failure(.connectionFailure)
        sut.viewDidLoad()
        let expectedEvent = VirtualCardDeletedEvent.errorAction(name: .connectionFailure, action: .cancel).event()
        sut.dismiss()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
}
