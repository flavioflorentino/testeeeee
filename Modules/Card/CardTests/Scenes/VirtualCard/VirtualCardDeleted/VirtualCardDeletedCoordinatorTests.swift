import Foundation
import UIKit
import XCTest
@testable import Card

final class VirtualCardDeletedCoordinatoSpy: VirtualCardDeletedCoordinatorDelegate {
    private(set) var actionSpy: [VirtualCardDeletedAction] = []
    
    func deletedDidNextStep(action: VirtualCardDeletedAction) {
        actionSpy.append(action)
    }
}

final class VirtualCardDeletedCoordinatorTests: XCTestCase {
    private let viewController = UIViewController()
    private let container = DependencyContainerMock()
    private let delegateSpy = VirtualCardDeletedCoordinatoSpy()
    
    private lazy var sut: VirtualCardDeletedCoordinator = {
        let coordinator = VirtualCardDeletedCoordinator(dependencies: container)
        coordinator.viewController = viewController
        coordinator.delegate = delegateSpy
        return coordinator
    }()
    
    func testPerform_WhenDismiss_ShouldDelegate(){
        sut.perform(action: .dismiss)
        XCTAssertEqual(delegateSpy.actionSpy, [.dismiss])
    }
    
    func testPerform_WhenSuccessed_ShouldDelegate(){
        sut.perform(action: .succeeded)
        XCTAssertEqual(delegateSpy.actionSpy, [.succeeded])
    }
    
    func testPerform_WhenRequest_ShouldDelegate(){
        sut.perform(action: .request)
        XCTAssertEqual(delegateSpy.actionSpy, [.request])
    }
}
