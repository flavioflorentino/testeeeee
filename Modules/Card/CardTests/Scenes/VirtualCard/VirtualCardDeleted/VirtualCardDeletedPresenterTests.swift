import Core
import UI
import XCTest
@testable import Card

private final class VirtualCardDeletedDisplaySpy: VirtualCardDeletedDisplaying {
    private(set) var loadingStateCount = 0
    private(set) var errorStateCount = 0
    private(set) var idealStateCount = 0
    private(set) var errorState: StatefulFeedbackViewModel?
    
    func displayLoadingState() {
        loadingStateCount += 1
    }
    
    func displayErrorState(error: StatefulFeedbackViewModel) {
        errorStateCount += 1
        errorState = error
    }
    
    func displayIdealState() {
        idealStateCount += 1
    }
}

private final class VirtualCardDeletedCoordinatorSpy: VirtualCardDeletedCoordinating {
    var viewController: UIViewController?
    var delegate: VirtualCardDeletedCoordinatorDelegate?
    
    private(set) var actionSpy: [VirtualCardDeletedAction] = []
    
    func perform(action: VirtualCardDeletedAction) {
        actionSpy.append(action)
    }
}

final class VirtualCardDeletedPresenterTests: XCTestCase {
    private let container = DependencyContainerMock()
    private lazy var coordinatorSpy = VirtualCardDeletedCoordinatorSpy()
    private lazy var viewControllerSpy = VirtualCardDeletedDisplaySpy()
    private lazy var sut: VirtualCardDeletedPresenter = {
        let presenter = VirtualCardDeletedPresenter(coordinator: coordinatorSpy, dependencies: container)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testDidNextStep_WhenActionDismiss_ShouldPerformCoordinator() {
        sut.didNextStep(action: .dismiss)
        XCTAssertEqual(coordinatorSpy.actionSpy, [.dismiss])
    }
    
    func testDidNextStep_WhenActionSuccessed_ShouldPerformCoordinator() {
        sut.didNextStep(action: .succeeded)
        XCTAssertEqual(coordinatorSpy.actionSpy, [.succeeded])
    }
    
    func testDidNextStep_WhenActionDelete_ShouldPerformCoordinator() {
        sut.didNextStep(action: .request)
        XCTAssertEqual(coordinatorSpy.actionSpy, [.request])
    }
    
    func testPresentLoadingState_WhenInteractorCall_ShouldUpdateViewController() {
        sut.presentLoadingState()
        XCTAssertEqual(viewControllerSpy.loadingStateCount, 1)
    }
    
    func testPresentErrorState_WhenConnectionFailure_ShouldDisplayConnectionError() {
        let errorMock = ApiError.connectionFailure
        sut.presentErrorState(error: errorMock)
        XCTAssertEqual(viewControllerSpy.errorStateCount, 1)
        XCTAssertNotNil(viewControllerSpy.errorState)
        XCTAssertEqual(viewControllerSpy.errorState?.content?.title, Strings.Default.Connection.title)
    }
    
    func testPresentErrorState_WhenServiceGotError_ShouldDisplayConnectionError() {
        let errorMock = ApiError.serverError
        sut.presentErrorState(error: errorMock)
        XCTAssertEqual(viewControllerSpy.errorStateCount, 1)
        XCTAssertNotNil(viewControllerSpy.errorState)
        XCTAssertEqual(viewControllerSpy.errorState?.content?.title, Strings.VirtualCard.Deleted.Error.title)
    }
    
    func testPresentIdealState_WhenCardIsRight_ShouldDisplayIdealState() {
        sut.presentIdealState()
        XCTAssertEqual(viewControllerSpy.idealStateCount, 1)
    }
}
