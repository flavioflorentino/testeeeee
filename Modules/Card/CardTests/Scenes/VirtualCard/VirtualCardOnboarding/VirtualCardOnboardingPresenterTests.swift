import Foundation
import XCTest
@testable import Card


final class VirtualCardOnboardingCoordinatorSpy: VirtualCardOnboardingCoordinating {
    var viewController: UIViewController?
    var delegate: VirtualCardOnboardingCoordinatorDelegate?
    
    private(set) var action: [VirtualCardOnboardingAction] = []
    
    func perform(action: VirtualCardOnboardingAction) {
        self.action.append(action)
    }
}

final class VirtualCardOnboardingPresenterTests: XCTestCase {
    lazy var coordinatorSpy = VirtualCardOnboardingCoordinatorSpy()
    lazy var sut: VirtualCardOnboardingPresenter = VirtualCardOnboardingPresenter(coordinator: coordinatorSpy)
    
    func testNextStep_WhenDismiss_ShouldReciveClose() {
        sut.didNextStep(action: .dismiss)
        XCTAssertEqual(coordinatorSpy.action, [.dismiss])

    }
    
    func testNextStep_WhenSendGenerate_ShouldReciveGenerate() {
        sut.didNextStep(action: .confirm)
        XCTAssertEqual(coordinatorSpy.action, [.confirm])
    }
}
