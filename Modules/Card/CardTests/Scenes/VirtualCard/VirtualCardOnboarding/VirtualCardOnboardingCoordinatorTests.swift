import Foundation
import UIKit
import XCTest
@testable import Card

final class VirtualCardOnboardingCoordinatoSpy: VirtualCardOnboardingCoordinatorDelegate {
    private(set) var actionSpy: [VirtualCardOnboardingAction] = []
    
    func onboardingDidNextStep(action: VirtualCardOnboardingAction) {
        actionSpy.append(action)
    }
}

final class VirtualCardOnboardingCoordinatorTests: XCTestCase {
    private let viewController = UIViewController()
    private let container = DependencyContainerMock()
    private let delegateSpy = VirtualCardOnboardingCoordinatoSpy()
    
    private lazy var sut: VirtualCardOnboardingCoordinator = {
        let coordinator = VirtualCardOnboardingCoordinator(dependencies: container)
        coordinator.viewController = viewController
        coordinator.delegate = delegateSpy
        return coordinator
    }()
    
    func testPerform_WhenActionIsClose_ShouldFlowEnd(){
        sut.perform(action: .dismiss)
        XCTAssertEqual(delegateSpy.actionSpy, [.dismiss])
    }
    
    func testPerform_WhenActionIsGenerateCard_ShouldSendToGenerateCard() {
        sut.perform(action: .confirm)
        XCTAssertEqual(delegateSpy.actionSpy, [.confirm])
    }
}

