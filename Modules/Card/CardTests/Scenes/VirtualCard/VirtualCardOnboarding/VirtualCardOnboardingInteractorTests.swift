import Foundation
import XCTest
import AnalyticsModule
@testable import Card

final class VirtualCardOnboardingPresentingSpy: VirtualCardOnboardingPresenting {
    private(set) var action: [VirtualCardOnboardingAction] = []
    
    func didNextStep(action: VirtualCardOnboardingAction) {
        self.action.append(action)
    }
}

final class VirtualCardOnboardingInteractorTests: XCTestCase {
    private lazy var presenterSpy = VirtualCardOnboardingPresentingSpy()
    private lazy var analytics = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(analytics)
    private lazy var sut = VirtualCardOnboardingInteractor(presenter: presenterSpy, dependencies: dependencies)
    
    func testCloseAction_WhenByFeedbackPage_ShouldCallPresenter() {
        let expectedEvent = VirtualCardOnboardingEvent.onboard(action: .dismiss).event()
        sut.dismiss(byFeedbackPage: true)
        
        XCTAssertEqual(presenterSpy.action, [.dismiss])
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testCloseAction_WhenNotByFeedbackPage_ShouldCallPresenter() {
        let expectedEvent = VirtualCardOnboardingEvent.onboard(action: .dismiss).event()
        sut.dismiss(byFeedbackPage: false)
        
        XCTAssertEqual(presenterSpy.action, [])
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testGenerateVirtualCardAction_WhenTapped_ShouldCallPresenter() {
        let expectedEvent = VirtualCardOnboardingEvent.onboard(action: .confirm).event()
        sut.confirm()
        
        XCTAssertEqual(presenterSpy.action, [.confirm])
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
}
