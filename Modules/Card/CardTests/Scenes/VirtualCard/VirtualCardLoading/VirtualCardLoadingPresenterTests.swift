import Core
import UI
import XCTest
@testable import Card

private final class VirtualCardLoadingDisplaySpy: VirtualCardLoadingDisplaying {
    
    private(set) var loadingStateCount = 0
    private(set) var errorStateCount = 0
    private(set) var errorState: StatefulFeedbackViewModel?
    
    func startLoading() {
        loadingStateCount += 1
    }
    func displayErrorState(with model: StatefulFeedbackViewModel) {
        errorState = model
        errorStateCount += 1
    }
}

private final class VirtualCardLoadingCoordinatorSpy: VirtualCardLoadingCoordinating {
    var viewController: UIViewController?
    var delegate: VirtualCardLoadingCoordinatorDelegate?
    
    private(set) var actionSpy: [VirtualCardLoadingAction] = []
    
    func perform(action: VirtualCardLoadingAction) {
        actionSpy.append(action)
    }
}

final class VirtualCardLoadingPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = VirtualCardLoadingCoordinatorSpy()
    private lazy var viewControllerSpy = VirtualCardLoadingDisplaySpy()
    private lazy var sut: VirtualCardLoadingPresenter = {
        let presenter = VirtualCardLoadingPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()

    func testDidNextStep_WhenActionClose_ShouldPerformCoordinator() {
        sut.didNextStep(action: .close)
        XCTAssertEqual(coordinatorSpy.actionSpy, [.close])
    }

    func testDidNextStep_WhenActionSuccessed_ShouldPerformCoordinator() {
        sut.didNextStep(action: .success(action: .cardInfo))
        XCTAssertEqual(coordinatorSpy.actionSpy, [.success(action: .cardInfo)])
    }


    func testPresentLoadingState_WhenInteractorCall_ShouldUpdateViewController() {
        sut.presentLoadState()
        XCTAssertEqual(viewControllerSpy.loadingStateCount, 1)
    }

    func testPresentErrorState_WhenConnectionFailure_ShouldDisplayConnectionError() {
        let errorMock = ApiError.connectionFailure
        sut.showError(errorMock)
        XCTAssertEqual(viewControllerSpy.errorStateCount, 1)
        XCTAssertNotNil(viewControllerSpy.errorState)
    }
}
