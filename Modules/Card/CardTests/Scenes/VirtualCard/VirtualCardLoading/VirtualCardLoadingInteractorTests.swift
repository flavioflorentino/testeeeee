import AnalyticsModule
import Core
import FeatureFlag
import UI
import XCTest
@testable import Card

private final class VirtualCardLoadingServicingSpy: VirtualCardLoadingServicing {
    func getVirtualCardButton(completion: @escaping CompletionVirtualCardAction) {
        completion(expectedResult)
    }
    
    var expectedResult: Result<VirtualCardActionResponse, ApiError> = .failure(ApiError.connectionFailure)
}

private final class VirtualCardLoadingPresentingSpy: VirtualCardLoadingPresenting {
    func presentLoadState() {
        loadingStateCount += 1
    }
    
    func showError(_ error: ApiError) {
        errorStateCount += 1
        errorApiSpy = error
    }
    
    var viewController: VirtualCardLoadingDisplaying?
    
    private(set) var nextStepAction: [VirtualCardLoadingAction] = []
    private(set) var loadingStateCount = 0
    private(set) var errorStateCount = 0
    private(set) var errorApiSpy: ApiError?

    func didNextStep(action: VirtualCardLoadingAction) {
        nextStepAction.append(action)
    }
}

final class VirtualCardLoadingInteractorTests: XCTestCase {
    private let serviceSpy = VirtualCardLoadingServicingSpy()
    private let presenterSpy = VirtualCardLoadingPresentingSpy()
    private lazy var container = DependencyContainerMock()
    
    private lazy var sut = VirtualCardLoadingInteractor(service: serviceSpy, presenter: presenterSpy, dependencies: container)
        
    func testLoad_WhenSuccess_CallsPresenterCorrectly() {
        serviceSpy.expectedResult = .success(.init(action: .cardInfo))
        sut.loadData()
        XCTAssertEqual(presenterSpy.nextStepAction, [.success(action: .cardInfo)])
    }
    
    func testLoad_WhenFail_CallsPresenterCorrectly() {
        serviceSpy.expectedResult = .failure(.serverError)
        sut.loadData()
        XCTAssertEqual(presenterSpy.errorStateCount, 1)
        XCTAssertEqual(presenterSpy.errorApiSpy, .serverError)
    }
    
    func testDismis_WhenCalled_CallsPresenterCorrectly() {
        sut.dismiss()
        XCTAssertEqual(presenterSpy.nextStepAction, [.close])
    }
}
