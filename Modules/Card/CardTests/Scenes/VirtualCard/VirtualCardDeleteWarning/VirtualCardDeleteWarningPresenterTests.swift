import Foundation
import XCTest
@testable import Card


final class VirtualCardDeleteWarningPresenterCoordinatorSpy: VirtualCardDeleteWarningCoordinating {
    var delegate: VirtualCardDeleteWarningCoordinatorDelegate?
    private(set) var actions: [VirtualCardDeleteWarningAction] = []

    func perform(action: VirtualCardDeleteWarningAction) {
        actions.append(action)
    }
}

final class VirtualCardDeleteWarningPresenterTests: XCTestCase {
    lazy var coordinatorSpy = VirtualCardDeleteWarningPresenterCoordinatorSpy()
    lazy var sut: VirtualCardDeleteWarningPresenter = VirtualCardDeleteWarningPresenter(coordinator: coordinatorSpy)

    func testNextStep_WhenSendClose_ShouldReceiveClose() {
        sut.didNextStep(action: .dismiss)
        XCTAssertEqual(coordinatorSpy.actions, [.dismiss])
    }
    
    func testNextStep_WhenSendDelete_ShouldReciveConfirm() {
        sut.didNextStep(action: .confirm)
        XCTAssertEqual(coordinatorSpy.actions, [.confirm])
    }
}
