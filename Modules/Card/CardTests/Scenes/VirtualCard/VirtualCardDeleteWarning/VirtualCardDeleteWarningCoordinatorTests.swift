import Foundation
import UIKit
import XCTest
@testable import Card

final class VirtualCardDeleteWarningCoordinatorSpy: VirtualCardDeleteWarningCoordinatorDelegate {
    private(set) var actionSpy: [VirtualCardDeleteWarningAction] = []
    
    func deleteWarningDidNextStep(action: VirtualCardDeleteWarningAction) {
        actionSpy.append(action)
    }
}

final class VirtualCardDeleteWarningCoordinatorTests: XCTestCase {
    lazy var delegateSpy = VirtualCardDeleteWarningCoordinatorSpy()
    
    private lazy var sut: VirtualCardDeleteWarningCoordinator = {
        let coordinator = VirtualCardDeleteWarningCoordinator()
        coordinator.delegate = delegateSpy
        return coordinator
    }()
    
    func testPerform_WhenActionIsDismiss_ShouldDelegate(){
        sut.perform(action: .dismiss)
        XCTAssertEqual(delegateSpy.actionSpy, [.dismiss])
    }
    
    func testPerform_WhenActionIsConfirm_ShouldSendToDeletedVirtalCard() {
        sut.perform(action: .confirm)
        XCTAssertEqual(delegateSpy.actionSpy, [.confirm])
    }
}
