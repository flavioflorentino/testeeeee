import Foundation
import XCTest
import AnalyticsModule
@testable import Card

final class VirtualCardDeleteWarningPresentingSpy: VirtualCardDeleteWarningPresenting {
    private(set) var actions: [VirtualCardDeleteWarningAction] = []

    func didNextStep(action: VirtualCardDeleteWarningAction) {
        actions.append(action)
    }
}

final class VirtualCardDeleteWarningInteractorTests: XCTestCase {
    private lazy var presenterSpy = VirtualCardDeleteWarningPresentingSpy()
    private lazy var analytics = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(analytics)
    private lazy var sut = VirtualCardDeleteWarningInteractor(presenter: presenterSpy, dependencies: dependencies)

    func testCloseAction_WhenTapped_ShouldCallPresenter() {
        let expectedEvent = VirtualCardDeleteWarningEvent.deleted(action: .dismiss).event()
        sut.dismiss()

        XCTAssertEqual(presenterSpy.actions, [.dismiss])
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }

    func testConfirmVirtualCardAction_WhenTapped_ShouldCallPresenter() {
        let expectedEvent = VirtualCardDeleteWarningEvent.deleted(action: .confirm).event()
        sut.confirm()
        
        XCTAssertEqual(presenterSpy.actions, [.confirm])
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
}
