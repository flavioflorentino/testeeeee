import XCTest
import Core
import AnalyticsModule
import FeatureFlag
@testable import Card

final class ValidateCardAnalyticsSpy: ValidateCardAnalyticsEvents {
    var logCalledCount = 0
    var didCheckNumbers = 0
    var didCheckNumbersWithError = 0
    
    func numbersChecked() {
        logCalledCount += 1
        didCheckNumbers += 1
    }
    
    func numbersCheckedError() {
        logCalledCount += 1
        didCheckNumbersWithError += 1
    }
}

final class ValidateCardPresenterSpy: ValidateCardPresenting {
    var title: String
    var callSetCount = 0
    var didSetErrorDescription = 0
    var didCallShowFeedbackErrors: [URL?] = []
    var action: ValidateCardAction?
    var errorDescription: String?
    
    init(with type: ValidateCardFactoryType) {
        title = type == .activateCard ? Strings.UnlockCard.title : Strings.ForgotPasswordCard.title
    }
    
    func present() {
        callSetCount += 1
    }
    
    func show(errorDescription: String) {
        didSetErrorDescription += 1
        self.errorDescription = errorDescription
    }
    
    func showFeedbackError(url: URL?) {
        didCallShowFeedbackErrors.append(url)
    }
    
    func perform(action: ValidateCardAction) {
        self.action = action
    }
}

final class ValidateCardServiceSpy: ValidateCardServicing {
    func validateCard(_ lastDigits: String, then completion: @escaping (Result<Void, ApiError>) -> Void) {
        switch lastDigits {
        case "wrongDigits":
            completion(.failure(.malformedRequest("wrongDigits")))
        default:
            completion(.success(()))
        }
    }
}

final class ValidateCardInteractorTests: XCTestCase {
    typealias Dependencies = HasFeatureManager & HasAnalytics
    let serviceSpy = ValidateCardServiceSpy()
    let presenterSpy = ValidateCardPresenterSpy(with: .activateCard)
    let analyticsSpy = ValidateCardAnalyticsSpy()
    private lazy var  featureManagerMock = FeatureManagerMock()

    private lazy var dependencies: Dependencies = DependencyContainerMock(analyticsSpy, featureManagerMock)

    lazy var sut = ValidateCardInteractor(service: serviceSpy, presenter: presenterSpy, analytics: analyticsSpy, dependencies: dependencies)
    
    func testDidClose_WhenUserTapped_ShouldClose() {
        sut.close()
        XCTAssertEqual(presenterSpy.action, .close)
    }
    
    func testDidConfirm_WhenUserTyped_WrongDigits() {
        sut.didTapConfirm(with: "wrongDigits")
        XCTAssertEqual(presenterSpy.didSetErrorDescription, 1)
        XCTAssertEqual(presenterSpy.errorDescription, "Não foi possível ativar o seu cartão, por favor, tente novamente")
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(analyticsSpy.didCheckNumbersWithError, 1)
    }
    
    func testDidConfirm_WhenUserTyped_RightDigits() {
        sut.didTapConfirm(with: "rightDigits")
        XCTAssertEqual(presenterSpy.action, .confirm)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(analyticsSpy.didCheckNumbers, 1)
    }
    
    func testDidSetTitle_WhenViewDidLoad() {
        sut.viewDidLoad()
        XCTAssertEqual(presenterSpy.callSetCount, 1)
    }
    
    func testDidConfirm_WhenUserTyped_WrongDigitsOnceOrTwiceFeedbackDoesntShow() {
        sut.didTapConfirm(with: "wrongDigits")
        XCTAssertEqual(presenterSpy.didCallShowFeedbackErrors.count, 0)
        sut.didTapConfirm(with: "wrongDigits")
        XCTAssertEqual(presenterSpy.didCallShowFeedbackErrors.count, 0)
    }
    
    func testDidConfirm_WhenUserTyped_WrongDigitsThreeTimesFeedbackShows() {
        let faqArticle = "articleActivateCardValueToTest"
        featureManagerMock.override(key: .articleActivateCard, with: faqArticle)
        sut.didTapConfirm(with: "wrongDigits")
        sut.didTapConfirm(with: "wrongDigits")
        sut.didTapConfirm(with: "wrongDigits")
        XCTAssertEqual(presenterSpy.didCallShowFeedbackErrors.count, 1)
    }
}
