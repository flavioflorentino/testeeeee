import XCTest
@testable import Card

final class ValidateCardCoordinatorSpy: ValidateCardCoordinating {
    var delegate: ValidateCardCoordinatorDelegate?
    
    var action: ValidateCardAction?
    var callActionCount = 0
    
    func perform(action: ValidateCardAction) {
        self.action = action
        callActionCount += 1
    }
}

final class ValidateCardControllerSpy: ValidateCardDisplaying {
    var didShowErrorDescription = ""
    var title = ""
    
    func show(errorDescription: String) {
        didShowErrorDescription = errorDescription
    }
    
    func setTitle(_ title: String) {
        self.title = title
    }
}

final class ValidateCardPresenterTests: XCTestCase {
    let coordinatorSpy = ValidateCardCoordinatorSpy()
    let controllerSpy = ValidateCardControllerSpy()
    
    func testDidClose_WhenUserTapped_ShouldClose() {
        let sut = ValidateCardPresenter(with: .activateCard, coordinator: coordinatorSpy)
        sut.perform(action: .close)
        XCTAssertEqual(coordinatorSpy.action, .close)
        XCTAssertEqual(coordinatorSpy.callActionCount, 1)
    }
    
    func testDidConfirm_WhenUserTapped_ShouldConfirm() {
        let sut = ValidateCardPresenter(with: .activateCard, coordinator: coordinatorSpy)
        sut.perform(action: .confirm)
        XCTAssertEqual(coordinatorSpy.action, .confirm)
        XCTAssertEqual(coordinatorSpy.callActionCount, 1)
    }
    
    func testDidSetTitle_WhenViewDidLoad_WhenActivateFlow() {
        let sut = ValidateCardPresenter(with: .activateCard, coordinator: coordinatorSpy)
        sut.viewController = controllerSpy
        sut.present()
        XCTAssertEqual(controllerSpy.title, Strings.UnlockCard.title)
    }
    
    func testDidSetTitle_WhenViewDidLoad_WhenForgotPasswordFlow() {
        let sut = ValidateCardPresenter(with: .forgotPasswordCard, coordinator: coordinatorSpy)
        sut.viewController = controllerSpy
        sut.present()
        XCTAssertEqual(controllerSpy.title, Strings.ForgotPasswordCard.title)
    }
    
    func testDidShowError_WhenWrongDigits_ShouldShowError() {
        let sut = ValidateCardPresenter(with: .forgotPasswordCard, coordinator: coordinatorSpy)
        sut.viewController = controllerSpy
        sut.show(errorDescription: "test")
        XCTAssertEqual(controllerSpy.didShowErrorDescription, "test")
    }
}
