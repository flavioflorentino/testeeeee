import Core
import XCTest
@testable import Card

private final class CardReplacementRequestedServicingSpy: CardReplacementRequestedServicing {
    public var result: Result<CardTracking, ApiError>?
    
    func tracking(completion: @escaping CompletionCardTracking) {
        guard let result = result else {
            return
        }
        completion(result)
    }
}

private final class CardReplacementRequestedPresentingSpy: CardReplacementRequestedPresenting {
    var viewController: CardReplacementRequestedDisplay?
    
    private(set) var requestedDateSpy = ""
    private(set) var didCallPresentSuccess = 0
    private(set) var didCallPresentError = 0
    private(set) var didCallPresentFAQ = 0
    private(set) var actionSpy: CardReplacementRequestedAction?
    
    func presentSuccess(requestedDate: String) {
        requestedDateSpy = requestedDate
        didCallPresentSuccess += 1
    }
    
    func presentError() {
        didCallPresentError += 1
    }
    
    func didNextStep(action: CardReplacementRequestedAction) {
        actionSpy = action
    }
    
    func presentFAQ() {
        didCallPresentFAQ += 1
    }
    
}

final class CardReplacementRequestedViewModelTests: XCTestCase {
    private var serviceSpy = CardReplacementRequestedServicingSpy()
    private var presenterSpy = CardReplacementRequestedPresentingSpy()
    private let container = DependencyContainerMock()
    private let trackingMockSuccess = CardTracking(requestAt: "26/05/2020")
    private let trackingMockInconst = CardTracking(requestAt: nil)
    
    private lazy var sut = CardReplacementRequestedViewModel(service: serviceSpy, presenter: presenterSpy, dependencies: container)
    
    func testLoadData_WhenServiceGetSuccess_ShouldSendToPresenter() {
        serviceSpy.result = Result.success(trackingMockSuccess)
        sut.loadData()
        XCTAssertEqual(presenterSpy.requestedDateSpy, "26/05/2020")
        XCTAssertEqual(presenterSpy.didCallPresentSuccess, 1)
    }
    
    func testLoadData_WhenServiceGetInconsistentData_ShouldSendToPresenter() {
        serviceSpy.result = Result.success(trackingMockInconst)
        sut.loadData()
        XCTAssertEqual(presenterSpy.didCallPresentError, 1)
    }
    
    func testLoadData_WhenServiceGetError_ShouldSendToPresenter() {
        serviceSpy.result = Result.failure(ApiError.bodyNotFound)
        sut.loadData()
        XCTAssertEqual(presenterSpy.didCallPresentError, 1)
    }
}
