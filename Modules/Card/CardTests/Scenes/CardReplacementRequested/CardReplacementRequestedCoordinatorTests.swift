import Core
import UIKit
import XCTest
@testable import Card

final class CardReplacementRequestedCoordinatorSpy: CardReplacementRequestedCoordinatorDelegate {
    private(set) var didOpenFAQCount = 0
    
    func didOpenFAQ() {
        didOpenFAQCount += 1
    }
}

final class CardReplacementRequestedCoordinatorTests: XCTestCase {
    private let viewController = UIViewController()
    private let container = DependencyContainerMock()
    private let delegateSpy = CardReplacementRequestedCoordinatorSpy()
    
    private lazy var sut: CardReplacementRequestedCoordinator = {
        let coordinator = CardReplacementRequestedCoordinator(dependencies: container)
        coordinator.viewController = viewController
        coordinator.delegate = delegateSpy
        return coordinator
    }()
    
    func testPerform_WhenActionIsOpenFAQ_ShouldViewControllerOpenFAQ() {
        sut.perform(action: .openFAQ)
        XCTAssertEqual(delegateSpy.didOpenFAQCount, 1)
    }
}
