import UI
import XCTest
@testable import Card

private class CardReplacementRequestedCoordinatingSpy: CardReplacementRequestedCoordinating {
    var viewController: UIViewController?
    var delegate: CardReplacementRequestedCoordinatorDelegate?
    
    private(set) var didPerformFAQ = 0
    
    func perform(action: CardReplacementRequestedAction) {
        didPerformFAQ += 1
    }
}

private class CardReplacementRequestedDisplaySpy: CardReplacementRequestedDisplay {
    private(set) var subtitleChecker = ""
    private(set) var faqChecker = ""
    private(set) var didCallDisplayError = 0
    private(set) var didCallDisplayFAQ = 0
    
    func displaySuccess(subtitle: NSAttributedString, faq: NSAttributedString) {
        subtitleChecker = subtitle.string
        faqChecker = faq.string
    }
    
    func displayError(error: StatefulErrorViewModel) {
        didCallDisplayError += 1
    }
    
    func displayFAQ(url: URL) {
        didCallDisplayFAQ += 1
    }
}

final class CardReplacementRequestedPresenterTests: XCTestCase {
    private var viewControllerSpy = CardReplacementRequestedDisplaySpy()
    private let coordinatorSpy = CardReplacementRequestedCoordinatingSpy()
    private let container = DependencyContainerMock()
    private let requestDateMock = "2020-05-26"
    
    private lazy var sut: CardReplacementRequestedPresenter = {
        let presenter = CardReplacementRequestedPresenter(coordinator: coordinatorSpy, dependencies: container)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testPresentSuccess_WhenReceiveRequestedDate_ShouldUpdateViewController() throws {
        sut.presentSuccess(requestedDate: requestDateMock)
        let messageDate = try XCTUnwrap(sut.formatDate(requestDateMock))
        XCTAssertEqual(viewControllerSpy.subtitleChecker, Strings.CardReplacementRequested.Subtitle.message(messageDate))
        XCTAssertEqual(viewControllerSpy.faqChecker, Strings.CardReplacementRequested.Faq.message)
    }
    
    func testPresentSuccess_WhenReceiveEmptyRequestedDate_ShoulDisplayError() {
        sut.presentSuccess(requestedDate: "")
        XCTAssertEqual(viewControllerSpy.didCallDisplayError, 1)
    }
    
    func testPresentError_WhenReceiveError_ShouldUpdateViewController() {
        sut.presentError()
        XCTAssertEqual(viewControllerSpy.didCallDisplayError, 1)
    }
    
    func testFormatDate_WhenInsertEmptyString_ShouldReturnNil() {
        XCTAssertNil(sut.formatDate(""))
    }
    
    func testFormatDate_WhenInsertAnyString_ShouldReturnNil() {
        XCTAssertNil(sut.formatDate("abc123"))
    }
    
    func testFormatDate_WhenInsertDateStringWithWrongFormat_ShouldReturnNil() {
        XCTAssertNil(sut.formatDate("26-MAI-2020"))
    }
    
    func testFormatDate_WhenInsertDateString_ShouldReturnFormattedString() {
        XCTAssertNotNil(sut.formatDate("2020-05-26"))
    }
}
