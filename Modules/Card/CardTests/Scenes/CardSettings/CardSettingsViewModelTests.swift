import XCTest
@testable import Card

private class CardSettingsServiceProtocolSpy: CardSettingsServiceProtocol {
    func creditStatus(completion: @escaping CompletionCardSettingsCreditAccount) { }
}

final class CardSettingsViewModelTests: XCTestCase {
    private lazy var sut: CardSettingsViewModel = {
        let service = CardSettingsServiceProtocolSpy()
        let coordinator = CardSettingsViewModel(service: service)
        return coordinator
    }()
    // isCardEnabled = false
    func testIsCardEnabled_WhenCalledWithBlocked_ShouldReturnFalse() {
        let cardEnabled = sut.isCardEnabled(physicalCardStatusType: .blocked)
        XCTAssertEqual(cardEnabled, false)
    }
    func testIsCardEnabled_WhenCalledWithWaitingActivation_ShouldReturnFalse() {
        let cardEnabled = sut.isCardEnabled(physicalCardStatusType: .waitingActivation)
        XCTAssertEqual(cardEnabled, false)
    }
    // isCardEnabled = true
    func testIsCardEnabled_WhenCalledWithActivated_ShouldReturnTrue() {
        let cardEnabled = sut.isCardEnabled(physicalCardStatusType: .activated)
        XCTAssertEqual(cardEnabled, true)
    }
    func testIsCardEnabled_WhenCalledWithDebitWaitingActivation_ShouldReturnTrue() {
        let cardEnabled = sut.isCardEnabled(physicalCardStatusType: .debitWaitingActivation)
        XCTAssertEqual(cardEnabled, true)
    }
    func testIsCardEnabled_WhenCalledWithNotExists_ShouldReturnTrue() {
        let cardEnabled = sut.isCardEnabled(physicalCardStatusType: .notExists)
        XCTAssertEqual(cardEnabled, true)
    }
    func testIsCardEnabled_WhenCalledWithNotRequested_ShouldReturnTrue() {
        let cardEnabled = sut.isCardEnabled(physicalCardStatusType: .notRequested)
        XCTAssertEqual(cardEnabled, true)
    }
}
