import Card

enum DollarExchangeRateInfoMock {
    static func generate() -> DollarExchangeRateInfo {
        guard let trackingData = try? MockCodable<DollarExchangeRateInfo>().loadCodableObject(
            resource: "dollarExchangeRateInfo",
            typeDecoder: .useDefaultKeys,
            decoderDateFormat: Date.fifthFormatter.dateFormat) else {
                fatalError("Failed to decode JSON")
        }
        return trackingData
    }
}
