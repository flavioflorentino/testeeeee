import XCTest
@testable import Card

// MARK: - Tests
final class DollarExchangeRateFactoryTests: XCTestCase {
    func testMakeShouldReturnViewController() {
        let viewController = DollarExchangeRateFactory.make()
        XCTAssertNotNil(viewController)
    }
}
