import XCTest
import Core
import AnalyticsModule
@testable import Card

// MARK: - Spies
private final class DollarExchangeRatePresenterSpy: DollarExchangeRatePresenting {
    // MARK: - Properties
    fileprivate(set) var viewController: DollarExchangeRateDisplaying?
    private(set) var didLoadExchangeRateCalledCount = 0
    private(set) var didCallOpenFAQCount = 0
    private(set) var didCallOpenHistory = 0
    private(set) var receivedErrorType: DollarExchangeRateErrorType?
    
    // MARK: - Functions
    func didNextStep(action: DollarExchangeRateAction) {
        switch action {
        case .openFAQ:
            didCallOpenFAQCount += 1
        case .openHistory:
            didCallOpenHistory += 1
        }
    }
    
    func didLoadExchangeRate(info: DollarExchangeRateInfo) {
        didLoadExchangeRateCalledCount += 1
    }
    
    func didReceiveError(with type: DollarExchangeRateErrorType) {
        receivedErrorType = type
    }
}

private class DollarExchangeRateServiceSpy: DollarExchangeRateServicing {
    // MARK: - Properties
    var isSuccess = false
    var errorType = ApiError.connectionFailure
    
    // MARK: - Functions
    func fetchDollarExchangeRateInfo(completion: @escaping DollarExchangeRateInfoCompletion) {
        let data = getDollarExchangeRateInfo()
        guard !isSuccess else {
            completion(.success(data))
            return
        }
        completion(.failure(errorType))
    }
    
    private func getDollarExchangeRateInfo() -> DollarExchangeRateInfo {
        return DollarExchangeRateInfoMock.generate()
    }
}

// MARK: - Tests
final class DollarExchangeRateInteractorTests: XCTestCase {
    
    // MARK: - Properties
    fileprivate var sut: DollarExchangeRateInteractor!
    fileprivate var serviceSpy: DollarExchangeRateServiceSpy!
    fileprivate var presenterSpy: DollarExchangeRatePresenterSpy!
    private let analyticsMock = AnalyticsSpy()
    
    // MARK: - Override
    override func setUp() {
        super.setUp()
        presenterSpy = DollarExchangeRatePresenterSpy()
        serviceSpy = DollarExchangeRateServiceSpy()
        sut = DollarExchangeRateInteractor(service: serviceSpy,
                                           presenter: presenterSpy,
                                           dependencies: DependencyContainerMock(analyticsMock))
        
    }
    
    // MARK: - Test Functions
    func testLoad_ShouldCallPresenterWithCorrectValueOnSuccessfulRequest() {
        serviceSpy.isSuccess = true
        sut.loadData()
        XCTAssertEqual(presenterSpy.didLoadExchangeRateCalledCount, 1)
    }
    
    func testLoad_ShouldCallPresenterWithCorrectErrorWhenNoConnection() {
        serviceSpy.isSuccess = false
        serviceSpy.errorType = .connectionFailure
        sut.loadData()
        XCTAssertEqual(presenterSpy.didLoadExchangeRateCalledCount, 0)
        XCTAssertNotNil(presenterSpy.receivedErrorType)
        XCTAssertEqual(presenterSpy.receivedErrorType, DollarExchangeRateErrorType.noConnection)
    }
    
    func testLoad_ShouldCallPresenterWithCorrectErrorWhenTimeout() {
        serviceSpy.isSuccess = false
        serviceSpy.errorType = .timeout
        sut.loadData()
        XCTAssertEqual(presenterSpy.didLoadExchangeRateCalledCount, 0)
        XCTAssertNotNil(presenterSpy.receivedErrorType)
        XCTAssertEqual(presenterSpy.receivedErrorType, DollarExchangeRateErrorType.noConnection)
    }
    
    func testLoad_ShouldCallPresenterWithCorrectErrorWhenServerFails() {
        serviceSpy.isSuccess = false
        serviceSpy.errorType = .serverError
        sut.loadData()
        XCTAssertEqual(presenterSpy.didLoadExchangeRateCalledCount, 0)
        XCTAssertNotNil(presenterSpy.receivedErrorType)
        XCTAssertEqual(presenterSpy.receivedErrorType, DollarExchangeRateErrorType.notAvailable)
    }
    
    func testOpenFAQ_ShouldCallPresenterWithAction() {
        sut.didTapFAQ()
        XCTAssertEqual(presenterSpy.didCallOpenFAQCount, 1)
    }
    
    func testOpenHistory_ShouldCallPresenterWithAction() {
        sut.didTapSeeAllHistory()
        XCTAssertEqual(presenterSpy.didCallOpenHistory, 1)
    }
}
