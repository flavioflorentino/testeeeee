import XCTest
@testable import Card

// MARK: - Spies
private final class DollarExcangeRateViewControllerSpy: DollarExchangeRateDisplaying {
        
    // MARK: - Properties
    private(set) var displayExchangeRateCount = 0
    private(set) var displayExchangeRateErrorCount = 0
    private(set) var currentExchangeRate: DollarExchangeCellPresenting?
    private(set) var latestExchangeRates: [DollarExchangeCellPresenting]?
    
    // MARK: - Functions
    func displayExchangeRate(current: DollarExchangeCellPresenting, latest: [DollarExchangeCellPresenting]) {
        displayExchangeRateCount += 1
        currentExchangeRate = current
        latestExchangeRates = latest
    }
    
    func displayExchangeRateError(with type: DollarExchangeRateErrorType) {
        displayExchangeRateErrorCount += 1
    }
}

// MARK: - Tests
final class DollarExchangeRatePresenterTests: XCTestCase {
    
    // MARK: - Properties
    private var viewControllerSpy = DollarExcangeRateViewControllerSpy()
    private var coordinatorSpy = DollarExchangeRateCoordinator(dependencies: DependencyContainerMock())
    private let container = DependencyContainerMock()
    
    private lazy var sut: DollarExchangeRatePresenter = {
        let sut = DollarExchangeRatePresenter(coordinator: coordinatorSpy, dependencies: container)
        sut.viewController = viewControllerSpy
        return sut
    }()
    
    // MARK: - Test Functions
    func testGetExchangeRate_WhenReceiveSuccessFromService_ShouldCallViewControllerWithItems() {
        let data = DollarExchangeRateInfoMock.generate()
        sut.didLoadExchangeRate(info: data)
        XCTAssertEqual(viewControllerSpy.displayExchangeRateCount, 1)
        XCTAssertNotNil(viewControllerSpy.currentExchangeRate)
        XCTAssertNotNil(viewControllerSpy.latestExchangeRates)
    }
    
    func testGetExchangeRate_WhenReceiveFailureFromService_ShouldCallViewControllerWithError() {
        sut.didReceiveError(with: .noConnection)
        XCTAssertEqual(viewControllerSpy.displayExchangeRateErrorCount, 1)
    }
    
    func testGetExchangeRate_WhenReceiveSuccessFromService_FormatsCurrentRateCorrectly() {
        let data = DollarExchangeRateInfoMock.generate()
        let presentableData = sut.generatePresentableData(from: data)
        sut.didLoadExchangeRate(info: data)
        
        XCTAssertEqual(viewControllerSpy.currentExchangeRate?.exchangeDate, presentableData.0.exchangeDate)
        XCTAssertEqual(viewControllerSpy.currentExchangeRate?.exchangeValue, presentableData.0.exchangeValue)
    }
    
    func testGetExchangeRate_WhenReceiveSuccessFromService_FormatsLatestRatesCorrectly() {
        let data = DollarExchangeRateInfoMock.generate()
        let presentableData = sut.generatePresentableData(from: data)
        sut.didLoadExchangeRate(info: data)
        guard
            let latestSample = viewControllerSpy.latestExchangeRates?.first,
            let presentableSample = presentableData.1.first
        else {
            XCTFail("Failed to decode latest rate values")
            return
        }
        XCTAssertEqual(latestSample.exchangeDate, presentableSample.exchangeDate)
        XCTAssertEqual(latestSample.exchangeValue, presentableSample.exchangeValue)
    }
}
