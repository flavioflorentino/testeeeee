import XCTest
@testable import Card

// MARK: - Spies
private final class DollarExchangeHistoryViewControllerSpy: DollarExchangeHistoryDisplay {
    // MARK: - Init
    init(filterType: DollarExchangeHistoryFilterType) {
        self.filterType = filterType
    }
    
    // MARK: - Properties
    var filterType: DollarExchangeHistoryFilterType
    private(set) var displayHistoryCount = 0
    private(set) var displayErrorCount = 0
    private(set) var history: [DollarExchangeCellPresenting]?
    
    // MARK: - Functions
    func renderView(from state: DollarExchangeHistoryViewState) {
        switch state {
        case .displaying(let history):
            self.history = history
            displayHistoryCount += 1
        case .error:
            displayErrorCount += 1
        default:
            break
        }
    }
}

// MARK: - Tests
final class DollarExchangeHistoryPresenterTests: XCTestCase {
    
    // MARK: - Properties
    private let container = DependencyContainerMock()
    private lazy var coordinatorSpy: DollarExchangeHistoryCoordinator = {
        DollarExchangeHistoryCoordinator(dependencies: container)
    }()
    
    private lazy var sut: DollarExchangeHistoryPresenter = {
        let sut = DollarExchangeHistoryPresenter(coordinator: coordinatorSpy, filterType: .lastFifteenDays)
        sut.viewController = viewControllerSpy
        return sut
    }()
    
    private lazy var viewControllerSpy: DollarExchangeHistoryViewControllerSpy = {
        let filterTypes = DollarExchangeHistoryFilterType.allCases
        let vc = DollarExchangeHistoryViewControllerSpy(filterType: .lastFifteenDays)
        return vc
    }()
    
    // MARK: - Test Functions
    func testGetHistory_WhenReceiveSuccessFromService_ShouldCallViewControllerWithItems() {
        let data: [DollarExchangeRate] = [.init(value: 1.9999, date: Date())]
        sut.didLoad(history: .init(history: data))
        XCTAssertEqual(viewControllerSpy.displayHistoryCount, 1)
        XCTAssertNotNil(viewControllerSpy.history)
    }

    func testGetHistory_WhenReceiveFailureFromService_ShouldCallViewControllerWithError() {
        sut.didReceiveError(with: .noConnection)
        XCTAssertEqual(viewControllerSpy.displayErrorCount, 1)
    }

    func testGetHistory_WhenReceiveSuccessFromService_ShouldFormatHistoryCorrectly() {
        let data: [DollarExchangeRate] = [.init(value: 1.9999, date: Date())]
        let presentableData = sut.generatePresentableData(from: data)
        sut.didLoad(history: .init(history: data))
        XCTAssertEqual(viewControllerSpy.history?.first?.exchangeDate, presentableData.first?.exchangeDate)
        XCTAssertEqual(viewControllerSpy.history?.first?.exchangeValue, presentableData.first?.exchangeValue)
    }
}
