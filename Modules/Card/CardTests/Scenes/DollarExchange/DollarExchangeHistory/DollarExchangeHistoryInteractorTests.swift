import XCTest
import Core
import AnalyticsModule
@testable import Card

// MARK: - Service Spy
private final class DollarExchangeHistoryServiceMock: DollarExchangeHistoryServicing {
    // MARK: - Properties
    var expectedResult: Result<DollarExchangeHistory, ApiError> = .failure(ApiError.connectionFailure)
    
    // MARK: - Functions
    func fetchDollarExchangeHistory(for lastDays: Int, completion: @escaping DollarExchangeHistoryCompletion) {
        completion(expectedResult)
    }
}

// MARK: - Presenter Spy
private final class DollarExchangeHistoryPresenterSpy: DollarExchangeHistoryPresenting {
    var viewController: DollarExchangeHistoryDisplay?
    var filterType: DollarExchangeHistoryFilterType

    init(filterType: DollarExchangeHistoryFilterType) {
        self.filterType = filterType
    }
    
    private(set) var didLoadHistoryCalledCount = 0
    private(set) var didCallOpenFAQCount = 0
    private(set) var didReceiveErrorCalledCount = 0
    private(set) var receivedErrorType: DollarExchangeRateErrorType?
    private(set) var receivedFilterType: DollarExchangeHistoryFilterType?
    
    func didNextStep(action: DollarExchangeHistoryAction) {
        switch action {
        case .openFAQ:
            didCallOpenFAQCount += 1
        }
    }
    
    func didLoad(history data: DollarExchangeHistory) {
        didLoadHistoryCalledCount += 1
    }
    
    func didReceiveError(with type: DollarExchangeRateErrorType) {
        receivedErrorType = type
    }
}

// MARK: - Tests
final class DollarExchangeHistoryInteractorTests: XCTestCase {
    
    // MARK: - Properties
    private let analyticsMock = AnalyticsSpy()
    private let serviceMock = DollarExchangeHistoryServiceMock()
    private let presenterSpy = DollarExchangeHistoryPresenterSpy(filterType: .lastFifteenDays)
    private lazy var sut: DollarExchangeHistoryInteractor = {
        return DollarExchangeHistoryInteractor(service: serviceMock, presenter: presenterSpy, dependencies: DependencyContainerMock(analyticsMock))
    }()
    
    // MARK: - Test Functions
    func testloadDollarExchangeHistory_WhenSuccessfulRequest_ShouldCallPresenterWithCorrectValue() {
        let data: [DollarExchangeRate] = [.init(value: 1.9999, date: Date())]
        serviceMock.expectedResult = .success(.init(history: data))
        sut.loadDollarExchangeHistory(isTryingAgain: false)
        XCTAssertEqual(presenterSpy.didLoadHistoryCalledCount, 1)
    }
    
    func testLoadDollarExchangeHistory_WhenNoConnection_ShouldCallPresenterWithCorrectError() {
        serviceMock.expectedResult = .failure(.connectionFailure)
        sut.loadDollarExchangeHistory(isTryingAgain: false)
        XCTAssertEqual(presenterSpy.didLoadHistoryCalledCount, 0)
        XCTAssertNotNil(presenterSpy.receivedErrorType)
        XCTAssertEqual(presenterSpy.receivedErrorType, DollarExchangeRateErrorType.noConnection)
    }
    
    func testLoadDollarExchangeHistory_WhenTimeout_ShouldCallPresenterWithCorrectError() {
        serviceMock.expectedResult = .failure(.timeout)
        sut.loadDollarExchangeHistory(isTryingAgain: false)
        XCTAssertEqual(presenterSpy.didLoadHistoryCalledCount, 0)
        XCTAssertNotNil(presenterSpy.receivedErrorType)
        XCTAssertEqual(presenterSpy.receivedErrorType, DollarExchangeRateErrorType.noConnection)
    }
    
    func testLoadDollarExchangeHistory_WhenServerFails_ShouldCallPresenterWithCorrectError() {
        serviceMock.expectedResult = .failure(.serverError)
        sut.loadDollarExchangeHistory(isTryingAgain: false)
        XCTAssertEqual(presenterSpy.didLoadHistoryCalledCount, 0)
        XCTAssertNotNil(presenterSpy.receivedErrorType)
        XCTAssertEqual(presenterSpy.receivedErrorType, DollarExchangeRateErrorType.notAvailable)
    }
    
    func testLoadDollarExchangeHistory_WhenRequestSucceeds_ShouldCallPresenterWithCorrectFilterType() {
        let data: [DollarExchangeRate] = [.init(value: 1.9999, date: Date())]
        serviceMock.expectedResult = .success(.init(history: data))
        sut.loadDollarExchangeHistory(isTryingAgain: false)
        XCTAssertEqual(presenterSpy.didLoadHistoryCalledCount, 1)
    }
    
    func testLoadDollarExchangeHistory_WhenRequestFails_ShouldCallPresenterWithCorrectFilterType() {
        serviceMock.expectedResult = .failure(.serverError)
        sut.loadDollarExchangeHistory(isTryingAgain: false)
        XCTAssertEqual(presenterSpy.didLoadHistoryCalledCount, 0)
    }
    
    func testOpenFAQ_WhenTapped_ShouldCallOpenFAQOnPresenter() {
        sut.openFAQ()
        XCTAssertEqual(presenterSpy.didCallOpenFAQCount, 1)
    }
}
