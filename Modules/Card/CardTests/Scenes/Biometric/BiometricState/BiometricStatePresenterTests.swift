import XCTest
@testable import Card

private final class BiometricStateCoordinatorSpy: BiometricStateCoordinating {
    // MARK: - Variables
    var viewController: UIViewController?
    
    private(set) var actionSelected: BiometricStateAction?
    private(set) var callAction = 0
    
    // MARK: - Public Methods
    func perform(action: BiometricStateAction) {
        actionSelected = action
        callAction += 1
    }
}

final class BiometricStatePresenterTests: XCTestCase {
    // MARK: - Variables
    private let coordinatorSpy = BiometricStateCoordinatorSpy()
    private let container = DependencyContainerMock()
        
    private lazy var sut = BiometricStatePresenter(coordinator: coordinatorSpy)

    // MARK: - Tests
    func testNextStep_WhenReceiveMessage_ShouldDismissScreen() {
        sut.didNextStep(action: .close)
        XCTAssertEqual(coordinatorSpy.actionSelected, .close)
        XCTAssertEqual(coordinatorSpy.callAction, 1)
    }
}
