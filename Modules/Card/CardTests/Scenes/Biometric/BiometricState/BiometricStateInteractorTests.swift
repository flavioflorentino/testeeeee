import XCTest
@testable import Card

private final class BiometricStatePresentingSpy: BiometricStatePresenting {
    // MARK: - Variables
    private(set) var action: BiometricStateAction?
    private(set) var callAction = 0
    
    // MARK: - Public Methods
    func didNextStep(action: BiometricStateAction) {
        self.action = action
        callAction += 1
    }
}

final class BiometricStateInteractorTests: XCTestCase {
    // MARK: - Variables
    private var presenterSpy = BiometricStatePresentingSpy()
    private lazy var sut = BiometricStateInteractor(presenter: presenterSpy)
    
    // MARK: - Public Methods
    func testClose_WhenPressCloseButton_ShouldCloseScreen() {
        sut.close()
        XCTAssertEqual(presenterSpy.action, .close)
        XCTAssertEqual(presenterSpy.callAction, 1)
    }
}
