import XCTest
import Core
import AnalyticsModule
@testable import Card

private final class AuthManagerMock: AuthContract {
    var isSuccess = true
    func authenticate(sucessHandler: @escaping (String, Bool) -> Void, cancelHandler: (() -> Void)?) {
        guard isSuccess else {
            cancelHandler?()
            return
        }
        sucessHandler("1234", true)
    }
}

final class RejectedCreditServiceSpy: RejectedCreditServicing {
    var getCardFormExpectedResult: Result<CardForm, ApiError> = .failure(.connectionFailure)
    var finishExpectedResult: Result<NoContent, ApiError> = .failure(.connectionFailure)
    
    func getCardForm(_ completion: @escaping (Result<CardForm, ApiError>) -> Void) {
        completion(getCardFormExpectedResult)
    }
    
    func finish(pin: String, _ completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        completion(finishExpectedResult)
    }
}

final class RejectedCreditPresenterSpy: RejectedCreditPresenting {
    var viewController: RejectedCreditDisplaying?
    
    var didPresentNextStepCount = 0
    var nextStep: RejectedCreditAction?
    
    var didPresentErrorCount = 0
    var error: ApiError?
    
    var didPresentLoadingCount = 0
    var shouldLoad = false
    
    var didPresentDataCount = 0
    var data: HomeAddress?
    
    var didPresentLinkCount = 0
    var url: URL?
    
    func presentNextStep(action: RejectedCreditAction) {
        didPresentNextStepCount += 1
        nextStep = action
    }
    
    func presentError(_ error: ApiError) {
        didPresentErrorCount += 1
        self.error = error
    }
    
    func presentLoading(_ loading: Bool) {
        didPresentLoadingCount += 1
        shouldLoad = loading
    }
    
    func presentData(address: HomeAddress) {
        didPresentDataCount += 1
        data = address
    }
    
    func presentLink(url: URL) {
        didPresentLinkCount += 1
        self.url = url
    }
}

class RejectedCreditInteractorTests: XCTestCase {
    
    fileprivate let authSpy = AuthManagerMock()
    let analyticsSpy = AnalyticsSpy()
    let serviceSpy = RejectedCreditServiceSpy()
    let presenterSpy = RejectedCreditPresenterSpy()
    
    lazy var sut = RejectedCreditInteractor(service: serviceSpy, presenter: presenterSpy, dependencies: DependencyContainerMock(analyticsSpy, authSpy))
    
    func testAskCard_WhenAuthenticatedAndSuccess_ShouldNextStep() {
        authSpy.isSuccess = true
        serviceSpy.finishExpectedResult = .success(.init())
        sut.askCard()
        XCTAssertEqual(presenterSpy.didPresentNextStepCount, 1)
        XCTAssertEqual(presenterSpy.nextStep, .askCard)
    }
    
    func testAskCard_WhenAuthenticatedAndFailure_ShouldntNextStep() {
        authSpy.isSuccess = true
        serviceSpy.finishExpectedResult = .failure(.connectionFailure)
        sut.askCard()
        XCTAssertEqual(presenterSpy.didPresentErrorCount, 1)
        XCTAssertEqual(analyticsSpy.logCalledCount, 2)
    }
    
    func testAskCard_WhenNotAuthenticated_ShouldLogAnalytics() {
        authSpy.isSuccess = false
        serviceSpy.finishExpectedResult = .success(.init())
        sut.askCard()
        XCTAssertEqual(analyticsSpy.logCalledCount, 2)
    }
    
    func testChangeAddress_ShouldNextStep() {
        sut.changeAddress()
        XCTAssertEqual(presenterSpy.didPresentNextStepCount, 1)
        XCTAssertEqual(presenterSpy.nextStep, .changeAddress(cardForm: .init()))
    }
    
    func testClose_ShouldNextStep() {
        sut.close()
        XCTAssertEqual(presenterSpy.didPresentNextStepCount, 1)
        XCTAssertEqual(presenterSpy.nextStep, .close)
    }
    
    func testLoadData_WhenSuccess_ShouldPresentData() {
        let address = HomeAddress()
        var cardForm = CardForm()
        cardForm.homeAddress = address

        serviceSpy.getCardFormExpectedResult = .success(cardForm)
        sut.loadData()
        XCTAssertEqual(presenterSpy.didPresentDataCount, 1)
        XCTAssertEqual(presenterSpy.data, address)
    }
}
