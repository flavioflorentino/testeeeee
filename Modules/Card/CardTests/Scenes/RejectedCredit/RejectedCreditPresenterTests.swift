import XCTest
import UI
import Core
import FeatureFlag
@testable import Card

final class RejectedCreditCoordinatorSpy: RejectedCreditCoordinating {
    var viewController: UIViewController?
    
    var delegate: RejectedCreditCoordinatingDelegate?
    
    var didPerformActionCount = 0
    var action: RejectedCreditAction?
    
    func perform(action: RejectedCreditAction) {
        didPerformActionCount += 1
        self.action = action
    }
}

final class RejectedCreditViewControllerSpy: RejectedCreditDisplaying {
    
    var didDisplayErrorCount = 0
    var errorModel: StatefulErrorViewModel?
    
    var didDisplayLoadingCount = 0
    var didDisplayLoading = false
    
    var didDisplayDataCount = 0
    var addressStr: String?
    var contractAttrStr: NSAttributedString?
    
    func displayLoading(_ loading: Bool) {
        didDisplayLoadingCount += 1
        didDisplayLoading = loading
    }
    
    func displayError(model: StatefulErrorViewModel) {
        didDisplayErrorCount += 1
        errorModel = model
        
    }
    
    func displayData(addressString: String, contractAttrString: NSAttributedString) {
        didDisplayDataCount += 1
        addressStr = addressString
        contractAttrStr = contractAttrString
    }
}

class RejectedCreditPresenterTests: XCTestCase {
    let featureManagerMock = FeatureManagerMock()
    let coordinatorSpy = RejectedCreditCoordinatorSpy()
    let controllerSpy = RejectedCreditViewControllerSpy()
    
    let address = HomeAddress(
        zipCode: "60811110",
        addressComplement: nil,
        city: "Fortaleza",
        cityId: nil,
        neighborhood: "Eng.Luciano Cavalcante",
        state: "Ceara",
        stateId: nil,
        streetName: "Rua Luiza Miranda Coelho",
        streetNumber: "1355",
        streetType: nil,
        street: "Rua Luiza Miranda Coelho"
    )
    
    lazy var sut: RejectedCreditPresenter = {
        let presenter = RejectedCreditPresenter(coordinator: coordinatorSpy, dependencies: DependencyContainerMock(featureManagerMock))
        presenter.viewController = controllerSpy
        return presenter
    }()
    
    override func setUp() {
        featureManagerMock.override(key: .termsDebitLink, with: "teste")
    }
    
    func testBuildAddressString_ShouldShowAddress() {
        let addressStr = "Rua Luiza Miranda Coelho 1355, Eng.Luciano Cavalcante - Ceara - Fortaleza, 60811-110"
        XCTAssertEqual(sut.buildAddressString(homeAddress: address), addressStr)
    }
    
    func testPresentData_ShouldShowData() {
        let addressStr = sut.buildAddressString(homeAddress: address)
        sut.presentData(address: address)
        XCTAssertEqual(controllerSpy.addressStr, addressStr)
        XCTAssertEqual(controllerSpy.didDisplayLoadingCount, 1)
        XCTAssertEqual(controllerSpy.didDisplayLoading, false)
    }
    
    func testPresentError_ShouldShowError() {
        let errorModel = StatefulErrorViewModel(.connectionFailure)
        sut.presentError(.connectionFailure)
        XCTAssertEqual(controllerSpy.didDisplayErrorCount, 1)
        XCTAssertEqual(controllerSpy.errorModel, errorModel)
    }
    
    func testPresentLoading_ShouldLoad() {
        sut.presentLoading(true)
        XCTAssertEqual(controllerSpy.didDisplayLoadingCount, 1)
        XCTAssertEqual(controllerSpy.didDisplayLoading, true)
    }
    
    func testPresentNextStep_ShouldNextStep() {
        sut.presentNextStep(action: .askCard)
        XCTAssertEqual(coordinatorSpy.didPerformActionCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .askCard)
    }
    
    
    func testGetWithoutSeparators_WhenPassingContentWithInitialAndFinalSeparators_ShouldReturnOnlyTheContent() {
        XCTAssertEqual(sut.getWithoutSeparators("- lado par"),"lado par")
        XCTAssertEqual(sut.getWithoutSeparators("- lado par -"),"lado par")
        XCTAssertEqual(sut.getWithoutSeparators(", lado par -"),"lado par")
        XCTAssertEqual(sut.getWithoutSeparators(". lado par -"),"lado par")
        XCTAssertEqual(sut.getWithoutSeparators("1. lado par -"),"1. lado par")
        XCTAssertEqual(sut.getWithoutSeparators("A. lado par -"),"A. lado par")
        XCTAssertEqual(sut.getWithoutSeparators("- lado/par -"),"lado/par")
        XCTAssertEqual(sut.getWithoutSeparators("- lado*par -"),"lado*par")
        XCTAssertEqual(sut.getWithoutSeparators(" lado*par "),"lado*par")
        XCTAssertEqual(sut.getWithoutSeparators(" la "),"la")
        XCTAssertEqual(sut.getWithoutSeparators("- lado, 1234, A par "),"lado, 1234, A par")
    }
    
    func testGetWithoutSeparators_WhenPassingNothingValid_ShouldReturnNil() {
        XCTAssertNil(sut.getWithoutSeparators(" - "))
        XCTAssertNil(sut.getWithoutSeparators(" "))
        XCTAssertNil(sut.getWithoutSeparators("-"))
        XCTAssertNil(sut.getWithoutSeparators(","))
    }
    
    func testGetWithoutSeparators_WhenPassingSingleCharRequirement_ShouldReturnTheContent() {
        XCTAssertEqual(sut.getWithoutSeparators("1", requireMinimumTwoCharacters: false),"1")
        XCTAssertEqual(sut.getWithoutSeparators("- 1 -", requireMinimumTwoCharacters: false),"1")
        XCTAssertEqual(sut.getWithoutSeparators("- a -", requireMinimumTwoCharacters: false),"a")
        XCTAssertEqual(sut.getWithoutSeparators("- a1 -", requireMinimumTwoCharacters: false),"a1")
        XCTAssertEqual(sut.getWithoutSeparators("1234", requireMinimumTwoCharacters: false),"1234")
    }
    
    func testGetFormattedStreet_WhenPassingStreetTypeAndName_ShouldReturnFormattedStreetTypeAndName() {
        XCTAssertEqual(sut.getFormattedStreet(streetType: "Rua", street: "Al Doutor"), "Rua Al Doutor")
    }
    
    func testGetFormattedStreetNumber_WhenPassingValue_ShouldReturnFormattedStreetNumber() {
        XCTAssertEqual(sut.getFormattedStreetNumber("28"), " 28")
        XCTAssertEqual(sut.getFormattedStreetNumber("1"), " 1")
        XCTAssertEqual(sut.getFormattedStreetNumber("- 12"), " 12")
        XCTAssertEqual(sut.getFormattedStreetNumber("- 12 -"), " 12")
    }
    
    func testGetFormattedNeighborhood_WhenPassingValue_ShouldReturnFormattedNeighborhood() {
        XCTAssertEqual(sut.getFormattedNeighborhood("Vila Buarque"), ", Vila Buarque")
        XCTAssertEqual(sut.getFormattedNeighborhood("- Vila Buarque"), ", Vila Buarque")
        XCTAssertEqual(sut.getFormattedNeighborhood("-Vila Buarque"), ", Vila Buarque")
    }
    
    func testGetFormattedAddressComplement_WhenPassingValue_ShouldReturnFormattedAddressComplement() {
        XCTAssertEqual(sut.getFormattedAddressComplement("lado par"), " - lado par")
        XCTAssertEqual(sut.getFormattedAddressComplement("- lado par"), " - lado par")
    }
    
    func testGetFormattedState_WhenPassingValue_ShouldReturnFormattedState() {
        XCTAssertEqual(sut.getFormattedState("PE"), " - PE")
        XCTAssertEqual(sut.getFormattedState("-PE"), " - PE")
        XCTAssertEqual(sut.getFormattedState("- PE"), " - PE")
    }
    
    func testGetFormattedCity_WhenPassingValue_ShouldReturnFormattedCity() {
        XCTAssertEqual(sut.getFormattedCity("Recife"), " - Recife")
        XCTAssertEqual(sut.getFormattedCity("-Recife"), " - Recife")
        XCTAssertEqual(sut.getFormattedCity("-Recife- "), " - Recife")
    }
    
    func testGetFormattedZipCode_WhenPassingValue_ShouldReturnFormattedZipCode() {
        XCTAssertEqual(sut.getFormattedZipCode("88888888"), ", 88888-888")
        XCTAssertEqual(sut.getFormattedZipCode("- 88888888"), ", 88888-888")
        XCTAssertEqual(sut.getFormattedZipCode(" , 88888888"), ", 88888-888")
    }
}

extension StatefulErrorViewModel: Equatable {
    public static func == (lhs: StatefulErrorViewModel, rhs: StatefulErrorViewModel) -> Bool {
        return lhs.image == rhs.image
    }
}

extension RejectedCreditAction: Equatable {
    public static func == (lhs: RejectedCreditAction, rhs: RejectedCreditAction) -> Bool {
        switch (lhs, rhs) {
        case let (.changeAddress(cardForm), .changeAddress(cardForm2)):
            return cardForm == cardForm2
        case (.askCard, .askCard):
            return true
        case (.close, .close):
            return true
        case (.link, .link):
            return true
        default:
            return false
        }
    }
}

extension CardForm: Equatable {
    public static func == (lhs: CardForm, rhs: CardForm) -> Bool {
        return
            lhs.name == rhs.name &&
            lhs.birthDate == rhs.birthDate &&
            lhs.cpf == rhs.cpf
    }
}
