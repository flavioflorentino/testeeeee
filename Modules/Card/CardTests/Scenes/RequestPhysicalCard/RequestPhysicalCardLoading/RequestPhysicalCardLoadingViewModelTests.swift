import AnalyticsModule
import Core
import FeatureFlag
import XCTest
@testable import Card

private final class RequestPhysicalCardLoadingPresentingSpy: RequestPhysicalCardLoadingPresenting {
    var viewController: RequestPhysicalCardLoadingDisplay?
    
    private(set) var callUpdateDeliveryAddress = 0
    private(set) var callShowError = 0
    private(set) var callEndFlow = 0
    private(set) var callDidNextStep = 0
    
    func updateDeliveryAddress(deliveryAddress: HomeAddress) {
        callUpdateDeliveryAddress += 1
    }
    
    func showError() {
        callShowError += 1
    }
    
    func endFlow() {
        callEndFlow += 1
    }
    
    func didNextStep(action: RequestPhysicalCardLoadingAction) {
        callDidNextStep += 1
    }
}

private final class RequestPhysicalCardLoadingServicingSpy: RequestPhysicalCardLoadingServicing {
    func getDeliveryAddress(completion: @escaping CompletionAddress) {
        let address = HomeAddress()
        completion(Result.success(address))
    }
}

final class RequestPhysicalCardLoadingInteractorTests: XCTestCase {
    private let presenterSpy = RequestPhysicalCardLoadingPresentingSpy()
    
    private lazy var sut: RequestPhysicalCardLoadingInteractor = {
        let container = DependencyContainerMock()
        let service = RequestPhysicalCardLoadingServicingSpy()
        return RequestPhysicalCardLoadingInteractor(service: service, presenter: presenterSpy)
    }()
    
    func testGetDeliveryAddress_WhenViewDidLoad_ShouldUpdatePresenter() {
        sut.getDeliveryAddress()
        XCTAssertEqual(presenterSpy.callUpdateDeliveryAddress, 1)
    }
    
    func testEndFlow_WhenServerReturnError_ShouldEndFlow() {
        sut.endFlow()
        XCTAssertEqual(presenterSpy.callEndFlow, 1)
    }
}
