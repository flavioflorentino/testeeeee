import Foundation
import XCTest
@testable import Card

private final class AccountAddressTests: XCTestCase {
    // MARK: - Variables
    private var accountAdressList: [AccountAddress] {
        let accountAdressList = try! MockCodable<[AccountAddress]>().loadCodableObject(
            resource: "accountAddress",
            typeDecoder: .convertFromSnakeCase)
        return accountAdressList
    }
    
    func testAAA() {
        guard let accountAddress = accountAdressList.first else {
            XCTFail("Parser error finded")
            return
        }
        XCTAssertEqual(accountAddress.address, "Al Doutor, 9, Casa, Centro, Curitiba - PR")
        XCTAssertEqual(accountAddress.city, "Curitiba")
        XCTAssertEqual(accountAddress.complement, "Casa")
        XCTAssertEqual(accountAddress.neighborhood, "Centro")
        XCTAssertNil(accountAddress.nick)
        XCTAssertEqual(accountAddress.number, 9)
        XCTAssertEqual(accountAddress.state, "PR")
        XCTAssertEqual(accountAddress.street, "Al Doutor")
        XCTAssertEqual(accountAddress.type, .residential)
        XCTAssertEqual(accountAddress.zipCode, "13200-000")
    }
}
