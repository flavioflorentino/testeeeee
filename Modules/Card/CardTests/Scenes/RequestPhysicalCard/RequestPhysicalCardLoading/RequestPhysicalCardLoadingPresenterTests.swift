import XCTest
@testable import Card

private final class RequestPhysicalCardLoadingViewControllerSpy: RequestPhysicalCardLoadingDisplay {
    private(set) var showError = false
    
    func showError(with errorMessage: String) {
        self.showError = true
    }
}

private final class RequestPhysicalCardLoadingCoordinatorSpy: RequestPhysicalCardLoadingCoordinating {
    var viewController: UIViewController?
    private(set) var callEndFlow = 0
    private(set) var callUpdateFlowCoordinator = 0
    
    func perform(action: RequestPhysicalCardLoadingAction) {
        switch action {
        case .endFlow:
            callEndFlow += 1
        case .updateFlowCoordinator:
            callUpdateFlowCoordinator += 1
        }
    }
}

final class RequestPhysicalCardLoadingPresenterTests: XCTestCase {
    private var viewControllerSpy = RequestPhysicalCardLoadingViewControllerSpy()
    private let coordinatorSpy = RequestPhysicalCardLoadingCoordinatorSpy()
    
    private lazy var sut: RequestPhysicalCardLoadingPresenter = {
        let sut = RequestPhysicalCardLoadingPresenter(coordinator: coordinatorSpy)
        sut.viewController = viewControllerSpy
        return sut
    }()
    
    func testShowError_WhenReturnsErrorFromServer_ShouldShowError() {
        sut.showError()
        XCTAssertTrue(viewControllerSpy.showError)
    }
    
    func testUpdateDeliveryAddress_WhenServerReturnAddress_ShouldPerformCoordinator() {
        let deliveryAddress = HomeAddress()
        sut.updateDeliveryAddress(deliveryAddress: deliveryAddress)
        XCTAssertEqual(coordinatorSpy.callUpdateFlowCoordinator, 1)
    }
    
    func testViewDidLoad_WhenPressCloseButton_ShouldEndFlow() {
        sut.endFlow()
        XCTAssertEqual(coordinatorSpy.callEndFlow, 1)
    }
}
