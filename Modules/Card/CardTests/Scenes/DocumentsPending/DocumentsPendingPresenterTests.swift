import XCTest
import UI
@testable import Card

final class DocumentsPendingCoordinatorSpy: DocumentsPendingCoordinating {
    var delegate: DocumentsPendingCoodinatingDelegate?
    
    var viewController: UIViewController?
    
    var didPerformActionCount = 0
    var action: DocumentsPendingAction?
    
    func perform(action: DocumentsPendingAction) {
        didPerformActionCount += 1
        self.action = action
    }
}

final class DocumentsPendingViewControllerSpy: DocumentsPendingDisplaying {
    var didDisplayDataCount = 0
    var data = [DetailsViewRow]()
    
    var didDisplayErrorCount = 0
    var errorModel: StatefulErrorViewModel?
    
    var didDisplayLoadingCount = 0
    var shouldDisplayLoading = false
    
    func displayData(with rows: [DetailsViewRow]) {
        didDisplayDataCount += 1
        self.data = rows
    }
    
    func displayError(model: StatefulErrorViewModel) {
        didDisplayErrorCount += 1
        self.errorModel = model
    }
    
    func displayLoading(_ shouldDisplay: Bool) {
        didDisplayLoadingCount += 1
        shouldDisplayLoading = shouldDisplay
    }
}

final class DocumentsPendingPresenterTests: XCTestCase {
    private let coordinatorSpy = DocumentsPendingCoordinatorSpy()
    private let controllerSpy = DocumentsPendingViewControllerSpy()
    private lazy var sut: DocumentsPendingPresenter = {
        let presenter = DocumentsPendingPresenter(coordinator: coordinatorSpy)
        presenter.viewController = controllerSpy
        return presenter
    }()
    
    func testPresentData_ShouldDisplayData() {
        let errors = ["Selfie", "ID (Front)", "ID (Back)"]
        let expectedRows: [DetailsViewRow] = [
            .bulletTextRow(text: "Selfie"),
            .spacer(spacing: Spacing.base00),
            .bulletTextRow(text: "ID (Front)"),
            .spacer(spacing: Spacing.base00),
            .bulletTextRow(text: "ID (Back)")
        ]
        sut.presentData(errors: errors)
        XCTAssertEqual(controllerSpy.data, expectedRows)
        XCTAssertEqual(controllerSpy.didDisplayDataCount, 1)
    }
    
    func testPresentError_ShouldDisplaytError() {
        sut.presentError(.connectionFailure)
        XCTAssertEqual(controllerSpy.didDisplayErrorCount, 1)
        XCTAssertEqual(controllerSpy.errorModel, .init(.connectionFailure))
    }
    
    func testPresentLoading_WhenTrue_ShouldDisplayLoading() {
        sut.presentLoading(true)
        XCTAssertEqual(controllerSpy.didDisplayLoadingCount, 1)
        XCTAssertEqual(controllerSpy.shouldDisplayLoading, true)
    }
    
    func testPresentLoading_WhenFalse_ShouldntDisplayLoading() {
        sut.presentLoading(false)
        XCTAssertEqual(controllerSpy.didDisplayLoadingCount, 1)
        XCTAssertEqual(controllerSpy.shouldDisplayLoading, false)
    }
    
    func testPresentNextStep_ShouldPerformNextStep() {
        sut.presentNextStep(action: .cancel)
        XCTAssertEqual(coordinatorSpy.didPerformActionCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .cancel)
    }
}
