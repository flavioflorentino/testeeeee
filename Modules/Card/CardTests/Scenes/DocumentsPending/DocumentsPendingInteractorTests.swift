import XCTest
import AnalyticsModule
import Core
@testable import Card

final class DocumentsPendingServiceSpy: DocumentsPendingServicing {
    var expectedResult: Result<CardForm, ApiError> = .failure(.connectionFailure)
    
    func loadCardForm(_ completion: @escaping (Result<CardForm, ApiError>) -> Void) {
        completion(expectedResult)
    }
}

final class DocumentsPendingPresenterSpy: DocumentsPendingPresenting {
    var viewController: DocumentsPendingDisplaying?
    
    var didPresentNextStepCount = 0
    var action: DocumentsPendingAction?
    
    var didPresentDataCount = 0
    var presentedData = [String]()
    
    var didPresentErrorCount = 0
    var error: ApiError?
    
    var didPresentLoadingCount = 0
    var shouldDisplayLoading = false
    
    func presentNextStep(action: DocumentsPendingAction) {
        didPresentNextStepCount += 1
        self.action = action
    }
    
    func presentData(errors: [String]) {
        didPresentDataCount += 1
        presentedData = errors
    }
    
    func presentError(_ error: ApiError) {
        didPresentErrorCount += 1
        self.error = error
    }
    
    func presentLoading(_ shouldDisplay: Bool) {
        didPresentLoadingCount += 1
        shouldDisplayLoading = shouldDisplay
    }
}

final class DocumentsPendingInteractorTests: XCTestCase {
    private let presenterSpy = DocumentsPendingPresenterSpy()
    private let analyticsSpy = AnalyticsSpy()
    private let serviceSpy = DocumentsPendingServiceSpy()
    
    private lazy var sut = DocumentsPendingInteractor(
        service: serviceSpy,
        presenter: presenterSpy,
        dependencies: DependencyContainerMock(analyticsSpy),
        errors: [
            "Selfie",
            "ID (Front)",
            "ID (Back)"
        ]
    )
    
    func testLoadData_WhenRequestFails_ShouldPresentError() {
        sut.loadData()
        XCTAssertEqual(presenterSpy.didPresentErrorCount, 1)
    }
    
    func testLoadData_WhenRequestSucceeds_ShouldPresentData() {
        serviceSpy.expectedResult = .success(CardForm())
        sut.loadData()
        XCTAssertEqual(presenterSpy.didPresentDataCount, 1)
        XCTAssertEqual(presenterSpy.presentedData, ["Selfie", "ID (Front)", "ID (Back)"])
    }
    
    func testConfirm_ShouldPresentNextStep() {
        let cardForm = CardForm()
        serviceSpy.expectedResult = .success(cardForm)
        sut.loadData()
        sut.confirm()
        XCTAssertEqual(presenterSpy.didPresentNextStepCount, 1)
        XCTAssertEqual(presenterSpy.action, .confirm(form: cardForm))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }
    
    func testCancel_ShouldPresentNextStep() {
        sut.cancel()
        XCTAssertEqual(presenterSpy.didPresentNextStepCount, 1)
        XCTAssertEqual(presenterSpy.action, .cancel)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }
}
