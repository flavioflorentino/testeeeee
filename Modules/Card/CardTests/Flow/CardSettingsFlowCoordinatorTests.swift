import Core
import XCTest
import UI
@testable import Card

private final class CardSettingsFlowCoordinatorTests: XCTestCase {
    private lazy var navigationSpy = UINavigationControllerSpy()
    private lazy var sut = CardSettingsFlowCoordinator(navigationController: navigationSpy)

    private lazy var settingsData: CardSettingsData = {
        let buttons = CardSettingsButtons(
            cardReplacement: CardReplacementButton(type: .requested, action: .tracking),
            virtualCard: VirtualCardButton(action: .onboarding, newFeature: true)
        )
        return CardSettingsData(cardType: .multiple, buttons: buttons)
    }()

    func testStart_WhenSomeClassCall_ShouldSetAController() {
        sut.start()
        XCTAssertEqual(navigationSpy.pushCallCount, 1)
        XCTAssertNotNil(sut.cardSettingsViewController)
    }
}
