import XCTest
import UI
@testable import Card

private final class HiringFlowCoordinatorTests: XCTestCase {
    private lazy var navigationSpy = UINavigationControllerSpy()
    private lazy var sut = HiringFlowCoordinator(isUpgradeDebitToCredit: false, navigationController: navigationSpy)
    
    func testPerform_WhenDueDay_ShouldOpenScreen() throws {
        sut.perform(action: .dueDay(cardForm: CardForm()))
        let viewController = try XCTUnwrap(navigationSpy.pushedViewController)
        XCTAssertEqual(navigationSpy.pushCallCount, 1)
        XCTAssertTrue(viewController.isKind(of: DueDayViewController.self))
    }
    
    func testPerform_WhenConfirmAddress_ShouldOpenScreen() throws {
        sut.perform(action: .confirmAddress(cardForm: CardForm(), bestPurchaseDay: 5))
        let viewController = try XCTUnwrap(navigationSpy.pushedViewController)
        XCTAssertEqual(navigationSpy.pushCallCount, 1)
        XCTAssertTrue(viewController.isKind(of: DeliveryConfirmAddressViewController.self))
    }
    
    func testPerform_WhenResume_ShouldOpenScreen() throws {
        var cardForm = CardForm()
        cardForm.creditGrantedByBank = 3500
        cardForm.dueDay = 10
        sut.cardForm = cardForm
        sut.bestPurchaseDay = 10
        sut.perform(action: .resume)
        let viewController = try XCTUnwrap(navigationSpy.pushedViewController)
        XCTAssertEqual(navigationSpy.pushCallCount, 1)
        XCTAssertTrue(viewController.isKind(of: HiringResumeViewController.self))
    }
    
    func testPerform_WhenCreditActivated_ShouldOpenScreen() throws {
        sut.perform(action: .creditActivated)
        let viewController = try XCTUnwrap(navigationSpy.pushedViewController)
        XCTAssertEqual(navigationSpy.pushCallCount, 1)
        XCTAssertTrue(viewController.isKind(of: NewHiringSuccessViewController.self))
    }
    
    func testPerform_WhenCreditWaitingActivation_ShouldOpenScreen() throws {
        sut.perform(action: .creditWaitingActivation)
        let viewController = try XCTUnwrap(navigationSpy.pushedViewController)
        XCTAssertEqual(navigationSpy.pushCallCount, 1)
        XCTAssertTrue(viewController.isKind(of: HiringWaitingActivationViewController.self))
    }
}
