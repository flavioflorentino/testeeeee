import XCTest
import UI
@testable import Card

final class ChangeCardPasswordFlowCoordinatorTests: XCTestCase {
    
    private var presentModaly = false
    private let navigationSpy = UINavigationControllerSpy()
    private lazy var sut = ChangeCardPasswordFlowCoordinator(
        navigationController: navigationSpy,
        presentModally: presentModaly
    )
    
    func testStart_whenTriggered_ShouldInitializeFirstViewController() throws {
        sut.start()
        presentModaly = false
        
        let viewController = try XCTUnwrap(navigationSpy.pushedViewController)
        XCTAssert(viewController.isKind(of: ValidateCardPasswordViewController.self))
        XCTAssertEqual(navigationSpy.pushCallCount, 1)
    }
    
    func testStart_whenInstantiatedModally_ShouldPresentWithStyledNavigationController() throws {
        presentModaly = true
        
        sut.start()
        
        let viewController = try XCTUnwrap(navigationSpy.viewControllerToPresent)
        XCTAssert(viewController.isKind(of: StyledNavigationController.self))
        XCTAssertEqual(navigationSpy.presentCallCount, 1)
    }
    
    func testStart_whenInstantiatedInStack_ShouldPushTheFirstViewController() throws {
        presentModaly = false
        
        sut.start()
        
        XCTAssertFalse(sut.presentingNavigationController is StyledNavigationController)
    }
    
    func testStart_whenInstantiatedModally_ShouldPresentModallyFirstViewController() throws {
        presentModaly = true
        
        sut.start()
        
        XCTAssertTrue(sut.presentingNavigationController is StyledNavigationController)
    }
}
