import AnalyticsModule
import Core
import FeatureFlag
import UI
import XCTest
@testable import Card

final class LimitAdjustmentServiceSpy: LimitAdjustmentServicing {

    lazy var expectedResultOfGet: Result<CreditLimit, ApiError> = .failure(.bodyNotFound)
    lazy var expectedResultSetNewLimit: Result<NoContent, ApiError>  = .failure(.bodyNotFound)
    func getCreditLimit(completion: @escaping LimitAdjustmentDataCompletion) {
        completion(expectedResultOfGet)
    }
    
    func putNewCreditLimit(limit: Double, completion: @escaping ((Result<NoContent, ApiError>) -> Void)) {
        completion(expectedResultSetNewLimit)
    }
}

final class LimitAdjustmentPresenterSpy: LimitAdjustmentPresenting {
    var viewController: LimitAdjustmentDisplaying?
    private(set) var validCurrencyValue = 0.0
    private(set) var validCurrencyValueCount = 0
    private(set) var receiveInfo: CreditLimit?
    private(set) var setupWithInfoLimitCount = 0
    private(set) var loadStateCount = 0
    private(set) var availableLimit = 0.0
    private(set) var errorStateCount = 0
    private(set) var newLimitSetCount = 0

    private(set) var receiveApiError: ApiError?
    private(set) var newLimitValueReceive = 0.0
    private(set) var actions: [LimitAdjustmentAction] = []
    private(set) var cardLimitLoadedReceive: Bool?
    
    func didNextStep(action: LimitAdjustmentAction) {
        actions.append(action)
    }
    
    func presentIdealState(info: CreditLimit) {
        receiveInfo = info
        setupWithInfoLimitCount += 1
    }
    
    
    func presentErrorState(error: ApiError, cardLimitLoaded: Bool) {
        receiveApiError = error
        cardLimitLoadedReceive = cardLimitLoaded
        errorStateCount += 1
    }
    
    func presentLoadState() {
        loadStateCount += 1
    }
    
    func presentAvailableLimit(value: Double) {
        availableLimit = value
    }
    
    func presentSuccessState(currentValue: Double) {
        newLimitSetCount += 1
        newLimitValueReceive = currentValue
    }
    
    func presentUpdatedCrediLimit(value: Double) {
        validCurrencyValue = value
        validCurrencyValueCount += 1
    }
}

final class LimitAdjustmentInteractorTests: XCTestCase {
    private lazy var container = DependencyContainerMock()
    private lazy var serviceSpy = LimitAdjustmentServiceSpy()
    private lazy var presenterSpy = LimitAdjustmentPresenterSpy()
    private lazy var sut = LimitAdjustmentInteractor(service: serviceSpy, presenter: presenterSpy, dependencies: container)
    
    func testPresentSuccessState_WhenIncrementIs10_ShouldReturnNextMultiple() {
        let infoLimit = CreditLimit(maxValue: 3000.0, minValue: 200, increment: 10, currentValue: 0.0, balance: 0.0)
        serviceSpy.expectedResultOfGet = .success(infoLimit)
        sut.loadLimit()
        sut.changeLimit(value: 210.0)
        XCTAssertEqual(presenterSpy.validCurrencyValue, 210.0)
        sut.changeLimit(value: 2999.0)
        XCTAssertEqual(presenterSpy.validCurrencyValue, 3000.0)
    }
    
    func testPresentSuccessState_WhenIncrementIs2_ShouldReturnNextMultiple() {
        let infoLimit = CreditLimit(maxValue: 3000.0, minValue: 200, increment: 2, currentValue: 0.0, balance: 0.0)
        serviceSpy.expectedResultOfGet = .success(infoLimit)
        sut.loadLimit()
        sut.changeLimit(value: 201.0)
        XCTAssertEqual(presenterSpy.validCurrencyValue, 202.0)
        sut.changeLimit(value: 2999.0)
        XCTAssertEqual(presenterSpy.validCurrencyValue, 3000.0)
    }

    func testPresentSuccessState_WhenValueIsMinimum_ShouldReturnMinimumValue() {
        let minValue = 200.0
        let infoLimit = CreditLimit(maxValue: 3000.0, minValue: minValue, increment: 10, currentValue: 0.0, balance: 0.0)
        serviceSpy.expectedResultOfGet = .success(infoLimit)
        sut.loadLimit()
        sut.changeLimit(value: minValue)
        XCTAssertEqual(presenterSpy.validCurrencyValue, minValue)
        XCTAssertEqual(presenterSpy.validCurrencyValueCount, 2)
    }

    func testPresentSuccessState_WhenValueIsMaximum_ShouldReturnMaximumValue() {
        let maxValue = 3000.0
        let infoLimit = CreditLimit(maxValue: maxValue, minValue: 200, increment: 10, currentValue: 0.0, balance: 0.0)
        serviceSpy.expectedResultOfGet = .success(infoLimit)
        sut.loadLimit()
        sut.changeLimit(value: maxValue)
        XCTAssertEqual(presenterSpy.validCurrencyValue, maxValue)
        XCTAssertEqual(presenterSpy.validCurrencyValueCount, 2)
    }
    
    func testPresentIdealState_WhenReciveSuccess_ShouldHoldValidInfoLimitAndPassToPresenter() throws {
        let expectedInfoLimit = CreditLimit(maxValue: 3000.0, minValue: 200, increment: 10, currentValue: 0.0, balance: 0.0)
        serviceSpy.expectedResultOfGet = .success(expectedInfoLimit)
        sut.loadLimit()
        let receiveInfoLimit = try XCTUnwrap(presenterSpy.receiveInfo)
        XCTAssertEqual(expectedInfoLimit.maxValue, receiveInfoLimit.maxValue)
        XCTAssertEqual(presenterSpy.setupWithInfoLimitCount, 1)
        XCTAssertEqual(presenterSpy.loadStateCount, 1)
    }
    
    func testPresentAvailableLimit_WhenReciveBalanceBiggerThanCurrentValueLimit_ShouldPassZero() {
        let expectedInfoLimit = CreditLimit(maxValue: 3000.0, minValue: 200, increment: 10, currentValue: 800, balance: 1000)
        serviceSpy.expectedResultOfGet = .success(expectedInfoLimit)
        sut.loadLimit()
        XCTAssertEqual(presenterSpy.availableLimit, 0.0)
    }
    
    func testPresentAvailableLimit_WhenReciveBalanceLowerThanCurrentValueLimit_ShouldReturnTheDiference() {
        let expectedInfoLimit = CreditLimit(maxValue: 3000.0, minValue: 200, increment: 10, currentValue: 800, balance: 799)
        serviceSpy.expectedResultOfGet = .success(expectedInfoLimit)
        sut.loadLimit()
        XCTAssertEqual(presenterSpy.availableLimit, 1)
    }
   
    func testPresentIdealState_WhenReciveErro_ShouldPassApiErrorToPresenter() throws {
        serviceSpy.expectedResultOfGet = .failure(.serverError)
        sut.loadLimit()
        XCTAssertEqual(presenterSpy.errorStateCount, 1)
        XCTAssertNotNil(presenterSpy.receiveApiError)
        let cardLimitLoadedReceived = try XCTUnwrap(presenterSpy.cardLimitLoadedReceive)
        XCTAssertEqual(cardLimitLoadedReceived, false)
    }
    
    func testPresentUpadatedCrediLimit_WhenReciveErro_ShouldPassApiErrorToPresenter() throws {
        serviceSpy.expectedResultSetNewLimit = .failure(.bodyNotFound)
        sut.setNewLimit()
        XCTAssertEqual(presenterSpy.errorStateCount, 1)
        XCTAssertNotNil(presenterSpy.receiveApiError)
        let cardLimitLoadedReceived = try XCTUnwrap(presenterSpy.cardLimitLoadedReceive)
        XCTAssertEqual(cardLimitLoadedReceived, true)
    }
    
    func  testPresentUpadatedCrediLimit_WhenReciveSuccess_ShouldCallNewLimitSetAndPassCurrentLimit() throws {
        let creditLimit = CreditLimit(maxValue: 3000.0, minValue: 200, increment: 10, currentValue: 800, balance: 799)
        serviceSpy.expectedResultOfGet = .success(creditLimit)
        let expectedLimit = 1000.0
        sut.loadLimit()
        sut.changeLimit(value: expectedLimit)
        serviceSpy.expectedResultSetNewLimit = .success(NoContent())
        sut.setNewLimit()
        
        XCTAssertEqual(presenterSpy.newLimitValueReceive, expectedLimit)
        XCTAssertEqual(presenterSpy.newLimitSetCount, 1)
    }
   
    func testCloseView_WhenCallTeFuction_ShouldPassToPresenterCloseAction() {
        sut.closeView()
        XCTAssertEqual(presenterSpy.actions, [.close])
    }
    
    func testTryAgain_WhenCreditLimitIsLoad_ShouldCallUpdateLimit() {
        let creditLimit = CreditLimit(maxValue: 3000.0, minValue: 200, increment: 10, currentValue: 800, balance: 799)
        serviceSpy.expectedResultOfGet = .success(creditLimit)
        sut.loadLimit()
        sut.tryAgain()
        XCTAssertEqual(presenterSpy.validCurrencyValueCount, 1)
    }
    
    func testTryAgain_WhenCreditLimitNotLoad_ShouldCallUpdateLimit() {
        sut.tryAgain()
        XCTAssertEqual(presenterSpy.errorStateCount, 1)
        XCTAssertEqual(presenterSpy.cardLimitLoadedReceive, false)
    }
    
    func testFindNextMultipleOfIncrement_WhenCreditLimitIsLoad_ShouldReturnMultipleEqualOrGreaterValue() throws {
        let creditLimit = CreditLimit(maxValue: 3000.0, minValue: 200, increment: 10, currentValue: 800, balance: 799)
        serviceSpy.expectedResultOfGet = .success(creditLimit)
        sut.loadLimit()
        let receive = try XCTUnwrap(sut.findNextMultipleOfIncrement(value: 205))
        let expected = 210.0
        XCTAssertEqual(receive, expected)
    }
    
    func testFindNextMultipleOfIncrement_WhenCreditLimitNotLoad_ShouldReturnNil()  {
        XCTAssertNil(sut.findNextMultipleOfIncrement(value: 205))
    }
    
    func testIsEqualMinOrMaxValue_WhenCreditLimitIsLoad_ShouldReturnTrueWhenValueIsEqualMaxOrMin() {
        let maxValue = 3000.0
        let minValue = 200.0
        let creditLimit = CreditLimit(maxValue: maxValue, minValue: minValue, increment: 10, currentValue: 800, balance: 799)
        serviceSpy.expectedResultOfGet = .success(creditLimit)
        sut.loadLimit()
        XCTAssertTrue(sut.isEqualMinOrMaxValue(maxValue))
        XCTAssertTrue(sut.isEqualMinOrMaxValue(minValue))
        XCTAssertFalse(sut.isEqualMinOrMaxValue(199.0))
        XCTAssertFalse(sut.isEqualMinOrMaxValue(2999.0))
    }
    
    func testTakeNextValueMultiple_WhenCreditLimitNotLoad_ShouldReturnZero() {
        XCTAssertEqual(sut.takeNextValueMultiple(value: 3000.0), 0.0)
    }
    
    func testTakeNextValueMultiple_WhenCreditLimitIsLoad_ShouldReturnMultipleValue() {
        let creditLimit = CreditLimit(maxValue: 3000.0, minValue: 200, increment: 10, currentValue: 800, balance: 799)
        serviceSpy.expectedResultOfGet = .success(creditLimit)
        sut.loadLimit()
        XCTAssertEqual(sut.takeNextValueMultiple(value: 3000.0), 3000.0)
        XCTAssertEqual(sut.takeNextValueMultiple(value: 200), 200)
        XCTAssertEqual(sut.takeNextValueMultiple(value: 2882), 2890.0)
    }
}
