import AnalyticsModule
import Core
import FeatureFlag
import UI
import XCTest
@testable import Card

class LimitAdjustmentCoordinatingSpy: LimitAdjustmentCoordinating {
    var viewController: UIViewController?
    private(set) var actions: [LimitAdjustmentAction] = []
    func perform(action: LimitAdjustmentAction) {
        actions.append(action)
    }
}

class LimitAdjustmentDisplayingSpy: LimitAdjustmentDisplaying {
    private(set) var displayLoadingStateCount = 0
    private(set) var receiveInfoCardLimit: CreditLimit?
    private(set) var displayIdealStateCount = 0
    private(set) var receiveDisplayCurrencyValue = 0.0
    private(set) var displayCurrencyValueCount = 0
    private(set) var displayErrorStateCount = 0
    private(set) var displayUpdatedLimitAvaliableLabelCount = 0
    private(set) var dislplaySuccessPopupCount = 0
    private(set) var displayFailurePopupCount = 0
    private(set) var attributedTextReceive: NSAttributedString?
    func displayUpdatedLimitAvaliableLabel(_ attributedText: NSAttributedString) {
        displayUpdatedLimitAvaliableLabelCount += 1
        attributedTextReceive = attributedText
    }
    
    func displayLoadingState() {
        displayLoadingStateCount += 1
    }
    
    func displayErrorState(error: StatefulViewModeling) {
        displayErrorStateCount += 1
    }
    
    func displayIdealState(info: CreditLimit) {
        receiveInfoCardLimit = info
        displayIdealStateCount += 1
    }
    
    func dislplaySuccessPopup(_ popupViewController: PopupViewController) {
        dislplaySuccessPopupCount += 1
    }
    
    func displayValidCurrencyValue(value: Double) {
        displayCurrencyValueCount += 1
        receiveDisplayCurrencyValue = value
    }
    
    func displayFailurePopup(_ popupViewController: PopupViewController) {
        displayFailurePopupCount += 1
    }

}

class LimitAdjustmentPresenterTests: XCTestCase {
    private lazy var container = DependencyContainerMock()
    private lazy var coordinatingSpy = LimitAdjustmentCoordinatingSpy()
    private lazy var displayingSpy = LimitAdjustmentDisplayingSpy()
    
    private lazy var sut: LimitAdjustmentPresenter = {
        let presenter = LimitAdjustmentPresenter(coordinator: coordinatingSpy, dependencies: container)
        presenter.viewController = displayingSpy
        return presenter
    }()
    
    func testDisplayIdealState_WhenInteractorPassValidCardLimitInfo_ShouldDisplayIdealState() throws {
        let expectedCardLimit = CreditLimit(maxValue: 2000, minValue: 200, increment: 20, currentValue: 200, balance: 30)
        sut.presentIdealState(info: expectedCardLimit)
        let recivedCardLimit = try XCTUnwrap(displayingSpy.receiveInfoCardLimit)
       
        XCTAssertEqual(expectedCardLimit.maxValue, recivedCardLimit.maxValue)
        XCTAssertEqual(expectedCardLimit.minValue, recivedCardLimit.minValue)
        XCTAssertEqual(expectedCardLimit.increment, recivedCardLimit.increment)
        XCTAssertEqual(expectedCardLimit.currentValue, recivedCardLimit.currentValue)
        XCTAssertEqual(expectedCardLimit.balance, recivedCardLimit.balance)
        
        XCTAssertEqual(displayingSpy.displayIdealStateCount, 1)
    }
    func testPresentAvailableLimit_WhenInteractorPassAvaliableValueLimit_ShouldDisplayValueFormattedScenarioOne() throws {
        sut.presentAvailableLimit(value: 2000)
        let received = try XCTUnwrap(displayingSpy.attributedTextReceive)
        let expecetedString = "R$ 2.000,00 disponível"
        XCTAssertEqual(received.string, expecetedString)
        XCTAssertEqual(displayingSpy.displayUpdatedLimitAvaliableLabelCount, 1)

    }
    
    func testPresentAvailableLimit_WhenHasNoLimitAvailable_ShouldDisplayValueFormattedScenarioTwo() throws {
        sut.presentAvailableLimit(value: 0)
        let received = try XCTUnwrap(displayingSpy.attributedTextReceive)
        let expecetedString = "R$ 0,00 disponível"
        XCTAssertEqual(received.string, expecetedString)
        XCTAssertEqual(displayingSpy.displayUpdatedLimitAvaliableLabelCount, 1)

    }
    func testDisplayLoadingState_WhenInteractorCall_ShouldUpdateViewControllerWithLoad() {
        sut.presentLoadState()
        XCTAssertEqual(displayingSpy.displayLoadingStateCount, 1)
    }
    
    func testCloseScene_WhenInteractorPassCloseAction_SholdPassActionToCoordinator() {
        sut.didNextStep(action: .close)
        XCTAssertEqual(coordinatingSpy.actions, [.close])
    }
    
    func testPresentUpdatedCrediLimit_WhenInteractorPassValidValue_SholdDisplayValidCurrencyValue(){
        let expectedCurrencyValue = 101.11
        sut.presentUpdatedCrediLimit(value: expectedCurrencyValue)
        XCTAssertEqual(expectedCurrencyValue, displayingSpy.receiveDisplayCurrencyValue)
        XCTAssertEqual(displayingSpy.displayCurrencyValueCount, 1)
    }

    func testPresentErrorState_WhenInteractorPassError_SholdDisplayErrorState() {
        let error = ApiError.connectionFailure
        sut.presentErrorState(error: error, cardLimitLoaded: true)
        sut.presentErrorState(error: error, cardLimitLoaded: false)
        
        XCTAssertEqual(displayingSpy.displayErrorStateCount, 1)
        XCTAssertEqual(displayingSpy.displayFailurePopupCount, 1)
    }
    
    func testPresentSuccessState_WhenInteractorPassNewLimit_SholdDislplaySuccessPopup() {
        let currentValue = 120.11
        sut.presentSuccessState(currentValue: currentValue)
        XCTAssertEqual(displayingSpy.dislplaySuccessPopupCount, 1)
    }
}
