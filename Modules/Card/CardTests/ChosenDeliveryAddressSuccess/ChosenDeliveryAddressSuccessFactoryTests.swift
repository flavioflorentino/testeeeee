import XCTest
@testable import Card

class ChosenDeliveryAddressSuccessFactoryTests: XCTestCase {
    func testMakeShouldReturnViewController() {
        let delegate = FakeChosenDeliveryAddressSuccessCoordinatorDelegate()
        let viewController = ChosenDeliveryAddressSuccessFactory.make(delegate: delegate)
        XCTAssertNotNil(viewController.viewModel is ChosenDeliveryAddressSuccessViewModel)
    }
}
