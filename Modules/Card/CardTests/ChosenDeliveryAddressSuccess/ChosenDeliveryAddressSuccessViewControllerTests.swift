import XCTest
@testable import Card

class ChosenDeliveryAddressSuccessViewControllerTests: XCTestCase {
    var sut: ChosenDeliveryAddressSuccessViewController!
    var fakeCoordinator: FakeChosenDeliveryAddressSuccessCoordinator!
    var fakeViewModel: FakeChosenDeliveryAddressSuccessViewModel!
    
    override func setUp() {
        super.setUp()
        fakeViewModel = FakeChosenDeliveryAddressSuccessViewModel()
        fakeCoordinator = FakeChosenDeliveryAddressSuccessCoordinator()
        
        let controller = ChosenDeliveryAddressSuccessViewController(viewModel: fakeViewModel)
        
        sut = UINavigationController(rootViewController: controller).viewControllers.first as? ChosenDeliveryAddressSuccessViewController
        
        fakeCoordinator.viewController = sut
        
        sut.beginAppearanceTransition(true, animated: false)
        sut.endAppearanceTransition()
    }
    
    func testDidTapConfirmThenCloseWasCalled() {
        sut.didTapConfirm()
        XCTAssertTrue(fakeViewModel.closeWasCalled)
    }
}
