import XCTest
@testable import Card

class ChosenDeliveryAddressSuccessCoordinatorTests: XCTestCase {
    var sut: ChosenDeliveryAddressSuccessCoordinator!
    var viewController = UIViewController()
    var fakeDelegate = FakeChosenDeliveryAddressSuccessCoordinatorDelegate()
    
    override func setUp() {
        super.setUp()
        sut = ChosenDeliveryAddressSuccessCoordinator()
        sut.delegate = fakeDelegate
        viewController = UIViewController()
        sut.viewController = viewController
    }
    func testPerformActionReadContractShouldCallDelegate() {
        sut.perform(action: .close)
        XCTAssertTrue(fakeDelegate.didCloseChosenDeliveryAddressSuccessWasCalled)
    }
}
