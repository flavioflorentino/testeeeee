import XCTest
@testable import Card

final class ChosenDeliveryAddressSuccessViewModelTests: XCTestCase {
    lazy var sut = ChosenDeliveryAddressSuccessViewModel(coordinator: coordinatorSpy)
    var coordinatorSpy = FakeChosenDeliveryAddressSuccessCoordinator()
    
    func testClose() {
        sut.close()
        XCTAssertEqual(coordinatorSpy.actionType, .close)
    }
}
