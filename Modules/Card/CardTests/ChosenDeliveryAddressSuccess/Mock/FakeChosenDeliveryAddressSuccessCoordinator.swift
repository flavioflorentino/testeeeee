@testable import Card

class FakeChosenDeliveryAddressSuccessCoordinator: ChosenDeliveryAddressSuccessCoordinatorProtocol {
    var viewController: UIViewController?
    var delegate: ChosenDeliveryAddressSuccessCoordinatorDelegate?
    
    var actionType: ChosenDeliveryAddressSuccessAction!
    
    func perform(action: ChosenDeliveryAddressSuccessAction) {
        actionType = action
    }
}
