@testable import Card

final class FakeChosenDeliveryAddressSuccessViewModel: ChosenDeliveryAddressSuccessViewModelInputs {
    
    var closeWasCalled = false
    
    func close() {
        closeWasCalled = true
    }
}
