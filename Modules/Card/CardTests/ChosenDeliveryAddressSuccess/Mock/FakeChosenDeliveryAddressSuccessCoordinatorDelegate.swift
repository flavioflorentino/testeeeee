@testable import Card

class FakeChosenDeliveryAddressSuccessCoordinatorDelegate: ChosenDeliveryAddressSuccessCoordinatorDelegate {
    var didCloseChosenDeliveryAddressSuccessWasCalled = false
    
    func didCloseChosenDeliveryAddressSuccess() {
        didCloseChosenDeliveryAddressSuccessWasCalled = true
    }
}
