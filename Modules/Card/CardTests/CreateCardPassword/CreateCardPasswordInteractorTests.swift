import AnalyticsModule
import Core
import Foundation
import XCTest
@testable import Card

private final class CreateCardPasswordPresentingSpy: CreateCardPasswordPresenting {
    var viewController: CreateCardPasswordDisplay?
    
    private(set) var callUpdateCount = 0
    private(set) var callErrorCount = 0
    private(set) var callNextStep = 0
    private(set) var selectedAction: CreateCardPasswordAction?
    
    func updateView() {
        callUpdateCount += 1
    }
    
    func show(errorDescription: String) {
        callErrorCount += 1
    }
    
    func didNextStep(action: CreateCardPasswordAction) {
        callNextStep += 1
        selectedAction = action
    }
}

private final class ServiceSpy: CreateCardPasswordServicing {
    var isSuccess = true
    
    func createCardPassword(_ cardPin: String, password: String, then completion: @escaping (Result<Void, ApiError>) -> Void) {
        guard isSuccess else {
            let error = ApiError.serverError
            completion(.failure(error))
            return
        }
        completion(.success(()))
    }
}

private final class AuthManagerSpy: AuthContract {
    func authenticate(sucessHandler: @escaping (String, Bool) -> Void, cancelHandler: (() -> Void)?) {
        sucessHandler("", true)
    }
}

class CreateCardPasswordInteractorTests: XCTestCase {
    // MARK: - Variables
    private var presenterSpy = CreateCardPasswordPresentingSpy()
    private let serviceSpy = ServiceSpy()
    private let authManagerSpy = AuthManagerSpy()
    private let analyticsSpy = AnalyticsSpy()
    
    private lazy var sut: CreateCardPasswordInteractor = {
        let container = DependencyContainerMock(analyticsSpy, authManagerSpy)
        let sut = CreateCardPasswordInteractor(presenter: presenterSpy, service: serviceSpy, dependencies: container)
        return sut
    }()
    
    func testDidTapHelper_WhenClick_ShouldOpenHelperScreen() {
        sut.didTapHelper()
        XCTAssertEqual(presenterSpy.callNextStep, 1)
        XCTAssertEqual(presenterSpy.selectedAction, .securityTips)
    }
    
    func testDidTapConfirmWithAuthentication_WhenSucess_ShouldPassSuccess() {
        sut.didTapConfirmWithAuthentication(and: "")
        XCTAssertEqual(presenterSpy.callNextStep, 1)
        XCTAssertEqual(presenterSpy.selectedAction, .success)
    }
    
    func testDidTapConfirmWithAuthentication_WhenError_ShouldPassErrorMessage() {
        serviceSpy.isSuccess = false
        sut.didTapConfirmWithAuthentication(and: "")
        XCTAssertEqual(presenterSpy.callNextStep, 0)
        XCTAssertEqual(presenterSpy.callErrorCount, 1)
    }
    
    func testEAS() {
        sut.updateView()
        XCTAssertEqual(presenterSpy.callUpdateCount, 1)
    }
}
