import XCTest
@testable import Card

private final class CreateCardPasswordCordinatingSpy: CreateCardPasswordCoordinating {
    var viewController: UIViewController?
    var delegate: CreateCardPasswordCoordinatorDelegate?
    
    //MARK: - perform
    private(set) var callPerformCount = 0
    private(set) var selectedAction: CreateCardPasswordAction?

    func perform(action: CreateCardPasswordAction) {
        callPerformCount += 1
        selectedAction = action
    }
}

private final class CreateCardPasswordViewControllerSpy: CreateCardPasswordDisplay {
    private(set) var callPresentCount = 0
    private(set) var callErrorCount = 0
    private(set) var callAuthenticateCount = 0

    
    func present(_ presenter: DigitFormPresenting) {
        callPresentCount += 1
    }
    
    func show(errorDescription: String) {
        callErrorCount += 1
    }
    
    func didAuthenticate() {
        callAuthenticateCount += 1
    }
}

class CreateCardPasswordPresenterTests: XCTestCase {
    private let coordinatorSpy = CreateCardPasswordCordinatingSpy()
    private let viewControllerSpy = CreateCardPasswordViewControllerSpy()
    
    private lazy var sut: CreateCardPasswordPresenting = {
        let sut = CreateCardPasswordPresenter(coordinator: coordinatorSpy)
        sut.viewController = viewControllerSpy
        return sut
    }()
    
    func testUpdateView_WhenAppear_DidRefreshUI() {
        sut.updateView()
        XCTAssertEqual(viewControllerSpy.callPresentCount, 1)
    }
    
    func testShow_WhenHasErrorDescription_ShouldShowADialogError() {
        let errorDescription = "Erro apresentado"
        sut.show(errorDescription: errorDescription)
        XCTAssertEqual(viewControllerSpy.callErrorCount, 1)
    }
    
    func testDidNextStep_WhenSuccess_ShouldCallScreen() {
        sut.didNextStep(action: .success)
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssertEqual(coordinatorSpy.selectedAction, .success)
    }
    
    func testDidNextStep_WhenSecurityTips_ShouldCallScreen() {
        sut.didNextStep(action: .securityTips)
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssertEqual(coordinatorSpy.selectedAction, .securityTips)
    }
}
