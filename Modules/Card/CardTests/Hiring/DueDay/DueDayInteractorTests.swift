import XCTest
import Core
import AnalyticsModule
@testable import Card

private final class DueDayServiceMock: DueDayServicing {
    var getDueDaysExpectedResult: Result<DueDayResponse, ApiError> = .failure(.connectionFailure)
    var saveDueDaysExpectedResult: Result<NoContent, ApiError> = .failure(.connectionFailure)
    
    func getDueDays(_ completion: @escaping (Result<DueDayResponse, ApiError>) -> Void) {
        completion(getDueDaysExpectedResult)
    }
    
    func saveDueDays(dueDay: Int, _ completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        completion(saveDueDaysExpectedResult)
    }
}

private final class DueDayPresenterSpy: DueDayPresenting {
    var viewController: DueDayDisplaying?
    
    var didPresentErrorCount = 0
    var screenData: DueDayInteractor.ControllerData?
    var action: DueDayAction?
    
    func presentError(_ error: ApiError) {
        didPresentErrorCount += 1
    }
    
    func presentControllerData(_ controllerData: DueDayInteractor.ControllerData) {
        screenData = controllerData
    }
    
    func presentNextStep(action: DueDayAction) {
        self.action = action
    }
}

final class DueDayInteractorTests: XCTestCase {
    fileprivate let analyticsSpy = AnalyticsSpy()
    fileprivate let serviceMock = DueDayServiceMock()
    fileprivate let presenterSpy = DueDayPresenterSpy()
    private lazy var sut = DueDayInteractor(service: serviceMock,
                                            presenter: presenterSpy,
                                            dependencies: DependencyContainerMock(analyticsSpy),
                                            cardForm: CardForm())
    
    func testConfirm_WhenRequestFails_ShouldPresentError() {
        sut.confirm()
        XCTAssertEqual(presenterSpy.didPresentErrorCount, 1)
    }
    
    func testConfirm_WhenRequestSucceeds_ShouldConfirm() {
        let dueDay = 1
        var cardForm = CardForm()
        cardForm.dueDay = dueDay
        sut.availableDays = [.init(dueDay: 1, bestPurchaseDay: 5)]
        sut.currentDueDay = dueDay
        serviceMock.saveDueDaysExpectedResult = .success(.init())
        sut.confirm()
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(presenterSpy.action, .confirm(cardForm: cardForm, bestPurchaseDay: 5))
    }
    
    func testSelectRow_ShouldLogAnalytics() {
        sut.availableDays = [.init(dueDay: 1, bestPurchaseDay: 5)]
        sut.selectRow(0)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }
    
    func testLoadData_WhenRequestSucceeds_ShouldPresentScreenData() {
        serviceMock.getDueDaysExpectedResult = .success(
            .init(
                currentDueDay: 1,
                validDueDays: [
                    .init(
                        dueDay: 1,
                        bestPurchaseDay: 5
                    )
                ]
            )
        )
        sut.loadData()
        XCTAssertEqual(presenterSpy.screenData, .init(selectedRow: 0,
                                                      availableDays: [1],
                                                      bestPurchaseDay: 5)
        )
    }
    
    func testLoadData_WhenRequestSucceeds_ShouldPresentError() {
        sut.loadData()
        XCTAssertEqual(presenterSpy.didPresentErrorCount, 1)
    }
}
