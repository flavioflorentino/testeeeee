import XCTest
import UI
@testable import Card

private final class DueDayCoordinatorSpy: DueDayCoordinating {
    var action: DueDayAction?
    weak var delegate: DueDayCoordinatingDelegate?
    
    func perform(action: DueDayAction) {
        self.action = action
    }
}

private final class DueDayViewControllerSpy: DueDayDisplaying {
    var screenData: DueDayRootViewData?
    var didLoadCount = 0
    var didShowErrorCount = 0
    
    func displayError(_ statefulError: StatefulErrorViewModel) {
        didShowErrorCount += 1
    }
    
    func displayControllerData(_ controllerData: DueDayRootViewData) {
        screenData = controllerData
    }
    
    func displayLoading(_ bool: Bool) {
        didLoadCount += bool ? 1 : 0
    }
}

final class DueDayPresenterTests: XCTestCase {
    fileprivate let coordinatorSpy = DueDayCoordinatorSpy()
    fileprivate let controllerSpy = DueDayViewControllerSpy()
    
    private lazy var sut: DueDayPresenter = {
        let presenter = DueDayPresenter(coordinator: coordinatorSpy, dependencies: DependencyContainerMock())
        presenter.viewController = controllerSpy
        return presenter
    }()
    
    func testPresentNextStep_CoordinatorShouldConfirm() {
        sut.presentNextStep(action: .confirm(cardForm: CardForm(), bestPurchaseDay: 5))
        XCTAssertEqual(coordinatorSpy.action, .confirm(cardForm: CardForm(), bestPurchaseDay: 5))
    }
    
    func testPresentScreenData_ShouldShowData() {
        sut.presentControllerData(.init(selectedRow: 5, availableDays: [1,2,3], bestPurchaseDay: nil))
        XCTAssertEqual(controllerSpy.screenData, DueDayRootViewData(selectedRow: 5, availableDays: [1,2,3], bestPurchaseDayAttrString: nil))
    }
    
    func testPresentError_ShouldShowError() {
        sut.presentError(.connectionFailure)
        XCTAssertEqual(controllerSpy.didShowErrorCount, 1)
    }
}
