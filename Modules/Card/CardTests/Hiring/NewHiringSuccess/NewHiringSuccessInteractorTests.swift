import AnalyticsModule
import FeatureFlag
import Core
import XCTest
@testable import Card

private final class NewHiringSuccessPresentingSpy: NewHiringSuccessPresenting {
    // MARK: - Variables
    var viewController: NewHiringSuccessDisplaying?
    private(set) var callPresentInitialSetup = 0
    private(set) var isCashbackEnable = false
    private(set) var action = [NewHiringSuccessAction]()
    
    func presentInitialSetup(isCashbackEnable: Bool) {
        self.isCashbackEnable = isCashbackEnable
        callPresentInitialSetup += 1
    }
    
    func didNextStep(action: NewHiringSuccessAction) {
        self.action.append(action)
    }
}

final class NewHiringSuccessInteractorTests: XCTestCase {
    // MARK: - Variables
    typealias Dependencies = HasAnalytics
    private let analyticsSpy = AnalyticsSpy()
    private let featureManagerMock = FeatureManagerMock()
    private let presenterSpy = NewHiringSuccessPresentingSpy()
    
    private lazy var sut: NewHiringSuccessInteractor = {
        let container = DependencyContainerMock(analyticsSpy, featureManagerMock)
        let sut = NewHiringSuccessInteractor(presenter: presenterSpy, dependencies: container)
        return sut
    }()

    override func setUp() {
        let remoteConfigMock = RemoteConfigMock()
        RemoteConfigSetup.inject(instance: remoteConfigMock)
        featureManagerMock.override(keys: .cashbackActivation, with: false)
    }
    
    // MARK: - Public Methods
    func testShowInitialSetup_WhenDidLoadAndFlagIsFalse_ShouldUpdateScreen() {
        featureManagerMock.override(keys: .cashbackActivation, with: false)
        sut.showInitialSetup()
        XCTAssertEqual(presenterSpy.callPresentInitialSetup, 1)
        XCTAssertEqual(presenterSpy.isCashbackEnable, false)
    }
    
    func testShowInitialSetup_WhenDidLoadAndFlagIsTrue_ShouldUpdateScreen() {
        featureManagerMock.override(keys: .cashbackActivation, with: true)
        sut.showInitialSetup()
        XCTAssertEqual(presenterSpy.callPresentInitialSetup, 1)
        XCTAssertEqual(presenterSpy.isCashbackEnable, true)
    }
    
    func testConfirm_WhenConfirmAndFlagIsFalse_ShouldCloseScreen() {
        featureManagerMock.override(keys: .cashbackActivation, with: false)
        sut.confirm()
        XCTAssertEqual(presenterSpy.action, [.close])
        let event = analyticsSpy.events.last
        let action = event?.properties["action"] as? String
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(event?.name, "PicPay Card -  Request - Card Activated Message")
        XCTAssertEqual(action, "act-view-close")
    }
    
    func testViewTerms_WhenSelect_ShouldSendEvent() {
        featureManagerMock.override(keys: .cashbackActivation, with: true)
        sut.confirm()
        XCTAssertEqual(presenterSpy.action, [.cashbackOnboarding])
        let event = analyticsSpy.events.last
        let action = event?.properties["action"] as? String
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(event?.name, "PicPay Card -  Request - Card Activated Message")
        XCTAssertEqual(action, "act-view-cashback-conditions")
    }
    
    func testTextForResume_WhenDidAppear_ShouldUpdateScreen() {
        sut.close()
        XCTAssertEqual(presenterSpy.action, [.close])
    }
}

