import XCTest
@testable import Card
import UI

private class NewHiringSuccessCoordinatingSpy: NewHiringSuccessCoordinating {
    // MARK: - Variables
    var delegate: HiringFlowCoordinating?
    private(set) var action = [NewHiringSuccessAction]()
    
    func perform(action: NewHiringSuccessAction) {
        self.action.append(action)
    }
}

private class NewHiringSuccessDisplaySpy: NewHiringSuccessDisplaying {
    // MARK: - Variables
    private(set) var displayScreenCallsCount = 0
    private(set) var description = ""
    private(set) var buttonTittle = ""
    private(set) var isCashbackEnable = false
    
    func displayScreen(description: String, buttonTittle: String, isCashbackEnable: Bool) {
        displayScreenCallsCount += 1
        self.description = description
        self.buttonTittle = buttonTittle
        self.isCashbackEnable = isCashbackEnable
    }
}

public class NewHiringSuccessPresenterTests: XCTestCase {
    // MARK: - Variables
    private var viewControllerSpy = NewHiringSuccessDisplaySpy()
    private let coordinatorSpy = NewHiringSuccessCoordinatingSpy()
    
    private lazy var sut: NewHiringSuccessPresenter = {
        let sut = NewHiringSuccessPresenter(coordinator: coordinatorSpy)
        sut.viewController = viewControllerSpy
        return sut
    }()

    // MARK: - Tests
    func testDidNextStep_WhenConfirmIsCashback_ShouldCallCoordinator() {
        sut.didNextStep(action: .cashbackOnboarding)
        XCTAssertEqual(coordinatorSpy.action, [.cashbackOnboarding])
    }
    
    func testDidNextStep_WhenCloseIsCalled_ShouldCallCoordinator() {
        sut.didNextStep(action: .close)
        XCTAssertEqual(coordinatorSpy.action, [.close])
    }
    
    func testPresentInitialSetup_WhenDidLoadAndCashbackIsTrue_ShouldUpdateScreen() {
        let description = "Seu cartão irá chegar em **até 10 dias úteis**, mas você já pode usá-lo aqui no PicPay como forma de pagamento na carteira e aproveitar 5% de dinheiro de volta!"
        sut.presentInitialSetup(isCashbackEnable: true)
        XCTAssertEqual(viewControllerSpy.displayScreenCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.description, description)
        XCTAssertEqual(viewControllerSpy.buttonTittle, "Ver condições de cashback")
        XCTAssertEqual(viewControllerSpy.isCashbackEnable, true)
    }
    
    func testPresentInitialSetup_WhenDidLoadAndCashbackIsFalse_ShouldUpdateScreen() {
        let description = "Seu cartão irá chegar em **até 10 dias úteis**, mas você já pode usá-lo aqui no PicPay como forma de pagamento na carteira."
        sut.presentInitialSetup(isCashbackEnable: false)
        XCTAssertEqual(viewControllerSpy.displayScreenCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.description, description)
        XCTAssertEqual(viewControllerSpy.buttonTittle, "Entendi")
        XCTAssertEqual(viewControllerSpy.isCashbackEnable, false)
    }
}
