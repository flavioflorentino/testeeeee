import XCTest
import AnalyticsModule
@testable import Card

private final class ChangeAddressPopupPresenterSpy: ChangeAddressPopupPresenting {
    var action: ChangeAddressPopupAction?
    var didPresentActionCount = 0
    
    func presentNextStep(action: ChangeAddressPopupAction) {
        self.action = action
        didPresentActionCount += 1
    }
}

final class ChangeAddressPopupInteractorTests: XCTestCase {
    fileprivate let presenterSpy = ChangeAddressPopupPresenterSpy()
    let analyticsSpy = AnalyticsSpy()
    lazy var sut = ChangeAddressPopupInteractor(presenter: presenterSpy, dependencies: DependencyContainerMock(analyticsSpy))
    
    func testConfirm_ShouldPerformConfirm() {
        sut.confirm()
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(presenterSpy.didPresentActionCount, 1)
        XCTAssertEqual(presenterSpy.action, .confirm)
    }
    
    func testCancel_ShouldPerformCancel() {
        sut.cancel()
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(presenterSpy.didPresentActionCount, 1)
        XCTAssertEqual(presenterSpy.action, .cancel)
    }

}
