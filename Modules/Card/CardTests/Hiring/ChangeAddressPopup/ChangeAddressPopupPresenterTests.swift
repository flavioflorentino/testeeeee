import XCTest
@testable import Card

private final class ChangeAddressPopupCoordinatorSpy: ChangeAddressPopupCoordinating {
    var viewController: UIViewController?
    
    var action: ChangeAddressPopupAction?
    var didPerformActionCount = 0
    
    func perform(action: ChangeAddressPopupAction) {
        self.action = action
        didPerformActionCount += 1
    }
}

final class ChangeAddressPopupPresenterTests: XCTestCase {
    fileprivate let coordinatorSpy = ChangeAddressPopupCoordinatorSpy()
    lazy var sut = ChangeAddressPopupPresenter(coordinator: coordinatorSpy)
    
    func testPresentNextStep_ShouldPerformAction() {
        sut.presentNextStep(action: .confirm)
        XCTAssertEqual(coordinatorSpy.didPerformActionCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .confirm)
    }
}
