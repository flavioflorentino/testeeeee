import AnalyticsModule
import Core
import XCTest
@testable import Card

private final class HiringResumeServicingSpy: HiringResumeServicing {
    var getFinishExpectedResult: Result<CreditRegistrationFinish, ApiError> = .success(
        CreditRegistrationFinish(registrationStatus: .activated,
                                                    creditStatus: nil,
                                                    debitStatus: nil,
                                                    offerEnabled: false)
    )
        
    func finish(pin: String, _ completion: @escaping CompletionCreditFinish) {
        completion(getFinishExpectedResult)
    }
}

private final class HiringResumePresentingSpy: HiringResumePresenting {
    // MARK: - Variables
    var viewController: HiringResumeDisplaying?
    private(set) var action = [HiringResumeAction]()
    private(set) var callTextForResume = 0
    private(set) var callTextForRules = 0
    private(set) var callShowError = 0
    private(set) var callOpenUrl = 0
    private(set) var selectedURL: URL?
    private(set) var callAuthenticate = 0
    private(set) var insertedPin: String?
    private(set) var callDidNextStep = 0
    private(set) var callDismissLoading = 0
    
    func textForResume() {
        callTextForResume += 1
    }
    
    func textForRules() {
        callTextForRules += 1
    }
    
    func showError(error: ApiError) {
        callShowError += 1
    }
    
    func openURL(with url: URL) {
        selectedURL = url
        callOpenUrl += 1
    }
    
    func authenticate(pin: String) {
        insertedPin = pin
        callAuthenticate += 1
    }
    
    func didNextStep(action: HiringResumeAction) {
        callDidNextStep += 1
        self.action.append(action)
    }
    
    func dismissLoading() {
        callDismissLoading += 1
    }
}

private final class AuthManagerMock: AuthContract {
    var isSuccess = true
    
    func authenticate(sucessHandler: @escaping (String, Bool) -> Void, cancelHandler: (() -> Void)?) {
        guard isSuccess else {
            cancelHandler?()
            return
        }
        sucessHandler("1234", true)
    }
}

private final class PPAuthManagerSpy: PPAuthSwiftContract {
    private(set) var callHandlePassword = 0
    
    func handlePassword() {
        callHandlePassword += 1
    }
}

final class HiringResumeInteractorTests: XCTestCase {
    // MARK: - Variables
    typealias Dependencies = HasAnalytics & AuthContract
    private let analyticsSpy = AnalyticsSpy()
    private let authManagerSpy = AuthManagerMock()
    private let ppAuthManagerSpy = PPAuthManagerSpy()
    private let presenterSpy = HiringResumePresentingSpy()
    private let serviceSpy = HiringResumeServicingSpy()
    
    private lazy var sut: HiringResumeInteractor = {
        let container = DependencyContainerMock(analyticsSpy, authManagerSpy, ppAuthManagerSpy)
        let sut = HiringResumeInteractor(service: serviceSpy, presenter: presenterSpy, dependencies: container)
        return sut
    }()

    private func getRequestError(in fileName: String) -> RequestError {
        let trackingData = try! MockCodable<RequestError>().loadCodableObject(
            resource: fileName,
            typeDecoder: .useDefaultKeys)
        return trackingData
    }
    
    // MARK: - Public Methods
    func testAgreeTerms_WhenSelect_ShouldSendEvent() {
        sut.trackAgreeTerms()
        let event = analyticsSpy.events.last
        let action = event?.properties["action"] as? String
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(event?.name, "PicPay Card - Request - Accept Terms")
        XCTAssertEqual(action, "act-card-agree-terms")
    }
    
    func testViewTerms_WhenSelect_ShouldSendEvent() {
        sut.trackViewTerms()
        let event = analyticsSpy.events.last
        let action = event?.properties["action"] as? String
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(event?.name, "PicPay Card - Request - Accept Terms")
        XCTAssertEqual(action, "act-view-terms")
    }
    
    func testTextForResume_WhenDidAppear_ShouldUpdateScreen() {
        sut.textForResume()
        XCTAssertEqual(presenterSpy.callTextForResume, 1)
    }
    
    func testOpenURL_WhenClickLink_ShouldOpenScreen() throws {
        let url = try XCTUnwrap(URL(string: "teste"))
        sut.openURL(with: url)
        XCTAssertEqual(presenterSpy.callOpenUrl, 1)
        XCTAssertEqual(presenterSpy.selectedURL, url)
    }
    
    func testAuthenticate_WhenIsNotAuthenticated_ShouldSendEvent() {
        authManagerSpy.isSuccess = false
        sut.authenticate()
        let event = analyticsSpy.events.last
        let action = event?.properties["action"] as? String
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(event?.name, "PicPay Card - Request - Password Authentication")
        XCTAssertEqual(action, "act-fingerprint-cancel")
    }
    
    func testAuthenticate_WhenIsAuthenticated_ShouldContinue() {
        authManagerSpy.isSuccess = true
        sut.authenticate()
        XCTAssertEqual(presenterSpy.callAuthenticate, 1)
        XCTAssertEqual(presenterSpy.insertedPin, "1234")
        
        let event = analyticsSpy.events.last
        let action = event?.properties["action"] as? String
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(event?.name, "PicPay Card - Request - Password Authentication")
        XCTAssertEqual(action, "act-fingerprint-used")
    }
    
    func testConfirm_WhenReceivePin_ShouldCallService() {
        sut.confirm(pin: "1234")
        XCTAssertEqual(presenterSpy.callDidNextStep, 1)
        XCTAssertEqual(presenterSpy.action, [.creditActivated])
        
        let event = analyticsSpy.events.last
        let state = event?.properties["state"] as? String
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(event?.name, "PicPay Card - Request - Password Authenticated")
        XCTAssertEqual(state, "st-authentication-validated")
    }
    
    func testConfirm_WhenTimeout_ShouldDisplayError() {
        serviceSpy.getFinishExpectedResult = .failure(ApiError.timeout)
        sut.confirm(pin: "1234")
        XCTAssertEqual(presenterSpy.callShowError, 1)
    }
    
    func testConfirm_WhenRequestError_ShouldDismissLoading() {
        let requestError = getRequestError(in: "passwordError")
        serviceSpy.getFinishExpectedResult = .failure(ApiError.otherErrors(body: requestError))
        sut.confirm(pin: "9876")
        XCTAssertEqual(presenterSpy.callDismissLoading, 1)
        XCTAssertEqual(ppAuthManagerSpy.callHandlePassword, 1)
        
        let event = analyticsSpy.events.last
        let state = event?.properties["state"] as? String
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(event?.name, "PicPay Card - Request - Password Authenticated")
        XCTAssertEqual(state, "st-authentication-failed")
    }
}
