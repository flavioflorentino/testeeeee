import Core
import FeatureFlag
import UI
import XCTest
@testable import Card

private final class HiringResumeCoordinatorSpy: HiringResumeCoordinating {
    // MARK: - Variables
    var viewController: UIViewController?
    private(set) var action = [HiringResumeAction]()

    func perform(action: HiringResumeAction) {
        self.action.append(action)
    }
    
    func open(with url: URL) {
    }
}

private final class HiringResumeViewControllerSpy: HiringResumeDisplaying {
    // MARK: - Variables
    private(set) var callTextForResume = 0
    private(set) var textForResume = ""
    private(set) var callTextForRules = 0
    private(set) var textForRules = ""
    private(set) var callDisplayError = 0
    private(set) var apiError: ApiError?
    private(set) var callAuthenticate = 0
    private(set) var insertedPin = ""
    private(set) var callDismissLoading = 0
    
    func displayTextForResume(text: NSMutableAttributedString) {
        textForResume = text.string
        callTextForResume += 1
    }
    
    func displayTextForTerms(text: NSAttributedString) {
        textForRules = text.string
        callTextForRules += 1
    }
    
    func displayError(error: ApiError) {
        apiError = error
        callDisplayError += 1
    }
    
    func authenticate(pin: String) {
        insertedPin = pin
        callAuthenticate += 1
    }
    
    func dismissLoading() {
        callDismissLoading += 1
    }
}

final class HiringResumePresenterTests: XCTestCase {
    private let featureManagerMock = FeatureManagerMock()
    private let controllerSpy = HiringResumeViewControllerSpy()
    private let coordinatorSpy = HiringResumeCoordinatorSpy()
    
    private lazy var sut: HiringResumePresenter = {
        featureManagerMock.override(key: .termsMultipleLink, with: "linkMultiplo")
        featureManagerMock.override(key: .termsDebitLink, with: "linkDebito")
        
        let container = DependencyContainerMock(featureManagerMock)
        var cardForm = CardForm()
        cardForm.dueDay = 10
        cardForm.creditGrantedByBank = 3500.00
        let sut = HiringResumePresenter(cardForm: cardForm,
                                        bestPurchaseDay: 20,
                                        isUpgradeDebitToCredit: false,
                                        coordinator: coordinatorSpy,
                                        dependencies: container)
        sut.viewController = controllerSpy
        return sut
    }()
    
    func testTextForResume_WhenUpdateScreen_ShouldUpdateResume() {
        let text = "Vencimento da sua fatura\nTodo dia 10\nMelhor dia para compra\nDia 20\nSeu limite de crédito\nR$ 3.500,00\n"
        sut.textForResume()
        XCTAssertEqual(controllerSpy.callTextForResume, 1)
        XCTAssertEqual(controllerSpy.textForResume, text)
    }
    
    func testTextForRules_WhenUpdateScreen_ShouldUpdateRules() {
        let text = "Eu concordo com o contrato de adesão do cartão múltiplo e débito."
        sut.textForRules()
        XCTAssertEqual(controllerSpy.callTextForRules, 1)
        XCTAssertEqual(controllerSpy.textForRules, text)
    }
    
    func testDidNextStep_WhenCreditActivate_ShouldShowScreen() {
        sut.didNextStep(action: .creditActivated)
        XCTAssertEqual(coordinatorSpy.action, [.creditActivated])
    }
    
    func testDidNextStep_WhenCreditWaitingActivate_ShouldShowScreen() {
        sut.didNextStep(action: .creditWaitingActivation)
        XCTAssertEqual(coordinatorSpy.action, [.creditWaitingActivation])
    }
    
    func testAuthenticate_WhenReceivePin_ShouldCallService() {
        sut.authenticate(pin: "1234")
        XCTAssertEqual(controllerSpy.callAuthenticate, 1)
        XCTAssertEqual(controllerSpy.insertedPin, "1234")
    }
    
    func testDismissLoading_WhenReceiveRequestError_ShouldUpdateUI() {
        sut.dismissLoading()
        XCTAssertEqual(controllerSpy.callDismissLoading, 1)
    }
}
