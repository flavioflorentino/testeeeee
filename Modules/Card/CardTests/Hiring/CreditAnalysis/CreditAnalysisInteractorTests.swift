import XCTest
import AnalyticsModule
import Core
@testable import Card

private final class CreditAnalysisPresentingSpy: CreditAnalysisPresenting {
    // MARK: - Variables
    var viewController: CreditAnalysisDisplaying?
    private(set) var callDidNextStepCount = 0
    private(set) var selectedAction: CreditAnalysisAction?
    private(set) var callPresentScreenCount = 0
    private(set) var callOpenFaq = 0
    private(set) var callPresentLoading = 0
    private(set) var callPresentError = 0
    
    func didNextStep(action: CreditAnalysisAction) {
        callDidNextStepCount += 1
        selectedAction = action
    }
    
    func presentScreen() {
        callPresentScreenCount += 1
    }

    func openFAQ() {
        callOpenFaq += 1
    }
    
    func presentLoading(enable: Bool) {
        callPresentLoading += 1
    }
    
    func presentError(_ error: ApiError) {
        callPresentError += 1
    }
}

private final class CreditAnalysisServiceSpy: CreditAnalysisServicing {
    // MARK: - Variables
    var finishExpectedResult: Result<CardForm, ApiError> = .success(CardForm())
    
    func loadRegistrationdata(cardForm: CardForm?, completion: @escaping CompletionGetRegistrationData) {
        completion(finishExpectedResult)
    }
}

final class CreditAnalysisInteractorTests: XCTestCase {
    // MARK: - Variables
    private var presenterSpy = CreditAnalysisPresentingSpy()
    private let serviceSpy = CreditAnalysisServiceSpy()
    
    typealias Dependencies = HasAnalytics
    private let analytics = AnalyticsSpy()
    private lazy var dependencies: Dependencies = DependencyContainerMock(analytics)
    
    private let form = CardForm()
    private lazy var sut = CreditAnalysisInteractor(
        service: serviceSpy,
        presenter: presenterSpy,
        dependencies: dependencies
    )
    
    // MARK: Public Methods
    func testLoadRegistrationData_WhenOpenScreen_ShouldCallPresentScreen() {
        sut.loadRegistrationData()
        XCTAssertEqual(presenterSpy.callPresentLoading, 2)
    }
    
    func testClose_WhenPressCloseButton_ShouldClose() {
        sut.close()
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.selectedAction, .close)
    }
    
    func testOpenFaq_WhenPressFAQButton_ShouldOpenFAQ() {
        sut.openFAQ()
        XCTAssertEqual(presenterSpy.callOpenFaq, 1)
    }
    
    func testSeeResume_WhenPressSeeResume_ShouldOpenResumeScreen() {
        sut.loadRegistrationData()
        sut.seeResume()
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.selectedAction, .seeResume(formModel: CardForm()))
        XCTAssertEqual(analytics.logCalledCount, 1)
        
        let (eventName, eventAction) = getEventProperties()
        XCTAssertEqual(eventName, "PicPay Card Request - Sign Up - Personal Info Analysis")
        XCTAssertEqual(eventAction, "act-view-summary")
    }
    
    func test1() {
        sut.confirm()
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.selectedAction, .close)
        XCTAssertEqual(analytics.logCalledCount, 1)
        
        let (eventName, eventAction) = getEventProperties()
        XCTAssertEqual(eventName, "PicPay Card Request - Sign Up - Personal Info Analysis")
        XCTAssertEqual(eventAction, "act-analysis-close")
    }
    
    private func getEventProperties() -> (String?, String?) {
        let event = analytics.events.last
        let action = event?.properties["action"] as? String
        return (event?.name, action)
    }
}

