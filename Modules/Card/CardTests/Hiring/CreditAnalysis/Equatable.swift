@testable import Card

extension CreditAnalysisAction: Equatable {
    public static func == (lhs: CreditAnalysisAction, rhs: CreditAnalysisAction) -> Bool {
        switch (lhs, rhs) {
        case (.close, .close):
            return true
        case (.faq, .faq):
            return true
        case (.seeResume(let lhsForm), .seeResume(let rhsForm)):
            return lhsForm == rhsForm
        default:
            return false
        }
    }
}


