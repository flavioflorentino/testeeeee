import FeatureFlag
import UI
import XCTest
@testable import Card

private final class CreditAnalysisViewControllerSpy: CreditAnalysisDisplaying {
    // MARK: - Variables
    private(set) var callDisplayLoadDataCount = 0
    private(set) var callDisplayLoadingCount = 0
    private(set) var callDisplayErrorCount = 0
    
    func displayLoadData(description: String, faq: NSMutableAttributedString) {
        callDisplayLoadDataCount += 1
    }
    
    func displayLoading(enable: Bool) {
        callDisplayLoadingCount += 1
    }
    
    func displayError(_ model: StatefulErrorViewModel) {
        callDisplayErrorCount += 1
    }
}

private final class CreditAnalysisCoordinatorSpy: CreditAnalysisCoordinating {
    var viewController: UIViewController?
    
    private(set) var callActionSelectedCount = 0
    private(set) var actionSelected: CreditAnalysisAction?

    func perform(action: CreditAnalysisAction) {
        callActionSelectedCount += 1
        actionSelected = action
    }
}

final class CreditAnalysisPresenterTests: XCTest {
    // MARK: - Variables
    private var viewControllerSpy = CreditAnalysisViewControllerSpy()
    private let coordinatorSpy = CreditAnalysisCoordinatorSpy()
    private let featureManagerMock = FeatureManagerMock()
    private lazy var dependencies = DependencyContainerMock(featureManagerMock)
    
    private lazy var sut: CreditAnalysisPresenter = {
        let sut = CreditAnalysisPresenter(coordinator: coordinatorSpy, dependencies: dependencies)
        sut.viewController = viewControllerSpy
        return sut
    }()
    
    // MARK: - Tests
    func testPresentScreen_WhenOpenScreen_ShouldUpdateScreen() {
        sut.presentScreen()
        XCTAssertEqual(viewControllerSpy.callDisplayLoadDataCount, 1)
    }
    
    func testDidNextStep_WhenPressCloseButton_ShouldClose() {
        sut.didNextStep(action: .close)
        XCTAssertEqual(coordinatorSpy.actionSelected, .close)
        XCTAssertEqual(coordinatorSpy.callActionSelectedCount, 1)
    }
    
    func testDidNextStep_WhenPressSeeResumeButton_ShouldOpenResumeScreen() {
        let form = CardForm()
        sut.didNextStep(action: .seeResume(formModel: form))
        XCTAssertEqual(coordinatorSpy.actionSelected, .seeResume(formModel: form))
        XCTAssertEqual(coordinatorSpy.callActionSelectedCount, 1)
    }
}
