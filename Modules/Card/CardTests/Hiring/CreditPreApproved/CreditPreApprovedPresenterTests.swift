import XCTest
import UI
@testable import Card

final class CreditPreApprovedCoordinatorSpy: CreditPreApprovedCoordinating {
    var action: CreditPreApprovedAction?
    var performCalledCount = 0

    
    func perform(action: CreditPreApprovedAction) {
        self.action = action
        performCalledCount += 1
    }
}

final class CreditPreApprovedViewControllerSpy: CreditPreApprovedDisplaying {
    var title: String?
    var description: String?
    var displayErrorCalledCount = 0
    
    func displayScreenData(title: String, description: String) {
        self.title = title
        self.description = description
    }
    
    func displayError(model: StatefulViewModeling) {
        displayErrorCalledCount += 1
    }
}

final class CreditPreApprovedPresenterTests: XCTestCase {
    private let coordinatorSpy = CreditPreApprovedCoordinatorSpy()
    private let controllerSpy = CreditPreApprovedViewControllerSpy()
    
    private lazy var sut: CreditPreApprovedPresenter = {
        let presenter = CreditPreApprovedPresenter(coordinator: coordinatorSpy, dependencies: DependencyContainerMock())
        presenter.viewController = controllerSpy
        return presenter
    }()
    
    private func testPresentNextStep_ShouldCallCoordinatorConfirmMethod() {
        sut.confirm()
        XCTAssertEqual(coordinatorSpy.performCalledCount, 1)
    }
    
    private func testPresentShowScreenData_ShouldShowScreenData() {
        let currencyString = NumberFormatter.toCurrencyString(with: 2000.0) ?? "R$ 0,00"

        var cardForm = CardForm()
        cardForm.creditGrantedByBank = 2000.0
        cardForm.name = "Crédito"
        sut.presentScreenData(with: cardForm)
        XCTAssertEqual(controllerSpy.title, Strings.CreditPreApproved.title)
        XCTAssertEqual(controllerSpy.description, Strings.CreditPreApproved.description("Crédito", currencyString))
    }
    
    private func testPresentShowScreenData_ShouldShowError() {
        sut.presentError(.malformedRequest("Error"))
        XCTAssertEqual(controllerSpy.displayErrorCalledCount, 1)
    }
}
