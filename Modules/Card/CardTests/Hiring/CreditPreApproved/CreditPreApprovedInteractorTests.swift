import XCTest
import Core
@testable import Card

final class CreditPreApprovedServiceMock: CreditPreApprovedServicing {
    var getCreditFormExpectedResult: Result<CardForm, ApiError> = .failure(ApiError.malformedRequest("Erro"))
    
    func getCreditForm(_ completion: @escaping (Result<CardForm, ApiError>) -> Void) {
            completion(getCreditFormExpectedResult)
    }
}

final class CreditPreApprovedPresenterSpy: CreditPreApprovedPresenting {
    var viewController: CreditPreApprovedDisplaying?
    
    var didShowErrorCount = 0
    var cardForm: CardForm?
    var didLoadCount = 0
    var didNextStepCount = 0
    var didConfirmCount = 0
    
    func presentNextStep(action: CreditPreApprovedAction) {
        didNextStepCount += 1
    }
    
    func presentError(_ error: ApiError) {
        didShowErrorCount += 1
    }
    
    func presentScreenData(with cardForm: CardForm) {
        self.cardForm = cardForm
    }

    func confirm() {
        didConfirmCount += 1
    }
}

final class CreditPreApprovedInteractorTests: XCTestCase {
    private let serviceMock = CreditPreApprovedServiceMock()
    private let presenterSpy =  CreditPreApprovedPresenterSpy()
    private lazy var sut = CreditPreApprovedInteractor(service: serviceMock, presenter: presenterSpy, dependencies: DependencyContainerMock())
    
    private func testLoadData_WhenServiceReceiveAnError_ShouldCallShowErrorl() {
        serviceMock.getCreditFormExpectedResult = .failure(ApiError.malformedRequest("Random error"))
        sut.loadData()
        XCTAssertEqual(presenterSpy.didShowErrorCount, 1)
    }
    
    private func testLoadData_WhenServiceReceiveASuccess_ShouldCallPresenterWithTheRightValues() {
        var cardForm = CardForm()
        cardForm.creditGrantedByBank = 2000.0
        cardForm.name = "Crédito"
        serviceMock.getCreditFormExpectedResult = .success(cardForm)
        sut.loadData()
        XCTAssertEqual(presenterSpy.didShowErrorCount, 0)
        XCTAssertEqual(presenterSpy.cardForm?.creditGrantedByBank, 2000.0)
        XCTAssertEqual(presenterSpy.cardForm?.name, "Crédito")
    }
    
    private func testConfirm_ShouldConfirmNextStep() {
        sut.confirm()
        XCTAssertEqual(presenterSpy.didNextStepCount, 1)
    }
    
    private func testClose_ShouldCloseNextStep() {
        sut.close()
        XCTAssertEqual(presenterSpy.didNextStepCount, 1)
    }
}
