import XCTest
@testable import Card

private final class HiringWaitingResumePresenterSpy: HiringWaitingResumePresenting {
    var viewController: HiringWaitingResumeDisplaying?
    
    var action: HiringWaitingResumeAction?
    var didPresentActionCount = 0
    
    var cardForm: CardForm?
    var didPresentRootViewDataCount = 0
    
    func presentRootViewData(cardForm: CardForm) {
        didPresentRootViewDataCount += 1
        self.cardForm = cardForm
    }
    
    func presentNextStep(action: HiringWaitingResumeAction) {
        didPresentActionCount += 1
        self.action = action
    }
}

final class HiringWaitingResumeInteractorTests: XCTestCase {
    private let presenterSpy = HiringWaitingResumePresenterSpy()
    
    private lazy var sut = HiringWaitingResumeInteractor(presenter: presenterSpy, cardForm: .init())
    
    func testConfirm_ShouldPresentAction() {
        sut.confirm()
        XCTAssertEqual(presenterSpy.action, .continue)
        XCTAssertEqual(presenterSpy.didPresentActionCount, 1)
    }
    
    func testLoadData_ShouldPresentRootViewData() {
        sut.loadData()
        XCTAssertEqual(presenterSpy.cardForm, .init())
        XCTAssertEqual(presenterSpy.didPresentRootViewDataCount, 1)
    }
}
