import XCTest
import UI
@testable import Card

private final class HiringWaitingResumeCoordinatorSpy: HiringWaitingResumeCoordinating {
    var viewController: UIViewController?
    
    var action: HiringWaitingResumeAction?
    var didCallActionCount = 0
    
    func perform(action: HiringWaitingResumeAction) {
        didCallActionCount += 1
        self.action = action
    }
}

private final class HiringWaitingResumeViewControllerSpy: HiringWaitingResumeDisplaying {
    var data = [DetailsViewRow]()
    var didDisplayRootViewcount = 0
    
    func displayRootView(data: [DetailsViewRow]) {
        didDisplayRootViewcount += 1
        self.data = data
    }
}

final class HiringWaitingResumePresenterTests: XCTestCase {
    
    private let coordinatorSpy = HiringWaitingResumeCoordinatorSpy()
    private let controllerSpy = HiringWaitingResumeViewControllerSpy()
    private lazy var sut = HiringWaitingResumePresenter(coordinator: coordinatorSpy)
    
    func testPresentNextStep_ShouldContinue() {
        sut.presentNextStep(action: .continue)
        XCTAssertEqual(coordinatorSpy.action, .continue)
        XCTAssertEqual(coordinatorSpy.didCallActionCount, 1)
    }
    
    func testPresentRootViewData_ShouldPresent() {
        sut.viewController = controllerSpy
        sut.presentRootViewData(cardForm: .init())
        XCTAssertEqual(controllerSpy.didDisplayRootViewcount, 1)
        XCTAssertEqual(controllerSpy.data.count, 27)
    }
}
