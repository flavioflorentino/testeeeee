import AnalyticsModule
import FeatureFlag
import XCTest
@testable import Card

private final class UpgradeDebitSuccessPresentingSpy: UpgradeDebitSuccessPresenting {
    // MARK: - Variables
    var viewController: UpgradeDebitSuccessDisplay?
    
    private(set) var action: UpgradeDebitSuccessAction?
    private(set) var callAction = 0
    private(set) var callSetupInitialInfo = 0
    private(set) var callOpenZendesk = 0
    
    // MARK: - Public Methods
    func didNextStep(action: UpgradeDebitSuccessAction) {
        self.action = action
        callAction += 1
    }
    
    func showInitialSetup() {
        callSetupInitialInfo += 1
    }
    
    func openZendesk() {
        callOpenZendesk += 1
    }
}

final class UpgradeDebitSuccessViewModelTests: XCTestCase {
    // MARK: - Variables
    typealias Dependencies = HasAnalytics & HasFeatureManager
    private let analytics = AnalyticsSpy()
    private let featureManagerMock = FeatureManagerMock()
    private let presenterSpy = UpgradeDebitSuccessPresentingSpy()
    private lazy var container: Dependencies = DependencyContainerMock(analytics, featureManagerMock)
    private lazy var sut = UpgradeDebitSuccessInteractor(presenter: presenterSpy, dependencies: container)

    
    // MARK: - Public Methods
    func testConfirmAction_WhenContinueButtonIsPressed_ShouldSendEvent() {
        sut.confirm()
        let event = analytics.events.last
        let action = event?.properties["action"] as? String
        XCTAssertEqual(analytics.logCalledCount, 1)
        XCTAssertEqual(event?.name, "PicPay Card - Upgrade - Card Activated Message")
        XCTAssertEqual(action, "act-go-wallet")
    }
    
    func testFAQ_WhenHelpButtonIsPressed_ShouldSendEvent() {
        sut.openFAQ()
        let event = analytics.events.last
        let action = event?.properties["action"] as? String
        XCTAssertEqual(analytics.logCalledCount, 1)
        XCTAssertEqual(event?.name, "PicPay Card - Upgrade - Card Activated Message")
        XCTAssertEqual(action, "act-help-center")
    }
    
    func testSetupInitialInfo_WhenHasLoadScreen_ShouldUpdateLayout() {
        sut.showInitialSetup()
        XCTAssertEqual(presenterSpy.callSetupInitialInfo, 1)
    }
    
    func testClose_WhenPressCloseButton_ShouldCloseScreen() {
        sut.close()
        XCTAssertEqual(presenterSpy.callAction, 1)
    }
    
    func testAllowZendeskKey_WhenFeatureFlagIsFalse_ShouldOpenZendesk() {
        sut.openFAQ()
        XCTAssertEqual(presenterSpy.callOpenZendesk, 1)
    }
    
    func testConfirmAction_WhenContinueButtonIsPressed_ShouldComunicateCoordinator() {
        sut.confirm()
        XCTAssertEqual(presenterSpy.action, .confirm)
    }
}
