import XCTest
@testable import Card
import UI

private class UpgradeDebitSuccessCoordinatingSpy: UpgradeDebitSuccessCoordinating {
    var viewController: UIViewController?

    //MARK: - perform
    private(set) var callsCount = 0

    func perform(action: UpgradeDebitSuccessAction) {
        callsCount += 1
    }
}

private class UpgradeDebitSuccessDisplaySpy: UpgradeDebitSuccessDisplay {
    //MARK: - displayContentInfo
    private(set) var displayInitialSetupCallsCount = 0
    private(set) var helpMessage = NSMutableAttributedString()

    func displayInitialSetup(helpMessage: NSMutableAttributedString) {
        self.helpMessage = helpMessage
        displayInitialSetupCallsCount += 1
    }
}

public class UpgradeDebitSuccessPresenterTests: XCTestCase {
    // MARK: - Variables
    private var viewControllerSpy = UpgradeDebitSuccessDisplaySpy()
    private let coordinatorSpy = UpgradeDebitSuccessCoordinatingSpy()
    private lazy var sut: UpgradeDebitSuccessPresenter = {
        let sut = UpgradeDebitSuccessPresenter(coordinator: coordinatorSpy)
        sut.viewController = viewControllerSpy
        return sut
    }()

    // MARK: - Tests

    func testDidNextStep_WhenConfirmIsCalled_ShouldCallCoordinator() {
        sut.didNextStep(action: .confirm)
        XCTAssertEqual(coordinatorSpy.callsCount, 1)
    }
    
    func testDidNextStep_WhenCloseIsCalled_ShouldCallCoordinator() {
        sut.didNextStep(action: .close)
        XCTAssertEqual(coordinatorSpy.callsCount, 1)
    }
    
    func testDidNextStep_WhenOpenZendeskIsCalled_ShouldCallCoordinator() {
        sut.didNextStep(action: .openZendesk)
        XCTAssertEqual(coordinatorSpy.callsCount, 1)
    }
    
    func testviewDidLoad_WhenViewIsCalled_ShouldCreateHelpMessageInViewController() {
        sut.showInitialSetup()
        XCTAssertEqual(viewControllerSpy.displayInitialSetupCallsCount,1)
        let helpMessage = viewControllerSpy.helpMessage
        XCTAssertEqual(helpMessage.string, "Dúvidas?\nConfira nossa Central de Ajuda.")
        validateCurrentLink(helpMessage, textSubstring: "Central de Ajuda", expectedURL: URL(string: "http://app.picpay.com/helpcenter?query=onde-posso-usar-o-meu-picpay-card"))
    }

    private func validateCurrentLink(_ attributed: NSAttributedString, textSubstring: String? = nil, expectedURL: URL?) {
        let range = NSString(string: attributed.string).range(of: textSubstring ?? attributed.string)
        let attributedSubstring = attributed.attributedSubstring(from: range)
        let currentURL = attributedSubstring.attributes(at: 0, effectiveRange: nil)[.link] as! URL
        XCTAssertEqual(currentURL, expectedURL)
    }
}
