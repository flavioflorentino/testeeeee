import AnalyticsModule
import FeatureFlag
import XCTest
@testable import Card

private final class HiringWaitingActivationPresentingSpy: HiringWaitingActivationPresenting {
    // MARK: - Variables
    var viewController: HiringWaitingActivationDisplaying?
    private(set) var action = [HiringWaitingActivationAction]()
    private(set) var callActionCount = 0
    private(set) var callActionPresentScreen = 0
    
    // MARK: - Public Methods
    func didNextStep(action: HiringWaitingActivationAction) {
        self.action.append(action)
        callActionCount += 1
    }
    
    func presentScreen(isUpgrade: Bool) {
        callActionPresentScreen += 1
    }
}

final class HiringWaitingActivationInteractorTests: XCTestCase {
    // MARK: - Variables
    private var presenterSpy = HiringWaitingActivationPresentingSpy()
    
    typealias Dependencies = HasAnalytics
    private let analytics = AnalyticsSpy()
    private lazy var dependencies: Dependencies = DependencyContainerMock(analytics)
    
    private lazy var sut = HiringWaitingActivationInteractor(presenter: presenterSpy, dependencies: dependencies, isUpgrade: false)
    
    // MARK: - Public Methods
    func testClose_WhenPressCloseButton_ShouldSendAction() {
        sut.pressClose()
        XCTAssertEqual(presenterSpy.callActionCount, 1)
        XCTAssertEqual(presenterSpy.action, [.close])
    }
    
    func testConfirm_WhenPressConfirmButton_ShouldSendEvent() {
        sut.pressConfirm()
        let expectedEvent = HiringWaitingActivationEvent.didSelectConfirm.event()
        
        XCTAssertEqual(presenterSpy.callActionCount, 1)
        XCTAssertEqual(presenterSpy.action, [.confirm])
        XCTAssertEqual(analytics.logCalledCount, 1)
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testLoadScreen_WhenDidLoad_ShouldCallPresenter() {
        sut.loadScreen()
        XCTAssertEqual(presenterSpy.callActionPresentScreen, 1)
    }
    
    func testTrackViewScreen_WhenCreditViewDidAppear_ShouldSendCreditEvent() {
        sut.trackScreenView()
        let expectedEvent = HiringWaitingActivationEvent.didViewCreditScreen.event()
        
        XCTAssertEqual(analytics.logCalledCount, 1)
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testTrackViewScreen_WhenUpgradeViewDidAppear_ShouldSendUpgradeEvent() {
        sut = HiringWaitingActivationInteractor(presenter: presenterSpy, dependencies: dependencies, isUpgrade: true)
        sut.trackScreenView()
        let expectedEvent = HiringWaitingActivationEvent.didViewUpgradeScreen.event()
        
        XCTAssertEqual(analytics.logCalledCount, 1)
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
}
