import XCTest
@testable import Card

private final class HiringWaitingActivationCoordinatorSpy: HiringWaitingActivationCoordinating {
    // MARK: - Variables
    var viewController: UIViewController?

    private(set) var actionSelected: HiringWaitingActivationAction?
    private(set) var callAction = 0

    // MARK: - Public Methods
    func perform(action: HiringWaitingActivationAction) {
        actionSelected = action
        callAction += 1
    }
}

private final class HiringWaitingActivationViewControllerSpy: HiringWaitingActivationDisplaying {
    // MARK: - Variables
    private(set) var title = ""
    private(set) var description = ""

    // MARK: - Public Methods
    func displayScreen(image: UIImage, title: String, description: String) {
        self.title = title
        self.description = description
    }
}

final class HiringWaitingActivationPresenterTests: XCTestCase {
    // MARK: - Variables
    private let coordinatorSpy = HiringWaitingActivationCoordinatorSpy()
    private let viewControllerSpy = HiringWaitingActivationViewControllerSpy()

    private lazy var sut: HiringWaitingActivationPresenter = {
        let sut = HiringWaitingActivationPresenter(coordinator: coordinatorSpy)
        sut.viewController = viewControllerSpy
        return sut
    }()

    // MARK: - Tests
    func testDidNextStep_WhenPressCloseButton_ShouldCloseScreen() {
        sut.didNextStep(action: .close)
        XCTAssertEqual(coordinatorSpy.actionSelected, .close)
        XCTAssertEqual(coordinatorSpy.callAction, 1)
    }
    
    func testPresentScreen_WhenIsUpgrade_ShouldUpdateUI() {
        sut.presentScreen(isUpgrade: true)
        XCTAssertEqual(viewControllerSpy.title, "Agora é com a gente!")
        XCTAssertEqual(viewControllerSpy.description, "Estamos finalizando o seu pedido e assim que estiver tudo pronto nós vamos te enviar uma notificação aqui pelo app e um e-mail.\n\nVocê nem precisa esperar um novo cartão, tá? Vamos ativar o crédito nessa mesma via que você já tem e que funciona como débito.")
    }
    
    func testPresentScreen_WhenIsNotUpgrade_ShouldUpdateUI() {
        sut.presentScreen(isUpgrade: false)
        XCTAssertEqual(viewControllerSpy.title, "Agora é com a gente!")
        XCTAssertEqual(viewControllerSpy.description, "Estamos preparando o seu PicPay Card e assim que estiver pronto nós vamos te enviar uma notificação aqui pelo app e um e-mail.")
    }
}
