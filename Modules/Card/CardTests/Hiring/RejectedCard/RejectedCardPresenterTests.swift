import XCTest
@testable import Card

final class RejectedCardCoordinatorSpy: RejectedCardCoordinating {
    var viewController: UIViewController?
    
    var action: RejectedCardAction?
    
    func perform(action: RejectedCardAction) {
        self.action = action
    }
}

final class RejectedCardPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = RejectedCardCoordinatorSpy()
    private lazy var sut = RejectedCardPresenter(coordinator: coordinatorSpy)
    
    func testPresentNextStep_ShouldPresent() {
        sut.presentNextStep(action: .close)
        XCTAssertEqual(coordinatorSpy.action, .close)
    }
}
