import XCTest
import AnalyticsModule
@testable import Card

final class RejectedCardPresenterSpy: RejectedCardPresenting {
    var didPresentNextStepCount = 0
    var action: RejectedCardAction?
    
    func presentNextStep(action: RejectedCardAction) {
        self.action = action
        didPresentNextStepCount += 1
    }
}

final class RejectedCardInteractorTests: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    private let presenterSpy = RejectedCardPresenterSpy()
    private lazy var sut = RejectedCardInteractor(presenter: presenterSpy, dependencies: DependencyContainerMock(analyticsSpy))
    
    func testConfirm_ShouldClose() {
        sut.confirm()
        XCTAssertEqual(presenterSpy.action, .close)
        XCTAssertEqual(presenterSpy.didPresentNextStepCount, 1)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }
}
