import XCTest
import UI
import Core
@testable import Card

private final class CardTrackingLoadingViewControllerSpy: CardTrackingLoadingDisplay {
    private(set) var errorData: StatefulFeedbackViewModel?
    private(set) var callDisplayLoadState = 0
    
    func displayErrorState(with errorData: StatefulFeedbackViewModel) {
        self.errorData = errorData
    }
    
    func displayLoadState() {
        callDisplayLoadState += 1
    }
}

private final class CardTrackingLoadingCoordinatorSpy: CardTrackingLoadingCoordinating {
    var viewController: UIViewController?
    private(set) var callUpdate = 0
    private(set) var callClose = 0
    
    func perform(action: CardTrackingLoadingAction) {
        switch action {
        case .update:
            callUpdate += 1
        case .close:
            callClose += 1
        }
    }
}

final class CardTrackingLoadingPresenterTests: XCTestCase {
    private var viewControllerSpy = CardTrackingLoadingViewControllerSpy()
    private let coordinatorSpy = CardTrackingLoadingCoordinatorSpy()
    
    private lazy var sut: CardTrackingLoadingPresenter = {
        let sut = CardTrackingLoadingPresenter(coordinator: coordinatorSpy)
        sut.viewController = viewControllerSpy
        return sut
    }()
    
    private func getTrackingData(in fileName: String) -> CardTrackingHistory {
        let trackingData = try! MockCodable<CardTrackingHistory>().loadCodableObject(
            resource: fileName,
            typeDecoder: .useDefaultKeys)
        return trackingData
    }
    
    func testError_WhenReturnConnectionError_ShouldDisplayConnectionErrorData() {
        let apiError: ApiError = .connectionFailure
        let expectedFeedBackViewModel = StatefulFeedbackViewModel(apiError)
        sut.showError(apiError)
        XCTAssertEqual(expectedFeedBackViewModel, viewControllerSpy.errorData)
    }
    
    func testError_WhenReturnServerError_ShouldDisplayServerErrorData() {
        let apiError: ApiError = .serverError
        let expectedFeedBackViewModel = StatefulFeedbackViewModel(apiError)
        sut.showError(apiError)
        XCTAssertEqual(expectedFeedBackViewModel, viewControllerSpy.errorData)
    }
    
    func testDidNextStep_WhenServerReturnResult_ShouldPerformCoordinator() {
        let trackingData = getTrackingData(in: "dataTrackingCorrect")
        sut.didNextStep(action: .update(trackingData: trackingData))
        XCTAssertEqual(coordinatorSpy.callUpdate, 1)
    }
    
    func testDidClose_WhenUserClose_ShouldPerformCoordinator() {
        sut.didNextStep(action: .close)
        XCTAssertEqual(coordinatorSpy.callClose, 1)
    }
}
