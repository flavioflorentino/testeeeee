import AnalyticsModule
import Core
import XCTest
@testable import Card

private final class CardTrackingLoadingPresentingSpy: CardTrackingLoadingPresenting {
    // MARK: - Variables
    var viewController: CardTrackingLoadingDisplay?

    private(set) var callClose = 0
    private(set) var callShowError = 0
    private(set) var callUpdate = 0
    private(set) var callPresentNextStep = 0
    private(set) var callPresentLoadState = 0
    
    // MARK: - Public Methods
    func didNextStep(action: CardTrackingLoadingAction) {
        switch action {
        case .update:
            callUpdate += 1
        case .close:
            callClose += 1
        }
    }
    
    func showError(_ apiError: ApiError) {
        callShowError += 1
    }

    func presentLoadState() {
        callPresentLoadState += 1
    }
}

private final class CardTrackingLoadingServiceSpy: CardTrackingLoadingServicing {
    // MARK: - Variables
    var isSuccess = false
    var trackingHistory: CardTrackingHistory?
    
    // MARK: - Public Methods
    func getTrackingData(completion: @escaping CompletionTracking) {
        if isSuccess {
            guard let trackingHistory = trackingHistory else {
                return
            }
            
            completion(Result.success(trackingHistory))
        }
        else {
            completion(.failure(.serverError))
        }
    }
}

final class CardTrackingLoadingViewModelTests: XCTestCase {
    // MARK: - Variables
    private var presenterSpy = CardTrackingLoadingPresentingSpy()
    private let serviceSpy = CardTrackingLoadingServiceSpy()
    private let analyticsSpy = AnalyticsSpy()
    private lazy var container = DependencyContainerMock(analyticsSpy)
    
    private func getTrackingData(in fileName: String) -> CardTrackingHistory {
        let trackingData = try! MockCodable<CardTrackingHistory>().loadCodableObject(
            resource: fileName,
            typeDecoder: .useDefaultKeys)
        return trackingData
    }
    
    private lazy var sut = CardTrackingLoadingInteractor(
        service: serviceSpy,
        presenter: presenterSpy,
        container: container
    )
    
    // MARK: - Public Methods
    func testUpdateData_WhenUserDidLoadWithCorrectData_ShouldUpdatePresenter() {
        serviceSpy.isSuccess = true
        serviceSpy.trackingHistory = getTrackingData(in: "dataTrackingCorrect")
        sut.updateData()
        XCTAssertEqual(presenterSpy.callUpdate, 1)
    }
    
    func testEndFlow_WhenUserEndFlow_ShouldCloseFlowInPresenter() {
        sut.endFlow()
        XCTAssertEqual(presenterSpy.callClose, 1)
    }
    
    func testUpdateData_WhenUserDidLoadWithIncorrectData_ShouldShowErrorInPresenter() {
        serviceSpy.isSuccess = false
        serviceSpy.trackingHistory = getTrackingData(in: "dataTrackingCorrect")
        sut.updateData()
        XCTAssertEqual(presenterSpy.callShowError, 1)
    }
    
    func testTryAgain_WhenInternetIsDown_ShouldSendErrorAnalytics() {
        serviceSpy.isSuccess = false
        serviceSpy.trackingHistory = getTrackingData(in: "dataTrackingCorrect")
        sut.didTryAgain(with: .connection)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(analyticsSpy.event?.name, CardTrackingEvent.didInternetError.event().name)
    }
    
    func testTryAgain_WhenServerDidntLoad_ShouldSendErrorAnalytics() {
        serviceSpy.isSuccess = false
        serviceSpy.trackingHistory = getTrackingData(in: "dataTrackingCorrect")
        sut.didTryAgain(with: .general)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(analyticsSpy.event?.name, CardTrackingEvent.didServerError.event().name)
    }
    
    func testPresentLoadState_WhenRequestIsTriggered_ShouldPresentaLoadingState() {
        sut.updateData()
        XCTAssertEqual(presenterSpy.callPresentLoadState, 1)
    }
    
    func testDismiss_WhenUserClose_ShouldCloseFlowInPresenter() {
        sut.dismiss()
        XCTAssertEqual(presenterSpy.callClose, 1)
    }
}
