import XCTest
@testable import Card

private final class CardArrivedViewControllerSpy: CardArrivedDisplay {
    // MARK: - Variables
    private(set) var callInitialSetup = 0
    
    // MARK: - Public Methods
    func displayInitialSetup(subTitle: NSMutableAttributedString, helpMessage: NSMutableAttributedString) {
        callInitialSetup += 1
    }
}

private final class CardArrivedCoordinatorSpy: CardArrivedCoordinating {
    // MARK: - Variables
    var viewController: UIViewController?
    
    private(set) var actionSelected: CardArrivedAction?
    private(set) var callAction = 0
    
    // MARK: - Public Methods
    func perform(action: CardArrivedAction) {
        actionSelected = action
        callAction += 1
    }
}

final class CardArrivedPresenterTests: XCTestCase {
    // MARK: - Variables
    private var viewControllerSpy = CardArrivedViewControllerSpy()
    private let coordinatorSpy = CardArrivedCoordinatorSpy()
        
    private lazy var sut: CardArrivedPresenter = {
        let sut = CardArrivedPresenter(coordinator: coordinatorSpy)
        sut.viewController = viewControllerSpy
        return sut
    }()

    // MARK: - Tests
    func testSetupInitialInfo_WhenLoadScreen_ShouldPresentInitialInfo() {
        sut.setupInitialInfo()
        XCTAssertEqual(viewControllerSpy.callInitialSetup, 1)
    }
    
    func testDidNextStep_WhenPressCloseButton_ShouldCloseScreen() {
        sut.didNextStep(action: .close)
        XCTAssertEqual(coordinatorSpy.actionSelected, CardArrivedAction.close)
        XCTAssertEqual(coordinatorSpy.callAction, 1)
    }
    
    func testDidNextStep_WhenPressContinueButton_ShouldChangeScreen() {
        sut.didNextStep(action: .confirm)
        XCTAssertEqual(coordinatorSpy.actionSelected, CardArrivedAction.confirm)
        XCTAssertEqual(coordinatorSpy.callAction, 1)
    }
    
    func testDidNextStep_WhenPressFAQLink_ShouldOpenHelpCenter() {
        guard let url = URL(string: "https://app.picpay.com/helpcenter") else {
            XCTFail()
            return
        }
        sut.didNextStep(action: .openHelpCenter(url: url))
        XCTAssertEqual(coordinatorSpy.actionSelected, CardArrivedAction.openHelpCenter(url: url))
        XCTAssertEqual(coordinatorSpy.callAction, 1)
    }
}

extension CardArrivedAction: Equatable {
    public static func == (lhs: CardArrivedAction, rhs: CardArrivedAction) -> Bool {
        switch (lhs, rhs) {
        case (.confirm, .confirm):
            return true
        case (.close, .close):
            return true
        case (.openHelpCenter, .openHelpCenter):
            return true
        default:
            return false
        }
    }
}
