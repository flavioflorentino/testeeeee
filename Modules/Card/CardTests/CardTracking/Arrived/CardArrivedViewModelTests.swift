import AnalyticsModule
import FeatureFlag
import XCTest
@testable import Card

private final class CardArrivedPresentingSpy: CardArrivedPresenting {
    // MARK: - Variables
    var viewController: CardArrivedDisplay?
    
    private(set) var actions: [CardArrivedAction] = []
    private(set) var callAction = 0
    private(set) var callSetupInitialInfo = 0
    private(set) var callHelpCenter = 0
    
    // MARK: - Public Methods
    func didNextStep(action: CardArrivedAction) {
        actions.append(action)
        callAction += 1
    }
    
    func setupInitialInfo() {
        callSetupInitialInfo += 1
    }
}

final class CardArrivedViewModelTests: XCTestCase {
    // MARK: - Variables
    typealias Dependencies = HasAnalytics & HasFeatureManager
    private let analytics = AnalyticsSpy()
    private let featureManagerMock = FeatureManagerMock()
    private let presenterSpy = CardArrivedPresentingSpy()
    private lazy var container: Dependencies = DependencyContainerMock(analytics, featureManagerMock)
    private lazy var sut = CardArrivedViewModel(presenter: presenterSpy, dependencies: container, cardType: .multiple)

    // MARK: - Public Methods
    func testSetupInitialInfo_WhenHasLoadScreen_ShouldUpdateLayout() {
        sut.setupInitialInfo()
        XCTAssertEqual(presenterSpy.callSetupInitialInfo, 1)
    }
    
    func testClose_WhenPressCloseButton_ShouldCloseScreen() {
        sut.close()
        XCTAssertEqual(presenterSpy.actions, [CardArrivedAction.close])
    }
    
    func testConfirm_WhenPressContinueButton_ShouldChangeScreen() {
        sut.confirm()
        XCTAssertEqual(presenterSpy.actions, [CardArrivedAction.confirm])
        
        let (eventName, eventAction) = getEventProperties()
        XCTAssertEqual(eventName, "PicPay Card - Card Tracking - Unblock Card")
        XCTAssertEqual(eventAction, "act-unblock-card")
        XCTAssertEqual(analytics.logCalledCount, 1)
    }
    
    func testEventFaq_WhenPressHelpButton_ShouldOpenExternalURL() throws {
        let faqArticleActivateCard = "faqArticleActivateCard"
        featureManagerMock.override(key: .articleActivateCard, with: faqArticleActivateCard)
        let urlExpected = try XCTUnwrap(URL(string: faqArticleActivateCard))
        sut.openFAQ()
        let (eventName, eventAction) = getEventProperties()
        XCTAssertEqual(eventName, "PicPay Card - Card Tracking - Unblock Card")
        XCTAssertEqual(eventAction, "act-help-center")
        XCTAssertEqual(analytics.logCalledCount, 1)
        XCTAssertEqual(presenterSpy.actions, [.openHelpCenter(url: urlExpected)])

    }
    
    private func getEventProperties() -> (String?, String?) {
        let event = analytics.events.last
        let action = event?.properties["action"] as? String
        return (event?.name, action)
    }
}
