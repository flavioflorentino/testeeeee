import Foundation
@testable import Card
import XCTest
import UI
import AnalyticsModule
import FeatureFlag
import Core



final class CardTrackingPresentingSpy: CardTrackingPresenting {
    var viewController: CardTrackingDisplay?
    private(set) var actions: [CardTrackingAction] = []
    private(set) var cardTrackingHistoryReceived: CardTrackingHistory?
    private(set) var isArrivedReceived: Bool?
    private(set) var setupHelpMessageCount = 0
    private(set) var updateHistoryCount = 0
    private(set) var setupEstimatedTimeCount = 0
    private(set) var setupErrorButtonCount = 0
    private(set) var setupReceivedCardButton = 0
   
    func didNextStep(action: CardTrackingAction) {
        actions.append(action)
    }
    
    func updateHistory(cardTrackingHistory: CardTrackingHistory, isArrived: Bool, cardType: CardTrackingType) {
        cardTrackingHistoryReceived = cardTrackingHistory
        isArrivedReceived = isArrived
        updateHistoryCount += 1
    }
    
    func setupEstimatedTime(cardTrackingHistory: CardTrackingHistory) {
        cardTrackingHistoryReceived = cardTrackingHistory
        setupEstimatedTimeCount += 1
    }
    
    func setupErrorButton(cardTrackingHistory: CardTrackingHistory) {
        cardTrackingHistoryReceived = cardTrackingHistory
        setupErrorButtonCount += 1
    }
    
    func setupReceivedCardButton(cardTrackingHistory: CardTrackingHistory) {
        cardTrackingHistoryReceived = cardTrackingHistory
        setupReceivedCardButton += 1
    }
    
    func setupHelpMessage() {
        setupHelpMessageCount += 1
    }

}

final class CardTrackingViewModelTest: XCTestCase {
    typealias Dependencies = HasFeatureManager & HasAnalytics
    private lazy var  featureManagerMock = FeatureManagerMock()
    private lazy var analytics = AnalyticsSpy()
    private lazy var dependencies: Dependencies = DependencyContainerMock(analytics, featureManagerMock)
    
    private lazy var presentSpy = CardTrackingPresentingSpy()
    private lazy var fileName = "dataTrackingCorrect"
    private lazy var sut: CardTrackingViewModel? = {
       guard let model = try? MockCodable<CardTrackingHistory>().loadCodableObject(
                resource: fileName,     
                typeDecoder: .useDefaultKeys) else { return nil }
        return CardTrackingViewModel(cardTrackingHistory: model, presenter: presentSpy, dependencies: dependencies)
    }()
    
    func testConfigureViews_WhenDidCall_ShouldConfigScreen() {
        sut?.configureViews()
        XCTAssertEqual(presentSpy.isArrivedReceived, false)
        XCTAssertEqual(presentSpy.updateHistoryCount, 1)
        XCTAssertEqual(presentSpy.setupEstimatedTimeCount, 1)
        XCTAssertEqual(presentSpy.setupReceivedCardButton, 1)
        XCTAssertEqual(presentSpy.setupHelpMessageCount, 1)
        
        let expectedEvent = CardTrackingHistoryAnalyticsEvents.cardTrackingEventsAction(.requested).event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testDidTapCardArrived_WhenDidCall_ShouldCallDidNextStep() {
        sut?.didTapCardArrived()
        XCTAssertEqual(presentSpy.actions, [CardTrackingAction.cardArrived(type: .multiple)])
        let expectedEvent = CardTrackingHistoryAnalyticsEvents.cardTrackingEventsAction(.received).event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testDidTapErrorButton_WhenDidCall_ShouldCallDidNextStep() {
        sut?.didTapErrorButton()
        XCTAssertEqual(presentSpy.actions, [CardTrackingAction.errorButton(type: .multiple)])
        let expectedEvent = CardTrackingHistoryAnalyticsEvents.cardTrackingEventsAction(.deliveryError).event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testDidTapCloseButton_WhenDidCall_ShouldCallDidNextStep() {
        sut?.didTapCloseButton()
        XCTAssertEqual(presentSpy.actions, [CardTrackingAction.close])
    }
    
    func testOpenFaq_WhendidCall_ShouldCallOpenZendesk() throws {
        let faqDeliveryFaileds = "faqDeliveryFailedValueToTest"
        let urlExpected = try XCTUnwrap(URL(string: faqDeliveryFaileds))
        featureManagerMock.override(key: .faqDeliveryCardFailed, with: faqDeliveryFaileds)
        sut?.openFAQ()
        let expectedEvent = CardTrackingHistoryAnalyticsEvents.cardTrackingEventsAction(.helpCenter).event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
        XCTAssertEqual(presentSpy.actions, [.openHelpCenter(url: urlExpected)])
    }
   
    func testTrackingByCardType_WhenCardTypeDebit_ShouldTrackingByCardType() {
        let model = CardTrackingHistory(currentStep: .delivered, cardType: .debit, notReceivedButtonEnabled: true, estimatedTime: TrackingEstimatedTime(text: "", highlightText: ""), tracking: [])
        sut = CardTrackingViewModel(cardTrackingHistory: model, presenter: presentSpy, dependencies: dependencies)
        sut?.trackingByCardType(action: .deliveryError)
        let expectedEvent = CardTrackingHistoryAnalyticsEvents.cardDebitCardTrackingEventsAction(.deliveryError).event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
}

