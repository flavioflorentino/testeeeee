import XCTest
@testable import Card

private class CardTrackingHistoryCellDisplayableSpy: CardTrackingHistoryCellDisplayable {
    
    //MARK: - updateFormattedDate
    private(set) var updateFormattedDateAttributedTextCallsCount = 0
    private(set) var updateFormattedDateAttributedTextReceivedInvocations: [NSAttributedString] = []
    //MARK: - updateFormattedTitle
    private(set) var updateFormattedTitleAttributedTextCallsCount = 0
    private(set) var updateFormattedTitleAttributedTextReceivedInvocations: [NSAttributedString] = []
    
    func updateFormattedDate(attributedText: NSAttributedString) {
        updateFormattedDateAttributedTextCallsCount += 1
        updateFormattedDateAttributedTextReceivedInvocations.append(attributedText)
    }
    
    func updateFormattedTitle(attributedText: NSAttributedString) {
        updateFormattedTitleAttributedTextCallsCount += 1
        updateFormattedTitleAttributedTextReceivedInvocations.append(attributedText)
    }
}

final class CardTrackingHistoryCellPresenterTests: XCTestCase {
    // MARK: - Variables
    private var viewControllerSpy = CardTrackingHistoryCellDisplayableSpy()
    
    // MARK: - Tests
    func testViewDidLoad_WhenViewControllerIsLoadedWithNormalItem_ShouldUpdateViewController() throws {
        let date = Date(timeIntervalSince1970: 1588767622)
        let sut = sutWith(historyItem: CardTrackingHistoryItem(date: date, title: "Solicitado", status: .waiting))
        sut.viewDidLoad()
        
        let attributedDate = try XCTUnwrap(viewControllerSpy.updateFormattedDateAttributedTextReceivedInvocations.last)
        
        XCTAssertEqual(viewControllerSpy.updateFormattedDateAttributedTextCallsCount, 1)
        XCTAssertEqual(attributedDate.string, "06 Mai 12:20")
        validateCurrentFont(attributedDate, textSubstring: "06 Mai", expectedFont: .systemFont(ofSize: 12, weight: .bold))
        validateCurrentFont(attributedDate, textSubstring: "12:20", expectedFont: .systemFont(ofSize: 12, weight: .medium))
        
        let attributedTitle = try XCTUnwrap(viewControllerSpy.updateFormattedTitleAttributedTextReceivedInvocations.last)
        XCTAssertEqual(attributedTitle.string, "Solicitado")
        validateCurrentFont(attributedTitle, expectedFont: .systemFont(ofSize: 14, weight: .medium))
        XCTAssertEqual(viewControllerSpy.updateFormattedTitleAttributedTextCallsCount, 1)
    }
    
      func testViewDidLoad_WhenViewControllerIsLoadedWithHighlightedItem_ShouldUpdateViewController() throws {
          let date = Date(timeIntervalSince1970: 1588854022)
          let sut = sutWith(historyItem: CardTrackingHistoryItem(date: date, title: "Na Transportadora", status: .inProgress))
          sut.viewDidLoad()
          
          let attributedDate = try XCTUnwrap(viewControllerSpy.updateFormattedDateAttributedTextReceivedInvocations.last)
          
          XCTAssertEqual(viewControllerSpy.updateFormattedDateAttributedTextCallsCount, 1)
          XCTAssertEqual(attributedDate.string, "07 Mai 12:20")
          validateCurrentFont(attributedDate, textSubstring: "07 Mai", expectedFont: .systemFont(ofSize: 14, weight: .heavy))
          validateCurrentFont(attributedDate, textSubstring: "12:20", expectedFont: .systemFont(ofSize: 14, weight: .medium))
          
          let attributedTitle = try XCTUnwrap(viewControllerSpy.updateFormattedTitleAttributedTextReceivedInvocations.last)
          XCTAssertEqual(attributedTitle.string, "Na Transportadora")
          validateCurrentFont(attributedTitle, expectedFont: .systemFont(ofSize: 18, weight: .semibold))
          XCTAssertEqual(viewControllerSpy.updateFormattedTitleAttributedTextCallsCount, 1)
      }
    
    private func sutWith(historyItem: CardTrackingHistoryItem) -> CardTrackingHistoryCellPresenting {
        let sut = CardTrackingHistoryCellPresenter(historyItem: historyItem, cardType: .multiple)
        sut.viewController = viewControllerSpy
        return sut
    }
    
    private func validateCurrentFont(_ attributed: NSAttributedString, textSubstring: String? = nil, expectedFont: UIFont) {
        let range = NSString(string: attributed.string).range(of: textSubstring ?? attributed.string)
        let attributedSubstring = attributed.attributedSubstring(from: range)
        let currentFont = attributedSubstring.attributes(at: 0, effectiveRange: nil)[.font] as! UIFont
        XCTAssertEqual(currentFont, expectedFont)
    }
}
