import XCTest
@testable import Card

class DeeplinkCardTypeTests: XCTestCase {
    func testDeeplinkCardType_WhenReceiveCreditValidateURL_ShouldInstantiateNeedCreditActive() throws {
        let url = try XCTUnwrap(URL(string: "picpay://picpay/card/credit/validate"))
        let sut = DeeplinkCardType(url: url)
        XCTAssertEqual(sut, .needCreditActive)
    }
    
    func testDeeplinkCardType_WhenReceiveDebitValidateURL_ShouldInstantiateNeedDebitActive() throws {
        let url = try XCTUnwrap(URL(string: "picpay://picpay/card/debit/validate"))
        let sut = DeeplinkCardType(url: url)
        XCTAssertEqual(sut, .needDebitActive)
    }
    
    func testDeeplinkCardType_WhenReceiveCreditRequestURL_ShouldInstantiateRequestCreditCard() throws {
        let url = try XCTUnwrap(URL(string: "picpay://picpay/card/credit/request"))
        let sut = DeeplinkCardType(url: url)
        XCTAssertEqual(sut, .requestCreditCard)
    }
    
    func testDeeplinkCardType_WhenReceiveActivateDebitURL_ShouldInstantiateActivateDebitCard() throws {
        let url = try XCTUnwrap(URL(string: "picpay://picpay/card/credit/activate-debit"))
        let sut = DeeplinkCardType(url: url)
        XCTAssertEqual(sut, .activateDebitCard)
    }
    
    func testDeeplinkCardType_WhenReceiveDebitRegistrationURL_ShouldInstantiateDebitRegistration() throws {
        let url = try XCTUnwrap(URL(string: "picpay://picpay/card/debit/registration"))
        let sut = DeeplinkCardType(url: url)
        XCTAssertEqual(sut, .debitRegistration)
    }
    
    func testDeeplinkCardType_WhenReceiveDebitRequestInProgressURL_ShouldInstantiateDebitRequestInProgress() throws {
        let url = try XCTUnwrap(URL(string: "picpay://picpay/card/debit/request-in-progress"))
        let sut = DeeplinkCardType(url: url)
        XCTAssertEqual(sut, .debitRequestInProgress)
    }
    
    func testDeeplinkCardType_WhenReceiveInvalidURL_ShouldNotInstantiate() throws {
        var url = try XCTUnwrap(URL(string: "picpay://picpay/car1d/debit/request-in-progress"))
        var sut = DeeplinkCardType(url: url)
        XCTAssertNil(sut)
        
        url = try XCTUnwrap(URL(string: "picpay://picpay/card/debi1t/request-in-progress"))
        sut = DeeplinkCardType(url: url)
        XCTAssertNil(sut)
        
        url = try XCTUnwrap(URL(string: "picpay://picpay/card/debit/re1quest-in-progress"))
        sut = DeeplinkCardType(url: url)
        XCTAssertNil(sut)
    }
    
    func testDeeplinkCardType_WhenReceiveHybridURL_ShouldInstantiateHybridFlow() throws {
        let url = try XCTUnwrap(URL(string: "picpay://picpay/card/register/hybrid"))
        let sut = DeeplinkCardType(url: url)
        XCTAssertNotNil(sut)
        XCTAssertEqual(sut, .hybrid)
    }
}
