import XCTest
@testable import Card

class CardDeeplinkHelperTests: XCTestCase {
    
    func testCardDeeplinkHelper_WhenReceiveNeedDebitActive_ShouldReturnActivateCardDebitFlowCoordinator() {
        let navigation = UINavigationController()
        let sut = CardDeeplinkHelper(cardType: .needDebitActive)
        let coordinator = sut.getCoordinating(navigation)
        XCTAssertNotNil(coordinator)
        XCTAssertTrue(coordinator is ActivateCardFlowCoordinator)
    }
    
    func testCardDeeplinkHelper_WhenReceiveRequestCreditCard_ShouldReturnRequestCardFlowCoordinator() {
        let navigation = UINavigationController()
        let sut = CardDeeplinkHelper(cardType: .requestCreditCard)
        let coordinator = sut.getCoordinating(navigation)
        XCTAssertNotNil(coordinator)
        XCTAssertTrue(coordinator is RequestPhysicalCardFlowCoordinator)
    }
    
    func testCardDeeplinkHelper_WhenReceiveActivateDebitCard_ShouldReturnActivateDebitCardFlowCoordinator() {
        let navigation = UINavigationController()
        let sut = CardDeeplinkHelper(cardType: .activateDebitCard)
        let coordinator = sut.getCoordinating(navigation)
        XCTAssertNotNil(coordinator)
        XCTAssertTrue(coordinator is ActivateDebitCardFlowCoordinator)
    }
    
    func testCardDeeplinkHelper_WhenReceiveDebitRegistration_ShouldReturnDebitRequestFlowCoordinator() {
         let navigation = UINavigationController()
         let sut = CardDeeplinkHelper(cardType: .debitRegistration)
         let coordinator = sut.getCoordinating(navigation)
         XCTAssertNotNil(coordinator)
         XCTAssertTrue(coordinator is DebitRequestFlowCoordinator)
     }
    
    func testCardDeeplinkHelper_WhenReceiveDebitRequestInProgress_ShouldReturnDebitRequestFlowCoordinator() {
         let navigation = UINavigationController()
         let sut = CardDeeplinkHelper(cardType: .debitRequestInProgress)
         let coordinator = sut.getCoordinating(navigation)
         XCTAssertNotNil(coordinator)
         XCTAssertTrue(coordinator is DebitRequestFlowCoordinator)
     }
    
    func testCardDeeplinkHelper_WhenReceiveTracking_ShouldReturnTrackingCoordinator() {
         let navigation = UINavigationController()
         let sut = CardDeeplinkHelper(cardType: .tracking)
         let coordinator = sut.getCoordinating(navigation)
         XCTAssertNotNil(coordinator)
         XCTAssertTrue(coordinator is CardTrackingFlowCoordinator)
     }
}
