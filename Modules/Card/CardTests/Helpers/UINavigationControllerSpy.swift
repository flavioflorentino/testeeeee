import UI
@testable import Card

final class UINavigationControllerSpy: UINavigationController {
    private(set) var pushCallCount = 0
    private(set) var presentCallCount = 0
    private(set) var popCallCount = 0
    private(set) var pushedViewController: UIViewController?
    private(set) var viewControllerToPresent: UIViewController?
    
    override func pushViewController(
        _ viewController: UIViewController,
        animated: Bool
    ) {
        pushCallCount += 1
        pushedViewController = viewController
        super.pushViewController(viewController, animated: true)
    }
    
    override func present(
        _ viewControllerToPresent: UIViewController,
        animated flag: Bool, completion: (() -> Void)? = nil
    ) {
        presentCallCount += 1
        self.viewControllerToPresent = viewControllerToPresent
        super.present(viewControllerToPresent, animated: flag, completion: completion)
    }
    
    override func popViewController(animated: Bool) -> UIViewController? {
        popCallCount += 1
        return super.popViewController(animated: animated)
    }
}
