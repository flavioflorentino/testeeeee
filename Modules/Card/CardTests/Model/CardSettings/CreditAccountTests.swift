import XCTest
@testable import Card

class CardSettingsTests: XCTestCase {
    func testLoadCodableObject_WhenDecode_ShouldCardSettingsWithoutBanner() throws {
        let settings: CardSettings = try loadCodableObject(fromJSON: "WithoutBanner")
        
        XCTAssertEqual(settings.cardWalletEnabled, true)
        XCTAssertEqual(settings.cardSettingsEnabled, true)
        XCTAssertEqual(settings.cardCreditSettingsEnabled, true)
        XCTAssertEqual(settings.physicalCardConfigurationEnabled, true)
        XCTAssertEqual(settings.physicalCardTrackingEnabled, true)
        XCTAssertEqual(settings.physicalCardBlockEnabled, true)
        XCTAssertEqual(settings.invoicePaymentEnabled, true)
        XCTAssertEqual(settings.upgradeDebitToCredit, true)
        XCTAssertTrue(settings.banners.isEmpty)
    }

    func testLoadCodableObject_WhenDecode_ShouldCardSettingsWithCardTracking() throws {
        let settings: CardSettings = try loadCodableObject(fromJSON: "WithCardTracking")
        
        XCTAssertEqual(settings.cardWalletEnabled, true)
        XCTAssertEqual(settings.cardSettingsEnabled, true)
        XCTAssertEqual(settings.cardCreditSettingsEnabled, true)
        XCTAssertEqual(settings.physicalCardConfigurationEnabled, true)
        XCTAssertEqual(settings.physicalCardTrackingEnabled, true)
        XCTAssertEqual(settings.physicalCardBlockEnabled, true)
        XCTAssertEqual(settings.invoicePaymentEnabled, true)
        XCTAssertEqual(settings.upgradeDebitToCredit, true)
        XCTAssertEqual(settings.banners, [CardSettings.Banner.cardTracking])
    }
    
    func testLoadCodableObject_WhenDecode_ShouldCardSettingsWithCardOffer() throws {
        let settings: CardSettings = try loadCodableObject(fromJSON: "WithCardOffer")
        
        XCTAssertEqual(settings.cardWalletEnabled, true)
        XCTAssertEqual(settings.cardSettingsEnabled, true)
        XCTAssertEqual(settings.cardCreditSettingsEnabled, true)
        XCTAssertEqual(settings.physicalCardConfigurationEnabled, true)
        XCTAssertEqual(settings.physicalCardTrackingEnabled, true)
        XCTAssertEqual(settings.physicalCardBlockEnabled, true)
        XCTAssertEqual(settings.invoicePaymentEnabled, true)
        XCTAssertEqual(settings.upgradeDebitToCredit, true)
        XCTAssertEqual(settings.banners, [CardSettings.Banner.cardOffer])
    }
    
    func testLoadCodableObject_WhenDecode_ShouldCardSettingsWithCardDebitOffer() throws {
        let settings: CardSettings = try loadCodableObject(fromJSON: "WithCardDebitOffer")
        
        XCTAssertEqual(settings.cardWalletEnabled, true)
        XCTAssertEqual(settings.cardSettingsEnabled, true)
        XCTAssertEqual(settings.cardCreditSettingsEnabled, true)
        XCTAssertEqual(settings.physicalCardConfigurationEnabled, true)
        XCTAssertEqual(settings.physicalCardTrackingEnabled, true)
        XCTAssertEqual(settings.physicalCardBlockEnabled, true)
        XCTAssertEqual(settings.invoicePaymentEnabled, true)
        XCTAssertEqual(settings.upgradeDebitToCredit, true)
        XCTAssertEqual(settings.banners, [CardSettings.Banner.cardDebitOffer])
    }
    
    func testLoadCodableObject_WhenDecode_ShouldCardSettingsWithcardDebitActivation() throws {
        let settings: CardSettings = try loadCodableObject(fromJSON: "WithCardDebitActivation")
        
        XCTAssertEqual(settings.cardWalletEnabled, true)
        XCTAssertEqual(settings.cardSettingsEnabled, true)
        XCTAssertEqual(settings.cardCreditSettingsEnabled, true)
        XCTAssertEqual(settings.physicalCardConfigurationEnabled, true)
        XCTAssertEqual(settings.physicalCardTrackingEnabled, true)
        XCTAssertEqual(settings.physicalCardBlockEnabled, true)
        XCTAssertEqual(settings.invoicePaymentEnabled, true)
        XCTAssertEqual(settings.upgradeDebitToCredit, true)
        XCTAssertEqual(settings.banners, [CardSettings.Banner.cardDebitActivation])
    }
    
    func testLoadCodableObject_WhenDecode_ShouldCardSettingsWithPhysicalCardActivation() throws {
        let settings: CardSettings = try loadCodableObject(fromJSON: "WithPhysicalCardActivation")
        
        XCTAssertEqual(settings.cardWalletEnabled, true)
        XCTAssertEqual(settings.cardSettingsEnabled, true)
        XCTAssertEqual(settings.cardCreditSettingsEnabled, true)
        XCTAssertEqual(settings.physicalCardConfigurationEnabled, true)
        XCTAssertEqual(settings.physicalCardTrackingEnabled, true)
        XCTAssertEqual(settings.physicalCardBlockEnabled, true)
        XCTAssertEqual(settings.invoicePaymentEnabled, true)
        XCTAssertEqual(settings.upgradeDebitToCredit, true)
        XCTAssertEqual(settings.banners, [CardSettings.Banner.physicalCardActivation])
    }
    
    func testLoadCodableObject_WhenDecode_ShouldCardSettingsWithPhysicalDebitActivation() throws {
        let settings: CardSettings = try loadCodableObject(fromJSON: "WithPhysicalDebitActivation")
        
        XCTAssertEqual(settings.cardWalletEnabled, true)
        XCTAssertEqual(settings.cardSettingsEnabled, true)
        XCTAssertEqual(settings.cardCreditSettingsEnabled, true)
        XCTAssertEqual(settings.physicalCardConfigurationEnabled, true)
        XCTAssertEqual(settings.physicalCardTrackingEnabled, true)
        XCTAssertEqual(settings.physicalCardBlockEnabled, true)
        XCTAssertEqual(settings.invoicePaymentEnabled, true)
        XCTAssertEqual(settings.upgradeDebitToCredit, true)
        XCTAssertEqual(settings.banners, [CardSettings.Banner.physicalDebitCardActivation])
    }
    
    func testLoadCodableObject_WhenDecode_ShouldCardSettingsWithMigrationPosCard() throws {
        let settings: CardSettings = try loadCodableObject(fromJSON: "WithMigrationPosCard")
        
        XCTAssertEqual(settings.cardWalletEnabled, true)
        XCTAssertEqual(settings.cardSettingsEnabled, true)
        XCTAssertEqual(settings.cardCreditSettingsEnabled, true)
        XCTAssertEqual(settings.physicalCardConfigurationEnabled, true)
        XCTAssertEqual(settings.physicalCardTrackingEnabled, true)
        XCTAssertEqual(settings.physicalCardBlockEnabled, true)
        XCTAssertEqual(settings.invoicePaymentEnabled, true)
        XCTAssertEqual(settings.upgradeDebitToCredit, true)
        XCTAssertEqual(settings.banners, [CardSettings.Banner.migrationPosCard])
    }
    
    func testLoadCodableObject_WhenDecode_ShouldCardSettingsWithAskPhysicalCard() throws {
        let settings: CardSettings = try loadCodableObject(fromJSON: "WithAskPhysicalCard")
        
        XCTAssertEqual(settings.cardWalletEnabled, true)
        XCTAssertEqual(settings.cardSettingsEnabled, true)
        XCTAssertEqual(settings.cardCreditSettingsEnabled, true)
        XCTAssertEqual(settings.physicalCardConfigurationEnabled, true)
        XCTAssertEqual(settings.physicalCardTrackingEnabled, true)
        XCTAssertEqual(settings.physicalCardBlockEnabled, true)
        XCTAssertEqual(settings.invoicePaymentEnabled, true)
        XCTAssertEqual(settings.upgradeDebitToCredit, true)
        XCTAssertEqual(settings.banners, [CardSettings.Banner.askPhysicalCard])
    }
    
    func testLoadCodableObject_WhenDecode_ShouldCardSettingsWithDebitRequestInProgress() throws {
        let settings: CardSettings = try loadCodableObject(fromJSON: "WithDebitRequestInProgress")
        
        XCTAssertEqual(settings.cardWalletEnabled, true)
        XCTAssertEqual(settings.cardSettingsEnabled, true)
        XCTAssertEqual(settings.cardCreditSettingsEnabled, true)
        XCTAssertEqual(settings.physicalCardConfigurationEnabled, true)
        XCTAssertEqual(settings.physicalCardTrackingEnabled, true)
        XCTAssertEqual(settings.physicalCardBlockEnabled, true)
        XCTAssertEqual(settings.invoicePaymentEnabled, true)
        XCTAssertEqual(settings.upgradeDebitToCredit, true)
        XCTAssertEqual(settings.banners, [CardSettings.Banner.debitRequestInProgress])
    }
    
    func testLoadCodableObject_WhenDecode_ShouldCardSettingsWithUpgradeDebitToCredit() throws {
        let settings: CardSettings = try loadCodableObject(fromJSON: "WithUpgradeDebitToCredit")
        
        XCTAssertEqual(settings.cardWalletEnabled, true)
        XCTAssertEqual(settings.cardSettingsEnabled, true)
        XCTAssertEqual(settings.cardCreditSettingsEnabled, true)
        XCTAssertEqual(settings.physicalCardConfigurationEnabled, true)
        XCTAssertEqual(settings.physicalCardTrackingEnabled, true)
        XCTAssertEqual(settings.physicalCardBlockEnabled, true)
        XCTAssertEqual(settings.invoicePaymentEnabled, true)
        XCTAssertEqual(settings.upgradeDebitToCredit, true)
        XCTAssertEqual(settings.banners, [CardSettings.Banner.upgradeDebitToCredit])
    }
}
