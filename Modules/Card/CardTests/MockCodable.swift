@testable import Card

class MockCodable<T: Decodable> {
    lazy var decoder: JSONDecoder = JSONDecoder()
    
    @discardableResult
    func loadCodableObject(resource: String,
                           typeDecoder: JSONDecoder.KeyDecodingStrategy = .convertFromSnakeCase,
                           decoderDateFormat: String? = nil) throws -> T {
        decoder.keyDecodingStrategy = typeDecoder
        if let dateFormat = decoderDateFormat {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = dateFormat
            decoder.dateDecodingStrategy = .formatted(dateFormatter)
        }
        let bundle = Bundle(for: type(of: self))
        let path = bundle.path(forResource: resource, ofType: "json")!
        let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
        
        let object = try decoder.decode(T.self, from: data)
        return object
    }
}
