import XCTest
@testable import Card

final class DynamicCarouselPresenterTests: XCTestCase {
    func testBuildAdvantageBanners_WhenDebitFlow_ShouldDisplayCashout() {
        let sut = DynamicCarouselPresenter(with: .debit, showCashout: true)
        XCTAssertEqual(3, sut.getAdvantages().count)
    }
    
    func testBuildAdvantageBanners_WhenDebitFlow_ShouldntDisplayCashout() {
        let sut = DynamicCarouselPresenter(with: .debit, showCashout: false)
        XCTAssertEqual(2, sut.getAdvantages().count)
    }
    
    func testBuildAdvantageBanners_WhenMultipleFlow_ShouldDisplayCashout() {
        let sut = DynamicCarouselPresenter(with: .multiple, showCashout: true)
        XCTAssertEqual(4, sut.getAdvantages().count)
    }
    
    func testBuildAdvantageBanners_WhenMultipleFlow_ShouldntDisplayCashout() {
        let sut = DynamicCarouselPresenter(with: .multiple, showCashout: false)
        XCTAssertEqual(3, sut.getAdvantages().count)
    }
    
    func testBuildAdvantageBanners_WhenMigrationFlow_ShouldDisplayCashout() {
        let sut = DynamicCarouselPresenter(with: .debitToMultiple, showCashout: true)
        XCTAssertEqual(4, sut.getAdvantages().count)
    }
    
    func testBuildAdvantageBanners_WhenMigrationFlow_ShouldntDisplayCashout() {
        let sut = DynamicCarouselPresenter(with: .debitToMultiple, showCashout: false)
        XCTAssertEqual(3, sut.getAdvantages().count)
    }
}
