import XCTest
import FeatureFlag
@testable import Card

final class DynamicOnboardingPresenterSpy: DynamicOnboardingPresenting {
    var viewController: DynamicOnboardingDisplaying?
    
    private (set) var action: DynamicOnboardingAction?
    private (set) var type: OnboardingTypeFlow?
    private (set) var cashout = 0
    
    func didNextStep(action: DynamicOnboardingAction) {
        self.action = action
    }
    
    func setScreenData(with type: OnboardingTypeFlow, showCashout: Bool) {
        self.type = type
        cashout += showCashout ? 1 : 0
    }
}

final class OnboardingAnalyticsSpy: OnboardingAnalytics {
    private (set) var callScrollBenefits = 0
    private (set) var callConfirm = 0
    private (set) var callHelpCenter = 0
    private (set) var callSeeRules = 0
    private (set) var callClose = 0
    private (set) var callViewDidAppear = 0
    
    func scrollBenefits() {
        callScrollBenefits += 1
    }
    
    func requestCard() {
        callConfirm += 1
    }
    
    func helpCenter() {
        callHelpCenter += 1
    }
    
    func seeRules() {
        callSeeRules += 1
    }
    
    func close() {
        callClose += 1
    }
    
    func viewDidAppear() {
        callViewDidAppear += 1
    }
}

final class DynamicOnboardingInteractorTests: XCTestCase {
    let presenterSpy = DynamicOnboardingPresenterSpy()
    let analyticsSpy = OnboardingAnalyticsSpy()
    let featureManagerMock = FeatureManagerMock()
    
    override func setUp() {
        let remoteConfigMock = RemoteConfigMock()
        RemoteConfigSetup.inject(instance: remoteConfigMock)
        featureManagerMock.override(keys: .opsCardOnboardingCashout, with: false)
    }
    
    func testDidClose_WhenPressButton_ShouldClose() {
        let sut = DynamicOnboardingInteractor(presenter: presenterSpy, type: .debit, analytics: analyticsSpy, dependencies: DependencyContainerMock(featureManagerMock))
        sut.didClose()
        XCTAssertEqual(presenterSpy.action, .close)
    }
    
    func testDidPressConfirm_WhenDebitPressButton_ShouldConfirm() {
        let sut = DynamicOnboardingInteractor(presenter: presenterSpy, type: .debit, analytics: analyticsSpy, dependencies: DependencyContainerMock(featureManagerMock))
        sut.didPressConfirm()
        XCTAssertEqual(analyticsSpy.callConfirm, 1)
        XCTAssertEqual(presenterSpy.action, .confirm)
    }
    
    func testDidPressConfirm__WhenMultiplePressButton_ShouldConfirm() {
        let sut = DynamicOnboardingInteractor(presenter: presenterSpy, type: .multiple, analytics: analyticsSpy, dependencies: DependencyContainerMock(featureManagerMock))
        sut.didPressConfirm()
        XCTAssertEqual(presenterSpy.action, .confirm)
        XCTAssertEqual(analyticsSpy.callConfirm, 1)
    }
    
    func testDidPressConfirm_WhenMigrationPressButton_ShouldConfirm() {
        let sut = DynamicOnboardingInteractor(presenter: presenterSpy, type: .debitToMultiple, analytics: analyticsSpy, dependencies: DependencyContainerMock(featureManagerMock))
        sut.didPressConfirm()
        XCTAssertEqual(presenterSpy.action, .confirm)
        XCTAssertEqual(analyticsSpy.callConfirm, 1)
    }
    
    func testSetScreenData_WhenDebitFlow_ShouldCashoutVisible() {
        featureManagerMock.override(keys: .opsCardOnboardingCashout, with: true)
        let sut = DynamicOnboardingInteractor(presenter: presenterSpy, type: .debit, analytics: analyticsSpy, dependencies: DependencyContainerMock(featureManagerMock))
        sut.setScreenData()
        XCTAssertEqual(presenterSpy.type, .debit)
        XCTAssertEqual(presenterSpy.cashout, 1)
    }
    
    func testSetScreenData_WhenDebitFlow_ShouldCashoutHidden() {
        featureManagerMock.override(keys: .opsCardOnboardingCashout, with: true)
        let sut = DynamicOnboardingInteractor(presenter: presenterSpy, type: .debit, analytics: analyticsSpy, dependencies: DependencyContainerMock(featureManagerMock))
        sut.setScreenData()
        XCTAssertEqual(presenterSpy.type, .debit)
        XCTAssertEqual(presenterSpy.cashout, 1)
    }
    
    func testSetScreenData_WhenMultipleFlow_ShouldCashoutVisible() {
        featureManagerMock.override(keys: .opsCardOnboardingCashout, with: true)
        let sut = DynamicOnboardingInteractor(presenter: presenterSpy, type: .multiple, analytics: analyticsSpy, dependencies: DependencyContainerMock(featureManagerMock))
        sut.setScreenData()
        XCTAssertEqual(presenterSpy.type, .multiple)
        XCTAssertEqual(presenterSpy.cashout, 1)
    }
    
    func testSetScreenData_WhenMultipleFlow_ShouldCashoutHidden() {
        featureManagerMock.override(keys: .opsCardOnboardingCashout, with: true)
        let sut = DynamicOnboardingInteractor(presenter: presenterSpy, type: .multiple, analytics: analyticsSpy, dependencies: DependencyContainerMock(featureManagerMock))
        sut.setScreenData()
        XCTAssertEqual(presenterSpy.type, .multiple)
        XCTAssertEqual(presenterSpy.cashout, 1)
    }
    
    func testSetScreenData_WhenMigationFlow_ShouldCashoutVisible() {
        featureManagerMock.override(keys: .opsCardOnboardingCashout, with: true)
        let sut = DynamicOnboardingInteractor(presenter: presenterSpy, type: .debitToMultiple, analytics: analyticsSpy, dependencies: DependencyContainerMock(featureManagerMock))
        sut.setScreenData()
        XCTAssertEqual(presenterSpy.type, .debitToMultiple)
        XCTAssertEqual(presenterSpy.cashout, 1)
    }
    
    func testSetScreenData_WhenMigationFlow_ShouldCashoutHidden() {
        featureManagerMock.override(keys: .opsCardOnboardingCashout, with: true)
        let sut = DynamicOnboardingInteractor(presenter: presenterSpy, type: .debitToMultiple, analytics: analyticsSpy, dependencies: DependencyContainerMock(featureManagerMock))
        sut.setScreenData()
        XCTAssertEqual(presenterSpy.type, .debitToMultiple)
        XCTAssertEqual(presenterSpy.cashout, 1)
    }
    
    func testDidInteract_WithAdvantages_WhenScroll_ShouldSendEvent() {
        let sut = DynamicOnboardingInteractor(presenter: presenterSpy, type: .debit, analytics: analyticsSpy, dependencies: DependencyContainerMock(featureManagerMock))
        sut.didInteractWithAdvantages()
        XCTAssertEqual(analyticsSpy.callScrollBenefits, 1)
    }
    
    func testDidPressRules_WhenPressIsDebit_ShouldNotSendEvent() {
        let sut = DynamicOnboardingInteractor(presenter: presenterSpy, type: .debit, analytics: analyticsSpy, dependencies: DependencyContainerMock(featureManagerMock))
        sut.didPressRules()
        XCTAssertEqual(analyticsSpy.callSeeRules, 0)
    }
    
    func testDidPressRules_WhenIsMultiple_ShouldSendEvent() {
        featureManagerMock.override(key: .linkRulesMultipleCardOnboarding, with: "url")
        let sut = DynamicOnboardingInteractor(presenter: presenterSpy, type: .multiple, analytics: analyticsSpy, dependencies: DependencyContainerMock(featureManagerMock))
        sut.didPressRules()
        XCTAssertEqual(analyticsSpy.callSeeRules, 1)
    }
    
    func testViewDidAppear_WhenIsMultiple_ShouldSendEvent() {
        let sut = DynamicOnboardingInteractor(presenter: presenterSpy, type: .multiple, analytics: analyticsSpy, dependencies: DependencyContainerMock(featureManagerMock))
        sut.trackScreenView()
        XCTAssertEqual(analyticsSpy.callViewDidAppear, 1)
    }
    
    func testViewDidAppear_WhenIsDebit_ShouldSendEvent() {
        let sut = DynamicOnboardingInteractor(presenter: presenterSpy, type: .debit, analytics: analyticsSpy, dependencies: DependencyContainerMock(featureManagerMock))
        sut.trackScreenView()
        XCTAssertEqual(analyticsSpy.callViewDidAppear, 1)
    }
}
