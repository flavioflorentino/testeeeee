import AnalyticsModule
import FeatureFlag
import XCTest
@testable import Card

final class DynamicOnboardingCoordinatorSpy: DynamicOnboardingCoordinating {
    var viewController: (UIViewController & DynamicOnboardingDisplaying)?
    var delegate: DynamicOnboardingCoordinatorDelegate?
    private(set) var action: DynamicOnboardingAction?
    private(set) var callAction = 0
    
    func perform(action: DynamicOnboardingAction) {
        self.action = action
        self.callAction += 1
    }
}

final class DynamicOnboardingViewControllerSpy: DynamicOnboardingDisplaying {
    
    private(set) var rows = [DynamicOnboardingTypeRows]()
    private(set) var pinnedViewButtonText = ""
    private(set) var didCallHideWarningMessage = 0
    
    func displayRows(with rows: [DynamicOnboardingTypeRows]) {
        self.rows = rows
    }
    
    func displayPinnedView(with text: String) {
        self.pinnedViewButtonText = text
    }
    
    func hideWarningMessage() {
        didCallHideWarningMessage += 1
    }
}

final class DynamicOnboardingPresenterTests: XCTestCase {
    let coordinatorSpy = DynamicOnboardingCoordinatorSpy()
    let controllerSpy = DynamicOnboardingViewControllerSpy()
    
    var featureManagerMock = FeatureManagerMock()
    var analyticsSpy = AnalyticsSpy()
    
    lazy var sut: DynamicOnboardingPresenter = {
        let sut = DynamicOnboardingPresenter(coordinator: coordinatorSpy, dependencies: DependencyContainerMock(analyticsSpy, featureManagerMock))
        sut.viewController = controllerSpy
        return sut
    }()
    
    override func setUp() {
        let remoteConfigMock = RemoteConfigMock()
        RemoteConfigSetup.inject(instance: remoteConfigMock)
        featureManagerMock.override(keys: .experimentDebitCardOfferButtonBool, with: false)
        featureManagerMock.override(keys: .experimentCreditCardOfferButtonBool, with: true)
    }
    
    func testDidNextStep_WhenPressClose_ShouldCloseScreen() {
        sut.didNextStep(action: .close)
        XCTAssertEqual(coordinatorSpy.action, .close)
        XCTAssertEqual(coordinatorSpy.callAction, 1)
    }
    
    func testDidNextStep_WhenPressConfirm_ShouldConfirm() {
        sut.didNextStep(action: .confirm)
        XCTAssertEqual(coordinatorSpy.action, .confirm)
        XCTAssertEqual(coordinatorSpy.callAction, 1)
    }
    
    func testDidNextStep_WhenShowURL_ShouldOpenWebview() {
        guard let url = URL(string: "teste") else {
            XCTAssert(false)
            return
        }
        sut.didNextStep(action: .presentScreen(url: url))
        XCTAssertEqual(coordinatorSpy.action, .presentScreen(url: url))
        XCTAssertEqual(coordinatorSpy.callAction, 1)
    }
    
    func testSetScreenData_WhenDebitFlow_ShouldUpdateScreen() {
        sut.setScreenData(with: .debit, showCashout: true)
        controllerSpy.rows.forEach {
            switch $0 {
            case let .header(view):
                XCTAssertTrue(view is DynamicOnboardingHeaderView)
            case let .carousel(view):
                XCTAssertTrue(view is DynamicCarouselView)
            case let .item(view):
                XCTAssertTrue(view is DynamicOnboardingItemView)
            }
        }
        
        XCTAssertEqual(controllerSpy.didCallHideWarningMessage, 1)
    }
    
    func testSetScreenData_WhenMultipleFlow() {
        sut.setScreenData(with: .multiple, showCashout: true)
        controllerSpy.rows.forEach {
            switch $0 {
            case let .header(view):
                XCTAssertTrue(view is DynamicOnboardingHeaderView)
            case let .carousel(view):
                XCTAssertTrue(view is DynamicCarouselView)
            case let .item(view):
                XCTAssertTrue(view is DynamicOnboardingItemView)
            }
        }
        
        XCTAssertEqual(controllerSpy.didCallHideWarningMessage, 0)
    }
    
    func testSetScreenData_WhenMigrationFlow() {
        sut.setScreenData(with: .debitToMultiple, showCashout: true)
        controllerSpy.rows.forEach {
            switch $0 {
            case let .header(view):
                XCTAssertTrue(view is DynamicOnboardingHeaderView)
            case let .carousel(view):
                XCTAssertTrue(view is DynamicCarouselView)
            case let .item(view):
                XCTAssertTrue(view is DynamicOnboardingItemView)
            }
        }
        
        XCTAssertEqual(controllerSpy.didCallHideWarningMessage, 0)
    }
}
