import XCTest
@testable import Card

final class HomeAddressFormatterTests: XCTestCase {
    // MARK: - Variables
    private var deliveryDebitAddress: HomeAddress {
        let cardForm = try! MockCodable<HomeAddress>().loadCodableObject(
            resource: "deliveryDebitAddress",
            typeDecoder: .convertFromSnakeCase)
        return cardForm
    }

    // MARK: - Tests
    func testUpdateAddress_WhenViewControllerIsLoaded_ShouldDisplayAddress() {
        let formatter = HomeAddressFormatter.presentFormatedAdress(homeAddress: deliveryDebitAddress)
        XCTAssertEqual(formatter, "Rua Al Doutor, Centro 12 - lado par - PR - Curitiba, 88888-888")
    }
    
    func testGetWithoutSeparators_WhenPassingContentWithInitialAndFinalSeparators_ShouldReturnOnlyTheContent() {
        XCTAssertEqual(HomeAddressFormatter.getWithoutSeparators("- lado par"),"lado par")
        XCTAssertEqual(HomeAddressFormatter.getWithoutSeparators("- lado par -"),"lado par")
        XCTAssertEqual(HomeAddressFormatter.getWithoutSeparators(", lado par -"),"lado par")
        XCTAssertEqual(HomeAddressFormatter.getWithoutSeparators(". lado par -"),"lado par")
        XCTAssertEqual(HomeAddressFormatter.getWithoutSeparators("1. lado par -"),"1. lado par")
        XCTAssertEqual(HomeAddressFormatter.getWithoutSeparators("A. lado par -"),"A. lado par")
        XCTAssertEqual(HomeAddressFormatter.getWithoutSeparators("- lado/par -"),"lado/par")
        XCTAssertEqual(HomeAddressFormatter.getWithoutSeparators("- lado*par -"),"lado*par")
        XCTAssertEqual(HomeAddressFormatter.getWithoutSeparators(" lado*par "),"lado*par")
        XCTAssertEqual(HomeAddressFormatter.getWithoutSeparators(" la "),"la")
        XCTAssertEqual(HomeAddressFormatter.getWithoutSeparators("- lado, 1234, A par "),"lado, 1234, A par")
    }
    
    func testGetWithoutSeparators_WhenPassingNothingValid_ShouldReturnNil() {
        XCTAssertNil(HomeAddressFormatter.getWithoutSeparators(" - "))
        XCTAssertNil(HomeAddressFormatter.getWithoutSeparators(" "))
        XCTAssertNil(HomeAddressFormatter.getWithoutSeparators("-"))
        XCTAssertNil(HomeAddressFormatter.getWithoutSeparators(","))
    }
    
    func testGetWithoutSeparators_WhenPassingSingleCharRequirement_ShouldReturnTheContent() {
        XCTAssertEqual(HomeAddressFormatter.getWithoutSeparators("1", requireMinimumTwoCharacters: false),"1")
        XCTAssertEqual(HomeAddressFormatter.getWithoutSeparators("- 1 -", requireMinimumTwoCharacters: false),"1")
        XCTAssertEqual(HomeAddressFormatter.getWithoutSeparators("- a -", requireMinimumTwoCharacters: false),"a")
        XCTAssertEqual(HomeAddressFormatter.getWithoutSeparators("- a1 -", requireMinimumTwoCharacters: false),"a1")
        XCTAssertEqual(HomeAddressFormatter.getWithoutSeparators("1234", requireMinimumTwoCharacters: false),"1234")
    }
    
    func testGetFormattedStreet_WhenPassingStreetTypeAndName_ShouldReturnFormattedStreetTypeAndName() {
        XCTAssertEqual(HomeAddressFormatter.getFormattedStreet(streetType: "Rua", street: "Al Doutor"), "Rua Al Doutor")
    }
    
    func testGetFormattedStreetNumber_WhenPassingValue_ShouldReturnFormattedStreetNumber() {
        XCTAssertEqual(HomeAddressFormatter.getFormattedStreetNumber("28"), " 28")
        XCTAssertEqual(HomeAddressFormatter.getFormattedStreetNumber("1"), " 1")
        XCTAssertEqual(HomeAddressFormatter.getFormattedStreetNumber("- 12"), " 12")
        XCTAssertEqual(HomeAddressFormatter.getFormattedStreetNumber("- 12 -"), " 12")
    }
    
    func testGetFormattedNeighborhood_WhenPassingValue_ShouldReturnFormattedNeighborhood() {
        XCTAssertEqual(HomeAddressFormatter.getFormattedNeighborhood("Vila Buarque"), ", Vila Buarque")
        XCTAssertEqual(HomeAddressFormatter.getFormattedNeighborhood("- Vila Buarque"), ", Vila Buarque")
        XCTAssertEqual(HomeAddressFormatter.getFormattedNeighborhood("-Vila Buarque"), ", Vila Buarque")
    }
    
    func testGetFormattedAddressComplement_WhenPassingValue_ShouldReturnFormattedAddressComplement() {
        XCTAssertEqual(HomeAddressFormatter.getFormattedAddressComplement("lado par"), " - lado par")
        XCTAssertEqual(HomeAddressFormatter.getFormattedAddressComplement("- lado par"), " - lado par")
    }
    
    func testGetFormattedState_WhenPassingValue_ShouldReturnFormattedState() {
        XCTAssertEqual(HomeAddressFormatter.getFormattedState("PE"), " - PE")
        XCTAssertEqual(HomeAddressFormatter.getFormattedState("-PE"), " - PE")
        XCTAssertEqual(HomeAddressFormatter.getFormattedState("- PE"), " - PE")
    }
    
    func testGetFormattedCity_WhenPassingValue_ShouldReturnFormattedCity() {
        XCTAssertEqual(HomeAddressFormatter.getFormattedCity("Recife"), " - Recife")
        XCTAssertEqual(HomeAddressFormatter.getFormattedCity("-Recife"), " - Recife")
        XCTAssertEqual(HomeAddressFormatter.getFormattedCity("-Recife- "), " - Recife")
    }
    
    func testGetFormattedZipCode_WhenPassingValue_ShouldReturnFormattedZipCode() {
        XCTAssertEqual(HomeAddressFormatter.getFormattedZipCode("88888888"), ", 88888-888")
        XCTAssertEqual(HomeAddressFormatter.getFormattedZipCode("- 88888888"), ", 88888-888")
        XCTAssertEqual(HomeAddressFormatter.getFormattedZipCode(" , 88888888"), ", 88888-888")
    }
}
