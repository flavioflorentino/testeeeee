import AssetsKit
import Core
import UI
import XCTest
@testable import Card

private final class DeliveryConfirmAddressViewControllerSpy: DeliveryConfirmAddressDisplaying {
    
    // MARK: - Variables
    private(set) var viewModel: DeliveryConfirmAddressViewModel?
    private(set) var callSetupScreenData = 0
    private(set) var callDisplayContractChecked = 0
    private(set) var contractCheckedSpy = false
    private(set) var callDisplayLoading = 0
    private(set) var callDismissLoading = 0
    private(set) var errorMessage: String?
    private(set) var callDisplayAlertError = 0
    
    // MARK: - Tests
    func setupScreenData(with viewModel: DeliveryConfirmAddressViewModel) {
        self.viewModel = viewModel
        self.callSetupScreenData += 1
    }
    
    func displayContract(checked: Bool) {
        callDisplayContractChecked += 1
        contractCheckedSpy = checked
    }
    
    func displayLoading() {
        self.callDisplayLoading += 1
    }
    
    func dismissLoading() {
        callDismissLoading += 1
    }
    
    func displayAlertError(with description: String) {
        self.errorMessage = description
        self.callDisplayAlertError += 1
    }
}

private final class DeliveryConfirmAddressCoordinatorSpy: DeliveryConfirmAddressCoordinating {
    // MARK: - Variables
    private(set) var callPerformWithAction = 0
    private(set) var actionSelected: DeliveryConfirmAddressAction?
    private(set) var callPresentSafariViewController = 0
    private(set) var url: URL?

    var viewController: UIViewController?
    
    // MARK: - Public Methods
    func perform(action: DeliveryConfirmAddressAction) {
        actionSelected = action
        callPerformWithAction += 1
    }
    
    func presentSafariViewController(url: URL) {
        self.url = url
        callPresentSafariViewController += 1
    }
}

final class DeliveryConfirmAddressPresenterTests: XCTestCase {
    // MARK: - Variables
    private var viewControllerSpy = DeliveryConfirmAddressViewControllerSpy()
    private let coordinatorSpy = DeliveryConfirmAddressCoordinatorSpy()

    private lazy var sut: DeliveryConfirmAddressPresenter = {
        let sut = DeliveryConfirmAddressPresenter(coordinator: coordinatorSpy)
        sut.viewController = viewControllerSpy
        return sut
    }()
    
    private var deliveryAddress: HomeAddress {
        let cardForm = try! MockCodable<HomeAddress>().loadCodableObject(
            resource: "deliveryDebitAddress",
            typeDecoder: .convertFromSnakeCase)
        return cardForm
    }
    
    // MARK: - Tests
    func testSetupSccreen_ShouldSetCorretStates() {
        sut.setupScreen(with: .debit, homeAddress: deliveryAddress, canChangeAddress: true, isContractHidden: false, isConfirmationEnable: false)
        
        let statesViewModel = DeliveryConfirmAddressViewModelStates(canChangeAddress: true,
                                                                    isContractHidden: false,
                                                                    isConfirmationEnable: false)
        
        XCTAssertEqual(viewControllerSpy.callSetupScreenData, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.states, statesViewModel)
    }
    
    func testSetupSccreen_WhenDebitFlow_ShouldSetDebitContent() {
        sut.setupScreen(with: .debit, homeAddress: deliveryAddress, canChangeAddress: true, isContractHidden: true, isConfirmationEnable: false)
        
        let address = "Rua Al Doutor, Centro 12 - lado par - PR - Curitiba, 88888-888"
        let description = "Ainda não temos um cartão de crédito pra você. Peça o seu PicPay Card débito e assim que o crédito for liberado, vamos te avisar, tá bom?"
        
        XCTAssertEqual(viewControllerSpy.callSetupScreenData, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.content.image, Card.Assets.iconUpgradeDebit.image)
        XCTAssertEqual(viewControllerSpy.viewModel?.content.title, "Tenha o seu PicPay Card\ndébito")
        XCTAssertEqual(viewControllerSpy.viewModel?.content.description.string, description)
        XCTAssertEqual(viewControllerSpy.viewModel?.content.buttonTitle, "Pedir meu cartão")
        XCTAssertEqual(viewControllerSpy.viewModel?.content.address.string, address)
        XCTAssertEqual(viewControllerSpy.viewModel?.content.contractTitle, nil)
    }
    
    func testSetupSccreen_WhenMultipleFlow_ShouldSetMultipleContent() {
        sut.setupScreen(with: .multiple, homeAddress: deliveryAddress, canChangeAddress: true, isContractHidden: true, isConfirmationEnable: true)
        
        let address = "Rua Al Doutor, Centro 12 - lado par - PR - Curitiba, 88888-888"
        let description = "Ah, é importante ter alguém em horário comercial para assinar a entrega, tá?"
        
        XCTAssertEqual(viewControllerSpy.callSetupScreenData, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.content.image, Resources.Illustrations.iluPersonHappyCard.image)
        XCTAssertEqual(viewControllerSpy.viewModel?.content.title, "Oba! Agora é só confirmar o endereço")
        XCTAssertEqual(viewControllerSpy.viewModel?.content.description.string, description)
        XCTAssertEqual(viewControllerSpy.viewModel?.content.buttonTitle, "Continuar")
        XCTAssertEqual(viewControllerSpy.viewModel?.content.address.string, address)
        XCTAssertEqual(viewControllerSpy.viewModel?.content.contractTitle, nil)
    }
    
    func testSetupSccreen_WhenUpgradeFlow_ShouldSetUpgradeContent() {
        sut.setupScreen(with: .upgrade, homeAddress: deliveryAddress, canChangeAddress: true, isContractHidden: true, isConfirmationEnable: true)
        
        let address = "Rua Al Doutor, Centro 12 - lado par - PR - Curitiba, 88888-888"
        let description = "Ah, é importante ter alguém em horário comercial para assinar a entrega, tá?"
        
        XCTAssertEqual(viewControllerSpy.callSetupScreenData, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.content.image, Resources.Illustrations.iluPersonHappyCard.image)
        XCTAssertEqual(viewControllerSpy.viewModel?.content.title, "Oba! Agora é só confirmar o endereço")
        XCTAssertEqual(viewControllerSpy.viewModel?.content.description.string, description)
        XCTAssertEqual(viewControllerSpy.viewModel?.content.buttonTitle, "Continuar")
        XCTAssertEqual(viewControllerSpy.viewModel?.content.address.string, address)
        XCTAssertEqual(viewControllerSpy.viewModel?.content.contractTitle, nil)
    }
    
    func testSetupSccreen_WhenDefaultFlow_ShouldSetDefaultContent() {
        sut.setupScreen(with: .replacement, homeAddress: deliveryAddress, canChangeAddress: false, isContractHidden: true, isConfirmationEnable: true)
        
        let address = "Rua Al Doutor, Centro 12 - lado par - PR - Curitiba, 88888-888"
        let description = "Precisamos que você confirme o endereço que quer receber o seu PicPay Card."
        
        XCTAssertEqual(viewControllerSpy.callSetupScreenData, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.content.image, Resources.Illustrations.iluPersonHappyCard.image)
        XCTAssertEqual(viewControllerSpy.viewModel?.content.title, "Oba! Agora é só\nconfirmar o pedido")
        XCTAssertEqual(viewControllerSpy.viewModel?.content.description.string, description)
        XCTAssertEqual(viewControllerSpy.viewModel?.content.buttonTitle, "Pedir meu PicPay Card")
        XCTAssertEqual(viewControllerSpy.viewModel?.content.address.string, address)
        XCTAssertEqual(viewControllerSpy.viewModel?.content.contractTitle, nil)
    }
    
    func testShowLoading_WhenIsRequested_ShouldDisplayLoadingView() {
        sut.showLoading()
        XCTAssertEqual(viewControllerSpy.callDisplayLoading, 1)
    }
    
    func testDismissLoading_WhenReceiveDismiss_ShouldUpdateScreen() {
        sut.dismissLoading()
        XCTAssertEqual(viewControllerSpy.callDismissLoading, 1)
    }
    
    func testChangeAddress_WhenIsCalled_ShouldPerformWithAction() {
        sut.changeAddress()
        XCTAssertEqual(coordinatorSpy.actionSelected, DeliveryConfirmAddressAction.changeAddress)
        XCTAssertEqual(coordinatorSpy.callPerformWithAction, 1)
    }
    
    func testClose_WhenIsCalled_ShouldPerformWithAction() {
        sut.close()
        XCTAssertEqual(coordinatorSpy.actionSelected, DeliveryConfirmAddressAction.close)
        XCTAssertEqual(coordinatorSpy.callPerformWithAction, 1)
    }
    
    func testConfirm_WhenConfirmButtoIsInvoked_ShouldPerformWithAction() {
        sut.confirm(registrationStatus: .activated)
        XCTAssertEqual(coordinatorSpy.actionSelected, DeliveryConfirmAddressAction.didConfirm(registrationStatus: .activated))
        XCTAssertEqual(coordinatorSpy.callPerformWithAction, 1)
    }
    
    func testPresentError_WhenServerError_ShouldDisplayErrorMessage() {
        sut.presentError(error: ApiError.serverError)
        XCTAssertNotNil(viewControllerSpy.errorMessage)
        XCTAssertEqual(viewControllerSpy.callDisplayAlertError, 1)
    }
    
    func testPresentError_WhenConnectionFailed_ShouldDisplayErrorMessage() {
        sut.presentError(error: ApiError.connectionFailure)
        XCTAssertEqual(viewControllerSpy.errorMessage, "Sem conexão com a internet")
        XCTAssertEqual(viewControllerSpy.callDisplayAlertError, 1)
    }
    
    func testPresentError_WhenTimeout_ShouldDisplayErrorMessage() {
        sut.presentError(error: ApiError.timeout)
        XCTAssertEqual(viewControllerSpy.errorMessage, "Parece que tivemos um problema na conexão. Tente novamente em alguns instantes.")
        XCTAssertEqual(viewControllerSpy.callDisplayAlertError, 1)
    }
    
    func testDisplayContractCheck_WhenTrue_ShouldDisplayChecked() {
        sut.presentContract(checked: true)
        XCTAssertEqual(viewControllerSpy.callDisplayContractChecked, 1)
        XCTAssertTrue(viewControllerSpy.contractCheckedSpy)
    }
    
    func testDisplayContractCheck_WhenFalse_ShouldDisplayUnchecked() {
        sut.presentContract(checked: false)
        XCTAssertEqual(viewControllerSpy.callDisplayContractChecked, 1)
        XCTAssertFalse(viewControllerSpy.contractCheckedSpy)
    }
    
    func testGetFormattedStreet_WhenPassingStreetTypeAndName_ShouldReturnFormattedStreetTypeAndName() {
        XCTAssertEqual(sut.getFormattedStreet(streetType: "Rua", street: "Al Doutor"), "Rua Al Doutor")
    }
    
    func testGetFormattedStreetNumber_WhenPassingValue_ShouldReturnFormattedStreetNumber() {
        XCTAssertEqual(sut.getFormattedStreetNumber("28"), " 28")
        XCTAssertEqual(sut.getFormattedStreetNumber("1"), " 1")
        XCTAssertEqual(sut.getFormattedStreetNumber("- 12"), " 12")
        XCTAssertEqual(sut.getFormattedStreetNumber("- 12 -"), " 12")
    }
    
    func testGetFormattedNeighborhood_WhenPassingValue_ShouldReturnFormattedNeighborhood() {
        XCTAssertEqual(sut.getFormattedNeighborhood("Vila Buarque"), ", Vila Buarque")
        XCTAssertEqual(sut.getFormattedNeighborhood("- Vila Buarque"), ", Vila Buarque")
        XCTAssertEqual(sut.getFormattedNeighborhood("-Vila Buarque"), ", Vila Buarque")
    }
    
    func testGetFormattedAddressComplement_WhenPassingValue_ShouldReturnFormattedAddressComplement() {
        XCTAssertEqual(sut.getFormattedAddressComplement("lado par"), " - lado par")
        XCTAssertEqual(sut.getFormattedAddressComplement("- lado par"), " - lado par")
    }
    
    func testGetFormattedState_WhenPassingValue_ShouldReturnFormattedState() {
        XCTAssertEqual(sut.getFormattedState("PE"), " - PE")
        XCTAssertEqual(sut.getFormattedState("-PE"), " - PE")
        XCTAssertEqual(sut.getFormattedState("- PE"), " - PE")
    }
    
    func testGetFormattedCity_WhenPassingValue_ShouldReturnFormattedCity() {
        XCTAssertEqual(sut.getFormattedCity("Recife"), " - Recife")
        XCTAssertEqual(sut.getFormattedCity("-Recife"), " - Recife")
        XCTAssertEqual(sut.getFormattedCity("-Recife- "), " - Recife")
    }
    
    func testGetFormattedZipCode_WhenPassingValue_ShouldReturnFormattedZipCode() {
        XCTAssertEqual(sut.getFormattedZipCode("88888888"), ", 88888-888")
        XCTAssertEqual(sut.getFormattedZipCode("- 88888888"), ", 88888-888")
        XCTAssertEqual(sut.getFormattedZipCode(" , 88888888"), ", 88888-888")
    }
    
    func testGetWithoutSeparators_WhenPassingContentWithInitialAndFinalSeparators_ShouldReturnOnlyTheContent() {
        XCTAssertEqual(sut.getWithoutSeparators("- lado par"),"lado par")
        XCTAssertEqual(sut.getWithoutSeparators("- lado par -"),"lado par")
        XCTAssertEqual(sut.getWithoutSeparators(", lado par -"),"lado par")
        XCTAssertEqual(sut.getWithoutSeparators(". lado par -"),"lado par")
        XCTAssertEqual(sut.getWithoutSeparators("1. lado par -"),"1. lado par")
        XCTAssertEqual(sut.getWithoutSeparators("A. lado par -"),"A. lado par")
        XCTAssertEqual(sut.getWithoutSeparators("- lado/par -"),"lado/par")
        XCTAssertEqual(sut.getWithoutSeparators("- lado*par -"),"lado*par")
        XCTAssertEqual(sut.getWithoutSeparators(" lado*par "),"lado*par")
        XCTAssertEqual(sut.getWithoutSeparators(" la "),"la")
        XCTAssertEqual(sut.getWithoutSeparators("- lado, 1234, A par "),"lado, 1234, A par")
    }
    
    func testGetWithoutSeparators_WhenPassingNothingValid_ShouldReturnNil() {
        XCTAssertNil(sut.getWithoutSeparators(" - "))
        XCTAssertNil(sut.getWithoutSeparators(" "))
        XCTAssertNil(sut.getWithoutSeparators("-"))
        XCTAssertNil(sut.getWithoutSeparators(","))
    }
    
    func testGetWithoutSeparators_WhenPassingSingleCharRequirement_ShouldReturnTheContent() {
        XCTAssertEqual(sut.getWithoutSeparators("1", requireMinimumTwoCharacters: false),"1")
        XCTAssertEqual(sut.getWithoutSeparators("- 1 -", requireMinimumTwoCharacters: false),"1")
        XCTAssertEqual(sut.getWithoutSeparators("- a -", requireMinimumTwoCharacters: false),"a")
        XCTAssertEqual(sut.getWithoutSeparators("- a1 -", requireMinimumTwoCharacters: false),"a1")
        XCTAssertEqual(sut.getWithoutSeparators("1234", requireMinimumTwoCharacters: false),"1234")
    }
}
