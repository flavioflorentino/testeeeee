import Core
import FeatureFlag
import XCTest
@testable import Card

private final class DeliveryConfirmAddressPresentingSpy: DeliveryConfirmAddressPresenting {
    // MARK: - Variables
    var viewController: DeliveryConfirmAddressDisplaying?
    
    private(set) var flow: DeliveryConfirmAddressFlow?
    private(set) var homeAddress: HomeAddress?
    private(set) var canChangeAddress: Bool?
    private(set) var isContractHidden: Bool?
    private(set) var isConfirmationEnable: Bool?
    private(set) var callSetupScreen = 0
    private(set) var callShowLoading = 0
    private(set) var callDismissLoading = 0
    private(set) var callChangedAddress = 0
    private(set) var callClose = 0
    private(set) var registrationStatus: RegistrationStatus?
    private(set) var callConfirm = 0
    private(set) var callPresentError = 0
    private(set) var callPresentGenericError = 0
    private(set) var callPresentContractCheck = 0
    private(set) var callPresentContractReader = 0
    private(set) var contractURL: URL?
    
    func setupScreen(with flow: DeliveryConfirmAddressFlow,
                     homeAddress: HomeAddress,
                     canChangeAddress: Bool,
                     isContractHidden: Bool,
                     isConfirmationEnable: Bool) {
        self.flow = flow
        self.homeAddress = homeAddress
        self.canChangeAddress = canChangeAddress
        self.isContractHidden = isContractHidden
        self.isConfirmationEnable = isConfirmationEnable
        self.callSetupScreen += 1
    }
    
    func showLoading() {
        self.callShowLoading += 1
    }
    
    func dismissLoading() {
        callDismissLoading += 1
    }
    
    func changeAddress() {
        self.callChangedAddress += 1
    }
    
    func close() {
        self.callClose += 1
    }
    
    func confirm(registrationStatus: RegistrationStatus?) {
        self.registrationStatus = registrationStatus
        self.callConfirm += 1
    }
    
    func presentError(error: ApiError) {
        self.callPresentError += 1
    }
    
    func presentGenericError() {
        self.callPresentGenericError += 1
    }
    
    func presentContract(checked: Bool) {
        self.callPresentContractCheck += 1
    }
    
    func presentContractReader(url: URL) {
        self.contractURL = url
        self.callPresentContractReader += 1
    }
}

private final class DeliveryConfirmAddressServiceSpy: DeliveryConfirmAddressServicing {
    // MARK: - Variables
    var flow: DeliveryConfirmAddressFlow = .debit
    var canChangeAddress = true
    var isSuccess = false
    var isConfirmNeeded = true
    var registrationStatus: RegistrationStatus?
    
    // MARK: - Public Methods
    func registrationFinish(password: String?, completion: @escaping CompletionRegistrationFinish) {
        // TODO: Fail test when registrationStatus is nil
        if isSuccess {
            guard let registrationStatus = registrationStatus else {
                return
            }
            completion(.success(registrationStatus))
        } else {
            completion(.failure(.serverError))
        }
    }
}

private final class AuthManagerSpy: AuthContract {
    
    func authenticate(sucessHandler: @escaping (String, Bool) -> Void, cancelHandler: (() -> Void)?) {
        sucessHandler("", true)
    }
}

private final class DeliveryEventsSpy: DeliveryConfirmAddressEvents {
    // MARK: - Variables
    private(set) var callAskCard = 0
    private(set) var callEditAddress = 0
    
    func askCard() {
        callAskCard += 1
    }
    
    func editAddress() {
        callEditAddress += 1
    }
}

private final class PPAuthManagerSpy: PPAuthSwiftContract {
    private(set) var callHandlePassword = 0
    
    func handlePassword() {
        callHandlePassword += 1
    }
}

final class DebitDeliveryConfirmAddressInteractorTests: XCTestCase {
    // MARK: - Variables
    private var presenterSpy = DeliveryConfirmAddressPresentingSpy()
    private let serviceSpy = DeliveryConfirmAddressServiceSpy()
    
    private let ppAuthManagerSpy = PPAuthManagerSpy()
    private var authManager: AuthContract = AuthManagerSpy()
    private var featureManager = FeatureManagerMock()
    
    private let deliveryEventsSpy = DeliveryEventsSpy()
    private let deliveryAddress = HomeAddress()
    
    private let contractURLDebit = "https://cdn.picpay.com/debito/oferta/Picpay-Debito-Contrato-de-Adesao.pdf"
    
    private lazy var sut: DeliveryConfirmAddressInteractor = {
        let container = DependencyContainerMock(authManager, featureManager, ppAuthManagerSpy)
        let sut = DeliveryConfirmAddressInteractor(service: serviceSpy,
                                                   presenter: presenterSpy,
                                                   deliveryAddress: deliveryAddress,
                                                   analyticsContext: deliveryEventsSpy,
                                                   dependencies: container)
        return sut
    }()
    
    private func getRequestError(in fileName: String) -> RequestError {
        // TODO: Remove forced unwrapped
        let trackingData = try! MockCodable<RequestError>().loadCodableObject(
            resource: fileName,
            typeDecoder: .useDefaultKeys)
        return trackingData
    }
    
    func testSetupScreen_WhenIsDebitFlowAndHasInitialValues_ShouldUpdatePresenter() {
        serviceSpy.flow = .debit
        serviceSpy.canChangeAddress = true
        sut.setupScreen()
        
        XCTAssertEqual(presenterSpy.flow, .debit)
        XCTAssertEqual(presenterSpy.homeAddress, deliveryAddress)
        XCTAssertEqual(presenterSpy.canChangeAddress, true)
        XCTAssertEqual(presenterSpy.isContractHidden, false)
        XCTAssertEqual(presenterSpy.isConfirmationEnable, false)
    }
    
    func testSetupScreen_WhenIsMultipleFlowAndHasInitialValues_ShouldUpdatePresenter() {
        serviceSpy.flow = .multiple
        serviceSpy.canChangeAddress = true
        sut.setupScreen()
        
        XCTAssertEqual(presenterSpy.flow, .multiple)
        XCTAssertEqual(presenterSpy.homeAddress, deliveryAddress)
        XCTAssertEqual(presenterSpy.canChangeAddress, true)
        XCTAssertEqual(presenterSpy.isContractHidden, true)
        XCTAssertEqual(presenterSpy.isConfirmationEnable, true)
    }
    
    func testSetupScreen_WhenIsUpgradeFlowAndHasInitialValues_ShouldUpdatePresenter() {
        serviceSpy.flow = .upgrade
        serviceSpy.canChangeAddress = true
        sut.setupScreen()
        
        XCTAssertEqual(presenterSpy.flow, .upgrade)
        XCTAssertEqual(presenterSpy.homeAddress, deliveryAddress)
        XCTAssertEqual(presenterSpy.canChangeAddress, true)
        XCTAssertEqual(presenterSpy.isContractHidden, true)
        XCTAssertEqual(presenterSpy.isConfirmationEnable, true)
    }
    
    func testSetupScreen_WhenIsReplacementFlowAndHasInitialValues_ShouldUpdatePresenter() {
        serviceSpy.flow = .replacement
        serviceSpy.canChangeAddress = false
        sut.setupScreen()
        
        XCTAssertEqual(presenterSpy.flow, .replacement)
        XCTAssertEqual(presenterSpy.homeAddress, deliveryAddress)
        XCTAssertEqual(presenterSpy.canChangeAddress, false)
        XCTAssertEqual(presenterSpy.isContractHidden, true)
        XCTAssertEqual(presenterSpy.isConfirmationEnable, true)
    }
    
    func testConfirm_WhenIsDebitAndReceiveSuccessFromService_ShouldPassAnActionToPresenter() {
        serviceSpy.canChangeAddress = true
        serviceSpy.isSuccess = true
        serviceSpy.registrationStatus = .activated
        sut.confirm()
        XCTAssertEqual(presenterSpy.registrationStatus, .activated)
        XCTAssertEqual(presenterSpy.callConfirm, 1)
        XCTAssertEqual(presenterSpy.callShowLoading, 1)
    }

    func testConfirm_WhenIsDebitAndReceiveFailureFromService_ShouldPassAnActionToPresenter() {
        serviceSpy.canChangeAddress = true
        serviceSpy.isSuccess = false
        sut.confirm()
        XCTAssertEqual(presenterSpy.callPresentError, 1)
        XCTAssertEqual(presenterSpy.callShowLoading, 1)
    }
    
    func testConfirm_WhenIsReplacementCardAndReceiveSuccessFromService_ShouldPassAnActionToPresenter() {
        serviceSpy.canChangeAddress = false
        serviceSpy.isSuccess = true
        serviceSpy.registrationStatus = .activated
        sut.confirm()
        XCTAssertEqual(presenterSpy.registrationStatus, .activated)
        XCTAssertEqual(presenterSpy.callConfirm, 1)
        XCTAssertEqual(presenterSpy.callShowLoading, 1)
    }
    
    func testConfirm_WhenIsReplacementCardAndReceiveFailureFromService_ShouldPassAnActionToPresenter() {
        serviceSpy.canChangeAddress = false
        serviceSpy.isSuccess = false
        sut.confirm()
        XCTAssertEqual(presenterSpy.callPresentError, 1)
        XCTAssertEqual(presenterSpy.callShowLoading, 1)
    }
    
    func testConfirm_WhenIsCreditAndReceiveSuccessFromService_ShouldPassAnActionToPresenter() {
        serviceSpy.canChangeAddress = true
        serviceSpy.isSuccess = true
        serviceSpy.registrationStatus = .activated
        sut.confirm()
        XCTAssertEqual(presenterSpy.registrationStatus, .activated)
        XCTAssertEqual(presenterSpy.callConfirm, 1)
        XCTAssertEqual(presenterSpy.callShowLoading, 1)
        XCTAssertEqual(deliveryEventsSpy.callAskCard, 1)
    }
    
    func testConfirm_WhenCalledFromViewModel_ShouldRequireAuthentication() {
        sut.confirm()
        XCTAssertEqual(presenterSpy.callShowLoading, 1)
    }
    
    func testClose_WhenPressCloseAction_ShouldCloseScreen() {
        sut.close()
        XCTAssertEqual(presenterSpy.callClose, 1)
    }

    func testChangeAddress_WhenCalledFromViewModel_ShouldPassAnActionToPresenter() {
        sut.changeAddress()
        XCTAssertEqual(presenterSpy.callChangedAddress, 1)
    }

    func testChangeAddress_WhenIsInvalid_ShouldRequireNewAddress() {
        sut.changeAddress()
        XCTAssertEqual(deliveryEventsSpy.callEditAddress, 1)
        XCTAssertEqual(presenterSpy.callChangedAddress, 1)
    }

    func testCheckContract_ShouldToggleAndPresent() {
        sut.checkContract()
        XCTAssertEqual(presenterSpy.callPresentContractCheck, 1)
    }

    func testReadContract_WhenContractsAreValid_ShouldPresent() {
        featureManager.override(keys: .linkContractDebitCard, with: contractURLDebit)
        sut.readContract()
        XCTAssertEqual(presenterSpy.callPresentContractReader, 1)
        XCTAssertEqual(presenterSpy.contractURL?.absoluteString, contractURLDebit)
    }

    func testReadContract_WhenContracsAreInvalid_ShouldPresentError() {
        featureManager.override(keys: .linkContractDebitCard, with: "")
        featureManager.override(keys: .termsMultipleLink, with: "")
        sut.readContract()
        XCTAssertEqual(presenterSpy.callPresentGenericError, 1)
    }
}
