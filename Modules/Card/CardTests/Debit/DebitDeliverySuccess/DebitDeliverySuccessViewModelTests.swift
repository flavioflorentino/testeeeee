import XCTest
import AnalyticsModule
@testable import Card

private final class DebitDeliverySuccessPresentingSpy: DebitDeliverySuccessPresenting {
    
    var viewController: DebitDeliverySuccessDisplaying?
    private (set) var action: DebitDeliverySuccessAction?
    private (set) var didNextStepCount = 0
    
    func didNextStep(action: DebitDeliverySuccessAction) {
        self.action = action
        self.didNextStepCount += 1
    }
    
    func loadDescription(text: String) { }
}

final class DebitDeliverySuccessViewModelTests: XCTestCase {
    
    private var presenterSpy = DebitDeliverySuccessPresentingSpy()
    private lazy var analytics = AnalyticsSpy()
    private lazy var container = DependencyContainerMock(analytics)
    private lazy var sut = DebitDeliverySuccessViewModel(presenter: presenterSpy, dependencies: container)
    
    func testConfirm_WhenSelectConfirm_ShouldActGoToDidConfirmAndSendEvent() {
        sut.didConfirm()
        let expectedEvent = DebitDeliverySuccessEvent.didSelectConfirm.event()
        XCTAssertEqual(presenterSpy.didNextStepCount, 1)
        XCTAssertEqual(presenterSpy.action, DebitDeliverySuccessAction.didConfirm)
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testAnalytics_WhenViewDidAppear_ShouldSendEvent() {
        sut.trackScreenView()
        let expectedEvent = DebitDeliverySuccessEvent.didViewScreen.event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
}
