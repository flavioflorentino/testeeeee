import Core
import XCTest
import Validator
import AnalyticsModule
import FeatureFlag
@testable import Card

private final class DebitDeliveryAddressPresentingSpy: DebitDeliveryAddressPresenting {
    // MARK: - Variables
    var viewController: DebitDeliveryAddressDisplay?
    
    private(set) var action: DebitDeliveryAddressAction?
    private(set) var addressSearch: HomeAddress?
    private(set) var cardForm: CardForm?
    private(set) var callShowError = 0
    private(set) var callShowPostalCodeError = 0
    
    
    private(set) var callPostalCodeError = 0
    private(set) var callStreetError = 0
    private(set) var callNeighborhoodError = 0
    private(set) var callStreetNumberError = 0
    private(set) var callComplementError = 0
    private(set) var presentStateFieldWithErrorMessageCallsCount = 0
    private(set) var presentStateFieldWithErrorMessageReceivedInvocations: [String?] = []
    private(set) var enableStateAndCityEditionPostalCodeIsValidStateCityCallsCount = 0
    private(set) var enableStateAndCityEditionPostalCodeIsValidStateCityReceivedInvocations: [(postalCodeIsValid: Bool, state: String, city: String)] = []
    private(set) var presentCityFieldWithErrorMessageCallsCount = 0
    private(set) var presentCityFieldWithErrorMessageReceivedInvocations: [String?] = []
    private(set) var callPresentPostalCodeError = 0
    private(set) var callPresentPostalCodeErrorWithApiErro: ApiError?
    private(set) var callPresentState = 0
    private(set) var callPresentStateWithState: DebitDeliveryAddressPostalCodeState?
    private(set) var callPresentAlertwarning = 0
    private(set) var callPresentSendAddress = 0
    private(set) var callPresentLoading = 0
    private(set) var callStartAddressCompletionAnimation = 0
    private(set) var startAddressCompletionAnimationWithHomeAddress: HomeAddress?

    // MARK: - Public Methods
    
    func didNextStep(action: DebitDeliveryAddressAction) {
        self.action = action
    }
    
    func setAddressInController(with addressSearch: HomeAddress) {
        self.addressSearch = addressSearch
    }
    
    func fillDebitAddressForm(with cardForm: CardForm) {
        self.cardForm = cardForm
    }
    
    func showError(errorType: DebitDeliveryErrorType) {
        callShowError += 1
    }
    
    func didPostalCodeError() {
        callShowPostalCodeError += 1
    }
    
    func presentPostalCodeFieldWith(errorMessage: String?) {
        callPostalCodeError += 1
    }
    
    func presentStreetFieldWith(errorMessage: String?) {
        callStreetError += 1
    }
    
    func presentNeighborhoodFieldWith(errorMessage: String?) {
        callNeighborhoodError += 1
    }
    
    func presentStreetNumberFieldWith(errorMessage: String?) {
        callStreetNumberError += 1
    }
    
    func presentComplementFieldWith(errorMessage: String?) {
        callComplementError += 1
    }
    
    func presentStateFieldWith(errorMessage: String?) {
        presentStateFieldWithErrorMessageCallsCount += 1
        presentStateFieldWithErrorMessageReceivedInvocations.append(errorMessage)
    }

    func presentCityFieldWith(errorMessage: String?) {
        presentCityFieldWithErrorMessageCallsCount += 1
        presentCityFieldWithErrorMessageReceivedInvocations.append(errorMessage)
    }

    func enableStateAndCityEdition(postalCodeIsValid: Bool, state: String, city: String) {
        enableStateAndCityEditionPostalCodeIsValidStateCityCallsCount += 1
        enableStateAndCityEditionPostalCodeIsValidStateCityReceivedInvocations.append((postalCodeIsValid: postalCodeIsValid, state: state, city: city))
    }

    func presentPostalCodeError(_ error: ApiError) {
        callPresentPostalCodeError += 1
        callPresentPostalCodeErrorWithApiErro = error
    }

    func presentState(state: DebitDeliveryAddressPostalCodeState) {
        callPresentState += 1
        callPresentState += 1
    }

    func presentAlertWarning() {
        callPresentAlertwarning += 1
    }

    func presentSendAddress() {
        callPresentSendAddress += 1
    }

    func presentLoading() {
        callPresentLoading += 1
    }

    func startAddressCompletionAnimation(with addressSearch: HomeAddress) {
        callStartAddressCompletionAnimation += 1
        startAddressCompletionAnimationWithHomeAddress = addressSearch
    }
}

private final class DebitDeliveryAddressServiceSpy: DebitDeliveryAddressServicing {
    
    // MARK: - Variables
    private(set) var updateModelCalledCount = 0
    public var result: Result<HomeAddress, ApiError>!
    
    // MARK: - Public Methods
    func addressBy(_ cep: String, then completion: @escaping (Result<HomeAddress, ApiError>) -> Void) {
        completion(result)
    }
    
    func update(cardForm: CardForm, then completion: CompletionAddressUpdate) {
        updateModelCalledCount += 1
    }
}

final class DebitDeliveryAddressViewModelTests: XCTestCase {
    // MARK: - Variables
    private var presenterSpy = DebitDeliveryAddressPresentingSpy()
    private let serviceSpy = DebitDeliveryAddressServiceSpy()
    private lazy var analyticsSpy = AnalyticsSpy()
    private lazy var featureManagerSpy = FeatureManagerMock()
    private lazy var container = DependencyContainerMock(analyticsSpy, featureManagerSpy)
    
    private var cardForm: CardForm {
        let cardForm = try! MockCodable<CardForm>().loadCodableObject(
            resource: "debitForm",
            typeDecoder: .useDefaultKeys)
        return cardForm
    }
    private var formsData: DeliveryAddressFormsData {
        return DeliveryAddressFormsData(
            postalCode: "88888888",
            streetName: "Rua Bela Vista",
            neighborhood: "Centro",
            streetNumber: "1234",
            complement: "Centro",
            city: "Curitiba",
            cityId: "124",
            state: "PR",
            stateId: "1245",
            noNumberButtonStatus: true)
    }
    private var deliveryDebitAddress: HomeAddress {
        let cardForm = try! MockCodable<HomeAddress>().loadCodableObject(
            resource: "deliveryDebitAddress",
            typeDecoder: .convertFromSnakeCase)
        return cardForm
    }
    private lazy var sut = DebitDeliveryAddressViewModel(service: serviceSpy, presenter: presenterSpy, dependencies: container, cardForm: cardForm)
    
    // MARK: - Public Methods
    func testSetupInitialForm_WhenHasInicialValues_ShouldUpdatePresenterDebitForm() {
        sut.viewDidLoad()
        XCTAssertNotNil(presenterSpy.cardForm)
    }
    
    func testAddressBy_WhenUserTypesPostalCode_ShouldReturnAddressSearch() {
        featureManagerSpy.override(keys: .isNewCardAddressScreenAvailable, with: false)
        serviceSpy.result = Result.success(deliveryDebitAddress)
        sut.addressBy("88888888")
        XCTAssertEqual(presenterSpy.addressSearch?.city, "Curitiba")
        XCTAssertEqual(presenterSpy.addressSearch?.state, "PR")
        XCTAssertEqual(presenterSpy.addressSearch?.zipCode, "88888888")
    }
    
    func testAddressByFailure_WhenUserTypesPostalCode_ShouldntReturnAddressSearch() {
        featureManagerSpy.override(keys: .isNewCardAddressScreenAvailable, with: false)
        serviceSpy.result = Result.failure(ApiError.bodyNotFound)
        sut.addressBy("88888888")
        XCTAssertNil(presenterSpy.addressSearch)
    }
    
    func testSendFormsData_WhenUserConfirmForms_ShouldntSyncModel() {
        sut.sendFormsData(formData: formsData)
        XCTAssertEqual(serviceSpy.updateModelCalledCount, 1)
    }
    
    func testAddressBy_WhenHasInvalidCEP_ShouldReturnErrorMessage() {
        sut.checkPostalCodeFieldWith(validationResult: ValidationResult.invalid([]))
        XCTAssertEqual(presenterSpy.callPostalCodeError, 1)
    }
    
    func testAddressBy_WhenHasInvalidStreet_ShouldReturnErrorMessage() {
        sut.checkStreetFieldWith(validationResult: ValidationResult.invalid([]))
        XCTAssertEqual(presenterSpy.callStreetError, 1)
    }
    
    func testAddressBy_WhenHasInvalidNeighborhood_ShouldReturnErrorMessage() {
        sut.checkNeighborhoodFieldWith(validationResult: ValidationResult.invalid([]))
        XCTAssertEqual(presenterSpy.callNeighborhoodError, 1)
    }
    
    func testAddressBy_WhenHasInvalidStreetNumber_ShouldReturnErrorMessage() {
        sut.checkStreetNumberFieldWith(validationResult: ValidationResult.invalid([]))
        XCTAssertEqual(presenterSpy.callStreetNumberError, 1)
    }
    
    func testAddressBy_WhenHasInvalidComplement_ShouldReturnErrorMessage() {
        sut.checkComplementFieldWith(validationResult: ValidationResult.invalid([]))
        XCTAssertEqual(presenterSpy.callComplementError, 1)
    }
    
    func testEnableStateAndCityEdition_WhenHasNotValid_ShouldEnableEdition() {
        sut.enableStateAndCityEdition(postalCodeIsValid: false, state: "State", city: "")
        XCTAssertEqual(presenterSpy.enableStateAndCityEditionPostalCodeIsValidStateCityCallsCount, 1)
    }

    func testOpenPostOffices_WhenItsCalled_ShouldSendAnActionOnPresenter() throws {
        let url = try XCTUnwrap(URL(string: "https://buscacepinter.correios.com.br/app/endereco/index.php?t"))
        sut.openPostOffices()
        XCTAssertEqual(presenterSpy.action, .openNavigation(url))
    }

    func testResetToBeginState_WhenItsCalled_ShouldSendAnActionOnPresenter() {
        sut.resetToBeginState()
        XCTAssertEqual(sut.state, .initial)
        XCTAssertEqual(sut.cardForm.homeAddress, nil)
    }

    func testValidationAddressConfirm_WhenHasAVoidForm_ShouldTrackDidTapNoNumberEventWithTrue() {
        sut.validationAddressConfirm(formData: DeliveryAddressFormsData(postalCode: nil,
                                                                        streetName: nil,
                                                                        neighborhood: nil,
                                                                        streetNumber: nil,
                                                                        complement: nil,
                                                                        city: nil,
                                                                        cityId: nil,
                                                                        state: nil,
                                                                        stateId: nil,
                                                                        noNumberButtonStatus: false))

        XCTAssertEqual(presenterSpy.callPresentLoading, 1)
    }

    func testTrackingNoNumber_WhenHasPassadTrueAsParameter_ShouldTrackDidTapNoNumberEventWithTrue() {
        sut.trackingNoNumber(checked: true)

        let expectedEvent = CardDeliveryAddressEvent.didTapNoNumber(checked: true).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }

    func testTrackingNoNumber_WhenHasPassadFalseAsParameter_ShouldTrackDidTapNoNumberEventWithFalse() {
        sut.trackingNoNumber(checked: false)

        let expectedEvent = CardDeliveryAddressEvent.didTapNoNumber(checked: false).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }

    func testViewWillPop_WhenItsCalled_ShouldTrackDidTapBackEvent() {
        sut.viewWillPop()

        let expectedEvent = CardDeliveryAddressEvent.didTapBack.event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
}
