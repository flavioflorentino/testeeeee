import XCTest
import AssetsKit
@testable import Card

private final class DebitDeliveryAddressViewControllerSpy: DebitDeliveryAddressDisplay {
    // MARK: - Variables
    private(set) var addressSearch: HomeAddress?
    private(set) var addressFormData: DeliveryAddressFormsData?
    private(set) var errorMessage: String?
    private(set) var callDismissIndicator = 0
    private(set) var callShowPostalCodeError = 0
    private(set) var callPostalCodeError = 0
    private(set) var callStreetError = 0
    private(set) var callNeighborhoodError = 0
    private(set) var callStreetNumberError = 0
    private(set) var callComplementError = 0
    private(set) var displayCityFieldErrorMessageCallsCount = 0
    private(set) var displayCityFieldErrorMessageReceivedInvocations: [String?] = []
    private(set) var displayStateFieldErrorMessageCallsCount = 0
    private(set) var displayStateFieldErrorMessageReceivedInvocations: [String?] = []
    private(set) var enableStateAndCityEditionEnableCallsCount = 0
    private(set) var enableStateAndCityEditionEnableReceivedInvocations: [Bool] = []
    private(set) var callDisplayPostCodeStep = 0
    private(set) var callDisplayFilledState = 0
    private(set) var callDisplayAlertwarningWithModel: DebitDeliveryAlertModel?
    private(set) var callDisplaySendAddress = 0

    // MARK: - Tests
    func setAddressInController(with addressSearch: HomeAddress) {
        self.addressSearch = addressSearch
    }
    
    func fillAddressForm(with addressFormData: DeliveryAddressFormsData) {
        self.addressFormData = addressFormData
    }
    
    func showError(with errorData: DebitDeliveryErrorData) {
        self.errorMessage = errorData.title
    }
    
    func dismissLoading() {
        callDismissIndicator += 1
    }
    
    func didPostalCodeError() {
        callShowPostalCodeError += 1
    }
    
    func displayPostalCodeFieldErrorMessage(_ message: String?) {
        callPostalCodeError += 1
    }
    
    func displayStreetFieldErrorMessage(_ message: String?) {
        callStreetError += 1
    }
    
    func displayNeighborhoodFieldErrorMessage(_ message: String?) {
        callNeighborhoodError += 1
    }
    
    func displayStreetNumberFieldErrorMessage(_ message: String?) {
        callStreetNumberError += 1
    }
    
    func displayComplementFieldErrorMessage(_ message: String?) {
        callComplementError += 1
    }
    
    func displayCityFieldErrorMessage(_ message: String?) {
        displayCityFieldErrorMessageCallsCount += 1
        displayCityFieldErrorMessageReceivedInvocations.append(message)
    }

    func displayStateFieldErrorMessage(_ message: String?) {
        displayStateFieldErrorMessageCallsCount += 1
        displayStateFieldErrorMessageReceivedInvocations.append(message)
    }

    func enableStateAndCityEdition(enable: Bool) {
        enableStateAndCityEditionEnableCallsCount += 1
        enableStateAndCityEditionEnableReceivedInvocations.append(enable)
    }

    func displayPostCodeStep() {
        callDisplayPostCodeStep += 1
    }

    func displayFilledState() {
        callDisplayFilledState += 1
    }

    func displayAlertWarning(_ model: DebitDeliveryAlertModel) {
        callDisplayAlertwarningWithModel = model
    }

    func displaySendAddress() {
        callDisplaySendAddress += 1
    }

    func displayLoading() {

    }

    func displayAlertwarning(_ model: DebitDeliveryAlertModel) {

    }

    func displayAddressCompletionAnimation(finished: @escaping () -> Void) {
        
    }
}

private final class DebitDeliveryAddressCoordinatorSpy: DebitDeliveryAddressCoordinating {
    
    // MARK: - Variables
    var viewController: UIViewController?

    private(set) var actionSelected: DebitDeliveryAddressAction?

    // MARK: - Public Methods
    func perform(action: DebitDeliveryAddressAction) {
        actionSelected = action
    }
}

final class DebitDeliveryAddressPresenterTests: XCTestCase {
    // MARK: - Variables
    private var viewControllerSpy = DebitDeliveryAddressViewControllerSpy()
    private let coordinatorSpy = DebitDeliveryAddressCoordinatorSpy()
    private let container = DependencyContainerMock()

    private lazy var sut: DebitDeliveryAddressPresenter = {
        let sut = DebitDeliveryAddressPresenter(coordinator: coordinatorSpy, dependencies: container)
        sut.viewController = viewControllerSpy
        return sut
    }()
    
    private func getCardForm(in fileName: String) -> CardForm {
        let cardForm = try! MockCodable<CardForm>().loadCodableObject(
            resource: fileName,
            typeDecoder: .convertFromSnakeCase)
        return cardForm
    }
    
    private var formsData: DeliveryAddressFormsData {
        return DeliveryAddressFormsData(
            postalCode: "88888888",
            streetName: "Rua Bela Vista",
            neighborhood: "Centro",
            streetNumber: "1234",
            complement: "Centro",
            city: "Curitiba",
            cityId: "124",
            state: "PR",
            stateId: "1245",
            noNumberButtonStatus: true)
    }
    private var deliveryDebitAddress: HomeAddress {
        let cardForm = try! MockCodable<HomeAddress>().loadCodableObject(
            resource: "deliveryDebitAddress",
            typeDecoder: .convertFromSnakeCase)
        return cardForm
    }

    // MARK: - Tests
    func testSetAddressInController_WhenUserSearchedCEP_ShouldDisplayAddressSearch() {
        sut.setAddressInController(with: deliveryDebitAddress)
        XCTAssertEqual(viewControllerSpy.addressSearch?.city, "Curitiba")
        XCTAssertEqual(viewControllerSpy.addressSearch?.state, "PR")
        XCTAssertEqual(viewControllerSpy.addressSearch?.zipCode, "88888888")
    }
    
    func testFillDebitAddressForm_WhenHasInicialValues_ShouldDisplayFormsData() {
        sut.fillDebitAddressForm(with: getCardForm(in: "debitForm"))
        XCTAssertEqual(viewControllerSpy.addressFormData?.city, "Sao Paulo")
        XCTAssertEqual(viewControllerSpy.addressFormData?.state, "SP")
    }
    
    func testFillDebitAddressForm_WhenHasInicialValues_ShouldFormattingCEP() {
        sut.fillDebitAddressForm(with: getCardForm(in: "debitForm"))
        XCTAssertEqual(viewControllerSpy.addressFormData?.postalCode, "01225-000")
    }
    
    func testFillForm_WhenHasValuesWithIncorrectCEP_ShouldReturnEmptyPostalCode() {
        sut.fillDebitAddressForm(with: getCardForm(in: "debitFormIncorrectZIP"))
        XCTAssertEqual(viewControllerSpy.addressFormData?.postalCode, "")
    }
    
    func testFillDebitAddressForm_WhenHasInvalidCEP_ShouldReturnErrorMessage() {
        sut.presentPostalCodeFieldWith(errorMessage: "Campo invalido")
        XCTAssertEqual(viewControllerSpy.callPostalCodeError, 1)
    }
    
    func testFillDebitAddressForm_WhenHasInvalidStreet_ShouldReturnErrorMessage() {
        sut.presentStreetFieldWith(errorMessage: "Campo invalido")
        XCTAssertEqual(viewControllerSpy.callStreetError, 1)
    }
    
    func testFillDebitAddressForm_WhenHasInvalidNeighborhood_ShouldReturnErrorMessage() {
        sut.presentNeighborhoodFieldWith(errorMessage: "Campo invalido")
        XCTAssertEqual(viewControllerSpy.callNeighborhoodError, 1)
    }
    
    func testFillDebitAddressForm_WhenHasInvalidStreetNumber_ShouldReturnErrorMessage() {
        sut.presentStreetNumberFieldWith(errorMessage: "Campo invalido")
        XCTAssertEqual(viewControllerSpy.callStreetNumberError, 1)
    }
    
    func testFillDebitAddressForm_WhenHasInvalidComplement_ShouldReturnErrorMessage() {
        sut.presentComplementFieldWith(errorMessage: "Campo invalido")
        XCTAssertEqual(viewControllerSpy.callComplementError, 1)
    }
    
    func testEnableStateAndCityEdition_WhenHasValidPostCodeAndCity_ShouldEnableEdition() {
        sut.enableStateAndCityEdition(postalCodeIsValid: true, state: "", city: "city")
        XCTAssertEqual(viewControllerSpy.enableStateAndCityEditionEnableReceivedInvocations.last, true)
    }
    
    func testEnableStateAndCityEdition_WhenHasValidPostCodeAndState_ShouldEnableEdition() {
        sut.enableStateAndCityEdition(postalCodeIsValid: true, state: "State", city: "")
        XCTAssertEqual(viewControllerSpy.enableStateAndCityEditionEnableReceivedInvocations.last, true)
    }
    
    func testEnableStateAndCityEdition_WhenHasNotValid_ShouldEnableEdition() {
        sut.enableStateAndCityEdition(postalCodeIsValid: false, state: "State", city: "")
        XCTAssertEqual(viewControllerSpy.enableStateAndCityEditionEnableReceivedInvocations.last, false)
    }

    func testShowError_WhenHasAConnectionError_ShouldHaveTheFollowingMessage() {
        sut.showError(errorType: .connection)
        XCTAssertEqual(viewControllerSpy.errorMessage, "Sem conexão")
    }

    func testShowError_WhenHasAGeneralError_ShouldHaveTheFollowingMessage() {
        sut.showError(errorType: .general)
        XCTAssertEqual(viewControllerSpy.errorMessage, "Algo deu errado")
    }

    func testPresentAlertwarning_WhenItIsCalled_ShouldHaveTheFollowingModel() {
        sut.presentAlertWarning()
        XCTAssertEqual(viewControllerSpy.callDisplayAlertwarningWithModel?.title, "Alteração de endereço")
        XCTAssertEqual(viewControllerSpy.callDisplayAlertwarningWithModel?.descriptionMessage, "Você alterou o endereço do CEP que está registrado no site dos Correios. A alteração está correta?")
        XCTAssertEqual(viewControllerSpy.callDisplayAlertwarningWithModel?.primaryButtonTitle, "Sim, está correta")
        XCTAssertEqual(viewControllerSpy.callDisplayAlertwarningWithModel?.secondaryButtonTitle, "Voltar")
        XCTAssertEqual(viewControllerSpy.callDisplayAlertwarningWithModel?.image, AssetsKit.Resources.Illustrations.iluWarning.image)
    }

    func testPresentStateFieldWith_WhenHasAStringPassedAsParameter_ShouldHaveTheFollowingState() {
        sut.presentStateFieldWith(errorMessage: "error message state")
        XCTAssertEqual(viewControllerSpy.displayStateFieldErrorMessageCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.displayStateFieldErrorMessageReceivedInvocations[0], "error message state")
    }

    func testPresentCityFieldWith_WhenHasAStringPassedAsParameter_ShouldHaveTheFollowingState() {
        sut.presentCityFieldWith(errorMessage: "error message city")
        XCTAssertEqual(viewControllerSpy.displayCityFieldErrorMessageCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.displayCityFieldErrorMessageReceivedInvocations[0], "error message city")
    }

    func testDidNextStep_WhenHasANewCardFormAsParameter_ShouldHaveCallDismissOnViewController() {
        sut.didNextStep(action: .didConfirm(cardForm: CardForm()))
        XCTAssertEqual(viewControllerSpy.callDismissIndicator, 1)
    }
}
