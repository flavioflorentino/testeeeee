import XCTest
import AnalyticsModule
@testable import Card

private final class DebitPersonalInfoPresentingSpy: DebitPersonalInfoPresenting {
    // MARK: - Variables
    var viewController: DebitPersonalInfoDisplay?
    
    private(set) var action: DebitPersonalInfoAction?
    private(set) var validText: String?
    private(set) var errorMessage: String?
    private(set) var enableButton: Bool?
    
    // MARK: - Public Methods
    func didNextStep(action: DebitPersonalInfoAction) {
        self.action = action
    }
    
    func updateMotherNameTextField(validText: String) {
        self.validText = validText
    }
    
    func updateTextFieldErrorMessage(message: String?) {
        self.errorMessage = message
    }
    
    func shouldEnableContinueButton(enable: Bool) {
        self.enableButton = enable
    }
}

private final class DebitPersonalInfoServiceSpy: DebitPersonalInfoServicing {
    // MARK: - Variables
    private(set) var syncModelCalledCount = 0
    
    // MARK: - Public Methods
    func syncModel(cardForm: CardForm) {
        syncModelCalledCount += 1
    }
}

final class DebitPersonalDebitViewModelTests: XCTestCase {
    // MARK: - Variables
    private var presenterSpy = DebitPersonalInfoPresentingSpy()
    private let serviceSpy = DebitPersonalInfoServiceSpy()
    private lazy var analytics = AnalyticsSpy()
    private lazy var container = DependencyContainerMock(analytics)
    
    private var cardForm: CardForm = {
        var cardForm = CardForm()
        cardForm.mothersName = "Juliana"
        return cardForm
    }()
    private lazy var sut = DebitPersonalInfoViewModel(service: serviceSpy, presenter: presenterSpy, dependencies: container, cardForm: cardForm)
    
    // MARK: - Public Methods
    func testSetupInitialForm_WhenHasInicialValues_ShouldUpdateText() {
        sut.setupInitialForm()
        XCTAssertEqual(presenterSpy.validText, "Juliana")
        XCTAssertNil(presenterSpy.errorMessage)
    }
    
    func testSanitizedMothersName_WhenMothersNameIsNil_ShouldIgnoreText() {
        sut.sanitizedMothersName(with: nil)
        XCTAssertNil(presenterSpy.validText)
    }
    
    func testSanitizedMothersName_WhenMothersNameContainsInvalidCharacter_ShouldSanitizeText() {
        sut.sanitizedMothersName(with: "Ju!1li@ana")
        XCTAssertEqual(presenterSpy.validText, "Juliana")
    }
    
    func testCheckMothersName_WhenMothersNameIsNil_ShouldIgnoreCheck() {
        sut.checkMothersName(text: nil)
        XCTAssertNil(presenterSpy.validText)
    }
    
    func testCheckMothersName_WhenMothersNameIsValid_ShouldNotShowErrorMessage() {
        sut.checkMothersName(text: "Juliana")
        XCTAssertNil(presenterSpy.errorMessage)
    }
    
    func testCheckMothersName_WhenMothersNameIsEmpty_ShouldShowErrorMessage() {
        sut.checkMothersName(text: "")
        XCTAssertEqual(presenterSpy.errorMessage, "Nome da mãe obrigatório")
    }
    
    func testValidateAndEnableContinue_WhenMothersNameIsEmpty_ShouldDisableButton() {
        sut.validateAndEnableContinue(text: "")
        XCTAssertEqual(presenterSpy.enableButton, false)
    }
    
    func testValidateAndEnableContinue_WhenMothersNameIsValid_ShouldEnableButton() {
        sut.validateAndEnableContinue(text: "Juliana")
        XCTAssertEqual(presenterSpy.enableButton, true)
    }
    
    func testConfirm_WhenMothersNameIsValid_ShouldActGoToAddress() {
        sut.confirm(motherName: "Juliana")
        let cardForm = CardForm()
        let expectedEvent = DebitPersonalInfoEvent.didSelectContinue.event()
        XCTAssertEqual(presenterSpy.action, DebitPersonalInfoAction.confirm(cardForm: cardForm))
        XCTAssertEqual(serviceSpy.syncModelCalledCount, 1)
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testConfirm_WhenMothersNameIsNil_ShouldIgnoreResult() {
        sut.confirm(motherName: nil)
        XCTAssertNil(presenterSpy.action)
    }
    
    func testAnalytics_WhenViewDidAppear_ShouldSendEvent() {
        sut.trackScreenView()
        let expectedEvent = DebitPersonalInfoEvent.didViewScreen.event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
}

extension DebitPersonalInfoAction: Equatable {
    public static func == (lhs: DebitPersonalInfoAction, rhs: DebitPersonalInfoAction) -> Bool {
        switch (lhs, rhs) {
        case (.confirm, .confirm):
            return true
        case (.close, _):
            return true
        case (_, .close):
            return true
        }
    }
}
