import XCTest
@testable import Card

private final class DebitPersonalInfoViewControllerSpy: DebitPersonalInfoDisplay {
    // MARK: - Variables
    private(set) var errorMessage: String?
    private(set) var enableButton: Bool?
    private(set) var mothersNameTextField: String?
    
    // MARK: - Tests
    func updateTextFieldErrorMessage(message: String?) {
        errorMessage = message
    }
    
    func enableContinueButton(enable: Bool) {
        enableButton = enable
    }

    func updateMothersNameTextFieldWith(string: String) {
        mothersNameTextField = string
    }
}

private final class DebitPersonalInfoCoordinatorSpy: DebitPersonalInfoCoordinating {
    // MARK: - Variables
    var viewController: UIViewController?
    var delegate: DebitPersonalInfoCoordinatorDelegate?
    
    private(set) var actionSelected: DebitPersonalInfoAction?
    
    // MARK: - Public Methods
    func perform(action: DebitPersonalInfoAction) {
        actionSelected = action
    }
}

final class DebitPersonalInfoPresenterTests: XCTestCase {
    // MARK: - Variables
    private var viewControllerSpy = DebitPersonalInfoViewControllerSpy()
    private let coordinatorSpy = DebitPersonalInfoCoordinatorSpy()
    private let container = DependencyContainerMock()
        
    private lazy var sut: DebitPersonalInfoPresenter = {
        let sut = DebitPersonalInfoPresenter(coordinator: coordinatorSpy, dependencies: container)
        sut.viewController = viewControllerSpy
        return sut
    }()

    // MARK: - Tests
    func testUpdateTextFieldErrorMessage_WhenErrorMessage_ShouldPresentError() {
        let errorMessage = "Nome da mãe obrigatório"
        sut.updateTextFieldErrorMessage(message: errorMessage)
        XCTAssertEqual(viewControllerSpy.errorMessage, errorMessage)
    }
    
    func testUpdateTextFieldErrorMessage_WhenErrorMessageIsNil_ShouldNotPresentError() {
        sut.updateTextFieldErrorMessage(message: nil)
        XCTAssertNil(viewControllerSpy.errorMessage)
    }
    
    func testShouldEnableContinueButton_WhenCanContinue_ShouldEnableButton() {
        sut.shouldEnableContinueButton(enable: true)
        XCTAssertEqual(viewControllerSpy.enableButton, true)
    }
    
    func testUpdateMotherNameTextField_WhenIsValidMothersName_ShouldUpdateTextField() {
        let motherName = "Juliana"
        sut.updateMotherNameTextField(validText: motherName)
        XCTAssertEqual(viewControllerSpy.mothersNameTextField, motherName)
    }
    
    func testDidNextStep_WhenPressContinueAction_ShouldChangeScreen() {
        let cardForm = CardForm()
        let action: DebitPersonalInfoAction = .confirm(cardForm: cardForm)
        sut.didNextStep(action: action)
        XCTAssertEqual(coordinatorSpy.actionSelected, action)
    }
}
