import XCTest
@testable import Card

private class ActivateCardFlowCoordinatorDelegateSpy: ActivateCardFlowCoordinatorDelegate {
    private(set) var didOpenWalletCallsCount = 0
    private(set) var didFinishFlowCallsCount = 0

    func didOpenWallet() {
        didOpenWalletCallsCount += 1
    }

    func didFinishFlow() {
        didFinishFlowCallsCount += 1
    }
}

final class ActivateCardSuccessCoordinatorTests: XCTestCase {
    private let delegateSpy = ActivateCardFlowCoordinatorDelegateSpy()
    
    private lazy var sut: ActivateCardSuccessCordinating = {
        let coordinator = ActivateCardSuccessCoordinator()
        coordinator.delegate = delegateSpy
        return coordinator
    }()
    
    func testDidTapAddMoneyToWallet_WhenCalledFromCoordinator_ShouldPassAnActionToDelegate() {
        sut.perform(action: .addMoneyToWallet)
        XCTAssertEqual(delegateSpy.didOpenWalletCallsCount, 1)
    }
    
    func testDidTapBackToStart_WhenCalledFromCoordinator_ShouldPassAnActionToDelegate() {
        sut.perform(action: .backToStart)
        XCTAssertEqual(delegateSpy.didFinishFlowCallsCount, 1)
     }
}
