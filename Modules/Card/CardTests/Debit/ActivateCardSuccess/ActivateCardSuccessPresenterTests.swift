import XCTest
@testable import Card

private final class ActivateCardSuccessCordinatingSpy: ActivateCardSuccessCordinating {
    var delegate: ActivateCardFlowCoordinatorDelegate?

    //MARK: - perform
    private(set) var performActionCallsCount = 0
    private(set) var performActionReceivedInvocations: [ActivateCardSuccessAction] = []

    func perform(action: ActivateCardSuccessAction) {
        performActionCallsCount += 1
        performActionReceivedInvocations.append(action)
    }
}

final class ActivateCardSuccessPresenterTests: XCTestCase {
    private let coordinatorSpy = ActivateCardSuccessCordinatingSpy()
    
    private lazy var sut: ActivateCardSuccessPresenting = {
        let sut = ActivateCardSuccessPresenter(coordinator: coordinatorSpy)
        return sut
    }()

    func testDidTapAddMoneyToWallet_WhenCalledFromCoordinator_ShouldPassAnActionToDelegate() {
        sut.addMoneyToWallet()
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.performActionReceivedInvocations.last, .addMoneyToWallet)
    }

    func testDidTapBackToStart_WhenCalledFromCoordinator_ShouldPassAnActionToDelegate() {
        sut.backToStart()
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.performActionReceivedInvocations.last, .backToStart)
    }
}
