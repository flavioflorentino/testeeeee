import AnalyticsModule
import Core
import XCTest
@testable import Card

private final class ActivateCardSuccessPresentingSpy: ActivateCardSuccessPresenting {
    private(set) var backToStartCallsCount = 0
    private(set) var addMoneyToWalletCallsCount = 0

    func backToStart() {
        backToStartCallsCount += 1
    }

    func addMoneyToWallet() {
        addMoneyToWalletCallsCount += 1
    }
}

final class ActivateCardSuccessViewModelTests: XCTestCase {
    // MARK: - Variables
    private var presenterSpy = ActivateCardSuccessPresentingSpy()
    private lazy var analytics = AnalyticsSpy()
    private lazy var container = DependencyContainerMock(analytics)
    
    private lazy var sut = ActivateCardSuccessViewModel(presenter: presenterSpy, dependencies: container)
    
    func testDidTapAddMoneyToWallet_WhenCalledFromViewModel_ShouldPassAnActionToPresenter() {
        sut.addMoneyToWallet()
        let expectedEvent = ActivateCardSuccessEvent.didAddMoney.event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
        XCTAssertEqual(presenterSpy.addMoneyToWalletCallsCount, 1)
    }
    
    func testDidTapBackToStart_WhenCalledFromViewModel_ShouldPassAnActionToPresenter() {
        sut.backToStart()
        let expectedEvent = ActivateCardSuccessEvent.didBackHome.event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
        XCTAssertEqual(presenterSpy.backToStartCallsCount, 1)
     }
}
