import UIKit
import UI

public class CircleView: UIView {
    private var desiredLineWidth: CGFloat = 1
    private var strokeColor: CGColor = Palette.black.cgColor
    
    convenience init(
        desiredLineWidth: CGFloat,
        strokeColor: CGColor
    ) {
        self.init(frame: CGRect.zero)
        self.desiredLineWidth = desiredLineWidth
        self.strokeColor = strokeColor
    }

    override init(frame: CGRect) {
         super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
         super.init(coder: aDecoder)
    }
    
    override public func draw(_ rect: CGRect) {
        drawRingFittingInsideView()
    }
    
    internal func drawRingFittingInsideView() {
        let halfSize: CGFloat = min( bounds.size.width / 2, bounds.size.height / 2)
        
        let circlePath = UIBezierPath(
            arcCenter: CGPoint(x: halfSize, y: halfSize),
            radius: CGFloat(halfSize - (desiredLineWidth / 2)),
            startAngle: CGFloat(0),
            endAngle: CGFloat(Double.pi * 2),
            clockwise: true)
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = strokeColor
        shapeLayer.lineWidth = desiredLineWidth
        
        layer.addSublayer(shapeLayer)
    }
}
