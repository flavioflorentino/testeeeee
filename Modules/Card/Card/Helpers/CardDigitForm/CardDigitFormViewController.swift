import UIKit
import Core
import UI

extension CardDigitFormViewController.Layout {
    enum Length {
        static let maxCodeCount = 4
        static let cardViewRatio: CGFloat = 0.56
        static let cardCodeLabelPercent: CGFloat = 0.45
        static let circleViewPercent: CGFloat = 0.5
    }
    enum Size {
        static let desiredLineWidth: CGFloat = 3
        static let labelKern: CGFloat = -0.36
    }
    enum Font {
        static let placeholderLabel = UIFont.boldSystemFont(ofSize: 22)
        static let cardCodeLabel = UIFont.boldSystemFont(ofSize: 20)
    }
}

public final class CardDigitFormViewController: UIViewController, LoadingViewProtocol, ViewConfiguration {
    fileprivate enum Layout {}
    
    public weak var delegate: DigitFormViewControllerDelegate?
    public var loadingView = LoadingView()
    
    public var state: DigitFormViewControllerState = .initial {
        didSet {
            updateState()
        }
    }
    
    private lazy var cardView: UIImageView = {
        let view = UIImageView()
        view.image = Assets.cardVerseDebit.image
        view.backgroundColor = .clear
        return view
    }()
    
    public var titleText: String? {
        didSet {
            self.title = titleText
        }
    }
    
    private var codeTextFieldBottom: NSLayoutConstraint?
    
    private lazy var codeTextfield: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.placeholder = Strings.CardDigitFormViewController.codeTextFieldPlaceHolder
        textfield.delegate = self
        textfield.isEnabled = true
        textfield.textColor = Palette.ppColorGrayscale500.color
        textfield.keyboardType = .decimalPad
        textfield.becomeFirstResponder()
        textfield.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        return textfield
    }()
    
    private lazy var placeholderLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.white.color.withAlphaComponent(0.3)
        label.font = Layout.Font.placeholderLabel
        label.attributedText = NSMutableAttributedString(
            string: Strings.CardDigitFormViewController.cardFullNumberPlaceholder,
            attributes: [NSAttributedString.Key.kern: Layout.Size.labelKern])
        return label
    }()
    
    private lazy var cardCodeLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = Palette.white.color
        label.font = Layout.Font.cardCodeLabel
        label.baselineAdjustment = .alignCenters
        label.attributedText = NSMutableAttributedString(
            string: Strings.CardDigitFormViewController.cardNumberPlaceholder,
            attributes: [NSAttributedString.Key.kern: Layout.Size.labelKern])
        return label
    }()
    
    private lazy var circleView = CircleView(
        desiredLineWidth: Layout.Size.desiredLineWidth,
        strokeColor: Palette.ppColorBranding400.cgColor
    )
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        buildLayout()
        addObservers()
        state = .initial
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        stopLoadingView()
        updateNavigationBarAppearance()
        codeTextfield.becomeFirstResponder()
    }
    
    private func updateState() {
        switch state {
        case .loading:
            startLoadingView()
        case .initial, .canceled:
            stopLoadingView()
        case .popup:
            showError(enable: true)
            stopLoadingView()
            codeTextfield.becomeFirstResponder()
        }
        delegate?.didUpdateState(state: state)
    }
    
    public func canceled() {
        state = .initial
    }
    
    public func show(errorDescription: String, alert: Alert? = nil) {
        state = .popup(error: errorDescription, alert: alert)
    }
    
    public func startLoading() {
        state = .loading
    }
    
    public func configureViews() {
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }
    
    public func buildViewHierarchy() {
        view.addSubview(cardView)
        view.addSubview(codeTextfield)
        view.addSubview(placeholderLabel)
        view.addSubview(cardCodeLabel)
        view.addSubview(circleView)
    }
    
    private func updateNavigationBarAppearance() {
        navigationController?.navigationBar.tintColor = Palette.ppColorBranding300.color
        navigationController?.navigationBar.isTranslucent = false
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.isTranslucent = true
            navigationController?.navigationBar.prefersLargeTitles = true
            navigationController?.navigationItem.largeTitleDisplayMode = .always
        }
        
        let backButton = UIBarButtonItem()
        backButton.title = String()
        navigationItem.backBarButtonItem = backButton
        parent?.navigationItem.backBarButtonItem = backButton
        
        guard let parentViewController = parent else {
            navigationController?
                .navigationBar
                .topItem?
                .backBarButtonItem = backButton
            return
        }
        
        parentViewController
            .navigationController?
            .navigationBar
            .topItem?
            .backBarButtonItem = backButton
    }
    
    private func showError(enable: Bool) {
        codeTextfield.errorMessage = enable ? Strings.CardDigitFormViewController.errorMessage : nil
    }
    
    private func showPopupWith(_ error: String) {
        let popup = PopupViewController(title: Strings.Default.ops,
                                        description: error,
                                        preferredType: .image(Assets.icoSadEmoji.image))
        
        let confirmAction = PopupAction(title: Strings.DigitForm.confirmButtonPopupTitle, style: .fill) { [weak self] in
            self?.state = .initial
        }
        
        popup.addAction(confirmAction)
        showPopup(popup)
    }
    
    public func setupConstraints() {
        cardView.layout {
            $0.leading == view.leadingAnchor + Spacing.base02
            $0.trailing == view.trailingAnchor - Spacing.base02
            $0.height == cardView.widthAnchor * Layout.Length.cardViewRatio
            $0.top == view.compatibleSafeAreaLayoutGuide.topAnchor + Spacing.base03
        }
        
        codeTextfield.layout {
            $0.leading == view.leadingAnchor + Spacing.base02
            $0.trailing == view.trailingAnchor - Spacing.base02
        }
        
        placeholderLabel.layout {
            $0.leading == cardView.leadingAnchor + Spacing.base03
            $0.bottom == cardView.bottomAnchor - Spacing.base05
        }
     
        cardCodeLabel.layout {
            $0.width == placeholderLabel.widthAnchor * Layout.Length.cardCodeLabelPercent
            $0.centerY == placeholderLabel.centerYAnchor
            $0.leading == placeholderLabel.trailingAnchor + Spacing.base01
        }
        
        circleView.layout {
            $0.width == placeholderLabel.widthAnchor * Layout.Length.circleViewPercent
            $0.height == circleView.widthAnchor
            $0.centerY == cardCodeLabel.centerYAnchor - 1
            $0.centerX == cardCodeLabel.centerXAnchor
        }
        
        codeTextFieldBottom = codeTextfield.bottomAnchor.constraint(
            equalTo: view.compatibleSafeAreaLayoutGuide.bottomAnchor,
            constant: 0)
        codeTextFieldBottom?.isActive = true
    }
}

extension CardDigitFormViewController {
    private func addObservers() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil)
    }
    
    @objc
    private func keyboardWillShow(notification: NSNotification) {
        guard let offset = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        codeTextFieldBottom?.constant = -offset.height
    }
    
    @objc
    private func keyboardWillHide(notification: NSNotification) {
        codeTextFieldBottom?.constant = 0
    }
}

@objc
public extension CardDigitFormViewController {
    func didConfirmTap() {
        guard let digits = codeTextfield.text,
            digits.count == Layout.Length.maxCodeCount else {
                return
        }
        delegate?.didTapConfirm(with: digits)
    }
    
    func didHelperTap() {
        delegate?.didTapHandler()
    }
    
    func didCloseTap() {
        view.endEditing(true)
        delegate?.didTapClose()
    }
}

extension CardDigitFormViewController: UITextFieldDelegate {
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let futureString = getCompleteString(with: string)
        cleanErrorMessageIfError(with: string)
        guard futureString.count > Layout.Length.maxCodeCount else {
            cardCodeLabel.text = insertPlaceholder(in: futureString)
            return true
        }
        return false
    }
    
    private func getCompleteString(with stringChar: String) -> String {
        var futureString = codeTextfield.text ?? ""
        if stringChar.isEmpty {
            futureString.removeLast()
        } else {
            futureString += stringChar
        }
        return futureString
    }
    
    private func cleanErrorMessageIfError(with string: String) {
        if
            let textFieldText = codeTextfield.text,
            textFieldText.count == Layout.Length.maxCodeCount,
            string.isEmpty {
            showError(enable: false)
        }
    }
    
    private func insertPlaceholder(in string: String?) -> String {
        guard var newString = string else {
            return String(repeating: "*", count: Layout.Length.maxCodeCount)
        }
        let stringCount = newString.count
        let missingCharactersCount = Layout.Length.maxCodeCount - stringCount
        if missingCharactersCount >= 1 {
            for _ in 1...missingCharactersCount {
                newString += "*"
            }
        }
        return newString
    }
    
    @objc
    func textFieldDidChange(_ textField: UITextField) {
        guard let text = textField.text, text.count == Layout.Length.maxCodeCount else {
            return
        }
        delegate?.didTapConfirm(with: text)
    }
}
