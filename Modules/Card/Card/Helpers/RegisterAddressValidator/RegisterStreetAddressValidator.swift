import Foundation
import Validator

public enum RegisterStreetAddressValidator {
    fileprivate static let addressTextRequiredLength = 1
    fileprivate static let addressTextMinLength = 3
    fileprivate static let addressTextMaxLength = 30
    
    enum ValidateError: Error, CustomStringConvertible, ValidationError {
        case streetRequired
        case streetInvalid
        case streetMinLength
        case streetMaxLength
        
        var description: String {
            message
        }
        
        var message: String {
            switch self {
            case .streetRequired:
                return Strings.RegisterStreetAddressValidator.required
            case .streetInvalid:
                return Strings.RegisterStreetAddressValidator.streetInvalid
            case .streetMinLength:
                return Strings.RegisterAdressValidator.minLength(RegisterStreetAddressValidator.addressTextMinLength)
            case .streetMaxLength:
                return Strings.RegisterAdressValidator.maxLength(RegisterStreetAddressValidator.addressTextMaxLength)
            }
        }
    }
    
    public static func rules() -> ValidationRuleSet<String> {
        var validationRules = ValidationRuleSet<String>()
        let textPattern = "[A-zÀ-ú0-9,. ]+|^$"
        validationRules.add(rule: ValidationRulePattern(pattern: textPattern, error: ValidateError.streetInvalid))
        validationRules.add(rule: ValidationRuleLength(min: addressTextRequiredLength, error: ValidateError.streetRequired))
        validationRules.add(rule: ValidationRuleLength(min: addressTextMinLength, error: ValidateError.streetMinLength))
        validationRules.add(rule: ValidationRuleLength(max: addressTextMaxLength, error: ValidateError.streetMaxLength))
        return validationRules
    }
}
