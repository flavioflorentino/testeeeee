import Foundation
import Validator

public enum RegisterStateAddressValidator {
    fileprivate static let stateTextMinLength = 1
    
    enum ValidateError: Error, CustomStringConvertible, ValidationError {
        case stateRequired
        
        var description: String {
            message
        }
        
        var message: String {
            switch self {
            case .stateRequired:
                return Strings.RegisterStateAddressValidator.required
            }
        }
    }
    
    public static func rules() -> ValidationRuleSet<String> {
        var validationRules = ValidationRuleSet<String>()
        validationRules.add(rule: ValidationRuleLength(min: stateTextMinLength, error: ValidateError.stateRequired))
        return validationRules
    }
}
