import Foundation
import Validator

public enum RegisterNeighborhoodAddressValidator {
    fileprivate static let neighborhoodTextRequiredLength = 1
    fileprivate static let neighborhoodTextMinLength = 2
    fileprivate static let neighborhoodTextMaxLength = 30
    
    enum ValidateError: Error, CustomStringConvertible, ValidationError {
        case neighborhoodRequired
        case neighborhoodInvalid
        case neighborhoodMinLength
        case neighborhoodMaxLength
        
        var description: String {
            message
        }
        
        var message: String {
            switch self {
            case .neighborhoodRequired:
                return Strings.RegisterNeighborhoodAddressValidator.required
            case .neighborhoodInvalid:
                return Strings.RegisterNeighborhoodAddressValidator.neighborhoodInvalid
            case .neighborhoodMinLength:
                return Strings.RegisterAdressValidator.minLength(RegisterNeighborhoodAddressValidator.neighborhoodTextMinLength)
            case .neighborhoodMaxLength:
                return Strings.RegisterAdressValidator.maxLength(RegisterNeighborhoodAddressValidator.neighborhoodTextMaxLength)
            }
        }
    }
    
    public static func rules() -> ValidationRuleSet<String> {
        var validationRules = ValidationRuleSet<String>()
        let textPattern = "[A-zÀ-ú0-9,. ]+|^$"
        validationRules.add(rule: ValidationRulePattern(pattern: textPattern, error: ValidateError.neighborhoodInvalid))
        validationRules.add(rule: ValidationRuleLength(min: neighborhoodTextRequiredLength, error: ValidateError.neighborhoodRequired))
        validationRules.add(rule: ValidationRuleLength(min: neighborhoodTextMinLength, error: ValidateError.neighborhoodMinLength))
        validationRules.add(rule: ValidationRuleLength(max: neighborhoodTextMaxLength, error: ValidateError.neighborhoodMaxLength))
        return validationRules
    }
}
