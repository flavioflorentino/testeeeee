import Foundation
import Validator

public enum RegisterCityAddressValidator {
    fileprivate static let cityTextMinLength = 1
    
    enum ValidateError: Error, CustomStringConvertible, ValidationError {
        case cityRequired
        
        var description: String {
            message
        }
        
        var message: String {
            switch self {
            case .cityRequired:
                return Strings.RegisterCityAddressValidator.required
            }
        }
    }
    
    public static func rules() -> ValidationRuleSet<String> {
        var validationRules = ValidationRuleSet<String>()
        validationRules.add(rule: ValidationRuleLength(min: cityTextMinLength, error: ValidateError.cityRequired))
        return validationRules
    }
}
