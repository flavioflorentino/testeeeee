import Foundation
import Validator

public enum RegisterNumberAddressValidator {
    fileprivate static let numberTextFieldMinLength = 1
    fileprivate static let numberTextFieldMaxLength = 6
    
    enum ValidateError: Error, CustomStringConvertible, ValidationError {
        case numberRequired
        case numberInvalid
        case numberMaxLength
        
        var description: String {
            message
        }
        
        var message: String {
            switch self {
            case .numberRequired:
                return Strings.RegisterNumberAddressValidator.required
            case .numberInvalid:
                return Strings.RegisterNumberAddressValidator.numberInvalid
            case .numberMaxLength:
                return Strings.RegisterAdressValidator.maxLength(RegisterNumberAddressValidator.numberTextFieldMaxLength)
            }
        }
    }
    
    public static func numberRules(noNumberIsChecked: Bool) -> ValidationRuleSet<String> {
        var validationRules = ValidationRuleSet<String>()
        guard !noNumberIsChecked else {
            return validationRules
        }
        let numberPattern = "[1-9][0-9]+|[1-9]"
        validationRules.add(rule: ValidationRuleLength(min: numberTextFieldMinLength, error: ValidateError.numberRequired))
        validationRules.add(rule: ValidationRuleLength(max: numberTextFieldMaxLength, error: ValidateError.numberMaxLength))
        validationRules.add(rule: ValidationRulePattern(pattern: numberPattern, error: ValidateError.numberInvalid))
        return validationRules
    }
}
