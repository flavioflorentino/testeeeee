import Foundation
import Validator

public enum RegisterComplementAddressValidator {
    fileprivate static let complementTextMaxLength = 15
    
    enum ValidateError: Error, CustomStringConvertible, ValidationError {
        case complementInvalid
        case complementMaxLength
        
        var description: String {
            message
        }
        
        var message: String {
            switch self {
            case .complementInvalid:
                return Strings.RegisterComplementAddressValidator.complementInvalid
            case .complementMaxLength:
                return Strings.RegisterAdressValidator.maxLength(RegisterComplementAddressValidator.complementTextMaxLength)
            }
        }
    }
    
    public static func rules() -> ValidationRuleSet<String> {
        var validationRules = ValidationRuleSet<String>()
        let textPattern = "[A-zÀ-ú0-9,. ]+|^$"
        validationRules.add(rule: ValidationRulePattern(pattern: textPattern, error: ValidateError.complementInvalid))
        validationRules.add(rule: ValidationRuleLength(max: complementTextMaxLength, error: ValidateError.complementMaxLength))
        return validationRules
    }
}
