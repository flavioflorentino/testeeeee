import AnalyticsModule
import Core
import FeatureFlag
import Foundation

typealias AppDependencies =
    HasDebitAPIManagerDependency &
    HasAuthManagerDependency &
    HasNoDependency &
    HasAnalytics &
    HasFeatureManager &
    HasMainQueue &
    HasCardConsumerManager &
    HasPPAuthSwiftContract &
    HasKeychainManager

final class DependencyContainer: AppDependencies {
    lazy var analytics: AnalyticsProtocol = Analytics.shared
    lazy var debitAPIManager: DebitAPIManagerContract = DebitAPIManager.shared
    lazy var authManager: AuthContract = AuthManager.shared
    lazy var featureManager: FeatureManagerContract = FeatureManager.shared
    lazy var mainQueue = DispatchQueue.main
    lazy var consumer: CardConsumerContract = CardConsumerManager.shared
    lazy var ppauth: PPAuthSwiftContract = PPAuthManager.shared
    lazy var keychain: KeychainManagerContract = KeychainManager()
}
