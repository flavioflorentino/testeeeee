import Core
import UI

public enum CardSetup {
    public static var deeplinkInstance: DeeplinkContract?
    public static var authInstance: AuthContract?
    public static var navigationInstance: NavigationContract?
    public static var consumerContract: CardConsumerContract?
    public static var ppAuthInstance: PPAuthSwiftContract?
    public static var cardSettingsFlowCoordinator: CardFlowProtocol?
    
    public static func inject(instance: DeeplinkContract) {
        deeplinkInstance = instance
    }
    public static func inject(instance: CardFlowProtocol) {
        cardSettingsFlowCoordinator = instance
    }
    public static func inject(instance: AuthContract) {
        authInstance = instance
    }
    
    public static func inject(instance: NavigationContract) {
        navigationInstance = instance
    }
    
    public static func inject(instance: CardConsumerContract) {
        consumerContract = instance
    }
    
    public static func inject(instance: PPAuthSwiftContract) {
        ppAuthInstance = instance
    }
}
