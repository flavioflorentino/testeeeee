import CustomerSupport
import FeatureFlag
import Foundation

protocol CustomerOptions {
    var development: FAQOptions { get }
    var production: FAQOptions { get }
    var path: PathFAQ { get }
}

extension CustomerOptions {
    func option() -> FAQOptions {
        #if DEBUG
        return development
        #else
        return production
        #endif
    }
}
