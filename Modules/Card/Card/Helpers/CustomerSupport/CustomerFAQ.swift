enum PathFAQ: String, CaseIterable {
    case home
    case activation = "se-meu-pedido-for-aprovado-quando-receberei-meu-picpay-card"
    case activationSuccess = "onde-posso-usar-o-meu-picpay-card"
}
