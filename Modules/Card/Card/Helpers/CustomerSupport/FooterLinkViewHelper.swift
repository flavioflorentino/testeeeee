import UI
import Foundation

public enum FooterLinkViewHelper {
    static func getHelpMessage() -> NSMutableAttributedString {
        let string = Strings.CardArrived.Faq.message
        let linked = Strings.CardArrived.Faq.link
        
        let attributed = NSMutableAttributedString(string: string)
        attributed.font(text: string, font: Typography.bodySecondary().font())
        attributed.textBackgroundColor(text: string, color: .clear)
        attributed.textColor(text: string, color: Colors.grayscale600.color)
        attributed.paragraph(aligment: .center, lineSpace: Spacing.base01)
        attributed.underline(text: linked)
        
        attributed.link(text: linked, link: PathFAQ.activation.rawValue)
        
        return attributed
    }
}
