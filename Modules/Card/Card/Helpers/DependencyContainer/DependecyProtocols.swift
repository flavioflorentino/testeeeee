import Foundation
protocol HasNoDependency {}

protocol HasDebitAPIManagerDependency {
    var debitAPIManager: DebitAPIManagerContract { get }
}

public protocol HasAuthManagerDependency {
    var authManager: AuthContract { get }
}
