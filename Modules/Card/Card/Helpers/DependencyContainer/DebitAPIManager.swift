import Core
import Foundation

public typealias CompletionGetDebitData = (Result<CardForm, ApiError>) -> Void
public typealias CompletionDebitRegistration = ((Result<CardForm, ApiError>) -> Void)?

public protocol DebitAPIManagerContract {
    func getDebitData(completion: @escaping CompletionGetDebitData)
    func setDebitRegistration(cardForm: CardForm, completion: CompletionDebitRegistration)
}

public class DebitAPIManager: DebitAPIManagerContract {
    public static var shared = DebitAPIManager()
    
    public func getDebitData(completion: @escaping CompletionGetDebitData) {
        Api<CardForm>(endpoint: CreditServiceEndpoint.debitData)
            .execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { result in
                DispatchQueue.main.async {
                    completion(result.map(\.model))
                }
            }
    }
    
    public func setDebitRegistration(cardForm: CardForm, completion: CompletionDebitRegistration) {
        Api<CardForm>(endpoint: CreditServiceEndpoint.setDebitRegistration(cardForm: cardForm))
            .execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { result in
                DispatchQueue.main.async {
                    completion?(result.map(\.model))
                }
            }
    }
}
