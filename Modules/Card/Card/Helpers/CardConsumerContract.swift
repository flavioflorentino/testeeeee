import Foundation

public protocol CardConsumerContract {
    var consumerId: Int { get }
    var token: String { get }
}

public protocol HasCardConsumerManager {
    var consumer: CardConsumerContract { get }
}

public class CardConsumerManager: NSObject, CardConsumerContract {
    public var token: String {
        CardSetup.consumerContract?.token ?? ""
    }
    
    public var consumerId: Int {
        CardSetup.consumerContract?.consumerId ?? 0
    }
    
    public static let shared = CardConsumerManager()
}
