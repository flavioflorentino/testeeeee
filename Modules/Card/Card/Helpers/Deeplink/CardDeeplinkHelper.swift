import UIKit
import UI

final class CardDeeplinkHelper {
    let cardType: DeeplinkCardType
    
    init(cardType: DeeplinkCardType) {
        self.cardType = cardType
    }
  
    func handleDeeplink() -> Bool {
        guard let navigation = CardSetup.navigationInstance?.getCurrentNavigation()
            else {
            return false
        }
        
        let coordinating = getCoordinating(navigation)
        
        CardSetup.navigationInstance?.updateCurrentCoordinating(coordinating)
        coordinating?.start()

        return true
    }
    
    func getCoordinating(_ navigation: UINavigationController) -> Coordinating? {
        switch cardType {
        case .needDebitActive, .needCreditActive:
            return ActivateCardFlowCoordinator(navigationController: navigation)
        case .requestCreditCard:
            return RequestPhysicalCardFlowCoordinator(navigationController: navigation)
        case .activateDebitCard:
            return ActivateDebitCardFlowCoordinator(navigationController: navigation)
        case
            .debitRegistration,
            .debitRequestInProgress:
            return DebitRequestFlowCoordinator(navigationController: navigation)
        case .tracking:
            return CardTrackingFlowCoordinator(navigationController: navigation)
        case .debitRequestSimplified:
            return RejectedCreditFlowCoordinator(navigationController: navigation)
        case .hybrid:
            return HybridDeeplinkFlowCoordinator(navigationController: navigation)
        case .virtualCard:
            return VirtualCardFlowCoordinator(navigation: navigation, isModal: true)
        case .changePassword:
            return ChangeCardPasswordFlowCoordinator(navigationController: navigation, presentModally: true)
        }
    }
}
