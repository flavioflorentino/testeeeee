import Foundation
import FeatureFlag

enum DeeplinkCardType: String {
    static let CARD_PATH = "card"
    
    case needCreditActive = "credit/validate"
    case needDebitActive = "debit/validate"
    case requestCreditCard = "credit/request"
    case activateDebitCard = "credit/activate-debit"
    case debitRegistration = "debit/registration"
    case debitRequestInProgress = "debit/request-in-progress"
    case tracking = "tracking"
    case virtualCard = "virtualcard"
    case debitRequestSimplified = "debit/simplified-registration"
    case hybrid = "register/hybrid"
    case changePassword = "change_password"
    
    var needAuthentication: Bool {
        true
    }
    
    var isEnabled: Bool {
        switch self {
        case .hybrid:
            return FeatureManager.isActive(.isCardRegisterHybridDeeplinkActivated)
        default:
            return true
        }
    }
    
    init?(url: URL) {
        guard 
            let path = url.pathComponents.first(where: { $0 != "/" }),
            path == DeeplinkCardType.CARD_PATH 
            else {
                return nil
        }
        
        let params = url.pathComponents.filter { $0 != "/" && $0 != DeeplinkCardType.CARD_PATH }
        
        let positionOfType: Int = 0
        
        guard params.indices.contains(positionOfType) else {
            return nil
        }
        
        let positionOfStatus: Int = 1
        
        guard params.indices.contains(positionOfStatus) else {
            self.init(rawValue: params[positionOfType])
            return
        }
        
        self.init(rawValue: "\(params[positionOfType])/\(params[positionOfStatus])")
    }
}
