import UIKit
import Foundation
import Core
import UI

public struct CardDeeplinkResolver: DeeplinkResolver {
    public init() {}
    
    public func canHandle(url: URL, isAuthenticated: Bool) -> DeeplinkResolverResult {
        guard let cardType = DeeplinkCardType(url: url), cardType.isEnabled else {
            return .notHandleable
        }
        
        if cardType.needAuthentication && !isAuthenticated {
            return .onlyWithAuth
        }
        
        return .handleable
    }
    
    public func open(url: URL, isAuthenticated: Bool) -> Bool {
        guard
            let cardType = DeeplinkCardType(url: url),
            canHandle(url: url, isAuthenticated: isAuthenticated) == .handleable
            else {
                return false
        }
        
        let deeplinkHandler = CardDeeplinkHelper(cardType: cardType)
        
        return deeplinkHandler.handleDeeplink()
    }
}
