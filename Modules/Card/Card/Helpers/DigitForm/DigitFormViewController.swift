import Core
import UI
import UIKit

public enum DigitFormViewControllerState {
    case initial
    case loading
    case canceled
    case popup(error: String, alert: Alert?)
    
    var isLoading: Bool {
        guard case .loading = self else {
            return false
        }
        return true
    }
}

public protocol DigitFormViewControllerDelegate: AnyObject {
    func didTapConfirm(with digits: String)
    func didTapHandler()
    func didTapClose()
    func didUpdateState(state: DigitFormViewControllerState)
    func didHiddenPassword(isHidden: Bool)
}

public extension DigitFormViewControllerDelegate {
    func didUpdateState(state: DigitFormViewControllerState) {}
}

public extension DigitFormViewControllerDelegate {
    func didTapHandler() { }
    func didTapClose() { }
    func didHiddenPassword(isHidden: Bool) { }
}

public class DigitFormViewController: UIViewController, LoadingViewProtocol, StyledNavigationDisplayable {
    private enum Layout {
        static let textBoldFont = UIFont.systemFont(ofSize: 16, weight: .semibold)
        static let texLightFont = UIFont.systemFont(ofSize: 16, weight: .light)
        static let sizeInputDigitForm = CGSize(width: 246, height: 56)
    }

    override public var preferredStatusBarStyle: UIStatusBarStyle {
        statusBarStyle
    }

    public weak var delegate: DigitFormViewControllerDelegate?
    public lazy var loadingView = LoadingView()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 16, weight: .light)
        return label
    }()
    private lazy var inputDigitsForm: InputDigitsForm = {
        let form = InputDigitsForm()
        form.translatesAutoresizingMaskIntoConstraints = false
        form.delegate = self
        return form
    }()
    private lazy var helperButton: UnderlinedButton = {
        let button = UnderlinedButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(Palette.ppColorBranding300.color, for: .normal)
        button.addTarget(self, action: #selector(didHelperTap), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    private lazy var confirmButton: ConfirmButton = {
        let button = ConfirmButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.isEnabled = false
        button.addTarget(self, action: #selector(didConfirmTap), for: .touchUpInside)
        return button
    }()
    private var confirmButtonBottom: NSLayoutConstraint?
    public var state: DigitFormViewControllerState = .initial {
        didSet {
            updateState()
        }
    }
    public var presenter: DigitFormPresenting? {
        didSet {
            update()
        }
    }
    deinit {
        removeObservers()
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        buildViewHierarchy()
        configureViews()
        setupConstraints()
        addObservers()
        state = .initial
        addGestureToView()
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        stopLoadingView()
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !UIAccessibility.isVoiceOverRunning {
            inputDigitsForm.becomeFirstResponder()
        }
        setNeedsStatusBarAppearanceUpdate()
    }
    
    public func canceled() {
        state = .initial
    }
    public func show(errorDescription: String, alert: Alert? = nil) {
        state = .popup(error: errorDescription, alert: alert)
    }
    public func startLoading() {
        state = .loading
    }
    // MARK: - Private Methods
    
    private func buildViewHierarchy() {
        view.addSubview(descriptionLabel)
        view.addSubview(inputDigitsForm)
        view.addSubview(helperButton)
        view.addSubview(confirmButton)
    }
    private func setupConstraints() {
        NSLayoutConstraint.leadingTrailing(equalTo: view,
                                           for: [descriptionLabel, confirmButton],
                                           constant: 16)
        
        NSLayoutConstraint.activate([
            inputDigitsForm.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            inputDigitsForm.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 24),
            inputDigitsForm.heightAnchor.constraint(equalToConstant: Layout.sizeInputDigitForm.height),
            inputDigitsForm.widthAnchor.constraint(equalToConstant: Layout.sizeInputDigitForm.width)
        ])
        
        NSLayoutConstraint.activate([
            helperButton.topAnchor.constraint(equalTo: inputDigitsForm.bottomAnchor, constant: 16),
            helperButton.leadingAnchor.constraint(equalTo: descriptionLabel.leadingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            confirmButton.heightAnchor.constraint(equalToConstant: 48)
        ])
        
        descriptionLabel.topAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.topAnchor, constant: 8).isActive = true
        confirmButtonBottom = confirmButton.bottomAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.bottomAnchor, constant: -24)
        confirmButtonBottom?.isActive = true
    }
    private func configureViews() {
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }
    private func showPopupWith(_ error: String, alert: Alert?) {
        let popup = PopupViewController(
            title: alert?.title?.string ?? Strings.Default.ops,
            description: alert?.text?.string ?? error,
            preferredType: .image(Assets.icoSadEmoji.image)
        )

        let action = (alert?.buttonsJson.first?["title"] as? String) ?? Strings.DigitForm.confirmButtonPopupTitle
        let confirmAction = PopupAction(title: action, style: .fill) { [weak self] in
            self?.state = .initial
        }

        popup.addAction(confirmAction)
        showPopup(popup)
    }
    private func update() {
        title = presenter?.title
        if let text = presenter?.description {
            descriptionLabel.attributedText = text.attributedStringWithFont(primary: Layout.texLightFont, secondary: Layout.textBoldFont)
        }
        if let helperTitle = presenter?.helperTitle {
            helperButton.isHidden = false
            helperButton.setTitle(helperTitle, for: .normal)
        }
        confirmButton.setTitle(presenter?.confirmTitle, for: .normal)
        inputDigitsForm.isSecureTextEntry = presenter?.isSecurityCode ?? false
    }
    private func updateState() {
        switch state {
        case .initial:
            stopLoadingView()
            confirmButton.isEnabled = false
            inputDigitsForm.resetText()
            inputDigitsForm.becomeFirstResponder()
        case .loading:
            startLoadingView()
            inputDigitsForm.resignFirstResponder()
        case let .popup(error, alert):
            showPopupWith(error, alert: alert)
            stopLoadingView()
            inputDigitsForm.resignFirstResponder()
        case .canceled:
            stopLoadingView()
        }
        delegate?.didUpdateState(state: state)
    }
    
    func addGestureToView() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(inputFormResignFirstResponder))
        view.addGestureRecognizer(gesture)
    }
    
    @objc
    func inputFormResignFirstResponder() {
        inputDigitsForm.resignFirstResponder()
    }
}

// MARK: - Keyboard Handlers

extension DigitFormViewController {
    private func addObservers() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }
    private func removeObservers() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    @objc
    private func keyboardWillShow(notification: NSNotification) {
        guard let offset = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
                return
        }
        confirmButtonBottom?.constant = -offset.height - 16
    }
    @objc
    private func keyboardWillHide(notification: NSNotification) {
        self.confirmButtonBottom?.constant = -16
    }
}

// MARK: Tap Handlers

@objc
public extension DigitFormViewController {
    func didConfirmTap() {
        guard let digits = inputDigitsForm.text(),
            inputDigitsForm.isFilled() else {
            return
        }
        delegate?.didTapConfirm(with: digits)
    }
    
    func didHelperTap() {
        delegate?.didTapHandler()
    }
    
    func didCloseTap() {
        view.endEditing(true)
        delegate?.didTapClose()
    }
}

// MARK: InputDigitsFormDelegate

extension DigitFormViewController: InputDigitsFormDelegate {
    public func formIsFilled(_ isFill: Bool) {
        confirmButton.isEnabled = isFill
    }
    public func tapHiddenForm(isHidden: Bool) {
        delegate?.didHiddenPassword(isHidden: isHidden)
    }
}
