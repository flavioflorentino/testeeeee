public protocol DigitFormPresenting {
    var title: String { get }
    var description: String { get }
    var helperTitle: String? { get }
    var confirmTitle: String { get }
    var isSecurityCode: Bool { get }
}
