import Foundation

public enum OnboardingTypeFlow {
    case debit
    case multiple
    case debitToMultiple
}
