import Foundation

public protocol HasPPAuthSwiftContract {
    var ppauth: PPAuthSwiftContract { get }
}

public protocol PPAuthSwiftContract {
    func handlePassword()
}

public class PPAuthManager: NSObject, PPAuthSwiftContract {
    public static let shared = PPAuthManager()

    public func handlePassword() {
        CardSetup.ppAuthInstance?.handlePassword()
    }
}
