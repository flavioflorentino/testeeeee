import Foundation
import Core
import UI

extension StatefulErrorViewModel {
    convenience init(_ error: ApiError) {
        switch error {
        case .connectionFailure:
            self.init(
                image: Assets.icoNoInternet.image,
                content: (title: Strings.ApiError.NoInternet.title, description: Strings.ApiError.NoInternet.description),
                button: (image: UI.Assets.icoRefresh.image, title: Strings.ApiError.NoInternet.button)
            )
        
        case .timeout:
            self.init(
                image: Assets.icoNoInternet.image,
                content: (title: Strings.ApiError.NotAvailable.title, description: Strings.ApiError.NotAvailable.description),
                button: (image: UI.Assets.icoRefresh.image, title: Strings.ApiError.NotAvailable.button)
            )
            
        default:
            self.init(
                image: Assets.icoSadEmoji.image,
                content: (title:error.requestError?.title.hasContent() ?? Strings.ApiError.Default.title, description: error.requestError?.message.hasContent() ?? Strings.ApiError.Default.description),
                button: (image: nil, title: Strings.ApiError.Default.button)
            )
        }
    }
}
