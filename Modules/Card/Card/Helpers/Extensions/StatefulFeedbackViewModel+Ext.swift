import Foundation
import UI
import AssetsKit
import Core

public extension StatefulFeedbackViewModel {
    convenience init(_ error: ApiError) {
        switch error {
        case .connectionFailure:
            self.init(image: Resources.Illustrations.iluNotFound.image, content: (title: Strings.Default.Connection.title, description: Strings.Default.Connection.verify), button: (image: Resources.Icons.icoRefresh.image, title: Strings.Default.retry), secondaryButton: (image: nil, title: Strings.Default.cancelButtonTitle))
        default:
            self.init(image: Resources.Illustrations.iluError.image, content: (title: Strings.Default.GenericError.title, description: Strings.Default.GenericError.tryAgain), button: (image: Resources.Icons.icoRefresh.image, title: Strings.Default.retry), secondaryButton: (image: nil, title: Strings.Default.cancelButtonTitle))
        }
    }
}
