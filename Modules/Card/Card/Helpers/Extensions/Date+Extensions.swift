import Foundation

extension DateFormatter {
    enum Format: String {
        case dayMonthAndYear = "dd/MM/yyyy"
        case dayAndMonth = "dd/MM"
        case dateAndTime = "dd/MM/yyyy HH:mm"
        case monthAndYear = "MM/yyyy"
        case monthAndYearReduced = "MM/yy"
        case day = "dd"
        case monthShortText = "MMM"
        case time = "HH:mm"
    }
}

extension Date {
    func formatted(in format: DateFormatter.Format = .dayMonthAndYear) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format.rawValue
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.locale = Locale(identifier: "pt_BR_POSIX")
        return formatter.string(from: self)
    }
}
