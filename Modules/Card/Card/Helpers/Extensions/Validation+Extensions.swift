import Validator

public extension ValidationResult {
    func merge(with result: ValidationResult) -> ValidationResult {
        switch self {
        case .valid:
            return result
        case .invalid(let errorMessages):
            switch result {
            case .valid:
                return self
            case .invalid(let errorMessagesAnother):
                return .invalid([errorMessages, errorMessagesAnother].flatMap { $0 })
            }
        }
    }
    
    func merge(with results: [ValidationResult]) -> ValidationResult {
        results.reduce(self) { $0.merge(with: $1) }
    }
    
    static func merge(results: [ValidationResult]) -> ValidationResult {
        ValidationResult.valid.merge(with: results)
    }
}
