import Foundation

public protocol AuthContract {
    func authenticate(sucessHandler: @escaping (_ password: String, _ isBiometric: Bool) -> Void, cancelHandler: (() -> Void)?)
}

public final class AuthManager: NSObject, AuthContract {
    public static let shared = AuthManager()
    
    private var authConfig: AuthContract? {
        CardSetup.authInstance
    }
    
    public func authenticate(sucessHandler: @escaping (_ password: String, Bool) -> Void, cancelHandler: (() -> Void)?) {
        authConfig?.authenticate(sucessHandler: sucessHandler, cancelHandler: cancelHandler)
    }
}
