import AssetsKit
import UI
import UIKit

protocol DeliveryConfirmAddressDisplaying: AnyObject {
    func setupScreenData(with viewModel: DeliveryConfirmAddressViewModel)
    func displayContract(checked: Bool)
    func displayLoading()
    func dismissLoading()
    func displayAlertError(with description: String)
}

final class DeliveryConfirmAddressViewController: ViewController<DeliveryConfirmAddressInteracting, DeliveryConfirmAddressRootView> {
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.setupScreen()
        updateNavigationBarAppearance()
    }
    
    override func configureViews() {
        rootView.delegate = self
    }

    private func updateNavigationBarAppearance() {
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.backgroundColor = Colors.backgroundPrimary.color
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.isTranslucent = true
            navigationController?.navigationBar.prefersLargeTitles = false
            navigationController?.navigationItem.largeTitleDisplayMode = .always
        }

        let backButton = UIBarButtonItem()
        backButton.title = String()
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        if let controllerCount = navigationController?.viewControllers.count, controllerCount <= 1 {
            addCloseButton()
        }
    }
    
    private func addCloseButton() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: Assets.icoClose.image,
            style: .plain,
            target: self,
            action: #selector(didTapClose)
        )
    }
    
    @objc
    private func didTapClose() {
        interactor.close()
    }

    private func showPopupError(messageDescription: String) {
        let popup = PopupViewController(title: Strings.Default.ops,
                                        description: messageDescription,
                                        preferredType: .image(Assets.icoSadEmoji.image))

        let confirmAction = PopupAction(title: Strings.Default.iGotIt, style: .fill) { [weak self] in
            self?.endState()
        }

        popup.addAction(confirmAction)
        showPopup(popup)
    }
}

extension DeliveryConfirmAddressViewController: DeliveryConfirmAddressRootViewDelegate {
    func didTapChangeAddress() {
        interactor.changeAddress()
    }

    func didTapContractReadCheck() {
        interactor.checkContract()
    }

    func didTapContractReader() {
        interactor.readContract()
    }

    func didTapConfirm() {
        interactor.confirm()
    }
}

// MARK: View Model Outputs
extension DeliveryConfirmAddressViewController: DeliveryConfirmAddressDisplaying {
    func setupScreenData(with viewModel: DeliveryConfirmAddressViewModel) {
        rootView.setupScreenData(with: viewModel)
    }
    
    func displayContract(checked: Bool) {
        rootView.displayContract(checked: checked)
    }
    
    func displayLoading() {
        beginState(animated: true, model: StateLoadingViewModel(message: Strings.Default.loading))
    }
    
    func dismissLoading() {
        endState()
    }
    
    func displayAlertError(with description: String) {
        endState()
        showPopupError(messageDescription: description)
    }
}

extension DeliveryConfirmAddressViewController: StatefulTransitionViewing {
    func didTryAgain() { }
}

extension DeliveryConfirmAddressViewController: StyledNavigationDisplayable {
    var prefersLargeTitle: Bool { false }
}
