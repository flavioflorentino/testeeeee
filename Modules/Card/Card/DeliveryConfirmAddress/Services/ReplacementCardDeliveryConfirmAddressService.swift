import Core
import Foundation

public final class ReplacementCardDeliveryConfirmAddressService: DeliveryConfirmAddressServicing {
    public var flow: DeliveryConfirmAddressFlow = .replacement
    public var canChangeAddress = false
    public var isConfirmNeeded = true
    
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    public func registrationFinish(password: String?, completion: @escaping CompletionRegistrationFinish) {
        guard let password = password else {
            completion(.failure(ApiError.unauthorized(body: RequestError())))
            return
        }
        
        let endpoint = CreditServiceEndpoint.cardNewRequest(addressType: .residential, password: password)
        
        Api<NoContent>(endpoint: endpoint).execute { result in
            self.dependencies.mainQueue.async {
                let mappedResult = result.map { $0.model }.map { _ -> RegistrationStatus? in
                    nil
                }
                
                completion(mappedResult)
            }
        }
    }
}
