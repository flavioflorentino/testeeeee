import Core
import Foundation

public enum DeliveryConfirmAddressFlow {
    case debit
    case multiple
    case upgrade
    case replacement
}

public typealias CompletionRegistrationFinish = (Result<RegistrationStatus?, ApiError>) -> Void

public protocol DeliveryConfirmAddressServicing {
    var flow: DeliveryConfirmAddressFlow { get }
    var canChangeAddress: Bool { get }
    var isConfirmNeeded: Bool { get }
    func registrationFinish(password: String?, completion: @escaping CompletionRegistrationFinish)
}

public extension DeliveryConfirmAddressServicing {
    func registrationFinish(password: String?, completion: @escaping CompletionRegistrationFinish) { }
}
