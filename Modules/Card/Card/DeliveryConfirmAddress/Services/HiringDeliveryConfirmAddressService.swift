import Core
import Foundation

public final class HiringDeliveryConfirmAddressService: DeliveryConfirmAddressServicing {
    public var flow: DeliveryConfirmAddressFlow = .upgrade
    public var canChangeAddress = true
    public var isConfirmNeeded = false
}
