import Core
import Foundation

public final class DebitDeliveryConfirmAddressService: DeliveryConfirmAddressServicing {
    public var flow: DeliveryConfirmAddressFlow = .debit
    public var canChangeAddress = true
    public var isConfirmNeeded = true
    
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    public func registrationFinish(password: String?, completion: @escaping CompletionRegistrationFinish) {
        guard let password = password else {
            completion(.failure(ApiError.unauthorized(body: RequestError())))
            return
        }
        
        let endpoint = CreditServiceEndpoint.creditRegistrationFinish(
            password: password,
            originFlow: OriginFlow(origin: .debit)
        )
        
        Api<CreditRegistrationFinish>(endpoint: endpoint).execute { result in
            self.dependencies.mainQueue.async {
              let mappedResult = result
                .mapError { $0 as ApiError }
                .map { $0.model }
                .map { model -> RegistrationStatus? in
                    model.registrationStatus
                }
              
              completion(mappedResult)
            }
        }
    }
}
