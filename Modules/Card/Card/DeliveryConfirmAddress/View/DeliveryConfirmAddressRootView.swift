import AssetsKit
import UI
import UIKit

protocol DeliveryConfirmAddressRootViewDelegate: AnyObject {
    func didTapChangeAddress()
    func didTapContractReadCheck()
    func didTapContractReader()
    func didTapConfirm()
}

private extension DeliveryConfirmAddressRootView.Layout {
    enum Size {
        static let homeImage = CGSize(width: 152, height: 144)
        static let changeAddressButtonEdge = UIEdgeInsets(top: 5.0, left: 0.0, bottom: 0.0, right: 0.0)
        static let contractReaderButtonEdge = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        static let contractReadCheck = CGSize(width: 24.0, height: 24.0)
    }
    enum Offset {
        static let base012: CGFloat = -12.0
    }
}

final class DeliveryConfirmAddressRootView: UIView, ViewConfiguration {
    fileprivate enum Layout {}
    weak var delegate: DeliveryConfirmAddressRootViewDelegate?

    private lazy var contentView = UIView()
    private lazy var contentContainerView = UIView()

    private lazy var homeImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
            .with(\.textColor, .black())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale900())
            .with(\.textAlignment, .center)
        return label
    }()
    
    // MARK: Address Container Session
    private lazy var addressContainerView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            SpacerView(size: Spacing.base03),
            addressContainerHeader,
            addressLabel,
            SpacerView(size: Spacing.base03)
        ])
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .center
        stackView.layer.cornerRadius = CornerRadius.light
        stackView.layer.borderWidth = Border.light
        stackView.layer.borderColor = Colors.grayscale100.color.cgColor
        return stackView
    }()
    
    private lazy var addressContainerHeader: UIView = {
        let view = UIView()
        view.clipsToBounds = false
        return view
    }()
    
    private lazy var changeAddressTitle: UILabel = {
        let label = UILabel()
        label.text = Strings.DeliveryConfirmAddress.Address.title
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .black())
            .with(\.textAlignment, .left)
        return label
    }()
    
    private lazy var changeAddressButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(didTapChangeAddress), for: .touchUpInside)
        button.setTitle(Strings.DeliveryConfirmAddress.Address.change, for: .normal)
        button.buttonStyle(LinkButtonStyle(size: .small, icon: nil))
        button.setIconImage(.pen, color: Colors.branding600.color, for: .normal, edgeInsets: Layout.Size.changeAddressButtonEdge)
        return button
    }()
    
    private lazy var addressLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 3
        return label
    }()
    
    // MARK: Contract + Confirm Session
    private lazy var contractContainerView: UIView = {
        let view = UIView()
        view.viewStyle(RoundedViewStyle(cornerRadius: .light))
            .with(\.border, .light(color: .grayscale100()))
        return view
    }()
    
    private lazy var contractReadCheckButton: UIButton = {
        let button = UIButton()
        button.setImage(Assets.iconUnchecked.image, for: .normal)
        button.setImage(Assets.iconChecked.image, for: .selected)
        button.addTarget(self, action: #selector(didTapContractReadCheck), for: .touchUpInside)
        return button
    }()
    
    private lazy var contractReaderButton: UIButton = {
        let button = UIButton()
        button.isEnabled = true
        button.addTarget(self, action: #selector(didTapContractReader), for: .touchUpInside)
        button.titleLabel?.numberOfLines = 0
        button.contentEdgeInsets = Layout.Size.contractReaderButtonEdge
        return button
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.isEnabled = false
        button.addTarget(self, action: #selector(didTapConfirm), for: .touchUpInside)
        button.buttonStyle(PrimaryButtonStyle()).with(\.typography, .bodyPrimary())
        return button
    }()
    
    init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    private func didTapChangeAddress() {
        delegate?.didTapChangeAddress()
    }

    @objc
    private func didTapContractReadCheck() {
        delegate?.didTapContractReadCheck()
    }
    
    @objc
    private func didTapContractReader() {
        delegate?.didTapContractReader()
    }
    
    @objc
    private func didTapConfirm() {
        delegate?.didTapConfirm()
    }
    
    func setupScreenData(with viewModel: DeliveryConfirmAddressViewModel) {
        homeImageView.image = viewModel.content.image
        titleLabel.text = viewModel.content.title
        descriptionLabel.attributedText = viewModel.content.description
        confirmButton.setTitle(viewModel.content.buttonTitle, for: .normal)
        addressLabel.attributedText = viewModel.content.address
        contractReaderButton.setAttributedTitle(viewModel.content.contractTitle, for: .normal)
        
        changeAddressButton.isHidden = !viewModel.states.canChangeAddress
        contractContainerView.isHidden = viewModel.states.isContractHidden
        confirmButton.isEnabled = viewModel.states.isConfirmationEnable
    }
    
    func displayContract(checked: Bool) {
        contractReadCheckButton.isSelected = checked
        contractContainerView.layer.borderColor = checked ? Colors.branding600.color.cgColor : Colors.grayscale100.color.cgColor
        confirmButton.isEnabled = checked
    }
    
    func buildViewHierarchy() {
        addressContainerHeader.addSubviews(changeAddressTitle,
                                           changeAddressButton)

        contractContainerView.addSubviews(contractReadCheckButton,
                                          contractReaderButton)
        
        contentContainerView.addSubviews(homeImageView,
                                         titleLabel,
                                         descriptionLabel,
                                         addressContainerView)
        
        contentView.addSubviews(contentContainerView,
                                contractContainerView,
                                confirmButton)

        addSubview(contentView)
    }

    func configureViews() {
        viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }
    
    func setupConstraints() {
        contentView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        contentContainerView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base01)
            $0.leading.trailing.equalToSuperview()
        }
        setupContainerConstraints()
        setupContractAndConfirmConstraints()
    }
}

private extension DeliveryConfirmAddressRootView {
    func setupContainerConstraints() {
        homeImageView.snp.makeConstraints {
            $0.top.equalTo(contentContainerView.compatibleSafeArea.top)
            $0.centerX.equalToSuperview()
            $0.size.equalTo(Layout.Size.homeImage)
        }
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(homeImageView.compatibleSafeArea.bottom)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Spacing.base08)
        }
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.compatibleSafeArea.bottom).offset(Spacing.base00)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        setupAddressConstraints()
    }
    
    func setupAddressConstraints() {
        addressContainerView.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.compatibleSafeArea.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        addressContainerHeader.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
        }
        changeAddressTitle.snp.makeConstraints {
            $0.top.equalTo(addressContainerHeader.compatibleSafeArea.top)
            $0.leading.equalTo(addressContainerHeader.compatibleSafeArea.leading).offset(Spacing.base03)
        }
        changeAddressButton.snp.makeConstraints {
            $0.top.equalTo(addressContainerHeader.compatibleSafeArea.top).offset(Layout.Offset.base012)
            $0.trailing.equalTo(addressContainerHeader.compatibleSafeArea.trailing).offset(-Spacing.base03)
        }
        addressLabel.snp.makeConstraints {
            $0.top.equalTo(changeAddressButton.compatibleSafeArea.bottom)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
    }
    
    func setupContractAndConfirmConstraints() {
        contractContainerView.snp.makeConstraints {
            $0.top.equalTo(contentContainerView.compatibleSafeArea.bottom).offset(Spacing.base02)
            $0.bottom.equalTo(confirmButton.compatibleSafeArea.top).offset(-Spacing.base02)
            $0.height.equalTo(Sizing.base09)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        contractReadCheckButton.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.size.equalTo(Layout.Size.contractReadCheck)
        }
        
        contractReaderButton.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalTo(contractReadCheckButton.compatibleSafeArea.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        confirmButton.snp.makeConstraints {
            $0.bottom.equalTo(compatibleSafeArea.bottom).offset(-Spacing.base03)
            $0.top.equalTo(contractContainerView.compatibleSafeArea.bottom).offset(Spacing.base02)
            $0.height.equalTo(Sizing.base06)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
    }
}
