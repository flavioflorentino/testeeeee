import Foundation

enum DeliveryConfirmAddressFactory {
    static func make(service: DeliveryConfirmAddressServicing,
                     analyticsContext: DeliveryConfirmAddressEvents,
                     deliveryAddress: HomeAddress?,
                     coordinatorOutput: @escaping DeliveryConfirmAddressCoordinatorOutput) -> DeliveryConfirmAddressViewController {
        let container = DependencyContainer()
        let coordinator: DeliveryConfirmAddressCoordinating = DeliveryConfirmAddressCoordinator(coordinatorOutput: coordinatorOutput)
        let presenter: DeliveryConfirmAddressPresenting = DeliveryConfirmAddressPresenter(coordinator: coordinator)
        let interactor = DeliveryConfirmAddressInteractor(service: service,
                                                          presenter: presenter,
                                                          deliveryAddress: deliveryAddress,
                                                          analyticsContext: analyticsContext,
                                                          dependencies: container)
        let viewController = DeliveryConfirmAddressViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
