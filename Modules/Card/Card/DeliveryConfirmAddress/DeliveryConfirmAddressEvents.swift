protocol DeliveryConfirmAddressEvents {
    func askCard()
    func editAddress()
    func authCanceled()
    func authFingerprintUsed()
    func authNumeralPasswordUsed()
    func authFailed()
    func authValidated()
    func checkContract()
    func readContract()
}

extension DeliveryConfirmAddressEvents {
    func authCanceled() {}
    func authFingerprintUsed() {}
    func authNumeralPasswordUsed() {}
    func authFailed() {}
    func authValidated() {}
    func checkContract() {}
    func readContract() {}
}
