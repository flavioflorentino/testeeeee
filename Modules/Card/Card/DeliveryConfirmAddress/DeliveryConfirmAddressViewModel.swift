import UIKit

struct DeliveryConfirmAddressViewModel: Equatable {
    let content: DeliveryConfirmAddressViewModelContent
    let states: DeliveryConfirmAddressViewModelStates
}

struct DeliveryConfirmAddressViewModelContent: Equatable {
    let image: UIImage
    let title: String
    let description: NSAttributedString
    let buttonTitle: String
    let address: NSAttributedString
    let contractTitle: NSAttributedString?
}

struct DeliveryConfirmAddressViewModelStates: Equatable {
    let canChangeAddress: Bool
    let isContractHidden: Bool
    let isConfirmationEnable: Bool
}
