import AnalyticsModule

struct HiringDeliveryconfirmAddressAnalytics: DeliveryConfirmAddressEvents {
    typealias Dependencies = HasAnalytics
    private var dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func askCard() {
        dependencies.analytics.log(HiringDeliveryConfirmationEvent.confirmAddress(action: .askCard))
    }

    func editAddress() {
        dependencies.analytics.log(HiringDeliveryConfirmationEvent.confirmAddress(action: .editAddress))
    }
}
