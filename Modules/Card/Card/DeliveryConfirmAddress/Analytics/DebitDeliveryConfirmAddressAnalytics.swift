import AnalyticsModule

struct DebitDeliveryConfirmAddressAnalytics: DeliveryConfirmAddressEvents {
    typealias Dependencies = HasAnalytics
    
    private var dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func askCard() {
        dependencies.analytics.log(DebitActivationEvent.confirmAddress(action: .confirmAddress))
    }
    
    func editAddress() {
        dependencies.analytics.log(DebitActivationEvent.confirmAddress(action: .changeAddress))
    }
    
    func authValidated() {
        dependencies.analytics.log(DebitActivationEvent.authenticated(state: .validaded))
    }
    
    func authFailed() {
        dependencies.analytics.log(DebitActivationEvent.authenticated(state: .failed))
    }
    
    func authCanceled() {
        dependencies.analytics.log(DebitActivationEvent.authentication(action: .cancel))
    }
    
    func authFingerprintUsed() {
        dependencies.analytics.log(DebitActivationEvent.authentication(action: .byometricPassword))
    }
    
    func authNumeralPasswordUsed() {
        dependencies.analytics.log(DebitActivationEvent.authentication(action: .numeralPassword))
    }
    
    func checkContract() {
        dependencies.analytics.log(DebitActivationEvent.confirmAddress(action: .checkContract))
    }
    
    func readContract() {
        dependencies.analytics.log(DebitActivationEvent.confirmAddress(action: .readContract))
    }
}
