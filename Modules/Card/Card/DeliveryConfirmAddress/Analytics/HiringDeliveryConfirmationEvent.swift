import AnalyticsModule

public enum HiringDeliveryConfirmationEvent: AnalyticsKeyProtocol {
    case confirmAddress(action: ConfirmAddressAction)

    private var name: String {
        "PicPay Card - Request - Sign Up - Confirm Address"
    }
    
    private var properties: [String: Any] {
        switch self {
        case .confirmAddress(let action):
            return [
                "action": action.rawValue
            ]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }
    
    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}

public extension HiringDeliveryConfirmationEvent {
    enum ConfirmAddressAction: String {
        case editAddress = "act-change-address"
        case askCard = "act-request-plastic-card"
    }
}
