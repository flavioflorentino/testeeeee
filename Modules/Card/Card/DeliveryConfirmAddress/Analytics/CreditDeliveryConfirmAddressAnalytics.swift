import AnalyticsModule

struct CreditDeliveryConfirmAddressAnalytics: DeliveryConfirmAddressEvents {
    typealias Dependencies = HasAnalytics
    
    private var dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func askCard() {
        dependencies.analytics.log(CreditRequestEvent.confirmAddress(action: .askCard))
    }

    func editAddress() {
        dependencies.analytics.log(CreditRequestEvent.confirmAddress(action: .editAddress))
    }
}
