import AnalyticsModule

struct ReplacementCardDeliveryConfirmAddressAnalytics: DeliveryConfirmAddressEvents {
    typealias Dependencies = HasAnalytics
    
    private var dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func askCard() {
        dependencies.analytics.log(DeliveryConfirmAddressEvent.confirmAddress(flow: .replacementCard, action: .confirm))
    }
    
    func editAddress() {
    }
    
    func authValidated() {
        dependencies.analytics.log(DeliveryConfirmAddressEvent.authenticated(flow: .replacementCard, state: .validaded))
    }
    
    func authFailed() {
        dependencies.analytics.log(DeliveryConfirmAddressEvent.authenticated(flow: .replacementCard, state: .failed))
    }
    
    func authCanceled() {
        dependencies.analytics.log(DeliveryConfirmAddressEvent.authentication(flow: .replacementCard, action: .cancel))
    }
    
    func authFingerprintUsed() {
        dependencies.analytics.log(DeliveryConfirmAddressEvent.authentication(flow: .replacementCard, action: .byometricPassword))
    }
    
    func authNumeralPasswordUsed() {
        dependencies.analytics.log(DeliveryConfirmAddressEvent.authentication(flow: .replacementCard, action: .numeralPassword))
    }
}
