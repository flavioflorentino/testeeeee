import AssetsKit
import Core
import Foundation
import UI
import FeatureFlag

protocol DeliveryConfirmAddressPresenting: AnyObject {
    var viewController: DeliveryConfirmAddressDisplaying? { get set }
    func setupScreen(with flow: DeliveryConfirmAddressFlow,
                     homeAddress: HomeAddress,
                     canChangeAddress: Bool,
                     isContractHidden: Bool,
                     isConfirmationEnable: Bool)
    func showLoading()
    func dismissLoading()
    func changeAddress()
    func close()
    func confirm(registrationStatus: RegistrationStatus?)
    func presentError(error: ApiError)
    func presentGenericError()
    func presentContract(checked: Bool)
    func presentContractReader(url: URL)
}

final class DeliveryConfirmAddressPresenter: DeliveryConfirmAddressPresenting {
    typealias Localizable = Strings.DeliveryConfirmAddress
    private let coordinator: DeliveryConfirmAddressCoordinating
    weak var viewController: DeliveryConfirmAddressDisplaying?

    init(coordinator: DeliveryConfirmAddressCoordinating) {
        self.coordinator = coordinator
    }
    
    func setupScreen(with flow: DeliveryConfirmAddressFlow,
                     homeAddress: HomeAddress,
                     canChangeAddress: Bool,
                     isContractHidden: Bool,
                     isConfirmationEnable: Bool) {
        let content = buildViewModelContent(with: flow, homeAddress: homeAddress, shouldBuildContract: !isContractHidden)
        let states = DeliveryConfirmAddressViewModelStates(canChangeAddress: canChangeAddress,
                                                           isContractHidden: isContractHidden,
                                                           isConfirmationEnable: isConfirmationEnable)

        let viewModel = DeliveryConfirmAddressViewModel(content: content,
                                                        states: states)
        
        viewController?.setupScreenData(with: viewModel)
    }
    
    func showLoading() {
        viewController?.displayLoading()
    }
    
    func dismissLoading() {
        viewController?.dismissLoading()
    }
    
    func changeAddress() {
        coordinator.perform(action: .changeAddress)
    }

    func close() {
        coordinator.perform(action: .close)
    }
    
    func confirm(registrationStatus: RegistrationStatus?) {
        coordinator.perform(action: .didConfirm(registrationStatus: registrationStatus))
    }
    
    func presentError(error: ApiError) {
        switch error {
        case .connectionFailure, .timeout, .unauthorized:
            guard let message = error.requestError?.message else {
                viewController?.displayAlertError(with: Localizable.FinishRegistration.errorOnRequest)
                return
            }
            viewController?.displayAlertError(with: message)
        default:
            viewController?.displayAlertError(with: Localizable.FinishRegistration.errorOnRequest)
        }
    }
    
    func presentGenericError() {
        viewController?.displayAlertError(with: Strings.Default.errorLoadingInformation)
    }
    
    func presentContract(checked: Bool) {
        viewController?.displayContract(checked: checked)
    }
    
    func presentContractReader(url: URL) {
        coordinator.presentSafariViewController(url: url)
    }
}

// MARK: Builders
private extension DeliveryConfirmAddressPresenter {
    func buildViewModelContent(with flow: DeliveryConfirmAddressFlow,
                               homeAddress: HomeAddress,
                               shouldBuildContract: Bool) -> DeliveryConfirmAddressViewModelContent {
        let image: UIImage
        let title: String
        let description: NSAttributedString
        let buttonTitle: String

        switch flow {
        case .debit:
            image = Card.Assets.iconUpgradeDebit.image
            title = Localizable.Debit.title
            let attributtedDescription = NSMutableAttributedString(string: Localizable.Debit.description)
            attributtedDescription.font(text: Localizable.Debit.highlight,
                                        font: Typography.bodyPrimary(.highlight).font())
            description = attributtedDescription
            buttonTitle = Localizable.Debit.confirmButton
        case .multiple, .upgrade:
            image = Resources.Illustrations.iluPersonHappyCard.image
            title = Localizable.Multiple.title
            description = NSAttributedString(string: Localizable.Multiple.description)
            buttonTitle = Localizable.Multiple.confirmButton
        default:
            image = Resources.Illustrations.iluPersonHappyCard.image
            title = Localizable.Default.title
            description = NSAttributedString(string: Localizable.Default.description)
            buttonTitle = Localizable.Default.confirmButton
        }
        
        return DeliveryConfirmAddressViewModelContent(image: image,
                                                      title: title,
                                                      description: description,
                                                      buttonTitle: buttonTitle,
                                                      address: buildAddress(homeAddress: homeAddress),
                                                      contractTitle: shouldBuildContract ? buildContractReaderButton() : nil)
    }
    
    func buildAddress(homeAddress: HomeAddress) -> NSAttributedString {
        var address = ""
        address.append(getFormattedStreet(streetType: homeAddress.streetType, street: homeAddress.street))
        address.append(getFormattedNeighborhood(homeAddress.neighborhood))
        address.append(getFormattedStreetNumber(homeAddress.streetNumber))
        address.append(getFormattedAddressComplement(homeAddress.addressComplement))
        address.append(getFormattedState(homeAddress.state))
        address.append(getFormattedCity(homeAddress.city))
        address.append(getFormattedZipCode(homeAddress.zipCode))
        
        if address.isEmpty {
            return NSAttributedString(string: "-")
        } else {
            let attributedText = NSMutableAttributedString(string: address)
            attributedText.font(text: address, font: Typography.bodySecondary().font())
            attributedText.paragraphStyle(text: address, paragraphStyle: paragraphAttributed())
            return attributedText
        }
    }
    
    func buildContractReaderButton() -> NSAttributedString {
        let contractText = Localizable.Contract.message
        let attributedText = NSMutableAttributedString(string: contractText)
        attributedText.font(text: contractText, font: Typography.bodyPrimary().font())
        attributedText.paragraphStyle(text: contractText, paragraphStyle: paragraphAttributed())
        attributedText.textColor(text: contractText, color: Colors.grayscale800.color)
        attributedText.textColor(text: Localizable.Contract.highlight, color: Colors.branding600.color)
        attributedText.underline(text: Localizable.Contract.highlight)
        
        return attributedText
    }
    
    func paragraphAttributed() -> NSParagraphStyle {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.26
        paragraphStyle.alignment = .left
        paragraphStyle.lineBreakMode = .byWordWrapping
        return paragraphStyle
    }
}

// MARK: Address getters
extension DeliveryConfirmAddressPresenter {
    func getFormattedStreet(streetType: String?, street: String?) -> String {
        guard let street = getWithoutSeparators(street) else {
            return ""
        }

        if let streetType = getWithoutSeparators(streetType) {
            return "\(streetType) \(street)"
        }
        return street
    }
    
    func getFormattedStreetNumber(_ streetNumber: String?) -> String {
        guard
            let streetNumber = getWithoutSeparators(streetNumber, requireMinimumTwoCharacters: false),
            streetNumber != "0"
            else {
                return ""
        }
        return " \(streetNumber)"
    }
    
    func getFormattedNeighborhood(_ neighborhood: String?) -> String {
        guard let neighborhood = getWithoutSeparators(neighborhood) else {
            return ""
        }
        return ", \(neighborhood)"
    }
    
    func getFormattedAddressComplement(_ addressComplement: String?) -> String {
        guard let addressComplement = getWithoutSeparators(addressComplement) else {
            return ""
        }
        
        return " - \(addressComplement)"
    }
    
    func getFormattedState(_ state: String?) -> String {
        guard let state = getWithoutSeparators(state) else {
            return ""
        }
        return " - \(state)"
    }
    
    func getFormattedCity(_ city: String?) -> String {
        guard let city = getWithoutSeparators(city) else {
            return ""
        }
        return " - \(city)"
    }
    
    func getFormattedZipCode(_ zipCode: String?) -> String {
        let mask = CustomStringMask(mask: "00000-000")
        guard
            let zipCode = getWithoutSeparators(zipCode),
            let zipCodeMasked = mask.maskedText(from: zipCode)
            else {
            return ""
        }
        return ", \(zipCodeMasked)"
    }
    
    func getWithoutSeparators(_ value: String?, requireMinimumTwoCharacters: Bool = true) -> String? {
        let minimumTwoCharactersPattern = "[A-Za-z0-9].*[A-Za-z0-9]"
        let minimumOneCharacterPattern = "[A-Za-z0-9]+"
        let pattern = requireMinimumTwoCharacters ? minimumTwoCharactersPattern : minimumOneCharacterPattern
        return getMatchValue(value, pattern: pattern)
    }
    
    func getMatchValue(_ value: String?, pattern: String) -> String? {
        guard
            let value = value,
            let regex = try? NSRegularExpression(pattern: pattern),
            let match = regex.firstMatch(in: value, range: NSRange(value.startIndex..., in: value)),
            let range = Range(match.range, in: value)
            else {
                return nil
        }
        return String(value[range])
    }
}
