import FeatureFlag
import Foundation

protocol DeliveryConfirmAddressInteracting: AnyObject {
    func setupScreen()
    func confirm()
    func close()
    func changeAddress()
    func checkContract()
    func readContract()
}

final class DeliveryConfirmAddressInteractor {
    typealias Dependencies = HasAuthManagerDependency & HasFeatureManager & HasPPAuthSwiftContract
    private let dependencies: Dependencies

    private let service: DeliveryConfirmAddressServicing
    private let presenter: DeliveryConfirmAddressPresenting
    private var deliveryAddress: HomeAddress?
    private var analyticsContext: DeliveryConfirmAddressEvents
    private var contractCheckedState = false

    private enum ErrorCode: String {
        case wrongPassword = "6003"
        case blockMinutes = "5006"
    }
    
    init(service: DeliveryConfirmAddressServicing,
         presenter: DeliveryConfirmAddressPresenting,
         deliveryAddress: HomeAddress?,
         analyticsContext: DeliveryConfirmAddressEvents,
         dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.deliveryAddress = deliveryAddress
        self.analyticsContext = analyticsContext
        self.dependencies = dependencies
    }
    
    private func didConfirmAddress(with password: String? = nil) {
        presenter.showLoading()
        service.registrationFinish(password: password) { [weak self] result in
            guard let self = self else {
                return
            }
            switch result {
            case .success(let registrationStatus):
                self.presenter.confirm(registrationStatus: registrationStatus)
                self.analyticsContext.authValidated()
            case .failure(let error):
                self.analyticsContext.authFailed()
                guard let code = error.requestError?.code, ErrorCode(rawValue: code) != nil else {
                    self.presenter.presentError(error: error)
                    return
                }
                self.dependencies.ppauth.handlePassword()
                self.presenter.dismissLoading()
            }
        }
    }
}

extension DeliveryConfirmAddressInteractor: DeliveryConfirmAddressInteracting {
    func setupScreen() {
        guard let homeAddress = deliveryAddress else {
            return
        }
        
        let flowNeedsContract = service.flow == .debit
        let confirmationEnable = service.flow == .debit ? contractCheckedState : true
        
        presenter.setupScreen(with: service.flow,
                              homeAddress: homeAddress,
                              canChangeAddress: service.canChangeAddress,
                              isContractHidden: !flowNeedsContract,
                              isConfirmationEnable: confirmationEnable)
    }
    
    func confirm() {
        analyticsContext.askCard()
        guard service.isConfirmNeeded else {
            presenter.confirm(registrationStatus: nil)
            return
        }
        dependencies.authManager.authenticate(sucessHandler: { [weak self] password, isBiometric in
            isBiometric ? self?.analyticsContext.authFingerprintUsed() : self?.analyticsContext.authNumeralPasswordUsed()
            self?.didConfirmAddress(with: password)
        }, cancelHandler: {
            self.analyticsContext.authCanceled()
        })
    }
    
    func close() {
        presenter.close()
    }
    
    func changeAddress() {
        guard service.canChangeAddress else {
            return
        }
        analyticsContext.editAddress()
        presenter.changeAddress()
    }
    
    func checkContract() {
        contractCheckedState.toggle()
        analyticsContext.checkContract()
        presenter.presentContract(checked: contractCheckedState)
    }
    
    func readContract() {
        let url = dependencies.featureManager.text(.linkContractDebitCard)
        if let contractURL = URL(string: url) {
            analyticsContext.readContract()
            presenter.presentContractReader(url: contractURL)
        } else {
            presenter.presentGenericError()
        }
    }
}
