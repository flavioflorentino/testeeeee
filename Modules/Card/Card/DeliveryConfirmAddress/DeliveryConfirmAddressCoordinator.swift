import SafariServices
import UIKit

enum DeliveryConfirmAddressAction {
    case didConfirm(registrationStatus: RegistrationStatus?)
    case changeAddress
    case close
}

// TODO: Refactor equatable and add unit tests
extension DeliveryConfirmAddressAction: Equatable {
    static func == (lhs: DeliveryConfirmAddressAction, rhs: DeliveryConfirmAddressAction) -> Bool {
        switch (rhs, lhs) {
        case (didConfirm, didConfirm), (changeAddress, changeAddress), (close, close):
            return true
        default:
            return false
        }
    }
}

protocol DeliveryConfirmAddressCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: DeliveryConfirmAddressAction)
    func presentSafariViewController(url: URL)
}

typealias DeliveryConfirmAddressCoordinatorOutput = (_ deliveryAddressAction: DeliveryConfirmAddressAction) -> Void

final class DeliveryConfirmAddressCoordinator: DeliveryConfirmAddressCoordinating {
    weak var viewController: UIViewController?
    private var coordinatorOutput: DeliveryConfirmAddressCoordinatorOutput
    
    init(coordinatorOutput: @escaping DeliveryConfirmAddressCoordinatorOutput) {
        self.coordinatorOutput = coordinatorOutput
    }
    
    func perform(action: DeliveryConfirmAddressAction) {
        coordinatorOutput(action)
    }
    
    func presentSafariViewController(url: URL) {
        open(url: url)
    }
}

// MARK: - SafariWebViewPresentable
extension DeliveryConfirmAddressCoordinator: SafariWebViewPresentable {}
