import AnalyticsModule

public enum CreditHiringEvent: AnalyticsKeyProtocol {
    case preApproved
    case resume(action: CardResumeAction)
    case dueDay(action: CardDueDayAction)
    case authentication(action: AuthenticationAction)
    case authenticated(state: AuthenticatedState)
    case cardActivated(action: CardActivatedAction)

    case changeAddress
    case cancelChangeAddress

    private var name: String {
        let eventName: String
        
        switch self {
        case .preApproved:
            eventName = "Config Card Approved"
        case .resume:
            eventName = "Accept Terms"
        case .dueDay:
            eventName = "Config Due Date"
        case .authentication:
            eventName = "Password Authentication"
        case .authenticated:
            eventName = "Password Authenticated"
        case .cardActivated:
          eventName = "Card Activated Message"
        case .changeAddress, .cancelChangeAddress:
            eventName = "Sign Up - Address"
        }
        
        return "PicPay Card - Request - \(eventName)"
    }
    
    private var properties: [String: Any] {
        switch self {
        case .preApproved:
            return ["action": "act-card-config"]
        case .resume(let action):
            return ["action": action.rawValue]
        case .dueDay(let action):
            return ["action": action.rawValue]
        case .authentication(let action):
            return ["action": action.rawValue]
        case .authenticated(let state):
            return ["state": state.rawValue]
        case .cardActivated(let action):
            return ["action": action.rawValue]
        case .changeAddress:
            return ["action": "act-change-address"]
        case .cancelChangeAddress:
            return ["action": "act-cancel-change-address"]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        if case .cardActivated = self {
            return  [.mixPanel, .appsFlyer]
        }
        return [.mixPanel]
    }
    
    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}

public extension CreditHiringEvent {
    enum CardResumeAction: String {
        case request = "act-card-agree-terms"
        case viewTerms = "act-view-terms"
        case finishSignup = "act-finish-signup"
    }
        
    enum CardDueDayAction: String {
        case selectDay = "act-due-date"
        case saveDay = "act-next-step"
    }
    
    enum CardActivatedAction: String {
        case request = "act-request-plastic-card"
        case help = "act-help-center"
    }
    
    enum AuthenticationAction: String {
        case cancel = "act-fingerprint-cancel"
        case byometricPassword = "act-fingerprint-used"
        case numeralPassword = "act-numeral-password-used"
    }
    
    enum AuthenticatedState: String {
        case failed = "st-authentication-failed"
        case validaded = "st-authentication-validated"
    }
}
