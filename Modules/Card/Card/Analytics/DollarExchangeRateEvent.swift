import AnalyticsModule

public enum DollarExchangeRateEvent: AnalyticsKeyProtocol {
    case didTapSeeAllHistory
    case didTapHelp
    case didTapBack
    case didTapTryAgain
    case didGenericError
    case didConnectionError
    case didOpenHistory
    case didTap15Days
    case didTap30Days
    case didTap180Days
    
    private var name: String {
        switch self {
        case .didTapSeeAllHistory, .didTapHelp, .didTapBack:
            return "Dollar quote"
        case .didTapTryAgain, .didGenericError:
            return "Generic error"
        case .didConnectionError:
            return "No Connection Error"
        case .didOpenHistory, .didTap15Days, .didTap30Days, .didTap180Days:
            return "Dollar historic"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .didTapSeeAllHistory:
            return ["action": "act-all-historic"]
        case .didTapHelp:
            return ["action": "act-help"]
        case .didTapBack:
            return ["action": "act-back"]
        case .didTapTryAgain:
            return ["action": "act-try-again"]
        case .didGenericError:
            return ["state": "st-server-error"]
        case .didConnectionError:
            return ["state": "st-connection-error"]
        case .didTap15Days:
            return ["action": "act-15-days"]
        case .didTap30Days:
            return ["action": "act-30-days"]
        case .didTap180Days:
            return ["action": "act-180-days"]
        case .didOpenHistory:
            return ["state": "st-open-historic"]
        }
    }
    
    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PicPay Card - Dollar Exchange Rate - \(name)", properties: properties, providers: [.mixPanel, .appsFlyer, .firebase])
    }
}
