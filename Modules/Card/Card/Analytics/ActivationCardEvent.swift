import AnalyticsModule

enum ActivationCardEvent: AnalyticsKeyProtocol {
    case didTapContractView(action: ContractActionType)
    case didTapActivatedCard
    case didShowActivatedCardError(reason: ActivationErrors)
    case didTapNewPasswordAction(action: NewPasswordActionType, visibility: PasswordVisibility)
    case didShowNewPasswordError(reason: NewPasswordErrors)
    case didAuthenticate

    private var name: String {
        switch self {
        case .didTapContractView:
            return "Contract Review"
        case .didTapActivatedCard:
            return "Numbers Checked"
        case .didShowActivatedCardError:
            return "Numbers Checkd Error"
        case .didTapNewPasswordAction:
            return "New Password"
        case .didShowNewPasswordError:
            return "New Password Error"
        case .didAuthenticate:
            return "Activated"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .didTapContractView(let action):
            return ["option_selected": action.rawValue]
        case .didShowActivatedCardError(let reason):
            return ["reason": reason.rawValue]
        case let .didTapNewPasswordAction(action, visibility):
            return [
                "action": action.rawValue,
                "state": visibility.rawValue
            ]
        case .didShowNewPasswordError(let reason):
            return ["reason": reason.rawValue]
        case .didTapActivatedCard, .didAuthenticate:
            return [:]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PicPay Card Activation - \(name)", properties: properties, providers: providers)
    }
}

extension ActivationCardEvent {
    enum ContractActionType: String {
        case contractChecked = "CONTRACT CHECKED"
        case readAndAgree = "READ AND AGREE"
        case contractOpened = "CONTRACT OPENED"
        case back = "BACK"
    }
    
    enum ActivationErrors: String {
        case digitsWrong = "LAST 4 DIGITS WRONG"
    }
    
    enum NewPasswordActionType: String {
        case info = "act-info"
        case magicEye = "act-magic-eye"
        case finished = "act-new-password"
    }

    enum PasswordVisibility: String {
        case show = "st-show-password"
        case hide = "st-hide-password"
    }
    
    enum NewPasswordErrors: String, CaseIterable {
        case sequencial = "SEQUENCIAL NUMBERS"
        case personal = "PERSONAL NUMBERS"
        case authentication = "AUTHENTICATION FAILED"
        
        private var description: String {
            switch self {
            case .sequencial:
                return "números sequenciais"
            case .personal:
                return "informações pessoais"
            case .authentication:
                return "Usuário/Senha incorretos"
            }
        }
        
        init?(message: String) {
            guard let type = NewPasswordErrors.allCases.first(where: { message.contains($0.description) }) else {
                return nil
            }
            self = type
        }
    }
}
