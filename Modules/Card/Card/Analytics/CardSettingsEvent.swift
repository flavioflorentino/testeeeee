import AnalyticsModule

enum CardSettingsEvent: AnalyticsKeyProtocol {
    case action(action: CardSettingsEventAction)
    case state(state: CardSettingsEventState)
    
    private var name: String {
        "Card"
    }
    
    private var properties: [String: Any] {
        switch self {
        case .action(let action):
            return [
                "action": action.rawValue
            ]
        case .state(let state):
            return [
                "state": state.rawValue
            ]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PicPay Card - Configuration - \(name)", properties: properties, providers: providers)
    }
}

extension CardSettingsEvent {
    enum CardSettingsEventAction: String {
        case back = "act-back"
        case changePassword = "act-picpaycard-change-password"
        case blockCard = "act-picpaycard-block-card"
        case dueDate = "act-picpaycard-due-date"
        case myLimit = "act-picpaycard-my-limit"
        case replacementRequested = "act-new-card-requested"
        case replacementRequestable = "act-request-new-card"
        case dollarExchange = "act-picpaycard-dollar-quote"
        case virtualCard = "act-my-virtual-card"
    }
    
    enum CardSettingsEventState: String {
        case cardBlocked = "st-card-blocked"
    }
}
