import AnalyticsModule

enum CardReplacementRequestedEvent: AnalyticsKeyProtocol {
    case information(action: CardReplacementRequestedAction)
    
    private var name: String {
        "Information"
    }
    
    private var properties: [String: Any] {
        switch self {
        case .information(let action):
            return [
                "action": action.rawValue
            ]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PicPay Card - Request New Card - \(name)", properties: properties, providers: providers)
    }
}

extension CardReplacementRequestedEvent {
    enum CardReplacementRequestedAction: String {
        case back = "act-back"
        case faq = "act-FAQ"
    }
}
