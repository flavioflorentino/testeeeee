import AnalyticsModule

enum BlockReplacementCardEvent: AnalyticsKeyProtocol {
    case reason(action: ReasonAction, flow: CardBlockReplacementReasonTypeFlow)
    
    private var name: String {
        switch self {
        case .reason:
            return "Reason"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .reason(let action, _):
            return [
                "action": action.rawValue
            ]
        }
    }

    private var flow: String {
        switch self {
        case .reason(_, let flow):
            return (flow == .block) ? "Block Card" : "Request New Card"
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PicPay - \(flow) - \(name)", properties: properties, providers: providers)
    }
}

extension BlockReplacementCardEvent {
    enum ReasonAction: String {
        case broken = "act-broken-card"
        case lost = "act-reason-lost-card"
        case stolen = "act-reason-stolen-card"
        case requestNew = "act-request-new-card"
        case notNow = "act-not-now"
    }
}
