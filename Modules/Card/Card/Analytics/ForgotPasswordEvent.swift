import AnalyticsModule

public enum ForgotPasswordEvent: AnalyticsKeyProtocol {
    case unblockCard
    case unblockCardError
    
    private var name: String {
        switch self {
        case .unblockCard:
            return "Numbers Checked"
        case .unblockCardError:
            return "Numbers Checked Error"
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }

    private var properties: [String: Any] {
        switch self {
        case .unblockCard:
            return [
                "state": "st-numbers-checked"
            ]
        case .unblockCardError:
            return [
                "state": "st-checked-numbers"
            ]
        }
    }
    
    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PicPay Card - Forgot Password - \(name)", properties: properties, providers: providers)
    }
}
