import AnalyticsModule

public enum CreditRequestEvent: AnalyticsKeyProtocol {
    case confirmAddress(action: ConfirmAddressAction)

    private var name: String {
        "PicPay Card - Request - Sign Up - Confirm Address"
    }
    
    private var properties: [String: Any] {
        switch self {
        case .confirmAddress(let action):
            return [
                "action": action.rawValue
            ]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        guard case let .confirmAddress(action) = self, action == .askCard else {
            return [.mixPanel, .firebase]
        }
        return [.mixPanel, .firebase, .appsFlyer]
    }
    
    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}

public extension CreditRequestEvent {
    enum ConfirmAddressAction: String {
        case editAddress = "act-change-address"
        case askCard = "act-request-plastic-card"
    }
}
