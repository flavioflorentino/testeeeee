import AnalyticsModule

public enum CardDeliveryAddressEvent: AnalyticsKeyProtocol {
    case didTapNotKnowMyPostalCode
    case didGetPostalCode(result: Bool)
    case didTapContinue(editedAddress: Bool)
    case didTapIsNotMyPostalCode
    case didTapBack
    case didTapNoNumber(checked: Bool)
    
    private var properties: [String: Any] {
        switch self {
        case .didTapNotKnowMyPostalCode:
            return ["action": "act-not-know-postal-code"]
        case let .didGetPostalCode(result):
            return ["state": "st-postal-code-\(result)"]
        case let .didTapContinue(editedAddress):
            guard editedAddress else {
                return ["action": "act-next-step"]
            }
            return ["action": "act-next-step", "state": "st-edit-address"]
        case .didTapIsNotMyPostalCode:
            return ["action": "act-not-my-postal-code"]
        case .didTapBack:
            return ["action": "act-previous-step"]
        case let .didTapNoNumber(checked):
            guard checked else {
                return ["action": "act-no-number"]
            }
            return ["action": "act-no-number", "state": "checked"]
        }
    }

    private var name: String {
        switch self {
        case .didTapIsNotMyPostalCode,
             .didGetPostalCode:
            return "Postal Code"
        default:
            return "Address"
        }
    }
    
    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PicPay Card  - Request - Sign Up - \(name)", properties: properties, providers: [.mixPanel, .appsFlyer, .firebase])
    }
}
