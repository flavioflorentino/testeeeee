import AnalyticsModule
import Foundation

public enum UnblockCardEvent: AnalyticsKeyProtocol {
    case contract(action: ContractUnblockState)
    case walletOfferClick
    case unblockCard(state: DebitUnblockState)
    case newPassword(action: DebitNewPasswordAction, state: Bool)
    case newPasswordError(state: DebitUnblockErrorState)
    case newPasswordAuth(action: DebitNewPasswordAuthErrorAction)
    case newPasswordAuthenticated(state: DebitNewPasswordAuthenticatedState)
    case passwordTips
    case success(action: SucessPasswordAction)
    case creditSuccess(action: CreditSucessAction)
    
    private var name: String {
        switch self {
        case .contract:
            return "Accept Terms"
        case .walletOfferClick:
            return "Wallet Offer Clicked"
        case .unblockCard:
            return "Numbers Checked"
        case .newPassword:
            return "New Password"
        case .newPasswordError:
            return "New Password Error"
        case .newPasswordAuth:
            return "New Password Authentication"
        case .newPasswordAuthenticated:
            return "New Password Authenticated"
        case .passwordTips:
            return "New Password Instructions"
        case .success:
            return "Success and Add Money"
        case .creditSuccess:
            return "Success"
        }
    }
    
    private var providers: [AnalyticsProvider] {
        if case .success = self {
            return [.mixPanel, .firebase, .appsFlyer]
        }
        return [.mixPanel, .firebase]
    }

    private var properties: [String: Any] {
        switch self {
        case .contract(let action):
            return [
                "action": action.rawValue
            ]
        case .unblockCard(let state):
            return [
                "state": state.rawValue
            ]
        case let .newPassword(action, state):
            return [
                "state": DebitNewPasswordState(isPasswordHidden: state).rawValue,
                "action": action.rawValue
            ]
        case .newPasswordError(let state):
            return [
                "state": state.rawValue
            ]
        case .newPasswordAuth(let action):
            return [
                "action": action.rawValue
            ]
        case .newPasswordAuthenticated(let state):
            return [
                "state": state.rawValue
            ]
        case .success(let action):
            return [
                "action": action.rawValue
            ]
        case .creditSuccess(let action):
            return [
                "action": action.rawValue
            ]
        case .walletOfferClick, .passwordTips:
            return [:]
        }
    }
    
    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PicPay Card - Unblock - \(name)", properties: properties, providers: providers)
    }
}

public extension UnblockCardEvent {
    enum ContractUnblockState: String {
        case agree = "act-agree-terms"
        case read = "act-read-terms"
        case sign = "act-sign-terms"
    }
    
    enum DebitUnblockState: String {
        case numbersChecked = "st-numbers-checked"
        case numbersError = "st-numbers-error"
    }
    
    enum DebitUnblockErrorState: String, CaseIterable {
        case sequencial = "st-change-password-sequential-numbers"
        case personal = "st-change-password-personal-numbers"
        case authentication = "st-password-error-authentication"
        case numbers = "st-password-format-error"
        
        private var description: String {
            switch self {
            case .sequencial:
                return "números sequenciais"
            case .personal:
                return "informações pessoais"
            case .authentication:
                return "Usuário/Senha incorretos"
            default:
                return ""
            }
        }
        
        init(message: String) {
            guard let type = DebitUnblockErrorState.allCases.first(where: { message.contains($0.description) }) else {
                self = .numbers
                return
            }
            self = type
        }
    }
    
    enum SucessPasswordAction: String {
        case addMoney = "act-add-money"
        case backHome = "act-back-home"
    }
    
    enum DebitNewPasswordAction: String {
        case finishTap = "act-finished-unblock"
        case eyeButtonTap = "act-magic-eye"
        case infoButtonTap = "act-info"
    }
    
    enum CreditSucessAction: String {
        case confirm = "act-success-activated-card"
    }
    
    enum DebitNewPasswordState: String {
        init(isPasswordHidden: Bool) {
            self = isPasswordHidden ? .passwordHide : .passwordShow
        }
        case passwordShow = "st-show-password"
        case passwordHide = "st-hide-password"
    }
    
    enum DebitNewPasswordAuthErrorAction: String {
        case cancel = "act-fingerprint-cancel"
        case byometricPassword = "act-fingerprint-used"
        case numeralPassword = "act-numeral-password-used"
    }
    
    enum DebitNewPasswordAuthenticatedState: String {
        case failed = "st-authentication-failed"
        case validaded = "st-authentication-validated"
    }
}
