import AnalyticsModule

enum CardBlockedEvent: AnalyticsKeyProtocol {
    case success(action: CardBlockedAction)
    
    private var name: String {
        switch self {
        case .success:
            return "Card Blocked Success"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .success(let action):
            return [
                "action": action.rawValue
            ]
        }
    }

    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PicPay Card - Block Card - \(name)", properties: properties, providers: providers)
    }
}

extension CardBlockedEvent {
    enum CardBlockedAction: String {
        case notNow = "act-not-now"
        case requestable = "act-request-new-card"
        case notRequestable = "act-go-configuration-card"
    }
}
