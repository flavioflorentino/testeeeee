import AnalyticsModule

public enum CardTrackingEvent: AnalyticsKeyProtocol {
    case didTapConfirm(type: CardTrackingType)
    case didTapHelpCenter(type: CardTrackingType)
    case didServerError
    case didInternetError
    case didOfferClicked
    case didTapCardNotReceived
    case didTapCardReceived
    
    private var name: String {
        switch self {
        case .didTapConfirm(let cardType), .didTapHelpCenter(let cardType):
            if let cardTypeName = cardType.analyticsName {
                return "Unblock Card - \(cardTypeName)"
            }
            return "Unblock Card"
        case .didServerError:
            return "Connection Error"
        case .didInternetError:
            return "Internet Connection Error"
        case .didOfferClicked:
            return "Offer Clicked"
        case .didTapCardReceived, .didTapCardNotReceived:
            return "Status Update"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .didTapConfirm:
            return ["action": "act-unblock-card"]
        case .didTapHelpCenter:
            return ["action": "act-help-center"]
        case .didServerError, .didInternetError:
            return ["action": "act-try-again"]
        case .didTapCardReceived:
            return ["action": "act-card-received"]
        case .didTapCardNotReceived:
            return ["action": "act-card-not-received"]
        default:
            return [:]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase, .appsFlyer]
    }
    
    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PicPay Card - Card Tracking - \(name)", properties: properties, providers: providers)
    }
}
