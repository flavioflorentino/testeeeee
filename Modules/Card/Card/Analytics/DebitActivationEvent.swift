import AnalyticsModule

public enum DebitActivationEvent: AnalyticsKeyProtocol {
    case onboarding(action: DebitOnboardingEventAction)
    case deliveryAddress
    case confirmAddress(action: DebitConfirmAddressEventAction)
    case dialogError(state: DialogErrorEventAction)
    case dialogErrorTryAgain(state: DialogErrorEventAction)
    case authentication(action: AuthenticationAction)
    case authenticated(state: AuthenticatedState)
    case waitingActivation
    case dropOffContinue
    
    private var name: String {
        switch self {
        case .onboarding:
            return "Offer Webview"
        case .deliveryAddress:
            return "Sign Up - Address"
        case .confirmAddress:
            return "Sign Up - Confirm Address"
        case .dialogError:
            return "Sign Up - Dialog Error"
        case .dialogErrorTryAgain:
            return "Sign Up - Dialog Try Again"
        case .waitingActivation:
            return "Sign Up - Not Approved Analysis"
        case .authentication:
            return "Sign Up - Authentication"
        case .authenticated:
            return "Sign Up - Authenticated"
        case .dropOffContinue:
            return "Sign Up - Drop Off Continue"
        }
    }
    
    private var providers: [AnalyticsProvider] {
        guard case let .onboarding(action) = self, action == .nextStep else {
            return [.mixPanel, .firebase]
        }
        return [.mixPanel, .firebase, .appsFlyer]
    }

    private var properties: [String: Any] {
        switch self {
        case .onboarding(let action):
            return [
                "action": action.rawValue
            ]
        case .deliveryAddress:
            return [
                "action": "act-next-step"
            ]
        case .confirmAddress(let action):
            return [
                "action": action.rawValue
            ]
        case .dialogError(let state), .dialogErrorTryAgain(let state):
            return [
                "state": state.rawValue
            ]
        case .waitingActivation:
            return [
                "action": "act-try-again"
            ]
        case .authentication(let action):
            return [
                "action": action.rawValue
            ]
        case .authenticated(let state):
            return [
                "state": state.rawValue
            ]
        case .dropOffContinue:
            return [
                "action": "act-signup-continue"
            ]
        }
    }
    
    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PicPay Card - Request - \(name)", properties: properties, providers: providers)
    }
}

public extension DebitActivationEvent {
    enum DebitOnboardingEventAction: String {
        case nextStep = "act-request-card"
        case help = "act-help-center"
        case dismiss = "act-webview-close"
    }
    
    enum DebitConfirmAddressEventAction: String {
        case changeAddress = "act-change-address"
        case confirmAddress = "act-request-plastic-card"
        case checkContract = "act-agree-terms"
        case readContract = "act-read-terms"
    }
    
    enum DialogErrorEventAction: String {
        case server = "st-server-error"
        case general = "st-connection-error"
    }
    
    enum AuthenticationAction: String {
        case cancel = "act-fingerprint-cancel"
        case byometricPassword = "act-fingerprint-used"
        case numeralPassword = "act-numeral-password-used"
    }
    
    enum AuthenticatedState: String {
        case failed = "st-authentication-failed"
        case validaded = "st-authentication-validated"
    }
}
