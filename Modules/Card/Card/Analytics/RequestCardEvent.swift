import AnalyticsModule

public enum RequestCardEvent: AnalyticsKeyProtocol {
    case applyCard
    case didViewPersonalInfoScreen
    case offerCard(action: OfferWebviewAction, isLoan: Bool)
    case personalInfo(action: PersonalInfoAction, isLoan: Bool)
    case birthplace(action: BirthplaceAction, isLoan: Bool)
    case address(action: AddressAction, isLoan: Bool)
    case idDocument(action: IDDocumentAction, isLoan: Bool)
    case additionalPersonalInfo(action: AdditionalPersonalAction, isLoan: Bool)
    case infoAnalysis(action: InfoAnalysisAction, isLoan: Bool?)
    
    private var name: String {
        let eventName: String
        let isLoanRequest: Bool?
        
        switch self {
        case .applyCard:
            isLoanRequest = false
            eventName = "Apply Card"
        case .didViewPersonalInfoScreen:
            isLoanRequest = false
            eventName = "Sign Up - Personal Info"
        case let .offerCard(_, isLoan):
            isLoanRequest = isLoan
            eventName = "Offer Webview"
        case let .personalInfo(_, isLoan):
            isLoanRequest = isLoan
            eventName = "Sign Up - Personal Info"
        case let .birthplace(_, isLoan):
            isLoanRequest = isLoan
            eventName = "Sign Up - Add Birthplace"
        case let .address(_, isLoan):
            isLoanRequest = isLoan
            eventName = "Sign Up - Address"
        case let .idDocument(_, isLoan):
            isLoanRequest = isLoan
            eventName = "Sign Up - ID Document"
        case let .additionalPersonalInfo(_, isLoan):
            isLoanRequest = isLoan
            eventName = "Sign Up - Additional Personal Info"
        case let .infoAnalysis(_, isLoan):
            isLoanRequest = isLoan
            eventName = "Sign Up - Personal Info Analysis"
        }
        
        return "\(self.prefix(forLoanRequest: isLoanRequest)) Request - \(eventName)"
    }
    
    private var properties: [String: Any] {
        switch self {
        case .applyCard, .didViewPersonalInfoScreen:
            return [:]
        case .offerCard(let action, _):
            return [
                "action": action.rawValue
            ]
        case .address(let action, _):
            return [
                "action": action.rawValue
            ]
        case .birthplace(let action, _):
            return [
                "action": action.rawValue
            ]
        case .idDocument(let action, _):
            return [
                "action": action.rawValue
            ]
        case .personalInfo(let action, _):
            return [
                "action": action.rawValue
            ]
        case .additionalPersonalInfo(action: let action, _):
            return [
                "action": action.rawValue
            ]
        case .infoAnalysis(action: let action, _):
            return [
                "action": action.rawValue
            ]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        switch self {
        case let .offerCard(action, isLoan) where action == .request && !isLoan:
            return [.mixPanel, .firebase, .appsFlyer]
        case .infoAnalysis:
            return [.mixPanel, .firebase, .appsFlyer]
        default:
            return [.mixPanel, .firebase]
        }
    }
    
    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
    
    private func prefix(forLoanRequest isLoan: Bool?) -> String {
        isLoan == true ? "Loan" : "PicPay Card"
    }
}

public extension RequestCardEvent {
    enum OfferWebviewAction: String {
        case request = "act-request-card"
        case help = "act-help-center"
        case seeRules = "act-see-rules"
        case close = "act-webview-close"
    }
    
    enum PersonalInfoAction: String {
        case nextStep = "act-next-step"
        case previousStep = "act-previous-step"
    }
    
    enum BirthplaceAction: String {
        case nextStep = "act-next-step"
        case previousStep = "act-previous-step"
    }
    
    enum AddressAction: String {
        case nextStep = "act-next-step"
        case previousStep = "act-previous-step"
        case addComercialAddress = "act-add-commercial-address"
    }
    
    enum IDDocumentAction: String {
        case nextStep = "act-next-step"
        case previousStep = "act-previous-step"
        case selectCNH = "act-select-cnh"
        case selectRG = "act-select-rg"
        case selectFuncional = "act-select-funcional"
        case selectRNE = "act-select-rne"
        
        public init?(document: String) {
            switch document {
            case "CNH":
                self = .selectCNH
            case "RG":
                self = .selectRG
            case "RNE":
                self = .selectRNE
            case "FUNCIONAL":
                self = .selectFuncional
            default:
                return nil
            }
        }
    }
    
    enum AdditionalPersonalAction: String {
        case nextStep = "act-next-step"
        case previousStep = "act-previous-step"
    }
    
    enum InfoAnalysisAction: String {
        case close = "act-analysis-close"
        case viewSummary = "act-view-summary"
    }
}
