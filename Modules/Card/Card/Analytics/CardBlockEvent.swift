import AnalyticsModule

enum CardBlockEvent: AnalyticsKeyProtocol {
    case confirmBlock(action: CardBlockAction)
    case authenticated(action: CardBlockAction)
    
    private var name: String {
        switch self {
        case .confirmBlock:
            return "Confirm Block Card"
        case .authenticated:
            return "Authenticated"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .confirmBlock(let action):
            return [
                "action": action.rawValue
            ]
        case .authenticated(let action):
            return [
                "action": action.rawValue
            ]
        }
    }

    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PicPay Card - Block Card - \(name)", properties: properties, providers: providers)
    }
}

extension CardBlockEvent {
    enum CardBlockAction: String {
        case confirm = "act-confirm-block-card"
        case notNow = "act-not-now"
        case fingerprintCancel = "act-fingerprint-cancel"
        case fingerprintUsed = "act-fingerprint-used"
        case numeralPasswordUsed = "act-numeral-password-cancel"
    }
}
