import AnalyticsModule

public enum CardUpgradeDebitEvent: AnalyticsKeyProtocol {
    case cardActivated(action: CardActivatedAction)
    
    private var name: String {
        let eventName: String
        
        switch self {
        case .cardActivated:
            eventName = "Card Activated Message"
        }
        
        return "PicPay Card - Upgrade - \(eventName)"
    }
    
    private var properties: [String: Any] {
        switch self {
        case .cardActivated(let action):
            return ["action": action.rawValue]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase, .appsFlyer]
    }
    
    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}

public extension CardUpgradeDebitEvent {
    enum CardActivatedAction: String {
        case openWallet = "act-go-wallet"
        case help = "act-help-center"
    }
}
