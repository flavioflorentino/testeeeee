import AnalyticsModule

public enum DebitUnblockEvent: AnalyticsKeyProtocol {
    case walletOfferClick
    case unblockCard(state: DebitUnblockState)
    
    private var name: String {
        switch self {
        case .walletOfferClick:
            return "Wallet Offer Clicked"
        case .unblockCard:
            return "Numbers Checked"
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }

    private var properties: [String: Any] {
        switch self {
        case .unblockCard(let state):
            return [
                "state": state.rawValue
            ]
        case .walletOfferClick:
            return [:]
        }
    }
    
    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PicPay Card - Unblock - \(name)", properties: properties, providers: providers)
    }
}

public extension DebitUnblockEvent {
    enum DebitUnblockState: String {
        case numbersChecked = "st-numbers-checked"
        case numbersError = "st-numbers-error"
    }
}
