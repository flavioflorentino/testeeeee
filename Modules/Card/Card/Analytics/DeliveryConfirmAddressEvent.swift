import AnalyticsModule

enum DeliveryConfirmAddressEvent: AnalyticsKeyProtocol {
    case confirmAddress(flow: DeliveryConfirmAddressFlow, action: DeliveryConfirmAddressAction)
    case success(flow: DeliveryConfirmAddressFlow, action: DeliveryConfirmAddressAction)
    case authentication(flow: DeliveryConfirmAddressFlow, action: AuthenticationAction)
    case authenticated(flow: DeliveryConfirmAddressFlow, state: AuthenticatedState)
    
    private var name: String {
        switch self {
        case .confirmAddress:
            return "Confirm Address"
        case .success:
            return "Confirm Address"
        case .authentication:
            return "Confirm Address - Authentication"
        case .authenticated:
            return "Confirm Address - Authenticated"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .confirmAddress(_, let action):
            return [
                "action": action.rawValue
            ]
        case .success(_, let action):
            return [
                "action": action.rawValue
            ]
        case .authentication(_, let action):
            return [
                "action": action.rawValue
            ]
        case .authenticated(_, let state):
            return [
                "state": state.rawValue
            ]
        }
    }

    private var flow: String {
        switch self {
        case .confirmAddress(let flow, _):
            return flow.rawValue
        case .success(let flow, _):
            return flow.rawValue
        case .authentication(let flow, _):
            return flow.rawValue
        case .authenticated(let flow, _):
            return flow.rawValue
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PicPay Card - \(flow) - \(name)", properties: properties, providers: providers)
    }
}

extension DeliveryConfirmAddressEvent {
    enum DeliveryConfirmAddressFlow: String {
        case replacementCard = "New Card"
        case credit = "Credit"
        case debit = "Debit"
    }
    
    enum DeliveryConfirmAddressAction: String {
        case notNow = "act-not-now"
        case confirm = "act-confirm-address"
        case faq = "act-FAQ"
        case finish = "act-go-configuration-card"
    }
    
    enum AuthenticationAction: String {
        case cancel = "act-fingerprint-cancel"
        case byometricPassword = "act-fingerprint-used"
        case numeralPassword = "act-numeral-password-used"
    }
    
    enum AuthenticatedState: String {
        case failed = "st-authentication-failed"
        case validaded = "st-authentication-validated"
    }
}
