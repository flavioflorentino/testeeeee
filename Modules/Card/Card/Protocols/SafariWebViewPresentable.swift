import Foundation
import SafariServices
import UIKit

protocol SafariWebViewPresentable {
    var viewController: UIViewController? { get set }
}

extension SafariWebViewPresentable {
    /// Opens the URL inside the app using SFSafariViewController
    func open(url: URL) {
        let safariVC = SFSafariViewController(url: url)
        if #available(iOS 11.0, *) {
            safariVC.configuration.barCollapsingEnabled = true
            safariVC.configuration.entersReaderIfAvailable = true
        }
        viewController?.present(safariVC, animated: true, completion: nil)
    }
}
