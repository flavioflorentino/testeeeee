import CustomerSupport
import Foundation
import UIKit

protocol HelpcenterDeeplinkPresentable {
    var viewController: UIViewController? { get set }
}

extension HelpcenterDeeplinkPresentable {
    func openHelpCenter(with url: URL) {
        let deeplinkResolver = HelpcenterDeeplinkResolver()
        _ = deeplinkResolver.open(url: url, isAuthenticated: true, from: viewController ?? UIViewController())
    }
    
    func openZendesk() {
        let option = CardTrackingHistoryCustomer().option()
        let faq = FAQFactory.make(option: option)
        viewController?.present(faq, animated: true)
    }
}
