import Foundation
import Core

enum CreditServiceEndpoint {
    typealias Dependencies = HasKeychainManager

    case creditAccount
    case creditAccountAddress
    case creditRegistrationFinish(password: String, originFlow: OriginFlow)
    case cardRequest(addressType: AddressType)
    case cardNewRequest(addressType: AddressType, password: String)
    case validate(String)
    case debitActivate(cardPin: String, password: String)
    case createPassword(cardPin: String, password: String)
    case validatePassword(cardPin: String, password: String)
    case changePassword(oldCardPin: String, newCardPin: String, password: String)
    case includePassword(cardPin: String, password: String)
    case cardBlock(type: CardType, reason: CardBlockReplacementReason, String)
    case setDebitRegistration(cardForm: CardForm)
    case debitData
    case addressSearch(cep: String)
    case cardTracking
    case exchangeRate
    case exchangeRateHistory(lastDays: Int)
    case finish(pin: String)
    case creditSetup
    case getDueDay
    case saveDueDay(day: Int)
    case virtualCardRequest(password: String)
    case virtualCardInfo(password: String)
    case virtualCardDelete(password: String)
    case virtualCardBlock
    case virtualCardUnlock
    case virtualCardAction
    case fetchCreditLimit
    case updateCreditLimit(limit: Double)
    case enableInvoiceNotification(invoiceId: String)
    case hybridFlowRegistration
    case registrationContract
    case getDebitReceipt(id: String)
}

extension CreditServiceEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .hybridFlowRegistration:
            return "credit/registration/hybrid-flow"
        case .creditAccount:
            return "credit/account"
        case .creditAccountAddress:
            return "credit/account/address"
        case .creditRegistrationFinish:
            return "credit/registration/finish"
        case .cardRequest:
            return "credit/card/request"
        case .cardNewRequest:
            return "credit/card/new/request"
        case .validate:
            return "credit/card/validate"
        case .createPassword:
            return "credit/card/activate"
        case .validatePassword:
            return "credit/card/validate-password"
        case .changePassword:
            return "credit/card/change-password"
        case .includePassword:
            return "credit/card/include-password"
        case .cardBlock:
            return "credit/card/block"
        case .debitActivate:
            return "credit/card/debit-activate"
        case .setDebitRegistration, .debitData, .creditSetup:
            return "credit/registration"
        case .addressSearch(let cep):
            return "address/search?zipcode=\(cep)"
        case .cardTracking:
            return "card/tracking"
        case .exchangeRate:
            return "credit/exchange-rate/latest"
        case .exchangeRateHistory(let days):
            return "credit/exchange-rate/history/\(days)"
        case .getDueDay, .saveDueDay:
            return "credit/account/dueday"
        case .virtualCardRequest:
            return "credit/virtual-card"
        case .virtualCardInfo:
            return "credit/virtual-card"
        case .virtualCardDelete:
            return "credit/virtual-card/deactivate"
        case .finish:
            return "credit/registration/finish"
        case .fetchCreditLimit, .updateCreditLimit:
            return "credit/account/limit"
        case .virtualCardBlock:
            return "credit/virtual-card/block"
        case .virtualCardUnlock:
            return "credit/virtual-card/block/disable"
        case .virtualCardAction:
            return "credit/virtual-card/action"
        case .enableInvoiceNotification(let invoiceId):
            return "credit/invoice/\(invoiceId)/enable-bank-slip-notification"
        case .registrationContract:
            return "credit/registration/contract"
        case let .getDebitReceipt(transaction_id):
            return "card/debit/transaction/receipt/\(transaction_id)"
        }
    }

    var method: HTTPMethod {
        switch self {
        case
            .cardRequest,
            .cardNewRequest,
            .validate,
            .createPassword,
            .cardBlock,
            .validatePassword,
            .debitActivate,
            .virtualCardRequest:
            return .post
        case
            .changePassword,
            .includePassword,
            .setDebitRegistration,
            .creditRegistrationFinish,
            .saveDueDay,
            .finish, 
            .updateCreditLimit,
            .virtualCardBlock,
            .virtualCardUnlock,
            .enableInvoiceNotification:
            return .put
        case
            .creditAccount,
            .creditAccountAddress,
            .cardTracking,
            .addressSearch,
            .debitData,
            .exchangeRate,
            .exchangeRateHistory,
            .creditSetup,
            .getDueDay,
            .virtualCardInfo,
            .virtualCardAction,
            .fetchCreditLimit,
            .hybridFlowRegistration,
            .registrationContract,
            .getDebitReceipt:
            return .get
        case .virtualCardDelete:
            return .delete
        }
    }
    
    var isTokenNeeded: Bool {
        false
    }
    
    var body: Data? {
        switch self {
        case .cardRequest(let addressType):
            return ["address_type": addressType.rawValue].toData()
        case .cardNewRequest(let addressType, _):
            return ["address_type": addressType.rawValue].toData()
        case .validate(let lastDigits):
            return ["last_digits": lastDigits].toData()
        case let .cardBlock(type: type, reason: reason, _):
            return ["type": type.rawValue, "reason": reason.rawValue].toData()
        case .setDebitRegistration(let cardForm):
            return prepareBody(with: cardForm, strategy: .convertToSnakeCase)
        case .creditRegistrationFinish(_, let originFlow):
            return prepareBody(with: originFlow, strategy: .convertToSnakeCase)
        case let .saveDueDay(day: day):
            return ["value": day].toData()
        case let .updateCreditLimit(limit: limit):
            return ["value": limit].toData()
        default:
            return nil
        }
    }
    
    var customHeaders: [String: String] {
        let dependencies: Dependencies = DependencyContainer()
        let token = dependencies.keychain.getData(key: KeychainKey.token) ?? String()
        switch self {
        case let .cardBlock(_, _, password):
            return [
                "Token": token,
                "password": password
            ]
        case let .createPassword(cardPin, password):
            return [
                "Token": token,
                "password": password,
                "card_pin": cardPin
            ]
        case let .validatePassword(cardPin, password):
            return [
                "token": token,
                "card_pin": cardPin,
                "password": password
            ]
        case let .changePassword(oldCardPin, newCardPin, password):
            return [
                "token": token,
                "old_card_pin": oldCardPin,
                "new_card_pin": newCardPin,
                "password": password
            ]
        case let .debitActivate(cardPin, password):
            return [
                "token": token,
                "card_pin": cardPin,
                "password": password
            ]
        case let .includePassword(cardPin, password):
            return [
                "token": token,
                "new_card_pin": cardPin,
                "password": password
            ]
        case let .creditRegistrationFinish(password, _):
            return [
              "Token": token,
              "password": password
            ]
        case let .cardNewRequest(_, password):
            return [
              "token": token,
              "password": password
            ]
        case let .virtualCardRequest(password):
            return [
                "token": token,
                "password": password
            ]
        case let .virtualCardInfo(password):
            return [
                "token": token,
                "password": password
            ]
        case let .virtualCardDelete(password):
            return [
                "token": token,
                "password": password
            ]
        case .finish(let pin):
            return [
                "token": token,
                "password": pin
            ]
        default:
            return ["token": token]
        }
    }
}
