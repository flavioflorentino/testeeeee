import UIKit
public enum CreateNewCardPasswordAction {
    case confirm(onSuccessAuth: (String) -> Void)
    case success
    case securityTips
}

public protocol CreateNewCardPasswordCoordinatorDelegate: AnyObject {
    func didConfirmCreateNewPin(onSuccess: @escaping (String) -> Void)
    func didSuccessCreateNewPin()
    func didOpenHelperSecurityTips()
}

public protocol CreateNewCardPasswordCoordinating {
    var viewController: UIViewController? { get set }
    var delegate: CreateNewCardPasswordCoordinatorDelegate? { get set }
    func perform(action: CreateNewCardPasswordAction)
}

public final class CreateNewCardPasswordCoordinator: CreateNewCardPasswordCoordinating {
    public var viewController: UIViewController?
    public weak var delegate: CreateNewCardPasswordCoordinatorDelegate?

    public func perform(action: CreateNewCardPasswordAction) {
        switch action {
        case .confirm(let successClosure):
            delegate?.didConfirmCreateNewPin(onSuccess: successClosure)
        case .success:
            delegate?.didSuccessCreateNewPin()
        case .securityTips:
            delegate?.didOpenHelperSecurityTips()
        }
    }
}
