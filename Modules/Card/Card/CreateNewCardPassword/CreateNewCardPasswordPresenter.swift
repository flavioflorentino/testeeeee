public struct CreateNewCardPasswordPresenter: DigitFormPresenting {
    public let title: String
    public let description: String
    public let confirmTitle: String
    public let isSecurityCode: Bool
    public var helperTitle: String?
    
    init() {
        title = Strings.CreateNewCardPassword.title
        description = Strings.CreateNewCardPassword.description
        confirmTitle = Strings.CreateNewCardPassword.confirmButton
        isSecurityCode = true
    }
}
