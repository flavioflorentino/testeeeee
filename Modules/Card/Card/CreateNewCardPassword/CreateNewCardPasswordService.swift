import Core
import Foundation

public protocol CreateNewCardPasswordServiceProtocol {
    func createNewCardPassword(_ cardPin: String, password: String, then completion: @escaping (Result<Void, ApiError>) -> Void)
}

public final class CreateNewCardPasswordService: CreateNewCardPasswordServiceProtocol {
    public func createNewCardPassword(_ cardPin: String, password: String, then completion: @escaping (Result<Void, ApiError>) -> Void) {
        let api = Api<NoContent>(endpoint: CreditServiceEndpoint.includePassword(cardPin: cardPin, password: password))
        api.execute { result in
            DispatchQueue.main.async {
                completion(result.map { _ in })
            }
        }
    }
}
