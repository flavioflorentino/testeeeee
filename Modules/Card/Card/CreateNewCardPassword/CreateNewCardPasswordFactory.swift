import Foundation

public enum CreateNewCardPasswordFactory {
    public static func make(delegate: CreateNewCardPasswordCoordinatorDelegate) -> CreateNewCardPasswordViewController {
        let service = CreateNewCardPasswordService()
        let viewModel = CreateNewCardPasswordViewModel(service: service)
        let coordinator = CreateNewCardPasswordCoordinator()
        let viewController = CreateNewCardPasswordViewController(viewModel: viewModel, coordinator: coordinator)
        coordinator.viewController = viewController
        coordinator.delegate = delegate
        viewModel.outputs = viewController
        return viewController
    }
}
