import UIKit
import UI

public class CreateNewCardPasswordViewController: LegacyViewController<CreateNewCardPasswordViewModelType, CreateNewCardPasswordCoordinating, UIView> {
    private var digitViewController = DigitFormViewController()

    override public func viewDidLoad() {
        super.viewDidLoad()
        viewModel.inputs.viewDidLoad()
        digitViewController.delegate = self
    }
    private func addChildViewController() {
        addChild(digitViewController)
        view.addSubview(digitViewController.view)
        digitViewController.didMove(toParent: self)
    }
    private func addConstraints() {
        digitViewController.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.constraintAllEdges(from: digitViewController.view, to: view)
    }
    
    override public func configureViews() {
        addChildViewController()
        addConstraints()
        configureNavigationButtons(enable: true)
    }
    private func configureNavigationButtons(enable: Bool) {
        navigationItem.hidesBackButton = !enable
        if enable {
            addHelperNavigationButton()
        } else {
            navigationItem.rightBarButtonItems = nil
        }
    }
    private func addHelperNavigationButton() {
        let helpIcon = Assets.icoExclamationMark.image
        let helpButton = UIBarButtonItem(image: helpIcon, style: .plain, target: self, action: #selector(didTapHelpButton))
        navigationItem.rightBarButtonItems = [helpButton]
    }
    @objc
    private func didTapHelpButton() {
        viewModel.inputs.didTapHelper()
    }
}

extension CreateNewCardPasswordViewController: CreateNewCardPasswordViewModelOutputs {
    public func show(errorDescription: String) {
        digitViewController.show(errorDescription: errorDescription)
    }
    public func present(_ presenter: DigitFormPresenting) {
        title = presenter.title
        digitViewController.presenter = presenter
    }
    public func perform(action: CreateNewCardPasswordAction) {
        coordinator.perform(action: action)
    }
    public func didAuthenticate() {
        view.endEditing(true)
        digitViewController.startLoading()
    }
}

extension CreateNewCardPasswordViewController: DigitFormViewControllerDelegate {
    public func didTapConfirm(with digits: String) {
        viewModel.inputs.didTapConfirmWithAuthentication(and: digits)
    }
    public func didUpdateState(state: DigitFormViewControllerState) {
        configureNavigationButtons(enable: !state.isLoading)
    }
}
