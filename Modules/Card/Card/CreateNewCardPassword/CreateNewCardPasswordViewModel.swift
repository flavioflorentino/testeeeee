import Core
import Foundation

public protocol CreateNewCardPasswordViewModelInputs {
    func viewDidLoad()
    func didTapConfirmWithAuthentication(and newCardPin: String)
    func didTapHelper()
}

public protocol CreateNewCardPasswordViewModelOutputs: AnyObject {
    func present(_ presenter: DigitFormPresenting)
    func show(errorDescription: String)
    func perform(action: CreateNewCardPasswordAction)
    func didAuthenticate()
}

public protocol CreateNewCardPasswordViewModelType: AnyObject {
    var inputs: CreateNewCardPasswordViewModelInputs { get }
    var outputs: CreateNewCardPasswordViewModelOutputs? { get set }
}

public final class CreateNewCardPasswordViewModel: CreateNewCardPasswordViewModelType, CreateNewCardPasswordViewModelInputs {
    public var inputs: CreateNewCardPasswordViewModelInputs { self }
    public weak var outputs: CreateNewCardPasswordViewModelOutputs?
    private let service: CreateNewCardPasswordServiceProtocol
    
    init(service: CreateNewCardPasswordServiceProtocol) {
        self.service = service
    }
    
    // MARK: - DigitFormViewModelInputs
    
    public func viewDidLoad() {
        let presenter = CreateNewCardPasswordPresenter()
        outputs?.present(presenter)
    }
    
    public func didTapConfirmWithAuthentication(and cardPin: String) {
        outputs?.perform(action: .confirm(
            onSuccessAuth: { [weak self] appPassword in
                self?.outputs?.didAuthenticate()
                self?.service.createNewCardPassword(cardPin, password: appPassword) { [weak self] result in
                    self?.resultHandler(result: result)
                }
            })
        )
    }
    
    public func didTapHelper() {
        outputs?.perform(action: .securityTips)
    }
    
    private func resultHandler(result: Result<Void, ApiError>) {
        switch result {
        case .success:
            outputs?.perform(action: .success)
        case .failure(.badRequest(let data)),
             .failure(.notFound(let data)),
             .failure(.unauthorized(let data)):
            outputs?.show(errorDescription: data.message)
        case .failure:
            outputs?.show(errorDescription: Strings.CreateCardPassword.defaultFailureMessage)
        }
    }
}
