import UIKit

enum CardAddressAction: Equatable {
    case didConfirm(cardForm: CardForm)
    case openNavigation(URL)
}

protocol CardAddressCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: CardAddressAction)
}

final class CardAddressCoordinator: CardAddressCoordinating, SafariWebViewPresentable {
    weak var viewController: UIViewController?
    private var cardAddressCoordinatorOutput: (CardAddressCoordinatorOutput) -> Void

    init(cardAddressCoordinatorOutput: @escaping (CardAddressCoordinatorOutput) -> Void) {
        self.cardAddressCoordinatorOutput = cardAddressCoordinatorOutput
    }
    
    func perform(action: CardAddressAction) {}
}
