import Core

final class CardTrackingAddressService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - CardAddressServicing
extension CardTrackingAddressService: CardAddressServicing {}
