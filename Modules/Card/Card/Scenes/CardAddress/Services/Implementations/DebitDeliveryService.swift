import Core

final class DebitDeliveryService {
    typealias Dependencies = HasDebitAPIManagerDependency & HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - CardAddressServicing
extension DebitDeliveryService: CardAddressServicing {
    func addressBy(_ cep: String, then completion: @escaping CompletionAddressSearch) {
        let api = Api<HomeAddress>(endpoint: CreditServiceEndpoint.addressSearch(cep: cep))
        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map { $0.model })
            }
        }
    }
    
    func update(cardForm: CardForm, then completion: CompletionAddressUpdate) {
        dependencies.debitAPIManager.setDebitRegistration(cardForm: cardForm) { result in
            completion?(result)
        }
    }
}
