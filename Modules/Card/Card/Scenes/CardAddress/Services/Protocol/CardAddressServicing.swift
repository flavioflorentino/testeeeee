import Core
import Foundation

protocol CardAddressServicing {
    func addressBy(_ cep: String, then completion: @escaping CompletionAddressSearch)
    func update(cardForm: CardForm, then completion: CompletionAddressUpdate)
    func updateCardTrackingAddress()
}

extension CardAddressServicing {
    func addressBy(_ cep: String, then completion: @escaping CompletionAddressSearch) {}
    func update(cardForm: CardForm, then completion: CompletionAddressUpdate) {}
    func updateCardTrackingAddress() {}
}
