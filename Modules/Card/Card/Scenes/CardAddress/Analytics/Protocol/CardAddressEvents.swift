public protocol CardAddressEvents {
    func logSomeEvent()
    func logOtherEvent()
}

extension CardAddressEvents {
    func logSomeEvent() {}
    func logOtherEvent() {}
}
