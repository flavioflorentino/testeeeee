import AnalyticsModule

struct CardTrackingAnalytics: CardAddressEvents {
    typealias Dependencies = HasAnalytics
    private var dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func logOtherEvent() {
        // log event specific to card tracking
    }
}
