import AnalyticsModule

struct DebitDeliveryAnalytics: CardAddressEvents {
    typealias Dependencies = HasAnalytics
    private var dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func logSomeEvent() {
        // Log event specific to Debit Delivery
    }
    
}
