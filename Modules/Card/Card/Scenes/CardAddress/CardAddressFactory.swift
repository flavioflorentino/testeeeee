import UIKit

enum CardAddressCoordinatorOutput {
    // Cases for debit
    case getServiceResponse(cardForm: CardForm)
    case update(cardForm: CardForm, currentStep: DebitStep)
    case confirmAddress(registrationStatus: RegistrationStatus)
    
    // Cases for card tracking
    case updatedAddress(address: HomeAddress)
    
    // General cases
    case close
}

enum CardAddressInput {
    case debitDelivery(cardForm: CardForm)
    case cardTracking
}

enum CardAddressFactory {
    static func make(with input: CardAddressInput, cardAddressCoordinatorOutput: @escaping (CardAddressCoordinatorOutput) -> Void) -> CardAddressViewController {
        switch input {
        case .debitDelivery(cardForm: _):
            fatalError("Not yet implemented")
        case .cardTracking:
            let container = DependencyContainer()
            let service: CardAddressServicing = CardTrackingAddressService(dependencies: container)
            let coordinator: CardAddressCoordinating = CardAddressCoordinator(cardAddressCoordinatorOutput: cardAddressCoordinatorOutput)
            let presenter: CardAddressPresenting = CardAddressPresenter(coordinator: coordinator, dependencies: container)
            let interactor = CardAddressInteractor(service: service, presenter: presenter, dependencies: container)
            let viewController = CardAddressViewController(interactor: interactor)
            
            coordinator.viewController = viewController
            presenter.viewController = viewController
            
            return viewController
        }
    }
}
