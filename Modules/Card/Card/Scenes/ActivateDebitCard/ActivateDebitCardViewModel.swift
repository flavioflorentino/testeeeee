import AnalyticsModule
import Core

public protocol ActivateDebitCardViewModelInputs {
    func viewDidLoad()
    func didTapConfirmWithAuthentication(and digits: String)
    func didClose()
}

public protocol ActivateDebitCardViewModelOutputs: AnyObject {
    func present(_ presenter: DigitFormPresenting)
    func show(errorDescription: String)
    func perform(action: ActivateDebitCardAction)
    func didAuthenticated()
}

public protocol ActivateDebitCardViewModelType: AnyObject {
    var inputs: ActivateDebitCardViewModelInputs { get }
    var outputs: ActivateDebitCardViewModelOutputs? { get set }
}

public final class ActivateDebitCardViewModel: ActivateDebitCardViewModelType, ActivateDebitCardViewModelInputs {
    public var inputs: ActivateDebitCardViewModelInputs { self }
    public weak var outputs: ActivateDebitCardViewModelOutputs?
    
    private let service: ActivateDebitCardServiceProtocol
    
    init(service: ActivateDebitCardServiceProtocol) {
        self.service = service
    }
    
    // MARK: - DigitFormViewModelInputs
    
    public func viewDidLoad() {
        let presenter = ActivateDebitCardPresenter()
        outputs?.present(presenter)
    }
    
    public func didTapConfirmWithAuthentication(and digits: String) {
        outputs?.perform(action: .confirm(onSuccessAuth: { [weak self] appPassword, _ in
            self?.outputs?.didAuthenticated()
            self?.service.activateDebitCard(digits, password: appPassword) { [weak self] result in
                self?.resultHandler(result: result)
            }}, cancelHandler: {})
        )
    }
    
    private func resultHandler(result: Result<Void, ApiError>) {
        switch result {
        case .success:
            outputs?.perform(action: .success)
        case .failure(.badRequest(let data)),
             .failure(.notFound(let data)),
             .failure(.unauthorized(let data)):
            outputs?.show(errorDescription: data.message)
        case .failure:
            outputs?.show(errorDescription: Strings.ActivateDebitCard.debitActivationFail)
        }
    }
    
    public func didClose() {
        outputs?.perform(action: .close)
    }
}
