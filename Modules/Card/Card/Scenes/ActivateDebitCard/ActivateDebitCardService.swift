import Core
import Foundation

public protocol ActivateDebitCardServiceProtocol {
    func activateDebitCard(_ cardPin: String, password: String, then completion: @escaping (Result<Void, ApiError>) -> Void)
}

public struct ActivateDebitCardService: ActivateDebitCardServiceProtocol {
    public func activateDebitCard(_ cardPin: String, password: String, then completion: @escaping (Result<Void, ApiError>) -> Void) {
        let api = Api<NoContent>(endpoint: CreditServiceEndpoint.debitActivate(cardPin: cardPin, password: password))
        api.execute { result in
            DispatchQueue.main.async {
                completion(result.map { _ in })
            }
        }
    }
}
