import UI
import UIKit

public class ActivateDebitCardViewController: LegacyViewController<ActivateDebitCardViewModelType, ActivateDebitCardCoordinatorProcotol, UIView> {
    private var digitViewController = DigitFormViewController()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        viewModel.inputs.viewDidLoad()
        digitViewController.delegate = self
        addChildViewController()
        addConstraints()
    }
    private func addChildViewController() {
        addChild(digitViewController)
        view.addSubview(digitViewController.view)
        digitViewController.didMove(toParent: self)
    }
    private func configureNavigationButtons(enable: Bool) {
        navigationItem.hidesBackButton = !enable
        if enable {
            addCloseButton()
        } else {
            navigationItem.rightBarButtonItems = nil
        }
    }
    private func addCloseButton() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: Assets.icoClose.image,
            style: .plain,
            target: digitViewController,
            action: #selector(digitViewController.didCloseTap)
        )
    }
    private func addConstraints() {
        digitViewController.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.constraintAllEdges(from: digitViewController.view, to: view)
    }
}

extension ActivateDebitCardViewController: ActivateDebitCardViewModelOutputs {
    public func show(errorDescription: String) {
        digitViewController.show(errorDescription: errorDescription)
    }
    public func present(_ presenter: DigitFormPresenting) {
        self.title = presenter.title
        digitViewController.presenter = presenter
    }
    public func perform(action: ActivateDebitCardAction) {
        coordinator.perform(action: action)
    }
    public func didAuthenticated() {
        view.endEditing(true)
        digitViewController.startLoadingView()
    }
}

extension ActivateDebitCardViewController: DigitFormViewControllerDelegate {
    public func didTapConfirm(with digits: String) {
        viewModel.inputs.didTapConfirmWithAuthentication(and: digits)
    }
    public func didTapClose() {
        viewModel.inputs.didClose()
    }
    public func didUpdateState(state: DigitFormViewControllerState) {
        configureNavigationButtons(enable: !state.isLoading)
    }
}
