import UIKit
public enum ActivateDebitCardAction {
    case confirm(onSuccessAuth: (String, Bool) -> Void, cancelHandler: () -> Void)
    case success
    case close
}

public protocol ActivateDebitCardCoordinatorDelegate: AnyObject {
    func didConfirmActivateDebit(onSuccess: @escaping (String, Bool) -> Void, cancelHandler: @escaping () -> Void)
    func didSuccessActivateDebit()
    func didCloseActivateDebit()
}

public protocol ActivateDebitCardCoordinatorProcotol {
    var viewController: UIViewController? { get set }
    var delegate: ActivateDebitCardCoordinatorDelegate? { get set }
    func perform(action: ActivateDebitCardAction)
}

public protocol ActivateDebitCardDelegate: AnyObject {
    func didActivateConfirm(onSuccess: @escaping (String) -> Void, onCanceledByUserBlock: @escaping () -> Void)
    func didActivateSuccess()
}

public final class ActivateDebitCardCoordinator: ActivateDebitCardCoordinatorProcotol {
    public var viewController: UIViewController?
    public weak var delegate: ActivateDebitCardCoordinatorDelegate?
    
    public func perform(action: ActivateDebitCardAction) {
        switch action {
        case let .confirm(successClosure, cancelHandler):
            delegate?.didConfirmActivateDebit(onSuccess: successClosure, cancelHandler: cancelHandler)
        case .success:
            delegate?.didSuccessActivateDebit()
        case .close:
            delegate?.didCloseActivateDebit()
        }
    }
}
