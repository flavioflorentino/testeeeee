import UIKit
public struct ActivateDebitCardPresenter: DigitFormPresenting {
    public let title: String
    public let description: String
    public let helperTitle: String?
    public let helperImage: UIImage?
    public let confirmTitle: String
    public let isSecurityCode: Bool
    public var showErrorInPopup: Bool
    
    init() {
        title = Strings.ActivateDebitCard.title
        description = Strings.ActivateDebitCard.description
        helperTitle = nil
        helperImage = nil
        confirmTitle = Strings.ActivateDebitCard.confirmButtonTitle
        isSecurityCode = true
        showErrorInPopup = false
    }
}
