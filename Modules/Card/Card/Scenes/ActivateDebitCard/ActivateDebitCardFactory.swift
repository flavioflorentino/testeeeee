import Foundation

public enum ActivateDebitCardFactory {
    public static func make(_ delegate: ActivateDebitCardCoordinatorDelegate) -> ActivateDebitCardViewController {
        let service = ActivateDebitCardService()
        let viewModel = ActivateDebitCardViewModel(service: service)
        let coordinator = ActivateDebitCardCoordinator()
        
        let viewController = ActivateDebitCardViewController(viewModel: viewModel, coordinator: coordinator)
        coordinator.viewController = viewController
        coordinator.delegate = delegate
        viewModel.outputs = viewController
        return viewController
    }
}
