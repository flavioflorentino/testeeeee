import Foundation
import AnalyticsModule

enum DocumentsPendingAnalyticsEvents: AnalyticsKeyProtocol {
    case confirm
    case cancel
    
    var name: String {
        "PicPay Card - Request - Sign Up - Info Pending"
    }
    
    var properties: [String: Any] {
        switch self {
        case .confirm:
            return ["action": "act-fix-info-pending"]
            
        case .cancel:
            return ["action": "act-do-after"]
        }
    }
    
    var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
