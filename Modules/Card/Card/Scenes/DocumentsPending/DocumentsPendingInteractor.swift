import Foundation
import AnalyticsModule

protocol DocumentsPendingInteracting: AnyObject {
    func loadData()
    func confirm()
    func cancel()
}

final class DocumentsPendingInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let registrationErrors: [String]
    private let service: DocumentsPendingServicing
    private let presenter: DocumentsPendingPresenting
    private var cardForm: CardForm?

    init(
        service: DocumentsPendingServicing,
        presenter: DocumentsPendingPresenting,
        dependencies: Dependencies,
        errors: [String]
    ) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.registrationErrors = errors
    }
}

// MARK: - DocumentsPendingInteracting
extension DocumentsPendingInteractor: DocumentsPendingInteracting {
    func confirm() {
        guard let form = cardForm else { return }
        dependencies.analytics.log(DocumentsPendingAnalyticsEvents.confirm)
        presenter.presentNextStep(action: .confirm(form: form))
    }
    
    func cancel() {
        dependencies.analytics.log(DocumentsPendingAnalyticsEvents.cancel)
        presenter.presentNextStep(action: .cancel)
    }
    
    func loadData() {
        presenter.presentLoading(true)
        service.loadCardForm { [weak self] in
            guard let strongSelf = self else { return }
            
            switch $0 {
            case let .success(cardForm):
                strongSelf.cardForm = cardForm
                strongSelf.presenter.presentData(errors: strongSelf.registrationErrors)
                strongSelf.presenter.presentLoading(false)
                
            case let .failure(error):
                strongSelf.presenter.presentError(error)
            }
        }
    }
}
