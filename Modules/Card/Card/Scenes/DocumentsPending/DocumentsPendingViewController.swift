import UI
import UIKit

protocol DocumentsPendingDisplaying: AnyObject {
    func displayData(with rows: [DetailsViewRow])
    func displayError(model: StatefulErrorViewModel)
    func displayLoading(_ shouldDisplay: Bool)
}

final class DocumentsPendingViewController: ViewController<DocumentsPendingInteracting, DocumentsPendingRootView> {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.loadData()
    }
    
    override func configureViews() {
        rootView.delegate = self
    }
}

// MARK: - DocumentsPendingDisplaying
extension DocumentsPendingViewController: DocumentsPendingDisplaying {
    func displayData(with rows: [DetailsViewRow]) {
        rootView.setupView(with: rows)
    }
    
    func displayError(model: StatefulErrorViewModel) {
        endState(animated: true, model: model)
    }
    
    func displayLoading(_ shouldDisplay: Bool) {
        shouldDisplay ? beginState() : endState()
    }
}

extension DocumentsPendingViewController: StatefulTransitionViewing {
    func didTryAgain() {
        interactor.loadData()
    }
}

extension DocumentsPendingViewController: DocumentsPendingRootViewDelegate {
    func didConfirm() {
        interactor.confirm()
    }
    
    func didCancel() {
        interactor.cancel()
    }
}
