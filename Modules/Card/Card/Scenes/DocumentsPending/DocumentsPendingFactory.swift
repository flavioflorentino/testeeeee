import UIKit

public enum DocumentsPendingFactory {
    public static func make(errors: [String], coordinatorDelegate: DocumentsPendingCoodinatingDelegate?) -> UIViewController {
        let container = DependencyContainer()
        let service: DocumentsPendingServicing = DocumentsPendingService(dependencies: container)
        let coordinator: DocumentsPendingCoordinating = DocumentsPendingCoordinator()
        let presenter: DocumentsPendingPresenting = DocumentsPendingPresenter(coordinator: coordinator)
        let interactor = DocumentsPendingInteractor(service: service, presenter: presenter, dependencies: container, errors: errors)
        let viewController = DocumentsPendingViewController(interactor: interactor)

        coordinator.viewController = viewController
        coordinator.delegate = coordinatorDelegate
        presenter.viewController = viewController

        return viewController
    }
}
