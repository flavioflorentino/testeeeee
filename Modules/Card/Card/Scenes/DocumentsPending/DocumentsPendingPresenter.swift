import Foundation
import UI
import Core

protocol DocumentsPendingPresenting: AnyObject {
    var viewController: DocumentsPendingDisplaying? { get set }
    func presentNextStep(action: DocumentsPendingAction)
    func presentData(errors: [String])
    func presentError(_ error: ApiError)
    func presentLoading(_ shouldDisplay: Bool)
}

final class DocumentsPendingPresenter {
    private let coordinator: DocumentsPendingCoordinating
    weak var viewController: DocumentsPendingDisplaying?

    init(coordinator: DocumentsPendingCoordinating) {
        self.coordinator = coordinator
    }
    
    internal func mapSequence(_ value: EnumeratedSequence<[String]>.Element, count: Int) -> [DetailsViewRow] {
        let isLastIndex = value.offset == count
        return isLastIndex
            ? [.bulletTextRow(text: value.element)]
            : [.bulletTextRow(text: value.element), .spacer(spacing: Spacing.base00)]
    }
}

// MARK: - DocumentsPendingPresenting
extension DocumentsPendingPresenter: DocumentsPendingPresenting {
    func presentData(errors: [String]) {
        let mappedRows = errors.enumerated()
            .map { mapSequence($0, count: errors.count - 1) }
            .flatMap { $0 }

        viewController?.displayData(with: mappedRows)
    }
    
    func presentNextStep(action: DocumentsPendingAction) {
        coordinator.perform(action: action)
    }
    
    func presentError(_ error: ApiError) {
        viewController?.displayError(model: .init(error))
    }
    
    func presentLoading(_ shouldDisplay: Bool) {
        viewController?.displayLoading(shouldDisplay)
    }
}
