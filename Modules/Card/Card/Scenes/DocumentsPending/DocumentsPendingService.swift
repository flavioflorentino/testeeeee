import Foundation
import Core

protocol DocumentsPendingServicing {
    func loadCardForm(_ completion: @escaping (Result<CardForm, ApiError>) -> Void)
}

final class DocumentsPendingService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - DocumentsPendingServicing
extension DocumentsPendingService: DocumentsPendingServicing {
    func loadCardForm(_ completion: @escaping (Result<CardForm, ApiError>) -> Void) {
        let api = Api<CardForm>(endpoint: CreditServiceEndpoint.creditSetup)
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        api.execute(jsonDecoder: decoder) {[weak self] result in
            let model = result.map(\.model)
            self?.dependencies.mainQueue.async { completion(model) }
        }
    }
}
