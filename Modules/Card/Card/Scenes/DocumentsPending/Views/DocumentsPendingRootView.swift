import UIKit
import UI

protocol DocumentsPendingRootViewDelegate: AnyObject {
    func didConfirm()
    func didCancel()
}

extension DocumentsPendingRootView.Layout {
    enum Size {
        static let linkButtonHeight: CGFloat = 14
    }
}

class DocumentsPendingRootView: UIView, ViewConfiguration {
    fileprivate enum Layout {}
    weak var delegate: DocumentsPendingRootViewDelegate?
    
    private let anchorView = UIView()
    
    private let infoImageView = InfoImageView()
    private let infoImageContainerView = UIView()
    
    private let detailsView = DetailsView()
    
    private lazy var warningLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.text, Strings.DocumentsPending.warningLabel)
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.DocumentsPending.confirmButton, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(confirm), for: .touchUpInside)
        return button
    }()
    
    private lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.DocumentsPending.cancelButton, for: .normal)
        button.buttonStyle(LinkButtonStyle())
            .with(\.textAttributedColor, (color: .grayscale400(), state: .normal))
        button.addTarget(self, action: #selector(cancel), for: .touchUpInside)
        return button
    }()
    
    init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        anchorView.addSubview(infoImageContainerView)
        infoImageContainerView.addSubviews(infoImageView, detailsView)
        addSubviews(anchorView, warningLabel, confirmButton, cancelButton)
    }
    
    func setupConstraints() {
        infoImageView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.trailing.top.equalToSuperview()
            $0.bottom.equalTo(detailsView.snp.top).offset(-Spacing.base02)
        }
        
        detailsView.snp.makeConstraints {
            $0.leading.trailing.bottom.equalToSuperview()
        }
        
        infoImageContainerView.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.top.greaterThanOrEqualToSuperview().offset(Spacing.base02)
            $0.bottom.lessThanOrEqualToSuperview().offset(-Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        anchorView.snp.makeConstraints {
            $0.top.equalTo(compatibleSafeArea.top)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(warningLabel.snp.top)
        }
        
        warningLabel.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(confirmButton.snp.top).offset(-Spacing.base02)
        }
        
        confirmButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(cancelButton.snp.top).offset(-Spacing.base02)
        }
        
        cancelButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(compatibleSafeArea.bottom).offset(-Spacing.base02)
            $0.height.equalTo(Layout.Size.linkButtonHeight)
        }
    }
    
    func configureViews() {
        anchorView.isHidden = true
        confirmButton.isHidden = true
        warningLabel.isHidden = true
        cancelButton.isHidden = true
        
        backgroundColor = Colors.backgroundPrimary.color
        infoImageView.setupView(
            image: Assets.icoExclamationYellow.image,
            title: Strings.DocumentsPending.InfoImageView.title,
            description: Strings.DocumentsPending.InfoImageView.description
        )
    }
    
    func setupView(with rows: [DetailsViewRow]) {
        anchorView.isHidden.toggle()
        confirmButton.isHidden.toggle()
        warningLabel.isHidden.toggle()
        cancelButton.isHidden.toggle()
        
        detailsView.render(rows)
    }
    
    @objc
    private func confirm() {
        delegate?.didConfirm()
    }
    
    @objc
    private func cancel() {
        delegate?.didCancel()
    }
}
