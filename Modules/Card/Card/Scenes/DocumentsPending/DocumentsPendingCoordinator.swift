import UIKit

enum DocumentsPendingAction: Equatable {
    case confirm(form: CardForm)
    case cancel
    
    static func == (lhs: DocumentsPendingAction, rhs: DocumentsPendingAction) -> Bool {
        switch (lhs, rhs) {
        case (.confirm, .confirm):
            return true
            
        case (.cancel, .cancel):
            return true
            
        default:
            return false
        }
    }
}

public protocol DocumentsPendingCoodinatingDelegate: AnyObject {
    func didConfirm(form: CardForm)
}

protocol DocumentsPendingCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: DocumentsPendingCoodinatingDelegate? { get set }
    func perform(action: DocumentsPendingAction)
}

final class DocumentsPendingCoordinator {
    weak var viewController: UIViewController?
    weak var delegate: DocumentsPendingCoodinatingDelegate?
}

// MARK: - DocumentsPendingCoordinating
extension DocumentsPendingCoordinator: DocumentsPendingCoordinating {
    func perform(action: DocumentsPendingAction) {
        switch action {
        case let .confirm(form):
            delegate?.didConfirm(form: form)
            
        case .cancel:
            viewController?.dismiss(animated: true)
        }
    }
}
