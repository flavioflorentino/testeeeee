public struct ValidateCardPasswordPresenter: DigitFormPresenting {
    public let title: String
    public let description: String
    public let confirmTitle: String
    public let isSecurityCode: Bool
    public var helperTitle: String?
    
    init() {
        title = Strings.ValidateCardPassword.title
        description = Strings.ValidateCardPassword.description
        confirmTitle = Strings.Default.continueButtonTitle
        isSecurityCode = true
        helperTitle = Strings.ValidateCardPassword.helper
    }
}
