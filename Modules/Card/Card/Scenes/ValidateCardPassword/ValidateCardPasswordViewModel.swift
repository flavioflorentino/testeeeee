import Core
import Foundation

public protocol ValidateCardPasswordViewModelInputs {
    func viewDidLoad()
    func didTapConfirmWithAuthentication(with cardPin: String)
    func close()
    func didTapForgotPassword()
}

public protocol ValidateCardPasswordViewModelOutputs: AnyObject {
    func present(_ presenter: DigitFormPresenting)
    func show(errorDescription: String, alert: Alert?)
    func perform(action: ValidateCardPasswordAction)
    func didAuthenticate()
}

public protocol ValidateCardPasswordViewModelType: AnyObject {
    var inputs: ValidateCardPasswordViewModelInputs { get }
    var outputs: ValidateCardPasswordViewModelOutputs? { get set }
}

public final class ValidateCardPasswordViewModel: ValidateCardPasswordViewModelType, ValidateCardPasswordViewModelInputs {
    public var inputs: ValidateCardPasswordViewModelInputs { self }
    public weak var outputs: ValidateCardPasswordViewModelOutputs?
    typealias Dependencies = HasAuthManagerDependency
    private let dependencies: Dependencies
    
    private let service: ValidateCardPasswordServicing
    
    init(service: ValidateCardPasswordServicing, dependencies: Dependencies) {
            self.service = service
            self.dependencies = dependencies
        }
    
    // MARK: - DigitFormViewModelInputs
    
    public func viewDidLoad() {
        let presenter = ValidateCardPasswordPresenter()
        outputs?.present(presenter)
    }
    
    public func didTapConfirmWithAuthentication(with cardPin: String) {
        dependencies.authManager.authenticate(sucessHandler: { [weak self] password, _ in
            self?.outputs?.didAuthenticate()
            self?.service.ValidateCardPassword(cardPin, password: password) { [weak self] result in
                self?.resultHandler(result: result, cardPin: cardPin)
            }
        }, cancelHandler: nil)
    }
    
    public func close() {
        outputs?.perform(action: .close)
    }
    
    public func didTapForgotPassword() {
        outputs?.perform(action: .forgotPassword)
    }
    
    private func resultHandler(result: Result<Void, ApiError>, cardPin: String) {
        switch result {
        case .success:
            outputs?.perform(action: .success(oldCardPin: cardPin))
        case .failure(.badRequest(let picPayError)),
             .failure(.notFound(let picPayError)),
             .failure(.unauthorized(let picPayError)):
            outputs?.show(errorDescription: picPayError.message, alert: picPayError.alert)
        case .failure:
            outputs?.show(errorDescription: Strings.CreateCardPassword.defaultFailureMessage, alert: nil)
        }
    }
}
