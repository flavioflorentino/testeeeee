import UIKit
import Core
import UI

public class ValidateCardPasswordViewController: LegacyViewController<ValidateCardPasswordViewModelType, ValidateCardPasswordCoordinating, UIView> {
    private var digitViewController = DigitFormViewController()
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        statusBarStyle
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        viewModel.inputs.viewDidLoad()
        digitViewController.delegate = self
    }
    
    override public func configureViews() {
        addChildViewController()
        addConstraints()
        configureNavigationBarButtons(enable: true)
    }
    
    private func configureNavigationBarButtons(enable: Bool) {
        navigationItem.hidesBackButton = !enable
    }
    
    private func addChildViewController() {
        addChild(digitViewController)
        view.addSubview(digitViewController.view)
        digitViewController.didMove(toParent: self)
    }
    
    private func addConstraints() {
        digitViewController.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.constraintAllEdges(from: digitViewController.view, to: view)
    }
}

extension ValidateCardPasswordViewController: ValidateCardPasswordViewModelOutputs {
    public func show(errorDescription: String, alert: Alert?) {
        digitViewController.show(errorDescription: errorDescription, alert: alert)
    }
    
    public func present(_ presenter: DigitFormPresenting) {
        title = presenter.title
        digitViewController.presenter = presenter
    }
    
    public func perform(action: ValidateCardPasswordAction) {
        coordinator.perform(action: action)
    }
    
    public func didAuthenticate() {
        view.endEditing(true)
        digitViewController.startLoading()
    }
}

extension ValidateCardPasswordViewController: DigitFormViewControllerDelegate {
    public func didTapConfirm(with digits: String) {
        viewModel.inputs.didTapConfirmWithAuthentication(with: digits)
    }
    
    public func didTapClose() {
        viewModel.inputs.close()
    }
    
    public func didTapHandler() {
        view.endEditing(true)
        viewModel.inputs.didTapForgotPassword()
    }
    
    public func didUpdateState(state: DigitFormViewControllerState) {
        configureNavigationBarButtons(enable: !state.isLoading)
    }
}

extension ValidateCardPasswordViewController: StyledNavigationDisplayable {}
