import Core
import Foundation

public protocol ValidateCardPasswordServicing {
    func ValidateCardPassword(_ cardPin: String, password: String, then completion: @escaping (Result<Void, ApiError>) -> Void)
}

public final class ValidateCardPasswordService: ValidateCardPasswordServicing {
    public func ValidateCardPassword(_ cardPin: String, password: String, then completion: @escaping (Result<Void, ApiError>) -> Void) {
        let api = Api<NoContent>(endpoint: CreditServiceEndpoint.validatePassword(cardPin: cardPin, password: password))
        api.execute { result in
            DispatchQueue.main.async {
                completion(result.map { _ in })
            }
        }
    }
}
