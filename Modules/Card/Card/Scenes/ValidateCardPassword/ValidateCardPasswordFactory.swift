import Foundation

public enum ValidateCardPasswordFactory {
    public static func make(_ delegate: ValidateCardPasswordCoordinatorDelegate) -> ValidateCardPasswordViewController {
        let container = DependencyContainer()
        let service = ValidateCardPasswordService()
        let viewModel = ValidateCardPasswordViewModel(service: service, dependencies: container)
        let coordinator = ValidateCardPasswordCoordinator()
        let viewController = ValidateCardPasswordViewController(viewModel: viewModel, coordinator: coordinator)
        coordinator.viewController = viewController
        coordinator.delegate = delegate
        viewModel.outputs = viewController
        return viewController
    }
}
