import UIKit
public enum ValidateCardPasswordAction {
    case confirm(onSuccessAuth: (String) -> Void)
    case success(oldCardPin: String)
    case close
    case forgotPassword
}

public protocol ValidateCardPasswordCoordinatorDelegate: AnyObject {
    func didConfirmValidateCardPin(onSuccess: @escaping (String) -> Void)
    func didSuccesValidateCardPin(oldCardPin: String)
    func didForgotPassword()
    func didCloseValidateCard()
}

public protocol ValidateCardPasswordCoordinating {
    var viewController: UIViewController? { get set }
    var delegate: ValidateCardPasswordCoordinatorDelegate? { get set }
    func perform(action: ValidateCardPasswordAction)
}

public final class ValidateCardPasswordCoordinator: ValidateCardPasswordCoordinating {
    public var viewController: UIViewController?
    public weak var delegate: ValidateCardPasswordCoordinatorDelegate?
    
    public func perform(action: ValidateCardPasswordAction) {
        switch action {
        case .confirm(let successClosure):
            delegate?.didConfirmValidateCardPin(onSuccess: successClosure)
        case .success(let oldCardPin):
            delegate?.didSuccesValidateCardPin(oldCardPin: oldCardPin)
        case .close:
            delegate?.didCloseValidateCard()
        case .forgotPassword:
            delegate?.didForgotPassword()
        }
    }
}
