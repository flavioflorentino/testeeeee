import Core
import Foundation

protocol RejectedCreditServicing {
    func getCardForm(_ completion: @escaping (Result<CardForm, ApiError>) -> Void)
    func finish(pin: String, _ completion: @escaping (Result<NoContent, ApiError>) -> Void)
}

final class RejectedCreditService {
    typealias Dependencies = HasDebitAPIManagerDependency & HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - RejectedCreditServicing
extension RejectedCreditService: RejectedCreditServicing {
    func getCardForm(_ completion: @escaping (Result<CardForm, ApiError>) -> Void) {
        dependencies.debitAPIManager.getDebitData { result in
            completion(result)
        }
    }
    
    func finish(pin: String, _ completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        let endpoint = CreditServiceEndpoint.creditRegistrationFinish(
            password: pin,
            originFlow: OriginFlow(origin: .debit)
        )

        Api<NoContent>(endpoint: endpoint).execute { result in
            self.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model)
                completion(mappedResult)
            }
        }
    }
}
