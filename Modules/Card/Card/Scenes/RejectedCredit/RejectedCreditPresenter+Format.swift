import Foundation
import Core
import UI

extension RejectedCreditPresenter {
    internal func buildAddressString(homeAddress: HomeAddress) -> String {
        var address = ""
        address.append(getFormattedStreet(streetType: homeAddress.streetType, street: homeAddress.street))
        address.append(getFormattedStreetNumber(homeAddress.streetNumber))
        address.append(getFormattedNeighborhood(homeAddress.neighborhood))
        address.append(getFormattedAddressComplement(homeAddress.addressComplement))
        address.append(getFormattedState(homeAddress.state))
        address.append(getFormattedCity(homeAddress.city))
        address.append(getFormattedZipCode(homeAddress.zipCode))
        
        return address
    }
    
    internal func getFormattedStreet(streetType: String?, street: String?) -> String {
        guard let street = getWithoutSeparators(street) else {
            return ""
        }

        if let streetType = getWithoutSeparators(streetType) {
            return "\(streetType) \(street)"
        }
        return street
    }
    
    internal func getFormattedStreetNumber(_ streetNumber: String?) -> String {
        guard
            let streetNumber = getWithoutSeparators(streetNumber, requireMinimumTwoCharacters: false),
            streetNumber != "0"
            else {
                return ""
        }
        return " \(streetNumber)"
    }
    
    internal func getFormattedNeighborhood(_ neighborhood: String?) -> String {
        guard let neighborhood = getWithoutSeparators(neighborhood) else {
            return ""
        }
        return ", \(neighborhood)"
    }
    
    internal func getFormattedAddressComplement(_ addressComplement: String?) -> String {
        guard let addressComplement = getWithoutSeparators(addressComplement) else {
            return ""
        }
        
        return " - \(addressComplement)"
    }
    
    internal func getFormattedState(_ state: String?) -> String {
        guard let state = getWithoutSeparators(state) else {
            return ""
        }
        return " - \(state)"
    }
    
    internal func getFormattedCity(_ city: String?) -> String {
        guard let city = getWithoutSeparators(city) else {
            return ""
        }
        return " - \(city)"
    }
    
    internal func getFormattedZipCode(_ zipCode: String?) -> String {
        let mask = CustomStringMask(mask: "00000-000")
        guard
            let zipCode = getWithoutSeparators(zipCode),
            let zipCodeMasked = mask.maskedText(from: zipCode)
            else {
            return ""
        }
        return ", \(zipCodeMasked)"
    }
    
    internal func getWithoutSeparators(_ value: String?, requireMinimumTwoCharacters: Bool = true) -> String? {
        let minimumTwoCharactersPattern = "[A-Za-z0-9].*[A-Za-z0-9]"
        let minimumOneCharacterPattern = "[A-Za-z0-9]+"
        let pattern = requireMinimumTwoCharacters ? minimumTwoCharactersPattern : minimumOneCharacterPattern
        return getMatchValue(value, pattern: pattern)
    }
    
    internal func getMatchValue(_ value: String?, pattern: String) -> String? {
        guard
            let value = value,
            let regex = try? NSRegularExpression(pattern: pattern),
            let match = regex.firstMatch(in: value, range: NSRange(value.startIndex..., in: value)),
            let range = Range(match.range, in: value)
            else {
                return nil
        }
        return String(value[range])
    }
}
