import Foundation

import AnalyticsModule

public enum RejectedCreditEvent: AnalyticsKeyProtocol {
    case cardAsked
    case changeAddress
    case readContract

    private var name: String {
        "PicPay Card - Request - Credit Denied and Debit Offer"
    }
    
    private var properties: [String: Any] {
        switch self {
        case .cardAsked:
            return ["action": "act-request-plastic-card"]
            
        case .changeAddress:
            return ["action": "act-change-address"]
            
        case .readContract:
            return ["action": "act-read-contract"]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }
    
    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
