import UIKit

enum RejectedCreditFactory {
    static func make(coordinatorDelegate: RejectedCreditCoordinatingDelegate) -> RejectedCreditViewController {
        let container = DependencyContainer()
        let coordinator: RejectedCreditCoordinating = RejectedCreditCoordinator(dependencies: container)
        let presenter: RejectedCreditPresenting = RejectedCreditPresenter(coordinator: coordinator, dependencies: container)
        let service = RejectedCreditService(dependencies: container)
        let interactor = RejectedCreditInteractor(service: service, presenter: presenter, dependencies: container)
        let viewController = RejectedCreditViewController(interactor: interactor)

        coordinator.viewController = viewController
        coordinator.delegate = coordinatorDelegate
        presenter.viewController = viewController

        return viewController
    }
}
