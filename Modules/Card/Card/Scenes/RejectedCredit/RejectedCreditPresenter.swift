import Foundation
import FeatureFlag
import Core
import UI

protocol RejectedCreditPresenting: AnyObject {
    var viewController: RejectedCreditDisplaying? { get set }
    func presentNextStep(action: RejectedCreditAction)
    func presentError(_ error: ApiError)
    func presentLoading(_ loading: Bool)
    func presentData(address: HomeAddress)
    func presentLink(url: URL)
}

final class RejectedCreditPresenter {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies

    private let coordinator: RejectedCreditCoordinating
    weak var viewController: RejectedCreditDisplaying?

    init(coordinator: RejectedCreditCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
}

// MARK: - RejectedCreditPresenting
extension RejectedCreditPresenter: RejectedCreditPresenting {
    func presentLink(url: URL) {
        presentNextStep(action: .link(url: url))
    }
    
    func presentData(address: HomeAddress) {
        let attrString = Strings.RejectedCredit.AcceptContractView.text.attributedStringWith(
            normalFont: Typography.bodyPrimary().font(),
            highlightFont: Typography.bodyPrimary(.highlight).font(),
            normalColor: Colors.grayscale600.color,
            highlightColor: Colors.grayscale600.color,
            textAlignment: .left,
            underline: false
        )
        attrString.link(text: Strings.RejectedCredit.AcceptContractView.highlight, link: dependencies.featureManager.text(.termsDebitLink))
        viewController?.displayLoading(false)
        viewController?.displayData(addressString: buildAddressString(homeAddress: address), contractAttrString: attrString)
    }
    
    func presentLoading(_ loading: Bool) {
        viewController?.displayLoading(loading)
    }
    
    func presentError(_ error: ApiError) {
        viewController?.displayError(model: .init(error))
    }
    
    func presentNextStep(action: RejectedCreditAction) {
        viewController?.displayLoading(false)
        coordinator.perform(action: action)
    }
}
