import UI
import UIKit
import AssetsKit

protocol RejectedCreditDisplaying: AnyObject {
    func displayLoading(_ loading: Bool)
    func displayError(model: StatefulErrorViewModel)
    func displayData(addressString: String, contractAttrString: NSAttributedString)
}

final class RejectedCreditViewController: ViewController<RejectedCreditInteracting, RejectedCreditRootView> {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBarAppearance()
        interactor.loadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    private func updateNavigationBarAppearance() {
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func configureViews() {
        rootView.delegate = self
    }
    
    @objc
    private func close() {
        interactor.close()
    }
}

// MARK: - RejectedCreditDisplaying
extension RejectedCreditViewController: RejectedCreditDisplaying {
    func displayData(addressString: String, contractAttrString: NSAttributedString) {
        rootView.setupView(addressString: addressString, contractAttrString: contractAttrString)
    }
    
    func displayError(model: StatefulErrorViewModel) {
        endState(animated: true, model: model, completion: nil)
    }
    
    func displayLoading(_ loading: Bool) {
        loading ? beginState() : endState()
    }
}

extension RejectedCreditViewController: RejectedCreditRootViewDelegate {
    func didPressChangeAddress() {
        interactor.changeAddress()
    }
    
    func didOpenLink(url: URL) {
        interactor.openLink(url: url)
    }
    
    func didPressAskCard() {
        interactor.askCard()
    }
    
    func didClose() {
        interactor.close()
    }
}

extension RejectedCreditViewController: StatefulTransitionViewing {
    func didTryAgain() {
        interactor.loadData()
    }
}
