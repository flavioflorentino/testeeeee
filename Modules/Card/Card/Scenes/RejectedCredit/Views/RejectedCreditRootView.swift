import UIKit
import UI

protocol RejectedCreditRootViewDelegate: AnyObject {
    func didPressAskCard()
    func didClose()
    func didOpenLink(url: URL)
    func didPressChangeAddress()
}

private extension RejectedCreditRootView.Layout {
    enum Size {
        static let linkButtonHeight: CGFloat = 14
    }
}

final class RejectedCreditRootView: UIView, ViewConfiguration {
    fileprivate enum Layout {}
    weak var delegate: RejectedCreditRootViewDelegate?
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.contentInset = .init(top: 0, left: 0, bottom: Spacing.base03, right: 0)
        scrollView.isHidden = true
        return scrollView
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [infoImageView, changeAddressInsetView, acceptContractView])
        stackView.distribution = .fill
        stackView.axis = .vertical
        stackView.spacing = Spacing.base03
        return stackView
    }()
    
    private lazy var infoImageView = InfoImageView()
    private lazy var acceptContractView = AcceptContractView()
    
    private lazy var changeAddressView = ChangeAddressView()
    private lazy var changeAddressLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textAlignment, .center)
            .with(\.text, Strings.RejectedCredit.ChangeAddressView.title)
        return label
    }()
    private lazy var changeAddressInsetView = UIView()
    
    private lazy var shadowView: UIView = {
        let view = UIView()
        view.viewStyle(RoundedViewStyle())
            .with(\.backgroundColor, .backgroundPrimary())
            .with(\.border, .light(color: .grayscale100()))
            .with(\.cornerRadius, .strong)
            .with(\.shadow, .medium(color: .black(.light), opacity: .light))
        if #available(iOS 11.0, *) {
            view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        }
        return view
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.RejectedCredit.ConfirmButton.title, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(askCard), for: .touchUpInside)
        button.isEnabled = false
        button.isHidden = true
        return button
    }()
    
    private lazy var notYetButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.RejectedCredit.NotYet.buttonTitle, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(close), for: .touchUpInside)
        return button
    }()
    
    init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubview(scrollView)
        scrollView.addSubview(stackView)
        
        changeAddressInsetView.addSubviews(changeAddressLabel, changeAddressView)
        shadowView.addSubviews(confirmButton, notYetButton)
        addSubview(shadowView)
    }
    
    func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.top.equalTo(compatibleSafeArea.top)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(shadowView.snp.top)
        }
        
        stackView.snp.makeConstraints {
            $0.leading.trailing.top.equalToSuperview().inset(Spacing.base03)
            $0.bottom.centerX.equalToSuperview()
        }
        
        changeAddressLabel.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(changeAddressView.snp.top).offset(-Spacing.base01)
        }
        
        changeAddressView.snp.makeConstraints {
            $0.leading.trailing.bottom.equalToSuperview()
        }
        
        confirmButton.snp.makeConstraints {
            $0.leading.trailing.top.equalToSuperview().inset(Spacing.base03)
            $0.bottom.equalTo(notYetButton.snp.top).offset(-Spacing.base02)
        }
        
        notYetButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.bottom.equalTo(compatibleSafeArea.bottom).offset(-Spacing.base03)
            $0.height.equalTo(Layout.Size.linkButtonHeight)
        }
        
        shadowView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundSecondary.color
        acceptContractView.backgroundColor = Colors.backgroundPrimary.color
        
        infoImageView.setupView(
            image: Assets.iconUpgradeDebit.image,
            title: Strings.RejectedCredit.ImageInfoView.title,
            description: Strings.RejectedCredit.ImageInfoView.description
        )
        acceptContractView.delegate = self
        changeAddressView.delegate = self
        
        changeAddressView.viewStyle(RoundedViewStyle())
            .with(\.backgroundColor, .backgroundPrimary())
            .with(\.border, .light(color: .grayscale100()))
            .with(\.cornerRadius, .medium)
            .with(\.shadow, .medium(color: .black(.light), opacity: .light))
    }
    
    @objc
    private func askCard() {
        delegate?.didPressAskCard()
    }
    
    @objc
    private func close() {
        delegate?.didClose()
    }
    
    func setupView(addressString: String, contractAttrString: NSAttributedString) {
        scrollView.isHidden = false
        confirmButton.isHidden = false
        changeAddressView.setupView(title: addressString)
        acceptContractView.setupView(contractText: contractAttrString)
    }
}

extension RejectedCreditRootView: AcceptContractViewDelegate {
    func didToggleCheckmark(isSelected: Bool) {
        confirmButton.isEnabled = isSelected
    }
    
    func didOpenLink(url: URL) {
        delegate?.didOpenLink(url: url)
    }
}

extension RejectedCreditRootView: ChangeAddressViewDelegate {
    func didPressChangeAddress() {
        delegate?.didPressChangeAddress()
    }
}
