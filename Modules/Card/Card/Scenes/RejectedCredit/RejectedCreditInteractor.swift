import Foundation
import AnalyticsModule

protocol RejectedCreditInteracting: AnyObject {
    func askCard()
    func close()
    func loadData()
    func changeAddress()
    func openLink(url: URL)
}

final class RejectedCreditInteractor {
    typealias Dependencies = HasAnalytics & HasAuthManagerDependency
    private let dependencies: Dependencies

    private let presenter: RejectedCreditPresenting
    private let service: RejectedCreditServicing
    
    private var cardForm: CardForm = .init()

    init(service: RejectedCreditServicing, presenter: RejectedCreditPresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.service = service
        self.dependencies = dependencies
    }
}

// MARK: - RejectedCreditInteracting
extension RejectedCreditInteractor: RejectedCreditInteracting {
    func openLink(url: URL) {
        dependencies.analytics.log(RejectedCreditEvent.readContract)
        presenter.presentLink(url: url)
    }
    
    func changeAddress() {
        dependencies.analytics.log(RejectedCreditEvent.changeAddress)
        presenter.presentNextStep(action: .changeAddress(cardForm: cardForm))
    }
    
    func loadData() {
        presenter.presentLoading(true)
        service.getCardForm { [weak self] result in
            switch result {
            case let .success(cardForm):
                self?.cardForm = cardForm
                guard let address = cardForm.homeAddress else {
                     self?.presenter.presentError(.serverError)
                     return
                }
                self?.presenter.presentData(address: address)
                
            case let .failure(error):
                self?.presenter.presentError(error)
            }
        }
    }
    
    func askCard() {
        dependencies.analytics.log(RejectedCreditEvent.cardAsked)
        dependencies.authManager.authenticate { [weak self] pin, _ in
            self?.confirm(pin: pin)
        } cancelHandler: {
            self.dependencies.analytics.log(CreditHiringEvent.authentication(action: .cancel))
        }
    }
    
    func close() {
        presenter.presentNextStep(action: .close)
    }
    
    private func confirm(pin: String) {
        presenter.presentLoading(true)
        service.finish(pin: pin) { [weak self] result in
            switch result {
            case .success:
                self?.dependencies.analytics.log(CreditHiringEvent.authenticated(state: .validaded))
                self?.presenter.presentNextStep(action: .askCard)
            case let .failure(error):
                self?.dependencies.analytics.log(CreditHiringEvent.authenticated(state: .failed))
                self?.presenter.presentError(error)
            }
        }
    }
}
