import UIKit

protocol RejectedCreditCoordinatingDelegate: AnyObject {
    func didAskCard()
    func didChangeAddress(cardForm: CardForm)
    func didCloseRejectCredit()
}

enum RejectedCreditAction {
    case changeAddress(cardForm: CardForm)
    case askCard
    case close
    case link(url: URL)
}

protocol RejectedCreditCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: RejectedCreditCoordinatingDelegate? { get set }
    func perform(action: RejectedCreditAction)
}

final class RejectedCreditCoordinator: SafariWebViewPresentable {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    weak var viewController: UIViewController?
    weak var delegate: RejectedCreditCoordinatingDelegate?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - RejectedCreditCoordinating
extension RejectedCreditCoordinator: RejectedCreditCoordinating {
    func perform(action: RejectedCreditAction) {
        switch action {
        case .askCard:
            delegate?.didAskCard()

        case .close:
            delegate?.didCloseRejectCredit()
            
        case let .changeAddress(cardForm: cardForm):
            delegate?.didChangeAddress(cardForm: cardForm)
            
        case let .link(url: url):
            open(url: url)
        }
    }
}
