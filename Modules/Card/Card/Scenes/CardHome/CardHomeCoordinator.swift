import UIKit

public enum CardHomeAction {
    case openCardSettings
}

public protocol CardHomeCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: CardHomeFlowCoordinatorDelegate? { get set }
    func perform(action: CardHomeAction)
}

public final class CardHomeCoordinator {
    public weak var viewController: UIViewController?
    public weak var delegate: CardHomeFlowCoordinatorDelegate?
}

// MARK: - CardHomeCoordinating
extension CardHomeCoordinator: CardHomeCoordinating {
    public func perform(action: CardHomeAction) {
        switch action {
        case .openCardSettings:
            delegate?.openCardSettings()
        }
    }
}
