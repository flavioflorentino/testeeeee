import UIKit

public enum CardHomeFactory {
    public static func make(delegate: CardHomeFlowCoordinatorDelegate) -> CardHomeViewController {
        let container = DependencyContainer()
        let service: CardHomeServicing = CardHomeService(dependencies: container)
        let coordinator: CardHomeCoordinating = CardHomeCoordinator()
        let presenter: CardHomePresenting = CardHomePresenter(coordinator: coordinator)
        let interactor = CardHomeInteractor(service: service, presenter: presenter, dependencies: container)
        let viewController = CardHomeViewController(interactor: interactor)

        coordinator.viewController = viewController
        coordinator.delegate = delegate
        presenter.viewController = viewController

        return viewController
    }
}
