import Core
import Foundation

public protocol CardHomeServicing {
}

public final class CardHomeService {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - CardHomeServicing
extension CardHomeService: CardHomeServicing {}
