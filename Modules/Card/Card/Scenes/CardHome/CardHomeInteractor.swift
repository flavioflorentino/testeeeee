import Foundation

public protocol CardHomeInteracting: AnyObject {
    func openCardSettings()
}

public final class CardHomeInteractor {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let service: CardHomeServicing
    private let presenter: CardHomePresenting

    init(service: CardHomeServicing, presenter: CardHomePresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - CardHomeInteracting
extension CardHomeInteractor: CardHomeInteracting {
    public func openCardSettings() {
        presenter.didNextStep(action: .openCardSettings)
    }
}
