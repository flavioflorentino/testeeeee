import UI
import UIKit
import AssetsKit

public protocol CardHomeDisplaying: AnyObject {}

private extension CardHomeViewController.Layout {
    enum ScreenRatio {
        static let mainCollectionHeightRatio: CGFloat = 0.55
        static let configCollectionHeightRatio: CGFloat = 0.25
        static let transactionsHeightRatio: CGFloat = 0.2
    }
}

public final class CardHomeViewController: ViewController<CardHomeInteracting, UIView> {
    // MARK: - Private Properties
    fileprivate enum Layout { }

    // MARK: - View Properties
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
        return refreshControl
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.insertSubview(refreshControl, at: 0)
        scrollView.contentOffset = .zero
        scrollView.showsVerticalScrollIndicator = false
        return scrollView
    }()
    
    private lazy var contentView: UIView = {
        UIView()
    }()
    
    private lazy var mainCollectionContainerView: UIView = {
        UIView()
    }()
    
    private lazy var configCollectionContainerView: UIView = {
        UIView()
    }()
    
    private lazy var transactionsContainerView: UIView = {
        UIView()
    }()
    
    // MARK: - Override Functions
    override public func viewDidLoad() {
        title = "PicPay Card"
        super.viewDidLoad()
        buildLayout()
    }
    
    // MARK: - Objc Selectors
    @objc
    func onRefresh() {
        run(after: 2) { [weak self] in
            self?.refreshControl.endRefreshing()
        }
    }
    
    @objc
    func settingsTapped() {
        viewModel.openCardSettings()
    }
    
    // MARK: - View Configuration
    override public func buildViewHierarchy() {
        contentView.addSubviews(mainCollectionContainerView,
                                configCollectionContainerView,
                                transactionsContainerView)
        scrollView.addSubview(contentView)
        scrollView.delegate = self
        view.addSubview(scrollView)
    }
    
    override public func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.size.equalToSuperview()
        }
        
        contentView.snp.makeConstraints {
            $0.edges.size.equalToSuperview()
        }
        
        mainCollectionContainerView.snp.makeConstraints {
            $0.leading.top.trailing.equalToSuperview()
            $0.height.equalToSuperview().multipliedBy(Layout.ScreenRatio.mainCollectionHeightRatio)
        }
        
        configCollectionContainerView.snp.makeConstraints {
            $0.top.equalTo(mainCollectionContainerView.snp.bottom)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalToSuperview().multipliedBy(Layout.ScreenRatio.configCollectionHeightRatio)
        }
        
        transactionsContainerView.snp.makeConstraints {
            $0.top.equalTo(configCollectionContainerView.snp.bottom)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalToSuperview().multipliedBy(Layout.ScreenRatio.transactionsHeightRatio)
        }
    }

    override public func configureViews() {
        mainCollectionContainerView.backgroundColor = .purple
        configCollectionContainerView.backgroundColor = .blue
        transactionsContainerView.backgroundColor = .green
        
        addSettingsRightBarButtonItem()
    }
    
    // MARK: - Private Functions
    // Temporary function to simulate refresh state
    private func run(after wait: TimeInterval, closure: @escaping () -> Void) {
        let queue = DispatchQueue.main
        queue.asyncAfter(deadline: DispatchTime.now() + wait, execute: closure)
    }
    
    private func addSettingsRightBarButtonItem() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: Assets.icoSettingsRedesign.image,
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(settingsTapped))
    }
}

// MARK: - CardHomeDisplaying
extension CardHomeViewController: CardHomeDisplaying {}

extension CardHomeViewController: StyledNavigationDisplayable {
    public var navigationBarStyle: DefaultNavigationStyle {
        RedesignNavigationStyle()
    }
}

extension CardHomeViewController: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // Disable vertical bounce at the bottom
        if scrollView.contentOffset.y > scrollView.contentSize.height - scrollView.bounds.height {
            scrollView.contentOffset.y = scrollView.contentSize.height - scrollView.bounds.height
        }
    }
}
