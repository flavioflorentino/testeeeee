import Foundation

public protocol CardHomePresenting: AnyObject {
    var viewController: CardHomeDisplaying? { get set }
    func didNextStep(action: CardHomeAction)
}

public final class CardHomePresenter {
    private let coordinator: CardHomeCoordinating
    public weak var viewController: CardHomeDisplaying?

    init(coordinator: CardHomeCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - CardHomePresenting
extension CardHomePresenter: CardHomePresenting {
    public func didNextStep(action: CardHomeAction) {
        coordinator.perform(action: action)
    }
}
