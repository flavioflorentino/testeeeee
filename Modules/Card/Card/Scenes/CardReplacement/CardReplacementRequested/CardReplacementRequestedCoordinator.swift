import UIKit

public enum CardReplacementRequestedAction {
    case openFAQ
    case externalLink(URL)
}

public protocol CardReplacementRequestedCoordinatorDelegate: AnyObject {
    func didOpenFAQ()
}

public protocol CardReplacementRequestedCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: CardReplacementRequestedCoordinatorDelegate? { get set }
    func perform(action: CardReplacementRequestedAction)
}

public final class CardReplacementRequestedCoordinator {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    public weak var viewController: UIViewController?
    public weak var delegate: CardReplacementRequestedCoordinatorDelegate?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - CardReplacementRequestedCoordinating
extension CardReplacementRequestedCoordinator: CardReplacementRequestedCoordinating {
    public func perform(action: CardReplacementRequestedAction) {
        switch action {
        case .openFAQ:
            delegate?.didOpenFAQ()
        case .externalLink(let url):
            open(url: url)
        }
    }
}
// MARK: - SafariWebViewPresentable
extension CardReplacementRequestedCoordinator: SafariWebViewPresentable {}
