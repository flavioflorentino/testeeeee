import FeatureFlag
import Foundation
import UI

public protocol CardReplacementRequestedPresenting: AnyObject {
    var viewController: CardReplacementRequestedDisplay? { get set }
    func presentSuccess(requestedDate: String)
    func presentError()
    func presentFAQ()
    func didNextStep(action: CardReplacementRequestedAction)
}

public final class CardReplacementRequestedPresenter {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies

    private let coordinator: CardReplacementRequestedCoordinating
    public weak var viewController: CardReplacementRequestedDisplay?
    
    init(coordinator: CardReplacementRequestedCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
}

// MARK: - CardReplacementRequestedPresenting
extension CardReplacementRequestedPresenter: CardReplacementRequestedPresenting {
    public func presentSuccess(requestedDate: String) {
        if let date = formatDate(requestedDate) {
            viewController?.displaySuccess(subtitle: buildSubtitle(requestedDate: date), faq: buildFAQ())
        } else {
            viewController?.displayError(error: buildErrorStateful())
        }
    }
    
    public func presentError() {
        viewController?.displayError(error: buildErrorStateful())
    }
    
    public func presentFAQ() {
        if let url = URL(string: dependencies.featureManager.text(.opsCardReplacementFaq)) {
            viewController?.displayFAQ(url: url)
        } else {
            viewController?.displayError(error: buildErrorStateful())
        }
    }
    
    public func didNextStep(action: CardReplacementRequestedAction) {
        coordinator.perform(action: action)
    }
    
    public func formatDate(_ requestedDate: String) -> String? {
        guard let date = Date.decodableFormatter.date(from: requestedDate) else {
            return nil
        }
        return date.formatted()
    }
    
    // MARK: - Builders
    private func buildSubtitle(requestedDate: String) -> NSAttributedString {
        let string = Strings.CardReplacementRequested.Subtitle.message(requestedDate)
        
        let attributed = NSMutableAttributedString(string: string)
        attributed.font(text: string, font: Typography.bodyPrimary().font())
        attributed.font(text: Strings.CardReplacementRequested.Subtitle.highlight, font: Typography.bodyPrimary(.highlight).font())
        attributed.textColor(text: string, color: Colors.grayscale900.color)
        attributed.paragraph(aligment: .center, lineSpace: 3.0)
        return attributed
    }
    
    private func buildFAQ() -> NSAttributedString {
        let string = Strings.CardReplacementRequested.Faq.message
        let linked = Strings.CardReplacementRequested.Faq.link
        
        let attributed = NSMutableAttributedString(string: string)
        attributed.textColor(text: string, color: Colors.grayscale900.color)
        attributed.textColor(text: linked, color: Colors.branding300.color)
        attributed.font(text: string, font: Typography.bodyPrimary().font())
        attributed.paragraph(aligment: .center, lineSpace: 3.0)
        attributed.underline(text: linked)
        return attributed
    }
    
    private func buildErrorStateful() -> StatefulErrorViewModel {
        StatefulErrorViewModel(
            image: Assets.icoSadEmoji.image,
            content: (
                title: Strings.Default.ops,
                description: Strings.Default.errorLoadingInformation
            ),
            button: (image: nil, title: Strings.Default.retry)
        )
    }
}
