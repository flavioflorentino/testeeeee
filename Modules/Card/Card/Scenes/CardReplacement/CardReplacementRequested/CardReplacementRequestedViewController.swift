import SafariServices
import UI

public protocol CardReplacementRequestedDisplay: AnyObject {
    func displaySuccess(subtitle: NSAttributedString, faq: NSAttributedString)
    func displayError(error: StatefulErrorViewModel)
    func displayFAQ(url: URL)
}

private extension CardReplacementRequestedViewController.Layout {
    enum ViewContainer {
        static let height: CGFloat = 288.0
    }
    enum ImageThumb {
        static let size = CGSize(width: 96.0, height: 88.0)
    }
}

public final class CardReplacementRequestedViewController: ViewController<CardReplacementRequestedViewModelInputs, UIView> {
    fileprivate enum Layout { }
    
    private lazy var viewContainer = UIView()
    private lazy var imageThumb: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.cardEnvelope.image
        imageView.contentMode = .center
        return imageView
    }()
    private lazy var labelTitle: UILabel = {
        let label = UILabel()
        label.text = Strings.CardReplacementRequested.title
        label
            .labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, .black())
            .with(\.textAlignment, .center)
        return label
    }()
    private lazy var labelSubtitle: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    private lazy var buttonFAQ: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(didTapButtonOpenFAQ), for: .touchUpInside)
        button.titleLabel?.numberOfLines = 0
        return button
    }()
    
    // MARK: - ViewController
    override public func buildViewHierarchy() {
        view.addSubview(viewContainer)
        viewContainer.addSubviews(imageThumb, labelTitle, labelSubtitle, buttonFAQ)
    }
    
    override public func setupConstraints() {
        viewContainer.snp.makeConstraints {
            $0.centerY.equalTo(view.snp.centerY)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.height.equalTo(Layout.ViewContainer.height)
        }
        imageThumb.snp.makeConstraints {
            $0.top.centerX.equalToSuperview()
            $0.size.equalTo(Layout.ImageThumb.size)
        }
        labelTitle.snp.makeConstraints {
            $0.top.equalTo(imageThumb.compatibleSafeArea.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
        }
        labelSubtitle.snp.makeConstraints {
            $0.top.equalTo(labelTitle.compatibleSafeArea.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        buttonFAQ.snp.makeConstraints {
            $0.top.equalTo(labelSubtitle.compatibleSafeArea.bottom).offset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }

    override public func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    // MARK: - Cycle
    override public func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }
    
    override public func didMove(toParent parent: UIViewController?) {
        super.didMove(toParent: parent)
        
        guard parent == nil else {
            return
        }
        viewModel.back()
    }
    
    @objc
    private func didTapButtonOpenFAQ() {
        viewModel.openFAQ()
    }
    
    private func loadData() {
        viewModel.loadData()
        beginState(animated: true, model: StateLoadingViewModel(message: Strings.Default.loading))
    }
}
// MARK: - CardReplacementRequestedDisplay
extension CardReplacementRequestedViewController: CardReplacementRequestedDisplay {
    public func displaySuccess(subtitle: NSAttributedString, faq: NSAttributedString) {
        labelSubtitle.attributedText = subtitle
        buttonFAQ.setAttributedTitle(faq, for: .normal)
        endState()
    }
    
    public func displayError(error: StatefulErrorViewModel) {
        endState(model: error)
    }
    
    public func displayFAQ(url: URL) {
        viewModel.openExternalLink(url: url)
    }
}

extension CardReplacementRequestedViewController: StatefulTransitionViewing {
    public func didTryAgain() {
        loadData()
    }
}

extension CardReplacementRequestedViewController: StyledNavigationDisplayable {
    public var prefersLargeTitle: Bool {
        false
    }
}
