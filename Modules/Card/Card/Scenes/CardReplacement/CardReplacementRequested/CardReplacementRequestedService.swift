import Core
import Foundation

protocol CardReplacementRequestedServicing {
    func tracking(completion: @escaping CompletionCardTracking)
}

public typealias CompletionCardTracking = (Result<CardTracking, ApiError>) -> Void

final class CardReplacementRequestedService {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - CardReplacementRequestedServicing
extension CardReplacementRequestedService: CardReplacementRequestedServicing {
    func tracking(completion: @escaping CompletionCardTracking) {
        Api<CardTracking>(endpoint: CreditServiceEndpoint.cardTracking).execute { result in
            DispatchQueue.main.async {
                let mappedResult = result
                    .mapError { $0 as ApiError }
                    .map { $0.model }
                completion(mappedResult)
            }
        }
    }
}
