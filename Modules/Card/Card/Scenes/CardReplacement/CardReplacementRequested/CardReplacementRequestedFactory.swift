import Foundation

public enum CardReplacementRequestedFactory {
    public static func make() -> CardReplacementRequestedViewController {
        let container = DependencyContainer()
        let service: CardReplacementRequestedServicing = CardReplacementRequestedService(dependencies: container)
        let coordinator: CardReplacementRequestedCoordinating = CardReplacementRequestedCoordinator(dependencies: container)
        let presenter: CardReplacementRequestedPresenting = CardReplacementRequestedPresenter(coordinator: coordinator, dependencies: container)
        let viewModel = CardReplacementRequestedViewModel(service: service, presenter: presenter, dependencies: container)
        let viewController = CardReplacementRequestedViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
