import AnalyticsModule
import Foundation

public protocol CardReplacementRequestedViewModelInputs: AnyObject {
    func loadData()
    func openFAQ()
    func back()
    func openExternalLink(url: URL)
}

public final class CardReplacementRequestedViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let service: CardReplacementRequestedServicing
    private let presenter: CardReplacementRequestedPresenting

    init(service: CardReplacementRequestedServicing, presenter: CardReplacementRequestedPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
    
    private func getRequestDate() {
        service.tracking { [weak self] result in
            guard let self = self else {
                return
            }
            
            switch result {
            case .success(let tracking):
                if let requestDate = tracking.requestAt {
                    self.presenter.presentSuccess(requestedDate: requestDate)
                } else {
                    self.presenter.presentError()
                }
            case .failure:
                self.presenter.presentError()
            }
        }
    }
}

// MARK: - CardReplacementRequestedViewModelInputs
extension CardReplacementRequestedViewModel: CardReplacementRequestedViewModelInputs {
    public func openExternalLink(url: URL) {
        presenter.didNextStep(action: .externalLink(url))
    }
    
    public func loadData() {
        getRequestDate()
    }
    public func openFAQ() {
        dependencies.analytics.log(CardReplacementRequestedEvent.information(action: .faq))
        presenter.presentFAQ()
    }
    public func back() {
        dependencies.analytics.log(CardReplacementRequestedEvent.information(action: .back))
    }
}
