import UI
import UIKit
import Core

public protocol HybridDeepLinkRegistrationDisplaying: AnyObject {
    func displayLoadView()
    func displayIdealScenario(_ model: StatefulFeedbackViewModel)
    func displayError(_ modelError: StatefulFeedbackViewModel)
}

public final class HybridDeepLinkRegistrationViewController: ViewController<HybridDeepLinkRegistrationInteracting, UIView> {
    fileprivate enum Layout { }

    override public func viewDidLoad() {
        super.viewDidLoad()

        interactor.loadData()
    }
}

// MARK: - HybridDeepLinkRegistrationDisplaying
extension HybridDeepLinkRegistrationViewController: HybridDeepLinkRegistrationDisplaying {
    public func displayError(_ modelError: StatefulFeedbackViewModel) {
        endState(animated: true, model: modelError)
    }
    
    public func displayIdealScenario(_ model: StatefulFeedbackViewModel) {
        endState(animated: true, model: model)
    }
    
    public func displayLoadView() {
        beginState()
    }
}

// MARK: - StatefulTransitionViewing
extension HybridDeepLinkRegistrationViewController: StatefulTransitionViewing {
    public func statefulViewForError() -> StatefulViewing {
        StatefulFeedbackView()
    }
    public func didTryAgain() {
        interactor.didTapConfirmButton()
    }
    
    public func didTapSecondaryButton() {
        interactor.secondaryButtonAction()
    }
}
