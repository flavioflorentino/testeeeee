import Core
import Foundation

public typealias HybridDeepLinkResult = (Result<HybridDeepLink, ApiError>) -> Void

public protocol HybridDeepLinkRegistrationServicing {
    func loadStateRegistration(completion: @escaping HybridDeepLinkResult)
}

final class HybridDeepLinkRegistrationService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - HybridDeepLinkRegistrationServicing
extension HybridDeepLinkRegistrationService: HybridDeepLinkRegistrationServicing {
    func loadStateRegistration(completion: @escaping HybridDeepLinkResult) {
        Api<HybridDeepLink>(endpoint: CreditServiceEndpoint.hybridFlowRegistration).execute { [weak self] result in
            let mappedResult = result.map { $0.model }
            self?.dependencies.mainQueue.async {
                completion(mappedResult)
            }
        }
    }
}
