import Foundation
import UIKit
import UI

public enum HybridDeeplinkFlowCoordinatorOutput: Equatable {
    case debit
    case credit
    case simplifiedDebit
    case close
    case cardHome
    case cardSettings
}

public final class HybridDeeplinkFlowCoordinator: Coordinating {
    private var navigationController: UINavigationController
    private let mainActivateNavigationController = StyledNavigationController()
    public var childViewController: [UIViewController] = []
    public var viewController: UIViewController?

    public init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    public func start() {
        let controller = HybridDeepLinkRegistrationFactory.make(coordinatorOutput: { [weak self] result in
            self?.coordinatorOutput(result)
        })
        mainActivateNavigationController.setNavigationBarHidden(true, animated: false)
        mainActivateNavigationController
            .presentationController?
            .delegate = navigationController.viewControllers.first as? UIAdaptivePresentationControllerDelegate
        mainActivateNavigationController.pushViewController(controller, animated: true)
        navigationController.present(mainActivateNavigationController, animated: true)
        viewController = mainActivateNavigationController
    }

    private func coordinatorOutput(_ coordinatorOutput: HybridDeeplinkFlowCoordinatorOutput) {
        switch coordinatorOutput {
        case .debit:
            self.startFlowInSameNavigation(coordinator: DebitRequestFlowCoordinator(navigationController: mainActivateNavigationController, parentNavigation: navigationController))
        case .cardHome:
            startCreditFlow(hasCard: true)
        case .credit:
            startCreditFlow()
        case .simplifiedDebit:
            startFlowInSameNavigation(coordinator: RejectedCreditFlowCoordinator(navigationController: mainActivateNavigationController, parentNavigation: navigationController))
        case .cardSettings:
            startFlowInSameNavigation(coordinator: CardSettingsFlowCoordinator(navigationController: mainActivateNavigationController))
        case .close:
            finishFlow()
        }
    }
    
    private func startFlowInSameNavigation(coordinator: FlowsCoordinating?) {
        coordinator?.startAnotherFlow()
        CardSetup.navigationInstance?.updateCurrentCoordinating(coordinator)
    }
   
    private func finishFlow(completion: (() -> Void)? = nil) {
        navigationController.dismiss(animated: true, completion: completion)
    }
    
    func startCreditFlow(hasCard: Bool = false) {
        let coordinator = CardSetup.cardSettingsFlowCoordinator
        coordinator?.hasCard = hasCard
        coordinator?.navigationController = mainActivateNavigationController
        startFlowInSameNavigation(coordinator: coordinator)
    }
}

public protocol NeedNavigation: AnyObject {
    var navigationController: UINavigationController? { get set }
}

public protocol FlowsCoordinating: Coordinating {
    func startAnotherFlow()
}

public protocol CardFlowProtocol: FlowsCoordinating & NeedNavigation {
    var hasCard: Bool { get set }
}
