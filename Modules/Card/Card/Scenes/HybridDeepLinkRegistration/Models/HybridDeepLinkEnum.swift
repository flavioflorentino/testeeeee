import Foundation
import UIKit
import UI
import AssetsKit

public enum HybridDeepLinkEnum: String, Decodable {
    case debit = "DEBIT"
    case credit = "CREDIT"
    case simplifiedDebit = "SIMPLIFIED_DEBIT"
    case offerNotFound = "OFFER_NOT_FOUND"
    case userHasDebit = "USER_ALREADY_HAS_DEBIT"
    case userHasCredit = "USER_ALREADY_HAS_CREDIT"
    case debitInProgress = "DEBIT_IN_PROGRESS"
    case creditInProgress = "CREDIT_IN_PROGRESS"
    
    func returnStatefulFeedbackViewModel() -> StatefulFeedbackViewModel {
        switch self {
        case .offerNotFound:
            return StatefulFeedbackViewModel(image: AssetsKit.Resources.Illustrations.iluPersonCard.image, content: (title: Strings.HybridDeepLinkRegistration.OfferNotFound.title, description: Strings.HybridDeepLinkRegistration.OfferNotFound.description), button: (image: nil, title: Strings.Default.iGotIt), secondaryButton: (image: nil, title: Strings.HybridDeepLinkRegistration.OfferNotFound.secondaryButton))
        case .creditInProgress, .debitInProgress, .simplifiedDebit:
            return StatefulFeedbackViewModel(image: Card.Assets.iconUpgradeDebit.image, content: (title: Strings.HybridDeepLinkRegistration.ProgressRegistration.title, description: Strings.HybridDeepLinkRegistration.ProgressRegistration.description), button: (image: nil, title: Strings.HybridDeepLinkRegistration.ProgressRegistration.primaryButton), secondaryButton: (image: nil, title: Strings.HybridDeepLinkRegistration.ProgressRegistration.secondaryButton))
        default:
            return StatefulFeedbackViewModel(image: Card.Assets.iconUpgradeDebit.image, content: (title: Strings.HybridDeepLinkRegistration.UserHasCard.title, description: Strings.HybridDeepLinkRegistration.UserHasCard.description), button: (image: nil, title: Strings.HybridDeepLinkRegistration.UserHasCard.primaryButton), secondaryButton: (image: nil, title: Strings.HybridDeepLinkRegistration.UserHasCard.secondaryButton))
        }
    }
}
