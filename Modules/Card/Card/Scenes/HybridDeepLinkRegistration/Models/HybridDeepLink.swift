import Foundation
import UIKit

public struct HybridDeepLink: Decodable {
    let cardOfferFlow: HybridDeepLinkEnum
    
    enum CodingKeys: String, CodingKey {
        case cardOfferFlow = "card_offer_flow"
    }
}
