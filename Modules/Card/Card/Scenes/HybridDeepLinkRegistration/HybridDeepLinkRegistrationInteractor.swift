import Foundation
import Core
import AnalyticsModule
import FeatureFlag
public protocol HybridDeepLinkRegistrationInteracting: AnyObject {
    func loadData()
    func dismiss()
    func didTapConfirmButton()
    func secondaryButtonAction()
}

public final class HybridDeepLinkRegistrationInteractor {
    typealias Dependencies = HasAnalytics & HasFeatureManager
    private let dependencies: Dependencies
    private let service: HybridDeepLinkRegistrationServicing
    private let presenter: HybridDeepLinkRegistrationPresenting
    private(set) var state: HybridDeepLinkEnum?
   
    init(service: HybridDeepLinkRegistrationServicing, presenter: HybridDeepLinkRegistrationPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - HybridDeepLinkRegistrationInteracting
extension HybridDeepLinkRegistrationInteractor: HybridDeepLinkRegistrationInteracting {
    public func secondaryButtonAction() {
        guard let deepLink = takeDeeplinkHelpCenter() else {
            dismiss()
            return
        }
        
        presenter.didNextStep(action: .helpCenter(deepLink))
    }
    
    public func didTapConfirmButton() {
        guard state != nil else {
            loadData()
            return
        }
        confirmeButtonTracking()
        switch state {
        case .userHasDebit:
            presenter.didNextStep(action: .cardSettings)
        case .userHasCredit:
            presenter.didNextStep(action: .cardHome)
        case .creditInProgress:
            presenter.didNextStep(action: .creditFlow)
        case .debitInProgress:
            presenter.didNextStep(action: .debitFlow)
        case .simplifiedDebit:
            presenter.didNextStep(action: .simplifiedDebitFlow)
        default:
            dismiss()
        }
    }
    
    public func loadData() {
        presenter.presentLoadState()
        service.loadStateRegistration { [weak self] result in
            switch result {
            case .success(let deepLink):
                self?.state = deepLink.cardOfferFlow
                self?.didCallPresentFlow()
                self?.trackingEvents()
            case .failure(let error):
                self?.presenter.presentError(error)
            }
        }
    }
                    
    func takeDeeplinkHelpCenter() -> String? {
        switch state {
        case .offerNotFound:
            return dependencies.featureManager.text(.helpCenterCardForUserWithNoOffer)
        case .userHasDebit:
            return dependencies.featureManager.text(.helpCenterCardForUserWithDebit)
        case .userHasCredit:
            return dependencies.featureManager.text(.helpCenterCardForUserWithCredit)
        default:
            return nil
        }
    }
    
    private func trackingEvents() {
        switch state {
        case .userHasDebit:
            dependencies.analytics.log(HybridDeepLinkRegistrationEvent.hasDebitCard)
        case .offerNotFound:
            dependencies.analytics.log(HybridDeepLinkRegistrationEvent.unavailableOffer)
        case .userHasCredit:
            dependencies.analytics.log(HybridDeepLinkRegistrationEvent.hasMultipleCard)
        case .simplifiedDebit, .creditInProgress, .debitInProgress:
            dependencies.analytics.log(HybridDeepLinkRegistrationEvent.progress(.inital))
        default:
            break
        }
    }
   
    private func confirmeButtonTracking() {
        switch state {
        case .simplifiedDebit, .creditInProgress, .debitInProgress:
            dependencies.analytics.log(HybridDeepLinkRegistrationEvent.progress(.confirm))
        default:
            break
        }
    }
    
    private func cancelButtonTracking() {
        switch state {
        case .simplifiedDebit, .creditInProgress, .debitInProgress:
            dependencies.analytics.log(HybridDeepLinkRegistrationEvent.progress(.cancel))
        default:
            break
        }
    }
    
    func didCallPresentFlow() {
        guard let state = state else {
            presenter.presentError(ApiError.bodyNotFound)
            return
        }
        
        switch state {
        case .debit:
            presenter.didNextStep(action: .debitFlow)
        case .credit:
            presenter.didNextStep(action: .creditFlow)
        default:
            presenter.presentIdealScenario(state)
        }
    }
    
    public func dismiss() {
        presenter.didNextStep(action: .dismiss)
        cancelButtonTracking()
    }
}
