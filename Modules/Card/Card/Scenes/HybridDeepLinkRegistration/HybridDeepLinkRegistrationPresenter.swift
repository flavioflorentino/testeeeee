import Foundation
import Core
import UI
import AssetsKit
public protocol HybridDeepLinkRegistrationPresenting: AnyObject {
    var viewController: HybridDeepLinkRegistrationDisplaying? { get set }
    func didNextStep(action: HybridDeepLinkRegistrationAction)
    func presentIdealScenario(_ flow: HybridDeepLinkEnum)
    func presentError(_ error: ApiError)
    func presentLoadState()
}

public final class HybridDeepLinkRegistrationPresenter {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let coordinator: HybridDeepLinkRegistrationCoordinating
    public weak var viewController: HybridDeepLinkRegistrationDisplaying?

    init(coordinator: HybridDeepLinkRegistrationCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
}

// MARK: - HybridDeepLinkRegistrationPresenting
extension HybridDeepLinkRegistrationPresenter: HybridDeepLinkRegistrationPresenting {
    public func presentLoadState() {
        viewController?.displayLoadView()
    }
    
    public func presentIdealScenario(_ flow: HybridDeepLinkEnum) {
        viewController?.displayIdealScenario(flow.returnStatefulFeedbackViewModel())
    }
    
    public func presentError(_ error: ApiError) {
        viewController?.displayError(buildErrorService(for: error))
    }
    
    public func didNextStep(action: HybridDeepLinkRegistrationAction) {
        coordinator.perform(action: action)
    }    
}
// MARK: Error builders
extension HybridDeepLinkRegistrationPresenter {
    private func buildErrorService(for error: ApiError) -> StatefulFeedbackViewModel {
        switch error {
        case .connectionFailure:
            return StatefulFeedbackViewModel(
                image: Resources.Illustrations.iluNoConnectionCircledBackground.image,
                content: (
                    title: Strings.HybridDeepLinkRegistration.ConectionFalure.title,
                    description: Strings.HybridDeepLinkRegistration.ConectionFalure.description
                ),
                button: (image: nil, title: Strings.Default.retry),
                secondaryButton: (image: nil, title: Strings.Default.cancelButtonTitle)
            )
        default:
            return StatefulFeedbackViewModel(
                image: Resources.Illustrations.iluError.image,
                content: (
                    title: Strings.HybridDeepLinkRegistration.GenericError.title,
                    description: Strings.HybridDeepLinkRegistration.GenericError.description
                ),
                button: (image: nil, title: Strings.Default.retry),
                secondaryButton: (image: nil, title: Strings.Default.cancelButtonTitle)
            )
        }
    }
}
