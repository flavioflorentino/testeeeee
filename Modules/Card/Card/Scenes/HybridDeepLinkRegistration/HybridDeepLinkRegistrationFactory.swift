import UIKit

public enum HybridDeepLinkRegistrationFactory {
     public static func make(coordinatorOutput: @escaping (HybridDeeplinkFlowCoordinatorOutput) -> Void) -> HybridDeepLinkRegistrationViewController {
        let container = DependencyContainer()
        let service: HybridDeepLinkRegistrationServicing = HybridDeepLinkRegistrationService(dependencies: container)
        let coordinator: HybridDeepLinkRegistrationCoordinating = HybridDeepLinkRegistrationCoordinator(dependencies: container)
        let presenter: HybridDeepLinkRegistrationPresenting = HybridDeepLinkRegistrationPresenter(coordinator: coordinator, dependencies: container)
        let interactor = HybridDeepLinkRegistrationInteractor(service: service, presenter: presenter, dependencies: container)
        let viewController = HybridDeepLinkRegistrationViewController(interactor: interactor)

        coordinator.coordinatorOutput = coordinatorOutput
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
