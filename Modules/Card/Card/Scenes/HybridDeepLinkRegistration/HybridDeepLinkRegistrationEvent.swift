import Foundation
import AnalyticsModule

public enum HybridDeepLinkRegistrationEvent: AnalyticsKeyProtocol {
    case unavailableOffer
    case hasDebitCard
    case hasMultipleCard
    case progress(ProgressActions)
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }
    private var name: String {
        switch self {
        case .unavailableOffer, .hasDebitCard, .hasMultipleCard:
            return "Unavailable offer"
        default:
            return "Continue Registration"
        }
    }
    private var properties: [String: Any] {
        switch self {
        case .unavailableOffer:
            return ["action": "act-access-deeplink-unavailable-offer"]
        case .hasDebitCard:
            return ["action": "act-access-deeplink-user-PPCard-debit"]
        case .hasMultipleCard:
            return ["action": "act-access-deeplink-user-PPCard-multiple"]
        case .progress(let action):
            return ["action": action.rawValue]
        }
    }

    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PicPay Card - Deeplink - \(name)", properties: properties, providers: providers)
    }
    
    public enum ProgressActions: String {
        case inital = "act-access-deeplink-show-continue-registration"
        case confirm = "act-access-deeplink-continue-registration"
        case cancel = "act-access-deeplink-cancel-continue-registration"
    }
}
