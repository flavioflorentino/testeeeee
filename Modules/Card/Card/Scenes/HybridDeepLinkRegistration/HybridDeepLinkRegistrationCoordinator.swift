import UIKit

public enum HybridDeepLinkRegistrationAction: Equatable {
    case debitFlow
    case creditFlow
    case simplifiedDebitFlow
    case dismiss
    case helpCenter(String)
    case cardSettings
    case cardHome
}

public protocol HybridDeepLinkRegistrationCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: HybridDeepLinkRegistrationAction)
    var coordinatorOutput: (HybridDeeplinkFlowCoordinatorOutput) -> Void { get set }
}

public final class HybridDeepLinkRegistrationCoordinator {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies
    public var coordinatorOutput: (HybridDeeplinkFlowCoordinatorOutput) -> Void = { _ in }

    public weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - HybridDeepLinkRegistrationCoordinating
extension HybridDeepLinkRegistrationCoordinator: HybridDeepLinkRegistrationCoordinating {
    public func perform(action: HybridDeepLinkRegistrationAction) {
        switch action {
        case .helpCenter(let deepLinkString):
            if let url = URL(string: deepLinkString) {
                CardSetup.deeplinkInstance?.open(url: url)
            }
        case .cardHome:
            coordinatorOutput(.cardHome)
        case .debitFlow:
            coordinatorOutput(.debit)
        case .creditFlow:
            coordinatorOutput(.credit)
        case .simplifiedDebitFlow:
            coordinatorOutput(.simplifiedDebit)
        case .dismiss:
            coordinatorOutput(.close)
        case .cardSettings:
            coordinatorOutput(.cardSettings)
        }
    }
}
