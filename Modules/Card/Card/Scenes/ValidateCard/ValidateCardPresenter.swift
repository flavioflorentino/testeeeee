import Foundation
import UI
import AssetsKit

public protocol ValidateCardPresenting {
    var title: String { get }
    
    func present()
    func show(errorDescription: String)
    func perform(action: ValidateCardAction)
    func showFeedbackError(url: URL?)
}

public class ValidateCardPresenter: ValidateCardPresenting {
    public let title: String
    private let coordinator: ValidateCardCoordinating
    private typealias Localizable = Strings.ValidateCard.Error.LastDigits
    
    weak var viewController: ValidateCardDisplaying?
    
    init(with type: ValidateCardFactoryType, coordinator: ValidateCardCoordinating) {
        title = type == .activateCard ? Strings.UnlockCard.title : Strings.ForgotPasswordCard.title
        self.coordinator = coordinator
    }
    
    public func present() {
        viewController?.setTitle(title)
    }
    
    public func show(errorDescription: String) {
        viewController?.show(errorDescription: errorDescription)
    }
    
    public func showFeedbackError(url: URL?) {
        let model = StatefulFeedbackViewModel(image: Resources.Illustrations.iluFallingDelivery.image, content: (title: Localizable.title, description: Localizable.subtitle), button: (image: nil, title: Localizable.button))
        guard let url = url else { return }
        coordinator.perform(action: .showFeedbackError(model: model, url: url))
    }
    
    public func perform(action: ValidateCardAction) {
        coordinator.perform(action: action)
    }
}
