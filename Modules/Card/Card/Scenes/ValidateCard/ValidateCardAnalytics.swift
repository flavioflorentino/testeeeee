public protocol ValidateCardAnalyticsEvents {
    func numbersChecked()
    func numbersCheckedError()
}
