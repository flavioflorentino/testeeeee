import UIKit
import UI

public enum ValidateCardAction {
    case confirm
    case close
    case showFeedbackError(model: StatefulFeedbackViewModel, url: URL)
}

extension ValidateCardAction: Equatable {}

public protocol ValidateCardCoordinatorDelegate: AnyObject {
    func didConfirmValidateCard()
    func didCloseValidateCard()
    func showIncorrectDigitFeedbackError(with model: StatefulFeedbackViewModel, url: URL)
}

public extension ValidateCardCoordinatorDelegate {
    func didOpenHelperValidateCard() {}
}

public protocol ValidateCardCoordinating {
    var delegate: ValidateCardCoordinatorDelegate? { get set }
    func perform(action: ValidateCardAction)
}

public final class ValidateCardCoordinator: ValidateCardCoordinating {
    public weak var delegate: ValidateCardCoordinatorDelegate?
    
    public func perform(action: ValidateCardAction) {
        switch action {
        case .confirm:
            delegate?.didConfirmValidateCard()
        case .close:
            delegate?.didCloseValidateCard()
        case let .showFeedbackError(model, url):
            delegate?.showIncorrectDigitFeedbackError(with: model, url: url)
        }
    }
}
