import Foundation

public enum ValidateCardFactoryType {
    case activateCard
    case forgotPasswordCard
}

public enum ValidateCardFactory {
    public static func make(
        _ delegate: ValidateCardCoordinatorDelegate,
        type: ValidateCardFactoryType,
        analyticsContext: ValidateCardAnalyticsEvents
    ) -> ValidateCardViewController {
        let service = ValidateCardService()
        let coordinator = ValidateCardCoordinator()
        let presenter = ValidateCardPresenter(with: type, coordinator: coordinator)
        let container = DependencyContainer()
        let interactor = ValidateCardInteractor(service: service, presenter: presenter, analytics: analyticsContext, dependencies: container)
        let viewController = ValidateCardViewController(interactor: interactor)
        
        coordinator.delegate = delegate
        
        presenter.viewController = viewController
        
        return viewController
    }
}
