import Core
import Foundation

public protocol ValidateCardServicing {
    func validateCard(_ lastDigits: String, then completion: @escaping (Result<Void, ApiError>) -> Void)
}

public final class ValidateCardService: ValidateCardServicing {
    public func validateCard(_ lastDigits: String, then completion: @escaping (Result<Void, ApiError>) -> Void) {
        let api = Api<NoContent>(endpoint: CreditServiceEndpoint.validate(lastDigits))
        api.execute { result in
            DispatchQueue.main.async {
                completion(result.map { _ in })
            }
        }
    }
}
