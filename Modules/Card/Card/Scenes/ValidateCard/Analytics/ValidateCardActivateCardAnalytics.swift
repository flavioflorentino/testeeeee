import AnalyticsModule
import Foundation

struct ValidateCardActivateCardAnalytics: ValidateCardAnalyticsEvents {
    func numbersCheckedError() {
        Analytics.shared.log(UnblockCardEvent.unblockCard(state: .numbersError))
    }

    func numbersChecked() {
        Analytics.shared.log(UnblockCardEvent.unblockCard(state: .numbersChecked))
    }
}
