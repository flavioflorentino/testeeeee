import AnalyticsModule
import Foundation

struct ValidateCardForgotPasswordCardAnalytics: ValidateCardAnalyticsEvents {
    func numbersCheckedError() {
        Analytics.shared.log(ForgotPasswordEvent.unblockCardError)
    }

    func numbersChecked() {
        Analytics.shared.log(ForgotPasswordEvent.unblockCard)
    }
}
