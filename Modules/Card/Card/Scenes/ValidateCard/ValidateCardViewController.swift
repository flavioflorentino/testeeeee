import UI
import UIKit

protocol ValidateCardDisplaying: AnyObject {
    func show(errorDescription: String)
    func setTitle(_ title: String)
}

public class ValidateCardViewController: ViewController<ValidaCardInteracting, UIView> {
    private var digitViewController = CardDigitFormViewController()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        interactor.viewDidLoad()
        digitViewController.delegate = self
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addCloseButton()
    }
    
    private func addCloseButton() {
        if navigationController?.viewControllers.count == 1 {
            navigationItem.rightBarButtonItem = UIBarButtonItem(
                image: Assets.icoClose.image,
                style: .plain,
                target: self,
                action: #selector(didTapCloseNavigation)
            )
        }
    }
    
    @objc
    private func didTapCloseNavigation() {
        interactor.close()
    }

    private func addChildViewController() {
        addChild(digitViewController)
        view.addSubview(digitViewController.view)
        digitViewController.didMove(toParent: self)
    }

    private func addConstraints() {
        digitViewController.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.constraintAllEdges(from: digitViewController.view, to: view)
    }

    override public func configureViews() {
        addChildViewController()
        addConstraints()
        configureNavigationBarButtons(enable: true)
    }

    private func configureNavigationBarButtons(enable: Bool) {
        navigationItem.hidesBackButton = !enable
        if !enable {
            navigationItem.rightBarButtonItem = nil
        }
    }
}

extension ValidateCardViewController: ValidateCardDisplaying {
    public func show(errorDescription: String) {
        digitViewController.show(errorDescription: errorDescription)
    }
    
    public func setTitle(_ title: String) {
        self.title = title
    }
}

extension ValidateCardViewController: DigitFormViewControllerDelegate {
    public func didTapConfirm(with digits: String) {
        view.endEditing(true)
        digitViewController.state = .loading
        interactor.didTapConfirm(with: digits)
    }
    
    public func didTapClose() {
        interactor.close()
    }
}
