import Core
import Foundation
import FeatureFlag
import AnalyticsModule

public protocol ValidaCardInteracting {
    func viewDidLoad()
    func didTapConfirm(with digits: String)
    func close()
}

final class ValidateCardInteractor: ValidaCardInteracting {
    private let service: ValidateCardServicing
    private let presenter: ValidateCardPresenting
    private let analytics: ValidateCardAnalyticsEvents
    private var incorrectDigitErrorCount = 0
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    init(service: ValidateCardServicing, presenter: ValidateCardPresenting, analytics: ValidateCardAnalyticsEvents, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.analytics = analytics
        self.dependencies = dependencies
    }
    
    // MARK: - DigitFormViewModelInputs
    
    func viewDidLoad() {
        presenter.present()
    }
    
    func didTapConfirm(with digits: String) {
        service.validateCard(digits) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success:
                self.presenter.perform(action: .confirm)
                self.analytics.numbersChecked()
            case .failure(let err):
                self.incorrectDigitErrorCount += 1
                let message = err.requestError?.message ?? Strings.UnlockCard.defaultFailureMessage
                if self.incorrectDigitErrorCount >= 3 {
                    self.presenter.showFeedbackError(url: URL(string: self.dependencies.featureManager.text(.articleActivateCard)))
                }
                self.presenter.show(errorDescription: message)
                self.analytics.numbersCheckedError()
            }
        }
    }
    
    func close() {
        presenter.perform(action: .close)
    }
}
