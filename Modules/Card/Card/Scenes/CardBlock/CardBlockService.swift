import Core
import Foundation

public enum CardType: String {
    case physical = "PHYSICAL"
    case digital = "DIGITAL"
}

public protocol CardBlockServicing {
    func cardBlock(type: CardType, reason: CardBlockReplacementReason, password: String, completion: @escaping (Result<Void, Error>) -> Void)
}

public final class CardBlockService: CardBlockServicing {
    public func cardBlock(type: CardType, reason: CardBlockReplacementReason, password: String, completion: @escaping (Result<Void, Error>) -> Void) {
        Api<NoContent>(endpoint: CreditServiceEndpoint.cardBlock(type: type, reason: reason, password)).execute { result in
            DispatchQueue.main.async {
                switch result {
                case .success:
                    completion(.success(()))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
}
