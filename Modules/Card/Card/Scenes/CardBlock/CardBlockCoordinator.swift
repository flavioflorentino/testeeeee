import UI
import UIKit

public protocol CardBlockDelegate: AnyObject {
    func didBlockConfirm(onSuccessAuth: @escaping (String, Bool) -> Void, cancelHandler: @escaping () -> Void)
    func didBlockSuccess()
}

public enum CardBlockAction {
    case confirm(onSuccessAuth: (String, Bool) -> Void, cancelHandler: () -> Void)
    case success
    case close
}

public protocol CardBlockCoordinating {
    var viewController: UIViewController? { get set }
    var delegate: CardBlockDelegate? { get set }
    func perform(action: CardBlockAction)
}

public final class CardBlockCoordinator: CardBlockCoordinating {
    public weak var viewController: UIViewController?
    public weak var delegate: CardBlockDelegate?
    
    public func perform(action: CardBlockAction) {
        switch action {
        case let .confirm(successClosure, cancelClosure):
            delegate?.didBlockConfirm(onSuccessAuth: successClosure, cancelHandler: cancelClosure)
        case .success:
            delegate?.didBlockSuccess()
        case .close:
            viewController?.navigationController?.popViewController(animated: true)
        }
    }
}
