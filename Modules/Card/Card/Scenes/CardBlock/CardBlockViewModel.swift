import AnalyticsModule
import Foundation

protocol CardBlockViewModelInputs {
    func authenticate()
    func success()
    func close()
}

protocol CardBlockViewModelOutputs: AnyObject {
    func didNextAction(_ action: CardBlockAction)
    func didReceiveAnError(_ error: Error)
    func didAuthenticated()
    func didBlockConfirmed()
}

protocol CardBlockViewModelType: AnyObject {
    var inputs: CardBlockViewModelInputs { get }
    var outputs: CardBlockViewModelOutputs? { get set }
}

final class CardBlockViewModel: CardBlockViewModelType, CardBlockViewModelInputs {
    typealias Dependencies = HasAnalytics
    var inputs: CardBlockViewModelInputs { self }
    weak var outputs: CardBlockViewModelOutputs?

    private let service: CardBlockServicing
    private let reason: CardBlockReplacementReason
    private let dependencies: Dependencies

    init(
        service: CardBlockServicing,
        reason: CardBlockReplacementReason,
        dependencies: Dependencies = DependencyContainer()
    ) {
        self.service = service
        self.reason = reason
        self.dependencies = dependencies
    }

    func authenticate() {
        outputs?.didNextAction(.confirm(onSuccessAuth: { [weak self] password, isBiometric  in
            guard let self = self else {
                return
            }
            self.dependencies.analytics.log(CardBlockEvent.authenticated(action: isBiometric ? .fingerprintUsed : .numeralPasswordUsed))
            self.outputs?.didAuthenticated()
            self.service.cardBlock(type: .physical, reason: self.reason, password: password, completion: { result in
                switch result {
                case .success:
                    self.outputs?.didBlockConfirmed()
                case .failure(let error):
                    self.outputs?.didReceiveAnError(error)
                }
            })
            }, cancelHandler: { [weak self] in
                self?.dependencies.analytics.log(CardBlockEvent.authenticated(action: .fingerprintCancel))
            }
        ))
    }

    func close() {
        dependencies.analytics.log(CardBlockEvent.authenticated(action: .notNow))
        outputs?.didNextAction(.close)
    }

    func success() {
        dependencies.analytics.log(CardBlockEvent.confirmBlock(action: .confirm))
        outputs?.didNextAction(.success)
    }
}
