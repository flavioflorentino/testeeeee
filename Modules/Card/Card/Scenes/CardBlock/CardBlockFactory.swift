import Foundation

enum CardBlockFactory {
    static func make(delegate: CardBlockDelegate, reason: CardBlockReplacementReason) -> CardBlockViewController {
        let service: CardBlockServicing = CardBlockService()
        let viewModel: CardBlockViewModelType = CardBlockViewModel(service: service, reason: reason)
        var coordinator: CardBlockCoordinating = CardBlockCoordinator()
        let viewController = CardBlockViewController(viewModel: viewModel, coordinator: coordinator)
        
        coordinator.delegate = delegate
        coordinator.viewController = viewController
        viewModel.outputs = viewController

        return viewController
    }
}
