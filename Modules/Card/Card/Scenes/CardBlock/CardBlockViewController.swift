import UI
import UIKit

final class CardBlockViewController: LegacyViewController<CardBlockViewModelType, CardBlockCoordinating, UIView>, LoadingViewProtocol {
    lazy var loadingView = LoadingView()
    
    private enum Layout {
        static let textTitle = UIFont.systemFont(ofSize: 28, weight: .bold)
        static let textDescription = UIFont.systemFont(ofSize: 16, weight: .light)
        static let textConfirmButton = UIFont.systemFont(ofSize: 16, weight: .semibold)
        static let textCancelButton = UIFont.systemFont(ofSize: 14, weight: .semibold)
    }
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.image = Assets.icoCardBlock.image
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = Layout.textTitle
        label.text = Strings.CardBlock.title
        label.textColor = Palette.ppColorGrayscale600.color
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = Layout.textDescription
        label.text = Strings.CardBlock.description
        label.textColor = Palette.ppColorGrayscale500.color
        return label
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 24
        button.backgroundColor = Palette.ppColorNegative300.color
        button.setTitleColor(Palette.white.color, for: .normal)
        button.titleLabel?.font = Layout.textConfirmButton
        button.addTarget(self, action: #selector(tapConfirm), for: .touchUpInside)
        button.setTitle(Strings.CardBlock.confirm, for: .normal)
        return button
    }()
    
    private lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(Strings.CardBlock.cancel, for: .normal)
        button.addTarget(self, action: #selector(tapClose), for: .touchUpInside)
        button.buttonStyle(LinkButtonStyle())
        return button
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
 
    override func buildViewHierarchy() {
        view.addSubview(contentView)
        contentView.addSubview(imageView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(confirmButton)
        contentView.addSubview(cancelButton)
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.leadingTrailing(equalTo: view,
                                           for: [contentView],
                                           constant: 16)
        NSLayoutConstraint.leadingTrailing(equalTo: contentView,
                                           for: [imageView, titleLabel, confirmButton, cancelButton],
                                           constant: 0)
        
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: contentView.topAnchor),
            imageView.heightAnchor.constraint(equalToConstant: 150),
            imageView.bottomAnchor.constraint(equalTo: titleLabel.topAnchor, constant: -16)
        ])
        
        NSLayoutConstraint.activate([
            titleLabel.bottomAnchor.constraint(equalTo: descriptionLabel.topAnchor, constant: -16),
            descriptionLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Spacing.base01),
            descriptionLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Spacing.base01),
            descriptionLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            descriptionLabel.widthAnchor.constraint(lessThanOrEqualToConstant: 300)
        ])
        
        NSLayoutConstraint.activate([
            confirmButton.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 24),
            confirmButton.heightAnchor.constraint(equalToConstant: 48)
        ])
        
        NSLayoutConstraint.activate([
            cancelButton.topAnchor.constraint(equalTo: confirmButton.bottomAnchor, constant: 16),
            cancelButton.heightAnchor.constraint(equalToConstant: 48),
            cancelButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
        
        NSLayoutConstraint.activate([
            contentView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    @objc
    private func tapConfirm() {
        viewModel.inputs.authenticate()
    }
    
    @objc
    private func tapClose() {
        viewModel.inputs.close()
    }
}

// MARK: View Model Outputs
extension CardBlockViewController: CardBlockViewModelOutputs {
    func didNextAction(_ action: CardBlockAction) {
        coordinator.perform(action: action)
    }
    
    func didReceiveAnError(_ error: Error) {
        stopLoadingView()
        navigationItem.hidesBackButton = false
        
        let popupViewController = PopupViewController(title: Strings.Default.ops,
                                                      description: Strings.Default.errorLoadingInformation,
                                                      preferredType: .image(Assets.icoSadEmoji.image))
        
        let okAction = PopupAction(title: Strings.Default.iGotIt, style: .fill)
        popupViewController.addAction(okAction)
        
        showPopup(popupViewController)
    }
    
    func didAuthenticated() {
        navigationItem.hidesBackButton = true
        startLoadingView()
    }
    
    func didBlockConfirmed() {
        viewModel.inputs.success()
        stopLoadingView()
        navigationItem.hidesBackButton = false
    }
}

extension CardBlockViewController: StyledNavigationDisplayable {
    var prefersLargeTitle: Bool {
        false
    }
}
