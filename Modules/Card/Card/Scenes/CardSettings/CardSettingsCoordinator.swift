import UI
import UIKit

public protocol CardSettingsDelegate: AnyObject {
    func didOpenBlockCard(_ settingsData: CardSettingsData)
    func didOpenDueDayCard()
    func didOpenLimitCard()
    func didOpenChangePassword()
    func didOpenCardReplacement(_ settingsData: CardSettingsData)
    func didOpenDollarExchangeRate()
    func didOpenVirtualCard(_ settingsData: CardSettingsData)
}

public enum CardSettingsAction {
    case close
    case changePassword
    case blockCard
    case dueDay
    case limitCard
    case replacementRequest
    case dollarExchangeRate
    case virtualCard
}

public protocol CardSettingsCoordinating {
    var viewController: UIViewController? { get set }
    var delegate: CardSettingsDelegate? { get set }
    func perform(action: CardSettingsAction, settingsData: CardSettingsData)
}

public final class CardSettingsCoordinator: CardSettingsCoordinating {
    public var viewController: UIViewController?
    public weak var delegate: CardSettingsDelegate?
    
    public func perform(action: CardSettingsAction, settingsData: CardSettingsData) {
        switch action {
        case .close:
            viewController?.dismiss(animated: true)
        case .changePassword:
            delegate?.didOpenChangePassword()
        case .blockCard:
            delegate?.didOpenBlockCard(settingsData)
        case .dueDay:
            delegate?.didOpenDueDayCard()
        case .limitCard:
            delegate?.didOpenLimitCard()
        case .replacementRequest:
            delegate?.didOpenCardReplacement(settingsData)
        case .dollarExchangeRate:
            delegate?.didOpenDollarExchangeRate()
        case .virtualCard:
            delegate?.didOpenVirtualCard(settingsData)
        }
    }
}
