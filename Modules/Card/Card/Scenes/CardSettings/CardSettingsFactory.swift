import Foundation

public enum CardSettingsFactory {
    public static func make(delegate: CardSettingsDelegate) -> CardSettingsViewController {
        let service: CardSettingsServiceProtocol = CardSettingsService()
        let viewModel: CardSettingsViewModelType = CardSettingsViewModel(service: service)
        var coordinator: CardSettingsCoordinating = CardSettingsCoordinator()
        let viewController = CardSettingsViewController(viewModel: viewModel, coordinator: coordinator)
        
        coordinator.delegate = delegate
        coordinator.viewController = viewController
        viewModel.outputs = viewController
        return viewController
    }
}
