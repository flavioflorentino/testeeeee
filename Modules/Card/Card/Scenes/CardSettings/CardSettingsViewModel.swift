import AnalyticsModule
import FeatureFlag
import Foundation
import Core
import UI
public enum CardItem: Int {
    case changePassword = 0
    case cardBlocked
    case dueDay
    case limitCard
    case replacement
    case dollarExchangeRate
    case virtualCard
}

public protocol CardSettingsViewModelInputs {
    func loadCreditStatus()
    func close()
    func didSelectedItem(_ item: Int)
}

public protocol CardSettingsViewModelOutputs: AnyObject {
    func didNextAction(_ action: CardSettingsAction, settingsData: CardSettingsData)
    func didReceiveAnError(_ error: ApiError)
    func didReceiveCardStatus(isCardEnable: Bool, sections: [Section<String, CardOptionCellPresenterProtocol>])
}

public protocol CardSettingsViewModelType: AnyObject {
    var inputs: CardSettingsViewModelInputs { get }
    var outputs: CardSettingsViewModelOutputs? { get set }
}

public final class CardSettingsViewModel: CardSettingsViewModelType, CardSettingsViewModelInputs {
    public var inputs: CardSettingsViewModelInputs { self }
    public weak var outputs: CardSettingsViewModelOutputs?
    private var settingsItems: [CardItem] = []
    private let service: CardSettingsServiceProtocol
    private var creditAccount: CreditAccount?
    private let container = DependencyContainer()
    
    private var settingsData: CardSettingsData {
        guard let account = creditAccount else {
            let buttons = CardSettingsButtons(
                cardReplacement: CardReplacementButton(type: .none, action: .none),
                virtualCard: VirtualCardButton(action: .none, newFeature: false)
            )
            return CardSettingsData(cardType: .none, buttons: buttons)
        }
        
        let picpayCardType: PicPayCardType = account.creditStatus != nil ? .multiple : .debit
        let settingsButtons = CardSettingsButtons(
            cardReplacement: account.settings.replacementButton,
            virtualCard: account.settings.virtualCardButton
        )
        
        return CardSettingsData(cardType: picpayCardType, buttons: settingsButtons)
    }
    
    public init(service: CardSettingsServiceProtocol) {
        self.service = service
    }
    
    public func close() {
        outputs?.didNextAction(.close, settingsData: settingsData)
    }

    public func didSelectedItem(_ item: Int) {
        let cardItem = settingsItems[item]
        trackingAnalytics(item: cardItem)

        switch cardItem {
        case .changePassword:
            outputs?.didNextAction(.changePassword, settingsData: settingsData)
        case .cardBlocked:
            outputs?.didNextAction(.blockCard, settingsData: settingsData)
        case .dueDay:
            outputs?.didNextAction(.dueDay, settingsData: settingsData)
        case .limitCard:
            outputs?.didNextAction(.limitCard, settingsData: settingsData)
        case .replacement:
            outputs?.didNextAction(.replacementRequest, settingsData: settingsData)
        case .dollarExchangeRate:
            outputs?.didNextAction(.dollarExchangeRate, settingsData: settingsData)
        case .virtualCard:
            outputs?.didNextAction(.virtualCard, settingsData: settingsData)
        }
    }
    
    public func loadCreditStatus() {
        service.creditStatus { [weak self] result in
            switch result {
            case .success(let value):
                self?.creditAccount = value
                self?.makePresenters(with: value)
            case .failure(let error):
                self?.outputs?.didReceiveAnError(error)
            }
        }
    }

    internal func isCardEnabled(physicalCardStatusType: CreditAccount.PhysicalCardStatusType) -> Bool {
        (physicalCardStatusType != .blocked) && (physicalCardStatusType != .waitingActivation)
    }
    
    private func makePresenters(with creditAccount: CreditAccount) {
        var cells: [CardOptionCellPresenter] = []
        let cardEnabled = isCardEnabled(physicalCardStatusType: creditAccount.physicalCardStatus)
        
        if container.featureManager.isActive(.opsVirtualCard), settingsData.buttons.virtualCard.action != .none {
            settingsItems.append(.virtualCard)
            cells.append(
                CardOptionCellPresenter(
                    image: Assets.icoSettingsVirtualcard.image,
                    title: Strings.VirtualCard.CardSettings.button,
                    detail: Strings.VirtualCard.CardSettings.description,
                    detailHighlight: false,
                    isEnabled: true,
                    isNewFeature: settingsData.buttons.virtualCard.newFeature
                )
            )
        }
        
        settingsItems.append(.changePassword)
        cells.append(
            CardOptionCellPresenter(
                image: Assets.icoSettingsPassword.image,
                title: Strings.CardSettings.changePassword,
                isEnabled: cardEnabled
            )
        )
        
        settingsItems.append(.cardBlocked)
        cells.append(
            CardOptionCellPresenter(
                image: Assets.icoSettingsBlock.image,
                title: Strings.CardSettings.cardBlock,
                isEnabled: creditAccount.settings.physicalCardBlockEnabled
            )
        )
        
        if creditAccount.settings.cardCreditSettingsEnabled {
            settingsItems.append(.dueDay)
            settingsItems.append(.limitCard)
            cells.append(contentsOf: [
                CardOptionCellPresenter(
                    image: Assets.icoSettingsDuedate.image,
                    title: Strings.CardSettings.validateDate,
                    detail: Strings.CardSettings.validateDay(creditAccount.dueDay ?? 0)
                ),
                CardOptionCellPresenter(
                    image: Assets.icoSettingsLimit.image,
                    title: Strings.CardSettings.myLimit,
                    detail: creditAccount.availableCredit?.toCurrencyString() ?? ""
                )
            ])
        }
        
        if container.featureManager.isActive(.opsCardReplacement), settingsData.buttons.cardReplacement.type != .none {
            settingsItems.append(.replacement)
            cells.append(
                CardOptionCellPresenter(
                    image: Assets.icoSettingsReplacement.image,
                    title: settingsData.buttons.cardReplacement.type == .requested ?
                           Strings.CardSettings.CardReplacement.requestedTitle :
                           Strings.CardSettings.CardReplacement.title,
                    detail: settingsData.buttons.cardReplacement.type == .requested ?
                            Strings.CardSettings.CardReplacement.statusSubtitle :
                            nil,
                    detailHighlight: false
                )
            )
        }
        
        if container.featureManager.isActive(.opsDollarExchangeRate) {
            settingsItems.append(.dollarExchangeRate)
            cells.append(
                CardOptionCellPresenter(
                    image: Assets.icoSettingsDollar.image,
                    title: Strings.DolarExchangeRate.settings,
                    isEnabled: true
                )
            )
        }
        
        outputs?.didReceiveCardStatus(isCardEnable: cardEnabled, sections: [Section(items: cells)])
    }
    
    private func trackingAnalytics(item: CardItem) {
        switch item {
        case .changePassword:
            container.analytics.log(CardSettingsEvent.action(action: .changePassword))
        case .cardBlocked:
            container.analytics.log(CardSettingsEvent.action(action: .blockCard))
        case .dueDay:
            container.analytics.log(CardSettingsEvent.action(action: .dueDate))
        case .limitCard:
            container.analytics.log(CardSettingsEvent.action(action: .myLimit))
        case .replacement:
            switch settingsData.buttons.cardReplacement.type {
            case .requestable:
                container.analytics.log(CardSettingsEvent.action(action: .replacementRequestable))
            case .requested:
                container.analytics.log(CardSettingsEvent.action(action: .replacementRequested))
            case .none:
                return
            }
        case .dollarExchangeRate:
            container.analytics.log(CardSettingsEvent.action(action: .changePassword))
        case .virtualCard:
            container.analytics.log(CardSettingsEvent.action(action: .virtualCard))
        }
    }
}
