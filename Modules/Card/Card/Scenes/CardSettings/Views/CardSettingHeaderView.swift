import UI
import UIKit

public class CardSettingHeaderView: UIView {
    private enum Layout {
        static let minCardSize: CGFloat = (UIScreen.main.bounds.height / 3)
    }
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Palette.ppColorBranding300.color
        return view
    }()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    public var isCardEnable: Bool = false {
        didSet {
            updateStateCard()
        }
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func updateStateCard() {
        imageView.image = isCardEnable ? Assets.cardFront.image : Assets.cardFrontBlocked.image
    }
}

extension CardSettingHeaderView: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(containerView)
        addSubview(imageView)
    }
    
    public func setupConstraints() {
        NSLayoutConstraint.leadingTrailing(equalTo: self,
                                           for: [containerView, imageView])

        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: topAnchor),
            containerView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -56)
        ])
        
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(lessThanOrEqualTo: topAnchor),
            imageView.bottomAnchor.constraint(equalTo: bottomAnchor),
            imageView.heightAnchor.constraint(equalToConstant: Layout.minCardSize)
        ])
    }
    
    public func configureViews() {
        backgroundColor = .clear
    }
    
    private func setupView() {
        buildViewHierarchy()
        configureViews()
        setupConstraints()
    }
}
