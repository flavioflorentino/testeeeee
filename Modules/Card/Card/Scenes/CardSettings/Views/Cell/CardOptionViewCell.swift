import UI
import UIKit

public class CardOptionViewCell: UITableViewCell {
    private enum Layout {
        static let textRegular = UIFont.systemFont(ofSize: 16, weight: .regular)
        static let textRegularSmall = Typography.caption(.default).font()
        static let textMedium = UIFont.systemFont(ofSize: 14, weight: .medium)
    }
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Palette.ppColorGrayscale000.color
        view.layer.cornerRadius = 5
        view.layer.shadowColor = Palette.ppColorGrayscale600.color.withAlphaComponent(0.5).cgColor
        view.layer.shadowOpacity = 0.2
        view.layer.shadowOffset = CGSize(width: 0, height: 2)
        view.layer.shadowRadius = 2
        return view
    }()
    
    private lazy var optionImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Layout.textRegular
        label.textColor = Palette.ppColorGrayscale500.color
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Layout.textMedium
        label.textColor = Palette.ppColorBranding300.color
        return label
    }()
    
    private lazy var arrowImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = .clear
        return imageView
    }()
    
    private lazy var flagLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle()).with(\.textColor, Colors.white.lightColor).with(\.textAlignment, .center)
        label.text = Strings.VirtualCard.CardSettings.flag
        label.backgroundColor = Colors.neutral400.color
        label.clipsToBounds = true
        label.layer.cornerRadius = Spacing.base00
        label.isHidden = true
        return label
    }()
    
    private var titleConstraintTopAnchor: NSLayoutConstraint?
    
    public var presenter: CardOptionCellPresenterProtocol? {
        didSet {
            update()
        }
    }
    
    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func prepareForReuse() {
        super.prepareForReuse()
        optionImageView.image = nil
        titleLabel.text = nil
        descriptionLabel.text = nil
    }
    
    private func update() {
        guard let presenter = presenter else {
            return
        }
        
        optionImageView.image = presenter.image
        titleLabel.text = presenter.title
        descriptionLabel.text = presenter.detail
        descriptionLabel.font = presenter.detailHighlight ? Layout.textMedium : Layout.textRegularSmall
        arrowImageView.image = presenter.rightImage
        configureView(isDescription: presenter.detail != nil)
        configureCell(presenter)
    }
    
    private func configureView(isDescription: Bool) {
        guard isDescription else {
            return
        }
        titleConstraintTopAnchor?.constant = 16
    }
    
    private func configureCell(_ presenter: CardOptionCellPresenterProtocol) {
        isUserInteractionEnabled = presenter.isEnabled
        if isUserInteractionEnabled {
            arrowImageView.image = Assets.icoArrowRight.image
            titleLabel.textColor = Palette.ppColorGrayscale500.color
            descriptionLabel.textColor = presenter.detailHighlight ? Palette.ppColorBranding300.color : Palette.ppColorGrayscale400.color
            flagLabel.isHidden = !presenter.isNewFeature
            optionImageView.opacity = Opacity.Style.full
        } else {
            arrowImageView.image = Assets.icoArrowRightDisable.image
            titleLabel.textColor = Palette.ppColorGrayscale300.color
            descriptionLabel.textColor = presenter.detailHighlight ? Palette.ppColorBranding300.color.withAlphaComponent(0.5) : Palette.ppColorGrayscale300.color
            flagLabel.isHidden = !presenter.isNewFeature
            optionImageView.opacity = Opacity.Style.light
        }
    }
}

extension CardOptionViewCell: ViewConfiguration {
    public func buildViewHierarchy() {        
        addSubview(containerView)
        containerView.addSubview(optionImageView)
        containerView.addSubview(titleLabel)
        containerView.addSubview(descriptionLabel)
        containerView.addSubview(arrowImageView)
        containerView.addSubview(flagLabel)
    }
    
    public func setupConstraints() {
        NSLayoutConstraint.leadingTrailing(equalTo: self,
                                           for: [containerView],
                                           constant: 16)
        
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: topAnchor, constant: 4),
            containerView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -4)
        ])
        
        NSLayoutConstraint.activate([
            optionImageView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 16),
            optionImageView.widthAnchor.constraint(equalToConstant: 40),
            optionImageView.heightAnchor.constraint(equalToConstant: 40),
            optionImageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 16),
            optionImageView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -16)
        ])
        
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: optionImageView.trailingAnchor, constant: 8),
            titleLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -16)
        ])
        
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 4),
            descriptionLabel.leadingAnchor.constraint(equalTo: optionImageView.trailingAnchor, constant: 8),
            descriptionLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -16),
            descriptionLabel.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -16)
        ])
        
        NSLayoutConstraint.activate([
            arrowImageView.widthAnchor.constraint(equalToConstant: 16),
            arrowImageView.heightAnchor.constraint(equalToConstant: 16),
            arrowImageView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -16),
            arrowImageView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor)
        ])
        
        flagLabel.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.trailing.equalTo(arrowImageView.compatibleSafeArea.leading).offset(-Spacing.base03)
            $0.width.equalTo(Spacing.base08)
            $0.height.equalTo(Spacing.base03)
        }
        
        titleConstraintTopAnchor = titleLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 24)
        titleConstraintTopAnchor?.priority = .defaultHigh
        titleConstraintTopAnchor?.isActive = true
        
        let titleBottomConstraint = titleLabel.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -24)
        titleBottomConstraint.priority = .defaultHigh
        titleBottomConstraint.isActive = true
    }
    
    public func configureViews() {
        backgroundColor = .clear
        selectionStyle = .none
    }
    
    private func setupView() {
        buildViewHierarchy()
        configureViews()
        setupConstraints()
    }
}
