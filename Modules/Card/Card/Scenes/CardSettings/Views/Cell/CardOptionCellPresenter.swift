import UIKit

public protocol CardOptionCellPresenterProtocol {
    var isEnabled: Bool { get }
    var image: UIImage { get }
    var title: String { get }
    var detail: String? { get }
    var detailHighlight: Bool { get }
    var rightImage: UIImage? { get }
    var isNewFeature: Bool { get }
}

public struct CardOptionCellPresenter: CardOptionCellPresenterProtocol {
    public var isEnabled: Bool
    public var image: UIImage
    public var title: String
    public var detail: String?
    public var detailHighlight: Bool
    public var rightImage: UIImage?
    public var isNewFeature: Bool
    
    public init(image: UIImage, title: String, detail: String? = nil, detailHighlight: Bool = true, isEnabled: Bool = true, rightImage: UIImage? = nil, isNewFeature: Bool = false) {
        self.isEnabled = isEnabled
        self.image = image
        self.title = title
        self.detail = detail
        self.detailHighlight = detailHighlight
        self.rightImage = rightImage ?? Assets.icoArrowRight.image
        self.isNewFeature = isNewFeature
    }
}
