import UI
import UIKit
import Core

public class CardSettingsViewController: LegacyViewController<CardSettingsViewModelType, CardSettingsCoordinating, UIView> {
    private enum Layout {
        static let headerSize: CGFloat = (UIScreen.main.bounds.height / 3)
        static let maxHeaderSize: CGFloat = (UIScreen.main.bounds.height / 2.3)
        static let estimatedRowHeight: CGFloat = 64
        static let marginHeaderTableView: CGFloat = 24
        static let contentInset = UIEdgeInsets(top: Layout.headerSize + marginHeaderTableView, left: 0.0, bottom: 0.0, right: 0.0)
    }
    
    private lazy var headerView: CardSettingHeaderView = {
        let view = CardSettingHeaderView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.register(CardOptionViewCell.self, forCellReuseIdentifier: String(describing: CardOptionViewCell.self))
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = Layout.estimatedRowHeight
        tableView.contentInset = Layout.contentInset
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        return tableView
    }()
    
    private var dataSource: TableViewHandler<String, CardOptionCellPresenterProtocol, CardOptionViewCell>?
    private var headerViewHeightConstraint: NSLayoutConstraint?
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        statusBarStyle
    }
    
    // MARK: - Methods Lifecycle
    override public func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshData()
        viewModel.inputs.loadCreditStatus()
        addCloseButton()
        guard navigationController?.navigationBar.isHidden ?? true else { return }
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Fix for iOS 12
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: Colors.white.lightColor]
        navigationController?.navigationBar.setNeedsLayout()
        navigationController?.navigationBar.layoutIfNeeded()
    }
    
    // MARK: - Methods Helpers
    override public func buildViewHierarchy() {
        view.addSubview(headerView)
        view.addSubview(tableView)
    }
    
    override public func setupConstraints() {
        NSLayoutConstraint.leadingTrailing(equalTo: view,
                                           for: [headerView, tableView])
        
        NSLayoutConstraint.activate([
            headerView.topAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.topAnchor),
            tableView.topAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.bottomAnchor)
        ])
        
        headerViewHeightConstraint = headerView.heightAnchor.constraint(equalToConstant: Layout.headerSize)
        headerViewHeightConstraint?.isActive = true
    }
    
    override public func configureViews() {
        title = Strings.CardSettings.configuring
        view.backgroundColor = Palette.ppColorGrayscale100.color
    }
    
    // MARK: - Private Methods Helper
    private func addCloseButton() {
        let barButtonItem = UIBarButtonItem(
            image: Assets.icoClose.image,
            style: .plain,
            target: self,
            action: #selector(tapClose)
        )
        if navigationController?.presentingViewController != nil {
            navigationItem.rightBarButtonItem = barButtonItem
        }
    }
    
    @objc
    private func tapClose() {
        viewModel.inputs.close()
    }
}

// MARK: View Model Outputs
extension CardSettingsViewController: CardSettingsViewModelOutputs {
    public func didNextAction(_ action: CardSettingsAction, settingsData: CardSettingsData) {
        coordinator.perform(action: action, settingsData: settingsData)
    }
    
    public func didReceiveAnError(_ error: ApiError) {
        endState(model: StatefulErrorViewModel(error))
    }
    
    public func didReceiveCardStatus(isCardEnable: Bool, sections: [Section<String, CardOptionCellPresenterProtocol>]) {
        dataSource = TableViewHandler(data: sections, cellType: CardOptionViewCell.self, configureCell: { _, model, cell in
            cell.presenter = model
        })
        
        headerView.isCardEnable = isCardEnable
        tableView.dataSource = dataSource
        tableView.reloadData()
        endState()
    }
    
    public func refreshData() {
        beginState()
        viewModel.inputs.loadCreditStatus()
    }
}
// MARK: - UITableViewDelegate
extension CardSettingsViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.inputs.didSelectedItem(indexPath.row)
    }
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentY = (Layout.headerSize - Layout.marginHeaderTableView) - (scrollView.contentOffset.y + Layout.headerSize)
        let height = min(max(contentY, 0), Layout.maxHeaderSize - Layout.marginHeaderTableView)
        headerViewHeightConstraint?.constant = height
        view.layoutIfNeeded()
    }
}

extension CardSettingsViewController: StyledNavigationDisplayable {
    public var navigationBarStyle: DefaultNavigationStyle {
        LightGreenNavigationStyle()
    }
    
    var statusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .lightContent
        }
        return .default
    }
}
// MARK: - StatefulTransitionViewing
extension CardSettingsViewController: StatefulTransitionViewing {
    public func didTryAgain() {
        refreshData()
    }
}
