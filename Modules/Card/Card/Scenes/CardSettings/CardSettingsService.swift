import Foundation
import Core

public typealias CompletionCardSettingsCreditAccount = (Result<CreditAccount, ApiError>) -> Void

public protocol CardSettingsServiceProtocol {
    func creditStatus(completion: @escaping CompletionCardSettingsCreditAccount)
}

public final class CardSettingsService: CardSettingsServiceProtocol {
    public func creditStatus(completion: @escaping CompletionCardSettingsCreditAccount) {
        Api<CreditAccount>(endpoint: CreditServiceEndpoint.creditAccount).execute { result in
            DispatchQueue.main.async {
                let mappedResult = result
                    .mapError { $0 as ApiError }
                    .map { $0.model }
                
                completion(mappedResult)
            }
        }
    }
}
