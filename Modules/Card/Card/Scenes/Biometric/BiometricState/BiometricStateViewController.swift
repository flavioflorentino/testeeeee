import UI
import UIKit

private extension BiometricStateViewController.Layout {
    enum Size {
        static let image: CGFloat = 100
    }
}

public final class BiometricStateViewController: ViewController<BiometricStateInteracting, UIView> {
    fileprivate enum Layout { }
    private typealias Localizable = Strings.BiometricStateViewController
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Spacing.base04
        stackView.distribution = .equalSpacing
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var imageView: UIImageView = {
        let image = UIImageView(image: Assets.icoNewCheck.image)
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
            .with(\.textAlignment, .center)
        label.text = Localizable.title
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale750())
            .with(\.textAlignment, .center)
        label.text = Localizable.description
        return label
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Strings.Default.iGotIt, for: .normal)
        button.addTarget(self, action: #selector(closePressed), for: .touchUpInside)
        return button
    }()

    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBarAppearance()
    }
    
    private func updateNavigationBarAppearance() {
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.isTranslucent = true
        addCloseButton()
    }
    
    private func addCloseButton() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: Assets.icoClose.image,
            style: .plain,
            target: self,
            action: #selector(closePressed)
        )
    }
    
    override public func buildViewHierarchy() {
        stackView.addArrangedSubview(imageView)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(descriptionLabel)
        view.addSubviews(stackView, confirmButton)
    }
    
    override public func setupConstraints() {
        imageView.snp.makeConstraints {
            $0.height.width.equalTo(Layout.Size.image)
        }
        
        stackView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.centerX.centerY.equalToSuperview()
        }
        
        confirmButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).inset(Spacing.base02)
        }
    }

    override public func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    @objc
    private func closePressed() {
        interactor.close()
    }
}
