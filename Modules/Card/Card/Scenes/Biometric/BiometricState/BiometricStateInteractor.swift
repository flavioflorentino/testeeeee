import Foundation

public protocol BiometricStateInteracting: AnyObject {
    func close()
}

final class BiometricStateInteractor {
    private let presenter: BiometricStatePresenting

    init(presenter: BiometricStatePresenting) {
        self.presenter = presenter
    }
}

// MARK: - BiometricStateInteracting
extension BiometricStateInteractor: BiometricStateInteracting {
    func close() {
        presenter.didNextStep(action: .close)
    }
}
