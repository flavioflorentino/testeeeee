import Foundation

protocol BiometricStatePresenting: AnyObject {
    func didNextStep(action: BiometricStateAction)
}

final class BiometricStatePresenter {
    private let coordinator: BiometricStateCoordinating

    init(coordinator: BiometricStateCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - BiometricStatePresenting
extension BiometricStatePresenter: BiometricStatePresenting {
    func didNextStep(action: BiometricStateAction) {
        coordinator.perform(action: action)
    }
}
