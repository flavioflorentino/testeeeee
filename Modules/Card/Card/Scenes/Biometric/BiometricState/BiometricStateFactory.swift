import Foundation
import UIKit

public enum BiometricStateFactory {
    public static func make() -> BiometricStateViewController {
        let coordinator: BiometricStateCoordinating = BiometricStateCoordinator()
        let presenter: BiometricStatePresenting = BiometricStatePresenter(coordinator: coordinator)
        let interactor = BiometricStateInteractor(presenter: presenter)
        let viewController = BiometricStateViewController(interactor: interactor)

        coordinator.viewController = viewController
        
        return viewController
    }
}
