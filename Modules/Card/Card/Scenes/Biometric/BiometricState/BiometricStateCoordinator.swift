import UIKit

enum BiometricStateAction {
    case close
}

protocol BiometricStateCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: BiometricStateAction)
}

// MARK: - BiometricStateCoordinating
final class BiometricStateCoordinator {
    weak var viewController: UIViewController?
}

extension BiometricStateCoordinator: BiometricStateCoordinating {
    func perform(action: BiometricStateAction) {
        viewController?.dismiss(animated: true)
    }
}
