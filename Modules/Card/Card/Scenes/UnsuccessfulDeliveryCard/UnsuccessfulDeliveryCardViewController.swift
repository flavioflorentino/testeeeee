import UI
import UIKit

public protocol UnsuccessfulDeliveryCardDisplaying: AnyObject {
    func displayInfo(model: StatefulFeedbackViewModel, hasFooter: Bool)
    func displayLoadState()
    func displayErrorState(model: StatefulFeedbackViewModel)
}

public final class UnsuccessfulDeliveryCardViewController: ViewController<UnsuccessfulDeliveryCardInteracting, UnsuccessfulDeliveryCardView> {
     override public func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(false, animated: true)
        rootView.delegate = self
        interactor.loadInfo()
    }
}

// MARK: - UnsuccessfulDeliveryCardDisplaying
extension UnsuccessfulDeliveryCardViewController: UnsuccessfulDeliveryCardDisplaying {
    public func displayErrorState(model: StatefulFeedbackViewModel) {
        endState(animated: true, model: model)
    }
    
    public func displayLoadState() {
        beginState()
    }
    
    public func displayInfo(model: StatefulFeedbackViewModel, hasFooter: Bool) {
        endState()
        rootView.setupDataInView(model, hasfooterView: hasFooter)
    }
}

// MARK: - StatefulTransitionViewing
extension UnsuccessfulDeliveryCardViewController: StatefulTransitionViewing {
    public func statefulViewForError() -> StatefulViewing {
        StatefulFeedbackView()
    }
    
    public func didTryAgain() {
        interactor.loadInfo()
    }
    
    public func didTapSecondaryButton() {
        interactor.dismiss()
    }
}

// MARK: - CardNavigationDisplayable
extension UnsuccessfulDeliveryCardViewController: StyledNavigationDisplayable {
    public var navigationBarStyle: DefaultNavigationStyle {
        ClearNavigationStyle(prefersLargeTitle: false)
    }
}

extension UnsuccessfulDeliveryCardViewController: UnsuccessfulDeliveryCardViewProtocol {
    public func openFaqHelpCenter() {
        interactor.openHelpcenter()
    }
    
    public func didPressLink() {
        interactor.openFaqChangeAddress()
    }
    
    public func didTapPrimaryButton() {
        interactor.openFaqDeliveryFailed()
    }
}
