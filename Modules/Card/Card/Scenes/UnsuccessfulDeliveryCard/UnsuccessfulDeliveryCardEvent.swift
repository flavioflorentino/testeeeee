import Foundation
import AnalyticsModule

public enum UnsuccessfulDeliveryCardEvent: AnalyticsKeyProtocol {
    case unsuccessAttempt(action: UnsuccessAttemptAction)
    case unsuccessAttemptDebit(action: UnsuccessAttemptAction)
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase, .eventTracker]
    }
    
    private var name: String {
        switch self {
        case .unsuccessAttempt:
            return "- Card Tracking - Unsuccessful Attempt"
        case .unsuccessAttemptDebit:
            return "- Card Tracking - Flash Safekeeping - Debit"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .unsuccessAttemptDebit(let action), .unsuccessAttempt(let action):
            return ["action": action.rawValue]
        }
    }

    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PicPay Card \(name)", properties: properties, providers: providers)
    }
    
    public enum UnsuccessAttemptAction: String {
        case helpCenter = "act-help-center"
        case changeAddress = "act-change-address"
        case resendCard = "act-request-resend-card"
    }
}
