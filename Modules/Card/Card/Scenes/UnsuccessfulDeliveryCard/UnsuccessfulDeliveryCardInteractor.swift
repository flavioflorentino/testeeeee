import Foundation
import FeatureFlag
import Core
import AnalyticsModule

public protocol UnsuccessfulDeliveryCardInteracting: AnyObject {
    func loadInfo()
    func openFaqDeliveryFailed()
    func dismiss()
    func openFaqChangeAddress()
    func openHelpcenter()
}

public final class UnsuccessfulDeliveryCardInteractor {
    typealias Dependencies = HasAnalytics & HasFeatureManager
    private let dependencies: Dependencies
    private let cardType: CardTrackingType
    private let presenter: UnsuccessfulDeliveryCardPresenting
    private let service: RequestPhysicalCardLoadingServicing
    
    init(presenter: UnsuccessfulDeliveryCardPresenting, cardType: CardTrackingType, dependencies: Dependencies, service: RequestPhysicalCardLoadingServicing) {
        self.presenter = presenter
        self.dependencies = dependencies
        self.service = service
        self.cardType = cardType
    }
}

// MARK: - UnsuccessfulDeliveryCardInteracting
extension UnsuccessfulDeliveryCardInteractor: UnsuccessfulDeliveryCardInteracting {
    public func openHelpcenter() {
        handlerDeeplink(value: dependencies.featureManager.text(.faqDeliveryCardFailed))
        handlerAnalytics(action: .helpCenter)
    }
    
    public func dismiss() {
        presenter.presentNextStep(action: .close)
    }
    
    public func openFaqDeliveryFailed() {
        handlerDeeplink(value: dependencies.featureManager.text(.faqDeliveryCardFailed))
        cardType == .debit ? handlerAnalytics(action: .resendCard) : handlerAnalytics(action: .helpCenter)
    }
    
    public func openFaqChangeAddress() {
        handlerDeeplink(value: dependencies.featureManager.text(.faqChangeCardAddress))
        handlerAnalytics(action: .changeAddress)
    }
    
    private func handlerDeeplink(value: String) {
        if let url = URL(string: value) {
            presenter.presentNextStep(action: .link(url: url))
        }
    }
    
    private func handlerAnalytics(action: UnsuccessfulDeliveryCardEvent.UnsuccessAttemptAction) {
        switch cardType {
        case .debit:
            dependencies.analytics.log(UnsuccessfulDeliveryCardEvent.unsuccessAttemptDebit(action: action))
        default:
            dependencies.analytics.log(UnsuccessfulDeliveryCardEvent.unsuccessAttempt(action: action))
        }
    }
    
    public func idealScenario(address: HomeAddress?) {
        guard
            let url = URL(string: dependencies.featureManager.text(.faqChangeCardAddress)),
            let address = address?.completeAddress
        else {
            presenter.presentErroState(.serverError)
            return
        }
        presenter.presentInfoAddress(UnsuccessfulDeliveryTypeCardModel.typeModel(type: cardType).getModel(deliveryAddress: address, url: url), hasFooter: cardType == .debit)
    }
    
    public func loadInfo() {
        presenter.presentLoadState()
        service.getDeliveryAddress { [weak self] result in
            switch result {
            case .success(let address):
                self?.idealScenario(address: address)
            case .failure(let error):
                self?.presenter.presentErroState(error)
            }
        }
    }
}
