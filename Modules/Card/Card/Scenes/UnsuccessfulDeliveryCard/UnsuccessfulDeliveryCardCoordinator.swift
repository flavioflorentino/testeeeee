import UIKit

public enum UnsuccessfulDeliveryCardAction: Equatable {
    case close
    case link(url: URL)
}

public protocol UnsuccessfulDeliveryCardCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: UnsuccessfulDeliveryCardAction)
}

public final class UnsuccessfulDeliveryCardCoordinator {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    public weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - UnsuccessfulDeliveryCardCoordinating
extension UnsuccessfulDeliveryCardCoordinator: UnsuccessfulDeliveryCardCoordinating, HelpcenterDeeplinkPresentable {
    public func perform(action: UnsuccessfulDeliveryCardAction) {
        switch action {
        case .link(let url):
            open(url: url)
        case .close:
            viewController?.navigationController?.popViewController(animated: true)
        }
    }
    
    public func open(url: URL) {
        openHelpCenter(with: url)
    }
}
