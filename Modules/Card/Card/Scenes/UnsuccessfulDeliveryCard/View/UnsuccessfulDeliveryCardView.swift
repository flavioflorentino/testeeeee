import UI
import UIKit

extension UnsuccessfulDeliveryCardView.Layout {
    enum Size: CGFloat {
        case regularImage = 144
        case smallImage = 100
    }
}
public protocol UnsuccessfulDeliveryCardViewProtocol: AnyObject {
    func didPressLink()
    func didTapPrimaryButton()
    func openFaqHelpCenter()
}

public class UnsuccessfulDeliveryCardView: UIView {
    fileprivate enum Layout {}
    
    public weak var delegate: UnsuccessfulDeliveryCardViewProtocol?
    private lazy var isSmallScreen = UIScreen.main.bounds.height < 570
    
    var height: CGFloat {
        let contentViewWidth = UIScreen.main.bounds.width
        let targetSize = CGSize(width: contentViewWidth, height: UIView.layoutFittingCompressedSize.height)
        return systemLayoutSizeFitting(targetSize).height
    }
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.backgroundPrimary.color
        return view
    }()
    
    lazy var footerView: FooterLinkView = {
        let view = FooterLinkView(attributedText: FooterLinkViewHelper.getHelpMessage())
        view.delegate = self
        view.isHidden = true
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var descriptionTextView: UITextView = {
        let textView = UITextView(frame: .zero)
        textView.linkTextAttributes = [NSAttributedString.Key.foregroundColor: Colors.branding400.color]
        textView.delegate = self
        textView.isEditable = false
        return textView
    }()
    
    lazy var primaryButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(pressPrimaryButton), for: .touchUpInside)
        return button
    }()
    
    init() {
        super.init(frame: .zero)
    }
    
    public func setupDataInView(_ model: StatefulFeedbackViewModel, hasfooterView: Bool = false) {
        titleLabel.text = model.content?.title
        descriptionTextView.attributedText = model.description
        imageView.image = model.image
        primaryButton.setTitle(model.button?.title, for: .normal)
        footerView.isHidden = !hasfooterView
        buildLayout()
        if !hasfooterView {
            removeFooterView()
        }
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    func pressPrimaryButton() {
        delegate?.didTapPrimaryButton()
    }
}

// MARK: - ViewConfiguration
extension UnsuccessfulDeliveryCardView: ViewConfiguration {
    public func buildViewHierarchy() {
        contentView.addSubviews(imageView, titleLabel, descriptionTextView, primaryButton, footerView)
        addSubview(contentView)
    }
   
    private func removeFooterView() {
        footerView.snp.removeConstraints()
        footerView.removeFromSuperview()
        primaryButton.snp.remakeConstraints {
            $0.top.equalTo(descriptionTextView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.bottom.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    public func setupConstraints() {
        let imageSize: Layout.Size = isSmallScreen ?.smallImage : .regularImage
        
        contentView.snp.makeConstraints {
            $0.edges.equalTo(compatibleSafeArea.edges)
        }
        
        imageView.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.size.lessThanOrEqualTo(imageSize.rawValue)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        descriptionTextView.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        primaryButton.snp.makeConstraints {
            $0.top.equalTo(descriptionTextView.snp.bottom).offset(Spacing.base03)
            $0.bottom.equalTo(footerView.snp.top).offset(-Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        footerView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(compatibleSafeArea.bottom)
        }
    }
    
    public func configureViews() {
        backgroundColor = Colors.grayscale100.color
    }
}
// MARK: - UITextViewDelegate
extension UnsuccessfulDeliveryCardView: UITextViewDelegate {
    public func textView(
        _ textView: UITextView,
        shouldInteractWith URL: URL,
        in characterRange: NSRange,
        interaction: UITextItemInteraction
    ) -> Bool {
        delegate?.didPressLink()
        return false
    }
    public func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        false
    }
}

// MARK: - FooterLinkViewDelegate
extension UnsuccessfulDeliveryCardView: FooterLinkViewDelegate {
    func didPressLink(url: URL) {
        delegate?.openFaqHelpCenter()
    }
}
