import UI
import Foundation
import AssetsKit
import Core

enum UnsuccessfulDeliveryTypeCardModel {
    typealias Localizable = Strings.UnsuccessfulDeliveryCard
    case debit
    case credit
    
    private func makeAttributed(_ addressString: String) -> NSAttributedString {
        var descripitionString: [String] = []
        switch self {
        case .debit:
            descripitionString = [Localizable.Debit.description, "**\(addressString)**", Localizable.Debit.Button.secondary]
        case .credit:
            descripitionString = [Localizable.description, Localizable.Description.firstPoint, "**\(addressString)**", Localizable.Description.secondPoint, Localizable.Button.secondary]
        }
        
        return descripitionString.reduce(into: "") { $0 += "\n\n\($1)" }.attributedStringWithFont(primary: Typography.bodySecondary(.default).font(), secondary: Typography.bodyPrimary(.highlight).font())
    }
    
    static func typeModel(type: CardTrackingType) -> UnsuccessfulDeliveryTypeCardModel {
        switch type {
        case .debit:
            return .debit
        default :
            return .credit
        }
    }
    
    private func makeDescripition(url: URL, addressString: String) -> NSMutableAttributedString {
        let descripition = makeAttributed(addressString)
        let attributed = NSMutableAttributedString(attributedString: descripition)
        let linkMessage = self == .credit ? Localizable.Button.secondary : Localizable.Debit.Button.secondary
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = self == .credit ? .left : .center
        attributed.font(text: linkMessage, font: Typography.bodySecondary().font())
        attributed.textBackgroundColor(text: linkMessage, color: .clear)
        attributed.textColor(text: descripition.string, color: Colors.grayscale600.color)
        attributed.paragraph(aligment: .center, lineSpace: Spacing.base00)
        attributed.underline(text: linkMessage)
        attributed.textColor(text: linkMessage,
                             color: Colors.branding300.color)
        if self == .credit {
            [Localizable.Description.firstPoint, Localizable.Description.secondPoint, addressString].forEach {
                attributed.paragraphStyle(text: $0, paragraphStyle: paragraphStyle)
            }
        }
        
        attributed.link(text: linkMessage, link: url.absoluteString)
        return attributed
    }
    
     func getModel(deliveryAddress: String, url: URL) -> StatefulFeedbackViewModel {
        switch self {
        case .debit:
            return StatefulFeedbackViewModel(image: Resources.Illustrations.iluSendWarningDelivery.image, content: (title: Localizable.Debit.title, description: nil), description: makeDescripition(url: url, addressString: deliveryAddress), button: (image: nil, title: Localizable.Debit.Button.primary))
            
        case .credit:
            return StatefulFeedbackViewModel(image: Resources.Illustrations.iluSendWarningDelivery.image, content: (title: Localizable.title, description: nil), description: makeDescripition(url: url, addressString: deliveryAddress), button: (image: nil, title: Localizable.Button.primary))
        }
    }
}
