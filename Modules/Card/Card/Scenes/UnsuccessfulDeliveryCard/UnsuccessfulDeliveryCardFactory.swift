import UIKit

public enum UnsuccessfulDeliveryCardFactory {
    public static func make(cardType: CardTrackingType) -> UnsuccessfulDeliveryCardViewController {
        let container = DependencyContainer()
        let coordinator: UnsuccessfulDeliveryCardCoordinating = UnsuccessfulDeliveryCardCoordinator(dependencies: container)
        let presenter: UnsuccessfulDeliveryCardPresenting = UnsuccessfulDeliveryCardPresenter(coordinator: coordinator)
        let service: RequestPhysicalCardLoadingServicing = RequestPhysicalCardLoadingService(dependencies: container)
        let interactor = UnsuccessfulDeliveryCardInteractor(presenter: presenter, cardType: cardType, dependencies: container, service: service)
        let viewController = UnsuccessfulDeliveryCardViewController(interactor: interactor)
        
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
