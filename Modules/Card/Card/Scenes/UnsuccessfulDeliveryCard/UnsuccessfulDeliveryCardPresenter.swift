import Foundation
import UI
import AssetsKit
import Core

public protocol UnsuccessfulDeliveryCardPresenting: AnyObject {
    var viewController: UnsuccessfulDeliveryCardDisplaying? { get set }
    func presentInfoAddress(_ model: StatefulFeedbackViewModel, hasFooter: Bool)
    func presentLoadState()
    func presentErroState(_ error: ApiError)
    func presentNextStep(action: UnsuccessfulDeliveryCardAction)
}

public final class UnsuccessfulDeliveryCardPresenter {
    typealias Localizable = Strings.UnsuccessfulDeliveryCard
    private let coordinator: UnsuccessfulDeliveryCardCoordinating
    public weak var viewController: UnsuccessfulDeliveryCardDisplaying?
    
    init(coordinator: UnsuccessfulDeliveryCardCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - UnsuccessfulDeliveryCardPresenting
extension UnsuccessfulDeliveryCardPresenter: UnsuccessfulDeliveryCardPresenting {
    public func presentNextStep(action: UnsuccessfulDeliveryCardAction) {
        coordinator.perform(action: action)
    }
    
    public func presentErroState(_ error: ApiError) {
        let model = StatefulFeedbackViewModel(error)
        viewController?.displayErrorState(model: model)
    }
    
    public func presentLoadState() {
        viewController?.displayLoadState()
    }
    
    public func presentInfoAddress(_ model: StatefulFeedbackViewModel, hasFooter: Bool) {
        viewController?.displayInfo(model: model, hasFooter: hasFooter)
    }
}
