import UIKit

protocol RequestPhysicalCardLoadingDisplay: AnyObject {
    func showError(with errorMessage: String)
}
