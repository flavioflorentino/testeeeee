import Core
import Foundation

protocol RequestPhysicalCardLoadingPresenting: AnyObject {
    var viewController: RequestPhysicalCardLoadingDisplay? { get set }
    func didNextStep(action: RequestPhysicalCardLoadingAction)
    func updateDeliveryAddress(deliveryAddress: HomeAddress)
    func showError()
    func endFlow()
}

final class RequestPhysicalCardLoadingPresenter: RequestPhysicalCardLoadingPresenting {
    private let coordinator: RequestPhysicalCardLoadingCoordinating
    weak var viewController: RequestPhysicalCardLoadingDisplay?

    init(coordinator: RequestPhysicalCardLoadingCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: RequestPhysicalCardLoadingAction) {
        coordinator.perform(action: action)
    }
    
    func updateDeliveryAddress(deliveryAddress: HomeAddress) {
        coordinator.perform(action: .updateFlowCoordinator(deliveryAddress: deliveryAddress))
    }
    
    func showError() {
        let message = Strings.DebitLoading.errorOnUpdateAddress
        viewController?.showError(with: message)
    }
    
    func endFlow() {
        coordinator.perform(action: .endFlow)
    }
}
