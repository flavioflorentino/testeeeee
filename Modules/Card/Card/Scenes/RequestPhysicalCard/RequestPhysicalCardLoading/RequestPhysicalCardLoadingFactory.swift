import Foundation

public enum RequestPhysicalCardLoadingFactory {
    public static func make(
        requestCardCoordinatorOutput: @escaping (RequestPhysicalCardCoordinatorOutput) -> Void,
        service: RequestPhysicalCardLoadingServicing
    ) -> RequestPhysicalCardLoadingViewController {
        let coordinator: RequestPhysicalCardLoadingCoordinating = RequestPhysicalCardLoadingCoordinator(
            requestCardCoordinatorOutput: requestCardCoordinatorOutput
        )
        let presenter: RequestPhysicalCardLoadingPresenting = RequestPhysicalCardLoadingPresenter(coordinator: coordinator)
        let interactor = RequestPhysicalCardLoadingInteractor(service: service, presenter: presenter)
        let viewController = RequestPhysicalCardLoadingViewController(viewModel: interactor)

        presenter.viewController = viewController

        return viewController
    }
}
