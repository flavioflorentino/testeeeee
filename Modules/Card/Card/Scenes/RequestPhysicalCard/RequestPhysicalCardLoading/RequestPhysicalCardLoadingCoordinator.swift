import UIKit

enum RequestPhysicalCardLoadingAction {
    case updateFlowCoordinator(deliveryAddress: HomeAddress)
    case endFlow
}

protocol RequestPhysicalCardLoadingCoordinating: AnyObject {
    func perform(action: RequestPhysicalCardLoadingAction)
}

final class RequestPhysicalCardLoadingCoordinator: RequestPhysicalCardLoadingCoordinating {
    var coordinadorOutput: (RequestPhysicalCardCoordinatorOutput) -> Void

    init(requestCardCoordinatorOutput: @escaping  (RequestPhysicalCardCoordinatorOutput) -> Void) {
        self.coordinadorOutput = requestCardCoordinatorOutput
    }
    
    func perform(action: RequestPhysicalCardLoadingAction) {
        switch action {
        case .updateFlowCoordinator(let deliveryAddress):
            coordinadorOutput(.getServiceResponse(deliveryAddress: deliveryAddress))
        case .endFlow:
            coordinadorOutput(.close)
        }
    }
}
