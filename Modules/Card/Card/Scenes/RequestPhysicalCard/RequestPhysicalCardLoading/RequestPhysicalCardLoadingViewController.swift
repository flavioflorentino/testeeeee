import UI
import UIKit

extension RequestPhysicalCardLoadingViewController.Layout {
    enum Size {
        static let activityIndicator = CGSize(width: Sizing.base05, height: Sizing.base05)
    }
    enum Constants {
        static let activityIndicatorScale: CGFloat = 1.5
    }
}

public final class RequestPhysicalCardLoadingViewController: ViewController<RequestPhysicalCardLoadingInteractorInputs, UIView> {
    fileprivate enum Layout {}
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fill
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var loadingLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textAlignment, .center)
            .with(\.textColor, Colors.black.color)
        label.text = Strings.DebitLoading.loadingLabel
        return label
    }()
    
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.startAnimating()
        activityIndicator.color = Colors.black.color
        activityIndicator.transform = CGAffineTransform(
            scaleX: Layout.Constants.activityIndicatorScale,
            y: Layout.Constants.activityIndicatorScale
        )
        return activityIndicator
    }()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        viewModel.getDeliveryAddress()
        
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
 
    override public func buildViewHierarchy() {
        stackView.addArrangedSubview(activityIndicator)
        stackView.addArrangedSubview(loadingLabel)
        
        view.addSubview(stackView)
    }
    
    override public func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    override public func setupConstraints() {
        activityIndicator.snp.makeConstraints {
            $0.height.width.equalTo(Layout.Size.activityIndicator)
        }
        
        stackView.snp.makeConstraints {
            $0.centerX.centerY.equalToSuperview()
            $0.width.equalToSuperview()
        }
    }
    
    private func showPopupError(messageDescription: String) {
        let popup = PopupViewController(
            title: Strings.Default.ops,
            description: messageDescription,
            preferredType: .image(Assets.icoSadEmoji.image))
        
        let confirmAction = PopupAction(title: Strings.Default.iGotIt, style: .fill) {
            self.didDismiss()
        }
        
        popup.addAction(confirmAction)
        showPopup(popup)
    }
    
    @objc
    private func didDismiss() {
        viewModel.endFlow()
    }
}

// MARK: View Model Outputs
extension RequestPhysicalCardLoadingViewController: RequestPhysicalCardLoadingDisplay {
    func showError(with errorMessage: String) {
        showPopupError(messageDescription: errorMessage)
    }
}

extension RequestPhysicalCardLoadingViewController: StyledNavigationDisplayable {
    public var prefersLargeTitle: Bool {
        false
    }
}
