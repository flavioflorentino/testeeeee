import Core
import Foundation

public protocol RequestPhysicalCardLoadingServicing {
    func getDeliveryAddress(completion: @escaping CompletionAddress)
}

public typealias CompletionAddress = (Result<HomeAddress?, ApiError>) -> Void

public final class RequestPhysicalCardLoadingService: RequestPhysicalCardLoadingServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    public func getDeliveryAddress(completion: @escaping CompletionAddress) {
        let endpoint = CreditServiceEndpoint.creditAccountAddress
        Api<[AccountAddress]>(endpoint: endpoint).execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { result in
            self.dependencies.mainQueue.async {
                let mappedResult = result.map { $0.model }
                let correctAddress = mappedResult.map {
                    self.chooseCorrectAddress(in: $0)
                }
                let deliveryAddress = correctAddress.map { address -> HomeAddress? in
                    self.convert(accountAddress: address)
                }
                completion(deliveryAddress)
            }
        }
    }
    
    private func chooseCorrectAddress(in addressList: [AccountAddress]) -> AccountAddress? {
        if let residential = addressList.first(where: { $0.type == .residential }) {
            return residential
        }
        return addressList.first
    }
    
    private func convert(accountAddress: AccountAddress?) -> HomeAddress? {
        guard let accountAddress = accountAddress else {
            return nil
        }
        var deliveryAddress = HomeAddress()
        deliveryAddress.city = accountAddress.city
        deliveryAddress.state = accountAddress.state
        deliveryAddress.street = accountAddress.street
        deliveryAddress.streetNumber = String(accountAddress.number)
        deliveryAddress.addressComplement = accountAddress.complement
        deliveryAddress.zipCode = accountAddress.zipCode
        return deliveryAddress
    }
}
