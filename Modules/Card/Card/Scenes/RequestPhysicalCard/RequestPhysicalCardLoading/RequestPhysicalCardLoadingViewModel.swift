import Foundation

public protocol RequestPhysicalCardLoadingInteractorInputs: AnyObject {
    func getDeliveryAddress()
    func endFlow()
}

final class RequestPhysicalCardLoadingInteractor {
    private let service: RequestPhysicalCardLoadingServicing
    private let presenter: RequestPhysicalCardLoadingPresenting

    init(service: RequestPhysicalCardLoadingServicing, presenter: RequestPhysicalCardLoadingPresenting) {
        self.service = service
        self.presenter = presenter
    }
}

extension RequestPhysicalCardLoadingInteractor: RequestPhysicalCardLoadingInteractorInputs {
    func getDeliveryAddress() {
        service.getDeliveryAddress { [weak self] result in
            switch result {
            case .success(let deliveryAddress):
                if let deliveryAddress = deliveryAddress {
                    self?.presenter.updateDeliveryAddress(deliveryAddress: deliveryAddress)
                } else {
                    self?.presenter.showError()
                }
            case .failure:
                self?.presenter.showError()
            }
        }
    }
    
    func endFlow() {
        presenter.endFlow()
    }
}
