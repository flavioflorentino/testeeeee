import UIKit
import UI

protocol CardBlockReplacementReasonDisplay: AnyObject {
    func displayReasons(data: [CardBlockReplacementReasonData])
    func displayText(description: String, confirmButtonTitle: String)
}

private extension CardBlockReplacementReasonViewController.Layout {
    enum Size {
        static let confirmButton: CGFloat = 48
        static let cancelButton: CGFloat = 40
    }
    enum Font {
        static let confirmButton = UIFont.systemFont(ofSize: 16, weight: .semibold)
        static let cancelButton = UIFont.systemFont(ofSize: 14, weight: .semibold)
    }
}

final class CardBlockReplacementReasonViewController: ViewController<CardBlockReplacementReasonViewModelInputs, UIView> {
    fileprivate enum Layout { }

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.textColor = Colors.grayscale700.color
        label.numberOfLines = 0
        return label
    }()
    private lazy var radioButtonForm: RadioButtonForm = {
        let form = RadioButtonForm()
        form.delegate = self
        return form
    }()
    private lazy var confirmButton: ConfirmButton = {
        let button = ConfirmButton()
        button.layer.cornerRadius = Layout.Size.confirmButton / 2
        button.setTitleColor(Colors.white.lightColor, for: .normal)
        button.titleLabel?.font = Layout.Font.confirmButton
        button.addTarget(self, action: #selector(tapConfirm), for: .touchUpInside)
        button.isEnabled = false
        return button
    }()
    private lazy var cancelButton: UIButton = {
        let button = UIButton(type: .system)
        button.layer.cornerRadius = Layout.Size.cancelButton / 2
        button.setTitleColor(Colors.grayscale400.color, for: .normal)
        button.titleLabel?.font = Layout.Font.cancelButton
        button.addTarget(self, action: #selector(tapClose), for: .touchUpInside)
        button.setTitle(Strings.CardBlockReplacementReason.cancel, for: .normal)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.updateView()
    }
 
    override func buildViewHierarchy() {
        view.addSubviews(descriptionLabel, radioButtonForm, confirmButton, cancelButton)
    }
    
    override func configureViews() {
        title = Strings.CardBlockReplacementReason.title
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    override func setupConstraints() {
        descriptionLabel.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.top.equalTo(self.view.compatibleSafeArea.top).offset(Spacing.base01)
        }
        radioButtonForm.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base04)
        }
        confirmButton.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Layout.Size.confirmButton)
        }
        cancelButton.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Layout.Size.cancelButton)
            $0.top.equalTo(confirmButton.snp.bottom).offset(Spacing.base02)
            $0.bottom.equalTo(self.view.compatibleSafeArea.bottom).inset(Spacing.base02)
        }
    }
}

// MARK: CardBlockReplacementReasonViewController - Tap Functions
@objc
extension CardBlockReplacementReasonViewController {
    private func tapConfirm() {
        guard let selectedReason = radioButtonForm.selectedValue() as? CardBlockReplacementReason else {
            return
        }
        viewModel.confirm(reason: selectedReason)
    }
    private func tapClose() {
        viewModel.cancel()
    }
}

// MARK: CardBlockReplacementReasonDisplay
extension CardBlockReplacementReasonViewController: CardBlockReplacementReasonDisplay {
    func displayReasons(data: [CardBlockReplacementReasonData]) {
        radioButtonForm.data = data
    }
    func displayText(description: String, confirmButtonTitle: String) {
        descriptionLabel.attributedText = description.attributedStringWithFont(
            primary: Typography.bodyPrimary().font(),
            secondary: Typography.bodyPrimary(.highlight).font()
        )
        confirmButton.setTitle(confirmButtonTitle, for: .normal)
    }
}

// MARK: RadioButtonFormDelegate
extension CardBlockReplacementReasonViewController: RadioButtonFormDelegate {
    public func didChangeSelected() {
        confirmButton.isEnabled = true
    }
}

extension CardBlockReplacementReasonViewController: StyledNavigationDisplayable {}
