import Foundation

enum CardBlockReplacementReasonTypeFlow {
    case block
    case replacement
}

enum CardBlockReplacementReasonFactory {
    static func make(
        delegate: CardBlockReplacementReasonCoordinatorDelegate,
        typeFlow: CardBlockReplacementReasonTypeFlow
    ) -> CardBlockReplacementReasonViewController {
        let coordinator: CardBlockReplacementReasonCoordinating = CardBlockReplacementReasonCoordinator()
        let presenter: CardBlockReplacementReasonPresenting =
            CardBlockReplacementReasonPresenter(coordinator: coordinator)
        let viewModel = CardBlockReplacementReasonViewModel(
            presenter: presenter,
            typeFlow: typeFlow
        )
        let viewController = CardBlockReplacementReasonViewController(viewModel: viewModel)

        coordinator.delegate = delegate
        presenter.viewController = viewController

        return viewController
    }
}
