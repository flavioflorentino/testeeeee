import AnalyticsModule

protocol CardBlockReplacementReasonViewModelInputs: AnyObject {
    func cancel()
    func confirm(reason: CardBlockReplacementReason)
    func updateView()
}

final class CardBlockReplacementReasonViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    private let presenter: CardBlockReplacementReasonPresenting
    private var typeFlow: CardBlockReplacementReasonTypeFlow

    init(
        presenter: CardBlockReplacementReasonPresenting,
        typeFlow: CardBlockReplacementReasonTypeFlow,
        dependencies: Dependencies = DependencyContainer()
    ) {
        self.presenter = presenter
        self.typeFlow = typeFlow
        self.dependencies = dependencies
    }
}

// MARK: - CardBlockReplacementReasonViewModelInputs
extension CardBlockReplacementReasonViewModel: CardBlockReplacementReasonViewModelInputs {
    func cancel() {
        dependencies.analytics.log(BlockReplacementCardEvent.reason(action: .notNow, flow: typeFlow))
        presenter.didNextStep(action: .cancel)
    }
    func confirm(reason: CardBlockReplacementReason) {
        trackReason(reason: reason)
        presenter.didNextStep(action: .confirm(reason: reason))
    }
    func updateView() {
        getReasons()
        getTextDescriptionConfirmButtonTitle()
    }
    func getReasons() {
        let lossReason = CardBlockReplacementReasonData(
            value: CardBlockReplacementReason.loss,
            description: Strings.CardBlockReplacementReason.lossReasonDescription
        )
        let stolenReason = CardBlockReplacementReasonData(
            value: CardBlockReplacementReason.stolen,
            description: Strings.CardBlockReplacementReason.stolenReasonDescription
        )
        let damagedReason = CardBlockReplacementReasonData(
            value: CardBlockReplacementReason.damaged,
            description: Strings.CardBlockReplacementReason.damagedReasonDescription
        )

        let reasons = (typeFlow == .block) ? [lossReason, stolenReason] : [damagedReason, lossReason, stolenReason]
        presenter.displayReasons(data: reasons)
    }
    func getTextDescriptionConfirmButtonTitle() {
        presenter.displayTextDescriptionConfirmButtonTitle(for: typeFlow)
    }
    func trackReason(reason: CardBlockReplacementReason) {
        switch reason {
        case .loss:
            dependencies.analytics.log(BlockReplacementCardEvent.reason(action: .lost, flow: typeFlow))
        case .stolen:
            dependencies.analytics.log(BlockReplacementCardEvent.reason(action: .stolen, flow: typeFlow))
        case .damaged:
            dependencies.analytics.log(BlockReplacementCardEvent.reason(action: .broken, flow: typeFlow))
        }
    }
}
