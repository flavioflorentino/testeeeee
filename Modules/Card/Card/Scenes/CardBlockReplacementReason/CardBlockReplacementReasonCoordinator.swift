import UIKit

public enum CardBlockReplacementReasonAction: Equatable {
    case cancel
    case confirm(reason: CardBlockReplacementReason)
}

public protocol CardBlockReplacementReasonCoordinatorDelegate: AnyObject {
    func cancelBlockReason()
    func confirmBlock(with reason: CardBlockReplacementReason)
}

protocol CardBlockReplacementReasonCoordinating: AnyObject {
    var delegate: CardBlockReplacementReasonCoordinatorDelegate? { get set }
    func perform(action: CardBlockReplacementReasonAction)
}

final class CardBlockReplacementReasonCoordinator: CardBlockReplacementReasonCoordinating {
    weak var delegate: CardBlockReplacementReasonCoordinatorDelegate?

    func perform(action: CardBlockReplacementReasonAction) {
        switch action {
        case .cancel:
            delegate?.cancelBlockReason()
        case .confirm(let reason):
            delegate?.confirmBlock(with: reason)
        }
    }
}
