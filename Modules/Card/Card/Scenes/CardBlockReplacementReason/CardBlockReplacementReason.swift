public enum CardBlockReplacementReason: String {
    case loss = "LOSS"
    case stolen = "STOLEN"
    case damaged = "DAMAGED"
}

typealias CardBlockReplacementReasonData = (value: CardBlockReplacementReason, description: String)
