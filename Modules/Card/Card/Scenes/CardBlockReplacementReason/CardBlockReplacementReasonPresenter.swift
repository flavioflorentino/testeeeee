import UI

protocol CardBlockReplacementReasonPresenting: AnyObject {
    var viewController: CardBlockReplacementReasonDisplay? { get set }
    func didNextStep(action: CardBlockReplacementReasonAction)
    func displayReasons(data: [CardBlockReplacementReasonData])
    func displayTextDescriptionConfirmButtonTitle(for typeFlow: CardBlockReplacementReasonTypeFlow)
}

final class CardBlockReplacementReasonPresenter {
    private let coordinator: CardBlockReplacementReasonCoordinating
    weak var viewController: CardBlockReplacementReasonDisplay?

    init(coordinator: CardBlockReplacementReasonCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - CardBlockReplacementReasonPresenting
extension CardBlockReplacementReasonPresenter: CardBlockReplacementReasonPresenting {
    func didNextStep(action: CardBlockReplacementReasonAction) {
        coordinator.perform(action: action)
    }
    func displayReasons(data: [CardBlockReplacementReasonData]) {
        viewController?.displayReasons(data: data)
    }
    func displayTextDescriptionConfirmButtonTitle(for typeFlow: CardBlockReplacementReasonTypeFlow) {
        if typeFlow == .block {
            viewController?.displayText(
                description: Strings.CardBlockReplacementReason.blockDescription,
                confirmButtonTitle: Strings.CardBlockReplacementReason.confirmBlock
            )
        } else {
            viewController?.displayText(
                description: Strings.CardBlockReplacementReason.replacementDescription,
                confirmButtonTitle: Strings.CardBlockReplacementReason.confirmReplacement
            )
        }
    }
}
