import UIKit

enum PasswordSecurityTipsAction {
    case createPassword
    case close
}

protocol PasswordSecurityTipsCoordinatorDelegate: AnyObject {
    func didOpenCardPassword()
    func didDismiss()
}

protocol PasswordSecurityTipsCoordinating: AnyObject {
    var delegate: PasswordSecurityTipsCoordinatorDelegate? { get set }
    var viewController: UIViewController? { get set }
    func perform(action: PasswordSecurityTipsAction)
}

final class PasswordSecurityTipsCoordinator: PasswordSecurityTipsCoordinating {
    weak var delegate: PasswordSecurityTipsCoordinatorDelegate?
    weak var viewController: UIViewController?
    
    func perform(action: PasswordSecurityTipsAction) {
        switch action {
        case .createPassword:
            delegate?.didOpenCardPassword()
        case .close:
            delegate?.didDismiss()
            viewController?.dismiss(animated: true)
        }
    }
}
