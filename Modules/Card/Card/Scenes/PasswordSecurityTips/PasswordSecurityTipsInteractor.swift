import Foundation

public protocol PasswordSecurityTipsInteracting {
    func updateView()
    func confirm()
    func close()
}

public final class PasswordSecurityTipsInteractor {
    private let presenter: PasswordSecurityTipsPresenting
    
    init(presenter: PasswordSecurityTipsPresenting) {
        self.presenter = presenter
    }
}

extension PasswordSecurityTipsInteractor: PasswordSecurityTipsInteracting {
    public func updateView() {
        presenter.updateView()
    }
    
    public func confirm() {
        presenter.didNextStep(action: .createPassword)
    }
    public func close() {
        presenter.didNextStep(action: .close)
    }
}
