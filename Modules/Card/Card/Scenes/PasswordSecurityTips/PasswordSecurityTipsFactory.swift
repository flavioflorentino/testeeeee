import Foundation

enum PasswordSecurityTipsFactory {
    static func make(delegate: PasswordSecurityTipsCoordinatorDelegate?) -> PasswordSecurityTipsViewController {
        let coordinator: PasswordSecurityTipsCoordinating = PasswordSecurityTipsCoordinator()
        let presenter = PasswordSecurityTipsPresenter(coordinator: coordinator)
        let interactor = PasswordSecurityTipsInteractor(presenter: presenter)
        
        let viewController = PasswordSecurityTipsViewController(interactor: interactor)
        
        presenter.viewController = viewController
        coordinator.delegate = delegate
        coordinator.viewController = viewController

        return viewController
    }
}
