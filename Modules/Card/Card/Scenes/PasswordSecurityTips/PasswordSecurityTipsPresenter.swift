import UIKit

protocol PasswordSecurityTipsPresenting: AnyObject {
    var viewController: PasswordSecurityTipsDisplay? { get set }
    func updateView()
    func didNextStep(action: PasswordSecurityTipsAction)
}

final class PasswordSecurityTipsPresenter: PasswordSecurityTipsPresenting {
    typealias Localizable = Strings.PasswordSecurityTips
    
    private let coordinator: PasswordSecurityTipsCoordinating
    weak var viewController: PasswordSecurityTipsDisplay?

    init(coordinator: PasswordSecurityTipsCoordinating) {
        self.coordinator = coordinator
    }
    
    func updateView() {
        let tips = [
            StepPresenter(position: .initial, number: Localizable.one, description: Localizable.notUseSequenceNumbers),
            StepPresenter(position: .middle, number: Localizable.two, description: Localizable.passwordInMultiplePlaces),
            StepPresenter(position: .final, number: Localizable.three, description: Localizable.avoidUsingData)
        ]
        
        let tipsModel = PasswordSecurityTipsModel(description: Localizable.createPasswordTransactions,
                                                  securityTip: Localizable.safetyTips,
                                                  passwordTitle: Localizable.createPassword,
                                                  tips: tips)
        viewController?.update(with: tipsModel)
    }
    
    func didNextStep(action: PasswordSecurityTipsAction) {
        coordinator.perform(action: action)
    }
}
