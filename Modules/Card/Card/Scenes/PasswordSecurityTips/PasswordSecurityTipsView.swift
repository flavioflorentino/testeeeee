import UI
import UIKit

public protocol PasswordSecurityTipsViewDelegate: AnyObject {
    func didTapCreatePassword()
}

public class PasswordSecurityTipsView: UIView {
    private enum Layout {
        static let textSemiBold = UIFont.systemFont(ofSize: 16, weight: .semibold)
        static let textMedium = UIFont.systemFont(ofSize: 16, weight: .medium)
        static let textSmall = UIFont.systemFont(ofSize: 16, weight: .light)
        static let sizeButton: CGFloat = 48.0
    }
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.font = Layout.textSmall
        label.textColor = Palette.ppColorGrayscale500.color
        return label
    }()
    
    private lazy var securityTipLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Layout.textMedium
        label.textColor = Palette.ppColorGrayscale600.color
        return label
    }()
    
    private lazy var stepsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var passwordButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = Layout.sizeButton / 2
        button.backgroundColor = Palette.ppColorBranding300.color
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = Layout.textSemiBold
        button.addTarget(self, action: #selector(tapCreatePassword), for: .touchUpInside)
        return button
    }()
    
    public weak var delegate: PasswordSecurityTipsViewDelegate?
    
    public var tipsModel: PasswordSecurityTipsModel? {
        didSet {
            update()
        }
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func update() {
        descriptionLabel.text = tipsModel?.description
        securityTipLabel.text = tipsModel?.securityTip
        passwordButton.setTitle(tipsModel?.passwordTitle, for: .normal)
        
        stepsStackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
        
        tipsModel?.tips.forEach { presenterTip in
            let view = StepView()
            view.presenter = presenterTip
            stepsStackView.addArrangedSubview(view)
        }
    }
    
    @objc
    public func tapCreatePassword() {
        delegate?.didTapCreatePassword()
    }
}

extension PasswordSecurityTipsView: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(descriptionLabel)
        addSubview(securityTipLabel)
        addSubview(stepsStackView)
        addSubview(passwordButton)
    }
        
    public func setupConstraints() {
        NSLayoutConstraint.leadingTrailing(equalTo: self,
                                           for: [descriptionLabel, securityTipLabel, stepsStackView, passwordButton],
                                           constant: 16)
        
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: compatibleSafeAreaLayoutGuide.topAnchor, constant: 16),
            stepsStackView.topAnchor.constraint(equalTo: securityTipLabel.bottomAnchor, constant: 16),
            securityTipLabel.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 40),
            passwordButton.bottomAnchor.constraint(equalTo: compatibleSafeAreaLayoutGuide.bottomAnchor, constant: -16),
            passwordButton.heightAnchor.constraint(equalToConstant: Layout.sizeButton)
        ])
    }
    
    public func configureViews() {
        backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    private func setupView() {
        buildViewHierarchy()
        configureViews()
        setupConstraints()
    }
}
