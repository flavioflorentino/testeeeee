import UI
import UIKit

protocol PasswordSecurityTipsDisplay: AnyObject {
    func update(with tipsModel: PasswordSecurityTipsModel)
}

public class PasswordSecurityTipsViewController: ViewController<PasswordSecurityTipsInteracting, PasswordSecurityTipsView> {
    override public func viewDidLoad() {
        super.viewDidLoad()
        interactor.updateView()
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBarAppearance()
    }
    
    override public func configureViews() {
        rootView.delegate = self
        title = Strings.PasswordSecurityTips.passwordCard
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }
    
    @objc
    public func tapClose() {
        performAction()
    }
    
    // MARK: - Private Methods Helpers
    private func updateNavigationBarAppearance() {
        navigationController?.navigationBar.tintColor = Palette.ppColorBranding300.color
        navigationController?.navigationBar.isTranslucent = true
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
            navigationController?.navigationItem.largeTitleDisplayMode = .always
        }
        
        let backButton = UIBarButtonItem()
        backButton.title = String()
        navigationItem.backBarButtonItem = backButton
        
        if navigationController?.viewControllers.first is PasswordSecurityTipsViewController {
            navigationItem.rightBarButtonItem = UIBarButtonItem(
                image: Assets.icoClose.image,
                style: .plain,
                target: self,
                action: #selector(tapClose)
            )
        }
    }
    private func performAction() {
        if navigationController?.viewControllers.first is PasswordSecurityTipsViewController {
            viewModel.close()
        } else {
            viewModel.confirm()
        }
    }
}

// MARK: View Model Outputs
extension PasswordSecurityTipsViewController: PasswordSecurityTipsDisplay {
    func update(with tipsModel: PasswordSecurityTipsModel) {
        rootView.tipsModel = tipsModel
    }
}

// MARK: - View Delegate
extension PasswordSecurityTipsViewController: PasswordSecurityTipsViewDelegate {
    public func didTapCreatePassword() {
        performAction()
    }
}
