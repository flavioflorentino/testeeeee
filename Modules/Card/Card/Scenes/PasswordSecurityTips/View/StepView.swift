import UI
import UIKit

public class StepView: UIView {
    private enum Layout {
        static let textSmall = UIFont.systemFont(ofSize: 16, weight: .light)
        static let textSemiBold = UIFont.systemFont(ofSize: 14, weight: .semibold)
    }
    
    public enum Position {
        case initial
        case middle
        case final
    }
    
    private lazy var numberLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.layer.cornerRadius = 14
        label.layer.borderWidth = 1
        label.layer.borderColor = Palette.ppColorBranding300.cgColor
        label.textColor = Palette.ppColorBranding300.color
        label.font = Layout.textSemiBold
        label.layer.addSublayer(dashedLine)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textColor = Palette.ppColorGrayscale500.color
        label.font = Layout.textSmall
        return label
    }()
    
    private lazy var dashedLine: CAShapeLayer = {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = Palette.ppColorGrayscale300.cgColor
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineWidth = 1
        shapeLayer.lineDashPattern = [2, 2]
        return shapeLayer
    }()
    
    var presenter: StepPresenter? {
        didSet {
            update()
        }
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func layoutSublayers(of layer: CALayer) {
        super.layoutSublayers(of: layer)
        
        guard let presenter = presenter else {
            return
        }
        
        dashedLine.path = addLinesFor(position: presenter.position)
    }
    
    // MARK: - Private Methods
    private func update() {
        numberLabel.text = presenter?.number
        descriptionLabel.text = presenter?.description
    }
    
    private func addLinesFor(position: StepPositionType) -> CGMutablePath {
        let path = CGMutablePath()
        
        if position == .initial || position == .middle {
            path.addLines(between: [
                CGPoint(x: numberLabel.frame.size.width / 2, y: numberLabel.frame.size.height),
                CGPoint(x: numberLabel.frame.size.width / 2, y: frame.size.height)
            ])
        }
        
        if position == .final || position == .middle {
            path.addLines(between: [
                CGPoint(x: numberLabel.frame.size.width / 2, y: -numberLabel.frame.origin.y),
                CGPoint(x: numberLabel.frame.size.width / 2, y: 0)
            ])
        }
        
        return path
    }
}

extension StepView: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(numberLabel)
        addSubview(descriptionLabel)
    }
    
    public func setupConstraints() {
        NSLayoutConstraint.activate([
            numberLabel.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            numberLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            numberLabel.heightAnchor.constraint(equalToConstant: 28),
            numberLabel.widthAnchor.constraint(equalToConstant: 28)
        ])
        
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            descriptionLabel.leadingAnchor.constraint(equalTo: numberLabel.trailingAnchor, constant: 16),
            descriptionLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            descriptionLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -24)
        ])
        
        heightAnchor.constraint(greaterThanOrEqualToConstant: 48).isActive = true
    }
    
    public func configureViews() {
        backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    private func setupView() {
        buildViewHierarchy()
        configureViews()
        setupConstraints()
    }
}
