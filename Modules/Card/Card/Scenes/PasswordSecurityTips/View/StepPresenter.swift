import Foundation

public enum StepPositionType {
    case initial
    case middle
    case final
}

public struct StepPresenter: Equatable {
    public var position: StepPositionType
    public var number: String
    public var description: String
}

public struct PasswordSecurityTipsModel: Equatable {
    let description: String
    let securityTip: String
    let passwordTitle: String
    let tips: [StepPresenter]
}
