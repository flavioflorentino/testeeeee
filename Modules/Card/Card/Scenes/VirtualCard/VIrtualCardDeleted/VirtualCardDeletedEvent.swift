import AnalyticsModule
import Core

enum VirtualCardDeletedEvent: AnalyticsKeyProtocol {
    case auth(action: VirtualCardDeletedAuth)
    case deleted(action: VirtualCardDeletedAction)
    case errorState(name: VirtualCardDeletedErrorName, state: VirtualCardDeletedErrorState)
    case errorAction(name: VirtualCardDeletedErrorName, action: VirtualCardDeletedErrorAction)
    
    private var name: String {
        switch self {
        case .auth:
            return "Authtenticated"
        case .deleted:
            return "Deleted"
        case let .errorState(name, _):
            return name.rawValue
        case let .errorAction(name, _):
            return name.rawValue
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .auth(action):
            return [
                "action": action.rawValue
            ]
        case let .deleted(action):
            return [
                "action": action.rawValue
            ]
        case let .errorState(_, state):
            return [
                "state": state.rawValue
            ]
        case let .errorAction(_, action):
            return [
                "state": action.rawValue
            ]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase, .eventTracker]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PicPay - Virtual Card - \(name)", properties: properties, providers: providers)
    }
}

extension VirtualCardDeletedEvent {
    enum VirtualCardDeletedAuth: String {
        case fingerprintCancel = "act-fingerprint-cancel"
        case fingerprint = "act-fingerprint-used"
        case numeralPassword = "act-numeral-password-used"
    }
    
    enum VirtualCardDeletedAction: String {
        case close = "act-close-new-virtual-card"
        case request = "act-new-virtual-card"
    }
    
    enum VirtualCardDeletedErrorAction: String {
        case tryAgain = "act-try-again"
        case cancel = "act-cancel"
    }
    
    enum VirtualCardDeletedErrorState: String {
        case connectionFailure = "st-connection-error"
        case serviceError = "st-service-error"
    }
    
    enum VirtualCardDeletedErrorName: String {
        case connectionFailure = "Delete - No connection error"
        case serviceError = "Delete - Generic error"
    }
}
