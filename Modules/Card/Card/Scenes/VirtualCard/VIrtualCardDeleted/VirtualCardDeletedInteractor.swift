import AnalyticsModule
import Foundation
import Core

// MARK: VirtualCardDeletedInteracting
protocol VirtualCardDeletedInteracting: AnyObject {
    func viewDidLoad()
    func tryAgain()
    func request()
    func dismiss()
}

// MARK: VirtualCardDeletedInteractor
final class VirtualCardDeletedInteractor {
    // MARK: Properties
    typealias Dependencies = HasAnalytics & HasAuthManagerDependency
    private let dependencies: Dependencies

    private let service: VirtualCardDeletedServicing
    private let presenter: VirtualCardDeletedPresenting
    private let input: VirtualCardFlowInput
    
    private var currentError: ApiError?

    // MARK: Init
    init(service: VirtualCardDeletedServicing, presenter: VirtualCardDeletedPresenting, dependencies: Dependencies, input: VirtualCardFlowInput) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.input = input
    }
    
    // MARK: Private Functions
    private func authenticate() {
        dependencies.authManager.authenticate(sucessHandler: { [weak self] password, isBiometric in
            self?.dependencies.analytics.log(VirtualCardDeletedEvent.auth(action: isBiometric ? .fingerprint : .numeralPassword))
            self?.serviceDelete(with: password)
        }, cancelHandler: { [weak self] in
            self?.dependencies.analytics.log(VirtualCardDeletedEvent.auth(action: .fingerprintCancel))
            self?.presenter.didNextStep(action: .dismiss)
        })
    }
    
    private func serviceDelete(with password: String) {
        presenter.presentLoadingState()
        service.delete(password: password) { [weak self] result in
            switch result {
            case .success:
                self?.currentError = nil
                self?.presenter.didNextStep(action: .succeeded)
                self?.presenter.presentIdealState()
            case let .failure(error):
                self?.currentError = error
                switch error {
                case .connectionFailure:
                    self?.dependencies.analytics.log(VirtualCardDeletedEvent.errorState(name: .connectionFailure, state: .connectionFailure))
                default:
                    self?.dependencies.analytics.log(VirtualCardDeletedEvent.errorState(name: .serviceError, state: .serviceError))
                }
                self?.presenter.didNextStep(action: .failed)
                self?.presenter.presentErrorState(error: error)
            }
        }
    }
    
    private func logCurrentError(isTryingAgain: Bool) {
        guard let currentError = currentError else { return }
        switch currentError {
        case .connectionFailure:
            dependencies.analytics.log(VirtualCardDeletedEvent.errorAction(name: .connectionFailure, action: isTryingAgain ? .tryAgain : .cancel))
        default:
            dependencies.analytics.log(VirtualCardDeletedEvent.errorAction(name: .serviceError, action: isTryingAgain ? .tryAgain : .cancel))
        }
    }
}

// MARK: - VirtualCardDeletedInteracting
extension VirtualCardDeletedInteractor: VirtualCardDeletedInteracting {
    func viewDidLoad() {
        input.button.action == .deleted ? presenter.presentIdealState() : authenticate()
    }
    
    func tryAgain() {
        viewDidLoad()
        logCurrentError(isTryingAgain: true)
    }
    
    func request() {
        dependencies.analytics.log(VirtualCardDeletedEvent.deleted(action: .request))
        presenter.didNextStep(action: .request)
    }
    
    func dismiss() {
        if currentError != nil {
            logCurrentError(isTryingAgain: false)
        } else {
            dependencies.analytics.log(VirtualCardDeletedEvent.deleted(action: .close))
        }
        presenter.didNextStep(action: .dismiss)
    }
}
