import Core
import Foundation
import UI
import AssetsKit

protocol VirtualCardDeletedPresenting: AnyObject {
    var viewController: VirtualCardDeletedDisplaying? { get set }
    func didNextStep(action: VirtualCardDeletedAction)
    func presentIdealState()
    func presentLoadingState()
    func presentErrorState(error: ApiError)
}

final class VirtualCardDeletedPresenter {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let coordinator: VirtualCardDeletedCoordinating
    weak var viewController: VirtualCardDeletedDisplaying?

    init(coordinator: VirtualCardDeletedCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
}

// MARK: - VirtualCardDeletedPresenting
extension VirtualCardDeletedPresenter: VirtualCardDeletedPresenting {
    func didNextStep(action: VirtualCardDeletedAction) {
        coordinator.perform(action: action)
    }
    
    func presentIdealState() {
        viewController?.displayIdealState()
    }
    
    func presentLoadingState() {
        viewController?.displayLoadingState()
    }
    
    func presentErrorState(error: ApiError) {
        viewController?.displayErrorState(error: buildErrorService(for: error))
    }
}

// MARK: Error builders
extension VirtualCardDeletedPresenter {
    private func buildErrorService(for error: ApiError) -> StatefulFeedbackViewModel {
        switch error {
        case .connectionFailure:
            return StatefulFeedbackViewModel(
                image: Resources.Illustrations.iluNotFound.image,
                content: (
                    title: Strings.Default.Connection.title,
                    description: Strings.Default.Connection.verify
                ),
                button: (image: nil, title: Strings.Default.retry),
                secondaryButton: (image: nil, title: Strings.Default.cancelButtonTitle)
            )
        default:
            return StatefulFeedbackViewModel(
                image: Resources.Illustrations.iluError.image,
                content: (
                    title: Strings.VirtualCard.Deleted.Error.title,
                    description: Strings.VirtualCard.Deleted.Error.description
                ),
                button: (image: nil, title: Strings.Default.retry),
                secondaryButton: (image: nil, title: Strings.Default.cancelButtonTitle)
            )
        }
    }
}
