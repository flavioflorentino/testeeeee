import UIKit

enum VirtualCardDeletedFactory {
    static func make(delegate: VirtualCardDeletedCoordinatorDelegate, input: VirtualCardFlowInput) -> VirtualCardDeletedViewController {
        let container = DependencyContainer()
        let service: VirtualCardDeletedServicing = VirtualCardDeletedService(dependencies: container)
        let coordinator: VirtualCardDeletedCoordinating = VirtualCardDeletedCoordinator(dependencies: container)
        let presenter: VirtualCardDeletedPresenting = VirtualCardDeletedPresenter(coordinator: coordinator, dependencies: container)
        let interactor = VirtualCardDeletedInteractor(service: service, presenter: presenter, dependencies: container, input: input)
        let viewController = VirtualCardDeletedViewController(interactor: interactor)

        coordinator.delegate = delegate
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
