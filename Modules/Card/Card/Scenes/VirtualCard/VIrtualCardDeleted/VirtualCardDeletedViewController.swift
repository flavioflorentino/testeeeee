import UI
import AssetsKit

protocol VirtualCardDeletedDisplaying: AnyObject {
    func displayLoadingState()
    func displayErrorState(error: StatefulFeedbackViewModel)
    func displayIdealState()
}

final class VirtualCardDeletedViewController: ViewController<VirtualCardDeletedInteracting, UIView> {
    fileprivate enum Layout { }
    typealias Localizable = Strings.VirtualCard.Deleted
    
    private lazy var feedbackView: ApolloFeedbackView = {
        let feedbackView = ApolloFeedbackView(content: .init(image: Resources.Illustrations.iluSuccess.image,
                                                             title: Localizable.title,
                                                             description: NSAttributedString(string: Localizable.description),
                                                             primaryButtonTitle: Localizable.button,
                                                             secondaryButtonTitle: Strings.Default.close))
        feedbackView.isHidden = true
        setupCallbacks(on: feedbackView)
        return feedbackView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.viewDidLoad()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(feedbackView)
    }
    
    override func setupConstraints() {
        feedbackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}

// MARK: - VirtualCardDeletedDisplaying
extension VirtualCardDeletedViewController: VirtualCardDeletedDisplaying {
    func displayLoadingState() {
        beginState()
    }
    
    func displayErrorState(error: StatefulFeedbackViewModel) {
        endState(model: error)
    }
    
    func displayIdealState() {
        endState()
        feedbackView.isHidden = false
    }
    
    private func setupCallbacks(on feedbackView: ApolloFeedbackView) {
        feedbackView.didTapPrimaryButton = { [weak self] in
            self?.interactor.request()
        }
        
        feedbackView.didTapSecondaryButton = { [weak self] in
            self?.interactor.dismiss()
        }
    }
}

extension VirtualCardDeletedViewController: StatefulTransitionViewing {
    func statefulViewForError() -> StatefulViewing {
        StatefulFeedbackView()
    }
    
    func didTryAgain() {
        interactor.tryAgain()
    }
    
    func didTapSecondaryButton() {
        interactor.dismiss()
    }
}

extension VirtualCardDeletedViewController: StyledNavigationDisplayable {
    public var navigationBarStyle: DefaultNavigationStyle {
        ClearNavigationStyle(prefersLargeTitle: false)
    }
}
