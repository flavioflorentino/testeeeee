import Core
import Foundation

public typealias VirtualCardDeleteCompletion = (Result<NoContent, ApiError>) -> Void

protocol VirtualCardDeletedServicing {
    func delete(password: String?, completion: @escaping VirtualCardDeleteCompletion)
}

final class VirtualCardDeletedService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - VirtualCardDeletedServicing
extension VirtualCardDeletedService: VirtualCardDeletedServicing {
    func delete(password: String?, completion: @escaping VirtualCardDeleteCompletion) {
        guard let password = password else {
            completion(.failure(ApiError.unauthorized(body: RequestError())))
            return
        }
        
        let endpoint = CreditServiceEndpoint.virtualCardDelete(password: password)
        
        Api<NoContent>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
