import UIKit

public enum VirtualCardDeletedAction {
    case dismiss
    case succeeded
    case request
    case failed
}

protocol VirtualCardDeletedCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: VirtualCardDeletedCoordinatorDelegate? { get set }
    func perform(action: VirtualCardDeletedAction)
}

protocol VirtualCardDeletedCoordinatorDelegate: AnyObject {
    func deletedDidNextStep(action: VirtualCardDeletedAction)
}

final class VirtualCardDeletedCoordinator {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    weak var viewController: UIViewController?
    weak var delegate: VirtualCardDeletedCoordinatorDelegate?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - VirtualCardDeletedCoordinating
extension VirtualCardDeletedCoordinator: VirtualCardDeletedCoordinating {
    func perform(action: VirtualCardDeletedAction) {
        delegate?.deletedDidNextStep(action: action)
    }
}
