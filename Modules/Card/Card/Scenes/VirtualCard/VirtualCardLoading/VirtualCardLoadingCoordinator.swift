import UIKit

enum VirtualCardLoadingAction {
    case success(action: VirtualCardButtonAction)
    case close
}

extension VirtualCardLoadingAction: Equatable {}

protocol VirtualCardLoadingCoordinating: AnyObject {
    var delegate: VirtualCardLoadingCoordinatorDelegate? { get set }
    func perform(action: VirtualCardLoadingAction)
}

protocol VirtualCardLoadingCoordinatorDelegate: AnyObject {
    func loadingDidNextStep(action: VirtualCardLoadingAction)
}

final class VirtualCardLoadingCoordinator {
    weak var delegate: VirtualCardLoadingCoordinatorDelegate?
}

// MARK: - VirtualCardLoadingCoordinating
extension VirtualCardLoadingCoordinator: VirtualCardLoadingCoordinating {
    func perform(action: VirtualCardLoadingAction) {
        delegate?.loadingDidNextStep(action: action)
    }
}
