import UIKit

enum VirtualCardLoadingFactory {
    static func make(delegate: VirtualCardLoadingCoordinatorDelegate) -> VirtualCardLoadingViewController {
        let container = DependencyContainer()
        let service: VirtualCardLoadingServicing = VirtualCardLoadingService(dependencies: container)
        let coordinator: VirtualCardLoadingCoordinating = VirtualCardLoadingCoordinator()
        coordinator.delegate = delegate
        let presenter: VirtualCardLoadingPresenting = VirtualCardLoadingPresenter(coordinator: coordinator)
        let interactor = VirtualCardLoadingInteractor(service: service, presenter: presenter, dependencies: container)
        let viewController = VirtualCardLoadingViewController(interactor: interactor)
        presenter.viewController = viewController
        return viewController
    }
}
