import Foundation
import UI
import Core

protocol VirtualCardLoadingPresenting: AnyObject {
    var viewController: VirtualCardLoadingDisplaying? { get set }
    func presentLoadState()
    func showError(_ error: ApiError)
    func didNextStep(action: VirtualCardLoadingAction)
}

final class VirtualCardLoadingPresenter {
    private let coordinator: VirtualCardLoadingCoordinating
    weak var viewController: VirtualCardLoadingDisplaying?

    init(coordinator: VirtualCardLoadingCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - VirtualCardLoadingPresenting
extension VirtualCardLoadingPresenter: VirtualCardLoadingPresenting {
    func presentLoadState() {
        viewController?.startLoading()
    }
    
    func showError(_ error: ApiError) {
        let model = StatefulFeedbackViewModel(error)
        viewController?.displayErrorState(with: model)
    }
    
    func didNextStep(action: VirtualCardLoadingAction) {
        coordinator.perform(action: action)
    }
}
