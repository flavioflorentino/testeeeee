import UI
import UIKit

protocol VirtualCardLoadingDisplaying: AnyObject {
    func startLoading()
    func displayErrorState(with model: StatefulFeedbackViewModel)
}

final class VirtualCardLoadingViewController: ViewController<VirtualCardLoadingInteracting, UIView> {
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(true, animated: true)
        viewModel.loadData()
    }
}

// MARK: - VirtualCardLoadingDisplaying
extension VirtualCardLoadingViewController: VirtualCardLoadingDisplaying {
    func displayErrorState(with model: StatefulFeedbackViewModel) {
        endState(animated: true, model: model)
    }
    
    func startLoading() {
        beginState()
    }
}

extension VirtualCardLoadingViewController: StatefulTransitionViewing {
    func statefulViewForError() -> StatefulViewing {
        StatefulFeedbackView()
    }
    
    func didTryAgain() {
        interactor.loadData()
    }
    
    func didTapSecondaryButton() {
        interactor.dismiss()
    }
}
