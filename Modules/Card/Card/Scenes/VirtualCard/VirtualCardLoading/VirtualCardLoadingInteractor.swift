import Foundation

protocol VirtualCardLoadingInteracting: AnyObject {
    func loadData()
    func dismiss()
}

final class VirtualCardLoadingInteractor {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let service: VirtualCardLoadingServicing
    private let presenter: VirtualCardLoadingPresenting

    init(service: VirtualCardLoadingServicing, presenter: VirtualCardLoadingPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - VirtualCardLoadingInteracting
extension VirtualCardLoadingInteractor: VirtualCardLoadingInteracting {
    func loadData() {
        presenter.presentLoadState()
        service.getVirtualCardButton { [weak self] result in
            switch result {
            case .success(let response):
                self?.presenter.didNextStep(action: .success(action: response.action))
            case .failure(let apiError):
                self?.presenter.showError(apiError)
            }
        }
    }
    
    func dismiss() {
        presenter.didNextStep(action: .close)
    }
}
