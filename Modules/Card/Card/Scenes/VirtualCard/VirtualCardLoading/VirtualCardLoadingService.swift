import Core
import Foundation

protocol VirtualCardLoadingServicing {
    func getVirtualCardButton(completion: @escaping CompletionVirtualCardAction)
}

typealias CompletionVirtualCardAction = (Result<VirtualCardActionResponse, ApiError>) -> Void

final class VirtualCardLoadingService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - VirtualCardLoadingServicing
extension VirtualCardLoadingService: VirtualCardLoadingServicing {
    public func getVirtualCardButton(completion: @escaping CompletionVirtualCardAction) {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        let api = Api<VirtualCardActionResponse>(endpoint: CreditServiceEndpoint.virtualCardAction)
        api.execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map { $0.model })
            }
        }
    }
}
