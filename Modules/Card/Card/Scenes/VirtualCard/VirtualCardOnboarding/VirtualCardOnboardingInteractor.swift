import Foundation
import AnalyticsModule
import Core

public protocol VirtualCardOnboardingInteracting: AnyObject {
    func dismiss(byFeedbackPage: Bool)
    func confirm()
}

public final class VirtualCardOnboardingInteractor {
    typealias Dependencies = HasAnalytics
    typealias TrackerAction = VirtualCardOnboardingEvent.VirtualCardOnboardingAction
    private let dependencies: Dependencies
    private let presenter: VirtualCardOnboardingPresenting
    
    init(presenter: VirtualCardOnboardingPresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - VirtualCardOnboardingInteracting
extension VirtualCardOnboardingInteractor: VirtualCardOnboardingInteracting {
    public func dismiss(byFeedbackPage: Bool) {
        dependencies.analytics.log(VirtualCardOnboardingEvent.onboard(action: .dismiss))
        if byFeedbackPage {
            presenter.didNextStep(action: .dismiss)
        }
    }
    
    public func confirm() {
        presenter.didNextStep(action: .confirm)
        dependencies.analytics.log(VirtualCardOnboardingEvent.onboard(action: .confirm))
    }
}
