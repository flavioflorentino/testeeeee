import UIKit

public enum VirtualCardOnboardingAction {
    case dismiss
    case confirm
}

public protocol VirtualCardOnboardingCoordinatorDelegate: AnyObject {
    func onboardingDidNextStep(action: VirtualCardOnboardingAction)
}

public protocol VirtualCardOnboardingCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: VirtualCardOnboardingAction)
    var delegate: VirtualCardOnboardingCoordinatorDelegate? { get set }
}

public final class VirtualCardOnboardingCoordinator {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies
    public weak var viewController: UIViewController?
    public weak var delegate: VirtualCardOnboardingCoordinatorDelegate?
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - VirtualCardOnboardingCoordinating
extension VirtualCardOnboardingCoordinator: VirtualCardOnboardingCoordinating {
    public func perform(action: VirtualCardOnboardingAction) {
        delegate?.onboardingDidNextStep(action: action)
    }
}
