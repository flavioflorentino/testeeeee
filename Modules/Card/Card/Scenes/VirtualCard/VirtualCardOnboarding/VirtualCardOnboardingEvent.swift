import Foundation
import AnalyticsModule

// MARK: - VirtualCardOnboardingEvent
public enum VirtualCardOnboardingEvent: AnalyticsKeyProtocol {
    case onboard(action: VirtualCardOnboardingAction)
    
    private var name: String {
        "Virtual Card - Onboarding"
    }
    
    private var properties: [String: Any] {
        switch self {
        case .onboard(let action):
            return [
                "action": action.rawValue
            ]
        }
    }
   
    private var providers: [AnalyticsProvider] {
        [.firebase, .mixPanel]
    }
    
    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PicPay Card - \(name)", properties: properties, providers: providers)
    }
    
    public enum VirtualCardOnboardingAction: String {
        case confirm = "act-generate-virtual-card"
        case dismiss = "act-close-virtual-card"
    }
}
