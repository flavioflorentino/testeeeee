import UI
import UIKit
import SnapKit

public final class VirtualCardOnboardingViewController: ViewController<VirtualCardOnboardingInteracting, UIView> {
    private lazy var feedbackView: ApolloFeedbackView = {
        let content = ApolloFeedbackViewContent(image: Assets.icoCardOnboard.image,
                                                title: Strings.VirtualCard.Onboarding.title,
                                                description: NSAttributedString(string: Strings.VirtualCard.Onboarding.description),
                                                primaryButtonTitle: Strings.VirtualCard.Onboarding.Button.confirm,
                                                secondaryButtonTitle: Strings.VirtualCard.Onboarding.Button.dismiss)
        let feedbackView = ApolloFeedbackView(content: content, style: .confirmation)
        setupCallbacks(on: feedbackView)
        return feedbackView
    }()
    
    private func setupCallbacks(on feedbackView: ApolloFeedbackView) {
        feedbackView.didTapPrimaryButton = { [weak self] in
            self?.interactor.confirm()
        }
        
        feedbackView.didTapSecondaryButton = { [weak self] in
            self?.interactor.dismiss(byFeedbackPage: true)
        }
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override public  func willMove(toParent parent: UIViewController?) {
        if parent == nil {
            interactor.dismiss(byFeedbackPage: false)
        }
    }
    
    override public func buildViewHierarchy() {
        view.addSubview(feedbackView)
    }
    
    override public func setupConstraints() {
        feedbackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    override public func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}

extension VirtualCardOnboardingViewController: StyledNavigationDisplayable {
    public var navigationBarStyle: DefaultNavigationStyle {
        ClearNavigationStyle(prefersLargeTitle: false)
    }
}
