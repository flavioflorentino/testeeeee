import AnalyticsModule
import UIKit

public enum VirtualCardOnboardingFactory {
    public static func make(delegate: VirtualCardOnboardingCoordinatorDelegate) -> VirtualCardOnboardingViewController {
        let container = DependencyContainer()
        let coordinator: VirtualCardOnboardingCoordinating = VirtualCardOnboardingCoordinator(dependencies: container)
        let presenter: VirtualCardOnboardingPresenting = VirtualCardOnboardingPresenter(coordinator: coordinator)
        let interactor = VirtualCardOnboardingInteractor(presenter: presenter, dependencies: container)
        let viewController = VirtualCardOnboardingViewController(interactor: interactor)

        coordinator.viewController = viewController
        coordinator.delegate = delegate
        return viewController
    }
}
