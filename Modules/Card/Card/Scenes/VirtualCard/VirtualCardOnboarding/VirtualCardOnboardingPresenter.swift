import Foundation
protocol VirtualCardOnboardingPresenting: AnyObject {
    func didNextStep(action: VirtualCardOnboardingAction)
}

final class VirtualCardOnboardingPresenter {
    private let coordinator: VirtualCardOnboardingCoordinating
    
    init(coordinator: VirtualCardOnboardingCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - VirtualCardOnboardingPresenting
extension VirtualCardOnboardingPresenter: VirtualCardOnboardingPresenting {
    func didNextStep(action: VirtualCardOnboardingAction) {
        coordinator.perform(action: action)
    }
}
