import AnalyticsModule
import Core
import FeatureFlag
import Foundation

private extension ApiError {
    var isVirtualCardCustomError: Bool {
        self.requestError?.code == "virtual_card_block_code_invalid"
    }
}

public protocol VirtualCardHomeInteracting: AnyObject {
    func loadData()
    func close()
    func closeCustomError()
    func clipboardAction()
    func deleteAction()
    func openFaq()
    func openHelpChat()
    func statusWarningDidOpen()
    func statusWarningDidCancel()
    func statusErrorDidRetry()
    func statusErrorDidCancel()
    func statusChangeRequested()
}

public final class VirtualCardHomeInteractor {
    typealias Dependencies = HasAnalytics & HasFeatureManager & HasAuthManagerDependency
    private let dependencies: Dependencies
    
    private let service: VirtualCardHomeServicing
    private let presenter: VirtualCardHomePresenting
    public var virtualCardModel: VirtualCard?
    public var virtualCardIsBlocked = false
    
    init(service: VirtualCardHomeServicing, presenter: VirtualCardHomePresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
    
    private func serviceLoadData(with password: String? = nil) {
        presenter.presentLoadingState()
        service.loadData(password: password) { [weak self] result in
            guard let self = self else {
                return
            }
            switch result {
            case let .success(virtualCard):
                self.virtualCardModel = virtualCard.card
                self.virtualCardIsBlocked = virtualCard.blocked
                
                let model = VirtualCardPresenterModel(
                    delete: self.dependencies.featureManager.isActive(.opsVirtualCardDelete),
                    block: self.dependencies.featureManager.isActive(.opsVirtualCardBlock),
                    result: virtualCard
                )
                self.presenter.presentIdealState(model: model)
            case let .failure(error):
                if error.isVirtualCardCustomError {
                    self.presenter.presentCustomErrorState()
                    self.dependencies.analytics.log(VirtualCardHomeEvent.customErrorAlert(name: .invalidStatus, state: .invalidStatus))
                } else {
                    self.presenter.presentErrorState(error: error)
                }
                self.trackErrorEvent(error: error)
            }
        }
    }
    
    private func serviceChangeStatus() {
        presenter.presentLoadingState()
        service.changeStatus(isBlocked: self.virtualCardIsBlocked) { [weak self] result in
            guard let self = self else {
                return
            }
            switch result {
            case .success:
                self.virtualCardIsBlocked.toggle()
                self.presenter.presentStatus(isBlocked: self.virtualCardIsBlocked)
            case let .failure(error):
                switch error {
                case .connectionFailure:
                    self.trackErrorEvent(error: error)
                    self.presenter.presentErrorState(error: error)
                default:
                    self.dependencies.analytics.log(
                        VirtualCardHomeEvent.errorAlert(name: self.virtualCardIsBlocked ? .block : .unlock, state: self.virtualCardIsBlocked ? .block : .unlock)
                    )
                    self.presenter.presentStatusError(isBlocked: self.virtualCardIsBlocked)
                }
            }
        }
    }
    
    private func trackErrorEvent(error: ApiError) {
        switch error {
        case .connectionFailure:
            dependencies.analytics.log(VirtualCardHomeEvent.state(state: .connectionFailure))
        default:
            dependencies.analytics.log(VirtualCardHomeEvent.state(state: .error))
        }
    }
}

// MARK: - VirtualCardHomeInteracting
extension VirtualCardHomeInteractor: VirtualCardHomeInteracting {
    public func loadData() {
        dependencies.authManager.authenticate(sucessHandler: { [weak self] password, isBiometric in
            self?.dependencies.analytics.log(VirtualCardHomeEvent.auth(action: isBiometric ? .fingerprint : .numeralPassword))
            self?.serviceLoadData(with: password)
        }, cancelHandler: {
            self.dependencies.analytics.log(VirtualCardHomeEvent.auth(action: .fingerprintCancel))
            self.presenter.didNextStep(action: .dismiss)
        })
    }
    
    public func close() {
        dependencies.analytics.log(
            VirtualCardHomeEvent.generated(name: virtualCardIsBlocked ? .blocked : .generated, action: .close)
        )
    }
    
    public func openHelpChat() {
        dependencies.analytics.log(
            VirtualCardHomeEvent.customErrorAlertAction(name: .invalidStatus, action: .callCenterChat)
        )
        presenter.didNextStep(action: .openHelpChat)
    }
    
    public func closeCustomError() {
        dependencies.analytics.log(
            VirtualCardHomeEvent.customErrorAlertAction(name: .invalidStatus, action: .close)
        )
        presenter.didNextStep(action: .dismiss)
    }
    
    public func clipboardAction() {
        dependencies.analytics.log(
            VirtualCardHomeEvent.generated(name: virtualCardIsBlocked ? .blocked : .generated, action: .clipboard)
        )
        
        guard let cardNumber = virtualCardModel?.number, cardNumber.isNotEmpty else {
            presenter.presentFeatureErrorState()
            return
        }
        
        let pasteboard = UIPasteboard.general
        pasteboard.string = cardNumber
        presenter.presentClipboardFeedback()
    }
    
    public func deleteAction() {
        dependencies.analytics.log(
            VirtualCardHomeEvent.generated(name: virtualCardIsBlocked ? .blocked : .generated, action: .delete)
        )
        presenter.didNextStep(action: .delete)
    }
    
    public func openFaq() {
        dependencies.analytics.log(
            VirtualCardHomeEvent.generated(name: virtualCardIsBlocked ? .blocked : .generated, action: .faq)
        )
        
        guard let faqURL = URL(string: dependencies.featureManager.text(.opsVirtualCardFaq)) else {
            presenter.presentFeatureErrorState()
            return
        }
        
        presenter.didNextStep(action: .faq(url: faqURL))
    }
    
    // MARK: - Block and Unlock
    public func statusWarningDidOpen() {
        dependencies.analytics.log(
            VirtualCardHomeEvent.generated(name: virtualCardIsBlocked ? .blocked : .generated, action: .blockWarning)
        )
        presenter.presentStatusWarning(isBlocked: virtualCardIsBlocked)
    }
    
    public func statusWarningDidCancel() {
        dependencies.analytics.log(
            virtualCardIsBlocked ? VirtualCardHomeEvent.dialogUnfreeze(action: .cancel) : VirtualCardHomeEvent.dialogFreeze(action: .cancel)
        )
    }
    
    public func statusErrorDidRetry() {
        dependencies.analytics.log(
            VirtualCardHomeEvent.errorAlertAction(name: virtualCardIsBlocked ? .block : .unlock, action: .retry)
        )
        serviceChangeStatus()
    }
    
    public func statusErrorDidCancel() {
        dependencies.analytics.log(
            VirtualCardHomeEvent.errorAlertAction(name: virtualCardIsBlocked ? .block : .unlock, action: .cancel)
        )
    }
    
    public func statusChangeRequested() {
        dependencies.analytics.log(
            virtualCardIsBlocked ? VirtualCardHomeEvent.dialogUnfreeze(action: .unlock) : VirtualCardHomeEvent.dialogFreeze(action: .block)
        )
        serviceChangeStatus()
    }
}
