import UIKit

public enum VirtualCardHomeFactory {
    public static func make(delegate: VirtualCardHomeCoordinatorDelegate, input: VirtualCardFlowInput) -> VirtualCardHomeViewController {
        let container = DependencyContainer()
        let service: VirtualCardHomeServicing = VirtualCardHomeService(dependencies: container, input: input)
        let coordinator: VirtualCardHomeCoordinating = VirtualCardHomeCoordinator(dependencies: container)
        let presenter: VirtualCardHomePresenting = VirtualCardHomePresenter(coordinator: coordinator, dependencies: container)
        let interactor = VirtualCardHomeInteractor(service: service, presenter: presenter, dependencies: container)
        let viewController = VirtualCardHomeViewController(interactor: interactor)

        coordinator.viewController = viewController
        coordinator.delegate = delegate
        presenter.viewController = viewController

        return viewController
    }
}
