import Core
import UI
import AssetsKit
import FeatureFlag
import AnalyticsModule

public protocol VirtualCardHomePresenting: AnyObject {
    var viewController: VirtualCardHomeDisplaying? { get set }
    func didNextStep(action: VirtualCardHomeAction)
    func presentClipboardFeedback()
    func presentLoadingState()
    func presentFeatureErrorState()
    func presentErrorState(error: ApiError)
    func presentCustomErrorState()
    func presentIdealState(model: VirtualCardPresenterModel)
    func presentStatusWarning(isBlocked: Bool)
    func presentStatusError(isBlocked: Bool)
    func presentStatus(isBlocked: Bool)
}

public final class VirtualCardHomePresenter {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let coordinator: VirtualCardHomeCoordinating
    public weak var viewController: VirtualCardHomeDisplaying?

    init(coordinator: VirtualCardHomeCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
}

// MARK: - VirtualCardHomePresenting
extension VirtualCardHomePresenter: VirtualCardHomePresenting {
    public func didNextStep(action: VirtualCardHomeAction) {
        coordinator.perform(action: action)
    }
    
    public func presentClipboardFeedback() {
        viewController?.displayClipboardFeedback()
    }
    
    public func presentLoadingState() {
        viewController?.displayLoadingState()
    }
    
    public func presentFeatureErrorState() {
        viewController?.displayErrorState(error: buildErrorService(for: .bodyNotFound))
    }
    
    public func presentErrorState(error: ApiError) {
        let error = buildErrorService(for: error)
        viewController?.displayErrorState(error: error)
    }
    
    public func presentCustomErrorState() {
        let customErrorView = ApolloFeedbackView(
            content: .init(
                image: Resources.Illustrations.iluError.image,
                title: Strings.VirtualCard.Home.Error.title,
                description: NSAttributedString(string: Strings.VirtualCard.Home.CustomError.subtitle),
                primaryButtonTitle: Strings.VirtualCard.Home.CustomError.buttonTitle,
                secondaryButtonTitle: Strings.Default.close)
        )

        viewController?.displayCustomErrorView(customErrorView)
    }
    
    public func presentIdealState(model: VirtualCardPresenterModel) {
        guard let display = buildVirtualCardDisplayModel(model: model) else {
            presentFeatureErrorState()
            return
        }
        
        viewController?.displayIdealState(model: display)
        coordinator.perform(action: .succeeded)
    }
    
    public func presentStatusWarning(isBlocked: Bool) {
        viewController?.displayStatusWarning(actionSheet: buildStatusWarning(isBlocked), action: buildStatusWarningAction(isBlocked))
    }
    
    public func presentStatusError(isBlocked: Bool) {
        viewController?.displayStatusError(popup: buildStatusAlertError(isBlocked))
    }
    
    public func presentStatus(isBlocked: Bool) {
        viewController?.displayStatus(isBlocked: isBlocked)
    }
}

// MARK: Error builders
extension VirtualCardHomePresenter {
    private func buildErrorService(for error: ApiError) -> StatefulFeedbackViewModel {
        switch error {
        case .connectionFailure:
            return StatefulFeedbackViewModel(
                image: Resources.Illustrations.iluNotFound.image,
                content: (
                    title: Strings.Default.Connection.title,
                    description: Strings.Default.Connection.verify
                ),
                button: (image: nil, title: Strings.Default.retry),
                secondaryButton: (image: nil, title: Strings.Default.cancelButtonTitle)
            )
        default:
            return StatefulFeedbackViewModel(
                image: Resources.Illustrations.iluError.image,
                content: (
                    title: Strings.VirtualCard.Home.Error.title,
                    description: Strings.VirtualCard.Home.Error.description
                ),
                button: (image: nil, title: Strings.Default.retry),
                secondaryButton: (image: nil, title: Strings.Default.cancelButtonTitle)
            )
        }
    }
}

// MARK: VirtualCard Builder
extension VirtualCardHomePresenter {
    private func buildVirtualCardDisplayModel(model: VirtualCardPresenterModel) -> VirtualCardDisplayModel? {
        guard let card = buildVirtualCardViewModel(card: model.result.card) else {
            return nil
        }
        
        return VirtualCardDisplayModel(
            card: card,
            isClipboardHidden: buildClipboardHidden(model: model),
            isDeleteHidden: buildDeleteHidden(model: model),
            isBlockHidden: buildBlockHidden(model: model),
            isBlocked: buildBlocked(model: model)
        )
    }
    
    private func buildVirtualCardViewModel(card: VirtualCard) -> VirtualCardViewModel? {
        guard let cvv = buildCVV(card: card),
              let date = buildExpirationDate(card: card),
              let name = buildName(card: card),
              let number = buildNumber(card: card) else {
            return nil
        }
        return VirtualCardViewModel(cvv: cvv, expiration: date, name: name, number: number)
    }
    
    private func buildCVV(card: VirtualCard) -> String? {
        guard let cardCVV = card.cvv, cardCVV.count == 3 else {
            return nil
        }
        return cardCVV
    }
    
    private func buildExpirationDate(card: VirtualCard) -> String? {
        guard let cardExp = card.expiration,
           let date = Date.decodableFormatter.date(from: cardExp) else {
            return nil
        }
        return date.formatted(in: .monthAndYearReduced)
    }
    
    private func buildName(card: VirtualCard) -> String? {
        guard let cardName = card.name, cardName.isNotEmpty else {
            return nil
        }
        return cardName
    }
    
    private func buildNumber(card: VirtualCard) -> String? {
        guard var cardNumber = card.number, cardNumber.count == 16 else {
            return nil
        }
        
        cardNumber.insert(contentsOf: "  ", at: cardNumber.index(cardNumber.startIndex, offsetBy: 4))
        cardNumber.insert(contentsOf: "  ", at: cardNumber.index(cardNumber.startIndex, offsetBy: 10))
        cardNumber.insert(contentsOf: "  ", at: cardNumber.index(cardNumber.startIndex, offsetBy: 16))
        return cardNumber
    }
    
    private func buildClipboardHidden(model: VirtualCardPresenterModel) -> Bool {
        model.result.blocked
    }
    
    private func buildDeleteHidden(model: VirtualCardPresenterModel) -> Bool {
        !model.delete
    }
    
    private func buildBlockHidden(model: VirtualCardPresenterModel) -> Bool {
        !(model.block && model.result.blockEnabled)
    }
    
    private func buildBlocked(model: VirtualCardPresenterModel) -> Bool {
        model.result.blocked
    }
}

// MARK: AlertControllers Builder
extension VirtualCardHomePresenter {
    private func buildStatusWarning(_ isBlocked: Bool) -> UIAlertController {
        UIAlertController(
            title: isBlocked ? Strings.VirtualCard.Unlock.ActionSheet.title : Strings.VirtualCard.Block.ActionSheet.title,
            message: isBlocked ? Strings.VirtualCard.Unlock.ActionSheet.message : Strings.VirtualCard.Block.ActionSheet.message,
            preferredStyle: .actionSheet
        )
    }
    
    private func buildStatusWarningAction(_ isBlocked: Bool) -> String {
        isBlocked ? Strings.VirtualCard.Unlock.ActionSheet.action : Strings.VirtualCard.Block.ActionSheet.action
    }
    
    private func buildStatusAlertError(_ isBlocked: Bool) -> PopupViewController {
        PopupViewController(
            title: isBlocked ? Strings.VirtualCard.Unlock.Alert.Error.title : Strings.VirtualCard.Block.Alert.Error.title,
            description: isBlocked ? Strings.VirtualCard.Unlock.Alert.Error.message : Strings.VirtualCard.Block.Alert.Error.message,
            image: Resources.Illustrations.iluError.image
        )
    }
}
