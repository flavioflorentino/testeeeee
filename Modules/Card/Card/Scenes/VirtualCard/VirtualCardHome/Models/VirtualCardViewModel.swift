import Foundation

public struct VirtualCardViewModel {
    let cvv: String
    let expiration: String
    let name: String
    let number: String
}

public struct VirtualCardPresenterModel {
    let delete: Bool
    let block: Bool
    let result: VirtualCardCriptoResult
}

public struct VirtualCardDisplayModel {
    let card: VirtualCardViewModel
    let isClipboardHidden: Bool
    let isDeleteHidden: Bool
    let isBlockHidden: Bool
    let isBlocked: Bool
}
