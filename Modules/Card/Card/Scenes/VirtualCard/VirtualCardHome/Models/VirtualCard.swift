import Foundation

public struct VirtualCardResult: Codable {
    public let info: String?
    public let blocked: Bool
    public let blockEnabled: Bool
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        info = try container.decodeIfPresent(String.self, forKey: .info) ?? ""
        blocked = try container.decodeIfPresent(Bool.self, forKey: .blocked) ?? false
        blockEnabled = try container.decodeIfPresent(Bool.self, forKey: .blockEnabled) ?? false
    }
    
    public init(info: String?, blocked: Bool, blockEnabled: Bool) {
        self.info = info
        self.blocked = blocked
        self.blockEnabled = blockEnabled
    }
}

public struct VirtualCard: Codable {
    public let number: String?
    public let cvv: String?
    public let expiration: String?
    public let name: String?
    
    public enum CodingKeys: String, CodingKey {
        case number = "card_number"
        case cvv = "cvv"
        case expiration = "expiration_date"
        case name = "name"
    }
}

public struct VirtualCardCriptoResult {
    public let card: VirtualCard
    public let blocked: Bool
    public let blockEnabled: Bool
}
