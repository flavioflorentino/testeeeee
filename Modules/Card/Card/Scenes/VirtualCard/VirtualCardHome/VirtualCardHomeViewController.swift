import UI
import UIKit

public protocol VirtualCardHomeDisplaying: AnyObject {
    func displayClipboardFeedback()
    func displayLoadingState()
    func displayErrorState(error: StatefulFeedbackViewModel)
    func displayCustomErrorView(_ customErrorView: ApolloFeedbackView)
    func displayIdealState(model: VirtualCardDisplayModel)
    func displayStatusWarning(actionSheet: UIAlertController, action: String)
    func displayStatusError(popup: PopupViewController)
    func displayStatus(isBlocked: Bool)
}

private extension VirtualCardHomeViewController.Layout {
    enum Size {
        static let card = CGSize(width: 0.0, height: 224.0)
    }
}

public final class VirtualCardHomeViewController: ViewController<VirtualCardHomeInteracting, UIView> {
    fileprivate enum Layout { }
    
    private lazy var successContainerView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            descriptionLabel,
            SpacerView(size: Spacing.base03),
            cardView,
            SpacerView(size: Spacing.base01),
            clipboardButton,
            SpacerView(size: Spacing.base04),
            blockButton,
            SpacerView(size: Spacing.base02),
            deleteButton,
            SpacerView(size: Spacing.base02)
        ])
        stackView.isHidden = true
        stackView.axis = .vertical
        stackView.alignment = .top
        return stackView
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.VirtualCard.Home.description
        label.labelStyle(BodyPrimaryLabelStyle()).with(\.textColor, .grayscale600())
        return label
    }()
    
    private lazy var cardView: VirtualCardView = {
        let view = VirtualCardView()
        view.delegate = self
        return view
    }()
    
    private lazy var clipboardButton: VirtualCardClipboardButton = {
        let button = VirtualCardClipboardButton()
        button.addTarget(self, action: #selector(didTapClipboardButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var blockButton: VirtualCardSwitchActionView = {
        let view = VirtualCardSwitchActionView()
        view.iconImage.image = Assets.icoSettingsBlock.image
        view.titleLabel.text = Strings.VirtualCard.Block.Button.title
        view.delegate = self
        return view
    }()
    
    private lazy var deleteButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.VirtualCard.Home.Cancel.title, for: .normal)
        button.addTarget(self, action: #selector(didTapDeleteButton), for: .touchUpInside)
        button.buttonStyle(LinkButtonStyle())
        return button
    }()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        interactor.loadData()
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isModal {
            navigationController?.setNavigationBarHidden(false, animated: true)
        }
    }
    
    override public func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        super.dismiss(animated: flag, completion: completion)
        interactor.close()
    }
    
    override public  func willMove(toParent parent: UIViewController?) {
        if parent == nil {
            interactor.close()
        }
    }
    
    override public func buildViewHierarchy() {
        view.addSubview(successContainerView)
    }
    
    override public func setupConstraints() {
        successContainerView.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top)
            $0.leading.trailing.equalToSuperview()
        }
        descriptionLabel.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.height.equalTo(Spacing.base03)
        }
        cardView.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.compatibleSafeArea.bottom).offset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.height.equalTo(Layout.Size.card.height)
        }
        clipboardButton.snp.makeConstraints {
            $0.top.equalTo(cardView.compatibleSafeArea.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.height.equalTo(Spacing.base06)
        }
        blockButton.snp.makeConstraints {
            $0.top.equalTo(clipboardButton.compatibleSafeArea.bottom).offset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.height.equalTo(Spacing.base08)
        }
        deleteButton.snp.makeConstraints {
            $0.top.equalTo(blockButton.compatibleSafeArea.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.height.equalTo(Spacing.base08)
        }
    }

    override public func configureViews() {
        addFAQButton()
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        title = Strings.VirtualCard.Home.title
    }
    
    // MARK: - Private Methods Helpers
    private func addFAQButton() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: Assets.icoCirclefaq.image,
            style: .plain,
            target: self,
            action: #selector(didTapFAQ)
        )
    }
    
    // MARK: - Button Actions
    @objc
    func didTapFAQ() {
        interactor.openFaq()
    }
    
    @objc
    func didTapClipboardButton() {
        interactor.clipboardAction()
    }
    
    @objc
    func didTapDeleteButton() {
        interactor.deleteAction()
    }
}

// MARK: AlertControllers
extension VirtualCardHomeViewController {
    private func presentActionSheet(actionSheet: UIAlertController, action: String) {
        let request = UIAlertAction(title: action, style: .default, handler: { [weak self] _ in
            self?.interactor.statusChangeRequested()
        })

        let cancel = UIAlertAction(title: Strings.Default.cancelButtonTitle, style: .cancel) { [weak self] _ in
            self?.interactor.statusWarningDidCancel()
        }

        actionSheet.addAction(request)
        actionSheet.addAction(cancel)
        present(actionSheet, animated: true)
    }
    
    private func presentAlert(popup: PopupViewController) {
        let retry = PopupAction(title: Strings.Default.retry, style: .tryAgain) {
            [weak self] in
            self?.interactor.statusErrorDidRetry()
        }
        let cancel = PopupAction(title: Strings.Default.cancelButtonTitle, style: .link) {
            [weak self] in
            self?.interactor.statusErrorDidCancel()
        }
        
        popup.addAction(retry)
        popup.addAction(cancel)
        showPopup(popup)
    }
}

// MARK: - VirtualCardHomeDisplaying
extension VirtualCardHomeViewController: VirtualCardHomeDisplaying {
    public func displayClipboardFeedback() {
        clipboardButton.isSelected.toggle()
        Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false) { _ in
            self.clipboardButton.isSelected.toggle()
        }
    }
    
    public func displayLoadingState() {
        successContainerView.isHidden = true
        beginState(animated: true, model: StateLoadingViewModel(message: Strings.Default.loading))
    }
    
    public func displayErrorState(error: StatefulFeedbackViewModel) {
        successContainerView.isHidden = true
        endState(model: error)
    }
    
    public func displayCustomErrorView(_ customErrorView: ApolloFeedbackView) {
        view.addSubview(customErrorView)
        customErrorView.snp.makeConstraints { $0.edges.size.equalToSuperview() }
        
        customErrorView.didTapPrimaryButton = { [weak self] in
            self?.interactor.openHelpChat()
        }
        customErrorView.didTapSecondaryButton = { [weak self] in
            self?.interactor.closeCustomError()
        }
    }
    
    public func displayIdealState(model: VirtualCardDisplayModel) {
        cardView.setupData(model: model)
        clipboardButton.isHidden = model.isClipboardHidden
        deleteButton.isHidden = model.isDeleteHidden
        blockButton.isHidden = model.isBlockHidden
        blockButton.setStatus(isBlocked: model.isBlocked)
        
        successContainerView.isHidden = false
        endState()
    }
    
    public func displayStatusWarning(actionSheet: UIAlertController, action: String) {
        presentActionSheet(actionSheet: actionSheet, action: action)
    }

    public func displayStatusError(popup: PopupViewController) {
        endState()
        successContainerView.isHidden = false
        presentAlert(popup: popup)
    }
    
    public func displayStatus(isBlocked: Bool) {
        endState()
        blockButton.setStatus(isBlocked: isBlocked)
        cardView.setStatus(isBlocked: isBlocked)
        clipboardButton.isHidden = isBlocked
        successContainerView.isHidden = false
    }
}

extension VirtualCardHomeViewController: StatefulTransitionViewing {
    public func didTryAgain() {
        interactor.loadData()
    }
    
    public func didTapSecondaryButton() {
        interactor.close()
    }
    
    public func statefulViewForError() -> StatefulViewing {
        StatefulFeedbackView()
    }
}

extension VirtualCardHomeViewController: StyledNavigationDisplayable {}

extension VirtualCardHomeViewController: VirtualCardSwitchActionViewDelegate {
    public func viewDidTap() {
        interactor.statusWarningDidOpen()
    }
}

extension VirtualCardHomeViewController: VirtualCardViewDelegate {
    public func blockedViewDidTap() {
        interactor.statusWarningDidOpen()
    }
}
