import Core
import Foundation

public typealias VirtualCardServiceCompletion = (Result<NoContent, ApiError>) -> Void
public typealias VirtualCardLoadDataCompletion = (Result<VirtualCardCriptoResult, ApiError>) -> Void

public protocol VirtualCardHomeServicing {
    func loadData(password: String?, completion: @escaping VirtualCardLoadDataCompletion)
    func changeStatus(isBlocked: Bool, completion: @escaping VirtualCardServiceCompletion)
}

public final class VirtualCardHomeService {
    typealias Dependencies = HasMainQueue & HasCardConsumerManager
    private let dependencies: Dependencies
    private let input: VirtualCardFlowInput
    init(dependencies: Dependencies, input: VirtualCardFlowInput) {
        self.dependencies = dependencies
        self.input = input
    }
    
    private func execute(endpoint: CreditServiceEndpoint, completion: @escaping VirtualCardLoadDataCompletion) {
        Api<VirtualCardResult>(endpoint: endpoint).execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case .success(let virtualCardResult):
                    self?.buildVirtualCard(with: virtualCardResult.model) { result in
                        completion(result)
                    }
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
    
    private func executeStatus(isBlocked: Bool, completion: @escaping VirtualCardServiceCompletion) {
        let endpoint = isBlocked ? CreditServiceEndpoint.virtualCardUnlock : CreditServiceEndpoint.virtualCardBlock
        Api<NoContent>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}

// MARK: - VirtualCardHomeServicing
extension VirtualCardHomeService: VirtualCardHomeServicing {
    public func loadData(password: String?, completion: @escaping VirtualCardLoadDataCompletion) {
        guard let password = password else {
            completion(.failure(ApiError.unauthorized(body: RequestError())))
            return
        }
        if input.button.action == .onboarding || input.button.action == .deleted {
            execute(endpoint: CreditServiceEndpoint.virtualCardRequest(password: password), completion: completion)
        } else {
            execute(endpoint: CreditServiceEndpoint.virtualCardInfo(password: password), completion: completion)
        }
    }
    
    public func changeStatus(isBlocked: Bool, completion: @escaping VirtualCardServiceCompletion) {
        executeStatus(isBlocked: isBlocked, completion: completion)
    }
}

// MARK: - Decrypt and build virtual card
extension VirtualCardHomeService {
    internal func buildVirtualCard(with virtualCardResult: VirtualCardResult, completion: @escaping VirtualCardLoadDataCompletion) {
        let decrypt = VirtualCardHomeDecrypt(token: dependencies.consumer.token, consumerId: dependencies.consumer.consumerId)
        if let json = decrypt.decryptInfo(with: virtualCardResult), let jsonData = json.data(using: .utf8) {
            do {
                let card = try JSONDecoder().decode(VirtualCard.self, from: jsonData)
                let criptoResult = VirtualCardCriptoResult(card: card, blocked: virtualCardResult.blocked, blockEnabled: virtualCardResult.blockEnabled)
                completion(.success(criptoResult))
            } catch {
                completion(.failure(ApiError.otherErrors(body: RequestError())))
            }
        } else {
            completion(.failure(ApiError.otherErrors(body: RequestError())))
        }
    }
}
