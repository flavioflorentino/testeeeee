import UI
import UIKit

public protocol VirtualCardViewDelegate: AnyObject {
    func blockedViewDidTap()
}

private extension VirtualCardView.Layout {
    enum Size {
        static let ppLogo = CGSize(width: 87.0, height: 30.0)
        static let brandLogo = CGSize(width: 60.0, height: 36.0)
        static let expiration = CGSize(width: 136.0, height: 0.0)
        static let cvv = CGSize(width: 48.0, height: 0.0)
        static let blocked = CGSize(width: 134.0, height: 134.0)
    }
    enum Offset {
        static let block: CGFloat = 12.0
    }
    enum CornerRadius {
        static let blocked: CGFloat = Size.blocked.width / 2
    }
}

public final class VirtualCardView: UIView, ViewConfiguration {
    fileprivate enum Layout { }
    
    public weak var delegate: VirtualCardViewDelegate?

    private lazy var cardView = UIView()
    private lazy var cardImageView = UIImageView(image: Assets.icoCardBackground.image)
    private lazy var ppLogoImageView = UIImageView(image: Assets.icoLogoPp.image)
    private lazy var brandLogoImageView = UIImageView(image: Assets.icoLogoMc.image)
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.VirtualCard.Home.Card.name
        label.labelStyle(BodySecondaryLabelStyle()).with(\.textColor, .grayscale200(.light))
        return label
    }()
    
    private lazy var nameValueLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight)).with(\.textColor, .white(.light))
        return label
    }()
    
    private lazy var numberLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.VirtualCard.Home.Card.number
        label.labelStyle(BodySecondaryLabelStyle()).with(\.textColor, .grayscale200(.light))
        return label
    }()
    
    private lazy var numberValueLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight)).with(\.textColor, .white(.light))
        return label
    }()
    
    private lazy var expirationLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.VirtualCard.Home.Card.expiration
        label.labelStyle(BodySecondaryLabelStyle()).with(\.textColor, .grayscale200(.light))
        return label
    }()
    
    private lazy var expirationValueLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight)).with(\.textColor, .white(.light))
        return label
    }()
    
    private lazy var cvvLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.VirtualCard.Home.Card.cvv
        label.labelStyle(BodySecondaryLabelStyle()).with(\.textColor, .grayscale200(.light))
        return label
    }()
    
    private lazy var cvvValueLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight)).with(\.textColor, .white(.light))
        return label
    }()
    
    private lazy var blockedImageView: VirtualCardBlockedView = {
        let view = VirtualCardBlockedView()
        view.layer.cornerRadius = Layout.CornerRadius.blocked
        view.delegate = self
        return view
    }()
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func buildViewHierarchy() {
        cardView.addSubviews(
            cardImageView,
            ppLogoImageView,
            brandLogoImageView,
            nameLabel,
            nameValueLabel,
            numberLabel,
            numberValueLabel,
            expirationLabel,
            expirationValueLabel,
            cvvLabel,
            cvvValueLabel
        )
        addSubviews(cardView, blockedImageView)
    }
    
    public func setupConstraints() {
        setupContraintsImages()
        setupConstraintsNameNumber()
        setupConstraintsExpirationCvv()
    }
    
    private func setupContraintsImages() {
        cardView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        cardImageView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        ppLogoImageView.snp.makeConstraints {
            $0.top.leading.equalToSuperview().offset(Spacing.base02)
            $0.size.equalTo(Layout.Size.ppLogo)
        }
        brandLogoImageView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.size.equalTo(Layout.Size.brandLogo)
        }
        blockedImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.blocked)
            $0.center.equalToSuperview()
        }
    }
    private func setupConstraintsNameNumber() {
        nameLabel.snp.makeConstraints {
            $0.top.equalTo(ppLogoImageView.compatibleSafeArea.bottom).offset(Spacing.base04)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        nameValueLabel.snp.makeConstraints {
            $0.top.equalTo(nameLabel.compatibleSafeArea.bottom)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        numberLabel.snp.makeConstraints {
            $0.top.equalTo(nameValueLabel.compatibleSafeArea.bottom).offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        numberValueLabel.snp.makeConstraints {
            $0.top.equalTo(numberLabel.compatibleSafeArea.bottom)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
    }
    
    private func setupConstraintsExpirationCvv() {
        expirationLabel.snp.makeConstraints {
            $0.top.equalTo(numberValueLabel.compatibleSafeArea.bottom).offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.width.equalTo(Layout.Size.expiration.width)
        }
        expirationValueLabel.snp.makeConstraints {
            $0.top.equalTo(expirationLabel.compatibleSafeArea.bottom)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.width.equalTo(Layout.Size.expiration.width)
        }
        cvvLabel.snp.makeConstraints {
            $0.top.equalTo(numberValueLabel.compatibleSafeArea.bottom).offset(Spacing.base01)
            $0.leading.equalTo(expirationValueLabel.compatibleSafeArea.trailing).offset(Layout.Offset.block)
            $0.width.equalTo(Layout.Size.cvv.width)
        }
        cvvValueLabel.snp.makeConstraints {
            $0.top.equalTo(cvvLabel.compatibleSafeArea.bottom)
            $0.leading.equalTo(cvvLabel.compatibleSafeArea.leading)
            $0.width.equalTo(Layout.Size.cvv.width)
        }
    }
    
    public func setupData(model: VirtualCardDisplayModel) {
        nameValueLabel.text = model.card.name
        numberValueLabel.text = model.card.number
        expirationValueLabel.text = model.card.expiration
        cvvValueLabel.text = model.card.cvv
        setStatus(isBlocked: model.isBlocked)
    }
    
    public func setStatus(isBlocked: Bool) {
        cardView.opacity = isBlocked ? .medium : .full
        blockedImageView.isHidden = !isBlocked
    }
}

extension VirtualCardView: VirtualCardBlockedViewDelegate {
    public func blockedViewDidTap() {
        delegate?.blockedViewDidTap()
    }
}
