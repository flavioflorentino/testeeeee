import UI
import UIKit

public protocol VirtualCardBlockedViewDelegate: AnyObject {
    func blockedViewDidTap()
}

private extension VirtualCardBlockedView.Layout {
    enum Size {
        static let groupView = CGSize(width: 130.0, height: 75.0)
        static let iconImageVIew = CGSize(width: 35.0, height: 35.0)
    }
}

public final class VirtualCardBlockedView: UIView, ViewConfiguration {
    fileprivate enum Layout { }

    public weak var delegate: VirtualCardBlockedViewDelegate?

    private lazy var groupView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            iconImageView,
            SpacerView(size: Spacing.base00),
            descriptionLabel
        ])
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .center
        stackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(groupViewAction)))
        return stackView
    }()

    private lazy var iconImageView = UIImageView(image: Assets.icoVirtualcardBlockedViewIcon.image)

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.VirtualCard.Block.View.Icon.message
        label.labelStyle(CaptionLabelStyle(type: .highlight)).with(\.textColor, .white()).with(\.textAlignment, .center)
        return label
    }()

    override public init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func buildViewHierarchy() {
        addSubview(groupView)
    }

    public func setupConstraints() {
        groupView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.groupView)
            $0.center.equalToSuperview()
        }
        iconImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.iconImageVIew)
        }
    }

    public func configureViews() {
        backgroundColor = Colors.black.color
    }

    @objc
    private func groupViewAction() {
        delegate?.blockedViewDidTap()
    }
}
