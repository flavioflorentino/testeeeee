import UI
import UIKit

public protocol VirtualCardSwitchActionViewDelegate: AnyObject {
    func viewDidTap()
}

public final class VirtualCardSwitchActionView: UIView, ViewConfiguration {
    public weak var delegate: VirtualCardSwitchActionViewDelegate?

    public lazy var iconImage = UIImageView()

    private lazy var groupLabels: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            titleLabel,
            SpacerView(size: Spacing.base00),
            descriptionLabel
        ])
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .leading
        return stackView
    }()

    public lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .default)).with(\.textColor, .grayscale850())
        return label
    }()

    public lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle(type: .default)).with(\.textColor, .grayscale700())
        return label
    }()

    public lazy var switchView: UISwitch = {
        let view = UISwitch()
        view.onTintColor = Colors.branding300.color
        view.set(width: 32.0, height: 19.0)
        return view
    }()

    public lazy var actionButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(didTapActionButton), for: .touchUpInside)
        button.setTitle(nil, for: .normal)
        return button
    }()

    // ViewConfiguration
    public func buildViewHierarchy() {
        addSubviews(iconImage, groupLabels, switchView, actionButton)
    }

    public func setupConstraints() {
        iconImage.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
        }
        groupLabels.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalTo(iconImage.compatibleSafeArea.trailing).offset(Spacing.base01)
        }
        switchView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalTo(groupLabels.compatibleSafeArea.trailing).offset(Spacing.base01)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        actionButton.snp.makeConstraints {
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }

    // Initialization
    override public init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
        setupLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // Layout
    override public func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        setupLayout()
    }

    private func setupLayout() {
        setupLayer()
        setupColors()
    }

    private func setupLayer() {
        layer.masksToBounds = false
        layer.shadowColor = Colors.grayscale100.change(.dark, to: .clear).color.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 2)
        layer.shadowOpacity = 1.0
        layer.borderColor = Colors.grayscale100.change(.dark, to: .clear).color.cgColor
        layer.borderWidth = Border.light
        layer.cornerRadius = CornerRadius.medium
    }

    private func setupColors() {
        backgroundColor = Colors.backgroundPrimary.change(.dark, to: Colors.backgroundTertiary.darkColor).color
    }

    @objc
    func didTapActionButton() {
        delegate?.viewDidTap()
    }

    public func setStatus(isBlocked: Bool) {
        typealias Localizable = Strings.VirtualCard.Block.ButtonDescription
        descriptionLabel.text = isBlocked ? Localizable.blocked : Localizable.unlocked
        switchView.isOn = isBlocked
    }
}

extension UISwitch {
    func set(width: CGFloat, height: CGFloat) {
        let standardHeight: CGFloat = 31
        let standardWidth: CGFloat = 51

        let heightRatio = height / standardHeight
        let widthRatio = width / standardWidth

        transform = CGAffineTransform(scaleX: widthRatio, y: heightRatio)
    }
}
