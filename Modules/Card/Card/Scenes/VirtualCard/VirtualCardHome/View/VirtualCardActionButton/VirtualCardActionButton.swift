import UI
import UIKit

public final class VirtualCardActionButton: UIButton {
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        setupLayout()
    }
    
    private func setupLayout() {
        layer.masksToBounds = false
        layer.shadowColor = Colors.grayscale100.change(.dark, to: .clear).color.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 2)
        layer.shadowOpacity = 1.0
        layer.borderColor = Colors.grayscale100.change(.dark, to: .clear).color.cgColor
        layer.borderWidth = Border.light
        layer.cornerRadius = CornerRadius.medium
        
        let backgroundColor = Colors.backgroundTertiary.change(.dark, to: Colors.backgroundTertiary.darkColor).color
        setBackgroundColor(backgroundColor, for: .normal)
        setBackgroundColor(Colors.backgroundTertiary.color, for: .highlighted)
        
        imageEdgeInsets.left = Spacing.base02
        
        setTitleColor(Colors.grayscale850.color, for: .normal)
        titleLabel?.font = Typography.bodyPrimary().font()
        titleEdgeInsets.left = Spacing.base03
        contentHorizontalAlignment = .left
    }
}
