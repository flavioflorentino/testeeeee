import UI
import UIKit

public final class VirtualCardClipboardButton: UIButton {
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        setupLayout()
    }
    
    private func setupLayout() {
        layer.borderWidth = 2.0
        layer.borderColor = Colors.branding600.color.cgColor
        layer.cornerRadius = Spacing.base03
        
        setTitle(Strings.VirtualCard.Home.Clipboard.copy, for: .normal)
        setTitle(Strings.VirtualCard.Home.Clipboard.copied, for: .selected)
        setTitleColor(Colors.white.lightColor, for: .normal)
        
        let color = Colors.branding600.change(.dark, to: Colors.white.lightColor).color
        setTitleColor(color, for: .selected)
        setBackgroundColor(Colors.branding600.color, for: .normal)
        setBackgroundColor(Colors.white.color, for: .selected)
        titleLabel?.font = Typography.bodyPrimary(.highlight).font()
    }
}
