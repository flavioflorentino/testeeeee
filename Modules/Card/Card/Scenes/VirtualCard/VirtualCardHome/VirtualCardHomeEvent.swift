import AnalyticsModule

enum VirtualCardHomeEvent: AnalyticsKeyProtocol {
    case generated(name: VirtualCardHomeName, action: VirtualCardHomeAction)
    case state(state: VirtualCardHomeState)
    case auth(action: VirtualCardHomeAuth)
    case dialogFreeze(action: VirtualCardDialogFreeze)
    case dialogUnfreeze(action: VirtualCardDialogUnfreeze)
    case errorAlert(name: VirtualCardAlertErrorName, state: VirtualCardAlertErrorState)
    case errorAlertAction(name: VirtualCardAlertErrorName, action: VirtualCardAlertErrorAction)
    case customErrorAlert(name: VirtualCardCustomErrorName, state: VirtualCardCustomErrorState)
    case customErrorAlertAction(name: VirtualCardCustomErrorName, action: VirtualCardCustomErrorAction)
    
    private var name: String {
        switch self {
        case let .generated(name, _):
            return name.rawValue
        case .state:
            return "Generic Error"
        case .auth:
            return "Authenticated"
        case .dialogFreeze:
            return "Block Card"
        case .dialogUnfreeze:
            return "Unblock Card"
        case let .errorAlert(name, _):
            return name.rawValue
        case let .errorAlertAction(name, _):
            return name.rawValue
        case let .customErrorAlert(name, _):
            return name.rawValue
        case let .customErrorAlertAction(name, _):
            return name.rawValue
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .generated(_, action):
            return [
                "action": action.rawValue
            ]
        case let .state(state):
            return [
                "state": state.rawValue
            ]
        case let .auth(action):
            return [
                "action": action.rawValue
            ]
        case let .dialogFreeze(action):
            return [
                "state": action.rawValue
            ]
        case let .dialogUnfreeze(action):
            return [
                "action": action.rawValue
            ]
        case let .errorAlert(_, action):
            return [
                "action": action.rawValue
            ]
        case let .errorAlertAction(_, action):
            return [
                "action": action.rawValue
            ]
        case let .customErrorAlert(_, state):
            return [
                "state": state.rawValue
            ]
        case let .customErrorAlertAction(_, action):
            return [
                "action": action.rawValue
            ]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PicPay - Virtual Card - \(name)", properties: properties, providers: providers)
    }
}

extension VirtualCardHomeEvent {
    enum VirtualCardHomeName: String {
        case generated = "Generated"
        case blocked = "Freeze"
    }
    
    enum VirtualCardHomeAction: String {
        case clipboard = "act-copy-number"
        case delete = "act-delete-card"
        case close = "act-back"
        case faq = "act-help"
        case blockWarning = "act-temporarilly-freeze-virtual-card"
    }
    
    enum VirtualCardHomeState: String {
        case error = "st-server-error"
        case connectionFailure = "st-connection-error"
    }
    
    enum VirtualCardHomeAuth: String {
        case fingerprintCancel = "act-fingerprint-cancel"
        case fingerprint = "act-fingerprint-used"
        case numeralPassword = "act-numeral-password-used"
    }
    
    enum VirtualCardDialogFreeze: String {
        case block = "act-freeze-virtual-card"
        case cancel = "act-cancel-freeze-virtual-card"
    }
    
    enum VirtualCardDialogUnfreeze: String {
        case unlock = "act-unfreeze-virtual-card"
        case cancel = "act-cancel-unfreeze-virtual-card"
    }
    
    enum VirtualCardAlertErrorName: String {
        case block = "Block Card - Error"
        case unlock = "Unblock Card - Error"
    }
    
    enum VirtualCardAlertErrorState: String {
        case block = "st-block-card-error"
        case unlock = "st-unblock-card-error"
    }
    
    enum VirtualCardAlertErrorAction: String {
        case retry = "act-try-again"
        case cancel = "act-cancel"
    }
    
    enum VirtualCardCustomErrorName: String {
        case invalidStatus = "Invalid Status"
    }
    
    enum VirtualCardCustomErrorState: String {
        case invalidStatus = "st-invalid-status"
    }
    
    enum VirtualCardCustomErrorAction: String {
        case callCenterChat = "act-call-center-chat"
        case close = "act-close"
    }
}
