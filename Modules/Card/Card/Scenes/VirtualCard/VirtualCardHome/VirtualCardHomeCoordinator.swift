import SafariServices
import UIKit
import CustomerSupport

public enum VirtualCardHomeAction: Equatable {
    case faq(url: URL)
    case dismiss
    case succeeded
    case delete
    case openHelpChat
}

public protocol VirtualCardHomeCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: VirtualCardHomeCoordinatorDelegate? { get set }
    func perform(action: VirtualCardHomeAction)
}

public protocol VirtualCardHomeCoordinatorDelegate: AnyObject {
    func homeDidNextStep(action: VirtualCardHomeAction)
}

public final class VirtualCardHomeCoordinator {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    public weak var viewController: UIViewController?
    public weak var delegate: VirtualCardHomeCoordinatorDelegate?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - VirtualCardHomeCoordinating
extension VirtualCardHomeCoordinator: VirtualCardHomeCoordinating, SafariWebViewPresentable {
    public func perform(action: VirtualCardHomeAction) {
        switch action {
        case let .faq(url):
            guard let viewController = viewController else { return }
            let deeplinkResolver = HelpcenterDeeplinkResolver()
            _ = deeplinkResolver.open(url: url, isAuthenticated: true, from: viewController)
        case .openHelpChat:
            guard let url = URL(string: "picpay://picpay/helpcenter/reason/card"), let vc = viewController else { return }
            let deeplinkResolver = HelpcenterDeeplinkResolver()
            _ = deeplinkResolver.open(url: url, isAuthenticated: true, from: vc)
        default:
            delegate?.homeDidNextStep(action: action)
        }
    }
}
