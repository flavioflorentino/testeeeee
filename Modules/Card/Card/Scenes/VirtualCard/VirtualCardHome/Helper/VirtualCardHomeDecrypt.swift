import Foundation
import SecurityModule

final class VirtualCardHomeDecrypt {
    let separator = " /_s_/"
    let token: String
    let consumerId: String
    
    init(token: String, consumerId: Int) {
        self.token = token
        self.consumerId = String(consumerId)
    }
    
    private func getIVAndEncryptData(from info: String?) -> (iv: String, encryptedInfo: String)? {
        guard let info = info else {
            return nil
        }

        let parts = info.components(separatedBy: separator)
        return  parts.count == 2 ? (iv: parts[0], encryptedInfo: parts[1]) : nil
    }
    
    func decryptInfo(with virtualCardResult: VirtualCardResult) -> String? {
        let password = token + consumerId
        let salt = consumerId + token
        guard let (iv, encryptedInfo) = getIVAndEncryptData(from: virtualCardResult.info),
              let key = try? GenerateKey().getSecretKey(password: password, salt: salt).get().toBase64() else {
            return nil
        }
        let cryptoAES = CryptoAES(keyInbase64: key, ivInBase64: iv)
        if let decryptedInfo = try? cryptoAES.decrypt(encryptValue: encryptedInfo).get() {
            return decryptedInfo
        }
        return nil
    }
}
