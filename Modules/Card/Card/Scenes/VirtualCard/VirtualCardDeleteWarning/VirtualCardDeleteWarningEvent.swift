import AnalyticsModule

enum VirtualCardDeleteWarningEvent: AnalyticsKeyProtocol {
    case deleted(action: VirtualCardDeleteWarningEventAction)

    private var name: String {
        "Virtual Card - Deleted"
    }
    
    private var properties: [String: Any] {
        switch self {
        case .deleted(let action):
            return  ["action": action.rawValue]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase, .eventTracker]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PicPay Card - \(name)", properties: properties, providers: providers)
    }
}

extension VirtualCardDeleteWarningEvent {
    enum VirtualCardDeleteWarningEventAction: String {
        case dismiss = "act-delete-card-false"
        case confirm = "act-delete-card-true"
    }
}
