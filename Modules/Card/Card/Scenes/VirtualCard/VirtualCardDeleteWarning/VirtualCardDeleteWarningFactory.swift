import UIKit

public enum VirtualCardDeleteWarningFactory {
    static func make(delegate: VirtualCardDeleteWarningCoordinatorDelegate) -> VirtualCardDeleteWarningViewController {
        let coordinator: VirtualCardDeleteWarningCoordinating = VirtualCardDeleteWarningCoordinator()
        let presenter: VirtualCardDeleteWarningPresenting = VirtualCardDeleteWarningPresenter(coordinator: coordinator)
        let interactor = VirtualCardDeleteWarningInteractor(presenter: presenter, dependencies: DependencyContainer())
        let viewController = VirtualCardDeleteWarningViewController(interactor: interactor)
        
        coordinator.delegate = delegate
        
        return viewController
    }
}
