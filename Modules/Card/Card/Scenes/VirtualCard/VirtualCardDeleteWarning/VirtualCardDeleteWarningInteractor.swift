import Foundation
import AnalyticsModule

public protocol VirtualCardDeleteWarningInteracting: AnyObject {
    func confirm()
    func dismiss()
}

public final class VirtualCardDeleteWarningInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let presenter: VirtualCardDeleteWarningPresenting

    init(presenter: VirtualCardDeleteWarningPresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - VirtualCardDeleteWarningInteracting
extension VirtualCardDeleteWarningInteractor: VirtualCardDeleteWarningInteracting {
    public func confirm() {
        dependencies.analytics.log(VirtualCardDeleteWarningEvent.deleted(action: .confirm))
        presenter.didNextStep(action: .confirm)
    }
    
    public func dismiss() {
        dependencies.analytics.log(VirtualCardDeleteWarningEvent.deleted(action: .dismiss))
        presenter.didNextStep(action: .dismiss)
    }
}
