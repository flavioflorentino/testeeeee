import UIKit

public enum VirtualCardDeleteWarningAction {
    case dismiss
    case confirm
}

public protocol VirtualCardDeleteWarningCoordinating: AnyObject {
    var delegate: VirtualCardDeleteWarningCoordinatorDelegate? { get set }
    func perform(action: VirtualCardDeleteWarningAction)
}

public protocol VirtualCardDeleteWarningCoordinatorDelegate: AnyObject {
    func deleteWarningDidNextStep(action: VirtualCardDeleteWarningAction)
}

public final class VirtualCardDeleteWarningCoordinator {
    public weak var delegate: VirtualCardDeleteWarningCoordinatorDelegate?
}

// MARK: - VirtualCardDeleteWarningCoordinating
extension VirtualCardDeleteWarningCoordinator: VirtualCardDeleteWarningCoordinating {
    public func perform(action: VirtualCardDeleteWarningAction) {
        delegate?.deleteWarningDidNextStep(action: action)
    }
}
