import UI
import UIKit
import AssetsKit

public final class VirtualCardDeleteWarningViewController: ViewController<VirtualCardDeleteWarningInteracting, UIView> {
    fileprivate enum Layout { }
    
    private typealias Localizable = Strings.VirtualCard.DeleteWarning
    
    private lazy var feedbackView: ApolloFeedbackView = {
        let content = ApolloFeedbackViewContent(image: Resources.Illustrations.iluWarning.image,
                                                title: Localizable.title,
                                                description: NSAttributedString(string: Localizable.description),
                                                primaryButtonTitle: Localizable.Button.confirm,
                                                secondaryButtonTitle: Localizable.Button.dismiss)
        let feedbackView = ApolloFeedbackView(content: content, style: .danger)
        setupCallbacks(on: feedbackView)
        return feedbackView
    }()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override public func buildViewHierarchy() {
        view.addSubview(feedbackView)
    }
    
    override public func setupConstraints() {
        feedbackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    override public func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    private func setupCallbacks(on feedbackView: ApolloFeedbackView) {
        feedbackView.didTapPrimaryButton = { [weak self] in
            self?.interactor.confirm()
        }
        
        feedbackView.didTapSecondaryButton = { [weak self] in
            self?.interactor.dismiss()
        }
    }
}

extension VirtualCardDeleteWarningViewController: StyledNavigationDisplayable {
    public var navigationBarStyle: DefaultNavigationStyle {
        ClearNavigationStyle(prefersLargeTitle: false)
    }
}
