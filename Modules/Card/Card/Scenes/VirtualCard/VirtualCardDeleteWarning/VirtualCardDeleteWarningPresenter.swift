import Foundation

public protocol VirtualCardDeleteWarningPresenting: AnyObject {
    func didNextStep(action: VirtualCardDeleteWarningAction)
}

public final class VirtualCardDeleteWarningPresenter {
    private let coordinator: VirtualCardDeleteWarningCoordinating
    
    init(coordinator: VirtualCardDeleteWarningCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - VirtualCardDeleteWarningPresenting
extension VirtualCardDeleteWarningPresenter: VirtualCardDeleteWarningPresenting {
    public func didNextStep(action: VirtualCardDeleteWarningAction) {
        coordinator.perform(action: action)
    }
}
