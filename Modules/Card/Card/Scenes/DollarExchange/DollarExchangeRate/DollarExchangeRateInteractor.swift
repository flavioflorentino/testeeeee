import Foundation
import AnalyticsModule

public protocol DollarExchangeRateInteracting: AnyObject {
    func loadData(isTryingAgain: Bool)
    func didTapFAQ()
    func didTapBack()
    func didTapSeeAllHistory()
}

public final class DollarExchangeRateInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let service: DollarExchangeRateServicing
    private let presenter: DollarExchangeRatePresenting

    init(service: DollarExchangeRateServicing, presenter: DollarExchangeRatePresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
    
    private func loadExchangeRate() {
        service.fetchDollarExchangeRateInfo { [weak self] result in
            switch result {
            case .success(let info):
                self?.presenter.didLoadExchangeRate(info: info)
            case .failure(let error):
                switch error {
                case .connectionFailure, .timeout:
                    self?.presenter.didReceiveError(with: .noConnection)
                    self?.dependencies.analytics.log(DollarExchangeRateEvent.didConnectionError)
                default:
                    self?.presenter.didReceiveError(with: .notAvailable)
                    self?.dependencies.analytics.log(DollarExchangeRateEvent.didGenericError)
                }
            }
        }
    }
}

// MARK: - DollarExchangeRateInteracting
extension DollarExchangeRateInteractor: DollarExchangeRateInteracting {
    public func didTapFAQ() {
        dependencies.analytics.log(DollarExchangeRateEvent.didTapHelp)
        presenter.didNextStep(action: .openFAQ)
    }
    
    public func didTapBack() {
        dependencies.analytics.log(DollarExchangeRateEvent.didTapBack)
    }
    
    public func loadData(isTryingAgain: Bool = false) {
        if isTryingAgain {
            dependencies.analytics.log(DollarExchangeRateEvent.didTapTryAgain)
        }
        loadExchangeRate()
    }
    
    public func didTapSeeAllHistory() {
        presenter.didNextStep(action: .openHistory)
    }
}
