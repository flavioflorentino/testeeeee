import Foundation
import Core
import UI

protocol DollarExchangeRatePresenting: AnyObject {
    var viewController: DollarExchangeRateDisplaying? { get set }
    func didNextStep(action: DollarExchangeRateAction)
    
    func didLoadExchangeRate(info: DollarExchangeRateInfo)
    func didReceiveError(with type: DollarExchangeRateErrorType)
}

final class DollarExchangeRatePresenter {
    typealias Dependencies = HasNoDependency
    typealias DollarExchangePresentedValues = (current: DollarExchangeCellPresenting, latest: [DollarExchangeCellPresenting])
    private let dependencies: Dependencies

    private let coordinator: DollarExchangeRateCoordinating
    weak var viewController: DollarExchangeRateDisplaying?

    init(coordinator: DollarExchangeRateCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
    
    internal func generatePresentableData(from info: DollarExchangeRateInfo) -> DollarExchangePresentedValues {
        let currentDate = info.current.date.formatted(in: .dayAndMonth)
        let currentValue = formatToCurrency(info.current.value)
        let current = DollarExchangeCellPresenter(exchangeDate: currentDate,
                                                  exchangeValue: currentValue)
        
        let latest = info.latest.map { rate -> DollarExchangeCellPresenter in
            let latestDate = rate.date.formatted(in: .dayMonthAndYear)
            let latestValue = formatToCurrency(rate.value)
            return DollarExchangeCellPresenter(exchangeDate: latestDate,
                                               exchangeValue: latestValue)
        }
        return (current, latest)
    }
    
    private func formatToCurrency(_ value: Double) -> String {
        value.toCurrencyString(minFractionDigits: 4,
                               maxFractionDigits: 4) ?? ""
    }
}

// MARK: - DollarExchangeRatePresenting
extension DollarExchangeRatePresenter: DollarExchangeRatePresenting {
    func didNextStep(action: DollarExchangeRateAction) {
        coordinator.perform(action: action)
    }
    
    func didLoadExchangeRate(info: DollarExchangeRateInfo) {
        let presentableData = generatePresentableData(from: info)
        viewController?.displayExchangeRate(current: presentableData.current, latest: presentableData.latest)
    }
    
    func didReceiveError(with type: DollarExchangeRateErrorType) {
        viewController?.displayExchangeRateError(with: type)
    }
}

extension DollarExchangeRateErrorType {
    private typealias Localizable = Strings.DollarExchangeRate
    
    public func buildErrorStateful() -> StatefulErrorViewModel {
        switch self {
        case .noConnection:
            return StatefulErrorViewModel(
                image: Assets.notFound.image,
                content: (
                    title: Localizable.noConnection,
                    description: Localizable.verifyYourConnection
                ),
                button: (image: nil, title: Strings.Default.retry)
            )
        case .notAvailable:
            return StatefulErrorViewModel(
                image: Assets.attention.image,
                content: (
                    title: Localizable.somethingWentWrong,
                    description: Localizable.outage
                ),
                button: (image: nil, title: Strings.Default.retry)
            )
        }
    }
}
