import FeatureFlag
import UIKit
import SafariServices

public enum DollarExchangeRateAction {
    case openFAQ
    case openHistory
}

public protocol DollarExchangeRateCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: DollarExchangeRateAction)
}

public final class DollarExchangeRateCoordinator: SafariWebViewPresentable {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    public weak var viewController: UIViewController?
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - DollarExchangeRateCoordinating
extension DollarExchangeRateCoordinator: DollarExchangeRateCoordinating {
    public func perform(action: DollarExchangeRateAction) {
        switch action {
        case .openFAQ:
            guard let url = URL(string: dependencies.featureManager.text(.opsDollarExchangeRateFaqUrl)) else { return }
            open(url: url)
        case .openHistory:
            let vc = DollarExchangeHistoryFactory.make()
            viewController?.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
