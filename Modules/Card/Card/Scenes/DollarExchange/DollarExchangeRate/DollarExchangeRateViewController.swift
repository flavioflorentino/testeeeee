import UI
import UIKit

public protocol DollarExchangeRateDisplaying: AnyObject {
    func displayExchangeRate(current: DollarExchangeCellPresenting, latest: [DollarExchangeCellPresenting])
    func displayExchangeRateError(with type: DollarExchangeRateErrorType)
}

// MARK: Layout
private extension DollarExchangeRateViewController.Layout {
    enum Size {
        static let rowHeight: CGFloat = 56
        static let headerHeight: CGFloat = 220
    }
}

private enum ViewState {
    case loading
    case error(errorType: DollarExchangeRateErrorType)
    case displayingExchangeRate(current: DollarExchangeCellPresenting, latest: [DollarExchangeCellPresenting])
}

public final class DollarExchangeRateViewController: ViewController<DollarExchangeRateInteracting, UIView> {
    // MARK: Private
    fileprivate enum Layout { }

    override public var preferredStatusBarStyle: UIStatusBarStyle {
        statusBarStyle
    }

    private var state: ViewState = .loading {
        didSet {
            renderState()
        }
    }
    
    // MARK: View Properties
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.register(DollarExchangeRateCell.self, forCellReuseIdentifier: DollarExchangeRateCell.identifier)
        tableView.rowHeight = Layout.Size.rowHeight
        tableView.contentInset = UIEdgeInsets.zero
        tableView.allowsSelection = false
        tableView.tableFooterView = UIView()
        tableView.estimatedSectionHeaderHeight = Layout.Size.headerHeight
        tableView.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        return tableView
    }()
    
    private lazy var sectionHeaderView: DollarExchangeRateHeaderView = {
        let view = DollarExchangeRateHeaderView()
        view.seeAllHistoryTapped = { [weak self] in
            self?.viewModel.didTapSeeAllHistory()
        }
        return view
    }()

    // MARK: Data Source
    private lazy var dataSource: TableViewDataSource<Int, DollarExchangeCellPresenting> = {
        let dataSource = TableViewDataSource<Int, DollarExchangeCellPresenting>(view: tableView)
        dataSource.add(section: 0)
        dataSource.itemProvider = { tableView, indexPath, presenter -> UITableViewCell? in
            let cellIdentifier = DollarExchangeRateCell.identifier
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? DollarExchangeRateCell
            cell?.configure(with: presenter)
            return cell
        }
        return dataSource
    }()

    // MARK: Override Functions
    override public func viewDidLoad() {
        super.viewDidLoad()
        state = .loading
        loadData()
    }

    override public func buildViewHierarchy() {
        view.addSubview(tableView)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: Assets.icoCirclefaq.image,
            style: .plain,
            target: self,
            action: #selector(didTapFAQ)
        )
    }

    override public func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }

    override public func configureViews() {
        title = Strings.DollarExchangeRate.title
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        tableView.dataSource = dataSource
        tableView.delegate = self
    }

    // MARK: Private Functions
    private func loadData() {
        viewModel.loadData(isTryingAgain: false)
    }
    
    override public  func willMove(toParent parent: UIViewController?) {
        if parent == nil {
            viewModel.didTapBack()
        }
    }
    
    private func renderState() {
        switch state {
        case let .displayingExchangeRate(current, latest):
            sectionHeaderView.configure(with: current)
            dataSource.add(items: latest, to: 0)
            endState()
        case let .error(type):
            let errorModel = type.buildErrorStateful()
            endState(model: errorModel)
        case .loading:
            beginState()
        }
    }
    
    // MARK: Objc Functions
    @objc
    func didTapFAQ() {
        viewModel.didTapFAQ()
    }
}

// MARK: DollarExchangeRateDisplay
extension DollarExchangeRateViewController: DollarExchangeRateDisplaying {
    public func displayExchangeRate(current: DollarExchangeCellPresenting, latest: [DollarExchangeCellPresenting]) {
        state = .displayingExchangeRate(current: current, latest: latest)
    }
    
    public func displayExchangeRateError(with type: DollarExchangeRateErrorType) {
        state = .error(errorType: type)
    }
}

// MARK: UITableViewDelegate
extension DollarExchangeRateViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        sectionHeaderView
    }
}

// MARK: - StatefulTransitionViewing
extension DollarExchangeRateViewController: StatefulTransitionViewing {
    public func statefulViewForLoading() -> StatefulViewing {
        DollarExchangeRateSkeletonView()
    }
    
    public func didTryAgain() {
        state = .loading
        viewModel.loadData(isTryingAgain: true)
    }
}

extension DollarExchangeRateViewController: StyledNavigationDisplayable {}
