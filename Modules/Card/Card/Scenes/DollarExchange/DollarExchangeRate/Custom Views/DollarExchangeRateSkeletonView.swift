import SkeletonView
import UI
import UIKit

extension DollarExchangeRateSkeletonView.Layout {
    enum Cell {
        static let height: CGFloat = 56
        static let exchangeRateRows = 6
        static let historyRows = 15
    }
}

public final class DollarExchangeRateSkeletonView: UIView, StatefulViewing {
    fileprivate enum Layout { }
    private let isShowingHistory: Bool
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(DollarExchangeRateCell.self, forCellReuseIdentifier: DollarExchangeRateCell.identifier)
        tableView.isUserInteractionEnabled = false
        tableView.tableFooterView = UIView()
        tableView.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        tableView.backgroundColor = .clear
        tableView.rowHeight = Layout.Cell.height
        return tableView
    }()
    
    public var viewModel: StatefulViewModeling?
    public weak var delegate: StatefulDelegate?
    
    public init(frame: CGRect = .zero, isShowingHistory: Bool = false) {
        self.isShowingHistory = isShowingHistory
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension DollarExchangeRateSkeletonView: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(tableView)
    }
    
    public func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    public func configureViews() {
        isSkeletonable = true
        backgroundColor = Colors.backgroundPrimary.color
    }
}

// MARK: - UITableViewDataSource
extension DollarExchangeRateSkeletonView: UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        isShowingHistory ? Layout.Cell.historyRows : Layout.Cell.exchangeRateRows
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DollarExchangeRateCell.identifier, for: indexPath)
        cell.showAnimatedSkeleton(usingColor: Colors.backgroundSecondary.color)
        return cell
    }
}

// MARK: - UITableViewDelegate
extension DollarExchangeRateSkeletonView: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard !isShowingHistory else { return UIView(frame: .zero) }
        return DollarExchangeRateHeaderView()
    }
}
