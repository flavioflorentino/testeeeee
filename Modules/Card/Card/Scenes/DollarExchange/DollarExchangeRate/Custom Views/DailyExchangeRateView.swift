import UI
import UIKit
import SkeletonView

// MARK: Layout
private extension DailyExchangeRateView.Layout {
    enum ViewSize {
        static let height = 110
        static let width = 344
    }
    
    enum LabelSize {
        static let height = 18
        static let width = 118
    }
    
    enum SkeletonViewSize {
        static let height = 16
        static let width = 92
    }
}
    
final class DailyExchangeRateView: UIView {
    // MARK: Private
    fileprivate enum Layout { }
    private typealias Localizable = Strings.DollarExchangeRate

    // MARK: View Properties
    private lazy var currencyNameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = Colors.white.lightColor
        label.font = Typography.bodyPrimary(.highlight).font()
        label.text = Localizable.currency
        return label
    }()

    private lazy var currencyValueLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = Colors.white.lightColor
        label.font = Typography.title(.large).font()
        return label
    }()

    private lazy var lastUpdateLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = Colors.white.lightColor
        label.font = Typography.caption().font()
        return label
    }()

    private lazy var containerStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            SpacerView(size: Spacing.base02),
            currencyNameLabel,
            SpacerView(size: Spacing.base02),
            currencyValueLabel,
            SpacerView(size: Spacing.base03),
            lastUpdateLabel,
            SpacerView(size: Spacing.base02)
        ])
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        return stackView
    }()
    
    private lazy var skeletonLoadingView: UIView = {
        let view = UIView()
        view.isSkeletonable = true
        view.backgroundColor = Colors.white.color
        view.cornerRadius = .medium
        view.layer.masksToBounds = true
        return view
    }()

    // MARK: - Initialization
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        buildLayout()
        skeletonLoadingView.showAnimatedSkeleton(usingColor: Colors.backgroundSecondary.color)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Open Functions
    func configure(with data: DollarExchangeCellPresenting) {
        skeletonLoadingView.stopSkeletonAnimation()
        skeletonLoadingView.isHidden = true
        lastUpdateLabel.text = Strings.DollarExchangeRate.lastUpdate(data.exchangeDate)
        currencyValueLabel.text = data.exchangeValue
    }
}

// MARK: ViewConfiguration
extension DailyExchangeRateView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(containerStackView)
        addSubview(skeletonLoadingView)
    }

    func configureViews() {
        cornerRadius = .light
        backgroundColor = Colors.branding600.color
    }

    func setupConstraints() {
        snp.makeConstraints {
            $0.height.equalTo(Layout.ViewSize.height)
            $0.width.lessThanOrEqualTo(Layout.ViewSize.width)
        }
        
        containerStackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        currencyValueLabel.snp.makeConstraints {
            $0.height.lessThanOrEqualTo(Layout.LabelSize.height)
            $0.width.equalTo(Layout.LabelSize.width)
        }
        
        skeletonLoadingView.snp.makeConstraints {
            $0.height.equalTo(Layout.SkeletonViewSize.height)
            $0.width.equalTo(Layout.SkeletonViewSize.width)
            $0.center.equalTo(currencyValueLabel)
        }
    }
}
