import UI
import UIKit

final class DollarExchangeRateHeaderView: UIView {
    private typealias Localizable = Strings.DollarExchangeRate
    fileprivate enum Layout { }

    // MARK: View Properties
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.textColor = Colors.grayscale600.color
        label.font = Typography.bodyPrimary(.default).font()
        label.numberOfLines = 2
        label.adjustsFontSizeToFitWidth = true
        label.text = Localizable.subtitle
        return label
    }()

    private lazy var recentsLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.textColor = Colors.black.color
        label.font = Typography.bodyPrimary(.highlight).font()
        label.text = Localizable.recents
        return label
    }()

    private lazy var seeAllHistoryButton: UnderlinedButton = {
        let button = UnderlinedButton()
        button.setTitle(Localizable.seeAllHistory, for: .normal)
        button.addTarget(self, action: #selector(didTapSeeAllHistory), for: .touchUpInside)
        return button
    }()

    private lazy var currencyValueView: DailyExchangeRateView = {
        let view = DailyExchangeRateView()
        return view
    }()

    private lazy var containerStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            SpacerView(size: Spacing.base02),
            subtitleLabel,
            SpacerView(size: Spacing.base02),
            currencyValueView,
            SpacerView(size: Spacing.base02),
            horizontalStackView,
            SpacerView(size: Spacing.base03)
        ])
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        return stackView
    }()

    private lazy var horizontalStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            recentsLabel,
            seeAllHistoryButton
        ])
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        return stackView
    }()
    
    var seeAllHistoryTapped: () -> Void = {}

    // MARK: - Initialization
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Open Functions
    func configure(with data: DollarExchangeCellPresenting) {
        currencyValueView.configure(with: data)
    }
    
    // MARK: - Objc Functions
    @objc
    func didTapSeeAllHistory() {
        seeAllHistoryTapped()
    }
}

// MARK: ViewConfiguration
extension DollarExchangeRateHeaderView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(containerStackView)
    }

    func setupConstraints() {
        containerStackView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.equalTo(Spacing.base02)
            $0.trailing.equalTo(-Spacing.base02)
        }
    }
}
