import SnapKit
import UI
import UIKit

private extension DollarExchangeRateCell.Layout {
    enum LabelSize {
        static let height = 20
        static let width = 94
    }
}

final class DollarExchangeRateCell: UITableViewCell {
    fileprivate enum Layout {}
    
    // MARK: View Properties
    private lazy var containerView: UIView = {
        let view = UIView()
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        view.isSkeletonable = true
        return view
    }()
    
    private(set) lazy var exchangeRateDateLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.textAlignment, .left)
        label.textColor = Colors.grayscale500.color
        label.isSkeletonable = true
        label.numberOfLines = 1
        label.linesCornerRadius = 8
        return label
    }()

    private(set) lazy var exchangeRateValueLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textAlignment, .left)
        label.textColor = Colors.grayscale700.color
        label.isSkeletonable = true
        label.numberOfLines = 1
        label.linesCornerRadius = 8
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        isSkeletonable = true
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Open Functions
    func configure(with presenter: DollarExchangeCellPresenting) {
        stopSkeletonAnimation()
        exchangeRateDateLabel.text = presenter.exchangeDate
        exchangeRateValueLabel.text = presenter.exchangeValue
    }
}

// MARK: ViewConfiguration
extension DollarExchangeRateCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(exchangeRateDateLabel)
        contentView.addSubview(exchangeRateValueLabel)
    }

    func setupConstraints() {
        exchangeRateDateLabel.snp.makeConstraints {
            $0.leading.equalTo(Spacing.base03)
            $0.centerY.equalToSuperview()
            $0.height.equalTo(Layout.LabelSize.height)
            $0.width.equalTo(Layout.LabelSize.width)
        }

        exchangeRateValueLabel.snp.makeConstraints {
            $0.trailing.equalTo(-Spacing.base03)
            $0.centerY.equalToSuperview()
            $0.height.equalTo(Layout.LabelSize.height)
            $0.width.equalTo(Layout.LabelSize.width)
        }
    }
}
