import Foundation

public protocol DollarExchangeCellPresenting {
    var exchangeDate: String { get }
    var exchangeValue: String { get }
}

public struct DollarExchangeCellPresenter: DollarExchangeCellPresenting {
    public var exchangeDate: String
    public var exchangeValue: String
}
