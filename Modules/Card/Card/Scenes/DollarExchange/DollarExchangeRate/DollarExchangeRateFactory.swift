import Foundation
import UIKit

public enum DollarExchangeRateFactory {
    public static func make() -> DollarExchangeRateViewController {
        let container = DependencyContainer()
        let service: DollarExchangeRateServicing = DollarExchangeRateService(dependencies: container)
        let coordinator: DollarExchangeRateCoordinating = DollarExchangeRateCoordinator(dependencies: container)
        let presenter: DollarExchangeRatePresenting = DollarExchangeRatePresenter(coordinator: coordinator, dependencies: container)
        let interactor = DollarExchangeRateInteractor(service: service, presenter: presenter, dependencies: container)
        let viewController = DollarExchangeRateViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
