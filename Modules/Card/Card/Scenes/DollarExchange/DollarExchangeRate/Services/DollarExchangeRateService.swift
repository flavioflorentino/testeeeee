import Foundation
import Core

public typealias DollarExchangeRateInfoCompletion = (Result<DollarExchangeRateInfo, ApiError>) -> Void

protocol DollarExchangeRateServicing {
    func fetchDollarExchangeRateInfo(completion: @escaping DollarExchangeRateInfoCompletion)
}

public final class DollarExchangeRateService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - DollarExchangeRateServicing
extension DollarExchangeRateService: DollarExchangeRateServicing {
    public func fetchDollarExchangeRateInfo(completion: @escaping DollarExchangeRateInfoCompletion) {
        let endpoint = CreditServiceEndpoint.exchangeRate
        Api<DollarExchangeRateInfo>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case .success(let info):
                    completion(.success(info.model))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
}
