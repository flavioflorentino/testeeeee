import UI
import UIKit
import Foundation

protocol DollarExchangeHistoryDisplay: AnyObject {
    func renderView(from state: DollarExchangeHistoryViewState)
}

// MARK: Layout
private extension DollarExchangeHistoryViewController.Layout {
    enum Size {
        static let rowHeight: CGFloat = 56
    }
    
    enum Animation {
        static let duration: TimeInterval = 0.3
    }
}

public final class DollarExchangeHistoryViewController: ViewController<DollarExchangeHistoryInteracting, UIView> {
    // MARK: Private Properties
    fileprivate enum Layout { }
    private var canTransitionToLargeTitle = false
    private var canTransitionToSmallTitle = true
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        .default
    }
    
    // MARK: View Properties
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.register(DollarExchangeRateCell.self, forCellReuseIdentifier: DollarExchangeRateCell.identifier)
        tableView.rowHeight = Layout.Size.rowHeight
        tableView.contentInset = UIEdgeInsets.zero
        tableView.allowsSelection = false
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        return tableView
    }()
    
    // MARK: Data Source
    private lazy var dataSource: TableViewDataSource<Int, DollarExchangeCellPresenting> = {
        let dataSource = TableViewDataSource<Int, DollarExchangeCellPresenting>(view: tableView)
        dataSource.add(section: 0)
        dataSource.itemProvider = { tableView, indexPath, presenter -> UITableViewCell? in
            let cellIdentifier = DollarExchangeRateCell.identifier
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? DollarExchangeRateCell
            cell?.configure(with: presenter)
            return cell
        }
        return dataSource
    }()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        beginState()
        viewModel.loadDollarExchangeHistory(isTryingAgain: false)
    }
    
    // MARK: Override
    override public func buildViewHierarchy() {
        view.addSubview(tableView)
    }
    
    override public func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    override public func configureViews() {
        tableView.dataSource = dataSource
        
        parent?.navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: Assets.icoCirclefaq.image,
            style: .plain,
            target: self,
            action: #selector(didTapFAQ)
        )
    }
}

// MARK: DollarExchangeHistoryDisplay
extension DollarExchangeHistoryViewController: DollarExchangeHistoryDisplay {
    func renderView(from state: DollarExchangeHistoryViewState) {
        switch state {
        case .displaying(let history):
            endState()
            tableView.isScrollEnabled = true
            dataSource.add(items: history, to: 0)
        case .error(let error):
            dataSource.remove(section: 0)
            let errorModel = error.buildErrorStateful()
            endState(model: errorModel)
        case .loading:
            beginState()
        }
    }
    
    // MARK: Objc Functions
    @objc
    func didTapFAQ() {
        viewModel.openFAQ()
    }
}

// MARK: - StatefulTransitionViewing
extension DollarExchangeHistoryViewController: StatefulTransitionViewing {
    public func statefulViewForLoading() -> StatefulViewing {
        DollarExchangeRateSkeletonView(isShowingHistory: true)
    }
    
    public func didTryAgain() {
        beginState()
        viewModel.loadDollarExchangeHistory(isTryingAgain: true)
    }
}

// MARK: UITableViewDelegate
extension DollarExchangeHistoryViewController: UITableViewDelegate {
    // Since the table views are nested inside the TabIndicatorView the navigation bar large titles doesn't change automatically on scroll, so we need to do it manually.
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if #available(iOS 13.0, *) {
           let isScrollingDown = scrollView.contentOffset.y > 0
            if canTransitionToLargeTitle && !isScrollingDown {
                navigationController?.navigationBar.prefersLargeTitles = true
                canTransitionToLargeTitle = false
                canTransitionToSmallTitle = true
            } else if canTransitionToSmallTitle && isScrollingDown {
                navigationController?.navigationBar.prefersLargeTitles = false
                canTransitionToLargeTitle = true
                canTransitionToSmallTitle = false
            }
        }
    }
}

extension DollarExchangeHistoryViewController: TabIndicatorViewControllerDelegate {
    public func didUpdateSelectedTab(on index: Int) {
        userMovedTo(page: index)
    }
    
    public func didSelectButton(on index: Int) {
        userMovedTo(page: index)
    }
    
    private func userMovedTo(page index: Int) {
        interactor.didUpdateSelectedTab(at: index)
    }
}

extension DollarExchangeHistoryViewController: StyledNavigationDisplayable {}

extension TabIndicatorViewController: StyledNavigationDisplayable {}
