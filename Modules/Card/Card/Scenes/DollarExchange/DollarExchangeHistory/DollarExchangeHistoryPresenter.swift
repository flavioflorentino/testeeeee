import Foundation

// MARK: - View State Enum
enum DollarExchangeHistoryViewState {
    case loading
    case error(errorType: DollarExchangeRateErrorType)
    case displaying(history: [DollarExchangeCellPresenting])
}

// MARK: - DollarExchangeHistoryPresenting
protocol DollarExchangeHistoryPresenting: AnyObject {
    // MARK: - Properties
    var viewController: DollarExchangeHistoryDisplay? { get set }
    var filterType: DollarExchangeHistoryFilterType { get }
    
    // MARK: - Functions
    func didNextStep(action: DollarExchangeHistoryAction)
    func didLoad(history data: DollarExchangeHistory)
    func didReceiveError(with type: DollarExchangeRateErrorType)
}

// MARK: - Presenter
public final class DollarExchangeHistoryPresenter {
    // MARK: - Properties
    private let coordinator: DollarExchangeHistoryCoordinating
    private var state: DollarExchangeHistoryViewState = .loading
    var filterType: DollarExchangeHistoryFilterType
    var viewController: DollarExchangeHistoryDisplay?
    
    // MARK: - Init
    init(coordinator: DollarExchangeHistoryCoordinating, filterType: DollarExchangeHistoryFilterType) {
        self.coordinator = coordinator
        self.filterType = filterType
    }
    
    // MARK: - Functions
    func generatePresentableData(from history: [DollarExchangeRate]) -> [DollarExchangeCellPresenting] {
        history.map { rate -> DollarExchangeCellPresenter in
            let latestDate = rate.date.formatted(in: .dayMonthAndYear)
            let latestValue = formatToCurrency(rate.value)
            return DollarExchangeCellPresenter(exchangeDate: latestDate,
                                               exchangeValue: latestValue)
        }
    }
    
    private func formatToCurrency(_ value: Double) -> String {
        value.toCurrencyString(minFractionDigits: 4,
                               maxFractionDigits: 4) ?? ""
    }
}

// MARK: - Protocol Implementation
extension DollarExchangeHistoryPresenter: DollarExchangeHistoryPresenting {
    func didNextStep(action: DollarExchangeHistoryAction) {
        coordinator.perform(action: action)
    }
    
    func didLoad(history data: DollarExchangeHistory) {
        let presentableData = generatePresentableData(from: data.history)
        state = .displaying(history: presentableData)
        viewController?.renderView(from: state)
    }
    
    func didReceiveError(with type: DollarExchangeRateErrorType) {
        state = .error(errorType: type)
        viewController?.renderView(from: state)
    }
}
