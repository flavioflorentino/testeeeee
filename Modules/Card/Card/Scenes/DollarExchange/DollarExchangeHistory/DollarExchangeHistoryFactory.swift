import Foundation
import UIKit
import UI

public enum DollarExchangeHistoryFactory {
    public static func make() -> TabIndicatorViewController {
        let titles = DollarExchangeHistoryFilterType.allCases.map { $0.name }
        let tabIndicatorViewController = TabIndicatorViewController(with: titles)
        let vcs = DollarExchangeHistoryFilterType.allCases.map { makeChildViewController(for: $0) }
        tabIndicatorViewController.delegates = vcs
        tabIndicatorViewController.setViewControllers(vcs, animated: false)
        tabIndicatorViewController.title = Strings.DollarExchangeHistory.title
        return tabIndicatorViewController
    }
    
    private static func makeChildViewController(for filterType: DollarExchangeHistoryFilterType) -> DollarExchangeHistoryViewController {
        let container = DependencyContainer()
        let service: DollarExchangeHistoryServicing = DollarExchangeHistoryService(dependencies: container)
        let coordinator: DollarExchangeHistoryCoordinating = DollarExchangeHistoryCoordinator(dependencies: container)
        let presenter: DollarExchangeHistoryPresenting = DollarExchangeHistoryPresenter(coordinator: coordinator, filterType: filterType)
        let interactor = DollarExchangeHistoryInteractor(service: service, presenter: presenter, dependencies: container)
        let viewController = DollarExchangeHistoryViewController(viewModel: interactor)
        coordinator.viewController = viewController
        presenter.viewController = viewController
        return viewController
    }
}
