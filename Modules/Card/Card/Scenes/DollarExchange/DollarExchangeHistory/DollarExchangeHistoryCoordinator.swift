import UIKit
import FeatureFlag

public enum DollarExchangeHistoryAction {
    case openFAQ
}

public protocol DollarExchangeHistoryCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: DollarExchangeHistoryAction)
}

public final class DollarExchangeHistoryCoordinator: SafariWebViewPresentable {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    public var viewController: UIViewController?
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - DollarExchangeHistoryCoordinating
extension DollarExchangeHistoryCoordinator: DollarExchangeHistoryCoordinating {
    public func perform(action: DollarExchangeHistoryAction) {
        switch action {
        case .openFAQ:
            guard let url = URL(string: dependencies.featureManager.text(.opsDollarExchangeRateFaqUrl)) else { return }
            open(url: url)
        }
    }
}
