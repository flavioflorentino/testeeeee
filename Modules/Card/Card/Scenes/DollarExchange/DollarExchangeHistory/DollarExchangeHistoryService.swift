import Foundation
import Core

public typealias DollarExchangeHistoryCompletion = (Result<DollarExchangeHistory, ApiError>) -> Void

public protocol DollarExchangeHistoryServicing {
    func fetchDollarExchangeHistory(for lastDays: Int, completion: @escaping DollarExchangeHistoryCompletion)
}

public final class DollarExchangeHistoryService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - DollarExchangeHistoryServicing
extension DollarExchangeHistoryService: DollarExchangeHistoryServicing {
    public func fetchDollarExchangeHistory(for lastDays: Int, completion: @escaping DollarExchangeHistoryCompletion) {
        let endpoint = CreditServiceEndpoint.exchangeRateHistory(lastDays: lastDays)
        Api<DollarExchangeHistory>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
