import Foundation
import Core
import AnalyticsModule

public enum DollarExchangeHistoryFilterType: Int, CaseIterable {
    case lastFifteenDays = 15
    case lastThirtyDays = 30
    case lastOneHundredAndEight = 180
    
    var name: String {
        Strings.DollarExchangeHistory.days(rawValue)
    }
    
    var analyticsEvent: DollarExchangeRateEvent {
        switch self {
        case .lastFifteenDays:
            return .didTap15Days
        case .lastThirtyDays:
            return .didTap30Days
        case .lastOneHundredAndEight:
            return .didTap180Days
        }
    }
}

public enum DollarExchangeRateErrorType {
    case noConnection
    case notAvailable
    
    init(from apiError: ApiError) {
        switch apiError {
        case .connectionFailure, .timeout:
            self = .noConnection
        default:
            self = .notAvailable
        }
    }
    
    var analyticsEvent: DollarExchangeRateEvent {
        switch self {
        case .noConnection:
            return DollarExchangeRateEvent.didConnectionError
        case .notAvailable:
            return DollarExchangeRateEvent.didGenericError
        }
    }
}

public protocol DollarExchangeHistoryInteracting: AnyObject {
    func loadDollarExchangeHistory(isTryingAgain: Bool)
    func openFAQ()
    func didTapBack()
    func didUpdateSelectedTab(at position: Int)
}

public final class DollarExchangeHistoryInteractor {
    private let service: DollarExchangeHistoryServicing
    private let presenter: DollarExchangeHistoryPresenting
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    init(service: DollarExchangeHistoryServicing, presenter: DollarExchangeHistoryPresenting, dependencies: Dependencies) {
        self.dependencies = dependencies
        self.service = service
        self.presenter = presenter
        
        dependencies.analytics.log(DollarExchangeRateEvent.didOpenHistory)
    }
}

// MARK: - DollarExchangeHistoryInteracting
extension DollarExchangeHistoryInteractor: DollarExchangeHistoryInteracting {
    public func loadDollarExchangeHistory(isTryingAgain: Bool) {
        if isTryingAgain {
            dependencies.analytics.log(DollarExchangeRateEvent.didTapTryAgain)
        }
        service.fetchDollarExchangeHistory(for: presenter.filterType.rawValue) { [weak self] result in
            switch result {
            case .success(let data):
                self?.presenter.didLoad(history: data)
            case .failure(let error):
                let errorType = DollarExchangeRateErrorType(from: error)
                self?.dependencies.analytics.log(errorType.analyticsEvent)
                self?.presenter.didReceiveError(with: errorType)
            }
        }
    }
    
    public func openFAQ() {
        dependencies.analytics.log(DollarExchangeRateEvent.didTapHelp)
        presenter.didNextStep(action: .openFAQ)
    }
    
    public func didUpdateSelectedTab(at position: Int) {
        guard
            let filterType = DollarExchangeHistoryFilterType.allCases[safe: position],
            filterType == presenter.filterType
        else { return }
        dependencies.analytics.log(filterType.analyticsEvent)
    }
    
    public func didTapBack() {
        dependencies.analytics.log(DollarExchangeRateEvent.didTapBack)
    }
}
