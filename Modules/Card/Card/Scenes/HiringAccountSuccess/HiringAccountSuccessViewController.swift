import UI
import UIKit
import AssetsKit

final class HiringAccountSuccessViewController: ViewController<HiringAccountSuccessInteracting, HiringAccountSuccessRootView> {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBarAppearance()
    }

    override func configureViews() {
        rootView.delegate = self
    }
    
    private func updateNavigationBarAppearance() {
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.backgroundColor = .clear
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
            navigationController?.navigationItem.largeTitleDisplayMode = .always
        }
        
        let barButtonItem = UIBarButtonItem(image: Resources.Icons.icoClose.image,
                                            style: .plain,
                                            target: self,
                                            action: #selector(close))
        barButtonItem.tintColor = Colors.branding600.color
        navigationItem.setRightBarButton(barButtonItem, animated: false)
    }
    
    @objc
    func close() {
        interactor.pressClose()
    }
}

// MARK: - HiringAccountSuccessRootViewDelegate
extension HiringAccountSuccessViewController: HiringAccountSuccessRootViewDelegate {
    func didPressConfirm() {
        interactor.pressConfirm()
    }
}
