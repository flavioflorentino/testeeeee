import Foundation

protocol HiringAccountSuccessPresenting: AnyObject {
    func didNextStep(action: HiringAccountSuccessAction)
}

final class HiringAccountSuccessPresenter {
    private let coordinator: HiringAccountSuccessCoordinating

    init(coordinator: HiringAccountSuccessCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - HiringAccountSuccessPresenting
extension HiringAccountSuccessPresenter: HiringAccountSuccessPresenting {
    func didNextStep(action: HiringAccountSuccessAction) {
        coordinator.perform(action: action)
    }
}
