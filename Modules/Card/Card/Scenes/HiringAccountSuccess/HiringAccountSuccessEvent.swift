import AnalyticsModule

enum HiringAccountSuccessEvent: AnalyticsKeyProtocol {
    case didTapConfirm

    private var name: String {
        "PicPay Card - Credit - Activated Success"
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: [:], providers: providers)
    }
}
