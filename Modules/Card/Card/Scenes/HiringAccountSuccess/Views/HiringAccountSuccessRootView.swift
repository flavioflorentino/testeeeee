import UIKit
import UI
import AssetsKit

protocol HiringAccountSuccessRootViewDelegate: AnyObject {
    func didPressConfirm()
}

final class HiringAccountSuccessRootView: UIView, ViewConfiguration {
    typealias Localizable = Strings.HiringAccountSuccess
    
    weak var delegate: HiringAccountSuccessRootViewDelegate?
    
    private lazy var infoImageViewContainer = UIView()
    private lazy var infoImageView = InfoImageView()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
            .with(\.typography, .bodyPrimary(.highlight))
        button.layer.masksToBounds = true
        button.setTitle(Localizable.confirm, for: .normal)
        button.addTarget(self, action: #selector(confirm), for: .touchUpInside)
        return button
    }()
    
    init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        infoImageViewContainer.addSubview(infoImageView)
        addSubviews(infoImageViewContainer, confirmButton)
    }
    
    func setupConstraints() {
        infoImageViewContainer.snp.makeConstraints {
            $0.top.equalTo(compatibleSafeArea.top).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.bottom.equalTo(confirmButton.snp.top).offset(-Spacing.base03)
        }
        
        infoImageView.snp.makeConstraints {
            $0.center.leading.trailing.equalToSuperview()
            $0.top.greaterThanOrEqualToSuperview()
            $0.bottom.lessThanOrEqualToSuperview()
        }
        
        confirmButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(compatibleSafeArea.bottom).offset(-Spacing.base03)
        }
    }
    
    func configureViews() {
        infoImageView.setupView(
            image: Assets.cardTop.image,
            title: Localizable.title,
            description: Localizable.description
        )
        backgroundColor = Colors.backgroundPrimary.color
    }
    
    @objc
    func confirm() {
        delegate?.didPressConfirm()
    }
}
