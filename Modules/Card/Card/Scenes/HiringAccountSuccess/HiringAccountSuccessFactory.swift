import UIKit

enum HiringAccountSuccessFactory {
    static func make(delegate: HiringAccountSuccessDelegate) -> UIViewController {
        let container = DependencyContainer()
        let coordinator: HiringAccountSuccessCoordinating = HiringAccountSuccessCoordinator(delegate: delegate)
        let presenter: HiringAccountSuccessPresenting = HiringAccountSuccessPresenter(coordinator: coordinator)
        let interactor = HiringAccountSuccessInteractor(presenter: presenter, dependencies: container)
        let viewController = HiringAccountSuccessViewController(interactor: interactor)

        return viewController
    }
}
