import UIKit

enum HiringAccountSuccessAction {
    case confirm
    case close
}

protocol HiringAccountSuccessCoordinating: AnyObject {
    func perform(action: HiringAccountSuccessAction)
}

protocol HiringAccountSuccessDelegate: AnyObject {
    func didConfirm()
    func close()
}

final class HiringAccountSuccessCoordinator {
    weak var delegate: HiringAccountSuccessDelegate?
    
    init(delegate: HiringAccountSuccessDelegate) {
        self.delegate = delegate
    }
}

// MARK: - HiringAccountSuccessCoordinating
extension HiringAccountSuccessCoordinator: HiringAccountSuccessCoordinating {
    func perform(action: HiringAccountSuccessAction) {
        switch action {
        case .confirm:
            delegate?.didConfirm()
        case .close:
            delegate?.close()
        }
    }
}
