import AnalyticsModule
import Foundation

protocol HiringAccountSuccessInteracting: AnyObject {
    func pressConfirm()
    func pressClose()
}

final class HiringAccountSuccessInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let presenter: HiringAccountSuccessPresenting

    init(presenter: HiringAccountSuccessPresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - HiringAccountSuccessInteracting
extension HiringAccountSuccessInteractor: HiringAccountSuccessInteracting {
    func pressConfirm() {
        presenter.didNextStep(action: .confirm)
        dependencies.analytics.log(HiringAccountSuccessEvent.didTapConfirm)
    }
    
    func pressClose() {
        presenter.didNextStep(action: .close)
    }
}
