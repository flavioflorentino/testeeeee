import UIKit

public enum BillNotificationFactory {
    public static func make(invoiceId: String) -> BillNotificationViewController {
        let container = DependencyContainer()
        let service: BillNotificationServicing = BillNotificationService(dependencies: container)
        let coordinator: BillNotificationCoordinating = BillNotificationCoordinator(dependencies: container)
        let presenter: BillNotificationPresenting = BillNotificationPresenter(coordinator: coordinator, dependencies: container)
        let interactor = BillNotificationInteractor(service: service, presenter: presenter, dependencies: container, invoiceId: invoiceId)
        let viewController = BillNotificationViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
