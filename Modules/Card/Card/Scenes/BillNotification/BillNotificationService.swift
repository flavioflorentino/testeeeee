import Core
import Foundation

protocol BillNotificationServicing {
    func putBillNotification(invoiceId: String, completion: @escaping CompletionBillNotification)
}

public typealias CompletionBillNotification = (Result<NoContent, ApiError>) -> Void

final class BillNotificationService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - BillNotificationServicing
extension BillNotificationService: BillNotificationServicing {
    public func putBillNotification(invoiceId: String, completion: @escaping CompletionBillNotification) {
        Api<NoContent>(endpoint: CreditServiceEndpoint.enableInvoiceNotification(invoiceId: invoiceId))
            .execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { result in
                let mappedResult = result.map { $0.model }
                self.dependencies.mainQueue.async {
                    completion(mappedResult)
                }
            }
    }
}
