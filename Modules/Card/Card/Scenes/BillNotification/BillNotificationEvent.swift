import AnalyticsModule

public enum BillNotificationEvent: AnalyticsKeyProtocol {
    case notified
    case genericError(action: BillNotificationAlertErrorAction)
    case connectionError(action: BillNotificationAlertErrorAction)
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }
   
    private var name: String {
        switch self {
        case .notified:
            return "Billet Notified"
        case .connectionError:
            return "No connection error"
        case .genericError:
            return "Generic error"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .notified:
            return ["action": "act-message-billet-notified"]
        case .connectionError(let action):
            return ["action": action.rawValue]
        case .genericError(let action):
            return ["action": action.rawValue]
        }
    }

    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PicPay Card - Invoice Payment - \(name)", properties: properties, providers: providers)
    }
    
    public enum BillNotificationAlertErrorAction: String {
        case retry = "act-try-again"
        case cancel = "act-cancel"
        case genericError = "st-service-error"
        case conectionError = "st-connection-error"
    }
}
