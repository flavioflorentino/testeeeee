import UIKit

public enum BillNotificationAction {
    case dismiss
}

public protocol BillNotificationCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: BillNotificationAction)
}

public final class BillNotificationCoordinator {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    public weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - BillNotificationCoordinating
extension BillNotificationCoordinator: BillNotificationCoordinating {
    public func perform(action: BillNotificationAction) {
        if action == .dismiss {
            viewController?.dismiss(animated: true)
        }
    }
}
