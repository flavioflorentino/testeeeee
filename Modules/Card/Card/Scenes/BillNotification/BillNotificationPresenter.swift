import Foundation
import UI
import AssetsKit
import Core

public protocol BillNotificationPresenting: AnyObject {
    var viewController: BillNotificationDisplaying? { get set }
    func didNextStep(action: BillNotificationAction)
    func presentIdealState()
    func presentErrorState(error: ApiError)
    func presentLoadState()
}

public final class BillNotificationPresenter {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies
    
    private let coordinator: BillNotificationCoordinating
    public weak var viewController: BillNotificationDisplaying?
    
    init(coordinator: BillNotificationCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
}

// MARK: - BillNotificationPresenting
extension BillNotificationPresenter: BillNotificationPresenting {
    public func presentLoadState() {
        viewController?.displayLoadState()
    }
    
    public func presentErrorState(error: ApiError) {
        viewController?.displaydReciveError(error)
    }
    
    public func presentIdealState() {
        let model = StatefulFeedbackViewModel(image: AssetsKit.Resources.Illustrations.iluClock1.image, content: (title: Strings.BillNotification.title, description: Strings.BillNotification.message), button: (image: nil, title: Strings.Default.iGotIt), secondaryButton: nil)
        viewController?.displayIdealState(model)
    }
    
    public func didNextStep(action: BillNotificationAction) {
        coordinator.perform(action: action)
    }
}
