import UI
import UIKit
import Core

public protocol BillNotificationDisplaying: AnyObject {
    func displayIdealState(_ model: StatefulFeedbackViewModel)
    func displaydReciveError(_ error: ApiError)
    func displayLoadState()
}

public final class BillNotificationViewController: ViewController<BillNotificationInteracting, UIView> {
    override public func viewDidLoad() {
        super.viewDidLoad()
        interactor.requestNotificationRegister()
    }
}

// MARK: - BillNotificationDisplaying
extension BillNotificationViewController: BillNotificationDisplaying {
    public func displayLoadState() {
        beginState()
    }
    
    public func displaydReciveError(_ error: ApiError) {
        endState(model: StatefulFeedbackViewModel(error))
    }
    
    public func displayIdealState(_ model: StatefulFeedbackViewModel) {
        endState(animated: true, model: model)
    }
}

// MARK: - StatefulTransitionViewing
extension BillNotificationViewController: StatefulTransitionViewing {
    public func statefulViewForError() -> StatefulViewing {
        StatefulFeedbackView()
    }
    public func didTryAgain() {
        interactor.feedbackPagePrimaryButtonDidTap()
    }
    
    public func didTapSecondaryButton() {
        interactor.trackingAlertStateError(tryAgain: false)
        interactor.dismiss()
    }
}
