import Foundation
import AnalyticsModule
import Core

public protocol BillNotificationInteracting: AnyObject {
    func dismiss()
    func iGotIt()
    func requestNotificationRegister()
    func trackingAlertStateError(tryAgain: Bool)
    func feedbackPagePrimaryButtonDidTap()
}

public final class BillNotificationInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    internal var currentError: ApiError?
    private let service: BillNotificationServicing
    private let presenter: BillNotificationPresenting
    private let invoiceId: String
    init(service: BillNotificationServicing, presenter: BillNotificationPresenting, dependencies: Dependencies, invoiceId: String) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.invoiceId = invoiceId
    }
}
//
// MARK: - BillNotificationInteracting
extension BillNotificationInteractor: BillNotificationInteracting {
    public func feedbackPagePrimaryButtonDidTap() {
        guard  currentError == nil else {
            trackingAlertStateError(tryAgain: true)
            requestNotificationRegister()
            return
        }
        iGotIt()
    }
    
    public func trackingAlertStateError(tryAgain: Bool) {
        switch currentError {
        case .connectionFailure:
            dependencies.analytics.log(BillNotificationEvent.connectionError(action: tryAgain ? .retry : .cancel))
        default:
            dependencies.analytics.log(BillNotificationEvent.genericError(action: tryAgain ? .retry : .cancel))
        }
    }
    
    private func trackingErrorService(error: ApiError) {
        switch error {
        case .connectionFailure:
            dependencies.analytics.log(BillNotificationEvent.connectionError(action: .conectionError))
        default:
            dependencies.analytics.log(BillNotificationEvent.genericError(action: .genericError))
        }
    }
    
    public func requestNotificationRegister() {
        presenter.presentLoadState()
        service.putBillNotification(invoiceId: invoiceId) { [weak self] result in
            switch result {
            case .success:
                self?.presenter.presentIdealState()
                self?.currentError = nil
            case .failure(let error):
                self?.presenter.presentErrorState(error: error)
                self?.trackingErrorService(error: error)
                self?.currentError = error
            }
        }
    }
    
    public func iGotIt() {
        dependencies.analytics.log(BillNotificationEvent.notified)
        dismiss()
    }
    
    public func dismiss() {
        presenter.didNextStep(action: .dismiss)
    }
}
