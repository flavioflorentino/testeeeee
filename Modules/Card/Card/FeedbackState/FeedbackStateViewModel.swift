import AnalyticsModule
import Foundation

public enum FeedbackStateType {
    case cardActivated
    case passwordChanged
    case cardBlocked
    case cardBlockedWithCardReplacement
    case debitAtivated
}

public protocol FeedbackStateViewModelInputs {
    func viewDidLoad()
    func close()
    func confirm()
}

public protocol FeedbackStateViewModelOutputs: AnyObject {
    func perform(_ action: FeedbackStateAction)
    func presenter(presenter: FeedbackStatePresenting)
}

public protocol FeedbackStateViewModelType: AnyObject {
    var inputs: FeedbackStateViewModelInputs { get }
    var outputs: FeedbackStateViewModelOutputs? { get set }
}

public final class FeedbackStateViewModel: FeedbackStateViewModelType, FeedbackStateViewModelInputs {
    public var inputs: FeedbackStateViewModelInputs { self }
    public weak var outputs: FeedbackStateViewModelOutputs?

    private let type: FeedbackStateType
    
    public typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies = DependencyContainer()
    
    public init(type: FeedbackStateType) {
        self.type = type
    }

    public func viewDidLoad() {
        let feedbackStatePresenter = FeedbackStatePresenter(type: type)
        outputs?.presenter(presenter: feedbackStatePresenter)
    }
    
    public func close() {
        trackingClose()
        outputs?.perform(.close)
    }
    public func confirm() {
        trackingConfirm()
        outputs?.perform(.confirm(feedbackType: type))
    }
    
    public func trackingClose() {
        switch type {
        case .cardBlocked, .cardBlockedWithCardReplacement:
            dependencies.analytics.log(CardBlockedEvent.success(action: .notNow))
        default:
            return
        }
    }
    
    public func trackingConfirm() {
        switch type {
        case .cardBlocked:
            dependencies.analytics.log(CardBlockedEvent.success(action: .notRequestable))
        case .cardBlockedWithCardReplacement:
            dependencies.analytics.log(CardBlockedEvent.success(action: .requestable))
        default:
            return
        }
    }
}
