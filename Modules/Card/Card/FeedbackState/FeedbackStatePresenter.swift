import UIKit
import FeatureFlag

public enum FeedbackStateImageSize {
    case small
    case big
}
public protocol FeedbackStatePresenting {
    var image: UIImage { get }
    var imageSize: FeedbackStateImageSize { get }
    var title: String { get }
    var description: String { get }
    var confirmTitle: String { get }
    var cancelTitle: String? { get }
    var hideCloseNavigationButton: Bool { get }
    var hideNavigationBackButton: Bool { get }
}

public struct FeedbackStatePresenter: FeedbackStatePresenting {
    public var image: UIImage
    public var imageSize: FeedbackStateImageSize
    public var title: String
    public var description: String
    public var confirmTitle: String
    public var cancelTitle: String?
    public var hideCloseNavigationButton: Bool
    public var hideNavigationBackButton: Bool

    // swiftlint:disable function_body_length
    init(type: FeedbackStateType) {
        switch type {
        case .cardActivated:
            image = Assets.cardActivated.image
            imageSize = .big
            title = Strings.FeedbackState.CardActive.title
            description = Strings.FeedbackState.CardActive.description
            confirmTitle = Strings.Default.iGotIt
            hideCloseNavigationButton = false
            hideNavigationBackButton = true
        case .passwordChanged:
            image = Assets.icoCheck.image
            imageSize = .small
            title = Strings.FeedbackState.PasswordChanged.title
            description = Strings.FeedbackState.PasswordChanged.description
            confirmTitle = Strings.Default.iGotIt
            hideCloseNavigationButton = false
            hideNavigationBackButton = true
        case .cardBlocked:
            image = Assets.icoCheck.image
            imageSize = .small
            title = Strings.FeedbackState.CardBlocked.title
            description = Strings.FeedbackState.CardBlocked.description
            confirmTitle = Strings.Default.iGotIt
            hideCloseNavigationButton = false
            hideNavigationBackButton = true
        case .cardBlockedWithCardReplacement:
            image = Assets.icoCheck.image
            imageSize = .small
            title = Strings.FeedbackState.CardBlocked.title
            description = Strings.FeedbackState.CardBlocked.description
            confirmTitle = Strings.FeedbackState.CardBlocked.confirm
            cancelTitle = Strings.FeedbackState.CardBlocked.cancel
            hideCloseNavigationButton = false
            hideNavigationBackButton = true
        case .debitAtivated:
            image = Assets.cardWhite.image
            imageSize = .big
            title = Strings.FeedbackState.DebitAtivated.title
            description = Strings.FeedbackState.DebitAtivated.description
            confirmTitle = Strings.Default.iGotIt
            hideCloseNavigationButton = false
            hideNavigationBackButton = true
        }
    }
}
