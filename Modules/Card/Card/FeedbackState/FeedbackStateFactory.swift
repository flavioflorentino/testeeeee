import Foundation

public enum FeedbackState {
    case close
    case confirm(feedbackType: FeedbackStateType)
}

public typealias FeedbackStateOutput = (FeedbackState) -> Void

public enum FeedbackStateFactory {
    public static func make(type: FeedbackStateType, delegate: FeedbackStateDelegate) -> FeedbackStateViewController {
        let viewModel: FeedbackStateViewModelType = FeedbackStateViewModel(type: type)
        var coordinator: FeedbackStateCoordinating = FeedbackStateCoordinator()
        let viewController = FeedbackStateViewController(viewModel: viewModel, coordinator: coordinator)
        
        coordinator.delegate = delegate
        coordinator.viewController = viewController
        viewModel.outputs = viewController

        return viewController
    }
    
    public static func make(
        type: FeedbackStateType,
        feedbackStateOutput: @escaping FeedbackStateOutput
    ) -> FeedbackStateViewController {
        let viewModel: FeedbackStateViewModelType = FeedbackStateViewModel(type: type)
        var coordinator: FeedbackStateCoordinating = FeedbackStateCoordinator(feedbackStateOutput: feedbackStateOutput)
        let viewController = FeedbackStateViewController(viewModel: viewModel, coordinator: coordinator)
        
        coordinator.viewController = viewController
        viewModel.outputs = viewController

        return viewController
    }
}
