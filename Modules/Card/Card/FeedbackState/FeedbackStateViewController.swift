import UI
import UIKit

public class FeedbackStateViewController: LegacyViewController<FeedbackStateViewModelType, FeedbackStateCoordinating, UIView> {
    private var imageSizeConstraint: NSLayoutConstraint?
    private var confirmButtonBottomConstraint: NSLayoutConstraint?
    private var cancelButtonHeightConstraint: NSLayoutConstraint?
    private lazy var contentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.image = Assets.icoCheck.image
        return imageView
    }()
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 28, weight: .bold)
        label.textColor = Palette.ppColorGrayscale600.color
        return label
    }()
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 16, weight: .light)
        label.textColor = Palette.ppColorGrayscale500.color
        return label
    }()
    private lazy var confirmButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 24
        button.backgroundColor = Palette.ppColorBranding300.color
        button.setTitleColor(Palette.white.color, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        button.setTitle(Strings.Default.iGotIt, for: .normal)
        button.addTarget(self, action: #selector(tapConfirm), for: .touchUpInside)
        return button
    }()
    private lazy var cancelButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 24
        button.setTitleColor(Palette.ppColorGrayscale400.color, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        button.setTitle(Strings.Default.iGotIt, for: .normal)
        button.addTarget(self, action: #selector(tapClose), for: .touchUpInside)
        return button
    }()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        viewModel.inputs.viewDidLoad()
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBarAppearance()
    }
 
    override public func buildViewHierarchy() {
        view.addSubview(contentView)
        contentView.addSubview(imageView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(confirmButton)
        contentView.addSubview(cancelButton)
    }
    
    override public func setupConstraints() {
        NSLayoutConstraint.leadingTrailing(equalTo: view,
                                           for: [contentView],
                                           constant: 16)
        NSLayoutConstraint.activate([
            contentView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -32)
        ])
        
        NSLayoutConstraint.leadingTrailing(equalTo: contentView,
                                           for: [titleLabel, descriptionLabel, confirmButton],
                                           constant: 0)
        
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: contentView.topAnchor),
            imageView.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 0.8),
            imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor),
            imageView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            imageView.bottomAnchor.constraint(equalTo: titleLabel.topAnchor, constant: -24)
        ])
        
        imageSizeConstraint = imageView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.4)
        
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 16)
        ])
        
        NSLayoutConstraint.activate([
            confirmButton.heightAnchor.constraint(equalToConstant: 48),
            confirmButton.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 24)
        ])
        NSLayoutConstraint.activate([
            cancelButton.widthAnchor.constraint(equalToConstant: 168),
            cancelButton.topAnchor.constraint(equalTo: confirmButton.bottomAnchor, constant: 8),
            cancelButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            cancelButton.centerXAnchor.constraint(equalTo: contentView.centerXAnchor)
        ])
        
        confirmButtonBottomConstraint = confirmButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        cancelButtonHeightConstraint = cancelButton.heightAnchor.constraint(equalToConstant: 40)
    }
    
    override public func configureViews() {
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }
    
    private func updateNavigationBarAppearance() {
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        }
        navigationController?.view.backgroundColor = Palette.ppColorGrayscale000.color
        navigationController?.navigationBar.barTintColor = Palette.ppColorGrayscale000.color
    }
    
    private func addCloseButtonOnNavigation() {
        let barButtonItem = UIBarButtonItem(
            image: Assets.icoClose.image,
            style: .plain,
            target: self,
            action: #selector(tapClose)
        )
        
        barButtonItem.tintColor = Palette.ppColorBranding300.color
        navigationItem.rightBarButtonItem = barButtonItem
    }
    private func hideBackButtonOnNavigation(hide: Bool) {
        navigationItem.hidesBackButton = hide
    }
    
    @objc
    private func tapClose() {
        viewModel.inputs.close()
    }
    
    @objc
    private func tapConfirm() {
        viewModel.inputs.confirm()
    }
}

// MARK: View Model Outputs
extension FeedbackStateViewController: FeedbackStateViewModelOutputs {
    public func perform(_ action: FeedbackStateAction) {
        coordinator.perform(action: action)
    }
    public func presenter(presenter: FeedbackStatePresenting) {
        imageView.image = presenter.image
        titleLabel.text = presenter.title
        descriptionLabel.text = presenter.description
        confirmButton.setTitle(presenter.confirmTitle, for: .normal)
        if let cancelTitle = presenter.cancelTitle {
            confirmButtonBottomConstraint?.isActive = false
            cancelButton.setTitle(cancelTitle, for: .normal)
        } else {
            confirmButtonBottomConstraint?.isActive = true
            cancelButtonHeightConstraint?.constant = 0
            cancelButton.isHidden = true
        }
        if presenter.hideCloseNavigationButton {
            addCloseButtonOnNavigation()
        }
        hideBackButtonOnNavigation(hide: presenter.hideNavigationBackButton)
        imageSizeConstraint?.isActive = (presenter.imageSize == .small)
    }
}
