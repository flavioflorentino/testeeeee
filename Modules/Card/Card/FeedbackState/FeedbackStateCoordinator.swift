import UI
import UIKit

public enum FeedbackStateAction {
    case confirm(feedbackType: FeedbackStateType)
    case close
}

public protocol FeedbackStateDelegate: AnyObject {
    func didConfirmFeedbackState(feedbackType: FeedbackStateType)
    func didCloseFeedbackState()
}

public extension FeedbackStateDelegate {
    func didCloseFeedbackState() { }
}

public protocol FeedbackStateCoordinating {
    var viewController: UIViewController? { get set }
    var delegate: FeedbackStateDelegate? { get set }
    func perform(action: FeedbackStateAction)
}

public final class FeedbackStateCoordinator: FeedbackStateCoordinating {
    public var viewController: UIViewController?
    public weak var delegate: FeedbackStateDelegate?
    private var feedbackStateOutput: FeedbackStateOutput?
    
    init(feedbackStateOutput: FeedbackStateOutput? = nil) {
        self.feedbackStateOutput = feedbackStateOutput
    }
    
    public func perform(action: FeedbackStateAction) {
        switch action {
        case .close:
            delegate?.didCloseFeedbackState()
            feedbackStateOutput?(.close)
        case .confirm(let type):
            delegate?.didConfirmFeedbackState(feedbackType: type)
            feedbackStateOutput?(.confirm(feedbackType: type))
        }
    }
}
