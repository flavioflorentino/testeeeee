import Foundation

// swiftlint:disable convenience_type
final class CardResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: CardResources.self).url(forResource: "CardResources", withExtension: "bundle") else {
            return Bundle(for: CardResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: CardResources.self)
    }()
}
