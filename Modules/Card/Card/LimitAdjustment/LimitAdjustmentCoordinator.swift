import UIKit

public enum LimitAdjustmentAction {
    case close
}

public protocol LimitAdjustmentCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: LimitAdjustmentAction)
}

public final class LimitAdjustmentCoordinator {
    public weak var viewController: UIViewController?
}

// MARK: - LimitAdjustmentCoordinating
extension LimitAdjustmentCoordinator: LimitAdjustmentCoordinating {
    public func perform(action: LimitAdjustmentAction) {
        switch action {
        case .close:
            viewController?.navigationController?.popViewController(animated: true)
        }
    }
}
