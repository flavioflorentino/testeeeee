import UIKit
import UI
import Core

protocol LimitAdjustmentViewDelegate: AnyObject {
    func onValueChange(value: Double)
    func didConfirmeNewLimit()
}

public class LimitAdjustmentView: UIView, ViewConfiguration {
    weak var delegate: LimitAdjustmentViewDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var currencyView: UI.CurrencyField = {
        let currencyView = UI.CurrencyField()
        currencyView.fontValue = Typography.currency(.large).font()
        currencyView.fontCurrency = Typography.title(.large).font()
        currencyView.textColor = Colors.grayscale700.color
        currencyView.isEnabled = false
        currencyView.positionCurrency = .top
        return currencyView
    }()
    
    private lazy var avaliableLimitLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        return label
    }()
    
    private lazy var slider: UISlider = {
        let slider = UISlider()
        slider.addTarget(self, action: #selector(limitSliderOnChange(_:)), for: .valueChanged)
        slider.minimumTrackTintColor = Colors.branding300.color
        slider.maximumTrackTintColor = Colors.grayscale300.color
        return slider
    }()
    
    private lazy var verticalStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [currencyView, avaliableLimitLabel, slider, horizontalStack])
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = Spacing.base03
        return stackView
    }()
    
    private lazy var confirmeButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Strings.LimitAdjustment.confirm, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(confirmNewLimit), for: .touchUpInside)
        return button
    }()
    
    private func makeInfoLabel() -> UILabel {
        let label = UILabel()
        label.textColor = Colors.grayscale500.color
        label.font = Typography.caption(.default).font()
        return label
    }
    
    private lazy var minLabel: UILabel = {
        makeInfoLabel()
    }()
    
    private lazy var maxLabel: UILabel = {
        makeInfoLabel()
    }()
    
    private lazy var horizontalStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [minLabel, maxLabel])
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        stackView.alignment = .lastBaseline
        return stackView
    }()

     public func buildViewHierarchy() {
        [verticalStack, confirmeButton].forEach { addSubview($0) }
    }
    
     public func setupConstraints() {
        verticalStack.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        slider.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
        }
        
        horizontalStack.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
        }
        
        confirmeButton.snp.makeConstraints {
            $0.height.equalTo(Spacing.base06)
            $0.bottom.equalToSuperview().inset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    public func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
    
    func setupViewWithInfoLimit(info: CreditLimit) {
        slider.maximumValue = Float(info.maxValue)
        slider.minimumValue = Float(info.minValue)
        slider.setValue(Float(info.currentValue), animated: true)
        maxLabel.text = info.maxValue.toCurrencyString()
        minLabel.text = info.minValue.toCurrencyString()
        currencyView.value = info.currentValue
    }
    
    func setupAttributedText(_ attributedText: NSAttributedString) {
        avaliableLimitLabel.attributedText = attributedText
    }
    
    func setCurrencyValue(value: Double) {
        currencyView.value = value
    }
}

extension LimitAdjustmentView: CurrencyFieldDelegate {
    public func onValueChange(value: Double) {
        delegate?.onValueChange(value: value)
    }
}

@objc extension LimitAdjustmentView {
    private func limitSliderOnChange(_ sender: UISlider) {
        let value = Double(sender.value)
       
        delegate?.onValueChange(value: value)
    }
    
    private func confirmNewLimit() {
        delegate?.didConfirmeNewLimit()
    }
}
