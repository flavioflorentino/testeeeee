import Foundation
import Core

public protocol LimitAdjustmentInteracting: AnyObject {
    func changeLimit(value: Double)
    func loadLimit()
    func setNewLimit()
    func closeView()
    func tryAgain()
}

public final class LimitAdjustmentInteractor {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    private var creditLimit: CreditLimit?
    private(set) var selectLimitValue: Double = 0.0
    private let service: LimitAdjustmentServicing
    private let presenter: LimitAdjustmentPresenting
    
    init(service: LimitAdjustmentServicing, presenter: LimitAdjustmentPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
    
    internal func findNextMultipleOfIncrement(value: Double) -> Double? {
        if let creditLimit = creditLimit {
            let increment = Double(creditLimit.increment)
            let divider = (value / increment).rounded(.up).magnitude
            return Double(divider * increment)
        }
        return nil
    }
    
    internal func isEqualMinOrMaxValue(_ value: Double) -> Bool {
        value == creditLimit?.maxValue || value == creditLimit?.minValue
    }
    
    internal func takeNextValueMultiple(value: Double) -> Double {
        isEqualMinOrMaxValue(value) ? value : findNextMultipleOfIncrement(value: value) ?? 0.0
    }
}

// MARK: - LimitAdjustmentInteracting
extension LimitAdjustmentInteractor: LimitAdjustmentInteracting {
    public func tryAgain() {
        if creditLimit != nil {
            setNewLimit()
        } else {
            loadLimit()
        }
    }
    
    public func closeView() {
        self.presenter.didNextStep(action: .close)
    }
    
    public func setNewLimit() {
        presenter.presentLoadState()
        service.putNewCreditLimit(limit: selectLimitValue) { [weak self] result in
            switch result {
            case .success:
                self?.presenter.presentSuccessState(currentValue: self?.selectLimitValue ?? 0)
            case .failure(let error):
                self?.presenter.presentErrorState(error: error, cardLimitLoaded: true)
            }
        }
    }
    
    public func changeLimit(value: Double) {
        selectLimitValue = takeNextValueMultiple(value: value)
        let avableLimit = selectLimitValue - (creditLimit?.balance ?? 0)
        presenter.presentAvailableLimit(value: avableLimit > 0 ? avableLimit : 0)
        presenter.presentUpdatedCrediLimit(value: selectLimitValue)
    }
    
    public func loadLimit() {
        presenter.presentLoadState()
        service.getCreditLimit { [weak self] result in
            switch result {
            case .success(let creditLimit):
                self?.creditLimit = creditLimit
                self?.changeLimit(value: creditLimit.currentValue)
                self?.presenter.presentIdealState(info: creditLimit)
            case .failure(let error):
                self?.presenter.presentErrorState(error: error, cardLimitLoaded: false)
            }
        }
    }
}
