import UIKit

public enum LimitAdjustmentFactory {
    public static func make() -> LimitAdjustmentViewController {
        let container = DependencyContainer()
        let service: LimitAdjustmentServicing = LimitAdjustmentService(dependencies: container)
        let coordinator: LimitAdjustmentCoordinating = LimitAdjustmentCoordinator()
        let presenter: LimitAdjustmentPresenting = LimitAdjustmentPresenter(coordinator: coordinator, dependencies: container)
        let interactor = LimitAdjustmentInteractor(service: service, presenter: presenter, dependencies: container)
        let viewController = LimitAdjustmentViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
