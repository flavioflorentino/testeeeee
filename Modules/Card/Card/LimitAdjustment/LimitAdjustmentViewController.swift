import UI

public protocol LimitAdjustmentDisplaying: AnyObject {
    func displayUpdatedLimitAvaliableLabel(_ attributedText: NSAttributedString)
    func displayLoadingState()
    func displayErrorState(error: StatefulViewModeling)
    func displayIdealState(info: CreditLimit)
    func dislplaySuccessPopup(_ popupViewController: PopupViewController)
    func displayValidCurrencyValue(value: Double)
    func displayFailurePopup(_ popupViewController: PopupViewController)
}

public final class LimitAdjustmentViewController: ViewController<LimitAdjustmentInteracting, LimitAdjustmentView> {
    override public func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadLimit()
    }
    
    override public func configureViews() {
        rootView.delegate = self
        title = Strings.LimitAdjustment.title
    }
    
    public func closeScene() {
        interactor.closeView()
    }
}

// MARK: - LimitAdjustmentDisplaying
extension LimitAdjustmentViewController: LimitAdjustmentDisplaying {
    public func displayFailurePopup(_ popupViewController: PopupViewController) {
        endState()
        let popupAction = PopupAction(title: Strings.Default.retry, style: .tryAgain) {
            [weak self] in
            self?.didTryAgain()
        }
        popupViewController.addAction(popupAction)
        showPopup(popupViewController)
    }
    
    public func displayValidCurrencyValue(value: Double) {
        rootView.setCurrencyValue(value: value)
    }
    
    public func dislplaySuccessPopup(_ popupViewController: PopupViewController) {
        endState()
        let buttonTitle = Strings.LimitAdjustment.Alert.title
        let okAction = PopupAction(title: buttonTitle, style: .fill) {
            [weak self] in
            self?.interactor.closeView()
        }
        popupViewController.addAction(okAction)
        showPopup(popupViewController)
    }
    
    public func displayLoadingState() {
        beginState()
    }
    
    public func displayErrorState(error: StatefulViewModeling) {
        endState(model: error)
    }
    
    public func displayIdealState(info: CreditLimit) {
        rootView.setupViewWithInfoLimit(info: info)
        rootView.setCurrencyValue(value: info.currentValue)
        endState()
    }
    
    public func displayUpdatedLimitAvaliableLabel(_ attributedText: NSAttributedString) {
        rootView.setupAttributedText(attributedText)
    }
}

// MARK: - StatefulTransitionViewing
extension LimitAdjustmentViewController: StatefulTransitionViewing {
    public func didTryAgain() {
        interactor.tryAgain()
    }
}

// MARK: - LimitAdjustmentViewDelegate
extension LimitAdjustmentViewController: LimitAdjustmentViewDelegate {
    func didConfirmeNewLimit() {
        interactor.setNewLimit()
    }
    
    func onValueChange(value: Double) {
        interactor.changeLimit(value: value)
    }
}

// MARK: - CardNavigationDisplayable
extension LimitAdjustmentViewController: StyledNavigationDisplayable {}
