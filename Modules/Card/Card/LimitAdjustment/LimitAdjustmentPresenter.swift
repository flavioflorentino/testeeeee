import Foundation
import UI
import Core
import AssetsKit

public protocol LimitAdjustmentPresenting: AnyObject {
    var viewController: LimitAdjustmentDisplaying? { get set }
    func didNextStep(action: LimitAdjustmentAction)
    func presentIdealState(info: CreditLimit)
    func presentSuccessState(currentValue: Double)
    func presentErrorState(error: ApiError, cardLimitLoaded: Bool)
    func presentLoadState()
    func presentUpdatedCrediLimit(value: Double)
    func presentAvailableLimit(value: Double)
}

public final class LimitAdjustmentPresenter {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let coordinator: LimitAdjustmentCoordinating
    public weak var viewController: LimitAdjustmentDisplaying?

    init(coordinator: LimitAdjustmentCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
}

// MARK: - LimitAdjustmentPresenting
extension LimitAdjustmentPresenter: LimitAdjustmentPresenting {
    public func presentErrorState(error: ApiError, cardLimitLoaded: Bool) {
        if cardLimitLoaded {
            viewController?.displayFailurePopup(buildErroPopupInSetNewLimit(for: error))
        } else {
            viewController?.displayErrorState(error: buildErrorService(for: error))
        }
    }
    
    public func presentUpdatedCrediLimit(value: Double) {
        viewController?.displayValidCurrencyValue(value: value)
    }
    
    public func presentLoadState() {
        viewController?.displayLoadingState()
    }
    
    public func presentSuccessState(currentValue: Double) {
        let image = Resources.Illustrations.iluSuccess.image
        let title = Strings.LimitAdjustment.Alert.title
        let description = Strings.LimitAdjustment.Alert.message(currentValue.toCurrencyString() ?? "0.00")
        let popupViewController = PopupViewController(title: title, description: description, image: image)
        viewController?.dislplaySuccessPopup(popupViewController)
    }
    
    public func presentIdealState(info: CreditLimit) {
        viewController?.displayIdealState(info: info)
    }
    
    public func presentAvailableLimit(value: Double) {
        let color = colorForLimit(from: value)
        let valueText = value.toCurrencyString()
        let text = String(format: Strings.LimitAdjustment.available(valueText ?? "0.00"))
        let attributedText = NSMutableAttributedString(string: text, attributes: [.foregroundColor: color, .font: UIFont.systemFont(ofSize: Spacing.base02)])
        let boldRange = NSRange(location: 0, length: valueText?.count ?? 0)
        attributedText.addAttributes([.font: UIFont.boldSystemFont(ofSize: Spacing.base02)], range: boldRange)
        viewController?.displayUpdatedLimitAvaliableLabel(attributedText)
    }
    
    public func didNextStep(action: LimitAdjustmentAction) {
        coordinator.perform(action: action)
    }
    
    private func colorForLimit(from value: Double) -> UIColor {
        value > 0.0 ? Colors.branding300.color : Colors.grayscale400.color
    }
    
    private func buildErroPopupInSetNewLimit(for error: ApiError) -> PopupViewController {
        let popupController = PopupViewController(title: error.requestError?.title ?? ApiError.timeout.requestError?.title, description: error.requestError?.message ?? ApiError.timeout.requestError?.message, image: Resources.Illustrations.iluError.image)
        return popupController
    }
    
    private func buildErrorService(for error: ApiError) -> StatefulErrorViewModel {
        switch error {
        case .connectionFailure:
            return StatefulErrorViewModel(
                image: Resources.Illustrations.iluNotFound.image,
                content: (
                    title: Strings.Default.Connection.title,
                    description: Strings.Default.Connection.verify
                ),
                button: (image: nil, title: Strings.Default.retry)
            )
        default:
            return StatefulErrorViewModel(
                image: Resources.Illustrations.iluError.image,
                content: (
                    title: error.requestError?.title,
                    description: error.requestError?.message
                ),
                button: (image: nil, title: Strings.Default.retry)
            )
        }
    }
}
