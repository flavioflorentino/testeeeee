import Foundation
import Core

public protocol LimitAdjustmentServicing {
    func getCreditLimit(completion: @escaping LimitAdjustmentDataCompletion)
    func putNewCreditLimit(limit: Double, completion: @escaping ((Result<NoContent, ApiError>) -> Void))
}

public typealias LimitAdjustmentDataCompletion = (Result<CreditLimit, ApiError>) -> Void

public final class LimitAdjustmentService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - LimitAdjustmentServicing
extension LimitAdjustmentService: LimitAdjustmentServicing {
    public func putNewCreditLimit(limit: Double, completion: @escaping ((Result<NoContent, ApiError>) -> Void)) {
        Api<NoContent>(endpoint: CreditServiceEndpoint.updateCreditLimit(limit: limit)).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    public func getCreditLimit(completion: @escaping (Result<CreditLimit, ApiError>) -> Void) {
        Api<CreditLimit>(endpoint: CreditServiceEndpoint.fetchCreditLimit).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
