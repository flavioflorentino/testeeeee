import Foundation

public struct CreditLimit: Decodable {
    let maxValue: Double
    let minValue: Double
    let increment: Int
    let currentValue: Double
    let balance: Double
    
    enum CodingKeys: String, CodingKey {
        case currentValue = "current_value"
        case maxValue = "max_value"
        case minValue = "min_value"
        case increment, balance
    }
}
