import UIKit
import UI

protocol ChangeAddressViewDelegate: AnyObject {
    func didPressChangeAddress()
}

final class ChangeAddressView: UIView, ViewConfiguration {
    weak var delegate: ChangeAddressViewDelegate?
    
    private lazy var addressLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale700.color)
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var changeAddressButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.RejectedCredit.ChangeAddressView.buttonTitle, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(changeAddress), for: .touchUpInside)
        return button
    }()
    
    init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubviews(addressLabel, changeAddressButton)
    }
    
    func setupConstraints() {
        addressLabel.snp.makeConstraints {
            $0.leading.trailing.top.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(changeAddressButton.snp.top).offset(-Spacing.base02)
        }
        
        changeAddressButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview()
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
        layer.cornerRadius = CornerRadius.medium
        clipsToBounds = true
    }
    
    @objc
    private func changeAddress() {
        delegate?.didPressChangeAddress()
    }
    
    func setupView(title: String) {
        addressLabel.text = title
    }
}
