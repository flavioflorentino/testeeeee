import UIKit
import UI

protocol AcceptContractViewDelegate: AnyObject {
    func didToggleCheckmark(isSelected: Bool)
    func didOpenLink(url: URL)
}

extension AcceptContractView.Layout {
    enum Size {
        static let checkmarkButton = 24
    }
}
final class AcceptContractView: UIView, ViewConfiguration {
    fileprivate enum Layout {}
    weak var delegate: AcceptContractViewDelegate?
    
    private lazy var checkmarkButton: UIButton = {
        let button = UIButton()
        button.setImage(Assets.icoGreyCircle.image, for: .normal)
        button.setImage(Assets.icoNewCheck.image, for: .selected)
        button.addTarget(self, action: #selector(toggleCheckmark), for: .touchUpInside)
        return button
    }()
    
    private lazy var termsTextView: UITextView = {
        let text = UITextView()
        text.font = Typography.bodyPrimary().font()
        text.textColor = Colors.grayscale700.color
        text.linkTextAttributes = [
            .foregroundColor: Colors.branding400.color,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        text.isScrollEnabled = false
        text.backgroundColor = .clear
        text.isEditable = false
        text.delegate = self
        return text
    }()
    
    init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    private func toggleCheckmark() {
        checkmarkButton.isSelected.toggle()
        delegate?.didToggleCheckmark(isSelected: checkmarkButton.isSelected)
    }
    
    func buildViewHierarchy() {
        addSubviews(checkmarkButton, termsTextView)
    }
    
    func setupConstraints() {
        checkmarkButton.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.checkmarkButton)
            $0.trailing.equalTo(termsTextView.snp.leading).offset(-Spacing.base02)
            $0.leading.equalToSuperview().inset(Spacing.base02)
            $0.centerY.equalToSuperview()
        }
        
        termsTextView.snp.makeConstraints {
            $0.top.trailing.bottom.equalToSuperview().inset(Spacing.base01)
        }
    }
    
    func setupView(contractText: NSAttributedString) {
        termsTextView.attributedText = contractText
    }
    
    func configureViews() {
        layer.borderWidth = Border.light
        layer.borderColor = Colors.grayscale100.color.cgColor
        layer.cornerRadius = CornerRadius.light
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard #available(iOS 13, *), traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) else {
            return
        }
        layer.borderColor = Colors.grayscale100.color.cgColor
    }
}

extension AcceptContractView: UITextViewDelegate {
    func textView(
        _ textView: UITextView, 
        shouldInteractWith URL: URL, 
        in characterRange: NSRange, 
        interaction: UITextItemInteraction
    ) -> Bool {
        delegate?.didOpenLink(url: URL)
        return false
    }
}
