import UIKit
import UI

extension DetailsView {
    func addSectionTitle(text: String, insets: UIEdgeInsets, labelConfig: DetailsViewRow.LabelConfig?) {
        stackView.addArrangedSubview(DetailsSectionTitleRow(text: text, insets: insets, labelConfig: labelConfig))
    }
}

final class DetailsSectionTitleRow: UIView, ViewConfiguration {
    private let insets: UIEdgeInsets
    
    private lazy var label: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, Colors.grayscale700.color)
        return label
    }()
    
    init(text: String, insets: UIEdgeInsets = .zero, labelConfig: DetailsViewRow.LabelConfig? = nil) {
        self.insets = insets
        super.init(frame: .zero)
        buildLayout()
        label.text = text
        guard let config = labelConfig else {
            return
        }
        label.font = config.font
        label.textColor = config.textColor
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubview(label)
    }
    
    func setupConstraints() {
        label.snp.makeConstraints {
            $0.top.equalToSuperview().inset(insets.top)
            $0.bottom.equalToSuperview().inset(insets.bottom)
            $0.leading.equalToSuperview().inset(insets.left)
            $0.trailing.equalToSuperview().inset(insets.right)
        }
    }
}
