import UIKit
import UI

extension DetailsView {
    func addBulletTextRow(text: String, insets: UIEdgeInsets, labelConfig: DetailsViewRow.LabelConfig?) {
        stackView.addArrangedSubview(BulletTextRow(text: text, insets: insets, labelConfig: labelConfig))
    }
}

extension BulletTextRow.Layout {
    enum Size {
        static let bulletHeight: CGFloat = 4
    }
}

final class BulletTextRow: UIView, ViewConfiguration {
    fileprivate enum Layout {}
    private let insets: UIEdgeInsets
    
    private lazy var label: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.grayscale800.color)
        return label
    }()
    
    private let bulletView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = Layout.Size.bulletHeight / 2
        view.backgroundColor = Colors.grayscale800.color
        return view
    }()
    
    init(text: String, insets: UIEdgeInsets = .zero, labelConfig: DetailsViewRow.LabelConfig? = nil) {
        self.insets = insets
        super.init(frame: .zero)
        buildLayout()
        label.text = text
        guard let config = labelConfig else {
            return
        }
        label.font = config.font
        label.textColor = config.textColor
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubviews(bulletView, label)
    }
    
    func setupConstraints() {
        bulletView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().inset(insets.left)
            $0.height.width.equalTo(Layout.Size.bulletHeight)
        }
        
        label.snp.makeConstraints {
            $0.top.equalToSuperview().inset(insets.top)
            $0.bottom.equalToSuperview().inset(insets.bottom)
            $0.leading.equalTo(bulletView.snp.trailing).offset(Spacing.base00)
            $0.trailing.equalToSuperview().inset(insets.right)
        }
    }
}
