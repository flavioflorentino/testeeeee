import UIKit
import UI

extension DetailsView {
    func addHorizontalTextRow(text: String, insets: UIEdgeInsets, labelConfig: DetailsViewRow.LabelConfig?) {
        stackView.addArrangedSubview(DetailsHorizontalTextRow(text: text, insets: insets, labelConfig: labelConfig))
    }
}

final class DetailsHorizontalTextRow: UIView, ViewConfiguration {
    private let insets: UIEdgeInsets
    
    private lazy var label: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale400.color)
        return label
    }()
    
    init(text: String, insets: UIEdgeInsets = .zero, labelConfig: DetailsViewRow.LabelConfig? = nil) {
        self.insets = insets
        super.init(frame: .zero)
        buildLayout()
        label.text = text
        guard let config = labelConfig else {
            return
        }
        label.font = config.font
        label.textColor = config.textColor
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubview(label)
    }
    
    func setupConstraints() {
        label.snp.makeConstraints {
            $0.top.equalToSuperview().inset(insets.top)
            $0.bottom.equalToSuperview().inset(insets.bottom)
            $0.leading.equalToSuperview().inset(insets.left)
            $0.trailing.equalToSuperview().inset(insets.right)
        }
    }
}
