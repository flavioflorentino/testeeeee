import UIKit
import UI

enum DetailsViewRow: Equatable {
    static func == (lhs: DetailsViewRow, rhs: DetailsViewRow) -> Bool {
        switch (lhs, rhs) {
        case let (
                .horizontalText(text: text1, insets: insets1, labelConfig: labelConfig1),
                .horizontalText(text: text2, insets: insets2, labelConfig: labelConfig2)
        ):
            return text1 == text2 && insets1 == insets2 && labelConfig1 == labelConfig2
            
        case let (
                .sectionTitle(text: text1, insets: insets1, labelConfig: labelConfig1),
                .sectionTitle(text: text2, insets: insets2, labelConfig: labelConfig2)
        ):
            return text1 == text2 && insets1 == insets2 && labelConfig1 == labelConfig2
            
        case let (
                .spacer(spacing: space1),
                .spacer(spacing: space2)
        ):
            return space1 == space2
            
        case let (
                .bulletTextRow(text: text1, insets: insets1, labelConfig: labelConfig1),
                .bulletTextRow(text: text2, insets: insets2, labelConfig: labelConfig2)
        ):
            return text1 == text2 && insets1 == insets2 && labelConfig1 == labelConfig2
            
        case (.none, .none):
            return true
            
        default:
            return false
        }
    }
    
    case horizontalText(text: String, insets: UIEdgeInsets = .zero, labelConfig: LabelConfig? = nil)
    case sectionTitle(text: String, insets: UIEdgeInsets = .zero, labelConfig: LabelConfig? = nil)
    case bulletTextRow(text: String, insets: UIEdgeInsets = .zero, labelConfig: LabelConfig? = nil)
    case spacer(spacing: CGFloat)
    case none
    
    struct LabelConfig: Equatable {
        var font: UIFont
        var textColor: UIColor
    }
}

extension DetailsViewRow.LabelConfig {
    init() {
        font = Typography.bodyPrimary().font()
        textColor = Colors.black.color
    }
}

extension DetailsViewRow {
    static func unwrap<T>(_ data: T?, _ transform: (T) -> DetailsViewRow) -> DetailsViewRow {
        data.map(transform) ?? .none
    }
}

final class DetailsView: UIView, ViewConfiguration {
    internal lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fill
        stackView.axis = .vertical
        stackView.spacing = 0
        return stackView
    }()
    
    init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubview(stackView)
    }
    
    func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    func render(_ rows: DetailsViewRow ...) {
        render(rows)
    }
    
    func render(_ rows: [DetailsViewRow]) {
        stackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
        
        rows.forEach {
            switch $0 {
            case let .horizontalText(text: text, insets: insets, labelConfig: labelConfig):
                addHorizontalTextRow(text: text, insets: insets, labelConfig: labelConfig)
                
            case let .sectionTitle(text: text, insets: insets, labelConfig: labelConfig):
                addSectionTitle(text: text, insets: insets, labelConfig: labelConfig)
                
            case let .spacer(spacing: spacing):
                let view = UIView()
                view.snp.makeConstraints { $0.height.equalTo(spacing) }
                stackView.addArrangedSubview(view)
                
            case let .bulletTextRow(text: text, insets: insets, labelConfig: labelConfig):
                addBulletTextRow(text: text, insets: insets, labelConfig: labelConfig)
                
            case .none:
                break
            }
        }
    }
}
