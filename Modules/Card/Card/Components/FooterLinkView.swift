import UIKit
import UI
import SnapKit

protocol FooterLinkViewDelegate: AnyObject {
    func didPressLink(url: URL)
}

final class FooterLinkView: UIView, ViewConfiguration {
    weak var delegate: FooterLinkViewDelegate?
    
    private lazy var helpLabel: UITextView = {
        let text = UITextView()
        text.font = Typography.bodyPrimary().font()
        text.textAlignment = .center
        text.textColor = Colors.grayscale700.color
        text.isScrollEnabled = false
        text.backgroundColor = .clear
        text.linkTextAttributes = [NSAttributedString.Key.foregroundColor: Colors.branding400.color]
        text.delegate = self
        return text
    }()
    
    init(attributedText: NSAttributedString? = nil) {
        super.init(frame: .zero)
        buildLayout()
        helpLabel.attributedText = attributedText
    }
        
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubview(helpLabel)
    }
    
    func setupConstraints() {
        helpLabel.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.grayscale100.color
    }
    
    func setupView(with attrString: NSAttributedString) {
        helpLabel.attributedText = attrString
    }
}

extension FooterLinkView: UITextViewDelegate {
    public func textView(
        _ textView: UITextView,
        shouldInteractWith URL: URL,
        in characterRange: NSRange,
        interaction: UITextItemInteraction
    ) -> Bool {
        delegate?.didPressLink(url: URL)
        return false
    }
    
    public func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        false
    }
}
