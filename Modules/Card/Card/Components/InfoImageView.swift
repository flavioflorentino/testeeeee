import UIKit
import UI
import SkeletonView

extension InfoImageView.Layout {
    enum Size {
        static let imageHeight: CGFloat = 176
    }
}

public final class InfoImageView: UIView, ViewConfiguration {
    public struct LabelSkeletonProperties {
        var isSkeletonable: Bool
        var skeletonNumberOfLines: Int
    }
    
    fileprivate enum Layout {}
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textAlignment, .center)
        label.layer.masksToBounds = true
        label.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textAlignment, .center)
            .with(\.tintColor, Colors.grayscale700.color)
        label.layer.masksToBounds = true
        label.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)
        return label
    }()
    
    public init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func buildViewHierarchy() {
        addSubviews(imageView, titleLabel, descriptionLabel)
    }
    
    public func setupConstraints() {
        imageView.snp.remakeConstraints {
            $0.top.centerX.equalToSuperview()
            $0.leading.greaterThanOrEqualToSuperview()
            $0.trailing.lessThanOrEqualToSuperview()
            $0.bottom.equalTo(titleLabel.snp.top).offset(-Spacing.base03)
            $0.height.lessThanOrEqualTo(Layout.Size.imageHeight)
        }
        titleLabel.snp.remakeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(descriptionLabel.snp.top).offset(-Spacing.base03)
        }
        descriptionLabel.snp.remakeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.bottom.lessThanOrEqualToSuperview()
        }
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        layoutSkeletonIfNeeded()
    }
    
    public func setupSkeletonProperties(
        titleSkeletonProperties: LabelSkeletonProperties,
        descriptionSkeletonProperties: LabelSkeletonProperties,
        isImageSkeletonable: Bool = false
    ) {
        imageView.isSkeletonable = isImageSkeletonable
        
        titleLabel.isSkeletonable = titleSkeletonProperties.isSkeletonable
        titleLabel.numberOfLines = titleSkeletonProperties.skeletonNumberOfLines
        titleLabel.snp.makeConstraints { $0.height.equalTo(CGFloat(titleSkeletonProperties.skeletonNumberOfLines) * titleLabel.font.pointSize) }
        
        descriptionLabel.isSkeletonable = descriptionSkeletonProperties.isSkeletonable
        descriptionLabel.numberOfLines = descriptionSkeletonProperties.skeletonNumberOfLines
        descriptionLabel.snp.makeConstraints { $0.height.equalTo(CGFloat(descriptionSkeletonProperties.skeletonNumberOfLines) * titleLabel.font.pointSize) }
    }
    
    public func setupView(
        image: UIImage? = nil,
        title: String? = nil,
        description: String? = nil,
        normalColor: UIColor = Colors.black.color,
        highlightColor: UIColor = Colors.black.color
    ) {
        setupConstraints()
        titleLabel.numberOfLines = 0
        descriptionLabel.numberOfLines = 0
        imageView.image = image
        titleLabel.text = title
        descriptionLabel.attributedText = description?.attributedStringWith(
            normalFont: Typography.bodyPrimary().font(),
            highlightFont: Typography.bodyPrimary(.highlight).font(),
            normalColor: normalColor,
            highlightColor: highlightColor,
            textAlignment: .center,
            underline: false
        )
    }
}
