import UI
import Foundation

public class CustomerSupportFeedbackView: UIView, ViewConfiguration {
    public var viewModel: StatefulViewModeling?
    private let url: URL
    public weak var delegate: StatefulDelegate? {
        didSet {
            statefulFeedbackView.delegate = delegate
        }
    }
    
    lazy var footerView: FooterLinkView = {
        let view = FooterLinkView(attributedText: getHelpMessage())
        view.delegate = self
        return view
    }()
    
    public var openFaq: ((URL) -> Void)?
    lazy var statefulFeedbackView = StatefulFeedbackView()
    
    public init(model: StatefulFeedbackViewModel, url: URL) {
        self.url = url
        super.init(frame: .zero)
        statefulFeedbackView.viewModel = model
        statefulFeedbackView.configureView(image: model.image, content: model.content, button: model.button)
        backgroundColor = Colors.grayscale100.color
        buildLayout()
    }
   
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func buildViewHierarchy() {
        addSubviews(statefulFeedbackView, footerView)
    }
    
    public func setupConstraints() {
        statefulFeedbackView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(footerView.snp.top)
        }
        
        footerView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            if #available(iOS 11.0, *) {
                $0.bottom.equalTo(safeAreaLayoutGuide.snp.bottom)
            } else {
                $0.bottom.equalToSuperview().inset(Spacing.base04)
            }
        }
    }
    
    func getHelpMessage() -> NSMutableAttributedString {
        let faqMessage = Strings.CardArrived.Faq.message
        let linkMessage = Strings.CardArrived.Faq.link
        
        let attributed = NSMutableAttributedString(string: faqMessage)
        attributed.font(text: faqMessage, font: Typography.bodySecondary().font())
        attributed.textBackgroundColor(text: faqMessage, color: .clear)
        attributed.textColor(text: faqMessage, color: Colors.grayscale600.color)
        attributed.paragraph(aligment: .center, lineSpace: Spacing.base00)
        attributed.underline(text: linkMessage)
        attributed.textColor(text: linkMessage,
                             color: Colors.branding300.color)
        attributed.link(text: linkMessage, link: url.absoluteString)
        
        return attributed
    }
}

extension CustomerSupportFeedbackView: FooterLinkViewDelegate {
    func didPressLink(url: URL) {
        openFaq?(url)
    }
}
extension CustomerSupportFeedbackView: StatefulViewing {}
