import UIKit
import UI

public final class CustomerSupportFeedbackViewController: UIViewController {
    // MARK: - Outlets
    
    private let customerFeedbackView: CustomerSupportFeedbackView
    
    // MARK: - Properties
    
    public var primaryButtonCallBack: (() -> Void)?
    public var secondaryButtonCallBack: (() -> Void)?
    public var helpCenterCallBack: ((URL) -> Void)? {
        didSet {
            customerFeedbackView.openFaq = helpCenterCallBack
        }
    }

    // MARK: - Initialization
    
    public init(model: StatefulFeedbackViewModel, url: URL) {
        self.customerFeedbackView = CustomerSupportFeedbackView(model: model, url: url)
        super.init(nibName: nil, bundle: nil)
        customerFeedbackView.delegate = self
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    override public func loadView() {
        view = customerFeedbackView
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        addCloseButton()
    }
    
    private func addCloseButton() {
        let barButtonItem = UIBarButtonItem(
            image: Assets.icoClose.image,
            style: .plain,
            target: self,
            action: #selector(tapClose)
        )
        
        if navigationController?.presentingViewController != nil {
            navigationItem.rightBarButtonItem = barButtonItem
        }
    }
    
    @objc
    private func tapClose() {
       dismiss(animated: true)
    }
}

extension CustomerSupportFeedbackViewController: StatefulDelegate {
    public func didTryAgain() {
        primaryButtonCallBack?()
    }
    
    public func didTapSecondaryButton() {
        secondaryButtonCallBack?()
    }
}
