import UI
import UIKit

public protocol CardSettingsFlowDelegate: AnyObject {
    func didOpenDueDayCard(from navigationController: UINavigationController)
    func didOpenLimitCard(from navigationController: UINavigationController, didUpdateCardLimit: @escaping () -> Void)
    func didAskAuthentication(onSuccess: @escaping (String) -> Void)
}

public protocol CardSettingsFlowCoordinating: Coordinating {
    var delegate: CardSettingsFlowDelegate? { get set }
}

public final class CardSettingsFlowCoordinator: CardSettingsFlowCoordinating, FlowsCoordinating {
    typealias Dependencies = HasAuthManagerDependency
    private let dependencies: Dependencies = DependencyContainer()

    public var childViewController: [UIViewController] = []
    public var viewController: UIViewController?
    public weak var delegate: CardSettingsFlowDelegate?
    public weak var cardSettingsViewController: CardSettingsViewController?

    private var childCoordinators: [Coordinating] = []
    private var navigationController: UINavigationController

    public init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    public func start() {
        let controller = CardSettingsFactory.make(delegate: self)
        childViewController.append(controller)
        cardSettingsViewController = controller
        navigationController.pushViewController(controller, animated: true)
    }
    
    public func startAnotherFlow() {
        let controller = CardSettingsFactory.make(delegate: self)
        navigationController.viewControllers = [controller]
        childViewController.append(controller)
        cardSettingsViewController = controller
    }
}

extension CardSettingsFlowCoordinator: CardSettingsDelegate {
    public func didOpenChangePassword() {
        let coordinator = ChangeCardPasswordFlowCoordinator(navigationController: navigationController)
        coordinator.delegate = self
        childCoordinators.append(coordinator)
        coordinator.start()
    }

    public func didOpenBlockCard(_ settingsData: CardSettingsData) {
        let coordinator = CardBlockReplacementCardFlowCoordinator(navigationController: navigationController, settingsData: settingsData)
        childCoordinators.append(coordinator)
        coordinator.delegate = self
        coordinator.start()
    }
    
    public func didOpenDueDayCard() {
        let dueDayController = DueDayFactory.make(delegate: self)
        navigationController.pushViewController(dueDayController, animated: true)
    }
    
    public func didOpenLimitCard() {
        let coordinator = LimitAdjustmentFlowCoordinator(navigationController: navigationController)
        coordinator.start()
    }
    
    public func didOpenCardReplacement(_ settingsData: CardSettingsData) {
        let coordinator = CardReplacementFlowCoordinator(navigationController: navigationController, settingsData: settingsData)
        coordinator.delegate = self
        childCoordinators.append(coordinator)
        coordinator.start()
    }
    
    public func didOpenDollarExchangeRate() {
        let controller = DollarExchangeRateFactory.make()
        navigationController.pushViewController(controller, animated: true)
    }
    
    public func didOpenVirtualCard(_ settingsData: CardSettingsData) {
        let input = VirtualCardFlowInput(
            button: settingsData.buttons.virtualCard
        )
        
        let coordinator = VirtualCardFlowCoordinator(navigation: navigationController, input: input)
        coordinator.delegate = self
        childCoordinators.append(coordinator)
        coordinator.start()
    }
}

extension CardSettingsFlowCoordinator: CardBlockReplacementCardFlowDelegate {
    public func didBlockConfirm(onSuccess: @escaping (String, Bool) -> Void, cancelHandler: @escaping () -> Void) {
        dependencies.authManager.authenticate(sucessHandler: onSuccess, cancelHandler: cancelHandler)
    }

    public func updateParentViewController() {
        if let controller = childViewController.first {
            controller.viewWillAppear(true)
        }
    }
}

extension CardSettingsFlowCoordinator: ChangeCardPasswordFlowDelegate {
    public func didConfirmValidatePin(onSuccess: @escaping (String) -> Void) {
        delegate?.didAskAuthentication(onSuccess: onSuccess)
    }
    public func didConfirmChangePin(onSuccess: @escaping (String) -> Void) {
        delegate?.didAskAuthentication(onSuccess: onSuccess)
    }
    public func didConfirmCreateNewPassword(onSuccess: @escaping (String) -> Void) {
        delegate?.didAskAuthentication(onSuccess: onSuccess)
    }
}

extension CardSettingsFlowCoordinator: CardReplacementFlowCoordinatorDelegate {
    public func cardReplacementRequested() {
        cardSettingsViewController?.refreshData()
    }
}

extension CardSettingsFlowCoordinator: DueDayCoordinatingDelegate {
    public func didConfirmDueDay(cardForm: CardForm?, bestPurchaseDay: Int?) {
        navigationController.popViewController(animated: true)
    }
}
extension CardSettingsFlowCoordinator: VirtualCardFlowCoordinatorDelegate {}
