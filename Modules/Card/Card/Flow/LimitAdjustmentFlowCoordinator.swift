import Foundation
import UI
import UIKit

public protocol LimitAdjustmentFlowCoordinatorDelegate: AnyObject {
}

public final class LimitAdjustmentFlowCoordinator: Coordinating {
    public var childViewController: [UIViewController] = []
    public var viewController: UIViewController?
    
    public weak var delegate: VirtualCardFlowCoordinatorDelegate?
    private let navigationController: UINavigationController
    
    private let dependencies = DependencyContainer()
    
    public init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    public func start() {
        let viewController = LimitAdjustmentFactory.make()
        show(controller: viewController)
    }
    
    private func show(controller: UIViewController) {
        childViewController.append(controller)
        navigationController.pushViewController(controller, animated: true)
    }
    
    private func dismiss() {
        navigationController.popToRootViewController(animated: true)
    }
}
