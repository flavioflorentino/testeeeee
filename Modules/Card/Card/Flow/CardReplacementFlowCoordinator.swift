import UI

protocol CardReplacementFlowCoordinatorDelegate: AnyObject {
    func cardReplacementRequested()
}

public final class CardReplacementFlowCoordinator: Coordinating {
    public var childViewController: [UIViewController] = []
    public var viewController: UIViewController?
    
    weak var delegate: CardReplacementFlowCoordinatorDelegate?
    private let navigationController: UINavigationController
    
    private let dependencies = DependencyContainer()
    private let settingsData: CardSettingsData
    
    public init(navigationController: UINavigationController, settingsData: CardSettingsData) {
        self.navigationController = navigationController
        self.settingsData = settingsData
    }
    
    public func start() {
        switch settingsData.buttons.cardReplacement.action {
        case .address:
            startRequestableFlow()
        case .tracking:
            startRequestedFlow()
        case .none:
            return
        }
    }
    
    private func dismiss() {
        navigationController.popToRootViewController(animated: true)
    }
    
    private func requestCompleted() {
        self.delegate?.cardReplacementRequested()
        dismiss()
    }
}

extension CardReplacementFlowCoordinator {
    private func startRequestedFlow() {
        let controller = CardReplacementRequestedFactory.make()
        childViewController.append(controller)
        navigationController.pushViewController(controller, animated: true)
    }
}

extension CardReplacementFlowCoordinator {
    private func startRequestableFlow() {
        let controller = RequestPhysicalCardLoadingFactory.make(
            requestCardCoordinatorOutput: requestLoadingOutput,
            service: RequestPhysicalCardLoadingService(dependencies: dependencies)
        )
        childViewController.append(controller)
        navigationController.pushViewController(controller, animated: false)
    }
    
    private func requestLoadingOutput(requestPhysicalOutput: RequestPhysicalCardCoordinatorOutput) {
        switch requestPhysicalOutput {
        case .getServiceResponse(let deliveryAddress):
            let controller = DeliveryConfirmAddressFactory.make(
                service: ReplacementCardDeliveryConfirmAddressService(dependencies: dependencies),
                analyticsContext: ReplacementCardDeliveryConfirmAddressAnalytics(dependencies: dependencies),
                deliveryAddress: deliveryAddress,
                coordinatorOutput: deliveryAddressOutput
            )
            // Rebuilds the navigation controller to remove the RequestPhysicalCardLoadingService from the stack
            navigationController.viewControllers = [navigationController.viewControllers.first, controller].compactMap { $0 }
        case .close:
            dismiss()
        }
    }
    
    private func deliveryAddressOutput(deliveryAddressAction: DeliveryConfirmAddressAction) {
        switch deliveryAddressAction {
        case .changeAddress, .close:
            dismiss()
        case .didConfirm:
            switch settingsData.cardType {
            case .multiple:
                let controller = ChosenDeliveryAddressSuccessFactory.make(delegate: self)
                navigationController.pushViewController(controller, animated: true)
            case .debit:
                let controller = DebitDeliverySuccessFactory.make { _ in
                    self.requestCompleted()
                    self.dependencies.analytics.log(DeliveryConfirmAddressEvent.success(flow: .replacementCard, action: .finish))
                }
                navigationController.pushViewController(controller, animated: true)
            case .none:
                return
            }   
        }
    }
}

extension CardReplacementFlowCoordinator: ChosenDeliveryAddressSuccessCoordinatorDelegate {
    public func didCloseChosenDeliveryAddressSuccess() {
        self.requestCompleted()
        dependencies.analytics.log(DeliveryConfirmAddressEvent.success(flow: .replacementCard, action: .finish))
    }
}
