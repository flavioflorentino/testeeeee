import UI
import UIKit

public protocol ChangeCardPasswordFlowDelegate: AnyObject {
    func didConfirmValidatePin(onSuccess: @escaping (String) -> Void)
    func didConfirmChangePin(onSuccess: @escaping (String) -> Void)
    func didConfirmCreateNewPassword(onSuccess: @escaping (String) -> Void)
}

public final class ChangeCardPasswordFlowCoordinator: Coordinating {
    private let navigationController: UINavigationController
    public weak var delegate: ChangeCardPasswordFlowDelegate?
    public var childViewController: [UIViewController] = []
    public var viewController: UIViewController?
    private let mainActiveNavigationController = StyledNavigationController()
    private let presentedModally: Bool
    var presentingNavigationController: UINavigationController {
        presentedModally
            ? mainActiveNavigationController
            : navigationController
    }
    
    public init(navigationController: UINavigationController, presentModally: Bool = false) {
        self.navigationController = navigationController
        self.presentedModally = presentModally
    }
    
    public func start() {
        let controller = ValidateCardPasswordFactory.make(self)
        controller.hidesBottomBarWhenPushed = true
        mainActiveNavigationController
            .presentationController?
            .delegate = navigationController.viewControllers.first as? UIAdaptivePresentationControllerDelegate
        mainActiveNavigationController.pushViewController(controller, animated: true)

        presentedModally
            ? navigationController.present(mainActiveNavigationController, animated: true)
            : navigationController.pushViewController(controller, animated: true)
    }
    
    private func dismissStack() {
        guard let cardSettingsController = presentingNavigationController.viewControllers.first(where: { $0 is CardSettingsViewController }) else {
            return presentingNavigationController.dismiss(animated: true)
        }
        
        presentingNavigationController.popToViewController(cardSettingsController, animated: true)
    }
}

extension ChangeCardPasswordFlowCoordinator: ValidateCardPasswordCoordinatorDelegate {
    public func didForgotPassword() {
        let forgotPasswordCardFlow = ForgotPasswordCardFlowCoordinator(navigationController: presentingNavigationController, delegate: self)
        forgotPasswordCardFlow.start()
    }
    
    public func didConfirmValidateCardPin(onSuccess: @escaping (String) -> Void) {
        delegate?.didConfirmValidatePin(onSuccess: onSuccess)
    }
    
    public func didSuccesValidateCardPin(oldCardPin: String) {
        let controller = ChangeCardPasswordFactory.make(oldCardPin: oldCardPin, delegate: self)
        presentingNavigationController.pushViewController(controller, animated: true)
    }
    
    public func didCloseValidateCard() {
        presentingNavigationController.popViewController(animated: true)
    }
}

extension ChangeCardPasswordFlowCoordinator: ChangeCardPasswordCoordinatorDelegate {
    public func didConfirmChangePin(onSuccess: @escaping (String) -> Void) {
        delegate?.didConfirmChangePin(onSuccess: onSuccess)
    }
    
    public func didSuccessChangePin() {
        let controller = FeedbackStateFactory.make(type: .passwordChanged, delegate: self)
        controller.navigationItem.setHidesBackButton(true, animated: true)
        presentingNavigationController.pushViewController(controller, animated: true)
    }
    
    public func didOpenHelperSecurityTips() {
        let controller = PasswordSecurityTipsFactory.make(delegate: nil)
        presentingNavigationController.pushViewController(controller, animated: true)
    }
}

extension ChangeCardPasswordFlowCoordinator: FeedbackStateDelegate {
    public func didConfirmFeedbackState(feedbackType: FeedbackStateType) {
        dismissStack()
    }
    public func didCloseFeedbackState() {
        dismissStack()
    }
}

extension ChangeCardPasswordFlowCoordinator: ForgotPasswordCardFlowDelegate {
    public func didAskAuthentication(onSuccess: @escaping (String) -> Void) {
        delegate?.didConfirmCreateNewPassword(onSuccess: onSuccess)
    }
}
