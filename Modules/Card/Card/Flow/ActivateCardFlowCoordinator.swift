import AnalyticsModule
import UI
import UIKit
import CustomerSupport

public protocol ActivateCardFlowCoordinatorDelegate: AnyObject {
    func didOpenWallet()
    func didFinishFlow()
}

public final class ActivateCardFlowCoordinator: Coordinating {
    private let navigationController: UINavigationController
    private let mainActivateNavigationController = UINavigationController()
    public var childViewController: [UIViewController] = []
    public var viewController: UIViewController?

    public init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    public func start() {
        let controller = ValidateCardFactory.make(self, type: .activateCard, analyticsContext: ValidateCardActivateCardAnalytics())
        mainActivateNavigationController
            .presentationController?
            .delegate = navigationController.viewControllers.first as? UIAdaptivePresentationControllerDelegate
        mainActivateNavigationController.pushViewController(controller, animated: true)
        navigationController.present(mainActivateNavigationController, animated: true)
    }
    
    private func updateParentViewControllerAppearance() {
        if let controller = navigationController.viewControllers.first {
            controller.viewWillAppear(true)
        }
    }
}

extension ActivateCardFlowCoordinator: ValidateCardCoordinatorDelegate {
    public func didConfirmValidateCard() {
        let controller = CreateCardPasswordFactory.make(self)
        mainActivateNavigationController.pushViewController(controller, animated: true)
    }
    
    public func didCloseValidateCard() {
        didFinishFlow()
    }
    
    public func showIncorrectDigitFeedbackError(with model: StatefulFeedbackViewModel, url: URL) {
        let controller = CustomerSupportFeedbackViewController(model: model, url: url)
        controller.primaryButtonCallBack = { [weak self] in
            guard let self = self else { return }
            self.mainActivateNavigationController.dismiss(animated: true, completion: {
                CardTrackingFlowCoordinator(navigationController: self.navigationController).start()
            })
        }
        
        controller.helpCenterCallBack = { url in
            let deeplinkResolver = HelpcenterDeeplinkResolver()
            _ = deeplinkResolver.open(url: url, isAuthenticated: true, from: controller)
        }
        
        mainActivateNavigationController.pushViewController(controller, animated: true)
    }
}

extension ActivateCardFlowCoordinator: PasswordSecurityTipsCoordinatorDelegate {
    public func didOpenCardPassword() {
        let controller = CreateCardPasswordFactory.make(self)
        mainActivateNavigationController.pushViewController(controller, animated: true)
    }
}

extension ActivateCardFlowCoordinator: CreateCardPasswordCoordinatorDelegate {
    public func didOpenHelperSecurityTips() {
        let controller = PasswordSecurityTipsFactory.make(delegate: self)
        let navigation = UINavigationController(rootViewController: controller)
        mainActivateNavigationController.present(navigation, animated: true)
    }
    
    public func didSuccessCardPassword() {
        let controller = ActivateCardSuccessFactory.make(self)
        mainActivateNavigationController.pushViewController(controller, animated: true)
    }
    
    public func didDismiss() {
        Analytics.shared.log(UnblockCardEvent.passwordTips)
    }
}

extension ActivateCardFlowCoordinator: ActivateCardFlowCoordinatorDelegate {
    public func didFinishFlow() {
        mainActivateNavigationController.dismiss(animated: true)
        updateParentViewControllerAppearance()
    }
    
    public func didOpenWallet() {
        didFinishFlow()
        
        guard let url = URL(string: "picpay://picpay/recharge") else {
            return
        }
        CardSetup.deeplinkInstance?.open(url: url)
    }
}
