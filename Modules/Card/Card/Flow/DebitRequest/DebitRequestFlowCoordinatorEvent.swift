import AnalyticsModule

enum DebitRequestFlowCoordinatorEvent: AnalyticsKeyProtocol {
    case flowWithoutOnboarding
    
    private var name: String {
        switch self {
        case .flowWithoutOnboarding:
            return "Offer without Webview"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .flowWithoutOnboarding:
            return [:]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PicPay Card - Request - \(name)", properties: properties, providers: providers)
    }
}
