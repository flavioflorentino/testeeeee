import UI

public struct VirtualCardFlowInput {
    let button: VirtualCardButton

    public init(button: VirtualCardButton) {
        self.button = button
    }
}

public protocol VirtualCardFlowCoordinatorDelegate: AnyObject {
}

public final class VirtualCardFlowCoordinator: Coordinating {
    public var childViewController: [UIViewController] = []
    public var viewController: UIViewController?

    public weak var delegate: VirtualCardFlowCoordinatorDelegate?
    private let navigationController: UINavigationController
    private let mainNavigationController = StyledNavigationController()
    private let dependencies = DependencyContainer()
    private var input: VirtualCardFlowInput?
    private let initialChildViewControllersCount: Int
    private let isModal: Bool

    private var navigationBeingPresented: UINavigationController {
        isModal ? mainNavigationController : navigationController
    }
    
    private enum NavigationStackAction {
        case removeFlow
        case removeDeleteConfirmation
        case removeDeleted
    }

    public init(navigation: UINavigationController, isModal: Bool = false, input: VirtualCardFlowInput? = nil) {
        self.initialChildViewControllersCount = isModal ? 0 : navigation.viewControllers.count
        self.navigationController = navigation
        self.isModal = isModal
        self.input = input
    }

    public func start() {
        guard let input = input else {
            show(controller: VirtualCardLoadingFactory.make(delegate: self))
            return
        }
        navigationController.setNavigationBarHidden(false, animated: true)
        switch input.button.action {
        case .onboarding:
            show(controller: VirtualCardOnboardingFactory.make(delegate: self))
        case .deleted:
            show(controller: VirtualCardDeletedFactory.make(delegate: self, input: input))
        case .cardInfo:
            show(controller: VirtualCardHomeFactory.make(delegate: self, input: input))
        case .none:
            return
        }
    }

    private func show(controller: UIViewController) {
        controller.hidesBottomBarWhenPushed = true
        childViewController.append(controller)
        if isModal {
            mainNavigationController.pushViewController(controller, animated: true)
            navigationController.present(mainNavigationController, animated: true)
        } else {
            navigationController.pushViewController(controller, animated: true)
        }
    }
    
    private func push(controller: UIViewController) {
        viewController = controller
        navigationBeingPresented.pushViewController(controller, animated: true)
    }
    
    private func replace(with newController: UIViewController) {
        navigationBeingPresented.viewControllers = [newController]
    }

    private func dismiss() {
        if isModal {
            mainNavigationController.dismiss(animated: true)
        } else {
            navigationController.popViewController(animated: true)
        }
    }

    private func reorganizeNavigationStack(action: NavigationStackAction) {
        switch action {
        case .removeFlow:
            guard initialChildViewControllersCount < (navigationBeingPresented.viewControllers.count - 1) else { return }
            let subrange = initialChildViewControllersCount..<(navigationBeingPresented.viewControllers.count - 1)
            navigationBeingPresented.viewControllers.removeSubrange(subrange)
        case .removeDeleteConfirmation:
            navigationBeingPresented.viewControllers.removeAll(where: { $0.isKind(of: VirtualCardDeleteWarningViewController.self) })
        case .removeDeleted:
            navigationBeingPresented.viewControllers.removeAll(where: { $0.isKind(of: VirtualCardDeletedViewController.self) })
        }
    }
}

extension VirtualCardFlowCoordinator: VirtualCardOnboardingCoordinatorDelegate {
    public func onboardingDidNextStep(action: VirtualCardOnboardingAction) {
        switch action {
        case .dismiss:
            dismiss()
        case .confirm:
            guard let input = input else { return }
            push(controller: VirtualCardHomeFactory.make(delegate: self, input: input))
        }
    }
}

extension VirtualCardFlowCoordinator: VirtualCardHomeCoordinatorDelegate {
    public func homeDidNextStep(action: VirtualCardHomeAction) {
        switch action {
        case .dismiss:
            dismiss()
        case .succeeded:
            reorganizeNavigationStack(action: .removeFlow)
        case .delete:
            push(controller: VirtualCardDeleteWarningFactory.make(delegate: self))
        default:
            return
        }
    }
}

extension VirtualCardFlowCoordinator: VirtualCardDeleteWarningCoordinatorDelegate {
    public func deleteWarningDidNextStep(action: VirtualCardDeleteWarningAction) {
        switch action {
        case .dismiss:
            dismiss()
        case .confirm:
            guard let input = input else { return }
            let inputFromDeleteWarning = VirtualCardFlowInput(
                button: VirtualCardButton(action: .cardInfo, newFeature: input.button.newFeature)
            )
            push(controller: VirtualCardDeletedFactory.make(delegate: self, input: inputFromDeleteWarning))
        }
    }
}

extension VirtualCardFlowCoordinator: VirtualCardDeletedCoordinatorDelegate {
    public func deletedDidNextStep(action: VirtualCardDeletedAction) {
        switch action {
        case .dismiss:
            dismiss()
        case .failed:
            reorganizeNavigationStack(action: .removeDeleteConfirmation)
        case .succeeded:
            reorganizeNavigationStack(action: .removeFlow)
        case .request:
            guard let input = input else { return }
            let inputFromDeleted = VirtualCardFlowInput(
                button: VirtualCardButton(action: .deleted, newFeature: input.button.newFeature)
            )
            push(controller: VirtualCardHomeFactory.make(delegate: self, input: inputFromDeleted))
        }
    }
}

extension VirtualCardFlowCoordinator: VirtualCardLoadingCoordinatorDelegate {
    func loadingDidNextStep(action: VirtualCardLoadingAction) {
        switch action {
        case .success(action: let action):
            let input = VirtualCardFlowInput(button: VirtualCardButton(action: action, newFeature: false))
            self.input = input
            switch action {
            case .onboarding:
                replace(with: VirtualCardOnboardingFactory.make(delegate: self))
            case .deleted:
                replace(with: VirtualCardDeletedFactory.make(delegate: self, input: input))
            case .cardInfo:
                replace(with: VirtualCardHomeFactory.make(delegate: self, input: input))
            case .none:
                return
            }
        case .close:
            dismiss()
        }
    }
}
