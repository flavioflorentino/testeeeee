import UIKit
import UI

public final class RejectedCreditFlowCoordinator: FlowsCoordinating {    
    public var viewController: UIViewController?
    public var childViewController: [UIViewController] = []
    
    private let mainRequestNavigationController = UINavigationController()
    private var navigationController: UINavigationController
    private let parentNavigation: UINavigationController?
    
    public init(navigationController: UINavigationController, parentNavigation: UINavigationController? = nil) {
        self.navigationController = navigationController
        self.parentNavigation = parentNavigation
    }
    
    public func start() {
        let controller = RejectedCreditFactory.make(coordinatorDelegate: self)
        mainRequestNavigationController
            .presentationController?
            .delegate = navigationController.viewControllers.first as? UIAdaptivePresentationControllerDelegate
        mainRequestNavigationController.pushViewController(controller, animated: true)
        navigationController.present(mainRequestNavigationController, animated: true)
        viewController = mainRequestNavigationController
    }
    
    public func startAnotherFlow() {
        let controller = RejectedCreditFactory.make(coordinatorDelegate: self)
        mainRequestNavigationController.pushViewController(controller, animated: true)
        navigationController.present(mainRequestNavigationController, animated: true)
    }
    
    private func finishFlow() {
        parentNavigation?.dismiss(animated: true)
        mainRequestNavigationController.dismiss(animated: true)
        updateParentViewControllerAppearance()
    }
    
    private func updateParentViewControllerAppearance() {
        if let controller = navigationController.viewControllers.first {
            controller.viewWillAppear(true)
        }
    }
    
    private func debitCoordinatorOutput(coordinatorOutput: DebitCoordinatorOutput) {
        if case .close = coordinatorOutput {
            finishFlow()
        } else if case .update = coordinatorOutput {
            mainRequestNavigationController.popViewController(animated: true)
        }
    }
}

// MARK: RejectedCreditCoordinatingDelegate
extension RejectedCreditFlowCoordinator: RejectedCreditCoordinatingDelegate {
    func didChangeAddress(cardForm: CardForm) {
        let controller = DebitDeliveryAddressFactory.make(cardForm: cardForm, debitCoordinatorOutput: debitCoordinatorOutput)
        mainRequestNavigationController.pushViewController(controller, animated: true)
    }
    
    func didAskCard() {
        let controller = DebitDeliverySuccessFactory.make { [weak self] _ in
            self?.finishFlow()
        }
        mainRequestNavigationController.pushViewController(controller, animated: true)
    }
    
    func didCloseRejectCredit() {
        finishFlow()
    }
}
