import UI

public protocol CardHomeFlowCoordinatorDelegate: AnyObject {
    func openCardSettings()
}

public final class CardHomeFlowCoordinator: Coordinating {
    public var childViewController: [UIViewController] = []
    public var viewController: UIViewController?
    private let navigationController: UINavigationController
    
    public init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    public func start() {
        let controller = CardHomeFactory.make(delegate: self)
        navigationController.pushViewController(controller, animated: true)
    }
    
    fileprivate func showCardSettings() {
        let coordinator = CardSettingsFlowCoordinator(navigationController: navigationController)
        coordinator.delegate = self
        coordinator.start()
    }
}

extension CardHomeFlowCoordinator: CardHomeFlowCoordinatorDelegate {
    public func openCardSettings() {
        showCardSettings()
    }
}

extension CardHomeFlowCoordinator: CardSettingsFlowDelegate {
    public func didOpenDueDayCard(from navigationController: UINavigationController) {}
    
    public func didOpenLimitCard(from navigationController: UINavigationController, didUpdateCardLimit: @escaping () -> Void) {}
    
    public func didAskAuthentication(onSuccess: @escaping (String) -> Void) {}
}
