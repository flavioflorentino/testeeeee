import SafariServices
import UI
import UIKit

public enum HiringFlowAction {
    case dueDay(cardForm: CardForm)
    case confirmAddress(cardForm: CardForm, bestPurchaseDay: Int)
    case resume
    case creditActivated
    case creditWaitingActivation
    case cashback
}

public protocol HiringFlowCoordinating: AnyObject {
    func perform(action: HiringFlowAction)
    func close()
}

public final class HiringFlowCoordinator: Coordinating {
    public var childViewController: [UIViewController] = []
    public var viewController: UIViewController?
    
    private let mainRequestNavigationController = UINavigationController()
    private let navigationController: UINavigationController
	private let cashbackURL = URL(string: "https://cdn.picpay.com/5porcento/5p-webview-card.html")
    private let walletDeepLink = "picpay://picpay/wallet"
    private let isUpgradeDebitToCredit: Bool
    
    var cardForm: CardForm?
    var bestPurchaseDay: Int?
    
    public init(isUpgradeDebitToCredit: Bool, navigationController: UINavigationController) {
        self.isUpgradeDebitToCredit = isUpgradeDebitToCredit
        self.navigationController = navigationController
    }
    
    public func start() {
        let controller = CreditPreApprovedFactory.make(delegate: self)
        mainRequestNavigationController
            .presentationController?
            .delegate = navigationController.viewControllers.first as? UIAdaptivePresentationControllerDelegate
        navigationController.viewControllers = [controller]
    }
        
    private func deliveryAddressOutput(deliveryAddressAction: DeliveryConfirmAddressAction) {
        switch deliveryAddressAction {
        case .changeAddress, .close:
            let controller = ChangeAddressPopupFactory.make()
            let navigation = UINavigationController(rootViewController: controller)
            navigationController.present(navigation, animated: true)
            
        case .didConfirm:
            perform(action: .resume)
        }
    }
    
    private func createSuccessViewController() -> UIViewController {
        guard isUpgradeDebitToCredit else {
            return NewHiringSuccessFactory.make(delegate: self)
        }
        return UpgradeDebitSuccessFactory.make { [weak self] output in
            self?.didUpgradeDebitSuccessOutput(output)
        }
    }

    func didUpgradeDebitSuccessOutput(_ output: UpgradeDebitSuccessCoordinatorOutput) {
        switch output {
        case .openWallet:
            finishFlow {
                guard let url = URL(string: self.walletDeepLink) else { return }
                CardSetup.deeplinkInstance?.open(url: url)
            }
        case .close:
            finishFlow()
        }
    }
    
    private func open(url: URL) -> UIViewController {
        let safariVC = SFSafariViewController(url: url)
        if #available(iOS 11.0, *) {
            safariVC.configuration.barCollapsingEnabled = false
            safariVC.configuration.entersReaderIfAvailable = true
        }
        return safariVC
    }
    
    private func finishFlow(_ completion: (() -> Void)? = nil) {
        navigationController.dismiss(animated: true, completion: completion)
    }
}

// MARK: - CreditPreApprovedCoordinating
extension HiringFlowCoordinator: HiringFlowCoordinating {
    public func perform(action: HiringFlowAction) {
        let controller: UIViewController
        
        switch action {
        case let .dueDay(cardForm):
            self.cardForm = cardForm
            controller = DueDayFactory.make(delegate: self, cardForm: cardForm)
            
        case let .confirmAddress(cardForm, bestPurchaseDay):
            self.cardForm = cardForm
            self.bestPurchaseDay = bestPurchaseDay
            let dependencies = DependencyContainer()
            controller = DeliveryConfirmAddressFactory.make(
                service: HiringDeliveryConfirmAddressService(),
                analyticsContext: CreditDeliveryConfirmAddressAnalytics(dependencies: dependencies),
                deliveryAddress: cardForm.homeAddress,
                coordinatorOutput: deliveryAddressOutput
            )
            
        case .resume:
            guard let cardForm = cardForm, let bestAvailableDay = bestPurchaseDay else {
                return
            }
            controller = HiringResumeFactory.make(cardForm: cardForm,
                                                  bestAvailableDay: bestAvailableDay,
                                                  isUpgradeDebitToCredit: isUpgradeDebitToCredit,
                                                  delegate: self)
            
        case .creditActivated:
            controller = createSuccessViewController()
            
        case .creditWaitingActivation:
            controller = HiringWaitingActivationFactory.make(isUpgrade: isUpgradeDebitToCredit)
            
        case .cashback:
            guard let url = cashbackURL else {
                finishFlow()
                return
            }
            navigationController.navigationBar.isHidden = true
            controller = open(url: url)
        }
        navigationController.pushViewController(controller, animated: true)
    }
    
    public func close() {
        finishFlow()
    }
}

extension HiringFlowCoordinator: DueDayCoordinatingDelegate {
    public func didConfirmDueDay(cardForm: CardForm?, bestPurchaseDay: Int?) {
        guard let cardForm = cardForm, let bestPurchaseDay = bestPurchaseDay else {
            return
        }
        guard !isUpgradeDebitToCredit else {
            self.bestPurchaseDay = bestPurchaseDay
            perform(action: .resume)
            return
        }
        perform(action: .confirmAddress(cardForm: cardForm, bestPurchaseDay: bestPurchaseDay))
    }
}
