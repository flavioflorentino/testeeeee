import AnalyticsModule
import FeatureFlag
import UIKit
import UI

public enum DebitCoordinatorOutput {
    case getServiceResponse(cardForm: CardForm)
    case update(cardForm: CardForm, currentStep: DebitStep)
    case confirmAddress(registrationStatus: RegistrationStatus)
    case close
}

public final class DebitRequestFlowCoordinator: FlowsCoordinating {
    private let navigationController: UINavigationController
    private var mainRequestNavigationController = UINavigationController()
    public var childViewController: [UIViewController] = []
    public var viewController: UIViewController?
    private var currentStep: DebitStep?
    private var cardForm: CardForm?
    private let dependencyContainer = DependencyContainer()
    private let parentNavigation: UINavigationController?
    
    private let defaultStepSequence: [DebitStep] = {
        let container = DependencyContainer()
        if container.featureManager.isActive(.isCardOnboardingVisibilityActivated) {
            return [.onboarding, .personalData, .address, .confirmation, .success]
        } else {
            container.analytics.log(DebitRequestFlowCoordinatorEvent.flowWithoutOnboarding)
            return [.personalData, .address, .confirmation, .success]
        }
    }()
    
    public init(navigationController: UINavigationController, parentNavigation: UINavigationController? = nil) {
        self.navigationController = navigationController
        self.parentNavigation = parentNavigation
    }
    
    public func start() {
        let controller = DebitLoadingFactory.make(debitCoordinatorOutput: debitCoordinatorOutput)
        mainRequestNavigationController
            .presentationController?
            .delegate = navigationController.viewControllers.first as? UIAdaptivePresentationControllerDelegate
        mainRequestNavigationController.pushViewController(controller, animated: true)
        navigationController.present(mainRequestNavigationController, animated: true)
    }
    
    public func startAnotherFlow() {
        let controller = DebitLoadingFactory.make(debitCoordinatorOutput: debitCoordinatorOutput)
        mainRequestNavigationController.pushViewController(controller, animated: true)
        navigationController.present(mainRequestNavigationController, animated: true)
    }
    
    private func debitCoordinatorOutput(coordinatorOutput: DebitCoordinatorOutput) {
        switch coordinatorOutput {
        case .getServiceResponse(let cardForm):
            createFlowStack(with: cardForm)
        case let .update(cardForm, currentStep):
            updateCurrentStep(with: currentStep)
            continueFlow(cardForm: cardForm)
        case .close:
            finishFlow()
        case .confirmAddress(let registrationStatus):
            addressConfirmed(with: registrationStatus)
            updateCurrentStep(with: cardForm?.registrationStep)
            guard let cardForm = cardForm else {
                return
            }
            continueFlow(cardForm: cardForm)
        }
    }
    
    private func addressConfirmed(with registrationStatus: RegistrationStatus) {
        if case .waitingActivation = registrationStatus {
            cardForm?.registrationStep = .waitingActivation
        } else if case .activated = registrationStatus {
            cardForm?.registrationStep = .success
        }
    }
    
    private func finishFlow() {
        parentNavigation?.dismiss(animated: true)
        mainRequestNavigationController.dismiss(animated: true)
        updateParentViewControllerAppearance()
    }
    
    private func updateParentViewControllerAppearance() {
        if let controller = navigationController.viewControllers.first {
            controller.viewWillAppear(true)
        }
    }
    
    private func createFlowStack(with cardForm: CardForm) {
        self.cardForm = cardForm
        guard let registrationStep = cardForm.registrationStep else {
            continueFlow(cardForm: cardForm)
            return
        }
        var controllers: [UIViewController] = []
        
        let index = Int(defaultStepSequence.firstIndex { $0 == registrationStep } ?? 0) + 1
        
        for i in 0...index {
            let formStep = defaultStepSequence[i]
            let controller = getControllerWith(registrationStep: formStep, cardForm: cardForm)
            controllers.append(controller)
            currentStep = formStep
        }
        mainRequestNavigationController.viewControllers = controllers
    }
    
    private func getControllerWith(registrationStep: DebitStep?, cardForm: CardForm) -> UIViewController {
        let container = DependencyContainer()
        guard let registrationStep = registrationStep else {
            return DynamicOnboardingFactory.make(with: .debit, delegate: self)
        }
        switch registrationStep {
        case .onboarding:
            return DynamicOnboardingFactory.make(with: .debit, delegate: self)
        case .personalData:
            return DebitPersonalInfoFactory.make(cardForm: cardForm, debitCoordinatorOutput: debitCoordinatorOutput, delegate: self)
        case .address:
            return DebitDeliveryAddressFactory.make(cardForm: cardForm, debitCoordinatorOutput: debitCoordinatorOutput)
        case .confirmation:
            let analyticsContext = DebitDeliveryConfirmAddressAnalytics(dependencies: container)
            return DeliveryConfirmAddressFactory.make(service: DebitDeliveryConfirmAddressService(dependencies: container),
                                                      analyticsContext: analyticsContext,
                                                      deliveryAddress: cardForm.homeAddress,
                                                      coordinatorOutput: deliveryAddressOutput)
        case .success, .waitingActivation:
            return DebitDeliverySuccessFactory.make(debitCoordinatorOutput: debitCoordinatorOutput)
        }
    }
    
    private func deliveryAddressOutput(deliveryAddressAction: DeliveryConfirmAddressAction) {
        switch deliveryAddressAction {
        case .changeAddress, .close:
            mainRequestNavigationController.popViewController(animated: true)
        case .didConfirm(let registrationStatus):
            guard let registrationStatus = registrationStatus else {
                return
            }
            debitCoordinatorOutput(coordinatorOutput: .confirmAddress(registrationStatus: registrationStatus))
        }
    }
    
    private func continueFlow(cardForm: CardForm) {
        self.cardForm = cardForm
        let controller = getControllerWith(registrationStep: currentStep, cardForm: cardForm)
        mainRequestNavigationController.pushViewController(controller, animated: false)
    }
    
    private func updateCurrentStep(with debitStep: DebitStep?) {
        self.currentStep = debitStep
        guard let step = currentStep else {
            currentStep = .onboarding
            return
        }
        switch step {
        case .onboarding:
            currentStep = .personalData
        case .personalData:
            currentStep = .address
        case .address:
            currentStep = .confirmation
        case .confirmation:
            currentStep = .success
        case .success,
             .waitingActivation:
            break
        }
    }
}

extension DebitRequestFlowCoordinator: DynamicOnboardingCoordinatorDelegate {
    public func onRegistrationDidRequest() {
        cardForm?.registrationStep = .onboarding
        updateCurrentStep(with: .onboarding)
        guard let cardForm = cardForm else { return }
        continueFlow(cardForm: cardForm)
    }
    
    public func onboardingDidClose() {
        finishFlow()
    }
}

extension DebitRequestFlowCoordinator: DebitPersonalInfoCoordinatorDelegate {
    public func debitPersonalInfoDidClose() {
        finishFlow()
    }
}
