import UI
import UIKit

public protocol ForgotPasswordCardFlowDelegate: AnyObject {
    func didAskAuthentication(onSuccess: @escaping (String) -> Void)
}

public final class ForgotPasswordCardFlowCoordinator: Coordinating {
    private let navigationController: UINavigationController
    public var childViewController: [UIViewController] = []
    public var viewController: UIViewController?
    private weak var forgotPasswordCardFlowDelegate: ForgotPasswordCardFlowDelegate?
    
    public init(navigationController: UINavigationController, delegate: ForgotPasswordCardFlowDelegate) {
        self.navigationController = navigationController
        forgotPasswordCardFlowDelegate = delegate
    }
    
    public func start() {
        let controller = ValidateCardFactory.make(self, type: .forgotPasswordCard, analyticsContext: ValidateCardForgotPasswordCardAnalytics())
        navigationController.pushViewController(controller, animated: true)
    }
    
    private func didFinishFlow() {
        navigationController.dismiss(animated: true)
    }
}
extension ForgotPasswordCardFlowCoordinator: ValidateCardCoordinatorDelegate {
    public func didConfirmValidateCard() {
        let controller = CreateNewCardPasswordFactory.make(delegate: self)
        navigationController.pushViewController(controller, animated: true)
    }
    
    public func didCloseValidateCard() { }
    public func showIncorrectDigitFeedbackError(with model: StatefulFeedbackViewModel, url: URL) {}
}

extension ForgotPasswordCardFlowCoordinator: CreateNewCardPasswordCoordinatorDelegate {
    public func didConfirmCreateNewPin(onSuccess: @escaping (String) -> Void) {
        forgotPasswordCardFlowDelegate?.didAskAuthentication(onSuccess: onSuccess)
    }
    
    public func didSuccessCreateNewPin() {
        let controller = FeedbackStateFactory.make(type: .passwordChanged, delegate: self)
        controller.navigationItem.setHidesBackButton(true, animated: true)
        navigationController.pushViewController(controller, animated: true)
    }
    
    public func didOpenHelperSecurityTips() {
        let controller = PasswordSecurityTipsFactory.make(delegate: nil)
        let navigation = UINavigationController(rootViewController: controller)
        navigationController.present(navigation, animated: true)
    }
}

extension ForgotPasswordCardFlowCoordinator: FeedbackStateDelegate {
    public func didCloseFeedbackState() {
        navigationController.dismiss(animated: true)
    }
    public func didConfirmFeedbackState(feedbackType: FeedbackStateType) {
        navigationController.dismiss(animated: true)
    }
}
