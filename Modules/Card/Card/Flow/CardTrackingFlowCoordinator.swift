import UIKit
import UI
import CustomerSupport

public enum CardTrackingCoordinatorOutput {
    case getServiceResponse(trackingData: CardTrackingHistory)
    case close
}

public enum CardHistoryOutput {
    case cardArrived(cardType: CardTrackingType)
    case errorButton(type: CardTrackingType)
    case close
    case openZendesk
    case openHelpCenter(url: URL)
}

public enum CardArrivedCoordinatorOutput {
    case unblockCard
    case close
    case openZendesk
    case openHelpCenter(url: URL)
}

public final class CardTrackingFlowCoordinator: Coordinating {
    private let navigationController: UINavigationController
    private let mainActivateNavigationController = StyledNavigationController()
    public var childViewController: [UIViewController] = []
    public var viewController: UIViewController?

    private var cardType: CardTrackingType = .unknown

    public init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    public func start() {
        let controller = CardTrackingLoadingFactory.make(coordinatorOutput: coordinatorOutput)
        mainActivateNavigationController
            .presentationController?
            .delegate = navigationController.viewControllers.first as? UIAdaptivePresentationControllerDelegate
        mainActivateNavigationController.pushViewController(controller, animated: true)
        navigationController.present(mainActivateNavigationController, animated: true)
        viewController = controller
    }

    private func coordinatorOutput(coordinatorOutput: CardTrackingCoordinatorOutput) {
        let controller: UIViewController
        switch coordinatorOutput {
        case .getServiceResponse(let trackingData):
            cardType = trackingData.cardType
            guard trackingData.arrived else {
                controller = CardTrackingFactory.make(cardTrackingHistory: trackingData, cardHistoryOutput: cardHistoryOutput)
                mainActivateNavigationController.viewControllers = [controller]
                return
            }
            controller = CardArrivedFactory.make(coordinatorOutput: arrivedCardOutput, cardType: trackingData.cardType)
            mainActivateNavigationController.viewControllers = [controller]
        case .close:
            finishFlow()
        }
    }

    private func cardHistoryOutput(coordinatorOutput: CardHistoryOutput) {
        switch coordinatorOutput {
        case .cardArrived(let type):
            let controller = CardArrivedFactory.make(coordinatorOutput: arrivedCardOutput, cardType: type)
            mainActivateNavigationController.pushViewController(controller, animated: true)
        case .close:
            finishFlow()
        case .errorButton(let type):
            let controller = UnsuccessfulDeliveryCardFactory.make(cardType: type)
            mainActivateNavigationController.pushViewController(controller, animated: true)
        case .openZendesk:
            openZendesk()
        case .openHelpCenter(let url):
            openHelpCenter(with: url)
        }
    }

    private func arrivedCardOutput(arrivedCardOutput: CardArrivedCoordinatorOutput) {
        switch arrivedCardOutput {
        case .unblockCard:
            let type = cardType.deeplinkCardType
            finishFlow {
                _ = CardDeeplinkHelper(cardType: type).handleDeeplink()
            }
        case .openZendesk:
            openZendesk()
        case .openHelpCenter(let url):
            openHelpCenter(with: url)
        case .close:
            finishFlow()
        }
    }

    private func finishFlow(completion: (() -> Void)? = nil) {
        mainActivateNavigationController.dismiss(animated: true, completion: completion)
    }

    func openHelpCenter(with url: URL) {
        let deeplinkResolver = HelpcenterDeeplinkResolver()
        _ = deeplinkResolver.open(url: url, isAuthenticated: true, from: mainActivateNavigationController)
    }
    
    func openZendesk() {
        let option = CardTrackingHistoryCustomer().option()
        let faq = FAQFactory.make(option: option)
        viewController?.present(faq, animated: true)
    }
}
