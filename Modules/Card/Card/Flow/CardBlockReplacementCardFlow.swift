import UI
import UIKit

public protocol CardBlockReplacementCardFlowDelegate: AnyObject {
    func didBlockConfirm(onSuccess: @escaping (String, Bool) -> Void, cancelHandler: @escaping () -> Void)
    func updateParentViewController()
}

public protocol CardBlockReplacementCardFlowCoordinating: Coordinating {
    var delegate: CardBlockReplacementCardFlowDelegate? { get set }
}

public final class CardBlockReplacementCardFlowCoordinator: CardBlockReplacementCardFlowCoordinating {
    public var childViewController: [UIViewController] = []
    public var viewController: UIViewController?
    public weak var delegate: CardBlockReplacementCardFlowDelegate?
    private let navigationController: UINavigationController
    private let settingsData: CardSettingsData
    private let container = DependencyContainer()
    
    private var cardReplacementEnable: Bool {
        container.featureManager.isActive(.opsCardReplacement)
    }

    init(navigationController: UINavigationController, settingsData: CardSettingsData) {
        self.navigationController = navigationController
        self.settingsData = settingsData
    }

    public func start() {
        let controller = CardBlockReplacementReasonFactory.make(delegate: self, typeFlow: .block)
        controller.hidesBottomBarWhenPushed = true
        childViewController.append(controller)
        navigationController.pushViewController(controller, animated: true)
    }

    private func dismissFlow() {
        navigationController.popViewController(animated: true)
    }
}

extension CardBlockReplacementCardFlowCoordinator: CardBlockReplacementReasonCoordinatorDelegate {
    public func cancelBlockReason() {
        dismissFlow()
    }

    public func confirmBlock(with reason: CardBlockReplacementReason) {
        let controller = CardBlockFactory.make(delegate: self, reason: reason)
        controller.hidesBottomBarWhenPushed = true
        childViewController.append(controller)
        navigationController.pushViewController(controller, animated: true)
    }
}

extension CardBlockReplacementCardFlowCoordinator: CardBlockDelegate {
    public func didBlockConfirm(onSuccessAuth: @escaping (String, Bool) -> Void, cancelHandler: @escaping () -> Void) {
        delegate?.didBlockConfirm(onSuccess: onSuccessAuth, cancelHandler: cancelHandler)
    }

    public func didBlockSuccess() {
        let controller = FeedbackStateFactory.make(
            type: cardReplacementEnable ? .cardBlockedWithCardReplacement : .cardBlocked,
            delegate: self
        )
        navigationController.pushViewController(controller, animated: true)
    }
}

extension CardBlockReplacementCardFlowCoordinator: FeedbackStateDelegate {
    public func didCloseFeedbackState() {
        dismissFlow()
        delegate?.updateParentViewController()
    }
    
    public func didConfirmFeedbackState(feedbackType: FeedbackStateType) {
        if feedbackType == .cardBlockedWithCardReplacement {
            let controller = RequestPhysicalCardLoadingFactory.make(
                requestCardCoordinatorOutput: requestLoadingOutput,
                service: RequestPhysicalCardLoadingService(dependencies: container)
            )
            navigationController.pushViewController(controller, animated: true)
        } else {
            dismissFlow()
            delegate?.updateParentViewController()
        }
    }
    
    private func requestLoadingOutput(requestPhysicalOutput: RequestPhysicalCardCoordinatorOutput) {
        switch requestPhysicalOutput {
        case .getServiceResponse(let deliveryAddress):
            let controller = DeliveryConfirmAddressFactory.make(
                service: ReplacementCardDeliveryConfirmAddressService(dependencies: container),
                analyticsContext: ReplacementCardDeliveryConfirmAddressAnalytics(dependencies: container),
                deliveryAddress: deliveryAddress,
                coordinatorOutput: deliveryAddressOutput
            )
            navigationController.viewControllers = [controller]
        case .close:
            dismissFlow()
            delegate?.updateParentViewController()
        }
    }
    
    private func deliveryAddressOutput(deliveryAddressAction: DeliveryConfirmAddressAction) {
        switch deliveryAddressAction {
        case .changeAddress, .close:
            dismissFlow()
            delegate?.updateParentViewController()
        case .didConfirm:
            switch settingsData.cardType {
            case .multiple:
                let controller = ChosenDeliveryAddressSuccessFactory.make(delegate: self)
                navigationController.pushViewController(controller, animated: true)
            case .debit:
                let controller = DebitDeliverySuccessFactory.make { _ in
                    self.dismissFlow()
                    self.delegate?.updateParentViewController()
                }
                navigationController.pushViewController(controller, animated: true)
            case .none:
                return
            }
        }
    }
}

extension CardBlockReplacementCardFlowCoordinator: ChosenDeliveryAddressSuccessCoordinatorDelegate {
    public func didCloseChosenDeliveryAddressSuccess() {
        dismissFlow()
        delegate?.updateParentViewController()
    }
}
