import AnalyticsModule
import UI
import UIKit

public final class HiringAccountSuccessFlowCoordinator: Coordinating {
    private let navigationController: UINavigationController
    private let mainActivateNavigationController = UINavigationController()
    public var childViewController: [UIViewController] = []
    public var viewController: UIViewController?
    private let creditHomeDeeplink = "picpay://picpay/credit/registration"

    public init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    public func start() {
        let controller = HiringAccountSuccessFactory.make(delegate: self)
        mainActivateNavigationController
            .presentationController?
            .delegate = navigationController.viewControllers.first as? UIAdaptivePresentationControllerDelegate
        mainActivateNavigationController.pushViewController(controller, animated: true)
        navigationController.present(mainActivateNavigationController, animated: true)
    }
    
    private func updateParentViewControllerAppearance() {
        if let controller = navigationController.viewControllers.first {
            controller.viewWillAppear(true)
        }
    }
    
    private func didFinishFlow() {
        mainActivateNavigationController.dismiss(animated: true)
        updateParentViewControllerAppearance()
    }
}

// MARK: - HiringAccountSuccessDelegate
extension HiringAccountSuccessFlowCoordinator: HiringAccountSuccessDelegate {
    func close() {
        didFinishFlow()
    }
    
    func didConfirm() {
        didFinishFlow()
        guard let url = URL(string: creditHomeDeeplink) else {
            return
        }
        CardSetup.deeplinkInstance?.open(url: url)
    }
}
