import FeatureFlag
import SafariServices
import UI
import UIKit

public enum RequestPhysicalCardCoordinatorOutput {
    case getServiceResponse(deliveryAddress: HomeAddress?)
    case close
}

public final class RequestPhysicalCardFlowCoordinator: Coordinating {
    private let navigationController: UINavigationController
    private let mainRequestNavigationController = UINavigationController()
    public var childViewController: [UIViewController] = []
    public var viewController: UIViewController?
    
    private let helpURL = FeatureManager.text(.featureFaqRequestCard)
    
    public init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    public func start() {
        let dependencies = DependencyContainer()
        let controller = RequestPhysicalCardLoadingFactory.make(
            requestCardCoordinatorOutput: requestLoadingOutput,
            service: RequestPhysicalCardLoadingService(dependencies: dependencies)
        )
        mainRequestNavigationController
            .presentationController?
            .delegate = navigationController.viewControllers.first as? UIAdaptivePresentationControllerDelegate
        mainRequestNavigationController.pushViewController(controller, animated: true)
        navigationController.present(mainRequestNavigationController, animated: true)
    }
    
    private func requestLoadingOutput(requestPhysicalOutput: RequestPhysicalCardCoordinatorOutput) {
        switch requestPhysicalOutput {
        case .getServiceResponse(let deliveryAddress):
            let container = DependencyContainer()
            let deliveryAddressController = DeliveryConfirmAddressFactory.make(
                service: CreditDeliveryConfirmAddressService(dependencies: container),
                analyticsContext: CreditDeliveryConfirmAddressAnalytics(dependencies: container),
                deliveryAddress: deliveryAddress,
                coordinatorOutput: deliveryAddressOutput)
            mainRequestNavigationController.viewControllers = [deliveryAddressController]
        case .close:
            didFinishFlow()
        }
    }
    
    private func deliveryAddressOutput(deliveryAddressAction: DeliveryConfirmAddressAction) {
        switch deliveryAddressAction {
        case .didConfirm:
            let controller = ChosenDeliveryAddressSuccessFactory.make(delegate: self)
            mainRequestNavigationController.pushViewController(controller, animated: true)
        case .changeAddress:
            guard let url = URL(string: helpURL) else {
                return
            }
            openExternalLink(url: url)
        case .close:
            didFinishFlow()
        }
    }
    
    private func openExternalLink(url: URL) {
        let safariVC = SFSafariViewController(url: url)
        
        if #available(iOS 11.0, *) {
            safariVC.configuration.barCollapsingEnabled = true
            safariVC.configuration.entersReaderIfAvailable = true
        }
        
        mainRequestNavigationController.present(safariVC, animated: true)
    }
    
    private func didFinishFlow() {
        navigationController.dismiss(animated: true)
        updateParentViewControllerAppearance()
    }
    
    private func updateParentViewControllerAppearance() {
        if let controller = navigationController.viewControllers.first {
            controller.viewWillAppear(true)
        }
    }
}

extension RequestPhysicalCardFlowCoordinator: ChosenDeliveryAddressSuccessCoordinatorDelegate {
    public func didCloseChosenDeliveryAddressSuccess() {
        didFinishFlow()
    }
}
