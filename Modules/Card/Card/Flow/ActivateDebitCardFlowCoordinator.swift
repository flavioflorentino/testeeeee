import UI
import UIKit

public final class ActivateDebitCardFlowCoordinator: Coordinating {
    private let navigationController: UINavigationController
    private let mainActivateDebitNavigationController = UINavigationController()
    public var childViewController: [UIViewController] = []
    public var viewController: UIViewController?
    
    public init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    public func start() {
        let controller = ActivateDebitCardFactory.make(self)
        mainActivateDebitNavigationController
            .presentationController?
            .delegate = navigationController.viewControllers.first as? UIAdaptivePresentationControllerDelegate
        mainActivateDebitNavigationController.pushViewController(controller, animated: true)
        navigationController.present(mainActivateDebitNavigationController, animated: true)
    }
    
    private func dismissFlow() {
        mainActivateDebitNavigationController.dismiss(animated: true)
        updateParentViewControllerAppearance()
    }
    
    private func updateParentViewControllerAppearance() {
        if let controller = navigationController.viewControllers.first {
            controller.viewWillAppear(true)
        }
    }
}

extension ActivateDebitCardFlowCoordinator: ActivateDebitCardCoordinatorDelegate {
    public func didConfirmActivateDebit(onSuccess: @escaping (String, Bool) -> Void, cancelHandler: @escaping () -> Void) {
        AuthManager.shared.authenticate(sucessHandler: onSuccess, cancelHandler: cancelHandler)
    }
    public func didSuccessActivateDebit() {
        let controller = FeedbackStateFactory.make(type: .debitAtivated, delegate: self)
        mainActivateDebitNavigationController.pushViewController(controller, animated: true)
    }
    public func didCloseActivateDebit() {
        dismissFlow()
    }
}

extension ActivateDebitCardFlowCoordinator: FeedbackStateDelegate {
    public func didConfirmFeedbackState(feedbackType: FeedbackStateType) {
        dismissFlow()
    }
}
