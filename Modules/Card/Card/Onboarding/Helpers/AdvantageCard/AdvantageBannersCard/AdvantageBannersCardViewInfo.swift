public struct AdvantageBannerCardViewInfo: AdvantageCardViewInfo {
    var title: String
    var description: String
    var bannersInfo: [AdvantageBannerViewInfo]
}
