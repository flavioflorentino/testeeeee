import UI
import UIKit

public final class AdvantageBannersCardView: UIView, AdvantageCardView {
    private enum Layout {
        static let fontNormal = UIFont.systemFont(ofSize: 16, weight: .light)
        static let fontHeavy = UIFont.systemFont(ofSize: 18, weight: .heavy)
    }
    
    private enum Size {
        static let horizontalMargin: CGFloat = 16.0
        static let marginFirstElementsY: CGFloat = 5.0
        static let marginSecondElementsY: CGFloat = 11.0
        static let titleLabelTopMargin: CGFloat = 32.0
        static let stackBottomMargin: CGFloat = -23.0
        static let titleLabelHeight: CGFloat = 40.0
        static let descriptionLabelHeight: CGFloat = 60.0
        static let cornerRadius: CGFloat = 16.0
        static let stackViewSpacing: CGFloat = 6.0
    }

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = .clear
        label.textColor = Palette.ppColorBranding300.color
        label.textAlignment = .center
        label.font = Layout.fontHeavy
        label.numberOfLines = 0
        return label
    }()

    private lazy var descriptionText: UITextView = {
        let text = UITextView()
        text.translatesAutoresizingMaskIntoConstraints = false
        text.backgroundColor = .clear
        text.font = Layout.fontNormal
        text.tintColor = Palette.ppColorGrayscale600.color
        text.isEditable = false
        text.textAlignment = .center
        text.isSelectable = true
        text.isScrollEnabled = false
        text.contentInset = .zero
        return text
    }()

    private lazy var bannersStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = Size.stackViewSpacing
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()

    override public init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setup() {
        configureViews()
        buildViewHierarchy()
        setupConstraints()
    }

    public func setupCard(info: AdvantageBannerCardViewInfo) {
        titleLabel.text = info.title
        descriptionText.text = info.description
        if info.bannersInfo.count == 2 {
            let firstBanner = createBanner(with: info.bannersInfo[0])
            let secondBanner = createBanner(with: info.bannersInfo[1])
            bannersStackView.addArrangedSubview(firstBanner)
            bannersStackView.addArrangedSubview(secondBanner)
        }
    }
    
    private func createBanner(with advantageInfo: AdvantageBannerViewInfo) -> AdvantageBannerView {
        let bannerView = AdvantageBannerView()
        bannerView.setupCard(info: advantageInfo)
        return bannerView
    }
}

extension AdvantageBannersCardView: ViewConfiguration {
    public func configureViews() {
        backgroundColor = Palette.ppColorGrayscale000.color
        layer.cornerRadius = Size.cornerRadius
    }

    public func buildViewHierarchy() {
        addSubview(titleLabel)
        addSubview(bannersStackView)
        addSubview(descriptionText)
    }

    public func setupConstraints() {
        NSLayoutConstraint.leadingTrailing(
            equalTo: self,
            for: [titleLabel, bannersStackView, descriptionText],
            constant: Size.horizontalMargin
        )

        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: Size.titleLabelTopMargin),
            titleLabel.heightAnchor.constraint(equalToConstant: Size.titleLabelHeight),

            descriptionText.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Size.marginFirstElementsY),
            descriptionText.heightAnchor.constraint(equalToConstant: Size.descriptionLabelHeight),

            bannersStackView.topAnchor.constraint(equalTo: descriptionText.bottomAnchor, constant: Size.marginSecondElementsY),
            bannersStackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: Size.stackBottomMargin)
        ])
    }
}
