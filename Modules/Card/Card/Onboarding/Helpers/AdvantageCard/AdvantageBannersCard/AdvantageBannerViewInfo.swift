public struct AdvantageBannerViewInfo {
    var percentText: String
    var title: String
    var description: String
}
