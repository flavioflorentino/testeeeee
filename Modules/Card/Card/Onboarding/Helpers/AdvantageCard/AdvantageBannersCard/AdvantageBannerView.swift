import UI
import UIKit

public final class AdvantageBannerView: UIView {
    private enum Layout {
        static let titleFont = UIFont.italicSystemFont(ofSize: 14)
        static let descriptionFont = UIFont.systemFont(ofSize: 12, weight: .light)
        static let percentFont = UIFont.systemFont(ofSize: 38, weight: .black)
    }
    private enum Size {
        static let cornerRadius: CGFloat = 6.0
        static let horizontalMargin: CGFloat = 15.0
        static let verticalMargin: CGFloat = 3.0
        static let percentLabelTopMargin: CGFloat = 15.0
        static let percentLabelHeight: CGFloat = 38.0
        static let titleLabelHeight: CGFloat = 13.0
        static let descriptionBottomMargin: CGFloat = -17.0
    }
    
    private lazy var percentLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = .clear
        label.textColor = Palette.ppColorBranding300.color
        label.textAlignment = .center
        label.font = Layout.percentFont
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = .clear
        label.textColor = Palette.white.color
        label.textAlignment = .center
        label.font = Layout.titleFont
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = .clear
        label.textColor = Palette.white.color
        label.textAlignment = .center
        label.font = Layout.descriptionFont
        label.numberOfLines = 0
        return label
    }()
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
     
    private func setup() {
        configureViews()
        buildViewHierarchy()
        setupConstraints()
    }
    
    public func setupCard(info: AdvantageBannerViewInfo) {
        percentLabel.text = info.percentText
        titleLabel.text = info.title
        descriptionLabel.text = info.description
    }
}

extension AdvantageBannerView: ViewConfiguration {
    public func configureViews() {
        backgroundColor = Palette.ppColorCardGray500.color
        layer.cornerRadius = Size.cornerRadius
    }
    
    public func buildViewHierarchy() {
        addSubview(percentLabel)
        addSubview(titleLabel)
        addSubview(descriptionLabel)
    }
    
    public func setupConstraints() {
        NSLayoutConstraint.leadingTrailing(
            equalTo: self,
            for: [percentLabel, titleLabel, descriptionLabel],
            constant: Size.horizontalMargin
        )
        
        NSLayoutConstraint.activate([
            percentLabel.topAnchor.constraint(equalTo: topAnchor, constant: Size.percentLabelTopMargin),
            percentLabel.heightAnchor.constraint(equalToConstant: Size.percentLabelHeight),
            
            titleLabel.topAnchor.constraint(equalTo: percentLabel.bottomAnchor, constant: Size.verticalMargin),
            titleLabel.heightAnchor.constraint(equalToConstant: Size.titleLabelHeight),
            
            descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Size.verticalMargin),
            descriptionLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: Size.descriptionBottomMargin)
        ])
    }
}
