import UIKit

public protocol AdvantageCardView: UIView {
    associatedtype CardInfo: AdvantageCardViewInfo
    
    func setupCard(info: CardInfo)
}
