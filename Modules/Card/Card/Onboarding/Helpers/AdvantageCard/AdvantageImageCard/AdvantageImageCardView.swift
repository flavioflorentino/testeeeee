import UI
import UIKit

final class AdvantageImageCardView: UIView, AdvantageCardView {
    private enum Layout {
        static let fontNormal = UIFont.systemFont(ofSize: 16, weight: .light)
        static let fontHeavy = UIFont.systemFont(ofSize: 18, weight: .heavy)
    }
    
    private enum Size {
        static let horizontalMargin: CGFloat = 16.0
        static let verticalMargin: CGFloat = 8.0
        static let titleLabelTopMargin: CGFloat = 14.0
        static let titleLabelHeight: CGFloat = 43.0
        static let thumbImageHeight: CGFloat = 130.0
        static let descriptionLabelHeight: CGFloat = 90.0
        static let cornerRadius: CGFloat = 16.0
    }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = .clear
        label.textColor = Palette.ppColorBranding300.color
        label.textAlignment = .center
        label.font = Layout.fontHeavy
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var thumbImage: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.backgroundColor = .clear
        image.contentMode = .center
        return image
    }()
    
    private lazy var descriptionText: UITextView = {
        let text = UITextView()
        text.translatesAutoresizingMaskIntoConstraints = false
        text.backgroundColor = .clear
        text.font = Layout.fontNormal
        text.tintColor = Palette.ppColorGrayscale600.color
        text.isEditable = false
        text.isSelectable = true
        text.isScrollEnabled = false
        text.linkTextAttributes = [NSAttributedString.Key.foregroundColor: Palette.ppColorPositive300.color]
        text.contentInset = .zero
        return text
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
     
    private func setup() {
        configureViews()
        buildViewHierarchy()
        setupConstraints()
    }
    
    func setupCard(info: AdvantageImageCardViewInfo) {
        titleLabel.text = info.title
        thumbImage.image = info.image
        descriptionText.attributedText = info.description
    }
}

extension AdvantageImageCardView: ViewConfiguration {
    public func configureViews() {
        backgroundColor = Palette.ppColorGrayscale000.color
        layer.cornerRadius = Size.cornerRadius
    }
    
    public func buildViewHierarchy() {
        addSubview(titleLabel)
        addSubview(thumbImage)
        addSubview(descriptionText)
    }
    
    public func setupConstraints() {
        NSLayoutConstraint.leadingTrailing(
            equalTo: self,
            for: [titleLabel, thumbImage, descriptionText],
            constant: Size.horizontalMargin
        )
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: Size.titleLabelTopMargin),
            titleLabel.heightAnchor.constraint(equalToConstant: Size.titleLabelHeight),
            
            thumbImage.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Size.verticalMargin),
            thumbImage.heightAnchor.constraint(equalToConstant: Size.thumbImageHeight),
            
            descriptionText.topAnchor.constraint(equalTo: thumbImage.bottomAnchor, constant: Size.verticalMargin),
            descriptionText.heightAnchor.constraint(equalToConstant: Size.descriptionLabelHeight)
        ])
    }
}
