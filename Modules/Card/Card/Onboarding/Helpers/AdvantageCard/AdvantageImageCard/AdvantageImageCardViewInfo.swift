import UIKit

public struct AdvantageImageCardViewInfo: AdvantageCardViewInfo {
    var title: String
    var image: UIImage
    var description: NSAttributedString
    var link: Bool
}
