import Foundation
public struct OnboardingScrollViewContentInfo {
    var title: NSAttributedString
    var description: NSAttributedString
    var advantages: [AdvantageCardViewInfo]
    var helpAction: NSAttributedString
    var rulesAction: NSAttributedString
}
