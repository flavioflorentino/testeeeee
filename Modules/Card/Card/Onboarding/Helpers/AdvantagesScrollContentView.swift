import FeatureFlag
import Foundation
import UI
import UIKit

public class AdvantagesScrollContentView: UIView {
    private enum Layout {
        static let advantageWidth = UIScreen.main.bounds.width - (UIScreen.main.bounds.width * 0.12)
        static let margin: CGFloat = 16.0
    }
    
    private lazy var advantageAnnuityView: AdvantageImageCardView = {
        let view = AdvantageImageCardView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var advantagePayView: AdvantageImageCardView = {
        let view = AdvantageImageCardView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var advantageMasterCardSurpreendaView: AdvantageImageCardView = {
        let view = AdvantageImageCardView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        configureViews()
        buildViewHierarchy()
        setupConstraints()
    }
    
    public func setupAdvantagesCards(cards: [AdvantageCardViewInfo]) {
        if cards.count >= 1, let cardViewInfo = cards[0] as? AdvantageImageCardViewInfo {
            advantageAnnuityView.setupCard(info: cardViewInfo)
        }
        if cards.count >= 2, let cardViewInfo = cards[1] as? AdvantageImageCardViewInfo {
            advantagePayView.setupCard(info: cardViewInfo)
        }
        if cards.count >= 3, let cardViewInfo = cards[2] as? AdvantageImageCardViewInfo {
            advantageMasterCardSurpreendaView.setupCard(info: cardViewInfo)
        }
    }
}

extension AdvantagesScrollContentView: ViewConfiguration {
    public func configureViews() {
    }
    
    public func buildViewHierarchy() {
        addSubview(advantageAnnuityView)
        addSubview(advantagePayView)
        addSubview(advantageMasterCardSurpreendaView)
    }
    
    public func setupConstraints() {
        NSLayoutConstraint.activate([
            advantageAnnuityView.leadingAnchor.constraint(equalTo: leadingAnchor),
            advantageAnnuityView.topAnchor.constraint(equalTo: topAnchor, constant: Layout.margin),
            advantageAnnuityView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Layout.margin),
            advantageAnnuityView.widthAnchor.constraint(equalToConstant: Layout.advantageWidth),

            advantagePayView.topAnchor.constraint(equalTo: topAnchor, constant: Layout.margin),
            advantagePayView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Layout.margin),
            advantagePayView.leadingAnchor.constraint(equalTo: advantageAnnuityView.trailingAnchor, constant: Layout.margin),
            advantagePayView.widthAnchor.constraint(equalToConstant: Layout.advantageWidth),

            advantageMasterCardSurpreendaView.topAnchor.constraint(equalTo: topAnchor, constant: Layout.margin),
            advantageMasterCardSurpreendaView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Layout.margin),
            advantageMasterCardSurpreendaView.leadingAnchor.constraint(equalTo: advantagePayView.trailingAnchor, constant: Layout.margin),
            advantageMasterCardSurpreendaView.widthAnchor.constraint(equalToConstant: Layout.advantageWidth),
            advantageAnnuityView.leadingAnchor.constraint(equalTo: leadingAnchor)
        ])
    }
}
