enum OnboardingABTestingState {
    case a
    case b
}

protocol OnboardingABTestingData {
    var confirmButtonText: String { get set }
}
