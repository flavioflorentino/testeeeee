enum OnboardingABTestingDataFactory {
    static func make(with type: OnboardingABTestingState) -> OnboardingABTestingData {
        switch type {
        case .a:
            return OnboardingAData()
        case .b:
            return OnboardingBData()
        }
    }
}
