import AnalyticsModule
import FeatureFlag
import Foundation

protocol UpgradeDebitSuccessInteracting: AnyObject {
    func showInitialSetup()
    func close()
    func confirm()
    func openFAQ()
}

final class UpgradeDebitSuccessInteractor {
    typealias Dependencies = HasAnalytics & HasFeatureManager
    private let dependencies: Dependencies
    
    private let presenter: UpgradeDebitSuccessPresenting
    
    init(presenter: UpgradeDebitSuccessPresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - UpgradeDebitSuccessInteracting
extension UpgradeDebitSuccessInteractor: UpgradeDebitSuccessInteracting {
    public func showInitialSetup() {
        presenter.showInitialSetup()
    }
    
    public func close() {
        presenter.didNextStep(action: .close)
    }
    
    public func confirm() {
        presenter.didNextStep(action: .confirm)
        dependencies.analytics.log(CardUpgradeDebitEvent.cardActivated(action: .openWallet))
    }
    
    public func openFAQ() {
        dependencies.analytics.log(CardUpgradeDebitEvent.cardActivated(action: .help))
        presenter.openZendesk()
    }
}
