import Foundation
import UIKit

public enum UpgradeDebitSuccessCoordinatorOutput {
    case openWallet
    case close
}

public typealias UpgradeDebitSuccessCompletion = (UpgradeDebitSuccessCoordinatorOutput) -> Void

public enum UpgradeDebitSuccessFactory {
    public static func make(coordinatorOutput: @escaping UpgradeDebitSuccessCompletion) -> UIViewController {
        let container = DependencyContainer()
        let coordinator: UpgradeDebitSuccessCoordinating = UpgradeDebitSuccessCoordinator(coordinadorOutput: coordinatorOutput)
        let presenter: UpgradeDebitSuccessPresenting = UpgradeDebitSuccessPresenter(coordinator: coordinator)
        let interactor = UpgradeDebitSuccessInteractor(presenter: presenter, dependencies: container)
        let viewController = UpgradeDebitSuccessViewController(interactor: interactor)
        
        coordinator.viewController = viewController
        presenter.viewController = viewController
        
        return viewController
    }
}
