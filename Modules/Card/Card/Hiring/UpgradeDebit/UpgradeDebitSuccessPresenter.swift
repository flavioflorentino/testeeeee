import Foundation
import UI

protocol UpgradeDebitSuccessPresenting: AnyObject {
    var viewController: UpgradeDebitSuccessDisplay? { get set }
    func didNextStep(action: UpgradeDebitSuccessAction)
    func showInitialSetup()
    func openZendesk()
}

final class UpgradeDebitSuccessPresenter {
    private let coordinator: UpgradeDebitSuccessCoordinating
    weak var viewController: UpgradeDebitSuccessDisplay?
    
    init(coordinator: UpgradeDebitSuccessCoordinating) {
        self.coordinator = coordinator
    }
    
    private let faqURL = "http://app.picpay.com/helpcenter?query=onde-posso-usar-o-meu-picpay-card"
    
    private func getHelpMessage() -> NSMutableAttributedString {
        let linked = Strings.UpgradeDebitSuccess.helpCenterLink
        let string = Strings.UpgradeDebitSuccess.helpCenter
        let attributed = createAttributedString(with: string, link: linked)
        return attributed
    }
    
    private func createAttributedString(with string: String, link: String) -> NSMutableAttributedString {
        let attributed = NSMutableAttributedString(string: string)
        attributed.font(text: string, font: Typography.bodySecondary().font())
        attributed.textBackgroundColor(text: string, color: .clear)
        attributed.textColor(text: string, color: Colors.grayscale600.color)
        attributed.paragraph(aligment: .center, lineSpace: 3.0)
        attributed.underline(text: link)
        attributed.link(text: link, link: faqURL)
        return attributed
    }
}

// MARK: - UpgradeDebitSuccessPresenting
extension UpgradeDebitSuccessPresenter: UpgradeDebitSuccessPresenting {
    func didNextStep(action: UpgradeDebitSuccessAction) {
        coordinator.perform(action: action)
    }
    
    func showInitialSetup() {
        viewController?.displayInitialSetup(helpMessage: getHelpMessage())
    }
    
    func openZendesk() {
        coordinator.perform(action: .openZendesk)
    }
}
