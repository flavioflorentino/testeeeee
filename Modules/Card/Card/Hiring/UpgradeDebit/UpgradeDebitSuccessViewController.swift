import UI
import UIKit

protocol UpgradeDebitSuccessDisplay: AnyObject {
    func displayInitialSetup(helpMessage: NSMutableAttributedString)
}

final class UpgradeDebitSuccessViewController: ViewController<UpgradeDebitSuccessInteracting, UIView> {
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .xLarge))
        label.text = Strings.UpgradeDebitSuccess.title
        label.textAlignment = .center
        return label
    }()
    
    private lazy var cardImage = UIImageView(image: Assets.cardTop.image)
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
        label.text = Strings.UpgradeDebitSuccess.description
        label.textAlignment = .center
        return label
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Strings.UpgradeDebitSuccess.button, for: .normal)
        button.addTarget(self, action: #selector(nextPressed), for: .touchUpInside)
        return button
    }()
    
    private lazy var helpLabel: UITextView = {
        let text = UITextView()
        text.font = Typography.bodyPrimary().font()
        text.textAlignment = .center
        text.textColor = Colors.grayscale700.color
        text.isScrollEnabled = false
        text.backgroundColor = .clear
        text.linkTextAttributes = [NSAttributedString.Key.foregroundColor: Colors.branding400.color]
        text.delegate = self
        return text
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.showInitialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBarAppearance()
    }
    
    private func updateNavigationBarAppearance() {
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.isTranslucent = true
        navigationItem.hidesBackButton = true
        addCloseButton()
    }
    
    private func addCloseButton() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: Assets.icoClose.image,
            style: .plain,
            target: self,
            action: #selector(closePressed)
        )
    }
    
    override func buildViewHierarchy() {
        view.addSubviews(cardImage, titleLabel, descriptionLabel, confirmButton, helpLabel)
    }
    
    override func setupConstraints() {
        cardImage.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base06)
            $0.centerX.equalToSuperview()
            $0.height.equalTo(cardImage.snp.width)
            $0.leading.equalToSuperview().offset(Spacing.base09)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(cardImage.snp.bottom).offset(Spacing.base03)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base05)
            $0.trailing.equalToSuperview().inset(Spacing.base05)
        }
        
        confirmButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(helpLabel.snp.top).inset(-Spacing.base02)
        }
        
        helpLabel.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).inset(Spacing.base02)
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    @objc
    private func nextPressed() {
        viewModel.confirm()
    }
    
    @objc
    private func closePressed() {
        viewModel.close()
    }
}

// MARK: UITextViewDelegate

extension UpgradeDebitSuccessViewController: UITextViewDelegate {
    public func textView(
        _ textView: UITextView,
        shouldInteractWith URL: URL,
        in characterRange: NSRange,
        interaction: UITextItemInteraction
    ) -> Bool {
        viewModel.openFAQ()
        return false
    }
}

// MARK: UpgradeDebitSuccessDisplay

extension UpgradeDebitSuccessViewController: UpgradeDebitSuccessDisplay {
    func displayInitialSetup(helpMessage: NSMutableAttributedString) {
        helpLabel.attributedText = helpMessage
    }
}
