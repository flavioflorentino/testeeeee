import CustomerSupport
import SafariServices
import UIKit

enum UpgradeDebitSuccessAction: Equatable {
    case confirm
    case close
    case openZendesk
}

protocol UpgradeDebitSuccessCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: UpgradeDebitSuccessAction)
}

final class UpgradeDebitSuccessCoordinator {
    private let coordinadorOutput: UpgradeDebitSuccessCompletion
    
    weak var viewController: UIViewController?
    
    init(coordinadorOutput: @escaping UpgradeDebitSuccessCompletion) {
        self.coordinadorOutput = coordinadorOutput
    }
}

// MARK: - UpgradeDebitSuccessCoordinating
extension UpgradeDebitSuccessCoordinator: UpgradeDebitSuccessCoordinating {
    func perform(action: UpgradeDebitSuccessAction) {
        switch action {
        case .confirm:
            coordinadorOutput(.openWallet)
        case .close:
            coordinadorOutput(.close)
        case .openZendesk:
            let option = HiringSuccessCustomer().option()
            let faq = FAQFactory.make(option: option)
            viewController?.navigationController?.pushViewController(faq, animated: true)
        }
    }
}
