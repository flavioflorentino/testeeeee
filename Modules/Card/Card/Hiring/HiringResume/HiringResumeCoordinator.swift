import SafariServices
import UIKit

enum HiringResumeAction {
    case creditActivated
    case creditWaitingActivation
    case close
}

protocol HiringResumeCoordinating: AnyObject {
    func perform(action: HiringResumeAction)
    func open(with url: URL)
    var viewController: UIViewController? { get set }
}

final class HiringResumeCoordinator: SafariWebViewPresentable {
    weak var delegate: HiringFlowCoordinating?
    weak var viewController: UIViewController?
}

// MARK: - HiringResumeCoordinating
extension HiringResumeCoordinator: HiringResumeCoordinating {
    func perform(action: HiringResumeAction) {
        switch action {
        case .creditActivated:
            delegate?.perform(action: .creditActivated)
        case .creditWaitingActivation:
            delegate?.perform(action: .creditWaitingActivation)
        case .close:
            delegate?.close()
        }
    }
    
    func open(with url: URL) {
        open(url: url)
    }
}
