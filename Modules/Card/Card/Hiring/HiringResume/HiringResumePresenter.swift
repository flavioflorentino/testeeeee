import Core
import FeatureFlag
import Foundation
import UI

protocol HiringResumePresenting: AnyObject {
    var viewController: HiringResumeDisplaying? { get set }
    func textForResume()
    func textForRules()
    func showError(error: ApiError)
    func openURL(with url: URL)
    func authenticate(pin: String)
    func didNextStep(action: HiringResumeAction)
    func dismissLoading()
}

final class HiringResumePresenter {
    private typealias Localizable = Strings.HiringResume
    
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    private let coordinator: HiringResumeCoordinating
    weak var viewController: HiringResumeDisplaying?

    private let bestPurchaseDay: Int
    private let cardForm: CardForm
    private let isUpgradeDebitToCredit: Bool
    private let multipleLinkContract: String
    private let debitLinkContract: String
    
    init(
        cardForm: CardForm,
        bestPurchaseDay: Int,
        isUpgradeDebitToCredit: Bool,
        coordinator: HiringResumeCoordinating,
        dependencies: Dependencies
    ) {
        self.cardForm = cardForm
        self.bestPurchaseDay = bestPurchaseDay
        self.isUpgradeDebitToCredit = isUpgradeDebitToCredit
        self.coordinator = coordinator
        self.dependencies = dependencies
        
        multipleLinkContract = dependencies.featureManager.text(.termsMultipleLink)
        debitLinkContract = dependencies.featureManager.text(.termsDebitLink)
    }
    
    private func titleAttributed(text: String) -> NSMutableAttributedString {
        let paragraph = NSMutableParagraphStyle()
        paragraph.paragraphSpacingBefore = 10
        let attributes: [NSAttributedString.Key: Any] = [
            .font: Typography.bodyPrimary().font(),
            .foregroundColor: Colors.grayscale700.color,
            .paragraphStyle: paragraph
        ]
        return NSMutableAttributedString(string: text, attributes: attributes)
    }
    
    private func contentAttributed(text: String) -> NSMutableAttributedString {
        let paragraph = NSMutableParagraphStyle()
        paragraph.paragraphSpacingBefore = 4
        let attributes: [NSAttributedString.Key: Any] = [
            .font: Typography.bodyPrimary(.highlight).font(),
            .foregroundColor: Colors.black.color,
            .paragraphStyle: paragraph
        ]
        return NSMutableAttributedString(string: text, attributes: attributes)
    }
    
    private func dueDayText() -> NSAttributedString {
        let content = titleAttributed(text: Localizable.dueDateYourInvoice)
        content.append(contentAttributed(text: "\(Localizable.everyDay(cardForm.dueDay ?? 0))\n"))
        return content
    }
    
    private func bestPurchaseDayText() -> NSAttributedString {
        let content = titleAttributed(text: Localizable.bestDayToBuyResume)
        content.append(contentAttributed(text: "\(Localizable.day(bestPurchaseDay))\n"))
        return content
    }
    
    private func selectedLimitText() -> NSAttributedString {
        guard let creditGrantedByBank = cardForm.creditGrantedByBank else {
            return NSAttributedString()
        }
        let selectedLimitFormatted: String = "\(creditGrantedByBank.toCurrencyString() ?? "")\n"
        let content = titleAttributed(text: Localizable.yourCreditLimit)
        content.append(contentAttributed(text: selectedLimitFormatted))
        return content
    }
    
    private func addressText() -> NSAttributedString {
        guard let homeAddress = cardForm.homeAddress else {
            return NSAttributedString()
        }
        let selectedLimitFormatted: String = HomeAddressFormatter.presentFormatedAdress(homeAddress: homeAddress)
        let content = titleAttributed(text: Localizable.titleAddress)
        content.append(contentAttributed(text: selectedLimitFormatted))
        return content
    }
    
    private func termsText() -> NSAttributedString {
        let content = NSMutableAttributedString()
        let attributes: [NSAttributedString.Key: Any] = [
            .font: Typography.bodySecondary().font(),
            .foregroundColor: Colors.grayscale700.color
        ]
        
        content.append(NSAttributedString(string: Localizable.isAgreeWithMembershipAgreement, attributes: attributes))

        content.font(text: Localizable.adhesionContract, font: Typography.bodySecondary(.highlight).font())
        content.link(text: Localizable.multiple, link: multipleLinkContract)
        content.link(text: Localizable.debit, link: debitLinkContract)

        return content
    }
}

// MARK: - HiringResumePresenting
extension HiringResumePresenter: HiringResumePresenting {
    func textForResume() {
        let resume = NSMutableAttributedString()
        resume.append(dueDayText())
        resume.append(bestPurchaseDayText())
        resume.append(selectedLimitText())
        if !isUpgradeDebitToCredit {
            resume.append(addressText())
        }
        viewController?.displayTextForResume(text: resume)
    }
    
    func textForRules() {
        viewController?.displayTextForTerms(text: termsText())
    }
    
    func showError(error: ApiError) {
        viewController?.displayError(error: error)
    }
    
    func authenticate(pin: String) {
        viewController?.authenticate(pin: pin)
    }
    
    func didNextStep(action: HiringResumeAction) {
        coordinator.perform(action: action)
    }
    
    func openURL(with url: URL) {
        coordinator.open(with: url)
    }
    
    func dismissLoading() {
        viewController?.dismissLoading()
    }
}
