import Core
import Foundation

protocol HiringResumeServicing {
    func finish(pin: String, _ completion: @escaping CompletionCreditFinish)
}

public typealias CompletionCreditFinish = (Result<CreditRegistrationFinish, ApiError>) -> Void

final class HiringResumeService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - HiringResumeServicing
extension HiringResumeService: HiringResumeServicing {
    func finish(pin: String, _ completion: @escaping CompletionCreditFinish) {
        let endpoint = CreditServiceEndpoint.creditRegistrationFinish(password: pin, originFlow: OriginFlow(origin: .credit))
        Api<CreditRegistrationFinish>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model)
                completion(mappedResult)
            }
        }
    }
}
