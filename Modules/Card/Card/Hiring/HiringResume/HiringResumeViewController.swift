import Core
import UI
import UIKit

protocol HiringResumeDisplaying: AnyObject {
    func displayTextForResume(text: NSMutableAttributedString)
    func displayTextForTerms(text: NSAttributedString)
    func displayError(error: ApiError)
    func authenticate(pin: String)
    func dismissLoading()
}

fileprivate extension HiringResumeViewController.Layout {
     enum Terms {
        static let borderWidth: CGFloat = 1
        static let cornerRadius: CGFloat = 5
     }
 }

final class HiringResumeViewController: ViewController<HiringResumeInteracting, HiringResumeRootView>, StatefulProviding {
    fileprivate enum Layout {}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.textForResume()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBarAppearance()
    }
    
    private func updateNavigationBarAppearance() {
        let backButton = UIBarButtonItem()
        backButton.title = String()
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: Assets.icoClose.image,
            style: .plain,
            target: self,
            action: #selector(didTapClose)
        )
    }
    
    override func configureViews() {
        title = Strings.HiringResume.title
        view.backgroundColor = Colors.backgroundPrimary.color
        
        rootView.delegate = self
    }
    
    @objc
    private func didTapClose() {
        interactor.didTapClose()
    }
}

// MARK: - HiringResumeRootViewDelegate
extension HiringResumeViewController: HiringResumeRootViewDelegate {
    func authenticate() {
        interactor.authenticate()
    }
    
    func trackAgreeTerms() {
        interactor.trackAgreeTerms()
    }
    
    func openURL(with url: URL) {
        interactor.trackViewTerms()
        interactor.openURL(with: url)
    }
}

// MARK: - HiringResumeDisplaying
extension HiringResumeViewController: HiringResumeDisplaying {
    func displayTextForResume(text: NSMutableAttributedString) {
        rootView.displayTextForResume(text: text)
    }
    
    func displayTextForTerms(text: NSAttributedString) {
        rootView.displayTextForTerms(text: text)
    }
    
    func displayError(error: ApiError) {
        endState()
        let popup = PopupViewController(
            title: Strings.Default.ops,
            description: error.localizedDescription,
            preferredType: .image(Assets.icoSadEmoji.image))
    
        let confirmAction = PopupAction(title: Strings.Default.iGotIt, style: .fill)
        popup.addAction(confirmAction)
        showPopup(popup)
    }
    
    func authenticate(pin: String) {
        beginState()
        interactor.confirm(pin: pin)
    }
    
    func dismissLoading() {
        endState()
    }
}
