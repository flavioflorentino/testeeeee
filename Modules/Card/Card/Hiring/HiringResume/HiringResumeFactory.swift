import UIKit

public enum HiringResumeFactory {
    public static func make(cardForm: CardForm,
                            bestAvailableDay: Int,
                            isUpgradeDebitToCredit: Bool,
                            delegate: HiringFlowCoordinating) -> UIViewController {
        let container = DependencyContainer()
        let service: HiringResumeServicing = HiringResumeService(dependencies: container)
        let coordinator = HiringResumeCoordinator()
        let presenter: HiringResumePresenting = HiringResumePresenter(cardForm: cardForm,
                                                                      bestPurchaseDay: bestAvailableDay,
                                                                      isUpgradeDebitToCredit: isUpgradeDebitToCredit,
                                                                      coordinator: coordinator,
                                                                      dependencies: container)
        let interactor = HiringResumeInteractor(service: service, presenter: presenter, dependencies: container)
        let viewController = HiringResumeViewController(interactor: interactor)

        coordinator.delegate = delegate
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
