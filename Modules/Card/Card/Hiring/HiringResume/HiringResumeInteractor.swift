import AnalyticsModule
import Foundation

protocol HiringResumeInteracting: AnyObject {
    func textForResume()
    func trackViewTerms()
    func trackAgreeTerms()
    func openURL(with url: URL)
    func authenticate()
    func confirm(pin: String)
    func didTapClose()
}

final class HiringResumeInteractor {
    typealias Dependencies = HasAnalytics & HasAuthManagerDependency & HasPPAuthSwiftContract
    private let dependencies: Dependencies
    
    private let service: HiringResumeServicing
    private let presenter: HiringResumePresenting
    
    private enum ErrorCode: String {
        case wrongPassword = "6003"
        case blockMinutes = "5006"
    }
    
    init(service: HiringResumeServicing, presenter: HiringResumePresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - HiringResumeInteracting
extension HiringResumeInteractor: HiringResumeInteracting {
    func textForResume() {
        presenter.textForResume()
        presenter.textForRules()
    }
    
    func trackViewTerms() {
        dependencies.analytics.log(CreditHiringEvent.resume(action: .viewTerms))
    }
    
    func trackAgreeTerms() {
        dependencies.analytics.log(CreditHiringEvent.resume(action: .request))
    }
    
    func authenticate() {
        dependencies.authManager.authenticate { [weak self] pin, isBiometric in
            let event = CreditHiringEvent.authentication(action: isBiometric ? .byometricPassword: .numeralPassword)
            self?.dependencies.analytics.log(event)
            self?.presenter.authenticate(pin: pin)
        } cancelHandler: {
            self.dependencies.analytics.log(CreditHiringEvent.authentication(action: .cancel))
        }
    }
    
    func openURL(with url: URL) {
        presenter.openURL(with: url)
    }
    
    func confirm(pin: String) {
        service.finish(pin: pin) { [weak self] result in
            switch result {
            case.success(let account):
                self?.dependencies.analytics.log(CreditHiringEvent.authenticated(state: .validaded))
                if account.registrationStatus == .activated {
                    self?.presenter.didNextStep(action: .creditActivated)
                } else {
                    self?.presenter.didNextStep(action: .creditWaitingActivation)
                }
            case .failure(let error):
                self?.dependencies.analytics.log(CreditHiringEvent.authenticated(state: .failed))

                guard let code = error.requestError?.code, ErrorCode(rawValue: code) != nil else {
                    self?.presenter.showError(error: error)
                    return
                }
                self?.dependencies.ppauth.handlePassword()
                self?.presenter.dismissLoading()
            }
        }
    }
    
    func didTapClose() {
        presenter.didNextStep(action: .close)
    }
}
