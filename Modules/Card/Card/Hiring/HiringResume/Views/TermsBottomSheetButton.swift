import UI
import UIKit

fileprivate extension TermsBottomSheetButton.Layout {
    enum Size {
        static let iconContainerCornerRadius: CGFloat = Sizing.base06 / 2
    }
}

final class TermsBottomSheetButton: UIButton, ViewConfiguration {
    fileprivate enum Layout { }
    
    private lazy var iconCircleContainer: UIView = {
        let view = UIView()
        view.border = Border.Style.light(color: .grayscale100(.default))
        view.applyRadiusCorners(radius: Layout.Size.iconContainerCornerRadius)
        return view
    }()
    
    private lazy var iconLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.icons(.medium).font()
        label.text = Iconography.fileExclamation.rawValue
        label.textColor = Colors.neutral800.color
        return label
    }()
    
    private lazy var termTitle: UILabel = {
        let label = UILabel()
        label.font = Typography.bodyPrimary().font()
        return label
    }()
    
    init(title: NSAttributedString) {
        super.init(frame: .zero)
        termTitle.attributedText = title
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        iconCircleContainer.addSubviews(iconLabel)
        addSubviews(iconCircleContainer, termTitle)
    }
    
    func setupConstraints() {
        iconLabel.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
        
        iconCircleContainer.snp.makeConstraints {
            $0.height.width.equalTo(Sizing.base06)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.centerY.equalToSuperview()
        }
        
        termTitle.snp.makeConstraints {
            $0.leading.equalTo(iconCircleContainer.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.centerY.equalToSuperview()
        }
    }
    
    func configureViews() {
        viewStyle(RoundedViewStyle(cornerRadius: .medium))
            .with(\.border, Border.Style.light(color: .grayscale100(.default)))
            .with(\.backgroundColor, Colors.backgroundPrimary.color)
        layer.shadowOffset = ShadowOffset.light.rawValue
        layer.shadowRadius = ShadowRadius.medium.rawValue
        layer.shadowColor = Colors.black.color.cgColor
        layer.shadowOpacity = 0.1
    }
}
