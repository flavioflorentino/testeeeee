import UI
import UIKit

protocol HiringResumeRootViewDelegate: AnyObject {
    func authenticate()
    func trackAgreeTerms()
    func openURL(with url: URL)
}

fileprivate extension HiringResumeRootView.Layout {
     enum Terms {
        static let borderWidth: CGFloat = 1
        static let cornerRadius: CGFloat = 5
     }
 }

class HiringResumeRootView: UIView, ViewConfiguration {
    fileprivate enum Layout {}

    weak var delegate: HiringResumeRootViewDelegate?
    
    private lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.backgroundColor = .clear
        scroll.alwaysBounceVertical = false
        return scroll
    }()
    
    private lazy var stackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [resumeTextView, importantLabel, termsContentview])
        stack.axis = .vertical
        stack.spacing = Spacing.base02
        return stack
    }()
    
    private lazy var resumeTextView: UITextView = {
        let text = UITextView()
        text.font = Typography.bodyPrimary().font()
        text.textColor = Colors.grayscale700.color
        text.isScrollEnabled = false
        text.isEditable = false
        text.backgroundColor = .clear
        return text
    }()
    
    private lazy var importantLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.text, Strings.HiringResume.importantProduct)
            .with(\.font, Typography.bodySecondary().font())
            .with(\.textColor, Colors.grayscale700.color)
        return label
    }()
    
    private lazy var checkmarkButton: UIButton = {
        let button = UIButton()
        button.setImage(Assets.icoGreyCircle.image, for: .normal)
        button.setImage(Assets.icoNewCheck.image, for: .selected)
        button.addTarget(self, action: #selector(agreeTerms), for: .touchUpInside)
        return button
    }()
    
    private lazy var termsContentview: UIView = {
        let view = UIView()
        view.layer.borderWidth = Layout.Terms.borderWidth
        view.layer.borderColor = Colors.grayscale100.lightColor.cgColor
        view.layer.cornerRadius = Layout.Terms.cornerRadius
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var termsTextView: UITextView = {
        let text = UITextView()
        text.font = Typography.bodyPrimary().font()
        text.textColor = Colors.grayscale700.color
        text.linkTextAttributes = [
            .foregroundColor: Colors.branding400.color,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        text.isScrollEnabled = false
        text.backgroundColor = .clear
        text.isEditable = false
        text.delegate = self
        return text
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Strings.HiringResume.confirmText, for: .normal)
        button.addTarget(self, action: #selector(confirm), for: .touchUpInside)
        button.isEnabled = false
        return button
    }()
    
    init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        termsContentview.addSubviews(checkmarkButton, termsTextView)
        scrollView.addSubview(stackView)
        addSubviews(scrollView, confirmButton)
    }
    
    func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.leading.trailing.top.equalToSuperview()
        }
        
        checkmarkButton.snp.makeConstraints {
            $0.height.width.equalTo(Spacing.base03)
            $0.trailing.equalTo(termsTextView.snp.leading).offset(-Spacing.base02)
            $0.leading.equalToSuperview().inset(Spacing.base02)
            $0.centerY.equalToSuperview()
        }
        
        termsTextView.snp.makeConstraints {
            $0.top.trailing.bottom.equalToSuperview().inset(Spacing.base01)
        }
        
        stackView.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(Spacing.base02)
            $0.centerX.equalToSuperview()
        }
        
        confirmButton.snp.makeConstraints {
            $0.top.equalTo(scrollView.snp.bottom)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(compatibleSafeArea.bottom).offset(-Spacing.base03)
        }
    }
    
    @objc
    private func agreeTerms() {
        delegate?.trackAgreeTerms()
        checkmarkButton.isSelected.toggle()
        confirmButton.isEnabled = checkmarkButton.isSelected
    }
    
    @objc
    private func confirm() {
        delegate?.authenticate()
    }
    
    func displayTextForResume(text: NSMutableAttributedString) {
        resumeTextView.attributedText = text
    }
    
    func displayTextForTerms(text: NSAttributedString) {
        termsTextView.attributedText = text
    }
}

extension HiringResumeRootView: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        delegate?.openURL(with: URL)
        return false
    }
}
