import UI
import UIKit

fileprivate extension TermsBottomSheetViewController.Layout {
    enum Size {
        static let viewHeight: CGFloat = 336
        static let indicatorWidth: CGFloat = 45
        static let indicatorHeight: CGFloat = 5
    }
}

final class TermsBottomSheetViewController: UIViewController, ViewConfiguration {
    fileprivate enum Layout { }
    
    private lazy var modalIndicator: UIView = {
        let view = UIView()
        view.viewStyle(RoundedViewStyle(cornerRadius: .light))
            .with(\.backgroundColor, Colors.grayscale300.color)
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textColor, Colors.grayscale950.color)
            .with(\.text, Strings.HiringResume.Terms.title)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .default))
            .with(\.textColor, Colors.grayscale700.color)
            .with(\.text, Strings.HiringResume.Terms.description)
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var debitTermButton: TermsBottomSheetButton = {
        let title = Strings.HiringResume.Terms.debit
        let attributedTitle = title.attributedStringWithFont(primary: Typography.bodyPrimary().font(),
                                                             secondary: Typography.bodyPrimary(.highlight).font())
        let button = TermsBottomSheetButton(title: attributedTitle)
        
        return button
    }()
    
    private lazy var creditTermButton: TermsBottomSheetButton = {
        let title = Strings.HiringResume.Terms.credit
        let attributedTitle = title.attributedStringWithFont(primary: Typography.bodyPrimary().font(),
                                                             secondary: Typography.bodyPrimary(.highlight).font())
        let button = TermsBottomSheetButton(title: attributedTitle)
        return button
    }()
    
    init() {
        super.init(nibName: nil, bundle: nil)
        preferredContentSize = CGSize(width: self.view.frame.width, height: Layout.Size.viewHeight)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildLayout()
    }
    
    func buildViewHierarchy() {
        view.addSubviews(modalIndicator,
                         titleLabel,
                         descriptionLabel,
                         debitTermButton,
                         creditTermButton)
    }
    
    func setupConstraints() {
        modalIndicator.snp.makeConstraints {
            $0.top.equalToSuperview().inset(Spacing.base03)
            $0.centerX.equalToSuperview()
            $0.width.equalTo(Layout.Size.indicatorWidth)
            $0.height.equalTo(Layout.Size.indicatorHeight)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(modalIndicator).inset(Spacing.base03)
            $0.centerX.equalToSuperview()
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        debitTermButton.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Sizing.base10)
        }
        
        creditTermButton.snp.makeConstraints {
            $0.top.equalTo(debitTermButton.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Sizing.base10)
        }
    }
    
    func configureViews() {
        view.viewStyle(RoundedViewStyle(cornerRadius: .strong))
            .with(\.backgroundColor, Colors.backgroundPrimary.color)
    }
}
