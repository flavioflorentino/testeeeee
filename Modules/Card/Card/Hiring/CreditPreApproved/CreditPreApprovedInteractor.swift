import Foundation
import AnalyticsModule

protocol CreditPreApprovedInteracting: AnyObject {
    func confirm()
    func loadData()
    func close()
}

final class CreditPreApprovedInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let service: CreditPreApprovedServicing
    private let presenter: CreditPreApprovedPresenting

    init(service: CreditPreApprovedServicing, presenter: CreditPreApprovedPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - CreditPreApprovedInteracting
extension CreditPreApprovedInteractor: CreditPreApprovedInteracting {
    func close() {
        presenter.presentNextStep(action: .close)
    }
    
    func loadData() {
        service.getCreditForm { [weak self] in
            switch $0 {
            case let .success(form):
                self?.presenter.presentScreenData(with: form)
                
            case let .failure(error):
                self?.presenter.presentError(error)
            }
        }
    }
    
    func confirm() {
        dependencies.analytics.log(CreditHiringEvent.preApproved)
        presenter.confirm()
    }
}
