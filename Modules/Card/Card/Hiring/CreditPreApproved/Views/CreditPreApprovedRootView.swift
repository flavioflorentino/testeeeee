import UIKit
import UI
import AssetsKit
import SkeletonView

protocol CreditPreApprovedRootViewDelegate: AnyObject {
    func didPressConfirm()
}

final class CreditPreApprovedRootView: UIView, ViewConfiguration {
    weak var delegate: CreditPreApprovedRootViewDelegate?
    
    private lazy var infoImageViewContainer = UIView()
    private lazy var infoImageView = InfoImageView()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
            .with(\.typography, .bodyPrimary(.highlight))
        button.layer.masksToBounds = true
        button.setTitle(Strings.CreditPreApproved.confirm, for: .normal)
        button.addTarget(self, action: #selector(confirm), for: .touchUpInside)
        button.isSkeletonable = true
        return button
    }()
    
    init() {
        super.init(frame: .zero)
        buildLayout()
        isSkeletonable = true
        backgroundColor = Colors.backgroundPrimary.color
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        infoImageViewContainer.addSubview(infoImageView)
        addSubviews(infoImageViewContainer, confirmButton)
    }
    
    func setupConstraints() {
        infoImageViewContainer.snp.makeConstraints {
            $0.top.equalTo(compatibleSafeArea.top).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.bottom.equalTo(confirmButton.snp.top).offset(-Spacing.base03)
        }
        
        infoImageView.snp.makeConstraints {
            $0.center.leading.trailing.equalToSuperview()
            $0.top.greaterThanOrEqualToSuperview()
            $0.bottom.lessThanOrEqualToSuperview()
        }
        
        confirmButton.snp.makeConstraints {
            $0.leading.equalTo(compatibleSafeArea.leading).inset(Spacing.base02)
            $0.trailing.equalTo(compatibleSafeArea.trailing).inset(Spacing.base02)
            $0.bottom.equalTo(compatibleSafeArea.bottom).offset(-Spacing.base03)
        }
    }
    
    func configureViews() {
        infoImageView.setupView(image: Resources.Illustrations.ilustra.image)
        infoImageView.setupSkeletonProperties(
            titleSkeletonProperties: .init(
                isSkeletonable: true,
                skeletonNumberOfLines: 2
            ),
            descriptionSkeletonProperties: .init(
                isSkeletonable: true,
                skeletonNumberOfLines: 5
            )
        )
        infoImageViewContainer.isSkeletonable = true
        infoImageView.isSkeletonable = true
    }
    
    func setupScreen(title: String, description: String) {
        infoImageView.setupView(
            image: Resources.Illustrations.ilustra.image,
            title: title,
            description: description
        )
    }
    
    @objc
    func confirm() {
        delegate?.didPressConfirm()
    }
}
