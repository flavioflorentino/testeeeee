import FeatureFlag
import Foundation
import Core
import UI

protocol CreditPreApprovedPresenting: AnyObject {
    var viewController: CreditPreApprovedDisplaying? { get set }
    func presentNextStep(action: CreditPreApprovedAction)
    func presentError(_ error: ApiError)
    func presentScreenData(with cardForm: CardForm)
    func confirm()
}

final class CreditPreApprovedPresenter {
    typealias Localizable = Strings.CreditPreApproved
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    private let coordinator: CreditPreApprovedCoordinating
    weak var viewController: CreditPreApprovedDisplaying?
    
    private var cardForm: CardForm?
    
    private var isCashbackEnable: Bool {
        dependencies.featureManager.isActive(.cashbackActivation)
    }
    
    init(coordinator: CreditPreApprovedCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
}

// MARK: - CreditPreApprovedPresenting
extension CreditPreApprovedPresenter: CreditPreApprovedPresenting {
    func presentNextStep(action: CreditPreApprovedAction) {
        coordinator.perform(action: action)
    }
    
    func presentError(_ error: ApiError) {
        let model = StatefulErrorViewModel(error)
        viewController?.displayError(model: model)
    }
    
    func presentScreenData(with cardForm: CardForm) {
        self.cardForm = cardForm
        let name = cardForm.name ?? ""
        let currencyString = NumberFormatter.toCurrencyString(with: cardForm.creditGrantedByBank ?? 0) ?? Localizable.noCredit
        let description = isCashbackEnable ? Localizable.Cashback.description(name): Localizable.description(name, currencyString)
        
        viewController?.displayScreenData(
            title: Strings.CreditPreApproved.title,
            description: description
        )
    }
    
    func confirm() {
        guard let cardForm = cardForm else {
            return
        }
        coordinator.perform(action: .confirm(cardForm: cardForm))
    }
}
