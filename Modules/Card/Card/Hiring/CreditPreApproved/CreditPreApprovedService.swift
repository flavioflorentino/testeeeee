import Foundation
import Core

protocol CreditPreApprovedServicing {
    func getCreditForm(_ completion: @escaping (Result<CardForm, ApiError>) -> Void)
}

final class CreditPreApprovedService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    private let jsonDecoder = JSONDecoder(.convertFromSnakeCase)

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - CreditPreApprovedServicing
extension CreditPreApprovedService: CreditPreApprovedServicing {
    func getCreditForm(_ completion: @escaping (Result<CardForm, ApiError>) -> Void) {
        let api = Api<CardForm>(endpoint: CreditServiceEndpoint.creditSetup)
        api.execute(jsonDecoder: self.jsonDecoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
