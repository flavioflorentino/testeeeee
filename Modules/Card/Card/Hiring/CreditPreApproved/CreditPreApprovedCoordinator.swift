import UIKit

enum CreditPreApprovedAction {
    case confirm(cardForm: CardForm)
    case close
}

protocol CreditPreApprovedCoordinating: AnyObject {
    func perform(action: CreditPreApprovedAction)
}

final class CreditPreApprovedCoordinator {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    weak var delegate: HiringFlowCoordinating?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - CreditPreApprovedCoordinating
extension CreditPreApprovedCoordinator: CreditPreApprovedCoordinating {
    func perform(action: CreditPreApprovedAction) {
        switch action {
        case let .confirm(cardForm):
            delegate?.perform(action: .dueDay(cardForm: cardForm))
            
        case .close:
            delegate?.close()
        }
    }
}
