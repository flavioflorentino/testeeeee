import AssetsKit
import UI
import UIKit
import SkeletonView

protocol CreditPreApprovedDisplaying: AnyObject {
    func displayScreenData(title: String, description: String)
    func displayError(model: StatefulViewModeling)
}

final class CreditPreApprovedViewController: ViewController<CreditPreApprovedInteracting, CreditPreApprovedRootView> {
    override var preferredStatusBarStyle: UIStatusBarStyle { .lightContent }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBarAppearance()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        rootView.layoutSkeletonIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        didTryAgain()
    }

    override func configureViews() {
        rootView.delegate = self
    }
    
    private func updateNavigationBarAppearance() {
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.backgroundColor = .clear
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
            navigationController?.navigationItem.largeTitleDisplayMode = .always
        }
        
        let barButtonItem = UIBarButtonItem(image: Resources.Icons.icoClose.image, style: .plain, target: self, action: #selector(close))
        barButtonItem.tintColor = Colors.white.color
        navigationItem.setRightBarButton(barButtonItem, animated: false)
    }
    
    @objc
    private func close() {
        interactor.close()
    }
}

// MARK: - CreditPreApprovedDisplaying
extension CreditPreApprovedViewController: CreditPreApprovedDisplaying {
    func displayLoading() {
        SkeletonAppearance.default.multilineHeight = 16
        SkeletonAppearance.default.multilineCornerRadius = 8
        rootView.showAnimatedGradientSkeleton()
    }
    
    func displayError(model: StatefulViewModeling) {
        endState(animated: true, model: model, completion: nil)
    }
    
    func displayScreenData(title: String, description: String) {
        rootView.hideSkeleton()
        rootView.setupScreen(title: title, description: description)
    }
}

extension CreditPreApprovedViewController: CreditPreApprovedRootViewDelegate, StatefulTransitionViewing {
    func didTryAgain() {
        endState()
        displayLoading()
        interactor.loadData()
    }
    
    func didPressConfirm() {
        interactor.confirm()
    }
}
