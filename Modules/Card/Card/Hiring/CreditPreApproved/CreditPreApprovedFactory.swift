import UIKit

enum CreditPreApprovedFactory {
    static func make(delegate: HiringFlowCoordinating) -> CreditPreApprovedViewController {
        let container = DependencyContainer()
        let service: CreditPreApprovedServicing = CreditPreApprovedService(dependencies: container)
        let coordinator = CreditPreApprovedCoordinator(dependencies: container)
        let presenter: CreditPreApprovedPresenting = CreditPreApprovedPresenter(coordinator: coordinator, dependencies: container)
        let interactor = CreditPreApprovedInteractor(service: service, presenter: presenter, dependencies: container)
        let viewController = CreditPreApprovedViewController(interactor: interactor)

        coordinator.delegate = delegate
        presenter.viewController = viewController

        return viewController
    }
}
