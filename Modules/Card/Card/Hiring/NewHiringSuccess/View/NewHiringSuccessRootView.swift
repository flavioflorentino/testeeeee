import UIKit
import UI

protocol NewHiringSuccessRootViewDelegate: AnyObject {
    func confirm()
}

private extension NewHiringSuccessRootView.Layout {
    enum Size {
        static let separatorHeight: CGFloat = 1
    }
}

final class NewHiringSuccessRootView: UIView, ViewConfiguration {
    fileprivate enum Layout { }
    
    weak var delegate: NewHiringSuccessRootViewDelegate?
    
    private lazy var infoImageview = InfoImageView()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Strings.HiringSuccess.button, for: .normal)
        button.addTarget(self, action: #selector(nextPressed), for: .touchUpInside)
        return button
    }()
    
    private lazy var cashbackLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
        label.textColor = Colors.grayscale600.color
        label.text = Strings.NewHiringSuccess.Cashback.secondDescription
        label.textAlignment = .center
        return label
    }()
    
    private lazy var separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale100.color
        return view
    }()
    
    private lazy var cashbackContainerView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            separatorView,
            cashbackLabel
        ])
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = Spacing.base02
        stackView.alignment = .center
        return stackView
    }()
    
    init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal func buildViewHierarchy() {
        addSubviews(infoImageview, cashbackContainerView, confirmButton)
    }
    
    internal func setupConstraints() {
        infoImageview.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base07)
            $0.centerX.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
        }

        separatorView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(Layout.Size.separatorHeight)
        }
        
        cashbackContainerView.snp.makeConstraints {
            $0.top.greaterThanOrEqualTo(infoImageview.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        confirmButton.snp.makeConstraints {
            $0.top.equalTo(cashbackContainerView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.bottom.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(compatibleSafeArea.bottom).inset(Spacing.base02)
        }
    }
    
    internal func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
    
    func displayScreen(description: String, buttonTittle: String, isCashbackEnable: Bool) {
        confirmButton.setTitle(buttonTittle, for: .normal)
        cashbackContainerView.isHidden = !isCashbackEnable
        
        infoImageview.setupView(image: Assets.cardTop.image,
                                title: Strings.HiringSuccess.title,
                                description: description,
                                normalColor: Colors.grayscale700.color,
                                highlightColor: Colors.grayscale700.color)
    }
    
    @objc
    private func nextPressed() {
        delegate?.confirm()
    }
}
