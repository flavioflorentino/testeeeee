import AnalyticsModule

enum HiringCardEvent: AnalyticsKeyProtocol {
    case didTapConfirm(action: HiringCashbackAction)
    case didTapClose

    private var name: String {
        "PicPay Card -  Request - Card Activated Message"
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .didTapConfirm(action):
            return ["action": action.rawValue]
        case .didTapClose:
            return ["action": "act-close"]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase, .appsFlyer]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}

extension HiringCardEvent {
    enum HiringCashbackAction: String {
        case cashbackConditions = "act-view-cashback-conditions"
        case close = "act-view-close"
    }
}
