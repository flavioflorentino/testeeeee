import Foundation
import UI

protocol NewHiringSuccessPresenting: AnyObject {
    var viewController: NewHiringSuccessDisplaying? { get set }
    func presentInitialSetup(isCashbackEnable: Bool)
    func didNextStep(action: NewHiringSuccessAction)
}

final class NewHiringSuccessPresenter {
    typealias Localizable = Strings.NewHiringSuccess
    
    private let coordinator: NewHiringSuccessCoordinating
    weak var viewController: NewHiringSuccessDisplaying?

    init(coordinator: NewHiringSuccessCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - NewHiringSuccessPresenting
extension NewHiringSuccessPresenter: NewHiringSuccessPresenting {
    func presentInitialSetup(isCashbackEnable: Bool) {
        let description = isCashbackEnable ? Localizable.Cashback.description: Localizable.description
        let buttonTittle = isCashbackEnable ? Localizable.Cashback.button: Localizable.button

        viewController?.displayScreen(description: description,
                                      buttonTittle: buttonTittle,
                                      isCashbackEnable: isCashbackEnable)
    }
    
    func didNextStep(action: NewHiringSuccessAction) {
        coordinator.perform(action: action)
    }
}
