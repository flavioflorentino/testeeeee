import UIKit

enum NewHiringSuccessAction {
    case close
    case cashbackOnboarding
}

protocol NewHiringSuccessCoordinating: AnyObject {
    var delegate: HiringFlowCoordinating? { get set }
    func perform(action: NewHiringSuccessAction)
}

final class NewHiringSuccessCoordinator {
    weak var delegate: HiringFlowCoordinating?
}

// MARK: - NewHiringSuccessCoordinating
extension NewHiringSuccessCoordinator: NewHiringSuccessCoordinating {
    func perform(action: NewHiringSuccessAction) {
        switch action {
        case .close:
            delegate?.close()
        case .cashbackOnboarding:
            delegate?.perform(action: .cashback)
        }
    }
}
