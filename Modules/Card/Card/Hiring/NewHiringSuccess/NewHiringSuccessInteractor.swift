import AnalyticsModule
import FeatureFlag
import Foundation

protocol NewHiringSuccessInteracting: AnyObject {
    func showInitialSetup()
    func close()
    func confirm()
}

final class NewHiringSuccessInteractor {
    typealias Dependencies = HasAnalytics & HasFeatureManager
    private let dependencies: Dependencies

    private let presenter: NewHiringSuccessPresenting
    private var isCashbackEnable: Bool {
        dependencies.featureManager.isActive(.cashbackActivation)
    }

    init(presenter: NewHiringSuccessPresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - NewHiringSuccessInteracting
extension NewHiringSuccessInteractor: NewHiringSuccessInteracting {
    func showInitialSetup() {
        presenter.presentInitialSetup(isCashbackEnable: isCashbackEnable)
    }
    
    func close() {
        dependencies.analytics.log(HiringCardEvent.didTapClose)
        presenter.didNextStep(action: .close)
    }
    
    func confirm() {
        guard isCashbackEnable else {
            dependencies.analytics.log(HiringCardEvent.didTapConfirm(action: .close))
            presenter.didNextStep(action: .close)
            return
        }
        dependencies.analytics.log(HiringCardEvent.didTapConfirm(action: .cashbackConditions))
        presenter.didNextStep(action: .cashbackOnboarding)
    }
}
