import UI
import UIKit

protocol NewHiringSuccessDisplaying: AnyObject {
    func displayScreen(description: String, buttonTittle: String, isCashbackEnable: Bool)
}

final class NewHiringSuccessViewController: ViewController<NewHiringSuccessInteracting, NewHiringSuccessRootView> {
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.showInitialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBarAppearance()
    }
    
    private func updateNavigationBarAppearance() {
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.isTranslucent = true
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }
        addCloseButton()
    }
    
    private func addCloseButton() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: Assets.icoClose.image,
            style: .plain,
            target: self,
            action: #selector(closePressed)
        )
        navigationItem.hidesBackButton = true
    }
    
    override func configureViews() {
        rootView.delegate = self
    }
    
    @objc
    private func closePressed() {
        interactor.close()
    }
}

// MARK: - NewHiringSuccessRootViewDelegate
extension NewHiringSuccessViewController: NewHiringSuccessRootViewDelegate {
    func confirm() {
        interactor.confirm()
    }
}

// MARK: - NewHiringSuccessDisplaying
extension NewHiringSuccessViewController: NewHiringSuccessDisplaying {
    func displayScreen(description: String, buttonTittle: String, isCashbackEnable: Bool) {
        rootView.displayScreen(description: description,
                               buttonTittle: buttonTittle,
                               isCashbackEnable: isCashbackEnable)
    }
}
