import UIKit

enum NewHiringSuccessFactory {
    static func make(delegate: HiringFlowCoordinating) -> NewHiringSuccessViewController {
        let container = DependencyContainer()
        let coordinator: NewHiringSuccessCoordinating = NewHiringSuccessCoordinator()
        let presenter: NewHiringSuccessPresenting = NewHiringSuccessPresenter(coordinator: coordinator)
        let interactor = NewHiringSuccessInteractor(presenter: presenter, dependencies: container)
        let viewController = NewHiringSuccessViewController(interactor: interactor)

        coordinator.delegate = delegate
        presenter.viewController = viewController

        return viewController
    }
}
