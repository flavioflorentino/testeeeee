import CustomerSupport
import Foundation

struct HiringSuccessCustomer: CustomerOptions {
    let development: FAQOptions = .home
    let production: FAQOptions = .article(id: "360044214651")
    let path: PathFAQ = .activationSuccess
}
