import Foundation

struct DueDayResponse: Decodable {
    let currentDueDay: Int?
    let validDueDays: [DueDay]
    
    struct DueDay: Decodable {
        let dueDay: Int
        let bestPurchaseDay: Int?
    }
}
