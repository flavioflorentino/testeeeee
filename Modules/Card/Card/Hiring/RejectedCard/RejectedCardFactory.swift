import UIKit

public enum RejectedCardFactory {
    public static func make() -> UIViewController {
        let container = DependencyContainer()
        let coordinator: RejectedCardCoordinating = RejectedCardCoordinator()
        let presenter: RejectedCardPresenting = RejectedCardPresenter(coordinator: coordinator)
        let interactor = RejectedCardInteractor(presenter: presenter, dependencies: container)
        let viewController = RejectedCardViewController(interactor: interactor)

        coordinator.viewController = viewController

        return viewController
    }
}
