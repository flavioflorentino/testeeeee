import UIKit
import CustomerSupport

enum RejectedCardAction {
    case close
}

protocol RejectedCardCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: RejectedCardAction)
}

final class RejectedCardCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - RejectedCardCoordinating
extension RejectedCardCoordinator: RejectedCardCoordinating, SafariWebViewPresentable {
    func perform(action: RejectedCardAction) {
        switch action {
        case .close:
            viewController?.dismiss(animated: true, completion: nil)
        }
    }
}
