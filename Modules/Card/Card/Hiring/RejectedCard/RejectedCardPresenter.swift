import Foundation
import UI

protocol RejectedCardPresenting: AnyObject {
    func presentNextStep(action: RejectedCardAction)
}

final class RejectedCardPresenter {
    private let coordinator: RejectedCardCoordinating

    init(coordinator: RejectedCardCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - RejectedCardPresenting
extension RejectedCardPresenter: RejectedCardPresenting {
    func presentNextStep(action: RejectedCardAction) {
        coordinator.perform(action: action)
    }
}
