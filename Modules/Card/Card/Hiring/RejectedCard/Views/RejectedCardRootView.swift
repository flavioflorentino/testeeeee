import UIKit
import UI

protocol RejectedCardRootViewDelegate: AnyObject {
    func didPressConfirm()
}

final class RejectedCardRootView: UIView, ViewConfiguration {
    weak var delegate: RejectedCardRootViewDelegate?
    
    private lazy var infoImageContainer = UIView()
    private lazy var infoImage = InfoImageView()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Strings.RejectedCard.confirmButtonTitle, for: .normal)
        button.addTarget(self, action: #selector(confirm), for: .touchUpInside)
        return button
    }()
    
    init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        infoImageContainer.addSubview(infoImage)
        addSubviews(infoImageContainer, confirmButton)
    }
    
    func setupConstraints() {
        infoImageContainer.snp.makeConstraints {
            $0.top.equalTo(compatibleSafeArea.top)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(confirmButton.snp.top)
        }
        
        infoImage.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        confirmButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.bottom.equalTo(compatibleSafeArea.bottom).offset(-Spacing.base03)
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
        infoImage.setupView(image: Assets.icoRejectedCard.image, title: Strings.RejectedCard.InfoImage.title, description: Strings.RejectedCard.InfoImage.description)
    }
    
    @objc
    private func confirm() {
        delegate?.didPressConfirm()
    }
}
