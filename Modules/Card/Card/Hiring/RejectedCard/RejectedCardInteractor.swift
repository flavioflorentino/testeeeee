import Foundation
import AnalyticsModule

protocol RejectedCardInteracting: AnyObject {
    func confirm()
}

final class RejectedCardInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let presenter: RejectedCardPresenting

    init(presenter: RejectedCardPresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - RejectedCardInteracting
extension RejectedCardInteractor: RejectedCardInteracting {
    func confirm() {
        dependencies.analytics.log(RejectedCardAnalyticEvent())
        presenter.presentNextStep(action: .close)
    }
}
