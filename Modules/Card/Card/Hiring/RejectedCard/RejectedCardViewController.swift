import UI
import UIKit

final class RejectedCardViewController: ViewController<RejectedCardInteracting, RejectedCardRootView> {
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func configureViews() {
        rootView.delegate = self
    }
}

extension RejectedCardViewController: RejectedCardRootViewDelegate {
    func didPressConfirm() {
        interactor.confirm()
    }
}
