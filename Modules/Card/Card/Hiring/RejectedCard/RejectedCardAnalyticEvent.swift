import Foundation
import AnalyticsModule

struct RejectedCardAnalyticEvent: AnalyticsKeyProtocol {
    private var name = "PicPay Card - Card Denied - Message"
    
    private var properties = ["action": "action-denied-message-close"]
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
