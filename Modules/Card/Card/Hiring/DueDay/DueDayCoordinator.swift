import UIKit

public enum DueDayAction: Equatable {
    case confirm(cardForm: CardForm? = nil, bestPurchaseDay: Int? = nil)
    
    public static func == (lhs: Self, rhs: Self) -> Bool {
        switch (lhs, rhs) {
        case let (.confirm(cardFormLhs, purchaseDayLhs), .confirm(cardForm, purchaseDay)):
            return cardFormLhs == cardForm && purchaseDayLhs == purchaseDay
        }
    }
}

protocol DueDayCoordinating: AnyObject {
    func perform(action: DueDayAction)
    var delegate: DueDayCoordinatingDelegate? { get set }
}

public protocol DueDayCoordinatingDelegate: AnyObject {
    func didConfirmDueDay(cardForm: CardForm?, bestPurchaseDay: Int?)
}

public final class DueDayCoordinator {
    weak var delegate: DueDayCoordinatingDelegate?
}

// MARK: - DueDayCoordinating
extension DueDayCoordinator: DueDayCoordinating {
    func perform(action: DueDayAction) {
        switch action {
        case let .confirm(cardForm, bestPurchaseDay):
            delegate?.didConfirmDueDay(cardForm: cardForm, bestPurchaseDay: bestPurchaseDay)
        }
    }
}
