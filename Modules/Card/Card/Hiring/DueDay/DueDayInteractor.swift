import Foundation
import AnalyticsModule

protocol DueDayInteracting: AnyObject {
    func loadData()
    func confirm()
    func selectRow(_ row: Int)
}

final class DueDayInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    struct ControllerData: Equatable {
        let selectedRow: Int?
        let availableDays: [Int]
        let bestPurchaseDay: Int?
    }

    private let service: DueDayServicing
    private let presenter: DueDayPresenting
    private var cardForm: CardForm?
    var availableDays = [DueDayResponse.DueDay]()
    
    var currentDueDay: Int? {
        didSet {
           presenter.presentControllerData(buildControllerData())
        }
    }

    init(service: DueDayServicing, presenter: DueDayPresenting, dependencies: Dependencies, cardForm: CardForm? = nil) {
        self.cardForm = cardForm
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
    
    private func buildControllerData() -> ControllerData {
        let currentDayRow = availableDays.enumerated().first(where: { $0.element.dueDay == currentDueDay })?.offset
        let bestPurchaseDay = availableDays.first(where: { $0.dueDay == currentDueDay })?.bestPurchaseDay
        return .init(
            selectedRow: currentDayRow,
            availableDays: availableDays.map { $0.dueDay },
            bestPurchaseDay: bestPurchaseDay
        )
    }
}

// MARK: - DueDayInteracting
extension DueDayInteractor: DueDayInteracting {
    func selectRow(_ row: Int) {
        dependencies.analytics.log(CreditHiringEvent.dueDay(action: .selectDay))
        currentDueDay = availableDays[row].dueDay
    }
    
    func confirm() {
        dependencies.analytics.log(CreditHiringEvent.dueDay(action: .saveDay))
        service.saveDueDays(dueDay: currentDueDay ?? 0) {
            switch $0 {
            case .success:
                self.cardForm?.dueDay = self.currentDueDay
                self.presenter.presentNextStep(action: .confirm(cardForm: self.cardForm, bestPurchaseDay: self.buildControllerData().bestPurchaseDay))
                
            case let .failure(error):
                self.presenter.presentError(error)
            }
        }
    }
    
    func loadData() {
        service.getDueDays {
            switch $0 {
            case let .success(dueDay):
                self.currentDueDay = dueDay.currentDueDay
                self.availableDays = dueDay.validDueDays
                self.presenter.presentControllerData(self.buildControllerData())
                
            case let .failure(error):
                self.presenter.presentError(error)
            }
        }
    }
}
