import Foundation
import Core
import UI

protocol DueDayPresenting: AnyObject {
    var viewController: DueDayDisplaying? { get set }
    
    func presentError(_ error: ApiError)
    func presentControllerData(_ controllerData: DueDayInteractor.ControllerData)
    func presentNextStep(action: DueDayAction)
}

final class DueDayPresenter {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let coordinator: DueDayCoordinating
    weak var viewController: DueDayDisplaying?

    init(coordinator: DueDayCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
    
    private func buildBestPurchaseDayText(bestPurchaseDay: Int?) -> NSAttributedString? {
        guard let bestPurchaseDay = bestPurchaseDay else {
            return nil
        }
        
        return Strings.DueDay.Header.bestPurchaseDay(bestPurchaseDay).attributedStringWith(
            normalFont: Typography.bodyPrimary().font(),
            highlightFont: Typography.bodyPrimary().font(),
            normalColor: Colors.grayscale800.color,
            highlightColor: Colors.branding400.color,
            textAlignment: .left,
            underline: false
        )
    }
}

// MARK: - DueDayPresenting
extension DueDayPresenter: DueDayPresenting {
    func presentNextStep(action: DueDayAction) {
        if case .confirm = action {
            viewController?.displayLoading(false)
        }
        coordinator.perform(action: action)
    }
    
    func presentError(_ error: ApiError) {
        viewController?.displayError(.init(error))
    }
    
    func presentControllerData(_ controllerData: DueDayInteractor.ControllerData) {
        viewController?.displayControllerData(
            .init(
                selectedRow: controllerData.selectedRow,
                availableDays: controllerData.availableDays,
                bestPurchaseDayAttrString: buildBestPurchaseDayText(bestPurchaseDay: controllerData.bestPurchaseDay)
            )
        )
    }
}
