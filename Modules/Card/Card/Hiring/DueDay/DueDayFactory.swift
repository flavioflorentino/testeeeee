import UIKit

public enum DueDayFactory {
    public static func make(delegate: DueDayCoordinatingDelegate, cardForm: CardForm? = nil) -> UIViewController {
        let container = DependencyContainer()
        let service: DueDayServicing = DueDayService(dependencies: container)
        let coordinator = DueDayCoordinator()
        let presenter: DueDayPresenting = DueDayPresenter(coordinator: coordinator, dependencies: container)
        let interactor = DueDayInteractor(service: service, presenter: presenter, dependencies: container, cardForm: cardForm)
        let viewController = DueDayViewController(interactor: interactor)

        coordinator.delegate = delegate
        presenter.viewController = viewController

        return viewController
    }
}
