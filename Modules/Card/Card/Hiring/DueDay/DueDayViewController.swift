import UI
import AssetsKit
import UIKit

protocol DueDayDisplaying: AnyObject {
    func displayError(_ statefulError: StatefulErrorViewModel)
    func displayControllerData(_ controllerData: DueDayRootViewData)
    func displayLoading(_ bool: Bool)
}

final class DueDayViewController: ViewController<DueDayInteracting, DueDayRootView> {
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.loadData()
        beginState()
    }

    override func configureViews() {
        rootView.delegate = self
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        title = Strings.DueDay.title
    }
}

extension DueDayViewController: StatefulTransitionViewing {
    func didTryAgain() {
        interactor.loadData()
    }
}

// MARK: - DueDayDisplaying
extension DueDayViewController: DueDayDisplaying {
    func displayLoading(_ bool: Bool) {
        bool ? beginState() : endState()
    }
    
    func displayControllerData(_ controllerData: DueDayRootViewData) {
        endState()
        rootView.setControllerData(controllerData)
    }
    
    func displayError(_ statefulError: StatefulErrorViewModel) {
        endState(animated: true, model: statefulError)
    }
}

extension DueDayViewController: DueDayRootViewDelegate {
    func didPressConfirm() {
        beginState()
        interactor.confirm()
    }
    
    func didSelectRow(row: Int) {
        interactor.selectRow(row)
    }
}

extension DueDayViewController: StyledNavigationDisplayable {}
