import UIKit
import UI
extension DueDayCollectionViewCell.Layout {
    enum Size {
        static let innerCircleOffset: CGFloat = -5
    }
}

final class DueDayCollectionViewCell: UICollectionViewCell, ViewConfiguration {
    fileprivate enum Layout {}
    
    private lazy var dayLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle()).with(\.textAlignment, .center)
        label.text = Strings.DueDay.day
        return label
    }()
    
    private lazy var numberLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .medium)).with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var innerCircle = UIView()
    
    private lazy var anchorView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        contentView.addSubviews(innerCircle, anchorView)
        anchorView.addSubviews(dayLabel, numberLabel)
    }
    
    func setupConstraints() {
        innerCircle.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.height.width.equalToSuperview().offset(Layout.Size.innerCircleOffset)
        }
        
        anchorView.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
        
        dayLabel.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
        }
        
        numberLabel.snp.makeConstraints {
            $0.bottom.leading.trailing.equalToSuperview()
            $0.top.equalTo(dayLabel.snp.bottom)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.layer.cornerRadius = contentView.frame.size.width / 2
        innerCircle.layer.cornerRadius = (contentView.frame.size.width + Layout.Size.innerCircleOffset) / 2
    }
    
    func configureViews() {
        contentView.layer.borderWidth = Border.light
    }
    
    func configure(with day: Int, selected: Bool) {
        numberLabel.text = "\(day)"
        setupColors(selected: selected)
    }
    
    private func setupColors(selected: Bool) {
        if selected {
            contentView.layer.borderColor = Colors.branding400.color.cgColor
            innerCircle.backgroundColor = Colors.branding400.color
            dayLabel.textColor = Colors.white.lightColor
            numberLabel.textColor = Colors.white.lightColor
        } else {
            contentView.layer.borderColor = Colors.grayscale300.color.cgColor
            innerCircle.backgroundColor = .clear
            dayLabel.textColor = Colors.grayscale300.lightColor
            numberLabel.textColor = Colors.grayscale300.lightColor
        }
    }
}
