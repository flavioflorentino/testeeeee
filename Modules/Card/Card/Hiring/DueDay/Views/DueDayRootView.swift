import UIKit
import UI

protocol DueDayRootViewDelegate: AnyObject {
    func didPressConfirm()
    func didSelectRow(row: Int)
}

struct DueDayRootViewData: Equatable {
    let selectedRow: Int?
    let availableDays: [Int]
    let bestPurchaseDayAttrString: NSAttributedString?
}

private extension DueDayRootView.Layout {
    enum Size {
        static let itemWidth: CGFloat = (UIScreen.main.bounds.width - Spacing.base04 - Spacing.base06) / 4
        static let itemSize: CGSize  = .init(width: itemWidth, height: itemWidth)
        static let minimumLineSpacing: CGFloat = 16
    }
}

final class DueDayRootView: UIView, ViewConfiguration {
    fileprivate enum Layout {}
    
    private enum Section {
        case main
    }
    
    weak var delegate: DueDayRootViewDelegate?
    
    private var controllerData = DueDayRootViewData(selectedRow: nil, availableDays: [], bestPurchaseDayAttrString: nil) {
        didSet {
            collectionViewDataSource.update(items: controllerData.availableDays, from: .main)
            updateBestPurchaseLabel()
        }
    }
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale800.color)
            .with(\.text, Strings.DueDay.Header.title)
        return label
    }()
    
    private lazy var bestPurchaseDayLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale800.color)
            .with(\.text, Strings.DueDay.Header.subtitlePlaceholder)
        return label
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
            .with(\.typography, .bodyPrimary(.highlight))
        button.setTitle(Strings.DueDay.continueButon, for: .normal)
        button.addTarget(self, action: #selector(confirm), for: .touchUpInside)
        button.isEnabled = false
        return button
    }()
    
    private lazy var daysCollectionView: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        flowLayout.itemSize = Layout.Size.itemSize
        flowLayout.minimumLineSpacing = Layout.Size.minimumLineSpacing
        flowLayout.minimumInteritemSpacing = Layout.Size.minimumLineSpacing
        let collection = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collection.showsVerticalScrollIndicator = false
        collection.delegate = self
        collection.backgroundColor = .clear
        collection.register(DueDayCollectionViewCell.self, forCellWithReuseIdentifier: DueDayCollectionViewCell.identifier)
        return collection
    }()
    
    private lazy var collectionViewDataSource: CollectionViewDataSource<Section, Int> = {
        let dataSource = CollectionViewDataSource<Section, Int>(view: daysCollectionView)
        dataSource.itemProvider = { view, indexPath, item -> UICollectionViewCell? in
            let cell = view.dequeueReusableCell(withReuseIdentifier: DueDayCollectionViewCell.identifier, for: indexPath) as? DueDayCollectionViewCell
            cell?.configure(with: item, selected: indexPath.row == self.controllerData.selectedRow)
            return cell
        }
        dataSource.add(section: .main)
        return dataSource
    }()
    
    init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal func buildViewHierarchy() {
        addSubviews(subtitleLabel, bestPurchaseDayLabel, confirmButton, daysCollectionView)
    }
    
    internal func setupConstraints() {
        subtitleLabel.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.top.equalTo(compatibleSafeArea.top)
        }
        
        bestPurchaseDayLabel.snp.makeConstraints {
            $0.top.equalTo(subtitleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        daysCollectionView.snp.makeConstraints {
            $0.top.equalTo(bestPurchaseDayLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(confirmButton.snp.top).offset(-Spacing.base03)
        }
        
        confirmButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(compatibleSafeArea.bottom).offset(-Spacing.base03)
        }
    }
    
    internal func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
        daysCollectionView.dataSource = collectionViewDataSource
    }
    
    func setControllerData(_ controllerData: DueDayRootViewData) {
        self.controllerData = controllerData
    }
    
    private func updateBestPurchaseLabel() {
        confirmButton.isEnabled = controllerData.selectedRow != nil
        bestPurchaseDayLabel.attributedText = controllerData.bestPurchaseDayAttrString
    }
    
    @objc
    private func confirm() {
        delegate?.didPressConfirm()
    }
}

extension DueDayRootView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didSelectRow(row: indexPath.row)
    }
}
