import Foundation
import Core

protocol DueDayServicing {
    func getDueDays(_ completion: @escaping (Result<DueDayResponse, ApiError>) -> Void)
    func saveDueDays(dueDay: Int, _ completion: @escaping (Result<NoContent, ApiError>) -> Void)
}

final class DueDayService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    private let jsonDecoder = JSONDecoder(.convertFromSnakeCase)
    private let jsonEncorder = JSONEncoder()

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - DueDayServicing
extension DueDayService: DueDayServicing {
    func getDueDays(_ completion: @escaping (Result<DueDayResponse, ApiError>) -> Void) {
        let api = Api<DueDayResponse>(endpoint: CreditServiceEndpoint.getDueDay)
        api.execute(jsonDecoder: jsonDecoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func saveDueDays(dueDay: Int, _ completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        let api = Api<NoContent>(endpoint: CreditServiceEndpoint.saveDueDay(day: dueDay))
        api.execute(jsonDecoder: jsonDecoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
