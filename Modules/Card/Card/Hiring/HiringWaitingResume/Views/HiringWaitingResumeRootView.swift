import UIKit
import UI

final class HiringWaitingResumeRootView: UIView, ViewConfiguration {
    private lazy var scrollView = UIScrollView()
    private lazy var detailView = DetailsView()
    
    init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubview(scrollView)
        scrollView.addSubview(detailView)
    }
    
    func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        detailView.snp.makeConstraints {
            $0.centerX.top.bottom.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
    
    func render(data: [DetailsViewRow]) {
        detailView.render(data)
    }
}
