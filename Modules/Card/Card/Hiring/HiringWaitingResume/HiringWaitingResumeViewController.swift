import UI
import UIKit

protocol HiringWaitingResumeDisplaying: AnyObject {
    func displayRootView(data: [DetailsViewRow])
}

final class HiringWaitingResumeViewController: ViewController<HiringWaitingResumeInteracting, HiringWaitingResumeRootView> {
    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.loadData()
        updateNavigationBarAppearance()
    }

    override func buildViewHierarchy() { }
    
    override func setupConstraints() { }

    override func configureViews() { }
    
    private func updateNavigationBarAppearance() {
        title = Strings.HiringWaitingResume.Scene.title
        navigationController?.navigationBar.isTranslucent = true
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        }
    }
}

// MARK: - HiringWaitingResumeDisplaying
extension HiringWaitingResumeViewController: HiringWaitingResumeDisplaying {
    func displayRootView(data: [DetailsViewRow]) {
        rootView.render(data: data)
    }
}
