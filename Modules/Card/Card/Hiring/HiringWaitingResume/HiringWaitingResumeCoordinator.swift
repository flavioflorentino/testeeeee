import UIKit

enum HiringWaitingResumeAction {
    case `continue`
}

protocol HiringWaitingResumeCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: HiringWaitingResumeAction)
}

final class HiringWaitingResumeCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - HiringWaitingResumeCoordinating
extension HiringWaitingResumeCoordinator: HiringWaitingResumeCoordinating {
    func perform(action: HiringWaitingResumeAction) {
        switch action {
        case .continue:
            viewController?.dismiss(animated: true)
        }
    }
}
