import UIKit

public enum HiringWaitingResumeFactory {
    public static func make(form: CardForm) -> UIViewController {
        let coordinator: HiringWaitingResumeCoordinating = HiringWaitingResumeCoordinator()
        let presenter: HiringWaitingResumePresenting = HiringWaitingResumePresenter(coordinator: coordinator)
        let interactor = HiringWaitingResumeInteractor(presenter: presenter, cardForm: form)
        let viewController = HiringWaitingResumeViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
