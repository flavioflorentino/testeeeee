import Foundation

protocol HiringWaitingResumeInteracting: AnyObject {
    func loadData()
    func confirm()
}

final class HiringWaitingResumeInteractor {
    private let presenter: HiringWaitingResumePresenting
    private let cardForm: CardForm

    init(presenter: HiringWaitingResumePresenting, cardForm: CardForm) {
        self.presenter = presenter
        self.cardForm = cardForm
    }
}

// MARK: - HiringWaitingResumeInteracting
extension HiringWaitingResumeInteractor: HiringWaitingResumeInteracting {
    func confirm() {
        presenter.presentNextStep(action: .continue)
    }
    
    func loadData() {
        presenter.presentRootViewData(cardForm: cardForm)
    }
}
