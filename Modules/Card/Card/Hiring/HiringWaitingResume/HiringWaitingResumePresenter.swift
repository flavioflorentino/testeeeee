import Foundation
import UI

protocol HiringWaitingResumePresenting: AnyObject {
    var viewController: HiringWaitingResumeDisplaying? { get set }
    func presentRootViewData(cardForm: CardForm)
    func presentNextStep(action: HiringWaitingResumeAction)
}

final class HiringWaitingResumePresenter {
    private let coordinator: HiringWaitingResumeCoordinating
    weak var viewController: HiringWaitingResumeDisplaying?

    init(coordinator: HiringWaitingResumeCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - HiringWaitingResumePresenting
extension HiringWaitingResumePresenter: HiringWaitingResumePresenting {
    func presentRootViewData(cardForm: CardForm) {
        viewController?.displayRootView(data: [
            .sectionTitle(text: Strings.HiringWaitingResume.PersonalData.sectionTitle),
            .spacer(spacing: Spacing.base02),
            .unwrap(cardForm.name, { .horizontalText(text: Strings.HiringWaitingResume.PersonalData.name($0)) }),
            .unwrap(cardForm.maskedCPF, { .horizontalText(text: Strings.HiringWaitingResume.PersonalData.cpf($0)) }),
            .unwrap(cardForm.mothersName, { .horizontalText(text: Strings.HiringWaitingResume.PersonalData.mothersName($0)) }),
            .unwrap(cardForm.birthDate, { .horizontalText(text: Strings.HiringWaitingResume.PersonalData.birthday($0)) }),
            .unwrap(cardForm.email, { .horizontalText(text: Strings.HiringWaitingResume.PersonalData.email($0)) }),
            .unwrap(cardForm.completeNumber, { .horizontalText(text: Strings.HiringWaitingResume.PersonalData.phone($0)) }),
            .spacer(spacing: Spacing.base04),
            .sectionTitle(text: Strings.HiringWaitingResume.Birthplace.sectionTitle),
            .spacer(spacing: Spacing.base02),
            .unwrap(cardForm.completeBirthPlace, { .horizontalText(text: $0) }),
            .spacer(spacing: Spacing.base04),
            .sectionTitle(text: Strings.HiringWaitingResume.Address.sectionTitle),
            .spacer(spacing: Spacing.base02),
            .unwrap(cardForm.homeAddress?.completeAddress, { .horizontalText(text: $0) }),
            .spacer(spacing: Spacing.base04),
            .sectionTitle(text: Strings.HiringWaitingResume.Document.sectionTitle),
            .spacer(spacing: Spacing.base02),
            .unwrap(cardForm.completeDocType, { .horizontalText(text: $0) }),
            .unwrap(cardForm.idDocument?.issuerOfficeInitials, { .horizontalText(text: Strings.HiringWaitingResume.Document.expeditionName($0)) }),
            .unwrap(cardForm.idDocument?.emissionDate, { .horizontalText(text: Strings.HiringWaitingResume.Document.emissionDate($0)) }),
            .spacer(spacing: Spacing.base04),
            .sectionTitle(text: Strings.HiringWaitingResume.Additional.sectionTitle),
            .spacer(spacing: Spacing.base02),
            .unwrap(cardForm.incomeString, { .horizontalText(text: Strings.HiringWaitingResume.Additional.income($0)) }),
            .unwrap(cardForm.patrimony?.minMaxValue, { .horizontalText(text: $0) })
        ])
    }
    
    func presentNextStep(action: HiringWaitingResumeAction) {
        coordinator.perform(action: action)
    }
}
