import UIKit
import FeatureFlag
import CustomerSupport

enum ChangeAddressPopupAction {
    case confirm
    case cancel
}

protocol ChangeAddressPopupCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ChangeAddressPopupAction)
}

final class ChangeAddressPopupCoordinator {
    typealias Dependencies = HasFeatureManager
    weak var viewController: UIViewController?
    
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - ChangeAddressPopupCoordinating
extension ChangeAddressPopupCoordinator: ChangeAddressPopupCoordinating {
    func perform(action: ChangeAddressPopupAction) {
        switch action {
        case .confirm:
            let faq = FAQFactory.make(option: .article(id: "360049689392"))
            viewController?.navigationController?.setNavigationBarHidden(false, animated: true)
            viewController?.navigationController?.pushViewController(faq, animated: true)
            
        case .cancel:
            viewController?.dismiss(animated: true, completion: nil)
        }
    }
}
