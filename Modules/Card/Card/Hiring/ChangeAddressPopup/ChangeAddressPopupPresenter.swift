import Foundation

protocol ChangeAddressPopupPresenting: AnyObject {
    func presentNextStep(action: ChangeAddressPopupAction)
}

final class ChangeAddressPopupPresenter {
    private let coordinator: ChangeAddressPopupCoordinating

    init(coordinator: ChangeAddressPopupCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - ChangeAddressPopupPresenting
extension ChangeAddressPopupPresenter: ChangeAddressPopupPresenting {
    func presentNextStep(action: ChangeAddressPopupAction) {
        coordinator.perform(action: action)
    }
}
