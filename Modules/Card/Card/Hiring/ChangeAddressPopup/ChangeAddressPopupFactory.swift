import UIKit

public enum ChangeAddressPopupFactory {
    public static func make() -> UIViewController {
        let container = DependencyContainer()
        let coordinator: ChangeAddressPopupCoordinating = ChangeAddressPopupCoordinator(dependencies: container)
        let presenter: ChangeAddressPopupPresenting = ChangeAddressPopupPresenter(coordinator: coordinator)
        let interactor = ChangeAddressPopupInteractor(presenter: presenter, dependencies: container)
        let viewController = ChangeAddressPopupViewController(interactor: interactor)

        coordinator.viewController = viewController

        return viewController
    }
}
