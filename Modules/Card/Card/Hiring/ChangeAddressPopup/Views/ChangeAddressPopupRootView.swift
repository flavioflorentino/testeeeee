import UIKit
import UI

protocol ChangeAddressPopupRootViewDelegate: AnyObject {
    func didPressConfirm()
    func didPressCancel()
}

extension ChangeAddressPopupRootView.Layout {
    enum Size {
        static let cancelButtonHeight = 14
    }
}

final class ChangeAddressPopupRootView: UIView, ViewConfiguration {
    fileprivate enum Layout {}
    weak var delegate: ChangeAddressPopupRootViewDelegate?
    
    private lazy var infoImageViewContainer = UIView()
    private lazy var infoImageView = InfoImageView()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Strings.RequestCardActivation.confirmButtonTitle, for: .normal)
        button.addTarget(self, action: #selector(confirm), for: .touchUpInside)
        return button
    }()
    
    private lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.RequestCardActivation.cancelButtonTitle, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(cancel), for: .touchUpInside)
        return button
    }()
    
    init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        infoImageViewContainer.addSubview(infoImageView)
        addSubviews(
            infoImageViewContainer,
            confirmButton,
            cancelButton
        )
    }
    
    func setupConstraints() {
        infoImageViewContainer.snp.makeConstraints {
            $0.top.equalTo(compatibleSafeArea.top)
            $0.leading.trailing.equalToSuperview()
        }
        
        infoImageView.snp.makeConstraints {
            $0.top.greaterThanOrEqualToSuperview().offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.bottom.lessThanOrEqualToSuperview().offset(-Spacing.base02)
            $0.center.equalToSuperview()
        }
        
        confirmButton.snp.makeConstraints {
            $0.top.greaterThanOrEqualTo(infoImageViewContainer.snp.bottom)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        cancelButton.snp.makeConstraints {
            $0.top.equalTo(confirmButton.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(compatibleSafeArea.bottom).offset(-Spacing.base03)
            $0.height.equalTo(Layout.Size.cancelButtonHeight)
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
        infoImageView.setupView(
            image: Assets.attention.image,
            title: Strings.RequestCardActivation.InfoImage.title,
            description: Strings.RequestCardActivation.InfoImage.description
        )
    }
    
    @objc
    private func confirm() {
        delegate?.didPressConfirm()
    }
    
    @objc
    private func cancel() {
        delegate?.didPressCancel()
    }
}
