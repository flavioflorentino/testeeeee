import UI
import UIKit

final class ChangeAddressPopupViewController: ViewController<ChangeAddressPopupInteracting, ChangeAddressPopupRootView> {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func configureViews() {
        rootView.delegate = self
    }
}

extension ChangeAddressPopupViewController: ChangeAddressPopupRootViewDelegate {
    func didPressConfirm() {
        interactor.confirm()
    }
    
    func didPressCancel() {
        interactor.cancel()
    }
}
