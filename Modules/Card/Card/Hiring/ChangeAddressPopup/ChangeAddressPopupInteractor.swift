import Foundation
import AnalyticsModule

protocol ChangeAddressPopupInteracting: AnyObject {
    func confirm()
    func cancel()
}

final class ChangeAddressPopupInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let presenter: ChangeAddressPopupPresenting

    init(presenter: ChangeAddressPopupPresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - ChangeAddressPopupInteracting
extension ChangeAddressPopupInteractor: ChangeAddressPopupInteracting {
    func confirm() {
        dependencies.analytics.log(CreditHiringEvent.changeAddress)
        presenter.presentNextStep(action: .confirm)
    }
    
    func cancel() {
        dependencies.analytics.log(CreditHiringEvent.cancelChangeAddress)
        presenter.presentNextStep(action: .cancel)
    }
}
