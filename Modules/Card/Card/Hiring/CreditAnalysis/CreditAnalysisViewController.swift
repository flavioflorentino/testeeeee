import AssetsKit
import UI
import UIKit

protocol CreditAnalysisDisplaying: AnyObject {
    func displayLoadData(description: String, faq: NSMutableAttributedString)
    func displayLoading(enable: Bool)
    func displayError(_ model: StatefulErrorViewModel)
}

final class CreditAnalysisViewController: ViewController<CreditAnalysisInteracting, CreditAnalysisRootView> {
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.loadRegistrationData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBarAppearance()
    }

    private func updateNavigationBarAppearance() {
        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.backgroundColor = .clear
        navigationItem.setHidesBackButton(true, animated: true)
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
            navigationController?.navigationItem.largeTitleDisplayMode = .always
        }
        
        let barButtonItem = UIBarButtonItem(image: Resources.Icons.icoClose.image, style: .plain, target: self, action: #selector(close))
        barButtonItem.tintColor = Colors.branding600.color
        navigationItem.setRightBarButton(barButtonItem, animated: false)
    }

    override func configureViews() {
        rootView.delegate = self
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    @objc
    func close() {
        interactor.close()
    }
}

// MARK: - CreditAnalysisRootViewDelegate
extension CreditAnalysisViewController: CreditAnalysisRootViewDelegate {
    func didConfirm() {
        interactor.confirm()
    }
    
    func didSeeResume() {
        interactor.seeResume()
    }
    
    func openFaq(url: URL) {
        interactor.openFAQ()
    }
}

extension CreditAnalysisViewController: StatefulTransitionViewing {
    func didTryAgain() {
        interactor.loadRegistrationData()
    }
}

// MARK: - CreditAnalysisDisplaying
extension CreditAnalysisViewController: CreditAnalysisDisplaying {
    func displayLoadData(description: String, faq: NSMutableAttributedString) {
        DispatchQueue.main.async {
            self.rootView.setupLoadData(description: description, faq: faq)
        }
    }
    
    func displayLoading(enable: Bool) {
        DispatchQueue.main.async {
            enable ? self.beginState() : self.endState()
        }
    }
    
    func displayError(_ model: StatefulErrorViewModel) {
        DispatchQueue.main.async {
            self.endState(animated: true, model: model, completion: nil)
        }
    }
}
