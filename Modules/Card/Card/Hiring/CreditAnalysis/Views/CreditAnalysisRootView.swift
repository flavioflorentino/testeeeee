import AssetsKit
import UIKit
import UI

protocol CreditAnalysisRootViewDelegate: AnyObject {
    func didConfirm()
    func didSeeResume()
    func openFaq(url: URL)
}

final class CreditAnalysisRootView: UIView, ViewConfiguration {
    weak var delegate: CreditAnalysisRootViewDelegate?
    
    private lazy var infoImageView: InfoImageView = {
        let img = InfoImageView()
        img.isHidden = true
        return img
    }()
    
    private let imageContainerView = UIView()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTouchConfirmButton), for: .touchUpInside)
        button.setTitle(Strings.CreditWaitingAnalysis.confirmTitle, for: .normal)
        button.isHidden = true
        return button
    }()
    
    private lazy var resumeButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.CreditWaitingAnalysis.resumeTitle, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(didTouchSeeResumeButton), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    
    private lazy var helpView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale050.color
        view.isHidden = true
        return view
    }()
    
    private lazy var helpLabel: UITextView = {
        let text = UITextView()
        text.font = Typography.bodyPrimary().font()
        text.textAlignment = .center
        text.textColor = Colors.grayscale700.color
        text.isScrollEnabled = false
        text.backgroundColor = .clear
        text.linkTextAttributes = [NSAttributedString.Key.foregroundColor: Colors.branding400.color]
        text.isEditable = false
        text.delegate = self
        return text
    }()
    
    init() {
        super.init(frame: .zero)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        helpView.addSubview(helpLabel)
        imageContainerView.addSubview(infoImageView)
        addSubviews(imageContainerView, confirmButton, resumeButton, helpView)
    }
    
    func setupConstraints() {
        imageContainerView.snp.makeConstraints {
            $0.top.equalTo(snp.topMargin).offset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(confirmButton.snp.top).offset(-Spacing.base04)
        }
        
        infoImageView.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.top.greaterThanOrEqualToSuperview()
            $0.bottom.lessThanOrEqualToSuperview()
            $0.leading.trailing.equalToSuperview()
        }
        
        confirmButton.snp.makeConstraints {
            $0.bottom.equalTo(resumeButton.snp.top).offset(-Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        resumeButton.snp.makeConstraints {
            $0.bottom.equalTo(helpLabel.snp.top).offset(-Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        helpView.snp.makeConstraints {
            $0.leading.trailing.bottom.equalToSuperview()
        }
        
        helpLabel.snp.makeConstraints {
            $0.top.equalToSuperview().inset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base04)
            $0.bottom.equalTo(compatibleSafeArea.bottom).inset(Spacing.base02)
        }
    }
    
    @objc
    func didTouchConfirmButton() {
        delegate?.didConfirm()
    }
    
    @objc
    func didTouchSeeResumeButton() {
        delegate?.didSeeResume()
    }
    
    func setupLoadData(description: String, faq: NSMutableAttributedString) {
        infoImageView.setupView(
            image: Resources.Illustrations.iluClock3.image,
            title: Strings.CreditWaitingAnalysis.title,
            description: description
        )
        helpLabel.attributedText = faq
        
        infoImageView.isHidden = false
        confirmButton.isHidden = false
        resumeButton.isHidden = false
        helpView.isHidden = false
    }
}

// MARK: - UITextViewDelegate
extension CreditAnalysisRootView: UITextViewDelegate {
    public func textView(
        _ textView: UITextView,
        shouldInteractWith URL: URL,
        in characterRange: NSRange,
        interaction: UITextItemInteraction
    ) -> Bool {
        delegate?.openFaq(url: URL)
        return false
    }
}
