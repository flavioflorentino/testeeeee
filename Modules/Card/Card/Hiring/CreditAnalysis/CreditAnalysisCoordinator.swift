import CustomerSupport
import SafariServices
import UIKit

enum CreditAnalysisAction: Equatable {
    case seeResume(formModel: CardForm)
    case faq(url: URL)
    case openZendesk(faqOption: FAQOptions)
    case close
    
    static func == (lhs: CreditAnalysisAction, rhs: CreditAnalysisAction) -> Bool {
        switch (lhs, rhs) {
        case (.close, .close):
            return true
        case (.faq, .faq):
            return true
        case let (.seeResume(lhsForm), .seeResume(rhsForm)):
            return lhsForm == rhsForm
        default:
            return false
        }
    }
}

protocol CreditAnalysisCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: CreditAnalysisAction)
}

final class CreditAnalysisCoordinator {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - CreditAnalysisCoordinating
extension CreditAnalysisCoordinator: CreditAnalysisCoordinating {
    func perform(action: CreditAnalysisAction) {
        switch action {
        case .seeResume(let formModel):
            let controller = HiringWaitingResumeFactory.make(form: formModel)
            viewController?.navigationController?.pushViewController(controller, animated: true)
            
        case .close:
            viewController?.navigationController?.dismiss(animated: true) { [weak self] in
                self?.viewController?.navigationController?.popToRootViewController(animated: true)
            }
        case let .openZendesk(faqOption):
            let faq = FAQFactory.make(option: faqOption)
            viewController?.navigationController?.pushViewController(faq, animated: true)
        case let .faq(url):
            let safariVC = SFSafariViewController(url: url)
            if #available(iOS 11.0, *) {
                safariVC.configuration.barCollapsingEnabled = true
                safariVC.configuration.entersReaderIfAvailable = true
            }
            viewController?.present(safariVC, animated: true, completion: nil)
        }
    }
}
