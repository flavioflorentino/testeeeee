import UIKit

public enum CreditAnalysisFactory {
    public static func make(isLoan: Bool = false) -> UIViewController {
        let container = DependencyContainer()
        let service: CreditAnalysisServicing = CreditAnalysisService(dependencies: container)
        let coordinator: CreditAnalysisCoordinating = CreditAnalysisCoordinator(dependencies: container)
        let presenter: CreditAnalysisPresenting = CreditAnalysisPresenter(coordinator: coordinator, dependencies: container)
        let interactor = CreditAnalysisInteractor(
            service: service,
            presenter: presenter,
            dependencies: container,
            isLoan: isLoan
        )
        let viewController = CreditAnalysisViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
