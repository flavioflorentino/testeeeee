import Core
import Foundation

protocol CreditAnalysisServicing {
    func loadRegistrationdata(cardForm: CardForm?, completion: @escaping CompletionGetRegistrationData)
}

typealias CompletionGetRegistrationData = (Result<CardForm, ApiError>) -> Void

final class CreditAnalysisService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - CreditAnalysisServicing
extension CreditAnalysisService: CreditAnalysisServicing {
    func loadRegistrationdata(cardForm: CardForm?, completion: @escaping CompletionGetRegistrationData) {
        dependencies.mainQueue.async {
            let api = Api<CardForm>(endpoint: CreditServiceEndpoint.creditSetup)
            let jsonDecoder = JSONDecoder()
            jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
            api.execute(jsonDecoder: jsonDecoder) { result in
                completion(result.map(\.model))
            }
        }
    }
}
