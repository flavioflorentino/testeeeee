import AnalyticsModule
import Foundation

protocol CreditAnalysisInteracting: AnyObject {
    func loadRegistrationData()
    func openFAQ()
    func seeResume()
    func confirm()
    func close()
}

final class CreditAnalysisInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    private let isLoan: Bool
    
    private let service: CreditAnalysisServicing
    private let presenter: CreditAnalysisPresenting
    
    private var cardForm: CardForm?
    
    init(
        service: CreditAnalysisServicing,
        presenter: CreditAnalysisPresenting,
        dependencies: Dependencies,
        isLoan: Bool = false
    ) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.isLoan = isLoan
    }
}

// MARK: - CreditAnalysisInteracting
extension CreditAnalysisInteractor: CreditAnalysisInteracting {
    func loadRegistrationData() {
        presenter.presentLoading(enable: true)
        service.loadRegistrationdata(cardForm: cardForm) { [weak self] result in
            switch result {
            case .success(let cardForm):
                self?.presenter.presentLoading(enable: false)
                self?.cardForm = cardForm
                self?.presenter.presentScreen()
                
            case .failure(let error):
                self?.presenter.presentError(error)
            }
        }
    }
    
    func openFAQ() {
        presenter.openFAQ()
    }
    
    func seeResume() {
        dependencies.analytics.log(RequestCardEvent.infoAnalysis(action: .viewSummary, isLoan: isLoan))
        guard let form = cardForm else {
            return
        }
        presenter.didNextStep(action: .seeResume(formModel: form))
    }
    
    func confirm() {
        dependencies.analytics.log(RequestCardEvent.infoAnalysis(action: .close, isLoan: isLoan))
        presenter.didNextStep(action: .close)
    }
    
    func close() {
        presenter.didNextStep(action: .close)
    }
}
