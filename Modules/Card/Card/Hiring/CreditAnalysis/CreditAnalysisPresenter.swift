import CustomerSupport
import Core
import FeatureFlag
import Foundation
import UI

protocol CreditAnalysisPresenting: AnyObject {
    var viewController: CreditAnalysisDisplaying? { get set }
    func didNextStep(action: CreditAnalysisAction)
    func openFAQ()
    func presentLoading(enable: Bool)
    func presentError(_ error: ApiError)
    func presentScreen()
}

final class CreditAnalysisPresenter {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies

    private let coordinator: CreditAnalysisCoordinating
    weak var viewController: CreditAnalysisDisplaying?
    
    private let faqURL = "http://app.picpay.com/helpcenter?query=onde-posso-usar-o-meu-picpay-card"

    init(coordinator: CreditAnalysisCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
    
    private func createAttributedString(with string: String, link: String) -> NSMutableAttributedString {
        let attributed = NSMutableAttributedString(string: string)
        attributed.font(text: string, font: Typography.bodySecondary().font())
        attributed.textBackgroundColor(text: string, color: .clear)
        attributed.textColor(text: string, color: Colors.grayscale600.color)
        attributed.paragraph(aligment: .center, lineSpace: 3.0)
        attributed.underline(text: link)
        attributed.link(text: link, link: faqURL)
        return attributed
    }
    
    private func getHelpMessage() -> NSMutableAttributedString {
        let linked = Strings.CreditWaitingAnalysis.helpCenterLink
        let string = Strings.CreditWaitingAnalysis.helpCenter
        let attributed = createAttributedString(with: string, link: linked)
        return attributed
    }
}

// MARK: - CreditAnalysisPresenting
extension CreditAnalysisPresenter: CreditAnalysisPresenting {
    func didNextStep(action: CreditAnalysisAction) {
        coordinator.perform(action: action)
    }
    
    func presentLoading(enable: Bool) {
        viewController?.displayLoading(enable: enable)
    }
    
    func openFAQ() {
        let options = FAQOptions.category(id: "360003297112")
        coordinator.perform(action: .openZendesk(faqOption: options))
    }
    
    func presentError(_ error: ApiError) {
        viewController?.displayError(.init(error))
    }
    
    func presentScreen() {
        let description = dependencies.featureManager.text(.creditPicPayRegistrationStatusAnalysisMessage)
        viewController?.displayLoadData(description: description, faq: getHelpMessage())
    }
}
