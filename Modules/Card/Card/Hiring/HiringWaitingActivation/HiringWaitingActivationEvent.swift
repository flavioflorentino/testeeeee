import AnalyticsModule

enum HiringWaitingActivationEvent: AnalyticsKeyProtocol {
    case didViewUpgradeScreen
    case didViewCreditScreen
    case didSelectConfirm
    
    private var name: String {
        switch self {
        case .didViewCreditScreen:
            return "Card Activated Message"
        case .didViewUpgradeScreen:
            return "Card Upgrade Message"
        case .didSelectConfirm:
            return "Card Analysis Message"
        }
    }
    
    var properties: [String: Any] {
        guard case .didSelectConfirm = self else {
            return ["action": "act-analysis-message-close"]
        }
        
        return [:]
    }
    
    var providers: [AnalyticsProvider] {
        [.mixPanel]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PicPay Card - Request - \(name)", properties: properties, providers: providers)
    }
}
