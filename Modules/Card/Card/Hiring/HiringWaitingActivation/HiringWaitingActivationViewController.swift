import UI
import UIKit
import AssetsKit

protocol HiringWaitingActivationDisplaying: AnyObject {
    func displayScreen(image: UIImage, title: String, description: String)
}

final class HiringWaitingActivationViewController: ViewController<HiringWaitingActivationInteracting, HiringWaitingActivationRootView> {
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.loadScreen()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBarAppearance()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        interactor.trackScreenView()
    }

    override func configureViews() {
        rootView.delegate = self
    }

     private func updateNavigationBarAppearance() {
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.isTranslucent = true
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }
        navigationItem.hidesBackButton = true
    }
    
    @objc
    func close() {
        interactor.pressClose()
    }
}

// MARK: - HiringWaitingActivationDisplaying
extension HiringWaitingActivationViewController: HiringWaitingActivationDisplaying {
    func displayScreen(image: UIImage, title: String, description: String) {
        rootView.configureViews(image: image, title: title, description: description)
    }
}

// MARK: - HiringWaitingActivationRootViewDelegate
extension HiringWaitingActivationViewController: HiringWaitingActivationRootViewDelegate {
    func didPressConfirm() {
        interactor.pressConfirm()
    }
}
