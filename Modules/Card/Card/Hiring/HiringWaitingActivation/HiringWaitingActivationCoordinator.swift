import UIKit

enum HiringWaitingActivationAction {
    case confirm
    case close
}

protocol HiringWaitingActivationCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: HiringWaitingActivationAction)
}

final class HiringWaitingActivationCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - HiringWaitingActivationCoordinating
extension HiringWaitingActivationCoordinator: HiringWaitingActivationCoordinating {
    func perform(action: HiringWaitingActivationAction) {
        viewController?.navigationController?.dismiss(animated: true)
    }
}
