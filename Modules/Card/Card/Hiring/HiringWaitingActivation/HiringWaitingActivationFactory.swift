import UIKit

public enum HiringWaitingActivationFactory {
    public static func make(isUpgrade: Bool) -> UIViewController {
        let container = DependencyContainer()
        let coordinator: HiringWaitingActivationCoordinating = HiringWaitingActivationCoordinator()
        let presenter: HiringWaitingActivationPresenting = HiringWaitingActivationPresenter(coordinator: coordinator)
        let interactor = HiringWaitingActivationInteractor(presenter: presenter, dependencies: container, isUpgrade: isUpgrade)
        let viewController = HiringWaitingActivationViewController(interactor: interactor)

        presenter.viewController = viewController
        coordinator.viewController = viewController

        return viewController
    }
}
