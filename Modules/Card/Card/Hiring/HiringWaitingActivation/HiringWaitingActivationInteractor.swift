import AnalyticsModule
import Foundation

protocol HiringWaitingActivationInteracting: AnyObject {
    func loadScreen()
    func pressConfirm()
    func pressClose()
    func trackScreenView()
}

final class HiringWaitingActivationInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let presenter: HiringWaitingActivationPresenting
    private let isUpgrade: Bool

    init(presenter: HiringWaitingActivationPresenting, dependencies: Dependencies, isUpgrade: Bool) {
        self.presenter = presenter
        self.dependencies = dependencies
        self.isUpgrade = isUpgrade
    }
}

// MARK: - HiringWaitingActivationInteracting
extension HiringWaitingActivationInteractor: HiringWaitingActivationInteracting {
    func trackScreenView() {
        let event = isUpgrade ? HiringWaitingActivationEvent.didViewUpgradeScreen : HiringWaitingActivationEvent.didViewCreditScreen
        dependencies.analytics.log(event)
    }
    
    func loadScreen() {
        presenter.presentScreen(isUpgrade: isUpgrade)
    }
    
    func pressConfirm() {
        dependencies.analytics.log(HiringWaitingActivationEvent.didSelectConfirm)
        presenter.didNextStep(action: .confirm)
    }
    
    func pressClose() {
        presenter.didNextStep(action: .close)
    }
}
