import Foundation
import AssetsKit

protocol HiringWaitingActivationPresenting: AnyObject {
    var viewController: HiringWaitingActivationDisplaying? { get set }
    func presentScreen(isUpgrade: Bool)
    func didNextStep(action: HiringWaitingActivationAction)
}

final class HiringWaitingActivationPresenter {
    typealias Localizable = Strings.HiringWaitingActivation
    private let coordinator: HiringWaitingActivationCoordinating
    weak var viewController: HiringWaitingActivationDisplaying?

    init(coordinator: HiringWaitingActivationCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - HiringWaitingActivationPresenting
extension HiringWaitingActivationPresenter: HiringWaitingActivationPresenting {
    func presentScreen(isUpgrade: Bool) {
        let image = isUpgrade ? Resources.Illustrations.iluCardHeart.image: Resources.Illustrations.iluClock3.image
        let title = Localizable.title
        let description = isUpgrade ? Localizable.Upgrade.description: Localizable.Credit.description
        viewController?.displayScreen(image: image, title: title, description: description)
    }
    
    func didNextStep(action: HiringWaitingActivationAction) {
        coordinator.perform(action: action)
    }
}
