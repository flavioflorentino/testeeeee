import AnalyticsModule
import FeatureFlag
import Foundation
import UI

public protocol DynamicOnboardingPresenting: AnyObject {
    var viewController: DynamicOnboardingDisplaying? { get set }
    func didNextStep(action: DynamicOnboardingAction)
    func setScreenData(with type: OnboardingTypeFlow, showCashout: Bool)
}

final class DynamicOnboardingPresenter {
    private typealias Localizable = Strings.DynamicOnboardingPresenter
    private let coordinator: DynamicOnboardingCoordinating
    weak var viewController: DynamicOnboardingDisplaying?

    typealias Dependencies = HasAnalytics & HasFeatureManager
    private let dependencies: Dependencies

    private enum ExternalURL {
        static let pdf = "https://cdn.picpay.com/debito/oferta/Picpay-Debito-Contrato-de-Adesao.pdf"
    }
    
    init(coordinator: DynamicOnboardingCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
    
    private func headerTitle(_ type: OnboardingTypeFlow) -> NSAttributedString {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        paragraphStyle.lineSpacing = 1.4
        
        switch type {
        case .debit:
            let text = NSMutableAttributedString(string: Strings.DynamicOnboarding.Header.Debit.text)
            text.textColor(text: Strings.DynamicOnboarding.Header.Debit.highlight, color: Colors.branding600.color)
            text.paragraphStyle(text: Strings.DynamicOnboarding.Header.Debit.text, paragraphStyle: paragraphStyle)
            return text
        case .multiple:
            let text = NSMutableAttributedString(string: Strings.DynamicOnboarding.Header.Multiple.text)
            text.textColor(text: Strings.DynamicOnboarding.Header.Multiple.highlight, color: Colors.branding600.color)
            text.paragraphStyle(text: Strings.DynamicOnboarding.Header.Multiple.text, paragraphStyle: paragraphStyle)
            return text
        case .debitToMultiple:
            let text = NSMutableAttributedString(string: Strings.DynamicOnboarding.Header.Upgrade.text)
            text.textColor(text: Strings.DynamicOnboarding.Header.Upgrade.highlight, color: Colors.branding600.color)
            text.paragraphStyle(text: Strings.DynamicOnboarding.Header.Upgrade.text, paragraphStyle: paragraphStyle)
            return text
        }
    }
    
    private func confirmButton(_ type: OnboardingTypeFlow) -> String {
        if type == .debitToMultiple {
            return Strings.DynamicOnboarding.Confirm.upgrade
        } else {
            return Strings.DynamicOnboarding.Confirm.request
        }
    }
}

// MARK: - DynamicOnboardingPresenting
extension DynamicOnboardingPresenter: DynamicOnboardingPresenting {
    func setScreenData(with type: OnboardingTypeFlow, showCashout: Bool) {
        switch type {
        case .debit:
            viewController?.displayRows(with:
                [
                    .header(DynamicOnboardingHeaderView(title: headerTitle(type))),
                    .item(DynamicOnboardingItemView(icon: Assets.iconOnboardingCard.image, description: Strings.DynamicOnboarding.Benefits.Debit.saves)),
                    .item(DynamicOnboardingItemView(icon: Assets.iconOnboardingPlace.image, description: Strings.DynamicOnboarding.Benefits.Debit.cashin)),
                    .item(DynamicOnboardingItemView(icon: Assets.iconOnboardingCash.image, description: Strings.DynamicOnboarding.Benefits.Debit.cashout))
                ]
            )
            
            viewController?.hideWarningMessage()
            
        case .multiple:
            viewController?.displayRows(with:
                [
                    .header(DynamicOnboardingHeaderView(title: headerTitle(type))),
                    .item(DynamicOnboardingItemView(icon: Assets.iconOnboardingCard.image, description: Strings.DynamicOnboarding.Benefits.Multiple.saves)),
                    .item(DynamicOnboardingItemView(icon: Assets.iconOnboardingShop.image, description: Strings.DynamicOnboarding.Benefits.Multiple.anywhere)),
                    .item(DynamicOnboardingItemView(icon: Assets.iconOnboardingCashback.image, description: Strings.DynamicOnboarding.Benefits.Multiple.cashback))
                ]
            )
            
        case .debitToMultiple:
            viewController?.displayRows(with:
                [
                    .header(DynamicOnboardingHeaderView(title: headerTitle(type))),
                    .item(DynamicOnboardingItemView(icon: Assets.iconOnboardingCard.image, description: Strings.DynamicOnboarding.Benefits.Upgrade.saves)),
                    .item(DynamicOnboardingItemView(icon: Assets.iconOnboardingShop.image, description: Strings.DynamicOnboarding.Benefits.Upgrade.anywhere)),
                    .item(DynamicOnboardingItemView(icon: Assets.iconOnboardingCashback.image, description: Strings.DynamicOnboarding.Benefits.Upgrade.cashback))
                ]
            )
        }

        viewController?.displayPinnedView(with: confirmButton(type))
    }
    
    func didNextStep(action: DynamicOnboardingAction) {
        coordinator.perform(action: action)
    }
}
