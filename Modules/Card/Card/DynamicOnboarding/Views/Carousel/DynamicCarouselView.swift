import UIKit
import UI

private extension DynamicCarouselView.Layout {
    enum Size {
        static let screenWidth = UIScreen.main.bounds.width
        static let itemHeight: CGFloat = 280
        static let offset: CGFloat = 20
    }
    
    enum Section {
        case main
    }
    
    enum Color {
        static let branding = Colors.branding400.color
    }
}

public protocol DynamicCarouselViewDelegate: AnyObject {
    func didInteractWithAdvantages()
}

public protocol DynamicCarouselDisplay: AnyObject {
    var delegate: DynamicCarouselViewDelegate? { get set }
}

final class DynamicCarouselView: UIView, ViewConfiguration, DynamicCarouselDisplay {
    fileprivate enum Layout {}
    
    private let presenter: DynamicCarouselPresenting
    weak var delegate: DynamicCarouselViewDelegate?
    
    private lazy var advantagesPageControl: UIPageControl = {
        let pageControl = UIPageControl()
        pageControl.pageIndicatorTintColor = Colors.grayscale300.color
        pageControl.currentPageIndicatorTintColor = Layout.Color.branding
        pageControl.backgroundColor = .clear
        pageControl.currentPage = 0
        pageControl.isUserInteractionEnabled = false
        return pageControl
    }()
    
    private lazy var advantagesCollectionView: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = .init(width: Layout.Size.screenWidth, height: Layout.Size.itemHeight)
        flowLayout.minimumLineSpacing = 0
        let collection = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collection.showsHorizontalScrollIndicator = false
        collection.isPagingEnabled = true
        collection.backgroundColor = .clear
        collection.delegate = self
        collection.register(AdvantageCellCollectionViewCell.self, forCellWithReuseIdentifier: AdvantageCellCollectionViewCell.identifier)
        return collection
    }()
    
    private lazy var collectionViewDataSource: CollectionViewDataSource<Layout.Section, AdvantageCardViewInfo> = {
        let dataSource = CollectionViewDataSource<Layout.Section, AdvantageCardViewInfo>(view: advantagesCollectionView)
        dataSource.itemProvider = { view, indexPath, item -> UICollectionViewCell? in
            let cell = view.dequeueReusableCell(withReuseIdentifier: AdvantageCellCollectionViewCell.identifier, for: indexPath) as? AdvantageCellCollectionViewCell
            cell?.configure(with: item)
            return cell
        }
        dataSource.add(section: .main)
        return dataSource
    }()
    
    init(presenter: DynamicCarouselPresenting) {
        self.presenter = presenter
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubviews(advantagesCollectionView, advantagesPageControl)
    }
    
    func setupConstraints() {
        advantagesCollectionView.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.itemHeight)
            $0.top.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(advantagesPageControl.snp.top).offset(-Layout.Size.offset)
        }
        advantagesPageControl.snp.makeConstraints {
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    func configureViews() {
        advantagesCollectionView.dataSource = collectionViewDataSource
        collectionViewDataSource.add(items: presenter.getAdvantages(), to: .main)
        advantagesPageControl.numberOfPages = presenter.getAdvantages().count
    }
}

extension DynamicCarouselView: UICollectionViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let index = scrollView.contentOffset.x / scrollView.frame.size.width
        advantagesPageControl.currentPage = Int(round(index))
        delegate?.didInteractWithAdvantages()
    }
}
