import Foundation
import UI

protocol DynamicCarouselPresenting: AnyObject {
    func getAdvantages() -> [AdvantageCardViewInfo]
}

final class DynamicCarouselPresenter {
    private typealias Localizable = Strings.DynamicCarouselView
    
    internal enum Layout {
        static let fontBodySecondary = Typography.bodySecondary(.default).font()
        static let fontBodySecondaryHighlight = Typography.bodySecondary(.highlight).font()
        static let paragraphLineSpace: CGFloat = 5.0
        static let paragraphAligment: NSTextAlignment = .center
    }
    
    private let type: OnboardingTypeFlow
    private let showCashout: Bool
    
    init(with type: OnboardingTypeFlow, showCashout: Bool) {
        self.type = type
        self.showCashout = showCashout
    }
}

// MARK: - DynamicOnboardingPresenting
extension DynamicCarouselPresenter: DynamicCarouselPresenting {
    func getAdvantages() -> [AdvantageCardViewInfo] {
        switch type {
        case .debit:
            return [
                buildAdvantagePayEverything(),
                showCashout ? buildAdvantageCashout24hours() : nil,
                buildAdvantageMasterCardSurpreendaView()
            ].compactMap { $0 }
            
        case .multiple:
            return [
                buildAdvantageAnnuity(),
                buildAdvantagePayEverything(),
                showCashout ? buildAdvantageCashout24hours() : nil,
                buildAdvantageMasterCardSurpreendaView()
            ].compactMap { $0 }
            
        case .debitToMultiple:
            return [
                buildAdvantageAnnuity(),
                buildAdvantagePayEverything(),
                showCashout ? buildAdvantageCashout24hours() : nil,
                buildAdvantageMasterCardSurpreendaView()
            ].compactMap { $0 }
        }
    }
}

extension DynamicCarouselPresenter {
    internal func buildAdvantageAnnuity() -> AdvantageCardViewInfo {
        let title = Localizable.AdvantageAnnuity.Title.text
        let image = Assets.icoCardAdvAnnuity.image
        let descr = Localizable.AdvantageAnnuity.Description.text
        
        let descrAtt = NSMutableAttributedString(string: descr)
        descrAtt.font(text: descr, font: Layout.fontBodySecondary)
        descrAtt.font(text: Localizable.AdvantageAnnuity.Description.highlight, font: Layout.fontBodySecondaryHighlight)
        descrAtt.textColor(text: descr, color: Colors.black.color)
        descrAtt.paragraph(aligment: Layout.paragraphAligment, lineSpace: Layout.paragraphLineSpace)
        
        return AdvantageImageCardViewInfo(title: title, image: image, description: descrAtt, link: true)
    }
    
    internal func buildAdvantageCashout24hours() -> AdvantageCardViewInfo {
        let title = Localizable.Advantage24Hours.title
        let image = Assets.iconCard.image
        
        let text = NSMutableAttributedString(string: Localizable.Advantage24Hours.description)
        text.paragraph(aligment: Layout.paragraphAligment, lineSpace: Layout.paragraphLineSpace)
        text.textColor(text: Localizable.AdvantagePayEverything.description, color: Colors.black.color)
        text.font(text: text.string, font: Layout.fontBodySecondary)
        text.textColor(text: Localizable.Advantage24Hours.highlight, color: Colors.critical600.color)
        
        return AdvantageImageCardViewInfo(
            title: title,
            image: image,
            description: text,
            link: false
        )
    }
    
    internal func buildAdvantagePayEverything() -> AdvantageCardViewInfo {
        let title = Localizable.AdvantagePayEverything.Title.text
        let image = Assets.icoCardAdvPayeverything.image
        let descr = Localizable.AdvantagePayEverything.Description.text
        
        let descrAtt = NSMutableAttributedString(string: descr)
        descrAtt.font(text: descr, font: Layout.fontBodySecondary)
        descrAtt.font(text: Localizable.AdvantagePayEverything.Description.highlight1, font: Layout.fontBodySecondaryHighlight)
        descrAtt.font(text: Localizable.AdvantagePayEverything.Description.highlight2, font: Layout.fontBodySecondaryHighlight)
        descrAtt.font(text: Localizable.AdvantagePayEverything.Description.highlight3, font: Layout.fontBodySecondaryHighlight)
        descrAtt.textColor(text: descr, color: Colors.black.color)
        descrAtt.paragraph(aligment: Layout.paragraphAligment, lineSpace: Layout.paragraphLineSpace)
        
        return AdvantageImageCardViewInfo(title: title, image: image, description: descrAtt, link: true)
    }
    
    internal func buildAdvantageMasterCardSurpreendaView() -> AdvantageCardViewInfo {
        let title = Localizable.AdvantageMasterCardSurpreenda.Title.text
        let image = Assets.icoCardAdvMcsurp.image
        let descr = Localizable.AdvantageMasterCardSurpreenda.Description.text
        
        let descrAtt = NSMutableAttributedString(string: descr)
        descrAtt.font(text: descr, font: Layout.fontBodySecondary)
        descrAtt.textColor(text: descr, color: Colors.black.color)
        descrAtt.paragraph(aligment: Layout.paragraphAligment, lineSpace: Layout.paragraphLineSpace)
        
        return AdvantageImageCardViewInfo(title: title, image: image, description: descrAtt, link: true)
    }
}
