import UI
import UIKit

final class AdvantageCellCollectionViewCell: UICollectionViewCell {
    private lazy var advantageImageCardView = AdvantageImageCardView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(with info: AdvantageCardViewInfo) {
        guard let info = info as? AdvantageImageCardViewInfo else {
            return
        }
        
        advantageImageCardView.setupCard(info: info)
    }
}

extension AdvantageCellCollectionViewCell: ViewConfiguration {
    internal func buildViewHierarchy() {
        contentView.addSubview(advantageImageCardView)
    }
    
    internal func setupConstraints() {
        advantageImageCardView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
    }
}
