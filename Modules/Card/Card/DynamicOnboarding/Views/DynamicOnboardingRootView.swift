import UIKit
import UI
import SnapKit

public protocol DynamicOnboardingRootViewDelegate: AnyObject {
    func didInteractWithAdvantages()
    func didPressConfirm()
    func didPressHelp()
    func didPressRules()
}

public enum DynamicOnboardingTypeRows {
    case header(UIView)
    case carousel((UIView & DynamicCarouselDisplay))
    case item(UIView)
}

public final class DynamicOnboardingRootView: UIView, ViewConfiguration {
    weak var delegate: DynamicOnboardingRootViewDelegate?
    
    private let scrollView = UIScrollView()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fill
        stackView.axis = .vertical
        stackView.spacing = 0
        return stackView
    }()
    
    private var stackHeight: ConstraintMakerEditable?
    
    private let pinnedView = BottomPinnedView()
    
    init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func buildViewHierarchy() {
        addSubview(scrollView)
        scrollView.addSubview(stackView)
        addSubview(pinnedView)
    }
    
    public func setupConstraints() {
        pinnedView.snp.makeConstraints {
            $0.left.right.equalToSuperview()
            $0.bottom.equalTo(compatibleSafeArea.bottom)
        }
        scrollView.snp.makeConstraints {
            $0.bottom.equalToSuperview()
            $0.top.equalTo(compatibleSafeArea.top)
            $0.leading.trailing.equalToSuperview()
        }
        stackView.snp.makeConstraints {
            $0.edges.centerX.equalToSuperview()
        }
    }
    
    public func configureViews() {
        scrollView.backgroundColor = Colors.backgroundPrimary.color
        backgroundColor = Colors.white.color
        pinnedView.delegate = self
        scrollView.contentInset = .init(top: 0, left: 0, bottom: Spacing.base12, right: 0)
    }
    
    func render(_ rows: [DynamicOnboardingTypeRows]) {
        rows.forEach {
            switch $0 {
            case let .header(view):
                stackView.addArrangedSubview(view)
            case let .carousel(view):
                stackView.addArrangedSubview(view)
                view.delegate = self
            case let .item(view):
                stackView.addArrangedSubview(view)
                
                switch view {
                case let view as DebitFooterView:
                    view.delegate = self
                case let view as MultipleFooterView:
                    view.delegate = self
                default:
                    break
                }
            }
        }
    }
    
    func displayPinnedView(with text: String) {
        pinnedView.setupText(text)
    }
    
    func hideWarningMessage() {
        pinnedView.hideWarningMessage()
    }
}

extension DynamicOnboardingRootView: DynamicCarouselViewDelegate, BottomPinnedViewDelegate, DebitFooterViewDelegate, MultipleFooterViewDelegate {
    func didTapRulesButton() {
        delegate?.didPressRules()
    }
    
    public func didTapHelpButton() {
        delegate?.didPressHelp()
    }
    
    func didPressConfirm() {
        delegate?.didPressConfirm()
    }
    
    public func didInteractWithAdvantages() {
        delegate?.didInteractWithAdvantages()
    }
}
