import UI
import UIKit

private extension DynamicOnboardingItemView.Layout {
    enum Size {
        static let itemImageView = CGSize(width: 59.0, height: 58.0)
    }
}

public final class DynamicOnboardingItemView: UIView {
    fileprivate enum Layout { }
    
    private lazy var itemImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle()).with(\.textColor, .grayscale600())
        return label
    }()
    
    convenience init(icon: UIImage, description: String) {
        self.init(frame: UIScreen.main.bounds)
        
        itemImageView.image = icon
        descriptionLabel.text = description
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        configureViews()
        buildViewHierarchy()
        setupConstraints()
    }
}

extension DynamicOnboardingItemView: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubviews(itemImageView, descriptionLabel)
    }
    
    public func setupConstraints() {
        itemImageView.snp.makeConstraints {
            $0.top.equalTo(compatibleSafeArea.top).offset(Spacing.base02)
            $0.leading.equalTo(compatibleSafeArea.leading).offset(Spacing.base02)
            $0.trailing.equalTo(descriptionLabel.compatibleSafeArea.leading).offset(-Spacing.base02)
            $0.size.equalTo(Layout.Size.itemImageView)
            $0.bottom.equalTo(compatibleSafeArea.bottom).offset(-Spacing.base02)
        }
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(compatibleSafeArea.top).offset(Spacing.base02)
            $0.leading.equalTo(itemImageView.compatibleSafeArea.trailing).offset(Spacing.base02)
            $0.trailing.equalTo(compatibleSafeArea.trailing).offset(-Spacing.base02)
            $0.bottom.equalTo(compatibleSafeArea.bottom).offset(-Spacing.base02)
        }
    }
    
    public func configureViews() {
        backgroundColor = .clear
    }
}
