import UI
import UIKit

private extension DynamicOnboardingHeaderView.Layout {
    enum Size {
        static let headerImageView = CGSize(width: 176.0, height: 176.0)
    }
}

final class DynamicOnboardingHeaderView: UIView {
    fileprivate enum Layout { }
    
    private lazy var headerImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.icoOnboardingCardThumb.image
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .medium)).with(\.textColor, .black())
        return label
    }()
    
    convenience init(title: NSAttributedString) {
        self.init(frame: UIScreen.main.bounds)
        titleLabel.attributedText = title
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        configureViews()
        buildViewHierarchy()
        setupConstraints()
    }
}

extension DynamicOnboardingHeaderView: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubviews(headerImageView, titleLabel)
    }
    
    public func setupConstraints() {
        headerImageView.snp.makeConstraints {
            $0.top.equalTo(compatibleSafeArea.top).offset(Spacing.base02)
            $0.centerX.equalTo(compatibleSafeArea.centerX)
            $0.size.equalTo(Layout.Size.headerImageView)
            $0.bottom.equalTo(titleLabel.compatibleSafeArea.top).offset(-Spacing.base02)
        }
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(headerImageView.compatibleSafeArea.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.bottom.equalTo(compatibleSafeArea.bottom).offset(-Spacing.base02)
        }
    }
    
    public func configureViews() {
    }
}
