import UI
import UIKit

extension CreditOnboardingHeaderView.Layout {
    enum Size {
        static let advantageTitleIcon = 20
    }
    
    enum Font {
        static let paragraphAligment: NSTextAlignment = .center
        static let paragraphLineSpace: CGFloat = 5.0
    }
}

class CreditOnboardingHeaderView: UIView {
    private typealias Localizable = Strings.CreditOnboardingHeaderView
    fileprivate enum Layout { }
        
    private lazy var titleHeaderLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textAlignment, .center)
        label.text = Localizable.title
        return label
    }()
    
    private lazy var headerCardImage: UIImageView = {
        let image = UIImageView(image: Assets.iconUpgradeDebit.image)
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
        label.attributedText = scrollContentDescription()
        return label
    }()
    
    private lazy var advantagesTitleHolder = UIView()
    
    private lazy var advantagesTitleIcon = UIImageView(image: UI.Assets.icoArrowCircledGreen.image)
    
    private lazy var advantagesTitle: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textAlignment, .center)
            .with(\.textColor, Colors.branding400.color)
        label.text = Localizable.Advantage.title.uppercased()
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        configureViews()
        buildViewHierarchy()
        setupConstraints()
    }
    
    func scrollContentDescription() -> NSAttributedString {
        let text = Localizable.description
        let attText = text.attributedStringWithFont(primary: Typography.bodyPrimary().font(),
                                                    secondary: Typography.bodyPrimary(.highlight).font())
        attText.paragraph(aligment: Layout.Font.paragraphAligment, lineSpace: Layout.Font.paragraphLineSpace)
        attText.textColor(text: Localizable.description, color: Palette.ppColorGrayscale600.color)
        return attText
    }
}

extension CreditOnboardingHeaderView: ViewConfiguration {
    public func buildViewHierarchy() {
        advantagesTitleHolder.addSubviews(advantagesTitleIcon, advantagesTitle)
        addSubviews(titleHeaderLabel, headerCardImage, descriptionLabel, advantagesTitleHolder)
    }
    
    public func setupConstraints() {
        titleHeaderLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        headerCardImage.snp.makeConstraints {
            $0.top.equalTo(titleHeaderLabel.snp.bottom).offset(Spacing.base03)
            $0.centerX.equalToSuperview()
            $0.leading.equalToSuperview().inset(Spacing.base07)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(headerCardImage.snp.bottom).offset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        advantagesTitleHolder.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base06)
            $0.centerX.equalToSuperview()
            $0.bottom.equalToSuperview().inset(Spacing.base03)
        }
        
        advantagesTitle.snp.makeConstraints {
            $0.trailing.centerY.equalToSuperview()
            $0.leading.equalTo(advantagesTitleIcon.snp.trailing).offset(Spacing.base01)
        }
        
        advantagesTitleIcon.snp.makeConstraints {
            $0.width.height.equalTo(Layout.Size.advantageTitleIcon)
            $0.centerY.leading.equalToSuperview()
        }
    }
}
