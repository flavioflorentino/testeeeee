import UI
import UIKit

extension DebitOnboardingHeaderView.Layout {
    enum Font {
        static let descrition = UIFont.systemFont(ofSize: 16, weight: .regular)
        static let descritionHighlight = UIFont.systemFont(ofSize: 16, weight: .bold)
    }
    
    enum Size {
        static let headerHeight = 216
        static let headerImageHeight: CGFloat = 156
        static let headerImageHHalf: CGFloat = headerImageHeight / 2
        static let advantageTitleIcon = 20
    }
}

class DebitOnboardingHeaderView: UIView, ViewConfiguration {
    fileprivate typealias Localizable = Strings.DebitOnboardingHeaderView
    fileprivate enum Layout { }
    
    private lazy var headerBackground: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorCardGray500.color(withCustomDark: .ppColorGrayscale500)
        return view
    }()
    
    private lazy var headerTitle: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textAlignment, .center)
            .with(\.textColor, Colors.black.darkColor)
        label.text = Localizable.title
        return label
    }()
    
    private lazy var headerImage: UIImageView = {
        let image = UIImageView(image: UI.Assets.icoCardV1.image)
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    private lazy var headerDescription: UILabel = {
        let label = UILabel()
        let text = Localizable.Description.text
        label.attributedText = text.attributedStringWithFont(primary: Layout.Font.descrition,
                                                             secondary: Layout.Font.descritionHighlight)
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var advantagesTitleHolder = UIView()
    
    private lazy var advantagesTitleIcon = UIImageView(image: UI.Assets.icoArrowCircledGreen.image)
    
    private lazy var advantagesTitle: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textAlignment, .center)
            .with(\.textColor, Colors.branding400.color)
        label.text = Localizable.Advantage.title.uppercased()
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal func buildViewHierarchy() {
        advantagesTitleHolder.addSubviews(advantagesTitleIcon, advantagesTitle)
        addSubviews(headerBackground, headerTitle, headerImage, headerDescription, advantagesTitleHolder)
    }
    
    func setupConstraints() {
        headerBackground.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.height.equalTo(Layout.Size.headerHeight)
        }
        
        headerTitle.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base05)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        headerImage.snp.makeConstraints {
            $0.top.equalTo(headerBackground.snp.bottom).inset(Layout.Size.headerImageHHalf)
            $0.height.equalTo(Layout.Size.headerImageHeight)
            $0.centerX.equalToSuperview()
        }
        
        headerDescription.snp.makeConstraints {
            $0.top.equalTo(headerImage.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }

        advantagesTitleHolder.snp.makeConstraints {
            $0.top.equalTo(headerDescription.snp.bottom).offset(Spacing.base06)
            $0.centerX.equalToSuperview()
            $0.bottom.equalToSuperview().inset(Spacing.base05)
        }
        
        advantagesTitle.snp.makeConstraints {
            $0.trailing.centerY.equalToSuperview()
            $0.leading.equalTo(advantagesTitleIcon.snp.trailing).offset(Spacing.base01)
        }
        
        advantagesTitleIcon.snp.makeConstraints {
            $0.width.height.equalTo(Layout.Size.advantageTitleIcon)
            $0.centerY.leading.equalToSuperview()
        }
    }
}
