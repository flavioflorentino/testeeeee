import UIKit
import UI

private extension DebitToMultipleOnboardingHeaderView.Layout {
    enum Font {
        static let descrition = UIFont.systemFont(ofSize: 16, weight: .regular)
        static let descritionHighlight = UIFont.systemFont(ofSize: 16, weight: .bold)
    }
}

final class DebitToMultipleOnboardingHeaderView: UIView, ViewConfiguration {
    fileprivate typealias Localizable = Strings.DebitToMultipleOnboardingHeaderView
    fileprivate enum Layout {}
    
    private lazy var tittleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textAlignment, .center)
        label.text = Localizable.title
        return label
    }()

    private lazy var image: UIImageView = {
        let image = UIImageView(image: Assets.iconUpgradeDebit.image)
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        let text = Localizable.description
        label.attributedText = text.attributedStringWithFont(primary: Layout.Font.descrition,
                                                             secondary: Layout.Font.descritionHighlight)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var advantageLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
        label.text = Localizable.advantage
        return label
    }()
    
    init() {
         super.init(frame: .zero)
         buildLayout()
     }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal func buildViewHierarchy() {
        addSubviews(tittleLabel, image, descriptionLabel, advantageLabel)
    }
    
    internal func setupConstraints() {
        tittleLabel.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base04)
        }
        
        image.snp.makeConstraints {
            $0.top.equalTo(tittleLabel.snp.bottom).offset(Spacing.base02)
            $0.centerX.equalToSuperview()
            $0.leading.equalToSuperview().inset(Spacing.base04)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(image.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base04)
        }
        
        advantageLabel.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.bottom.equalToSuperview().inset(Spacing.base04)
        }
    }
}
