import UIKit
import UI

protocol BottomPinnedViewDelegate: AnyObject {
    func didPressConfirm()
}

fileprivate extension BottomPinnedView.Layout {
    enum Colors {
        static let buttonColor = UI.Colors.branding400.color
        static let shadowOpacity: Float = 0.1
    }
    
    enum Size {
        static let cornerRadius: CGFloat = 20
        static let shadowOffset = CGSize(width: 0, height: -8)
        static let shadowRadius: CGFloat = 6
    }
}

final class BottomPinnedView: UIView, ViewConfiguration {
    internal enum Layout {}

    weak var delegate: BottomPinnedViewDelegate?

    private lazy var shadowView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = Layout.Size.cornerRadius
        return view
    }()
    
    private lazy var warningMessage: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle()).with(\.textColor, .grayscale500()).with(\.textAlignment, .center)
        label.text = Strings.DynamicOnboarding.warning
        return label
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didPressConfirm), for: .touchUpInside)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal func buildViewHierarchy() {
        addSubview(shadowView)
        shadowView.addSubviews(warningMessage, confirmButton)
    }
    
    internal func setupConstraints() {
        shadowView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        warningMessage.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.left.right.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(confirmButton.compatibleSafeArea.top).offset(-Spacing.base01)
        }
        confirmButton.snp.makeConstraints {
            $0.top.equalTo(warningMessage.compatibleSafeArea.bottom).inset(Spacing.base01)
            $0.left.right.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base01)
        }
    }
    
    func setupText(_ text: String) {
        confirmButton.setTitle(text, for: .normal)
    }
    
    func hideWarningMessage() {
        warningMessage.isHidden = true
    }
    
    func configureViews() {
        layer.masksToBounds = false
    }
    
    @objc
    private func didPressConfirm() {
        delegate?.didPressConfirm()
    }
}
