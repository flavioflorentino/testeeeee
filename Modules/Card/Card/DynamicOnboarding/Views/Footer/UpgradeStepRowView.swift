import UIKit
import UI

extension UpgradeStepRowView.Layout {
    enum Size {
        static let imageSize = CGSize(width: 24, height: 24)
    }
}

final class UpgradeStepRowView: UIView, ViewConfiguration {
    internal enum Layout {}
    
    private lazy var numberLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
        label.textAlignment = .center
        label.clipsToBounds = true
        label.layer.cornerRadius = 12
        label.layer.borderWidth = 1
        label.layer.borderColor = Colors.neutral600.color.cgColor
        label.textColor = Colors.neutral600.color
        return label
    }()
    
    private lazy var stepLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
        label.textColor = Colors.grayscale600.color
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal func buildViewHierarchy() {
        addSubviews(numberLabel, stepLabel)
    }
    
    internal func setupConstraints() {
        numberLabel.snp.makeConstraints {
            $0.leading.equalToSuperview()
            $0.centerY.equalTo(stepLabel)
            $0.size.equalTo(Layout.Size.imageSize)
        }
        stepLabel.snp.makeConstraints {
            $0.trailing.equalToSuperview().offset(-Spacing.base01)
            $0.leading.equalTo(numberLabel.snp.trailing).offset(Spacing.base01)
            $0.top.bottom.equalToSuperview()
        }
    }
    
    func configure(row: Int, text: String) {
        numberLabel.text = String(row)
        stepLabel.text = text
    }
}
