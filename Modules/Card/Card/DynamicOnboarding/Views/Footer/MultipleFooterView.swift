import UIKit
import UI

protocol MultipleFooterViewDelegate: HelpDelegate {
    func didTapRulesButton()
}

extension MultipleFooterView.Layout {
    enum Size {
        static let insets = UIEdgeInsets(top: 24.0, left: 0, bottom: 24.0, right: 0)
        static let lineHeight: CGFloat = 20
    }
}

final class MultipleFooterView: UIView, ViewConfiguration {
    typealias Localizable = Strings.MultipleFooterView
    weak var delegate: MultipleFooterViewDelegate?
    
    fileprivate enum Layout {}
    
    private lazy var creditAnalysisLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textAlignment, .center)
        label.text = Localizable.creditAnalysis
        return label
    }()
    
    private lazy var helpTextView: UITextView = {
        let textView = UITextView()
        textView.tintColor = Colors.grayscale200.color
        textView.backgroundColor = .clear
        textView.isScrollEnabled = false
        textView.isEditable = false
        textView.contentSize = .zero
        textView.attributedText = setupHelp()
        setupTapGesture(in: textView, selector: #selector(helpTextViewTapped))
        return textView
    }()
    
    private lazy var rulesTextView: UITextView = {
        let textView = UITextView()
        textView.backgroundColor = Colors.grayscale700.lightColor
        textView.isScrollEnabled = false
        textView.isEditable = false
        textView.textContainerInset = Layout.Size.insets
        textView.attributedText = setupRules()
        setupTapGesture(in: textView, selector: #selector(rulesTextViewTapped))
        return textView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal func buildViewHierarchy() {
        addSubviews(creditAnalysisLabel, helpTextView, rulesTextView)
    }
    
    internal func setupConstraints() {
        creditAnalysisLabel.snp.makeConstraints {
            $0.leading.top.trailing.equalToSuperview().inset(Spacing.base02)
        }
        helpTextView.snp.makeConstraints {
            $0.top.equalTo(creditAnalysisLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        rulesTextView.snp.makeConstraints {
            $0.top.equalTo(helpTextView.snp.bottom).offset(Spacing.base02)
            $0.leading.bottom.trailing.equalToSuperview()
        }
    }
    
    @objc
    private func rulesTextViewTapped() {
        delegate?.didTapRulesButton()
    }
    
    @objc
    private func helpTextViewTapped() {
        delegate?.didTapHelpButton()
    }
    
    private func setupTapGesture(in textView: UITextView, selector: Selector) {
        let tapGesture = UITapGestureRecognizer(target: self, action: selector)
        tapGesture.delegate = self
        textView.addGestureRecognizer(tapGesture)
    }
    
    private func setupHelp() -> NSAttributedString {
        Localizable.Help.message.attributedStringWith(
            normalFont: Typography.bodySecondary().font(),
            highlightFont: Typography.bodySecondary(.highlight).font(),
            normalColor: Colors.black.color,
            highlightColor: Colors.branding400.color,
            textAlignment: .center,
            underline: true,
            lineHeight: Layout.Size.lineHeight
        )
    }
    
    private func setupRules() -> NSAttributedString {
        Localizable.Rules.button.attributedStringWith(
            normalFont: Typography.bodySecondary().font(),
            highlightFont: Typography.linkSecondary().font(),
            normalColor: Colors.white.lightColor,
            highlightColor: Colors.white.lightColor,
            textAlignment: .center,
            underline: true
        )
    }
}

extension MultipleFooterView: UIGestureRecognizerDelegate {
    public func gestureRecognizer(
        _ gestureRecognizer: UIGestureRecognizer,
        shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer
    ) -> Bool {
        true
    }
}
