import UIKit
import UI

final class UpgradeFooterView: UIView, ViewConfiguration {
    typealias Localizable = Strings.UpgradeFooterView
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
        label.text = Localizable.Steps.title
        return label
    }()
    
    private let steps = [
        Localizable.Steps.first,
        Localizable.Steps.second,
        Localizable.Steps.third
    ]
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubviews(titleLabel, stackView)
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview().inset(Spacing.base04)
        }
        stackView.snp.makeConstraints {
            $0.leading.trailing.bottom.equalToSuperview().inset(Spacing.base04)
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
        }
    }
    
    func configureViews() {
        steps.enumerated().forEach {
            let view = UpgradeStepRowView()
            view.configure(row: $0.offset, text: $0.element)
            stackView.addArrangedSubview(view)
        }
    }
}
