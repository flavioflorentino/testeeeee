import UIKit
import UI

protocol HelpDelegate: AnyObject {
    func didTapHelpButton()
}

protocol DebitFooterViewDelegate: HelpDelegate {}

final class DebitFooterView: UIView, ViewConfiguration {
    private typealias Localizable = Strings.DebitFooterView
    weak var delegate: DebitFooterViewDelegate?
    
    private lazy var helpButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(didTapHelpButton), for: .touchUpInside)
        button.titleLabel?.numberOfLines = 0
        button.setAttributedTitle(buildHelp(), for: .normal)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubview(helpButton)
    }
    
    func setupConstraints() {
        helpButton.snp.makeConstraints {
            $0.top.equalToSuperview().offset(12)
            $0.left.right.equalToSuperview().inset(12)
            $0.bottom.equalToSuperview().offset(-24)
        }
    }
    
    private func buildHelp() -> NSAttributedString {
        Localizable.Help.text.attributedStringWith(
            normalFont: Typography.bodySecondary().font(),
            highlightFont: Typography.bodySecondary(.highlight).font(),
            normalColor: Colors.grayscale700.color,
            highlightColor: Colors.branding400.color,
            textAlignment: .center,
            underline: true
        )
    }
    
    @objc
    private func didTapHelpButton() {
        delegate?.didTapHelpButton()
    }
}
