import CustomerSupport
import SafariServices
import UIKit

public enum DynamicOnboardingAction: Equatable {
    case close
    case confirm
    case presentScreen(url: URL)
    case openZendesk(faqOption: FAQOptions)
}

public protocol DynamicOnboardingCoordinatorDelegate: AnyObject {
    func onRegistrationDidRequest()
    func onboardingDidClose()
}

public protocol DynamicOnboardingCoordinating: AnyObject {
    var viewController: (UIViewController & DynamicOnboardingDisplaying)? { get set }
    var delegate: DynamicOnboardingCoordinatorDelegate? { get set }
    func perform(action: DynamicOnboardingAction)
}

final class DynamicOnboardingCoordinator: NSObject {
    weak var viewController: (UIViewController & DynamicOnboardingDisplaying)?
    weak var delegate: DynamicOnboardingCoordinatorDelegate?
    
    private func makeSafariViewController(url: URL) -> SFSafariViewController {
        let safariVC = SFSafariViewController(url: url)
        if #available(iOS 11.0, *) {
            safariVC.configuration.barCollapsingEnabled = true
            safariVC.configuration.entersReaderIfAvailable = true
        }
        return safariVC
    }
}

// MARK: - DynamicOnboardingCoordinating
extension DynamicOnboardingCoordinator: DynamicOnboardingCoordinating {
    func perform(action: DynamicOnboardingAction) {
        switch action {
        case .close:
            delegate?.onboardingDidClose()
        case .confirm:
            delegate?.onRegistrationDidRequest()
        case let .presentScreen(url):
            let safariVC = makeSafariViewController(url: url)
            viewController?.present(safariVC, animated: true)
        case .openZendesk(let faqOption):
            let faq = FAQFactory.make(option: faqOption)
            viewController?.navigationController?.pushViewController(faq, animated: true)
        }
    }
}
