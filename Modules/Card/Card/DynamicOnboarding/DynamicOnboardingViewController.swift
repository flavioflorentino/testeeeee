import UI
import UIKit

public protocol DynamicOnboardingDisplaying: AnyObject {
    func displayRows(with rows: [DynamicOnboardingTypeRows])
    func displayPinnedView(with text: String)
    func hideWarningMessage()
}

final class DynamicOnboardingViewController: ViewController<DynamicOnboardingInteracting, DynamicOnboardingRootView> {
    override func viewDidLoad() {
        super.viewDidLoad()
        addCloseButton()
        interactor.setScreenData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        interactor.trackScreenView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBarAppearance()
    }
    
    private func updateNavigationBarAppearance() {
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.backgroundColor = Colors.backgroundPrimary.color
        navigationController?.navigationBar.isTranslucent = true
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
            navigationController?.navigationItem.largeTitleDisplayMode = .always
        }
        
        let backButton = UIBarButtonItem()
        backButton.title = String()
        navigationItem.backBarButtonItem = backButton
        
        navigationItem.hidesBackButton = true
    }
    
    override func configureViews() {
        super.configureViews()
        
        rootView.delegate = self
    }
    
    private func addCloseButton() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: Assets.icoClose.image,
            style: .plain,
            target: self,
            action: #selector(didTapClose)
        )
    }
    
    @objc
    private func didTapClose() {
        interactor.didClose()
    }
}

// MARK: - DynamicOnboardingDisplaying
extension DynamicOnboardingViewController: DynamicOnboardingDisplaying {
    func displayPinnedView(with text: String) {
        rootView.displayPinnedView(with: text)
    }
    
    func hideWarningMessage() {
        rootView.hideWarningMessage()
    }
    
    func displayRows(with rows: [DynamicOnboardingTypeRows]) {
        rootView.render(rows)
    }
}

extension DynamicOnboardingViewController: DynamicOnboardingRootViewDelegate {
    func didPressRules() {
        interactor.didPressRules()
    }
    
    func didPressHelp() {
        interactor.didPressHelp()
    }
    
    func didPressConfirm() {
        interactor.didPressConfirm()
    }
    
    public func didInteractWithAdvantages() {
        interactor.didInteractWithAdvantages()
    }
}
