import CustomerSupport
import FeatureFlag
import Foundation

public protocol DynamicOnboardingInteracting: AnyObject {
    func didClose()
    func setScreenData()
    func didInteractWithAdvantages()
    func didPressConfirm()
    func didPressHelp()
    func didPressRules()
    func trackScreenView()
}

final class DynamicOnboardingInteractor {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    private let analytics: OnboardingAnalytics
    
    private let presenter: DynamicOnboardingPresenting
    private let type: OnboardingTypeFlow
    private var didScrollAdvantages = false
    
    var helpLink: URL? {
        switch type {
        case .debit:
            return URL(string: dependencies.featureManager.text(.linkFaqDebitCardOnboarding))
        case .multiple:
            return URL(string: dependencies.featureManager.text(.linkFaqMultipleCardOnboarding))
        case .debitToMultiple:
            return nil
        }
    }
    
    var helpOption: FAQOptions? {
        switch type {
        case .debit:
            return CardOnboardingMultipleCustomer().option()
        case .multiple:
            return CardOnboardingDebitCustomer().option()
        case .debitToMultiple:
            return nil
        }
    }
    
    init(presenter: DynamicOnboardingPresenting, type: OnboardingTypeFlow, analytics: OnboardingAnalytics, dependencies: Dependencies) {
        self.presenter = presenter
        self.type = type
        self.analytics = analytics
        self.dependencies = dependencies
    }
}

// MARK: - DynamicOnboardingInteracting
extension DynamicOnboardingInteractor: DynamicOnboardingInteracting {
    func didClose() {
        analytics.close()
        presenter.didNextStep(action: .close)
    }
    
    func didPressRules() {
        if case .multiple = type, let url = URL(string: dependencies.featureManager.text(.linkRulesMultipleCardOnboarding)) {
            analytics.seeRules()
            presenter.didNextStep(action: .presentScreen(url: url))
        }
    }
    
    func didPressHelp() {
        analytics.helpCenter()
        if let option = helpOption {
            presenter.didNextStep(action: .openZendesk(faqOption: option))
        } else if let url = helpLink {
            presenter.didNextStep(action: .presentScreen(url: url))
        }
    }
    
    func didPressConfirm() {
        analytics.requestCard()
        presenter.didNextStep(action: .confirm)
    }
    
    func didInteractWithAdvantages() {
        guard !didScrollAdvantages else {
            return
        }
        analytics.scrollBenefits()
        didScrollAdvantages = true
    }
    
    func setScreenData() {
        presenter.setScreenData(with: type, showCashout: dependencies.featureManager.isActive(.opsCardOnboardingCashout))
        analytics.viewDidLoad()
    }
    
    func trackScreenView() {
        analytics.viewDidAppear()
    }
}
