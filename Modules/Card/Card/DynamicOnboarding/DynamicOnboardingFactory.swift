import UIKit

public enum DynamicOnboardingFactory {
    public static func make(with type: OnboardingTypeFlow, delegate: DynamicOnboardingCoordinatorDelegate?) -> UIViewController {
        let dependencies = DependencyContainer()
        let coordinator: DynamicOnboardingCoordinating = DynamicOnboardingCoordinator()
        let presenter: DynamicOnboardingPresenting = DynamicOnboardingPresenter(coordinator: coordinator, dependencies: dependencies)
        
        let analytics: OnboardingAnalytics
        switch type {
        case .multiple, .debit:
            analytics = OnboardingCreditAnalytics(dependencies: dependencies)
        case .debitToMultiple:
            analytics = OnboardingUpgradeAnalytics(dependencies: dependencies)
        }
        let interactor = DynamicOnboardingInteractor(presenter: presenter,
                                                     type: type,
                                                     analytics: analytics,
                                                     dependencies: dependencies)
        let viewController = DynamicOnboardingViewController(interactor: interactor)

        coordinator.viewController = viewController
        coordinator.delegate = delegate
        presenter.viewController = viewController

        return viewController
    }
}
