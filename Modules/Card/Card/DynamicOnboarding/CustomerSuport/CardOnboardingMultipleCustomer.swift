import CustomerSupport
import Foundation

struct CardOnboardingMultipleCustomer: CustomerOptions {
    let development: FAQOptions
    let production: FAQOptions
    let path: PathFAQ
    
    init() {
        development = .home
        production = .category(id: "360003297112")
        path = .activation
    }
}
