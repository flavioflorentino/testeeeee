import AnalyticsModule
import FeatureFlag
import Foundation

struct OnboardingUpgradeAnalytics: OnboardingAnalytics {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func viewDidLoad() {
        dependencies.analytics.log(DebitUpgradeAnalytics.viewDidLoadState)
    }
    
    func scrollBenefits() {
        dependencies.analytics.log(DebitUpgradeAnalytics.interactWithAdvantages)
    }
    
    func requestCard() {
        dependencies.analytics.log(DebitUpgradeAnalytics.confirmUpgrade)
    }
    
    func close() {
        dependencies.analytics.log(DebitUpgradeAnalytics.close)
    }
}
