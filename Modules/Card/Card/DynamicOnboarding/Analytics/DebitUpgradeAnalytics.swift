import AnalyticsModule

public enum DebitUpgradeAnalytics: AnalyticsKeyProtocol {
    case viewDidLoadState
    case interactWithAdvantages
    case confirmUpgrade
    case close
    
    private var properties: [String: Any] {
        switch self {
        case .viewDidLoadState:
            return ["action": "st-new-webview-multiple"]
        case .interactWithAdvantages:
            return ["action": "act-scroll-benefits"]
        case .confirmUpgrade:
            return ["action": "act-update-to-credit"]
        case .close:
            return ["action": "act-webview-close"]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }
    
    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PicPay Card - Request - Offer Webview", properties: properties, providers: providers)
    }
}
