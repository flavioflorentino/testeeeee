import AnalyticsModule
import FeatureFlag
import Foundation

struct OnboardingCreditAnalytics: OnboardingAnalytics {
    let testType: OnboardingABTestingState
    
    typealias Dependencies = HasFeatureManager & HasAnalytics
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
        
        let buttonTestFlag = dependencies.featureManager.isActive(.experimentCreditCardOfferButtonBool)
        self.testType = buttonTestFlag ? .b : .a
    }
    
    func viewDidLoad() {
        dependencies.analytics.log(OnboardingEvent(testType: testType).sendEvent(action: .viewDidLoadState))
    }
    
    func viewDidAppear() {
        dependencies.analytics.log(OnboardingEvent(testType: testType).sendEvent(action: .viewDidAppear))
    }
    
    func scrollBenefits() {
        dependencies.analytics.log(OnboardingEvent(testType: testType).sendEvent(action: .interactWithAdvantages))
    }
    
    func requestCard() {
        dependencies.analytics.log(OnboardingEvent(testType: testType).sendEvent(action: .interactWithAdvantages))
    }
    
    func helpCenter() {
        dependencies.analytics.log(OnboardingEvent(testType: testType).sendEvent(action: .interactWithAdvantages))
    }
    
    func seeRules() {
        dependencies.analytics.log(OnboardingEvent(testType: testType).sendEvent(action: .interactWithAdvantages))
    }
    
    func close() {
        dependencies.analytics.log(OnboardingEvent(testType: testType).sendEvent(action: .interactWithAdvantages))
    }
}
