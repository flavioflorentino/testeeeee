public protocol OnboardingAnalytics {
    func viewDidLoad()
    func viewDidAppear()
    func scrollBenefits()
    func requestCard()
    func helpCenter()
    func seeRules()
    func close()
}

extension OnboardingAnalytics {
    func viewDidLoad() {} // this function is implemented by the protocol subscribers
    func viewDidAppear() {}
    func helpCenter() {}
    func seeRules() {}
}
