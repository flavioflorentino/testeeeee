import AnalyticsModule
import Foundation

struct OnboardingEvent {
    let testType: OnboardingABTestingState
    
    enum OnboardingEventsType: String {
        case viewDidAppear
        case viewDidLoadState = "st-new-webview"
        case openWebView = "act-open-webview"
        case requestCard = "act-request-card"
        case helpCenter = "act-help-center"
        case webviewClose = "act-webview-close"
        case seeRules = "act-see-rules"
        case interactWithAdvantages = "act-scroll-benefits"
    }
    
    private var state: String {
        switch testType {
        case .a:
            return "st-abtest-a"
        case .b:
            return "st-abtest-b"
        }
    }
    
    private var name: String {
        "PicPay Card - Request - Offer Webview"
    }
    
    private func providers(_ event: OnboardingEventsType) -> [AnalyticsProvider] {
        switch event {
        case .viewDidAppear:
            return [.appsFlyer]
        default:
            return [.firebase, .mixPanel]
        }
    }
    
    private func properties(_ event: OnboardingEventsType) -> [String: Any] {
        switch event {
        case .viewDidAppear:
            return ["state": state]
        default:
            return ["action": event.rawValue, "state": state]
        }
    }

    func sendEvent(action: OnboardingEventsType) -> AnalyticsKeyProtocol {
        AnalyticsEvent(name, properties: properties(action), providers: providers(action))
    }
}
