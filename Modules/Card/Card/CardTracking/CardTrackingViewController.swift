import UIKit
import SnapKit
import UI

protocol CardTrackingDisplay: AnyObject {
    func updateHistory(history: [CardTrackingHistoryItem], isArrived: Bool, cardType: CardTrackingType)
    func showEstimatedTime(attributedText: NSAttributedString)
    func showErrorButton()
    func showReceivedCardButton()
    func displayInitialSetup(helpMessage: NSMutableAttributedString)
}

private extension CardTrackingViewController.Layout {
    enum Spacing {
        static let topLogoImage: CGFloat = 44
    }
    enum Size {
        static let dividerHeight: CGFloat = 1.0
        static let logoImageWidth: CGFloat = 111.0
        static let logoImageHeight: CGFloat = 103.9
        static let close = CGSize(width: Sizing.base03, height: Sizing.base03)
    }
    enum Length {
        static let numberOfLinesEstimatedTime = 2
    }
}

public final class CardTrackingViewController: ViewController<CardTrackingViewModelInputs, UIView> {
    fileprivate enum Layout {}
    
    private lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.bounces = false
        scroll.backgroundColor = Colors.grayscale050.color
        return scroll
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.backgroundPrimary.color
        return view
    }()
    
    private lazy var topView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var receivedCardButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Strings.CardTracking.ReceivedButton.title, for: .normal)
        button.isHidden = true
        button.addTarget(self, action: #selector(receivedCardAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var errorButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(SecondaryButtonStyle())
        button.setTitle(Strings.CardTracking.ErrorButton.title, for: .normal)
        button.isHidden = true
        button.addTarget(self, action: #selector(deliveryErrorAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var bottomView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.spacing = .zero
        view.alignment = .top
        return view
    }()
    
    private lazy var bottomViewTopConstraint: NSLayoutConstraint = {
        let constraint = bottomView.topAnchor.constraint(equalTo: topView.bottomAnchor)
        constraint.constant = Spacing.base05
        return constraint
    }()
    
    private lazy var estimatedTimeView: UIView = {
        let view = UIView()
        view.isHidden = true
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var dividerView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale100.color
        return view
    }()
    
    private lazy var estimatedTimeLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textAlignment, .center)
            .with(\.textColor, .grayscale600())
        label.numberOfLines = Layout.Length.numberOfLinesEstimatedTime
        label.text = Strings.CardTracking.title
        return label
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textAlignment, .center)
        label.text = Strings.CardTracking.title
        return label
    }()
    
    private lazy var logoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.icoLogoTracking.image
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var helpView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale050.color
        return view
    }()
    
    private lazy var helpLabel: UITextView = {
        let text = UITextView()
        text.font = Typography.bodyPrimary().font()
        text.textAlignment = .center
        text.textColor = Colors.grayscale700.color
        text.isScrollEnabled = false
        text.backgroundColor = .clear
        text.linkTextAttributes = [NSAttributedString.Key.foregroundColor: Colors.branding400.color]
        text.delegate = self
        return text
    }()
        
    private var bottomSafeAreaInsets: CGFloat {
        if #available(iOS 11.0, *) {
            return scrollView.safeAreaInsets.bottom
        }
        return 0
    }
    
    private var contentSizeOffSetScrollView: CGFloat {
        (scrollView.frame.height - scrollView.contentSize.height) - bottomSafeAreaInsets
    }
    
    private lazy var cardTrackingHistoryViewController = CardTrackingHistoryViewController()
    
    override public func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        
        contentView.addSubview(topView)
        contentView.addSubview(bottomView)
        
        topView.addSubview(logoImageView)
        topView.addSubview(titleLabel)
        topView.addSubview(cardTrackingHistoryViewController.view)
        
        estimatedTimeView.addSubview(dividerView)
        estimatedTimeView.addSubview(estimatedTimeLabel)
        
        bottomView.addArrangedSubview(receivedCardButton)
        bottomView.addArrangedSubview(errorButton)
        bottomView.addArrangedSubview(estimatedTimeView)
      
        helpView.addSubview(helpLabel)
        bottomView.addArrangedSubview(helpView)
    }
    
    override public func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.top.leading.bottom.trailing.width.equalToSuperview()
        }
        
        contentView.snp.makeConstraints {
            $0.top.bottom.leading.trailing.width.equalToSuperview()
        }
        
        contentView.addConstraint(bottomViewTopConstraint)
        
        topView.snp.makeConstraints {
            $0.leading.top.trailing.width.equalToSuperview()
        }
        
        bottomView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        
        receivedCardButton.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        errorButton.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        estimatedTimeView.snp.makeConstraints {
            $0.leading.trailing.width.equalToSuperview()
        }
        
        helpView.snp.makeConstraints {
            $0.leading.trailing.width.equalToSuperview()
            $0.height.equalTo(Sizing.base09)
        }
        setupStructureConstraints()
    }
    
    private func setupStructureConstraints() {
        logoImageView.snp.makeConstraints {
            $0.top.equalTo(Layout.Spacing.topLogoImage)
            $0.centerX.equalToSuperview()
            $0.width.equalTo(Layout.Size.logoImageWidth)
            $0.height.equalTo(Layout.Size.logoImageHeight)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(logoImageView.snp.bottom).offset(Spacing.base02)
            $0.height.equalTo(Sizing.base05)
            $0.centerX.equalToSuperview()
            $0.leading.equalTo(topView.snp.leading).offset(Spacing.base00)
            $0.trailing.equalTo(topView.snp.trailing).offset(-Spacing.base00)
        }
        
        cardTrackingHistoryViewController.view.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base05)
            $0.leading.trailing.bottom.width.equalToSuperview()
        }
        
        dividerView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base03)
            $0.trailing.equalToSuperview().offset(-Spacing.base03)
            $0.height.equalTo(Layout.Size.dividerHeight)
        }
        
        estimatedTimeLabel.snp.makeConstraints {
            $0.top.equalTo(dividerView.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base05)
            $0.trailing.equalToSuperview().offset(-Spacing.base05)
            $0.bottom.equalToSuperview()
        }
        
        helpLabel.snp.makeConstraints {
            $0.leading.trailing.centerY.equalToSuperview()
        }
    }
    
    override public func configureViews() {
        viewModel.configureViews()
    }
    
    override public func configureStyles() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            isModalInPresentation = true
        }
        addCloseButton()
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateBottomViewPosition()
    }
    
    private func updateBottomViewPosition() {
        if contentSizeOffSetScrollView > 0 {
            bottomViewTopConstraint.constant += contentSizeOffSetScrollView
            view.layoutIfNeeded()
        }
    }
    
    private func addCloseButton() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: Assets.icoClose.image,
            style: .plain,
            target: self,
            action: #selector(closePressed)
        )
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    @objc
    private func closePressed() {
        viewModel.didTapCloseButton()
    }
}

@objc
extension CardTrackingViewController {
    func receivedCardAction() {
        viewModel.didTapCardArrived()
    }
    
    func deliveryErrorAction() {
        viewModel.didTapErrorButton()
    }
}

// MARK: CardTrackingDisplay
extension CardTrackingViewController: CardTrackingDisplay {
    func updateHistory(history: [CardTrackingHistoryItem], isArrived: Bool, cardType: CardTrackingType) {
        cardTrackingHistoryViewController.update(history: history, isArrived: isArrived, cardType: cardType)
    }
    
    func showEstimatedTime(attributedText: NSAttributedString) {
        bottomView.setSpacing(Spacing.base02, after: estimatedTimeView)
        estimatedTimeLabel.attributedText = attributedText
        estimatedTimeView.isHidden = false
    }
    
    func showErrorButton() {
        bottomView.setSpacing(Spacing.base04, after: errorButton)
        errorButton.isHidden = false
    }
    
    func showReceivedCardButton() {
        bottomView.setSpacing(Spacing.base04, after: receivedCardButton)
        receivedCardButton.isHidden = false
    }
    
    func displayInitialSetup(helpMessage: NSMutableAttributedString) {
        helpLabel.attributedText = helpMessage
    }
}

extension CardTrackingViewController: UITextViewDelegate {
    public func textView(
        _ textView: UITextView,
        shouldInteractWith URL: URL,
        in characterRange: NSRange,
        interaction: UITextItemInteraction
    ) -> Bool {
        viewModel.openFAQ()
        return false
    }
    
    public func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        false
    }
}

extension CardTrackingViewController: StyledNavigationDisplayable {
    public var navigationBarStyle: DefaultNavigationStyle {
        ClearNavigationStyle(prefersLargeTitle: false)
    }
}
