import Foundation

public enum CardTrackingFactory {
    public static func make(cardTrackingHistory: CardTrackingHistory, cardHistoryOutput: @escaping (CardHistoryOutput) -> Void) -> CardTrackingViewController {
        let container = DependencyContainer()
        let coordinator: CardTrackingCoordinating = CardTrackingCoordinator(coordinadorOutput: cardHistoryOutput)
        let presenter: CardTrackingPresenting = CardTrackingPresenter(coordinator: coordinator)
        let viewModel = CardTrackingViewModel(cardTrackingHistory: cardTrackingHistory, presenter: presenter, dependencies: container)
        let viewController = CardTrackingViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
