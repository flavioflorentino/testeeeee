import Foundation

enum CardArrivedFactory {
    static func make(coordinatorOutput: @escaping (CardArrivedCoordinatorOutput) -> Void, cardType: CardTrackingType) -> CardArrivedViewController {
        let container = DependencyContainer()
        let coordinator: CardArrivedCoordinating = CardArrivedCoordinator(coordinadorOutput: coordinatorOutput)
        let presenter: CardArrivedPresenting = CardArrivedPresenter(coordinator: coordinator)
        let viewModel = CardArrivedViewModel(presenter: presenter, dependencies: container, cardType: cardType)
        let viewController = CardArrivedViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
