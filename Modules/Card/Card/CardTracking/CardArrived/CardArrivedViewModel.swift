import AnalyticsModule
import FeatureFlag
import Foundation

protocol CardArrivedViewModelInputs: AnyObject {
    func setupInitialInfo()
    func close()
    func confirm()
    func openFAQ()
}

final class CardArrivedViewModel {
    typealias Dependencies = HasAnalytics & HasFeatureManager
    private let dependencies: Dependencies

    private let presenter: CardArrivedPresenting
    private let cardType: CardTrackingType

    init(presenter: CardArrivedPresenting, dependencies: Dependencies, cardType: CardTrackingType) {
        self.presenter = presenter
        self.dependencies = dependencies
        self.cardType = cardType
    }
}

// MARK: - CardArrivedViewModelInputs
extension CardArrivedViewModel: CardArrivedViewModelInputs {
    func setupInitialInfo() {
        presenter.setupInitialInfo()
    }
    
    func close() {
        presenter.didNextStep(action: .close)
    }
    
    func confirm() {
        presenter.didNextStep(action: .confirm)
        dependencies.analytics.log(CardTrackingEvent.didTapConfirm(type: cardType))
    }
    
    func openFAQ() {
        dependencies.analytics.log(CardTrackingEvent.didTapHelpCenter(type: cardType))
        if let url = URL(string: dependencies.featureManager.text(.articleActivateCard)) {
            presenter.didNextStep(action: .openHelpCenter(url: url))
        }
    }
}
