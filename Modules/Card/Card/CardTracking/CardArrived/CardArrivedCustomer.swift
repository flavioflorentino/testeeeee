import CustomerSupport
import Foundation

struct CardArrivedCustomer: CustomerOptions {
    let development: FAQOptions
    let production: FAQOptions
    let path: PathFAQ
    
    init() {
        development = .home
        production = .article(id: "360043766252")
        path = .activation
    }
}
