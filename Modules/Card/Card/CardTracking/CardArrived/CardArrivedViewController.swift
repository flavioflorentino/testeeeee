import UIKit
import UI

protocol CardArrivedDisplay: AnyObject {
    func displayInitialSetup(subTitle: NSMutableAttributedString, helpMessage: NSMutableAttributedString)
}

final class CardArrivedViewController: ViewController<CardArrivedViewModelInputs, UIView> {
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .xLarge))
        label.text = Strings.CardArrived.title
        label.textAlignment = .center
        return label
    }()
    
    private lazy var cardImage: UIImageView = {
       let imageView = UIImageView()
        imageView.image = Assets.pinCard.image
        return imageView
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
        label.textAlignment = .center
        return label
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Strings.CardArrived.Confirm.title, for: .normal)
        button.addTarget(self, action: #selector(nextPressed), for: .touchUpInside)
        return button
    }()
    
    private lazy var helpView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale050.color
        return view
    }()
    
    private lazy var helpLabel: UITextView = {
        let text = UITextView()
        text.font = Typography.bodyPrimary().font()
        text.textAlignment = .center
        text.textColor = Colors.grayscale700.color
        text.isScrollEnabled = false
        text.backgroundColor = .clear
        text.linkTextAttributes = [NSAttributedString.Key.foregroundColor: Colors.branding400.color]
        text.delegate = self
        return text
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.setupInitialInfo()
        if #available(iOS 13.0, *) {
            isModalInPresentation = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBarAppearance()
    }
    
    private func updateNavigationBarAppearance() {
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.isTranslucent = true
        addCloseButton()
    }
    
    private func addCloseButton() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: Assets.icoClose.image,
            style: .plain,
            target: self,
            action: #selector(closePressed)
        )
    }

    override func buildViewHierarchy() {
        helpView.addSubview(helpLabel)
        view.addSubviews(titleLabel, cardImage, descriptionLabel, confirmButton, helpView)
    }
    
    override func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(view.snp.top).offset(Spacing.base07)
            $0.leading.equalToSuperview().offset(Spacing.base05)
            $0.trailing.equalToSuperview().offset(-Spacing.base05)
        }
        
        cardImage.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base03)
            $0.centerX.equalToSuperview()
            $0.height.equalTo(cardImage.snp.width)
            $0.leading.equalToSuperview().offset(Spacing.base08)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(cardImage.snp.bottom).offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base05)
            $0.trailing.equalToSuperview().offset(-Spacing.base05)
        }

        confirmButton.snp.makeConstraints {
            $0.bottom.equalTo(helpView.snp.top).offset(-Spacing.base03)
            $0.leading.equalToSuperview().inset(Spacing.base06)
            $0.trailing.equalToSuperview().inset(Spacing.base06)
        }
        
        helpView.snp.makeConstraints {
            $0.bottom.equalTo(view.snp.bottom)
            $0.leading.equalTo(view.compatibleSafeArea.leading)
            $0.trailing.equalTo(view.compatibleSafeArea.trailing)
        }
        
        helpLabel.snp.makeConstraints {
            $0.bottom.equalTo(view.compatibleSafeArea.bottom)
            $0.leading.equalTo(view.compatibleSafeArea.leading)
            $0.trailing.equalTo(view.compatibleSafeArea.trailing)
            $0.top.equalTo(helpView.snp.top).offset(Spacing.base02)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    @objc
    private func nextPressed() {
        viewModel.confirm()
    }
    
    @objc
    private func closePressed() {
        viewModel.close()
    }
}

// MARK: CardArrivedDisplay
extension CardArrivedViewController: CardArrivedDisplay {
    func displayInitialSetup(subTitle: NSMutableAttributedString, helpMessage: NSMutableAttributedString) {
        descriptionLabel.attributedText = subTitle
        helpLabel.attributedText = helpMessage
    }
}

extension CardArrivedViewController: UITextViewDelegate {
    func textView(
        _ textView: UITextView,
        shouldInteractWith URL: URL,
        in characterRange: NSRange,
        interaction: UITextItemInteraction
    ) -> Bool {
        viewModel.openFAQ()
        return false
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        false
    }
}

extension CardArrivedViewController: StyledNavigationDisplayable {
    var navigationBarStyle: DefaultNavigationStyle {
        ClearNavigationStyle(prefersLargeTitle: false)
    }
}
