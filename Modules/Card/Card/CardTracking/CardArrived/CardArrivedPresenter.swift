import Foundation
import UI

protocol CardArrivedPresenting: AnyObject {
    var viewController: CardArrivedDisplay? { get set }
    func didNextStep(action: CardArrivedAction)
    func setupInitialInfo()
}

final class CardArrivedPresenter {
    private let coordinator: CardArrivedCoordinating
    weak var viewController: CardArrivedDisplay?
    
    init(coordinator: CardArrivedCoordinating) {
        self.coordinator = coordinator
    }
    
    private func getSubtitle() -> NSMutableAttributedString {
        let string = Strings.CardArrived.Subtitle.message
        let highlight = Strings.CardArrived.Subtitle.highlight
        
        let attributed = NSMutableAttributedString(string: string)
        attributed.font(text: string, font: Typography.bodyPrimary().font())
        attributed.font(text: highlight, font: Typography.bodyPrimary(.highlight).font())
        attributed.paragraph(aligment: .center, lineSpace: 3.0)
        
        return attributed
    }
}

// MARK: - CardArrivedPresenting
extension CardArrivedPresenter: CardArrivedPresenting {
    func didNextStep(action: CardArrivedAction) {
        coordinator.perform(action: action)
    }
    
    func setupInitialInfo() {
        viewController?.displayInitialSetup(subTitle: getSubtitle(), helpMessage: FooterLinkViewHelper.getHelpMessage())
    }
}
