import CustomerSupport
import SafariServices
import UIKit

enum CardArrivedAction {
    case confirm
    case close
    case openHelpCenter(url: URL)
}

protocol CardArrivedCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: CardArrivedAction)
}

final class CardArrivedCoordinator {
    weak var viewController: UIViewController?
    private let coordinadorOutput: (CardArrivedCoordinatorOutput) -> Void
    
    init(coordinadorOutput: @escaping (CardArrivedCoordinatorOutput) -> Void) {
        self.coordinadorOutput = coordinadorOutput
    }
}

// MARK: - CardArrivedCoordinating
extension CardArrivedCoordinator: CardArrivedCoordinating {
    func perform(action: CardArrivedAction) {
        switch action {
        case .confirm:
            coordinadorOutput(.unblockCard)
        case .close:
            coordinadorOutput(.close)
        case .openHelpCenter(let url):
            coordinadorOutput(.openHelpCenter(url: url))
        }
    }
}
