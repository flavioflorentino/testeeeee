import AnalyticsModule

public enum CardTrackingHistoryAnalyticsEvents: AnalyticsKeyProtocol {
    case cardTrackingEventsAction(CardTrackingEventsAction)
    case cardDebitCardTrackingEventsAction(CardTrackingEventsAction)
    private var name: String {
        switch self {
        case .cardTrackingEventsAction:
            return "Card Tracking - Status Update"
        case .cardDebitCardTrackingEventsAction:
            return "Card Tracking - Status Update - Debit"
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase, .appsFlyer]
    }
   
    private var properties: [String: Any] {
        switch self {
        case .cardDebitCardTrackingEventsAction(let action),
             .cardTrackingEventsAction(let action):
            return ["action": action.rawValue]
        }
    }
    
    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PicPay Card - \(name)", properties: properties, providers: providers)
    }
    
    public enum CardTrackingEventsAction: String {
        case requested = "st-card-requested"
        case production = "st-card-in-production"
        case carrier = "st-on-carrier"
        case route = "st-card-in-route"
        case delivered = "st-card-delivered"
        case helpCenter = "act-help-center"
        case deliveryError = "st-card-delivery-error"
        case received = "act-card-received"
        case notReceived = "act-card-not-received"
    }
}
