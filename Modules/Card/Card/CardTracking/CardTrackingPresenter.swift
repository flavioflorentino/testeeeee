import UIKit
import Foundation
import UI

protocol CardTrackingPresenting: AnyObject {
    var viewController: CardTrackingDisplay? { get set }
    func didNextStep(action: CardTrackingAction)
    func updateHistory(cardTrackingHistory: CardTrackingHistory, isArrived: Bool, cardType: CardTrackingType)
    func setupEstimatedTime(cardTrackingHistory: CardTrackingHistory)
    func setupErrorButton(cardTrackingHistory: CardTrackingHistory)
    func setupReceivedCardButton(cardTrackingHistory: CardTrackingHistory)
    func setupHelpMessage()
}

private extension CardTrackingPresenter.Layout {
    enum Font {
        static let highlightEstimatedTime = UIFont.systemFont(ofSize: 16, weight: .bold)
    }
    enum Lenght {
        static let helpMessageLineSpacing = CGFloat(3)
    }
}

public final class CardTrackingPresenter {
    fileprivate enum Layout {}

    private let coordinator: CardTrackingCoordinating
    weak var viewController: CardTrackingDisplay?
    
    init(coordinator: CardTrackingCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - CardTrackingPresenting
extension CardTrackingPresenter: CardTrackingPresenting {
    func setupEstimatedTime(cardTrackingHistory: CardTrackingHistory) {
        guard let estimatedTime = cardTrackingHistory.estimatedTime else {
            return
        }
        
        let fullString = "\(estimatedTime.text)\n\(estimatedTime.highlightText)"
        let attributedString = fullString.attributedString(
            highlights: [estimatedTime.highlightText],
            attrs: [.font: Layout.Font.highlightEstimatedTime]
        )
        
        viewController?.showEstimatedTime(attributedText: attributedString)
    }
    
    func updateHistory(cardTrackingHistory: CardTrackingHistory, isArrived: Bool, cardType: CardTrackingType) {
        viewController?.updateHistory(history: cardTrackingHistory.tracking, isArrived: isArrived, cardType: cardType)
    }
    
    func didNextStep(action: CardTrackingAction) {
        coordinator.perform(action: action)
    }
    
    func setupErrorButton(cardTrackingHistory: CardTrackingHistory) {
        if cardTrackingHistory.notArrivedButtonEnabled {
            viewController?.showErrorButton()
        }
    }
    
    func setupReceivedCardButton(cardTrackingHistory: CardTrackingHistory) {
        if cardTrackingHistory.confirmButtonEnabled == true {
            viewController?.showReceivedCardButton()
        }
    }
    
    func setupHelpMessage() {
        viewController?.displayInitialSetup(helpMessage: FooterLinkViewHelper.getHelpMessage())
    }
}
