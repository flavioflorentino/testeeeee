import AnalyticsModule
import FeatureFlag
import Foundation

public protocol CardTrackingViewModelInputs: AnyObject {
    func configureViews()
    func didTapCardArrived()
    func didTapErrorButton()
    func didTapCloseButton()
    func openFAQ()
}

public final class CardTrackingViewModel {
    typealias Dependencies = HasAnalytics & HasFeatureManager
    private let dependencies: Dependencies
    private let presenter: CardTrackingPresenting
    let cardTrackingHistory: CardTrackingHistory
    
    init(cardTrackingHistory: CardTrackingHistory, presenter: CardTrackingPresenting, dependencies: Dependencies) {
        self.cardTrackingHistory = cardTrackingHistory
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - CardTrackingViewModelInputs
extension CardTrackingViewModel: CardTrackingViewModelInputs {
    public func didTapCardArrived() {
        presenter.didNextStep(action: .cardArrived(type: cardTrackingHistory.cardType))
        trackingByCardType(action: .received)
    }
    
    public func didTapErrorButton() {
        presenter.didNextStep(action: .errorButton(type: cardTrackingHistory.cardType))
        if cardTrackingHistory.cardType == .multiple {
            dependencies.analytics.log(CardTrackingHistoryAnalyticsEvents.cardTrackingEventsAction(.deliveryError))
        } else {
            dependencies.analytics.log(CardTrackingHistoryAnalyticsEvents.cardTrackingEventsAction(.notReceived))
        }
    }
    
    public func didTapCloseButton() {
        presenter.didNextStep(action: .close)
    }
    
    public func configureViews() {
        presenter.updateHistory(cardTrackingHistory: cardTrackingHistory, isArrived: cardTrackingHistory.arrived, cardType: cardTrackingHistory.cardType)
        presenter.setupEstimatedTime(cardTrackingHistory: cardTrackingHistory)
        presenter.setupReceivedCardButton(cardTrackingHistory: cardTrackingHistory)
        presenter.setupErrorButton(cardTrackingHistory: cardTrackingHistory)
        presenter.setupHelpMessage()
        trackingStateCard()
    }
    
    public func openFAQ() {
        guard let url = URL(string: dependencies.featureManager.text(.faqDeliveryCardFailed)) else { return }
        presenter.didNextStep(action: .openHelpCenter(url: url))
        dependencies.analytics.log(CardTrackingHistoryAnalyticsEvents.cardTrackingEventsAction(.helpCenter))
    }
    
    internal func trackingByCardType(action: CardTrackingHistoryAnalyticsEvents.CardTrackingEventsAction) {
        guard cardTrackingHistory.cardType == .multiple else {
            dependencies.analytics.log(CardTrackingHistoryAnalyticsEvents.cardDebitCardTrackingEventsAction(action))
            return
        }
        dependencies.analytics.log(CardTrackingHistoryAnalyticsEvents.cardTrackingEventsAction(action))
    }
   
    internal func trackingStateCard() {
        guard let action = takeStateCard() else {
            return
        }
       
        trackingByCardType(action: action)
    }
    
    internal func takeStateCard() -> CardTrackingHistoryAnalyticsEvents.CardTrackingEventsAction? {
        switch cardTrackingHistory.currentStep {
        case .requested:
            return .requested
        case .delivered:
            return .delivered
        case .inProduction:
            return .production
        case .inRoute:
            return .route
        case .onCarrier:
            return .carrier
        case .deliveredError:
            return .deliveryError
        case .unknown:
            return nil
        }
    }
}
