import UIKit
import SnapKit
import UI

public class CardTrackingHistoryViewController: UIViewController {
    lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    public init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        buildLayout()
    }
    
    func update(history: [CardTrackingHistoryItem], isArrived: Bool, cardType: CardTrackingType) {
        stackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
        
        for (index, historyItem) in history.enumerated() {
            let cell = CardTrackingHistoryCellViewController(historyItem: historyItem, isArrived: isArrived, cardType: cardType)
            let isLast = index == (history.count - 1)
            cell.isLast = isLast
            stackView.addArrangedSubview(cell.view)
            
            if !isLast {
                let nextItem = history[index + 1]
                let nextItemIsBlank = nextItem.status == .waiting
                stackView.addArrangedSubview(CardTrackingSeparatorViewController(isGreen: !nextItemIsBlank).view)
            }
        }
    }
}

extension CardTrackingHistoryViewController: ViewConfiguration {
    public func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    public func buildViewHierarchy() {
        view.addSubview(stackView)
    }
}
