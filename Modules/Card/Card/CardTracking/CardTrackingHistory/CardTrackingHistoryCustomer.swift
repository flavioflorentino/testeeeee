import CustomerSupport
import Foundation

struct CardTrackingHistoryCustomer: CustomerOptions {
    let development: FAQOptions
    let production: FAQOptions
    let path: PathFAQ
    
    init() {
        development = .home
        production = .article(id: "360043765752")
        path = .activation
    }
}
