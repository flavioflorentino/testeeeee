import UIKit
import SnapKit
import UI
import AssetsKit

protocol CardTrackingHistoryCellDisplayable: AnyObject {
    func updateFormattedDate(attributedText: NSAttributedString)
    func updateFormattedTitle(attributedText: NSAttributedString)
}

extension CardTrackingHistoryCellViewController.Layout {
    enum Size {
        static let height: CGFloat = Sizing.base02
        static let heightHighlight: CGFloat = 24
        static let heightArrived: CGFloat = Sizing.base07
        static let widthCircle: CGFloat = Sizing.base02
        static let widthImage: CGFloat = Sizing.base04
        static let arrivedImageSize: CGFloat = Sizing.base08
    }
}

public class CardTrackingHistoryCellViewController: UIViewController {
    fileprivate enum Layout {}
    
    private let historyItem: CardTrackingHistoryItem
    private let isArrived: Bool
    private let presenter: CardTrackingHistoryCellPresenting
    
    var isLast: Bool = false
    
    private lazy var dateLabel: UILabel = {
        let label = UILabel()
        label.lineBreakMode = .byTruncatingHead
        return label
    }()
    
    private lazy var titleLabel = UILabel()
    
    private lazy var centerView: UIView = {
        switch historyItem.status {
        case .waiting:
            return emptyCircleView
        case .done:
            if isArrived && isLast {
                return arrivedImageView
            } else {
                return filledCircleView
            }
        case .error:
            return errorImageView
        case .inProgress:
            return highlightImageView
        case .unknown:
            return UIView()
        }
    }()
    
    private lazy var errorImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.icoExclamationMark.image.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = Colors.critical900.color
        
        return imageView
    }()
    
    private lazy var emptyCircleView: UIView = {
        let view = UIView()
        view.layer.borderWidth = Border.light
        view.layer.cornerRadius = CornerRadius.medium
        view.layer.borderColor = Colors.grayscale300.color.cgColor
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var filledCircleView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = CornerRadius.medium
        view.backgroundColor = Colors.branding400.color
        return view
    }()
    
    private lazy var highlightImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.icoCardTracking.image
        return imageView
    }()
    
    private lazy var arrivedImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = Resources.Icons.icoPinCardCompose.image
        return imageView
    }()
    
    init(historyItem: CardTrackingHistoryItem, isArrived: Bool, cardType: CardTrackingType) {
        self.historyItem = historyItem
        self.isArrived = isArrived
        self.presenter = CardTrackingHistoryCellPresenter(historyItem: historyItem, cardType: cardType)
        super.init(nibName: nil, bundle: nil)
        self.presenter.viewController = self
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        buildLayout()
        presenter.viewDidLoad()
    }
}

extension CardTrackingHistoryCellViewController: ViewConfiguration {
    public func setupConstraints() {
        centerView.snp.makeConstraints {
            $0.center.equalToSuperview().offset(-Spacing.base01)
            if historyItem.status.isHighlight {
                $0.height.equalTo(Layout.Size.heightHighlight)
            } else if isArrived && isLast {
                $0.height.equalTo(Layout.Size.arrivedImageSize)
            } else {
                $0.height.equalTo(Layout.Size.height)
            }
            $0.top.bottom.equalToSuperview()
        }
        dateLabel.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.trailing.equalTo(centerView.snp.leading).offset(-Spacing.base01)
            $0.leadingMargin.equalTo(view.snp.leadingMargin).offset(Spacing.base00)
        }
        titleLabel.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalTo(centerView.snp.trailing).offset(Spacing.base01)
            $0.trailingMargin.equalTo(view.snp.trailingMargin).offset(-Spacing.base00)
        }
        emptyCircleView.snp.makeConstraints {
            $0.width.equalTo(Layout.Size.widthCircle)
        }
        filledCircleView.snp.makeConstraints {
            $0.width.equalTo(Layout.Size.widthCircle)
        }
        errorImageView.snp.makeConstraints {
            $0.width.equalTo(Layout.Size.widthCircle)
        }
        highlightImageView.snp.makeConstraints {
            $0.width.equalTo(Layout.Size.widthImage)
        }
        arrivedImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.arrivedImageSize)
        }
    }
    
    public func buildViewHierarchy() {
        view.addSubview(dateLabel)
        view.addSubview(centerView)
        view.addSubview(titleLabel)
    }
}

extension CardTrackingHistoryCellViewController: CardTrackingHistoryCellDisplayable {
    func updateFormattedDate(attributedText: NSAttributedString) {
        dateLabel.attributedText = attributedText
    }
    
    func updateFormattedTitle(attributedText: NSAttributedString) {
        titleLabel.attributedText = attributedText
    }
}
