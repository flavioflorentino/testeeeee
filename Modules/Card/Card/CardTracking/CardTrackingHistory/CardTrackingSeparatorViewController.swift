import UIKit
import SnapKit
import UI

extension CardTrackingSeparatorViewController.Layout {
    enum Size {
        static let width = 1
    }
}

class CardTrackingSeparatorViewController: UIViewController {
    fileprivate enum Layout {}
    
    private let isGreen: Bool
    private lazy var separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = isGreen ? Colors.branding400.color : Colors.grayscale300.color
        return view
    }()
    
    init(isGreen: Bool) {
        self.isGreen = isGreen
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildLayout()
    }
}

extension CardTrackingSeparatorViewController: ViewConfiguration {
    public func setupConstraints() {
        separatorView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.height.equalTo(Sizing.base02)
            $0.width.equalTo(Layout.Size.width)
            $0.center.equalToSuperview().offset(-Spacing.base01)
        }
    }
    
    public func buildViewHierarchy() {
        view.addSubview(separatorView)
    }
}
