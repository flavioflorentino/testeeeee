import UIKit
import UI

protocol CardTrackingHistoryCellPresenting: AnyObject {
    var viewController: CardTrackingHistoryCellDisplayable? { get set }
    func viewDidLoad()
}

extension CardTrackingHistoryCellPresenter.Layout {
    enum Font {
        static let date = UIFont.systemFont(ofSize: 12, weight: .bold)
        static let dateHighlight = UIFont.systemFont(ofSize: 14, weight: .heavy)
        static let time = UIFont.systemFont(ofSize: 12, weight: .medium)
        static let timeHighlight = UIFont.systemFont(ofSize: 14, weight: .medium)
        static let title = UIFont.systemFont(ofSize: 14, weight: .medium)
        static let titleHighlight = UIFont.systemFont(ofSize: 18, weight: .semibold)
    }
    
    enum Color {
        static let date = Colors.grayscale300.color
        static let titleBlank = Colors.grayscale300.color
        static let titleDone = Colors.branding400.color
        static let titleHighlight = Colors.grayscale600.color
        static let titleError = Colors.critical900.color
    }
}

public class CardTrackingHistoryCellPresenter {
    fileprivate enum Layout {}
    
    private let historyItem: CardTrackingHistoryItem
    private let cardType: CardTrackingType
    weak var viewController: CardTrackingHistoryCellDisplayable?
    
    init(historyItem: CardTrackingHistoryItem, cardType: CardTrackingType) {
        self.historyItem = historyItem
        self.cardType = cardType
    }
    
    func updateDate() {
        let timeFont = historyItem.status.isHighlight ? Layout.Font.timeHighlight : Layout.Font.time
        let dateFont = historyItem.status.isHighlight ? Layout.Font.dateHighlight : Layout.Font.date
        
        var text = ""
        if let day = historyItem.date?.formatted(in: .day), var month = historyItem.date?.formatted(in: .monthShortText) {
            month.capitalizeFirstLetter()
            text.append("**\(day) \(month)**")
        }
        if let time = historyItem.date?.formatted(in: .time), cardType == .multiple {
            text.append(text.isEmpty ? "" : " ")
            text.append(time)
        }
        text.removeAll(where: { $0 == "." })
        
        let attributedString = text.attributedStringWith(
            normalFont: timeFont,
            highlightFont: dateFont,
            normalColor: Layout.Color.date,
            highlightColor: Layout.Color.date,
            textAlignment: .right,
            underline: false
        )
        
        viewController?.updateFormattedDate(attributedText: attributedString)
    }
    
    func updateTitle() {
        var textColor = Layout.Color.titleBlank
        let font = historyItem.status.isHighlight ? Layout.Font.titleHighlight : Layout.Font.title
        switch historyItem.status {
        case .waiting, .unknown:
            textColor = Layout.Color.titleBlank
        case .done:
            textColor = Layout.Color.titleDone
        case .inProgress:
            textColor = Layout.Color.titleHighlight
        case .error:
            textColor = Layout.Color.titleError
        }
        let attributedString = NSMutableAttributedString(
            string: historyItem.title ?? "",
            attributes: [.foregroundColor: textColor, .font: font]
        )
        
        viewController?.updateFormattedTitle(attributedText: attributedString)
    }
}

extension CardTrackingHistoryCellPresenter: CardTrackingHistoryCellPresenting {
    func viewDidLoad() {
        updateDate()
        updateTitle()
    }
}
