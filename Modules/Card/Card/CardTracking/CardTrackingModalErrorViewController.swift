import UIKit
import SnapKit
import UI

extension CardTrackingModalErrorViewController.Layout {
    enum Font {
        static let normal = Typography.bodyPrimary().font()
        static let bold = Typography.bodyPrimary(.highlight).font()
    }
}

public final class CardTrackingModalErrorViewController: UIViewController, ViewConfiguration {
    fileprivate enum Layout {}
    
    private var addressText: String
    private var didTapChangeAddress: (() -> Void)
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fill
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale700())
        label.attributedText = titleContentDescription()
        label.textAlignment = .center
        return label
    }()
    
    private lazy var secondTitleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        label.text = Strings.CardTrackingModalError.secondTitle
        return label
    }()
    
    private lazy var addressLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .left)
        label.text = addressText
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var firstNumberLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        label.text = Strings.CardTrackingModalError.numberOne
        return label
    }()
    
    private lazy var secondNumberLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        label.text = Strings.CardTrackingModalError.numberTwo
        return label
    }()
    
    private lazy var firstDescriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale700())
        label.attributedText = firstItemContentDescription()
        label.textAlignment = .left
        label.textColor = Colors.grayscale700.color
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var secondDescriptionLabel: UILabel = {
        let label = UILabel()
        label.attributedText = secondItemContentDescription()
        label.textAlignment = .left
        label.textColor = Colors.grayscale700.color
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var changeAddressButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(didClickButton), for: .touchUpInside)
        button.setTitle(Strings.CardTrackingModalError.button, for: .normal)
        return button
    }()
    
    private lazy var firstItemStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fill
        stackView.axis = .horizontal
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var secondItemStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fill
        stackView.axis = .horizontal
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var addressStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fill
        stackView.axis = .vertical
        stackView.spacing = Spacing.base00
        return stackView
    }()
    
    override public func viewDidLayoutSubviews() {
        firstItemStackView.layoutIfNeeded()
    }
    
    public init(addressText: String, didTapChangeAddress: @escaping () -> Void) {
        self.addressText = addressText
        self.didTapChangeAddress = didTapChangeAddress
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        buildLayout()
    }
    
    @objc
    public func didClickButton() {
        didTapChangeAddress()
    }
    
    public func buildViewHierarchy() {
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(secondTitleLabel)
        
        addressStackView.addArrangedSubview(firstDescriptionLabel)
        addressStackView.addArrangedSubview(addressLabel)
        
        firstItemStackView.addArrangedSubview(createStack(for: firstNumberLabel))
        firstItemStackView.addArrangedSubview(addressStackView)
        secondItemStackView.addArrangedSubview(createStack(for: secondNumberLabel))
        secondItemStackView.addArrangedSubview(secondDescriptionLabel)
        
        stackView.addArrangedSubview(firstItemStackView)
        stackView.addArrangedSubview(secondItemStackView)
        stackView.addArrangedSubview(changeAddressButton)
        
        view.addSubview(stackView)
    }
    
    private func createStack(for label: UILabel) -> UIStackView {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.addArrangedSubview(label)
        stackView.addArrangedSubview(UIView())
        return stackView
    }
    
    func titleContentDescription() -> NSAttributedString {
        let text = Strings.CardTrackingModalError.title
        let descriptionHighlight = Strings.CardTrackingModalError.Title.highlight
        let attText = text.attributedStringWithFont(primary: Layout.Font.normal, secondary: Layout.Font.bold)
        attText.font(text: descriptionHighlight, font: Layout.Font.bold)
        return attText
    }
    
    func firstItemContentDescription() -> NSAttributedString {
        let text = Strings.CardTrackingModalError.firstDescription
        let descriptionHighlight = Strings.CardTrackingModalError.FirstDescription.highlight
        let attText = text.attributedStringWithFont(primary: Layout.Font.normal, secondary: Layout.Font.bold)
        attText.font(text: descriptionHighlight, font: Layout.Font.bold)
        return attText
    }
    
    func secondItemContentDescription() -> NSAttributedString {
        let text = Strings.CardTrackingModalError.secondDescription
        let descriptionHighlight = Strings.CardTrackingModalError.SecondDescription.highlight
        let attText = text.attributedStringWithFont(primary: Layout.Font.normal, secondary: Layout.Font.bold)
        attText.font(text: descriptionHighlight, font: Layout.Font.bold)
        return attText
    }
    
    public func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    public func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.width.equalToSuperview()
        }
        
        firstNumberLabel.snp.makeConstraints {
            $0.width.equalTo(Spacing.base03)
        }
        
        secondNumberLabel.snp.makeConstraints {
            $0.width.equalTo(firstNumberLabel)
        }
        
        addCustomSpacing(in: stackView, spacing: Spacing.base02, after: titleLabel)
        addCustomSpacing(in: stackView, spacing: Spacing.base04, after: secondTitleLabel)
        addCustomSpacing(in: stackView, spacing: Spacing.base03, after: firstItemStackView)
        addCustomSpacing(in: stackView, spacing: Spacing.base03, after: secondItemStackView)
        addCustomSpacing(in: addressStackView, spacing: Spacing.base01, after: firstDescriptionLabel)
    }
    
    func addCustomSpacing(in stackView: UIStackView, spacing: CGFloat, after arrangedSubview: UIView) {
        if #available(iOS 11.0, *) {
            stackView.setCustomSpacing(spacing, after: arrangedSubview)
        } else {
            let separatorView = UIView(frame: .zero)
            separatorView.translatesAutoresizingMaskIntoConstraints = false
            switch stackView.axis {
            case .horizontal:
                separatorView.widthAnchor.constraint(equalToConstant: spacing).isActive = true
            default:
                separatorView.heightAnchor.constraint(equalToConstant: spacing).isActive = true
            }
            if let index = stackView.arrangedSubviews.firstIndex(of: arrangedSubview) {
                stackView.insertArrangedSubview(separatorView, at: index + 1)
            }
        }
    }
}
