import UIKit

enum CardTrackingAction: Equatable {
    case cardArrived(type: CardTrackingType)
    case errorButton(type: CardTrackingType)
    case close
    case openHelpCenter(url: URL)
}

protocol CardTrackingCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: CardTrackingAction)
}

public final class CardTrackingCoordinator {
    weak var viewController: UIViewController?
    private let coordinadorOutput: (CardHistoryOutput) -> Void
    
    init(coordinadorOutput: @escaping (CardHistoryOutput) -> Void) {
        self.coordinadorOutput = coordinadorOutput
    }
}

// MARK: - CardTrackingCoordinating
extension CardTrackingCoordinator: CardTrackingCoordinating {
    func perform(action: CardTrackingAction) {
        switch action {
        case .cardArrived(let type):
            coordinadorOutput(.cardArrived(cardType: type))
        case .close:
            coordinadorOutput(.close)
        case .errorButton(let cardType):
            coordinadorOutput(.errorButton(type: cardType))
        case .openHelpCenter(let url):
            coordinadorOutput(.openHelpCenter(url: url))
        }
    }
}
