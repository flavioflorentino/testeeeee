import Foundation

public struct HomeAddress: Codable {
    var zipCode: String?
    var addressComplement: String?
    var city: String?
    var cityId: String?
    var neighborhood: String?
    var state: String?
    var stateId: String?
    var streetName: String?
    var streetNumber: String?
    var streetType: String?
    var street: String?
    var manualUpdated: Bool?
}

extension HomeAddress: Equatable {}

extension HomeAddress {
    var completeAddress: String? {
        var address = [streetName ?? street, streetNumber, city, state]
            .compactMap { $0 }
            .joined(separator: ", ")
        
        if let stateId = stateId {
            address.append("-\(stateId)")
        }
        
        if let zipCode = zipCode {
            address.append(", CEP: \(zipCode)")
        }
        
        return address
    }
}
