import Foundation

public struct CardSettings: Codable {
    public let cardWalletEnabled: Bool
    public let cardSettingsEnabled: Bool
    public let cardCreditSettingsEnabled: Bool
    public let physicalCardConfigurationEnabled: Bool
    public let physicalCardTrackingEnabled: Bool
    public let physicalCardBlockEnabled: Bool
    public let replacementButton: CardReplacementButton
    public let virtualCardButton: VirtualCardButton
    public let appPaymentEnabled: Bool
    public let invoicePaymentEnabled: Bool
    public let upgradeDebitToCredit: Bool
    
    public let banners: [Banner]

    enum CodingKeys: String, CodingKey {
        case cardWalletEnabled = "card_wallet_enabled"
        case cardSettingsEnabled = "card_settings_enabled"
        case cardCreditSettingsEnabled = "card_credit_settings_enabled"
        case physicalCardConfigurationEnabled = "physical_card_configuration_enabled"
        case physicalCardTrackingEnabled = "physical_card_tracking_enabled"
        case physicalCardBlockEnabled = "physical_card_block_enabled"
        case replacementButton = "replacement_button"
        case virtualCardButton = "virtual_card_button"
        case appPaymentEnabled = "app_payment_enabled"
        case invoicePaymentEnabled = "invoice_payment_enabled"
        case upgradeDebitToCredit = "upgrade_debit_to_credit_flow"
        case banners = "banners_enabled"
    }

     public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        cardWalletEnabled = try container.decodeIfPresent(Bool.self, forKey: .cardWalletEnabled) ?? false
        cardSettingsEnabled = try container.decodeIfPresent(Bool.self, forKey: .cardSettingsEnabled) ?? false
        cardCreditSettingsEnabled = try container.decodeIfPresent(Bool.self, forKey: .cardCreditSettingsEnabled) ?? true
        physicalCardConfigurationEnabled = try container.decodeIfPresent(Bool.self, forKey: .physicalCardConfigurationEnabled) ?? false
        physicalCardTrackingEnabled = try container.decodeIfPresent(Bool.self, forKey: .physicalCardTrackingEnabled) ?? false
        physicalCardBlockEnabled = try container.decodeIfPresent(Bool.self, forKey: .physicalCardBlockEnabled) ?? false
        replacementButton = try container.decodeIfPresent(CardReplacementButton.self, forKey: .replacementButton) ?? CardReplacementButton(type: .none, action: .none)
        virtualCardButton = try container.decodeIfPresent(VirtualCardButton.self, forKey: .virtualCardButton) ?? VirtualCardButton(action: .none, newFeature: false)
        appPaymentEnabled = try container.decodeIfPresent(Bool.self, forKey: .appPaymentEnabled) ?? false
        invoicePaymentEnabled = try container.decodeIfPresent(Bool.self, forKey: .invoicePaymentEnabled) ?? false
        upgradeDebitToCredit = try container.decodeIfPresent(Bool.self, forKey: .upgradeDebitToCredit) ?? false
        let bannerThrowables = try container.decode([Throwable<Banner>].self, forKey: .banners)
        banners = bannerThrowables.compactMap { try? $0.result.get() }
    }
    
    public init() {
        cardWalletEnabled = false
        cardSettingsEnabled = false
        cardCreditSettingsEnabled = false
        physicalCardConfigurationEnabled = true
        physicalCardTrackingEnabled = true
        physicalCardBlockEnabled = false
        replacementButton = CardReplacementButton(type: .none, action: .none)
        virtualCardButton = VirtualCardButton(action: .none, newFeature: false)
        appPaymentEnabled = false
        invoicePaymentEnabled = false
        upgradeDebitToCredit = false
        banners = []
    }
    
    public enum Banner: String, Codable {
        case cardTracking = "PHYSICAL_CARD_TRACKING"
        case cardOffer = "CARD_OFFER"
        case cardDebitOffer = "CARD_DEBIT_OFFER"
        case cardDebitActivation = "CARD_DEBIT_ACTIVATION"
        case physicalCardActivation = "PHYSICAL_CARD_ACTIVATION"
        case physicalDebitCardActivation = "PHYSICAL_DEBIT_CARD_ACTIVATION"
        case migrationPosCard = "MIGRATION_POS_CARD"
        case askPhysicalCard = "ASK_PHYSICAL_CARD"
        case debitRequestInProgress = "DEBIT_REQUEST_IN_PROGRESS"
        case upgradeDebitToCredit = "UPGRADE_DEBIT_TO_CREDIT"
        case simplifiedDebitRegistration = "SIMPLIFIED_DEBIT_REGISTRATION"
    }
}
