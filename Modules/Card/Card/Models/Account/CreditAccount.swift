import Foundation

public struct CreditAccount: Codable {
    public let availableCredit: Double?
    public let availableLimit: Double?
    public let creditStatus: String?
    public let dueDay: Int?
    public let isOfferEnabled: Bool
    public let offerText: String
    public let registrationStatus: RegistrationStatus
    public let selectedLimit: Double?
    public let isPhysicalCardEnabled: Bool
    public let physicalCardStatus: PhysicalCardStatusType
    public let settings: CardSettings
    
    public enum CodingKeys: String, CodingKey {
        case availableCredit = "available_credit"
        case availableLimit = "available_limit"
        case creditStatus = "credit_status"
        case dueDay = "due_day"
        case isOfferEnabled = "offer_enabled"
        case offerText = "offer_text"
        case registrationStatus = "registration_status"
        case selectedLimit = "selected_limit"
        case isPhysicalCardEnabled = "physical_card_enabled"
        case physicalCardStatus = "physical_card_status"
        case settings = "settings"
    }
    
    public enum RegistrationStatus: String, Codable {
        case empty = "EMPTY"
        case inProgress = "IN_PROGRESS"
        case underAnalysis = "UNDER_ANALYSIS"
        case reviewRequested = "REVIEW_REQUESTED"
        case completed = "COMPLETED"
        case approved = "APPROVED"
        case notExist = "NOT_EXISTS"
        case denied = "DENIED"
        case creditWaitingActivation = "CREDIT_WAITING_ACTIVATION"
        case creditActivated = "CREDIT_ACTIVATED"
    }
    
    public enum PhysicalCardStatusType: String, Codable {
        case notExists = "NOT_EXISTS"
        case activated = "ACTIVATED"
        case waitingActivation = "WAITING_ACTIVATION"
        case blocked = "BLOCKED"
        case notRequested = "NOT_REQUESTED"
        case debitWaitingActivation = "DEBIT_WAITING_ACTIVATION"
    
        public init(from decoder: Decoder) throws {
            self = try PhysicalCardStatusType(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .notExists
        }
    }
}

public struct CardReplacementButton: Codable {
    public let type: CardReplacementButtonType
    public let action: CardReplacementButtonAction
    
    public enum CardReplacementButtonType: String, Codable {
        case none = "NONE"
        case requestable = "REQUESTABLE"
        case requested = "REQUESTED"

        public init(from decoder: Decoder) throws {
            self = try CardReplacementButtonType(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .none
        }
    }

    public enum CardReplacementButtonAction: String, Codable {
        case none = "NONE"
        case address = "ADDRESS"
        case tracking = "TRACKING"

        public init(from decoder: Decoder) throws {
            self = try CardReplacementButtonAction(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .none
        }
    }
}

public struct VirtualCardButton: Codable {
    public let action: VirtualCardButtonAction
    public let newFeature: Bool
    
    public enum CodingKeys: String, CodingKey {
        case action = "action"
        case newFeature = "new_feature"
    }
}

public enum VirtualCardButtonAction: String, Codable {
    case none = "NONE"
    case onboarding = "ONBOARDING"
    case cardInfo = "CARD_INFO"
    case deleted = "DELETED"

    public init(from decoder: Decoder) throws {
        self = try VirtualCardButtonAction(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .none
    }
}

public struct VirtualCardActionResponse: Codable {
    let action: VirtualCardButtonAction
}
    
