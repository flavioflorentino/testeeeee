struct CardTrackingHistoryItem: Decodable {
    let date: Date?
    let title: String?
    let status: Status
    
    enum Status: String, Decodable {
        case done = "DONE"
        case inProgress = "IN_PROGRESS"
        case waiting = "WAITING"
        case error = "ERROR"
        case unknown
        
        init(from decoder: Decoder) throws {
            self = try Status(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .unknown
        }
        
        var isHighlight: Bool {
            self == .inProgress
        }
    }
}
