import Foundation

public enum AddressType: String, Codable {
    case residential = "RESIDENTIAL"
    case comercial = "COMERCIAL"
    case none
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let value = try container.decode(String.self)
        
        guard let type = AddressType(rawValue: value) else {
            self = .none
            return
        }
        
        self = type
    }
}

public struct AccountAddress: Codable {
    public let street: String
    public let number: Int
    public let complement: String?
    public let neighborhood: String
    public let state: String
    public let city: String
    public let type: AddressType
    public let nick: String?
    public let zipCode: String

    public var address: String {
        if let nick = nick {
            return nick
        }
        
        let cityState = [city, state].joined(separator: " - ")
        return [street, "\(number)", complement, neighborhood, cityState].compactMap({ $0 }).joined(separator: ", ")
    }
}
