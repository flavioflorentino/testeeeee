import Foundation

public enum DebitStep: String, Codable {
    case onboarding = ""
    case personalData = "PERSONAL_DATA"
    case address = "ADDRESS"
    case confirmation = "CONFIRMATION"
    case waitingActivation = "WAITING_ACTIVATION"
    case success
}
