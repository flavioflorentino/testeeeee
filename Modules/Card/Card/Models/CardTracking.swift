import Foundation

public struct CardTracking: Codable {
    public let requestAt: String?
    
    public enum CodingKeys: String, CodingKey {
        case requestAt = "requested_at"
    }
}
