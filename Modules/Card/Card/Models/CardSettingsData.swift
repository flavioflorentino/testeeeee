public struct CardSettingsData {
    let cardType: PicPayCardType
    public let buttons: CardSettingsButtons
    
    public init(cardType: PicPayCardType, buttons: CardSettingsButtons) {
        self.cardType = cardType
        self.buttons = buttons
    }
    
    public func empty() -> CardSettingsData {
        let buttons = CardSettingsButtons(
            cardReplacement: CardReplacementButton(type: .none, action: .none),
            virtualCard: VirtualCardButton(action: .none, newFeature: false)
        )
        return CardSettingsData(cardType: .none, buttons: buttons)
    }
}

public enum PicPayCardType {
    case none
    case multiple
    case debit
}

public struct CardSettingsButtons {
    let cardReplacement: CardReplacementButton
    public let virtualCard: VirtualCardButton
    
    public init(cardReplacement: CardReplacementButton, virtualCard: VirtualCardButton) {
        self.cardReplacement = cardReplacement
        self.virtualCard = virtualCard
    }
}
