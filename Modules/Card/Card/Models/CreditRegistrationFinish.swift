import Foundation

public struct CreditRegistrationFinish: Codable {
    let registrationStatus: RegistrationStatus
    let creditStatus: Status?
    let debitStatus: Status?
    let offerEnabled: Bool?
    
    enum Status: String, Codable {
        case onRegistration = "ON_REGISTRATION"
        case notExists
        
        init(from decoder: Decoder) throws {
            self = try Status(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .notExists
        }
    }
    
    enum CodingKeys: String, CodingKey {
        case registrationStatus = "registration_status"
        case creditStatus = "credit_status"
        case debitStatus = "debit_status"
        case offerEnabled = "offer_enabled"
     }
}
