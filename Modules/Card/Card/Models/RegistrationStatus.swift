public enum RegistrationStatus: String, Codable {
    case activated = "CREDIT_ACTIVATED"
    case waitingActivation = "CREDIT_WAITING_ACTIVATION"
    case inProgress = "IN_PROGRESS"
    case empty = "EMPTY"
    case notExists = "NOT_EXISTS"
    
    public init(from decoder: Decoder) throws {
        self = try RegistrationStatus(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .notExists
    }
}
