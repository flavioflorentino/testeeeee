import Foundation
import UI

public struct CardForm: Codable {
    var registrationStatus: RegistrationStatus?
    var registrationStep: DebitStep?
    
    var creditGrantedByBank: Double?
    var debitStatus: String?
    
    var cpf: String?
    var email: String?
    var name: String?
    var birthDate: String?
    var ddd: String?
    var phone: String?
    
    var countryOfBirthName: String?
    var stateOfBirthName: String?
    var cityOfBirthName: String?
    
    var idDocument: IdDocument?
    
    var mothersName: String?
    var homeAddress: HomeAddress?
    
    var occupation: CreditOccupation?
    var profession: CreditProfession?
    var income: Double?
    var patrimony: CreditPatrimony?
    var dueDay: Int?
    
    struct CreditPatrimony: Codable, Equatable {
        let id: Int?
        let minValue: Double
        let maxValue: Double
    }
    
    struct CreditProfession: Codable, Equatable {
        let id: Int?
        let name: String?
    }
    
    struct CreditOccupation: Codable, Equatable {
        let id: Int?
        let name: String?
    }
    
    struct IdDocument: Codable, Equatable {
        var number: String?
        var issuerOfficeInitials: String?
        var emissionDate: String?
        var typeName: String?
    }
   
    public init() {}
}

extension CardForm {
    var maskedCPF: String? {
        let mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cpf)
        return mask.maskedText(from: cpf)
    }
    
    var completeBirthPlace: String? {
        guard let country = countryOfBirthName, let state = stateOfBirthName, let city = cityOfBirthName else {
            return nil
        }
        return "\(city), \(state), \(country)"
    }
    
    var completeNumber: String? {
        guard let ddd = ddd, let number = phone else {
            return nil
        }
        let mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cellPhoneWithDDD)
        return mask.maskedText(from: ddd + number)
    }
    
    var completeDocType: String? {
        guard let docType = idDocument?.typeName else {
            return nil
        }
        
        let mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.rg)
        guard let masked = mask.maskedText(from: idDocument?.number) else { return nil }
        
        return "\(docType): \(masked)"
    }
    
    var incomeString: String? {
        guard let income = income else {
            return nil
        }
        return NumberFormatter.toCurrencyString(with: income)
    }
}

extension CardForm.CreditPatrimony {
    var minMaxValue: String? {
        guard let minValueFormatted = NumberFormatter.toCurrencyString(with: minValue) else { return nil }
        guard let maxValueFormatted = NumberFormatter.toCurrencyString(with: maxValue) else { return nil }
        
        if maxValue == -1 {
            return Strings.HiringWaitingResume.Additional.patrimonyGreaterThan(minValueFormatted)
        }
        return Strings.HiringWaitingResume.Additional.patrimonyFromTo(minValueFormatted, maxValueFormatted)
    }
}

extension CardForm: Equatable { }
