import Foundation

struct OriginFlow: Encodable {
    let origin: Origin
    
    enum CodingKeys: String, CodingKey {
        case origin = "originFlow"
    }
    
    enum Origin: String, Codable {
        case debit = "DEBIT"
        case credit = "CREDIT"
    }
}
