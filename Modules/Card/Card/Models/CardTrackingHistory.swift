import Foundation

public struct CardTrackingHistory: Decodable {
    let currentStep: CardTrackingCurrentStep
    let cardType: CardTrackingType
    let notReceivedButtonEnabled: Bool
    let estimatedTime: TrackingEstimatedTime?
    let tracking: [CardTrackingHistoryItem]
    
    var arrived: Bool {
        currentStep == .delivered
    }
    
    var confirmButtonEnabled: Bool {
        currentStep == .onCarrier || currentStep == .inRoute
    }
    
    var notArrivedButtonEnabled: Bool {
        (currentStep == .deliveredError && cardType == .multiple)
            ||
        (notReceivedButtonEnabled && cardType == .debit)
    }
}

public enum CardTrackingCurrentStep: String, Decodable {
    case requested = "REQUESTED"
    case inProduction = "IN_PRODUCTION"
    case onCarrier = "ON_CARRIER"
    case inRoute = "IN_ROUTE"
    case delivered = "DELIVERED"
    case deliveredError = "DELIVERED_ERROR"
    case unknown
    
    public init(from decoder: Decoder) throws {
        self = try CardTrackingCurrentStep(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .unknown
    }
}

public enum CardTrackingType: String, Decodable {
    case debit = "DEBIT"
    case multiple = "MULTIPLE"
    case unknown
    
    public init(from decoder: Decoder) throws {
        self = try CardTrackingType(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .unknown
    }
    
    var deeplinkCardType: DeeplinkCardType {
        self == .multiple ? .needCreditActive : .needDebitActive
    }
    
    var analyticsName: String? {
        switch self {
        case .debit:
            return "Debit"
        default:
            return nil
        }
    }
}
