import Foundation

public struct DollarExchangeRateInfo: Codable {
    let current: DollarExchangeRate
    /// Returns the latest rates up to 6 days
    let latest: [DollarExchangeRate]
}

public struct DollarExchangeRate: Codable {
    let value: Double
    let date: Date
}

public struct DollarExchangeHistory: Codable {
    let history: [DollarExchangeRate]
}
