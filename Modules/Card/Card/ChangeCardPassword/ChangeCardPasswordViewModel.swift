import Core
import Foundation

public protocol ChangeCardPasswordViewModelInputs {
    func viewDidLoad()
    func didTapConfirmWithAuthentication(and newCardPin: String)
    func didTapHelper()
}

public protocol ChangeCardPasswordViewModelOutputs: AnyObject {
    func present(_ presenter: DigitFormPresenting)
    func show(errorDescription: String)
    func perform(action: ChangeCardPasswordAction)
    func didAuthenticate()
}

public protocol ChangeCardPasswordViewModelType: AnyObject {
    var inputs: ChangeCardPasswordViewModelInputs { get }
    var outputs: ChangeCardPasswordViewModelOutputs? { get set }
}

public final class ChangeCardPasswordViewModel: ChangeCardPasswordViewModelType, ChangeCardPasswordViewModelInputs {
    public var inputs: ChangeCardPasswordViewModelInputs { self }
    public weak var outputs: ChangeCardPasswordViewModelOutputs?
    
    typealias Dependencies = HasAuthManagerDependency
    private let dependencies: Dependencies
    
    private let service: ChangeCardPasswordServicing
    private let oldCardPin: String
    
    init(oldCardPin: String, service: ChangeCardPasswordServicing, dependencies: Dependencies) {
        self.oldCardPin = oldCardPin
        self.service = service
        self.dependencies = dependencies
    }
    
    // MARK: - DigitFormViewModelInputs
    
    public func viewDidLoad() {
        let presenter = ChangeCardPasswordPresenter()
        outputs?.present(presenter)
    }
    
    public func didTapConfirmWithAuthentication(and newCardPin: String) {
        dependencies.authManager.authenticate(sucessHandler: { [weak self] appPassword, _ in
            guard let self = self else { return }
            self.outputs?.didAuthenticate()
            self.service.changeCardPassword(self.oldCardPin, newCardPin: newCardPin, password: appPassword) { [weak self] result in
                self?.resultHandler(result: result)
            }
        }, cancelHandler: nil)
    }
    
    public func didTapHelper() {
        outputs?.perform(action: .securityTips)
    }
    
    private func resultHandler(result: Result<Void, ApiError>) {
        switch result {
        case .success:
            outputs?.perform(action: .success)
        case .failure(.badRequest(let picPayError)),
             .failure(.notFound(let picPayError)),
             .failure(.unauthorized(let picPayError)):
            let message = picPayError.message
            outputs?.show(errorDescription: message)
        case .failure:
            outputs?.show(errorDescription: Strings.CreateCardPassword.defaultFailureMessage)
        }
    }
}
