import Core
import Foundation

public protocol ChangeCardPasswordServicing {
    func changeCardPassword(_ oldCardPin: String, newCardPin: String, password: String, then completion: @escaping (Result<Void, ApiError>) -> Void)
}

public final class ChangeCardPasswordService: ChangeCardPasswordServicing {
    public func changeCardPassword(_ oldCardPin: String, newCardPin: String, password: String, then completion: @escaping (Result<Void, ApiError>) -> Void) {
        let api = Api<NoContent>(endpoint: CreditServiceEndpoint.changePassword(oldCardPin: oldCardPin, newCardPin: newCardPin, password: password))
        api.execute { result in
            DispatchQueue.main.async {
                completion(result.map { _ in })
            }
        }
    }
}
