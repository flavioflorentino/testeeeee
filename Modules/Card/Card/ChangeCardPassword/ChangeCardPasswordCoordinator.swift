import UIKit
public enum ChangeCardPasswordAction {
    case confirm(onSuccessAuth: (String) -> Void)
    case success
    case securityTips
}

public protocol ChangeCardPasswordCoordinatorDelegate: AnyObject {
    func didConfirmChangePin(onSuccess: @escaping (String) -> Void)
    func didSuccessChangePin()
    func didOpenHelperSecurityTips()
}

public protocol ChangeCardPasswordCoordinating {
    var viewController: UIViewController? { get set }
    var delegate: ChangeCardPasswordCoordinatorDelegate? { get set }
    func perform(action: ChangeCardPasswordAction)
}

public final class ChangeCardPasswordCoordinator: ChangeCardPasswordCoordinating {
    public var viewController: UIViewController?
    public weak var delegate: ChangeCardPasswordCoordinatorDelegate?

    public func perform(action: ChangeCardPasswordAction) {
        switch action {
        case .confirm(let successClosure):
            delegate?.didConfirmChangePin(onSuccess: successClosure)
        case .success:
            delegate?.didSuccessChangePin()
        case .securityTips:
            delegate?.didOpenHelperSecurityTips()
        }
    }
}
