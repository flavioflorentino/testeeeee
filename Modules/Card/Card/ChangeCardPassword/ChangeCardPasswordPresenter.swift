public struct ChangeCardPasswordPresenter: DigitFormPresenting {
    public let title: String
    public let description: String
    public let confirmTitle: String
    public let isSecurityCode: Bool
    public var helperTitle: String?
    
    init() {
        title = Strings.ChangeCardPassword.title
        description = Strings.ChangeCardPassword.description
        confirmTitle = Strings.ChangeCardPassword.confirmButton
        isSecurityCode = true
    }
}
