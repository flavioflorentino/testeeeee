import Foundation

public enum ChangeCardPasswordFactory {
    public static func make(oldCardPin cardPin: String, delegate: ChangeCardPasswordCoordinatorDelegate) -> ChangeCardPasswordViewController {
        let container = DependencyContainer()
        let service = ChangeCardPasswordService()
        let viewModel = ChangeCardPasswordViewModel(oldCardPin: cardPin, service: service, dependencies: container)
        let coordinator = ChangeCardPasswordCoordinator()
        let viewController = ChangeCardPasswordViewController(viewModel: viewModel, coordinator: coordinator)
        coordinator.viewController = viewController
        coordinator.delegate = delegate
        viewModel.outputs = viewController
        return viewController
    }
}
