import UIKit
import UI

public class ChangeCardPasswordViewController: LegacyViewController<ChangeCardPasswordViewModelType, ChangeCardPasswordCoordinating, UIView> {
    private var digitViewController = DigitFormViewController()

    override public func viewDidLoad() {
        super.viewDidLoad()
        viewModel.inputs.viewDidLoad()
        digitViewController.delegate = self
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    private func addChildViewController() {
        addChild(digitViewController)
        view.addSubview(digitViewController.view)
        digitViewController.didMove(toParent: self)
    }
    private func addConstraints() {
        digitViewController.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.constraintAllEdges(from: digitViewController.view, to: view)
    }
    
    override public func configureViews() {
        addChildViewController()
        addConstraints()
        configureNavigationBarButtons(enable: true)
    }
    private func configureNavigationBarButtons(enable: Bool) {
        navigationItem.hidesBackButton = !enable
        if enable {
            addHelperNavigationButton()
        } else {
            navigationItem.rightBarButtonItem = nil
        }
    }
    private func addHelperNavigationButton() {
        let helpIcon = Assets.icoExclamationMark.image
        let helpButton = UIBarButtonItem(image: helpIcon, style: .plain, target: self, action: #selector(didTapHelpButton))
        navigationItem.rightBarButtonItems = [helpButton]
    }
    @objc
    private func didTapHelpButton() {
        viewModel.inputs.didTapHelper()
    }
}

extension ChangeCardPasswordViewController: ChangeCardPasswordViewModelOutputs {
    public func show(errorDescription: String) {
        digitViewController.show(errorDescription: errorDescription)
    }
    public func present(_ presenter: DigitFormPresenting) {
        title = presenter.title
        digitViewController.presenter = presenter
    }
    public func perform(action: ChangeCardPasswordAction) {
        coordinator.perform(action: action)
    }
    public func didAuthenticate() {
        view.endEditing(true)
        digitViewController.startLoading()
    }
}

extension ChangeCardPasswordViewController: DigitFormViewControllerDelegate {
    public func didTapConfirm(with digits: String) {
        viewModel.inputs.didTapConfirmWithAuthentication(and: digits)
    }
    public func didUpdateState(state: DigitFormViewControllerState) {
        configureNavigationBarButtons(enable: !state.isLoading)
    }
}
