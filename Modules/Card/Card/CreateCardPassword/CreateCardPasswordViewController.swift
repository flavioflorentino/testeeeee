import UIKit
import AnalyticsModule
import Foundation
import UI

protocol CreateCardPasswordDisplay: AnyObject {
    func present(_ presenter: DigitFormPresenting)
    func show(errorDescription: String)
    func didAuthenticate()
}

public class CreateCardPasswordViewController: ViewController<CreateCardPasswordInteracting, UIView> {
    private var digitViewController = DigitFormViewController()
    private var isPasswordHidden = true
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        interactor.updateView()
        digitViewController.delegate = self
    }
    private func addChildViewController() {
        addChild(digitViewController)
        view.addSubview(digitViewController.view)
        digitViewController.didMove(toParent: self)
    }
    
    private func addConstraints() {
        digitViewController.view.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    override public func configureViews() {
        addChildViewController()
        addConstraints()
        configureNavigationButtons(enable: true)
    }
    
    private func configureNavigationButtons(enable: Bool) {
        navigationItem.hidesBackButton = !enable
        if enable {
            addHelperNavigationButton()
        } else {
            navigationItem.rightBarButtonItems = nil
        }
    }
    
    private func addHelperNavigationButton() {
        let helpIcon = Assets.icoExclamationMark.image
        let helpButton = UIBarButtonItem(image: helpIcon, style: .plain, target: self, action: #selector(didTapHelpButton))
        navigationItem.rightBarButtonItems = [helpButton]
    }
    
    @objc
    private func didTapHelpButton() {
        interactor.didTapHelper()
        Analytics.shared.log(UnblockCardEvent.newPassword(action: .infoButtonTap, state: isPasswordHidden))
    }
}

extension CreateCardPasswordViewController: DigitFormViewControllerDelegate {
    public func didTapConfirm(with digits: String) {
        Analytics.shared.log(UnblockCardEvent.newPassword(action: .finishTap, state: isPasswordHidden))
        interactor.didTapConfirmWithAuthentication(and: digits)
    }
    
    public func didUpdateState(state: DigitFormViewControllerState) {
        configureNavigationButtons(enable: !state.isLoading)
    }
    
    public func didHiddenPassword(isHidden: Bool) {
        self.isPasswordHidden = isHidden
        Analytics.shared.log(UnblockCardEvent.newPassword(action: .eyeButtonTap, state: isPasswordHidden))
    }
}

extension CreateCardPasswordViewController: CreateCardPasswordDisplay {
    func present(_ presenter: DigitFormPresenting) {
        self.title = presenter.title
        digitViewController.presenter = presenter
    }
    
    func show(errorDescription: String) {
        digitViewController.show(errorDescription: errorDescription)
    }
    
    func didAuthenticate() {
        view.endEditing(true)
        digitViewController.startLoading()
    }
}
