import Foundation

enum CreateCardPasswordFactory {
    static func make(_ delegate: CreateCardPasswordCoordinatorDelegate) -> CreateCardPasswordViewController {
        let dependencies = DependencyContainer()
        let service = CreateCardPasswordService(dependencies: dependencies)
        let coordinator = CreateCardPasswordCoordinator()
        let presenter = CreateCardPasswordPresenter(coordinator: coordinator)
        let interactor = CreateCardPasswordInteractor(presenter: presenter, service: service, dependencies: dependencies)
        
        let viewController = CreateCardPasswordViewController(interactor: interactor)
        
        presenter.viewController = viewController
        coordinator.viewController = viewController
        coordinator.delegate = delegate
        
        return viewController
    }
}
