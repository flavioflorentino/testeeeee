import UIKit
public enum CreateCardPasswordAction {
    case success
    case securityTips
}

public protocol CreateCardPasswordCoordinatorDelegate: AnyObject {
    func didOpenHelperSecurityTips()
    func didSuccessCardPassword()
}

public protocol CreateCardPasswordCoordinating {
    var viewController: UIViewController? { get set }
    var delegate: CreateCardPasswordCoordinatorDelegate? { get set }
    func perform(action: CreateCardPasswordAction)
}

public final class CreateCardPasswordCoordinator: CreateCardPasswordCoordinating {
    public var viewController: UIViewController?
    public weak var delegate: CreateCardPasswordCoordinatorDelegate?
    
    public func perform(action: CreateCardPasswordAction) {
        switch action {
        case .success:
            delegate?.didSuccessCardPassword()
        case .securityTips:
            delegate?.didOpenHelperSecurityTips()
        }
    }
}
