import UIKit

public struct CreateCardPasswordModel: DigitFormPresenting {
    public let title: String
    public let description: String
    public let confirmTitle: String
    public let isSecurityCode: Bool
    public var helperTitle: String?

    init() {
        title = Strings.CreateCardPassword.title
        description = Strings.CreateCardPassword.description
        confirmTitle = Strings.CreateCardPassword.confirmButton
        isSecurityCode = true
    }
}

protocol CreateCardPasswordPresenting: AnyObject {
    var viewController: CreateCardPasswordDisplay? { get set }
    func updateView()
    func show(errorDescription: String)
    func didNextStep(action: CreateCardPasswordAction)
}

final class CreateCardPasswordPresenter: CreateCardPasswordPresenting {
    private let coordinator: CreateCardPasswordCoordinating
    weak var viewController: CreateCardPasswordDisplay?

    init(coordinator: CreateCardPasswordCoordinating) {
        self.coordinator = coordinator
    }
    
    func updateView() {
        viewController?.present(CreateCardPasswordModel())
    }
    
    func show(errorDescription: String) {
        viewController?.show(errorDescription: errorDescription)
    }
    
    func didNextStep(action: CreateCardPasswordAction) {
        coordinator.perform(action: action)
    }
}
