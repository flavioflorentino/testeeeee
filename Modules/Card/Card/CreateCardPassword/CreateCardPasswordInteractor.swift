import AnalyticsModule
import Core
import Foundation

public protocol CreateCardPasswordInteracting {
    func updateView()
    func didTapConfirmWithAuthentication(and digits: String)
    func didTapHelper()
}

public final class CreateCardPasswordInteractor {
    typealias Dependencies = HasAnalytics & HasAuthManagerDependency
    private let dependencies: Dependencies
    
    private let presenter: CreateCardPasswordPresenting
    private let service: CreateCardPasswordServicing
    
    init(presenter: CreateCardPasswordPresenting, service: CreateCardPasswordServicing, dependencies: Dependencies) {
        self.presenter = presenter
        self.service = service
        self.dependencies = dependencies
    }
    
    // MARK: - Private Methods
    private func sendAnalyticsError(errorMessage: String) {
        let passwordError = UnblockCardEvent.DebitUnblockErrorState(message: errorMessage)
        if passwordError == .authentication {
            dependencies.analytics.log(UnblockCardEvent.newPasswordAuthenticated(state: .failed))
        } else {
            dependencies.analytics.log(UnblockCardEvent.newPasswordError(state: passwordError))
        }
    }
}

// MARK: - CreateCardPasswordViewModelInputs
extension CreateCardPasswordInteractor: CreateCardPasswordInteracting {
    public func updateView() {
        presenter.updateView()
    }
    
    public func didTapConfirmWithAuthentication(and digits: String) {
        dependencies.authManager.authenticate { [weak self] appPassword, isBiometric in
            Analytics.shared.log(UnblockCardEvent.newPasswordAuth(action: isBiometric ? .byometricPassword : .numeralPassword))
            self?.service.createCardPassword(digits, password: appPassword) { [weak self] result in
                self?.resultHandler(result: result)
            }
        } cancelHandler: {
            self.dependencies.analytics.log(UnblockCardEvent.newPasswordAuth(action: .cancel))
        }
    }
    
    private func resultHandler(result: Result<Void, ApiError>) {
        switch result {
        case .success:
            dependencies.analytics.log(UnblockCardEvent.newPasswordAuthenticated(state: .validaded))
            presenter.didNextStep(action: .success)
        case .failure(.badRequest(let picPayError)),
             .failure(.notFound(let picPayError)),
             .failure(.unauthorized(let picPayError)):
            sendAnalyticsError(errorMessage: picPayError.message)
            let message = picPayError.message
            presenter.show(errorDescription: message)
        case .failure:
            dependencies.analytics.log(UnblockCardEvent.newPasswordAuthenticated(state: .failed))
            presenter.show(errorDescription: Strings.CreateCardPassword.defaultFailureMessage)
        }
    }
    
    public func didTapHelper() {
        presenter.didNextStep(action: .securityTips)
    }
}
