import Core
import Foundation

public protocol CreateCardPasswordServicing {
    func createCardPassword(_ cardPin: String, password: String, then completion: @escaping (Result<Void, ApiError>) -> Void)
}

public final class CreateCardPasswordService: CreateCardPasswordServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    public func createCardPassword(_ cardPin: String, password: String, then completion: @escaping (Result<Void, ApiError>) -> Void) {
        let api = Api<NoContent>(endpoint: CreditServiceEndpoint.createPassword(cardPin: cardPin, password: password))
        api.execute { result in
            self.dependencies.mainQueue.async {
                completion(result.map { _ in })
            }
        }
    }
}
