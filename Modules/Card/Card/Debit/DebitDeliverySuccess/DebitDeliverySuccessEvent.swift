import AnalyticsModule

public enum DebitDeliverySuccessEvent: AnalyticsKeyProtocol {
    case didViewScreen
    case didSelectConfirm
    
    private var name: String {
        switch self {
        case .didViewScreen, .didSelectConfirm:
            return "Sign Up - Approved"
        }
    }
    
    private var properties: [String: Any] {
        guard case .didSelectConfirm = self else { return [:] }
        return ["action": "act-debit-approved"]
    }
    
    private var providers: [AnalyticsProvider] {
        [.appsFlyer, .mixPanel, .firebase]
    }
    
    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PicPay Card - Request - \(name)", properties: properties, providers: providers)
    }
}
