import UIKit

enum DebitDeliverySuccessAction {
    case didConfirm
}

protocol DebitDeliverySuccessCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: DebitDeliverySuccessAction)
}

final class DebitDeliverySuccessCoordinator: DebitDeliverySuccessCoordinating {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    weak var viewController: UIViewController?
    private var debitCoordinatorOutput: (DebitCoordinatorOutput) -> Void

    init(dependencies: Dependencies, debitCoordinatorOutput: @escaping (DebitCoordinatorOutput) -> Void) {
        self.dependencies = dependencies
        self.debitCoordinatorOutput = debitCoordinatorOutput
    }
    
    func perform(action: DebitDeliverySuccessAction) {
        debitCoordinatorOutput(.close)
    }
}
