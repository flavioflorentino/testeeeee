import AnalyticsModule
import FeatureFlag
import Foundation

public protocol DebitDeliverySuccessViewModelInputs: AnyObject {
    func viewDidLoad()
    func trackScreenView()
    func didConfirm()
}

final class DebitDeliverySuccessViewModel {
    typealias Dependencies = HasAnalytics & HasFeatureManager
    private let dependencies: Dependencies

    private let presenter: DebitDeliverySuccessPresenting

    init(presenter: DebitDeliverySuccessPresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

extension DebitDeliverySuccessViewModel: DebitDeliverySuccessViewModelInputs {
    func viewDidLoad() {
        let description = dependencies.featureManager.text(.debitCardRequestedText)
        presenter.loadDescription(text: description.isEmpty ? Strings.DebitDeliverySuccess.description : description)
    }
    
    func trackScreenView() {
        dependencies.analytics.log(DebitDeliverySuccessEvent.didViewScreen)
    }
    
    func didConfirm() {
        dependencies.analytics.log(DebitDeliverySuccessEvent.didSelectConfirm)
        presenter.didNextStep(action: .didConfirm)
    }
}
