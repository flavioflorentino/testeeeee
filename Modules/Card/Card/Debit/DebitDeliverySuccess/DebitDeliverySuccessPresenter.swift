import Core
import Foundation

protocol DebitDeliverySuccessPresenting: AnyObject {
    var viewController: DebitDeliverySuccessDisplaying? { get set }
    func loadDescription(text: String)
    func didNextStep(action: DebitDeliverySuccessAction)
}

final class DebitDeliverySuccessPresenter: DebitDeliverySuccessPresenting {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let coordinator: DebitDeliverySuccessCoordinating
    weak var viewController: DebitDeliverySuccessDisplaying?

    init(coordinator: DebitDeliverySuccessCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
    
    func loadDescription(text: String) {
        viewController?.displayDescription(text: text)
    }
    
    func didNextStep(action: DebitDeliverySuccessAction) {
        coordinator.perform(action: action)
    }
}
