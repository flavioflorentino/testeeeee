import UI
import UIKit

public protocol DebitDeliverySuccessDisplaying: AnyObject {
    func displayDescription(text: String)
}

extension DebitDeliverySuccessViewController.Layout {
    enum Size {
        static let paragraphLineSpace: CGFloat = 5.0
    }
    enum Font {
        static let title = UIFont.systemFont(ofSize: 28, weight: .bold)
        static let description = UIFont.systemFont(ofSize: 16, weight: .light)
        static let descriptionSemiBold = UIFont.systemFont(ofSize: 16, weight: .semibold)
    }
}

public final class DebitDeliverySuccessViewController: ViewController<DebitDeliverySuccessViewModelInputs, UIView> {
    fileprivate enum Layout {}

    private lazy var itemsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = Spacing.base03
        stackView.distribution = .equalSpacing
        return stackView
    }()
    
    private lazy var cardImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = Assets.cardEnvelope.image
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.DebitDeliverySuccess.title
        label.font = Layout.Font.title
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle()).with(\.textColor, .grayscale900()).with(\.textAlignment, .center)
        return label
    }()

    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didConfirm), for: .touchUpInside)
        button.setTitle(Strings.DebitDeliverySuccess.confirmButton, for: .normal)
        return button
    }()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(true, animated: true)
        viewModel.viewDidLoad()
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.trackScreenView()
    }
    
    @objc
    private func didConfirm() {
        viewModel.didConfirm()
    }
 
    override public func buildViewHierarchy() {
        view.addSubview(itemsStackView)
        itemsStackView.addArrangedSubview(cardImage)
        itemsStackView.addArrangedSubview(titleLabel)
        itemsStackView.addArrangedSubview(descriptionLabel)
        itemsStackView.addArrangedSubview(confirmButton)
    }
    
    override public func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    override public func setupConstraints() {
        itemsStackView.layout {
            $0.centerX == view.centerXAnchor
            $0.centerY == view.centerYAnchor
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
        }
        confirmButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
}

extension DebitDeliverySuccessViewController: DebitDeliverySuccessDisplaying {
    public func displayDescription(text: String) {
        descriptionLabel.text = text
    }
}
