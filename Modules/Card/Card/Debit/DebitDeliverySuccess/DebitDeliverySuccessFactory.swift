import Foundation

public enum DebitDeliverySuccessFactory {
    public static func make(debitCoordinatorOutput: @escaping (DebitCoordinatorOutput) -> Void) -> DebitDeliverySuccessViewController {
        let container = DependencyContainer()
        let coordinator: DebitDeliverySuccessCoordinating = DebitDeliverySuccessCoordinator(
            dependencies: container,
            debitCoordinatorOutput: debitCoordinatorOutput
        )
        let presenter: DebitDeliverySuccessPresenting = DebitDeliverySuccessPresenter(
            coordinator: coordinator,
            dependencies: container
        )
        let viewModel = DebitDeliverySuccessViewModel(presenter: presenter, dependencies: container)
        let viewController = DebitDeliverySuccessViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
