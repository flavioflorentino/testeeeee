import AnalyticsModule

public enum DebitPersonalInfoEvent: AnalyticsKeyProtocol {
    case didViewScreen
    case didSelectContinue
    
    private var name: String {
        switch self {
        case .didViewScreen, .didSelectContinue:
            return "Sign Up - Mother Name"
        }
    }
    
    private var properties: [String: Any] {
        guard case .didSelectContinue = self else { return [:] }
        return ["action": "act-next-step"]
    }
    
    private var providers: [AnalyticsProvider] {
        [.appsFlyer, .mixPanel, .firebase]
    }
    
    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PicPay Card - Request - \(name)", properties: properties, providers: providers)
    }
}
