protocol DebitPersonalInfoDisplay: AnyObject {
    func updateTextFieldErrorMessage(message: String?)
    func enableContinueButton(enable: Bool)
    func updateMothersNameTextFieldWith(string: String)
}
