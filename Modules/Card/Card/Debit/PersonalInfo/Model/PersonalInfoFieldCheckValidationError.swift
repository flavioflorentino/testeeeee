import Validator

enum PersonalInfoFieldCheckValidationError: String, ValidationError {
    case motherNameRequired = "Nome da mãe obrigatório"
    
    var message: String {
        self.rawValue
    }
}
