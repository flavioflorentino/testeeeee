import Core
import Foundation

protocol DebitPersonalInfoServicing {
    typealias DebitSyncCompletion = (Result<CardForm, ApiError>) -> Void
    
    func syncModel(cardForm: CardForm)
}

final class DebitPersonalInfoService: DebitPersonalInfoServicing {
    typealias Dependencies = HasDebitAPIManagerDependency
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func syncModel(cardForm: CardForm) {
        dependencies.debitAPIManager.setDebitRegistration(cardForm: cardForm, completion: nil)
    }
}
