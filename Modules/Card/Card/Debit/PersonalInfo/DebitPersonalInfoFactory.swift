import Foundation

public enum DebitPersonalInfoFactory {
    public static func make(
        cardForm: CardForm,
        debitCoordinatorOutput: @escaping (DebitCoordinatorOutput) -> Void,
        delegate: DebitPersonalInfoCoordinatorDelegate
    ) -> DebitPersonalInfoViewController {
        let container = DependencyContainer()
        let service: DebitPersonalInfoServicing = DebitPersonalInfoService(dependencies: container)
        let coordinator: DebitPersonalInfoCoordinating = DebitPersonalInfoCoordinator(
            dependencies: container,
            debitCoordinatorOutput: debitCoordinatorOutput
        )
        let presenter: DebitPersonalInfoPresenting = DebitPersonalInfoPresenter(coordinator: coordinator, dependencies: container)
        let viewModel = DebitPersonalInfoViewModel(service: service, presenter: presenter, dependencies: container, cardForm: cardForm)
        let viewController = DebitPersonalInfoViewController(viewModel: viewModel)
        
        coordinator.viewController = viewController
        coordinator.delegate = delegate
        presenter.viewController = viewController

        return viewController
    }
}
