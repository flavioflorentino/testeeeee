import Core
import Foundation

protocol DebitPersonalInfoPresenting: AnyObject {
    var viewController: DebitPersonalInfoDisplay? { get set }
    func didNextStep(action: DebitPersonalInfoAction)
    func updateMotherNameTextField(validText: String)
    func updateTextFieldErrorMessage(message: String?)
    func shouldEnableContinueButton(enable: Bool)
}

final class DebitPersonalInfoPresenter: DebitPersonalInfoPresenting {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let coordinator: DebitPersonalInfoCoordinating
    weak var viewController: DebitPersonalInfoDisplay?

    init(coordinator: DebitPersonalInfoCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
    
    func didNextStep(action: DebitPersonalInfoAction) {
        coordinator.perform(action: action)
    }
    
    func updateMotherNameTextField(validText: String) {
        viewController?.updateMothersNameTextFieldWith(string: validText)
    }
    
    func updateTextFieldErrorMessage(message: String?) {
        viewController?.updateTextFieldErrorMessage(message: message)
    }
    
    func shouldEnableContinueButton(enable: Bool) {
        viewController?.enableContinueButton(enable: enable)
    }
}
