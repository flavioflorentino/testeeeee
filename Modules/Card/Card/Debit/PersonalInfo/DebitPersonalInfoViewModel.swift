import AnalyticsModule
import Foundation
import Validator

public protocol DebitPersonalInfoViewModelInputs: AnyObject {
    func trackScreenView()
    func setupInitialForm()
    func close()
    func sanitizedMothersName(with name: String?)
    func checkMothersName(text: String?)
    func validateAndEnableContinue(text: String)
    func confirm(motherName: String?)
}

final class DebitPersonalInfoViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let service: DebitPersonalInfoServicing
    private let presenter: DebitPersonalInfoPresenting
    private var cardForm: CardForm
    
    init(service: DebitPersonalInfoServicing, presenter: DebitPersonalInfoPresenting, dependencies: Dependencies, cardForm: CardForm) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.cardForm = cardForm
    }
    
    // MARK: Private Methods
    private func ruleMothersName() -> ValidationRuleSet<String> {
        var rule = ValidationRuleSet<String>()
        rule.add(rule: ValidationRuleLength(min: 2, max: 100, error: PersonalInfoFieldCheckValidationError.motherNameRequired))
        return rule
    }
}

extension DebitPersonalInfoViewModel: DebitPersonalInfoViewModelInputs {
    func trackScreenView() {
        dependencies.analytics.log(DebitPersonalInfoEvent.didViewScreen)
    }
    
    func setupInitialForm() {
        sanitizedMothersName(with: cardForm.mothersName)
        checkMothersName(text: cardForm.mothersName)
    }
    
    func close() {
        presenter.didNextStep(action: .close)
    }
    
    func sanitizedMothersName(with name: String?) {
        guard let text = name else {
            return
        }
        
        let validText = text.replacingOccurrences(
            of: "[^a-zA-Z\u{00C0}-\u{00FF} ]",
            with: "",
            options: .regularExpression
        )
        presenter.updateMotherNameTextField(validText: validText)
    }
    
    func checkMothersName(text: String?) {
        guard let text = text else {
            return
        }
        
        let result = text.validate(rules: ruleMothersName())
        var message: String?

        if case let .invalid(error) = result,
            let err = error.first {
            message = err.message
        }
        presenter.updateTextFieldErrorMessage(message: message)
    }
    
    func validateAndEnableContinue(text: String) {
        let result = text.validate(rules: ruleMothersName())
        presenter.shouldEnableContinueButton(enable: result.isValid)
    }
    
    func confirm(motherName: String?) {
        guard let motherName = motherName else {
            return
        }
        cardForm.mothersName = motherName
        cardForm.registrationStep = .personalData
        service.syncModel(cardForm: cardForm)
        presenter.didNextStep(action: .confirm(cardForm: cardForm))
        dependencies.analytics.log(DebitPersonalInfoEvent.didSelectContinue)
    }
}
