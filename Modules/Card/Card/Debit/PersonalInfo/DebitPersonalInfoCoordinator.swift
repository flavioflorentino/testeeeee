import UIKit

enum DebitPersonalInfoAction {
    case confirm(cardForm: CardForm)
    case close
}

public protocol DebitPersonalInfoCoordinatorDelegate: AnyObject {
    func debitPersonalInfoDidClose()
}

protocol DebitPersonalInfoCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: DebitPersonalInfoCoordinatorDelegate? { get set }
    func perform(action: DebitPersonalInfoAction)
}

final class DebitPersonalInfoCoordinator: DebitPersonalInfoCoordinating {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies
    weak var delegate: DebitPersonalInfoCoordinatorDelegate?
    
    weak var viewController: UIViewController?
    private var debitCoordinatorOutput: (DebitCoordinatorOutput) -> Void
    
    init(dependencies: Dependencies, debitCoordinatorOutput: @escaping (DebitCoordinatorOutput) -> Void) {
        self.dependencies = dependencies
        self.debitCoordinatorOutput = debitCoordinatorOutput
    }
    
    func perform(action: DebitPersonalInfoAction) {
        if case let .confirm(cardForm) = action {
            debitCoordinatorOutput(DebitCoordinatorOutput.update(
                cardForm: cardForm,
                currentStep: .personalData)
            )
        }
        if case .close = action {
            delegate?.debitPersonalInfoDidClose()
        }
    }
}
