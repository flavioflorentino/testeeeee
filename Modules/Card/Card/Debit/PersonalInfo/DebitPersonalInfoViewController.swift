import UI
import UIKit

private extension DebitPersonalInfoViewController.Layout {
    enum Fonts {
        static let light = UIFont.systemFont(ofSize: 16, weight: .light)
    }
    enum Color {
        static let branding = Palette.ppColorBranding300.color
    }
}

public final class DebitPersonalInfoViewController: ViewController<DebitPersonalInfoViewModelInputs, UIView> {
    fileprivate enum Layout { }

    private lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.bounces = false
        return scroll
    }()
    
    private lazy var scrollViewContentView = UIView()
    private lazy var descriptionLabel: UILabel = {
       let label = UILabel()
        label.textColor = Palette.ppColorGrayscale500.color
        label.font = Layout.Fonts.light
        label.text = Strings.DebitPessoalInfo.description
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var motherNameTextfield: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.defaultForm()
        textfield.delegate = self
        textfield.addTarget(self, action: #selector(checkMotherNameTextfield), for: .editingChanged)
        textfield.placeholder = Strings.DebitPessoalInfo.placeholder
        let doneToolBar = DoneToolBar(doneText: Strings.DebitPessoalInfo.doneToolbar)
        textfield.inputAccessoryView = doneToolBar
        return textfield
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(tapConfirm), for: .touchUpInside)
        button.setTitle(Strings.DebitPessoalInfo.confirm, for: .normal)
        button.isEnabled = false
        return button
    }()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        addCloseButton()
        hideKeyboardWhenTappedAround()
        viewModel.setupInitialForm()
        configureViewPresentation()
    }
    
    private func configureViewPresentation() {
        if #available(iOS 13.0, *) {
            isModalInPresentation = true
        }
        title = Strings.DebitPessoalInfo.title
    }
 
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBarAppearance()
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.trackScreenView()
    }
    
    private func updateNavigationBarAppearance() {
        navigationController?.setNavigationBarHidden(false, animated: false)
        
        navigationController?.navigationBar.tintColor = Layout.Color.branding
        navigationController?.navigationBar.isTranslucent = false
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.isTranslucent = true
            navigationController?.navigationBar.prefersLargeTitles = true
            navigationController?.navigationItem.largeTitleDisplayMode = .always
        }
        
        let backButton = UIBarButtonItem()
        backButton.title = String()
        navigationItem.backBarButtonItem = backButton
    }
    
    private func addCloseButton() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: Assets.icoClose.image,
            style: .plain,
            target: self,
            action: #selector(didTapClose)
        )
    }
    
    override public func buildViewHierarchy() {
        scrollViewContentView.addSubviews(descriptionLabel, motherNameTextfield)
        scrollView.addSubview(scrollViewContentView)
        view.addSubviews(scrollView, confirmButton)
    }
    
    override public func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    override public func setupConstraints() {
        scrollView.layout {
            $0.top == view.topAnchor
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
        }
        scrollViewContentView.layout {
            $0.top == scrollView.topAnchor
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
        }
        descriptionLabel.layout {
            $0.top == scrollViewContentView.topAnchor + Spacing.base03
            $0.leading == scrollViewContentView.leadingAnchor + Spacing.base03
            $0.trailing == scrollViewContentView.trailingAnchor - Spacing.base03
        }
        motherNameTextfield.layout {
            $0.top == descriptionLabel.bottomAnchor + Spacing.base06
            $0.leading == scrollViewContentView.leadingAnchor + Spacing.base03
            $0.trailing == scrollViewContentView.trailingAnchor - Spacing.base03
            $0.bottom == scrollViewContentView.bottomAnchor
        }
        confirmButton.snp.makeConstraints {
            $0.top.equalTo(scrollView.snp.bottom).inset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).offset(-Spacing.base03)
        }
    }
    
    // MARK: - Private Methods
    private func hideKeyboardWhenTappedAround() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
    }
    
    private func updateButtonState(enabled: Bool) {
        confirmButton.isEnabled = enabled
    }
    
    @objc
    private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc
    private func checkMotherNameTextfield() {
        viewModel.sanitizedMothersName(with: motherNameTextfield.text)
        viewModel.checkMothersName(text: motherNameTextfield.text)
    }
    
    @objc
    private func tapConfirm() {
        viewModel.confirm(motherName: motherNameTextfield.text)
    }
    
    @objc
    private func didTapClose() {
        viewModel.close()
    }
}

// MARK: View Model Outputs
extension DebitPersonalInfoViewController: DebitPersonalInfoDisplay {
    func updateTextFieldErrorMessage(message: String?) {
        motherNameTextfield.errorMessage = message
    }
    
    func enableContinueButton(enable: Bool) {
        updateButtonState(enabled: enable)
    }
    
    func updateMothersNameTextFieldWith(string: String) {
        motherNameTextfield.text = string
        viewModel.validateAndEnableContinue(text: string)
    }
}

extension DebitPersonalInfoViewController: UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
