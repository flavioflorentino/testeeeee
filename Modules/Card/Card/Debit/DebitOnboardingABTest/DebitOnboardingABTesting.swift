enum DebitOnboardingABTestingState {
    case a
    case b
}

protocol DebitOnboardingABTestingData {
    var nextStepButtonText: String { get set }
}
