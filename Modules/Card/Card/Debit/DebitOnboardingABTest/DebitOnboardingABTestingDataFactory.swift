enum DebitOnboardingABTestingDataFactory {
    static func make(with type: DebitOnboardingABTestingState) -> DebitOnboardingABTestingData {
        switch type {
        case .a:
            return DebitOnboardingAData()
        case .b:
            return DebitOnboardingBData()
        }
    }
}
