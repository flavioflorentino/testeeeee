import Foundation

public enum ActivateCardSuccessFactory {
    public static func make(_ delegate: ActivateCardFlowCoordinatorDelegate) -> ActivateCardSuccessViewController {
        let container = DependencyContainer()
        var coordinator: ActivateCardSuccessCordinating = ActivateCardSuccessCoordinator()
        let presenter: ActivateCardSuccessPresenting = ActivateCardSuccessPresenter(coordinator: coordinator)
        let viewModel = ActivateCardSuccessViewModel(presenter: presenter, dependencies: container)
        let viewController = ActivateCardSuccessViewController(viewModel: viewModel)
        
        coordinator.delegate = delegate
        viewController.dependencies = container

        return viewController
    }
}
