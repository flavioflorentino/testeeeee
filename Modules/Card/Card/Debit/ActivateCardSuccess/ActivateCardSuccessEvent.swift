import AnalyticsModule

public enum ActivateCardSuccessEvent: AnalyticsKeyProtocol {
    case didBackHome
    case didAddMoney
    
    private var name: String {
        switch self {
        case .didBackHome:
            return "Unblock - Sucess"
        case .didAddMoney:
            return "Unblock - Add Money"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .didAddMoney:
            return ["action": "act-add-money"]
        case .didBackHome:
            return ["action": "act-back-home"]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.appsFlyer]
    }
    
    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PicPay Card - \(name)", properties: properties, providers: providers)
    }
}
