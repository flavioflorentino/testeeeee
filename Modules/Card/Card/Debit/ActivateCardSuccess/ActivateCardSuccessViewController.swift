import AnalyticsModule
import UI
import UIKit

extension ActivateCardSuccessViewController.Layout {
    enum Length {
        static let titleMaxLines = 2
        static let subtitleMaxLines = 3
    }
    enum Font {
        static let title = Typography.title(.xLarge).font()
        static let subtitle = Typography.bodyPrimary(.default).font()
        static let addMoneyToWalletButton = Typography.title(.small).font()
        static let actionMessage = Typography.bodyPrimary(.default).font()
        static let doneMessage = Typography.bodySecondary(.default).font()
        static let actionMessageBold = Typography.bodyPrimary(.highlight).font()
    }
    enum Size {
        static let lineHeightMultiple: CGFloat = 1.26
    }
    enum Paragraph {
        static let lineSpace: CGFloat = 5
        static let aligment: NSTextAlignment = .center
    }
}

public final class ActivateCardSuccessViewController: ViewController<ActivateCardSuccessViewModelInputs, UIView> {
    private typealias Localizable = Strings.ActivateCardSuccess
    public var dependencies: HasAnalytics?
    
    fileprivate enum Layout {}
    
    private lazy var topContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var centerContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var centerImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFit
        imageView.image = Assets.cardActivated.image
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.title
        label.font = Layout.Font.title
        label.textColor = Colors.black.color
        label.textAlignment = .center
        label.numberOfLines = Layout.Length.titleMaxLines
        return label
    }()
    
    private lazy var paragraphStyle: NSParagraphStyle = {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = Layout.Size.lineHeightMultiple
        paragraphStyle.alignment = .center
        paragraphStyle.lineBreakMode = .byWordWrapping
        return paragraphStyle
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = Layout.Length.subtitleMaxLines
        let attributes: [NSAttributedString.Key: Any] = [
          .font: Layout.Font.subtitle,
          .foregroundColor: Colors.grayscale700.color,
          .paragraphStyle: paragraphStyle
        ]

        label.attributedText = NSAttributedString(string: Localizable.subtitle, attributes: attributes)
        return label
    }()
    
    private lazy var bottomContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale050.color
        return view
    }()
    
    private lazy var actionMessageLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = .zero
        let descr = Localizable.messageAction
        
        let descrAtt = NSMutableAttributedString(string: descr)
        descrAtt.font(text: descr, font: Layout.Font.actionMessage)
        descrAtt.font(text: Localizable.PicPayCard.highlight, font: Layout.Font.actionMessageBold)
        descrAtt.font(text: Localizable.PicPay.highlight, font: Layout.Font.actionMessageBold)
        descrAtt.textColor(text: descr, color: Colors.grayscale600.color)
        descrAtt.paragraph(aligment: Layout.Paragraph.aligment, lineSpace: Layout.Paragraph.lineSpace)
                
        label.attributedText = descrAtt
        return label
    }()
    
    private lazy var addMoneyToWalletButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = Colors.branding400.color
        button.addTarget(self, action: #selector(didTapAddMoneyToWallet), for: .touchUpInside)
        button.setTitle(Localizable.addMoneyToWalletButton, for: .normal)
        button.titleLabel?.font = Layout.Font.addMoneyToWalletButton
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.textAlignment = .center
        button.layer.cornerRadius = Spacing.base03
        return button
    }()
    
    private lazy var doneButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(didTapBackToStart), for: .touchUpInside)
        let attributes: [NSAttributedString.Key: Any] = [
            .font: Layout.Font.doneMessage,
            .foregroundColor: Colors.branding500.color,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        let attributedText = NSAttributedString(string: Localizable.doneMessage, attributes: attributes)
        button.setAttributedTitle(attributedText, for: .normal)
        button.titleLabel?.font = Layout.Font.doneMessage
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.textAlignment = .center
        return button
    }()
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationItem.hidesBackButton = true
    }

    override public func configureViews() {
        view.backgroundColor = Colors.white.color
    }
    
    override public func buildViewHierarchy() {
        topContainerView.addSubview(centerContainerView)
        centerContainerView.addSubview(centerImageView)
        centerContainerView.addSubview(titleLabel)
        centerContainerView.addSubview(subtitleLabel)
        
        bottomContainerView.addSubview(actionMessageLabel)
        bottomContainerView.addSubview(addMoneyToWalletButton)
        bottomContainerView.addSubview(doneButton)
        
        view.addSubview(topContainerView)
        view.addSubview(bottomContainerView)
    }

    override public func setupConstraints() {
        topContainerView.layout {
            $0.top == view.topAnchor
            $0.bottom == bottomContainerView.topAnchor
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
        }
        
        centerContainerView.layout {
            $0.centerY == topContainerView.centerYAnchor
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
        }
        
        bottomContainerView.layout {
            $0.bottom == view.bottomAnchor
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
        }

        setupStructureConstraints()
    }
    
    private func setupStructureConstraints() {
        centerImageView.layout {
            $0.top == centerContainerView.topAnchor
            $0.leading == centerContainerView.leadingAnchor + Spacing.base09
            $0.trailing == centerContainerView.trailingAnchor - Spacing.base09
        }

        titleLabel.layout {
            $0.top == centerImageView.bottomAnchor
            $0.centerX == centerContainerView.centerXAnchor
            $0.leading == centerContainerView.leadingAnchor + Spacing.base02
            $0.trailing == centerContainerView.trailingAnchor - Spacing.base02
        }
        
        subtitleLabel.layout {
            $0.top == titleLabel.bottomAnchor + Spacing.base01
            $0.bottom == centerContainerView.bottomAnchor
            $0.centerX == centerContainerView.centerXAnchor
            $0.leading == centerContainerView.leadingAnchor + Spacing.base02
            $0.trailing == centerContainerView.trailingAnchor - Spacing.base02
        }
        
        actionMessageLabel.layout {
            $0.top == bottomContainerView.topAnchor + Spacing.base02
            $0.centerX == bottomContainerView.centerXAnchor
            $0.leading == bottomContainerView.leadingAnchor + Spacing.base02
            $0.trailing == bottomContainerView.trailingAnchor - Spacing.base02
        }

        addMoneyToWalletButton.layout {
            $0.top == actionMessageLabel.bottomAnchor + Spacing.base02
            $0.centerX == bottomContainerView.centerXAnchor
            $0.height == Spacing.base06
            $0.leading == bottomContainerView.leadingAnchor + Spacing.base02
            $0.trailing == bottomContainerView.trailingAnchor - Spacing.base02
        }
        
        doneButton.layout {
            $0.top == addMoneyToWalletButton.bottomAnchor + Spacing.base01
            $0.bottom == bottomContainerView.compatibleSafeAreaLayoutGuide.bottomAnchor - Spacing.base01
            $0.centerX == bottomContainerView.centerXAnchor
        }
    }
    
    private func updateNavigationBarAppearance() {
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        }
    }
}

@objc
extension ActivateCardSuccessViewController {
    private func didTapAddMoneyToWallet() {
        dependencies?.analytics.log(UnblockCardEvent.success(action: .addMoney))
        viewModel.addMoneyToWallet()
    }
    
    private func didTapBackToStart() {
        dependencies?.analytics.log(UnblockCardEvent.success(action: .backHome))
        viewModel.backToStart()
    }
}
