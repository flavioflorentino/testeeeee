import Foundation

protocol ActivateCardSuccessPresenting: AnyObject {
    func backToStart()
    func addMoneyToWallet()
}

final class ActivateCardSuccessPresenter: ActivateCardSuccessPresenting {
    private let coordinator: ActivateCardSuccessCordinating

    init(coordinator: ActivateCardSuccessCordinating) {
       self.coordinator = coordinator
    }
    
    func backToStart() {
        coordinator.perform(action: .backToStart)
    }
    
    func addMoneyToWallet() {
        coordinator.perform(action: .addMoneyToWallet)
    }
}
