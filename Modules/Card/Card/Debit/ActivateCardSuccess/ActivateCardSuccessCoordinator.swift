import UIKit

public enum ActivateCardSuccessAction {
    case backToStart
    case addMoneyToWallet
}

protocol ActivateCardSuccessCordinating {
    var delegate: ActivateCardFlowCoordinatorDelegate? { get set }
    func perform(action: ActivateCardSuccessAction)
}

final class ActivateCardSuccessCoordinator: ActivateCardSuccessCordinating {
    weak var delegate: ActivateCardFlowCoordinatorDelegate?
    
    func perform(action: ActivateCardSuccessAction) {
        switch action {
        case .addMoneyToWallet:
            delegate?.didOpenWallet()
        case .backToStart:
            delegate?.didFinishFlow()
        }
    }
}
