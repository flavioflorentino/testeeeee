import AnalyticsModule

public protocol ActivateCardSuccessViewModelInputs: AnyObject {
    func backToStart()
    func addMoneyToWallet()
}

final class ActivateCardSuccessViewModel {    
    typealias Dependencies = HasAnalytics
    
    private let dependencies: Dependencies
    private let presenter: ActivateCardSuccessPresenting
    
    init(presenter: ActivateCardSuccessPresenting, dependencies: Dependencies) {
        self.dependencies = dependencies
        self.presenter = presenter
    }
}

extension ActivateCardSuccessViewModel: ActivateCardSuccessViewModelInputs {
    func backToStart() {
        dependencies.analytics.log(ActivateCardSuccessEvent.didBackHome)
        presenter.backToStart()
    }
    
    func addMoneyToWallet() {
        dependencies.analytics.log(ActivateCardSuccessEvent.didAddMoney)
        presenter.addMoneyToWallet()
    }
}
