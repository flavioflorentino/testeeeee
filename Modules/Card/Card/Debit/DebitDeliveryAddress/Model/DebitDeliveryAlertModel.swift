import Foundation
import UIKit
import AssetsKit

public struct DebitDeliveryAlertModel {
    let title: String
    let descriptionMessage: String
    let image: UIImage?
    let primaryButtonTitle: String
    let secondaryButtonTitle: String
}
