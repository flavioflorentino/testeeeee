import Foundation

public class DeliveryAddressFormsData {
    var postalCode: String
    var streetName: String
    var neighborhood: String
    var streetNumber: String
    var complement: String
    var city: String
    var cityId: String
    var state: String
    var stateId: String
    var noNumberButtonStatus: Bool
    
    public init(
        postalCode: String?,
        streetName: String?,
        neighborhood: String?,
        streetNumber: String?,
        complement: String?,
        city: String?,
        cityId: String?,
        state: String?,
        stateId: String?,
        noNumberButtonStatus: Bool
    ) {
        self.postalCode = postalCode ?? ""
        self.streetName = streetName ?? ""
        self.neighborhood = neighborhood ?? ""
        self.streetNumber = streetNumber ?? ""
        self.complement = complement ?? ""
        self.city = city ?? ""
        self.cityId = cityId ?? ""
        self.state = state ?? ""
        self.stateId = stateId ?? ""
        self.noNumberButtonStatus = noNumberButtonStatus
    }
}
