import UIKit
import AssetsKit
struct DebitDeliveryErrorData {
    let title: String
    let descriptionMessage: String
    let imageName: AssetsKit.ImageAsset
    let errorType: DebitDeliveryErrorType
}
