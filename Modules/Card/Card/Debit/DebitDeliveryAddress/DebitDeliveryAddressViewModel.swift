import AnalyticsModule
import Core
import Foundation
import Validator
import FeatureFlag

public enum DebitDeliveryAddressPostalCodeState {
    case initial
    case filledPostCode
    case invalid
}

public protocol DebitDeliveryAddressViewModelInputs: AnyObject {
    func addressBy(_ cep: String)
    func enableStateAndCityEdition(postalCodeIsValid: Bool, state: String, city: String)
    func sendFormsData(formData: DeliveryAddressFormsData)
    func viewDidLoad()
    
    func checkPostalCodeFieldWith(validationResult: ValidationResult)
    func checkStreetFieldWith(validationResult: ValidationResult)
    func checkNeighborhoodFieldWith(validationResult: ValidationResult)
    func checkStreetNumberFieldWith(validationResult: ValidationResult)
    func checkComplementFieldWith(validationResult: ValidationResult)
    func checkStateFieldWith(validationResult: ValidationResult)
    func checkCityFieldWith(validationResult: ValidationResult)
    func openPostOffices()
    func resetToBeginState()
    func validationAddressConfirm(formData: DeliveryAddressFormsData)
    func trackingNoNumber(checked: Bool)
    func viewWillPop()
}

final class DebitDeliveryAddressViewModel {
    typealias Dependencies = HasDebitAPIManagerDependency & HasAnalytics & HasFeatureManager
    private let dependencies: Dependencies

    internal var state: DebitDeliveryAddressPostalCodeState = .initial
    private let service: DebitDeliveryAddressServicing
    private let presenter: DebitDeliveryAddressPresenting
    internal var cardForm: CardForm
    private var searchedAddress: HomeAddress?
    init(service: DebitDeliveryAddressServicing, presenter: DebitDeliveryAddressPresenting, dependencies: Dependencies, cardForm: CardForm) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.cardForm = cardForm
    }
    
    private func validationErrorMessageFor(result: ValidationResult) -> String? {
        guard
            case let .invalid(error) = result,
            let err = error.first
            else {
                return nil
        }
        
        return err.message
    }
    
    func checkPostalCodeFieldWith(validationResult: ValidationResult) {
        presenter.presentPostalCodeFieldWith(errorMessage: validationErrorMessageFor(result: validationResult))
    }
    
    func checkStreetFieldWith(validationResult: ValidationResult) {
        presenter.presentStreetFieldWith(errorMessage: validationErrorMessageFor(result: validationResult))
    }
    
    func checkNeighborhoodFieldWith(validationResult: ValidationResult) {
        presenter.presentNeighborhoodFieldWith(errorMessage: validationErrorMessageFor(result: validationResult))
    }
    
    func checkStreetNumberFieldWith(validationResult: ValidationResult) {
        presenter.presentStreetNumberFieldWith(errorMessage: validationErrorMessageFor(result: validationResult))
    }
    
    func checkComplementFieldWith(validationResult: ValidationResult) {
        presenter.presentComplementFieldWith(errorMessage: validationErrorMessageFor(result: validationResult))
    }
    
    func checkCityFieldWith(validationResult: ValidationResult) {
        presenter.presentCityFieldWith(errorMessage: validationErrorMessageFor(result: validationResult))
    }
    
    func checkStateFieldWith(validationResult: ValidationResult) {
        presenter.presentStateFieldWith(errorMessage: validationErrorMessageFor(result: validationResult))
    }
}

extension DebitDeliveryAddressViewModel: DebitDeliveryAddressViewModelInputs {
    func validationAddressConfirm(formData: DeliveryAddressFormsData) {
        if userChangedAddressData(deliveryAddress: formData) {
            presenter.presentAlertWarning()
            dependencies.analytics.log(CardDeliveryAddressEvent.didTapContinue(editedAddress: true))
        } else {
            presenter.presentLoading()
            sendFormsData(formData: formData)
            dependencies.analytics.log(CardDeliveryAddressEvent.didTapContinue(editedAddress: false))
        }
    }
    
    func enableStateAndCityEdition(postalCodeIsValid: Bool, state: String, city: String) {
        presenter.enableStateAndCityEdition(postalCodeIsValid: postalCodeIsValid, state: state, city: city)
    }
    
    public func addressBy(_ cep: String) {
        service.addressBy(cep) { [weak self] result in
            switch result {
            case .success(let addressSearch):
                guard let self = self else { return }
                self.searchedAddress = addressSearch
                self.state = .filledPostCode
                if self.dependencies.featureManager.isActive(.isNewCardAddressScreenAvailable) {
                    self.presenter.startAddressCompletionAnimation(with: addressSearch)
                } else {
                    self.presenter.setAddressInController(with: addressSearch)
                    self.presenter.presentState(state: .filledPostCode)
                }
                self.dependencies.analytics.log(CardDeliveryAddressEvent.didGetPostalCode(result: true))
            case .failure(let error):
                self?.searchedAddress = nil
                self?.state = .invalid
                self?.presenter.presentState(state: .invalid)
                self?.presenter.presentPostalCodeError(error)
                self?.dependencies.analytics.log(CardDeliveryAddressEvent.didGetPostalCode(result: false))
            }
        }
    }
    
    func sendFormsData(formData: DeliveryAddressFormsData) {
        let cardForm = update(cardForm: self.cardForm, with: formData)
        service.update(cardForm: cardForm) { [weak self] result in
            guard let self = self else {
                return
            }
            switch result {
            case .success(let cardForm):
                self.cardForm.registrationStep = .address
                self.presenter.didNextStep(action: .didConfirm(cardForm: cardForm))
            case .failure(let apiError):
                var errorType: DebitDeliveryErrorType
                switch apiError {
                case .connectionFailure, .timeout:
                    errorType = .connection
                default:
                    errorType = .general
                }
                self.presenter.showError(errorType: errorType)
            }
        }
    }
    
    private func userChangedAddressData(deliveryAddress: DeliveryAddressFormsData) -> Bool {
        guard let address = searchedAddress,
              let addressType = address.streetType,
              let addressName = address.street
        else {
            return false
        }

        let streetName = addressType + " " + addressName
        return streetName != deliveryAddress.streetName || address.neighborhood != deliveryAddress.neighborhood
    }
    
    private func update(cardForm: CardForm, with formData: DeliveryAddressFormsData) -> CardForm {
        var updatedCardForm = cardForm
        if updatedCardForm.homeAddress == nil {
            updatedCardForm.homeAddress = HomeAddress()
        }
        updatedCardForm.registrationStep = .address
        updatedCardForm.homeAddress?.street = formData.streetName
        updatedCardForm.homeAddress?.city = formData.city
        updatedCardForm.homeAddress?.streetType = nil
        updatedCardForm.homeAddress?.addressComplement = formData.complement
        updatedCardForm.homeAddress?.neighborhood = formData.neighborhood
        updatedCardForm.homeAddress?.state = formData.state
        updatedCardForm.homeAddress?.streetNumber = getStreetNumber(with: formData)
        updatedCardForm.homeAddress?.zipCode = getCorrentPostalCode(with: formData.postalCode)
        updatedCardForm.homeAddress?.manualUpdated = userChangedAddressData(deliveryAddress: formData)
        if let searchedAddress = searchedAddress {
            updatedCardForm.homeAddress?.cityId = searchedAddress.cityId
            updatedCardForm.homeAddress?.stateId = searchedAddress.stateId
        } else {
            updatedCardForm.homeAddress?.cityId = formData.cityId
            updatedCardForm.homeAddress?.stateId = formData.stateId
        }
        return updatedCardForm
    }
    
    private func getStreetNumber(with formData: DeliveryAddressFormsData) -> String? {
        if formData.noNumberButtonStatus || formData.streetNumber.isEmpty {
            return "0"
        }
        return formData.streetNumber
    }
    
    private func getCorrentPostalCode(with postalCodeString: String) -> String {
        postalCodeString.replacingOccurrences(of: "-", with: "")
    }
    
    func viewDidLoad() {
        presenter.fillDebitAddressForm(with: cardForm)
        if cardForm.homeAddress == nil {
            presenter.presentState(state: state)
        }
    }
    
    func openPostOffices() {
        dependencies.analytics.log(CardDeliveryAddressEvent.didTapNotKnowMyPostalCode)
        if let url = URL(string: "https://buscacepinter.correios.com.br/app/endereco/index.php?t") {
            presenter.didNextStep(action: .openNavigation(url))
        }
    }
    
    func resetToBeginState() {
        dependencies.analytics.log(CardDeliveryAddressEvent.didTapIsNotMyPostalCode)
        state = .initial
        cardForm.homeAddress = nil
        viewDidLoad()
    }

    func trackingNoNumber(checked: Bool) {
        dependencies.analytics.log(CardDeliveryAddressEvent.didTapNoNumber(checked: checked))
    }

    func viewWillPop() {
        dependencies.analytics.log(CardDeliveryAddressEvent.didTapBack)
    }
}
