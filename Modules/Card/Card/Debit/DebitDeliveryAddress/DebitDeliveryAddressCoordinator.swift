import UIKit

enum DebitDeliveryAddressAction: Equatable {
    case didConfirm(cardForm: CardForm)
    case openNavigation(URL)
}

protocol DebitDeliveryAddressCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: DebitDeliveryAddressAction)
}

final class DebitDeliveryAddressCoordinator: DebitDeliveryAddressCoordinating, SafariWebViewPresentable {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    weak var viewController: UIViewController?
    private var debitCoordinatorOutput: (DebitCoordinatorOutput) -> Void

    init(dependencies: Dependencies, debitCoordinatorOutput: @escaping (DebitCoordinatorOutput) -> Void) {
        self.dependencies = dependencies
        self.debitCoordinatorOutput = debitCoordinatorOutput
    }
    
    func perform(action: DebitDeliveryAddressAction) {
        switch action {
        case .didConfirm(let cardForm):
            debitCoordinatorOutput(.update(cardForm: cardForm, currentStep: .address))
        case .openNavigation(let url):
            open(url: url)
        }
    }
}
