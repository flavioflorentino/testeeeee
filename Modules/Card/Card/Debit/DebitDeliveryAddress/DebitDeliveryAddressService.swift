import Core
import Foundation

protocol DebitDeliveryAddressServicing {
    func addressBy(_ cep: String, then completion: @escaping CompletionAddressSearch)
    func update(cardForm: CardForm, then completion: CompletionAddressUpdate)
}

public typealias CompletionAddressUpdate = ((Result<CardForm, ApiError>) -> Void)?
public typealias CompletionAddressSearch = (Result<HomeAddress, ApiError>) -> Void

final class DebitDeliveryAddressService: DebitDeliveryAddressServicing {
    typealias Dependencies = HasDebitAPIManagerDependency & HasMainQueue
    private let dependencies: Dependencies

    func addressBy(_ cep: String, then completion: @escaping CompletionAddressSearch) {
        let api = Api<HomeAddress>(endpoint: CreditServiceEndpoint.addressSearch(cep: cep))
        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map { $0.model })
            }
        }
    }
    
    func update(cardForm: CardForm, then completion: CompletionAddressUpdate) {
        dependencies.debitAPIManager.setDebitRegistration(cardForm: cardForm) { result in
            completion?(result)
        }
    }
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}
