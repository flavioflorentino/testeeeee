import UIKit
import Address
import AnalyticsModule
import UI
import Validator
import AssetsKit

// MARK: - Layout
extension DebitDeliveryAddressViewController.Layout {
    enum Length {
        static let cepFieldMaxLength = 9
        static let numberFieldMaxLength = 6
        static let addressTextMaxLength = 30
        static let neighborhoodTextMaxLength = 30
        static let complementTextMaxLength = 15
        static let cityTextMaxLength = 30
        static let stateTextMaxLength = 30
    }
    enum Size {
        static let descriptionHeight: CGFloat = 45
        static let textFieldHeight: CGFloat = 60
        static let withoutNumberHeight: CGFloat = 40
        static let withoutNumberWidth: CGFloat = 106
    }
    enum Font {
        static let title = UIFont.systemFont(ofSize: 28, weight: .bold)
        static let description = UIFont.systemFont(ofSize: 16, weight: .light)
        static let withoutNumber = UIFont.systemFont(ofSize: 14, weight: .regular)
    }
    enum Insets {
        static let withoutNumberContentEdgeInsets = UIEdgeInsets(top: 16, left: 0, bottom: 0, right: 0)
        static let withoutNumberImageEdgeInsets = UIEdgeInsets(top: 4, left: -4, bottom: 4, right: 4)
    }
    enum Spacing {
        static let proportionalCityWidth: CGFloat = 0.6
        static let keyboardMargin: CGFloat = 16
    }
}

private enum ViewControllerState {
    case loading
    case normal
}

// swiftlint:disable file_length
public final class DebitDeliveryAddressViewController: ViewController<DebitDeliveryAddressViewModelInputs, UIView>, LoadingViewProtocol {
    fileprivate enum Layout {}
    
    public var loadingView = LoadingView()
    
    // MARK: - Private Properties
    private var selectedStateId: String?
    private var selectedCityId: String?
    private var state: ViewControllerState = .normal {
        didSet {
            updateState()
        }
    }
    
    // MARK: - View Properties
    private lazy var controls: [UIControl] = {
        [
            postalCodeTextfield,
            streetTextfield,
            neighborhoodTextfield,
            streetNumberTextfield,
            complementTextfield,
            stateTextfield,
            cityTextfield
        ]
    }()
    
    private lazy var keyboardScrollViewHandler = KeyboardScrollViewHandler(scrollView: scrollView)
    
    private lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.backgroundColor = .clear
        scroll.alwaysBounceVertical = false
        return scroll
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var formsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Spacing.base02
        stackView.distribution = .equalSpacing
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var withoutNumberButton: UIButton = {
        let button = UIButton()
        let unselectedColorButton = Palette.ppColorGrayscale700.color
        let selectedColotButton = Palette.ppColorBranding300.color
        button.addTarget(self, action: #selector(self.withoutNumberButtonAction), for: .touchUpInside)
        button.setTitle(Strings.DebitDeliveryAddress.withoutNumber, for: .normal)
        button.setTitleColor(unselectedColorButton, for: .normal)
        button.setTitleColor(selectedColotButton, for: .selected)
        button.tintColor = button.titleColor(for: .normal)
        button.setImage(Assets.iconUnchecked.image.withRenderingMode(.alwaysTemplate), for: .normal)
        button.setImage(Assets.iconChecked.image.withRenderingMode(.alwaysOriginal), for: .selected)
        button.contentEdgeInsets = Layout.Insets.withoutNumberContentEdgeInsets
        button.contentHorizontalAlignment = .left
        button.imageEdgeInsets = Layout.Insets.withoutNumberImageEdgeInsets
        button.imageView?.contentMode = .scaleAspectFit
        button.backgroundColor = view.backgroundColor
        button.titleLabel?.font = Layout.Font.withoutNumber
        return button
    }()
    
    private lazy var descriptionLabel: UITextView = {
        let textView = UITextView()
        textView.text = Strings.DebitDeliveryAddress.description
        textView.font = Layout.Font.description
        textView.isEditable = false
        textView.backgroundColor = .clear
        textView.textContainerInset = UIEdgeInsets.zero
        textView.isScrollEnabled = false
        textView.textContainer.lineFragmentPadding = 0
        return textView
    }()
    
    private lazy var linkButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Strings.DebitDeliveryAddress.postalCodeDontKnowText, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(didTapDontKnowPostalCode), for: .touchUpInside)
        return button
    }()
    
    private lazy var isNotMyPostCodeButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Strings.DebitDeliveryAddress.isNotMyPostalCode, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(didTapIsNotMyPostalCodeButton), for: .touchUpInside)
        button.alpha = 0.0
        return button
    }()
    
    private lazy var postalCodeTextfield: ValidationTextField = {
        let textfield = ValidationTextField(placeholder: Strings.DebitDeliveryAddress.cepText, maxLength: Layout.Length.cepFieldMaxLength, hasCharacterLimitBehavior: true)
        textfield.delegate = self
        textfield.addTarget(
            self,
            action: #selector(checkPostalCodeTextField(_:)),
            for: UIControl.Event.editingChanged)
        textfield.textContentType = .postalCode
        textfield.keyboardType = .numberPad
        return textfield
    }()
    
    private lazy var loadingIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.hidesWhenStopped = true
        return indicator
    }()
    
    private lazy var postalCodeMasker: TextFieldMasker = {
        let mask = CustomStringMask(mask: "00000-000")
        return TextFieldMasker(textMask: mask)
    }()
    
    private lazy var streetTextfield: ValidationTextField = {
        let textfield = ValidationTextField(placeholder: Strings.DebitDeliveryAddress.streetText, maxLength: Layout.Length.addressTextMaxLength, hasCharacterLimitBehavior: true)
        textfield.delegate = self
        textfield.addTarget(
            self,
            action: #selector(checkStreetNameTextField),
            for: UIControl.Event.editingChanged)
        textfield.textContentType = .streetAddressLine1
        return textfield
    }()
    
    private lazy var neighborhoodTextfield: ValidationTextField = {
        let textfield = ValidationTextField(placeholder: Strings.DebitDeliveryAddress.neighboardhoodText, maxLength: Layout.Length.neighborhoodTextMaxLength, hasCharacterLimitBehavior: true)
        textfield.delegate = self
        textfield.addTarget(
            self,
            action: #selector(checkNeighborhoodTextField(_:)),
            for: UIControl.Event.editingChanged)
        textfield.textContentType = .sublocality
        return textfield
    }()
    
    private lazy var streetNumberTextfield: ValidationTextField = {
        let textfield = ValidationTextField(placeholder: Strings.DebitDeliveryAddress.streetNumberText, maxLength: Layout.Length.numberFieldMaxLength, hasCharacterLimitBehavior: true)
        textfield.keyboardType = .numberPad
        textfield.delegate = self
        textfield.addTarget(
            self,
            action: #selector(checkStreetNumberTextField(_:)),
            for: UIControl.Event.editingChanged)
        return textfield
    }()
    
    private lazy var complementTextfield: ValidationTextField = {
        let textfield = ValidationTextField(placeholder: Strings.DebitDeliveryAddress.complementText, maxLength: Layout.Length.complementTextMaxLength, hasCharacterLimitBehavior: true)
        textfield.delegate = self
        textfield.addTarget(
            self,
            action: #selector(checkComplementTextField(_:)),
            for: UIControl.Event.editingChanged)
        return textfield
    }()
    
    private lazy var cityStateStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var cityTextfield: ValidationTextField = {
        let textfield = ValidationTextField(placeholder: Strings.DebitDeliveryAddress.cityText, maxLength: Layout.Length.cityTextMaxLength, hasCharacterLimitBehavior: true)
        textfield.isEnabled = false
        textfield.delegate = self
        textfield.addTarget(
            self,
            action: #selector(checkCityTextField(_:)),
            for: UIControl.Event.editingChanged)
        return textfield
    }()
    
    private lazy var stateTextfield: ValidationTextField = {
        let textfield = ValidationTextField(placeholder: Strings.DebitDeliveryAddress.stateText, maxLength: Layout.Length.stateTextMaxLength, hasCharacterLimitBehavior: true)
        textfield.isEnabled = false
        textfield.delegate = self
        textfield.addTarget(
            self,
            action: #selector(checkStateTextField(_:)),
            for: UIControl.Event.editingChanged)
        return textfield
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.isEnabled = true
        button.alpha = 0.0
        button.addTarget(self, action: #selector(didConfirm), for: .touchUpInside)
        button.setTitle(Strings.DebitDeliveryAddress.confirmButton, for: .normal)
        return button
    }()
    
    lazy var greenCheckImageView: UIImageView = {
        let imageView = UIImageView(image: Resources.Icons.icoCheck.image)
        imageView.alpha = 0.0
        return imageView
    }()
    
    // MARK: - Override Functions
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBarAppearance()
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        keyboardScrollViewHandler.registerForKeyboardNotifications()
        postalCodeMasker.bind(to: postalCodeTextfield)
        viewModel.viewDidLoad()
        configureViews()
        configTextFieldsInpuAccessory()
        configureViewPresentation()
    }

    // MARK: - viewWillDisappear
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        if isMovingFromParent {
            viewModel.viewWillPop()
        }
    }

    private func configureViewPresentation() {
        if #available(iOS 13.0, *) {
            isModalInPresentation = true
        }
        title = Strings.DebitDeliveryAddress.title
    }
    
    private func updateNavigationBarAppearance() {
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.backgroundColor = Palette.ppColorGrayscale000.color
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.isTranslucent = true
            navigationController?.navigationBar.prefersLargeTitles = true
            navigationController?.navigationItem.largeTitleDisplayMode = .always
        }
        
        let backButton = UIBarButtonItem()
        backButton.title = String()
        navigationItem.backBarButtonItem = backButton
    }
    
    override public func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    // MARK: - buildViewHierarchy
    override public func buildViewHierarchy() {
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(formsStackView)
        contentView.addSubview(confirmButton)
        contentView.addSubview(withoutNumberButton)
        contentView.addSubview(loadingIndicator)
        contentView.addSubview(linkButton)
        contentView.addSubview(postalCodeTextfield)
        contentView.addSubview(isNotMyPostCodeButton)
        contentView.addSubview(greenCheckImageView)
        
        formsStackView.addArrangedSubview(streetTextfield)
        formsStackView.addArrangedSubview(neighborhoodTextfield)
        formsStackView.addArrangedSubview(streetNumberTextfield)
        formsStackView.addArrangedSubview(complementTextfield)
        formsStackView.addArrangedSubview(cityStateStackView)
        cityStateStackView.addArrangedSubview(stateTextfield)
        cityStateStackView.addArrangedSubview(cityTextfield)
        
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
    }
    
    override public func setupConstraints() {
        scrollView.layout {
            $0.top == view.topAnchor
            $0.bottom == view.bottomAnchor
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
        }
        
        contentView.layout {
            $0.top == scrollView.topAnchor
            $0.leading == scrollView.leadingAnchor
            $0.trailing == scrollView.trailingAnchor
            $0.bottom == scrollView.bottomAnchor
            $0.width == view.widthAnchor
        }
        
        loadingIndicator.layout {
            $0.trailing == scrollView.trailingAnchor - Spacing.base02
            $0.centerY == postalCodeTextfield.centerYAnchor
        }
        
        setupStructureConstraints()
        setupTextFieldConstraints()
        
        greenCheckImageView.snp.makeConstraints {
            $0.centerY.equalTo(postalCodeTextfield)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.size.equalTo(20)
        }
    }
    
    // MARK: - Constraints Setup
    private func setupStructureConstraints() {
        postalCodeTextfield.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base03)
        }
        
        isNotMyPostCodeButton.snp.makeConstraints {
            $0.trailing.equalTo(postalCodeTextfield.snp.trailing)
            $0.centerY.equalTo(postalCodeTextfield.snp.centerY)
        }
        
        descriptionLabel.layout {
            $0.top == contentView.topAnchor + Spacing.base03
            $0.leading == contentView.leadingAnchor + Spacing.base02
            $0.trailing == contentView.trailingAnchor - Spacing.base02
            $0.height == Layout.Size.descriptionHeight
        }
        
        formsStackView.layout {
            $0.top == postalCodeTextfield.bottomAnchor + Spacing.base02
            $0.leading == contentView.leadingAnchor + Spacing.base02
            $0.trailing == contentView.trailingAnchor - Spacing.base02
        }
        confirmButton.snp.makeConstraints {
            $0.top.equalTo(formsStackView.snp.bottom).offset(Spacing.base05)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(contentView.snp.bottom).inset(Spacing.base03)
        }
        
        linkButton.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.bottom.equalTo(contentView.snp.bottom).inset(Spacing.base04)
        }
    }
    
    private func setupTextFieldConstraints() {
        postalCodeTextfield.layout {
            $0.height == Layout.Size.textFieldHeight
            $0.width == formsStackView.widthAnchor
        }
        streetTextfield.layout {
            $0.height == Layout.Size.textFieldHeight
            $0.width == formsStackView.widthAnchor
        }
        neighborhoodTextfield.layout {
            $0.height == Layout.Size.textFieldHeight
            $0.width == formsStackView.widthAnchor
        }
        withoutNumberButton.layout {
            $0.top == neighborhoodTextfield.bottomAnchor + Spacing.base01
            $0.trailing == contentView.trailingAnchor - Spacing.base02
            $0.height == Layout.Size.textFieldHeight
            $0.width == Layout.Size.withoutNumberWidth
        }
        streetNumberTextfield.layout {
            $0.height == Layout.Size.textFieldHeight
            $0.leading == formsStackView.leadingAnchor
            $0.trailing == withoutNumberButton.leadingAnchor - Spacing.base02
        }
        complementTextfield.layout {
            $0.height == Layout.Size.textFieldHeight
            $0.width == formsStackView.widthAnchor
        }
        cityStateStackView.layout {
            $0.height == Layout.Size.textFieldHeight
            $0.width == formsStackView.widthAnchor
        }
        cityTextfield.layout {
            $0.width == cityStateStackView.widthAnchor * Layout.Spacing.proportionalCityWidth
        }
    }
    
    // MARK: - @objc Selectors
    @objc
    private func didConfirm() {
        interactor.validationAddressConfirm(formData: getFormsData())
        Analytics.shared.log(DebitActivationEvent.deliveryAddress)
    }
    
    // MARK: - didTapDontKnowPostalCode
    @objc
    private func didTapDontKnowPostalCode() {
        viewModel.openPostOffices()
    }
    
    @objc
    private func didTapIsNotMyPostalCodeButton() {
        interactor.resetToBeginState()
        postalCodeTextfield.textFieldState = .blank
    }
    
    // MARK: - Private Functions
    private func sendData() {
        guard isFormValid() else { return }
        sendFormsData()
        state = .loading
    }
    
    private func updateState() {
        switch state {
        case .loading:
            startLoadingView()
            confirmButton.isEnabled = false
        case .normal:
            stopLoadingView()
            confirmButton.isEnabled = true
        }
    }
    
    private func showPopupError(with errorData: DebitDeliveryErrorData) {
        sendShowPopupError(with: errorData)
        state = .normal
        
        let confirmAction = ApolloAlertAction(title: Strings.Default.retry) { [weak self] in
            self?.sendTryAgainPopupError(with: errorData)
            self?.sendData()
        }
        
        showApolloAlert(image: errorData.imageName.image, title: errorData.title, subtitle: errorData.descriptionMessage, primaryButtonAction: confirmAction, linkButtonAction: ApolloAlertAction(title: Strings.Default.cancelButtonTitle, completion: {}))
    }
    
    private func sendTryAgainPopupError(with errorData: DebitDeliveryErrorData) {
        let dialogState: DebitActivationEvent.DialogErrorEventAction = errorData.errorType == .general ? .general: .server
        Analytics.shared.log(DebitActivationEvent.dialogErrorTryAgain(state: dialogState))
    }
    
    private func sendShowPopupError(with errorData: DebitDeliveryErrorData) {
        let dialogState: DebitActivationEvent.DialogErrorEventAction = errorData.errorType == .general ? .general: .server
        Analytics.shared.log(DebitActivationEvent.dialogError(state: dialogState))
    }
}

extension DebitDeliveryAddressViewController {
    private func hideKeyboardWhenTappedAround() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc
    private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc
    private func withoutNumberButtonAction(sender: Any?) {
        withoutNumberButton.isSelected.toggle()
        streetNumberTextfield.isEnabled.toggle()
        if withoutNumberButton.isSelected {
            streetNumberTextfield.text = ""
            streetNumberTextfield.underlineErrorMessage = nil
        }
        renderTextFields()
        checkConfirmButton()
        viewModel.trackingNoNumber(checked: withoutNumberButton.isSelected)
    }
}

extension DebitDeliveryAddressViewController {
    // MARK: - Text Field Listeners
    @objc
    private func checkPostalCodeTextField(_ sender: ValidationTextField) {
        viewModel.checkPostalCodeFieldWith(validationResult: sender.validate())
        if let postalCode = sender.text {
            let postalCodeNumbers = postalCode
                .components(separatedBy: CharacterSet.decimalDigits.inverted)
                .joined()
            if postalCodeNumbers.count == Layout.Length.cepFieldMaxLength - 1 {
                viewModel.addressBy(postalCodeNumbers)
                loadingIndicator.startAnimating()
            }
        }
        renderTextFields()
    }
    
    @objc
    private func checkStreetNameTextField(_ sender: ValidationTextField) {
        renderTextField(sender)
        checkConfirmButton()
    }
    
    @objc
    private func checkNeighborhoodTextField(_ sender: ValidationTextField) {
        renderTextField(sender)
        checkConfirmButton()
    }
    
    @objc
    private func checkStreetNumberTextField(_ sender: ValidationTextField) {
        renderTextField(sender)
        checkConfirmButton()
    }
    
    @objc
    private func checkComplementTextField(_ sender: ValidationTextField) {
        renderTextField(sender)
        checkConfirmButton()
    }
    
    @objc
    private func checkStateTextField(_ sender: ValidationTextField) {
        renderTextField(sender)
        checkConfirmButton()
    }
    
    @objc
    private func checkCityTextField(_ sender: ValidationTextField) {
        renderTextField(sender)
        checkConfirmButton()
    }
    
    // MARK: - Private Functions
    private func checkConfirmButton() {
        confirmButton.isEnabled = isFormValid()
    }
    
    private func getFormsData() -> DeliveryAddressFormsData {
        DeliveryAddressFormsData(
            postalCode: postalCodeTextfield.text,
            streetName: streetTextfield.text,
            neighborhood: neighborhoodTextfield.text,
            streetNumber: streetNumberTextfield.text,
            complement: complementTextfield.text,
            city: cityTextfield.text,
            cityId: selectedCityId,
            state: stateTextfield.text,
            stateId: selectedCityId,
            noNumberButtonStatus: withoutNumberButton.isSelected)
    }
    
    private func sendFormsData() {
        viewModel.sendFormsData(formData: getFormsData())
    }
    
    private func configTextFieldsInpuAccessory() {
        let doneToolBar = DoneToolBar(doneText: Strings.DebitDeliveryAddress.doneToolbar)
        formsStackView.arrangedSubviews.forEach {
            let textField = $0 as? UITextField
            textField?.inputAccessoryView = doneToolBar
        }
    }
    
    private enum ValidateError: String, Error, CustomStringConvertible, ValidationError {
        case cepRequired
        
        var description: String {
            self.rawValue
        }
        
        var message: String {
            switch self {
            case .cepRequired:
                return Strings.DebitDeliveryAddress.postalCodeError
            }
        }
    }
    
    private func renderTextFields() {
        [postalCodeTextfield, streetTextfield, neighborhoodTextfield, streetNumberTextfield, complementTextfield, stateTextfield, cityTextfield].forEach { renderTextField($0) }
    }
    
    private func renderTextField(_ textField: ValidationTextField) {
        guard let text = textField.text else { return }
        if text.count <= (textField.maxlength ?? 0) {
            textField.textFieldState = text.isEmpty ? .blank : .valid
        }
    }
}

// MARK: - UITextFieldDelegate
extension DebitDeliveryAddressViewController: UITextFieldDelegate {
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let newText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string), textField == postalCodeTextfield {
            return newText.count <= Layout.Length.cepFieldMaxLength
        }
        
        guard let maskTextField = textField as? ValidationTextField else { return true }
        if [stateTextfield, cityTextfield].contains(maskTextField) {
            return false
        }
        return true
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let textField = textField as? ValidationTextField else {
            return
        }
        textField.underlineErrorMessage = nil
        
        if textField == stateTextfield {
            textField.resignFirstResponder()
            selectState()
        }
        
        if textField == cityTextfield {
            textField.resignFirstResponder()
            if let selectedState = stateTextfield.text, selectedState.isNotEmpty {
                selectCity(uf: selectedState)
            } else {
                selectState { stateInitials in
                    DispatchQueue.main.async {
                        self.selectCity(uf: stateInitials)
                    }
                }
            }
        }
    }
    
    func selectState(completion: ((String) -> Void)? = nil) {
        let viewController = SelectRegionFactory.make(type: .state) { [weak self] state in
            guard let self = self else {
                return
            }
            self.cityTextfield.text = nil
            self.selectedCityId = nil
            self.stateTextfield.text = state.getSelectionValue()
            self.selectedStateId = String(state.getId())
            self.viewModel.checkStateFieldWith(validationResult: self.stateTextfield.validate())
            self.viewModel.checkCityFieldWith(validationResult: self.cityTextfield.validate())
            self.checkConfirmButton()
            completion?(state.getSelectionValue())
        }
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func selectCity(uf: String) {
        let viewController = SelectRegionFactory.make(type: .city(uf: uf)) { [weak self] city in
            guard let self = self else { return }
            self.cityTextfield.text = city.getSelectionValue()
            self.selectedCityId = String(city.getId())
            self.viewModel.checkCityFieldWith(validationResult: self.cityTextfield.validate())
            self.checkConfirmButton()
        }
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case postalCodeTextfield:
            streetTextfield.becomeFirstResponder()
        case streetTextfield:
            neighborhoodTextfield.becomeFirstResponder()
        case neighborhoodTextfield:
            streetNumberTextfield.becomeFirstResponder()
        case streetNumberTextfield:
            complementTextfield.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return true
    }
}

// MARK: View Model Outputs
extension DebitDeliveryAddressViewController: DebitDeliveryAddressDisplay {
    func displayAlertWarning(_ model: DebitDeliveryAlertModel) {
        let confirmAction = ApolloAlertAction(title: model.primaryButtonTitle) { [weak self] in
            self?.sendData()
        }
    
        showApolloAlert(image: model.image, title: model.title, subtitle: model.descriptionMessage, primaryButtonAction: confirmAction, linkButtonAction: ApolloAlertAction(title: model.secondaryButtonTitle, completion: {}))
    }
    
    func displayPostCodeStep() {
        loadingIndicator.stopAnimating()
        postalCodeTextfield.isEnabled = true
        formsStackView.alpha = 0
        confirmButton.alpha = 0.0
        withoutNumberButton.alpha = 0.0
        postalCodeTextfield.rightIconView = nil
        linkButton.isHidden = false
        postalCodeTextfield.isUserInteractionEnabled = true
        isNotMyPostCodeButton.alpha = 0.0
    }
    
    func displayFilledState() {
        loadingIndicator.stopAnimating()
        postalCodeTextfield.isEnabled = false
        formsStackView.alpha = 1
        confirmButton.alpha = 1.0
        withoutNumberButton.alpha = 1.0
        linkButton.isHidden = true
        postalCodeTextfield.isUserInteractionEnabled = false
        isNotMyPostCodeButton.alpha = 1.0
        confirmButton.alpha = 1.0
    }
    
    func displayCityFieldErrorMessage(_ message: String?) {
        cityTextfield.underlineErrorMessage = message
    }
    
    func displayStateFieldErrorMessage(_ message: String?) {
        stateTextfield.underlineErrorMessage = message
    }
    
    func displayPostalCodeFieldErrorMessage(_ message: String?) {
        postalCodeTextfield.underlineErrorMessage = message
    }
    
    func displayStreetFieldErrorMessage(_ message: String?) {
        streetTextfield.underlineErrorMessage = message
    }
    
    func displayNeighborhoodFieldErrorMessage(_ message: String?) {
        neighborhoodTextfield.underlineErrorMessage = message
    }
    
    func displayStreetNumberFieldErrorMessage(_ message: String?) {
        streetNumberTextfield.underlineErrorMessage = message
    }
    
    func displayComplementFieldErrorMessage(_ message: String?) {
        complementTextfield.underlineErrorMessage = message
    }
    
    func displayAddressCompletionAnimation(finished: @escaping () -> Void) {
        loadingIndicator.stopAnimating()
        UIView.animate(withDuration: 0.5,
                       delay: 0.0,
                       options: .transitionCrossDissolve,
                       animations: { [weak self] in
                        self?.greenCheckImageView.alpha = 1.0
                        self?.postalCodeTextfield.isEnabled = false
                       }, completion: { _ in
                        UIView.animate(withDuration: 0.5,
                                       delay: 0.5,
                                       options: .transitionCrossDissolve,
                                       animations: { [weak self] in
                                            self?.greenCheckImageView.alpha = 0.0
                                            self?.isNotMyPostCodeButton.alpha = 1.0
                                        self?.confirmButton.alpha = 1.0
                                            finished()
                                        })
                                   })
    }
    
    func displayLoading() {
        state = .loading
    }
    
    func setAddressInController(with addressSearch: HomeAddress) {
        stateTextfield.text = addressSearch.state
        cityTextfield.text = addressSearch.city
        neighborhoodTextfield.text = addressSearch.neighborhood
        streetTextfield.text = addressSearch.streetName
        selectedCityId = addressSearch.cityId
        selectedStateId = addressSearch.stateId
        streetTextfield.underlineErrorMessage = nil
        neighborhoodTextfield.underlineErrorMessage = nil
        checkConfirmButton()
        renderTextFields()
    }
    
    func didPostalCodeError() {
        loadingIndicator.stopAnimating()
        postalCodeTextfield.textFieldState = .invalid
        stateTextfield.text = nil
        cityTextfield.text = nil
        neighborhoodTextfield.text = nil
        streetTextfield.text = nil
        streetNumberTextfield.text = nil
        checkConfirmButton()
    }
    
    func fillAddressForm(with addressFormData: DeliveryAddressFormsData) {
        stateTextfield.text = addressFormData.state
        cityTextfield.text = addressFormData.city
        neighborhoodTextfield.text = addressFormData.neighborhood
        streetTextfield.text = addressFormData.streetName
        streetNumberTextfield.text = addressFormData.streetNumber
        postalCodeTextfield.text = addressFormData.postalCode
        complementTextfield.text = addressFormData.complement
        withoutNumberButton.isSelected = addressFormData.noNumberButtonStatus
        streetNumberTextfield.isEnabled = !addressFormData.noNumberButtonStatus
        selectedCityId = addressFormData.cityId
        postalCodeTextfield.isEnabled = false
        linkButton.isHidden = true
        isNotMyPostCodeButton.alpha = 1.0
        confirmButton.alpha = 1.0
        renderTextFields()
        checkConfirmButton()
    }
    
    private func isFormValid() -> Bool {
        let requiredTextFelds = [postalCodeTextfield, streetTextfield, neighborhoodTextfield, cityTextfield, stateTextfield]
        let isRequiredTextFeldsValid = requiredTextFelds.filter({ $0.textFieldState == .valid }).count == requiredTextFelds.count
        let isNumberFieldValid = streetNumberTextfield.textFieldState == .valid || withoutNumberButton.isSelected
        let isComplementNotInvalid = complementTextfield.textFieldState != .invalid
        return isRequiredTextFeldsValid && isNumberFieldValid && isComplementNotInvalid
    }
    
    func enableStateAndCityEdition(enable: Bool) {
        stateTextfield.isUserInteractionEnabled = enable
        cityTextfield.isUserInteractionEnabled = enable
    }
    
    func showError(with errorData: DebitDeliveryErrorData) {
        state = .normal
        showPopupError(with: errorData)
    }
    
    func dismissLoading() {
        state = .normal
    }
}
