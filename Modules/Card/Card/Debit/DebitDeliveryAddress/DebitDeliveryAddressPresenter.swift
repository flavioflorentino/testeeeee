import Core
import Foundation
import AssetsKit
import UI

protocol DebitDeliveryAddressPresenting: AnyObject {
    var viewController: DebitDeliveryAddressDisplay? { get set }
    func didNextStep(action: DebitDeliveryAddressAction)
    func setAddressInController(with addressSearch: HomeAddress)
    func fillDebitAddressForm(with cardForm: CardForm)
    func showError(errorType: DebitDeliveryErrorType)
    func presentPostalCodeError(_ error: ApiError)
    func presentPostalCodeFieldWith(errorMessage: String?)
    func presentStreetFieldWith(errorMessage: String?)
    func presentNeighborhoodFieldWith(errorMessage: String?)
    func presentStreetNumberFieldWith(errorMessage: String?)
    func presentComplementFieldWith(errorMessage: String?)
    func presentStateFieldWith(errorMessage: String?)
    func presentCityFieldWith(errorMessage: String?)
    func presentLoading()
    func enableStateAndCityEdition(postalCodeIsValid: Bool, state: String, city: String)
    func presentState(state: DebitDeliveryAddressPostalCodeState)
    func presentAlertWarning()
    func startAddressCompletionAnimation(with addressSearch: HomeAddress)
}

enum DebitDeliveryErrorType {
    case general
    case connection
}

final class DebitDeliveryAddressPresenter: DebitDeliveryAddressPresenting {
    func presentPostalCodeError(_ error: ApiError) {
        switch error {
        case .connectionFailure:
            showError(errorType: .connection)
        default:
            showError(errorType: .general)
        }
    }
    
    func enableStateAndCityEdition(postalCodeIsValid: Bool, state: String, city: String) {
        viewController?.enableStateAndCityEdition(enable: postalCodeIsValid && (state.isEmpty || city.isEmpty))
    }
    
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies
    
    private let coordinator: DebitDeliveryAddressCoordinating
    weak var viewController: DebitDeliveryAddressDisplay?
    
    init(coordinator: DebitDeliveryAddressCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
    
    func didNextStep(action: DebitDeliveryAddressAction) {
        viewController?.dismissLoading()
        coordinator.perform(action: action)
    }
    
    func startAddressCompletionAnimation(with addressSearch: HomeAddress) {
        viewController?.displayAddressCompletionAnimation(finished: { [weak self] in
            guard let self = self else { return }
            var updatedAddressSearch = addressSearch
            updatedAddressSearch.streetName = self.getCorrectAddress(with: addressSearch)
            self.viewController?.setAddressInController(with: updatedAddressSearch)
            self.presentState(state: .filledPostCode)
        })
    }
    
    func setAddressInController(with addressSearch: HomeAddress) {
        var updatedAddressSearch = addressSearch
        updatedAddressSearch.streetName = getCorrectAddress(with: addressSearch)
        viewController?.setAddressInController(with: updatedAddressSearch)
    }

    func presentState(state: DebitDeliveryAddressPostalCodeState) {
        switch state {
        case .initial:
            viewController?.displayPostCodeStep()
        case .filledPostCode:
            viewController?.displayFilledState()
        case .invalid:
            viewController?.didPostalCodeError()
        }
    }
    
    func presentLoading() {
        viewController?.displayLoading()
    }

    private func getCorrectAddress(with deliverySearch: HomeAddress?) -> String? {
        guard
            let deliverySearch = deliverySearch,
            let street = deliverySearch.street
            else {
                return nil
        }
        guard let streetType = deliverySearch.streetType else {
            return deliverySearch.street
        }
        return streetType + " " + street
    }
    
    func fillDebitAddressForm(with cardForm: CardForm) {
        let noNumberButtonStatus = testIfNoNumberIsActive(in: cardForm)
        let postalCode = applyFormatting(in: cardForm.homeAddress?.zipCode ?? "")
        let addressFormsData = DeliveryAddressFormsData(
            postalCode: postalCode,
            streetName: getCorrectAddress(with: cardForm.homeAddress),
            neighborhood: cardForm.homeAddress?.neighborhood,
            streetNumber: getStreetNumber(with: cardForm),
            complement: cardForm.homeAddress?.addressComplement,
            city: cardForm.homeAddress?.city,
            cityId: cardForm.homeAddress?.cityId,
            state: cardForm.homeAddress?.state,
            stateId: cardForm.homeAddress?.stateId,
            noNumberButtonStatus: noNumberButtonStatus)
        viewController?.fillAddressForm(with: addressFormsData)
    }
    
    private func getStreetNumber(with cardForm: CardForm) -> String? {
        testIfNoNumberIsActive(in: cardForm) ? "" : cardForm.homeAddress?.streetNumber
    }
    
    private func testIfNoNumberIsActive(in cardForm: CardForm) -> Bool {
        cardForm.homeAddress?.streetNumber == "0"
    }
    
    func showError(errorType: DebitDeliveryErrorType) {
        var model: DebitDeliveryErrorData
        switch errorType {
        case .general:
            model = DebitDeliveryErrorData(title: Strings.DebitDeliveryErrorData.General.title,
                                           descriptionMessage: Strings.DebitDeliveryErrorData.General.description,
                                           imageName: AssetsKit.Resources.Illustrations.iluError,
                                           errorType: .general)
        case .connection:
            model = DebitDeliveryErrorData(title: Strings.DebitDeliveryErrorData.Connection.title,
                                           descriptionMessage: Strings.DebitDeliveryErrorData.Connection.description,
                                           imageName: AssetsKit.Resources.Illustrations.iluNotFound,
                                           errorType: .connection)
        }
        viewController?.showError(with: model)
    }
    
    private func applyFormatting(in postalCode: String) -> String {
        let postalcodeRegex = "^[0-9]{8}"
        let pinPredicate = NSPredicate(format: "SELF MATCHES %@", postalcodeRegex)
        if pinPredicate.evaluate(with: postalCode) {
            var correctPostalCode = postalCode
            let index = correctPostalCode.index(correctPostalCode.endIndex, offsetBy: -3)
            correctPostalCode.insert("-", at: index)
            return correctPostalCode
        }
        return ""
    }
    
    func presentAlertWarning() {
        let model = DebitDeliveryAlertModel(title: Strings.DebitDeliveryWarningAlert.title,
                                            descriptionMessage: Strings.DebitDeliveryWarningAlert.description,
                                            image: AssetsKit.Resources.Illustrations.iluWarning.image,
                                            primaryButtonTitle: Strings.DebitDeliveryWarningAlert.primaryButtonTitle,
                                            secondaryButtonTitle: Strings.Default.back)
        viewController?.displayAlertWarning(model)
    }
    
    func presentPostalCodeFieldWith(errorMessage: String?) {
        viewController?.displayPostalCodeFieldErrorMessage(errorMessage)
    }
    
    func presentStreetFieldWith(errorMessage: String?) {
        viewController?.displayStreetFieldErrorMessage(errorMessage)
    }
    
    func presentNeighborhoodFieldWith(errorMessage: String?) {
        viewController?.displayNeighborhoodFieldErrorMessage(errorMessage)
    }
    
    func presentStreetNumberFieldWith(errorMessage: String?) {
        viewController?.displayStreetNumberFieldErrorMessage(errorMessage)
    }
    
    func presentComplementFieldWith(errorMessage: String?) {
        viewController?.displayComplementFieldErrorMessage(errorMessage)
    }
    
    func presentStateFieldWith(errorMessage: String?) {
        viewController?.displayStateFieldErrorMessage(errorMessage)
    }
    
    func presentCityFieldWith(errorMessage: String?) {
        viewController?.displayCityFieldErrorMessage(errorMessage)
    }
}
