import Foundation
import FeatureFlag

public enum DebitDeliveryAddressFactory {
    public static func make(cardForm: CardForm, debitCoordinatorOutput: @escaping (DebitCoordinatorOutput) -> Void) -> UIViewController {
        let container = DependencyContainer()
        let service: DebitDeliveryAddressServicing = DebitDeliveryAddressService(dependencies: container)
        let coordinator: DebitDeliveryAddressCoordinating = DebitDeliveryAddressCoordinator(
            dependencies: container,
            debitCoordinatorOutput: debitCoordinatorOutput
        )
        let presenter: DebitDeliveryAddressPresenting = DebitDeliveryAddressPresenter(coordinator: coordinator, dependencies: container)
        let viewModel = DebitDeliveryAddressViewModel(service: service, presenter: presenter, dependencies: container, cardForm: cardForm)

        let viewController: UIViewController
        
        if container.featureManager.isActive(.isNewCardAddressScreenAvailable) {
            viewController = DebitDeliveryAddressViewController(viewModel: viewModel)
        } else {
            viewController = DebitDeliveryAddressViewControllerOld(viewModel: viewModel)
        }

        coordinator.viewController = viewController
        presenter.viewController = viewController as? DebitDeliveryAddressDisplay

        return viewController
    }
}
