import UIKit
import UI

protocol DebitDeliveryAddressDisplay: AnyObject {
    func setAddressInController(with addressSearch: HomeAddress)
    func didPostalCodeError()
    func fillAddressForm(with addressFormData: DeliveryAddressFormsData)
    func showError(with errorData: DebitDeliveryErrorData)
    func dismissLoading()
    func displayLoading()
    func displayPostalCodeFieldErrorMessage(_ message: String?)
    func displayStreetFieldErrorMessage(_ message: String?)
    func displayNeighborhoodFieldErrorMessage(_ message: String?)
    func displayStreetNumberFieldErrorMessage(_ message: String?)
    func displayComplementFieldErrorMessage(_ message: String?)
    func displayCityFieldErrorMessage(_ message: String?)
    func displayStateFieldErrorMessage(_ message: String?)
    func enableStateAndCityEdition(enable: Bool)
    func displayPostCodeStep()
    func displayFilledState()
    func displayAlertWarning(_ model: DebitDeliveryAlertModel)
    func displayAddressCompletionAnimation(finished: @escaping () -> Void)
}
