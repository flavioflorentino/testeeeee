import Foundation

public protocol DebitLoadingViewModelInputs: AnyObject {
    func updateDebitForm()
    func endFlow()
}

final class DebitLoadingViewModel {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let service: DebitLoadingServicing
    private let presenter: DebitLoadingPresenting

    init(service: DebitLoadingServicing, presenter: DebitLoadingPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

extension DebitLoadingViewModel: DebitLoadingViewModelInputs {
    func updateDebitForm() {
        service.updateDebitForm { [weak self] result in
            switch result {
            case let .success(cardForm):
                self?.presenter.updateDebitForm(cardForm: cardForm)
            case .failure:
                self?.presenter.showError()
            }
        }
    }
    
    func endFlow() {
        presenter.endFlow()
    }
}
