import Core
import Foundation

protocol DebitLoadingServicing {
    func updateDebitForm(completion: @escaping CompletionCardForm)
}

public typealias CompletionCardForm = (Result<CardForm, ApiError>) -> Void

final class DebitLoadingService: DebitLoadingServicing {
    typealias Dependencies = HasDebitAPIManagerDependency
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func updateDebitForm(completion: @escaping CompletionCardForm) {
        dependencies.debitAPIManager.getDebitData { result in
            completion(result)
        }
    }
}
