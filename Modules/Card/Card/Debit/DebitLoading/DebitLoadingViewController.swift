import UI
import UIKit

extension DebitLoadingViewController.Layout {
    enum Size {
        static let activityIndicatorHeight: CGFloat = 40.0
    }
    enum Font {
        static let loading = UIFont.systemFont(ofSize: 16, weight: .regular)
    }
    enum Constants {
        static let activityIndicatorScale: CGFloat = 1.5
    }
}

public final class DebitLoadingViewController: ViewController<DebitLoadingViewModelInputs, UIView> {
    fileprivate enum Layout {}
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fill
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var loadingLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.DebitLoading.loadingLabel
        label.textAlignment = .center
        label.textColor = Palette.ppColorGrayscale500.color
        label.font = Layout.Font.loading
        return label
    }()
    
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.startAnimating()
        activityIndicator.color = Palette.ppColorGrayscale500.color
        activityIndicator.transform = CGAffineTransform(
            scaleX: Layout.Constants.activityIndicatorScale,
            y: Layout.Constants.activityIndicatorScale
        )
        activityIndicator.tintColor = Palette.ppColorGrayscale500.color
        return activityIndicator
    }()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        viewModel.updateDebitForm()
        
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
 
    override public func buildViewHierarchy() {
        stackView.addArrangedSubview(activityIndicator)
        stackView.addArrangedSubview(loadingLabel)
        
        view.addSubview(stackView)
    }
    
    override public func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    override public func setupConstraints() {
        activityIndicator.layout {
            $0.height == Layout.Size.activityIndicatorHeight
            $0.width == Layout.Size.activityIndicatorHeight
        }
        stackView.layout {
            $0.centerX == view.centerXAnchor
            $0.centerY == view.centerYAnchor
            $0.width == view.widthAnchor
        }
    }
    
    private func showPopupError(messageDescription: String) {
        let popup = PopupViewController(
            title: Strings.Default.ops,
            description: messageDescription,
            preferredType: .image(Assets.icoSadEmoji.image))
        
        let confirmAction = PopupAction(title: Strings.Default.iGotIt, style: .fill) {
            self.didDismiss()
        }
        
        popup.addAction(confirmAction)
        showPopup(popup)
    }
    
    @objc
    private func didDismiss() {
        viewModel.endFlow()
    }
}

// MARK: View Model Outputs
extension DebitLoadingViewController: DebitLoadingDisplay {
    func showError(with errorMessage: String) {
        showPopupError(messageDescription: errorMessage)
    }
}
