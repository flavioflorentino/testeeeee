import UIKit

protocol DebitLoadingDisplay: AnyObject {
    func showError(with errorMessage: String)
}
