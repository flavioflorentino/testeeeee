import UIKit

enum DebitLoadingAction {
    case updateFlowCoordinator(cardForm: CardForm)
    case endFlow
}

protocol DebitLoadingCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: DebitLoadingAction)
}

final class DebitLoadingCoordinator: DebitLoadingCoordinating {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    weak var viewController: UIViewController?
    var coordinadorOutput: (DebitCoordinatorOutput) -> Void

    init(dependencies: Dependencies, debitCoordinatorOutput: @escaping (DebitCoordinatorOutput) -> Void) {
        self.dependencies = dependencies
        self.coordinadorOutput = debitCoordinatorOutput
    }
    
    func perform(action: DebitLoadingAction) {
        switch action {
        case .updateFlowCoordinator(let cardForm):
            coordinadorOutput(.getServiceResponse(cardForm: cardForm))
        case .endFlow:
            coordinadorOutput(.close)
        }
    }
}
