import Core
import Foundation

protocol DebitLoadingPresenting: AnyObject {
    var viewController: DebitLoadingDisplay? { get set }
    func didNextStep(action: DebitLoadingAction)
    func updateDebitForm(cardForm: CardForm)
    func showError()
    func endFlow()
}

final class DebitLoadingPresenter: DebitLoadingPresenting {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let coordinator: DebitLoadingCoordinating
    weak var viewController: DebitLoadingDisplay?

    init(coordinator: DebitLoadingCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
    
    func didNextStep(action: DebitLoadingAction) {
        coordinator.perform(action: action)
    }
    
    func updateDebitForm(cardForm: CardForm) {
        coordinator.perform(action: .updateFlowCoordinator(cardForm: cardForm))
    }
    
    func showError() {
        let message = Strings.DebitLoading.errorOnUpdateAddress
        viewController?.showError(with: message)
    }
    
    func endFlow() {
        coordinator.perform(action: .endFlow)
    }
}
