import Foundation

public enum DebitLoadingFactory {
    public static func make(debitCoordinatorOutput: @escaping (DebitCoordinatorOutput) -> Void) -> DebitLoadingViewController {
        let container = DependencyContainer()
        let service: DebitLoadingServicing = DebitLoadingService(dependencies: container)
        let coordinator: DebitLoadingCoordinating = DebitLoadingCoordinator(
            dependencies: container,
            debitCoordinatorOutput: debitCoordinatorOutput
        )
        let presenter: DebitLoadingPresenting = DebitLoadingPresenter(coordinator: coordinator, dependencies: container)
        let viewModel = DebitLoadingViewModel(service: service, presenter: presenter, dependencies: container)
        let viewController = DebitLoadingViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
