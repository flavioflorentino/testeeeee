import UIKit

public enum CardTrackingLoadingAction {
    case update(trackingData: CardTrackingHistory)
    case close
}

protocol CardTrackingLoadingCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: CardTrackingLoadingAction)
}

public final class CardTrackingLoadingCoordinator {
    weak var viewController: UIViewController?
    var coordinadorOutput: (CardTrackingCoordinatorOutput) -> Void
    
    typealias CardTrackingCompletion = (CardTrackingCoordinatorOutput) -> Void
    
    init(coordinadorOutput: @escaping CardTrackingCompletion) {
        self.coordinadorOutput = coordinadorOutput
    }
}

// MARK: - CardTrackingLoadingCoordinating
extension CardTrackingLoadingCoordinator: CardTrackingLoadingCoordinating {
    func perform(action: CardTrackingLoadingAction) {
        switch action {
        case .update(let trackingData):
            coordinadorOutput(.getServiceResponse(trackingData: trackingData))
        case .close:
            coordinadorOutput(.close)
        }
    }
}
