import UI

protocol CardTrackingLoadingDisplay: AnyObject {
    func displayErrorState(with model: StatefulFeedbackViewModel)
    func displayLoadState()
}
