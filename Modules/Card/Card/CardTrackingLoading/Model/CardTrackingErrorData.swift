import UIKit

struct CardTrackingErrorData {
    let title: String
    let descriptionMessage: String
    let imageName: ImageAsset
    let errorType: CardTrackingErrorType
}
