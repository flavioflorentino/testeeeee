import Foundation

enum CardTrackingLoadingFactory {
    static func make(coordinatorOutput: @escaping (CardTrackingCoordinatorOutput) -> Void) -> CardTrackingLoadingViewController {
        let container = DependencyContainer()
        let service: CardTrackingLoadingServicing = CardTrackingLoadingService(dependencyContainer: container)
        let coordinator: CardTrackingLoadingCoordinating = CardTrackingLoadingCoordinator(coordinadorOutput: coordinatorOutput)
        let presenter: CardTrackingLoadingPresenting = CardTrackingLoadingPresenter(coordinator: coordinator)
        let interactor = CardTrackingLoadingInteractor(service: service, presenter: presenter, container: container)
        let viewController = CardTrackingLoadingViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
