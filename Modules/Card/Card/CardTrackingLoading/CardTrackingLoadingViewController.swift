import UIKit
import SnapKit
import UI

extension CardTrackingLoadingViewController.Layout {
    enum Size {
        static let activityIndicatorSize = CGSize(width: 40.0, height: 40.0)
    }
    enum Constants {
        static let activityIndicatorScale: CGFloat = 1.5
    }
}

final class CardTrackingLoadingViewController: ViewController<CardTrackingLoadingInteractorInputs, UIView> {
    fileprivate enum Layout {}
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fill
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var loadingLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
        label.text = Strings.DebitLoading.loadingLabel
        label.textAlignment = .center
        label.textColor = Colors.grayscale700.color
        return label
    }()
    
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.startAnimating()
        activityIndicator.color = Colors.grayscale700.color
        activityIndicator.transform = CGAffineTransform(
            scaleX: Layout.Constants.activityIndicatorScale,
            y: Layout.Constants.activityIndicatorScale
        )
        activityIndicator.tintColor = Colors.grayscale700.color
        return activityIndicator
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.updateData()
        
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
 
    override func buildViewHierarchy() {
        stackView.addArrangedSubview(activityIndicator)
        stackView.addArrangedSubview(loadingLabel)
        
        view.addSubview(stackView)
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    override func setupConstraints() {
        activityIndicator.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.activityIndicatorSize)
        }
        stackView.snp.makeConstraints {
            $0.centerX.equalTo(view.snp.centerX)
            $0.centerY.equalTo(view.snp.centerY)
            $0.width.equalToSuperview()
        }
    }
    
    @objc
    private func didDismiss() {
        interactor.endFlow()
    }
}

// MARK: View Model Outputs
extension CardTrackingLoadingViewController: CardTrackingLoadingDisplay {
    func displayErrorState(with model: StatefulFeedbackViewModel) {
        endState(animated: true, model: model)
    }
    
    public func displayLoadState() {
        beginState()
    }
}

extension CardTrackingLoadingViewController: StatefulTransitionViewing {
    func statefulViewForError() -> StatefulViewing {
        StatefulFeedbackView()
    }
    
    func didTryAgain() {
        interactor.updateData()
    }
    
    func didTapSecondaryButton() {
        interactor.dismiss()
    }
}
