import Core
import Foundation

protocol CardTrackingLoadingServicing {
    func getTrackingData(completion: @escaping CompletionTracking)
}

public typealias CompletionTracking = (Result<CardTrackingHistory, ApiError>) -> Void

final class CardTrackingLoadingService {
    typealias Dependencies = HasMainQueue
    var container: Dependencies
    init(dependencyContainer: Dependencies) {
        self.container = dependencyContainer
    }
}

// MARK: - CardTrackingLoadingServicing
extension CardTrackingLoadingService: CardTrackingLoadingServicing {
    public func getTrackingData(completion: @escaping CompletionTracking) {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        let api = Api<CardTrackingHistory>(endpoint: CreditServiceEndpoint.cardTracking)
        api.execute(jsonDecoder: decoder) { [weak self] result in
            self?.container.mainQueue.async {
                completion(result.map { $0.model })
            }
        }
    }
}
