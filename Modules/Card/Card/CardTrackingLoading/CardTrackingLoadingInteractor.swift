import AnalyticsModule
import Foundation

protocol CardTrackingLoadingInteractorInputs: AnyObject {
    func updateData()
    func endFlow()
    func didTryAgain(with errorType: CardTrackingErrorType)
    func dismiss()
}

final class CardTrackingLoadingInteractor {
    private let service: CardTrackingLoadingServicing
    private let presenter: CardTrackingLoadingPresenting
    private let container: Dependencies
    
    typealias Dependencies = HasAnalytics

    init(service: CardTrackingLoadingServicing, presenter: CardTrackingLoadingPresenting, container: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.container = container
    }
}

// MARK: - CardTrackingLoadingViewModelInputs
extension CardTrackingLoadingInteractor: CardTrackingLoadingInteractorInputs {
    func endFlow() {
        presenter.didNextStep(action: .close)
    }
    
    func didTryAgain(with errorType: CardTrackingErrorType) {
        sendAnalytics(with: errorType)
        updateData()
    }
    
    func dismiss() {
        presenter.didNextStep(action: .close)
    }
    
    private func sendAnalytics(with errorType: CardTrackingErrorType) {
        let analytics: CardTrackingEvent = errorType == .connection ? .didInternetError : .didServerError
        container.analytics.log(analytics)
    }
    
    func updateData() {
        presenter.presentLoadState()
        service.getTrackingData { [weak self] result in
            switch result {
            case .success(let trackingData):
                self?.presenter.didNextStep(action: .update(trackingData: trackingData))
            case .failure(let apiError):
                self?.presenter.showError(apiError)
            }
        }
    }
}
