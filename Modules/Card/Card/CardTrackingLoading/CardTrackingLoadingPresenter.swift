import Foundation
import Core
import UI

enum CardTrackingErrorType {
    case general
    case connection
}

protocol CardTrackingLoadingPresenting: AnyObject {
    var viewController: CardTrackingLoadingDisplay? { get set }
    func didNextStep(action: CardTrackingLoadingAction)
    func showError(_ apiError: ApiError)
    func presentLoadState()
}

final class CardTrackingLoadingPresenter {
    private let coordinator: CardTrackingLoadingCoordinating
    weak var viewController: CardTrackingLoadingDisplay?

    init(coordinator: CardTrackingLoadingCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - CardTrackingLoadingPresenting
extension CardTrackingLoadingPresenter: CardTrackingLoadingPresenting {
    func showError(_ apiError: ApiError) {
        let model = StatefulFeedbackViewModel(apiError)
        viewController?.displayErrorState(with: model)
    }
    
    public func presentLoadState() {
        viewController?.displayLoadState()
    }
    
    func didNextStep(action: CardTrackingLoadingAction) {
        coordinator.perform(action: action)
    }
}
