import Core
import Foundation

public struct DebitReceiptResponse: Decodable {
    public let data: DebitReceiptData
}

public struct DebitReceiptData: Decodable {
    public let receipt: [[AnyHashable: Any]]

    private enum CodingKeys: String, CodingKey {
        case receipt
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let data = try container.decode(AnyCodable.self, forKey: .receipt)
        receipt = data.value as? [[AnyHashable: Any]] ?? []
    }
}
