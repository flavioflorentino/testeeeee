import Core
import Foundation

public protocol DebitReceiptServicing {
    func getReceipt(transactionID: String, completion: @escaping (Result<DebitReceiptResponse, ApiError>) -> Void)
}

public final class DebitReceiptService {
    private let dependencies: HasMainQueue

    public init(dependencies: HasMainQueue) {
        self.dependencies = dependencies
    }
}

// MARK: - ReceiptServicing
extension DebitReceiptService: DebitReceiptServicing {
    public func getReceipt(transactionID: String, completion: @escaping (Result<DebitReceiptResponse, ApiError>) -> Void) {
        let endpoint = CreditServiceEndpoint.getDebitReceipt(id: transactionID)
        Api<DebitReceiptResponse>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
