import Core
import Foundation

public protocol ChosenDeliveryAddressSuccessViewModelInputs {
    func close()
}

public final class ChosenDeliveryAddressSuccessViewModel: ChosenDeliveryAddressSuccessViewModelInputs {
    private let coordinator: ChosenDeliveryAddressSuccessCoordinatorProtocol
    
    init(coordinator: ChosenDeliveryAddressSuccessCoordinatorProtocol) {
        self.coordinator = coordinator
    }
    
    public func close() {
        coordinator.perform(action: .close)
    }
}
