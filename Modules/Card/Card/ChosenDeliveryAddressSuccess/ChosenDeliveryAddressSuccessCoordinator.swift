import UIKit

public enum ChosenDeliveryAddressSuccessAction {
    case close
}

public protocol ChosenDeliveryAddressSuccessCoordinatorDelegate: AnyObject {
    func didCloseChosenDeliveryAddressSuccess()
}

public protocol ChosenDeliveryAddressSuccessCoordinatorProtocol {
    var viewController: UIViewController? { get set }
    var delegate: ChosenDeliveryAddressSuccessCoordinatorDelegate? { get set }
    func perform(action: ChosenDeliveryAddressSuccessAction)
}

public final class ChosenDeliveryAddressSuccessCoordinator: ChosenDeliveryAddressSuccessCoordinatorProtocol {
    public weak var viewController: UIViewController?
    public weak var delegate: ChosenDeliveryAddressSuccessCoordinatorDelegate?
    
    public func perform(action: ChosenDeliveryAddressSuccessAction) {
        switch action {
        case .close:
            delegate?.didCloseChosenDeliveryAddressSuccess()
        }
    }
}
