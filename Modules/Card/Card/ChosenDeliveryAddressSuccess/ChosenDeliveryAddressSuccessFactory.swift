import Foundation
import UIKit

public enum ChosenDeliveryAddressSuccessFactory {
    public static func make(delegate: ChosenDeliveryAddressSuccessCoordinatorDelegate) -> ChosenDeliveryAddressSuccessViewController {
        let coordinator = ChosenDeliveryAddressSuccessCoordinator()
        let viewModel = ChosenDeliveryAddressSuccessViewModel(coordinator: coordinator)
        let viewController = ChosenDeliveryAddressSuccessViewController(viewModel: viewModel)
        
        coordinator.delegate = delegate
        coordinator.viewController = viewController

        return viewController
    }
}
