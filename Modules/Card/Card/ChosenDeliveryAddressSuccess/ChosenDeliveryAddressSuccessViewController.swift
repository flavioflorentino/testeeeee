import UI
import UIKit

// swiftlint:disable type_name
public class ChosenDeliveryAddressSuccessViewController: ViewController<ChosenDeliveryAddressSuccessViewModelInputs, UIView> {
    private enum Layout {
        static let textTitleBoldFont = UIFont.systemFont(ofSize: 24, weight: .semibold)
        static let textBodyMediumFont = UIFont.systemFont(ofSize: 14, weight: .semibold)
        static let textBodyLightFont = UIFont.systemFont(ofSize: 14, weight: .light)
        static let textButtonFont = UIFont.systemFont(ofSize: 16, weight: .semibold)
        static let sizeButton: CGFloat = 48.0
        static let holdBodyLabelTopAnchor: CGFloat = 8.0
    }
    
    private lazy var stackItems: UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .vertical
        stack.alignment = .center
        stack.spacing = 24
        stack.distribution = .equalSpacing
        return stack
    }()
    
    private lazy var icoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = Assets.cardEnvelope.image
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Layout.textTitleBoldFont
        label.text = Strings.ChosenDeliveryAddressSuccess.title
        label.textColor = Palette.ppColorGrayscale600.color
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var bodyLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Palette.ppColorGrayscale500.color
        label.numberOfLines = 0
        label.font = Layout.textBodyLightFont
        let text = Strings.ChosenDeliveryAddressSuccess.subtitle
        var attributedText = text.attributedStringWithFont(primary: Layout.textBodyLightFont, secondary: Layout.textBodyMediumFont)
        attributedText.addSpacing(of: 5)
        label.attributedText = attributedText
        label.textAlignment = .center
        return label
    }()
    
    private lazy var holdBodyLabelView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Palette.ppColorGrayscale100.color
        return view
    }()
    
    private lazy var confirmButton: ConfirmButton = {
        let button = ConfirmButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(Strings.ChosenDeliveryAddressSuccess.confirm, for: .normal)
        button.layer.cornerRadius = Layout.sizeButton / 2
        button.backgroundColor = Palette.ppColorBranding300.color
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = Layout.textButtonFont
        button.addTarget(self, action: #selector(didTapConfirm), for: .touchUpInside)
        return button
    }()
    
    override public func viewDidLoad() {
        super.viewDidLoad()

        updateNavigationBarAppearance()
        addCloseButton()
    }
    
    // MARK: - Methods Helpers
    override public func buildViewHierarchy() {
        view.addSubview(stackItems)
        holdBodyLabelView.addSubview(bodyLabel)
        
        stackItems.addArrangedSubview(icoImageView)
        stackItems.addArrangedSubview(titleLabel)
        stackItems.addArrangedSubview(holdBodyLabelView)
        
        view.addSubview(confirmButton)
    }
    
    override public func setupConstraints() {
        NSLayoutConstraint.leadingTrailing(equalTo: view,
                                           for: [stackItems],
                                           constant: 0)
        NSLayoutConstraint.leadingTrailing(equalTo: stackItems,
                                           for: [titleLabel],
                                           constant: 16)
        NSLayoutConstraint.leadingTrailing(equalTo: stackItems,
                                           for: [holdBodyLabelView],
                                           constant: 0)
        NSLayoutConstraint.leadingTrailing(equalTo: holdBodyLabelView,
                                           for: [bodyLabel],
                                           constant: 16)
        NSLayoutConstraint.leadingTrailing(equalTo: view,
                                           for: [confirmButton],
                                           constant: 16)
        
        NSLayoutConstraint.activate([
            titleLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 62)
        ])
        NSLayoutConstraint.activate([
            holdBodyLabelView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Layout.holdBodyLabelTopAnchor),
            holdBodyLabelView.heightAnchor.constraint(greaterThanOrEqualToConstant: 88)
        ])
        NSLayoutConstraint.activate([
            bodyLabel.topAnchor.constraint(equalTo: holdBodyLabelView.topAnchor),
            bodyLabel.bottomAnchor.constraint(equalTo: holdBodyLabelView.bottomAnchor, constant: -28)
        ])
        
        NSLayoutConstraint.activate([
            stackItems.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            stackItems.bottomAnchor.constraint(greaterThanOrEqualTo: view.compatibleSafeAreaLayoutGuide.topAnchor, constant: 16),
            stackItems.bottomAnchor.constraint(lessThanOrEqualTo: confirmButton.topAnchor, constant: -16)
        ])
        NSLayoutConstraint.activate([
            confirmButton.bottomAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.bottomAnchor, constant: -24),
            confirmButton.heightAnchor.constraint(equalToConstant: Layout.sizeButton)
        ])
    }
    
    override public func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale100.color
    }
    
    @objc
    public func didTapConfirm() {
        viewModel.close()
    }

    // MARK: - Private Methods Helpers
    private func updateNavigationBarAppearance() {
        navigationItem.hidesBackButton = true
        navigationController?.navigationBar.tintColor = Palette.ppColorBranding300.color
        navigationController?.navigationBar.isTranslucent = false
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
            navigationController?.navigationItem.largeTitleDisplayMode = .always
        }
    }
    
    private func addCloseButton() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: Assets.icoClose.image,
            style: .plain,
            target: self,
            action: #selector(didTapClose)
        )
    }
    @objc
    private func didTapClose() {
        viewModel.close()
    }
}
