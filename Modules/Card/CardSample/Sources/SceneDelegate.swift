import Card
import UIKit
import Core

typealias Dependencies = HasKeychainManager

final class SampleDependencies: Dependencies {
    lazy var keychain: KeychainManagerContract = KeychainManager()
}

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?
    let dependencies = SampleDependencies()
    let navigation = UINavigationController(rootViewController: BillNotificationFactory.make(invoiceId: "7000000000"))
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        if let windowScene = scene as? UIWindowScene {
            dependencies.keychain.set(key: KeychainKey.token, value: "21ee0d3432b3456b8551ed7452230c7b")
            let window = UIWindow(windowScene: windowScene)
            window.rootViewController = navigation
            self.window = window
            window.makeKeyAndVisible()
        }
    }
}
