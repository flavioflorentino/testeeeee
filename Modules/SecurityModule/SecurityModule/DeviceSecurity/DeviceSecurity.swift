import Foundation
import LocalAuthentication

public enum DeviceBiometricType {
    case none
    case touchID
    case faceID
}

public enum DeviceSecurity {
    public static func biometricType(for context: LAContext = LAContext()) -> DeviceBiometricType {
        if #available(iOS 11.0, *) {
            switch context.biometryType {
            case .faceID:
                return .faceID
            case .touchID:
                return .touchID
            default:
                return .none
            }
        } else {
            return context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) ? .touchID : .none
        }
    }

    public static func canEvaluate(for context: LAContext = LAContext()) -> Bool {
        context.canEvaluatePolicy(.deviceOwnerAuthentication, error: nil)
    }
    
    public static func evaluateBiometry(
        reasonDescription: String,
        context: LAContext = LAContext(),
        completion: @escaping (_ authorized: Bool, _ error: LAError?) -> Void
    ) {
        context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reasonDescription) { success, error in
            DispatchQueue.main.async {
                completion(success, error as? LAError)
            }
        }
    }
}
