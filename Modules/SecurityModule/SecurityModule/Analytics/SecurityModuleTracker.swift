import AnalyticsModule
import Foundation

struct SecurityModuleTracker: AnalyticsKeyProtocol {
    var eventType: AppProtectionEvent
    var eventProperties: [AppProtectionEventProperty]

    private var properties: [String: Any] {
        guard eventProperties.isNotEmpty else {
            return [:]
        }
        
        var properties = [String: Any]()
        for property in eventProperties {
            properties[property.name.rawValue] = property.value.rawValue
        }
        return properties
    }

    private var name: String { eventType.rawValue }

    private var providers: [AnalyticsProvider] {
        [.eventTracker, .mixPanel]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
