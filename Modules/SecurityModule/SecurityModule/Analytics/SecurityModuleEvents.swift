import Foundation

// MARK: - Event Names
enum AppProtectionEvent: String {
    case screenViewed = "Screen Viewed"
    case buttonClicked = "Button Clicked"
    case dialogViewed = "Dialog Viewed"
}

// MARK: - Event Properties
struct AppProtectionEventProperty {
    enum Name: String {
        case screenName = "screen_name"
        case buttonName = "button_name"
        case dialogName = "dialog_name"
    }

    enum Value: String {
        case auth = "AUTH"
        case logout = "LOGOUT"
        case fallback = "APP_PROTECTION_REQUESTED_FALLBACK"
        case protectionRequested = "APP_PROTECTION_REQUESTED"
    }
    
    var name: Name
    var value: Value
}
