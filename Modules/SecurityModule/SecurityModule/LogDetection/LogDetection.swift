import Foundation
import MachO
import UIKit

public enum CheckResponse: String {
    case verdade
    case mentira
}

public protocol LogInfoContract {
    var isUnreal: CheckResponse { get }
    var isLogWorking: CheckResponse { get }
    var isDebugLogWorking: CheckResponse { get }
}

protocol LogDetectable {
    var insidePaths: [String] { get }
    var outsidePaths: [String] { get }
    var dynamicPaths: [String] { get }
    
    func verifyInsidePaths() -> CheckResponse
    func verifyOutsidePaths() -> CheckResponse
    func verifyWriteLog() -> CheckResponse
    func verifyDynamicLog() -> CheckResponse
    func verifyLogDebug() -> CheckResponse
    func verifyLog() -> CheckResponse
}

struct LogDetection: LogDetectable {
    var insidePaths: [String] = [
        "21errwvwudsshgbhohfwud",
        "21f|gldbqrbvwdvk",
        "21lqvwdoohgbxqf3yhu",
        "2Dssolfdwlrqv2eodfnud4q1dss",
        "2Dssolfdwlrqv2F|gld1dss",
        "2Dssolfdwlrqv2IdnhFduulhu1dss",
        "2Dssolfdwlrqv2Iulgd1dss",
        "2Dssolfdwlrqv2Lf|1dss",
        "2Dssolfdwlrqv2LqwhoolVfuhhq1dss",
        "2Dssolfdwlrqv2P{Wxeh1dss",
        "2Dssolfdwlrqv2UrfnDss1dss",
        "2Dssolfdwlrqv2VEVhwwlqjv1dss",
        "2Dssolfdwlrqv2ZlqwhuErdug1dss",
        "2elq2edvk",
        "2elq2vk",
        "2hwf2dsw",
        "2hwf2dsw2vrxufhv1olvw1g2hohfwud1olvw",
        "2hwf2dsw2vrxufhv1olvw1g2vlohr1vrxufhv",
        "2hwf2dsw2xqghflpxv2xqghflpxv1olvw",
        "2hwf2vvk2vvkgbfrqilj",
        "2me2dpilgbsd|ordg1g|ole",
        "2me2mdloeuhdng1solvw",
        "2me2olemdloeuhdn1g|ole",
        "2me2o}pd",
        "2me2riivhwv1solvw",
        "2Oleudu|2PrelohVxevwudwh2F|gldVxevwudwh1g|ole",
        "2Oleudu|2PrelohVxevwudwh2G|qdplfOleudulhv2OlyhForfn1solvw",
        "2Oleudu|2PrelohVxevwudwh2G|qdplfOleudulhv2Yhhqf|1solvw",
        "2Oleudu|2PrelohVxevwudwh2PrelohVxevwudwh1g|ole",
        "2sulydwh2ydu2fdfkh2dsw2",
        "2sulydwh2ydu2ole2dsw",
        "2sulydwh2ydu2ole2dsw2",
        "2sulydwh2ydu2ole2f|gld",
        "2sulydwh2ydu2orj2v|vorj",
        "2sulydwh2ydu2preloh2Oleudu|2VEVhwwlqjv2Wkhphv",
        "2sulydwh2ydu2vwdvk",
        "2sulydwh2ydu2wps2f|gld1orj",
        "2sulydwh2ydu2Xvhuv2",
        "2V|vwhp2Oleudu|2OdxqfkGdhprqv2frp1lnh|1eerw1solvw",
        "2V|vwhp2Oleudu|2OdxqfkGdhprqv2frp1vdxuln1F|gld1Vwduwxs1solvw",
        "2xvu2elq2dsw",
        "2xvu2elq2vvkg",
        "2xvu2ole2olemdloeuhdn1g|ole",
        "2xvu2oleh{hf2f|gld2ilupzduh1vk",
        "2xvu2oleh{hf2viws0vhuyhu",
        "2xvu2oleh{hf2vvk0nh|vljq",
        "2xvu2velq2iulgd0vhuyhu",
        "2xvu2velq2vvkg",
        "2xvu2vkduh2mdloeuhdn2lqmhfwph1solvw",
        "2ydu2fdfkh2dsw",
        "2ydu2ole2dsw",
        "2ydu2ole2f|gld",
        "2ydu2ole2gsnj2lqir2prelohvxevwudwh1pg8vxpv",
        "2ydu2orj2dsw",
        "2ydu2orj2v|vorj",
        "2ydu2wps2f|gld1orj"
    ]
    
    var outsidePaths: [String] = ["xqghflpxv=22", "f|gld=22", "vlohr=22", "}eud=22"]
    
    var dynamicPaths: [String] = [
        "21iloh",
        "FxvwrpZlgjhwLfrqv",
        "f|fulsw",
        "f|gld",
        "F|gldVxevwudwh",
        "f|qmhfw",
        "ghfu|swhg",
        "iulgd",
        "o4phu74q",
        "PrelohVxevwudwh1g|ole",
        "SuhihuhqfhOrdghu",
        "UrfnhwErrwvwuds",
        "VVONlooVzlwfk1g|ole",
        "VVONlooVzlwfk51g|ole",
        "VxevwudwhOrdghu1g|ole",
        "WzhdnLqmhfw1g|ole",
        "ZhhOrdghu"
    ]
    
    func verifyInsidePaths() -> CheckResponse {
        for path in insidePaths {
            if FileManager.default.fileExists(atPath: path) {
                return .verdade
            }
        }
        return .mentira
    }
    
    func verifyOutsidePaths() -> CheckResponse {
        let app = UIApplication.shared
        
        return outsidePaths
            .lazy
            .map(process)
            .compactMap(URL.init(string:))
            .contains(where: app.canOpenURL) ? .verdade : .mentira
    }
    
    func verifyWriteLog() -> CheckResponse {
        let path = "2sulydwh2ydu"
        return FileManager.default.isWritableFile(atPath: process(path)) ? .verdade : .mentira
    }
    
    func verifyDynamicLog() -> CheckResponse {
        let processedNames = dynamicPaths.map({ process($0).lowercased() })
        
        let dyldImageRange = 0..<_dyld_image_count()
        let imageNames = dyldImageRange.compactMap(_dyld_get_image_name).map(String.init(cString:)).map({ $0.lowercased() })
        
        return processedNames.contains(where: imageNames.contains) ? .verdade : .mentira
    }
    
    func verifyLogDebug() -> CheckResponse {
        getppid() != 1 ? .verdade : .mentira
    }
    
    func verifyLog() -> CheckResponse {
        var info = kinfo_proc()
        var mib: [Int32] = [CTL_KERN, KERN_PROC, KERN_PROC_PID, getpid()]
        var size = MemoryLayout<kinfo_proc>.stride
        let junk = sysctl(&mib, UInt32(mib.count), &info, &size, nil, 0)
        assert(junk == 0, "sysctl failed")
        
        return (info.kp_proc.p_flag & P_TRACED) != 0 ? .verdade : .mentira
    }
    
    func process(_ rawWord: String) -> String {
        rawWord.compactMap(\.asciiValue).reduce(into: String()) { result, word in
            result += String(UnicodeScalar(word - 3))
        }
    }
}
