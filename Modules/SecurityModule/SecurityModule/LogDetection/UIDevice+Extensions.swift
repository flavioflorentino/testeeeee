import UIKit

extension UIDevice: LogInfoContract {
    public var isUnreal: CheckResponse {
        guard ProcessInfo().environment["SIMULATOR_DEVICE_NAME"] == nil else {
            return .verdade
        }
        
        #if targetEnvironment(simulator)
        return .verdade
        #else
        return .mentira
        #endif
    }
    
    public var isLogWorking: CheckResponse {
        let detector = LogDetection()
        
        let methods = [
            detector.verifyOutsidePaths(),
            detector.verifyInsidePaths(),
            detector.verifyWriteLog(),
            detector.verifyDynamicLog()
        ]
        
        if methods.allSatisfy({ $0 == .mentira }) {
            return .mentira
        }
        
        return .verdade
    }
    
    public var isDebugLogWorking: CheckResponse {
        let detector = LogDetection()
        
        let methods = [
            detector.verifyLog(),
            detector.verifyLogDebug()
        ]
        
        if methods.allSatisfy({ $0 == .mentira }) {
            return .mentira
        }
        
        return .verdade
    }
}
