import Core
import Foundation

enum LogDetectionServiceEndPoint {
    case sendLog(detected: Bool, isUnreal: Bool)
}

extension LogDetectionServiceEndPoint: ApiEndpointExposable {
    var path: String {
        switch self {
        // Used for PF app
        case .sendLog:
            return "api/updateDeviceDetails.json"
        }
    }
    
    var method: HTTPMethod {
        .post
    }
    
    var body: Data? {
        switch self {
        case let .sendLog(detected, isReal):
            // temporary keys
            return ["google_service_framework": detected, "drm_unique_id": isReal].toData()
        }
    }
}
