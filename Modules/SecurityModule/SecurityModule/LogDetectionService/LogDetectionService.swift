import Core
import FeatureFlag
import Foundation
import UIKit

public protocol LogDetectionServicing {
    func sendLog(log: LogInfoContract)
}

public extension LogDetectionServicing {
    func sendLog(_ log: LogInfoContract = UIDevice.current) {
        sendLog(log: log)
    }
}

public final class LogDetectionService: LogDetectionServicing {
    public init () { }
    
    public func sendLog(log: LogInfoContract) {
        guard FeatureManager.isActive(.featureAppSecVerifyLogDetectionBool) else {
            return
        }
        
        let isLogDetected = log.isLogWorking == .verdade
        let isUnreal = log.isUnreal == .verdade
        
        let endpoint = LogDetectionServiceEndPoint.sendLog(detected: isLogDetected, isUnreal: isUnreal)
        
        Api<NoContent>(endpoint: endpoint).execute { _ in }
    }
}
