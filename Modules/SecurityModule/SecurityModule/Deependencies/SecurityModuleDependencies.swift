import AnalyticsModule
import Core
import FeatureFlag

typealias SecurityModuleDependencies =
    HasMainQueue &
    HasAnalytics &
    HasFeatureManager

final class SecurityModuleDependencyContainer: SecurityModuleDependencies {
    lazy var mainQueue = DispatchQueue.main
    lazy var analytics: AnalyticsProtocol = Analytics.shared
    lazy var featureManager: FeatureManagerContract = FeatureManager.shared
}
