import Foundation

/// Wraper for the observer token received from NotificationCenter.addObserver(forName:object:queue:using:) to unregister it in deinit.
final class NotificationObserverToken {
    let notificationCenter: NotificationCenter
    let token: Any

    init(token: Any, notificationCenter: NotificationCenter = .default) {
        self.notificationCenter = notificationCenter
        self.token = token
    }

    deinit {
        notificationCenter.removeObserver(token)
    }
}
