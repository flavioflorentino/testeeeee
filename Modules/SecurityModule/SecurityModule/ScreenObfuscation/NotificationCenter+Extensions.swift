import Foundation

extension NotificationCenter {
    /// Convenience wrapper for addObserver(forName:object:queue:using:) that returns our custom NotificationObserverToken.
    func observe(name: NSNotification.Name?,
                 object obj: Any?,
                 queue: OperationQueue?,
                 using block: @escaping (Notification) -> Void) -> NotificationObserverToken {
        let token = addObserver(forName: name, object: obj, queue: queue, using: block)
        return NotificationObserverToken(token: token, notificationCenter: self)
    }
}
