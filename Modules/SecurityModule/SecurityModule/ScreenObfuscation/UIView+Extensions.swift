import FeatureFlag
import Foundation
import UIKit

public protocol ObfuscableView {
    func setHiddenComponentsInAppSwitcher(_ hide: Bool)
    func applyObfuscation()
    func removeObfuscation()
}
extension UIView: ObfuscableView {
    public func setHiddenComponentsInAppSwitcher(_ hide: Bool) {
        guard FeatureManager().isActive(.featureAppSwitcherObfuscation) else {
            return
        }

        let notificationCenter = NotificationCenter.default
        
        hide ? addObservers(notificationCenter) : removeObservers(notificationCenter)
    }
    
    @objc
    public func applyObfuscation() {
        UIView.animate(withDuration: 0.5) {
            self.alpha = 0
        }
    }
    
    @objc
    public func removeObfuscation() {
        UIView.animate(withDuration: 0.5) {
            self.alpha = 1
        }
    }
    
    private func addObservers(_ notificationCenter: NotificationCenter) {
        notificationCenter.addObserver(self,
                                       selector: #selector(applyObfuscation),
                                       name: UIApplication.willResignActiveNotification,
                                       object: nil)
        
        // treatment for when is the standard flow
        notificationCenter.addObserver(self,
                                       selector: #selector(removeObfuscation),
                                       name: UIApplication.didBecomeActiveNotification,
                                       object: nil)
        
        // treatment for when are going to open a deeplink
        notificationCenter.addObserver(self,
                                       selector: #selector(removeObfuscation),
                                       name: UIApplication.willEnterForegroundNotification,
                                       object: nil)
    }
    
    private func removeObservers(_ notificationCenter: NotificationCenter) {
        notificationCenter.removeObserver(self,
                                          name: UIApplication.willResignActiveNotification,
                                          object: nil)
        
        notificationCenter.removeObserver(self,
                                          name: UIApplication.didBecomeActiveNotification,
                                          object: nil)
        
        notificationCenter.removeObserver(self,
                                          name: UIApplication.willEnterForegroundNotification,
                                          object: nil)
    }
}
