import Foundation
import UI
import UIKit

public protocol AppSwitcherObfuscating {
    init(window: UIWindow)
    func setupObfuscationView(image: UIImage, appType: AppType)
}

public final class AppSwitcherObfuscator: AppSwitcherObfuscating {
    private var notificationsObservers: [NotificationObserverToken] = []
    private let window: UIWindow
    
    public required init(window: UIWindow) {
        self.window = window
    }
    
    public func setupObfuscationView(image: UIImage, appType: AppType) {
        notificationsObservers = []
        
        let notificationCenter = NotificationCenter.default
        let obfuscationView = createObfuscationView(image: image, appType: appType)
        
        let willResignActiveNotification = notificationCenter.observe(
            name: UIApplication.willResignActiveNotification,
            object: nil,
            queue: nil
        ) { [weak self] _ in
            self?.addObfuscationView(obfuscationView)
        }
        
        // treatment for when is the standard flow
        let didBecomeActiveNotification = notificationCenter.observe(
            name: UIApplication.didBecomeActiveNotification,
            object: nil,
            queue: nil
        ) { [weak self] _ in
            self?.removeObfuscationView(obfuscationView)
        }
        
        // treatment for when are going to open a deeplink
        let willEnterForegroundNotification = notificationCenter.observe(
            name: UIApplication.willEnterForegroundNotification,
            object: nil,
            queue: nil
        ) { [weak self] _ in
            self?.removeObfuscationView(obfuscationView)
        }
        
        notificationsObservers.append(willResignActiveNotification)
        notificationsObservers.append(didBecomeActiveNotification)
        notificationsObservers.append(willEnterForegroundNotification)
    }
    
    public func addObfuscationView(_ obfuscationView: UIView) {
        obfuscationView.alpha = 0.0
        window.addSubview(obfuscationView)
        setupView(obfuscationView: obfuscationView)
        window.bringSubviewToFront(obfuscationView)
        
        UIView.animate(withDuration: 0.5) {
            obfuscationView.alpha = 1.0
        }
    }
    
    public func removeObfuscationView(_ obfuscationView: UIView) {
        UIView.animate(
            withDuration: 0.5,
            animations: {
                obfuscationView.alpha = 0.0
            }, completion: {_ in
                obfuscationView.removeFromSuperview()
            }
        )
    }
    
    func createObfuscationView(image: UIImage, appType: AppType) -> UIView {
        ObfuscationView(imageView: UIImageView(image: image), appType: appType)
    }
    
    func setupView(obfuscationView: UIView) {
        obfuscationView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            obfuscationView.bottomAnchor.constraint(equalTo: window.bottomAnchor),
            obfuscationView.topAnchor.constraint(equalTo: window.topAnchor),
            obfuscationView.leadingAnchor.constraint(equalTo: window.leadingAnchor),
            obfuscationView.trailingAnchor.constraint(equalTo: window.trailingAnchor)
        ])
    }
}
