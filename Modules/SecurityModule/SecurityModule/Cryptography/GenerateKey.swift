import Foundation
import CryptoSwift

public class GenerateKey {
    public enum GenerateKeyErrors: Error {
        case generateKey
        case calculate
    }
    
    public init () {}

    public func getSecretKey(password: String, salt: String, iterations: Int = 1_000, bytes keyLength: Int = 32) -> Result <[UInt8], GenerateKeyErrors> {
        do {
            let generate = try PKCS5.PBKDF2(password: password.bytes, salt: salt.bytes, iterations: iterations, keyLength: keyLength, variant: .sha256)
            return calculate(generate: generate)
        } catch {
            return .failure(.generateKey)
        }
    }
    
    fileprivate func calculate(generate: PKCS5.PBKDF2) -> Result<[UInt8], GenerateKeyErrors> {
        do {
            let result = try generate.calculate()
            return .success(result)
        } catch {
            return .failure(.calculate)
        }
    }
}
