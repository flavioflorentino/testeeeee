import CryptoSwift

public class CryptoAES {
    public enum ErrorCrypto: Error, Equatable {
        case decodedFrom64
        case encodeTo64
        case decryptError(CryptoSwift.AES.Error?)
        case encryptionError(CryptoSwift.AES.Error?)
    }
    
    private let iv: [UInt8]
    private let key: [UInt8]
    
    public init(iv: [UInt8], key: [UInt8]) {
        self.iv = iv
        self.key = key
    }
    
    public init(keyInbase64 key: String, ivInBase64 iv: String) {
        self.key = key.decodedFrom64()
        self.iv = iv.decodedFrom64()
    }
    
    public func decrypt(encryptValue: String) -> Result<String, ErrorCrypto> {
        let decodedFrom64 = encryptValue.decodedFrom64()
        if decodedFrom64.isEmpty {
            return .failure(.decodedFrom64)
        }
        do {
            let decrypt = try AES(key: key, blockMode: CBC(iv: iv), padding: .pkcs5).decrypt(decodedFrom64)
            let result = String(decoding: decrypt, as: UTF8.self)
            return .success(result)
        } catch {
            return .failure(.decryptError(error as? AES.Error))
        }
    }
    
    public func encryption(plainText: String) -> Result<String, ErrorCrypto> {
        do {
            let encryptData = try AES(key: key, blockMode: CBC(iv: iv), padding: .pkcs5).encrypt(plainText.bytes)
            if let result = encryptData.toBase64() {
                return .success(result)
            } else {
                return .failure(.encodeTo64)
            }
        } catch {
            return .failure(.encryptionError(error as? AES.Error))
        }
    }
}

extension String {
    func decodedFrom64(options: Data.Base64DecodingOptions = .ignoreUnknownCharacters) -> [UInt8] {
        Data(base64Encoded: self, options: .ignoreUnknownCharacters)?.bytes ?? []
    }
}
