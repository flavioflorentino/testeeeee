import UIKit

public enum AppAccessFactory {
    public static func make(window: UIWindow?, logoutAction: (() -> Void)?) -> AppAccessController {
        let container = SecurityModuleDependencyContainer()
        let service = AppAccessService()
        let presenter = AppAccessPresenter()
        let interactor = AppAccessInteractor(logoutAction: logoutAction,
                                             service: service,
                                             presenter: presenter,
                                             dependencies: container)
        let view = AppAccessView()
        let controller = AppAccessController(window: window, view: view, interactor: interactor)

        presenter.controller = controller

        return controller
    }
}
