import UI
import UIKit

public protocol AppAccessProtocol {
    func applicationDidFinishLaunching(with properties: AppAccessProperties)
    func applicationWillResignActive(with properties: AppAccessProperties)
    func applicationDidEnterBackground(with properties: AppAccessProperties)
    func applicationDidBecomeActive(with properties: AppAccessProperties)
}

protocol AppAccessDisplaying: AnyObject {
    func showAccessProtectionView()
    func removeAccessProtectionView(duration: TimeInterval, completion: (() -> Void)?)
    func updateLogoutButtonTitle(_ title: String)
    func showAccessButtons(_ show: Bool)
    func displaySecurityErrorPopup(with title: String, message: String, buttonTitle: String)
}

public final class AppAccessController: AppAccessProtocol, AppAccessDisplaying {
    private weak var window: UIWindow?
    private let accessView: AppSecurityViewProtocol
    private let interactor: AppAccessInteracting

    init(window: UIWindow?, view: AppSecurityViewProtocol, interactor: AppAccessInteracting) {
        self.window = window
        self.accessView = view
        self.interactor = interactor
        bindViewActions()
    }

    // MARK: - AppAccessProtocol

    public func applicationDidFinishLaunching(with properties: AppAccessProperties) {
        interactor.shouldChangeState(for: .didFinishLaunching, with: properties)
    }

    public func applicationWillResignActive(with properties: AppAccessProperties) {
        interactor.shouldChangeState(for: .willResignActive, with: properties)
    }

    public func applicationDidEnterBackground(with properties: AppAccessProperties) {
        interactor.shouldChangeState(for: .didEnterBackground, with: properties)
    }

    public func applicationDidBecomeActive(with properties: AppAccessProperties) {
        interactor.shouldChangeState(for: .didBecomeActive, with: properties)
    }

    // MARK: - AppAccessDisplaying

    func showAccessProtectionView() {
        window?.addSubview(accessView)
        window?.bringSubviewToFront(accessView)
        accessView.snp.makeConstraints { $0.edges.equalToSuperview() }
    }

    func removeAccessProtectionView(duration: TimeInterval, completion: (() -> Void)?) {
        UIView.animate(
            withDuration: duration,
            animations: {
                self.accessView.alpha = 0
            }, completion: { _ in
                self.accessView.removeFromSuperview()
                self.accessView.alpha = 1
                self.interactor.accessViewRemovedFromWindow()
                completion?()
            }
        )
    }

    func updateLogoutButtonTitle(_ title: String) {
        accessView.updateLogoutButtonTitle(title)
    }

    func showAccessButtons(_ show: Bool) {
        accessView.showButtons(show)
    }

    func displaySecurityErrorPopup(with title: String, message: String, buttonTitle: String) {
        let primaryButton = ApolloAlertAction(title: buttonTitle, completion: { })

        window?.rootViewController?.showApolloAlert(title: title,
                                                    subtitle: message,
                                                    primaryButtonAction: primaryButton)
    }
}

private extension AppAccessController {
    func bindViewActions() {
        accessView.accessAction = { [weak self] in
            self?.interactor.didTapAccess()
        }

        accessView.logoutAction = { [weak self] in
            self?.interactor.didTapLogout()
        }
    }
}
