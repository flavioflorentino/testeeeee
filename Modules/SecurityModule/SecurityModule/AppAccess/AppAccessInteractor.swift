import AnalyticsModule
import Foundation
import LocalAuthentication

enum AppEvent {
    case didFinishLaunching
    case didBecomeActive
    case willResignActive
    case didEnterBackground
    case none
}

enum AppSecurityState {
    case visible
    case removingProtection
    case notVisible
    case evaluating
    case fallback
}

protocol AppAccessInteracting: AnyObject {
    func shouldChangeState(for appEvent: AppEvent, with properties: AppAccessProperties)
    func accessViewRemovedFromWindow()
    func didTapAccess()
    func didTapLogout()
}

final class AppAccessInteractor: AppAccessInteracting {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let service: AppAccessServicing
    private let presenter: AppAccessPresenting
    private var logoutAction: (() -> Void)?

    private var lastAppEvent: AppEvent = .none
    private(set) var currentState: AppSecurityState = .notVisible

    init(logoutAction: (() -> Void)?, service: AppAccessServicing, presenter: AppAccessPresenting, dependencies: Dependencies) {
        self.logoutAction = logoutAction
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }

    // MARK: - AppAccessInteracting

    func shouldChangeState(for appEvent: AppEvent, with properties: AppAccessProperties) {
        switch (currentState, (lastAppEvent, appEvent)) {
        case (.notVisible, (_, .didFinishLaunching)),
             (.notVisible, (_, .willResignActive)):
            shouldShowProtectionView(properties)

        case (.visible, (.didFinishLaunching, .didBecomeActive)),
             (.visible, (.didEnterBackground, .didBecomeActive)):
            showBiometryInput(properties)

        case (.visible, (_, .didBecomeActive)):
            hideProtectionView()

        default:
            break
        }

        lastAppEvent = appEvent
    }

    func accessViewRemovedFromWindow() {
        currentState = .notVisible
    }

    func didTapAccess() {
        let properties = [
            AppProtectionEventProperty(name: .screenName, value: .fallback),
            AppProtectionEventProperty(name: .buttonName, value: .auth)
        ]
        trackEvent(type: .buttonClicked, properties: properties)
        evaluateBiometry()
    }

    func didTapLogout() {
        let properties = [
            AppProtectionEventProperty(name: .screenName, value: .fallback),
            AppProtectionEventProperty(name: .buttonName, value: .logout)
        ]
        trackEvent(type: .buttonClicked, properties: properties)
        logoutAction?()
        currentState = .notVisible
    }
}

private extension AppAccessInteractor {
    func trackEvent(type: AppProtectionEvent, properties: [AppProtectionEventProperty]) {
        let tracker = SecurityModuleTracker(eventType: type, eventProperties: properties)
        dependencies.analytics.log(tracker)
    }

    func shouldShowProtectionView(_ properties: AppAccessProperties) {
        guard properties.isFlagOn, properties.isLoggedIn, properties.isProtectionActive else { return }

        presenter.showAccessProtectionView()
        presenter.showAccessButtons(false)
        currentState = .visible
    }

    func hideProtectionView() {
        currentState = .removingProtection
        presenter.removeAccessProtectionView()
    }

    func showBiometryInput(_ properties: AppAccessProperties) {
        trackEvent(type: .dialogViewed,
                   properties: [AppProtectionEventProperty(name: .dialogName, value: .protectionRequested)])

        presenter.updateUsername(properties.username)
        evaluateBiometry()
    }

    func evaluateBiometry() {
        currentState = .evaluating
        presenter.showAccessButtons(false)

        let reason = service.biometricType == .faceID
            ? Strings.AppProtection.FaceId.reason
            : Strings.AppProtection.TouchId.reason

        service.evaluateBiometry(reason: reason) { [weak self] authorized, error in
            if authorized {
                self?.hideProtectionView()
            } else {
                self?.handleUnauthorizedBiometry(error)
            }
        }
    }

    func handleUnauthorizedBiometry(_ error: LAError?) {
        trackEvent(type: .screenViewed,
                   properties: [AppProtectionEventProperty(name: .screenName, value: .fallback)])

        currentState = .fallback
        presenter.showAccessButtons(true)

        if case .passcodeNotSet = error?.code {
            presenter.presentSecurityErrorPopup()
        }
    }
}
