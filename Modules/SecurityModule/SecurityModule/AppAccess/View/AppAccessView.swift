import AssetsKit
import UI
import UIKit

protocol AppSecurityViewProtocol: UIView {
    var accessAction: (() -> Void)? { get set }
    var logoutAction: (() -> Void)? { get set }
    func showButtons(_ show: Bool)
    func updateLogoutButtonTitle(_ title: String)
}

private extension AppAccessView.Layout {
    enum Size {
        static let imageSize = CGSize(width: 200, height: 64)
    }
}

final class AppAccessView: UIView, ViewConfiguration, AppSecurityViewProtocol {
    fileprivate enum Layout { }

    var accessAction: (() -> Void)?
    var logoutAction: (() -> Void)?

    private lazy var backgroundImageView = UIImageView(image: Resources.Logos.consumerLogoWhite.image)

    private lazy var accessButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.AppProtection.AccessButton.title, for: .normal)
        button.addTarget(self, action: #selector(didTapAccess), for: .touchUpInside)
        button.buttonStyle(SecondaryButtonStyle())
            .with(\.borderColor, (Colors.Style.white(.light), UIControl.State.normal))
            .with(\.textColor, (Colors.Style.white(.light), UIControl.State.normal))
        return button
    }()

    private lazy var logoutButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(didTapLogout), for: .touchUpInside)
        return button
    }()

    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func buildViewHierarchy() {
        addSubview(backgroundImageView)
        addSubview(accessButton)
        addSubview(logoutButton)
    }

    func setupConstraints() {
        backgroundImageView.snp.makeConstraints {
            $0.centerX.centerY.equalToSuperview()
            $0.size.equalTo(Layout.Size.imageSize)
        }

        accessButton.snp.makeConstraints {
            $0.leading.trailing.equalTo(logoutButton)
            $0.bottom.equalTo(logoutButton.snp.top).offset(-Spacing.base04)
        }

        logoutButton.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.bottom.equalTo(compatibleSafeArea.bottom).offset(-Spacing.base05)
        }
    }

    func configureViews() {
        backgroundColor = .branding600()
    }

    // MARK: - AppSecurityViewProtocol

    func updateLogoutButtonTitle(_ title: String) {
        logoutButton.setTitle(title, for: .normal)
        logoutButton.buttonStyle(LinkButtonStyle())
            .with(\.textAttributedColor, (Colors.Style.white(.light), UIControl.State.normal))
    }

    func showButtons(_ show: Bool) {
        accessButton.isHidden = !show
        logoutButton.isHidden = !show
    }
}

// MARK: - Button Actions

@objc
private extension AppAccessView {
    func didTapAccess() {
        accessAction?()
    }

    func didTapLogout() {
        logoutAction?()
    }
}
