import Foundation
import LocalAuthentication

protocol AppAccessServicing {
    var biometricType: DeviceBiometricType { get }
    func evaluateBiometry(reason: String, completion: @escaping (_ authorized: Bool, LAError?) -> Void)
}

final class AppAccessService: AppAccessServicing {
    var biometricType: DeviceBiometricType { DeviceSecurity.biometricType() }

    func evaluateBiometry(reason: String, completion: @escaping (Bool, LAError?) -> Void) {
        DeviceSecurity.evaluateBiometry(reasonDescription: reason, completion: completion)
    }
}
