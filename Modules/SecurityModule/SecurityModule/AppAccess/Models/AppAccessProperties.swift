import Foundation

public struct AppAccessProperties {
    let isFlagOn: Bool
    let isLoggedIn: Bool
    let isProtectionActive: Bool
    let username: String

    public init(isFlagOn: Bool, isLoggedIn: Bool, isProtectionActive: Bool, username: String) {
        self.isFlagOn = isFlagOn
        self.isLoggedIn = isLoggedIn
        self.isProtectionActive = isProtectionActive
        self.username = username
    }
}
