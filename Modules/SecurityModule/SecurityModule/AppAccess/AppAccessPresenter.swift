import Foundation

protocol AppAccessPresenting: AnyObject {
    var controller: AppAccessDisplaying? { get set }
    func showAccessProtectionView()
    func removeAccessProtectionView()
    func updateUsername(_ username: String)
    func showAccessButtons(_ show: Bool)
    func presentSecurityErrorPopup()
}

final class AppAccessPresenter: AppAccessPresenting {
    weak var controller: AppAccessDisplaying?

    // MARK: - AppAccessPresenting

    func showAccessProtectionView() {
        controller?.showAccessProtectionView()
    }

    func removeAccessProtectionView() {
        controller?.removeAccessProtectionView(duration: 0.3, completion: nil)
    }

    func updateUsername(_ username: String) {
        let buttonTitle = Strings.AppProtection.LogoutButton.title(username)
        controller?.updateLogoutButtonTitle(buttonTitle)
    }

    func showAccessButtons(_ show: Bool) {
        controller?.showAccessButtons(show)
    }

    func presentSecurityErrorPopup() {
        controller?.displaySecurityErrorPopup(
            with: Strings.AppProtection.Popup.Error.title,
            message: Strings.AppProtection.Popup.Error.message,
            buttonTitle: Strings.AppProtection.Popup.Error.action
        )
    }
}
