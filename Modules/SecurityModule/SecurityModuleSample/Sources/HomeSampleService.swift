import Foundation

protocol HomeSampleServicing {}

final class HomeSampleService { }

// MARK: - HomeSampleServicing
extension HomeSampleService: HomeSampleServicing {}
