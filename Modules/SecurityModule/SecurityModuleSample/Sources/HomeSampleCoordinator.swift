import UIKit

enum HomeSampleAction {
    case openJailbreak
    case openSimulator
    case openDebug
}

protocol HomeSampleCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: HomeSampleAction, value: Bool)
}

final class HomeSampleCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - HomeSampleCoordinating
extension HomeSampleCoordinator: HomeSampleCoordinating {
    func perform(action: HomeSampleAction, value: Bool) {
        let message: String
        
        switch action {
        case .openJailbreak:
            message = "This device is jailbroken: \(value)"
            
        case .openSimulator:
            message = "This device is a simulator: \(value)"
            
        case .openDebug:
            message = "This device is in debug mode: \(value)"
        }
        
        showMessage(message)
    }
    
    private func showMessage(_ msg: String) {
        let alert = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default)
        alert.addAction(action)
        viewController?.present(alert, animated: true, completion: nil)
    }
}
