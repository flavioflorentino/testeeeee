import Foundation
import UIKit

protocol HomeSamplePresenting: AnyObject {
    var viewController: HomeSampleDisplay? { get set }
    func didNextStep(action: HomeSampleAction, value: Bool)
}

final class HomeSamplePresenter {
    private let coordinator: HomeSampleCoordinating
    weak var viewController: HomeSampleDisplay?
    
    init(coordinator: HomeSampleCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - HomeSamplePresenting
extension HomeSamplePresenter: HomeSamplePresenting {
    func didNextStep(action: HomeSampleAction, value: Bool) {
        coordinator.perform(action: action, value: value)
    }
}
