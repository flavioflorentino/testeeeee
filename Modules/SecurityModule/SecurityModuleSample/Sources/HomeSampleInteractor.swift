import Foundation
import SecurityModule

protocol HomeSampleInteracting: AnyObject {
    func verifyJailbreak()
    func verifySimulator()
    func verifyDebug()
}

final class HomeSampleInteractor {
    private let service: HomeSampleServicing
    private let presenter: HomeSamplePresenting
    
    init(service: HomeSampleServicing, presenter: HomeSamplePresenting) {
        self.service = service
        self.presenter = presenter
    }
}

// MARK: - HomeSampleInteracting
extension HomeSampleInteractor: HomeSampleInteracting {
    func verifyJailbreak() {
        let hasJailbreak = UIDevice.current.isLogWorking == .verdade
        presenter.didNextStep(action: .openJailbreak, value: hasJailbreak)
    }
    
    func verifySimulator() {
        let isSimulator = UIDevice.current.isUnreal == .verdade
        presenter.didNextStep(action: .openSimulator, value: isSimulator)
    }
    
    func verifyDebug() {
        let isDebugMode = UIDevice.current.isDebugLogWorking == .verdade
        presenter.didNextStep(action: .openDebug, value: isDebugMode)
    }
}
