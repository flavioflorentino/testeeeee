import SnapKit
import UI
import UIKit

protocol HomeSampleDisplay: AnyObject {
    func displaySomething()
}

private extension HomeSampleViewController.Layout {
    enum Button {
        static let height = 60
        static let width = 250
    }
    
    enum Spacing {
        static let stackView = CGFloat(50)
    }
}

final class HomeSampleViewController: UIViewController {
    fileprivate enum Layout {}
    
    private let interactor: HomeSampleInteractor
    
    private lazy var jailbreakButton: UIButton = {
        let button = UIButton()
        button.setTitle("Verify Jailbreak", for: .normal)
        button.backgroundColor = .red
        button.addTarget(self, action: #selector(verifyJailbreak), for: .touchUpInside)
        button.contentMode = .center
        
        return button
    }()
    
    private lazy var simulatorButton: UIButton = {
        let button = UIButton()
        button.setTitle("Verify Simulator", for: .normal)
        button.backgroundColor = .blue
        button.addTarget(self, action: #selector(verifySimulator), for: .touchUpInside)
        button.contentMode = .center
        
        return button
    }()
    
    private lazy var debugButton: UIButton = {
        let button = UIButton()
        button.setTitle("Verify Debug mode", for: .normal)
        button.backgroundColor = .darkGray
        button.addTarget(self, action: #selector(verifyDebug), for: .touchUpInside)
        button.contentMode = .center
        
        return button
    }()
    
    private lazy var stackview: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = Layout.Spacing.stackView
        stackView.distribution = .fillEqually
        
        return stackView
    }()
    
    init(interactor: HomeSampleInteractor) {
        self.interactor = interactor
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildViewHierarchy()
        setupConstraints()
        configureViews()
    }
    
    func buildViewHierarchy() {
        stackview.addArrangedSubview(jailbreakButton)
        stackview.addArrangedSubview(simulatorButton)
        stackview.addArrangedSubview(debugButton)
        view.addSubview(stackview)
    }
    
    func configureViews() {
        view.backgroundColor = .white
    }
    
    func setupConstraints() {
        stackview.snp.makeConstraints {
            $0.centerX.centerY.equalToSuperview()
        }
        
        jailbreakButton.snp.makeConstraints {
            $0.height.equalTo(Layout.Button.height)
            $0.width.equalTo(Layout.Button.width)
        }
        
        simulatorButton.snp.makeConstraints {
            $0.height.equalTo(Layout.Button.height)
            $0.width.equalTo(Layout.Button.width)
        }
        
        debugButton.snp.makeConstraints {
            $0.height.equalTo(Layout.Button.height)
            $0.width.equalTo(Layout.Button.width)
        }
    }
    
    @objc
    func verifyJailbreak() {
        interactor.verifyJailbreak()
    }
    
    @objc
    func verifySimulator() {
        interactor.verifySimulator()
    }
    
    @objc
    func verifyDebug() {
        interactor.verifyDebug()
    }
}

// MARK: HomeSampleDisplay
extension HomeSampleViewController: HomeSampleDisplay {
    func displaySomething() { }
}
