import Foundation

enum HomeSampleFactory {
    static func make() -> HomeSampleViewController {
        let service: HomeSampleServicing = HomeSampleService()
        let coordinator: HomeSampleCoordinating = HomeSampleCoordinator()
        let presenter: HomeSamplePresenting = HomeSamplePresenter(coordinator: coordinator)
        let interactor = HomeSampleInteractor(service: service, presenter: presenter)
        let viewController = HomeSampleViewController(interactor: interactor)
        
        coordinator.viewController = viewController
        presenter.viewController = viewController
        
        return viewController
    }
}
