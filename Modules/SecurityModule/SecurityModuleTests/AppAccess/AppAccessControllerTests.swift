@testable import SecurityModule
import UI
import XCTest

private class RootControllerSpy: UIViewController {
    private(set) var viewControllerPresented: UIViewController?
    private(set) var isPresentViewControllerCalled = false

    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        viewControllerPresented = viewControllerToPresent
        isPresentViewControllerCalled = true
        super.present(viewControllerToPresent, animated: false)
        completion?()
    }
}

private class AppSecurityViewProtocolSpy: UIView, AppSecurityViewProtocol {
    private(set) var showButtonsCallCount = 0
    private(set) var updateLogoutButtonTitleCallCount = 0

    var accessAction: (() -> Void)?
    var logoutAction: (() -> Void)?

    func showButtons(_ show: Bool) {
        showButtonsCallCount += 1
    }

    func updateLogoutButtonTitle(_ title: String) {
        updateLogoutButtonTitleCallCount += 1
    }
}

private class AppAccessInteractingSpy: AppAccessInteracting {
    private(set) var shouldChangeStateCallCount = 0
    private(set) var accessViewRemovedFromWindowCallCount = 0
    private(set) var didTapAccessCallCount = 0
    private(set) var didTapLogoutCallCount = 0
    private(set) var appEvent: AppEvent = .none

    func shouldChangeState(for appEvent: AppEvent, with properties: AppAccessProperties) {
        shouldChangeStateCallCount += 1
        self.appEvent = appEvent
    }

    func accessViewRemovedFromWindow() {
        accessViewRemovedFromWindowCallCount += 1
    }

    func didTapAccess() {
        didTapAccessCallCount += 1
    }

    func didTapLogout() {
        didTapLogoutCallCount += 1
    }
}

final class AppAccessControllerTests: XCTestCase {
    private let windowSpy = UIWindow()
    private let rootControllerSpy = RootControllerSpy()
    private let viewSpy = AppSecurityViewProtocolSpy()
    private let interactorSpy = AppAccessInteractingSpy()
    private let properties = AppAccessProperties(isFlagOn: true,
                                                 isLoggedIn: true,
                                                 isProtectionActive: true,
                                                 username: "")

    private lazy var sut = AppAccessController(window: windowSpy, view: viewSpy, interactor: interactorSpy)

    func testApplicationDidFinishLaunching_ShouldCallShouldChangeStateWithDidFinishLaunchingEvent() {
        sut.applicationDidFinishLaunching(with: properties)

        XCTAssertEqual(interactorSpy.shouldChangeStateCallCount, 1)
        XCTAssertEqual(interactorSpy.appEvent, .didFinishLaunching)
    }

    func testApplicationWillResignActive_ShouldCallShouldChangeStateWithWillResignActiveEvent() {
        sut.applicationWillResignActive(with: properties)

        XCTAssertEqual(interactorSpy.shouldChangeStateCallCount, 1)
        XCTAssertEqual(interactorSpy.appEvent, .willResignActive)
    }

    func testApplicationDidEnterBackground_ShouldCallShouldChangeStateWithDidEnterBackgroundEvent() {
        sut.applicationDidEnterBackground(with: properties)

        XCTAssertEqual(interactorSpy.shouldChangeStateCallCount, 1)
        XCTAssertEqual(interactorSpy.appEvent, .didEnterBackground)
    }

    func testApplicationDidBecomeActive_ShouldCallShouldChangeStateWithDidBecomeActiveEvent() {
        sut.applicationDidBecomeActive(with: properties)

        XCTAssertEqual(interactorSpy.shouldChangeStateCallCount, 1)
        XCTAssertEqual(interactorSpy.appEvent, .didBecomeActive)
    }

    func testShowAccessProtectionView_ShouldAddViewToWindowSubviews() {
        sut.showAccessProtectionView()

        XCTAssertTrue(windowSpy.subviews.contains { $0 is AppSecurityViewProtocol })
    }

    func testRemoveAccessProtectionView_ShouldRemoveViewFromWindow() {
        windowSpy.addSubview(viewSpy)
        let expec = expectation(description: "Removing view")

        sut.removeAccessProtectionView(duration: 0) {
            expec.fulfill()
        }

        wait(for: [expec], timeout: 0.1)
        XCTAssertFalse(windowSpy.subviews.contains { $0 is AppSecurityViewProtocol })
        XCTAssertEqual(viewSpy.alpha, 1)
        XCTAssertEqual(interactorSpy.accessViewRemovedFromWindowCallCount, 1)
    }

    func testUpdateLogoutButtonTitle_ShouldCallViewUpdateLogoutButtonTitle() {
        sut.updateLogoutButtonTitle("")

        XCTAssertEqual(viewSpy.updateLogoutButtonTitleCallCount, 1)
    }

    func testShowAccessButtons_ShouldCallViewShowButtons() {
        sut.showAccessButtons(true)

        XCTAssertEqual(viewSpy.showButtonsCallCount, 1)
    }

    func testDisplaySecurityErrorPopup_ShouldPresentPopupFromWindowRootViewController() {
        windowSpy.rootViewController = rootControllerSpy

        sut.displaySecurityErrorPopup(with: "", message: "", buttonTitle: "")

        XCTAssertTrue(rootControllerSpy.isPresentViewControllerCalled)
    }

    func testAccessAction_ShouldCallInteractorDidTapAccess() {
        sut = AppAccessController(window: windowSpy, view: viewSpy, interactor: interactorSpy)

        viewSpy.accessAction?()

        XCTAssertEqual(interactorSpy.didTapAccessCallCount, 1)
    }

    func testLogoutAction_ShouldCallInteractorLogoutAction() {
        sut = AppAccessController(window: windowSpy, view: viewSpy, interactor: interactorSpy)

        viewSpy.logoutAction?()

        XCTAssertEqual(interactorSpy.didTapLogoutCallCount, 1)
    }
}
