@testable import SecurityModule
import XCTest

private class AppAccessDisplayingSpy: AppAccessDisplaying {
    private(set) var showAccessProtectionViewCallCount = 0
    private(set) var removeAccessProtectionViewCallCount = 0
    private(set) var updateLogoutButtonTitleCallCount = 0
    private(set) var showAccessButtonsCallCount = 0
    private(set) var displaySecurityErrorPopupCallCount = 0
    private(set) var duration: TimeInterval = 0

    func showAccessProtectionView() {
        showAccessProtectionViewCallCount += 1
    }

    func removeAccessProtectionView(duration: TimeInterval, completion: (() -> Void)?) {
        removeAccessProtectionViewCallCount += 1
        self.duration = duration
    }

    func updateLogoutButtonTitle(_ title: String) {
        updateLogoutButtonTitleCallCount += 1
    }

    func showAccessButtons(_ show: Bool) {
        showAccessButtonsCallCount += 1
    }

    func displaySecurityErrorPopup(with title: String, message: String, buttonTitle: String) {
        displaySecurityErrorPopupCallCount += 1
    }
}

final class AppAccessPresenterTests: XCTestCase {
    private let controllerSpy = AppAccessDisplayingSpy()

    private lazy var sut: AppAccessPresenter = {
        let presenter = AppAccessPresenter()
        presenter.controller = controllerSpy
        return presenter
    }()

    func testShowAccessProtectionView_ShouldCallControllerShowAccessProtectionView() {
        sut.showAccessProtectionView()

        XCTAssertEqual(controllerSpy.showAccessProtectionViewCallCount, 1)
    }

    func testRemoveAccessProtectionView_ShouldCallControllerRemoveAccessProtectionView() {
        sut.removeAccessProtectionView()

        XCTAssertEqual(controllerSpy.removeAccessProtectionViewCallCount, 1)
        XCTAssertEqual(controllerSpy.duration, 0.3)
    }

    func testUpdateUsername_ShouldCallControllerUpdateLogoutButtonTitle() {
        sut.updateUsername("")

        XCTAssertEqual(controllerSpy.updateLogoutButtonTitleCallCount, 1)
    }

    func testShowAccessButtons_ShouldCallControllerShowAccessButtons() {
        sut.showAccessButtons(true)

        XCTAssertEqual(controllerSpy.showAccessButtonsCallCount, 1)
    }

    func testPresentSecurityErrorPopup_ShouldCallControllerDisplaySecurityErrorPopup() {
        sut.presentSecurityErrorPopup()

        XCTAssertEqual(controllerSpy.displaySecurityErrorPopupCallCount, 1)
    }
}
