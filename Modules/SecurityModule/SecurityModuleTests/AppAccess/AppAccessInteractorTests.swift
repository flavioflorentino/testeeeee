import AnalyticsModule
@testable import SecurityModule
import LocalAuthentication
import XCTest

private class AppAccessServicingMock: AppAccessServicing {
    private(set) var biometricTypeCallCount = 0
    private(set) var evaluateBiometryCallCount = 0
    private(set) var reason: String?

    var deviceType: DeviceBiometricType = .none
    var evaluationAuthorized = true
    var evaluationError: LAError?

    var biometricType: DeviceBiometricType {
        biometricTypeCallCount += 1
        return deviceType
    }

    func evaluateBiometry(reason: String, completion: @escaping (Bool, LAError?) -> Void) {
        evaluateBiometryCallCount += 1
        self.reason = reason
        completion(evaluationAuthorized, evaluationError)
    }
}

private class AppAccessPresentingSpy: AppAccessPresenting {
    weak var controller: AppAccessDisplaying?
    private(set) var showAccessProtectionViewCallCount = 0
    private(set) var removeAccessProtectionViewCallCount = 0
    private(set) var updateUsernameCallCount = 0
    private(set) var showAccessButtonsCallCount = 0
    private(set) var presentSecurityErrorPopupCallCount = 0
    private(set) var showAccessButtons = false

    func showAccessProtectionView() {
        showAccessProtectionViewCallCount += 1
    }

    func removeAccessProtectionView() {
        removeAccessProtectionViewCallCount += 1
    }

    func updateUsername(_ username: String) {
        updateUsernameCallCount += 1
    }

    func showAccessButtons(_ show: Bool) {
        showAccessButtonsCallCount += 1
        showAccessButtons = show
    }

    func presentSecurityErrorPopup() {
        presentSecurityErrorPopupCallCount += 1
    }
}

final class AppAccessInteractorTests: XCTestCase {
    private let serviceMock = AppAccessServicingMock()
    private let presenterSpy = AppAccessPresentingSpy()
    private let analyticsSpy = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(analyticsSpy)
    private var didCallLogoutAction = false

    private lazy var sut = AppAccessInteractor(logoutAction: logoutAction,
                                               service: serviceMock,
                                               presenter: presenterSpy,
                                               dependencies: dependencies)

    func testShouldChangeState_WhenFlagIsFalse_ShouldNotShowView() {
        sut.shouldChangeState(for: .didFinishLaunching, with: properties(isFlagOn: false))

        XCTAssertEqual(presenterSpy.showAccessProtectionViewCallCount, 0)
        XCTAssertEqual(sut.currentState, .notVisible)
    }

    func testShouldChangeState_WhenIsLoggedInIsFalse_ShouldNotShowView() {
        sut.shouldChangeState(for: .didFinishLaunching, with: properties(isLoggedIn: false))

        XCTAssertEqual(presenterSpy.showAccessProtectionViewCallCount, 0)
        XCTAssertEqual(sut.currentState, .notVisible)
    }

    func testShouldChangeState_WhenIsProtectionActiveIsFalse_ShouldNotShowView() {
        sut.shouldChangeState(for: .didFinishLaunching, with: properties(isProtectionActive: false))

        XCTAssertEqual(presenterSpy.showAccessProtectionViewCallCount, 0)
        XCTAssertEqual(sut.currentState, .notVisible)
    }

    func testShouldChangeState_WhenAppDidFinishLaunching_ShouldShowView() {
        sut.shouldChangeState(for: .didFinishLaunching, with: properties())

        XCTAssertEqual(presenterSpy.showAccessProtectionViewCallCount, 1)
        XCTAssertEqual(presenterSpy.showAccessButtonsCallCount, 1)
        XCTAssertFalse(presenterSpy.showAccessButtons)
        XCTAssertEqual(sut.currentState, .visible)
    }

    func testShouldChangeState_WhenAppWillResignActive_ShouldShowView() {
        sut.shouldChangeState(for: .willResignActive, with: properties())

        XCTAssertEqual(presenterSpy.showAccessProtectionViewCallCount, 1)
        XCTAssertEqual(presenterSpy.showAccessButtonsCallCount, 1)
        XCTAssertFalse(presenterSpy.showAccessButtons)
        XCTAssertEqual(sut.currentState, .visible)
    }

    func testShouldChangeState_WhenProtectionIsVisibleAndAppDidBecomeActiveFromLaunch_ShouldTrackEvent() {
        sut.shouldChangeState(for: .didFinishLaunching, with: properties())

        sut.shouldChangeState(for: .didBecomeActive, with: properties())

        let eventProperties = [AppProtectionEventProperty(name: .dialogName, value: .protectionRequested)]
        let expectedEvent = SecurityModuleTracker(eventType: .dialogViewed,
                                                  eventProperties: eventProperties).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }

    func testShouldChangeState_WhenProtectionIsVisibleAndAppDidBecomeActiveFromBackgroundAndBiometryIsAuthorized_ShouldHideView() {
        sut.shouldChangeState(for: .willResignActive, with: properties())
        sut.shouldChangeState(for: .didEnterBackground, with: properties())

        sut.shouldChangeState(for: .didBecomeActive, with: properties())

        XCTAssertEqual(presenterSpy.updateUsernameCallCount, 1)
        XCTAssertEqual(presenterSpy.showAccessButtonsCallCount, 2)
        XCTAssertFalse(presenterSpy.showAccessButtons)
        XCTAssertEqual(presenterSpy.removeAccessProtectionViewCallCount, 1)
    }

    func testShouldChangeState_WhenProtectionIsVisibleAndAppDidBecomeActiveFromBackgroundAndBiometryIsUnauthorized_ShouldTrackEvent() {
        sut.shouldChangeState(for: .willResignActive, with: properties())
        sut.shouldChangeState(for: .didEnterBackground, with: properties())
        serviceMock.evaluationAuthorized = false

        sut.shouldChangeState(for: .didBecomeActive, with: properties())

        let eventProperties = [AppProtectionEventProperty(name: .screenName, value: .fallback)]
        let expectedEvent = SecurityModuleTracker(eventType: .screenViewed,
                                                  eventProperties: eventProperties).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }

    func testShouldChangeState_WhenProtectionIsVisibleAndAppDidBecomeActiveFromBackgroundAndBiometryIsUnauthorized_ShouldShowFallbackButtons() {
        sut.shouldChangeState(for: .willResignActive, with: properties())
        sut.shouldChangeState(for: .didEnterBackground, with: properties())
        serviceMock.evaluationAuthorized = false

        sut.shouldChangeState(for: .didBecomeActive, with: properties())

        XCTAssertEqual(presenterSpy.showAccessButtonsCallCount, 3)
        XCTAssertTrue(presenterSpy.showAccessButtons)
        XCTAssertEqual(presenterSpy.removeAccessProtectionViewCallCount, 0)
    }

    func testShouldChangeState_WhenProtectionIsVisibleAndAppDidBecomeActiveFromBackgroundAndBiometryIsUnauthorizedAndErrorIsPasscodeNotSet_ShouldShowErrorPopup() {
        sut.shouldChangeState(for: .willResignActive, with: properties())
        sut.shouldChangeState(for: .didEnterBackground, with: properties())
        serviceMock.evaluationAuthorized = false
        serviceMock.evaluationError = LAError(.passcodeNotSet)

        sut.shouldChangeState(for: .didBecomeActive, with: properties())

        XCTAssertEqual(presenterSpy.presentSecurityErrorPopupCallCount, 1)
    }

    func testShouldChangeState_WhenProtectionIsVisibleAndAppDidBecomeActive_ShouldHideView() {
        sut.shouldChangeState(for: .willResignActive, with: properties())

        sut.shouldChangeState(for: .didBecomeActive, with: properties())

        XCTAssertEqual(presenterSpy.removeAccessProtectionViewCallCount, 1)
    }

    func testAccessViewRemovedFromWindow_ShouldChangeCurrentStateToNotVisible() {
        sut.shouldChangeState(for: .willResignActive, with: properties())
        XCTAssertEqual(sut.currentState, .visible)

        sut.accessViewRemovedFromWindow()

        XCTAssertEqual(sut.currentState, .notVisible)
    }

    func testDidTapAccess_ShouldTrackEvent() {
        sut.didTapAccess()

        let eventProperties = [
            AppProtectionEventProperty(name: .screenName, value: .fallback),
            AppProtectionEventProperty(name: .buttonName, value: .auth)
        ]
        let expectedEvent = SecurityModuleTracker(eventType: .buttonClicked,
                                                  eventProperties: eventProperties).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }

    func testDidTapAccess_WhenBiometricTypeIsFaceID_ShouldHaveFaceIDReasonString() {
        serviceMock.deviceType = .faceID

        sut.didTapAccess()

        XCTAssertEqual(serviceMock.evaluateBiometryCallCount, 1)
        XCTAssertEqual(serviceMock.reason, Strings.AppProtection.FaceId.reason)
    }

    func testDidTapAccess_WhenBiometricTypeIsTouchID_ShouldHaveTouchIDReasonString() {
        serviceMock.deviceType = .touchID

        sut.didTapAccess()

        XCTAssertEqual(serviceMock.evaluateBiometryCallCount, 1)
        XCTAssertEqual(serviceMock.reason, Strings.AppProtection.TouchId.reason)
    }

    func testDidTapAccess_WhenBiometricTypeIsNone_ShouldHaveTouchIDReasonString() {
        sut.didTapAccess()

        XCTAssertEqual(serviceMock.evaluateBiometryCallCount, 1)
        XCTAssertEqual(serviceMock.reason, Strings.AppProtection.TouchId.reason)
    }

    func testDidTapLogout_ShouldTrackEvent() {
        sut.didTapLogout()

        let eventProperties = [
            AppProtectionEventProperty(name: .screenName, value: .fallback),
            AppProtectionEventProperty(name: .buttonName, value: .logout)
        ]
        let expectedEvent = SecurityModuleTracker(eventType: .buttonClicked,
                                                  eventProperties: eventProperties).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }

    func testDidTapLogout_ShouldCallLogoutAction() {
        sut.didTapLogout()

        XCTAssertTrue(didCallLogoutAction)
        XCTAssertEqual(sut.currentState, .notVisible)
    }

    private func logoutAction() {
        didCallLogoutAction = true
    }

    private func properties(isFlagOn: Bool = true,
                            isLoggedIn: Bool = true,
                            isProtectionActive: Bool = true,
                            username: String = "") -> AppAccessProperties {
        AppAccessProperties(isFlagOn: isFlagOn,
                            isLoggedIn: isLoggedIn,
                            isProtectionActive: isProtectionActive,
                            username: username)
    }
}
