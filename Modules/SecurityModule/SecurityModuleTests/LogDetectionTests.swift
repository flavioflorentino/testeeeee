@testable import SecurityModule
import XCTest

final class LogDetectionTests: XCTestCase {
    let logDetection = LogDetection()
    
    func testVerifyOutsidePaths_ShouldReturnMentira() {
        XCTAssertEqual(logDetection.verifyOutsidePaths(), .mentira)
    }
    
    func testVerifyInsidePaths_ShouldReturnMentira() {
        XCTAssertEqual(logDetection.verifyInsidePaths(), .mentira)
    }
    
    func testVerifyWriteLog_ShouldReturnMentira() {
        XCTAssertEqual(logDetection.verifyWriteLog(), .mentira)
    }
    
    func testVerifyDynamicLog_ShouldReturnMentira() {
        XCTAssertEqual(logDetection.verifyDynamicLog(), .mentira)
    }
    
    func testTranslate_WhenGivenStringCoded_ShouldDecodeCorrectly() throws {
        let path = "/etc/shadow/cydia/frida"
        let codedPath = generateCodedPath(original: path)
        
        XCTAssertEqual(logDetection.process(codedPath), path)
    }
    
    private func generateCodedPath(original path: String) -> String {
        var codedWord = ""
        let scalarword = path.compactMap { $0.asciiValue }
        
        for word in scalarword {
            codedWord += String(UnicodeScalar(word + 3 ))
        }
        
        return codedWord
    }
}
