import Foundation
import XCTest
import CryptoSwift
@testable import SecurityModule

class GenerateKeyTests: XCTestCase {
   
    func testGenerateKey_WhenPassValidData_ShouldMatchWithTheExpectedStringScenario1() throws {
        let generateKey = GenerateKey()
        let password = "1234567890123456"
        let salt = "1234"
        let result = try generateKey.getSecretKey(password: password, salt: salt).get()
        let expected = "H5nP2Yt93Ae4ehx0HukDXfM8vBD3CJ9/TZJzUwFNmZQ="
        XCTAssertEqual(result.toBase64(), expected)
    }
    
    func testGenerateKey_WhenPassValidData_ShouldMatchWithTheExpectedStringScenario2() throws {
        let generateKey = GenerateKey()
        let password = "5a88eb2f1b5c4252ac8866bea11654cf"
        let salt = "09978878"
        let result = try generateKey.getSecretKey(password: password, salt: salt).get()
        let expected = "UMp5MbcU5JP7SA2REragKpk2cAQMp1vxoOuxFzpA3iI="
        XCTAssertEqual(result.toBase64(), expected)
    }
    
    func testGenerateKey_WhenPassEmptySalt_ShouldReciveErrorGenereteKey() throws {
        let generateKey = GenerateKey()
        let password = "12345"
        let salt = ""
        XCTAssertEqual(generateKey.getSecretKey(password: password, salt: salt),.failure(.generateKey))
    }
    
}

