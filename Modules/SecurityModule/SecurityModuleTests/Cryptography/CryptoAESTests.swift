import Foundation
import XCTest
import CryptoSwift
@testable import SecurityModule

class CryptoAESTests: XCTestCase {
   
    func testEncryption_WhenPassValidKeyInBase64AndIVBase64_ShouldReturnAValueEncrypted() throws {
        let key = "H5nP2Yt93Ae4ehx0HukDXfM8vBD3CJ9/TZJzUwFNmZQ="
        let plainText = "123"
        let iv = "MTIzNDU2Nzg5MDAwMDAwMA=="
        let encryptionValue = try CryptoAES(keyInbase64: key, ivInBase64: iv).encryption(plainText: plainText).get()
        let expected = "w553hTf32AQ37jlsoIXbig=="
        XCTAssertEqual(encryptionValue, expected)
    }
    
    func testEncryption_WhenPassValidKeyInBase64_ShouldReturnAValueEncrypted() throws {
        let key = "H5nP2Yt93Ae4ehx0HukDXfM8vBD3CJ9/TZJzUwFNmZQ="
        let plainText = "123"
        let iv = "1234567890000000"
        let keyDecodetFrom64 = try XCTUnwrap(key.decodedFrom64())
        let encryptionValue = try CryptoAES(iv: iv.bytes, key: keyDecodetFrom64).encryption(plainText: plainText).get()
        let expected = "w553hTf32AQ37jlsoIXbig=="
        XCTAssertEqual(encryptionValue, expected)
    }
    
    func testEncryption_WhenPassAInvalidSizeIV_ShouldReciveErrorDecodedFrom64() {
        let sut = CryptoAES(iv: "123456789A".bytes, /// invalid iv, a Valid IV must be lenght equal 16
                            key: "picpay1233333333333333333333333".bytes)
        let result = sut.encryption(plainText: "123")
        XCTAssertEqual(result, .failure(.encryptionError(.invalidKeySize)))
    }
    
    func testEncryption_WhenPassAInvalidSizekey_ShouldReciveErrorEncryptionError() {
        let sut = CryptoAES(iv: "1234567890000000".bytes,
                            key: "picpay1233333".bytes)  /// invalid key a Valid kemust be lenght equal 32
        XCTAssertEqual(sut.encryption(plainText: "teste"), .failure(.encryptionError(.invalidKeySize)))
    }
    
    func testDecrypt_WhenPassStringThatsNotInbase64_ShouldReciveErrorDecodedFrom64() {
        let sut = CryptoAES(iv: "123456789ABCDEF".bytes, key: "picpay1233333333333333333333333".bytes)
        XCTAssertEqual(sut.decrypt(encryptValue: "teste"), .failure(.decodedFrom64))
    }
    
    func testDecrypt_WhenPassValidKeyInBase64_ShouldReturnAValueDecrypted() throws {
        let key = "H5nP2Yt93Ae4ehx0HukDXfM8vBD3CJ9/TZJzUwFNmZQ="
        let iv = "1234567890000000"
        let encryptedData = "w553hTf32AQ37jlsoIXbig=="
        let keyDecodetFrom64 = try XCTUnwrap(key.decodedFrom64())
        let decryptValue = try CryptoAES(iv: iv.bytes, key: keyDecodetFrom64).decrypt(encryptValue: encryptedData).get()
        let expected = "123"
        XCTAssertEqual(decryptValue, expected)
    }
    
    func testDecrypt_WhenPassAnInvalidSizeIV_ShouldReturnDecryptError() throws {
        let key = "H5nP2Yt93Ae4ehx0HukDXfM8vBD3CJ9/TZJzUwFNmZQ="
        let iv = "1234567890" ///invalid iv, a Valid IV must be lenght equal 16
        let encryptedData = "w553hTf32AQ37jlsoIXbig=="
        let keyDecodetFrom64 = try XCTUnwrap(key.decodedFrom64())
        let decryptValue = CryptoAES(iv: iv.bytes, key: keyDecodetFrom64).decrypt(encryptValue: encryptedData)
        XCTAssertEqual(decryptValue, .failure(.decryptError(nil)))
    }
    
    func testDecrypt_WhenPassAnWrongKey_ShouldReturnDecryptError() throws {
        let key = "H5nP2Yt93Ae4ehx0HukDXfM8vBD3CJ9/TZJzUwFNm123" /// Wrong key
        let iv = "1234567890000000"
        let encryptedData = "w553hTf32AQ37jlsoIXbig=="
        let keyDecodetFrom64 = try XCTUnwrap(key.decodedFrom64())
        let decryptValue = CryptoAES(iv: iv.bytes, key: keyDecodetFrom64).decrypt(encryptValue: encryptedData)
        XCTAssertEqual(decryptValue, .failure(.decryptError(.invalidKeySize)))
    }
    
    func testDecrypt_WhenPassAValidDataEncrypted_ShouldReturnAStrigObject() throws {
        let key = "dglgyxfdxgfaislzsfuzxoumazhmshzw"
        let iv = "vazutetezygpgwno"
        let encryptedData = "F5E4zOOWrlWKxxFc4qrQzdWWBXTPqNgHsneYk3Obl7JsgT1kMx7XIGEVTCITMeKp88sn89XMk8dlMrTecUMrhC+1AOI6gKOgYNdPn8u13EYGFrJukbAeV5tZr8fJcdN7UEJ2yTpdRmb+/bctFe77pw=="
        let decryptValue = try CryptoAES(iv: iv.bytes, key: key.bytes).decrypt(encryptValue: encryptedData).get()
        let planText = ###"{"name":"RENAN MACHADO","cvv":"616","expiration_date":"2028-10-08","card_number":"2227630000101110"}"###
        
        XCTAssertEqual(decryptValue, planText)
    }
}

