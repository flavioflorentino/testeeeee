import LocalAuthentication
@testable import SecurityModule
import XCTest

final private class FaceIdLAContextMock: LAContext {
    private(set) var reasonDescriptionUsed = ""
    
    override func canEvaluatePolicy(_ policy: LAPolicy, error: NSErrorPointer) -> Bool {
        return true
    }
    
    override func evaluatePolicy(_ policy: LAPolicy, localizedReason: String, reply: @escaping (Bool, Error?) -> Void) {
        reasonDescriptionUsed = localizedReason
        reply(true, nil)
    }
    
    override var biometryType: LABiometryType {
        .faceID
    }
}

final private class TouchIdLAContextMock: LAContext {
    override func canEvaluatePolicy(_ policy: LAPolicy, error: NSErrorPointer) -> Bool {
        return true
    }
    
    override func evaluatePolicy(_ policy: LAPolicy, localizedReason: String, reply: @escaping (Bool, Error?) -> Void) {
        reply(true, nil)
    }
    
    override var biometryType: LABiometryType {
        .touchID
    }
}

final private class NoBiometryLAContextMock: LAContext {
    override func canEvaluatePolicy(_ policy: LAPolicy, error: NSErrorPointer) -> Bool {
        return true
    }
    
    override var biometryType: LABiometryType {
        .none
    }
}

final private class NoPolicyEvaluationLAContextMock: LAContext {
    override func canEvaluatePolicy(_ policy: LAPolicy, error: NSErrorPointer) -> Bool {
        return false
    }
}

final class DeviceSecurityTests: XCTestCase {
    
    func testBiometricType_WhenFaceIdIsEnabled_ShouldReturnFaceIdType() {
        let faceIdContext = FaceIdLAContextMock()
        let type = DeviceSecurity.biometricType(for: faceIdContext)
        
        XCTAssertEqual(type, .faceID)
    }
    
    func testBiometricType_WhenTouchIdIsEnabled_ShouldReturnTouchIdType() {
        let touchIdContext = TouchIdLAContextMock()
        let type = DeviceSecurity.biometricType(for: touchIdContext)
        
        XCTAssertEqual(type, .touchID)
    }
    
    func testBiometricType_WhenNoBiometryIsPresent_ShouldReturnNone() {
        let noBiometryContext = NoBiometryLAContextMock()
        let type = DeviceSecurity.biometricType(for: noBiometryContext)
        
        XCTAssertEqual(type, .none)
    }
    
    func testBiometricType_WhenPolicyEvalutionIsNotPossible_ShouldReturnNone() {
        let noPolicyContext = NoPolicyEvaluationLAContextMock()
        let type = DeviceSecurity.biometricType(for: noPolicyContext)
        
        XCTAssertEqual(type, .none)
    }
    
    func testcanEvaluate_WhenEvaluationIsNotPossible_ShouldReturnFalse() {
        let noPolicyContext = NoPolicyEvaluationLAContextMock()

        XCTAssertFalse(DeviceSecurity.canEvaluate(for: noPolicyContext))
    }

    func testEvaluateBiometry_WhenEvaluationIsPossibleAndBiometryIsValidated_ShouldReturnTrue() {
        let faceIdContext = FaceIdLAContextMock()

        DeviceSecurity.evaluateBiometry(
            reasonDescription: Strings.AppProtection.FaceId.reason,
            context: faceIdContext
        ) { authorized, _ in
            XCTAssertTrue(authorized)
            XCTAssertEqual(faceIdContext.reasonDescriptionUsed, Strings.AppProtection.FaceId.reason)
        }
    }
}
