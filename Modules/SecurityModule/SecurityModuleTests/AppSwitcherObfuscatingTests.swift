import UI
@testable import SecurityModule
import XCTest

protocol ObfuscableFake: UIViewController {
    var appSwitcherObfuscator: AppSwitcherObfuscating? { get set }
    func deinitObfuscator()
}

extension ObfuscableFake where Self: UIViewController {
    func setupObfuscationConfiguration(window: UIWindow? = UIApplication.shared.keyWindow) {
        guard let window = window else {
            return
        }
        appSwitcherObfuscator = AppSwitcherObfuscator(window: window)
        appSwitcherObfuscator?.setupObfuscationView(image: Assets.greenFilledCheckmark.image, appType: .personal)
    }
    
    func deinitObfuscator() {
        // Make sure to not show obfuscator view in screens that don't implement Obfuscable
        appSwitcherObfuscator = nil
    }
}

private final class AppSwitcherObfuscationViewControllerMock: UIViewController, ObfuscableFake {
    var appSwitcherObfuscator: AppSwitcherObfuscating?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupObfuscationConfiguration()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        deinitObfuscator()
    }
}

final class AppSwitcherObfuscatingTests: XCTestCase {
    private let sut = AppSwitcherObfuscationViewControllerMock()
    private let window = UIApplication.shared.keyWindow
    private func loadViewController(sut: UIViewController) {
        window?.rootViewController = sut
        UIView.setAnimationsEnabled(false)
    }
    
    // Tests with feature flag ON
    func testAddObfuscationView_WhenFeatureFlagIsTrue_ShouldAddViewInFront() {
        loadViewController(sut: sut)
        
        expectation(forNotification: UIApplication.willResignActiveNotification, object: nil, handler: nil)
        NotificationCenter.default.post(name: UIApplication.willResignActiveNotification, object: nil)
        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertNotNil(window?.subviews.first(where: { $0 is ObfuscationView }))
    }
    
    func testRemoveObfuscationView_WhenFeatureFlagIsTrue_ShouldRemoveViewInFront() {
        loadViewController(sut: sut)
        
        expectation(forNotification: UIApplication.willResignActiveNotification, object: nil, handler: nil)
        NotificationCenter.default.post(name: UIApplication.willResignActiveNotification, object: nil)
        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertNotNil(window?.subviews.first(where: { $0 is ObfuscationView }))
        
        let expectationRemoveView = XCTestExpectation(description: "test remove obfuscationView flag true - success")
        expectation(forNotification: UIApplication.didBecomeActiveNotification, object: nil, handler: nil)
        NotificationCenter.default.post(name: UIApplication.didBecomeActiveNotification, object: nil)
        waitForExpectations(timeout: 0.5, handler: nil)
        
        // used to wait for the process of removing the view
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            XCTAssertNil(self.window?.subviews.first(where: { $0 is ObfuscationView }))
            expectationRemoveView.fulfill()
        }
        wait(for: [expectationRemoveView], timeout: 3.0)
    }
}
