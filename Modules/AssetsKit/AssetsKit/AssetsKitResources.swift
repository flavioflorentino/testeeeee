import Foundation

// swiftlint:disable convenience_type
final class AssetsKitResources {    
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: AssetsKitResources.self).url(forResource: "AssetsKitResources", withExtension: "bundle") else {
            return Bundle(for: AssetsKitResources.self)
        }
        return Bundle(url: url) ?? Bundle(for: AssetsKitResources.self)
    }()
}
