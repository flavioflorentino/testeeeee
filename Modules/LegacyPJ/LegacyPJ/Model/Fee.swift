public struct Fee {
    public let firstDay: String?
    public let lastDay: String?
    public let defaultDay: String?
    public let items: [String: PercentageFee]
    public let daysToReleaseCc: Int?
    public let zeroFee: Bool
}
