import Foundation

public enum FacialBiometricsStatus: String, Decodable {
    case required
    case accepted
    case retry
    case denied
    case pending
    case statusError
    case uploadError
}
