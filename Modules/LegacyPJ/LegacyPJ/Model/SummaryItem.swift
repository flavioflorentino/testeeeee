import Foundation
import SwiftyJSON

public struct SummaryItem {
    public let id: Int
    public let date: String
    public let dateId: String
    public let totalReceived: String
    
    init?(json: JSON) {
        guard let date = json["date"].string,
            let totalReceived = json["total_received"].string,
            let dateId = json["date_id"].string
            else {
                return nil
        }
        
        let id = json["id"].int
        self.id = id ?? 0
        
        self.date = date
        self.totalReceived = totalReceived
        self.dateId = dateId
    }
}
