import Foundation
import SwiftyJSON

public struct Operator: Equatable {
    public var id: Int?
    public var username: String?
    public var password: String?
    public var imageUrl: String?
    
    public init() {}
    
    public init(user: User) {
        self.id = user.id
        self.username = user.username
        self.password = user.password
        self.imageUrl = user.imageUrl
    }
    
    public init?(json: JSON) {
        if let id = json["id"].int {
            self.id = id
        }
        
        if let username = json["username"].string {
            self.username = username
        }
        
        if let password = json["password"].string {
            self.password = password
        }
        
        if let image = json["img_url"].string {
            self.imageUrl = image
        }
    }
}
