import Foundation
import SwiftyJSON

public struct Pagination {
    public let hasNext: Bool
    public let nextPage: Int
    
    public init() {
        hasNext = false
        nextPage = 0
    }
    
    public init(json: JSON) {
        if let next = json["next"].bool {
            self.hasNext = next
        } else {
            self.hasNext = false
        }
        if let nextPage = json["next_page"].int {
            self.nextPage = nextPage
        } else {
            self.nextPage = 0
        }
    }
}
