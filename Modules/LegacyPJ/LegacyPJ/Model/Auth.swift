import Foundation
import SwiftyJSON

public struct Auth {
    public let accessToken: String
    public let tokenType: String
    public let expires: Date
    public let refreshToken: String
    public var biometry: FacialBiometricsStatus
    public let completed: Bool
    
    public init?(json: JSON) {
        guard
            let accessToken = json["access_token"].string,
            let tokenType = json["token_type"].string,
            let expires = json["expires_in"].int,
            let refreshToken = json["refresh_token"].string
        else {
            return nil
        }
        
        self.accessToken = accessToken
        self.tokenType = tokenType
        self.refreshToken = refreshToken
        self.expires = Date(timeIntervalSince1970: TimeInterval(expires))
        if let biometry = json["biometry"].string, let status = FacialBiometricsStatus(rawValue: biometry) {
            self.biometry = status
        } else {
            self.biometry = .accepted
        }
        self.completed = json["completed"].bool ?? true
    }
}

public extension Auth {
    init(
        accessToken: String,
        tokenType: String,
        expires: Date,
        refreshToken: String,
        biometry: FacialBiometricsStatus?,
        completed: Bool
    ) {
        self.accessToken = accessToken
        self.tokenType = tokenType
        self.expires = expires
        self.refreshToken = refreshToken
        self.biometry = biometry ?? .accepted
        self.completed = completed
    }
    
    func toJSON() -> JSON {
        let json: JSON = [
            "access_token": self.accessToken,
            "token_type": self.tokenType,
            "expires_in": self.expires.timeIntervalSince1970,
            "refresh_token": self.refreshToken,
            "biometry": self.biometry.rawValue,
            "completed": self.completed
        ]
        return json
    }
    
    func toJSONString() -> String? {
        toJSON().rawString()
    }
}
