public struct Partner {
    public var name: String = ""
    public var email: String = ""
    public var cpf: String = ""
    public var birthday: String = ""
    public var motherName: String = ""
    
    public var address = PartnerAddress()
    
    public init() { }
}

public extension Partner {
    func toDictionary() -> [String: Any] {
        let dictionary: [String: Any] = [
            "name": name,
            "mail": email,
            "cpf": cpf,
            "birthday": birthday,
            "motherName": motherName
        ]
        
        return dictionary
    }
}

public struct PartnerAddress {
    public var cep: String = ""
    public var street: String = ""
    public var addressNumber: String = ""
    public var complement: String = ""
    public var district: String = ""
    public var city: String = ""
    public var state: String = ""
}
