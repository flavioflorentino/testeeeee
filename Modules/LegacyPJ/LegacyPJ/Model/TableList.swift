import Foundation
import SwiftyJSON

public final class TableList<DataType> {
    public var sections: [TableListSectionData<DataType>]
    public var pagination: Pagination
    public var isLoading: Bool
    
    public init() {
        sections = [TableListSectionData<DataType>()]
        pagination = Pagination()
        isLoading = false
    }
    
    public func populate(section: Int, list: [DataType]) {
        sections[section].rows.removeAll()
        for data in list {
            sections[section].rows.append(TableListRowData<DataType>(data: data))
        }
    }
    
    public func append(section: Int, list: [DataType]) {
        for data in list {
            sections[section].rows.append(TableListRowData<DataType>(data: data))
        }
    }
    
    public subscript(_ input: IndexPath) -> DataType? {
        sections[input.section].rows[input.row].data
    }
}

public final class TableListRowData<DataType> {
    public var data: DataType
    public var isLoading: Bool
    
    public init(data: DataType) {
        self.data = data
        self.isLoading = false
    }
}

public final class TableListSectionData<DataType> {
    public var rows: [TableListRowData<DataType>]
    
    public init() {
        rows = []
    }
}

public final class TableHeader {
    public enum HeaderType: String {
        case message
        case unkwoned
    }
    
    public var headerType: HeaderType
    public var text: String
    
    public init?(dict: JSON) {
        guard let typeString = dict["type"].string else {
            return nil
        }
        
        self.headerType = .unkwoned
        if let type = HeaderType(rawValue: typeString) {
            self.headerType = type
        }
        
        text = dict["text"].string ?? ""
    }
}
