import Foundation
import SwiftyJSON

public struct User {
    public var id: Int?
    public var name: String?
    public var username: String?
    public var isAdmin: Bool = false
    public var phone: String?
    public var email: String?
    public var imageUrl: String?
    public var cpf: String?
    public var birthDate: String?
    public var password: String?
    
    public init() {}
    
    public init?(json: JSON) {
        id = json["id"].int
        name = json["name"].string
        email = json["email"].string
        username = json["username"].string
        
        isAdmin = json["super"].boolValue || json["role"].boolValue
        
        phone = json["mobile_phone"].string
        imageUrl = json["img_url"].string
        cpf = json["cpf"].string
        
        if let birthDate = json["birth_date_formated"].string {
            self.birthDate = birthDate
        } else if let birthDate = json["birth_date"].string {
            self.birthDate = birthDate
        }
    }

    public init(
        id: Int? = nil,
        name: String? = nil,
        username: String? = nil,
        isAdmin: Bool = false,
        phone: String? = nil,
        email: String? = nil,
        imageUrl: String? = nil,
        cpf: String? = nil,
        birthDate: String? = nil,
        password: String? = nil
    ) {
        self.id = id
        self.name = name
        self.username = username
        self.isAdmin = isAdmin
        self.phone = phone
        self.email = email
        self.imageUrl = imageUrl
        self.cpf = cpf
        self.birthDate = birthDate
        self.password = password
    }
    
    public func toJSON() -> JSON {
        JSON(toDictionary())
    }
    
    public func toJSONString() -> String? {
        toJSON().rawString()
    }
    
    public func toDictionary() -> [String: Any] {
        var p: [String: Any] = [:]
        
        if let id = self.id {
            p["id"] = id
        }
        if let name = self.name {
            p["name"] = name
        }
        if let username = self.username {
            p["username"] = username
        }
        if let email = self.email {
            p["email"] = email
        }
        
        p["role"] = isAdmin ? 1 : 0
        
        if let phone = self.phone {
            p["phone"] = phone
        }
        if let imageUrl = self.imageUrl {
            p["img_url"] = imageUrl
        }
        if let cpf = self.cpf {
            p["cpf"] = cpf.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        }
        if let birthDate = self.birthDate {
            p["birth_date"] = birthDate
        }
        if let password = self.password {
            p["password"] = password
        }
        
        return p
    }
}
