import Foundation
import SwiftyJSON

public struct HistoryItem {
    public var id: Int
    public var date: String
    public var time: String
    public var totalReceived: String
    public var totalReceipts: Int?
    public var operatorr: Operator?
    
    public init?(json: JSON) {
        guard
            let id = json["id"].int,
            let dateTime = json["date"].string,
            let totalReceived = json["total_received"].string
            else {
                return nil
        }
        
        self.id = id
        if let date = dateTime[0..<11] {
            self.date = date
        } else {
            self.date = ""
        }
        if let time = dateTime[11..<16] {
            self.time = time
        } else {
            self.time = ""
        }
        self.totalReceived = totalReceived
        
        totalReceipts = json["num_receipts"].int
        operatorr = Operator(json: json["operator"])
    }
}
