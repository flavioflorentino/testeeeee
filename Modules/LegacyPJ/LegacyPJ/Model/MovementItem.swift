import Foundation
import SwiftyJSON

public struct MovementItem {
    public let label: String
    public var list: [MovementItemRow]
    
    public init?(json: JSON) {
        guard let label = json["label"].string else {
            return nil
        }
            
        var items: [MovementItemRow] = []
        if let list = json["list"].array {
            for i in list {
                if let detail = MovementItemRow(json: i) {
                    items.append(detail)
                }
            }
        }
        
        self.list = items
        self.label = label
    }
}

public final class MovementItemRow {
    public let label: String
    public let details: [MovementDetail]
    public var expanded: Bool = false
    public let balanceValue: String
    public let balanceLabel: String
    
    public init?(json: JSON) {
        guard
            let label = json["label"].string,
            let balance = json["balance"]["value"].string,
            let balanceLabel = json["balance"]["label"].string
            else {
                return nil
        }
        var details: [MovementDetail] = []
        if let list = json["list"].array {
            for i in list {
                if let detail = MovementDetail(json: i) {
                    details.append(detail)
                }
            }
        }
        
        self.details = details
        self.label = label
        self.balanceValue = balance
        self.balanceLabel = balanceLabel
    }
}

public struct MovementDetail {
    public let name: String
    public let value: String
    
    public init?(json: JSON) {
        guard
            let name = json["name"].string,
            let value = json["value"].string
            else {
                return nil
        }
        
        self.name = name
        self.value = "\(value)"
    }
}
