import Foundation
import SwiftyJSON

public struct UserAuth: Equatable {
    public var auth: Auth
    public var user: User
    
    public init(auth: Auth, user: User) {
        self.auth = auth
        self.user = user
    }
    
    public init?(json: JSON) {
        guard
            let auth = Auth(json: json["auth"]),
            let user = User(json: json["user"])
        else {
            return nil
        }
        
        self.user = user
        self.auth = auth
    }
}

public extension UserAuth {
    static func == (lhs: UserAuth, rhs: UserAuth) -> Bool {
        lhs.user.id == rhs.user.id
    }
    
    func toJSON() -> JSON {
        let json: JSON = [
            "user": user.toJSON(),
            "auth": auth.toJSON()
        ]
        return json
    }
    
    func toJSONString() -> String? {
        toJSON().rawString()
    }
}
