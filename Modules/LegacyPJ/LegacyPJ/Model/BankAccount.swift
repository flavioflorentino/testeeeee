import Foundation
import SwiftyJSON

public struct BankAccount {
    public var id: Int?
    public var branch: String = ""
    public var branchDigit: String = ""
    public var account: String = ""
    public var accountDigit: String = ""
    public var type: String = ""
    public var operation: String = ""
    public var cpf: String?
    public var cnpj: String?
    public var bank: Bank?

    public init() {}

    public init?(json: JSON) {
        id = json["id"].int
        branch = json["agencia"].string ?? ""
        branchDigit = json["agencia_dv"].string ?? ""
        account = json["conta"].string ?? ""
        accountDigit = json["conta_dv"].string ?? ""
        type = json["tipo"].string ?? ""
        operation = json["operacao"].string ?? ""
        cpf = json["cpf"].string
        cnpj = json["cnpj"].string
        bank = Bank(json: json["bank"])
    }

    /// Get Adress as Dictionary
    public func toDictionary() -> [String: Any] {
        var p: [String: Any] = [:]

        p["agencia"] = branch
        p["agencia_dv"] = branchDigit
        p["conta"] = account
        p["conta_dv"] = accountDigit
        p["tipo"] = type
        p["operacao"] = operation
        p["document_bank"] = cnpj

        if let cpf = cpf {
            p ["document_bank"] = cpf
        }

        if let bankCode = self.bank?.code {
            p["banco_id"] = bankCode
        }

        return p
    }
}
