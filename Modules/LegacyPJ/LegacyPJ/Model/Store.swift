import Foundation
import SwiftyJSON

public struct Store: Equatable {
    public var address: Address?
    public var phone: String?
    public var restrictToScanner: Bool
    
    public init?(json: JSON) {
        self.address = Address(json: json["address"])
        
        if let phone = json["phones"].string {
            self.phone = phone
        }
        
        self.restrictToScanner = json["restrict_to_scanner"].bool ?? false
    }
    
    public func toDictionary() -> [String: Any] {
        var p: [String: Any] = [:]
        
        if let phone = self.phone {
            p["phone"] = phone
        }
        if let address = self.address {
            p["address"] = address.toDictionary()
        }
        p["restrict_to_scanner"] = restrictToScanner
        
        return p
    }
}
