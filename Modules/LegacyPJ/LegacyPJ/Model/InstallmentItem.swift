import Foundation
import SwiftyJSON

public struct InstallmentItem {
    public var numberOfInstallments: Int = 0
    public var adictionalFee: String = ""
    public var selected = false
    
    public init?(json: JSON) {
        guard let numberOfInstallments = json["number_of_installments"].int,
              let adictionalFee = json["fee"].string,
              let selected = json["selected"].bool
        else {
            return nil
        }
        
        self.numberOfInstallments = numberOfInstallments
        
        self.adictionalFee = adictionalFee.replacingOccurrences(of: ".", with: ",")
        
        self.selected = selected
    }
    
    /// Get InstallmentItem as Dictionary
    public func toDictionary() -> [String: Any] {
        var dict: [String: Any] = [:]
        
        dict["number_of_installments"] = self.numberOfInstallments
        dict["fee"] = self.adictionalFee
        dict["selected"] = self.selected
        
        return dict
    }
}
