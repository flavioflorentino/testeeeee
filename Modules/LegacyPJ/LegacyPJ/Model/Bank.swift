import Foundation
import SwiftyJSON

public struct Bank: Decodable {
    public let name: String
    public let code: String
    public let image: String?
    public let form: String
    
    enum CodingKeys: String, CodingKey {
        case code = "id"
        case name
        case image = "img_url"
        case form
    }

    public init?(json: JSON) {
        guard let code = json["id"].string, let name = json["name"].string else {
            return nil
        }
        
        self.code = code
        self.name = name
        self.image = json["img_url"].string
        self.form = json["form"].string ?? ""
    }
    
    public init(_ code: String, _ name: String, _ image: String, _ form: String) {
        self.name = name
        self.code = code
        self.image = image
        self.form = form
    }
    
    public func toDictionary() -> [String: Any] {
        let dictionary: [String: Any] = [
            "name": name,
            "code": code,
            "image": image ?? "",
            "form": form
        ]
        
        return dictionary
    }
}
