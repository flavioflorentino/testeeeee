import Foundation
import SwiftyJSON

public struct Consumer: Equatable {
    public var name: String = ""
    public var username: String = ""
    public var imageUrl: String = ""
    
    init() {}
    
    init(json: JSON) {
        if let name = json["name"].string {
            self.name = name
        }
        
        if let username = json["username"].string {
            self.username = username
        }
        
        if let imageUrl = json["img_url"].string {
            self.imageUrl = imageUrl
        }
    }
}
