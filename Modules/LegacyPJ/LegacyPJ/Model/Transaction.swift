import Foundation
import SwiftyJSON

public enum TransactionStatusLegacy: String, Equatable {
    case unknown = ""
    case canceled = "R"
}

public enum TransactionType: String, Equatable {
    case transaction = "transaction"
    case collector = "collector"
    case unknown = ""
}

public final class Transaction: Equatable {
    public static func == (lhs: Transaction, rhs: Transaction) -> Bool {
        lhs.id == rhs.id
    }
    
    public var id: Int = 0
    public var date: String = ""
    public var siteValue: String = ""
    public var isPix: Bool = false
    public var status = TransactionStatusLegacy.unknown
    public var consumer: Consumer?
    public var sellerOperator: Operator?
    public var type: TransactionType = .unknown
    public var rawDate: String = ""
    public var timestamp: String?
    
    public init(json: JSON) {
        if let id = json["id"].int {
            self.id = id
        }
        
        if let date = json["date"].string {
            self.date = date
        }
        
        if let siteValue = json["site_value"].string {
            self.siteValue = siteValue
        }
        
        if let isPix = json["is_pix"].bool {
            self.isPix = isPix
        }
        
        if let statusString = json["status_id"].string, let status = TransactionStatusLegacy(rawValue: statusString) {
            self.status = status
        }
        
        if let t = json["type"].string {
            if let type = TransactionType(rawValue: t) {
                self.type = type
            }
        }
        
        if let date = json["date_common"].string {
            rawDate = date
        }

        if let timestamp = json["timestamp"].string {
            self.timestamp = timestamp
        }
        
        self.consumer = Consumer(json: json["consumer"])
        
        self.sellerOperator = Operator(json: json["operator"])
    }

    public init() { }
}
