import Foundation
import SwiftyJSON

public struct Address: Equatable {
    public var cep: String?
    public var address: String?
    public var district: String?
    public var city: String?
    public var state: String?
    public var number: String?
    public var complement: String?
    public var lat: Double?
    public var lon: Double?
    public var isResidentialAddress = false
    
    public init() {}
    
    public init?(json: JSON) {
        guard
            let cep = json["cep"].string,
            let address = json["address"].string,
            let district = json["district"].string,
            let city = json["city"].string,
            let state = json["state"].string
            else {
                return nil
        }
        
        self.cep = cep
        self.address = address
        self.district = district
        self.city = city
        self.state = state
        
        if let number = json["number"].string {
            self.number = number
        }
        
        if let complement = json["complement"].string {
            self.complement = complement
        }
        
        if let lat = json["location"]["lat"].double {
            self.lat = lat
        }
        
        if let lon = json["location"]["lon"].double {
            self.lon = lon
        }
    }
    
    /// Get Adress as Dictionary
    public func toDictionary() -> [String: Any] {
        var p: [String: Any] = [:]
        
        if let cep = self.cep {
            p["cep"] = cep
        }
        if let address = self.address {
            p["address"] = address
        }
        if let district = self.district {
            p["district"] = district
        }
        if let city = self.city {
            p["city"] = city
        }
        if let state = self.state {
            p["state"] = state
        }
        if let number = self.number {
            p["number"] = number
        }
        if let complement = self.complement {
            p["complement"] = complement
        }
        
        p["location"] = [
            "lat": self.lat ?? 0,
            "lon": self.lon ?? 0
        ]

        p["is_residential_address"] = isResidentialAddress

        return p
    }
}
