import Foundation
import SwiftyJSON

public struct CodePicPay {
    public let key: String
    public let expire: Date
    
    public init(key: String, expire: Date) {
        self.key = key
        self.expire = expire
    }
    
    public init?(json: JSON) {
        guard
            let key = json["key"].string ,
            let expire = json["expire_key"].int
            else {
              return nil
        }
        
        self.key = key
        self.expire = Date(timeIntervalSinceNow: TimeInterval(expire))
    }
}
