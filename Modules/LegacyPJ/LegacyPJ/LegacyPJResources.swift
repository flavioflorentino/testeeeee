import Foundation

// swiftlint:disable convenience_type
final class LegacyPJResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: LegacyPJResources.self).url(forResource: "LegacyPJResources", withExtension: "bundle") else {
            return Bundle(for: LegacyPJResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: LegacyPJResources.self)
    }()
}
