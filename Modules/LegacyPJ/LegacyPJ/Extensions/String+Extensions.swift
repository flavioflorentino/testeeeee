extension String {
    var onlyNumbers: String {
        components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
    }
    
    func chopPrefix(_ count: Int = 1) -> String {
        String(suffix(from: index(startIndex, offsetBy: count)))
    }
    
    func chopSuffix(_ count: Int = 1) -> String {
        String(prefix(upTo: index(endIndex, offsetBy: -count)))
    }
    
    func removingWhiteSpaces() -> String {
        replacingOccurrences(of: " ", with: "")
    }
}

// MARK: - Subscript

extension String {
    /// SwifterSwift: Safely subscript string with index.
    ///
    /// - Parameter i: index.
    subscript(i: Int) -> String? {
        guard i >= 0 && i < count else {
            return nil
        }
        return String(self[index(startIndex, offsetBy: i)])
    }
    
    subscript(bounds: CountableRange<Int>) -> String {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return String(self[start..<end])
    }
    
    /// SwifterSwift: Safely subscript string within a half-open range.
    ///
    /// - Parameter range: Half-open range.
    subscript(range: CountableRange<Int>) -> String? {
        guard let lowerIndex = index(startIndex, offsetBy: max(0, range.lowerBound), limitedBy: endIndex) else {
            return nil
        }
        guard let upperIndex = index(lowerIndex, offsetBy: range.upperBound - range.lowerBound, limitedBy: endIndex) else {
            return nil
        }
        return String(self[lowerIndex..<upperIndex])
    }
}
