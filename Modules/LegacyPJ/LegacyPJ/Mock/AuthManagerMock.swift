import Foundation

public final class AuthManagerMock: AuthManagerProtocol {
    public var trackingWrapper: AuthManagerTrackingWrapperProtocol?
    
    public var isAuthenticated: Bool { isAuthenticatedUser }
    
    public var user: User?

    public var userAuth: UserAuth? { nil }
    
    public var authenticatedUsers: [UserAuth] = []
    
    public var actionResultToReturn: AuthManager.PerformActionResult = .canceled
    
    public var isAuthenticatedUser = true
    
    public init() { }
}

public extension AuthManagerMock {
    func authenticateNewAccount(_ auth: Auth, completion: @escaping ((LegacyPJError?) -> Void)) { }
    
    func refreshAccount(_ auth: Auth, completion: @escaping ((LegacyPJError?) -> Void)) { }
    
    func changeCurrentAccount(userAuth: UserAuth, completion: @escaping ((LegacyPJError?) -> Void)) { }
    
    func login(cnpj: String, username: String, password: String, completion: @escaping ((LegacyPJError?) -> Void)) { }
    
    func logout(userAuth: UserAuth?, changeAccount: Bool, completion: @escaping ((LegacyPJError?) -> Void)) { }
    
    func storeUserAuth(userAuth: UserAuth) { }
    
    func removeUserAuth(username: String?) { }
    
    func removeUserAuth(userAuth: UserAuth) { }
    
    func updateUser(auth: Auth, user: User) { }
    
    func storeUserRegister(model: Partner) { }
    
    func storeSignUpRegister(data: Data) { }
    
    func performActionWithAuthorization(_ message: String?, _ action: @escaping ((AuthManager.PerformActionResult) -> Void)) {
        switch actionResultToReturn {
        case .success(let pin):
            action(.success(pin))
        case .failure(let error):
            action(.failure(error))
        case .canceled:
            action(.canceled)
        }
    }
}
