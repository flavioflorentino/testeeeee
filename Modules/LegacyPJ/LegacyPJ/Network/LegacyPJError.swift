import SwiftyJSON

public struct LegacyPJError: Error {
    public let message: String
    public let code: Int
    public let type: String
    public let title: String?
    
    var localizedDescription: String {
        self.message
    }
    
    public init(message: String, code: Int = 0, type: String = "", title: String? = nil) {
        self.message = message
        self.code = code
        self.type = type
        self.title = title
    }
    
    init(apiJSON: [String: JSON]) {
        if let message = apiJSON["error_message"]?.string {
            self.message = message
        } else {
            self.message = ""
        }
        
        if let type = apiJSON["error_type"]?.string {
            self.type = type
        } else {
            self.type = ""
        }
        
        if let code = apiJSON["code"]?.int {
            self.code = code
        } else {
            self.code = 0
        }
        title = apiJSON["error_title"]?.string
    }
}

extension LegacyPJError: LocalizedError {
    public var errorDescription: String? {
        NSLocalizedString(self.message, comment: "")
    }
    
    public var isPermissionDenied: Bool {
        type == "permission_denied" && code == 400
    }
}

public extension LegacyPJError {
    static var invalidCnpj: LegacyPJError {
        LegacyPJError(message: Strings.Errors.invalidCnpj)
    }
    
    static var requestError: LegacyPJError {
        LegacyPJError(message: Strings.Errors.requestError)
    }
}
