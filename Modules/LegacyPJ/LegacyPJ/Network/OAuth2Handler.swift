import Alamofire
import Core
import Foundation
import SwiftyJSON
import FeatureFlag

public struct OAuth2HandlerProperties {
    let apiUrl: String
    
    public init(apiUrl: String) {
        self.apiUrl = apiUrl
    }
}

public protocol OAuth2HandlerWrapperProtocol {
    func logoutUserForInvalidToken()
    func displayWelcome()
}

public final class OAuth2Handler {
    typealias Dependecies = HasFeatureManager & HasLocationManager & HasAuthManager & HasKeychainManager & HasKeychainManagerPersisted
     
    // MARK: - Private Properties
    
    private var wrapper: OAuth2HandlerWrapperProtocol
    private var properties: OAuth2HandlerProperties
    private var defaultHeaders: [String: String] = [:]
    private var dependencies: Dependecies
    
    private let lock = NSLock()
    
    private var isRefreshing = false
    private var requestsToRetry: [RequestRetryCompletion] = []
    
    private let sessionManager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        
        let pinningConfiguration = PinningConfigurationApi()
        
        guard pinningConfiguration.shouldPin else {
            debugPrint("[OAuth2Handler] Alamofire isn't pinning")
            return SessionManager(configuration: configuration)
        }
        
        var policies: [String: ServerTrustPolicy] = [:]
        pinningConfiguration.domainsTrusted.forEach { domain in
            let trustPolicy = ServerTrustPolicy.pinCertificates(
                certificates: pinningConfiguration.generateSecCertificates(),
                validateCertificateChain: true,
                validateHost: true
            )
            policies[domain] = trustPolicy
        }
        
        let serverPolicyManager = ServerTrustPolicyManager(policies: policies)
        let sessionManager = SessionManager(configuration: configuration, serverTrustPolicyManager: serverPolicyManager)
        
        return sessionManager
    }()
    
    init(properties: OAuth2HandlerProperties,
         headers: [String: String],
         wrapper: OAuth2HandlerWrapperProtocol,
         dependencies: Dependecies = DependencyContainer()
    ) {
        self.properties = properties
        self.defaultHeaders = headers
        self.wrapper = wrapper
        self.dependencies = dependencies
    }
}

// MARK: - Private Methods

private extension OAuth2Handler {
    func refreshTokens(completion: @escaping ((_ succeeded: Bool) -> Void) ) {
        guard !isRefreshing, let userAuth = dependencies.authManager.userAuth else { return }
        
        isRefreshing = true
        
        let urlString = "\(properties.apiUrl)/oauth/access-token"
        let parameters: [String: Any] = [
            "refresh_token": userAuth.auth.refreshToken,
            "client_id": Environment.apiClientId,
            "client_secret": Environment.apiClientSecret,
            "grant_type": "refresh_token"
        ]
        
        // Try to refresh the token
        let request = sessionManager.request(
            urlString,
            method: .post,
            parameters: parameters,
            encoding: JSONEncoding.default
        )
            
        request.responseJSON { [weak self] response in
            guard let self = self else { return }
            
            debugPrint(response)
            
            guard
                let json = response.result.value as? [String: Any],
                let auth = Auth(json: JSON(json)["data"]) else
            {
                self.logout(completion: completion)
                return
            }
            
            // Refresh the authenticated account
            self.dependencies.authManager.refreshAccount(auth, completion: { error in
                completion(error == nil)
                self.isRefreshing = false
            })
        }
        
        debugPrint("-------------- REFRESH TOKEN -----------")
        debugPrint(request)
    }
    
    func logout(completion: @escaping(_ succeeded: Bool) -> Void) {
        // When the refresh token is invalid we logout the current user
        dependencies.authManager.logout(userAuth: nil, changeAccount: false) { [weak self] _ in
            guard let self = self else { return }
            
            if self.dependencies.featureManager.isActive(.isAppCoordinatorAvailable) {
                self.wrapper.displayWelcome()
            } else {
                self.wrapper.logoutUserForInvalidToken()
            }
            completion(false)
            self.isRefreshing = false
        }
    }
    
    func setupDefaultHeaders(headers: [String: String]) {
        var headers: [String: String] = [:]
        
        let deviceOs = "ios"
        
        headers["device_os"] = deviceOs
        headers["device-os"] = deviceOs
        headers["device_model"] = UIDevice.current.deviceModel
        headers["device_id"] = UIDevice.current.picpayDeviceId(dependencies: dependencies)
        headers["installation_id"] = UIDevice.current.installationId
        headers["timezone"] = TimeZone.current.identifier
        headers[CorrelationId.headerField] = CorrelationId.id
        
        let dependencies: HasLocationManager = DependencyContainer()
        guard let location = LocationDescriptor(dependencies.locationManager.authorizedLocation) else {
            defaultHeaders = headers
            return
        }

        headers[LocationHeaders.latitude.rawValue] = location.latitude
        headers[LocationHeaders.longitude.rawValue] = location.longitude
        headers[LocationHeaders.accuracy.rawValue] = location.accuracy
        headers[LocationHeaders.locationTimestamp.rawValue] = location.locationTimestamp
        headers[LocationHeaders.currentTimestamp.rawValue] = location.currentTimestamp

        defaultHeaders = headers
    }
}

// MARK: - RequestRetrier

extension OAuth2Handler: RequestRetrier {
    public func should(
        _ manager: SessionManager,
        retry request: Request,
        with error: Error,
        completion: @escaping RequestRetryCompletion
    ) {
        lock.lock()
        defer {
            lock.unlock()
        }
        
        guard let response = request.task?.response as? HTTPURLResponse,
            response.statusCode == 401,
            dependencies.authManager.userAuth != nil else {
            completion(false, 0.0)
            return
        }
        
        requestsToRetry.append(completion)
        
        // it prevents calling the refresh request twice times
        guard !isRefreshing else { return }
        refreshTokens { [weak self] succeeded in
            guard let self = self else { return }
            
            self.lock.lock()
            defer {
                self.lock.unlock()
            }
            
            // peform the request retry
            self.requestsToRetry.forEach { $0(succeeded, 0.0) }
            self.requestsToRetry.removeAll()
        }
    }
}

// MARK: - RequestAdapter

extension OAuth2Handler: RequestAdapter {
    public func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        // Add Picpay default Headers
        var urlRequest = urlRequest
        
        // Access Token
        if let accessToken = dependencies.authManager.userAuth?.auth.accessToken {
            urlRequest.setValue("Bearer " + accessToken, forHTTPHeaderField: "Authorization")
        }

        for header in defaultHeaders {
            urlRequest.setValue(header.value, forHTTPHeaderField: header.key)
        }
        
        return urlRequest
    }
}
