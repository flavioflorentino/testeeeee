import Core
import Foundation

public enum Endpoint { }

// MARK: - Private Methods

private extension Endpoint {
    static var baseUrl: String {
        guard let apiUrl = Environment.apiUrl else {
            fatalError("You need to define the api url")
        }
        
        return apiUrl
    }
    
    static func apiUrl(_ url: String) -> String {
        baseUrl + url
    }
}

// MARK: - Public Methods

public extension Endpoint {
    // MARK: - Account
    
    static func coordinateForAddress() -> String {
        apiUrl("/account/coordinates")
    }
    
    static func accountVerifyMobilePhone() -> String {
        apiUrl("/account/verify-mobile-phone")
    }
    
    static func accountVerifyMobilePhoneCode() -> String {
        apiUrl("/account/verify-mobile-phone-code")
    }
    
    static func signUpValidateCnpj() -> String {
        apiUrl("/account/validate-cnpj")
    }
    
    static func verifyDuplicatedUser() -> String {
        apiUrl("/account/verify-duplicate-user")
    }
    
    static func receiptFeeList() -> String {
        apiUrl("/account/receipt-fees")
    }
    
    static func signupUploadLogo() -> String {
        apiUrl("/account/seller-logo")
    }
    
    static func createAccount() -> String {
        apiUrl("/account")
    }
    
    static func verifyAccountIntegrity() -> String {
        apiUrl("/account/validate-qsa")
    }
    
    static func verifyCep() -> String {
        apiUrl("/account/verify-cep")
    }
    
    // MARK: - Transactions
    
    static func transactionList() -> String {
        apiUrl("/transaction")
    }
    
    // MARK: - Bank
    
    static func bankList() -> String {
        apiUrl("/list-banks")
    }
    
    // MARK: - Seller
    
    static func sellerFees() -> String {
        apiUrl("/seller/fees")
    }
    
    static func sellerAuthenticated() -> String {
        apiUrl("/seller")
    }
    
    static func updateSeller() -> String {
        apiUrl("/seller")
    }
    
    static func updateLogo() -> String {
        apiUrl("/seller/update-logo")
    }
    
    static func bankAccount() -> String {
        apiUrl("/seller/bank-account")
    }
    
    static func updateBankAccount() -> String {
        apiUrl("/seller/bank-account")
    }
    
    static func operatorList() -> String {
        apiUrl("/seller/operator")
    }
    
    static func updateAddress() -> String {
        apiUrl("/seller/address")
    }
    
    static func updateDisplayName() -> String {
        apiUrl("/seller/displayname")
    }
    
    static func updateRestrictScannerPayment() -> String {
        apiUrl("/seller/restrict-scanner-payment")
    }
    
    static func updatePhone() -> String {
        apiUrl("/seller/phones")
    }
    
    static func promocode() -> String {
        apiUrl("/seller/indication-program")
    }
    
    static func linkCompanyCNPJUserCPF() -> String {
        apiUrl("/seller/company/link")
    }
    
    // MARK: - PPCode
    static func ppcode() -> String {
        apiUrl("/ppcode")
    }
    
    // MARK: - UserSeller
    
    static func operatorCreate() -> String {
        apiUrl("/user-seller")
    }
    
    static func operatorUpdate() -> String {
        apiUrl("/user-seller/operator")
    }
    
    static func operatorDelete(id: Int) -> String {
        apiUrl("/user-seller/\(id)")
    }
    
    // MARK: - Plan
    
    static func updatePlanFee() -> String {
        apiUrl("/plan/fee")
    }
    
    // MARK: - Login
    
    static func webMonitor() -> String {
        apiUrl("/login/authorize-token-two-factor")
    }
    
    // MARK: - Terms
    
    static func urlTermsOfUse() -> String {
        apiUrl("/terms-of-use")
    }
    
    static func campaignUrlTermsAndConditions() -> String {
        apiUrl("/site/cashback-terms-webview")
    }
}

// MARK: - Internal Methods

extension Endpoint {
    // MARK: - Transactions
    
    static func transactionListByDate(date: String) -> String {
        apiUrl("/transaction/resume/\(date)")
    }
    
    static func transactionUserSellerList(_ userSellerId: Int?) -> String {
        if let id = userSellerId {
            return apiUrl("/transaction/user-seller/\(id)")
        } else {
            return apiUrl("/transaction/user-seller")
        }
    }
    
    static func transactionHistoryListUserSeller(_ userSellerId: Int?) -> String {
        if let id = userSellerId {
            return apiUrl("/transaction/history/user-seller/\(id)")
        } else {
            return apiUrl("/transaction/history")
        }
    }
    
    static func transactionHistory(id: Int) -> String {
        apiUrl("/transaction/history/\(id)")
    }
    
    static func transactionsSummaryByDay() -> String {
        apiUrl("/transaction/resume/daily")
    }
    
    static func transactionsSummaryByMonth() -> String {
        apiUrl("/transaction/resume/monthly")
    }
    
    static func cancelTransaction() -> String {
        apiUrl("/transaction/cancel-transaction")
    }
    
    // MARK: - Operators
    
    static func operatorCashierBallance() -> String {
        apiUrl("/user-seller/cashier-ballances")
    }
    
    // MARK: - User
    
    static func userAuthenticated() -> String {
        apiUrl("/authenticated")
    }
    
    static func updateUser() -> String {
        apiUrl("/user-seller")
    }
    
    static func userUpdateToken() -> String {
        apiUrl("/notification-token")
    }
    
    static func userChangePassword() -> String {
        apiUrl("/user-seller")
    }
    
    static func userSellerDistinctId() -> String {
        apiUrl("/user-seller/distinct-id")
    }
    
    // MARK: - Transactions
    
    static func movementList() -> String {
        apiUrl("/movements")
    }
    
    static func movementFutureList() -> String {
        apiUrl("/next-movements")
    }
    
    static func movementExportation() -> String {
        apiUrl("/transaction/account-statement")
    }
    
    // MARK: - Installments
    
    static func maxInstallments() -> String {
        apiUrl("/plan/max-installments")
    }
    
    static func installmentFees() -> String {
        apiUrl("/installment-fees")
    }
    
    // MARK: - Authentication
    
    static func auth() -> String {
        apiUrl("/oauth/access-token")
    }
    static func authCnpj() -> String {
        apiUrl("/login/verify-cnpj")
    }
}
