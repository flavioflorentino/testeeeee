import Foundation

open class Api {
    // MARK: - Private Properties
    
    public let requestManager: RequestManager
    
    // MARK: - Initializer
    public init(requestManager: RequestManager = RequestManager.shared) {
        self.requestManager = requestManager
    }
}
