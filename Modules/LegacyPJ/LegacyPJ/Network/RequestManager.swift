import Alamofire
import Core
import Foundation
import SwiftyJSON
import FeatureFlag

public typealias UploadProgress = ((_ progress: Double) -> Void)
public typealias BaseCompletion = ((_ error: LegacyPJError?) -> Void)

/// Class responsable to manger Any Api request
public final class RequestManager {
    typealias Dependencies = HasKeychainManager
    
    public private(set) static var shared = RequestManager()
    private(set) var sessionManager: SessionManager
    
    // Manager for public requests
    private var publicSessionManager: SessionManager
    
    private init(properties: OAuth2HandlerProperties,
                 headers: [String: String],
                 wrapper: OAuth2HandlerWrapperProtocol
    ) {
        let oauthHandler = OAuth2Handler(properties: properties, headers: headers, wrapper: wrapper)
        
        sessionManager = SessionManager()
        publicSessionManager = SessionManager()
        
        let pinningConfiguration = PinningConfigurationApi()

        if pinningConfiguration.shouldPin {
            let policies = generateTrustPolicies(configuration: pinningConfiguration)
            let serverPolicyManager = ServerTrustPolicyManager(policies: policies)
            sessionManager = Alamofire.SessionManager(serverTrustPolicyManager: serverPolicyManager)
            publicSessionManager = Alamofire.SessionManager(serverTrustPolicyManager: serverPolicyManager)
        }
        
        sessionManager.adapter = oauthHandler
        sessionManager.retrier = oauthHandler
        
        publicSessionManager.adapter = oauthHandler
    }
    
    private init() {
        sessionManager = SessionManager()
        publicSessionManager = SessionManager()
    }
}

// MARK: - Public Methods

public extension RequestManager {
    static func setup(properties: OAuth2HandlerProperties, headers: [String: String], wrapper: OAuth2HandlerWrapperProtocol) {
        shared = RequestManager(properties: properties, headers: headers, wrapper: wrapper)
        setupPinning()
    }

    /// Make an authenticated request for API
    ///
    /// - parameter urlString: URL string
    /// - parameter method: HTTP Method
    /// - parameter parameters: parameters
    /// - parameter headers: Headers
    /// - parameter pin: User Password for validation
    func apiRequest(
        urlString: String,
        method: Alamofire.HTTPMethod,
        parameters: Parameters? = nil,
        headers: [String: String] = [:],
        pin: String? = nil
    ) -> DataRequest {
        let commonHeaders = applyPin(headers, pin: pin)
        let encoding: ParameterEncoding = method == .get ? URLEncoding.default : JSONEncoding.default

        let request = sessionManager.request(
            urlString,
            method: method,
            parameters: parameters,
            encoding: encoding,
            headers: commonHeaders
        )
        
        logRequest(request: request)
        logParameters(parameters: parameters)
        
        return request.validate()
    }
    
    /// Make a public request for API
    func apiPublicRequest(
        urlString: String,
        method: Alamofire.HTTPMethod,
        parameters: Parameters? = nil,
        headers: [String: String] = [:]
    ) -> DataRequest {
        var commonHeaders: [String: String] = [
            "Accept": "application/json"
        ]
        
        headers.forEach { commonHeaders.updateValue($0, forKey: $1) }
        let encoding: ParameterEncoding = (method == .get ? URLEncoding.default : JSONEncoding.default)
        
        let request = publicSessionManager.request(
            urlString,
            method: method,
            parameters: parameters,
            encoding: encoding,
            headers: headers
        )
        
        logRequest(request: request)
        
        return request
    }
    
    /// Upload a file
    ///
    /// - parameter urlString: URL string
    /// - parameter method: HTTP Method
    /// - parameter files: Files to send
    /// - parameter headers: Headers
    /// - parameter pin: User Password for validation
    func apiUpload(
        _ urlString: String,
        method: Alamofire.HTTPMethod,
        files: [FileUpload],
        uploadProgress: UploadProgress?,
        parameters: Parameters? = nil,
        headers: [String: String] = [:],
        pin: String? = nil,
        completion: @escaping ((DataResponse<JSON>) -> Void)
    ) {
        let commonHeaders = applyPin(headers, pin: pin)
        
        sessionManager.upload(
            multipartFormData: multipartBuilder(files: files, parameters: parameters),
            to: urlString,
            method: method,
            headers: commonHeaders,
            encodingCompletion: multipartCompletion(uploadProgress: uploadProgress, completion: completion)
        )
    }
    
    /// Upload a file
    ///
    /// - parameter urlString: URL string
    /// - parameter method: HTTP Method
    /// - parameter files: Files to send
    /// - parameter headers: Headers
    /// - parameter pin: User Password for validation
    func apiPublicUpload(
        _ urlString: String,
        method: Alamofire.HTTPMethod,
        files: [FileUpload],
        parameters: Parameters? = nil,
        headers: [String: String] = [:],
        uploadProgress: UploadProgress? = nil,
        completion: @escaping ((DataResponse<JSON>) -> Void)
    ) {
        publicSessionManager.upload(
            multipartFormData: multipartBuilder(files: files, parameters: parameters),
            to: urlString,
            method: method,
            headers: headers,
            encodingCompletion: multipartCompletion(uploadProgress: uploadProgress, completion: completion)
        )
    }
}

// MARK: - Private Methods

private extension RequestManager {
    static func setupPinning() {
        let dependencies: Dependencies = DependencyContainer()
        let rawDomains: String
        let shouldPin: PinResponse
        
        if AppMode.current == .debug {
            shouldPin = .no
            rawDomains = "bizapi.ppay.me,ppay.me,picpay.com,bizapi.picpay.com"
        } else {
            shouldPin = FeatureManager.shared.isActive(.featureCertificatePinning) ? .yes : .no
            let rawDomainUnformatted = FeatureManager.shared.text(.featureCertificatePinningDomains)
            rawDomains = rawDomainUnformatted.chopPrefix().chopSuffix().removingWhiteSpaces()
        }
        
        dependencies.keychain.set(key: KeychainKey.shouldPin, value: String(shouldPin.rawValue))
        let domains = rawDomains.replacingOccurrences(of: "\"", with: "")
        dependencies.keychain.set(key: KeychainKey.pinDomains, value: domains)
    }
    
    func generateTrustPolicies(configuration: PinningConfigurationApi) -> [String: ServerTrustPolicy] {
        var dict: [String: ServerTrustPolicy] = [:]
        let certificates = configuration.generateSecCertificates()
        if !configuration.shouldPin {
            debugPrint("[RequestManager] Alamofire isn't pinning")
            return dict
        }
        configuration.domainsTrusted.forEach { domain in
            let trustPolicy = ServerTrustPolicy.pinCertificates(
                certificates: certificates,
                validateCertificateChain: true,
                validateHost: true
            )
            dict[domain] = trustPolicy
        }
        
        return dict
    }
    
    /// Merge the headers with pin
    /// - parameter headers: Headers
    /// - parameter pin: User Password for validation
    func applyPin(_ headers: [String: String] = [:], pin: String? = nil) -> [String: String] {
        var commonHeaders: [String: String] = [:]
        headers.forEach { commonHeaders.updateValue($0, forKey: $1) }
        
        if let password = pin {
            commonHeaders["pin"] = password
        }
        
        return commonHeaders
    }
    
    func multipartBuilder(files: [FileUpload], parameters: Parameters?) -> ((MultipartFormData) -> Void) {
        { multipartFormData in
            for file in files {
                multipartFormData.append(file.data, withName: file.key, fileName: file.fileName, mimeType: file.mimeType)
            }
            
            if let parameters = parameters {
                for (key, value) in parameters {
                    let v = "\(value)"
                    guard let data = v.data(using: .utf8) else { continue }
                    multipartFormData.append(data, withName: key)
                }
            }
        }
    }
    
    func multipartCompletion(
        uploadProgress: UploadProgress?,
        completion: @escaping ((DataResponse<JSON>) -> Void)
    ) -> ((SessionManager.MultipartFormDataEncodingResult) -> Void) {
        { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload
                    .uploadProgress(closure: { (progress: Progress) in
                        uploadProgress?(progress.fractionCompleted)
                    })
                    .responseApi(completionHandler: completion)
                
            case .failure:
                let error = LegacyPJError(message: Strings.Errors.errorDecodeForUpload, code: 1_006)
                completion(DataResponse<JSON>(request: nil, response: nil, data: nil, result: Result.failure(error)))
            }
        }
    }
    
    func logRequest(request: Request) {
        debugPrint("----------------- REQUEST -------------------")
        debugPrint("")
        debugPrint(request)
        debugPrint("----------------- FIM REQUEST -------------------")
        debugPrint("")
    }
    
    func logParameters(parameters: Parameters?) {
        debugPrint("----------------- REQUEST PARAMETERS -------------------")
        debugPrint("")
        debugPrint(parameters as Any)
        debugPrint("----------------- FIM REQUEST PARAMETERS -------------------")
        debugPrint("")
    }
}
