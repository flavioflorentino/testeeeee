public struct FileUpload {
    var key: String
    var data: Data
    var fileName: String
    var mimeType: String
    
    public init(key: String, data: Data, fileName: String, mimeType: String) {
        self.key = key
        self.data = data
        self.fileName = fileName
        self.mimeType = mimeType
    }
}
