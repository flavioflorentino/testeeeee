import Alamofire
import Foundation
import SwiftyJSON

extension Alamofire.DataRequest {
    /// Serialize the response for the picpay api response format
    static func apiResponseSerializer(filterbyJsonData: Bool = true) -> DataResponseSerializer<JSON> {
        DataResponseSerializer { _, res, data, error in
            debugPrint("------ REPONSE ------")
            debugPrint("")
            debugPrint(data ?? "")
            debugPrint("------ fim REPONSE -------")
            debugPrint("")
            
            guard error == nil else {
                if res?.statusCode == 401 {
                    return .failure(LegacyPJError(message: Strings.Errors.errorApi401, code: 1_401))
                }
                return .failure(LegacyPJError(message: Strings.Errors.errorApiRequest, code: 1_001))
            }
            
            guard let validData = data else {
                let reason = Strings.Errors.errorDecodeServer
                return .failure(LegacyPJError(message: reason, code: 1_002))
            }
            
            let json = try? JSON(data: validData)
            return sanitizeError(json: json ?? "", filterbyJsonData: filterbyJsonData)
        }
    }
    
    /// Process the result seeking for picpay error
    static func sanitizeError(json: JSON, filterbyJsonData: Bool) -> Result<JSON> {
        debugPrint("---- JSON -----")
        debugPrint(json)
        debugPrint("------ fim JSON -------")
        
        if let meta = json["meta"].dictionary, meta["error_message"]?.string != nil {
            let error = LegacyPJError(apiJSON: meta)
            return .failure(error)
        }
        
        if json.null != nil {
            return .failure(LegacyPJError(message: Strings.Errors.errorDecodeServerJson, code: 1_003))
        }
        
        guard filterbyJsonData else {
            return .success(json)
        }
        
        if json["data"].null != nil {
            return .failure(LegacyPJError(message: Strings.Errors.errorDecodeServerJsonData, code: 1_004))
        }
        
        return .success(json["data"])
    }
    
    @discardableResult
    public func responseApi(completionHandler: @escaping (DataResponse<JSON>) -> Void) -> Self {
        let responseSerializer = DataRequest.apiResponseSerializer()
        return response(responseSerializer: responseSerializer, completionHandler: completionHandler)
    }
    
    @discardableResult
    public func responseAuthApi(completionHandler: @escaping (DataResponse<JSON>) -> Void) -> Self {
        let responseSerializer = DataRequest.apiResponseSerializer(filterbyJsonData: false)
        return response(responseSerializer: responseSerializer, completionHandler: completionHandler)
    }
    
    /// Request api and execute completion handler
    public func responseApiBaseCompletion(completionHandler: @escaping BaseCompletion) {
        response(responseSerializer: DataRequest.apiResponseSerializer(), completionHandler: { response in
            switch response.result {
            case .success:
                completionHandler(nil)
                return
            case .failure:
                completionHandler(response.result.picpayError)
                return
            }
        })
    }
}

public extension Result {
    var picpayError: LegacyPJError? {
        switch self {
        case .success:
            return nil
        case .failure(let error):
            return error as? LegacyPJError
        }
    }
}
