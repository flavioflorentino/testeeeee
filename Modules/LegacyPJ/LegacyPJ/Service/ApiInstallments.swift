import Alamofire
import Foundation
import SwiftyJSON

public final class ApiInstallments: Api { }

public extension ApiInstallments {
    func data(_ completion: @escaping((Swift.Result<(fees: [InstallmentItem], header: String?), LegacyPJError>) -> Void) ) {
        requestManager.apiRequest(urlString: Endpoint.installmentFees(), method: .get)
            .responseApi { response in
                switch response.result {
                case .success(let json):
                    guard let list = json["list"].array else {
                        completion(.failure(LegacyPJError(message: Strings.Errors.errorDecodeServerJsonData, code: 1_004)))
                        return
                    }
                    
                    var fees: [InstallmentItem] = []
                    
                    for item in list {
                        if let installment = InstallmentItem(json: item) {
                            fees.append(installment)
                        }
                    }
                    
                    completion(.success((fees, json["header"].string)))
                case .failure:
                    guard let error = response.result.picpayError else {
                        completion(.failure(LegacyPJError.requestError))
                        return
                    }
                    completion(.failure(error))
                }
            }
    }
    
    // Save selected installment on server
    // - value -> O novo valor do número de parcelas
    // - pin   -> Senha do usuário
    func saveMaxInstallments(value: Int, pin: String, _ completion: @escaping((_ error: LegacyPJError?) -> Void) ) {
        let params: Parameters = [
            "max_installments": value
        ]
        
        requestManager.apiRequest(urlString: Endpoint.maxInstallments(), method: .put, parameters: params, pin: pin)
            .responseApi { response in
                switch response.result {
                case .success(let json):
                    if json["success"].bool != nil {
                        completion(nil)
                    } else {
                        completion(LegacyPJError(message: Strings.Errors.errorDecodeServerJsonData, code: 1_004))
                    }
                case .failure:
                    completion(response.result.picpayError)
                }
            }
    }
}
