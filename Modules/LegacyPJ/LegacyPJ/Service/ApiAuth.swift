import Alamofire
import Core
import Foundation

public final class ApiAuth: Api { }

// MARK: - Public Methods

public extension ApiAuth {
    /// Authenticate a user
    ///
    /// - parameter cpnj: Seller CNPJ
    /// - parameter username: Username
    /// - parameter password: User password
    /// - parameter closure: callback Auth -> Auth data
    func auth(cnpj: String, username: String, password: String, completion: @escaping ((_ auth: Auth?, _ error: LegacyPJError?) -> Void) ) {
        let parameters: Parameters =
            [
                "client_id": Environment.apiClientId,
                "client_secret": Environment.apiClientSecret,
                "grant_type": "password",
                "password": password,
                "username": [
                    "cnpj": cnpj.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(),
                    "email": username
                ]
            ]
        
        requestManager.apiPublicRequest(urlString: Endpoint.auth(), method: .post, parameters: parameters as [String: AnyObject]?).responseApi { response in
            switch response.result {
            case .success(var json):
                
                // adjusts the expiration time (the server sends only the expires time since now)
                // to stores this the expire time needs to transformed in timestamp
                if let expire = json["expires_in"].int {
                    json["expires_in"].int = Int(Date(timeIntervalSinceNow: TimeInterval(expire)).timeIntervalSince1970)
                }
                
                if let auth = Auth(json: json) {
                    completion(auth, nil)
                } else {
                    completion(nil, LegacyPJ.LegacyPJError(message: "Não foi possivel efetuar o login. Erro Cod: 001") )
                }
            case .failure:
                completion(nil, response.result.picpayError )
            }
        }
    }
    
    /// Check if the cnpj is a valid cnpj
    ///
    /// - parameter cpnj: Seller CNPJ
    /// - parameter closure: callback
    func validate(cnpj: String, completion: @escaping ((_ error: LegacyPJError?) -> Void) ) {
        let p: Parameters = [
            "cnpj": cnpj.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        ]
        
        requestManager.apiPublicRequest(urlString: Endpoint.authCnpj(), method: .post, parameters: p).responseApi { response in
            switch response.result {
            case .success:
                completion(nil)
            case .failure:
                completion(response.result.picpayError)
            }
        }
    }
}
