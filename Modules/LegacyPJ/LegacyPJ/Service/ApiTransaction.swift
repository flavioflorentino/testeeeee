import Alamofire
import Foundation
import SwiftyJSON

public struct TransactionListResponse {
    public let pagination: Pagination
    public let list: [Transaction]
    
    public init(pagination: Pagination, list: [Transaction]) {
        self.pagination = pagination
        self.list = list
    }
}

public final class ApiTransaction: Api {
    public typealias TransactionListCompletion = (Swift.Result<TransactionListResponse, LegacyPJError>) -> Void
    public typealias LoadHistoryCompletion = (Swift.Result<(list: [HistoryItem], hasNext: Bool), LegacyPJError>) -> Void
    public typealias SumaryListResult = Swift.Result<(list: [SummaryItem], pagination: Pagination), LegacyPJError>
    public typealias UserSellerTransactionCompletion = (Swift.Result<(list: TransactionListResponse, totalValue: String), LegacyPJError>) -> Void
}

// MARK: - Public Methods
public extension ApiTransaction {
    /// Retrive the list of transaction
    /// - parameter page: page for pagination
    func list(
        _ page: Int?,
        endpoint: String = Endpoint.transactionList(),
        _ completion: @escaping(TransactionListCompletion)
    ) {
        let p: Parameters = ["page": page ?? ""]
        transactionList(url: endpoint, parameters: p, completion)
    }
    
    /// Retrive the list of transaction
    /// - parameter date: date on format dd-mm-yyyy or mm-yyyy
    /// - parameter page: page for pagination
    func listByDate(
        _ date: String,
        page: Int?,
        _ completion: @escaping(TransactionListCompletion)
    ) {
        let p: Parameters = ["page": page ?? ""]
        transactionList(url: Endpoint.transactionListByDate(date: date), parameters: p, completion)
    }

    /// Retrive the list of transaction
    private func transactionList(
        url: String,
        parameters: Parameters,
        _ completion: @escaping(TransactionListCompletion)
    ) {
        requestManager.apiRequest(urlString: url, method: .get, parameters: parameters)
            .responseApi { response in
                switch response.result {
                case .success(let json):
                    let list: [Transaction] = json["list"].array?.compactMap { Transaction(json: $0) } ?? []
                    let pag = Pagination(json: json)
                    
                    completion(.success(TransactionListResponse(pagination: pag, list: list)))
                case .failure:
                    completion(.failure(response.result.picpayError ?? LegacyPJError.requestError))
                }
            }
    }
    
    /// Retrive the list of transaction
    /// - parameter userSellerId: User id
    /// - parameter page: page
    func userSellerTransactionList(
        _ userSellerId: Int?,
        page: Int?,
        _ completion: @escaping(UserSellerTransactionCompletion)
    ) {
        let p: Parameters = ["page": page ?? ""]
        requestManager.apiRequest(urlString: Endpoint.transactionUserSellerList(userSellerId), method: .get, parameters: p)
            .responseApi { response in
                switch response.result {
                case .success(let json):
                    let list: [Transaction] = json["list"].array?.compactMap { Transaction(json: $0) } ?? []
                    let pag = Pagination(json: json)
                    let totalValue = json["total_value"].string ?? ""
                    
                    completion(.success((TransactionListResponse(pagination: pag, list: list), totalValue)))
                case .failure:
                    completion(.failure(response.result.picpayError ?? LegacyPJError.requestError))
                }
            }
    }
    
    /// Retrive the history list of transactions
    /// - parameter userSellerId: User id
    /// - parameter lastId: last loaded id
    func userSellerTransactionHistory(_ userSellerId: Int?, lastId: Int?, _ completion: @escaping(LoadHistoryCompletion)) {
        var p: Parameters = ["t": "_"]
        if let id = lastId {
            p["last_id"] = id
        }
        
        requestManager.apiRequest(urlString: Endpoint.transactionHistoryListUserSeller(userSellerId), method: .get, parameters: p)
            .responseApi { response in
                switch response.result {
                case .success(let json):
                    let list: [HistoryItem] = json["list"].array?.compactMap { HistoryItem(json: $0) } ?? []
                    let hasNext = json["next"].bool ?? false
                    completion(.success((list: list, hasNext: hasNext)))
                    
                case .failure:
                    completion(.failure(response.result.picpayError ?? LegacyPJError.requestError))
                }
            }
    }
    
    /// Retrive the history item
    /// - parameter id: history id
    func transactionHistory(_ id: Int, _ completion: @escaping((_ item: HistoryItem?, _ error: LegacyPJError?) -> Void) ) {
        requestManager.apiRequest(urlString: Endpoint.transactionHistory(id: id), method: .get)
            .responseApi { response in
                switch response.result {
                case .success(let json):
                    guard let item = HistoryItem(json: json) else {
                        completion(nil, LegacyPJError(message: Strings.Errors.errorDecodeObject))
                        return
                    }
                    
                    completion(item, nil)
                case .failure:
                    completion(nil, response.result.picpayError)
                }
            }
    }
    
    /// Cancel a transaction
    /// - parameter pin: User passowrd
    /// - parameter id: Transaction id
    func cancelTransaction(pin: String, id: Int, _ completion: @escaping((_ error: LegacyPJError?) -> Void) ) {
        let p: Parameters = ["id": id]
        
        requestManager.apiRequest(urlString: Endpoint.cancelTransaction(), method: .post, parameters: p, pin: pin)
            .responseApi { response in
                switch response.result {
                case .success:
                    completion(nil)
                case .failure:
                    completion(response.result.picpayError)
                }
            }
    }
    
    /// Retrive the summary list of transactions by day
    /// - parameter page: page
    func summaryListByDay(_ page: Int?, _ completion: @escaping(SumaryListResult) -> Void) {
        let p: Parameters = ["page": page ?? ""]
        summaryList(url: Endpoint.transactionsSummaryByDay(), parameters: p, completion)
    }
    
    /// Retrive the summary list of transactions by month
    /// - parameter page: page
    func summaryListByMonth(_ page: Int?, _ completion: @escaping(SumaryListResult) -> Void) {
        let p: Parameters = ["page": page ?? ""]
        summaryList(url: Endpoint.transactionsSummaryByMonth(), parameters: p, completion)
    }
    
    private func summaryList(
        url: String,
        parameters: Parameters,
        _ completion: @escaping(SumaryListResult) -> Void
    ) {
        let request = requestManager.apiRequest(urlString: url, method: .get, parameters: parameters)
            
        request.responseApi { response in
            switch response.result {
            case .success(let json):
                let list: [SummaryItem] = json["list"].array?.compactMap { SummaryItem(json: $0) } ?? []
                let pag = Pagination(json: json)
                completion(.success((list: list, pagination: pag)))
            case .failure:
                completion(.failure(response.result.picpayError ?? LegacyPJError.requestError))
            }
        }
    }
}
