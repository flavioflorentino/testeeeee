import Alamofire
import SwiftyJSON

public typealias PercentageFee = (rawValue: Double, value: String)

public final class ApiCommon: Api { }

public extension ApiCommon {
    /// Get the bank list
    ///
    /// - parameter data: image data
    /// - parameter closure: callback.
    func bankList(completion: @escaping(Swift.Result<[Bank], LegacyPJError>) -> Void) {
        requestManager.apiPublicRequest(urlString: Endpoint.bankList(), method: .get).responseApi { response in
            switch response.result {
            case .success(let json):
                let bankList: [Bank] = json["list"].array?.compactMap { Bank(json: $0) } ?? []
                completion(.success(bankList))
            case .failure:
                completion(.failure(response.result.picpayError ?? LegacyPJError.requestError))
            }
        }
    }
    
    /// Get the address from cep
    ///
    /// - parameter cep: cep
    /// - parameter closure: callback.
    func addressByCEP(cep: String, _ completion: @escaping((_ address: Address?, _ error: LegacyPJError?) -> Void) ) {
        let p: Parameters = ["cep": cep.onlyNumbers]
        
        requestManager.apiPublicRequest(urlString: Endpoint.verifyCep(), method: .post, parameters: p)
            .responseApi { response in
                switch response.result {
                case .success(let json):
                    
                    if let address = Address(json: json) {
                        completion(address, nil)
                    }
                    
                    completion(nil, nil)
                case .failure:
                    completion(nil, response.result.picpayError)
                }
            }
    }
    
    /// Get the address from cep
    ///
    /// - parameter address: address
    /// - parameter closure: callback.
    func addressCoordinate(
        address: Address,
        _ completion: @escaping((_ lat: Double?, _ lon: Double?, _ error: LegacyPJError?) -> Void)
    ) {
        let p: Parameters = [
            "cep": address.cep ?? "",
            "address": address.address ?? "",
            "number": address.number ?? "",
            "district": address.district ?? "",
            "city": address.city ?? "",
            "state": address.state ?? ""
        ]
        
        requestManager.apiPublicRequest(urlString: Endpoint.coordinateForAddress(), method: .post, parameters: p)
            .responseApi { response in
                switch response.result {
                case .success(let json):
                    
                    var lat: Double = 0.0
                    var lon: Double = 0.0
                    
                    if let lt = json["lat"].double {
                        lat = lt
                    }
                    if let ln = json["lon"].double {
                        lon = ln
                    }
                    
                    completion(lat, lon, nil)
                case .failure:
                    completion(nil, nil, response.result.picpayError)
                }
            }
    }
    
    /// Get the list of fee
    ///
    /// - parameter closure: callback.
    func feeList(_ completion: @escaping((Fee?, _ error: LegacyPJError?) -> Void) ) {
        requestManager.apiPublicRequest(urlString: Endpoint.sellerFees(), method: .get).responseApi { response in
            switch response.result {
            case .success(let json):
                let fee = self.parseFee(json: json)
                completion(fee, nil)
            case .failure:
                completion(nil, response.result.picpayError)
            }
        }
    }
}

private extension ApiCommon {
    func parseFee(json: JSON) -> Fee {
        var list = [String: PercentageFee]()
        var firstDay = ""
        var lastDay = ""
        var defaultDay = ""
        var daysToReleaseCc = 0
        var zeroFee = false
        
        // adjust the list of fee
        if let items = json["list"].array {
            for i in items {
                if
                    let dic = i.dictionary,
                    let key = dic["day"]?.string,
                    let value = dic["percentage_str"]?.string,
                    let rawValue = dic["percentage"]?.double {
                    list[key] = (rawValue, value)
                }
            }
            
            if let d = items.first?["day"] {
                firstDay = "\(d)"
            }
            
            if let d = items.last?["day"] {
                lastDay = "\(d)"
            }
        }
        
        if let d = json["default_day"].int {
            defaultDay = "\(d)"
        }
        
        if let d = json["days_to_release_cc"].int {
            daysToReleaseCc = d
        }
        
        if let isZeroFeeActivate = json["zero_fee"].bool {
            zeroFee = isZeroFeeActivate
        }
        
        return Fee(
            firstDay: firstDay,
            lastDay: lastDay,
            defaultDay: defaultDay,
            items: list,
            daysToReleaseCc: daysToReleaseCc,
            zeroFee: zeroFee
        )
    }
}
