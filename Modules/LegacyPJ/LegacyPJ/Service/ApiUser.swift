import Alamofire
import Foundation

public final class ApiUser: Api { }

// MARK: - Public Methods

public extension ApiUser {
    /// retrive the user data
    /// - parameter lastId: last loaded id
    func data(_ completion: @escaping((_ user: User?, _ error: LegacyPJError?) -> Void) ) {
        requestManager.apiRequest(urlString: Endpoint.userAuthenticated(), method: .get)
            .responseApi { response in
                switch response.result {
                case .success(let json):
                    
                    let user = User(json: json)
                    
                    completion(user, nil)
                    return
                case .failure:
                    completion(nil, response.result.picpayError)
                    return
                }
            }
    }
    
    /// Update the user data
    /// - parameter user: uesr to update
    func update(pin: String, user: User, _ completion: @escaping((_ error: LegacyPJError?) -> Void) ) {
        var p = user.toDictionary()
        p["user_seller_id"] = user.id ?? 0
        
        requestManager.apiRequest(urlString: Endpoint.updateUser(), method: .put, parameters: p, pin: pin )
            .responseApi { response in
                switch response.result {
                case .success:
                    completion(nil)
                case .failure:
                    completion(response.result.picpayError)
                }
            }
    }
    
    /// Change the user password
    /// - parameter currentPassword: the current password
    /// - parameter password: the new password
    func changePassword(_ currentPassword: String, password: String, id: Int, _ completion: @escaping((_ error: LegacyPJError?) -> Void) ) {
        let p: Parameters = [
            "user_seller_id": id,
            "password": password
        ]
        
        requestManager.apiRequest(urlString: Endpoint.userChangePassword(), method: .put, parameters: p, pin: currentPassword)
            .responseApi { response in
                switch response.result {
                case .success:
                    
                    completion(nil)
                    
                    return
                case .failure:
                    completion(response.result.picpayError)
                    return
                }
            }
    }
    
    /// Update notification token
    /// - parameter user: uesr to update
    func updateNotificationToken(_ token: String, _ completion: @escaping ((_ error: LegacyPJError?) -> Void) ) {
        let p = ["token": token]
        
        requestManager.apiRequest(urlString: Endpoint.userUpdateToken(), method: .post, parameters: p)
            .responseApi { response in
                switch response.result {
                case .success:
                    completion(nil)
                    return
                case .failure:
                    completion(response.result.picpayError)
                    return
                }
            }
    }
    
    /// Update the mixpanel id
    /// - parameter distinctId: distinct id
    func updateMixpanelId(_ distinctId: String, _ completion: @escaping ((_ error: LegacyPJError?) -> Void) ) {
        let p = ["distinct_id": distinctId]
        
        requestManager.apiRequest(urlString: Endpoint.userSellerDistinctId(), method: .post, parameters: p)
            .responseApi { response in
                switch response.result {
                case .success:
                    completion(nil)
                    return
                case .failure:
                    completion(response.result.picpayError)
                    return
                }
            }
    }
}
