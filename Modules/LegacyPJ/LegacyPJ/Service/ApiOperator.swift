import UIKit

public final class ApiOperator: Api { }

// MARK: - Public Methods
public extension ApiOperator {
    ///
    /// - parameter pin: User password
    func cashierBallance(pin: String, _ completion: @escaping((_ historyItem: HistoryItem?, _ error: LegacyPJError?) -> Void) ) {
        requestManager.apiRequest(urlString: Endpoint.operatorCashierBallance(), method: .post, parameters: nil, pin: pin)
            .responseApi { response in
                switch response.result {
                case .success(let json):
                    if let item = HistoryItem(json: json) {
                        completion(item, nil)
                    } else {
                        completion(nil, LegacyPJError(message: Strings.Errors.errorDecodeObject))
                    }
                    
                case .failure:
                    completion(nil, response.result.picpayError)
                }
            }
    }
}
