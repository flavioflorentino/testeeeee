import Alamofire
import Foundation
import SwiftyJSON

public struct MovementItemListResponse {
    public var header: TableHeader?
    
    public var error: LegacyPJError?
    public var list: [MovementItem] = []
    public var hasNext: Bool = false

    public init(error: LegacyPJError?) {
        self.error = error
    }
    
    public init(list: [MovementItem], hasNext: Bool, tableHeader: TableHeader?) {
        self.list = list
        self.hasNext = hasNext
        self.header = tableHeader
    }
}

public final class ApiMovement: Api { }

// MARK: - Public Methods
public extension ApiMovement {
    /// Retrive the list of movements
    /// - parameter lastId: last loaded id
    func list(_ lastId: Int?, _ completion: @escaping((_ response: MovementItemListResponse) -> Void) ) {
        var p: Parameters = [:]
        if let id = lastId {
            p["last_id"] = id
        }
        
        self.movementList(url: Endpoint.movementList(), parameters: p, completion)
    }
    
    /// Retrive the list of future movements
    /// - parameter date: date on format dd-mm-yyyy or mm-yyyy
    /// - parameter lastId: last loaded id
    func listFutureMovement(_ lastId: Int?, _ completion: @escaping((_ reponse: MovementItemListResponse) -> Void) ) {
        var p: Parameters = [:]
        if let id = lastId {
            p["last_id"] = id
        }
        
        self.movementList(url: Endpoint.movementFutureList(), parameters: p, completion)
    }
    
    /// Retrive the list of movement
    private func movementList(
        url: String,
        parameters: Parameters,
        _ completion: @escaping((_ reponse: MovementItemListResponse) -> Void)
    ) {
        requestManager.apiRequest(urlString: url, method: .get, parameters: parameters)
            .responseApi { response in
                switch response.result {
                case .success(let json):
                    var list: [MovementItem] = []
                    var hasNext: Bool = false
                    
                    if let items = json["list"].array {
                        for item in items {
                            if let movment = MovementItem(json: item) {
                                list.append(movment)
                            }
                        }
                    }
                    
                    if let hasMore = json["next"].bool {
                        hasNext = hasMore
                    }
                    
                    var header: TableHeader?
                    if json["header"].exists(), let h = TableHeader(dict: json["header"]) {
                        header = h
                    }
                    
                    completion(MovementItemListResponse(list: list, hasNext: hasNext, tableHeader: header))
                    return
                case .failure:
                    completion(MovementItemListResponse(error: response.result.picpayError))
                    return
                }
            }
    }
    
    /// Export movement and send it to the user's email
    /// - parameter from: initial date with format yyy-MM-dd
    /// - parameter to: final date with format yyy-MM-dd
    /// - parameter type: the file format PDF or CSV
    func exportMovement(from: String, to: String, type: String, _ completion: @escaping((_ email: String?, _ error: LegacyPJError?) -> Void) ) {
        let p: Parameters = [
            "from": from,
            "to": to,
            "type": type
        ]
        
        requestManager.apiRequest(urlString: Endpoint.movementExportation(), method: .post, parameters: p)
            .responseApi { response in
                switch response.result {
                case .success(let json):
                    let email: String? = json["email"].string
                    completion(email, nil)
                    return
                case .failure:
                    completion(nil, response.result.picpayError)
                    return
                }
            }
    }
}
