import Core
import CoreSellerAccount
import Foundation
import SwiftyJSON

public protocol AuthManagerTrackingWrapperProtocol {
    func userDidLogin(_ userId: String)
    func createAliasForUser(_ userId: String)
    func trackLogout()
    func updateDistinticId()
}

public protocol HasAuthManager {
    var authManager: AuthManagerProtocol { get }
}

public enum KeychainKeyLegacyPJ: String, KeychainKeyable {
    case apiAuthIndex = "api_auth_index"
    case apiAuth = "api_auth"
    case userRegister = "user_register"
    case signUpRegister = "singUp_register"
}

public protocol AuthManagerProtocol: AnyObject {
    var user: User? { get }
    var userAuth: UserAuth? { get }
    var authenticatedUsers: [UserAuth] { get }
    var isAuthenticated: Bool { get }
    var trackingWrapper: AuthManagerTrackingWrapperProtocol? { get set }
    
    func performActionWithAuthorization(
        _ message: String?,
        _ action: @escaping ((_ result: AuthManager.PerformActionResult) -> Void)
    )
    func updateUser(auth: Auth, user: User)
    func storeUserAuth(userAuth: UserAuth)
    func storeUserRegister(model: Partner)
    func storeSignUpRegister(data: Data)
    func removeUserAuth(username: String?)
    func removeUserAuth(userAuth: UserAuth)
    func authenticateNewAccount(_ auth: Auth, completion: @escaping((_ error: LegacyPJError?) -> Void ))
    func refreshAccount(_ auth: Auth, completion: @escaping((_ error: LegacyPJError?) -> Void ))
    func changeCurrentAccount(userAuth: UserAuth, completion: @escaping ( (_ error: LegacyPJError?) -> Void ))
    func login(cnpj: String, username: String, password: String, completion: @escaping ((_ error: LegacyPJError?) -> Void))
    func logout(userAuth: UserAuth?, changeAccount: Bool, completion: @escaping((_ error: LegacyPJError?) -> Void))
}

public extension AuthManagerProtocol {
    func performActionWithAuthorization(
       _ message: String? = nil,
       _ action: @escaping ((_ result: AuthManager.PerformActionResult) -> Void)
    ) {
        performActionWithAuthorization(message, action)
    }
}

public final class AuthManager: AuthManagerProtocol {
    public enum PerformActionResult {
        case canceled
        case success(String)
        case failure(LegacyPJError)
    }
    
    // MARK: - Public Properties
    
    public static let shared = AuthManager()
    
    public var user: User? {
        userAuth?.user
    }
    
    public var userAuth: UserAuth? {
        didSet {
            guard let accessToken = userAuth?.auth.accessToken else { return }
            keychain.set(key: KeychainKey.token, value: accessToken)
            container.sellerInfo.resetAccountDetails()
        }
    }
    
    public var authenticatedUsers: [UserAuth]
    
    public var isAuthenticated: Bool { userAuth != nil }
    
    public var trackingWrapper: AuthManagerTrackingWrapperProtocol?
    
    // MARK: - Private Properties
    
    private let container: HasKeychainManager & HasSellerInfo = DependencyContainer()
    private lazy var keychain = container.keychain
    
    private var topController: UIViewController? {
        guard let window = UIApplication.shared.keyWindow, let rootViewController = window.rootViewController else { return nil }

        var topController = rootViewController

        while let newTopController = topController.presentedViewController {
            topController = newTopController
        }
        return topController
    }
    
    // MARK: - Initializer
    
    private init() {
        authenticatedUsers = []
        loadAuth()
    }
}

// MARK: - Public Methods

public extension AuthManager {
    func login(cnpj: String, username: String, password: String, completion: @escaping ((_ error: LegacyPJError?) -> Void)) {
        let apiAuth = ApiAuth()
        apiAuth.auth(cnpj: cnpj, username: username, password: password) { [weak self] auth, error in
            guard let self = self else { return }
            if let auth = auth {
                self.authenticateUser(auth, completion: completion)
                self.trackingWrapper?.updateDistinticId()
            } else {
                completion(error)
            }
        }
    }
    
    func updateUser(auth: Auth, user: User) {
        let authUser = UserAuth(auth: auth, user: user)
        userAuth = authUser
        storeUserAuth(userAuth: authUser)
    }
    
    /// Ask the user the password to authenticate then call de action
    func performActionWithAuthorization(
        _ message: String? = nil,
        _ action: @escaping ((_ result: PerformActionResult) -> Void)
    ) {
        let msg = message ?? Strings.Auth.alertMsg
        var passswordTextField: UITextField?
        
        let alert = UIAlertController(title: Strings.Auth.alertTitle, message: msg, preferredStyle: .alert)
        alert.addTextField { textField in
            textField.placeholder = Strings.Auth.alertPassword
            textField.isSecureTextEntry = true
            textField.keyboardType = .default
            passswordTextField = textField
        }
        
        let cancelAction = UIAlertAction(title: Strings.Default.cancel, style: .cancel) { _ in
            action(.canceled)
        }
        
        let confirmAction = UIAlertAction(title: Strings.Default.ok, style: .default) { alertAction in
            alertAction.isEnabled = false
            if let password = alert.textFields?.first?.text {
                action(.success(password))
            } else {
                action(.failure(LegacyPJError(message: Strings.Auth.errorWrongPassword)) )
            }
        }
        
        alert.addAction(cancelAction)
        alert.addAction(confirmAction)
        
        topController?.present(alert, animated: true) {
            passswordTextField?.becomeFirstResponder()
        }
    }
    
    func logout(
        userAuth: UserAuth? = nil,
        changeAccount: Bool = true,
        completion: @escaping((_ error: LegacyPJError?) -> Void)
    ) {
        if let userAuth = userAuth {
            removeUserAuth(userAuth: userAuth)
        } else if let userAuth = self.userAuth {
            removeUserAuth(userAuth: userAuth)
        }

        self.userAuth = nil

        // change to another account
        if let firstUserAuth = authenticatedUsers.first, changeAccount == true {
            authenticateUser(firstUserAuth.auth, completion: completion)
        } else {
            trackingWrapper?.trackLogout()
            completion(nil)
        }
    }
    
    func removeUserAuth(username: String?) {
        authenticatedUsers.removeAll { $0.user.username == username }
        storeAuthenticatedUsers()
    }
    
    /// Remove the user from list
    func removeUserAuth(userAuth: UserAuth) {
        if let index = authenticatedUsers.firstIndex(of: userAuth) {
            authenticatedUsers.remove(at: index)
            storeAuthenticatedUsers()
        }
    }
    
    func storeUserRegister(model: Partner) {
        if let authDic = try? JSON(data: model.toDictionary().toData() ?? Data(), options: .allowFragments).rawString() {
            keychain.set(key: KeychainKeyLegacyPJ.userRegister, value: authDic)
        }
    }
    
    func storeSignUpRegister(data: Data) {
        if let authDic = try? JSON(data: data, options: .allowFragments).rawString() {
            keychain.set(key: KeychainKeyLegacyPJ.signUpRegister, value: authDic)
        }
    }

    /// Store auth on keychain
    func storeUserAuth(userAuth: UserAuth) {
        self.userAuth = userAuth
        if !authenticatedUsers.contains(userAuth) {
            authenticatedUsers.append(userAuth)
            storeAuthenticatedUsers()
        } else if let index = authenticatedUsers.firstIndex(of: userAuth) {
            authenticatedUsers.remove(at: index)
            authenticatedUsers.append(userAuth)
            storeAuthenticatedUsers()
        }

        if let index = authenticatedUsers.firstIndex(of: userAuth) {
            keychain.set(key: KeychainKeyLegacyPJ.apiAuthIndex, value: String(index))
        }
    }
    
    /// Make the authentication of new accont
    func authenticateNewAccount(_ auth: Auth, completion: @escaping((_ error: LegacyPJError?) -> Void )) {
        authenticateUser(auth) { [weak self] error in
            if let userAuth = self?.userAuth, let userId = userAuth.user.id {
                self?.trackingWrapper?.createAliasForUser(String(format: "%d", userId))
            }
            completion(error)
        }
    }

    func refreshAccount(_ auth: Auth, completion: @escaping((_ error: LegacyPJError?) -> Void )) {
        authenticateUser(auth, completion: completion)
    }

    /// Change the current user
    func changeCurrentAccount(userAuth: UserAuth, completion: @escaping ( (_ error: LegacyPJError?) -> Void )) {
        if authenticatedUsers.contains(userAuth) {
            authenticateUser(userAuth.auth, completion: completion)
        }
    }
}

// MARK: - Private Methods

private extension AuthManager {
    /// Store the list of authenticated users
    func storeAuthenticatedUsers() {
        var jsonList: [JSON] = []
        for item in authenticatedUsers {
            jsonList.append(item.toJSON())
        }
        
        if let authJson = JSON(jsonList).rawString() {
            keychain.set(key: KeychainKeyLegacyPJ.apiAuth, value: authJson)
        }
    }
    
    /// Load the stored auth data
    func loadAuth() {
        /// try to load authenticated users list
        if
            let jsonString = keychain.getData(key: KeychainKeyLegacyPJ.apiAuth),
            let dataFromString = jsonString.data(using: .utf8, allowLossyConversion: false),
            let json = try? JSON(data: dataFromString)
        {
            var list: [UserAuth] = []
            if let usersList = json.array {
                for item in usersList {
                    guard let userAuth = UserAuth(json: item) else { continue }
                    list.append(userAuth)
                }
            }
            authenticatedUsers = list
        }

        /// Load current user
        if
            let apiAuthIndex = keychain.getData(key: KeychainKeyLegacyPJ.apiAuthIndex),
            let index = Int(apiAuthIndex),
            index < authenticatedUsers.count
        {
            userAuth = authenticatedUsers[index]
        }

        // fallback load the first authenticated user
        if userAuth == nil && authenticatedUsers.first != nil {
            userAuth = authenticatedUsers.first
        }
    }
    
    func authenticateUser(_ auth: Auth, completion: ( (_ error: LegacyPJError?) -> Void )?) {
        // Load user data
        let apiUser = ApiUser()
        userAuth = UserAuth(auth: auth, user: User())
        apiUser.data { [weak self] user, error in
            if let user = user {
                let authUser = UserAuth(auth: auth, user: user)
                self?.userAuth = authUser
                self?.storeUserAuth(userAuth: authUser)

                if let id = user.id {
                    self?.trackingWrapper?.userDidLogin(String(format: "%d", id))
                }
            }
            DispatchQueue.main.async {
                completion?(error)
            }
        }
    }
}
