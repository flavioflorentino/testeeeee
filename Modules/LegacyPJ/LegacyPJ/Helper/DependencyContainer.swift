import Core
import CoreSellerAccount
import FeatureFlag
import Foundation

typealias AppDependencies =
    HasKeychainManager &
    HasFeatureManager &
    HasLocationManager &
    HasAuthManager &
    HasKeychainManager &
    HasKeychainManagerPersisted &
    HasSellerInfo

final class DependencyContainer: AppDependencies {
    lazy var keychainWithPersistent: KeychainManagerContract = KeychainManager(isPersistent: true)
    lazy var keychain: KeychainManagerContract = KeychainManager()
    lazy var featureManager: FeatureManagerContract = FeatureManager.shared
    lazy var locationManager: LocationManagerContract = LocationManager.shared
    lazy var authManager: AuthManagerProtocol = AuthManager.shared
    lazy var sellerInfo: SellerInfoManagerProtocol = SellerInfoManager.shared
}
