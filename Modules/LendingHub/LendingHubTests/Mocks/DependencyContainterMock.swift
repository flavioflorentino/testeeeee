import AnalyticsModule
import Core
import FeatureFlag
import XCTest
@testable import LendingHub

/// Don't use this class for testing, use LoanDependencyContainerMock
final class LendingHubDependencyContainer {
    init() {
        fatalError("Use a LoanDependencyContainerMock on test target")
    }
}

final class LendingHubDependencyContainerMock: AppDependencies {
    lazy var deeplink: DeeplinkContract? = resolve()
    lazy var mainQueue: DispatchQueue = resolve()
    lazy var analytics: AnalyticsProtocol = resolve()
    lazy var featureManager: FeatureManagerContract = resolve()

    private let dependencies: [Any]

    init(_ dependencies: Any...) {
        self.dependencies = dependencies
    }
}

extension LendingHubDependencyContainerMock {
    func resolve<T>() -> T {
        let resolved = dependencies.compactMap { $0 as? T }

        switch resolved.first {
        case .none:
            fatalError("LendingHubDependencyContainerMock could not resolve dependency: \(T.self)\n")
        case .some where resolved.count > 1:
            fatalError("LendingHubDependencyContainerMock resolved mutiple dependencies for: \(T.self)\n")
        case .some(let mock):
            return mock
        }
    }

    func resolve() -> DispatchQueue {
        dependencies.compactMap { $0 as? DispatchQueue }.first ?? DispatchQueue(label: "LendingHubDependencyContainerMock")
    }
}
