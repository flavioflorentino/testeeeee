import XCTest
import Core
import AnalyticsModule
import FeatureFlag
@testable import LendingHub

private final class LendingHubPresenterSpy: LendingHubPresenting {
    var viewController: LendingHubDisplaying?
    private(set) var presentItemsCallCount = 0
    private(set) var didNextStepCallCount = 0
    private(set) var presentErrorCallCount = 0
    private(set) var performedAction: LendingHubAction?
    private(set) var presentedItems = [LendingHubItem]()

    func present(items: [LendingHubItem]) {
        presentItemsCallCount += 1
        presentedItems = items
    }

    func didNextStep(action: LendingHubAction) {
        didNextStepCallCount += 1
        performedAction = action
    }

    func presentError() {
        presentErrorCallCount += 1
    }
}

final class LendingHubInteractorTests: XCTestCase {
    private let presenterSpy = LendingHubPresenterSpy()
    private let analyticsMock = AnalyticsSpy()
    private lazy var dependencies = LendingHubDependencyContainerMock(
        analyticsMock,
        featureManagerMock
    )

    private let featureManagerMock: FeatureManagerMock = {
        let featureManagerMock = FeatureManagerMock()
        featureManagerMock.override(keys: .releaseP2PLending, with: false)
        return featureManagerMock
    }()

    private lazy var bannerSpy = LoanWalletBanner(type: .hasLoan, hasP2POffer: true)

    private lazy var sut: LendingHubInteractor = {
        LendingHubInteractor(banner: bannerSpy,
                             presenter: presenterSpy,
                             dependencies: dependencies)
    }()

    func testFetchHubItems_whenHasLoan_ShouldPresentItems() {
        bannerSpy = LoanWalletBanner(type: .hasLoan, hasP2POffer: false)

        sut.fetchHubItems()
        XCTAssertEqual(presenterSpy.presentItemsCallCount, 1)
        XCTAssertTrue(presenterSpy.presentedItems.contains(.hasLoan))
        XCTAssertFalse(presenterSpy.presentedItems.contains(.hasOffer))
        XCTAssertFalse(presenterSpy.presentedItems.contains(.p2pLending))
    }

    func testFetchHubItems_whenHasOffer_ShouldPresentItems() {
        bannerSpy = LoanWalletBanner(type: .hasOffer, hasP2POffer: false)

        sut.fetchHubItems()
        XCTAssertEqual(presenterSpy.presentItemsCallCount, 1)
        XCTAssertFalse(presenterSpy.presentedItems.contains(.hasLoan))
        XCTAssertTrue(presenterSpy.presentedItems.contains(.hasOffer))
        XCTAssertFalse(presenterSpy.presentedItems.contains(.p2pLending))
    }

    func testFetchHubItems_whenHasP2POffer_ShouldPresentItems() {
        bannerSpy = LoanWalletBanner(type: .noOffer, hasP2POffer: true)
        featureManagerMock.override(keys: .releaseP2PLending, with: true)

        sut.fetchHubItems()
        XCTAssertEqual(presenterSpy.presentItemsCallCount, 1)
        XCTAssertFalse(presenterSpy.presentedItems.contains(.hasLoan))
        XCTAssertFalse(presenterSpy.presentedItems.contains(.hasOffer))
        XCTAssertTrue(presenterSpy.presentedItems.contains(.p2pLending))
    }

    func testShowItem_ShouldPresentNextStep() {
        sut.lendingItems = [.hasLoan]
        sut.showItem(at: 0)

        XCTAssertEqual(presenterSpy.didNextStepCallCount, 1)
        XCTAssertEqual(presenterSpy.performedAction, .show(item: .hasLoan))
    }
}
