import XCTest
import Core
import LendingComponents
@testable import LendingHub

private final class LendingHubCoordinatorSpy: LendingHubCoordinating {
    var viewController: UIViewController?
    private(set) var performActionCallCount = 0
    private(set) var performedAction: LendingHubAction?

    func perform(action: LendingHubAction) {
        performActionCallCount += 1
        performedAction = action
    }
}

private final class LendingHubControllerSpy: LendingHubDisplaying {
    private(set) var displayItemsCallCount = 0

    func display(items: [LendingProductCellViewModeling]) {
        displayItemsCallCount += 1
    }
}

final class LendingHubPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = LendingHubCoordinatorSpy()
    private let controllerSpy = LendingHubControllerSpy()

    private lazy var sut: LendingHubPresenter = {
        let sut = LendingHubPresenter(coordinator: coordinatorSpy)
        sut.viewController = controllerSpy
        return sut
    }()

    func testPresentItems_ShouldDisplayItems() {
        sut.present(items: LendingHubItem.allCases)
        XCTAssertEqual(controllerSpy.displayItemsCallCount, 1)
    }

    func testDidNextStep_ShouldPerformAction() {
        sut.didNextStep(action: .show(item: .hasLoan))
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .show(item: .hasLoan))
    }
}
