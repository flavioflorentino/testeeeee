import XCTest
import Core
@testable import LendingHub

private final class DeeplinkWrapperSpy: DeeplinkContract {
    private(set) var callOpenDeeplinkCount = 0

    func open(url: URL) -> Bool {
        callOpenDeeplinkCount += 1
        return true
    }
}

private final class LendingFlowDelegateSpy: LendingFlowDelegate {
    private(set) var startLoanFlowCallCount = 0
    private(set) var startLoanOfferFlowCallCount = 0
    private(set) var startP2PFlowCallCount = 0
    private(set) var finishCallCount = 0

    func startLoanFlow() {
        startLoanFlowCallCount += 1
    }

    func startLoanOfferFlow() {
        startLoanOfferFlowCallCount += 1
    }

    func startP2PLendingFlow() {
        startP2PFlowCallCount += 1
    }

    func finish() {
        finishCallCount += 1
    }
}

final class LendingHubCoordinatorTest: XCTestCase {
    private lazy var delegateSpy = LendingFlowDelegateSpy()
    private lazy var deeplinkSpy = DeeplinkWrapperSpy()
    lazy var sut = LendingHubCoordinator(dependencies: LendingHubDependencyContainerMock(deeplinkSpy),
                                         delegate: delegateSpy)

    func test_performShowItem_whenHasLoan_ShouldStartLoanFlow() {
        sut.perform(action: .show(item: .hasLoan))

        XCTAssertEqual(delegateSpy.startLoanFlowCallCount, 1)
        XCTAssertEqual(delegateSpy.startP2PFlowCallCount, 0)
        XCTAssertEqual(delegateSpy.startLoanOfferFlowCallCount, 0)
    }

    func test_performShowItem_whenHasOffer_ShouldStartLoanOfferFlow() {
        sut.perform(action: .show(item: .hasOffer))

        XCTAssertEqual(delegateSpy.startLoanOfferFlowCallCount, 1)
        XCTAssertEqual(delegateSpy.startLoanFlowCallCount, 0)
        XCTAssertEqual(delegateSpy.startP2PFlowCallCount, 0)
    }

    func test_performShowItem_whenP2pLending_ShouldStartP2PFlow() {
        sut.perform(action: .show(item: .p2pLending))

        XCTAssertEqual(delegateSpy.startP2PFlowCallCount, 1)
        XCTAssertEqual(delegateSpy.startLoanOfferFlowCallCount, 0)
        XCTAssertEqual(delegateSpy.startLoanFlowCallCount, 0)
    }

    func test_performShowFAQ_ShouldOpenFAQArticle() {
        let urlString = "picpay://picpay/helpcenter/category/360003819012"
        sut.perform(action: .showFAQArticle(url: urlString))

        XCTAssertEqual(deeplinkSpy.callOpenDeeplinkCount, 1)
        XCTAssertEqual(delegateSpy.startP2PFlowCallCount, 0)
        XCTAssertEqual(delegateSpy.startLoanOfferFlowCallCount, 0)
        XCTAssertEqual(delegateSpy.startLoanFlowCallCount, 0)
    }
}
