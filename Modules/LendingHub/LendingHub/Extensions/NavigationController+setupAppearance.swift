import UI
import UIKit

extension UINavigationController {
    func setupAppearance() {
        let bar = navigationBar
        navigationBar.backgroundColor = Colors.backgroundPrimary.color
        bar.isTranslucent = true

        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.backgroundColor = Colors.backgroundPrimary.color
            appearance.shadowColor = .clear
            appearance.titleTextAttributes = [.foregroundColor: Colors.black.color]
            appearance.largeTitleTextAttributes = [.foregroundColor: Colors.black.color, .font: Typography.title(.xLarge).font()]
            bar.prefersLargeTitles = false
            bar.tintColor = Colors.brandingBase.color
            bar.setBackgroundImage(nil, for: .default)
            bar.standardAppearance = appearance
            bar.compactAppearance = appearance
            bar.scrollEdgeAppearance = appearance
            navigationItem.largeTitleDisplayMode = .never
        } else {
            bar.tintColor = Colors.brandingBase.color
            bar.barTintColor = Colors.black.color
            bar.setBackgroundImage(UIImage(), for: .default)
            bar.shadowImage = UIImage()
        }

        let backButton = UIBarButtonItem()
        backButton.title = String()
        bar.topItem?.backBarButtonItem = backButton
    }
}
