import Core

public protocol LendingHubSetupContract: AnyObject {
    var deeplink: DeeplinkContract? { get set }
}

public final class LendingHubSetup: LendingHubSetupContract {
    public static let shared = LendingHubSetup()
    public var deeplink: DeeplinkContract?

    private init() {}

    public func inject(instance: Any) {
        switch instance {
        case let instance as DeeplinkContract:
            deeplink = instance
        default:
            assertionFailure("🚨\nAttempted to inject unexpected depedency: \(instance)\n")
        }
    }
}
