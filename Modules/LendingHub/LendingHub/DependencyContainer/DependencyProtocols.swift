import Foundation
import Core

protocol HasNoDependency {}

protocol HasDeeplink {
    var deeplink: DeeplinkContract? { get }
}
