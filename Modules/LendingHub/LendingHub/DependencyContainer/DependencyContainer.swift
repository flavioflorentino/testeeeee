import AnalyticsModule
import Core
import FeatureFlag
import Foundation

typealias AppDependencies =
    HasNoDependency &
    HasMainQueue &
    HasAnalytics &
    HasFeatureManager &
    HasDeeplink

final class DependencyContainer: AppDependencies {
    lazy var mainQueue: DispatchQueue = .main
    lazy var analytics: AnalyticsProtocol = Analytics.shared
    lazy var featureManager: FeatureManagerContract = FeatureManager.shared
    lazy var deeplink: DeeplinkContract? = LendingHubSetup.shared.deeplink
}
