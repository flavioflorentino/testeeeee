import UI
import UIKit
import AssetsKit
import LendingComponents

protocol LendingHubDisplaying: AnyObject {
    func display(items: [LendingProductCellViewModeling])
}

private extension LendingHubViewController.Layout {
    enum Height {
        static let estimatedRowHeight: CGFloat = 97
        static let estimatedSectionHeaderHeight: CGFloat = 242
    }
}

final class LendingHubViewController: ViewController<LendingHubInteracting, UIView> {
    private typealias Localizable = Strings.LendingHub.Home
    fileprivate enum Layout { }

    private enum Section {
        case main
    }

    let cellIdentifier = String(describing: LendingProductTableViewCell.self)
    let sectionHeaderIdentifier = String(describing: LendingHeaderView.self)

    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.register(LendingProductTableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        tableView.register(LendingHeaderView.self, forHeaderFooterViewReuseIdentifier: sectionHeaderIdentifier)
        tableView.backgroundColor = Colors.backgroundPrimary.color
        tableView.estimatedRowHeight = Layout.Height.estimatedRowHeight
        tableView.estimatedSectionHeaderHeight = Layout.Height.estimatedSectionHeaderHeight
        tableView.rowHeight = UITableView.automaticDimension
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.delegate = self
        return tableView
    }()

    private lazy var tableViewDataSource: TableViewDataSource<Section, LendingProductCellViewModeling> = {
        let dataSource = TableViewDataSource<Section, LendingProductCellViewModeling>(view: tableView) { tableView, indexPath, item -> UITableViewCell? in
            let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier, for: indexPath) as? LendingProductTableViewCell
            cell?.setup(for: item)
            return cell
        }
        dataSource.add(section: .main)
        return dataSource
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.fetchHubItems()
    }

    override func buildViewHierarchy() {
        view.addSubview(tableView)
    }
    
    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }

    override func configureViews() {
        tableView.dataSource = tableViewDataSource
        setUpLoanNavigationAppearance()
    }

    private func setUpLoanNavigationAppearance() {
        guard let navigation = navigationController else {
            return
        }
        navigation.setupAppearance()

        let infoButton = UIBarButtonItem(image: Resources.Icons.icoQuestion.image,
                                         style: .plain,
                                         target: self,
                                         action: #selector(didTapInfo))
        navigationItem.rightBarButtonItem = infoButton
    }
}

@objc
private extension LendingHubViewController {
    func didTapInfo() {
        interactor.openFAQ()
    }
}

extension LendingHubViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        interactor.showItem(at: indexPath.row)
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: sectionHeaderIdentifier) as? LendingHeaderView
        let content = LendingHeaderViewContent(image: Resources.Illustrations.iluGirlCoinCircledBackground.image,
                                               title: Localizable.headline,
                                               description: Localizable.subhead)
        header?.configureViews(content: content)
        return header
    }
}

// MARK: - LendingHubDisplaying
extension LendingHubViewController: LendingHubDisplaying {
    func display(items: [LendingProductCellViewModeling]) {
        tableViewDataSource.update(items: items, from: .main)
    }
}
