import Foundation
import UIKit

enum LendingHubAction: Equatable {
    case show(item: LendingHubItem)
    case showFAQArticle(url: String)
    case finish
}

protocol LendingHubCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: LendingHubAction)
}

final class LendingHubCoordinator {
    typealias Dependencies = HasDeeplink
    private let dependencies: Dependencies
    weak var viewController: UIViewController?
    weak var delegate: LendingFlowDelegate?

    init(dependencies: Dependencies, delegate: LendingFlowDelegate) {
        self.delegate = delegate
        self.dependencies = dependencies
    }
}

// MARK: - LendingHubCoordinating
extension LendingHubCoordinator: LendingHubCoordinating {
    func perform(action: LendingHubAction) {
        switch action {
        case let .show(item):
            show(item: item)
        case let .showFAQArticle(urlString):
            guard let url = URL(string: urlString) else { return }
            dependencies.deeplink?.open(url: url)
        case .finish:
            delegate?.finish()
        }
    }

    private func show(item: LendingHubItem) {
        switch item {
        case .hasLoan:
            delegate?.startLoanFlow()
        case .hasOffer:
            delegate?.startLoanOfferFlow()
        case .p2pLending:
            delegate?.startP2PLendingFlow()
        }
    }
}
