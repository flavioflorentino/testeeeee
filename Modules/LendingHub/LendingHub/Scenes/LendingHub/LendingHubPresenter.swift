import Foundation
import Core
import AssetsKit
import UI

protocol LendingHubPresenting: AnyObject {
    var viewController: LendingHubDisplaying? { get set }
    func present(items: [LendingHubItem])
    func didNextStep(action: LendingHubAction)
}

final class LendingHubPresenter {
    private let coordinator: LendingHubCoordinating
    weak var viewController: LendingHubDisplaying?

    init(coordinator: LendingHubCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - LendingHubPresenting
extension LendingHubPresenter: LendingHubPresenting {
    func present(items: [LendingHubItem]) {
        viewController?.display(items: items)
    }
    
    func didNextStep(action: LendingHubAction) {
        coordinator.perform(action: action)
    }
}
