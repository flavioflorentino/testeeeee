import UIKit
import LendingComponents
import AssetsKit

enum LendingHubItem: CaseIterable {
    case hasLoan
    case hasOffer
    case p2pLending
}

extension LendingHubItem: LendingProductCellViewModeling {
    private typealias Localizable = Strings.LendingHub.Home

    var title: String {
        switch self {
        case .hasLoan, .hasOffer:
            return Localizable.cpLendingItemTitle
        case .p2pLending:
            return Localizable.p2pLendingItemTitle
        }
    }

    var description: String {
        switch self {
        case .hasLoan:
            return Localizable.hasLoanDescription
        case .hasOffer:
            return Localizable.hasOfferDescription
        case .p2pLending:
            return Localizable.p2pLendingItemDescription
        }
    }

    var image: UIImage {
        switch self {
        case .hasLoan, .hasOffer:
            return Resources.Icons.icoMoneyStack.image
        case .p2pLending:
            return Resources.Icons.icoGreenUsers.image
        }
    }
}
