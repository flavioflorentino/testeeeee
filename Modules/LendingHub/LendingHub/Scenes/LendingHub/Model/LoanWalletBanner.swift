import Foundation

public enum LoanWalletBannerType: String, Decodable {
    case noOffer = "NO_OFFER" // Usuário sem oferta de empréstimo
    case hasOffer = "HAS_OFFER" // Usuário com oferta de empréstimo
    case hasLoan = "HAS_LOAN" // Usuário com empréstimo já contratado
}

public struct LoanWalletBanner: Decodable {
    public let type: LoanWalletBannerType
    public let hasP2POffer: Bool

    enum CodingKeys: String, CodingKey {
        case type = "loanWalletBanner"
        case hasP2POffer = "p2pLendingOffer"
    }
}
