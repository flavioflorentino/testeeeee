enum LendingHubFactory {
    static func make(with banner: LoanWalletBanner, delegate: LendingFlowDelegate) -> LendingHubViewController {
        let container = DependencyContainer()
        let coordinator: LendingHubCoordinating = LendingHubCoordinator(dependencies: container, delegate: delegate)
        let presenter: LendingHubPresenting = LendingHubPresenter(coordinator: coordinator)
        let interactor = LendingHubInteractor(banner: banner, presenter: presenter, dependencies: container)
        let viewController = LendingHubViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
