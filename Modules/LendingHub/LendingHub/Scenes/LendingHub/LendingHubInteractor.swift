import Foundation
import AnalyticsModule
import FeatureFlag

protocol LendingHubInteracting: AnyObject {
    func fetchHubItems()
    func openFAQ()
    func showItem(at index: Int)
    func close()
}

final class LendingHubInteractor {
    typealias Dependencies = HasAnalytics & HasFeatureManager
    private let dependencies: Dependencies

    private let banner: LoanWalletBanner
    private let presenter: LendingHubPresenting
    var lendingItems = [LendingHubItem]()

    init(banner: LoanWalletBanner,
         presenter: LendingHubPresenting,
         dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
        self.banner = banner
    }
}

// MARK: - LendingHubInteracting
extension LendingHubInteractor: LendingHubInteracting {
    func close() {
        presenter.didNextStep(action: .finish)
    }

    func fetchHubItems() {
        handleBannerType(banner)
    }

    func showItem(at index: Int) {
        presenter.didNextStep(action: .show(item: lendingItems[index]))
    }

    func openFAQ() {
        presenter.didNextStep(action: .showFAQArticle(url: FAQArticle.hub.rawValue))
    }
}

// MARK: - Private Methods
private extension LendingHubInteractor {
    private func handleBannerType(_ banner: LoanWalletBanner) {
        var items = [LendingHubItem]()

        switch banner.type {
        case .hasOffer:
            items.append(.hasOffer)
            log(event: .didSeeLendingOfferAccess)
        case .hasLoan:
            items.append(.hasLoan)
            log(event: .didSeeLendingMonitoringAccess)
        case .noOffer:
            break
        }

        if dependencies.featureManager.isActive(.releaseP2PLending)
            && banner.hasP2POffer {
            items.append(.p2pLending)
            log(event: .didSeeP2PLendingAccess)
        }

        lendingItems = items
        presenter.present(items: lendingItems)
    }

    private func log(event: LendingHubEvent) {
        self.dependencies.analytics.log(event)
    }
}
