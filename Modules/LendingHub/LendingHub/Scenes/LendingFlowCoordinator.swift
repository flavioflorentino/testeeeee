import UI
import Loan
import LoanOffer
import P2PLending

public protocol LendingFlowCoordinating: Coordinating {
    func start(didFinish: @escaping () -> Void)
    func close()
}

protocol LendingFlowDelegate: AnyObject {
    func startLoanFlow()
    func startLoanOfferFlow()
    func startP2PLendingFlow()
    func finish()
}

public final class LendingFlowCoordinator {
    public var childViewController: [UIViewController] = []
    public var viewController: UIViewController?
    private let navigationController: UINavigationController
    private let banner: LoanWalletBanner
    private var childCoordinators: [Coordinating] = []
    var didFinish: () -> Void = {}

    public init(with banner: LoanWalletBanner,
                navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.banner = banner
    }

    deinit {
        childCoordinators = []
    }
}

extension LendingFlowCoordinator: LendingFlowCoordinating {
    public func start(didFinish: @escaping () -> Void) {
        self.didFinish = didFinish
        let controller = LendingHubFactory.make(with: banner, delegate: self)
        controller.hidesBottomBarWhenPushed = true
        self.viewController = controller
        navigationController.pushViewController(controller, animated: true)
    }

    public func close() {
        navigationController.popViewController(animated: true)
        childCoordinators = []
        didFinish()
    }
}

extension LendingFlowCoordinator: LendingFlowDelegate {
    func startLoanFlow() {
        let coordinator = LoanCoordinator(with: navigationController, from: .hub)
        childCoordinators.append(coordinator)
        coordinator.start(flow: .loanAgreements) {
            self.childCoordinators = []
        }
    }

    func startLoanOfferFlow() {
        let coordinator = LoanOfferCoordinator(with: navigationController, from: .hub)
        childCoordinators.append(coordinator)
        coordinator.start(flow: .hire) {
            self.childCoordinators = []
        }
    }

    func startP2PLendingFlow() {
        let controller = LendingOnboardingFactory.make()
        navigationController.pushViewController(controller, animated: true)
    }

    func finish() {
        self.close()
    }
}
