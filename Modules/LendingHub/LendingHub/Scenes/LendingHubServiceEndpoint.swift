import Core

enum LoanHubServiceEndpoint {
    case loanWalletBanner
}

extension LoanHubServiceEndpoint: ApiEndpointExposable {
    var path: String {
         "personal-credit/offers/wallet-banner"
    }

    var method: HTTPMethod {
        .get
    }
}
