public enum LendingErrorStatus: Equatable {
    case hubAccess

    var value: String {
        switch self {
        case .hubAccess:
            return "Acesso Hub"
        }
    }
}
