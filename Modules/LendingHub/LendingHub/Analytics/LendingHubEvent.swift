import AnalyticsModule

public enum LendingHubEvent: AnalyticsKeyProtocol, Equatable {
    case didSeeHubAccess(origin: OriginType)
    case didEnterLendingHub
    case didSeeP2PLendingAccess
    case didSeeLendingOfferAccess
    case didSeeLendingMonitoringAccess
    case didReceiveError(error: LendingErrorStatus)

    private var name: String {
        switch self {
        case .didSeeHubAccess:
            return "Saw Credit Hub Access"
        case .didSeeP2PLendingAccess:
            return "Saw Hub P2P Lending Offer"
        case .didSeeLendingOfferAccess:
            return "Saw Hub Lending Offer"
        case .didSeeLendingMonitoringAccess:
            return "Saw Hub Lending Monitoring"
        case .didReceiveError:
            return "Saw Credit Hub Access"
        case .didEnterLendingHub:
            return "Lending Hub Accessed"
        }
    }

    private var properties: [String: Any] {
        switch self {
        case let .didSeeHubAccess(origin):
            return ["origin": origin.name]
        case let .didReceiveError(error):
            return ["Action": error.value]
        default:
            return [:]
        }
    }

    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.mixPanel])
    }
}
