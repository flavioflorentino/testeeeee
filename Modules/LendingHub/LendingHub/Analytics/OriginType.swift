public enum OriginType: Equatable {
    case wallet
    case hub

    var name: String {
        switch self {
        case .wallet:
            return "Carteira"
        case .hub:
            return "Hub"
        }
    }
}
