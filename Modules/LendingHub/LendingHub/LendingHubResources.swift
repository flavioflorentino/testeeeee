import Foundation

// swiftlint:disable convenience_type
final class LendingHubResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: LendingHubResources.self).url(forResource: "LendingHubResources", withExtension: "bundle") else {
            return Bundle(for: LendingHubResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: LendingHubResources.self)
    }()
}
