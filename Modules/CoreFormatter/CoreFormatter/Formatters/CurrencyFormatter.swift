import Foundation

public final class CurrencyFormatter: CoreNumberFormatterImplementation, CoreNumberFormatter {
    public static var shared = CurrencyFormatter()
    
    private init() {
        super.init(numberStyle: .currency)
    }
    
    public static func string(from number: NumberConvertible, options: NumberFormatingOptions = NumberFormatingOptions()) -> String? {
        shared.formatDescriptor = options
        return shared.numberFormatter.string(from: number.asNSNumber)
    }
    
    public static func number(from string: String?, options: NumberFormatingOptions = NumberFormatingOptions()) -> NSNumber? {
        guard let string = string else { return nil }
        shared.formatDescriptor = options
        return shared.numberFormatter.number(from: string)
    }
}
