import Foundation

public final class PercentFormatter: CoreNumberFormatterImplementation, CoreNumberFormatter {
    public static var shared = PercentFormatter()
    
    private init() {
        super.init(numberStyle: .percent)
    }
    
    public static func string(from number: NumberConvertible, options: NumberFormatingOptions = NumberFormatingOptions()) -> String? {
        shared.formatDescriptor = options
        return shared.numberFormatter.string(from: number.asNSNumber)
    }
    
    public static func number(from string: String?, options: NumberFormatingOptions = NumberFormatingOptions()) -> NSNumber? {
        guard let string = string else { return nil }
        shared.formatDescriptor = options
        return shared.numberFormatter.number(from: string)
    }
}
