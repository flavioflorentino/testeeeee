import Foundation

public protocol CoreNumberFormatter {
    static func string(from number: NumberConvertible, options: NumberFormatingOptions) -> String?
    static func number(from string: String?, options: NumberFormatingOptions) -> NSNumber?
}

public extension CoreNumberFormatter {
    static func double(from string: String?, options: NumberFormatingOptions = NumberFormatingOptions()) -> Double? {
        number(from: string, options: options)?.doubleValue
    }
    
    static func integer(from string: String?, options: NumberFormatingOptions = NumberFormatingOptions()) -> Int? {
        number(from: string, options: options)?.intValue
    }
    
    static func float(from string: String?, options: NumberFormatingOptions = NumberFormatingOptions()) -> Float? {
        number(from: string, options: options)?.floatValue
    }
}
