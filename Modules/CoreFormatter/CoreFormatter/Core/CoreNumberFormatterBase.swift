import Foundation

public class CoreNumberFormatterImplementation {
    let numberFormatter: NumberFormatter
    
    var formatDescriptor = NumberFormatingOptions() {
        didSet {
            guard formatDescriptor != oldValue else {
                return
            }
            apply(format: formatDescriptor)
        }
    }
    
    init(numberStyle: NumberFormatter.Style) {
        self.numberFormatter = NumberFormatter()
        self.numberFormatter.numberStyle = numberStyle
        apply(format: formatDescriptor)
    }
    
    func apply(format: NumberFormatingOptions) {
        numberFormatter.locale = format.locale
        numberFormatter.minimumFractionDigits = format.numberOfFractionDigits
        numberFormatter.maximumFractionDigits = format.numberOfFractionDigits
    }
}
