import Foundation

public struct NumberFormatingOptions: Equatable {
    public var numberOfFractionDigits: Int
    public var locale: Locale
    
    public init(numberOfFractionDigits: Int = 2, locale: Locale = Locale(identifier: "pt-br")) {
        self.numberOfFractionDigits = numberOfFractionDigits
        self.locale = locale
    }
}
