import Foundation

public protocol NumberConvertible {
    var asNSNumber: NSNumber { get }
}

extension Int: NumberConvertible {
    public var asNSNumber: NSNumber {
        self as NSNumber
    }
}

extension Double: NumberConvertible {
    public var asNSNumber: NSNumber {
        self as NSNumber
    }
}

extension Float: NumberConvertible {
    public var asNSNumber: NSNumber {
        self as NSNumber
    }
}
