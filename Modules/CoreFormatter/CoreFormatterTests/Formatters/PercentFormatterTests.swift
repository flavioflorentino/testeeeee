import XCTest
import Foundation
import CoreFormatter

final class PercentFormatterTests: XCTestCase {
    private func percentNumberFormatter(options: NumberFormatingOptions) -> NumberFormatter {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .percent
        numberFormatter.minimumFractionDigits = options.numberOfFractionDigits
        numberFormatter.maximumFractionDigits = options.numberOfFractionDigits
        numberFormatter.locale = options.locale
        return numberFormatter
    }
    
    func testStringDefaultFormatting_WhenNoOptionIsProvided_ShouldUseDefaultFormattingOptions() throws {
        let options = NumberFormatingOptions()
        let formatter = percentNumberFormatter(options: options)
        let value = 0.1
        let expectedString = try XCTUnwrap(formatter.string(from: NSNumber(value: value)))
        
        XCTAssertEqual(PercentFormatter.string(from: value), expectedString)
    }
    
    func testStringCustomFormatting_WhenProvidingCustomOptions_ShouldUseCustomFormattingOptions() throws {
        let options = NumberFormatingOptions(numberOfFractionDigits: 3, locale: Locale(identifier: "en-US"))
        let formatter = percentNumberFormatter(options: options)
        let value = 0.1
        let expectedString = try XCTUnwrap(formatter.string(from: NSNumber(value: value)))
        
        XCTAssertEqual(PercentFormatter.string(from: value, options: options), expectedString)
    }
    
    func testNSNumberDefaultFormatting_WhenNoOptionIsProvided_ShouldUseDefaultFormattingOptions() throws {
        let options = NumberFormatingOptions()
        let formatter = percentNumberFormatter(options: options)
        let string = try XCTUnwrap(formatter.string(from: 0.1))
        let expectedValue = NSNumber(value: 0.1)
        
        XCTAssertEqual(PercentFormatter.number(from: string), expectedValue)
    }
    
    func testNSNumberCustomFormatting_WhenProvidingCustomOptions_ShouldUseCustomFormattingOptions() throws {
        let options = NumberFormatingOptions(numberOfFractionDigits: 4, locale: Locale(identifier: "en-US"))
        let formatter = percentNumberFormatter(options: options)
        let string = try XCTUnwrap(formatter.string(from: 0.1))
        let expectedValue = NSNumber(value: 0.1)
        
        XCTAssertEqual(PercentFormatter.number(from: string, options: options), expectedValue)
    }
    
    func testDoubleDefaultFormatting_WhenNoOptionIsProvided_ShouldUseDefaultFormattingOptions() throws {
        let options = NumberFormatingOptions()
        let formatter = percentNumberFormatter(options: options)
        let string = try XCTUnwrap(formatter.string(from: 0.1))
        let expectedValue = 0.1
        
        XCTAssertEqual(PercentFormatter.double(from: string), expectedValue)
    }
    
    func testDoubleCustomFormatting_WhenProvidingCustomOptions_ShouldUseCustomFormattingOptions() throws {
        let options = NumberFormatingOptions(numberOfFractionDigits: 4, locale: Locale(identifier: "en-US"))
        let formatter = percentNumberFormatter(options: options)
        let string = try XCTUnwrap(formatter.string(from: 0.1))
        let expectedValue = 0.1
        
        XCTAssertEqual(PercentFormatter.double(from: string, options: options), expectedValue)
    }
    
    func testFloatDefaultFormatting_WhenNoOptionIsProvided_ShouldUseDefaultFormattingOptions() throws {
        let options = NumberFormatingOptions()
        let formatter = percentNumberFormatter(options: options)
        let string = try XCTUnwrap(formatter.string(from: 0.1))
        let expectedValue: Float = 0.1
        
        XCTAssertEqual(PercentFormatter.float(from: string), expectedValue)
    }
    
    func testFloatCustomFormatting_WhenProvidingCustomOptions_ShouldUseCustomFormattingOptions() throws {
        let options = NumberFormatingOptions(numberOfFractionDigits: 4, locale: Locale(identifier: "en-US"))
        let formatter = percentNumberFormatter(options: options)
        let string = try XCTUnwrap(formatter.string(from: 0.1))
        let expectedValue: Float = 0.1
        
        XCTAssertEqual(PercentFormatter.float(from: string, options: options), expectedValue)
    }
    
    func testIntDefaultFormatting_WhenNoOptionIsProvided_ShouldUseDefaultFormattingOptions() throws {
        let options = NumberFormatingOptions()
        let formatter = percentNumberFormatter(options: options)
        let string = try XCTUnwrap(formatter.string(from: 1))
        let expectedValue = 1
        
        XCTAssertEqual(PercentFormatter.integer(from: string), expectedValue)
    }
    
    func testIntCustomFormatting_WhenProvidingCustomOptions_ShouldUseCustomFormattingOptions() throws {
        let options = NumberFormatingOptions(numberOfFractionDigits: 4, locale: Locale(identifier: "en-US"))
        let formatter = percentNumberFormatter(options: options)
        let string = try XCTUnwrap(formatter.string(from: 1))
        let expectedValue = 1
        
        XCTAssertEqual(PercentFormatter.integer(from: string, options: options), expectedValue)
    }
}
