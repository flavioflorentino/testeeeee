import XCTest
import Foundation
import CoreFormatter

final class CurrencyFormatterTests: XCTestCase {
    private func currencyNumberFormatter(options: NumberFormatingOptions) -> NumberFormatter {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.minimumFractionDigits = options.numberOfFractionDigits
        numberFormatter.maximumFractionDigits = options.numberOfFractionDigits
        numberFormatter.locale = options.locale
        return numberFormatter
    }
    
    func testStringDefaultFormatting_WhenNoOptionIsProvided_ShouldUseDefaultFormattingOptions() throws {
        let options = NumberFormatingOptions()
        let formatter = currencyNumberFormatter(options: options)
        let value = 42.0
        let expectedString = try XCTUnwrap(formatter.string(from: NSNumber(value: value)))
        
        XCTAssertEqual(CurrencyFormatter.string(from: value), expectedString)
    }
    
    func testStringCustomFormatting_WhenProvidingCustomOptions_ShouldUseCustomFormattingOptions() throws {
        let options = NumberFormatingOptions(numberOfFractionDigits: 3, locale: Locale(identifier: "en-US"))
        let formatter = currencyNumberFormatter(options: options)
        let value = 42.0
        let expectedString = try XCTUnwrap(formatter.string(from: NSNumber(value: value)))
        
        XCTAssertEqual(CurrencyFormatter.string(from: value, options: options), expectedString)
    }
    
    func testNSNumberDefaultFormatting_WhenNoOptionIsProvided_ShouldUseDefaultFormattingOptions() throws {
        let options = NumberFormatingOptions()
        let formatter = currencyNumberFormatter(options: options)
        let string = try XCTUnwrap(formatter.string(from: 42.0))
        let expectedValue = NSNumber(value: 42.0)
        
        XCTAssertEqual(CurrencyFormatter.number(from: string), expectedValue)
    }
    
    func testNSNumberCustomFormatting_WhenProvidingCustomOptions_ShouldUseCustomFormattingOptions() throws {
        let options = NumberFormatingOptions(numberOfFractionDigits: 4, locale: Locale(identifier: "en-US"))
        let formatter = currencyNumberFormatter(options: options)
        let string = try XCTUnwrap(formatter.string(from: 42.0))
        let expectedValue = NSNumber(value: 42.0)
        
        XCTAssertEqual(CurrencyFormatter.number(from: string, options: options), expectedValue)
    }
    
    func testDoubleDefaultFormatting_WhenNoOptionIsProvided_ShouldUseDefaultFormattingOptions() throws {
        let options = NumberFormatingOptions()
        let formatter = currencyNumberFormatter(options: options)
        let string = try XCTUnwrap(formatter.string(from: 42.0))
        let expectedValue = 42.0
        
        XCTAssertEqual(CurrencyFormatter.double(from: string), expectedValue)
    }
    
    func testDoubleCustomFormatting_WhenProvidingCustomOptions_ShouldUseCustomFormattingOptions() throws {
        let options = NumberFormatingOptions(numberOfFractionDigits: 4, locale: Locale(identifier: "en-US"))
        let formatter = currencyNumberFormatter(options: options)
        let string = try XCTUnwrap(formatter.string(from: 42.0))
        let expectedValue = 42.0
        
        XCTAssertEqual(CurrencyFormatter.double(from: string, options: options), expectedValue)
    }
    
    func testFloatDefaultFormatting_WhenNoOptionIsProvided_ShouldUseDefaultFormattingOptions() throws {
        let options = NumberFormatingOptions()
        let formatter = currencyNumberFormatter(options: options)
        let string = try XCTUnwrap(formatter.string(from: 42.0))
        let expectedValue: Float = 42.0
        
        XCTAssertEqual(CurrencyFormatter.float(from: string), expectedValue)
    }
    
    func testFloatCustomFormatting_WhenProvidingCustomOptions_ShouldUseCustomFormattingOptions() throws {
        let options = NumberFormatingOptions(numberOfFractionDigits: 4, locale: Locale(identifier: "en-US"))
        let formatter = currencyNumberFormatter(options: options)
        let string = try XCTUnwrap(formatter.string(from: 42.0))
        let expectedValue: Float = 42.0
        
        XCTAssertEqual(CurrencyFormatter.float(from: string, options: options), expectedValue)
    }
    
    func testIntDefaultFormatting_WhenNoOptionIsProvided_ShouldUseDefaultFormattingOptions() throws {
        let options = NumberFormatingOptions()
        let formatter = currencyNumberFormatter(options: options)
        let string = try XCTUnwrap(formatter.string(from: 42.0))
        let expectedValue = 42
        
        XCTAssertEqual(CurrencyFormatter.integer(from: string), expectedValue)
    }
    
    func testIntCustomFormatting_WhenProvidingCustomOptions_ShouldUseCustomFormattingOptions() throws {
        let options = NumberFormatingOptions(numberOfFractionDigits: 4, locale: Locale(identifier: "en-US"))
        let formatter = currencyNumberFormatter(options: options)
        let string = try XCTUnwrap(formatter.string(from: 42.0))
        let expectedValue = 42
        
        XCTAssertEqual(CurrencyFormatter.integer(from: string, options: options), expectedValue)
    }
}
