struct RegistrationStatus: Codable {
    var hash: String?
    var completed: Bool
    var currentStep: String
    var sequence: [String?]
    var personalInfo: PersonalInfo?
    var address: AddressModel?
}
