import UIKit

enum RegistrationRulesFactory {
    static func make() -> RegistrationRulesViewController {
        let dependencies = DependencyContainer()
        let coordinator: RegistrationRulesCoordinating = RegistrationRulesCoordinator()
        let presenter: RegistrationRulesPresenting = RegistrationRulesPresenter(coordinator: coordinator, dependencies: dependencies)
        let interactor = RegistrationRulesInteractor(presenter: presenter, dependencies: dependencies)
        let viewController = RegistrationRulesViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
