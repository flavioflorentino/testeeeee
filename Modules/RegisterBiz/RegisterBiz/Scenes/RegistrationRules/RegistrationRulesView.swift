import Foundation
import UI
import UIKit
import SnapKit

final class RegistrationRulesView: UIView, ViewConfiguration {
    private lazy var iconLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(IconLabelStyle(type: .large))
            .with(\.textColor, .branding600())
            .with(\.textAlignment, .center)
        label.setContentCompressionResistancePriority(.required, for: .horizontal)
        return label
    }()

    private lazy var infoLabel: UILabel = {
        var label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .black)
            .with(\.textAlignment, .left)
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func buildViewHierarchy() {
        addSubview(iconLabel)
        addSubview(infoLabel)
    }

    func setupConstraints() {
        iconLabel.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base04)
        }

        infoLabel.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.equalTo(iconLabel.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base04)
        }
    }
}

// MARK: Setup Methods
extension RegistrationRulesView {
    func setIcon(iconName: String?) {
        iconLabel.text = iconName
    }

    func setInfo(_ info: String) {
        infoLabel.text = info
    }
}
