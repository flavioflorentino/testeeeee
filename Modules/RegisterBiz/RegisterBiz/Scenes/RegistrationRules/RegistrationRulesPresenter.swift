import Foundation
import FeatureFlag
import UI
import UIKit

protocol RegistrationRulesPresenting: AnyObject {
    var viewController: RegistrationRulesDisplay? { get set }
    func setupLinks()
    func didNextStep(action: RegistrationRulesAction)
    func open(url: URL)
}

final class RegistrationRulesPresenter {
    private typealias Localizable = Strings.RegistrationIntro
    typealias Dependencies = HasFeatureManager
    
    private let coordinator: RegistrationRulesCoordinating
    private let dependencies: Dependencies
    weak var viewController: RegistrationRulesDisplay?
    
    init(coordinator: RegistrationRulesCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
}

// MARK: - RegistrationRulesPresenting
extension RegistrationRulesPresenter: RegistrationRulesPresenting {
    func didNextStep(action: RegistrationRulesAction) {
        coordinator.perform(action: action)
    }
    
    func setupLinks() {
        let attributedText = AttributedTextURLHelper.createAttributedTextWithLinks(
            text: Localizable.rulesInfoDataCollect,
            attributedTextURLDelegate: self,
            linkTypes: [LinkType.contract.rawValue, LinkType.privacy.rawValue]
        )
        viewController?.setAgreementTextView(linkText: attributedText, linkAttributes: AttributedTextURLHelper.linkAttributes)
    }
    
    func open(url: URL) {
        coordinator.perform(action: .open(url: url))
    }
}

extension RegistrationRulesPresenter: AttributedTextURLDelegate {
    enum LinkType: String {
        case contract
        case privacy
    }
    
    func getUrlInfo(linkType: String) -> AttributedTextURLInfo {
        switch linkType {
        case LinkType.contract.rawValue:
            return AttributedTextURLInfo(
                urlAddress: dependencies.featureManager.text(.urlTerms),
                text: Localizable.rulesInfoDataCollectContract
            )
        case LinkType.privacy.rawValue:
            return AttributedTextURLInfo(
                urlAddress: Localizable.rulesPrivacyPolicyLink,
                text: Localizable.rulesInfoDataCollectPrivacyPolicy
            )
        default:
            break
        }
        return AttributedTextURLInfo(urlAddress: "", text: "")
    }
}
