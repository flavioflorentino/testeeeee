import UIKit
import SafariServices

enum RegistrationRulesAction: Equatable {
    case register
    case open(url: URL)
    
    static func == (lhs: RegistrationRulesAction, rhs: RegistrationRulesAction) -> Bool {
        switch (lhs, rhs) {
        case (.register, .register):
            return true
        case let (.open(url: lhsURL), .open(url: rhsURL)):
            return lhsURL == rhsURL
        default:
            return false
        }
    }
}

protocol RegistrationRulesCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: RegistrationRulesAction)
}

final class RegistrationRulesCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - RegistrationRulesCoordinating
extension RegistrationRulesCoordinator: RegistrationRulesCoordinating {
    func perform(action: RegistrationRulesAction) {
        switch action {
        case .register:
            guard let controller = viewController as? RegistrationBizFinishProtocol else {
                return
            }
            let status = RegistrationStatus(
                completed: false,
                currentStep: RegistrationFlowStep.rulesRegister.rawValue,
                sequence: []
            )
            controller.finishThisStepToRegister(status: status)
            
        case .open(let url):
            let safariViewController = SFSafariViewController(url: url)
            viewController?.navigationController?.present(safariViewController, animated: true)
        }
    }
}
