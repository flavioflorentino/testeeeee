import Foundation

protocol StatusProgressPresenting: AnyObject {
    var viewController: StatusProgressDisplaying? { get set }
    func didNextStep(action: StatusProgressAction)
    func setupView(configuration: StatusProgressConfiguration)
}

final class StatusProgressPresenter {
    private let coordinator: StatusProgressCoordinating
    weak var viewController: StatusProgressDisplaying?

    init(coordinator: StatusProgressCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - StatusProgressPresenting
extension StatusProgressPresenter: StatusProgressPresenting {
    func didNextStep(action: StatusProgressAction) {
        coordinator.perform(action: action)
    }
    
    func setupView(configuration: StatusProgressConfiguration) {
        viewController?.setupView(configuration: configuration)
    }
}
