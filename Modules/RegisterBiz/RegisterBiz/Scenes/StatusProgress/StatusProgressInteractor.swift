import Foundation

protocol StatusProgressInteracting: AnyObject {
    func nextStep()
    func setupView()
}

final class StatusProgressInteractor {
    private let presenter: StatusProgressPresenting
    private let configuration: StatusProgressConfiguration

    init(presenter: StatusProgressPresenting, configuration: StatusProgressConfiguration) {
        self.presenter = presenter
        self.configuration = configuration
    }
}

// MARK: - StatusProgressInteracting
extension StatusProgressInteractor: StatusProgressInteracting {
    func nextStep() {
        presenter.didNextStep(action: .finish(step: configuration.step))
    }
    
    func setupView() {
        presenter.setupView(configuration: configuration)
    }
}
