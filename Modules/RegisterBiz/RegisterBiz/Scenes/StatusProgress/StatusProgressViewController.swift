import UI
import UIKit

protocol StatusProgressDisplaying: AnyObject {
    func setupView(configuration: StatusProgressConfiguration)
}

private extension StatusProgressViewController.Layout {
    enum Size {
        static let imageSize: CGFloat = 226
    }
}

final class StatusProgressViewController: ViewController<StatusProgressInteracting, UIView>, RegistrationBizFinishProtocol {
    fileprivate enum Layout { }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, .black())
        return label
    }()
    
    private lazy var subTitleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale600())
        return label
    }()
    
    private lazy var imageView = UIImageView()
    
    private lazy var nextStepButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(confirmAction), for: .touchUpInside)
        return button
    }()
    
    weak var flowCoordinatorDelegate: RegistrationBizFinishDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.setupView()
    }

    override func buildViewHierarchy() {
        view.addSubviews(
            titleLabel,
            subTitleLabel,
            imageView,
            nextStepButton
        )
    }
    
    override func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        subTitleLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        imageView.snp.makeConstraints {
            $0.centerX.centerY.equalToSuperview()
            $0.size.greaterThanOrEqualTo(Layout.Size.imageSize)
        }
        
        nextStepButton.snp.makeConstraints {
            $0.leading.trailing.bottom.equalToSuperview().inset(Spacing.base02)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.white.color
    }
}

@objc
private extension StatusProgressViewController {
    func confirmAction() {
        viewModel.nextStep()
    }
}

// MARK: - StatusProgressDisplaying
extension StatusProgressViewController: StatusProgressDisplaying {
    func setupView(configuration: StatusProgressConfiguration) {
        titleLabel.text = configuration.title
        subTitleLabel.text = configuration.subTitle
        imageView.image = configuration.image
        nextStepButton.setTitle(configuration.buttonTitle, for: .normal)
    }
}
