import UIKit

enum StatusProgressFactory {
    static func make(configuration: StatusProgressConfiguration) -> StatusProgressViewController {
        let container = DependencyContainer()
        let coordinator: StatusProgressCoordinating = StatusProgressCoordinator()
        let presenter: StatusProgressPresenting = StatusProgressPresenter(coordinator: coordinator)
        let interactor = StatusProgressInteractor(presenter: presenter, configuration: configuration)
        let viewController = StatusProgressViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
