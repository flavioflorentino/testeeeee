import UIKit

enum StatusProgressAction: Equatable {
    case finish(step: String)
}

protocol StatusProgressCoordinating: AnyObject {
    var viewController: RegistrationBizFinishProtocol? { get set }
    func perform(action: StatusProgressAction)
}

final class StatusProgressCoordinator {
    weak var viewController: RegistrationBizFinishProtocol?
}

// MARK: - StatusProgressCoordinating
extension StatusProgressCoordinator: StatusProgressCoordinating {
    func perform(action: StatusProgressAction) {
        switch action {
        case .finish(let step):
            let status = RegistrationStatus(completed: false, currentStep: step, sequence: [])
            viewController?.finishThisStepToRegister(status: status)
        }
    }
}
