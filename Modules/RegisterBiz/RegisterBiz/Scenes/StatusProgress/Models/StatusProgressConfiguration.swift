import Foundation
import AssetsKit
import UIKit

protocol StatusProgressConfiguration {
    var title: String { get }
    var subTitle: String { get }
    var image: UIImage? { get }
    var buttonTitle: String { get }
    var step: String { get }
}

struct AlmostThereProgress: StatusProgressConfiguration {
    private typealias Localizable = Strings.AlmostThere
    var title: String {
        Localizable.title
    }
    
    var subTitle: String {
        Localizable.subTitle
    }
    
    var image: UIImage? {
        UIImage(asset: Resources.Illustrations.iluVictory)
    }
    
    var buttonTitle: String {
        Localizable.titleButton
    }
    
    var step: String {
        RegistrationFlowStep.almostThere.rawValue
    }
}

struct RegisterCompleteProgress: StatusProgressConfiguration {
    private typealias Localizable = Strings.RegisterComplete
    
    var title: String {
        Localizable.title
    }
    
    var subTitle: String {
        Localizable.subTitle
    }
    
    var image: UIImage? {
        UIImage(asset: Resources.Illustrations.iluTool)
    }
    
    var buttonTitle: String {
        Localizable.titleButton
    }
    
    var step: String {
        RegistrationFlowStep.registerComplete.rawValue
    }
}
