import Foundation
import UI

protocol RegistrationAddressInteracting: AnyObject {
    func update(textFields: [UIPPFloatingTextField])
    func validateAndSubmit(textFields: [UIPPFloatingTextField])
    func submitValues(model: AddressModel)
}

final class RegistrationAddressInteractor {
    private let service: RegistrationAddressServicing
    private let presenter: RegistrationAddressPresenting

    init(service: RegistrationAddressServicing, presenter: RegistrationAddressPresenting) {
        self.service = service
        self.presenter = presenter
    }
}

// MARK: - RegistrationAddressInteracting
extension RegistrationAddressInteractor: RegistrationAddressInteracting {
    func update(textFields: [UIPPFloatingTextField]) {
        let result: [(message: String?, textField: UIPPFloatingTextField)] = textFields.map {
            switch $0.validate() {
            case .invalid(let error):
                return(error.first?.message, $0)
            default:
                return("", $0)
            }
        }
        
        presenter.updateFields(fields: result)
    }
    
    func validateAndSubmit(textFields: [UIPPFloatingTextField]) {
        let result = textFields.allSatisfy { $0.validate().isValid }
        presenter.validateSubmit(value: result)
    }
    
    func submitValues(model: AddressModel) {
        service.registerAddress(model: model) {[weak self] result in
            switch result {
            case .success:
                self?.presenter.didNextStep(action: .nextScreen(model: model))
            case .failure:
                self?.presenter.presentError()
            }
        }
    }
}
