import UI
import AssetsKit
import UIKit

protocol RegistrationAddressDisplaying: ApolloLoadable {
    func update(fields: [(message: String?, textField: UIPPFloatingTextField?)])
    func submitValues()
    func handleListError(list: [(message: String, field: String)])
}

final class RegistrationAddressViewController: ViewController<RegistrationAddressInteracting, UIView>, RegistrationBizFinishProtocol {
    private typealias Localizable = Strings.PersonalAddress
    private typealias ErrorLocalizable = Strings.Error
    
    fileprivate enum Layout {}
    
private enum TextFieldNames: String, CaseIterable {
    case cep
    case street
    case number
    case complement
    case district
    case city
    case state

    var type: Int {
        TextFieldNames.allCases.firstIndex(of: self) ?? .min
    }
}
    
    weak var flowCoordinatorDelegate: RegistrationBizFinishDelegate?
    
    // MARK: - Private Lazy Vars
    private lazy var scroll = UIScrollView()
    
    private lazy var contentView: RegistrationAddressView = {
        let view = RegistrationAddressView()
        view.delegate = self
        return view
    }()
    
    // MARK: - Life Cycle
    override func buildViewHierarchy() {
        view.addSubview(scroll)
        scroll.addSubview(contentView)
    }
    
    func addObservers() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow(_:)),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide(_:)),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    
    override func setupConstraints() {
        scroll.snp.makeConstraints {
            $0.centerX.centerY.top.leading.trailing.bottom.equalToSuperview()
        }
        
        contentView.snp.makeConstraints {
            $0.top.bottom.leading.trailing.width.equalTo(scroll)
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        addObservers()
        navigationController?.navigationBar.setup(appearance: RegisterBizNavBarAppearanceDefault())
        title = Localizable.title
    }
}

// MARK: - View Delegate
extension RegistrationAddressViewController: RegistrationAddressViewDelegate {
    func didTapConfirmButton(inputs: [UIPPFloatingTextField]) {
        startApolloLoader()
        interactor.update(textFields: inputs)
        interactor.validateAndSubmit(textFields: inputs)
    }

    func textDidBeginEditing(textField: UITextField) {
        scroll.contentOffset.y = textField.frame.origin.y - Sizing.base06
    }
}

// MARK: - Observer Functions
extension RegistrationAddressViewController {
    @objc
    func keyboardWillShow(_ notification: NSNotification) {
        guard let keyboardRect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        
        scroll.snp.updateConstraints {
            $0.bottom.equalToSuperview().inset(keyboardRect.height)
        }
    }
    
    @objc
    func keyboardWillHide(_ notification: NSNotification) {
        scroll.snp.updateConstraints {
            $0.bottom.equalToSuperview()
        }
        
        scroll.contentOffset.y = 0
    }
}

// MARK: - View Model Outputs
extension RegistrationAddressViewController: RegistrationAddressDisplaying {
    func update(fields: [(message: String?, textField: UIPPFloatingTextField?)]) {
        fields.forEach {
            guard let textField = $0.textField else { return }

            if let message = $0.message, !message.isEmpty {
                textField.lineColor = Colors.branding300.color
            }
            textField.errorMessage = $0.message
        }
    }
    
    func submitValues() {
        guard
            let cep = contentView.getCEPText(),
            let street = contentView.getStreetText(),
            let number = contentView.getNumberText(),
            let complement = contentView.getComplementText(),
            let district = contentView.getDistrictText(),
            let city = contentView.getCityText(),
            let state = contentView.getStateText()
        else {
            return
        }
        
        let model = AddressModel(cep: cep,
                                 street: street,
                                 number: number,
                                 complement: complement,
                                 district: district,
                                 city: city,
                                 state: state)
        interactor.submitValues(model: model)
    }
    
    func handleListError(list: [(message: String, field: String)]) {
        let result: [(message: String?, textField: UIPPFloatingTextField?)] = list.map {
            guard let fieldName = TextFieldNames(rawValue: $0.field) else {
                return (nil, contentView.getFieldWith(tag: TextFieldNames.cep.type))
            }
            return ($0.message, contentView.getFieldWith(tag: fieldName.type))
        }
        
        update(fields: result)
    }
}
