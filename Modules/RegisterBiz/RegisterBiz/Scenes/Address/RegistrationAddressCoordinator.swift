import UIKit
import UI

enum RegistrationAddressAction: Equatable {
    case nextScreen(model: AddressModel)
    case handleError(controller: ApolloFeedbackViewController)
}

protocol RegistrationAddressCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: RegistrationAddressAction)
}

final class RegistrationAddressCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - RegistrationAddressCoordinating
extension RegistrationAddressCoordinator: RegistrationAddressCoordinating {
    func perform(action: RegistrationAddressAction) {
        switch action {
        case .nextScreen(let address):
            guard let controller = viewController as? RegistrationBizFinishProtocol else { return }
            let status = RegistrationStatus(
                completed: false,
                currentStep: RegistrationFlowStep.personAddress.rawValue,
                sequence: [],
                address: address)
            controller.finishThisStepToRegister(status: status)
        case .handleError(let controller):
            viewController?.present(controller, animated: true)
        }
    }
}
