import UI
import UIKit
import Validator

protocol RegistrationAddressViewDelegate: AnyObject {
    func didTapConfirmButton(inputs: [UIPPFloatingTextField])
    func textDidBeginEditing(textField: UITextField)
}

private enum InputMessageErrors: String, ValidationError {
    case cepMessageError
    case streetMessageError
    case numberMessageError
    case districtMessageError
    case cityMessageError
    case stateMessageError
    
    var message: String {
        switch self {
        case .cepMessageError:
            return Strings.PersonalAddress.cepMessageError
        default:
            return Strings.PersonalAddress.inputMessageError
        }
    }
}

final class RegistrationAddressView: UIView, ViewConfiguration {
    private typealias Localizable = Strings.PersonalAddress
    
    fileprivate enum Layout {}
        
    private enum TextFieldType: Int {
        case cep = 0
        case street
        case number
        case complement
        case district
        case city
        case state
    }
    
    weak var delegate: RegistrationAddressViewDelegate?
    
    // MARK: - Private Lazy Vars
    private var inputs: [UIPPFloatingTextField] = []
    
    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.subTitle
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale600())
        return label
    }()

    private lazy var cepTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = Localizable.cepPlaceholder
        textField.border = .none
        textField.returnKeyType = .next
        textField.delegate = self
        textField.tag = TextFieldType.cep.rawValue
        inputs.append(textField)
        return textField
    }()
    
    private lazy var streetTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = Localizable.streetPlaceholder
        textField.border = .none
        textField.returnKeyType = .next
        textField.delegate = self
        textField.tag = TextFieldType.street.rawValue
        inputs.append(textField)
        return textField
    }()
    
    private lazy var numberTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = Localizable.numberPlaceholder
        textField.keyboardType = .numberPad
        textField.border = .none
        textField.returnKeyType = .next
        textField.delegate = self
        textField.tag = TextFieldType.number.rawValue
        inputs.append(textField)
        return textField
    }()
    
    private lazy var complementTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = Localizable.complementPlaceholder
        textField.border = .none
        textField.returnKeyType = .next
        textField.tag = TextFieldType.complement.rawValue
        textField.delegate = self
        inputs.append(textField)
        return textField
    }()
    
    private lazy var districtTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = Localizable.districtPlaceholder
        textField.border = .none
        textField.returnKeyType = .done
        textField.delegate = self
        textField.tag = TextFieldType.district.rawValue
        inputs.append(textField)
        return textField
    }()
    
    private lazy var cityTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = Localizable.cityPlaceholder
        textField.border = .none
        textField.returnKeyType = .done
        textField.delegate = self
        textField.tag = TextFieldType.city.rawValue
        inputs.append(textField)
        return textField
    }()
    
    private lazy var stateTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = Localizable.statePlaceholder
        textField.border = .none
        textField.returnKeyType = .done
        textField.delegate = self
        textField.tag = TextFieldType.state.rawValue
        inputs.append(textField)
        return textField
    }()
    
    private lazy var stackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .fill
        stack.distribution = .fillEqually
        stack.spacing = Spacing.base03
        return stack
    }()
    
    private lazy var okButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.buttonOKTitle, for: .normal)
        button.addTarget(self, action: #selector(confirmAction), for: .touchUpInside)
        button.buttonStyle(PrimaryButtonStyle())
        return button
    }()
    
    private lazy var cepNumberMask: TextFieldMasker = {
        let mask = CustomStringMask(mask: "00000-000")
        return TextFieldMasker(textMask: mask)
    }()
    
    // MARK: - Life Cycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        endEditing(true)
    }
    
    func configureViews() {
        setupTextFieldMasks()
        setupTextFieldRules()
    }
    
    func setupConstraints() {
        infoLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base05)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.greaterThanOrEqualTo(Spacing.base08)
        }
        
        stackView.snp.makeConstraints {
            $0.top.equalTo(infoLabel.snp_bottomMargin).offset(Spacing.base05)
            $0.leading.equalTo(snp.leading).offset(Spacing.base02)
            $0.trailing.equalTo(snp.trailing).inset(Spacing.base02)
        }
        
        okButton.snp.makeConstraints {
            $0.bottom.equalToSuperview().offset(Spacing.base01)
            $0.top.equalTo(stackView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        cepTextField.snp.makeConstraints {
            $0.height.equalTo(Sizing.base05)
        }
        
        streetTextField.snp.makeConstraints {
            $0.height.equalTo(Sizing.base05)
        }
        
        numberTextField.snp.makeConstraints {
            $0.height.equalTo(Sizing.base05)
        }
        
        complementTextField.snp.makeConstraints {
            $0.height.equalTo(Sizing.base05)
        }
        
        districtTextField.snp.makeConstraints {
            $0.height.equalTo(Sizing.base05)
        }
        
        cityTextField.snp.makeConstraints {
            $0.height.equalTo(Sizing.base05)
        }
        
        stateTextField.snp.makeConstraints {
            $0.height.equalTo(Sizing.base05)
        }
    }
    
    func buildViewHierarchy() {
        addSubview(infoLabel)
        addSubview(stackView)
        addSubview(okButton)
        
        stackView.addArrangedSubview(cepTextField)
        stackView.addArrangedSubview(streetTextField)
        stackView.addArrangedSubview(numberTextField)
        stackView.addArrangedSubview(complementTextField)
        stackView.addArrangedSubview(districtTextField)
        stackView.addArrangedSubview(cityTextField)
        stackView.addArrangedSubview(stateTextField)
    }
}

private extension RegistrationAddressView {
    @objc
    func confirmAction() {
        delegate?.didTapConfirmButton(inputs: inputs)
    }
}

// MARK: - Setup TextField Configuration
extension RegistrationAddressView {
    private func setupTextFieldMasks() {
        cepNumberMask.bind(to: cepTextField)
    }
    
    private func setupTextFieldRules() {
        var cepRules = ValidationRuleSet<String>()
        cepRules.add(rule: ValidationRuleLength(min: 9, error: InputMessageErrors.cepMessageError))
        cepTextField.validationRules = cepRules
        
        var streetRules = ValidationRuleSet<String>()
        streetRules.add(rule: ValidationRuleLength(min: 4, error: InputMessageErrors.streetMessageError))
        streetTextField.validationRules = streetRules
        
        var numberRules = ValidationRuleSet<String>()
        numberRules.add(rule: ValidationRuleLength(min: 1, error: InputMessageErrors.numberMessageError))
        numberTextField.validationRules = numberRules
        
        var districtRules = ValidationRuleSet<String>()
        districtRules.add(rule: ValidationRuleLength(min: 4, error: InputMessageErrors.districtMessageError))
        districtTextField.validationRules = districtRules
        
        var cityRules = ValidationRuleSet<String>()
        cityRules.add(rule: ValidationRuleLength(min: 4, error: InputMessageErrors.cityMessageError))
        cityTextField.validationRules = cityRules
        
        var stateRules = ValidationRuleSet<String>()
        stateRules.add(rule: ValidationRuleLength(min: 2, error: InputMessageErrors.stateMessageError))
        stateTextField.validationRules = stateRules
    }
}

// MARK: - Internal Functions

extension RegistrationAddressView {
    func getCEPText() -> String? {
        cepTextField.text
    }
    
    func getStreetText() -> String? {
        streetTextField.text
    }
    
    func getNumberText() -> String? {
        numberTextField.text
    }
    
    func getComplementText() -> String? {
        complementTextField.text
    }
    
    func getDistrictText() -> String? {
        districtTextField.text
    }
    
    func getCityText() -> String? {
        cityTextField.text
    }
    
    func getStateText() -> String? {
        stateTextField.text
    }
    
    func getFieldWith(tag: Int) -> UIPPFloatingTextField? {
        guard tag < inputs.count, tag >= 0 else {
            return nil
        }
        
        return inputs[tag]
    }
}

extension RegistrationAddressView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField.tag {
        case TextFieldType.state.rawValue:
            textField.resignFirstResponder()
            return true
        default:
            inputs[textField.tag + 1].becomeFirstResponder()
            return false
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        delegate?.textDidBeginEditing(textField: textField)
    }
}
