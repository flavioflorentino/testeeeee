import UIKit

enum RegistrationAddressFactory {
    // FIXME: Refatorar nome do typealias para RegistrationController
    static func make() -> RegistrionController {
        let container = DependencyContainer()
        let service: RegistrationAddressServicing = RegistrationAddressService(dependencies: container)
        let coordinator: RegistrationAddressCoordinating = RegistrationAddressCoordinator()
        let presenter: RegistrationAddressPresenting = RegistrationAddressPresenter(coordinator: coordinator)
        let interactor = RegistrationAddressInteractor(service: service, presenter: presenter)
        let viewController = RegistrationAddressViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
