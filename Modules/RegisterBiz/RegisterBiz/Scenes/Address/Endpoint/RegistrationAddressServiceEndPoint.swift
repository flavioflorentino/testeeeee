import Core

enum RegistrationAddressServiceEndPoint {
    case verifyAddress(hash: String)
}

extension RegistrationAddressServiceEndPoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .verifyAddress:
            // FIXME: Corrigir path
            return "/account/verify-address"
        }
    }

    var method: HTTPMethod {
        .post
    }

    var body: Data? {
        switch self {
        case let .verifyAddress(hash):
            // FIXME: Corrigir body
            return ["hash": hash].toData()
        }
    }
}
