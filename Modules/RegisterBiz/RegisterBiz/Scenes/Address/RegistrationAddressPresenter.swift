import UI
import Core
import AssetsKit
import Foundation

protocol RegistrationAddressPresenting: AnyObject {
    var viewController: RegistrationAddressDisplaying? { get set }
    func didNextStep(action: RegistrationAddressAction)
    func updateFields(fields: [(message: String?, textField: UIPPFloatingTextField)])
    func validateSubmit(value: Bool)
    func presentError()
}

final class RegistrationAddressPresenter: RegistrationAddressPresenting {
    private let coordinator: RegistrationAddressCoordinating
    weak var viewController: RegistrationAddressDisplaying?
    
    private typealias Localizable = Strings.PersonalInfo
    private typealias ErrorLocalizable = Strings.Error

    init(coordinator: RegistrationAddressCoordinating) {
        self.coordinator = coordinator
    }

    func didNextStep(action: RegistrationAddressAction) {
        viewController?.stopApolloLoader(completion: nil)
        coordinator.perform(action: action)
    }
    
    func updateFields(fields: [(message: String?, textField: UIPPFloatingTextField)]) {
        viewController?.update(fields: fields)
    }
    
    func validateSubmit(value: Bool) {
        guard value else {
            presentError()
            return
        }
        
        viewController?.submitValues()
    }
    
    func presentError() {
        viewController?.stopApolloLoader(completion: nil)
        coordinator.perform(action: .handleError(controller: createGenericError()))
    }
}

private extension RegistrationAddressPresenter {
    func createGenericError() -> ApolloFeedbackViewController {
        let description = NSAttributedString(string: ErrorLocalizable.description)
        let feedbackContent = ApolloFeedbackViewContent(image: Resources.Illustrations.iluConstruction.image,
                                                        title: Localizable.title,
                                                        description: description,
                                                        primaryButtonTitle: ErrorLocalizable.titleButton,
                                                        secondaryButtonTitle: "")
        let feedbackController = ApolloFeedbackViewController(content: feedbackContent)
        feedbackController.didTapPrimaryButton = {
            feedbackController.dismiss(animated: true)
        }

        return feedbackController
    }
}
