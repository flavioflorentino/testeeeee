import Core
import Foundation

protocol RegistrationAddressServicing {
    func registerAddress(model: AddressModel, _ completion: @escaping (Result<VerifyAddress, ApiError>) -> Void)
}

struct VerifyAddress: Decodable {
    let verify: Bool
}

final class RegistrationAddressService {
    typealias Dependencies = HasMainQueue & HasKeychainManager
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - RegistrationAddressServicing
extension RegistrationAddressService: RegistrationAddressServicing {
    func registerAddress(model: AddressModel, _ completion: @escaping (Result<VerifyAddress, ApiError>) -> Void) {
        let endPoint = RegistrationAddressServiceEndPoint.verifyAddress(hash: model.cep)
        Api<VerifyAddress>(endpoint: endPoint).execute {[weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map { $0.model })
            }
        }
    }
}

struct AddressModel: Codable, Equatable {
    let cep: String
    let street: String
    let number: String
    let complement: String
    let district: String
    let city: String
    let state: String
}
