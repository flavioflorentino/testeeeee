import Foundation

protocol SMSValidationInteracting: AnyObject {
    func doSomething()
}

final class SMSValidationInteractor {
    private let service: SMSValidationServicing
    private let presenter: SMSValidationPresenting

    init(service: SMSValidationServicing, presenter: SMSValidationPresenting) {
        self.service = service
        self.presenter = presenter    }
}

// MARK: - SMSValidationInteracting
extension SMSValidationInteractor: SMSValidationInteracting {
    func doSomething() {
        presenter.displaySomething()
    }
}
