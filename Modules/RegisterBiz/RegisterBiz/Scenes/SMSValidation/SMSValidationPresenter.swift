import Foundation

protocol SMSValidationPresenting: AnyObject {
    var viewController: SMSValidationDisplaying? { get set }
    func displaySomething()
    func didNextStep(action: SMSValidationAction)
}

final class SMSValidationPresenter {
    private let coordinator: SMSValidationCoordinating
    weak var viewController: SMSValidationDisplaying?

    init(coordinator: SMSValidationCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - SMSValidationPresenting
extension SMSValidationPresenter: SMSValidationPresenting {
    func displaySomething() {
        viewController?.displaySomething()
    }
    
    func didNextStep(action: SMSValidationAction) {
        coordinator.perform(action: action)
    }
}
