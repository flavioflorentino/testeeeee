import UI
import UIKit

protocol SMSValidationDisplaying: AnyObject {
    func displaySomething()
}

private extension SMSValidationViewController.Layout {
    //example
    enum Size {
        static let imageHeight: CGFloat = 90.0
    }
}

final class SMSValidationViewController: ViewController<SMSValidationInteracting, UIView> {
    fileprivate enum Layout { }

    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.doSomething()
    }

    override func buildViewHierarchy() { }
    
    override func setupConstraints() { }

    override func configureViews() { }
}

// MARK: - SMSValidationDisplaying
extension SMSValidationViewController: SMSValidationDisplaying {
    func displaySomething() { }
}
