import UIKit

enum SMSValidationAction {
}

protocol SMSValidationCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: SMSValidationAction)
}

final class SMSValidationCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - SMSValidationCoordinating
extension SMSValidationCoordinator: SMSValidationCoordinating {
    func perform(action: SMSValidationAction) {
    }
}
