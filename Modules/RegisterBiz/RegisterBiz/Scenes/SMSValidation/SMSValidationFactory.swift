import UIKit

enum SMSValidationFactory {
    static func make() -> SMSValidationViewController {
        let container = DependencyContainer()
        let service: SMSValidationServicing = SMSValidationService()
        let coordinator: SMSValidationCoordinating = SMSValidationCoordinator()
        let presenter: SMSValidationPresenting = SMSValidationPresenter(coordinator: coordinator)
        let interactor = SMSValidationInteractor(service: service, presenter: presenter)
        let viewController = SMSValidationViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
