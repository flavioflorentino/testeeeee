import NetworkPJ
import Core

protocol RegistrationPersonalInfoServicing {
    func registerPersonalInfo(model: PersonalInfo, _ completion: @escaping (Result<HashUser?, ApiError>) -> Void)
}

struct HashUser: Decodable {
    let hash: String
}

final class RegistrationPersonalInfoService: RegistrationPersonalInfoServicing {
    typealias Dependencies = HasMainQueue & HasKeychainManager
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func registerPersonalInfo(model: PersonalInfo, _ completion: @escaping (Result<HashUser?, ApiError>) -> Void) {        
        let endPoint = PersonalInfoServiceEndPoint.userData(model: model)
        
        BizApi<HashUser>(endpoint: endPoint).bizExecute {[weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map { $0.model.data })
            }
        }
    }
}

struct PersonalInfo: Codable & Equatable {
    let name: String
    let mail: String
    let cpf: String
    let cellPhone: String
    let birthday: String
    let motherName: String
    
    var formatedBirthday: String {
        let dateFormatter = configureFormat()
        let stringFormatter = configureFormat(dateFormat: "yyyy-MM-dd")
        
        guard let dateObject = dateFormatter.date(from: birthday) else {
            return ""
        }

        let stringDate = stringFormatter.string(from: dateObject)
        return stringDate
    }
    
    private func configureFormat(dateFormat: String = "dd/MM/yyyy") -> DateFormatter {
        let formatter = DateFormatter()
        formatter.calendar = Calendar.current
        formatter.locale = Locale(identifier: "pt_BR_POSIX")
        formatter.dateFormat = dateFormat
        return formatter
    }
}

struct PersonalInfoAndHashUser: Codable & Equatable {
    let hash: String
    let personalInfo: PersonalInfo
}
