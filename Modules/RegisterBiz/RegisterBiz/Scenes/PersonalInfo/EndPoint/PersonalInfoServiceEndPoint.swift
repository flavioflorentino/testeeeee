import Core

enum PersonalInfoServiceEndPoint {
    case userData(model: PersonalInfo)
}

extension PersonalInfoServiceEndPoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .userData:
            return "/v1/biz-register/user-data"
        }
    }
    
    var method: HTTPMethod {
        .post
    }
    
    var body: Data? {
        switch self {
        case let .userData(model):
            return [
                "name": model.name,
                "email": model.mail,
                "document": model.cpf,
                "birthday": model.formatedBirthday,
                "cellphone": model.cellPhone,
                "mother": model.motherName
            ].toData()
        }
    }
}
