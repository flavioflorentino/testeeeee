import AnalyticsModule
import Foundation

enum RegistrationPersonalInfoAnalytics: String, AnalyticsKeyProtocol {
    case didLoadView = "OB Personal Data"
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(rawValue, providers: [.mixPanel])
    }
}
