import Core
import UI
import Foundation
import AssetsKit

protocol RegistrationPersonalInfoPresenting: AnyObject {
    var viewController: RegistrationPersonalInfoDisplay? { get set }
    func updateFields(fields: [(message: String?, textField: UIPPFloatingTextField)])
    func didNextStep(action: RegistrationPersonalInfoAction)
    func validateSubmit(value: Bool)
    func presentError()
    func presentListError(list: [(message: String, field: String)])
    func presentHashError(message: String)
}

final class RegistrationPersonalInfoPresenter: RegistrationPersonalInfoPresenting {
    private let coordinator: RegistrationPersonalInfoCoordinating
    weak var viewController: RegistrationPersonalInfoDisplay?
    
    private typealias Localizable = Strings.PersonalInfo
    private typealias ErrorLocalizable = Strings.Error

    init(coordinator: RegistrationPersonalInfoCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: RegistrationPersonalInfoAction) {
        viewController?.stopApolloLoader(completion: nil)
        coordinator.perform(action: action)
    }
    
    func updateFields(fields: [(message: String?, textField: UIPPFloatingTextField)]) {
        viewController?.update(fields: fields)
    }
    
    func validateSubmit(value: Bool) {
        guard value else {
            coordinator.perform(action: .handleError(controller: createGenericError()))
            return
        }
        
        viewController?.submitValues()
    }
    
    func presentError() {
        viewController?.stopApolloLoader(completion: nil)
        coordinator.perform(action: .handleError(controller: createGenericError()))
    }
    
    func presentListError(list: [(message: String, field: String)]) {
        viewController?.stopApolloLoader(completion: nil)
        viewController?.handleListError(list: list)
    }
    
    func presentHashError(message: String) {
        viewController?.stopApolloLoader(completion: nil)
        let snackar = ApolloSnackbar(text: message, iconType: .error)
        coordinator.perform(action: .hashError(snack: snackar))
    }
}

private extension RegistrationPersonalInfoPresenter {
    func createGenericError() -> ApolloFeedbackViewController {
        let description = NSAttributedString(string: ErrorLocalizable.description)
        let feedbackContent = ApolloFeedbackViewContent(image: Resources.Illustrations.iluConstruction.image,
                                                        title: Localizable.title,
                                                        description: description,
                                                        primaryButtonTitle: ErrorLocalizable.titleButton,
                                                        secondaryButtonTitle: "")
        let feedbackController = ApolloFeedbackViewController(content: feedbackContent)
        feedbackController.didTapPrimaryButton = {
            feedbackController.dismiss(animated: true)
        }
        
        return feedbackController
    }
}
