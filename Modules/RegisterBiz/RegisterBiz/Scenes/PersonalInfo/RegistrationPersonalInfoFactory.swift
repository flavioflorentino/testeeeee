import Foundation

enum RegistrationPersonalInfoFactory {
    static func make() -> RegistrationPersonalInfoViewController {
        let container = DependencyContainer()
        let service: RegistrationPersonalInfoServicing = RegistrationPersonalInfoService(dependencies: container)
        let coordinator: RegistrationPersonalInfoCoordinating = RegistrationPersonalInfoCoordinator()
        let presenter: RegistrationPersonalInfoPresenting = RegistrationPersonalInfoPresenter(coordinator: coordinator)
        let interactor = RegistrationPersonalInfoInteractor(service: service, presenter: presenter, dependencies: container)
        let viewController = RegistrationPersonalInfoViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
