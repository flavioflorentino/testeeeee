import UI
import UIKit
import Validator
import AssetsKit

protocol RegistrationPersonalInfoDisplay: ApolloLoadable {
    func update(fields: [(message: String?, textField: UIPPFloatingTextField?)])
    func submitValues()
    func handleListError(list: [(message: String, field: String)])
}

private extension RegistrationPersonalInfoViewController.Layout {
    enum Size {
        static let offsetKeyboard: CGFloat = 50
    }
}

final class RegistrationPersonalInfoViewController: ViewController<RegistrationPersonalInfoInteracting, UIView>, RegistrationBizFinishProtocol {
    weak var flowCoordinatorDelegate: RegistrationBizFinishDelegate?
    
    private typealias Localizable = Strings.PersonalInfo
    private typealias ErrorLocalizable = Strings.Error
    
    fileprivate enum Layout {}
    
    private enum TextFieldName: String {
        case name
        case mail = "email"
        case cpf = "document"
        case birthday
        case cellPhone = "cellphone"
        case motherName = "mother"
        
        var type: TextFieldType {
            switch self {
            case .name:
                return TextFieldType.name
            case .mail:
                return TextFieldType.mail
            case .cpf:
                return TextFieldType.cpf
            case .birthday:
                return TextFieldType.birthday
            case .cellPhone:
                return TextFieldType.cellPhone
            case .motherName:
                return TextFieldType.motherName
            }
        }
    }
    
    // MARK: - Private Lazy Vars
    
    private lazy var scroll = UIScrollView()
    
    private lazy var contentView: RegistrationPersonalInfoView = {
        let view = RegistrationPersonalInfoView()
        view.delegate = self
        return view
    }()
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.trackingViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.endEditing(true)
    }
    
    func addObservers() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow(_:)),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide(_:)),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    
    override func buildViewHierarchy() {
        view.addSubview(scroll)
        scroll.addSubview(contentView)
    }
    
    override func setupConstraints() {
        scroll.snp.makeConstraints {
            $0.centerX.centerY.top.leading.trailing.bottom.equalToSuperview()
        }
        
        contentView.snp.makeConstraints {
            $0.top.bottom.leading.trailing.width.equalTo(scroll)
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.white.color
        addObservers()
        navigationController?.navigationBar.setup(appearance: RegisterBizNavBarAppearanceDefault())
        title = Localizable.title
    }
}

// MARK: - View Delegate
extension RegistrationPersonalInfoViewController: RegistrationPersonalInfoViewDelegate {
    func didTapConfirmButton(inputs: [UIPPFloatingTextField]) {
        startApolloLoader()
        interactor.update(textFields: inputs)
        interactor.validateAndSubmit(textFields: inputs)
    }
    
    func textDidBeginEditing(textField: UITextField) {
        scroll.contentOffset.y = textField.frame.origin.y - Layout.Size.offsetKeyboard
    }
}

// MARK: - Oberserver Functions
extension RegistrationPersonalInfoViewController {
    @objc
    func keyboardWillShow(_ notification: NSNotification) {
        guard let keyboardRect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        
        scroll.snp.updateConstraints {
            $0.bottom.equalToSuperview().inset(keyboardRect.height)
        }
    }
    
    @objc
    func keyboardWillHide(_ notification: NSNotification) {
        scroll.snp.updateConstraints {
            $0.bottom.equalToSuperview()
        }
        
        scroll.contentOffset.y = 0
    }
}

// MARK: View Model Outputs
extension RegistrationPersonalInfoViewController: RegistrationPersonalInfoDisplay {
    func update(fields: [(message: String?, textField: UIPPFloatingTextField?)]) {
        fields.forEach {
            if let message = $0.message, !message.isEmpty {
                $0.textField?.lineColor = Colors.branding300.color
            }
            $0.textField?.errorMessage = $0.message
        }
    }
    
    func submitValues() {
        guard
            let name = contentView.getNameText(),
            let mail = contentView.getMailText(),
            let cpf = contentView.getCPFText(),
            let cell = contentView.getCellPhone(),
            let birthday = contentView.getBirthdayText(),
            let motherName = contentView.getMotherNameText()
            else {
                return
        }
        
        let model = PersonalInfo(
            name: name,
            mail: mail,
            cpf: cpf,
            cellPhone: cell,
            birthday: birthday,
            motherName: motherName
        )
        
        interactor.submitValues(model: model)
    }
    
    func handleListError(list: [(message: String, field: String)]) {
        let result: [(message: String?, textField: UIPPFloatingTextField?)] = list.map {
            guard let fieldName = TextFieldName(rawValue: $0.field) else {
                return ($0.message, contentView.getFieldWith(tag: TextFieldType.name.rawValue))
            }
            
            return ($0.message, contentView.getFieldWith(tag: fieldName.type.rawValue))
        }
        
        update(fields: result)
    }
}
