import UIKit
import UI

enum RegistrationPersonalInfoAction: Equatable {
    case nextScreen(model: PersonalInfoAndHashUser)
    case hashError(snack: ApolloSnackbar)
    case handleError(controller: ApolloFeedbackViewController)
}

protocol RegistrationPersonalInfoCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: RegistrationPersonalInfoAction)
}

final class RegistrationPersonalInfoCoordinator: RegistrationPersonalInfoCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: RegistrationPersonalInfoAction) {
        switch action {
        case .nextScreen(let model):
            guard let controller = viewController as? RegistrationBizFinishProtocol else { return }
            let status = RegistrationStatus(
                hash: model.hash,
                completed: false,
                currentStep: RegistrationFlowStep.person.rawValue,
                sequence: [],
                personalInfo: model.personalInfo
            )
            controller.finishThisStepToRegister(status: status)
        case .hashError(let snack):
            viewController?.showSnackbar(snack)
        case .handleError(let controller):
            viewController?.present(controller, animated: true)
        }
    }
}
