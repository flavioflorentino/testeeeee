import AnalyticsModule
import Foundation
import UI
import Core

private enum RegistrationPersonalErrors: String {
    case invalidHash = "RE-2"
    case invalidCPF = "RE-1"
    case invalidFields = "validation_error"
}

protocol RegistrationPersonalInfoInteracting: AnyObject {
    func update(textFields: [UIPPFloatingTextField])
    func validateAndSubmit(textFields: [UIPPFloatingTextField])
    func submitValues(model: PersonalInfo)
    func trackingViewDidLoad()
}

final class RegistrationPersonalInfoInteractor {
    typealias Localizable = Strings.PersonalInfo
    typealias Dependecies = HasAnalytics
    
    private let dependencies: Dependecies
    private let service: RegistrationPersonalInfoServicing
    private let presenter: RegistrationPersonalInfoPresenting
    
    init(service: RegistrationPersonalInfoServicing, presenter: RegistrationPersonalInfoPresenting, dependencies: Dependecies) {
        self.dependencies = dependencies
        self.service = service
        self.presenter = presenter
    }
}

extension RegistrationPersonalInfoInteractor: RegistrationPersonalInfoInteracting {
    func update(textFields: [UIPPFloatingTextField]) {
        let result: [(message: String?, textField: UIPPFloatingTextField)] = textFields.map {
            switch $0.validate() {
            case .invalid(let error):
                return(error.first?.message, $0)
            default:
                return("", $0)
            }
        }
        
        presenter.updateFields(fields: result)
    }
    
    func validateAndSubmit(textFields: [UIPPFloatingTextField]) {
        let result = textFields.allSatisfy { $0.validate().isValid }
        presenter.validateSubmit(value: result)
    }
    
    func submitValues(model: PersonalInfo) {
        service.registerPersonalInfo(model: model) {[weak self] result in
            switch result {
            case .success(let hashModel):
                guard let hashModel = hashModel else {
                    self?.presenter.presentError()
                    return
                }
                let personalAndHashModel = PersonalInfoAndHashUser(hash: hashModel.hash, personalInfo: model)
                self?.presenter.didNextStep(action: .nextScreen(model: personalAndHashModel))
            case .failure(let error):
                self?.handlerRegistrationErrors(error: error.requestError)
            }
        }
    }
    
    func trackingViewDidLoad() {
        dependencies.analytics.log(RegistrationPersonalInfoAnalytics.didLoadView)
    }
}

private extension RegistrationPersonalInfoInteractor {
    func handlerRegistrationErrors(error: RequestError?) {
        guard let error = error else {
            presenter.presentError()
            return
        }
        
        let errorStatus = RegistrationPersonalErrors(rawValue: error.code)
        
        switch errorStatus {
        case .invalidCPF:
            presenter.presentListError(list: [("document", error.message)])
        case .invalidHash:
            presenter.presentHashError(message: error.message)
        case .invalidFields:
            let list = generatedInvalidFieldList(errors: error.errors)
            presenter.presentListError(list: list)
        default:
            presenter.presentError()
        }
    }
    
    func generatedInvalidFieldList(errors: [RequestError]) -> [(String, String)] {
        errors.compactMap {
            guard let field = $0.field else { return nil }
            return ($0.message, field)
        }
    }
}
