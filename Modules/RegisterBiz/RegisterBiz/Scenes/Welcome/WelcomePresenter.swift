import Foundation

protocol WelcomePresenting: AnyObject {
    var viewController: WelcomeDisplaying? { get set }
    func didNextStep(action: WelcomeAction)
    func hideZeroTax(_ shouldHide: Bool)
}

final class WelcomePresenter {
    private let coordinator: WelcomeCoordinating
    weak var viewController: WelcomeDisplaying?

    init(coordinator: WelcomeCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - WelcomePresenting
extension WelcomePresenter: WelcomePresenting {
    func didNextStep(action: WelcomeAction) {
        coordinator.perform(action: action)
    }
    
    func hideZeroTax(_ shouldHide: Bool) {
        viewController?.hideZeroTax(shouldHide)
    }
}
