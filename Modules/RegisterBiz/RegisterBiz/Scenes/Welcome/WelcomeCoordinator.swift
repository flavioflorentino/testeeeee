import UIKit

enum WelcomeAction {
    case rules
}

protocol WelcomeCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: WelcomeAction)
}

final class WelcomeCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - WelcomeCoordinating
extension WelcomeCoordinator: WelcomeCoordinating {
    func perform(action: WelcomeAction) {
        guard let controller = viewController as? RegistrationBizFinishProtocol else {
            return
        }
        let status = RegistrationStatus(
            completed: false,
            currentStep: RegistrationFlowStep.welcome.rawValue,
            sequence: []
        )
        controller.finishThisStepToRegister(status: status)
    }
}
