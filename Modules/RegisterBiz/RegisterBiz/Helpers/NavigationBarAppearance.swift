import UI
import UIKit

extension UINavigationBar {
    func setup(appearance: NavigationBarAppearance) {
        shadowImage = appearance.shadowImage
        tintColor = appearance.tintColor
        barTintColor = appearance.barTintColor
        barStyle = appearance.barStyle
        isTranslucent = appearance.isTranslucent
        titleTextAttributes = appearance.titleTextAttributes
        setBackgroundImage(appearance.backgroundImage, for: .default)
    }
}

protocol NavigationBarAppearance {
    var shadowImage: UIImage? { get }
    var tintColor: UIColor { get }
    var barTintColor: UIColor { get }
    var barStyle: UIBarStyle { get }
    var isTranslucent: Bool { get }
    var titleTextAttributes: [NSAttributedString.Key: Any] { get }
    var backgroundImage: UIImage? { get }
}

extension NavigationBarAppearance {
    var shadowImage: UIImage? {
        nil
    }
    
    var tintColor: UIColor {
        Colors.backgroundPrimary.color
    }
    
    var barTintColor: UIColor {
        Colors.grayscale600.color
    }
    
    var barStyle: UIBarStyle {
        .default
    }
    
    var isTranslucent: Bool {
        true
    }
    
    var titleTextAttributes: [NSAttributedString.Key: Any] {
        [:]
    }
    
    var backgroundImage: UIImage? {
        nil
    }
}

struct RegisterBizNavBarAppearanceDefault: NavigationBarAppearance {
    let barTintColor: UIColor = Colors.white.lightColor
    let tintColor: UIColor = Colors.branding600.lightColor
    let isTranslucent: Bool = false
    let titleTextAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: Colors.black.lightColor]
    let shadowImage: UIImage? = UIImage()
    let backgroundImage: UIImage? = UIImage()
}
