import Core

protocol RegistrationFlowKeychainHelping {
    var hasPersistedRegistrationProgess: Bool { get }
    func getRegistrationProgress() -> RegistrationStatus?
    func deleteRegistrationProgress() -> Bool
    func saveRegistrationProgress(status: RegistrationStatus)
}

final class RegistrationFlowKeychainHelper: RegistrationFlowKeychainHelping {
    var hasPersistedRegistrationProgess: Bool {
        dependencies.keychain.getData(key: RegisterBizKeychainKey.registerStatus) != nil
    }
    
    typealias Dependencies = HasKeychainManager
    let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func getRegistrationProgress() -> RegistrationStatus? {
        guard let model: RegistrationStatus = dependencies.keychain.getModelData(key: RegisterBizKeychainKey.registerStatus),
            !model.completed else {
            return nil
        }

        return model
    }
    
    func deleteRegistrationProgress() -> Bool {
        dependencies.keychain.clearValue(key: RegisterBizKeychainKey.registerStatus)
    }
    
    func saveRegistrationProgress(status: RegistrationStatus) {
        guard var currentStatus: RegistrationStatus = dependencies.keychain.getModelData(key: RegisterBizKeychainKey.registerStatus) else {
            dependencies.keychain.set(key: RegisterBizKeychainKey.registerStatus, value: status)
            return
        }
        
        currentStatus.currentStep = status.currentStep
        currentStatus.completed = status.completed
        
        if let hash = status.hash, !hash.isEmpty {
            currentStatus.hash = hash
        }
        
        if let personalInfo = status.personalInfo {
            currentStatus.personalInfo = personalInfo
        }
        
        if !status.sequence.isEmpty {
            currentStatus.sequence = status.sequence
        }
        
        dependencies.keychain.set(key: RegisterBizKeychainKey.registerStatus, value: currentStatus)
    }
}
