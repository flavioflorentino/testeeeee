import FeatureFlag

protocol RegistrationFlowServiceHelping {
    var registrationRouteOnFirebase: [RegistrationFlowStep] { get }
}

final class RegistrationFlowServiceHelper: RegistrationFlowServiceHelping {
    var registrationRouteOnFirebase: [RegistrationFlowStep] {
        let route = dependencies.featureManager.text(.featureRegistrationPJSteps)
        return route.split(separator: ",").compactMap {
            RegistrationFlowStep(rawValue: String($0))
        }
    }
    
    typealias Dependencies = HasFeatureManager
    let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}
