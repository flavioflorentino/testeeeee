import Core
import FeatureFlag
import AnalyticsModule

typealias RegisterBizDependencies = HasFeatureManager & HasKeychainManager & HasMainQueue & HasAnalytics

final class DependencyContainer: RegisterBizDependencies {
    lazy var featureManager: FeatureManagerContract = FeatureManager.shared
    lazy var keychain: KeychainManagerContract = KeychainManager()
    lazy var mainQueue = DispatchQueue.main
    lazy var analytics: AnalyticsProtocol = Analytics.shared
}
