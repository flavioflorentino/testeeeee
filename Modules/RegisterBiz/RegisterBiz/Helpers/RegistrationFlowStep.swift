typealias RegistrionController = UIViewController & RegistrationBizFinishProtocol
enum RegistrationFlowStep: String {
    case welcome
    case rulesRegister = "rules_register"
    case person
    case smsCode = "sms_code"
    case personAddress = "person_address"
    case almostThere = "almost_there"
    case feedback
    case aboutCompany = "about_compay"
    case haveLocation = "have_location"
    case companyAddress = "company_address"
    case password
    case terms
    case biometry
    case additionalInformation = "additional_information"
    case configurationAdditional = "configuration_additional"
    case nameCompany = "name_company"
    case fee
    case informationBank = "information_bank"
    case listBank = "list_bank"
    case formBank = "form_bank"
    case confirmInformationBank = "confirm_information_bank"
    case registerComplete = "register_complete"
    case registerCompleteProgress = "register_complete_progress"
    
    func factoryOfController() -> RegistrionController {
        //PARA PROXIMAS PRS
        //IMPLEMENTAR TODOS OS CASES, PROVAVELMENTE SERA ALGO QUE TERA EVOLUCAO GRADUAL
        switch self {
        case .welcome:
            let controller = WelcomeFactory.make()
            return controller
        case .rulesRegister:
            return RegistrationRulesFactory.make()
        case .person:
            return RegistrationPersonalInfoFactory.make()
        case .almostThere:
            return StatusProgressFactory.make(configuration: AlmostThereProgress())
        case .registerComplete:
            return StatusProgressFactory.make(configuration: RegisterCompleteProgress())
        case .personAddress:
            return RegistrationAddressFactory.make()
        default:
            return TempController()
        }
    }
}

//PARA PROXIMAS PRS
//CONTROLLER FAKE E TEMPORARIA ATE COMECAR A CRIAR AS CONTROLLER DEFINITIVAS NO MODULO
private final class TempController: UIViewController, RegistrationBizFinishProtocol {
    weak var flowCoordinatorDelegate: RegistrationBizFinishDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .red
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        let status = RegistrationStatus(completed: false, currentStep: "person", sequence: [])
        finishThisStepToRegister(status: status)
    }
}
