import Core
import Foundation

enum KeychainKey: String, KeychainKeyable {
    case userRegister = "user_register"
    case addressRegister = "address_register"
}
