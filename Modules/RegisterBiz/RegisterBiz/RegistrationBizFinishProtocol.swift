import Foundation

protocol RegistrationBizFinishProtocol: AnyObject {
    var flowCoordinatorDelegate: RegistrationBizFinishDelegate? { get set }
    func finishThisStepToRegister(status: RegistrationStatus)
}

extension RegistrationBizFinishProtocol where Self: UIViewController {
    func finishThisStepToRegister(status: RegistrationStatus) {
        flowCoordinatorDelegate?.finishStep(status: status)
    }
}

protocol RegistrationBizFinishDelegate: AnyObject {
    func finishStep(status: RegistrationStatus)
}
