import Foundation

public protocol RegistrationFlowCoordinatorDelegate: AnyObject {
    func finishFlowRegistration()
}

public final class RegistrationFlowCoordinator {
    // MARK: PRIVATE VARS
    private let navigation: UINavigationController
    
    private var sequence: [RegistrationFlowStep] = []
    private var currentViewController: UIViewController?
    
    private weak var delegate: RegistrationFlowCoordinatorDelegate?
    
    var keychain: RegistrationFlowKeychainHelping = RegistrationFlowKeychainHelper(dependencies: DependencyContainer())
    var service: RegistrationFlowServiceHelping = RegistrationFlowServiceHelper(dependencies: DependencyContainer())
    
    public init(navigation: UINavigationController, delegate: RegistrationFlowCoordinatorDelegate? = nil) {
        self.navigation = navigation
        self.delegate = delegate
    }
    
    // MARK: CONFIGURE FLOW
    public func start() {
        guard keychain.hasPersistedRegistrationProgess else {
            configureNewRegister()
            return
        }

        restoreRegister()
    }
    
    private func configureNewRegister() {
        let steps: [RegistrationFlowStep] = service.registrationRouteOnFirebase
        sequence = steps
        guard let step = steps.first else {
            return
        }
        
        loadStep(step: step)
    }
    
    private func restoreRegister() {
        guard let progress = keychain.getRegistrationProgress(),
            let currentStep = RegistrationFlowStep(rawValue: progress.currentStep) else {
            configureNewRegister()
            return
        }
        
        restoreNavigationSequence(progress: progress.sequence, currentStep: currentStep)
        
        guard let step = nextStep(currentStep: currentStep) else {
            configureNewRegister()
            return
        }
        
        loadStep(step: step)
    }
    
    // MARK: SHOW STEPS
    private func loadStep(step: RegistrationFlowStep) {
        guard !isLastStep(step: step) else {
            finishRegister()
            return
        }
        
        let controller = instantiateViewControllers(step: step)
        currentViewController = controller
        navigation.pushViewController(controller, animated: true)
    }
    
    private func instantiateViewControllers(step: RegistrationFlowStep) -> RegistrionController {
        var controller = step.factoryOfController()
        controller.flowCoordinatorDelegate = self
        return controller
    }
    
    // MARK: VALIDATIONS
    private func isLastStep(step: RegistrationFlowStep) -> Bool {
        step == sequence.last
    }
    
    private func finishRegister() {
        guard keychain.deleteRegistrationProgress() else { return }
        
        delegate?.finishFlowRegistration()
    }
    
    // MARK: UTILS
    private func convertStringStepsToEnumSteps(steps: [String?]) -> [RegistrationFlowStep] {
        steps.compactMap {
            guard let step = $0 else {
                return nil
            }
            return RegistrationFlowStep(rawValue: step)
        }
    }
    
    private func restoreNavigationSequence(progress: [String?], currentStep: RegistrationFlowStep) {
        let sequence = convertStringStepsToEnumSteps(steps: progress)
        self.sequence = sequence
        
        guard let index = sequence.firstIndex(of: currentStep) else {
            configureNewRegister()
            return
        }
        
        let completedSteps = Array(sequence[0..<index])
        let controllers = completedSteps.compactMap {
            instantiateViewControllers(step: $0)
        }
        
        navigation.viewControllers = controllers
    }
    
    private func nextStep(currentStep: RegistrationFlowStep) -> RegistrationFlowStep? {
        guard let index = sequence.firstIndex(of: currentStep) else {
                //PARA OS PROXIMOS PRS
                //ADICIONAR TELA DE ERRO
                return nil
        }
        let nextIndex = index + 1
        guard nextIndex < sequence.count else { return nil }
        return sequence[nextIndex]
    }
    
    private func nextStep(currentStep: String) -> RegistrationFlowStep? {
        guard let step = RegistrationFlowStep(rawValue: currentStep) else {
            return nil
        }
        
        return nextStep(currentStep: step)
    }
}

extension RegistrationFlowCoordinator: RegistrationBizFinishDelegate {
    func finishStep(status: RegistrationStatus) {
        keychain.saveRegistrationProgress(status: status)
        guard let step = nextStep(currentStep: status.currentStep) else {
            return
        }
        loadStep(step: step)
    }
}
