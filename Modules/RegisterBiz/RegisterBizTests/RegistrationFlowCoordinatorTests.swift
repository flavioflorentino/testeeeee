import Foundation
import XCTest
@testable import RegisterBiz

final class RegistrationFlowCoordinatorTests: XCTestCase {
    private lazy var navigationSpy = UINavigationController()
    private lazy var viewControllerSpy = RegistrationViewControllerSpy()
    private lazy var keychainSpy = KeychainHelperSpy()
    private lazy var serviceMock = RegistrationServiceMock()
    lazy var sut: RegistrationFlowCoordinator = {
        let flow = RegistrationFlowCoordinator(navigation: navigationSpy, delegate: viewControllerSpy)
        flow.keychain = keychainSpy
        flow.service = serviceMock
        return flow
    }()

    func testStartFunction_WhenThereIsProgress_ShouldGetProgressInKeychain() {
        keychainSpy.hasRegistration = true
        sut.start()
        XCTAssertEqual(keychainSpy.getProgressCount, 1)
        XCTAssertEqual(serviceMock.getRouteCount, 0)
    }
    
    func testStartFunction_WhenThereIsNoProgress_ShouldGetRouteOnFirebase() {
        keychainSpy.hasRegistration = false
        sut.start()
        XCTAssertEqual(serviceMock.getRouteCount, 1)
        XCTAssertEqual(keychainSpy.getProgressCount, 0)
    }
    
    func testStartFunction_WhenThereIsProgressAndUnkowStep_ShouldStartNewRegister() {
        keychainSpy.hasRegistration = true
        keychainSpy.fakeProgress = RegistrationStatus(completed: false, currentStep: "unkown", sequence: [""])
        sut.start()
        XCTAssertEqual(keychainSpy.getProgressCount, 1)
        XCTAssertEqual(serviceMock.getRouteCount, 1)
    }
    
    func testStartFunction_WhenThereIsProgressAndInvalidSequence_ShouldStartNewRegister() {
        keychainSpy.hasRegistration = true
        keychainSpy.fakeProgress = RegistrationStatus(completed: false, currentStep: "person", sequence: [""])
        sut.start()
        XCTAssertEqual(keychainSpy.getProgressCount, 1)
        XCTAssertEqual(serviceMock.getRouteCount, 1)
    }
    
    func testFinishStep_WhenReceiveActionWithStatusProgressAndNextStepIsSucess_ShouldSaveRegistrationProgress() {
        keychainSpy.hasRegistration = false
        sut.start()
        let fakeProgress = RegistrationStatus(completed: false, currentStep: "person", sequence: ["welcome", "rule_register", "person", "smsCode"])
        sut.finishStep(status: fakeProgress)
        XCTAssertEqual(keychainSpy.saveProgressCount, 1)
    }
    
    func testFinishFlowCoordinator_WhenNextStepIsFinal_ShouldCallDelegateRegistrationCount() {
        serviceMock.finalFlow = true
        sut.start()
        XCTAssertEqual(viewControllerSpy.finishFlowCount, 1)
    }
}

private final class KeychainHelperSpy: RegistrationFlowKeychainHelping {
    var hasRegistration = false
    var deleteSuccess = true
    var fakeProgress = RegistrationStatus(
        completed: false,
        currentStep: "person",
        sequence: ["welcome", "rules_register", "person", "sms_code", "person_address"]
    )
    
    private (set) var getProgressCount = 0
    private (set) var saveProgressCount = 0
    
    var hasPersistedRegistrationProgess: Bool {
        hasRegistration
    }
    
    func getRegistrationProgress() -> RegistrationStatus? {
        getProgressCount += 1
        
        guard hasRegistration else {
            return nil
        }
        
        return fakeProgress
    }
    
    func deleteRegistrationProgress() -> Bool {
        deleteSuccess
    }
    
    func saveRegistrationProgress(status: RegistrationStatus) {
        saveProgressCount += 1
    }
}

private final class RegistrationServiceMock: RegistrationFlowServiceHelping {
    var finalFlow = false
    private (set) var getRouteCount = 0
    var registrationRouteOnFirebase: [RegistrationFlowStep] {
        getRouteCount += 1
        return finalFlow ? [.registerComplete] : [.welcome, .rulesRegister, .person, .smsCode, .personAddress]
    }
}

private final class RegistrationViewControllerSpy: UIViewController, RegistrationFlowCoordinatorDelegate {
    private (set) var finishFlowCount = 0
    func finishFlowRegistration() {
        finishFlowCount += 1
    }
}
