import Foundation
import UIKit

public final class ViewControllerMock: UIViewController {
    private(set) var didPresentControllerCount = 0
    private(set) var didDismissControllerCount = 0
    private(set) var viewControllerPresented = UIViewController()
    
    override public func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        didPresentControllerCount += 1
        viewControllerPresented = viewControllerToPresent
        super.present(viewControllerToPresent, animated: flag, completion: completion)
    }
    
    override public func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        didDismissControllerCount += 1
        super.dismiss(animated: flag, completion: completion)
    }
}
