import AnalyticsModule
import Core
import FeatureFlag
@testable import RegisterBiz

final class DependencyContainerMock: RegisterBizDependencies {
    lazy var keychain: KeychainManagerContract = resolve(default: KeychainManagerMock())
    lazy var mainQueue: DispatchQueue = resolve(default: .main)
    lazy var analytics: AnalyticsProtocol = resolve(default: AnalyticsSpy())
    lazy var featureManager: FeatureManagerContract = resolve(default: FeatureManagerMock())
    
    private let dependencies: [Any]
    
    init(_ dependencies: Any...) {
        self.dependencies = dependencies
    }
}

extension DependencyContainerMock {
    func resolve<T>(default: T) -> T {
        guard let mock = dependencies.first(where: { $0 is T }) as? T else {
            return `default`
        }
        return mock
    }
}
