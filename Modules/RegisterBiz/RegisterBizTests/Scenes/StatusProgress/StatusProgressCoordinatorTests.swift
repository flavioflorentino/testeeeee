import Foundation
import XCTest
import UI
import UIKit
@testable import RegisterBiz

class StatusProgressCoordinatorTests: XCTestCase {
    private lazy var controllerSpy = RegistrationViewControllerSpy()
    private lazy var sut: StatusProgressCoordinating = {
        let coordinator = StatusProgressCoordinator()
        coordinator.viewController = controllerSpy
        return coordinator
    }()
    
    func testPerform_WhenReceiveActionFromPresenter_ShouldCallPerform() {
        sut.perform(action: .finish(step: "almosth_there"))
        XCTAssertEqual(controllerSpy.finishFlowCount, 1)
    }
}

private final class RegistrationViewControllerSpy: UIViewController, RegistrationBizFinishProtocol {
    weak var flowCoordinatorDelegate: RegistrationBizFinishDelegate?
    private (set) var finishFlowCount = 0
    
    func finishThisStepToRegister(status: RegistrationStatus) {
        finishFlowCount += 1
    }
}
