import Foundation
import XCTest
@testable import RegisterBiz

class StatusProgressInteractorTests: XCTestCase {
    private lazy var spy = StatusProgressPresenterSpy()
    private lazy var sut = StatusProgressInteractor(presenter: spy, configuration: AlmostThereProgress())
    
    func testNextStep_WhenReceiveActionFromController_ShouldCallNextStep() {
        sut.nextStep()
        XCTAssertEqual(spy.nextStepCount, 1)
        XCTAssertEqual(spy.currentAciton, .finish(step: "almost_there"))
    }
    
    func testSetupView_WhenREceiveActionFromController_ShouldCallSetupViewInPresenter() {
        sut.setupView()
        XCTAssertEqual(spy.setupViewCount, 1)
    }
}

private final class StatusProgressPresenterSpy: StatusProgressPresenting {
    var viewController: StatusProgressDisplaying?
    
    private(set) var nextStepCount = 0
    private(set) var currentAciton: StatusProgressAction?
    private(set) var setupViewCount = 0
    
    func didNextStep(action: StatusProgressAction) {
        nextStepCount += 1
        currentAciton = action
    }
    
    func setupView(configuration: StatusProgressConfiguration) {
        setupViewCount += 1
    }
}
