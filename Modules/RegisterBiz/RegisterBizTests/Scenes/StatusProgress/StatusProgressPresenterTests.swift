import Foundation
import XCTest
@testable import RegisterBiz

class StatusProgressPresenterTests: XCTestCase {
    private lazy var displaySpy = StatusProgressDisplaySpy()
    private lazy var spy = StatusProgressCoordinatorSpy()
    private lazy var sut: StatusProgressPresenter = {
        let presenter = StatusProgressPresenter(coordinator: spy)
        presenter.viewController = displaySpy
        return presenter
    }()
    
    func testNextStep_WhenReceiveActionFromInteractor_ShouldCallPerform() {
        sut.didNextStep(action: .finish(step: "almost_there"))
        
        XCTAssertEqual(spy.actionCount, 1)
        XCTAssertEqual(spy.currentAction, .finish(step: "almost_there"))
    }
    
    func textSetupView_WhenReceiveActionFromInteractor_ShouldCallSetupInDisplay() {
        sut.setupView(configuration: AlmostThereProgress())
        XCTAssertEqual(displaySpy.setupViewCount, 1)
    }
}

private final class StatusProgressCoordinatorSpy: StatusProgressCoordinating {
    var viewController: RegistrationBizFinishProtocol?
    
    private(set) var actionCount = 0
    private(set) var currentAction: StatusProgressAction?
    
    func perform(action: StatusProgressAction) {
        actionCount += 1
        currentAction = action
    }
}

private final class StatusProgressDisplaySpy: StatusProgressDisplaying {
    private(set) var setupViewCount = 0
    
    func setupView(configuration: StatusProgressConfiguration) {
        setupViewCount += 1
    }
}
