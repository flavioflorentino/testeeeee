import XCTest
import UI
@testable import RegisterBiz

final class RegistrationPersonalInfoCoordinatorTests: XCTestCase {
    private lazy var controllerWithProtocolSpy = ViewControllerSpy()
    private lazy var controllerSpy = ViewControllerMock()
    private lazy var navigationSpy = NavigationControllerSpy(rootViewController: controllerSpy)
    private lazy var hashModel: PersonalInfoAndHashUser = {
        let personalInfo = PersonalInfo(
            name: "nome teste",
            mail: "mail@teste.com",
            cpf: "123456789-00",
            cellPhone: "(11)91234-5678",
            birthday: "01/01/1990",
            motherName: "nome mae teste"
       )
        return PersonalInfoAndHashUser(hash: "hashmockuser", personalInfo: personalInfo)
    }()
    
    private lazy var sut: RegistrationPersonalInfoCoordinator = {
        let coordinator = RegistrationPersonalInfoCoordinator()
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()
    
    func testDidNextStep_WhenReceiveFromPresenter_ShouldCallFinishDelegate() {
        sut.viewController = makeSpy(protocols: true)
        sut.perform(action: .nextScreen(model: hashModel))
        
        XCTAssertEqual(controllerWithProtocolSpy.finishStepRegisterCount, 1)
    }
    
    func testHandleError_WhenReceiveFromPresenterWithoutBizFinishProtocol_ShouldPresentViewController() {
        sut.viewController = makeSpy(protocols: false)
        let attribute = NSAttributedString(string: "teste")
        let viewContent = ApolloFeedbackViewContent(
            type: .icon(style: .success),
            title: "teste",
            description: attribute,
            primaryButtonTitle: "teste"
        )
        sut.perform(action: .handleError(controller: ApolloFeedbackViewController(content: viewContent, style: .confirmation)))
        
        XCTAssertEqual(controllerSpy.didPresentControllerCount, 1)
    }
}

private extension RegistrationPersonalInfoCoordinatorTests {
    func makeSpy(protocols: Bool) -> UIViewController? {
        guard protocols else {
            return NavigationControllerSpy(rootViewController: controllerSpy).topViewController
        }
        
        return NavigationControllerSpy(rootViewController: controllerWithProtocolSpy).topViewController
    }
}

private class ViewControllerSpy: UIViewController, RegistrationBizFinishProtocol {
    weak var flowCoordinatorDelegate: RegistrationBizFinishDelegate?
    var finishStepRegisterCount = 0
    
    func finishThisStepToRegister(status: RegistrationStatus) {
        finishStepRegisterCount += 1
    }
}
