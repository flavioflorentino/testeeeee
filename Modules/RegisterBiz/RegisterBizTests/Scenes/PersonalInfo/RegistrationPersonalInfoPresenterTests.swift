import XCTest
import Validator
import UI
@testable import RegisterBiz

final class RegistrationPersonalInfoPresenterTests: XCTestCase {
    private lazy var hashModel: PersonalInfoAndHashUser = {
        let personalInfo = PersonalInfo(
            name: "nome teste",
            mail: "mail@teste.com",
            cpf: "123456789-00",
            cellPhone: "(11)91234-5678",
            birthday: "01/01/1990",
            motherName: "nome mae teste"
       )
        return PersonalInfoAndHashUser(hash: "hashmockuser", personalInfo: personalInfo)
    }()
    
    private lazy var coordinatorSpy = RegistrationPersonalInfoCoordinatorSpy()
    private lazy var display = RegistrationPersonalInfoDisplaySpy()
    private lazy var sut: RegistrationPersonalInfoPresenter = {
        let presenter = RegistrationPersonalInfoPresenter(coordinator: coordinatorSpy)
        presenter.viewController = display
        return presenter
    }()
    
    func testDidNextStep_WhenReceiveNextScreenActionFromInteractor_ShouldCallNextStepInCoordinator() {
        sut.didNextStep(action: .nextScreen(model: hashModel))
        
        XCTAssertEqual(coordinatorSpy.actionSelected, RegistrationPersonalInfoAction.nextScreen(model: hashModel))
        XCTAssertEqual(display.stopLoadCount, 1)
    }
    
    func testValidateSubmit_WhenReceiveSuccessValueFromInterctor_ShouldCallSubmitValueCount() {
        sut.validateSubmit(value: true)
        
        XCTAssertEqual(display.submitValuesCount, 1)
    }
    
    func testValidateSubmit_WhenIsValueIsFalseFromInteractor_ShouldCallCoordinatorScreenError() {
        sut.validateSubmit(value: false)
        
        XCTAssertEqual(coordinatorSpy.handleErrorCount, 1)
    }
    
    func testUpdateFields_WhenReceiveAllTextFieldsWithErrorsFromInteractor_ShouldUpdateFieldsWithMessageError() {
        let list = [
            ("generic error", UIPPFloatingTextField()),
            ("generic error 2", UIPPFloatingTextField()),
            ("", UIPPFloatingTextField())
        ]
        
        sut.updateFields(fields: list)
        
        XCTAssertEqual(display.listUpdateFields.first?.message, "generic error")
    }
    
    func testUpdateFields_WhenReceiveAllTextFieldsFromInteractor_ShouldUpdateFieldsWithOutMessageError() {
        let list = [
            ("", UIPPFloatingTextField()),
            ("", UIPPFloatingTextField()),
            ("", UIPPFloatingTextField())
        ]
        
        sut.updateFields(fields: list)
        
        XCTAssertEqual(display.listUpdateFields.first?.message, "")
    }
    
    func testHandleError_WhenReceiveActionFromInteractor_ShouldCallCoordinatorScreenError() {
        sut.presentError()
        XCTAssertEqual(coordinatorSpy.handleErrorCount, 1)
        XCTAssertEqual(display.stopLoadCount, 1)
    }
    
    func testPresentListError_WhenReceiveActionFromInteractor_ShouldCallDisplayListErrorCount() {
        let list = [
            ("generic error", "name"),
            ("generic error 2", "email"),
            ("", "document")
        ]
        
        sut.presentListError(list: list)
        
        XCTAssertEqual(display.handleListErrorCount, 1)
        XCTAssertEqual(display.stopLoadCount, 1)
    }
    
    func testPresentHashError_WhenReceiveActionFromInteractor_ShouldCallCoordinatorPerformCount() {
        sut.presentHashError(message: "hash user error")
        
        XCTAssertEqual(coordinatorSpy.actionSelected, RegistrationPersonalInfoAction.hashError(snack: ApolloSnackbar(text: "hash user error", iconType: .error)))
        XCTAssertEqual(display.stopLoadCount, 1)
    }
}

private class RegistrationPersonalInfoCoordinatorSpy: RegistrationPersonalInfoCoordinating {
    var viewController: UIViewController?
    private(set) var actionSelected: RegistrationPersonalInfoAction?
    private(set) var nextScreenCount = 0
    private(set) var hashErrorCount = 0
    private(set) var handleErrorCount = 0
    
    func perform(action: RegistrationPersonalInfoAction) {
        actionSelected = action
        switch action {
        case .nextScreen:
            nextScreenCount += 1
        case .hashError:
            hashErrorCount += 1
        case .handleError:
            handleErrorCount += 1
        }
    }
}

private class RegistrationPersonalInfoDisplaySpy: RegistrationPersonalInfoDisplay {
    private(set) var submitValuesCount = 0
    private(set) var listUpdateFields: [(message: String?, textField: UIPPFloatingTextField?)] = []
    private(set) var handleListErrorCount = 0
    private(set) var startLoadCount = 0
    private(set) var stopLoadCount = 0
    
    func update(fields: [(message: String?, textField: UIPPFloatingTextField?)]) {
        listUpdateFields = fields
    }
    
    func submitValues() {
        submitValuesCount += 1
    }
    
    func handleListError(list: [(message: String, field: String)]) {
        handleListErrorCount += 1
    }
    
    func startApolloLoader(loadingText: String) {
        startLoadCount += 1
    }
    
    func stopApolloLoader(completion: (() -> Void)?) {
        stopLoadCount += 1
    }
}
