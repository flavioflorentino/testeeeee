import XCTest
import Core
import Validator
import UI
import AnalyticsModule
@testable import RegisterBiz

final class RegistrationPersonalInfoInteractorTests: XCTestCase {
    private let service = RegistrationPersonalInfoServiceMock()
    private let presenter = RegistrationPersonalInfoPresenterSpy()
    private let analyticsSpy = AnalyticsSpy()
    private lazy var container = DependencyContainerMock(analyticsSpy)
    private lazy var sut = RegistrationPersonalInfoInteractor(service: service, presenter: presenter, dependencies: container)
    
    func testSubmitValue_WhenSubmitIsCPFFailureFromViewController_ShouldCallHandleError() {
        service.isSuccess = false
        service.codeError = "RE-1"
        
        let moodelMock = PersonalInfo(
            name: "nome usuario",
            mail: "nome@usuario.com",
            cpf: "12345678900",
            cellPhone: "11123456789",
            birthday: "10/10/1990",
            motherName: "nome mae usuario"
        )
        
        sut.submitValues(model: moodelMock)
        
        XCTAssertNotNil(presenter.listError)
        XCTAssertEqual(presenter.handleListErrorCount, 1)
    }
    
    func testSubmitValue_WhenSubmitIsHashFailureFromViewController_ShouldCallHandleError() {
        service.isSuccess = false
        service.codeError = "RE-2"
        
        let moodelMock = PersonalInfo(
            name: "nome usuario",
            mail: "nome@usuario.com",
            cpf: "12345678900",
            cellPhone: "11123456789",
            birthday: "10/10/1990",
            motherName: "nome mae usuario"
        )
        
        sut.submitValues(model: moodelMock)

        XCTAssertEqual(presenter.handleErrorCount, 1)
        XCTAssertEqual(presenter.error, "Ocorreu um erro ao carregar as informações, tente novamente.")
    }
    
    func testSubmitValue_WhenSubmitIsFieldsFailFromViewController_ShouldCallHandleError() {
        service.isSuccess = false
        service.codeError = "validation_error"
        
        let moodelMock = PersonalInfo(
            name: "nome usuario",
            mail: "nome@usuario.com",
            cpf: "12345678900",
            cellPhone: "11123456789",
            birthday: "10/10/1990",
            motherName: "nome mae usuario"
        )
        
        sut.submitValues(model: moodelMock)
        
        XCTAssertNotNil(presenter.listError)
        XCTAssertEqual(presenter.handleListErrorCount, 1)
    }
    
    func testSubmitValue_WhenSubmitIsFailureFromViewController_ShouldCallHandleError() {
        service.isSuccess = false
        
        let moodelMock = PersonalInfo(
            name: "nome usuario",
            mail: "nome@usuario.com",
            cpf: "12345678900",
            cellPhone: "11123456789",
            birthday: "10/10/1990",
            motherName: "nome mae usuario"
        )
        
        sut.submitValues(model: moodelMock)

        XCTAssertEqual(presenter.handleErrorCount, 1)
    }
    
    func testSubmitValues_WhenSubmitIsSuccessFromViewController_ShouldCallNextStep() {
        service.isSuccess = true
        
        let moodelMock = PersonalInfo(
            name: "nome usuario",
            mail: "nome@usuario.com",
            cpf: "12345678900",
            cellPhone: "11123456789",
            birthday: "10/10/1990",
            motherName: "nome mae usuario"
        )
        
        let personalInfoModel = PersonalInfoAndHashUser(hash: "hashmockuser", personalInfo: moodelMock)
        
        sut.submitValues(model: moodelMock)
        
        XCTAssertEqual(presenter.actionSelected, RegistrationPersonalInfoAction.nextScreen(model: personalInfoModel))
    }
    
    func testSubmitValues_WhenSubmitIsSuccessWithoutModelFromViewController_ShouldCallNextStep() {
        service.isSuccess = true
        service.isHasModel = false
        
        let moodelMock = PersonalInfo(
            name: "nome usuario",
            mail: "nome@usuario.com",
            cpf: "12345678900",
            cellPhone: "11123456789",
            birthday: "10/10/1990",
            motherName: "nome mae usuario"
        )
        
        sut.submitValues(model: moodelMock)
        
        XCTAssertEqual(presenter.handleErrorCount, 1)
    }
    
    func testValidateAndSubmitSuccess_WhenReceiveAllTextFieldsFormViewController_ShouldCallValidateSubmitInPresenter() {
        let list = createPPFloatingFields(with: true)
        sut.validateAndSubmit(textFields: list)
        
        XCTAssertTrue(presenter.isValidated)
    }
    
    func testValidateAndSubmitError_WhenReceiveAllTextFieldsFormViewController_ShouldCallValidateSubmitInPresenter() {
        let list = createPPFloatingFields(with: false)
        sut.validateAndSubmit(textFields: list)
        
        XCTAssertFalse(presenter.isValidated)
    }
    
    func testUpdateValues_WhenReceiveAllTextFieldsWithErrorsFromViewController_ShouldUpdateFieldsWithMessageError() {
        let list = createPPFloatingFields(with: false)
        sut.update(textFields: list)
        
        XCTAssertEqual(presenter.listUpdateFields.first?.message, "generic error")
    }
    
    func testUpdateValues_WhenReceiveAllTextFieldsFromViewController_ShouldUpdateFieldsWithOutMessageError() {
        let list = createPPFloatingFields(with: true)
        
        sut.update(textFields: list)
        
        XCTAssertEqual(presenter.listUpdateFields.first?.message, "")
    }
    
    func testTrackingViewDidLoad_WhenReceiveActionFromViewController_ShouldCallTrackingCount() {
        sut.trackingViewDidLoad()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }
}

// MARK: - Private functios used by configuration
private extension RegistrationPersonalInfoInteractorTests {
    private enum TestsValidateError: String, ValidationError {
        case generic = "generic error"
        
        var message: String {
            rawValue
        }
    }
    
    private func createPPFloatingFields(with text: Bool) -> [UIPPFloatingTextField] {
        let nameTextField = UIPPFloatingTextField()
        let mailTextField = UIPPFloatingTextField()
        let cpfTextField = UIPPFloatingTextField()
        let cellPhoneTextField = UIPPFloatingTextField()
        let birthdayTextField = UIPPFloatingTextField()
        let motherNameTextField = UIPPFloatingTextField()
        
        if text {
            nameTextField.text = "Nome usuário teste"
            mailTextField.text = "email@usuario.com"
            cpfTextField.text = "123.456.789-00"
            cellPhoneTextField.text = "(11) 91234-5678"
            birthdayTextField.text = "10/10/1990"
            motherNameTextField.text = "Nome mãe usuário"
        }
        
        let list = [nameTextField, mailTextField, cpfTextField, cellPhoneTextField, birthdayTextField, motherNameTextField]
        configureValidatorsFields(inputs: list)
        
        return list
    }
    
    private func configureValidatorsFields(inputs: [UIPPFloatingTextField]) {
        var nameRule = ValidationRuleSet<String>()
        var nameInput = inputs[0]
        nameRule.add(rule: ValidationRuleLength(min: 10, error: TestsValidateError.generic))
        nameInput.validationRules = nameRule

        var emailRules = ValidationRuleSet<String>()
        var mailInput = inputs[1]
        emailRules.add(rule: ValidationRulePattern(pattern: EmailValidationPattern.standard, error: TestsValidateError.generic))
        mailInput.validationRules = emailRules

        var cpfRules = ValidationRuleSet<String>()
        var cpfInput = inputs[2]
        cpfRules.add(rule: ValidationRuleLength(min: 14, error: TestsValidateError.generic))
        cpfInput.validationRules = cpfRules

        var birthdayRules = ValidationRuleSet<String>()
        var birthdayInput = inputs[3]
        birthdayRules.add(rule: ValidationRuleLength(min: 10, error: TestsValidateError.generic))
        birthdayInput.validationRules = birthdayRules

        var motherNameRules = ValidationRuleSet<String>()
        var motherNameInput = inputs[4]
        motherNameRules.add(rule: ValidationRuleLength(min: 5, error: TestsValidateError.generic))
        motherNameInput.validationRules = motherNameRules
    }
}

// MARK: - Mock Class
private final class RegistrationPersonalInfoServiceMock: RegistrationPersonalInfoServicing {
    var isSuccess = true
    var codeError: String = ""
    var isHasModel = true
    private let mockModel = HashUser(hash: "hashmockuser")
    
    func registerPersonalInfo(model: PersonalInfo, _ completion: @escaping (Result<HashUser?, ApiError>) -> Void) {
        guard isSuccess else {
            var requestError = RequestError()
            requestError.code = codeError
            completion(.failure(ApiError.badRequest(body: requestError)))
            return
        }
        
        completion(.success(isHasModel ? mockModel : nil))
    }
}

// MARK: - Spy Class
private final class RegistrationPersonalInfoPresenterSpy: RegistrationPersonalInfoPresenting {
    var viewController: RegistrationPersonalInfoDisplay?
    
    private(set) var actionSelected: RegistrationPersonalInfoAction?
    private(set) var handleErrorCount = 0
    private(set) var handleListErrorCount = 0
    private(set) var error: String?
    private(set) var listUpdateFields: [(message: String?, textField: UIPPFloatingTextField)] = []
    private(set) var isValidated = false
    private(set) var listError: [(message: String, field: String)]?
    
    func updateFields(fields: [(message: String?, textField: UIPPFloatingTextField)]) {
        listUpdateFields = fields
    }
    
    func didNextStep(action: RegistrationPersonalInfoAction) {
        actionSelected = action
    }
    
    func validateSubmit(value: Bool) {
        isValidated = value
    }
    
    func presentError() {
        handleErrorCount += 1
    }
    
    func presentListError(list: [(message: String, field: String)]) {
        listError = list
        handleListErrorCount += 1
    }
    
    func presentHashError(message: String) {
        handleErrorCount += 1
        error = message
    }
}
