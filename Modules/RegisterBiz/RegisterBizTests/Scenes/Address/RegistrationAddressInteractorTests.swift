import XCTest
import Core
import Validator
import UI
@testable import RegisterBiz

// MARK: - Spy Class
private final class RegistrationAddressPresenterSpy: RegistrationAddressPresenting {
    var viewController: RegistrationAddressDisplaying?
    
    private(set) var actionSelected: RegistrationAddressAction?
    private(set) var presentErrorCount = 0
    private(set) var listUpdateFields: [(message: String?, textField: UIPPFloatingTextField)] = []
    private(set) var isValidated = false
    
    func updateFields(fields: [(message: String?, textField: UIPPFloatingTextField)]) {
        listUpdateFields = fields
    }
    
    func didNextStep(action: RegistrationAddressAction) {
        actionSelected = action
    }
    
    func validateSubmit(value: Bool) {
        isValidated = value
    }
    
    func presentError() {
        presentErrorCount += 1
    }
}

// MARK: - Mock Class
private final class RegistrationAddressServiceMock: RegistrationAddressServicing {
    var isSuccess = true
    
    func registerAddress(model: AddressModel, _ completion: @escaping (Result<VerifyAddress, ApiError>) -> Void) {
        guard isSuccess else {
            completion(.failure(ApiError.serverError))
            return
        }
        
        let model = VerifyAddress(verify: true)
        completion(.success(model))
    }
}

final class RegistrationAddressInteractorTests: XCTestCase {
    private let service = RegistrationAddressServiceMock()
    private let presenter = RegistrationAddressPresenterSpy()
    private lazy var sut = RegistrationAddressInteractor(service: service, presenter: presenter)
    private let addressMock = AddressModel(
                                cep: "00000-000",
                                street: "Nome rua teste",
                                number: "000",
                                complement: "Complemento teste",
                                district: "Bairro teste",
                                city: "Cidade teste",
                                state: "Estado teste")
    
    func testUpdateValues_WhenReceiveAllTextFieldsValidFromViewController_ShouldUpdateFields() {
        let list = createPPFloatingFields(with: true)
        sut.update(textFields: list)
        
        XCTAssertEqual(presenter.listUpdateFields.first?.message, "")
    }
    
    func testValidateAndSubmit_WhenReceiveAllTextFieldsFromService_ShouldValidateSubmitInPresenter() {
        let list = createPPFloatingFields(with: true)
        sut.validateAndSubmit(textFields: list)
        
        XCTAssertTrue(presenter.isValidated)
    }
    
    func testSubmitValues_WhenRegisterReturnsSuccessFromViewController_ShouldCallNextStep() {
        service.isSuccess = true
        sut.submitValues(model: addressMock)
        
        XCTAssertEqual(presenter.actionSelected, RegistrationAddressAction.nextScreen(model: addressMock))
    }
    
    func testSubmitValues_WhenSubmitIsFailureFromViewController_ShouldHandleError() {
        service.isSuccess = false
        sut.submitValues(model: addressMock)
        
        XCTAssertEqual(presenter.presentErrorCount, 1)
    }
}

// MARK: - Private functions used by configuration
private extension RegistrationAddressInteractorTests {
    private func createPPFloatingFields(with text: Bool) -> [UIPPFloatingTextField] {
        let cepTextField = UIPPFloatingTextField()
        let streetTextField = UIPPFloatingTextField()
        let numberTextField = UIPPFloatingTextField()
        let complementTextField = UIPPFloatingTextField()
        let districtTextField = UIPPFloatingTextField()
        let cityTextField = UIPPFloatingTextField()
        let stateTextField = UIPPFloatingTextField()
        
        if text {
            cepTextField.text = "00000-000"
            streetTextField.text = "Nome rua teste"
            numberTextField.text = "000"
            complementTextField.text = "Complemento teste"
            districtTextField.text = "Bairro teste"
            cityTextField.text = "Cidade teste"
            stateTextField.text = "Estado teste"
        }
        
        let list = [
            cepTextField,
            streetTextField,
            numberTextField,
            complementTextField,
            districtTextField,
            cityTextField,
            stateTextField
        ]
        return list
    }
}
