import XCTest
import Validator
import UI
@testable import RegisterBiz

// MARK: - Spy Classes
private class RegistrationAddressCoordinatorSpy: RegistrationAddressCoordinating {
    var viewController: UIViewController?
    private(set) var actionSelected: RegistrationAddressAction?
    
    func perform(action: RegistrationAddressAction) {
        actionSelected = action
    }
}

private class RegistrationAddressDisplaySpy: RegistrationAddressDisplaying {
    private(set) var errorCount = 0
    private(set) var listErrorCount = 0
    private(set) var submitValuesCount = 0
    private(set) var genericErrorCount = 0
    
    private(set) var listUpdateFields: [(message: String?, textField: UIPPFloatingTextField?)] = []
    
    private(set) var startApolloLoaderCount = 0
    private(set) var stopApolloLoaderCount = 0
    
    func handleError() {
        errorCount += 1
    }
    
    func update(fields: [(message: String?, textField: UIPPFloatingTextField?)]) {
        listUpdateFields = fields
    }
    
    func submitValues() {
        submitValuesCount += 1
    }

    func startApolloLoader(loadingText: String) {
        startApolloLoaderCount += 1
    }
    
    func stopApolloLoader(completion: (() -> Void)?) {
        stopApolloLoaderCount += 1
    }
    
    func handleListError(list: [(message: String, field: String)]) {
        listErrorCount += 1
    }
}

final class RegistrationAddressPresenterTests: XCTestCase {
    private lazy var coordinator = RegistrationAddressCoordinatorSpy()
    private lazy var display = RegistrationAddressDisplaySpy()
    
    private let addressMock = AddressModel(
                                cep: "00000-000",
                                street: "Nome rua teste",
                                number: "000",
                                complement: "Complemento teste",
                                district: "Bairro teste",
                                city: "Cidade teste",
                                state: "Estado teste")
    
    private lazy var sut: RegistrationAddressPresenter = {
        let presenter = RegistrationAddressPresenter(coordinator: coordinator)
        presenter.viewController = display
        return presenter
    }()
    
    func testDidNextStep_WhenReceiveNextScreenActionFromInteractor_ShouldCallNextStepInCoordinator() {
        sut.didNextStep(action: .nextScreen(model: addressMock))
        
        XCTAssertEqual(coordinator.actionSelected, RegistrationAddressAction.nextScreen(model: addressMock))
    }
    
    func testUpdateFields_WhenReceiveAllTextFieldsWithoutErrorsFromInteractor_ShouldUpdateFields() {
        let list = [
            ("", UIPPFloatingTextField()),
            ("", UIPPFloatingTextField()),
            ("", UIPPFloatingTextField())
        ]
        sut.updateFields(fields: list)
        
        XCTAssertEqual(display.listUpdateFields.first?.message, "")
    }
    
    func testUpdateFields_WhenReceiveAllTextFieldsWithErrorsFromInteractor_ShouldUpdateFieldsWithMessageError() {
        let list = [
            ("generic error", UIPPFloatingTextField()),
            ("generic error 2", UIPPFloatingTextField()),
            ("", UIPPFloatingTextField())
        ]
        sut.updateFields(fields: list)
        
        XCTAssertEqual(display.listUpdateFields.first?.message, "generic error")
    }
    
    func testValidateSubmit_WhenReceiveSuccesValueFromInteractor_ShouldCallSubmitValue() {
        sut.validateSubmit(value: true)
        
        XCTAssertEqual(display.submitValuesCount, 1)
    }
    
    func testValidateSubmit_WhenValueIsFalse_ShouldCallCoordinatorScreenError() {
        sut.validateSubmit(value: false)
        XCTAssertEqual(display.stopApolloLoaderCount, 1)
    }
}
