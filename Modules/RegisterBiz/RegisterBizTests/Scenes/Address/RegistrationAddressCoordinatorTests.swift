import Foundation
import XCTest
import UI
import UIKit
@testable import RegisterBiz

private final class RegistrationAddressViewControllerSpy: UIViewController, RegistrationBizFinishProtocol {
    weak var flowCoordinatorDelegate: RegistrationBizFinishDelegate?
    private (set) var finishFlowCount = 0
    
    func finishThisStepToRegister(status: RegistrationStatus) {
        finishFlowCount += 1
    }
}

class RegistrationAddressCoordinatorTests: XCTestCase {
    private lazy var controllerSpy = RegistrationAddressViewControllerSpy()
    private lazy var sut: RegistrationAddressCoordinating = {
        let coordinator = RegistrationAddressCoordinator()
        coordinator.viewController = controllerSpy
        return coordinator
    }()
    private let addressMock = AddressModel(
                                cep: "00000-000",
                                street: "Nome rua teste",
                                number: "000",
                                complement: "Complemento teste",
                                district: "Bairro teste",
                                city: "Cidade teste",
                                state: "Estado teste")
    
    func testDidNextStep_WhenReceiveNextScreenFromPresenter_ShouldPushNextViewController() {
        sut.perform(action: .nextScreen(model: addressMock))
        
        XCTAssertEqual(controllerSpy.finishFlowCount, 1)
    }
}
