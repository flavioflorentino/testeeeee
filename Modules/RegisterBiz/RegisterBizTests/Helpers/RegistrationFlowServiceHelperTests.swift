import XCTest
import FeatureFlag
@testable import RegisterBiz

final class RegistrationFlowServiceHelperTests: XCTestCase {
    private lazy var featureMock = FeatureManagerMock()
    private lazy var dependenciesMock = DependencyContainerMock(featureMock)
    lazy var sut = RegistrationFlowServiceHelper(dependencies: dependenciesMock)
    
    func testGetRegistrationSteps_WhenReceiveAction_ShouldReturnRegistrationStepsList() {
        let expectedList: [RegistrationFlowStep] = [.welcome, .rulesRegister, .person, .smsCode, .personAddress]
        featureMock.override(key: .featureRegistrationPJSteps, with: "welcome,rules_register,person,sms_code,person_address")
        let list = sut.registrationRouteOnFirebase
        XCTAssertEqual(list, expectedList)
    }
}
