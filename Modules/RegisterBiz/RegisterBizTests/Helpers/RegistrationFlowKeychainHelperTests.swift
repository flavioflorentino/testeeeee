import XCTest
import Core
@testable import RegisterBiz

final class RegistrationFlowKeychainHelperTests: XCTestCase {
    private lazy var keychainMock = KeychainManagerMock()
    private lazy var dependenciesMock = DependencyContainerMock(keychainMock)
    lazy var sut = RegistrationFlowKeychainHelper(dependencies: dependenciesMock)
    
    func testHasPersistedRegistrationProgess_WhenNoHaveProgress_ShouldReturnFalseValue() {
        keychainMock.clearAll()
        XCTAssertFalse(sut.hasPersistedRegistrationProgess)
    }
    
    func testhasPersistedRegistrationProgess_WhenHaveProgress_ShouldReturnTrueValue() {
        let fakeProgress = RegistrationStatus(completed: false, currentStep: "person", sequence: [])
        keychainMock.set(key: RegisterBizKeychainKey.registerStatus, value: fakeProgress)
        XCTAssertTrue(sut.hasPersistedRegistrationProgess)
    }
    
    func testDeleteProgress_WhenNoHaveProgress_ShouldReturnTrueValue() {
        keychainMock.clearAll()
        XCTAssertTrue(sut.deleteRegistrationProgress())
    }
    
    func testDeleteProgress_WhenHaveProgress_ShouldReturnTrueValue() {
        let fakeProgress = RegistrationStatus(completed: false, currentStep: "person", sequence: [])
        keychainMock.set(key: RegisterBizKeychainKey.registerStatus, value: fakeProgress)
        XCTAssertTrue(sut.deleteRegistrationProgress())
    }
    
    func testGetRegisterProgress_WhenHavePogressIncomplete_ShouldReturnRegistrationStatusModel() {
        let fakeProgress = RegistrationStatus(completed: false, currentStep: "person", sequence: [])
        keychainMock.set(key: RegisterBizKeychainKey.registerStatus, value: fakeProgress)
        XCTAssertNotNil(sut.getRegistrationProgress)
    }
    
    func testGetRegisterProgress_WhenNoHavePogress_ShouldReturnNilValue() {
        keychainMock.clearAll()
        XCTAssertNil(sut.getRegistrationProgress())
    }
    
    func testGetRegisterProgress_WhenHavePogressComplete_ShouldReturnNilValue() {
        let fakeProgress = RegistrationStatus(completed: true, currentStep: "person", sequence: [])
        keychainMock.set(key: RegisterBizKeychainKey.registerStatus, value: fakeProgress)
        XCTAssertNil(sut.getRegistrationProgress())
    }
    
    func testSaveRegisterProgress_WhenValueIsComplete_ShouldHaveValueInKeychain() {
        let fakeProgress = RegistrationStatus(completed: false, currentStep: "person", sequence: ["welcome", "person"])
        sut.saveRegistrationProgress(status: fakeProgress)
        let model: RegistrationStatus? = keychainMock.getModelData(key: RegisterBizKeychainKey.registerStatus)
        XCTAssertNotNil(model)
        XCTAssertEqual(model?.sequence.count, 2)
        XCTAssertNotNil(model?.currentStep)
    }
    
    func testSaveRegisterProgress_WhenValueNoHaveSequence_ShouldHaveValueInKeychain() {
        let fakeProgress = RegistrationStatus(completed: false, currentStep: "person", sequence: [])
        sut.saveRegistrationProgress(status: fakeProgress)
        let model: RegistrationStatus? = keychainMock.getModelData(key: RegisterBizKeychainKey.registerStatus)
        XCTAssertNotNil(model)
        XCTAssertEqual(model?.sequence.count, 0)
    }
    
    func testSaveRegisterProgress_WhenValueNoHaveName_ShouldHaveValueInKeychain() {
        let fakeProgress = RegistrationStatus(completed: false, currentStep: "person", sequence: [], personalInfo: nil)
        sut.saveRegistrationProgress(status: fakeProgress)
        let model: RegistrationStatus? = keychainMock.getModelData(key: RegisterBizKeychainKey.registerStatus)
        XCTAssertNotNil(model)
        XCTAssertNil(model?.personalInfo)
    }
    
    func testSaveRegisterProgress_WhenHaveValueInKeychain_ShouldHaveValueInKeychain() {
        let fakeProgress = RegistrationStatus(completed: false, currentStep: "person", sequence: [], personalInfo: nil)
        keychainMock.set(key: RegisterBizKeychainKey.registerStatus, value: fakeProgress)
        
        let personalFake = PersonalInfo(
            name: "name",
            mail: "mail@mail.com",
            cpf: "123456789-00",
            cellPhone: "(11)91234-5678",
            birthday: "01/01/1990",
            motherName: "nome mae teste"
        )
        let overrideFakeProgress = RegistrationStatus(completed: false, currentStep: "adress", sequence: [], personalInfo: personalFake)
        sut.saveRegistrationProgress(status: overrideFakeProgress)
        let model: RegistrationStatus? = keychainMock.getModelData(key: RegisterBizKeychainKey.registerStatus)
        XCTAssertNotNil(model)
        XCTAssertEqual(model?.currentStep, "adress")
        XCTAssertEqual(model?.personalInfo?.name, "name")
    }
}
