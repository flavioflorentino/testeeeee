import UI
import XCTest
@testable import ReceiptKit

private final class DefaultReceiptDisplayingSpy: DefaultReceiptDisplaying {
    private(set) var callDisplayLoadingStateCount = 0
    private(set) var callDisplaySuccessStateCount = 0
    private(set) var viewModels: [ReceiptCellViewModeling]?
    private(set) var callDisplayErrorStateCount = 0
    private(set) var errorViewModel: StatefulFeedbackViewModel?
    
    func displayLoadingState() {
        callDisplayLoadingStateCount += 1
    }
    
    func displaySuccessState(with viewModels: [ReceiptCellViewModeling]) {
        callDisplaySuccessStateCount += 1
        self.viewModels = viewModels
    }
    
    func displayErrorState(with viewModel: StatefulFeedbackViewModel) {
        callDisplayErrorStateCount += 1
        errorViewModel = viewModel
    }
}

private final class DefaultReceiptCoordinatorSpy: DefaultReceiptCoordinating {
    var viewController: UIViewController?
    
    private(set) var callPerformCount = 0
    private(set) var action: DefaultReceiptAction?
    
    func perform(action: DefaultReceiptAction) {
        callPerformCount += 1
        self.action = action
    }
}

final class DefaultReceiptPresenterTests: XCTestCase {
    private let viewControllerSpy = DefaultReceiptDisplayingSpy()
    private let coordinatorSpy = DefaultReceiptCoordinatorSpy()
    private lazy var sut: DefaultReceiptPresenting = {
        let sut = DefaultReceiptPresenter(coordinator: coordinatorSpy)
        sut.viewController = viewControllerSpy
        return sut
    }()
    
    func testPresentLoading_ShouldCallViewController() {
        sut.presentLoading()
     
        XCTAssertEqual(viewControllerSpy.callDisplayLoadingStateCount, 1)
    }
    
    func testPresentReceipt_ShouldCallViewControllerWithTheRightValues() {
        let model = ReceiptModel(id: "2", itens: [])
        sut.presentReceipt(with: model)
     
        XCTAssertEqual(viewControllerSpy.callDisplaySuccessStateCount, 1)
        XCTAssertEqual(viewControllerSpy.viewModels?.count, 0)
    }
    
    func testPresentError_ShouldCallViewControllerWithTheRightValues() {
        sut.presentError()
     
        XCTAssertEqual(viewControllerSpy.callDisplayErrorStateCount, 1)
        XCTAssertEqual(viewControllerSpy.errorViewModel?.content?.title, Strings.Error.title)
        XCTAssertEqual(viewControllerSpy.errorViewModel?.content?.description, Strings.Error.message)
    }
    
    func testPresentAction_ShouldCallTheCoordinatorWithTheRightAction() {
        sut.present(action: .close)
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .close)
    }
}
