import XCTest
@testable import ReceiptKit

final class DefaultReceiptCoordinatorTests: XCTestCase {
    private let dependenciesMock = DependencyContainerMock()
    private let viewControllerSpy = ViewControllerSpy()
    private lazy var navControllerSpy = NavigationControllerSpy(rootViewController: viewControllerSpy)
    private lazy var sut: DefaultReceiptCoordinating = {
        let coordinator = DefaultReceiptCoordinator(dependencies: dependenciesMock)
        coordinator.viewController = navControllerSpy.topViewController
        return coordinator
    }()
    
    func testPerform_WhenActionIsMGM_ShouldOpenMGMViewController() throws {
        sut.perform(action: .mgm)
        
        let mgmSpy = try XCTUnwrap(dependenciesMock.legacy.mgm as? MGMSpy)
        XCTAssertEqual(mgmSpy.callOpenCount, 1)
    }
    
    func testPerform_WhenActionIsFaq_ShouldOpenDeeplink() throws {
        sut.perform(action: .faq("https://meajuda.picpay.com"))
        
        let faqSpy = try XCTUnwrap(dependenciesMock.legacy.deeplink as? DeeplinkSpy)
        XCTAssertEqual(faqSpy.callOpenCount, 1)
    }
    
    func testPerform_WhenActionIsBilletPayment_ShouldOpenBilletFlow() throws {
        sut.perform(action: .billetPayment)
        
        let billetSpy = try XCTUnwrap(dependenciesMock.legacy.billet as? BilletSpy)
        XCTAssertEqual(billetSpy.callOpenCount, 1)
    }
    
    func testPerform_WhenActionIsClose_ShouldCloseReceiptViewController() {
        sut.perform(action: .close)
        
        XCTAssertEqual(viewControllerSpy.callDismissControllerCount, 1)
    }
}
