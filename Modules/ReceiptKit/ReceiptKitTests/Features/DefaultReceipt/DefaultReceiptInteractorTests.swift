import Core
import XCTest
@testable import ReceiptKit

private final class DefaultReceiptPresenterSpy: DefaultReceiptPresenting {
    var viewController: DefaultReceiptDisplaying?
    
    private(set) var callPresentLoadingCount = 0
    private(set) var callPresentReceiptCount = 0
    private(set) var model: ReceiptModel?
    private(set) var callPresentErrorCount = 0
    private(set) var callPresentActionCount = 0
    private(set) var action: DefaultReceiptAction?
    
    func presentLoading() {
        callPresentLoadingCount += 1
    }
    
    func presentReceipt(with model: ReceiptModel) {
        callPresentReceiptCount += 1
        self.model = model
    }
    
    func presentError() {
        callPresentErrorCount += 1
    }
    
    func present(action: DefaultReceiptAction) {
        callPresentActionCount += 1
        self.action = action
    }
}

final class BilletFormInteractortTests: XCTestCase {
    private let model = ReceiptModel(id: "2", itens: [])
    private let presenterSpy = DefaultReceiptPresenterSpy()
    private lazy var sut = DefaultReceiptInteractor(model: model, presenter: presenterSpy)
    
    func testFetchReceiptIfNeeded_WhenInteractorWasInstantiatedWithAModel_ShouldCallPresenterWithTheRightValues() {
        sut.fetchReceiptIfNeeded()
        
        XCTAssertEqual(presenterSpy.callPresentLoadingCount, 1)
        XCTAssertEqual(presenterSpy.callPresentReceiptCount, 1)
    }
    
    func testFetchReceiptIfNeeded_WhenInteractorWasInstantiatedWithAServiceAndServiceReturnASuccess_ShouldCallPresenterWithTheRightValues() {
        let entryPoint = ReceiptEntryPoint(id: "2", type: .pav)
        let service = DefaultReceiptServiceMock(entryPoint: entryPoint)
        sut = DefaultReceiptInteractor(service: service, presenter: presenterSpy)
        
        let model = ReceiptModel(id: "2", itens: [])
        service.expectedResult = .success(model)
        
        sut.fetchReceiptIfNeeded()
        
        XCTAssertEqual(presenterSpy.callPresentLoadingCount, 1)
        XCTAssertEqual(presenterSpy.callPresentReceiptCount, 1)
        XCTAssertEqual(presenterSpy.model, model)
    }
    
    func testFetchReceiptIfNeeded_WhenInteractorWasInstantiatedWithAServiceAndServiceReturnAFailure_ShouldCallPresenterWithTheRightValues() {
        let entryPoint = ReceiptEntryPoint(id: "2", type: .pav)
        let service = DefaultReceiptServiceMock(entryPoint: entryPoint)
        sut = DefaultReceiptInteractor(service: service, presenter: presenterSpy)
        
        service.expectedResult = .failure(ApiError.connectionFailure)
        
        sut.fetchReceiptIfNeeded()
        
        XCTAssertEqual(presenterSpy.callPresentLoadingCount, 1)
        XCTAssertEqual(presenterSpy.callPresentErrorCount, 1)
    }
    
    func testOpenMGM_ShouldCallCoordinatorWithTheRightAction() {
        sut.openMGM()
        
        XCTAssertEqual(presenterSpy.callPresentActionCount, 1)
        XCTAssertEqual(presenterSpy.action, .mgm)
    }
    
    func testOpenFaq_ShouldCallCoordinatorWithTheRightAction() {
        sut.openFaq(with: "")
        
        XCTAssertEqual(presenterSpy.callPresentActionCount, 1)
        XCTAssertEqual(presenterSpy.action, .faq(""))
    }
    
    func testOpenBilletPayment_ShouldCallCoordinatorWithTheRightAction() {
        sut.openBilletPayment()
        
        XCTAssertEqual(presenterSpy.callPresentActionCount, 1)
        XCTAssertEqual(presenterSpy.action, .billetPayment)
    }
    
    func testClose_ShouldCallPresenterWithTheRightValues() {
        sut.close()
        
        XCTAssertEqual(presenterSpy.callPresentActionCount, 1)
        XCTAssertEqual(presenterSpy.action, .close)
    }
}
