import XCTest
@testable import ReceiptKit

final class DefaultReceiptFactoryTests: XCTestCase {
    func testMake_WhenInitializedWithAnIdAndType_ShouldReturnTheRightViewController() {
        let entryPoint = ReceiptEntryPoint(id: "123", type: .pav)
        let navViewController = DefaultReceiptFactory.make(with: entryPoint)
        
        XCTAssertTrue(navViewController.topViewController is DefaultReceiptViewController)
    }
    
    func testMake_WhenInitializedWithAService_ShouldReturnTheRightViewController() {
        let entryPoint = ReceiptEntryPoint(id: "123", type: .pav)
        let navViewController = DefaultReceiptFactory.make(with: DefaultReceiptServiceMock(entryPoint: entryPoint))
        
        XCTAssertTrue(navViewController.topViewController is DefaultReceiptViewController)
    }
    
    func testMake_WhenInitializedWithAModel_ShouldReturnTheRightViewController() {
        let model = ReceiptModel(id: "2", itens: [])
        let navViewController = DefaultReceiptFactory.make(with: model)
        
        XCTAssertTrue(navViewController.topViewController is DefaultReceiptViewController)
    }
}
