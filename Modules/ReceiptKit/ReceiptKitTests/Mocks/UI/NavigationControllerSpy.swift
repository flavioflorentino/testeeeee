import UIKit

final class NavigationControllerSpy: UINavigationController {
    private(set) var callPushViewControllerCount = 0
    private(set) var callPopViewControllerCount = 0
    private(set) var callPresentViewControllerCount = 0
    private(set) var callDismissViewControllerCount = 0
    
    private(set) var pushedViewController: UIViewController?
    private(set) var popedViewController: UIViewController?
    private(set) var viewControllerPresented: UIViewController?
    
    public override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        pushedViewController = viewController
        callPushViewControllerCount += 1
        super.pushViewController(viewController, animated: false)
    }
    
    public override func popViewController(animated: Bool) -> UIViewController? {
        let viewController = super.popViewController(animated: false)
        popedViewController = viewController
        callPopViewControllerCount += 1
        return viewController
    }
    
    public override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        viewControllerPresented = viewControllerToPresent
        callPresentViewControllerCount += 1
        super.present(viewControllerToPresent, animated: false)
        completion?()
    }
    
    public override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        callDismissViewControllerCount += 1
        super.dismiss(animated: false)
        completion?()
    }

    // MARK: - Helpers
    public func tapAtBackBarButton() {
        guard let controller = viewControllers.first(where: { $0 != topViewController }) else { return }

        setViewControllers([controller], animated: false)
        delegate?.navigationController?(self, didShow: controller, animated: true)
    }
}
