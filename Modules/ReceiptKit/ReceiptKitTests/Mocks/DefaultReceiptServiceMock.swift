import Core
@testable import ReceiptKit

final class DefaultReceiptServiceMock: DefaultReceiptServicing {
    let entryPoint: ReceiptEntryPoint
    
    var expectedResult: Result<ReceiptModel, ApiError> = .failure(.connectionFailure)
    
    init(entryPoint: ReceiptEntryPoint) {
        self.entryPoint = entryPoint
    }
    
    func fetchReceipt(completion: (Result<ReceiptModel, ApiError>) -> Void) {
        completion(expectedResult)
    }
}
