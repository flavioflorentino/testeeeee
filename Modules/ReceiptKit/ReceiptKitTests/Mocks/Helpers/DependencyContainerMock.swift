import Core
@testable import ReceiptKit
import XCTest

final class DependencyContainerMock: Dependencies {
    lazy var mainQueue: DispatchQueue = resolve(default: .main)
    lazy var legacy: ReceiptKitSetupable = resolve(default: ReceiptKitSetupMock())
    
    private let dependencies: [Any]
    
    init(_ dependencies: Any...) {
        self.dependencies = dependencies
    }
}

extension DependencyContainerMock {
    func resolve<T>(default: T) -> T {
        guard let mock = dependencies.first(where: { $0 is T }) as? T else {
            return `default`
        }
        return mock
    }
}
