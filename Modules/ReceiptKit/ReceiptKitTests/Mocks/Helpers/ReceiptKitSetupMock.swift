import Core
@testable import ReceiptKit
import UIKit

final class DeeplinkSpy: DeeplinkContract {
    private(set) var callOpenCount = 0
    
    func open(url: URL) -> Bool {
        callOpenCount += 1
        return true
    }
}

final class MGMSpy: MGMContract {
    private(set) var callOpenCount = 0
   
    func open(with parent: UIViewController) -> Bool {
        callOpenCount += 1
        return true
    }
}

final class BilletSpy: BilletContract {
    private(set) var callOpenCount = 0
    
    func open(with parent: UIViewController) -> Bool {
        callOpenCount += 1
        return true
    }
}

final class ReceiptKitSetupMock: ReceiptKitSetupable {
    var deeplink: DeeplinkContract? = DeeplinkSpy()
    var billet: BilletContract? = BilletSpy()
    var mgm: MGMContract? = MGMSpy()
}
