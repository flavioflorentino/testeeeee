import ReceiptKit
import UIKit

final class HomeViewController: UIViewController {
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 16
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    private lazy var button1: UIButton = {
        let button = UIButton()
        button.setTitle("Default Receipt", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .green
        button.addTarget(self, action: #selector(didTouchButton1), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    init() {
        super.init(nibName: nil, bundle: nil)
        
        view.addSubview(stackView)
        stackView.addArrangedSubview(button1)
        
        stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        stackView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        view.backgroundColor = .white
    }
    
    private lazy var json: String = {
        """
        {
            "data": {
                "receipt": [
                    {
                        "type":"transaction",
                        "id":"29084215",
                        "total":"R$ 134,90",
                        "date":"09/12/2019 11:14",
                        "is_cancelled":true,
                        "share": {
                            "text":"Visitei Pagamento de contas e paguei com PicPay http://www.picpay.com/locais? #PicPay",
                            "url":"http://www.picpay.com/locais?"
                        },
                        "payee": {
                            "type":"store",
                            "id":"36748",
                            "name":"Pagamento de contas",
                            "img_url":"https://picpay.s3.amazonaws.com/sellers/d2f7d0c310e8d6522074bfff51621718.jpg",
                            "is_verified":false
                        },
                        "list": [
                            {
                                "label":"Cartão",
                                "text":"************4676 Master"
                            },
                            {
                                "label":"Pagto. Cartão",
                                "text":"R$ 138,93(1x 138,93)"
                            },
                            {
                                "label":"Taxa de conveniência do cartão",
                                "text":"R$ 4,03"
                            },
                            {
                                "label":"Cashback (dinheiro de volta)",
                                "text":"R$ 6,95"
                            },
                            {
                                "label":"Total pago",
                                "text":"R$ 138,93"
                            }
                        ]
                    },
                    {
                        "type":"transaction_status",
                        "title": {
                            "color":"#ED1846",
                            "value":"Pagamento cancelado"
                        },
                        "subtitle": {
                            "color":"#666666",
                            "value":"Pagamento cancelado"
                        },
                        "icon": {
                            "type":"image",
                            "value":"cancel_icon"
                        }
                    },
                    {
                        "type":"transaction_status_label",
                        "title": {
                            "color":"#ED1846",
                            "value":"Pagamento cancelado"
                        },
                        "icon": {
                            "type":"image",
                            "value":"cancel_icon"
                        }
                    },
                    {
                        "type":"list",
                        "itens": [
                            {
                                "label": {
                                    "color":"#000000",
                                    "value":"Vencimento"
                                },
                                "value": {
                                    "color":"#666666",
                                    "value":"23/12/2019"
                                }
                            },
                            {
                                "label": {
                                    "color":"#000000",
                                    "value":"Beneficiário"
                                },
                                "value": {
                                    "color":"#666666",
                                    "value":"CAIXA ECONOMICA FEDERAL"
                                }
                            }
                        ]
                    },
                    {
                        "type":"label_value_item",
                        "label": {
                            "color":"#666666",
                            "value":"Código de barras"
                        },
                        "value": {
                            "color":"#666666",
                            "value":"10496.48023 41900.100045 01004.229009 2 81120000013490"
                        }
                    },
                    {
                        "type": "button_action",
                        "action": "pay_new_boleto",
                        "title": "Pagar outro boleto"
                    },
                    {
                        "type":"cashback",
                        "title": {
                            "color":"#4a4a4a",
                            "value":"Seu último pagamento rendeu cashback"
                        },
                        "subtitle": {
                            "color":"#666666",
                            "value":"Você ganhou 6,95 reais para usar no PicPay com o que quiser."
                        },
                        "icon": {
                            "type":"asset",
                            "value":"congratulations_cashback"
                        },
                        "button": {
                            "title":"Amei!",
                            "color":"#11c76f",
                            "action":"dismiss"
                        }
                    },
                    {
                        "type":"mgm_offer",
                        "order":100,
                        "img_url":"https://s3.amazonaws.com/cdn.picpay.com/apps/picpay/imgs/mgm_receipt.png",
                        "title": {
                            "color":"#21c25e",
                            "value":"Indique e ganhe R$10"
                        },
                        "button": {
                            "title":"INDICAR AGORA",
                            "type":"ghost",
                            "action":"open_mgm",
                            "action_data": {
                                "event_properties": {
                                    "receipt_type":"Pav"
                                }
                            }
                        },
                        "text": {
                            "color":"#666666",
                            "value":"Gostando do PicPay? Indique seus amigos e ganhe R$10 por cada indicação."
                        }
                    }
                ]
            }
        }
        """
    }()
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    func didTouchButton1() {
        guard let data = json.data(using: .utf8) else { return }
        
        do {
            let model = try JSONDecoder().decode(ReceiptModel.self, from: data)
            
            let viewController = DefaultReceiptFactory.make(with: model)
            
            present(viewController, animated: true)
        } catch {
            print(error)
        }
    }
}
