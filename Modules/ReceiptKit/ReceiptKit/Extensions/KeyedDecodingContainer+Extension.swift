import Foundation

protocol DecodableClassFamily: Decodable {
    associatedtype BaseType: Decodable
    static var discriminator: Discriminator { get }

    func getType() -> BaseType.Type
}

enum Discriminator: String, CodingKey {
    case type
}

extension KeyedDecodingContainer {
    func decodeHeterogeneousArray<F: DecodableClassFamily>(family: F.Type, forKey key: K) throws -> [F.BaseType] {
        var container = try nestedUnkeyedContainer(forKey: key)
        var containerCopy = container
        var items: [F.BaseType] = []
        
        while !container.isAtEnd {
            let typeContainer = try container.nestedContainer(keyedBy: Discriminator.self)
            
            do {
                let family = try typeContainer.decode(F.self, forKey: F.discriminator)
                let type = family.getType()
                
                let item = try containerCopy.decode(type)
                items.append(item)
            } catch let e as DecodingError {
                switch e {
                case .dataCorrupted(let context) where context.codingPath.last?.stringValue == F.discriminator.stringValue:
                    _ = try containerCopy.decode(F.BaseType.self)
                default:
                    throw e
                }
            }
        }
        
        return items
    }
}
