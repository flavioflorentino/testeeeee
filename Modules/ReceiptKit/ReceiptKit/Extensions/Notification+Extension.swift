import Foundation

public extension NSNotification.Name {
    enum PrivacyReview {
        public static let shouldOpen = NSNotification.Name("ShouldOpen")
    }
    
    enum Feed {
        public static let needReload = NSNotification.Name("FeedNeedReload")
    }
}

public extension NSNotification {
    enum PrivacyReview {
        public static let shouldOpen = NSNotification(name: Name.PrivacyReview.shouldOpen, object: nil)
    }
    
    enum Feed {
        public static let needReload = NSNotification(name: Name.Feed.needReload, object: nil)
    }
}
