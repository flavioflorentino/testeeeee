protocol HasNoDependency {}

protocol HasLegacy {
    var legacy: ReceiptKitSetupable { get }
}
