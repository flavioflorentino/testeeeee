import Foundation
import Core

typealias Dependencies = HasNoDependency & HasMainQueue & HasLegacy

final class DependencyContainer: Dependencies {
    lazy var mainQueue = DispatchQueue.main
    lazy var legacy: ReceiptKitSetupable = ReceiptKitSetup.shared
}
