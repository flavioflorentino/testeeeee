import Core

public protocol ReceiptKitSetupable: AnyObject {
    var deeplink: DeeplinkContract? { get }
    var billet: BilletContract? { get }
    var mgm: MGMContract? { get }
}

public final class ReceiptKitSetup: ReceiptKitSetupable {
    public static let shared = ReceiptKitSetup()
    
    public var deeplink: DeeplinkContract?
    public var billet: BilletContract?
    public var mgm: MGMContract?
    
    private init() {}
    
    public func inject(instance: Any) {
        switch instance {
        case let instance as DeeplinkContract:
            deeplink = instance
        case let instance as BilletContract:
            billet = instance
        case let instance as MGMContract:
            mgm = instance
        default:
            assertionFailure("🚨\nAttempted to inject unexpected depedency: \(instance)\n")
        }
    }
}
