enum QuantumValue: Decodable {
    case int(Int)
    case string(String)
    
    enum QuantumError: Error {
        case missingValue
    }
    
    var value: String {
        switch self {
        case .int(let value):
            return "\(value)"
        case .string(let value):
            return value
        }
    }

    init(from decoder: Decoder) throws {
        if let int = try? decoder.singleValueContainer().decode(Int.self) {
            self = .int(int)
            return
        }

        if let string = try? decoder.singleValueContainer().decode(String.self) {
            self = .string(string)
            return
        }

        throw QuantumError.missingValue
    }
}
