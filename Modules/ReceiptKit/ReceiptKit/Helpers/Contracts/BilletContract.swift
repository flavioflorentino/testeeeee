import UIKit

public protocol BilletContract {
    @discardableResult
    func open(with parent: UIViewController) -> Bool
}
