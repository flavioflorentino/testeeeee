import UIKit

public protocol MGMContract {
    @discardableResult
    func open(with parent: UIViewController) -> Bool
}
