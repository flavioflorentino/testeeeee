protocol ReceiptListCellViewModeling: ReceiptCellViewModeling {
    var itens: [(title: String, value: String)] { get }
}

struct ReceiptListCellViewModel: ReceiptListCellViewModeling {
    let type: ReceiptClassFamily
    let itens: [(title: String, value: String)]
    
    init(item: ReceiptListItem) {
        type = item.type
        itens = item.itens.map {
            (title: $0.label.value, value: $0.value.value)
        }
    }
}
