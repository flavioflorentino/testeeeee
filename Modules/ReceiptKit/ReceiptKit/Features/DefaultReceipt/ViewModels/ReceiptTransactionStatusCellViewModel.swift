protocol ReceiptTransactionStatusCellViewModeling: ReceiptCellViewModeling {
    var title: String { get }
    var subtitle: String { get }
}

struct ReceiptTransactionStatusCellViewModel: ReceiptTransactionStatusCellViewModeling {
    let type: ReceiptClassFamily
    let title: String
    let subtitle: String
    
    init(item: ReceiptTransactionStatusItem) {
        type = item.type
        title = item.title.value
        subtitle = item.subtitle.value
    }
}
