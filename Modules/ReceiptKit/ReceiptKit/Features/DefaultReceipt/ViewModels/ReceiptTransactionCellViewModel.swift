protocol ReceiptTransactionCellViewModeling: ReceiptCellViewModeling {
    var title: String { get }
    var transactionCode: String { get }
    var date: String { get }
    var itens: [(title: String, value: String)] { get }
    var value: String { get }
    var imageUrl: URL? { get }
    var isCancelled: Bool { get }
}

struct ReceiptTransactionCellViewModel: ReceiptTransactionCellViewModeling {
    let type: ReceiptClassFamily
    let title: String
    let transactionCode: String
    let date: String
    let itens: [(title: String, value: String)]
    let value: String
    let imageUrl: URL?
    let isCancelled: Bool
    
    init(item: ReceiptTransactionItem) {
        type = item.type
        title = item.payee.name
        transactionCode = "\(Strings.Cells.transaction) \(item.id)"
        date = item.date
        itens = item.list.map {
            (title: $0.label, value: $0.text)
        }
        value = item.total
        imageUrl = URL(string: item.payee.imgUrl)
        isCancelled = item.isCancelled
    }
}
