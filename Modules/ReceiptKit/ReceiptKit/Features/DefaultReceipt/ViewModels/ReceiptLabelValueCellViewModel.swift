protocol ReceiptLabelValueCellViewModeling: ReceiptCellViewModeling {
    var title: String { get }
    var value: String { get }
}

struct ReceiptLabelValueCellViewModel: ReceiptLabelValueCellViewModeling {
    let type: ReceiptClassFamily
    let title: String
    let value: String
    
    init(item: ReceiptLabelValueItem) {
        type = item.type
        title = item.label.value
        value = item.value.value
    }
}
