protocol ReceiptBilletCellViewModeling: ReceiptCellViewModeling {
    var title: String { get }
}

struct ReceiptBilletCellViewModel: ReceiptBilletCellViewModeling {
    let type: ReceiptClassFamily
    let title: String
    
    init(item: ReceiptBilletItem) {
        type = item.type
        title = item.title
    }
}
