protocol ReceiptMGMCellViewModeling: ReceiptCellViewModeling {
    var title: String { get }
    var text: String { get }
    var imageUrl: URL? { get }
    var button: String { get }
}

struct ReceiptMGMCellViewModel: ReceiptMGMCellViewModeling {
    let type: ReceiptClassFamily
    let title: String
    let text: String
    let imageUrl: URL?
    let button: String
    
    init(item: ReceiptMGMItem) {
        type = item.type
        title = item.title.value
        text = item.text.value
        imageUrl = URL(string: item.image)
        button = item.button.title
    }
}
