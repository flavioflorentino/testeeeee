protocol ReceiptFaqCellViewModeling: ReceiptCellViewModeling {
    var title: String { get }
    var linkTitle: String { get }
    var linkUrl: String { get }
}

struct ReceiptFaqCellViewModel: ReceiptFaqCellViewModeling {
    let type: ReceiptClassFamily
    let title: String
    let linkTitle: String
    let linkUrl: String
    
    init(item: ReceiptLabelValueItem) {
        type = item.type
        title = item.label.value
        linkTitle = item.value.value
        linkUrl = item.value.url ?? ""
    }
}
