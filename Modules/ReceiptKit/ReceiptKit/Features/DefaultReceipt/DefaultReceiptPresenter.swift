import AssetsKit
import Foundation
import UI

protocol DefaultReceiptPresenting: AnyObject {
    var viewController: DefaultReceiptDisplaying? { get set }
    func presentLoading()
    func presentReceipt(with model: ReceiptModel)
    func presentError()
    func present(action: DefaultReceiptAction)
}

final class DefaultReceiptPresenter {
    private let coordinator: DefaultReceiptCoordinating
    weak var viewController: DefaultReceiptDisplaying?

    init(coordinator: DefaultReceiptCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - DefaultReceiptPresenting

extension DefaultReceiptPresenter: DefaultReceiptPresenting {
    func presentLoading() {
        viewController?.displayLoadingState()
    }
    
    func presentReceipt(with model: ReceiptModel) {
        let viewModels: [ReceiptCellViewModeling] = model.itens
            .map {
                switch $0 {
                case let item as ReceiptTransactionItem where $0.type == .transaction:
                    return ReceiptTransactionCellViewModel(item: item)
                
                case let item as ReceiptTransactionStatusItem where $0.type == .transactionStatus:
                    return ReceiptTransactionStatusCellViewModel(item: item)
                    
                case let item as ReceiptListItem where $0.type == .list:
                    return ReceiptListCellViewModel(item: item)
                    
                case let item as ReceiptLabelValueItem where $0.type == .labelValue:
                    return ReceiptLabelValueCellViewModel(item: item)
                    
                case let item as ReceiptLabelValueItem where $0.type == .faq:
                    return ReceiptFaqCellViewModel(item: item)
                
                case let item as ReceiptBilletItem where $0.type == .billet:
                    return ReceiptBilletCellViewModel(item: item)
                    
                case let item as ReceiptMGMItem where $0.type == .mgmOffer:
                    return ReceiptMGMCellViewModel(item: item)
                    
                default:
                    return nil
                }
            }
            .compactMap { $0 }

        viewController?.displaySuccessState(with: viewModels)
    }
    
    func presentError() {
        let viewModel = StatefulFeedbackViewModel(
            image: Resources.Illustrations.iluFalling.image,
            content: (
                title: Strings.Error.title,
                description: Strings.Error.message
            ),
            button: (
                image: Resources.Icons.icoRefresh.image,
                title: Strings.Common.tryAgain
            )
        )
        viewController?.displayErrorState(with: viewModel)
    }
    
    func present(action: DefaultReceiptAction) {
        coordinator.perform(action: action)
    }
}
