import SnapKit
import UI
import UIKit

final class TitleValueView: UIView {
    // MARK: - Views
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
            .with(\.textColor, .grayscale700())
        return label
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .right)
        return label
    }()
    
    // MARK: - Inits
    
    init(title: String, value: String, isCancelled: Bool = false) {
        super.init(frame: .zero)
        
        titleLabel.text = title
        valueLabel.text = value
        
        if isCancelled {
            underlineLabels()
        }
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func underlineLabels() {
        let labelsToUnderline = [titleLabel, valueLabel]
        
        labelsToUnderline.forEach {
            var attributes = $0.attributedText?.attributes(at: 0, effectiveRange: nil) ?? [:]
            attributes[.font] = $0.font
            attributes[.foregroundColor] = $0.textColor
            attributes[.strikethroughStyle] = NSUnderlineStyle.single.rawValue
            attributes[.strikethroughColor] = $0.textColor
            
            $0.attributedText = NSAttributedString(string: $0.text ?? "", attributes: attributes)
        }
    }
}

// MARK: - ViewConfiguration

extension TitleValueView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(titleLabel)
        addSubview(valueLabel)
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.leading.bottom.equalToSuperview()
        }
        
        valueLabel.snp.makeConstraints {
            $0.top.trailing.bottom.equalToSuperview()
            $0.leading.equalTo(titleLabel.snp.trailing).offset(Spacing.base00).priority(.high)
        }
    }
}
