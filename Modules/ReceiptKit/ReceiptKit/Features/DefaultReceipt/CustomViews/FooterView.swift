import AssetsKit
import SnapKit
import UI
import UIKit

final class FooterView: UIView {
    private typealias Localizable = Strings.Footer
    
    // MARK: - Views
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
            .with(\.text, Localizable.title)
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var sacContainerView = UIView()
    
    private lazy var sacLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
            .with(\.text, Localizable.saq)
            .with(\.textAlignment, .right)
        label.setContentHuggingPriority(.defaultLow, for: .horizontal)
        return label
    }()
    
    private lazy var sacNumberLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.text, Localizable.saqNumber)
            .with(\.textAlignment, .left)
        label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        return label
    }()
    
    private lazy var ombudsmanContainerView = UIView()
    
    private lazy var ombudsmanLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
            .with(\.text, Localizable.ombudsman)
            .with(\.textAlignment, .right)
        return label
    }()
    
    private lazy var ombudsmanNumberLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.text, Localizable.ombudsmanNumber)
            .with(\.textAlignment, .left)
        return label
    }()
    
    private lazy var picpayLogo: UIImageView = {
        let image = UIImage(asset: Resources.Logos.picpayLogoReceipt)
        return UIImageView(image: image)
    }()
    
    private lazy var cnpjLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.text, "\(Localizable.cnpj) \(Localizable.cnpjNumber)")
            .with(\.textAlignment, .center)
        return label
    }()
    
    // MARK: - Inits
    
    init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - ViewConfiguration

extension FooterView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(titleLabel)
        addSubview(sacContainerView)
        sacContainerView.addSubviews(sacLabel, sacNumberLabel)
        addSubview(ombudsmanContainerView)
        ombudsmanContainerView.addSubviews(ombudsmanLabel, ombudsmanNumberLabel)
        addSubview(picpayLogo)
        addSubview(cnpjLabel)
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        sacContainerView.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.centerX.equalToSuperview()
        }
        
        sacLabel.snp.makeConstraints {
            $0.top.leading.bottom.equalToSuperview()
        }
        
        sacNumberLabel.snp.makeConstraints {
            $0.top.trailing.bottom.equalToSuperview()
            $0.leading.equalTo(sacLabel.snp.trailing).offset(Spacing.base00)
        }
        
        ombudsmanContainerView.snp.makeConstraints {
            $0.top.equalTo(sacContainerView.snp.bottom).offset(Spacing.base02)
            $0.centerX.equalToSuperview()
        }
        
        ombudsmanLabel.snp.makeConstraints {
            $0.top.leading.bottom.equalToSuperview()
        }
        
        ombudsmanNumberLabel.snp.makeConstraints {
            $0.top.trailing.bottom.equalToSuperview()
            $0.leading.equalTo(ombudsmanLabel.snp.trailing).offset(Spacing.base00)
        }
        
        picpayLogo.snp.makeConstraints {
            $0.top.equalTo(ombudsmanContainerView.snp.bottom).offset(Spacing.base03)
            $0.centerX.equalToSuperview()
        }
        
        cnpjLabel.snp.makeConstraints {
            $0.top.equalTo(picpayLogo.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.bottom.equalToSuperview().inset(Spacing.base02)
        }
    }
}
