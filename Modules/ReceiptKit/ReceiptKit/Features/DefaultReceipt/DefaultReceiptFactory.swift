import CorePayment
import UIKit

public struct ReceiptEntryPoint: SuccessPayment {
    public let transactionId: String
    public let type: ReceiptType
    
    public init(id: String, type: ReceiptType) {
        self.transactionId = id
        self.type = type
    }
}

public enum DefaultReceiptFactory {
    public static func make(with entryPoint: ReceiptEntryPoint) -> UINavigationController {
        let dependencies = DependencyContainer()
        let coordinator: DefaultReceiptCoordinating = DefaultReceiptCoordinator(dependencies: dependencies)
        let service: DefaultReceiptServicing = DefaultReceiptService(entryPoint: entryPoint, dependencies: dependencies)
        let presenter: DefaultReceiptPresenting = DefaultReceiptPresenter(coordinator: coordinator)
        let interactor = DefaultReceiptInteractor(service: service, presenter: presenter)
        let viewController = DefaultReceiptViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.presentationController?.delegate = viewController

        return navigationController
    }
    
    public static func make(with service: DefaultReceiptServicing) -> UINavigationController {
        let dependencies = DependencyContainer()
        let coordinator: DefaultReceiptCoordinating = DefaultReceiptCoordinator(dependencies: dependencies)
        let presenter: DefaultReceiptPresenting = DefaultReceiptPresenter(coordinator: coordinator)
        let interactor = DefaultReceiptInteractor(service: service, presenter: presenter)
        let viewController = DefaultReceiptViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.presentationController?.delegate = viewController

        return navigationController
    }
    
    public static func make(with model: ReceiptModel) -> UINavigationController {
        let dependencies = DependencyContainer()
        let coordinator: DefaultReceiptCoordinating = DefaultReceiptCoordinator(dependencies: dependencies)
        let presenter: DefaultReceiptPresenting = DefaultReceiptPresenter(coordinator: coordinator)
        let interactor = DefaultReceiptInteractor(model: model, presenter: presenter)
        let viewController = DefaultReceiptViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController
        
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.presentationController?.delegate = viewController
        
        if #available(iOS 13.0, *) {
            navigationController.isModalInPresentation = true
        }

        return navigationController
    }
}
