import Core

enum DefaultReceiptEndpoint {
    case fetchReceipt(id: String, type: ReceiptType)
}

extension DefaultReceiptEndpoint: ApiEndpointExposable {
    var path: String {
        "api/receipt.json"
    }
    
    var method: HTTPMethod {
        .post
    }
    
    var body: Data? {
        switch self {
        case let .fetchReceipt(id, type):
            return ["transaction_id": id, "type": type.rawValue].toData()
        }
    }
}
