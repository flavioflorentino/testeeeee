import SnapKit
import UI
import UIKit

protocol DefaultReceiptDisplaying: AnyObject {
    func displayLoadingState()
    func displaySuccessState(with viewModels: [ReceiptCellViewModeling])
    func displayErrorState(with viewModel: StatefulFeedbackViewModel)
}

private extension DefaultReceiptViewController.Layout {
    enum TableView {
        static let rowHeight = UITableView.automaticDimension
        static let estimatedRowHeight: CGFloat = 50
    }
}

final class DefaultReceiptViewController: ViewController<DefaultReceiptInteracting, UIView> {
    fileprivate enum Layout { }
    
    // MARK: - Views
    private lazy var footerContainerView = UIView()
    private lazy var footerView = FooterView()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.estimatedRowHeight = Layout.TableView.estimatedRowHeight
        tableView.rowHeight = Layout.TableView.rowHeight
        tableView.separatorStyle = .none
        tableView.register(cellType: ReceiptTransactionCell.self)
        tableView.register(cellType: ReceiptTransactionStatusCell.self)
        tableView.register(cellType: ReceiptListCell.self)
        tableView.register(cellType: ReceiptFaqCell.self)
        tableView.register(cellType: ReceiptLabelValueCell.self)
        tableView.register(cellType: ReceiptBilletCell.self)
        tableView.register(cellType: ReceiptMGMCell.self)
        footerContainerView.addSubview(footerView)
        tableView.tableFooterView = footerContainerView
        tableView.backgroundColor = .clear
        return tableView
    }()
    
    private lazy var dataSource: TableViewDataSource<Int, ReceiptCellViewModeling> = {
        let dataSource = TableViewDataSource<Int, ReceiptCellViewModeling>(view: tableView)
        dataSource.itemProvider = bindCell(_:_:_:)
        return dataSource
    }()
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        interactor.fetchReceiptIfNeeded()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        tableView.updateTableFooterView(with: footerView)
    }
    
    // MARK: - View Layout

    override func buildViewHierarchy() {
        view.addSubview(tableView)
    }
    
    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        footerView.snp.makeConstraints {
            $0.width.equalTo(tableView)
        }
    }

    override func configureViews() {
        setupNavigationItens()
        
        title = Strings.Common.title
        
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        
        tableView.dataSource = dataSource
    }
}

// MARK: - Private methods

private extension DefaultReceiptViewController {
    func setupNavigationItens() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: Strings.Common.close,
                                                           style: .plain,
                                                           target: self,
                                                           action: #selector(didTapClose))
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action,
                                                            target: self,
                                                            action: #selector(didTapShare))
    }
    
    @objc
    func didTapClose() {
        interactor.close()
    }
    
    @objc
    func didTapShare() {
        guard let image = generateImage() else { return }
        
        let activityController = UIActivityViewController(activityItems: [image], applicationActivities: nil)
        present(activityController, animated: true, completion: nil)
    }
    
    func generateImage() -> UIImage? {
        view.clipsToBounds = false
        
        let originalTableFrame = view.frame
        var modifiedTableFrame = view.frame

        tableView.contentOffset = .zero
        modifiedTableFrame.size.height = tableView.contentSize.height
        view.frame = modifiedTableFrame
        
        let image = view.snapshot
        
        view.frame = originalTableFrame
        
        return image
    }
    
    func bindCell(_ tableView: UITableView, _ indexPath: IndexPath, _ viewModel: ReceiptCellViewModeling) -> UITableViewCell {
        switch viewModel {
        case let viewModel as ReceiptTransactionCellViewModel:
            let cell = tableView.dequeueReusableCell(for: indexPath, cellType: ReceiptTransactionCell.self)
            cell.configure(with: viewModel)
            return cell
            
        case let viewModel as ReceiptTransactionStatusCellViewModel:
            let cell = tableView.dequeueReusableCell(for: indexPath, cellType: ReceiptTransactionStatusCell.self)
            cell.configure(with: viewModel)
            return cell
            
        case let viewModel as ReceiptListCellViewModel:
            let cell = tableView.dequeueReusableCell(for: indexPath, cellType: ReceiptListCell.self)
            cell.configure(with: viewModel)
            return cell
            
        case let viewModel as ReceiptFaqCellViewModel:
            let cell = tableView.dequeueReusableCell(for: indexPath, cellType: ReceiptFaqCell.self)
            cell.configure(with: viewModel, and: self)
            return cell
            
        case let viewModel as ReceiptLabelValueCellViewModel:
            let cell = tableView.dequeueReusableCell(for: indexPath, cellType: ReceiptLabelValueCell.self)
            cell.configure(with: viewModel)
            return cell
            
        case let viewModel as ReceiptBilletCellViewModel:
            let cell = tableView.dequeueReusableCell(for: indexPath, cellType: ReceiptBilletCell.self)
            cell.configure(with: viewModel, and: self)
            return cell
            
        case let viewModel as ReceiptMGMCellViewModel:
            let cell = tableView.dequeueReusableCell(for: indexPath, cellType: ReceiptMGMCell.self)
            cell.configure(with: viewModel, and: self)
            return cell
            
        default:
            return UITableViewCell()
        }
    }
}

extension DefaultReceiptViewController: ReceiptFaqCellDelegate {
    func didTapFaqLink(with url: String) {
        interactor.openFaq(with: url)
    }
}

extension DefaultReceiptViewController: ReceiptBilletCellDelegate {
    func didTapPayAnotherButton() {
        interactor.openBilletPayment()
    }
}

extension DefaultReceiptViewController: ReceiptMGMCellDelegate {
    func didTapMGMButton() {
        interactor.openMGM()
    }
}

// MARK: - DefaultReceiptDisplaying

extension DefaultReceiptViewController: DefaultReceiptDisplaying {
    func displayLoadingState() {
        beginState()
    }
    
    func displaySuccessState(with viewModels: [ReceiptCellViewModeling]) {
        dataSource.add(items: viewModels, to: 0)
        endState()
    }
    
    func displayErrorState(with viewModel: StatefulFeedbackViewModel) {
        endState(model: viewModel)
    }
}

// MARK: - StatefulTransitionViewing

extension DefaultReceiptViewController: StatefulTransitionViewing {
    func didTryAgain() {
        interactor.fetchReceiptIfNeeded()
    }
    
    func didTapSecondaryButton() {
        interactor.close()
    }
}

// MARK: - UIAdaptivePresentationControllerDelegate
extension DefaultReceiptViewController: UIAdaptivePresentationControllerDelegate {
    func presentationControllerDidAttemptToDismiss(_ presentationController: UIPresentationController) {
        interactor.close()
    }
}
