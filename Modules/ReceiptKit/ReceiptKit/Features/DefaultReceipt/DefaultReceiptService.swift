import Core
import Foundation

public protocol DefaultReceiptServicing {
    var entryPoint: ReceiptEntryPoint { get }
  
    func fetchReceipt(completion: @escaping (Result<ReceiptModel, ApiError>) -> Void)
}

final class DefaultReceiptService: DefaultReceiptServicing {
    let entryPoint: ReceiptEntryPoint
    
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(entryPoint: ReceiptEntryPoint, dependencies: Dependencies) {
        self.entryPoint = entryPoint
        self.dependencies = dependencies
    }
    
    func fetchReceipt(completion: @escaping (Result<ReceiptModel, ApiError>) -> Void) {
        let endpoint = DefaultReceiptEndpoint.fetchReceipt(id: entryPoint.transactionId, type: entryPoint.type)
        let api = Api<ReceiptModel>(endpoint: endpoint)
        
        api.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
