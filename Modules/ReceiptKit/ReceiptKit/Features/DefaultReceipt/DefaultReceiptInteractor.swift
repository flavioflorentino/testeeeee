import Foundation

protocol DefaultReceiptInteracting: AnyObject {
    func fetchReceiptIfNeeded()
    func openFaq(with url: String)
    func openBilletPayment()
    func openMGM()
    func close()
}

final class DefaultReceiptInteractor {
    private var service: DefaultReceiptServicing?
    private var model: ReceiptModel?
    private let presenter: DefaultReceiptPresenting

    init(service: DefaultReceiptServicing, presenter: DefaultReceiptPresenting) {
        self.service = service
        self.presenter = presenter
    }
    
    init(model: ReceiptModel, presenter: DefaultReceiptPresenting) {
        self.model = model
        self.presenter = presenter
    }
}

// MARK: - DefaultReceiptInteracting

extension DefaultReceiptInteractor: DefaultReceiptInteracting {
    func fetchReceiptIfNeeded() {
        presenter.presentLoading()
        
        if let model = model {
            presenter.presentReceipt(with: model)
            return
        }
        
        guard let service = service else {
            return presenter.presentError()
        }
        
        service.fetchReceipt { result in
            switch result {
            case .success(let model):
                self.presenter.presentReceipt(with: model)
            case .failure:
                self.presenter.presentError()
            }
        }
    }
    
    func openFaq(with url: String) {
        presenter.present(action: .faq(url))
    }
    
    func openBilletPayment() {
        presenter.present(action: .billetPayment)
    }
    
    func openMGM() {
        presenter.present(action: .mgm)
    }
    
    func close() {
        presenter.present(action: .close)
    }
}
