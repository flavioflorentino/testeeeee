import UIKit

enum DefaultReceiptAction: Equatable {
    case faq(String)
    case billetPayment
    case mgm
    case close
}

protocol DefaultReceiptCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: DefaultReceiptAction)
}

final class DefaultReceiptCoordinator {
    typealias Dependencies = HasLegacy
    private let dependencies: Dependencies
    
    weak var viewController: UIViewController?
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - DefaultReceiptCoordinating

extension DefaultReceiptCoordinator: DefaultReceiptCoordinating {
    func perform(action: DefaultReceiptAction) {
        switch action {
        case .faq(let urlString):
            guard let url = URL(string: urlString) else { return }
            dependencies.legacy.deeplink?.open(url: url)
            
        case .billetPayment:
            guard let viewController = viewController else { return }
            dependencies.legacy.billet?.open(with: viewController)
        
        case .mgm:
            guard let viewController = viewController else { return }
            dependencies.legacy.mgm?.open(with: viewController)
            
        case .close:
            viewController?.dismiss(animated: true) {
                NotificationCenter.default.post(name: NSNotification.PrivacyReview.shouldOpen.name, object: nil)
                NotificationCenter.default.post(name: NSNotification.Feed.needReload.name, object: nil)
            }
        }
    }
}
