import SnapKit
import UI
import UIKit

final class ReceiptListCell: UITableViewCell {
    // MARK: - Views
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        stackView.backgroundColor = .clear
        return stackView
    }()
    
    private lazy var separator = SeparatorView(style: BackgroundViewStyle(color: .grayscale100()))
    
    // MARK: - Inits
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Configure
    
    func configure(with viewModel: ReceiptListCellViewModel) {
        guard stackView.arrangedSubviews.isEmpty else { return }
        
        viewModel.itens.forEach {
            let view = TitleValueView(title: $0.title, value: $0.value)
            stackView.addArrangedSubview(view)
        }
    }
}

// MARK: - ViewConfiguration

extension ReceiptListCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(stackView)
        contentView.addSubview(separator)
    }
    
    func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        separator.snp.makeConstraints {
            $0.top.equalTo(stackView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    func configureViews() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
