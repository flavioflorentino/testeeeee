import SnapKit
import UI
import UIKit

final class ReceiptTransactionCell: UITableViewCell {
    // MARK: - Views
    
    private lazy var payeeView = UIView()
    
    private lazy var iconImage: UIImageView = {
        let imageView = UIImageView()
        imageView.imageStyle(RoundedImageStyle(cornerRadius: .full))
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
            .with(\.textColor, .grayscale700())
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base00
        stackView.backgroundColor = .clear
        return stackView
    }()
    
    private lazy var transactionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
        return label
    }()
    
    private lazy var dateLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
        return label
    }()
    
    private lazy var headerSeparator = SeparatorView(style: BackgroundViewStyle(color: .grayscale600()))
    
    private lazy var listStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        stackView.backgroundColor = .clear
        return stackView
    }()
    
    private lazy var footerSeparator = SeparatorView(style: BackgroundViewStyle(color: .grayscale600()))
    
    private lazy var titleValueLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .medium))
            .with(\.text, Strings.Cells.total)
            .with(\.textColor, .grayscale700())
        return label
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .right)
        return label
    }()
    
    private lazy var cellSeparator = SeparatorView(style: BackgroundViewStyle(color: .grayscale100()))
    
    // MARK: - Inits
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Configure
    
    func configure(with viewModel: ReceiptTransactionCellViewModel) {
        titleLabel.text = viewModel.title
        transactionLabel.text = viewModel.transactionCode
        dateLabel.text = viewModel.date
        iconImage.setImage(url: viewModel.imageUrl)
        valueLabel.text = viewModel.value
        
        guard listStackView.arrangedSubviews.isEmpty else { return }
        
        viewModel.itens.forEach {
            let view = TitleValueView(title: $0.title, value: $0.value, isCancelled: viewModel.isCancelled)
            listStackView.addArrangedSubview(view)
        }
        
        if viewModel.isCancelled {
            underlineLabels()
        }
    }
    
    private func underlineLabels() {
        let labelsToUnderline = [titleValueLabel, valueLabel]
        
        labelsToUnderline.forEach {
            var attributes = $0.attributedText?.attributes(at: 0, effectiveRange: nil) ?? [:]
            attributes[.font] = $0.font
            attributes[.foregroundColor] = $0.textColor
            attributes[.strikethroughStyle] = NSUnderlineStyle.single.rawValue
            attributes[.strikethroughColor] = $0.textColor
            
            $0.attributedText = NSAttributedString(string: $0.text ?? "", attributes: attributes)
        }
    }
}

// MARK: - ViewConfiguration

extension ReceiptTransactionCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(payeeView)
        payeeView.addSubviews(iconImage, titleLabel, stackView)
        stackView.addArrangedSubview(transactionLabel)
        stackView.addArrangedSubview(dateLabel)
        contentView.addSubview(headerSeparator)
        contentView.addSubview(listStackView)
        contentView.addSubview(footerSeparator)
        contentView.addSubview(titleValueLabel)
        contentView.addSubview(valueLabel)
        contentView.addSubview(cellSeparator)
    }
    
    func setupConstraints() {
        payeeView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        iconImage.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.leading.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.trailing.equalToSuperview()
            $0.leading.equalTo(iconImage.snp.trailing).offset(Spacing.base02)
        }
        
        stackView.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base00)
            $0.leading.equalTo(iconImage.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview()
            $0.bottom.equalToSuperview().inset(Spacing.base03)
        }
        
        headerSeparator.snp.makeConstraints {
            $0.top.equalTo(payeeView.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        listStackView.snp.makeConstraints {
            $0.top.equalTo(headerSeparator.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        footerSeparator.snp.makeConstraints {
            $0.top.equalTo(listStackView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        titleValueLabel.snp.makeConstraints {
            $0.top.equalTo(footerSeparator.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
        }
        
        valueLabel.snp.makeConstraints {
            $0.top.equalTo(footerSeparator.snp.bottom).offset(Spacing.base02)
            $0.leading.equalTo(titleValueLabel.snp.trailing).offset(Spacing.base00).priority(.high)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        cellSeparator.snp.makeConstraints {
            $0.top.equalTo(titleValueLabel.snp.bottom).offset(Spacing.base02)
            $0.top.equalTo(valueLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    func configureViews() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
