import SnapKit
import UI
import UIKit

protocol ReceiptBilletCellDelegate: AnyObject {
    func didTapPayAnotherButton()
}

final class ReceiptBilletCell: UITableViewCell {
    // MARK: - Views
    
    private lazy var payAnotherButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(SecondaryButtonStyle())
        button.addTarget(self, action: #selector(didTapPayAnotherButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var separator = SeparatorView(style: BackgroundViewStyle(color: .grayscale100()))
    
    // MARK: - Properties
    
    private weak var delegate: ReceiptBilletCellDelegate?
    
    // MARK: - Inits
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Configure
    
    func configure(with viewModel: ReceiptBilletCellViewModel, and delegate: ReceiptBilletCellDelegate) {
        payAnotherButton.setTitle(viewModel.title, for: .normal)
        
        self.delegate = delegate
    }
}

@objc
private extension ReceiptBilletCell {
    func didTapPayAnotherButton() {
        delegate?.didTapPayAnotherButton()
    }
}

extension ReceiptBilletCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubviews(payAnotherButton, separator)
    }
    
    func setupConstraints() {
        payAnotherButton.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        separator.snp.makeConstraints {
            $0.top.equalTo(payAnotherButton.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    func configureViews() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
