import SnapKit
import UI
import UIKit

protocol ReceiptFaqCellDelegate: AnyObject {
    func didTapFaqLink(with url: String)
}

final class ReceiptFaqCell: UITableViewCell {
    // MARK: - Views
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var faqButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(didTapLink), for: .touchUpInside)
        return button
    }()
    
    private lazy var separator = SeparatorView(style: BackgroundViewStyle(color: .grayscale100()))
    
    // MARK: Properties
    
    private weak var delegate: ReceiptFaqCellDelegate?
    private var faqUrl: String?
    
    // MARK: - Inits
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Configure
    
    func configure(with viewModel: ReceiptFaqCellViewModel, and delegate: ReceiptFaqCellDelegate) {
        titleLabel.text = viewModel.title
        faqButton.setTitle(viewModel.linkTitle, for: .normal)
        
        faqButton.buttonStyle(LinkButtonStyle(size: .small))
            .with(\.contentHorizontalAlignment, .center)
            .with(\.contentVerticalAlignment, .center)
        
        faqUrl = viewModel.linkUrl
        self.delegate = delegate
    }
    
    @objc
    func didTapLink() {
        guard let url = faqUrl else { return }
        delegate?.didTapFaqLink(with: url)
    }
}

// MARK: - ViewConfiguration

extension ReceiptFaqCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(faqButton)
        contentView.addSubview(separator)
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        faqButton.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview().inset(Spacing.base02)
        }
        
        separator.snp.makeConstraints {
            $0.top.equalTo(faqButton.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    func configureViews() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
