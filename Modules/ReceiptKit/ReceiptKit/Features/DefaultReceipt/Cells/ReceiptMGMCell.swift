import SnapKit
import UI
import UIKit

protocol ReceiptMGMCellDelegate: AnyObject {
    func didTapMGMButton()
}

private extension ReceiptMGMCell.Layout {
    enum Size {
        static let icon = CGSize(width: 150, height: 150)
    }
}

final class ReceiptMGMCell: UITableViewCell {
    fileprivate enum Layout { }
    
    // MARK: - Views
    
    private lazy var iconImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
            .with(\.textColor, .brandingBase())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var txtLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var mgmButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(SecondaryButtonStyle(size: .default))
        button.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
        return button
    }()
    
    // MARK: Properties
    
    private weak var delegate: ReceiptMGMCellDelegate?
    
    // MARK: - Inits
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Configure
    
    func configure(with viewModel: ReceiptMGMCellViewModel, and delegate: ReceiptMGMCellDelegate) {
        iconImage.setImage(url: viewModel.imageUrl)
        titleLabel.text = viewModel.title
        txtLabel.text = viewModel.text
        mgmButton.setTitle(viewModel.button, for: .normal)
        
        self.delegate = delegate
    }
    
    @objc
    func didTapButton() {
        delegate?.didTapMGMButton()
    }
}

extension ReceiptMGMCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(iconImage)
        contentView.addSubview(titleLabel)
        contentView.addSubview(txtLabel)
        contentView.addSubview(mgmButton)
    }
    
    func setupConstraints() {
        iconImage.snp.makeConstraints {
            $0.top.equalToSuperview().inset(Spacing.base02)
            $0.centerX.equalToSuperview()
            $0.size.equalTo(Layout.Size.icon)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(iconImage.snp.bottom).offset(Spacing.base00)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        txtLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        mgmButton.snp.makeConstraints {
            $0.top.equalTo(txtLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.bottom.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureViews() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
