public enum ReceiptButtonType: String, Decodable {
    case ghost
}

public enum ReceiptButtonActionType: String, Decodable {
    case mgm = "open_mgm"
}

struct ReceiptButtonEventProperties: Decodable {
    enum ReceiptType: String, Decodable {
        case pav = "Pav"
        case pix = "PixTransaction"
    }
    
    let receiptType: ReceiptType?
    
    enum CodingKeys: String, CodingKey {
        case receiptType = "receipt_type"
    }
}

struct ReceiptButtonItem: Decodable {
    let title: String
    let type: ReceiptButtonType
    let action: ReceiptButtonActionType
    let eventProperties: ReceiptButtonEventProperties?
    
    enum CodingKeys: String, CodingKey {
        case title
        case type
        case action
        case actionData = "action_data"
        case eventProperties = "event_properties"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        title = try container.decode(String.self, forKey: .title)
        type = try container.decode(ReceiptButtonType.self, forKey: .type)
        action = try container.decode(ReceiptButtonActionType.self, forKey: .action)
        
        let actionData = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .actionData)
        eventProperties = try actionData.decode(ReceiptButtonEventProperties.self, forKey: .eventProperties)
    }
}
