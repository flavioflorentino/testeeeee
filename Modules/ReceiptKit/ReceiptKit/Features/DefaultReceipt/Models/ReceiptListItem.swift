class ReceiptListItem: ReceiptBaseItem {
    let itens: [ReceiptLabelValueItem]
    
    enum CodingKeys: String, CodingKey {
        case itens
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        itens = try container.decode([ReceiptLabelValueItem].self, forKey: .itens)
        
        try super.init(from: decoder)
    }
}
