struct ReceiptValueItem: Decodable {
    let color: String
    let value: String
    let url: String?
}
