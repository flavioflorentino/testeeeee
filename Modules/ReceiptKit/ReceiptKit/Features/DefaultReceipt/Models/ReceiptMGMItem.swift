class ReceiptMGMItem: ReceiptBaseItem {
    let image: String
    let title: ReceiptValueItem
    let button: ReceiptButtonItem
    let text: ReceiptValueItem
    
    enum CodingKeys: String, CodingKey {
        case image = "img_url"
        case title
        case button
        case text
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        image = try container.decode(String.self, forKey: .image)
        title = try container.decode(ReceiptValueItem.self, forKey: .title)
        button = try container.decode(ReceiptButtonItem.self, forKey: .button)
        text = try container.decode(ReceiptValueItem.self, forKey: .text)
        
        try super.init(from: decoder)
    }
}
