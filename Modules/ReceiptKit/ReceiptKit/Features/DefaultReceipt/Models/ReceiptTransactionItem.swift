enum ReceiptPayeeType: String, Decodable {
    case store
}

struct ReceiptPayeeItem: Decodable {
    let type: ReceiptPayeeType
    let id: String
    let name: String
    let imgUrl: String
    let isVerified: Bool
    
    enum CodingKeys: String, CodingKey {
        case type
        case id
        case name
        case imgUrl = "img_url"
        case isVerified = "is_verified"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        type = try container.decode(ReceiptPayeeType.self, forKey: .type)
        
        let rawId = try container.decode(QuantumValue.self, forKey: .id)
        id = rawId.value

        name = try container.decode(String.self, forKey: .name)
        imgUrl = try container.decode(String.self, forKey: .imgUrl)
        isVerified = try container.decodeIfPresent(Bool.self, forKey: .isVerified) ?? false
    }
}

struct ReceiptLabelTextItem: Decodable {
    let label: String
    let text: String
}

class ReceiptTransactionItem: ReceiptBaseItem {
    let id: String
    let total: String
    let date: String
    let isCancelled: Bool
    
    let shareText: String
    let shareUrl: String
    
    let payee: ReceiptPayeeItem
    let list: [ReceiptLabelTextItem]
    
    enum CodingKeys: String, CodingKey {
        case type
        case id
        case total
        case date
        case isCancelled = "is_cancelled"
        case share
        case shareText = "text"
        case shareUrl = "url"
        case payee
        case list
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let rawId = try container.decode(QuantumValue.self, forKey: .id)
        id = rawId.value
        
        total = try container.decode(String.self, forKey: .total)
        date = try container.decode(String.self, forKey: .date)
        isCancelled = try container.decode(Bool.self, forKey: .isCancelled)
        
        let shareContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .share)
        shareText = try shareContainer.decode(String.self, forKey: .shareText)
        shareUrl = try shareContainer.decode(String.self, forKey: .shareUrl)
        
        payee = try container.decode(ReceiptPayeeItem.self, forKey: .payee)
        list = try container.decode([ReceiptLabelTextItem].self, forKey: .list)
        
        try super.init(from: decoder)
    }
}
