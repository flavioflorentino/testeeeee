public enum ReceiptType: String {
    case pix
    case pav
    case p2p
    case p2pExternalTransfer = "P2pExternalTransfer"
    case pixReceiver
    case p2pLending
    
    public init?(rawValue: String) {
        switch rawValue {
        case "pix":
            self = .pix
        case "pav":
            self = .pav
        case "p2p":
            self = .p2p
        case "P2pExternalTransfer":
            self = .p2pExternalTransfer
        case "pixReceiver":
            self = .pixReceiver
        case "p2pLending":
            self = .p2pLending
        default:
            return nil
        }
    }
}
