public class ReceiptBaseItem: Decodable {
    let type: ReceiptClassFamily
    
    enum CodingKeys: String, CodingKey {
        case type
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        type = try container.decodeIfPresent(ReceiptClassFamily.self, forKey: .type) ?? .unknown
    }
}
