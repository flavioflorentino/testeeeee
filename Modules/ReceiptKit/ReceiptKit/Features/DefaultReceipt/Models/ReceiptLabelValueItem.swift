class ReceiptLabelValueItem: ReceiptBaseItem {
    let label: ReceiptValueItem
    let value: ReceiptValueItem
    
    enum CodingKeys: String, CodingKey {
        case label
        case value
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        label = try container.decode(ReceiptValueItem.self, forKey: .label)
        value = try container.decode(ReceiptValueItem.self, forKey: .value)
        
        try super.init(from: decoder)
    }
}
