public struct ReceiptModel: Decodable {
    public let id: String?
    public let itens: [ReceiptBaseItem]
    
    enum CodingKeys: String, CodingKey {
        case data
        case id
        case itens = "receipt"
    }
    
    public init(id: String, itens: [ReceiptBaseItem]) {
        self.id = id
        self.itens = itens
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let data = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        
        let rawId = try data.decodeIfPresent(QuantumValue.self, forKey: .id)
        id = rawId?.value

        itens = try data.decodeHeterogeneousArray(family: ReceiptClassFamily.self, forKey: .itens)
    }
}

extension ReceiptModel: Equatable {
    public static func == (lhs: ReceiptModel, rhs: ReceiptModel) -> Bool {
        lhs.id == rhs.id && lhs.itens.count == rhs.itens.count
    }
}
