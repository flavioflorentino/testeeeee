class ReceiptBilletItem: ReceiptBaseItem {
    let title: String
    
    enum CodingKeys: String, CodingKey {
        case title
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        title = try container.decode(String.self, forKey: .title)
        
        try super.init(from: decoder)
    }
}
