enum ReceiptClassFamily: String, DecodableClassFamily {
    typealias BaseType = ReceiptBaseItem
    
    static var discriminator: Discriminator { .type }

    case transaction
    case transactionStatus = "transaction_status"
    case list
    case labelValue = "label_value_item"
    case faq
    case mgmOffer = "mgm_offer"
    case billet = "button_action"
    case unknown
    
    func getType() -> ReceiptBaseItem.Type {
        switch self {
        case .transaction:
            return ReceiptTransactionItem.self
        case .transactionStatus:
            return ReceiptTransactionStatusItem.self
        case .list:
            return ReceiptListItem.self
        case .labelValue, .faq:
            return ReceiptLabelValueItem.self
        case .billet:
            return ReceiptBilletItem.self
        case .mgmOffer:
            return ReceiptMGMItem.self
        default:
            return ReceiptBaseItem.self
        }
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let rawValue = try container.decode(RawValue.self)
        self = ReceiptClassFamily(rawValue: rawValue) ?? .unknown
    }
}
