class ReceiptTransactionStatusItem: ReceiptBaseItem {
    let title: ReceiptValueItem
    let subtitle: ReceiptValueItem
    
    enum CodingKeys: String, CodingKey {
        case title
        case subtitle
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        title = try container.decode(ReceiptValueItem.self, forKey: .title)
        subtitle = try container.decode(ReceiptValueItem.self, forKey: .subtitle)
        
        try super.init(from: decoder)
    }
}
