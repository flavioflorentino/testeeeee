import Foundation

public protocol RemoteConfigContractOverride {
    func override<T>(key: FeatureConfig, with value: T) where T: Decodable
}

public protocol ThirdPartyRemoteConfigMock: ThirdPartyRemoteConfigContract & RemoteConfigContractOverride { }

#if DEBUG
public final class RemoteConfigMock: ThirdPartyRemoteConfigMock {
    internal let defaultConfiguration: ThirdPartyRemoteConfigContract?
    internal lazy var overrides: [String: Any] = [:]
    
    public init(defaultConfiguration: ThirdPartyRemoteConfigContract? = RemoteConfigSetup.remoteConfigInstance) {
        self.defaultConfiguration = defaultConfiguration
    }
    
    public static func with<T: Decodable>(key: FeatureConfig, value: T) -> RemoteConfigMock {
        let rcmock = RemoteConfigMock()
        rcmock.override(key: key, with: value)
        return rcmock
    }
    
    public static func with<T: Decodable>(keys: FeatureConfig..., value: T) -> RemoteConfigMock {
        let rcmock = RemoteConfigMock()
        rcmock.override(keys: keys, with: value)
        return rcmock
    }
    
    public static func with(dict: [FeatureConfig: Any]) -> RemoteConfigMock {
        let rcmock = RemoteConfigMock()
        rcmock.override(with: dict)
        return rcmock
    }
    
    public func activate() {
        defaultConfiguration?.activate()
    }
    
    public func fetch(withExpirationDuration: TimeInterval, successHandler: @escaping () -> Void) {
        defaultConfiguration?.fetch(withExpirationDuration: withExpirationDuration, successHandler: successHandler)
    }
    
    public func object<T>(type: T.Type, for key: FeatureConfig) -> T? {
        object(type: type.self, for: key.rawValue)
    }
    
    public func object<T>(type: T.Type, for key: String) -> T? {
        guard let value = overrides[key] as? T else {
            return defaultConfiguration?.object(type: T.self, for: key)
        }
        return value
    }
    
    /// Overrides a pre defined value for a settings
    /// - Parameters:
    ///   - key: The settings FeatureConfig to be overriden
    ///   - value: The value to override
    public func override<T>(key: FeatureConfig, with value: T) where T: Decodable {
        overrides[key.rawValue] = value
    }
    
    /// Overrides a pre defined value for a settings
    /// - Parameters:
    ///   - keys: The settings FeatureConfig to be overriden
    ///   - value: The value to override
    public func override<T>(keys: FeatureConfig..., with value: T) where T: Decodable {
        override(keys: keys, with: value)
    }
    
    /// Overrides a pre defined value for a settings, for when you need to setup a several keys with different values
    /// - Parameters:
    ///   - dict: The dictionary containing FeatureConfigs and Values
    public func override(with dict: [FeatureConfig: Any]) {
        overrides = Dictionary(uniqueKeysWithValues: dict.map { key, value in ((key.rawValue), value) })
    }
    
    private func override<T>(keys: [FeatureConfig], with value: T) where T: Decodable {
        keys.forEach({ key in overrides[key.rawValue] = value })
    }
}
#endif
