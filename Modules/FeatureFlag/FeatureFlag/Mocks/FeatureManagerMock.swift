import Foundation

#if DEBUG
public final class FeatureManagerMock: FeatureManagerContract {
    internal lazy var overrides: [String: Any] = [:]
    public init() { }
    
    public func isActive(_ featureConfig: FeatureConfig) -> Bool {
        resolve(featureConfig)
    }
    
    public func text(_ featureConfig: FeatureConfig) -> String {
        resolve(featureConfig)
    }
    
    public func number(_ featureConfig: FeatureConfig) -> NSNumber? {
        resolve(featureConfig)
    }
    
    public func int(_ featureConfig: FeatureConfig) -> Int? {
        resolve(featureConfig)
    }
    
    public func json(_ featureConfig: FeatureConfig) -> [String: Any]? {
        resolve(featureConfig)
    }
    
    public func object<T>(_ featureConfig: FeatureConfig, type: T.Type) -> T? where T: Decodable {
        resolve(featureConfig)
    }
    
    public static func isActive(_ featureConfig: FeatureConfig) -> Bool {
        resolve(featureConfig)
    }

    public static func text(_ featureConfig: FeatureConfig) -> String {
        resolve(featureConfig)
    }

    public static func number(_ featureConfig: FeatureConfig) -> NSNumber? {
        resolve(featureConfig)
    }

    public static func int(_ featureConfig: FeatureConfig) -> Int? {
        resolve(featureConfig)
    }

    public static func json(_ featureConfig: FeatureConfig) -> [String: Any]? {
        resolve(featureConfig)
    }

    public static func object<T>(_ featureConfig: FeatureConfig, type: T.Type) -> T? where T: Decodable {
        resolve(featureConfig)
    }

    public func update(completion: (() -> Void)?) {
        completion?()
    }
    
    /// Overrides a pre defined value for a settings
    /// - Parameters:
    ///   - key: The settings FeatureConfig to be overriden
    ///   - value: The value to override
    public func override<T>(key: FeatureConfig, with value: T) where T: Decodable {
        overrides[key.rawValue] = value
    }
    
    /// Overrides a pre defined value for a settings, for when you need to setup a several keys with the same value
    /// - Parameters:
    ///   - keys: The setting FeatureConfig keys to be overriden
    ///   - value: The value to override
    public func override<T>(keys: FeatureConfig..., with value: T) where T: Decodable {
        keys.forEach({ key in overrides[key.rawValue] = value })
    }
    
    /// Overrides a pre defined value for a settings, for when you need to setup a several keys with different values
    /// - Parameters:
    ///   - dict: The dictionary containing FeatureConfigs and Values
    public func override(with dict: [FeatureConfig: Any]) {
        overrides = Dictionary(uniqueKeysWithValues: dict.map { key, value in ((key.rawValue), value) })
    }
}

public extension FeatureManagerMock {
    func resolve<T>(_ featureConfig: FeatureConfig) -> T {
        guard let resolved = overrides[featureConfig.rawValue] as? T else {
            if let defaultObject = RemoteConfigSetup.remoteConfigInstance?.object(type: T.self, for: featureConfig.rawValue) {
                return defaultObject
            } else {
                fatalError("Could not find remoteconfiginstance or cast object failed")
            }
        }
        return resolved
    }
    
    static func resolve<T>(_ featureConfig: FeatureConfig) -> T {
        fatalError("It needs implementation for Static funcs")
    }
}
#endif
