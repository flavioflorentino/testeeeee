import Foundation

public final class FeatureFlagControlModel: CustomStringConvertible {
    let key: FeatureConfig
    var value: Bool
    var text: String {
        key.rawValue.replacingOccurrences(of: "feature_", with: "")
            .replacingOccurrences(of: "_bool", with: "")
    }
    
    init(key: FeatureConfig, value: Bool) {
        self.key = key
        self.value = value
    }
    
    public var description: String {
        "\(key.rawValue) | \(value)"
    }
}
