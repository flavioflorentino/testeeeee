import Foundation
import UIKit

protocol FeatureFlagControlCellDelegate: AnyObject {
    func switchDidActuate(model: FeatureFlagControlModel)
    func saveCacheDidActuate(value: Bool)
}

final class FeatureFlagControlCell: UITableViewCell {
    static let identifier = String(describing: self)
    private var viewModel: FeatureFlagControlModel?
    private weak var delegate: FeatureFlagControlCellDelegate?
    
    let cacheLabel = "Salvar Cache"
    
    private lazy var flagNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 16)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var switchButton: UISwitch = {
        let switchButton = UISwitch()
        switchButton.translatesAutoresizingMaskIntoConstraints = false
        switchButton.addTarget(self, action: #selector(switchDidChange), for: .valueChanged)
        return switchButton
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        buildLayout()
    }
    
    func configure(with switchModel: FeatureFlagControlModel, andDelegate delegate: FeatureFlagControlCellDelegate) {
        self.viewModel = switchModel
        self.delegate = delegate
        
        flagNameLabel.text = switchModel.text
        switchButton.setOn(switchModel.value, animated: false)
        selectionStyle = .none
    }
    
    func configureCacheCell(delegate: FeatureFlagControlCellDelegate) {
        self.delegate = delegate
        self.viewModel = nil
        flagNameLabel.text = cacheLabel
        switchButton.setOn(FeatureFlagControlCache.isOn, animated: false)
        selectionStyle = .none
    }
    
    @objc
    private func switchDidChange() {
        if flagNameLabel.text == cacheLabel {
            delegate?.saveCacheDidActuate(value: switchButton.isOn)
        } else {
            guard let model = viewModel else { return }
            model.value = switchButton.isOn
            delegate?.switchDidActuate(model: model)
        }
    }
}

private extension FeatureFlagControlCell {
    func buildLayout() {
        buildHierarchy()
        buildConstraints()
    }
    
    func buildHierarchy() {
        contentView.addSubview(flagNameLabel)
        contentView.addSubview(switchButton)
    }
    
    func buildConstraints() {
        NSLayoutConstraint.activate([
            flagNameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16),
            flagNameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            flagNameLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -16)
        ])
        
        NSLayoutConstraint.activate([
            switchButton.centerYAnchor.constraint(equalTo: flagNameLabel.centerYAnchor),
            switchButton.leadingAnchor.constraint(equalTo: flagNameLabel.trailingAnchor, constant: 8),
            switchButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            switchButton.widthAnchor.constraint(equalToConstant: 50)
        ])
    }
}
