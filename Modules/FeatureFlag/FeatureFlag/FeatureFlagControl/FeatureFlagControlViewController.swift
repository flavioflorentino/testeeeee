import Foundation
import UIKit

public final class FeatureFlagControlViewController: UIViewController {
    public enum Section: Int {
        case recents
        case all
    }
    
    private var filteredDataSource: [FeatureFlagControlModel]
    private var dataSource: [Section: [FeatureFlagControlModel]]
    private var switchesActivatedOnSearch: [FeatureFlagControlModel] = []
    
    private var allSwitches: [FeatureFlagControlModel] {
        dataSource[.all] ?? []
    }
    
    private var recentSwitches: [FeatureFlagControlModel] {
        dataSource[.recents] ?? []
    }
    
    private var isSearching: Bool {
        guard let searchText = searchController.searchBar.text else { return false }
        return !searchText.isEmpty
    }
    
    private let saveCacheRow = 1
    
    init(with dataSource: [Section: [FeatureFlagControlModel]]) {
        self.dataSource = dataSource
        self.filteredDataSource = []
        super.init(nibName: nil, bundle: nil)
        self.filteredDataSource = allSwitches
        setupViewController()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var searchController: UISearchController = {
        let controller = UISearchController(searchResultsController: nil)
        controller.searchResultsUpdater = self
        controller.obscuresBackgroundDuringPresentation = false
        controller.searchBar.placeholder = "Busque sua flag"
        controller.searchBar.autocapitalizationType = .none
        controller.searchBar.delegate = self
        return controller
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(FeatureFlagControlCell.self, forCellReuseIdentifier: FeatureFlagControlCell.identifier)
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 64
        return tableView
    }()
    
    private func setupViewController() {
        title = "🛠 Feature Flag Control 🛠"
        
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
        } else {
            navigationItem.titleView = searchController.searchBar
        }
        
        view.addSubview(tableView)
        setupConstraints()
        tableView.reloadData()
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
    }
}

extension FeatureFlagControlViewController: UISearchResultsUpdating {
    public func updateSearchResults(for searchController: UISearchController) {
        guard let flagSearched = searchController.searchBar.text else { return }
        
        filteredDataSource = flagSearched.isEmpty ? allSwitches : allSwitches.filter {
            $0.text.range(of: flagSearched, options: .caseInsensitive) != nil
        }

        tableView.reloadData()
    }
}

extension FeatureFlagControlViewController: UITableViewDataSource {
    public func numberOfSections(in tableView: UITableView) -> Int {
        isSearching ? 1 : 2
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            return filteredDataSource.count
        }
        return section == Section.recents.rawValue ? recentSwitches.count + saveCacheRow : allSwitches.count
    }
    
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if isSearching {
            return nil
        }
        return section == Section.recents.rawValue ? "Recentes" : "Todos"
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: FeatureFlagControlCell.identifier) as? FeatureFlagControlCell else {
            return UITableViewCell()
        }
        
        guard !isSearching else {
            let model = filteredDataSource[indexPath.row]
            cell.configure(with: model, andDelegate: self)
            
            return cell
        }
        
        if indexPath.section == Section.recents.rawValue && indexPath.row == 0 {
            cell.configureCacheCell(delegate: self)
            return cell
        }
        
        var model: FeatureFlagControlModel
        if indexPath.section == Section.all.rawValue {
            model = allSwitches[indexPath.row]
        } else {
            model = recentSwitches[indexPath.row - saveCacheRow]
        }
        
        cell.configure(with: model, andDelegate: self)
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let index = indexPath.row - saveCacheRow
            let model = recentSwitches[index]
            var switches = recentSwitches
            switches.remove(at: index)
            
            FeatureFlagControlCache.remove(model.key.rawValue)
            dataSource[.recents] = switches
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: .fade)
            tableView.endUpdates()
        }
    }
    
    public func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if isSearching {
            return false
        }
        return indexPath.section == Section.recents.rawValue && indexPath.row != 0
    }
}

extension FeatureFlagControlViewController: FeatureFlagControlCellDelegate {
    func saveCacheDidActuate(value: Bool) {
        FeatureFlagControlCache.saveCache(value: value)
    }
    
    func switchDidActuate(model: FeatureFlagControlModel) {
        if !isSearching {
            updateCellsWith(model)
        } else {
            switchesActivatedOnSearch.append(model)
        }
        
        FeatureFlagControlCache.save(key: model.key.rawValue, value: model.value)
        FeatureFlagControl.shared.remoteConfigMock.override(key: model.key, with: model.value)
    }
    
    func updateCellsWith(_ model: FeatureFlagControlModel) {
        if FeatureFlagControlCache.isOn && !recentSwitchesHas(model.key) {
            updateRowInsertion(model: model)
        }
        
        if FeatureFlagControlCache.isOn {
            updateRowTwin(model)
        }
    }
    
    func recentSwitchesHas(_ key: FeatureConfig) -> Bool {
        recentSwitches.contains(where: { $0.key == key })
    }
    
    func updateRowInsertion(model: FeatureFlagControlModel) {
        var switches = recentSwitches
        switches.append(model)
        dataSource[.recents] = switches
        tableView.beginUpdates()
        tableView.insertRows(at: [IndexPath(row: recentSwitches.count, section: Section.recents.rawValue)], with: .automatic)
        tableView.endUpdates()
    }
    
    func updateRowTwin(_ model: FeatureFlagControlModel) {
        guard let allIndexPath = updateDataSource(model.key, value: model.value, isRecent: false),
              let recentIndexPath = updateDataSource(model.key, value: model.value, isRecent: true) else {
            return
        }
        
        tableView.beginUpdates()
        tableView.reloadRows(at: [recentIndexPath, allIndexPath], with: .fade)
        tableView.endUpdates()
    }
    func updateDataSource(_ key: FeatureConfig, value: Bool, isRecent: Bool) -> IndexPath? {
        let switches = isRecent ? recentSwitches : allSwitches
        guard let index = switches.firstIndex(where: { $0.key == key }) else { return nil }
        let model = dataSource[isRecent ? .recents : .all]?[index]
        model?.value = value
        
        let indexPath = IndexPath(item: isRecent ? index + 1 : index,
                                  section: isRecent ? Section.recents.rawValue : Section.all.rawValue)
        
        return indexPath
    }
}

extension FeatureFlagControlViewController: UISearchBarDelegate {
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        // 💩💩💩
        // Withgout the dispatch this code its called too fast and too furious
        // making the app crash, i wholeheartedly accept suggestions to improve this
        // and to get rid of asyncAfter
        // 💩💩💩
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if !self.switchesActivatedOnSearch.isEmpty {
                self.switchesActivatedOnSearch.forEach {
                    self.updateCellsWith($0)
                }
                self.switchesActivatedOnSearch = []
            }
        }
    }
}
