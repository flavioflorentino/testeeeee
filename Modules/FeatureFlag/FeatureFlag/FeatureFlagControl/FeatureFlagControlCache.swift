import Foundation

enum FeatureFlagControlCache {
    private static var shouldSaveCache: Bool {
        retrieve(key: "save_cache")
    }
    
    private static var saveCacheKey: String {
        "save_cache"
    }
    
    private static var userDefaultSuiteName: String {
        "FeatureFlagControl"
    }
    
    private static var featureFlagControlDefault: UserDefaults? {
        UserDefaults(suiteName: userDefaultSuiteName)
    }
    
    static var isOn: Bool {
        retrieve(key: saveCacheKey)
    }
    
    static func saveCache(value: Bool) {
        save(key: saveCacheKey, value: value)
    }
    
    static func save(key: String, value: Bool) {
        if !shouldSaveCache && key != saveCacheKey {
            return
        }
        
        if key == saveCacheKey && !value {
            resetFeatureFlagControlCache()
        }
        featureFlagControlDefault?.setValue(value, forKey: key)
    }
    
    static func remove(_ key: String) {
        featureFlagControlDefault?.removeObject(forKey: key)
    }
    
    static func retrieve(key: String) -> Bool {
        featureFlagControlDefault?.value(forKey: key) as? Bool ?? false
    }
    
    static func hasValueOnCacheFor(key: String) -> Bool {
        ((featureFlagControlDefault?.value(forKey: key)) != nil)
    }
    
    static func allKeys() -> [FeatureFlagControlModel] {
        guard let allKeysAndValues = featureFlagControlDefault?.dictionaryRepresentation() else {
            return []
        }
        
        var keyAndValueArray: [FeatureFlagControlModel] = []
        for (key, value) in allKeysAndValues {
            guard let config = FeatureConfig(rawValue: key), let boolValue = value as? Bool else {
                continue
            }
            keyAndValueArray.append(FeatureFlagControlModel(key: config, value: boolValue))
        }
        
        return keyAndValueArray
    }
    
    private static func resetFeatureFlagControlCache() {
        let all = featureFlagControlDefault?.dictionaryRepresentation() ?? [:]
        for (key, _) in all {
            featureFlagControlDefault?.removeObject(forKey: key)
        }
    }
}
