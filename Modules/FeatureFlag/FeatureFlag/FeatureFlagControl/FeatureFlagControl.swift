import Foundation

struct FeatureFlagControlRemoteConfigMock: ThirdPartyRemoteConfigMock {
    func activate() { }
    func fetch(withExpirationDuration: TimeInterval, successHandler: @escaping () -> Void) { }
    func override<T>(key: FeatureConfig, with value: T) where T: Decodable { }
    
    func object<T>(type: T.Type, for key: String) -> T? {
        type as? T
    }
}

public final class FeatureFlagControl {
    public static var shared: FeatureFlagControl = {
        FeatureFlagControl()
    }()
    
    public var remoteConfigMock: ThirdPartyRemoteConfigMock = {
        #if DEBUG
        return RemoteConfigMock()
        #else
        return FeatureFlagControlRemoteConfigMock()
        #endif
    }()
    
    private init() { }
    
    private (set) var switches: [FeatureFlagControlModel] = []
    private (set) var switchesSection: [FeatureFlagControlViewController.Section: [FeatureFlagControlModel]] = [:]
    
    public func buildSwitches(from dict: [String: Any]) {
        let switches = filterOutNotBoolAndInvalidKeys(from: dict)
        let sorted = sortAlphabetically(switches)
        self.switches = sorted
        self.switchesSection[.all] = sorted
    }
    
    public func activateCache() {
        FeatureFlagControlCache.allKeys().forEach {
            FeatureFlagControl.shared.remoteConfigMock.override(key: $0.key, with: $0.value)
        }
    }
    
    public func buildCacheSwitches() {
        let switchesCache = FeatureFlagControlCache.allKeys()
        let sorted = sortAlphabetically(switchesCache)
        self.switchesSection[.recents] = sorted
    }
    
    public func controller() -> FeatureFlagControlViewController {
        buildCacheSwitches()
        return FeatureFlagControlViewController(with: switchesSection)
    }
}

extension FeatureFlagControl {
    private func filterOutNotBoolAndInvalidKeys(from dict: [String: Any]) -> [FeatureFlagControlModel] {
        var mapped: [FeatureFlagControlModel] = []
        
        for (key, value) in dict {
            guard
                let config = FeatureConfig(rawValue: key),
                value as? Bool != nil
                else {
                    continue
            }            
            var currentValue = FeatureManager.shared.isActive(config)
            if FeatureFlagControlCache.hasValueOnCacheFor(key: key) {
                currentValue = FeatureFlagControlCache.retrieve(key: key)
            }
            mapped.append(FeatureFlagControlModel(key: config, value: currentValue))
        }
        
        return mapped
    }
    
    private func sortAlphabetically(_ switches: [FeatureFlagControlModel]) -> [FeatureFlagControlModel] {
        switches.sorted(by: { $0.text < $1.text })
    }
}
