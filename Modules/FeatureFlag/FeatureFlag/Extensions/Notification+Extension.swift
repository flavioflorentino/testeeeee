import Foundation

public extension Notification.Name {
    enum RemoteConfig {
        public static let fetched = Notification.Name("RemoteConfigFetched")
    }
}
