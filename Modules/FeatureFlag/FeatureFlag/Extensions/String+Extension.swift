import Foundation

extension String {
    var boolValue: Bool {
        Bool(self) ?? false
    }
}
