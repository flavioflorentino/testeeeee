import Foundation

// swiftlint:disable:next type_body_length
public enum FeatureConfig: String {
    // MARK: Onboarding
    case firstGift = "feature_birthday_gift_message_1"
    case secondGift = "feature_birthday_gift_message_2"
    case thirdGift = "feature_birthday_gift_message_3"
    case buttonGift = "feature_birthday_gift_cta"
    case originalOnboardingTitle = "feature_onboardingContaOriginal_title"
    case originalOnboardingDescription = "feature_onboardingContaOriginal_description"
    case linkHelpDebit = "feature_debit_helpcenter_deeplink"
    
    // MARK: PCI
    case pciLinx = "feature_pci_linx"
    case pciP2P = "feature_pci_p2p"
    case pciPAV = "feature_pci_pav"
    case pciP2M = "feature_pci_p2m"
    case pciTvRecharge = "feature_pci_tvRecharge"
    case pciVoucher = "feature_pci_voucher"
    case pciTransitPass = "feature_pci_transitPass"
    case pciPhoneRecharge = "feature_pci_phoneRecharge"
    case pciBluezone = "feature_pci_bluezone"
    case pciParking = "feature_pci_parking"
    case pciBirthday = "feature_pci_birthday"
    case pciNewFriendGift = "feature_pci_new_friend_gift"
    case pciBill = "feature_pci_bill"
    case pciEcommerce = "feature_pci_ecommerce"
    case pciSubscription = "feature_pci_subscription"
    case pciRecharge = "feature_pci_recharge"
    case pciCardVerification = "feature_pci_card_verification"
    case pciCardInsert = "feature_pci_card_insert"
    case pciInvoiceCard = "feature_pci_invoice_card"
    
    // MARK: Ecommerce
    case isPagTesouroOnlyCreditCardAvailable = "is_pagtesouro_only_credit_card_available"

    // MARK: New Payments
    case newEcommerce = "feature_new_ecommerce"
    case newTvRecharge = "feature_new_tv_recharge"
    case newP2P = "feature_new_p2p"
    case newP2M = "feature_new_p2m"
    case newVoucher = "feature_new_voucher"
    case newParking = "feature_new_parking"
    case newTransitPass = "feature_new_transit_pass"
    case newPhoneRecharge = "feature_new_phone_recharge"
    case newBirthday = "feature_new_birthday"
    case newInvoiceCard = "feature_new_invoice_card"
    
    case newOnboarding = "feature_new_onboarding"
    case onboardingContacts = "feature_onboarding_contacts"
    case onboardingCreditCard = "feature_onboarding_credit_card"
    case onboardingFirstAction = "feature_onboarding_first_action"
    case onboardingFirstActionList = "feature_onboarding_first_action_list"
    case cardVerification = "feature_challenge_credit_card"
    case newChangePassword = "feature_new_change_password"
    case featureFriendListRecommendation = "feature_friend_list_recommendation"
    
    case phoneRecurrenceReminder = "feature_phone_recurrence_reminder"
    case transportRecurrenceReminder = "feature_transport_recurrence_reminder"
  
    // MARK: Student Account
    case universityAccountBenefitsWebview = "university_account_benefits_webview"
    case universityAccountRulesWebview = "university_account_terms_webview"
    case universityAccountRegisterWebview = "university_account_register_webview"
    case storeWithUniversityAccountLabel = "feature_store_with_university_account_label"
    case settingsWithUniversityAccountWebview = "feature_settings_with_university_account_webview"
    case universityAccountBenefitDescription = "university_account_benefit_description"
    case featureStudentAccountProfileComponent = "feature_student_account_profile_component"
    case featureStudentAccountProfileComponentEnabled = "feature_student_account_profile_component_enabled"
    case featureStudentSignUpFlow = "feature_student_sign_up_flow"
    case featureShowStudentAccountPromotion = "feature_student_account_promotion"
    
    case opportunityCellsList = "feature_feed_with_opportunity_cells_list"
    case feedWithOpportunityCells = "feature_feed_with_opportunity_cells"
    case feedOpportunityScrollSendEvent = "feature_feed_opportunity_scroll_send_event"
    
    // MARK: TFA
    case featureTfaCreditcardFlowActive = "feature_tfa_creditcard_flow_active"
    
    case externalTransfer = "feature_external_transfer"
    case paymentRequestV2 = "feature_payment_request_V2"
    case paymentRequestDenounce = "feature_payment_request_denounce"

    case userPromotionsList = "feature_user_promotions_list"

    case phoneRegisterWithWhatsApp = "feature_phone_register_with_whatsapp"
    case codeVerifyHelpCenterPath = "feature_code_verify_help_center_path"
    case documentInUseDialog = "feature_cpf_in_use_dialog"
    case documentInUseFaqURL = "feature_cpf_in_use_faq_url"

    case subscriptionInformationsInSettings = "feature_subscriptions_information_in_settings"
    
    case featureBetaLink = "feature_beta_link"
    
    case landingScreenImagesV2 = "feature_landing_screen_images_v2"
    case landingScreenTextV2 = "feature_landing_screen_text_v2"
    case referralScreenText = "feature_landing_screen_text_referral"
    case enterWithPromotionalCode = "feature_enter_with_promotional_code"
    case featureRechargePhoneContactsList = "feature_recharge_phone_contacts_list"

    case vendingMachinePayments = "feature_vending_machine_payments"
    
    case featureQrCodeScanCashout24 = "feature_qr_code_scan_cashout_24"
    
    case featureSavings = "feature_config_savings"
    case featureReport = "feature_report"
    case featureReportProfile = "feature_report_profile"
    case picPayMeQrCode = "feature_pp_me_code"
    case faqErrorDepositDebit = "faq_error_deposit_debit"
  
    // MARK: Registration
    case dynamicRegistrationSteps = "feature_dynamic_registration_steps"
    case dynamicRegistrationStepsSequence = "feature_dynamic_registration_steps_sequence"
    case continueRegistration = "feature_signup_continue"
    case registrationCompliance = "feature_account_setup_realtime"

    // MARK: Bacen
    case faqJudicialBlockade = "faq_judicial_blockade"
    
    // MARK: Recharge
    case featureDepositDebit = "feature_deposit_debit"
    case featureOriginalDeeplink = "feature_original_deeplink_url"
    case releasePicpayAccountPresentingBool = "release_picpay_account_presenting_bool"
    
    // MARK: DeepLink Track Event
    case deepLinkTrackEvent = "deeplink_track_event"
    
    // MARK: FeesAndLimits
    case featureSectionPersonal = "taxes_and_limits_show_section_personal"
    case featureSectionPro = "taxes_and_limits_show_section_pro"
    case featureSectionLimits = "taxes_and_limits_show_section_limits"
    
    // MARK: UpgradeChecklist
    case upgradeDeeplink = "upgrade_checklist_deeplink"
    case upgradeDeeplinkFees = "upgrade_checklist_deeplink_fees_and_limits"
    case upgradeDeeplinkStatus = "upgrade_checklist_deeplink_status"
    
    // MARK: DGTicket
    case ticketCents = "feature_transportpass_allow_cents"
    
    // MARK: Transferencia e Deposito Banco
    case featureBank = "feature_availability_texts"
    
    // MARK: Boleto
    case boletoData = "feature_boleto_data"
    
    // MARK: Ouvidoria
    case ombudsmanUrl = "feature_ombudsman_url"
    
    // MARK: Search
    case tabList = "feature_tabs_list"
    case searchResultCache = "feature_search_result_cache"
    case searchLocalFilter = "feature_search_local_filter"
    case searchCardStyle = "feature_search_cardStyle"
    case searchHighlight = "feature_search_highlights"
    case newSearchEngineMain = "experiment_search_switch_new_api_tab_main_string"
    case newSearchEngineConsumer = "experiment_search_switch_new_api_tab_consumers_string"
    case newSearchEnginePlaces = "experiment_search_switch_new_api_tab_places_string"
    case newSearchEngineStore = "experiment_search_switch_new_api_tab_store_string"
    case searchBarLabel = "experiment_search_search_bar_label_string"
    
    case thirdPartyBankAccount = "feature_third_party_bank_account"

    case deactivateAccountScreenTitle = "feature_deactivateAccount_screenTitle"
    case deactivateAccountScreenDesc = "feature_deactivateAccount_screenDescription"
    case deactivateAccountSuccessTitle = "feature_deactivateAccount_alertSuccessTitle"
    case deactivateAccountSuccessDesc = "feature_deactivateAccount_alertSuccessDescription"
    case deactivateAccountConfirmTitle = "feature_deactivateAccount_dialogConfirmationTitle"

    case mGMCode = "feature_mgm_code"
    case mGB = "feature_mgb"

    case changeEmailPendingText = "feature_changeEmail_pending_text"
    case changeEmailConfirmedText = "feature_changeEmail_confirmed_text"
    case changeEmailUnconfirmedText = "feature_changeEmail_unconfirmed_text"
    case changeEmailHeaderText = "feature_changeEmail_header_text"
    
    case creditCardOBPopupInsertText = "feature_creditcardobpopup_text"
    
    case subscription = "feature_subscription"
    case subscriptionCard = "feature_subscription_card"
    case onboardMessages = "feature_onboarding_carousel"
    case leavePicPayProTitle = "feature_leavePicPayPro_title"
    case leavePicPayProText = "feature_leavePicPayPro_text"
    case leavePicPayProCTA = "feature_leavePicPayPro_cta"
    
    case leavePicPayProAlertTitle = "feature_leavePicPayPro_alertTitle"
    case leavePicPayProAlertMessage = "feature_leavePicPayPro_alertMessage"
    case leavePicPayProAlertCTA = "feature_leavePicPayPro_alertCta"
    case featureInstallmentNewButton = "feature_installment_new_button"
    
    // MARK: Card
    case featureFaqRequestCard = "feature_faq_request_card"
    case flagCardDebitOffer = "flag_card_debit_offer"
    case opsCardWalletBadgeBool = "ops_card_wallet_badge_bool"
    case opsCardReplacement = "ops_card_replacement_killswitch_bool"
    case opsCardReplacementFaq = "ops_card_replacement_faq_url"
    case opsCardTracking = "ops_card_tracking_presenting_bool"
    case opsDollarExchangeRate = "feature_dollar_exchange_rate"
    case opsDollarExchangeRateFaqUrl = "feature_dollar_exchange_rate_faq_url"
    case opsVirtualCard = "feature_virtual_card"
    case opsVirtualCardFaq = "feature_virtual_card_faq"
    case opsVirtualCardDelete = "feature_virtual_card_delete"
    case opsVirtualCardBlock = "is_virtualcard_block_available"
    case opsCardOnboardingCashout = "ops_card_onboarding_cashout_bool"
    case opsCardRequestAddressConfirmationFaqUrl = "ops_card_request_address_confirmation_faq_url"
    case experimentCardCreditLimitPresentingBool = "experiment_card_credit_limit_presenting_bool"
    case experimentCardDueDayPresentingBool = "experiment_card_due_day_presenting_bool"
    case linkFaqMultipleCardOnboarding = "link_faq_multiple_card_onboarding"
    case linkFaqDebitCardOnboarding = "link_faq_debit_card_onboarding"
    case isCardIdentityVerificationAvailable = "is_card_identity_verification_available"
    case linkContractDebitCard = "link_contract_debitCard"
    case faqChangeCardAddress = "faq_change_card_address"
    case faqDeliveryCardFailed = "faq_delivery_card_failed"
    case articleActivateCard = "article_activate_card"
    case experimentDebitCardOfferButtonBool = "experiment_debit_card_offer_button_conversion_bool"
    case experimentCreditCardOfferButtonBool = "experiment_credit_card_offer_button_conversion_bool"
    case isNewCardAddressScreenAvailable = "is_newcardaddress_screen_available"
    case isRegisterCardAvailableChangesInflow = "is_registerCard_availableChangesInflow"
    case debitCardRequestedText = "debit_card_requested_text"

    case helpCenterCardForUserWithNoOffer = "helpcenter_card_for_user_with_no_offer"
    case helpCenterCardForUserWithDebit = "helpcenter_card_for_user_with_debit"
    case helpCenterCardForUserWithCredit = "helpcenter_card_for_user_with_credit"
    
    case isCardBilletNotificationAvailable = "is_card_billet_notification_available"
    case isCardRegisterHybridDeeplinkActivated = "is_card_register_hybrid_deeplink_activated"
    case isCardOnboardingVisibilityActivated = "experiment_card_onboarding_visibility_boolean"
    
    case cardRedesignHome = "card_redesign_home"
    
    // MARK: Settings
    case address = "feature_address_active"
    case identityVerification = "feature_identity_active"
    case creditPicPay = "feature_creditpicpay"
    case creditPicpayLimitAjustment = "feature_creditpicpay_limit_adjustment"
    case cardRegistration = "feature_card_registration"
    case tPS = "feature_tps_active"
    case featureJoinBeta = "feature_join_beta"
    case renewalSettings = "feature_registration_renewal"
    case nativeScannerActive = "native_scanner_active"
    case taxAndLimitsWebView = "feature_taxAndLimits_webView"
    case isIncomeStatementsAvailable = "is_income_statements_available"

    case featureWalletCards = "feature_wallet_cards"

    case creditPicPayRegistrationStatusAnalysisMessage = "creditpicpay_registration_status_analysis_message"
    case creditpicpayOfferWebview = "creditpicpay_offer_webview"
    case creditpicpayTimeOfAnalysis = "creditpicpay_time_of_analysis"
    case textInstallmentNoInterestFooter = "text_installment_no_interest_footer"
    case cardAddressSearchPostalCodeUrl = "cardaddress_search_postal_code_url"
    
    case featureChallengeIdentity = "feature_challenge_identity"
    case featureChallengePopupCancel = "feature_challenge_popup_cancel"
    
    case linkRulesMultipleCardOnboarding = "link_rules_multiple_card_onboarding"

    // MARK: ATM24
    case featureCashout24hPopup = "feature_cashout24h_popup"
    
    case isActiveInstallmentP2P = "feature_installment_p2p_active"
    case touchIdAnalytics = "touch_id_analytics"
    case helpCenter = "feature_helpCenter"
    case featureShowWithdrawFeeEnabled = "feature_show_withdraw_fee_enabled"
    
    // MARK: - New Home
    case featureOpenAppTabV2 = "feature_open_app_tab_v2"
    case experimentSearchHomeBool = "experiment_search_home_bool"
    
    // MARK: Loan
    case featureLoan = "feature_loan"
    case loanSettings = "feature_loan_settings"
    case opsLoanInstallmentsStatus = "feature_loan_installments_status"
    case releaseLoanRegisterStatus = "release_loan_register_status"
    case opsFeatureLoanPicpayPayment = "ops_feature_loan_picpay_payment"
    case opsFeatureLoanSlipPayment = "ops_feature_loan_slip_payment"
    case opsFeatureLoanPayment = "ops_feature_loan_payment"
    case opsLoanWalletBanner = "ops_loan_wallet_banner"
    case releaseFeatureLoanSelfie = "release_feature_loan_selfie"
    case isFeatureLoanSimulationDueDateAvailable = "is_loan_simulation_due_date_available"
    case featureLoanConstracts = "feature_loan_contracts"
    case isAnticipateLoanInstallmentsAvailable = "is_anticipate_loan_installments_available"
    
    // MARK: P2M Scanner
    case featureP2mScannerBottomTitle = "feature_p2m_scanner_bottom_title"
    case featureP2mScannerBottomSubtitle = "feature_p2m_scanner_bottom_subtitle"
    case featureP2mScannerAcquirersEnabled = "feature_p2m_scanner_acquirers_enabled"
    case featureP2mTefScannerBool = "feature_p2m_tef_scanner_bool"
    
    // MARK: App Tour (Tutorial)
    case appTour = "feature_app_tour"
    
    // MARK: Payment Screen Bottom Sheet
    case featureShowOptionsBottomSheet = "feature_show_options_bottom_sheet"
    
    // Identity Validation
    case featureNewIdentityValidationFlow = "feature_new_identity_validation_flow"

    // MARK: AppSec
    case featureCertificatePinning = "feature_certificate_pinning"
    case featureCertificatePinningDomains = "feature_certificate_pinning_domains"
    case featureAppSwitcherObfuscation = "feature_app_switcher_obfuscation"
    case featureAppSecObfuscationMask = "feature_appsec_obfuscation_mask"
    case featureAppSecVerifyLogDetectionBool = "feature_appsec_verify_log_detection_bool"
    
	// Gift to a new friend
    case newFriendGift = "feature_new_friend_gift"
    case newFriendGiftTexts = "feature_new_friend_gift_texts"

    // Promotions
    case featurePromotionDetailsPhoneNumber = "feature_promotion_details_phone_number"
    case featurePromotionsListV2 = "feature_promotions_list_v2"
    
    // MARK: CashIn
    case faqVirtualCardRegistration = "faq_virtual_card_registration"
    case faqVirtualCardHelper = "faq_virtual_card_helper"
    case opsHelpRechargeTransactionSummaryString = "ops_help_recharge_transaction_summary_string"
    case featureDepositDebitCardChargeBool = "feature_deposit_debit_card_charge_bool"
    case opsHelpGovernmentRechargeString = "ops_help_government_recharge_string"
    case opsCashInTutorialWireTransferString = "feature_flag_ip_query_help"
    case opsCashInNotificationUpgradeAccountString = "ops_cash_in_notification_upgrade_account_string"
    case opsCashInNotificationReminderString = "ops_cash_in_notification_reminder_string"
    case featureTransferReceiptContactInfo = "feature_transfer_receipt_contact_info"
    case cashInMethodsMoreInfoUrl = "cash_in_methods_more_info_url"
    case isCashInRedesignAvailable = "is_cashin_redesign_available"
    case isCashinAccountAdjustmentsAvailable = "is_cashin_account_adjustments_available"
    
    // LINX
    case opsLinxKillswitchBool = "ops_linx_killswitch_bool"

    // Permissions
    case featureInitialPermissionsAfterLogin = "feature_initial_permissions_after_login"

    // Feed
    case featureOpportunitiesCardsFromRemote = "feature_opportunities_cards_from_remote"

    // MARK: - New feature flag pattern
    
    // MARK: Billet
    case experimentBilletPaymentDetailValues = "experiment_billet_payment_values_string"
    case releaseHowToPayBilletPresenting = "release_how_to_pay_billet_presenting_bool"
    case opsFaqHowToPayBilletUrl = "ops_faq_how_to_pay_billet_url"
    case opsBilletSelectionHelpUrl = "ops_billet_selection_help_url"
    case releaseNewBilletPaymentPresentingBool = "release_new_billet_payment_presenting_bool"
    case opsBilletPaymentFaqUrl = "ops_billet_payment_faq_url"
    case opsBilletPaymentInstructionsString = "ops_billet_payment_instructions_string"
    case releaseBilletPaymentSchedulePresentingBool = "release_billet_payment_schedule_presenting_bool"
    case isBarcodeV2Available = "is_barcode_v2_available"
    case isNewBilletHubAvailable = "is_new_billet_hub_available"
    case billetHubFaqUrl = "billet_hub_faq_url"
    case billetFollowUpFaqUrl = "billet_follow_up_faq_url"
    case isFollowUpAvailable = "is_follow_up_available"
    
    // Advertising
    case opsCardPinnedBannersHomeBool = "ops_card_pinned_banners_home_bool"
    case isAdvertisingInAppDialogAvailable = "is_advertising_inapp_available"
    
    // MARK: MGM
    case featureMgmIndicationCenter = "feature_mgm_indication_center"
    case referralRewardValue = "referral_reward_value"
    
    // Feature Management
    case blockMqttConnnection = "feature_block_connection_mqtt"
    case featureFeed = "feature_feed"
    case featureSearchMain = "feature_search_main"
    case featureSearchStore = "feature_search_store"
    case featureSearchLocale = "feature_search_locale"
    case featureSearchMainLocale = "feature_search_main_locale"
    case featureSearchSuggestions = "feature_search_suggestions"
    case featureSearchConsumers = "feature_search_consumers"
    case featurePicPayFlagsApi = "feature_picpay_flags_api"
    
    // MARK: Engagement
    case experimentLayoutSearchBool = "experiment_layout_search_bool"
    case experimentHintLabelSearchBool = "experiment_hint_label_search_bool"
    case releaseOpportunityCardsV2 = "release_opportunity_cards_v2"
    case experimentNewSuggestionRouteBool = "experiment_new_suggestion_route_bool"
    case experimentNewDialogLimitFeePaymentBool = "experiment_new_dialog_limit_fee_payment_v2_bool"
    case experimentInvertedInstallment = "experiment_inverted_installment_list_bool"
    case experimentNewAccessPromotionsMgmBool = "experiment_new_access_promotions_mgm_bool"
    case experimentNewAccessQrCode = "experiment_new_access_qr_code"
    case experimentNewLabelTransactionLocalDescription = "experiment_new_label_transaction_local_description"
    case experimentNewLabelTransactionLocalTitle = "experiment_new_label_transaction_local_title"
    case experimentShowSearchOnTransactionsMenu = "experiment_show_search_on_transactions_menu"
    case experimentNewAccessSettings = "experiment_new_access_settings"
    case experimentNewFeedCenteredHeader = "experiment_new_feed_centered_header"
    case experimentNewLabelPayButtonTabBarBool = "experiment_new_label_pay_button_tabbar_bool"
    case experimentShowScannerOnTransactionsMenu = "experiment_show_scanner_on_transactions_menu"

    // MARK: - CustomerSupport
    case chatbotUnlogedUser = "feature_chatbot_usuario_deslogado"
    case chatbotCovid19 = "feature_chatbot_motivo_covid19"
    case featureMinimumCharactersTicketMessage = "feature_zendesk_ticket_min_size"
    case featureHighlightArticlesZendesk = "ops_zendesk_highlight_articles_string"
    case featureHelpCenterNewExperienceBool = "feature_helpcenter_new_experience_bool"
    case featureExeceptionTags = "cs_multiple_tickets_contactreasons_tags"
    case isNewFAQHomeEndpointAvailable = "is_new_faq_home_endpoint_available"
    
    // MARK: Feedzai
    case transaction_p2p_v2_feedzai = "feature_transaction_p2p_feedzai"
    case transaction_pav_v2_feedzai = "feature_transaction_pav_feedzai"
    
    // MARK: - P2P
    case experimentPayPersonBool = "experiment_pay_person_bool"
    
    // MARK: Card
    case cardChangeAddressZendesk = "deeplink_card_credit_change_address"
    case featureCardBiometric = "ops_card_biometric_bool"
    case termsMultipleLink = "feature_membership_agreement"
    case termsDebitLink = "link_card_debit_contract"
    case simplifiedDebitRegister = "ops_card_simplified_debit_register"
    case cashbackActivation = "ops_cashback_activation_flow"
    
    // MARK: Mobility
    case releaseMetroRioFlowBool = "release_metro_rio_flow_bool"
    
    // MARK: P2PLending
    case releaseP2PLending = "release_p2plending_bool"
    case isP2PLendingBorrowerInstallmentsAvailable = "is_p2plending_borrower_installments_available"
    case isP2PLendingInvestorInstallmentsAvailable = "is_p2plending_investor_installments_available"
    case isP2PLendingBorrowerInstallmentDetailsAvailable = "is_p2plending_borrower_installments_details_available"
    case isP2PLendingCCBAuthAvailable = "is_p2plending_ccb_authentication_available"

    // MARK: Store
    case experimentStoreOpenNewStoreBool = "experiment_store_open_new_store_bool"
    case featureStoreHomeScreenId = "feature_store_home_screen_id"
    case storeTooltipShowCount = "max_store_tooltip_show_count"
    case settingsTooltipShowCount = "max_settings_tooltip_show_count"
    
    // MARK: - Transition for new layer api
    case transitionApiObjToCore = "experiment_api_obj_to_core_bool"
    case transitionApiSwiftJsonDGToCore = "experiment_api_dg_to_core_bool"
    case transitionApiSwiftJsonTEFToCore = "experiment_api_tef_to_core_bool"
    case transitionApiSwiftJsonCardHiringToCore = "experiment_api_card_hiring_to_core_bool"
    case transitionApiSwiftJsonSubscriptionsToCore = "experiment_api_subscriptions_to_core_bool"
    
    case opsTransitionCreditApiObjToCore = "ops_transaction_credit_api_obj_to_core_bool"
    
    // MARK: - Wallet
    case experimentWalletHeaderLayoutV2PresentingBool = "experiment_wallet_header_layout_v2_presenting_bool"
    case isStatementDetailEnabled = "is_statement_detail_enabled"
    case experimentWalletLayoutV1Available = "experiment_wallet_layout_v1_available"
    case walletConfigValues = "wallet_config_values"
    
    // MARK: - Password Reset
    case isPasswordResetEmailAvailable = "is_password_reset_email_available"
    case isPasswordResetLoggedinAvailable = "is_password_reset_loggedin_available"
    
    // PIX
    case releasePixKeyRegistrationHomePopupBool = "release_pix_key_registration_home_popup_bool"
    case releasePixKeyManagementOnSettingsBool = "release_pix_key_management_on_settings_bool"
    case releasePixCreateKeyValidationEnableBool = "release_pix_create_key_validation_enable_bool"
    case releasePixIdentityValidationBool = "release_pix_identity_validation_bool"
    case releasePixQRCodePFPaymentEnableBool = "release_pix_qrcode_pf_payment_enable_bool"
    case featurePixHubKeys = "feature_pix_hub_keys"
    case featurePixHubQrCode = "feature_pix_hub_qr_code"
    case featurePixHubManualBank = "feature_pix_hub_manual_bank"
    case featurePixHubReceiving = "feature_pix_hub_receiving"
    case featurePixHubManageKeys = "feature_pix_hub_manage_keys"
    case featurePixHubCopyPaste = "feature_pix_hub_copy_paste"
    case isPixKeysAgendaAvailable = "is_pix_keys_agenda_available"
    case featurePixkeyPhoneWithDDI = "feature_pix_key_phone_with_ddi"
    case isPixCopiedRandomKeyHomePopUpAvailable = "is_pix_copied_random_key_home_popup_available"
    case isPixCopyPasteHomePopUpAvailable = "is_pix_copy_paste_home_popup_available"
    case isPixCopiedKeyTransactionsPopUpAvailable = "is_pix_copied_key_transactions_popup_available"
    case isPixDailyLimitAvailable = "is_pix_daily_limit_available"
    case isPixDailyLimitAvailablePJ = "is_pix_daily_limit_available_pj"
    case pixDailyLimitFaqUrlPj = "pix_daily_limit_faq_url_pj"
    case pixDailyLimitFaqUrlPf = "pix_daily_limit_faq_url_pf"
    case isPixHubVersionBAvailablePF = "is_pix_hub_version_b_available_pf"
    case pixHubFaqUrlPf = "pix_hub_faq_url_pf"

    // MARK: - New Transactions
    case experimentNewTransactionScreenBool = "experiment_new_transaction_screen_bool"
    
    // MARK: - Direct Message
    case releaseChatPocPresentingBool = "release_chat_poc_presenting_bool"
    case featureDirectMessageEnabled = "feature_direct_message_enabled"
    case directMessageSBEnabled = "is_directmessage_sendbird_available"
    case directMessageSBUserQueryEnabled = "is_directmessage_userquery_available"
    case directMessageSBViaSearchEnabled = "is_directmessage_viasearch_available"
    case directMessageSBChatBlockEnabled = "is_directmessage_chatblock_available"
    case directMessageSBSessionDelegateAvailable = "is_directmessage_sessiondelegate_available"
    
    // MARK: - Identify Contacts
    case identifyContactsEnable = "identify_contacts_enable"
    
    // App Protection
    case releaseAppProtectionShowBool = "release_app_protection_show_bool"

    case urlTerms = "url_terms_of_use"
    case registrationTaxZero = "feature_tax_zero"
    
    // MARK: - Social Feed
    case featureNewFeed = "feature_new_feed"
    
    // MARK: - Review Privacy
    case isReviewPrivacyAvailable = "is_review_privacy_available"
    case isReviewPrivacyHomeAvailable = "is_review_privacy_home_available"
    
    // MARK: - Cashout
    case isWithdrawReceiptAvailable = "is_withdraw_receipt_available"
    case featureCashoutWithdrawalPixBool = "feature_cashout_withdrawal_pix_bool"
    case experimentReviewPrivacyText = "experiment_review_privacy_text"
    case cashoutHelpcenterContactReasontag = "cashout_helpcenter_contact_reasontag"
    
    // MARK: - Income Statement (informe de rendimentos)
    case incomeStatementsHelpUrl = "income_statements_help_url"
    case incomeStatementsEmptyHelpUrl = "income_statements_empty_help_url"
    case incomeStatementsErrorHelpUrl = "income_statements_error_help_url"
    
    // MARK: - PJ Flags
    case allowDeepLinkRouter = "feature_deeplink_router"
    case allowQRCodeFlow = "feature_print_qr_code"
    case allowApplyMaterial = "feature_apply_material"
    case allowNewReceive = "feature_receive"
    case releaseScannerPresenting = "release_scanner_presenting_bool"
    case featureFacialBiometry = "feature_facial_biometry"
    case bill2FA = "feature_bill_2fa"
    case releaseCashbackPresentingBool = "release_cashback_presenting_bool"
    case releaseBankAccountWarning = "release_bank_account_warning_bool"
    case releasePIXPresenting = "release_pix_presenting_bool"
    case releasePIXCashoutPresenting = "release_pix_cashout_presenting_bool"
    case releasePIXCopyPastePresenting = "release_pix_copy_paste_presenting_bool"
    case releasePIXTransactionsButtonPresenting = "release_pix_transactions_button_presenting_bool"
    case featureResidenceChangeLock = "feature_residence_change_lock"
    case featureCheckResidenceLock = "feature_check_residence_lock"
    case releaseNewHome = "release_new_home_bool"
    case releaseHubPixPresenting = "release_hub_pix_presenting_bool"
    case featureRegistrationIntroPresenting = "feature_registration_intro_presenting_bool"
    case isBusinessWhatsappAvailable = "is_business_whatsapp_available"
    case isReceivementAccountAvailable = "is_receivement_account_available"
    case isAccountShutdownAvailable = "is_account_shutdown_available"
    case urlWhatsAppExplanationWebview = "whatsapp_explanation_webview_url"
    case urlWhatsAppExplanationFaq = "whatsapp_explanation_faq_url"
    case isAppCoordinatorAvailable = "is_app_coordinator_available"
    case isExternalLinkAvailable = "is_external_link_available"
    case isBilletPaymentAvailable = "is_billet_payment_available"
    case isFastAccessAvailable = "is_fast_access_available"
    case isNavigationBarAppearanceDefaultAvailable = "is_navigation_bar_appearance_default_available"
    case isAppLocalDataUnavailable = "is_app_local_data_unavailable"
    case isAccountInfoRenewalAvailable = "is_account_info_renewal_available"
    case isFeatureInAppReviewAvailable = "is_feature_in_app_review_available"
    case inAppReviewTagsLiquidation = "in_app_review_tags_liquidation"
    case inAppReviewTagsPayment = "in_app_review_tags_payment"
    case isNewAccountManagementAvailable = "is_new_account_management_available"
    case isTransferCarouselButtonAvailable = "is_transfer_carousel_button_available"
    
    // URLs
    case urlWallet = "url_wallet_contract"
    case urlCompleteAccount = "url_complete_account"
    case urlPicPayScheme = "url_picpay_scheme"
    case urlPicPayAppStore = "url_picpay_appstore"
    
    // FAQ
    case faqArticleJudicialBlockade = "url_faq_article_judicial_blockade"
    case faqSectionAccountManagement = "account_management_faq_section_id"
    
    // MARK: - Register PJ
    case featureRegistrationPJSteps = "ops_feature_registration_pj_steps"

    // MARK: - EventTracker
    case isEventTrackerProxyEnabled = "is_event_tracker_proxy_enabled"
    
    case opsNewFlowPasswordScenePJ = "ops_new_flow_password_scene_pj"
  
    // MARK: - ReceiptKit
    case isNewReceiptAvailable = "is_new_receipt_available"
    
    // MARK: - Registration
    case isEmptyfieldAnalyzeRegister = "feature_majoca_emptyfield_analyzeregister"

    // MARK: - Seller Profile
    case isWhatsappSellerAvailable = "is_whatsapp_seller_available"

    // MARK: - Invoice Promotion
    case isInvoiceCashbackPromotionAvailable = "is_invoice_cashback_promotion_available"
}
