import Foundation

public protocol HasFeatureManager {
    var featureManager: FeatureManagerContract { get }
}

public protocol FeatureManagerContract {
    func isActive(_ featureConfig: FeatureConfig) -> Bool
    func text(_ featureConfig: FeatureConfig) -> String
    func number(_ featureConfig: FeatureConfig) -> NSNumber?
    func int(_ featureConfig: FeatureConfig) -> Int?
    func json(_ featureConfig: FeatureConfig) -> [String: Any]?
    func object<T: Decodable>(_ featureConfig: FeatureConfig, type: T.Type) -> T?
    
    static func isActive(_ featureConfig: FeatureConfig) -> Bool
    static func text(_ featureConfig: FeatureConfig) -> String
    static func number(_ featureConfig: FeatureConfig) -> NSNumber?
    static func int(_ featureConfig: FeatureConfig) -> Int?
    static func json(_ featureConfig: FeatureConfig) -> [String: Any]?
    static func object<T: Decodable>(_ featureConfig: FeatureConfig, type: T.Type) -> T?
    func update(completion: (() -> Void)?)
}

// Module Config
public protocol ThirdPartyRemoteConfigContract {
    func activate()
    func fetch(withExpirationDuration: TimeInterval, successHandler: @escaping () -> Void)
    func object<T>(type: T.Type, for key: String) -> T?
}

public enum RemoteConfigSetup {
    public static var remoteConfigInstance: ThirdPartyRemoteConfigContract?

    public static func inject(instance: ThirdPartyRemoteConfigContract) {
        remoteConfigInstance = instance
    }
}

public class FeatureManager: NSObject, FeatureManagerContract {
    public static let shared = FeatureManager()
    
    private var remoteConfig: ThirdPartyRemoteConfigContract? {
        RemoteConfigSetup.remoteConfigInstance
    }
    
    @objc
    public func update(completion: (() -> Void)? = nil) {
        FeatureManager.shared.fetch(completion: completion)
    }
    
    private func fetch(completion: (() -> Void)? = nil) {
        var expirationDuration = 43_200
        #if DEBUG || DEVELOPMENT
        expirationDuration = 0
        #endif
        
        remoteConfig?.fetch(withExpirationDuration: TimeInterval(expirationDuration), successHandler: {
            FeatureManager.shared.remoteConfig?.activate()
            
            NotificationCenter.default.post(name: Notification.Name.RemoteConfig.fetched, object: nil)
            completion?()
        })
    }
    
    public static func isActive(_ featureConfig: FeatureConfig) -> Bool {
        FeatureManager.shared.isActive(featureConfig.rawValue)
    }
    
    public static func text(_ featureConfig: FeatureConfig) -> String {
        FeatureManager.shared.text(featureConfig.rawValue)
    }
    
    public static func number(_ featureConfig: FeatureConfig) -> NSNumber? {
        FeatureManager.shared.number(featureConfig.rawValue)
    }
    
    public static func int(_ featureConfig: FeatureConfig) -> Int? {
        FeatureManager.shared.int(featureConfig.rawValue)
    }
    
    @available(*, deprecated, message: "Use object<T: Decodable> instead or if your object is not codable, use .text")
    public static func json(_ featureConfig: FeatureConfig) -> [String: Any]? {
        FeatureManager.shared.json(featureConfig.rawValue)
    }
    
    public static func object<T: Decodable>(_ featureConfig: FeatureConfig, type: T.Type) -> T? {
        FeatureManager.shared.object(featureConfig.rawValue, type: type)
    }
    
    private func isActive(_ featureName: String) -> Bool {
        remoteConfig?.object(type: Bool.self, for: featureName) ?? false
    }
    
    private func text(_ featureName: String) -> String {
        remoteConfig?.object(type: String.self, for: featureName) ?? ""
    }
    
    private func number(_ featureName: String) -> NSNumber? {
        if let number = remoteConfig?.object(type: NSNumber.self, for: featureName) {
            return number
        }
        
        if let string = remoteConfig?.object(type: String.self, for: featureName),
            let double = Double(string) {
            return NSNumber(value: double)
        }
        
        return nil
    }
    
    private func int(_ featureName: String) -> Int? {
        remoteConfig?.object(type: Int.self, for: featureName)
    }
    
    private func data(for featureName: String) -> Data? {
        remoteConfig?.object(type: String.self, for: featureName)?.data(using: .utf8)
    }
    
    @available(*, deprecated, message: "To be removed when .json function is removed")
    private func json(_ featureName: String) -> [String: Any]? {
         if let data = data(for: featureName) {
             do {
                 return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
             } catch {
                 print(error.localizedDescription)
             }
         }
         return nil
    }
    
    private func object<T>(_ featureName: String, type: T.Type) -> T? where T: Decodable {
        let jsonDecoder = JSONDecoder()
        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
        
        guard let data = data(for: featureName) else {
            return nil
        }
        if let result = try? jsonDecoder.decode(T.self, from: data) {
            return result
        }
        
        return nil
    }
}

public extension FeatureManager {
    func isActive(_ featureConfig: FeatureConfig) -> Bool {
        FeatureManager.isActive(featureConfig)
    }
    func text(_ featureConfig: FeatureConfig) -> String {
        FeatureManager.text(featureConfig)
    }
    func number(_ featureConfig: FeatureConfig) -> NSNumber? {
        FeatureManager.number(featureConfig)
    }
    func int(_ featureConfig: FeatureConfig) -> Int? {
        FeatureManager.int(featureConfig)
    }
    func json(_ featureConfig: FeatureConfig) -> [String: Any]? {
        FeatureManager.json(featureConfig)
    }
    func object<T: Decodable>(_ featureConfig: FeatureConfig, type: T.Type) -> T? {
        FeatureManager.object(featureConfig, type: type)
    }
}

@objc
public extension FeatureManager {
    static func isActiveInstallmentP2POBJC() -> Bool {
        isActive(.isActiveInstallmentP2P)
    }
    
    static func isActiveHelpCenterOBJC() -> Bool {
        isActive(.helpCenter)
    }
    
    static func isActiveTouchIdAnalyticsOBJC() -> Bool {
        isActive(.touchIdAnalytics)
    }
    
    static func isActiveFeatureInstallmentNewButtonOBJC() -> Bool {
        isActive(.featureInstallmentNewButton)
    }
    
    static func isActivePaymentRequestDenounceOBJC() -> Bool {
        isActive(.paymentRequestDenounce)
    }
    
    static func isActiveExperimentNewDialogLimitFeePaymentBoolOBJC() -> Bool {
        isActive(.experimentNewDialogLimitFeePaymentBool)
    }
    
    static func isActiveExperimentInvertedInstallmentsOBJC() -> Bool {
        isActive(.experimentInvertedInstallment)
    }
}
