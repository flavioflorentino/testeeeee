@testable import IssueReport
import UI
import XCTest

private final class PaymentRequestReportCoordinatorSpy: PaymentRequestReportCoordinating {
    var viewController: UIViewController?
    private(set) var performCallsCount = 0
    private(set) var action: PaymentRequestReportAction?
    
    func perform(action: PaymentRequestReportAction) {
        performCallsCount += 1
        self.action = action
    }
}

private final class PaymentRequestReportViewControllerSpy: PaymentRequestReportDisplay {
    private(set) var displayIssuesCallCount = 0
    private(set) var issues: [PaymentRequestReportIssue]?
    private(set) var displayLoadingCallCount = 0
    private(set) var hideLoadingCallCount = 0
    private(set) var displayStatusAlertViewWithDataCallCount = 0
    
    func displayIssues(_ issues: [PaymentRequestReportIssue]) {
        displayIssuesCallCount += 1
        self.issues = issues
    }
    
    func displayLoading() {
        displayLoadingCallCount += 1
    }
    
    func hideLoading() {
        hideLoadingCallCount += 1
    }
    
    func displayStatusAlertViewWithData(_ data: StatusAlertData) {
        displayStatusAlertViewWithDataCallCount += 1
    }
}

final class PaymentRequestReportPresenterTests: XCTestCase {
    // MARK: - Variables
    private let viewController = PaymentRequestReportViewControllerSpy()
    private let coordinator = PaymentRequestReportCoordinatorSpy()
    private lazy var presenter: PaymentRequestReportPresenter = {
        let presenter = PaymentRequestReportPresenter(coordinator: coordinator)
        presenter.viewController = viewController
        return presenter
    }()
    private let issues = [
        PaymentRequestReportIssue(id: "1", order: 1, message: "Issue 1"),
        PaymentRequestReportIssue(id: "2", order: 2, message: "Issue 2")
    ]
    
    // MARK: - presenIssues
    func testPresentIssues_ShouldCallDisplayIssues() throws {
        presenter.presentIssues(issues)
        
        let fakeIssues = try XCTUnwrap(issues)
        XCTAssertEqual(viewController.displayIssuesCallCount, 1)
        XCTAssertEqual(fakeIssues, issues)
    }
    
    // MARK: - presentLoading
    func testPresentLoading_ShouldCallDisplayLoading() {
        presenter.presentLoading()
        XCTAssertEqual(viewController.displayLoadingCallCount, 1)
    }
    
    // MARK: - hideLoading
    func testHideLoading_ShouldCallHideLoading() {
        presenter.hideLoading()
        XCTAssertEqual(viewController.hideLoadingCallCount, 1)
    }
    
    // MARK: - presentRequestErrorViewWithDescription
    func testPresentRequestErrorViewWithDescription_ShouldCallDisplayStatusAlertViewWithData() {
        presenter.presentRequestErrorViewWithDescription("Description")
        XCTAssertEqual(viewController.displayStatusAlertViewWithDataCallCount, 1)
    }
    
    // MARK: - presentStatusReportUnderAnalysis
    func testPresentStatusReportUnderAnalysis_ShouldCallDisplayStatusAlertViewWithData() {
        presenter.presentStatusReportUnderAnalysis()
        XCTAssertEqual(viewController.displayStatusAlertViewWithDataCallCount, 1)
    }
    
    // MARK: - presentStatusReportSent
    func testPresentStatusReportSent_ShouldCallDisplayStatusAlertViewWithData() {
        presenter.presentStatusReportSent()
        XCTAssertEqual(viewController.displayStatusAlertViewWithDataCallCount, 1)
    }
}
