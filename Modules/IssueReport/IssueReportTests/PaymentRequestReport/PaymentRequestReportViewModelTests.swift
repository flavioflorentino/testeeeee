import AnalyticsModule
import Core
@testable import IssueReport
import XCTest

private final class PaymentRequestReportPresenterSpy: PaymentRequestReportPresenting {
    var viewController: PaymentRequestReportDisplay?
    
    private(set) var didNextStepCallCount = 0
    private(set) var action: PaymentRequestReportAction?
    private(set) var presentIssuesCallCount = 0
    private(set) var issues: [PaymentRequestReportIssue]?
    private(set) var presentLoadingCallCount = 0
    private(set) var hideLoadingCallCount = 0
    private(set) var presentRequestErrorViewWithDescriptionCallCount = 0
    private(set) var description: String?
    private(set) var presentStatusReportUnderAnalysisCallCount = 0
    private(set) var presentStatusReportSentCallCount = 0
    
    func didNextStep(action: PaymentRequestReportAction) {
        didNextStepCallCount += 1
        self.action = action
    }
    
    func presentIssues(_ issues: [PaymentRequestReportIssue]) {
        presentIssuesCallCount += 1
        self.issues = issues
    }
    
    func presentLoading() {
        presentLoadingCallCount += 1
    }
    
    func hideLoading() {
        hideLoadingCallCount += 1
    }
    
    func presentRequestErrorViewWithDescription(_ description: String) {
        presentRequestErrorViewWithDescriptionCallCount += 1
        self.description = description
    }
    
    func presentStatusReportUnderAnalysis() {
        presentStatusReportUnderAnalysisCallCount += 1
    }
    
    func presentStatusReportSent() {
        presentStatusReportSentCallCount += 1
    }
}

private class PaymentRequestReportServiceMock: PaymentRequestReportServicing {
    var requestIssuesResult: Result<[PaymentRequestReportIssue], ApiError>?
    var sendIssueReportResult: Result<NoContent, ApiError>?
    
    func requestIssues(
        transactionId: String,
        completion: @escaping (Result<[PaymentRequestReportIssue], ApiError>) -> Void
    ) {
        guard let result = requestIssuesResult else {
            return
        }
        completion(result)
    }
    
    func sendIssueReportWithId(_ id: String, transactionId: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        guard let result = sendIssueReportResult else {
            return
        }
        completion(result)
    }
}

final class PaymentRequestReportViewModelTests: XCTestCase {
    // MARK: - Variables
    private let presenter = PaymentRequestReportPresenterSpy()
    private let service = PaymentRequestReportServiceMock()
    private let analytics = AnalyticsSpy()
    private let userId = "12345"
    private let issues = [
        PaymentRequestReportIssue(id: "1", order: 1, message: "Issue 1"),
        PaymentRequestReportIssue(id: "2", order: 2, message: "Issue 2")
    ]
    
    private lazy var viewModel = PaymentRequestReportViewModel(
        service: service,
        presenter: presenter,
        transactionId: "1",
        dependencies: DependencyContainerMock(analytics)
    )
    
    // MARK: - requestIssues
    func testRequestIssues_WhenServiceReturnIssues_ShouldCallPresentIssuesAndShowAndHideLoadings() throws {
        let issues = self.issues
        service.requestIssuesResult = .success(issues)
        viewModel.requestIssues()

        XCTAssertEqual(presenter.presentLoadingCallCount, 1)
        XCTAssertEqual(presenter.hideLoadingCallCount, 1)
        let fakeIssues = try XCTUnwrap(presenter.issues)
        XCTAssertEqual(issues, fakeIssues)
    }
    
    func testRequestIssues_WhenServiceReturnFailure_ShouldCallPresentRequestErrorViewWithDescriptionCallCount() throws {
        service.requestIssuesResult = .failure(.serverError)
        viewModel.requestIssues()
        let fakeDescription = try XCTUnwrap(presenter.description)
        
        XCTAssertEqual(presenter.presentRequestErrorViewWithDescriptionCallCount, 1)
        XCTAssertEqual(fakeDescription, Strings.ReportPaymentRequest.weCouldntShowIssueReportInfos)
    }
    
    func testRequestIssues_WhenServiceReturnsErrorCode902_ShouldCallPresentStatusReportUnderAnalysis() {
        var requestError = RequestError()
        requestError.code = "902"
        service.requestIssuesResult = .failure(.otherErrors(body: requestError))
        viewModel.requestIssues()

        XCTAssertEqual(presenter.presentStatusReportUnderAnalysisCallCount, 1)
    }
    
    // MARK: - sendIssueReport
    func testSendIssueReport_WhenServiceReturnSuccess_ShouldCallPresentStatusReportSent() {
        service.requestIssuesResult = .success(issues)
        viewModel.requestIssues()
        
        service.sendIssueReportResult = .success(NoContent())
        viewModel.sendIssueReportWithIndex(0)

        XCTAssertEqual(presenter.presentStatusReportSentCallCount, 1)
        try? assertEvent(PaymentIssueReportEvent.navigation(from: .reportIssueScreen, to: .reportSuccess, timestamp: ""))
    }
    
    func testSendIssueReport_WhenServiceReturnsErrorCode902_ShouldCallPresentStatusReportUnderAnalysis() {
        service.requestIssuesResult = .success(issues)
        viewModel.requestIssues()
        
        var requestError = RequestError()
        requestError.code = "902"
        service.sendIssueReportResult = .failure(ApiError.otherErrors(body: requestError))
        viewModel.sendIssueReportWithIndex(0)

        XCTAssertEqual(presenter.presentStatusReportUnderAnalysisCallCount, 1)
    }
    
    func testSendIssueReport_WhenServiceReturnServerError_ShouldCallPresentRequestErrorViewWithDescription() throws {
        service.requestIssuesResult = .success(issues)
        viewModel.requestIssues()
        
        service.sendIssueReportResult = .failure(.serverError)
        viewModel.sendIssueReportWithIndex(0)
        
        let fakeDescription = try XCTUnwrap(presenter.description)

        XCTAssertEqual(presenter.presentRequestErrorViewWithDescriptionCallCount, 1)
        XCTAssertEqual(fakeDescription, Strings.ReportPaymentRequest.weCouldntSentYourInfo)
        try? assertEvent(PaymentIssueReportEvent.navigation(from: .reportIssueScreen, to: .reportError, timestamp: ""))
    }
    
    // MARK: - didPressAlertButton
    func testDidPressAlertButton_WhenRequestIssuesFailedBeforeAndNowServiceReturnIssues_ShouldCallPresentIssues() {
        service.requestIssuesResult = .failure(.serverError)
        viewModel.requestIssues()
        
        service.requestIssuesResult = .success(self.issues)
        viewModel.didPressAlertButton()

        XCTAssertEqual(presenter.presentIssuesCallCount, 1)
    }
    
    func testDidPressAlertButton_WhenRequestIssuesFailedBeforeAndNowServiceFailsAgain_ShouldCallPresentRequestErrorViewWithDescriptionCallCountOneMoreTime() throws {
        service.requestIssuesResult = .failure(.serverError)
        viewModel.requestIssues()
        
        viewModel.didPressAlertButton()
        
        let fakeDescription = try XCTUnwrap(presenter.description)

        XCTAssertEqual(presenter.presentRequestErrorViewWithDescriptionCallCount, 2)
        XCTAssertEqual(fakeDescription, Strings.ReportPaymentRequest.weCouldntShowIssueReportInfos)
    }
    
    func testDidPressAlertButton_WhenSendIssueFailedBeforeAndNowServiceReturnSuccess_ShouldCallPresentStatusReportSent() {
        service.requestIssuesResult = .success(issues)
        viewModel.requestIssues()
        
        service.sendIssueReportResult = .failure(.serverError)
        viewModel.sendIssueReportWithIndex(0)
        
        service.sendIssueReportResult = .success(NoContent())
        viewModel.didPressAlertButton()

        XCTAssertEqual(presenter.presentStatusReportSentCallCount, 1)
    }
    
    func testDidPressAlertButton_WhenSendIssueFailedBeforeAndNowServiceFailsAgain_ShouldCallPresentRequestErrorViewWithDescriptionCallCountOneMoreTime() throws {
        service.requestIssuesResult = .success(issues)
        viewModel.requestIssues()
        
        service.sendIssueReportResult = .failure(.serverError)
        viewModel.sendIssueReportWithIndex(0)
        
        service.sendIssueReportResult = .failure(.serverError)
        viewModel.didPressAlertButton()
        
        let fakeDescription = try XCTUnwrap(presenter.description)

        XCTAssertEqual(presenter.presentRequestErrorViewWithDescriptionCallCount, 2)
        XCTAssertEqual(fakeDescription, Strings.ReportPaymentRequest.weCouldntSentYourInfo)
    }
    
    private func assertEvent(_ event: AnalyticsKeyProtocol) throws {
        guard let loggedEvent = analytics.event else {
            return XCTFail("event logged is nil and it is not equal to \(event.event())")
        }
        
        let eventProperties = try XCTUnwrap(event.event().properties as? [String: String])
        let loggedProperties = try XCTUnwrap(loggedEvent.properties as? [String: String])
        
        XCTAssertEqual(eventProperties["from"], loggedProperties["from"])
        XCTAssertEqual(eventProperties["to"], loggedProperties["to"])
        XCTAssertNotNil(loggedProperties["timestamp"])
    }
}
