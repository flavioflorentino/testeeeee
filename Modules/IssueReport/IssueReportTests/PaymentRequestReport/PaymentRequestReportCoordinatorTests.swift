@testable import IssueReport
import XCTest

final class PaymentRequestReportCoordinatorTests: XCTestCase {
    // MARK: - Variables
    private lazy var coordinator: PaymentRequestReportCoordinator = {
        let coordinator = PaymentRequestReportCoordinator()
        coordinator.viewController = viewController
        return coordinator
    }()
    private var viewController = SpyViewController()
    
    // MARK: - perform
    func testPerform_WhenActionIsFinish_ShouldDismissViewController() throws {
        coordinator.perform(action: .finishFlow)
        XCTAssertEqual(viewController.isDismissCalled, true)
    }
}
