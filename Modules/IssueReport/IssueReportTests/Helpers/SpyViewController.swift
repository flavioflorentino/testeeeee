import UIKit

final class SpyViewController: UIViewController {
    var isDismissCalled: Bool = false
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        isDismissCalled = true
        super.dismiss(animated: flag, completion: completion)
    }
}
