import Core

enum IssueReportServiceEndpoint {
    case issueReports(transactionId: String)
    case sendIssueReport(transactionId: String, complaintId: String)
}

// MARK: - ApiEndpointExposable
extension IssueReportServiceEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case let .issueReports(transactionId):
            return "p2p-charge/transactions/\(transactionId)/complaints"
        case let .sendIssueReport(transactionId, complaintId):
            return "p2p-charge/transactions/\(transactionId)/complaints/\(complaintId)"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .issueReports:
            return .get
        case .sendIssueReport:
            return .post
        }
    }
}
