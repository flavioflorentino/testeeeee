import Foundation

// swiftlint:disable convenience_type
final class IssueReportResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: IssueReportResources.self).url(forResource: "IssueReportResources", withExtension: "bundle") else {
            return Bundle(for: IssueReportResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: IssueReportResources.self)
    }()
}
