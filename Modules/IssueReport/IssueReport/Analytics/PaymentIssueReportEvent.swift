import AnalyticsModule

enum PaymentIssueReportEvent: AnalyticsKeyProtocol {
    enum Navigation: String {
        case reportIssueScreen = "denounce screen"
        case reportSuccess = "success"
        case reportError = "error"
    }
    
    case navigation(
        from: Navigation,
        to: Navigation,
        timestamp: String
    )
    
    private var properties: [String: Any] {
        switch self {
        case let .navigation(from, to, timestamp):
            return [
                "from": from.rawValue,
                "to": to.rawValue,
                "timestamp": timestamp
            ]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("User Navigated", properties: properties, providers: [.mixPanel, .appsFlyer, .firebase])
    }
}
