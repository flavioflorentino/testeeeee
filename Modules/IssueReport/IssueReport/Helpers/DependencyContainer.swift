import AnalyticsModule

typealias IssueReportDependencies = HasAnalytics & HasMainQueue

final class DependencyContainer: IssueReportDependencies {
    lazy var mainQueue = DispatchQueue.main
    lazy var analytics: AnalyticsProtocol = Analytics.shared
}
