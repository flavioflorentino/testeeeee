protocol HasMainQueue {
    var mainQueue: DispatchQueue { get }
}
