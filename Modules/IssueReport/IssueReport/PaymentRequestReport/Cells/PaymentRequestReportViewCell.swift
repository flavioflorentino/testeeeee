import UI
import UIKit

private extension PaymentRequestReportViewCell.Layout {
    static let descriptionTrailing: CGFloat = 12.0
}

public final class PaymentRequestReportViewCell: UITableViewCell {
    fileprivate enum Layout { }
    
    private lazy var circularCheckView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.icoButtonGlyphUnchecked.image.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = Colors.grayscale500.color
        return imageView
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale750())
        label.backgroundColor = Colors.backgroundPrimary.color
        return label
    }()
    
    func configureCellWithText(_ text: String) {
        descriptionLabel.text = text
        buildLayout()
    }
}

extension PaymentRequestReportViewCell: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(circularCheckView)
        addSubview(descriptionLabel)
    }
    
    public func configureViews() {
        selectionStyle = .none
    }
    
    public func setupConstraints() {
        circularCheckView.snp.makeConstraints {
            $0.centerY.leading.equalToSuperview()
        }
        descriptionLabel.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalTo(circularCheckView.snp.trailing).offset(Layout.descriptionTrailing)
        }
    }
    
    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        circularCheckView.image = selected
            ? Assets.icoButtonGlyphChecked.image
            : Assets.icoButtonGlyphUnchecked.image.withRenderingMode(.alwaysTemplate)
        circularCheckView.tintColor = selected
            ? Colors.branding400.color
            : Colors.grayscale500.color
    }
}
