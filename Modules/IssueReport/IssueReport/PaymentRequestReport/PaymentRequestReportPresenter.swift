import Foundation
import UI

protocol PaymentRequestReportPresenting: AnyObject {
    var viewController: PaymentRequestReportDisplay? { get set }
    func didNextStep(action: PaymentRequestReportAction)
    func presentIssues(_ issues: [PaymentRequestReportIssue])
    func presentLoading()
    func hideLoading()
    func presentRequestErrorViewWithDescription(_ description: String)
    func presentStatusReportUnderAnalysis()
    func presentStatusReportSent()
}

final class PaymentRequestReportPresenter {
    private let coordinator: PaymentRequestReportCoordinating
    weak var viewController: PaymentRequestReportDisplay?

    init(coordinator: PaymentRequestReportCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - PaymentRequestReportPresenting
extension PaymentRequestReportPresenter: PaymentRequestReportPresenting {
    func didNextStep(action: PaymentRequestReportAction) {
        coordinator.perform(action: action)
    }
    
    func presentIssues(_ issues: [PaymentRequestReportIssue]) {
        viewController?.displayIssues(issues)
    }
    
    func presentLoading() {
        viewController?.displayLoading()
    }
    
    func hideLoading() {
        viewController?.hideLoading()
    }
    
    func presentRequestErrorViewWithDescription(_ description: String) {
        let data = StatusAlertData(
            icon: Assets.reportIssueError.image,
            title: Strings.ReportPaymentRequest.somethingWentWrong,
            text: description,
            buttonTitle: Strings.ReportPaymentRequest.tryAgain
        )
        viewController?.displayStatusAlertViewWithData(data)
    }
    
    func presentStatusReportUnderAnalysis() {
        let data = StatusAlertData(
            icon: Assets.like.image,
            title: Strings.ReportPaymentRequest.ReportUnderAnalysis.title,
            text: Strings.ReportPaymentRequest.ReportUnderAnalysis.description,
            buttonTitle: Strings.ReportPaymentRequest.okIUnderstood)
        viewController?.displayStatusAlertViewWithData(data)
    }
    
    func presentStatusReportSent() {
        let data = StatusAlertData(
            icon: Assets.like.image,
            title: Strings.ReportPaymentRequest.ReportSent.title,
            text: Strings.ReportPaymentRequest.ReportSent.description,
            buttonTitle: Strings.ReportPaymentRequest.okIUnderstood)
        viewController?.displayStatusAlertViewWithData(data)
    }
}
