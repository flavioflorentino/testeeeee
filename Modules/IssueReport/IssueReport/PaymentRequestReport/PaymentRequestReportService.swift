import Core

protocol PaymentRequestReportServicing {
    func requestIssues(
        transactionId: String,
        completion: @escaping (Result<[PaymentRequestReportIssue], ApiError>) -> Void
    )
    
    func sendIssueReportWithId(
        _ id: String,
        transactionId: String,
        completion: @escaping (Result<NoContent, ApiError>) -> Void
    )
}

final class PaymentRequestReportService {
    typealias Dependencies = HasMainQueue
    // MARK: - Variables
    let dependencies: HasMainQueue
    
    // MARK: - Life Cycle
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - PaymentRequestReportServicing
extension PaymentRequestReportService: PaymentRequestReportServicing {
    func requestIssues(
        transactionId: String,
        completion: @escaping (Result<[PaymentRequestReportIssue], ApiError>) -> Void
    ) {
        let endpoint = IssueReportServiceEndpoint.issueReports(transactionId: transactionId)
        Api<[PaymentRequestReportIssue]>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func sendIssueReportWithId(
        _ id: String,
        transactionId: String,
        completion: @escaping (Result<NoContent, ApiError>) -> Void
    ) {
        let endpoint = IssueReportServiceEndpoint.sendIssueReport(
            transactionId: transactionId,
            complaintId: id
        )
        Api<NoContent>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
