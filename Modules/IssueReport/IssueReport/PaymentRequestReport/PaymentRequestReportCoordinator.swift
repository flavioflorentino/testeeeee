import UIKit

enum PaymentRequestReportAction {
    case finishFlow
}

protocol PaymentRequestReportCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: PaymentRequestReportAction)
}

final class PaymentRequestReportCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - PaymentRequestReportCoordinating
extension PaymentRequestReportCoordinator: PaymentRequestReportCoordinating {
    func perform(action: PaymentRequestReportAction) {
        switch action {
        case .finishFlow:
            viewController?.dismiss(animated: true)
        }
    }
}
