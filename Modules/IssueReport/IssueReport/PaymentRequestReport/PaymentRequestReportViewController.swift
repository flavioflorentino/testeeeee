import UI

protocol PaymentRequestReportDisplay: AnyObject {
    func displayIssues(_ issues: [PaymentRequestReportIssue])
    func displayLoading()
    func hideLoading()
    func displayStatusAlertViewWithData(_ data: StatusAlertData)
}

private extension PaymentRequestReportViewController.Layout {
    static let rowHeight: CGFloat = 48.0
}

public final class PaymentRequestReportViewController: ViewController<PaymentRequestReportViewModelInputs, UIView> {
    fileprivate enum Layout { }
    
    enum Section {
        case main
    }
    
    private lazy var tableViewDataSource: TableViewDataSource<Section, PaymentRequestReportIssue>? = {
        let dataSource = TableViewDataSource<Section, PaymentRequestReportIssue>(view: optionsTableView)
        dataSource.itemProvider = { tableView, indexPath, item -> PaymentRequestReportViewCell? in
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PaymentRequestReportViewCell.self), for: indexPath) as? PaymentRequestReportViewCell
            cell?.configureCellWithText(item.message)
            return cell
        }
        return dataSource
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.ReportPaymentRequest.title
        label
            .labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, .grayscale800())
        label.backgroundColor = Colors.backgroundPrimary.color
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.ReportPaymentRequest.description
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
        label.backgroundColor = Colors.backgroundPrimary.color
        return label
    }()
    
    private lazy var optionsTableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = Colors.backgroundPrimary.color
        tableView.separatorStyle = .none
        tableView.register(
            PaymentRequestReportViewCell.self,
            forCellReuseIdentifier: String(describing: PaymentRequestReportViewCell.self)
        )
        tableView.rowHeight = Layout.rowHeight
        tableView.bounces = false
        return tableView
    }()
    
    private var helpAttrText: NSAttributedString? {
        let attrText = NSMutableAttributedString(
            string: Strings.ReportPaymentRequest.inCaseOfDoubt,
            attributes: [.foregroundColor: Colors.grayscale700.color]
        )
        let underlineText = NSMutableAttributedString(
            string: Strings.ReportPaymentRequest.acessTheHelpSection,
            attributes: [.foregroundColor: Colors.branding600.color]
        )
        underlineText.addAttribute(
            .underlineStyle,
            value: NSUnderlineStyle.single.rawValue,
            range: NSRange(location: 0, length: underlineText.length)
        )
        let dot = NSMutableAttributedString(
            string: Strings.ReportPaymentRequest.dot,
            attributes: [.foregroundColor: Colors.grayscale700.color]
        )
        attrText.append(underlineText)
        attrText.append(dot)
        return attrText
    }
    
    private lazy var helpLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textAlignment, .center)
        label.attributedText = helpAttrText
        label.backgroundColor = Colors.backgroundPrimary.color
        return label
    }()
    
    private lazy var sendButton: UIButton = {
        let button = UIButton()
        button.isEnabled = false
        button.setTitle(Strings.ReportPaymentRequest.send, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTapSendButton), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Life Cycle
    override public func viewDidLoad() {
        super.viewDidLoad()
        viewModel.requestIssues()
    }
    
    override public func buildViewHierarchy() {
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
        view.addSubview(optionsTableView)
        view.addSubview(helpLabel)
        view.addSubview(sendButton)
    }
    
    override public func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base06)
            $0.leading.equalToSuperview().offset(Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        optionsTableView.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base02)
        }
        
        helpLabel.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        sendButton.snp.makeConstraints {
            $0.top.equalTo(helpLabel.snp.bottom).offset(Spacing.base04)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base02)
        }
    }
    
    override public func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        optionsTableView.dataSource = tableViewDataSource
        optionsTableView.delegate = self
    }
}

// MARK: PaymentRequestReportDisplay
extension PaymentRequestReportViewController: PaymentRequestReportDisplay {
    func displayIssues(_ issues: [PaymentRequestReportIssue]) {
        tableViewDataSource?.add(items: issues, to: .main)
    }
    
    func displayLoading() {
        beginState()
    }
    
    func hideLoading() {
        endState()
    }
    
    func displayStatusAlertViewWithData(_ data: StatusAlertData) {
        let statusAlertView = StatusAlertView.show(on: view, data: data)
        statusAlertView.statusAlertDelegate = self
    }
}

extension PaymentRequestReportViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        sendButton.isEnabled = true
    }
}

private extension PaymentRequestReportViewController {
    @objc
    func didTapSendButton() {
        guard let selectedRow = optionsTableView.indexPathForSelectedRow?.row else {
            return
        }
        viewModel.sendIssueReportWithIndex(selectedRow)
    }
}

extension PaymentRequestReportViewController: StatefulTransitionViewing { }

extension PaymentRequestReportViewController: StatusAlertViewDelegate {
    public func didTouchOnButton() {
        viewModel.didPressAlertButton()
    }
}
