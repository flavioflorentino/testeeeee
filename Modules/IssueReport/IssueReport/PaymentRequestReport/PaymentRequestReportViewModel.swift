import AnalyticsModule
import Core
import Foundation

public protocol PaymentRequestReportViewModelInputs: AnyObject {
    func requestIssues()
    func sendIssueReportWithIndex(_ index: Int)
    func didPressAlertButton()
}

final class PaymentRequestReportViewModel {
    private let reportUnderAnalysisErrorCode = "902"

    typealias Dependencies = HasAnalytics
    
    private let service: PaymentRequestReportServicing
    private let presenter: PaymentRequestReportPresenting
    private let transactionId: String
    private let dependencies: Dependencies
    private var alertClosure: (() -> Void)?
    private var issues = [PaymentRequestReportIssue]()

    init(
        service: PaymentRequestReportServicing,
        presenter: PaymentRequestReportPresenting,
        transactionId: String,
        dependencies: Dependencies = DependencyContainer()
    ) {
        self.service = service
        self.presenter = presenter
        self.transactionId = transactionId
        self.dependencies = dependencies
    }
}

// MARK: - PaymentRequestReportViewModelInputs
extension PaymentRequestReportViewModel: PaymentRequestReportViewModelInputs {
    func requestIssues() {
        presenter.presentLoading()
        service.requestIssues(transactionId: transactionId) { [weak self] result in
            guard let self = self else {
                return
            }
            self.presenter.hideLoading()
            switch result {
            case let .success(complaints):
                self.issues = complaints.sorted { $0.order < $1.order }
                self.presenter.presentIssues(self.issues)
            case let .failure(error):
                switch error {
                case let .otherErrors(body: error) where self.isReportUnderAnalysisWithErrorCode(error.code):
                    self.handleSendIssueReportUnderAnalysis()
                default:
                    self.alertClosure = self.requestIssues
                    self.presenter.presentRequestErrorViewWithDescription(Strings.ReportPaymentRequest.weCouldntShowIssueReportInfos)
                }
            }
        }
    }
    
    func sendIssueReportWithIndex(_ index: Int) {
        presenter.presentLoading()
        guard index < issues.count else {
            return
        }
        let issueId = issues[index].id
        service.sendIssueReportWithId(issueId, transactionId: transactionId) { [weak self] result in
            guard let self = self else {
                return
            }
            self.presenter.hideLoading()
            switch result {
            case .success:
                self.handleSendIssueReportSuccess()
            case let .failure(error):
                switch error {
                case let .otherErrors(body: error) where self.isReportUnderAnalysisWithErrorCode(error.code):
                    self.handleSendIssueReportUnderAnalysis()
                default:
                    self.handleSendIssueReportError(index: index)
                }
            }
        }
    }
    
    func didPressAlertButton() {
        alertClosure?()
    }
}

private extension PaymentRequestReportViewModel {
    func handleSendIssueReportSuccess() {
        alertClosure = { [weak self] in
            self?.presenter.didNextStep(action: .finishFlow)
        }
        presenter.presentStatusReportSent()
        callNavigationAnalytics(from: .reportIssueScreen, to: .reportSuccess)
    }
    
    func handleSendIssueReportUnderAnalysis() {
        alertClosure = { [weak self] in
            self?.presenter.didNextStep(action: .finishFlow)
        }
        presenter.presentStatusReportUnderAnalysis()
        callNavigationAnalytics(from: .reportIssueScreen, to: .reportError)
    }
    
    func handleSendIssueReportError(index: Int) {
        alertClosure = { [weak self] in
            self?.sendIssueReportWithIndex(index)
        }
        presenter.presentRequestErrorViewWithDescription(Strings.ReportPaymentRequest.weCouldntSentYourInfo)
        callNavigationAnalytics(from: .reportIssueScreen, to: .reportError)
    }
    
    func isReportUnderAnalysisWithErrorCode(_ code: String) -> Bool {
        code == reportUnderAnalysisErrorCode
    }
    
    func callNavigationAnalytics(
        from: PaymentIssueReportEvent.Navigation,
        to: PaymentIssueReportEvent.Navigation
    ) {
        dependencies.analytics.log(PaymentIssueReportEvent.navigation(
            from: from,
            to: to,
            timestamp: DateFormatter.timestampFormatter.string(from: Date())
        ))
    }
}
