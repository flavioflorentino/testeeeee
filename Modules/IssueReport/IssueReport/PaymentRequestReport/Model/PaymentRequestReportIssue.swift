import Foundation

struct PaymentRequestReportIssue: Decodable, Equatable {
    let id: String
    let order: Int
    let message: String
}
