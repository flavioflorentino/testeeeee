import UIKit

@objc
public final class PaymentRequestReportFactory: NSObject {
    @objc
    public static func make(transactionId: String) -> UIViewController {
        let container = DependencyContainer()
        let service: PaymentRequestReportServicing = PaymentRequestReportService(dependencies: container)
        let coordinator: PaymentRequestReportCoordinating = PaymentRequestReportCoordinator()
        let presenter: PaymentRequestReportPresenting = PaymentRequestReportPresenter(coordinator: coordinator)
        let viewModel = PaymentRequestReportViewModel(
            service: service,
            presenter: presenter,
            transactionId: transactionId
        )
        let viewController = PaymentRequestReportViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
