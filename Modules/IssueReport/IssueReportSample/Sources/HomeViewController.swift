import IssueReport
import UIKit

class HomeViewController: UIViewController {
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        present(PaymentRequestReportFactory.make(transactionId: "5f15f97b49765b3fa7608739"), animated: true)
    }
}
