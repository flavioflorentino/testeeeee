import Foundation

// swiftlint:disable convenience_type
final class ValidationsResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: ValidationsResources.self).url(forResource: "ValidationsResources", withExtension: "bundle") else {
            return Bundle(for: ValidationsResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: ValidationsResources.self)
    }()
}
