import Foundation

public enum GenericValidationError: LocalizedError {
    case incompleteData
    case invalidData
    
    public var errorDescription: String? {
        switch self {
        case .incompleteData:
            return Strings.GenericError.incompleteData
        case .invalidData:
            return Strings.GenericError.invalidData
        }
    }
}
