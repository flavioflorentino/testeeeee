import Foundation

public protocol RegexDescriptor {
    var predicate: NSPredicate { get }
}

public struct AccountRegexDescriptor: RegexDescriptor {
    let regexString: String
    public var predicate: NSPredicate {
        NSPredicate(format: "SELF MATCHES %@", regexString)
    }
    
    public init(regexString: String) {
        self.regexString = regexString
    }
    
    public static let email = AccountRegexDescriptor(regexString: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}")
    public static let randomKey = AccountRegexDescriptor(regexString: "[A-Fa-f0-9]{8}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{12}")
}

public struct RegexValidator: StringValidationRule {
    // MARK: - Life Cycle
    let descriptor: RegexDescriptor
    public init(descriptor: RegexDescriptor) {
        self.descriptor = descriptor
    }
    
    // MARK: - Validations
    public func validate(_ value: String) throws {
        guard descriptor.predicate.evaluate(with: value) else {
            throw GenericValidationError.invalidData
        }
    }
}
