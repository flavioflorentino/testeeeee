import Foundation

public struct CellphoneWithDDDAndDDIValidator: StringValidationRule {
    // MARK: - Life Cycle
    public init() {}

    // MARK: - Validations
    public func validate(_ value: String) throws {
        guard let regex = try? NSRegularExpression(pattern: "^[+]{1}[0-9]{1,71}$") else {
            throw GenericValidationError.invalidData
        }
        let range = NSRange(location: 0, length: value.utf16.count)
        if regex.firstMatch(in: value, options: [], range: range) == nil {
            throw GenericValidationError.invalidData
        }
        if value.count < 6 {
            throw GenericValidationError.incompleteData
        }
    }
}
