import Foundation

public struct CNPJValidator: StringValidationRule {
    // MARK: - Life Cycle
    public init() {}
    
    // MARK: - Validations
    public func validate(_ value: String) throws {
        let numbers = value.compactMap(\.wholeNumberValue)
        try rangeValidations(numbers)
        try repeatedNumbersValidation(numbers)
        try cnpjValidation(numbers)
    }
    
    private func rangeValidations(_ value: [Int]) throws {
        if value.count < 14 {
            throw GenericValidationError.incompleteData
        } else if value.count > 14 {
            throw GenericValidationError.invalidData
        }
    }
    
    private func repeatedNumbersValidation(_ value: [Int]) throws {
        guard Set(value).count > 1 else {
            throw GenericValidationError.invalidData
        }
    }
    
    private func cnpjValidation(_ cnpj: [Int]) throws {
        let originalFirstDigit = cnpj[12]
        let originalSecondDigit = cnpj[13]
        var calcFirstDigit = 0, calcSecondDigit = 0
        let weights = [6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2]
        
        for index in 0...11 {
            let indexValue = cnpj[index]
            calcFirstDigit += indexValue * weights[index + 1]
            calcSecondDigit += indexValue * weights[index]
        }
        calcSecondDigit += cnpj[12] * 2
        
        calcFirstDigit = calcFirstDigit * 10 % 11
        calcFirstDigit = calcFirstDigit == 10 ? 0 : calcFirstDigit
        
        calcSecondDigit = calcSecondDigit * 10 % 11
        calcSecondDigit = calcSecondDigit == 10 ? 0 : calcSecondDigit
        
        guard calcFirstDigit == originalFirstDigit && calcSecondDigit == originalSecondDigit else {
            throw GenericValidationError.invalidData
        }
    }
}
