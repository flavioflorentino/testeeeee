import Foundation

public struct CellphoneWithDDDValidator: StringValidationRule {
    // MARK: - Life Cycle
    public init() {}
    
    // MARK: - Validations
    public func validate(_ value: String) throws {
        let numbers = value.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        try rangeValidations(numbers)
    }
    
    private func rangeValidations(_ phone: String) throws {
        if phone.count < 10 {
            throw GenericValidationError.incompleteData
        } else if phone.count > 11 {
            throw GenericValidationError.invalidData
        }
    }
}
