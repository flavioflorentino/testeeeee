import Foundation

public struct CPFValidator: StringValidationRule {
    // MARK: - Life Cycle
    public init() {}
    
    // MARK: - Validations
    public func validate(_ value: String) throws {
        let numbers = value.compactMap(\.wholeNumberValue)
        try rangeValidations(numbers)
        try repeatedNumbersValidation(numbers)
        try cpfValidation(numbers)
    }
    
    private func rangeValidations(_ value: [Int]) throws {
        if value.count < 11 {
            throw GenericValidationError.incompleteData
        } else if value.count > 11 {
            throw GenericValidationError.invalidData
        }
    }
    
    private func repeatedNumbersValidation(_ value: [Int]) throws {
        guard Set(value).count > 1 else {
            throw GenericValidationError.invalidData
        }
    }
    
    private func cpfValidation(_ cpf: [Int]) throws {
        let originalFirstDigit = cpf[9]
        let originalSecondDigit = cpf[10]
        var calcFirstDigit = 0, calcSecondDigit = 0
        
        for index in 0...8 {
            let indexValue = cpf[index]
            calcFirstDigit += indexValue * (10 - index)
            calcSecondDigit += indexValue * (11 - index)
        }
        calcSecondDigit += cpf[9] * 2
        
        calcFirstDigit = calcFirstDigit * 10 % 11
        calcFirstDigit = calcFirstDigit == 10 ? 0 : calcFirstDigit
        
        calcSecondDigit = calcSecondDigit * 10 % 11
        calcSecondDigit = calcSecondDigit == 10 ? 0 : calcSecondDigit
        
        guard calcFirstDigit == originalFirstDigit && calcSecondDigit == originalSecondDigit else {
            throw GenericValidationError.invalidData
        }
    }
}
