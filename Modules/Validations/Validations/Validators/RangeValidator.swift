import Foundation

public struct RangeValidator: StringValidationRule {
    // MARK: - Life Cycle
    private let minCount: Int?
    private let maxCount: Int?
    
    public init(minCount: Int?, maxCount: Int?) {
        self.minCount = minCount
        self.maxCount = maxCount
    }
    
    public init(count: Int) {
        self.minCount = count
        self.maxCount = count
    }
    
    public init(range: ClosedRange<Int>) {
        self.minCount = range.lowerBound
        self.maxCount = range.upperBound
    }
    
    // MARK: - Validations
    public func validate(_ value: String) throws {
        if let minCount = minCount, value.count < minCount {
            throw GenericValidationError.incompleteData
        } else if let maxCount = maxCount, value.count > maxCount {
            throw GenericValidationError.invalidData
        }
    }
    
    public func validateForInterval(_ value: String) throws {
        if let minCount = minCount, value.count < minCount {
            throw GenericValidationError.incompleteData
        }
        
        if let maxCount = maxCount,
           value.count < maxCount || value.count > maxCount {
            throw GenericValidationError.invalidData
        }
    }
}
