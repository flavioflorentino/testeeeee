import Foundation

public protocol StringValidationRule {
    func validate(_ value: String) throws
}

public protocol NumericValidationRule {
    func validate<T: Numeric & Comparable>(_ value: T) throws
}
