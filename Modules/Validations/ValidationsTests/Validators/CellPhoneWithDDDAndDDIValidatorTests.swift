@testable import Validations
import XCTest

final class CellphoneWithDDDAndDDIValidatorTests: XCTestCase {
    let validator = CellphoneWithDDDAndDDIValidator()

    // MARK: - Validate
    func testValidate_WhenThereIsAPlusSignAndFiveNumbers_ShouldNotThrowAnError() throws {
        XCTAssertNoThrow(try validator.validate("+12345"))
    }
    
    func testValidate_WhenThereIsAPlusSignAndANumber_ShouldThrowAnIncompleteDataError() throws {
        XCTAssertThrowsError(try validator.validate("+1")) { error in
            guard
                let validationError = error as? GenericValidationError,
                case GenericValidationError.incompleteData = validationError
                else {
                    XCTFail("Incorrect Error Type")
                    return
            }
        }
    }

    func testValidate_WhenThereIsOnlyAPlusSign_ShouldThrowAnInvalidDataError() throws {
        XCTAssertThrowsError(try validator.validate("+")) { error in
            guard
                let validationError = error as? GenericValidationError,
                case GenericValidationError.invalidData = validationError
                else {
                    XCTFail("Incorrect Error Type")
                    return
            }
        }
    }

    func testValidate_WhenThereIsNoPlusSign_ShouldThrowAnInvalidDataError() throws {
        XCTAssertThrowsError(try validator.validate("123456789")) { error in
            guard
                let validationError = error as? GenericValidationError,
                case GenericValidationError.invalidData = validationError
                else {
                    XCTFail("Incorrect Error Type")
                    return
            }
        }
    }

    func testValidate_WhenCellphoneCountIsGreaterThan72_ShouldThrowAnInvalidDataError() throws {
        XCTAssertThrowsError(try validator.validate("+\(String(repeating: "1", count: 72))")) { error in
            guard
                let validationError = error as? GenericValidationError,
                case GenericValidationError.invalidData = validationError
                else {
                    XCTFail("Incorrect Error Type")
                    return
            }
        }
    }
}
