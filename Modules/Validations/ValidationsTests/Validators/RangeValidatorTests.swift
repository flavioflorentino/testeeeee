@testable import Validations
import XCTest

final class RangeValidatorTests: XCTestCase {
    // MARK: - validate
    func testValidate_WhenHasNoMaxCountAndValueCountIsLessThanMinCount_ShouldThrowIncompleteData() {
        let validator = RangeValidator(minCount: 2, maxCount: nil)
        XCTAssertThrowsError(try validator.validate("1")) { error in
            guard
                let validationError = error as? GenericValidationError,
                case GenericValidationError.incompleteData = validationError
                else {
                    XCTFail("Incorrect Error Type")
                    return
            }
        }
    }
    
    func testValidate_WhenHasNoMaxCountAndValueCountIsGreaterThanOrEqualMinCount_ShouldNotThrowError() {
        let validator = RangeValidator(minCount: 2, maxCount: nil)
        XCTAssertNoThrow(try validator.validate("123"))
    }
    
    func testValidate_WhenHasNoMinCountAndValueCountIsLessThanOrEqualMaxCount_ShouldNotThrowError() {
        let validator = RangeValidator(minCount: nil, maxCount: 3)
        XCTAssertNoThrow(try validator.validate("1"))
    }
    
    func testValidate_WhenHasNoMinCountAndValueCountIsGreaterThanMaxCount_ShouldThrowInvalidData() {
        let validator = RangeValidator(minCount: nil, maxCount: 3)
        XCTAssertThrowsError(try validator.validate("1234")) { error in
            guard
                let validationError = error as? GenericValidationError,
                case GenericValidationError.invalidData = validationError
                else {
                    XCTFail("Incorrect Error Type")
                    return
            }
        }
    }
    
    func testValidate_WhenValueCountIsLessThanCount_ShouldThrowIncompleteData() {
        let validator = RangeValidator(count: 2)
        XCTAssertThrowsError(try validator.validate("1")) { error in
            guard
                let validationError = error as? GenericValidationError,
                case GenericValidationError.incompleteData = validationError
                else {
                    XCTFail("Incorrect Error Type")
                    return
            }
        }
    }
    
    func testValidate_WhenValueCountIsEqualToCount_ShouldNotThrowError() {
        let validator = RangeValidator(count: 2)
        XCTAssertNoThrow(try validator.validate("12"))
    }
    
    func testValidate_WhenValueCountIsGreaterThanCount_ShouldThrowInvalidData() {
        let validator = RangeValidator(count: 2)
        XCTAssertThrowsError(try validator.validate("123")) { error in
            guard
                let validationError = error as? GenericValidationError,
                case GenericValidationError.invalidData = validationError
                else {
                    XCTFail("Incorrect Error Type")
                    return
            }
        }
    }
    
    
    func testValidate_WhenValueCountIsLessThanRangeLowerBound_ShouldThrowIncompleteData() {
        let validator = RangeValidator(range: 2...3)
        XCTAssertThrowsError(try validator.validate("1")) { error in
            guard
                let validationError = error as? GenericValidationError,
                case GenericValidationError.incompleteData = validationError
                else {
                    XCTFail("Incorrect Error Type")
                    return
            }
        }
    }
    
    func testValidate_WhenValueCountIsInRangeValues_ShouldNotThrowError() {
        let validator = RangeValidator(range: 2...3)
        XCTAssertNoThrow(try validator.validate("12"))
    }
    
    func testValidate_WhenValueCountIsGreaterThanRangeUpperBound_ShouldThrowInvalidData() {
        let validator = RangeValidator(range: 2...3)
        XCTAssertThrowsError(try validator.validate("1234")) { error in
            guard
                let validationError = error as? GenericValidationError,
                case GenericValidationError.invalidData = validationError
                else {
                    XCTFail("Incorrect Error Type")
                    return
            }
        }
    }
    
    func testValidateForInterval_WhenValueCountIsLessThanRangeLowerBound_ShouldThrowIncompleteData() {
        let validator = RangeValidator(range: 2...3)
        XCTAssertThrowsError(try validator.validateForInterval("1")) { error in
            guard
                let validationError = error as? GenericValidationError,
                case GenericValidationError.incompleteData = validationError
                else {
                    XCTFail("Incorrect Error Type")
                    return
            }
        }
    }
    
    func testValidateForInterval_WhenValueCountIsGreaterThanRangeUpperBound_ShouldThrowInvalidData() {
        let validator = RangeValidator(range: 2...3)
        XCTAssertThrowsError(try validator.validateForInterval("1234")) { error in
            guard
                let validationError = error as? GenericValidationError,
                case GenericValidationError.invalidData = validationError
                else {
                    XCTFail("Incorrect Error Type")
                    return
            }
        }
    }
    
    func testValidateForInterval_WhenValueCountIsInRangeValues_ShouldNotThrowError() {
        let validator = RangeValidator(range: 2...3)
        XCTAssertNoThrow(try validator.validateForInterval("123"))
    }
}
