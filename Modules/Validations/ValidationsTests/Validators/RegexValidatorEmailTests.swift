@testable import Validations
import XCTest

final class RegexValidatorEmailTests: XCTestCase {
    let validator = RegexValidator(descriptor: AccountRegexDescriptor.email)
    
    // MARK: - Validate
    func testValidate_WhenEmailIsValid_ShouldNotThrowAnError() throws {
        XCTAssertNoThrow(try validator.validate("TEst@test.com"))
    }
    
    func testValidate_WhenEmailIsValidWithPlus_ShouldNotThrowAnError() throws {
        XCTAssertNoThrow(try validator.validate("test+1@test.com"))
    }
    
    func testValidate_WhenEmailIsNotValidWithoutDomain_ShouldThrowAnInvalidDataError() throws {
        XCTAssertThrowsError(try validator.validate("test@test")) { error in
            guard
                let validationError = error as? GenericValidationError,
                case GenericValidationError.invalidData = validationError
                else {
                    XCTFail("Incorrect Error Type")
                    return
            }
        }
    }
    
    func testValidate_WhenEmailIsNotValidWithoutAt_ShouldThrowAnInvalidDataError() throws {
        XCTAssertThrowsError(try validator.validate("testtest.com")) { error in
            guard
                let validationError = error as? GenericValidationError,
                case GenericValidationError.invalidData = validationError
                else {
                    XCTFail("Incorrect Error Type")
                    return
            }
        }
    }
    
    func testValidate_WhenEmailIsNotValidWithInvalidSymbol_ShouldThrowAnInvalidDataError() throws {
        XCTAssertThrowsError(try validator.validate("te$%#st@test.com")) { error in
            guard
                let validationError = error as? GenericValidationError,
                case GenericValidationError.invalidData = validationError
                else {
                    XCTFail("Incorrect Error Type")
                    return
            }
        }
    }
}
