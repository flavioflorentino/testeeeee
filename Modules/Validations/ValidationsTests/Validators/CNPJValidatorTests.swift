@testable import Validations
import XCTest

final class CNPJValidatorTests: XCTestCase {
    let validator = CNPJValidator()
    
    // MARK: - Validate
    func testValidate_WhenCNPJIsValid_ShouldNotThrowAnError() throws {
        XCTAssertNoThrow(try validator.validate("12.345.678/9101-40"))
    }
    
    func testValidate_WhenCNPJIsValidWithoutMask_ShouldNotThrowAnError() throws {
        XCTAssertNoThrow(try validator.validate("12345678910140"))
    }
    
    func testValidate_WhenCNPJIsInvalid_ShouldThrowAnInvalidDataError() throws {
        XCTAssertThrowsError(try validator.validate("12.345.678/9101-41")) { error in
            guard
                let validationError = error as? GenericValidationError,
                case GenericValidationError.invalidData = validationError
                else {
                    XCTFail("Incorrect Error Type")
                    return
            }
        }
    }
    
    func testValidate_WhenCNPJIsInvalidWithLetters_ShouldThrowAnIncompleteDataError() throws {
        XCTAssertThrowsError(try validator.validate("12.345.678/9101-ab")) { error in
            guard
                let validationError = error as? GenericValidationError,
                case GenericValidationError.incompleteData = validationError
                else {
                    XCTFail("Incorrect Error Type")
                    return
            }
        }
    }
    
    func testValidate_WhenCNPJIsRepeatedNumbers_ShouldThrowAnInvalidDataError() throws {
        let errorMessage = "No Error Throw"
        let validationTypeCompletion: (Error) -> Void = { error in
            guard
                let validationError = error as? GenericValidationError,
                case GenericValidationError.invalidData = validationError
                else {
                    XCTFail("Incorrect Error Type")
                    return
            }
        }
        XCTAssertThrowsError(try validator.validate("00.000.000/0000-00"), errorMessage, validationTypeCompletion)
        XCTAssertThrowsError(try validator.validate("11.111.111/1111-11"), errorMessage, validationTypeCompletion)
        XCTAssertThrowsError(try validator.validate("22.222.222/2222-22"), errorMessage, validationTypeCompletion)
        XCTAssertThrowsError(try validator.validate("33.333.333/3333-33"), errorMessage, validationTypeCompletion)
        XCTAssertThrowsError(try validator.validate("44.444.444/4444-44"), errorMessage, validationTypeCompletion)
        XCTAssertThrowsError(try validator.validate("55.555.555/5555-55"), errorMessage, validationTypeCompletion)
        XCTAssertThrowsError(try validator.validate("66.666.666/6666-66"), errorMessage, validationTypeCompletion)
        XCTAssertThrowsError(try validator.validate("77.777.777/7777-77"), errorMessage, validationTypeCompletion)
        XCTAssertThrowsError(try validator.validate("88.888.888/8888-88"), errorMessage, validationTypeCompletion)
        XCTAssertThrowsError(try validator.validate("99.999.999/9999-99"), errorMessage, validationTypeCompletion)
    }
    
    func testValidate_WhenCNPJIsIncomplete_ShouldThrowAnIncompleteDataError() throws {
        XCTAssertThrowsError(try validator.validate("12.345.678/9101")) { error in
            guard
                let validationError = error as? GenericValidationError,
                case GenericValidationError.incompleteData = validationError
                else {
                    XCTFail("Incorrect Error Type")
                    return
            }
        }
    }
    
    func testValidate_WhenCNPJWithCountGreaterThan11_ShouldThrowAnInvalidDataError() throws {
        XCTAssertThrowsError(try validator.validate("12.345.678/9101-401")) { error in
            guard
                let validationError = error as? GenericValidationError,
                case GenericValidationError.invalidData = validationError
                else {
                    XCTFail("Incorrect Error Type")
                    return
            }
        }
    }
}
