@testable import Validations
import XCTest

final class CPFValidatorTests: XCTestCase {
    let validator = CPFValidator()
    
    // MARK: - Validate
    func testValidate_WhenCPFIsValid_ShouldNotThrowAnError() throws {
        XCTAssertNoThrow(try validator.validate("123.456.789-09"))
    }
    
    func testValidate_WhenCPFIsValidWithoutMask_ShouldNotThrowAnError() throws {
        XCTAssertNoThrow(try validator.validate("12345678909"))
    }
    
    func testValidate_WhenCPFIsInvalidWithLetters_ShouldThrowAnIncompleteDataError() throws {
        XCTAssertThrowsError(try validator.validate("123.456.789-ab")) { error in
            guard
                let validationError = error as? GenericValidationError,
                case GenericValidationError.incompleteData = validationError
                else {
                    XCTFail("Incorrect Error Type")
                    return
            }
        }
    }
    
    func testValidate_WhenCPFIsInvalid_ShouldThrowAnInvalidDataError() throws {
        XCTAssertThrowsError(try validator.validate("123.456.789-10")) { error in
            guard
                let validationError = error as? GenericValidationError,
                case GenericValidationError.invalidData = validationError
                else {
                    XCTFail("Incorrect Error Type")
                    return
            }
        }
    }
    
    func testValidate_WhenCPFIsRepeatedNumbers_ShouldThrowAnInvalidDataError() throws {
        let errorMessage = "No Error Throw"
        let validationTypeCompletion: (Error) -> Void = { error in
            guard
                let validationError = error as? GenericValidationError,
                case GenericValidationError.invalidData = validationError
                else {
                    XCTFail("Incorrect Error Type")
                    return
            }
        }
        XCTAssertThrowsError(try validator.validate("000.000.000-00"), errorMessage, validationTypeCompletion)
        XCTAssertThrowsError(try validator.validate("111.111.111-11"), errorMessage, validationTypeCompletion)
        XCTAssertThrowsError(try validator.validate("222.222.222-22"), errorMessage, validationTypeCompletion)
        XCTAssertThrowsError(try validator.validate("333.333.333-33"), errorMessage, validationTypeCompletion)
        XCTAssertThrowsError(try validator.validate("444.444.444-44"), errorMessage, validationTypeCompletion)
        XCTAssertThrowsError(try validator.validate("555.555.555-55"), errorMessage, validationTypeCompletion)
        XCTAssertThrowsError(try validator.validate("666.666.666-66"), errorMessage, validationTypeCompletion)
        XCTAssertThrowsError(try validator.validate("777.777.777-77"), errorMessage, validationTypeCompletion)
        XCTAssertThrowsError(try validator.validate("888.888.888-88"), errorMessage, validationTypeCompletion)
        XCTAssertThrowsError(try validator.validate("999.999.999-99"), errorMessage, validationTypeCompletion)
    }
    
    func testValidate_WhenCPFIsIncomplete_ShouldThrowAnIncompleteDataError() throws {
        XCTAssertThrowsError(try validator.validate("000.000.001")) { error in
            guard
                let validationError = error as? GenericValidationError,
                case GenericValidationError.incompleteData = validationError
                else {
                    XCTFail("Incorrect Error Type")
                    return
            }
        }
    }
    
    func testValidate_WhenCPFWithCountGreaterThan11_ShouldThrowAnInvalidDataError() throws {
        XCTAssertThrowsError(try validator.validate("000.000.001-911")) { error in
            guard
                let validationError = error as? GenericValidationError,
                case GenericValidationError.invalidData = validationError
                else {
                    XCTFail("Incorrect Error Type")
                    return
            }
        }
    }
}
