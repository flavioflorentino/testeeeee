@testable import Validations
import XCTest

final class CellphoneWithDDDValidatorTests: XCTestCase {
    let validator = CellphoneWithDDDValidator()
    
    // MARK: - Validate
    func testValidate_WhenCellphoneCountIsEqualTo11_ShouldNotThrowAnError() throws {
        XCTAssertNoThrow(try validator.validate("(11) 11111-1111"))
    }
    
    func testValidate_WhenCellphoneCountIsEqualTo11WithoutMask_ShouldNotThrowAnError() throws {
        XCTAssertNoThrow(try validator.validate("11111111111"))
    }
    
    func testValidate_WhenCellphoneCountIsEqualTo10_ShouldNotThrowAnError() throws {
        XCTAssertNoThrow(try validator.validate("(11) 1111-1111"))
    }
    
    func testValidate_WhenCellphoneCountIsEqualTo10WithoutMask_ShouldNotThrowAnError() throws {
        XCTAssertNoThrow(try validator.validate("1111111111"))
    }
    
    func testValidate_WhenCellphoneCountIsLessThan10_ShouldThrowAnIncompleteDataError() throws {
        XCTAssertThrowsError(try validator.validate("(11) 1111-111")) { error in
            guard
                let validationError = error as? GenericValidationError,
                case GenericValidationError.incompleteData = validationError
                else {
                    XCTFail("Incorrect Error Type")
                    return
            }
        }
    }
    
    func testValidate_WhenCellphoneCountIsGreaterThan11_ShouldThrowAnInvalidDataError() throws {
        XCTAssertThrowsError(try validator.validate("(11) 11111-11111")) { error in
            guard
                let validationError = error as? GenericValidationError,
                case GenericValidationError.invalidData = validationError
                else {
                    XCTFail("Incorrect Error Type")
                    return
            }
        }
    }
}
