import Foundation

// swiftlint:disable convenience_type
final class IncomeTaxReturnResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: IncomeTaxReturnResources.self).url(forResource: "IncomeTaxReturnResources", withExtension: "bundle") else {
            return Bundle(for: IncomeTaxReturnResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: IncomeTaxReturnResources.self)
    }()
}
