import Foundation
import Core

public enum IncomeTaxReturnSetup {
    public static var deeplinkInstance: DeeplinkContract?
    
    public static func inject(instance: DeeplinkContract) {
        deeplinkInstance = instance
    }
}
