import AnalyticsModule
import Core
import FeatureFlag

typealias IncomeTaxReturnDependencies =
    HasMainQueue &
    HasAnalytics &
    HasFeatureManager

final class DependencyContainer: IncomeTaxReturnDependencies {
    lazy var mainQueue = DispatchQueue.main
    lazy var analytics: AnalyticsProtocol = Analytics.shared
    lazy var featureManager: FeatureManagerContract = FeatureManager.shared
}
