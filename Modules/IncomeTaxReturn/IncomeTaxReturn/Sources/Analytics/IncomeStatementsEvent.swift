import AnalyticsModule
import Foundation

public enum IncomeStatementsExternalEvent: AnalyticsKeyProtocol {
    case openingButtonClicked
    
    public func event() -> AnalyticsEventProtocol {
        let button = IncomeStatementsEvent.Button(
            name: .incomeStatements,
            screenName: .settings,
            businessContext: .begining
        )
        return IncomeStatementsEvent.buttonClicked(button).event()
    }
}

enum IncomeStatementsEvent: AnalyticsKeyProtocol {
    case screenViewed(Screen)
    case buttonClicked(Button)
    
    private var name: String {
        switch self {
        case .screenViewed:
            return "Screen Viewed"
        case .buttonClicked:
            return "Button Clicked"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .screenViewed(screen):
            var propertiesDict = [
                "screen_name": screen.name.rawValue,
                "business_context": screen.businessContext.rawValue
            ]
            
            if screen.presentedFromDeeplink {
                propertiesDict["method_call"] = "DEEPLINK"
            }
            return propertiesDict
            
        case let .buttonClicked(button):
            return [
                "screen_name": button.screenName.rawValue,
                "button_name": button.name.rawValue,
                "business_context": button.businessContext.rawValue
            ]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.firebase, .mixPanel]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}

extension IncomeStatementsEvent {
    struct Screen {
        let name: ScreenName
        let businessContext: BusinessContext
        let presentedFromDeeplink: Bool
    }
    
    enum ScreenName: String {
        case settings = "AJUSTES"
        case incomeStatements = "INFORME_DE_RENDIMENTOS"
        case feedbackEmptyList = "INFORME_DE_RENDIMENTOS_SEM_RENDIMENTOS"
        case feedbackFetchingFailure = "INFORME_DE_RENDIMENTOS_ERRO_INFORMACOES"
    }
    
    enum BusinessContext: String {
        case incomeStatements = "INFORME_DE_RENDIMENTOS"
        case begining = "INICIO"
    }
}

extension IncomeStatementsEvent {
    struct Button {
        let name: ButtonName
        let screenName: ScreenName
        let businessContext: BusinessContext
    }
    
    enum ButtonName: String {
        case incomeStatements = "INFORME_DE_RENDIMENTOS"
        case download = "BAIXAR"
    }
}
