import Core

enum IncomeStatementsEndpoint {
    case incomeStatementList
}

extension IncomeStatementsEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .incomeStatementList:
            return "report-income/reports"
        }
    }
}
