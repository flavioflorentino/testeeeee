import Foundation

protocol IncomeStatementsPresenting: AnyObject {
    var viewController: IncomeStatementsDisplaying? { get set }
    func presentLoadingAnimation()
    func hideLoadingAnimation()
    func presentNewItems(_ items: [IncomeStatement])
    func presentFeedback(state: IncomeStatementsFeedbackState)
    func hideFeedback()
    func didNextStep(action: IncomeStatementsAction)
}

final class IncomeStatementsPresenter {
    private let coordinator: IncomeStatementsCoordinating
    weak var viewController: IncomeStatementsDisplaying?

    init(coordinator: IncomeStatementsCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - IncomeStatementsPresenting
extension IncomeStatementsPresenter: IncomeStatementsPresenting {
    func presentLoadingAnimation() {
        viewController?.displayLoadingAnimation()
    }
    
    func hideLoadingAnimation() {
        viewController?.hideLoadingAnimation()
    }
    
    func presentNewItems(_ items: [IncomeStatement]) {
        viewController?.displayNewItems(items)
    }
    
    func presentFeedback(state: IncomeStatementsFeedbackState) {
        viewController?.displayFeedback(state.viewModel, helpType: state.helpType)
    }
    
    func hideFeedback() {
        viewController?.hideFeedback()
    }
    
    func didNextStep(action: IncomeStatementsAction) {
        coordinator.perform(action: action)
    }
}
