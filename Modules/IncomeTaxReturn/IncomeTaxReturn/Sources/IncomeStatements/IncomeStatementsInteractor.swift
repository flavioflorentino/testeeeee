import AnalyticsModule
import Core

protocol IncomeStatementsInteracting: AnyObject {
    func fetchIncomeStatements()
    func closeScene()
    func openHelpCenter(helpType: IncomeStatementsHelpType)
    func openIncomeStatement(url: URL)
}

final class IncomeStatementsInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let service: IncomeStatementsServicing
    private let presenter: IncomeStatementsPresenting

    private let presentedFromDeeplink: Bool
    
    init(
        service: IncomeStatementsServicing,
        presenter: IncomeStatementsPresenting,
        dependencies: Dependencies,
        presentedFromDeeplink: Bool
    ) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.presentedFromDeeplink = presentedFromDeeplink
    }
    
    private func trackScreenViewed(_ screenName: IncomeStatementsEvent.ScreenName) {
        let event = IncomeStatementsEvent.screenViewed(
            IncomeStatementsEvent.Screen(
                name: screenName,
                businessContext: .incomeStatements,
                presentedFromDeeplink: presentedFromDeeplink
            )
        )
        dependencies.analytics.log(event)
    }
    
    private func trackDownloadButtonClicked() {
        let event = IncomeStatementsEvent.buttonClicked(
            IncomeStatementsEvent.Button(
                name: .download,
                screenName: .incomeStatements,
                businessContext: .incomeStatements
            )
        )
        dependencies.analytics.log(event)
    }
}

// MARK: - IncomeStatementsInteracting
extension IncomeStatementsInteractor: IncomeStatementsInteracting {
    func fetchIncomeStatements() {
        presenter.presentLoadingAnimation()
        service.getIncomeStatementsResponse { [weak self] result in
            self?.presenter.hideLoadingAnimation()
            
            switch result {
            case .success(let incomeStatementList):
                if incomeStatementList.reportsRequests.isEmpty {
                    self?.presenter.presentFeedback(state: .emptyList)
                    self?.trackScreenViewed(.feedbackEmptyList)
                    return
                }
                self?.presenter.presentNewItems(incomeStatementList.reportsRequests)
                self?.trackScreenViewed(.incomeStatements)
                
            case .failure:
                self?.presenter.presentFeedback(state: .fetchingFailure)
                self?.trackScreenViewed(.feedbackFetchingFailure)
            }
        }
    }
    
    func closeScene() {
        presenter.didNextStep(action: .close)
    }
    
    func openHelpCenter(helpType: IncomeStatementsHelpType) {
        presenter.didNextStep(action: .helpCenter(helpType))
    }
    
    func openIncomeStatement(url: URL) {
        trackDownloadButtonClicked()
        presenter.didNextStep(action: .incomeStatement(url: url))
    }
}
