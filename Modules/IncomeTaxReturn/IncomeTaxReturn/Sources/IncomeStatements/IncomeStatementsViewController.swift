import UI
import UIKit
import SnapKit

protocol IncomeStatementsDisplaying: AnyObject {
    func displayLoadingAnimation()
    func hideLoadingAnimation()
    func displayNewItems(_ items: [IncomeStatement])
    func displayFeedback(_ viewModel: ApolloFeedbackViewContent, helpType: IncomeStatementsHelpType)
    func hideFeedback()
}

private extension IncomeStatementsViewController.Layout {
    static let rowHeight: CGFloat = 64.0
}

final class IncomeStatementsViewController: ViewController<IncomeStatementsInteracting, UIView> {
    fileprivate enum Layout {}
    
    var loadingView = LoadingView()
    
    private typealias ItemProviderType = TableViewDataSource<Int, IncomeStatement>.ItemProvider
    
    private lazy var itemProvider: ItemProviderType = { tableView, indexPath, item -> UITableViewCell? in
        let cell = tableView.dequeueReusableCell(withIdentifier: IncomeStatementCell.identifier, for: indexPath)
        let taxReturnCell = cell as? IncomeStatementCell
        taxReturnCell?.setupView(model: item)
        taxReturnCell?.delegate = self
        return taxReturnCell
    }
    
    private lazy var tableViewDataSource: TableViewDataSource<Int, IncomeStatement> = {
        let dataSource = TableViewDataSource<Int, IncomeStatement>(view: tableView)
        dataSource.itemProvider = itemProvider
        return dataSource
    }()
    
    private lazy var tableHeaderView = IncomeStatementsTableHeaderView()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.rowHeight = Layout.rowHeight
        tableView.separatorStyle = .singleLine
        tableView.separatorColor = Colors.grayscale400.color
        tableView.backgroundColor = .clear
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.delegate = self
        tableView.register(IncomeStatementCell.self, forCellReuseIdentifier: IncomeStatementCell.identifier)
        tableView.tableFooterView = UIView()
        tableView.allowsSelection = false
        return tableView
    }()
    
    private lazy var helpButton: UIBarButtonItem = {
        let label = UILabel()
        label
            .labelStyle(IconLabelStyle(type: .medium))
            .with(\.textColor, .branding600())
            .with(\.text, Iconography.questionCircle.rawValue)
            .with(\.isUserInteractionEnabled, true)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapCustomerSupportButton))
        label.addGestureRecognizer(tapGesture)
        label.accessibilityLabel = Strings.Accessibility.faqButton
        return UIBarButtonItem(customView: label)
    }()
    
    private lazy var additionalInfoView = IncomeStatementsAdditionalInfoView()
    
    private var feedbackView: ApolloFeedbackView?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = tableViewDataSource
        interactor.fetchIncomeStatements()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if tableView.tableHeaderView == nil {
            configureHeaderView()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureNavigationBarColor()
    }

    override func buildViewHierarchy() {
        view.addSubviews(tableView, additionalInfoView)
    }
    
    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.leading.trailing.top.equalToSuperview()
            $0.bottom.equalTo(additionalInfoView.snp.top)
        }
        
        additionalInfoView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(view.compatibleSafeArea.bottom)
        }
    }

    override func configureViews() {
        title = Strings.IncomeStatements.title
        view.backgroundColor = Colors.backgroundPrimary.color
        navigationItem.rightBarButtonItem = helpButton
        
        tableView.isHidden = true
        additionalInfoView.isHidden = true
        tableHeaderView.isAccessibilityElement = false
    }
    
    private func configureNavigationBarColor() {
        guard let navBar = navigationController?.navigationBar else {
            return
        }
        navBar.tintColor = Colors.branding600.color
        navBar.barTintColor = Colors.backgroundPrimary.color
        navBar.titleTextAttributes = [.foregroundColor: Colors.black.color]
    }
    
    private func configureHeaderView() {
        updateViewHeight(for: tableHeaderView)
        tableView.tableHeaderView = tableHeaderView
        tableView.layoutIfNeeded()
    }
    
    private func updateViewHeight(for view: UIView) {
        let expectedSize = view.systemLayoutSizeFitting(
            CGSize(width: tableView.frame.width, height: .leastNonzeroMagnitude),
            withHorizontalFittingPriority: .required,
            verticalFittingPriority: .defaultLow)
        if view.frame.size.height != expectedSize.height {
            view.frame.size.height = expectedSize.height
        }
    }
    
    @objc
    private func tapCustomerSupportButton() {
        interactor.openHelpCenter(helpType: .general)
    }
}

extension IncomeStatementsViewController: UITableViewDelegate {
}

extension IncomeStatementsViewController: IncomeStatementCellDelegate {
    func tapDownloadButton(url: URL) {
        interactor.openIncomeStatement(url: url)
    }
}

extension IncomeStatementsViewController: LoadingViewProtocol {
    // Implement method from protocol to start loading view with no fading animation
    func startLoadingView() {
        loadingView.alpha = 1
        view.addSubview(loadingView)
        
        loadingView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}

// MARK: - IncomeStatementsDisplaying
extension IncomeStatementsViewController: IncomeStatementsDisplaying {
    func displayLoadingAnimation() {
        startLoadingView()
    }
    
    func hideLoadingAnimation() {
        stopLoadingView()
    }
    
    func displayNewItems(_ items: [IncomeStatement]) {
        tableViewDataSource.remove(section: 0)
        tableViewDataSource.add(items: items, to: 0)
        
        tableView.isHidden = false
        additionalInfoView.isHidden = false
        UIAccessibility.post(notification: UIAccessibility.Notification.screenChanged, argument: view)
    }
    
    func displayFeedback(_ viewModel: ApolloFeedbackViewContent, helpType: IncomeStatementsHelpType) {
        let feedbackView = ApolloFeedbackView(content: viewModel)
        feedbackView.didTapPrimaryButton = { [weak self] in
            self?.interactor.closeScene()
        }
        
        feedbackView.didTapSecondaryButton = { [weak self] in
            self?.interactor.openHelpCenter(helpType: helpType)
        }
        
        view.addSubview(feedbackView)

        feedbackView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(view.compatibleSafeArea.bottom)
        }

        self.feedbackView = feedbackView
    }
    
    func hideFeedback() {
        feedbackView?.removeFromSuperview()
        feedbackView = nil
    }
}
