import SnapKit
import UI
import UIKit

final class IncomeStatementsAdditionalInfoView: UIView {
    private lazy var textContainerView: UIView = {
        let view = UIView()
        view.viewStyle(RoundedViewStyle(cornerRadius: .light))
        view.backgroundColor = Colors.grayscale050.color
        return view
    }()
    
    private lazy var textLabel: UILabel = {
        let label = UILabel()
        let text = Strings.IncomeStatements.additionalMessage
        label.labelStyle(BodySecondaryLabelStyle())
        label.attributedText = text.attributedStringWithFont(
            primary: Typography.bodySecondary(.default).font(),
            secondary: Typography.bodySecondary(.highlight).font()
        )
        label.backgroundColor = .clear
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension IncomeStatementsAdditionalInfoView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(textContainerView)
        textContainerView.addSubview(textLabel)
    }
    
    func setupConstraints() {
        textContainerView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base04)
        }
        
        textLabel.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureViews() {
        backgroundColor = .clear
    }
}
