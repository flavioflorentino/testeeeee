import AssetsKit
import SnapKit
import UI
import UIKit

private extension IncomeStatementsTableHeaderView.Layout {
    static let headerImageHeight: CGFloat = 140.0
}

final class IncomeStatementsTableHeaderView: UIView {
    fileprivate enum Layout {}
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView(image: Resources.Illustrations.iluCash.image)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var textLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
        let text = Strings.IncomeStatements.headerMessage
        label.attributedText = text.attributedStringWithFont(
            primary: Typography.bodyPrimary(.default).font(),
            secondary: Typography.bodyPrimary(.highlight).font(),
            textAlignment: .center
        )
        label.backgroundColor = .clear
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension IncomeStatementsTableHeaderView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubviews(imageView, textLabel)
    }
    
    func setupConstraints() {
        imageView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.centerX.equalToSuperview()
            $0.height.equalTo(Layout.headerImageHeight)
        }
        
        textLabel.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.bottom.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureViews() {
        backgroundColor = .clear
    }
}
