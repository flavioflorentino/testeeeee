import SnapKit
import UI
import UIKit

protocol IncomeStatementCellDelegate: AnyObject {
    func tapDownloadButton(url: URL)
}

final class IncomeStatementCell: UITableViewCell {
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
        return label
    }()
    
    private lazy var actionButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.General.download, for: .normal)
        button.buttonStyle(LinkButtonStyle(size: .default, icon: (name: Iconography.arrowDown, alignment: .left)))
        button.setTitleColor(Colors.branding600.color, for: .normal)
        button.addTarget(self, action: #selector(tapDownloadButton), for: .touchUpInside)
        button.isUserInteractionEnabled = true
        button.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        return button
    }()
    
    weak var delegate: IncomeStatementCellDelegate?
    
    private var urlPdf: URL?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func accessibilityActivate() -> Bool {
        super.accessibilityActivate()
        tapDownloadButton()
        return true
    }
    
    func setupView(model: IncomeStatement) {
        titleLabel.text = model.fiscalYear
        accessibilityLabel = Strings.Accessibility.incomeStatementCell(model.fiscalYear)
        urlPdf = model.urlPdf
    }
    
    @objc
    private func tapDownloadButton() {
        guard let urlPdf = urlPdf else { return }
        delegate?.tapDownloadButton(url: urlPdf)
    }
}

extension IncomeStatementCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubviews(titleLabel, actionButton)
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.centerY.equalToSuperview().offset(Spacing.base01)
            $0.trailing.equalTo(actionButton.snp.leading).offset(-Spacing.base02)
        }
        
        actionButton.snp.makeConstraints {
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.centerY.equalTo(titleLabel)
        }
    }
    
    func configureViews() {
        selectionStyle = .none
        separatorInset = UIEdgeInsets(top: 0, left: Spacing.base02, bottom: 0, right: Spacing.base02)
        contentView.isUserInteractionEnabled = true

        contentView.isAccessibilityElement = false
        isAccessibilityElement = true
        accessibilityTraits = .button
    }
}
