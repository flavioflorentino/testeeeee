import UIKit

public enum IncomeStatementsFactory {
    public static func make(presentedFromDeeplink: Bool = false) -> UIViewController {
        let container = DependencyContainer()
        let service: IncomeStatementsServicing = IncomeStatementsService(dependencies: container)
        let coordinator: IncomeStatementsCoordinating = IncomeStatementsCoordinator(dependencies: container)
        let presenter: IncomeStatementsPresenting = IncomeStatementsPresenter(coordinator: coordinator)
        let interactor = IncomeStatementsInteractor(
            service: service,
            presenter: presenter,
            dependencies: container,
            presentedFromDeeplink: presentedFromDeeplink
        )
        let viewController = IncomeStatementsViewController(interactor: interactor)

        coordinator.deeplinkHandler = IncomeTaxReturnSetup.deeplinkInstance
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
