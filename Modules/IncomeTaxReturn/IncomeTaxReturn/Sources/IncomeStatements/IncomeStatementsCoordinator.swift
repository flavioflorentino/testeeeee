import Core
import FeatureFlag
import SafariServices

enum IncomeStatementsAction: Equatable {
    case close
    case helpCenter(IncomeStatementsHelpType)
    case incomeStatement(url: URL)
}

protocol IncomeStatementsCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var deeplinkHandler: DeeplinkContract? { get set }
    func perform(action: IncomeStatementsAction)
}

final class IncomeStatementsCoordinator {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    weak var viewController: UIViewController?
    
    var deeplinkHandler: DeeplinkContract?
    
    private var isFirstControllerInNavigation: Bool {
        viewController?.navigationController?.viewControllers.first == viewController
    }
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - IncomeStatementsCoordinating
extension IncomeStatementsCoordinator: IncomeStatementsCoordinating {
    func perform(action: IncomeStatementsAction) {
        switch action {
        case .close:
            if isFirstControllerInNavigation {
                viewController?.navigationController?.dismiss(animated: true)
                return
            }
            viewController?.navigationController?.popViewController(animated: true)
            
        case .helpCenter(let helpType):
            let urlPath = dependencies.featureManager.text(helpType.featureConfig)
            guard let url = URL(string: urlPath) else { return }
            deeplinkHandler?.open(url: url)
            
        case .incomeStatement(let url):
            let safariViewController = SFSafariViewController(url: url)
            viewController?.navigationController?.present(safariViewController, animated: true)
        }
    }
}
