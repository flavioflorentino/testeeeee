import Core
import Foundation

protocol IncomeStatementsServicing {
    func getIncomeStatementsResponse(completion: @escaping (Result<IncomeStatementsResponse, ApiError>) -> Void)
}

final class IncomeStatementsService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - IncomeStatementsServicing
extension IncomeStatementsService: IncomeStatementsServicing {
    func getIncomeStatementsResponse(completion: @escaping (Result<IncomeStatementsResponse, ApiError>) -> Void) {
        let endpoint = IncomeStatementsEndpoint.incomeStatementList
        let decoder = JSONDecoder(.convertFromSnakeCase)
        Api<IncomeStatementsResponse>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
