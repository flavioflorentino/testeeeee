import Foundation
import FeatureFlag

enum IncomeStatementsHelpType {
    case general
    case emptyList
    case fetchingFailure
    
    var featureConfig: FeatureConfig {
        switch self {
        case .general:
            return .incomeStatementsHelpUrl
        case .emptyList:
            return .incomeStatementsEmptyHelpUrl
        case .fetchingFailure:
            return .incomeStatementsErrorHelpUrl
        }
    }
}
