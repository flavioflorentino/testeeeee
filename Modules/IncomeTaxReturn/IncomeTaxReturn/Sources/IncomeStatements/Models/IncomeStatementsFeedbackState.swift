import AssetsKit
import Foundation
import UI
import UIKit

enum IncomeStatementsFeedbackState {
    private typealias Localizable = Strings.IncomeStatements
    
    case emptyList
    case fetchingFailure
    
    var viewModel: ApolloFeedbackViewContent {
        switch self {
        case .emptyList:
            return ApolloFeedbackViewContent(
                image: Resources.Illustrations.iluPeople.image,
                title: Localizable.Empty.title,
                description: NSAttributedString(string: Localizable.Empty.description),
                primaryButtonTitle: Localizable.Empty.primaryButtonTitle,
                secondaryButtonTitle: Localizable.Empty.secondaryButtonTitle
            )
        case .fetchingFailure:
            return ApolloFeedbackViewContent(
                image: Resources.Illustrations.iluNotFoundCircledBackground.image,
                title: Localizable.FetchingFailure.title,
                description: NSAttributedString(string: Localizable.FetchingFailure.description),
                primaryButtonTitle: Localizable.FetchingFailure.primaryButtonTitle,
                secondaryButtonTitle: Localizable.FetchingFailure.secondaryButtonTitle
            )
        }
    }
    
    var helpType: IncomeStatementsHelpType {
        switch self {
        case .emptyList:
            return .emptyList
        case .fetchingFailure:
            return .fetchingFailure
        }
    }
}
