import Foundation

enum IncomeStatementsError: Error {
    case invalidPdfUrlOnParse
}

struct IncomeStatementsResponse: Decodable, Equatable {
    let reportsRequests: [IncomeStatement]
}

struct IncomeStatement: Decodable, Equatable {
    let fiscalYear: String
    let urlPdf: URL
    
    private enum CodingKeys: String, CodingKey {
        case fiscalYear, urlPdf
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        fiscalYear = try container.decode(String.self, forKey: .fiscalYear)
        
        let urlPdfPath = try container.decode(String.self, forKey: .urlPdf)

        guard let url = URL(string: urlPdfPath) else {
            throw IncomeStatementsError.invalidPdfUrlOnParse
        }
        
        urlPdf = url
    }
}
