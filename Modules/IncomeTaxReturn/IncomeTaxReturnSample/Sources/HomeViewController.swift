import IncomeTaxReturn
import UIKit
import UI
import SnapKit

class HomeViewController: UIViewController {
    private lazy var button: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("Informe de Rendimentos", for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTouchButton), for: .touchUpInside)
        return button
    }()
    
    init() {
        super.init(nibName: nil, bundle: nil)
        buildViewHierarchy()
        setupConstraints()
        configureViews()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let navBar = navigationController?.navigationBar else {
            return
        }
        navBar.tintColor = Colors.grayscale700.color
        navBar.barTintColor = Colors.grayscale050.color
        navBar.titleTextAttributes = [.foregroundColor: Colors.grayscale700.color]
    }
    
    @objc
    func didTouchButton() {
        guard let navigationController = navigationController else {
            return
        }
        navigationController.pushViewController(IncomeStatementsFactory.make(), animated: true)
    }
}

extension HomeViewController: ViewConfiguration {
    func buildViewHierarchy() {
        view.addSubview(button)
    }
    
    func setupConstraints() {
        button.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base04)
            $0.trailing.equalToSuperview().offset(-Spacing.base04)
        }
    }
    
    func configureViews() {
        title = "Ajustes"
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}
