import AssetsKit
import Core
@testable import IncomeTaxReturn
import UI
import XCTest

private class IncomeStatementsDisplayingSpy: IncomeStatementsDisplaying {
    // MARK: - displayLoadingAnimation
    private(set) var displayLoadingAnimationCallsCount = 0

    func displayLoadingAnimation() {
        displayLoadingAnimationCallsCount += 1
    }

    // MARK: - hideLoadingAnimation
    private(set) var hideLoadingAnimationCallsCount = 0

    func hideLoadingAnimation() {
        hideLoadingAnimationCallsCount += 1
    }

    // MARK: - displayNewItems
    private(set) var displayNewItemsCallsCount = 0
    private(set) var displayNewItemsReceivedInvocations: [[IncomeStatement]] = []

    func displayNewItems(_ items: [IncomeStatement]) {
        displayNewItemsCallsCount += 1
        displayNewItemsReceivedInvocations.append(items)
    }

    // MARK: - displayFeedback
    private(set) var displayFeedbackCallsCount = 0
    private(set) var displayFeedbackReceivedInvocations: [(viewModel: ApolloFeedbackViewContent, helpType: IncomeStatementsHelpType)] = []

    func displayFeedback(_ viewModel: ApolloFeedbackViewContent, helpType: IncomeStatementsHelpType) {
        displayFeedbackCallsCount += 1
        displayFeedbackReceivedInvocations.append((viewModel: viewModel, helpType: helpType))
    }

    // MARK: - hideFeedback
    private(set) var hideFeedbackCallsCount = 0

    func hideFeedback() {
        hideFeedbackCallsCount += 1
    }
}

private class IncomeStatementsCoordinatingSpy: IncomeStatementsCoordinating {
    var viewController: UIViewController?
    var deeplinkHandler: DeeplinkContract?

    // MARK: - perform
    private(set) var performActionCallsCount = 0
    private(set) var performActionReceivedInvocations: [IncomeStatementsAction] = []

    func perform(action: IncomeStatementsAction) {
        performActionCallsCount += 1
        performActionReceivedInvocations.append(action)
    }
}

final class IncomeStatementsPresenterTests: XCTestCase {
    private let coordinatorSpy = IncomeStatementsCoordinatingSpy()
    private let viewControllerSpy = IncomeStatementsDisplayingSpy()
    
    private lazy var sut: IncomeStatementsPresenting = {
        let presenter = IncomeStatementsPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testPresentLoadingAnimation_ShouldDisplayLoadingAnimation() {
        sut.presentLoadingAnimation()
        
        XCTAssertEqual(viewControllerSpy.displayLoadingAnimationCallsCount, 1)
    }
    
    func testHideLoadingAnimation_ShouldHideLoadingAnimation() {
        sut.hideLoadingAnimation()
        
        XCTAssertEqual(viewControllerSpy.hideLoadingAnimationCallsCount, 1)
    }
    
    func testPresentNewItems_ShouldDisplayNewItems() throws {
        let response: IncomeStatementsResponse = try JSONFileDecoder().load(resource: "IncomeStatementsResponseMock")
        let reports = response.reportsRequests
        
        sut.presentNewItems(reports)
        
        XCTAssertEqual(viewControllerSpy.displayNewItemsCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.displayNewItemsReceivedInvocations.first, reports)
    }
    
    func testPresentFeedback_WhenIsEmptyState_ShouldDisplayEmptyStateFeedback() {
        sut.presentFeedback(state: .emptyList)
        
        XCTAssertEqual(viewControllerSpy.displayFeedbackCallsCount, 1)
        let expectedFeedbackContent = ApolloFeedbackViewContent(
            image: Resources.Illustrations.iluPeople.image,
            title: Strings.IncomeStatements.Empty.title,
            description: NSAttributedString(string: Strings.IncomeStatements.Empty.description),
            primaryButtonTitle: Strings.IncomeStatements.Empty.primaryButtonTitle,
            secondaryButtonTitle: Strings.IncomeStatements.Empty.secondaryButtonTitle
        )
        
        let feedbackItem = viewControllerSpy.displayFeedbackReceivedInvocations.first
        XCTAssertEqual(feedbackItem?.viewModel, expectedFeedbackContent)
        XCTAssertEqual(feedbackItem?.helpType, .emptyList)
    }
    
    func testPresentFeedback_WhenIsFetchingFailureState_ShouldDisplayFetchingFailureStateFeedback() {
        sut.presentFeedback(state: .fetchingFailure)
        
        XCTAssertEqual(viewControllerSpy.displayFeedbackCallsCount, 1)
        let expectedFeedbackContent = ApolloFeedbackViewContent(
            image: Resources.Illustrations.iluNotFoundCircledBackground.image,
            title: Strings.IncomeStatements.FetchingFailure.title,
            description: NSAttributedString(string: Strings.IncomeStatements.FetchingFailure.description),
            primaryButtonTitle: Strings.IncomeStatements.FetchingFailure.primaryButtonTitle,
            secondaryButtonTitle: Strings.IncomeStatements.FetchingFailure.secondaryButtonTitle
        )
        
        let feedbackItem = viewControllerSpy.displayFeedbackReceivedInvocations.first
        XCTAssertEqual(feedbackItem?.viewModel, expectedFeedbackContent)
        XCTAssertEqual(feedbackItem?.helpType, .fetchingFailure)
    }
    
    func testHideFeedback_ShouldHideFeedbackView() {
        sut.hideFeedback()
        
        XCTAssertEqual(viewControllerSpy.hideFeedbackCallsCount, 1)
    }
    
    func testDidNextStep_WhenActionIsClose_ShouldCloseScene() {
        sut.didNextStep(action: .close)
        
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.performActionReceivedInvocations.first, .close)
    }
    
    func testDidNextStep_WhenActionIsHelpCenterWithEmptyListType_ShouldOpenEmptyListHelp() {
        sut.didNextStep(action: .helpCenter(.emptyList))
        
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.performActionReceivedInvocations.first, .helpCenter(.emptyList))
    }
    
    func testDidNextStep_WhenActionIsHelpCenterWithFetchingFailureType_ShouldOpenFetchingFailureHelp() {
        sut.didNextStep(action: .helpCenter(.fetchingFailure))
        
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.performActionReceivedInvocations.first, .helpCenter(.fetchingFailure))
    }
    
    func testDidNextStep_WhenActionIsHelpCenterWithGeneralType_ShouldOpenGeneralHelp() {
        sut.didNextStep(action: .helpCenter(.general))
        
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.performActionReceivedInvocations.first, .helpCenter(.general))
    }
    
    func testDidNextStep_WhenActionIsIncomeStatement_ShouldOpenIncomeStatement() throws {
        let url = try XCTUnwrap(URL(string: "http://foo.com/statement1.pdf"))
        
        sut.didNextStep(action: .incomeStatement(url: url))
        
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.performActionReceivedInvocations.first, .incomeStatement(url: url))
    }
}
