@testable import IncomeTaxReturn
import XCTest

final class IncomeStatementsResponseTests: XCTestCase {
    func testDecodeIncomeStatementsRespose_WhenUrlsAreValid_ShouldDecodeSuccefully() throws {
        let response: IncomeStatementsResponse = try JSONFileDecoder().load(resource: "IncomeStatementsResponseMock")
        let reportRequests = response.reportsRequests
        XCTAssertEqual(reportRequests.count, 3)
        
        XCTAssertEqual(reportRequests[0].fiscalYear, "2020")
        XCTAssertEqual(reportRequests[0].urlPdf.absoluteString, "https://www.foo.org/statement0.pdf")
        
        XCTAssertEqual(reportRequests[1].fiscalYear, "2019")
        XCTAssertEqual(reportRequests[1].urlPdf.absoluteString, "https://www.foo.org/statement1.pdf")
        
        XCTAssertEqual(reportRequests[2].fiscalYear, "2018")
        XCTAssertEqual(reportRequests[2].urlPdf.absoluteString, "https://www.foo.org/statement2.pdf")
    }
    
    func testDecodeIncomeStatementsRespose_WhenAnUrlIsInvalid_ShouldThrowError() {
        do {
            let _: IncomeStatementsResponse = try JSONFileDecoder().load(resource: "IncomeStatementsResponseInvalidMock")
        } catch {
            XCTAssert(error is IncomeStatementsError)
        }
    }
}
