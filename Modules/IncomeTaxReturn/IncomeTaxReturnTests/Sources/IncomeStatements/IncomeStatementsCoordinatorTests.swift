import Core
import FeatureFlag
@testable import IncomeTaxReturn
import SafariServices
import XCTest

private final class DeeplinkWrapperSpy: DeeplinkContract {
    private(set) var callOpenDeeplinkCount = 0
    private(set) var url: URL?
    
    func open(url: URL) -> Bool {
        callOpenDeeplinkCount += 1
        self.url = url
        return true
    }
}

final class IncomeStatementsCoordinatorTests: XCTestCase {
    private let navControllerSpy = NavigationControllerSpy(rootViewController: UIViewController())
    private let deeplinkWrapperSpy = DeeplinkWrapperSpy()
    private let featureManagerMock = FeatureManagerMock()
    
    private lazy var sut: IncomeStatementsCoordinating = {
        let coordinator = IncomeStatementsCoordinator(dependencies: DependencyContainerMock(featureManagerMock))
        coordinator.viewController = navControllerSpy.topViewController
        coordinator.deeplinkHandler = deeplinkWrapperSpy
        return coordinator
    }()
    
    func testPerformAction_WhenActionIsCloseAndIsFirstControllerInNavigation_ShouldDismisNavigationController() {
        sut.perform(action: .close)
        
        XCTAssertEqual(navControllerSpy.dismissCallsCount, 1)
    }
    
    func testPerformAction_WhenActionIsCloseAndIsNotFirstControllerInNavigation_ShouldDismisNavigationController() {
        navControllerSpy.addChild(UIViewController())
        sut.viewController = navControllerSpy.topViewController
        
        sut.perform(action: .close)
        
        XCTAssertEqual(navControllerSpy.popViewControllerCallsCount, 1)
    }
    
    func testPerformAction_WhenActionIsOpenHelpCenterWithGeneralType_ShouldOpenHelpCenter() throws {
        let helpUrl = "picpay://picpay/helpcenter/article/statementHelp0"
        featureManagerMock.override(keys: .incomeStatementsHelpUrl, with: helpUrl)
        
        sut.perform(action: .helpCenter(.general))
        XCTAssertEqual(deeplinkWrapperSpy.callOpenDeeplinkCount, 1)
        let expectedUrl = try XCTUnwrap(URL(string: helpUrl))
        XCTAssertEqual(deeplinkWrapperSpy.url, expectedUrl)
    }
    
    func testPerformAction_WhenActionIsOpenHelpCenterWithEmptyListType_ShouldOpenHelpCenter() throws {
        let helpUrl = "picpay://picpay/helpcenter/article/statementHelp1"
        featureManagerMock.override(keys: .incomeStatementsEmptyHelpUrl, with: helpUrl)
        
        sut.perform(action: .helpCenter(.emptyList))
        
        XCTAssertEqual(deeplinkWrapperSpy.callOpenDeeplinkCount, 1)
        let expectedUrl = try XCTUnwrap(URL(string: helpUrl))
        XCTAssertEqual(deeplinkWrapperSpy.url, expectedUrl)
    }
    
    func testPerformAction_WhenActionIsOpenHelpCenterWithFetchingFailureType_ShouldOpenHelpCenter() throws {
        let helpUrl = "picpay://picpay/helpcenter/article/statementHelp2"
        featureManagerMock.override(keys: .incomeStatementsErrorHelpUrl, with: helpUrl)
        
        sut.perform(action: .helpCenter(.fetchingFailure))
        
        XCTAssertEqual(deeplinkWrapperSpy.callOpenDeeplinkCount, 1)
        let expectedUrl = try XCTUnwrap(URL(string: helpUrl))
        XCTAssertEqual(deeplinkWrapperSpy.url, expectedUrl)
    }
    
    func testPerformAction_WhenActionIsIncomeStatement_ShouldOpenIncomeStatement() throws {
        let url = try XCTUnwrap(URL(string: "http://foo.com/statement1.pdf"))
        sut.perform(action: .incomeStatement(url: url))
        
        XCTAssertEqual(navControllerSpy.presentViewControllerCallsCount, 1)
        XCTAssert(navControllerSpy.lastPresentedViewController is SFSafariViewController)
    }
}
