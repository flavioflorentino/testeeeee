import AnalyticsModule
import Core
@testable import IncomeTaxReturn
import UI
import XCTest

final class IncomeStatementsServicingMock: IncomeStatementsServicing {
    var result: Result<IncomeStatementsResponse, ApiError>?
    
    // MARK: - getIncomeStatementsResponse
    func getIncomeStatementsResponse(completion: @escaping (Result<IncomeStatementsResponse, ApiError>) -> Void) {
        guard let result = result else {
            XCTFail("Mocked retult for getIncomeStatementsResponse method is nil")
            return
        }
        completion(result)
    }
}

private class IncomeStatementsPresentingSpy: IncomeStatementsPresenting {
    var viewController: IncomeStatementsDisplaying?

    // MARK: - presentLoadingAnimation
    private(set) var presentLoadingAnimationCallsCount = 0

    func presentLoadingAnimation() {
        presentLoadingAnimationCallsCount += 1
    }

    // MARK: - hideLoadingAnimation
    private(set) var hideLoadingAnimationCallsCount = 0

    func hideLoadingAnimation() {
        hideLoadingAnimationCallsCount += 1
    }

    // MARK: - presentNewItems
    private(set) var presentNewItemsCallsCount = 0
    private(set) var presentNewItemsReceivedInvocations: [[IncomeStatement]] = []

    func presentNewItems(_ items: [IncomeStatement]) {
        presentNewItemsCallsCount += 1
        presentNewItemsReceivedInvocations.append(items)
    }

    // MARK: - presentFeedback
    private(set) var presentFeedbackTypeCallsCount = 0
    private(set) var presentFeedbackTypeReceivedInvocations: [IncomeStatementsFeedbackState] = []

    func presentFeedback(state: IncomeStatementsFeedbackState) {
        presentFeedbackTypeCallsCount += 1
        presentFeedbackTypeReceivedInvocations.append(state)
    }

    // MARK: - hideError
    private(set) var hideFeedbackCallsCount = 0

    func hideFeedback() {
        hideFeedbackCallsCount += 1
    }

    // MARK: - didNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didNextStepActionReceivedInvocations: [IncomeStatementsAction] = []

    func didNextStep(action: IncomeStatementsAction) {
        didNextStepActionCallsCount += 1
        didNextStepActionReceivedInvocations.append(action)
    }
}

final class StatementListInteractorTests: XCTestCase {
    private var serviceMock = IncomeStatementsServicingMock()
    private var presenterSpy = IncomeStatementsPresentingSpy()
    private let analyticsSpy = AnalyticsSpy()
    private lazy var sut: IncomeStatementsInteracting = IncomeStatementsInteractor(
        service: serviceMock,
        presenter: presenterSpy,
        dependencies: DependencyContainerMock(analyticsSpy),
        presentedFromDeeplink: false
    )
    
    func testFetchIncomeStatements_WhenSuccessResponse_ShouldPresentNewItems() throws {
        let response: IncomeStatementsResponse = try JSONFileDecoder().load(resource: "IncomeStatementsResponseMock")
        let reports = response.reportsRequests
        serviceMock.result = .success(IncomeStatementsResponse(reportsRequests: reports))
        
        sut.fetchIncomeStatements()
        
        XCTAssertEqual(presenterSpy.presentLoadingAnimationCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingAnimationCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentNewItemsCallsCount, 1)
        XCTAssertEqual(reports, presenterSpy.presentNewItemsReceivedInvocations.first)
    }
    
    func testFetchIncomeStatements_WhenSuccessResponseButNoItemsReturned_ShouldPresentNotFoundFeedback() {
        serviceMock.result = .success(IncomeStatementsResponse(reportsRequests: []))
        
        sut.fetchIncomeStatements()
        
        XCTAssertEqual(presenterSpy.presentLoadingAnimationCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingAnimationCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentFeedbackTypeCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentFeedbackTypeReceivedInvocations.first, .emptyList)
    }
    
    func testFetchIncomeStatements_WhenErrorResponse_ShouldPresentError() {
        serviceMock.result = .failure(.badRequest(body: RequestError()))

        sut.fetchIncomeStatements()

        XCTAssertEqual(presenterSpy.presentLoadingAnimationCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingAnimationCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentFeedbackTypeCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentFeedbackTypeReceivedInvocations.first, .fetchingFailure)
    }
    
    func testCloseScene_ShouldCloseTheScene() {
        sut.closeScene()
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations.first, .close)
    }
    
    func testOpenHelpCenter_WithGeneralHelpType_ShouldPresentGeneralHelp() {
        sut.openHelpCenter(helpType: .general)
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations.first, .helpCenter(.general))
    }
    
    func testOpenHelpCenter_WithEmptyListHelpType_ShouldPresentEmptyListHelp() {
        sut.openHelpCenter(helpType: .emptyList)
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations.first, .helpCenter(.emptyList))
    }
    
    func testOpenHelpCenter_WithFetchingFailureHelpType_ShouldPresentFetchingFailureHelp() {
        sut.openHelpCenter(helpType: .fetchingFailure)
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations.first, .helpCenter(.fetchingFailure))
    }
    
    func testOpenIncomeStatement() throws {
        let url = try XCTUnwrap(URL(string: "http://foo.com/statement1.pdf"))
        
        sut.openIncomeStatement(url: url)
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations.first, .incomeStatement(url: url))
    }
}
