import Foundation

public protocol AccessibilityConfiguration: AnyObject {
    func buildAccessibilityHierarchy()
    func updateAccessibilityAttributes()
    func setupdAccessibility()
}

public extension AccessibilityConfiguration {
    func buildAccessibilityHierarchy() { }
    func updateAccessibilityAttributes() { }
    
    func setupdAccessibility() {
        buildAccessibilityHierarchy()
        updateAccessibilityAttributes()
    }
}
