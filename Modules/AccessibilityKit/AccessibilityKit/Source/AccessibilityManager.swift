import Foundation

public protocol AccessibilityManagerContract {
    var featuresStatus: [String: Any] { get }
    func isActive(for featureType: AccessibilityFeatureType) -> Bool
    func announcement(from type: AccessibilityAnnouncementType, value: String)
}

public final class AccessibilityManager: AccessibilityManagerContract {
    private let featureTypes: [AccessibilityFeatureType]
    private let featureProvider: AccessibilityFeatureProvidering
    private let announcementProvider: AccessibilityAnnouncementProviding
    
    private var notificationObservers: AccessibilityNotificationObserving?
    
    public var featuresStatus: [String: Any] {
        featureProvider.status
    }
    
    public static let shared = AccessibilityManager()
    
    public init(
        featureTypes: [AccessibilityFeatureType] = AccessibilityFeatureType.allCases,
        featureProvider: AccessibilityFeatureProvidering = AccessibilityFeatureProvider(),
        announcementProvider: AccessibilityAnnouncementProviding = AccessibilityAnnouncementProvider()
    ) {
        self.featureTypes = featureTypes
        self.featureProvider = featureProvider
        self.announcementProvider = announcementProvider
        
        configureObservers()
    }
    
    public func isActive(for featureType: AccessibilityFeatureType) -> Bool {
        featureProvider.isFeatureActive(for: featureType)
    }
    
    public func announcement(from type: AccessibilityAnnouncementType, value: String) {
        announcementProvider.post(announcement: type, string: value)
    }
    
    // MARK: - Private Methods
    private func configureObservers() {
        notificationObservers = AccessibilityNotificationObserver(featureProvider: featureProvider)
        notificationObservers?.configureFeatureObservers(from: featureTypes)
    }
}
