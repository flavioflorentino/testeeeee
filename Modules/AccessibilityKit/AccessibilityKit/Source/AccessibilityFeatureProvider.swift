import Foundation
import UIKit

public protocol AccessibilityFeatureProvidering {
    var isAssistiveTouchEnabled: Bool { get }
    var isDarkerSystemColorsEnabled: Bool { get }
    var isGuidedAccessEnabled: Bool { get }
    var hearingDevicePairedEar: UIAccessibility.HearingDeviceEar { get }
    var largerTextCatagory: UIContentSizeCategory { get }
    var isOnOffSwitchLabelsEnabled: Bool { get }
    var isShakeToUndoEnabled: Bool { get }
    var isSpeakScreenEnabled: Bool { get }
    var isSpeakSelectionEnabled: Bool { get }
    var isClosedCaptioningEnabled: Bool { get }
    var isGrayscaleEnabled: Bool { get }
    var isMonoAudioEnabled: Bool { get }
    var isVideoAutoplayEnabled: Bool { get }
    var isDifferentiateWithoutColorEnabled: Bool { get }
    var isBoldTextEnabled: Bool { get }
    var isInvertColorsEnabled: Bool { get }
    var isReduceTransparencyEnabled: Bool { get }
    var isSwitchControlEnabled: Bool { get }
    var isReduceMotionEnabled: Bool { get }
    var isVoiceOverEnabled: Bool { get }
    
    var status: [String: Any] { get }
    func isFeatureActive(for type: AccessibilityFeatureType) -> Bool
}

public final class AccessibilityFeatureProvider: AccessibilityFeatureProvidering {
    public var isAssistiveTouchEnabled: Bool {
        UIAccessibility.isAssistiveTouchRunning
    }
    
    public var isDarkerSystemColorsEnabled: Bool {
        UIAccessibility.isDarkerSystemColorsEnabled
    }
    
    public var isGuidedAccessEnabled: Bool {
        UIAccessibility.isGuidedAccessEnabled
    }
    
    public var hearingDevicePairedEar: UIAccessibility.HearingDeviceEar {
        UIAccessibility.hearingDevicePairedEar
    }
    
    public var largerTextCatagory: UIContentSizeCategory {
        UIScreen.main.traitCollection.preferredContentSizeCategory
    }
    
    public var isOnOffSwitchLabelsEnabled: Bool {
        if #available(iOS 13.0, *) {
            return UIAccessibility.isOnOffSwitchLabelsEnabled
        }
        
        return false
    }
    
    public var isShakeToUndoEnabled: Bool {
        UIAccessibility.isShakeToUndoEnabled
    }
    
    public var isSpeakScreenEnabled: Bool {
        UIAccessibility.isSpeakScreenEnabled
    }
    
    public var isSpeakSelectionEnabled: Bool {
        UIAccessibility.isSpeakSelectionEnabled
    }
    
    public var isClosedCaptioningEnabled: Bool {
        UIAccessibility.isClosedCaptioningEnabled
    }
    
    public var isGrayscaleEnabled: Bool {
        UIAccessibility.isGrayscaleEnabled
    }
    
    public var isMonoAudioEnabled: Bool {
        UIAccessibility.isMonoAudioEnabled
    }
    
    public var isVideoAutoplayEnabled: Bool {
        if #available(iOS 13.0, *) {
            return UIAccessibility.isVideoAutoplayEnabled
        }
        
        return false
    }
    
    public var isDifferentiateWithoutColorEnabled: Bool {
        if #available(iOS 13.0, *) {
            return UIAccessibility.shouldDifferentiateWithoutColor
        }
        
        return false
    }
    
    public var isBoldTextEnabled: Bool {
        UIAccessibility.isBoldTextEnabled
    }
    
    public var isInvertColorsEnabled: Bool {
        UIAccessibility.isInvertColorsEnabled
    }
    
    public var isReduceTransparencyEnabled: Bool {
        UIAccessibility.isReduceTransparencyEnabled
    }
    
    public var isSwitchControlEnabled: Bool {
        UIAccessibility.isSwitchControlRunning
    }
    
    public var isReduceMotionEnabled: Bool {
        UIAccessibility.isReduceMotionEnabled
    }
    
    public var isVoiceOverEnabled: Bool {
        UIAccessibility.isVoiceOverRunning
    }
    
    public var status: [String: Any] {
        let featureTypes = AccessibilityFeatureType.allCases
        var featureStatus = [String: Any]()
            
        featureTypes.lazy.forEach {
            featureStatus[$0.rawValue] = isFeatureActive(for: $0)
        }
        
        return featureStatus
    }
    
    public init() { }
    
    // swiftlint:disable:next cyclomatic_complexity function_body_length
    public func isFeatureActive(for type: AccessibilityFeatureType) -> Bool {
        switch type {
        case .assistiveTouch:
            return isAssistiveTouchEnabled
        case .darkerSystemColors:
            return isDarkerSystemColorsEnabled
        case .guidedAccess:
            return isGuidedAccessEnabled
        case .hearingDevice:
            return hearingDevicePairedEar.rawValue != 0
        case .onOffSwitchLabels:
            return isOnOffSwitchLabelsEnabled
        case .shakeToUndo:
            return isShakeToUndoEnabled
        case .speakScreen:
            return isSpeakScreenEnabled
        case .speakSelection:
            return isSpeakSelectionEnabled
        case .closedCaptioning:
            return isClosedCaptioningEnabled
        case .grayscale:
            return isGrayscaleEnabled
        case .monoAudio:
            return isMonoAudioEnabled
        case .videoAutoplay:
            return isVideoAutoplayEnabled
        case .largerText:
            return largerTextCatagory != .medium
        case .differentiateWithoutColor:
            return isDifferentiateWithoutColorEnabled
        case .invertColors:
            return isInvertColorsEnabled
        case .reduceTransparency:
            return isReduceTransparencyEnabled
        case .switchControl:
            return isSwitchControlEnabled
        case .boldText:
            return isBoldTextEnabled
        case .reduceMotion:
            return isReduceMotionEnabled
        case .voiceOver:
            return isVoiceOverEnabled
        }
    }
}
