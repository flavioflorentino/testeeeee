import Foundation

public extension Notification.Name {
    static let accessibilityFeatureStatusDidChange = Notification.Name("AccessibilityFeatureStatusDidChange")
}
