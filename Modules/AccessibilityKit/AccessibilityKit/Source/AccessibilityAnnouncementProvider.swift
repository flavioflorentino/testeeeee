import Foundation
import UIKit

public protocol AccessibilityAnnouncementProviding {
    func post(announcement: AccessibilityAnnouncementType, string: String)
}

public final class AccessibilityAnnouncementProvider: AccessibilityAnnouncementProviding {
    private var lastAccessibilityAnnouncement = String()
    private var accessibilityAnnouncements = [AccessibilityAnnouncement]()
    
    public init() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didFinishNotification(_:)),
            name: UIAccessibility.announcementDidFinishNotification,
            object: nil
        )
    }
    
    public func post(announcement: AccessibilityAnnouncementType, string: String) {
        guard accessibilityAnnouncements.isEmpty else {
            return accessibilityAnnouncements.append(AccessibilityAnnouncement(type: announcement, value: string))
        }
        
        lastAccessibilityAnnouncement = string
        UIAccessibility.post(notification: announcement.announcementType, argument: string)
    }
    
    @objc
    private func didFinishNotification(_ sender: Notification) {
        guard
            let announcementStringValue = sender.userInfo?[UIAccessibility.announcementStringValueUserInfoKey] as? String,
            let nextAnnouncement = accessibilityAnnouncements.first,
            announcementStringValue == lastAccessibilityAnnouncement
        else {
            return
        }
        
        lastAccessibilityAnnouncement = nextAnnouncement.value
        UIAccessibility.post(notification: nextAnnouncement.type.announcementType, argument: nextAnnouncement.value)
        accessibilityAnnouncements.removeFirst()
    }
}
