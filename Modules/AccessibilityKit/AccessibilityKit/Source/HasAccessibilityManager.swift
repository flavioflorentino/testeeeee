import Foundation

public protocol HasAccessibilityManager {
    var accessibilityManager: AccessibilityManagerContract { get }
}
