import Foundation

public enum AccessibilityAnnouncementType {
    case announcement
    case layoutChanged
    case pageScrolled
    case pauseAssistiveTechnology
    case resumeAssistiveTechnology
    case screenChanged
    
    internal var announcementType: UIAccessibility.Notification {
        switch self {
        case .announcement:
            return .announcement
        case .layoutChanged:
            return .layoutChanged
        case .pageScrolled:
            return .pageScrolled
        case .pauseAssistiveTechnology:
            return .pauseAssistiveTechnology
        case .resumeAssistiveTechnology:
            return .resumeAssistiveTechnology
        case .screenChanged:
            return .screenChanged
        }
    }
}

struct AccessibilityAnnouncement {
    let type: AccessibilityAnnouncementType
    let value: String
}
