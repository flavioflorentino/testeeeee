import Foundation

public enum AccessibilityFeatureType: String, CaseIterable {
    case assistiveTouch = "ASSISTIVE_TOUCH"
    case darkerSystemColors = "DARKER_SYSTEM_COLORS"
    case guidedAccess = "GUIDED_ACCESS"
    case hearingDevice = "HEARING_DEVICE"
    case onOffSwitchLabels = "ON_OFF_SWITCH_LABELS"
    case shakeToUndo = "SHAKE_TO_UNDO"
    case speakScreen = "SPEAK_SCREEN"
    case speakSelection = "SPEAK_SELECTION"
    case closedCaptioning = "CLOSED_CAPTIONING"
    case grayscale = "GRAYSCALE"
    case monoAudio = "MONO_AUDIO"
    case videoAutoplay = "VIDEO_AUTOPLAY"
    case largerText = "LARGER_TEXT"
    case differentiateWithoutColor = "DIFFERENTIATE_WITHOUT_COLOR"
    case invertColors = "INVERT_COLORS"
    case reduceTransparency = "REDUCE_TRANSPARENCY"
    case switchControl = "SWITCH_CONTROL"
    case boldText = "BOLD_TEXT"
    case reduceMotion = "REDUCE_MOTION"
    case voiceOver = "VOICE_OVER"
}

public struct AccessibilityFeature: Equatable {
    public let type: AccessibilityFeatureType
    public let status: String
}
