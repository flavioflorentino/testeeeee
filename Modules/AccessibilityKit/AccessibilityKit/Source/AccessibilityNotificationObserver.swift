import Foundation

public protocol AccessibilityNotificationObserving {
    func configureFeatureObservers(from featureTypes: [AccessibilityFeatureType])
    func postNotification(from featureTypes: AccessibilityFeatureType, status: String)
}

// swiftlint:disable:next type_body_length
public final class AccessibilityNotificationObserver: AccessibilityNotificationObserving {
    private let featureProvider: AccessibilityFeatureProvidering
    private let notificationCenter: NotificationCenter
    
    public init(
        featureProvider: AccessibilityFeatureProvidering,
        notificationCenter: NotificationCenter = NotificationCenter.default
    ) {
        self.featureProvider = featureProvider
        self.notificationCenter = notificationCenter
    }
    
    public func configureFeatureObservers(from featureTypes: [AccessibilityFeatureType]) {
        configureObservers(for: featureTypes)
    }
    
    public func postNotification(from featureTypes: AccessibilityFeatureType, status: String) {
        let featureStatus = AccessibilityFeature(type: featureTypes, status: status)
        notificationCenter.post(name: .accessibilityFeatureStatusDidChange, object: featureStatus)
    }
    
    // MARK: - Private Methods
    // swiftlint:disable:next cyclomatic_complexity function_body_length
    private func configureObservers(for featuresTypes: [AccessibilityFeatureType]) {
        if featuresTypes.contains(.assistiveTouch) {
            notificationCenter.addObserver(
                self,
                selector: #selector(assistiveTouchStatusChanged),
                name: UIAccessibility.assistiveTouchStatusDidChangeNotification,
                object: nil
            )
        }
        
        if featuresTypes.contains(.darkerSystemColors) {
            notificationCenter.addObserver(
                self,
                selector: #selector(darkerSystemColorsStatusChanged),
                name: UIAccessibility.darkerSystemColorsStatusDidChangeNotification,
                object: nil
            )
        }
        
        if featuresTypes.contains(.differentiateWithoutColor) {
            if #available(iOS 13.0, *) {
                notificationCenter.addObserver(
                    self,
                    selector: #selector(differentiateWithoutColorStatusChanged),
                    name: Notification.Name(UIAccessibility.differentiateWithoutColorDidChangeNotification),
                    object: nil
                )
            }
        }
        
        if featuresTypes.contains(.guidedAccess) {
            notificationCenter.addObserver(
                self,
                selector: #selector(guidedAccessStatusChanged),
                name: UIAccessibility.guidedAccessStatusDidChangeNotification,
                object: nil
            )
        }
        
        if featuresTypes.contains(.hearingDevice) {
            notificationCenter.addObserver(
                self,
                selector: #selector(guidedAccessStatusChanged),
                name: UIAccessibility.hearingDevicePairedEarDidChangeNotification,
                object: nil
            )
        }
        
        if featuresTypes.contains(.largerText) {
            notificationCenter.addObserver(
                self,
                selector: #selector(largerTextStatusChanged),
                name: UIContentSizeCategory.didChangeNotification,
                object: nil
            )
        }
        
        if featuresTypes.contains(.onOffSwitchLabels) {
            if #available(iOS 13.0, *) {
                notificationCenter.addObserver(
                    self,
                    selector: #selector(onOffSwitchLabelsStatusChanged),
                    name: UIAccessibility.onOffSwitchLabelsDidChangeNotification,
                    object: nil
                )
            }
        }
        
        if featuresTypes.contains(.shakeToUndo) {
            notificationCenter.addObserver(
                self,
                selector: #selector(shakeToUndoStatusChanged),
                name: UIAccessibility.shakeToUndoDidChangeNotification,
                object: nil
            )
        }
        
        if featuresTypes.contains(.speakScreen) {
            notificationCenter.addObserver(
                self,
                selector: #selector(speakScreenStatusChanged),
                name: UIAccessibility.speakScreenStatusDidChangeNotification,
                object: nil
            )
        }
        
        if featuresTypes.contains(.speakSelection) {
            notificationCenter.addObserver(
                self,
                selector: #selector(speakSelectionStatusChanged),
                name: UIAccessibility.speakSelectionStatusDidChangeNotification,
                object: nil
            )
        }

        if featuresTypes.contains(.boldText) {
            notificationCenter.addObserver(
                self,
                selector: #selector(boldTextStatusChanged),
                name: UIAccessibility.boldTextStatusDidChangeNotification,
                object: nil
            )
        }
        
        if featuresTypes.contains(.closedCaptioning) {
            notificationCenter.addObserver(
                self,
                selector: #selector(closedCaptioningStatusChanged),
                name: UIAccessibility.closedCaptioningStatusDidChangeNotification,
                object: nil
            )
        }
        
        if featuresTypes.contains(.grayscale) {
            notificationCenter.addObserver(
                self,
                selector: #selector(grayscaleStatusChanged),
                name: UIAccessibility.grayscaleStatusDidChangeNotification,
                object: nil
            )
        }
        
        if featuresTypes.contains(.invertColors) {
            notificationCenter.addObserver(
                self,
                selector: #selector(invertColorsStatusChanged),
                name: UIAccessibility.invertColorsStatusDidChangeNotification,
                object: nil
            )
        }
        
        if featuresTypes.contains(.monoAudio) {
            notificationCenter.addObserver(
                self,
                selector: #selector(monoAudioStatusChanged),
                name: UIAccessibility.monoAudioStatusDidChangeNotification,
                object: nil
            )
        }
        
        if featuresTypes.contains(.switchControl) {
            notificationCenter.addObserver(
                self,
                selector: #selector(switchControlStatusChanged),
                name: UIAccessibility.switchControlStatusDidChangeNotification,
                object: nil
            )
        }
        
        if featuresTypes.contains(.reduceMotion) {
            notificationCenter.addObserver(
                self,
                selector: #selector(reduceMotionStatusChanged),
                name: UIAccessibility.reduceMotionStatusDidChangeNotification,
                object: nil
            )
        }
        
        if featuresTypes.contains(.reduceTransparency) {
            notificationCenter.addObserver(
                self,
                selector: #selector(reduceTransparencyStatusChanged),
                name: UIAccessibility.reduceTransparencyStatusDidChangeNotification,
                object: nil
            )
        }
        
        if featuresTypes.contains(.videoAutoplay) {
            if #available(iOS 13.0, *) {
                notificationCenter.addObserver(
                    self,
                    selector: #selector(videoAutoplayStatusChanged),
                    name: UIAccessibility.videoAutoplayStatusDidChangeNotification,
                    object: nil
                )
            }
        }
        
        if featuresTypes.contains(.voiceOver) {
            guard #available(iOS 11.0, *) else {
                return notificationCenter.addObserver(
                    self,
                    selector: #selector(voiceOverStatusChanged),
                    name: Notification.Name(UIAccessibilityVoiceOverStatusChanged),
                    object: nil
                )
            }
            
            notificationCenter.addObserver(
                self,
                selector: #selector(voiceOverStatusChanged),
                name: UIAccessibility.voiceOverStatusDidChangeNotification,
                object: nil
            )
        }
    }
    
    // MARK: - Private Methods
    @objc
    private func assistiveTouchStatusChanged() {
        let statusString = convertToStatusString(from: featureProvider.isAssistiveTouchEnabled)
        postNotification(from: .assistiveTouch, status: statusString)
    }
    
    @objc
    private func darkerSystemColorsStatusChanged() {
        let statusString = convertToStatusString(from: featureProvider.isDarkerSystemColorsEnabled)
        postNotification(from: .darkerSystemColors, status: statusString)
    }
    
    @objc
    private func differentiateWithoutColorStatusChanged() {
        let statusString = convertToStatusString(from: featureProvider.isDifferentiateWithoutColorEnabled)
        postNotification(from: .differentiateWithoutColor, status: statusString)
    }
    
    @objc
    private func guidedAccessStatusChanged() {
        let statusString = convertToStatusString(from: featureProvider.isGuidedAccessEnabled)
        postNotification(from: .guidedAccess, status: statusString)
    }
    
    @objc
    private func hearingDeviceStatusChanged() {
        let isHearingDevicePairedEarEnabled = featureProvider.hearingDevicePairedEar == .both ||
            featureProvider.hearingDevicePairedEar == .left ||
            featureProvider.hearingDevicePairedEar == .right
        
        let statusString = convertToStatusString(from: isHearingDevicePairedEarEnabled)
        
        postNotification(from: .hearingDevice, status: statusString)
    }
    
    @objc
    private func largerTextStatusChanged() {
        postNotification(from: .largerText, status: featureProvider.largerTextCatagory.rawValue)
    }
    
    @objc
    private func onOffSwitchLabelsStatusChanged() {
        let statusString = convertToStatusString(from: featureProvider.isOnOffSwitchLabelsEnabled)
        postNotification(from: .onOffSwitchLabels, status: statusString)
    }
    
    @objc
    private func shakeToUndoStatusChanged() {
        let statusString = convertToStatusString(from: featureProvider.isShakeToUndoEnabled)
        postNotification(from: .shakeToUndo, status: statusString)
    }
    
    @objc
    private func speakScreenStatusChanged() {
        let statusString = convertToStatusString(from: featureProvider.isSpeakScreenEnabled)
        postNotification(from: .speakScreen, status: statusString)
    }
    
    @objc
    private func speakSelectionStatusChanged() {
        let statusString = convertToStatusString(from: featureProvider.isSpeakSelectionEnabled)
        postNotification(from: .speakSelection, status: statusString)
    }
    
    @objc
    private func boldTextStatusChanged() {
        let statusString = convertToStatusString(from: featureProvider.isBoldTextEnabled)
        postNotification(from: .boldText, status: statusString)
    }
    
    @objc
    private func closedCaptioningStatusChanged() {
        let statusString = convertToStatusString(from: featureProvider.isClosedCaptioningEnabled)
        postNotification(from: .closedCaptioning, status: statusString)
    }
    
    @objc
    private func grayscaleStatusChanged() {
        let statusString = convertToStatusString(from: featureProvider.isGrayscaleEnabled)
        postNotification(from: .grayscale, status: statusString)
    }
    
    @objc
    private func invertColorsStatusChanged() {
        let statusString = convertToStatusString(from: featureProvider.isInvertColorsEnabled)
        postNotification(from: .invertColors, status: statusString)
    }
    
    @objc
    private func monoAudioStatusChanged() {
        let statusString = convertToStatusString(from: featureProvider.isMonoAudioEnabled)
        postNotification(from: .monoAudio, status: statusString)
    }
    
    @objc
    private func reduceTransparencyStatusChanged() {
        let statusString = convertToStatusString(from: featureProvider.isReduceTransparencyEnabled)
        postNotification(from: .reduceTransparency, status: statusString)
    }
    
    @objc
    private func videoAutoplayStatusChanged() {
        let statusString = convertToStatusString(from: featureProvider.isVideoAutoplayEnabled)
        postNotification(from: .videoAutoplay, status: statusString)
    }
    
    @objc
    private func switchControlStatusChanged() {
        let statusString = convertToStatusString(from: featureProvider.isSwitchControlEnabled)
        postNotification(from: .switchControl, status: statusString)
    }
    
    @objc
    private func reduceMotionStatusChanged() {
        let statusString = convertToStatusString(from: featureProvider.isReduceMotionEnabled)
        postNotification(from: .reduceMotion, status: statusString)
    }
    
    @objc
    private func voiceOverStatusChanged() {
        let statusString = convertToStatusString(from: featureProvider.isVoiceOverEnabled)
        postNotification(from: .voiceOver, status: statusString)
    }
    
    private func convertToStatusString(from value: Bool) -> String {
        value ? "ENABLED" : "DISABLED"
    }
}
