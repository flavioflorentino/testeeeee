import Foundation

// swiftlint:disable convenience_type
final class AccessibilityKitResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: AccessibilityKitResources.self).url(forResource: "AccessibilityKitResources", withExtension: "bundle") else {
            return Bundle(for: AccessibilityKitResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: AccessibilityKitResources.self)
    }()
}
