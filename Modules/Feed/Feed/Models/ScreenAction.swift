import Foundation

enum ScreenType: String, Decodable {
    case transactionDetail = "TRANSACTION_DETAIL"
    case abuseReportDetail = "ABUSE_REPORT_DETAIL"
    case abuseReportSuccess = "ABUSE_REPORT_SUCCESS"
}

struct ScreenAction: Decodable, Equatable {
    let type: ScreenType
}
