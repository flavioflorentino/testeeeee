import Foundation

enum ActionType: String, Decodable, Equatable {
    case deeplink = "DEEPLINK"
    case screen = "SCREEN"
    case options = "OPTIONS"
}

struct Action: Decodable, Equatable {
    let type: ActionType
    let data: DataActionUnion
}

enum DataActionUnion: Decodable, Equatable {
    case deeplink(DeeplinkAction)
    case screen(ScreenAction)
    case options(OptionsAction)
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        
        if let value = try? container.decode(DeeplinkAction.self) {
            self = .deeplink(value)
            return
        }
        
        if let value = try? container.decode(ScreenAction.self) {
            self = .screen(value)
            return
        }
        
        if let value = try? container.decode(OptionsAction.self) {
            self = .options(value)
            return
        }
        
        throw DecodingError.typeMismatch(
            DataActionUnion.self,
            DecodingError.Context(
                codingPath: decoder.codingPath,
                debugDescription: "Wrong type for ActionUnion"
            )
        )
    }
}
