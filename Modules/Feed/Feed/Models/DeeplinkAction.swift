import Foundation

struct DeeplinkAction: Decodable, Equatable {
    let url: URL
}
