import Foundation

enum ImageShape: String, Decodable, Equatable {
    case circle = "CIRCLE"
    case square = "SQUARE"
}

enum ImageType: String, Decodable, Equatable {
    case consumer = "CONSUMER"
    case seller = "SELLER"
    case action = "ACTION"
}

struct ImageObject: Decodable, Equatable {
    let shape: ImageShape
    let type: ImageType
    let imageURL: URL
    let action: Action?

    private enum CodingKeys: String, CodingKey {
        case shape
        case type
        case imageURL = "imageUrl"
        case action
    }
}
