import Foundation

struct OptionsAction: Decodable, Equatable {
    let title: String
    let message: String
    let actions: [Abuse]
}
