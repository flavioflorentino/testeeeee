import Foundation

enum OptionType: String, Decodable, Equatable {
    case returnPaymentP2P = "RETURN_P2P_PAYMENT"
    case returnPaymentPix = "RETURN_PIX_PAYMENT"
    case makePrivate = "MAKE_PRIVATE"
    case enableNotification = "ENABLE_NOTIFICATIONS"
    case disableNotification = "DISABLE_NOTIFICATIONS"
    case hideTransaction = "HIDE_TRANSACTION"
    case reportContent = "REPORT_CONTENT"
    case viewReceiptP2P = "VIEW_P2P_RECEIPT"
    case viewReceiptBiz = "VIEW_BIZ_RECEIPT"
    case viewReceiptPixTransaction = "VIEW_PIX_TRANSACTION_RECEIPT"
    case viewReceiptPixRefund = "VIEW_PIX_REFUND_RECEIPT"
    case viewReceiptStore = "VIEW_STORE_RECEIPT"
}

struct Resource: Decodable, Equatable {
    let id: String
    let type: String
}

struct ItemDetail: Decodable, Equatable {
    let id: UUID
    let resource: Resource
    let actionLog: ActionLog?
    let userTestimonial: UserTestimonial?
    let postDetail: PostDetail?
    let like: Like?
    let options: [OptionType]
    let comments: [Comment]
    
    private enum CodingKeys: String, CodingKey {
        case id, resource, actionLog, userTestimonial, postDetail, like, options, comments
    }
    
    init(
        id: UUID,
        resource: Resource,
        actionLog: ActionLog?,
        userTestimonial: UserTestimonial?,
        postDetail: PostDetail?,
        like: Like?,
        options: [OptionType],
        comments: [Comment]
    ) {
        self.id = id
        self.resource = resource
        self.actionLog = actionLog
        self.userTestimonial = userTestimonial
        self.postDetail = postDetail
        self.like = like
        self.options = options
        self.comments = comments
    }
}
