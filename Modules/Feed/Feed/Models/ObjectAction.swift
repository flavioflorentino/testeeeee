import Foundation

struct ObjectAction: Decodable, Equatable {
    let text: String
    let action: Action
}
