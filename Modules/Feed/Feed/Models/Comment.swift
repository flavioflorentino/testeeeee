import Foundation

struct Comment: Decodable, Equatable {
    let image: ImageObject
    let username: String
    let text: String
    let date: Date
}
