import Foundation

enum ItemType: String, Decodable, Equatable {
    case transactional = "TRANSACTIONAL"
    case informational = "INFORMATIONAL"
    case carousel = "CAROUSEL"
}

struct Item: Decodable, Equatable {
    let id: UUID
    let type: ItemType
    let action: Action?
    let actionLog: ActionLog?
    let userTestimonial: UserTestimonial?
    let additionalInfo: AdditionalInfo?
    let postDetail: PostDetail?
    let socialAction: SocialAction?
    let banners: [Banner]
    
    private enum CodingKeys: String, CodingKey {
        case id, type, action, actionLog, userTestimonial, additionalInfo, postDetail, socialAction, banners
    }
    
    init(
        id: UUID,
        type: ItemType,
        banners: [Banner],
        action: Action? = nil,
        actionLog: ActionLog? = nil,
        userTestimonial: UserTestimonial? = nil,
        additionalInfo: AdditionalInfo? = nil,
        postDetail: PostDetail? = nil,
        socialAction: SocialAction? = nil
    ) {
        self.id = id
        self.type = type
        self.action = action
        self.actionLog = actionLog
        self.userTestimonial = userTestimonial
        self.additionalInfo = additionalInfo
        self.postDetail = postDetail
        self.socialAction = socialAction
        self.banners = banners
    }
}
