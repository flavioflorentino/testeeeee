import Foundation

struct Attribute: Decodable, Equatable {
    let startIndex: Int
    let endIndex: Int
    let action: Action?
}
