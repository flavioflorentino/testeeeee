import Foundation

struct ActionLog: Decodable, Equatable {
    let images: [ImageObject]
    let text: TextObject
}
