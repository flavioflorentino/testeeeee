import Foundation

struct SocialAction: Decodable, Equatable {
    let likeCount: Int
    let commentCount: Int
    let liked: Bool
    let commented: Bool
}
