import Foundation

enum AdditionalStatus: String, Decodable, Equatable {
    case success = "SUCCESS"
    case informative = "INFORMATIVE"
    case warning = "WARNING"
    case error = "ERROR"
}

enum AdditionalType: String, Decodable, Equatable {
    case bill = "BILL"
    case checkCircle = "CHECK_CIRCLE"
    case leftCircle = "LEFT_CIRCLE"
    case exclamation = "EXCLAMATION"
    case money = "MONEY"
    case negativeCircle = "NEGATIVE_CIRCLE"
    case clock = "CLOCK"
    case calendar = "CALENDAR"
}

struct AdditionalInfo: Decodable, Equatable {
    let status: AdditionalStatus
    let type: AdditionalType
    let text: TextObject
    let primaryAction: ObjectAction?
    let secondaryAction: ObjectAction?
    let action: Action?
}
