import Foundation

struct TextObject: Decodable, Equatable {
    let value: String
    let attributes: [Attribute]
    
    private enum CodingKeys: String, CodingKey {
        case value, attributes
    }
    
    init(value: String, attributes: [Attribute]) {
        self.value = value
        self.attributes = attributes
    }
}
