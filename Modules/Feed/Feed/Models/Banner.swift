import Foundation

struct Banner: Decodable, Equatable {
    let id: UUID
    let text: String
    let description: String?
    let imageURL: URL
    let action: Action
    let primaryAction: ObjectAction?

    private enum CodingKeys: String, CodingKey {
        case id, text, description, action, primaryAction
        case imageURL = "imageUrl"
    }
}
