import Foundation

struct AbuseData: Decodable, Equatable {
    let title: String
    let message: String
    let data: [Abuse]
}

struct Abuse: Decodable, Equatable {
    let id: UUID
    let text: String
    let action: Action
}
