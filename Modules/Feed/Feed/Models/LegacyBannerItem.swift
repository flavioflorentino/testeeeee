import Foundation

struct LegacyBannerItem: Decodable, Equatable {
    let id: String
    let bannerTitle: String
    let description: String?
    let action: String
    let image: URL
    let deeplink: URL
}
