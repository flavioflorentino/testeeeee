import Foundation

struct Like: Decodable, Equatable {
    let liked: Bool
    let latestLikes: [String]
    let total: Int
    
    private enum CodingKeys: String, CodingKey {
        case liked, latestLikes, total
    }
    
    init(liked: Bool, latestLikes: [String], total: Int) {
        self.liked = liked
        self.latestLikes = latestLikes
        self.total = total
    }
}
