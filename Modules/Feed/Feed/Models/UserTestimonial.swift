import Foundation

struct UserTestimonial: Decodable, Equatable {
    let text: TextObject
}
