import Foundation

enum PrivacyType: String, Decodable, Equatable {
    case `public` = "PUBLIC"
    case `private` = "PRIVATE"
}

struct PostDetail: Decodable, Equatable {
    let privacy: PrivacyType
    let date: Date
    let currency: Currency
}
