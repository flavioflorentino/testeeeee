import Foundation

struct ItemData: Decodable, Equatable {
    let lastItem: Int
    let data: [Item]
}
