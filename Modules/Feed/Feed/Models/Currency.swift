import Foundation
import UI

enum CurrencyType: String, Decodable, Equatable {
    case neutral = "NEUTRAL"
    case debit = "DEBIT"
    case credit = "CREDIT"

    var color: UIColor {
        switch self {
        case .debit:
            return Colors.notification600.color
        case .credit:
            return Colors.success600.color
        case .neutral:
            return Colors.grayscale400.color
        }
    }
}

struct Currency: Decodable, Equatable {
    let type: CurrencyType
    let value: String
}
