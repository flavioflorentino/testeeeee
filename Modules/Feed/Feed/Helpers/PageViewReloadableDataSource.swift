import UIKit

class PageViewReloadableDataSource<ViewController: UIPageViewController>: NSObject {
    private(set) var data: [UIViewController] = [] {
        didSet {
            guard !data.isEmpty else { return }
            viewController?.setViewControllers([data[0]], direction: .forward, animated: true)
            (viewController?.view.subviews.first as? UIScrollView)?.isScrollEnabled = data.count > 1
        }
    }
    
    weak var viewController: ViewController?
    
    init(viewController: ViewController) {
        super.init()
        self.viewController = viewController
    }
    
    func add(viewControllers: [UIViewController]) {
        var currentViewControllers = data
        currentViewControllers.append(contentsOf: viewControllers)
        data = currentViewControllers
    }
    
    func update(from index: Int, viewController: UIViewController) {
        guard !data.isEmpty, index <= data.count - 1 else { return }
        data[index] = viewController
    }
    
    func update(viewControllers: [UIViewController]) {
        data = viewControllers
    }
    
    func remove(at index: Int) {
        guard !data.isEmpty, index <= data.count - 1 else { return }
        data.remove(at: index)
    }
    
    func remove(at viewController: UIViewController) {
        guard let index = data.firstIndex(of: viewController) else { return }
        data.remove(at: index)
    }
}
