import Foundation
import UIKit

final class TableViewDelegate: NSObject, UITableViewDelegate {
    enum ActionStyle {
        case normal
        case destructive
        
        var tableViewRowActionStyle: UITableViewRowAction.Style {
            switch self {
            case .normal:
                return .normal
            case .destructive:
                return .destructive
            }
        }
        
        @available(iOS 11.0, *)
        var contextualActionStyle: UIContextualAction.Style {
            switch self {
            case .normal:
                return .normal
            case .destructive:
                return .destructive
            }
        }
    }
    
    typealias EditActionConfiguration = (style: ActionStyle, title: String?, image: UIImage?, completion: (() -> Void)?)
    typealias EditActionsProvider = (_ tableView: UITableView, _ indexPath: IndexPath) -> [EditActionConfiguration]?
    
    var editActionsProvider: EditActionsProvider?
    
    // swiftlint:disable:next discouraged_optional_collection
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        guard let actions = editActionsProvider?(tableView, indexPath) else { return nil }
        
        let rowActions = actions.map { action -> UITableViewRowAction in
            let rowAction = UITableViewRowAction(style: action.style.tableViewRowActionStyle, title: action.title) { _, _ in
                action.completion?()
                tableView.reloadData()
            }
            
            if let image = action.image {
                rowAction.backgroundColor = UIColor(patternImage: image)
            }
            
            return rowAction
        }
        
        return rowActions
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        guard let actions = editActionsProvider?(tableView, indexPath) else { return nil }
        
        let contextualActions = actions.map { action -> UIContextualAction in
            let contextualAction = UIContextualAction(style: action.style.contextualActionStyle, title: action.title) { _, _, _ in
                action.completion?()
                tableView.reloadData()
            }
            
            contextualAction.image = action.image
            
            return contextualAction
        }
        
        return UISwipeActionsConfiguration(actions: contextualActions)
    }
}
