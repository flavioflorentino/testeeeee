import Foundation
import UIKit

final class PageViewDataSource<Item: UIViewController>: PageViewPaginableDataSource<UIPageViewController>, UIPageViewControllerDataSource {
    func pageViewController(
        _ pageViewController: UIPageViewController,
        viewControllerBefore viewController: UIViewController
    ) -> UIViewController? {
        guard let index = data.firstIndex(of: viewController), data.indices.contains(index - 1) else {
            return nil
        }
        
        let beforeIndex = data.index(before: index)
        return data[beforeIndex]
    }
    
    func pageViewController(
        _ pageViewController: UIPageViewController,
        viewControllerAfter viewController: UIViewController
    ) -> UIViewController? {
        guard let index = data.firstIndex(of: viewController), data.indices.contains(index + 1) else {
            return nil
        }
        
        let afterIndex = data.index(after: index)
        return data[afterIndex]
    }
}
