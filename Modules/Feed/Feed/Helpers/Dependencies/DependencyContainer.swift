import AnalyticsModule
import Core
import FeatureFlag

protocol HasNoDependency { }

typealias ModuleDependencies =
    HasNoDependency &
    HasMainQueue &
    HasAnalytics &
    HasCurrentDate &
    HasTimeZone &
    HasFeatureManager &
    HasNotificationCenter &
    HasAuthenticationManager &
    HasReceiptManager &
    HasDeeplinkManager &
    HasConsumerManager

final class DependencyContainer: ModuleDependencies {
    lazy var mainQueue = DispatchQueue.main
    lazy var analytics: AnalyticsProtocol = Analytics.shared
    lazy var currentDate = Date()
    lazy var timeZone = TimeZone.current
    lazy var featureManager: FeatureManagerContract = FeatureManager.shared
    lazy var notificationCenter = NotificationCenter.default
    lazy var authenticationManager: AuthenticationContract = AuthenticationManager.shared
    lazy var receiptManager: ReceiptContract = ReceiptManager.shared
    lazy var deeplinkManager: DeeplinkContract = DeeplinkManager.shared
    lazy var consumerManager: FeedConsumerContract = ConsumerManager.shared
}
