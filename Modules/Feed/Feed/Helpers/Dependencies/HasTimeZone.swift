import Foundation

protocol HasTimeZone {
    var timeZone: TimeZone { get }
}
