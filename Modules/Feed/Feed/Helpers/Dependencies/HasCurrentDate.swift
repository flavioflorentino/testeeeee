import Foundation

protocol HasCurrentDate {
    var currentDate: Date { get }
}
