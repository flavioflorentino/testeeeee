import Foundation

protocol HasNotificationCenter {
    var notificationCenter: NotificationCenter { get }
}
