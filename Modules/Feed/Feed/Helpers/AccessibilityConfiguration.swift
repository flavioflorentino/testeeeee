import Foundation
import UIKit

protocol AccessibilityConfiguration {
    func setupAccessibility()
    func buildAccessibilityElements()
    func configureAccessibilityAttributes()
}

extension AccessibilityConfiguration {
    func setupAccessibility() {
        buildAccessibilityElements()
        configureAccessibilityAttributes()
    }
    
    func buildAccessibilityElements() { }
    func configureAccessibilityAttributes() { }
}

protocol AccessibilityConfigurationDelegate: AnyObject {
    func accessibilityCustomAction(_ customAction: UIAccessibilityCustomAction)
}
