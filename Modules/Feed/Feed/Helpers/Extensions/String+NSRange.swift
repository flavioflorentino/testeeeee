import Foundation

extension String {
    subscript(value: NSRange) -> Substring {
        self[value.lowerBound..<value.upperBound]
    }

    subscript(value: CountableRange<Int>) -> Substring {
        self[index(at: value.lowerBound)..<index(at: value.upperBound)]
    }
    
    private func index(at offset: Int) -> String.Index {
        index(startIndex, offsetBy: offset)
    }
}
