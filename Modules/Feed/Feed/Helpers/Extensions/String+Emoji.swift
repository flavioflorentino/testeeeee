import Foundation

extension String {    
    var containsOnlyEmoji: Bool {
        !isEmpty && !contains { !$0.isEmoji }
    }
    
    var emojis: [Character] {
        filter { $0.isEmoji }
    }
}
