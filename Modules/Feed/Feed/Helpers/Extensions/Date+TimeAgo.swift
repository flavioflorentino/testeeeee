import Foundation

extension Date {
    func timeAgoSince(
        _ date: Date,
        dateFormat: String,
        timeZone: TimeZone = .current,
        locale: Locale = .init(identifier: "pt_BR")
    ) -> String {
        let calender = Calendar(identifier: .iso8601)
        let unitFlags = Set<Calendar.Component>([.second, .minute, .hour, .day, .weekOfYear, .month, .year])
        
        let earliest = earlierDate(date)
        let latest = (earliest == self) ? date : self

        let component = calender.dateComponents(unitFlags, from: earliest, to: latest)
        
        if let day = component.day, day >= 2 {
            let dateFormatted = dateFormatter(from: self, dateFormat: dateFormat, timeZone: timeZone, locale: locale)
            let hourFormatted = dateFormatter(from: self, dateFormat: "HH:mm", timeZone: timeZone, locale: locale)
            return Strings.otherDate(dateFormatted, hourFormatted)
        }
        
        if let day = component.day, day >= 1 {
            let hourFormatted = dateFormatter(from: self, dateFormat: "HH:mm", timeZone: timeZone, locale: locale)
            return Strings.yesterday(hourFormatted)
        }
        
        if let hour = component.hour, hour >= 2 {
            return Strings.hoursAgo(hour)
        }
        
        if let hour = component.hour, hour >= 1 {
            return Strings.hourAgo(hour)
        }
        
        if let minute = component.minute, minute >= 2 {
            return Strings.minutesAgo(minute)
        }
        
        if let minute = component.minute, minute >= 1 {
            return Strings.minuteAgo(minute)
        }
        
        return Strings.timeAgo
    }
    
    // MARK: - Private Methods
    private func earlierDate(_ date: Date) -> Date {
        (timeIntervalSince1970 <= date.timeIntervalSince1970) ? self : date
    }
    
    private func dateFormatter(
        from date: Date,
        dateFormat: String,
        timeZone: TimeZone = .current,
        locale: Locale = .init(identifier: "pt_BR")
    ) -> String {
        let formatter = DateFormatter()
        formatter.timeZone = timeZone
        formatter.locale = locale
        formatter.dateFormat = dateFormat
        return formatter.string(from: date)
    }
}
