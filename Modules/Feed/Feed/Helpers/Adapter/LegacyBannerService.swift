import Core
import Foundation

protocol LegacyBannerServicing: AnyObject {
    func listBanners(_ completion: @escaping (Result<[LegacyBannerItem], ApiError>) -> Void)
}

final class LegacyBannerService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension LegacyBannerService: LegacyBannerServicing {
    func listBanners(_ completion: @escaping (Result<[LegacyBannerItem], ApiError>) -> Void) {
        let request = Api<[LegacyBannerItem]>(endpoint: LegacyBannerServiceEndpoint.banners)
        
        request.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
