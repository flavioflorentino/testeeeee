import Core
import Foundation

protocol LegacyBannerAdaptering: AnyObject {
    func listBanners(with items: [Item], _ onSuccess: @escaping ([Item]) -> Void)
}

final class LegacyBannerAdapter: LegacyBannerAdaptering {
    private let service: LegacyBannerServicing
    
    init(service: LegacyBannerServicing) {
        self.service = service
    }
    
    func listBanners(with items: [Item], _ completion: @escaping ([Item]) -> Void) {
        service.listBanners { [weak self] result in
            switch result {
            case .success(let banners):
                let mergedItems = self?.mergeItems(from: banners, items: items) ?? []
                completion(mergedItems)
            case .failure:
                completion(items)
            }
        }
    }
    
    // MARK: - Private Methods
    private func mergeItems(from banners: [LegacyBannerItem], items: [Item]) -> [Item] {
        let convertedItems = convertLegacyBannerForItems(banners)
        
        if items.isEmpty, let first = convertedItems.first {
            return [first]
        }
        
        let indexesToInsert = discoveryPositionIndexes(from: convertedItems, at: items)
        var mergedItems = items
        
        zip(convertedItems, indexesToInsert).forEach { item, index in
            mergedItems.insert(item, at: index)
        }
        
        return mergedItems
    }
    
    private func convertLegacyBannerForItems(_ legacyItems: [LegacyBannerItem]) -> [Item] {
        let banners = legacyItems.compactMap {
            Banner(
                id: UUID(uuidString: $0.id) ?? UUID(),
                text: $0.bannerTitle,
                description: $0.description,
                imageURL: $0.image,
                action: Action(
                    type: .deeplink,
                    data: .deeplink(
                        DeeplinkAction(
                            url: $0.deeplink
                        )
                    )
                ),
                primaryAction: ObjectAction(
                    text: $0.action,
                    action: Action(
                        type: .deeplink,
                        data: .deeplink(
                            DeeplinkAction(
                                url: $0.deeplink
                            )
                        )
                    )
                )
            )
        }
        
        return configurationItems(from: banners)
    }
    
    // swiftlint:disable:next function_body_length
    private func configurationItems(from banners: [Banner]) -> [Item] {
        let firstGroupTotalItems = min(4, banners.count)
        let firstGroupItems = Array(banners.prefix(firstGroupTotalItems))
        
        guard banners.count > firstGroupTotalItems else {
            return [
                Item(
                    id: UUID(),
                    type: .carousel,
                    banners: firstGroupItems
                )
            ]
        }

        let middleIndex = firstGroupTotalItems
        
        let secondGroupItems = [banners[middleIndex]]
        let thirdGroupItems = Array(banners.dropFirst(middleIndex + 1))

        return [
            Item(
                id: UUID(),
                type: .carousel,
                banners: firstGroupItems
            ),
            Item(
                id: UUID(),
                type: .informational,
                banners: secondGroupItems
            ),
            Item(
                id: UUID(),
                type: .carousel,
                banners: thirdGroupItems
            )
        ]
    }
    
    private func discoveryPositionIndexes(from additionalItems: [Item], at items: [Item]) -> [Int] {
        var lastIndex = 0

        return (0 ..< additionalItems.count).map {
            guard $0 != 0 else {
                lastIndex = 1
                return lastIndex
            }

            var index = max(lastIndex, min(lastIndex + 5, items.count))
            if lastIndex == index {
                index += 1
            }

            lastIndex = index
            return lastIndex
        }
    }
}
