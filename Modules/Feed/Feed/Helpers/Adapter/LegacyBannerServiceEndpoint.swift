import Core
import Foundation

enum LegacyBannerServiceEndpoint {
    case banners
}

extension LegacyBannerServiceEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .banners:
            return "banners"
        }
    }
}
