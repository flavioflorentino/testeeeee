import Foundation

public typealias Actionable = DeeplinkActionable & ScreenActionable

public protocol DeeplinkActionable: AnyObject {
    func deeplink(_ url: URL)
}

public protocol ScreenActionable: AnyObject {
    func screen(for type: ActionScreenType)
}
