import UI
import UIKit

struct IconographyButtonProperties {
    var title: String?
    var iconName: Iconography
    var color: Colors.Style
}

extension IconographyButton.Layout {
    enum Size {
        static let iconSize = CGSize(width: 24, height: 24)
        static let buttonHeight = Sizing.Button.default.rawValue
    }
}

final class IconographyButton: UIControl {
    fileprivate enum Layout { }
    
    // MARK: Properties
    var properties: IconographyButtonProperties? {
        didSet {
            configureButton()
        }
    }
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
            
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
        return label
    }()

    // MARK: Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension IconographyButton {
    func configureButton() {
        guard let properties = properties else { return }
        
        titleLabel.textColor = properties.color.rawValue
        titleLabel.text = properties.title
        
        let image = UIImage(
            iconName: properties.iconName.rawValue,
            font: Typography.icons(.large).font(),
            color: properties.color.rawValue,
            size: Sizing.ImageView.xSmall.rawValue
        )
        
        imageView.image = image
    }
    
    @objc
    func buttonTapped() {
        sendActions(for: .touchUpInside)
    }
}

extension IconographyButton: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(imageView)
        addSubview(titleLabel)
    }
    
    func setupConstraints() {
        imageView.snp.makeConstraints {
            $0.top.leading.bottom.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.trailing.bottom.equalToSuperview()
            $0.leading.equalTo(imageView.snp.trailing).offset(Spacing.base01)
        }
    }
    
    func configureViews() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(buttonTapped))
        addGestureRecognizer(tapGesture)
    }
}
