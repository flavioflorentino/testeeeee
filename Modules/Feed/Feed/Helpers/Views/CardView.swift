import Foundation
import UI
import UIKit

extension CardView.Layout {
    enum CornerRadius {
        static let `default` = UI.CornerRadius.medium
    }
}

final class CardView: UIView {
    fileprivate enum Layout {}
    
    private var _backgroundColor: UIColor?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateStylesIfNeeded()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)

        guard #available(iOS 13, *), traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) else {
            return
        }
        
        updateStylesIfNeeded()
    }
    
    func setBackgroundColor(_ uiColor: UIColor?) {
        _backgroundColor = uiColor
    }
    
    // MARK: - Private Methods
    private func updateStylesIfNeeded() {
        let bezierPath = UIBezierPath(roundedRect: layer.bounds, cornerRadius: Layout.CornerRadius.default)
        viewStyle(CardViewStyle(cgPath: bezierPath.cgPath))
        
        if _backgroundColor != nil {
            backgroundColor = _backgroundColor
        }
        
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
    }
}
