import Foundation
import UI
import UIKit

protocol InteractiveLabelDelegate: AnyObject {
    func didSelectTextWithinTheRanges(_ text: String)
}

final class InteractiveLabel: UILabel {
    private let layoutManager = NSLayoutManager()
    private let textContainer = NSTextContainer()
    private let textStorage = NSTextStorage()
    
    override var text: String? {
        didSet {
            attributedText = NSAttributedString(string: text ?? String())
        }
    }
    
    // swiftlint:disable:next implicitly_unwrapped_optional
    override var textColor: UIColor! {
        didSet {
            updateAttributedString()
        }
    }
    
    override var attributedText: NSAttributedString? {
        didSet {
            updateAttributedString()
        }
    }
    
    override var lineBreakMode: NSLineBreakMode {
        didSet {
            textContainer.lineBreakMode = lineBreakMode
        }
    }
    
    override var numberOfLines: Int {
        didSet {
            textContainer.maximumNumberOfLines = numberOfLines
        }
    }
    
    override var intrinsicContentSize: CGSize {
        guard let text = text, !text.isEmpty else { return .zero }

        textContainer.size = CGSize(width: preferredMaxLayoutWidth, height: .greatestFiniteMagnitude)
        
        let size = layoutManager.usedRect(for: textContainer)
        return CGSize(width: ceil(size.width), height: ceil(size.height))
    }
    
    var ranges: [NSRange] = [] {
        didSet {
           updateAttributedString()
        }
    }
    
    var attributeFontNormal = Typography.bodyPrimary().font() {
        didSet {
            updateAttributedString()
        }
    }
    
    var attributeFontHighlight = Typography.bodyPrimary(.highlight).font() {
        didSet {
            updateAttributedString()
        }
    }
    
    weak var delegate: InteractiveLabelDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        guard !ranges.isEmpty, checkTouchWasWithinRanges(ranges, in: point) else {
            return false
        }
        
        return true
    }
    
    override func drawText(in rect: CGRect) {
        let range = NSRange(location: 0, length: textStorage.length)
            
        textContainer.size = rect.size
        let newOrigin = textOrigin(inRect: rect)
            
        layoutManager.drawBackground(forGlyphRange: range, at: newOrigin)
        layoutManager.drawGlyphs(forGlyphRange: range, at: newOrigin)
    }

    // MARK: - Private Methods
    @objc
    private func handleAction(_ sender: UITapGestureRecognizer) {
        guard !ranges.isEmpty else { return }
        
        let string = text ?? String()
        
        ranges.lazy.forEach { range in
            if touchInText(from: sender.location(in: self), range: range) {
                let startIndex = string.index(string.startIndex, offsetBy: range.location)
                let endIndex = string.index(string.startIndex, offsetBy: range.location + range.length)
                
                delegate?.didSelectTextWithinTheRanges(String(string[startIndex..<endIndex]))
            }
        }
    }
    
    private func checkTouchWasWithinRanges(_ ranges: [NSRange], in position: CGPoint) -> Bool {
        ranges.contains { touchInText(from: position, range: $0) }
    }
    
    private func setup() {
        isUserInteractionEnabled = true
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(handleAction(_:)))
        addGestureRecognizer(gesture)
        
        textStorage.addLayoutManager(layoutManager)
        layoutManager.addTextContainer(textContainer)
        
        textContainer.lineFragmentPadding = 0.0
    }
    
    private func textOrigin(inRect rect: CGRect) -> CGPoint {
        let usedRect = layoutManager.usedRect(for: textContainer)
        let correction = (rect.height - usedRect.height) / 2
        let glyphOriginY = correction > 0 ? rect.origin.y + correction : rect.origin.y
        return CGPoint(x: rect.origin.x, y: glyphOriginY)
    }
    
    private func updateAttributedString() {
        guard let attributedString = attributedText, attributedString.length > 0 else {
            textStorage.setAttributedString(NSAttributedString())
            setNeedsDisplay()
            return
        }
        
        let mutableAttributedString = NSMutableAttributedString(attributedString: attributedString)
        var range = NSRange(location: 0, length: 0)
        var attributes = mutableAttributedString.attributes(at: 0, effectiveRange: &range)
        
        attributes[.font] = attributeFontNormal
        attributes[.foregroundColor] = textColor
        
        mutableAttributedString.setAttributes(attributes, range: range)
        
        ranges.lazy.forEach {
            mutableAttributedString.addAttribute(
                .font,
                value: attributeFontHighlight,
                range: $0
            )
        }
        
        textStorage.setAttributedString(mutableAttributedString)
        setNeedsDisplay()
    }

    private func touchInText(
        from positionOfTouchInLabel: CGPoint,
        range: NSRange
    ) -> Bool {
        let labelSize = bounds.size
        
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        
        let textContainerOffset = CGPoint(
            x: (labelSize.width - textBoundingBox.size.width) * 0.0 - textBoundingBox.origin.x,
            y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y
        )
        
        let locationOfTouchInTextContainer = CGPoint(
            x: positionOfTouchInLabel.x - textContainerOffset.x,
            y: positionOfTouchInLabel.y - textContainerOffset.y
        )
            
        let indexOfCharacter = layoutManager.characterIndex(
            for: locationOfTouchInTextContainer,
            in: textContainer,
            fractionOfDistanceBetweenInsertionPoints: nil
        )
            
        return NSLocationInRange(indexOfCharacter, range)
    }
}
