import UI
import UIKit
import SnapKit

final class ScrollView<ContentView: UIView>: UIView {
    private let scrollView = UIScrollView()
    private let containerView = UIView()
    private var fixedViewBottomAnchor: Constraint?
    private var scrollViewBottomAnchor: Constraint?
    let visibleAreaLayoutGuide = UILayoutGuide()
    let contentView: ContentView
    var keyboardHandler: KeyboardHandler? {
        didSet {
            listenKeyboardChanges()
        }
    }
    
    init(_ contentView: ContentView, keyboardHandler: KeyboardHandler? = nil) {
        self.contentView = contentView
        self.keyboardHandler = keyboardHandler
        super.init(frame: .zero)
        buildLayout()
        listenKeyboardChanges()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func listenKeyboardChanges() {
        keyboardHandler?.onHeightChangeCompletion = { keyboardHeight in
            self.layoutIfNeeded()
            self.scrollViewBottomAnchor?.deactivate()
            self.scrollView.snp.makeConstraints {
                self.scrollViewBottomAnchor = $0.bottom.equalTo(self.compatibleSafeArea.bottom).inset(keyboardHeight).constraint
            }
            self.layoutIfNeeded()
        }
    }
}

extension ScrollView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(contentView)
        containerView.addLayoutGuide(visibleAreaLayoutGuide)
    }
    
    func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.leading.equalTo(compatibleSafeArea.leading)
            $0.top.equalTo(compatibleSafeArea.top)
            $0.trailing.equalTo(compatibleSafeArea.trailing)
            scrollViewBottomAnchor = $0.bottom.equalTo(compatibleSafeArea.bottom).constraint
        }
        containerView.snp.makeConstraints {
            $0.top.bottom.centerX.width.equalToSuperview()
            $0.height.equalToSuperview().priority(.low)
        }
        contentView.snp.makeConstraints {
            $0.leading.trailing.top.bottom.equalToSuperview()
        }
        visibleAreaLayoutGuide.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.height.equalTo(scrollView)
        }
    }
}
