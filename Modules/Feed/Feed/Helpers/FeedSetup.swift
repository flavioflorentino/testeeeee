import Core

public enum FeedSetup {
    public private(set) static var receiptInstance: ReceiptContract?
    public private(set) static var authenticationInstance: AuthenticationContract?
    public private(set) static var deeplinkInstance: DeeplinkContract?
    public private(set) static var consumerInstance: FeedConsumerContract?
    
    public static func inject(instance: ReceiptContract) {
        receiptInstance = instance
    }
    
    public static func inject(instance: AuthenticationContract) {
        authenticationInstance = instance
    }
    
    public static func inject(instance: DeeplinkContract) {
        deeplinkInstance = instance
    }
    
    public static func inject(instance: FeedConsumerContract) {
        consumerInstance = instance
    }
}

// MARK: - Receipt Contract
protocol HasReceiptManager {
    var receiptManager: ReceiptContract { get }
}

final class ReceiptManager: ReceiptContract {
    private var receiptConfiguration: ReceiptContract? {
        FeedSetup.receiptInstance
    }
    
    static let shared = ReceiptManager()
    
    private init() { }
    
    func open(withId id: String, receiptType: String, feedItemId: String?) {
        receiptConfiguration?.open(withId: id, receiptType: receiptType, feedItemId: feedItemId)
    }
}

// MARK: - Authentication Contract
protocol HasAuthenticationManager {
    var authenticationManager: AuthenticationContract { get }
}

final class AuthenticationManager: AuthenticationContract {
    private var authenticationConfiguration: AuthenticationContract? {
        FeedSetup.authenticationInstance
    }
    
    static let shared = AuthenticationManager()
    
    var isAuthenticated: Bool {
        authenticationConfiguration?.isAuthenticated ?? false
    }
    
    private init() { }
    
    func authenticate(_ completion: @escaping AuthenticationResult) {
        authenticationConfiguration?.authenticate(completion)
    }

    func disableBiometricAuthentication() {
        authenticationConfiguration?.disableBiometricAuthentication()
    }
}

// MARK: - Deeplink Contract
protocol HasDeeplinkManager {
    var deeplinkManager: DeeplinkContract { get }
}

final class DeeplinkManager: DeeplinkContract {
    private var deeplinkConfiguration: DeeplinkContract? {
        FeedSetup.deeplinkInstance
    }
    
    static let shared = DeeplinkManager()
    
    private init() { }
    
    func open(url: URL) -> Bool {
        deeplinkConfiguration?.open(url: url) ?? false
    }
}

// MARK: - Consumer Contract
public protocol FeedConsumerContract {
    var consumerId: Int { get }
    var name: String { get }
    var username: String? { get }
    var imageURL: URL? { get }
}

protocol HasConsumerManager {
    var consumerManager: FeedConsumerContract { get }
}

final class ConsumerManager: FeedConsumerContract {
    private var consumerConfiguration: FeedConsumerContract? {
        FeedSetup.consumerInstance
    }
    
    static let shared = ConsumerManager()
    
    var consumerId: Int {
        consumerConfiguration?.consumerId ?? .zero
    }
    
    var name: String {
        consumerConfiguration?.name ?? String()
    }
    
    var username: String? {
        consumerConfiguration?.username
    }
    
    var imageURL: URL? {
        consumerConfiguration?.imageURL
    }
    
    private init() { }
}
