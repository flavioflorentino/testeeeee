import Foundation
import UIKit

final class KeyboardHandler {
    typealias KeyboardHeightCompletion = (CGFloat) -> Void
    
    var onHeightChangeCompletion: KeyboardHeightCompletion?
    private let notificationCenter: NotificationCenter
    
    init(notificationCenter: NotificationCenter) {
        self.notificationCenter = notificationCenter
        setupNotificationCenter()
    }
    
    private func setupNotificationCenter() {
        notificationCenter.addObserver(self,
                                       selector: #selector(keyboardWillShow(_:)),
                                       name: UIWindow.keyboardWillShowNotification,
                                       object: nil)
        notificationCenter.addObserver(self,
                                       selector: #selector(keyboardWillHide(_:)),
                                       name: UIWindow.keyboardWillHideNotification,
                                       object: nil)
    }
    
    @objc
    private func keyboardWillShow(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo,
              let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
            return
        }
        
        updateKeyboardConstraint(userInfo: userInfo, inset: keyboardFrame.cgRectValue.size.height)
    }
    
    @objc
    private func keyboardWillHide(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo else {
            return
        }
        updateKeyboardConstraint(userInfo: userInfo)
    }
    
    private func updateKeyboardConstraint(userInfo: [AnyHashable: Any], inset: CGFloat = 0.0) {
        guard let animationDuration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber,
              let animationCurve = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber else {
            return
        }
        
        UIView.animate(withDuration: animationDuration.doubleValue,
                       delay: 0,
                       options: UIView.AnimationOptions(rawValue: animationCurve.uintValue),
                       animations: {
                            self.onHeightChangeCompletion?(inset)
                       },
                       completion: nil)
    }
}
