import UI
import UIKit

enum GeometricType {
    case square
    case circle
    
    var cornerRadius: CornerRadius.Style {
        switch self {
        case .square:
            return .light
        case .circle:
            return .full
        }
    }
}

struct GeometricImageStyle: ImageStyle {
    typealias View = UIImageView
    
    private let type: GeometricType
    private let size: CGSize
    
    init(type: GeometricType, size: Sizing.ImageView) {
        self.type = type
        self.size = size.rawValue
    }
    
    func makeStyle(_ style: StyleCore<View>) {
        style.with(\.size, size)
        style.with(\.cornerRadius, type.cornerRadius)
        style.with(\.border, .medium(color: .white()))
        style.with(\.layer.masksToBounds, true)
    }
}
