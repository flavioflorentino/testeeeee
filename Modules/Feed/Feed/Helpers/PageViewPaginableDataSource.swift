import UIKit

class PageViewPaginableDataSource<ViewController: UIPageViewController>: PageViewReloadableDataSource<ViewController> {
    func next(from index: Int) {
        if !data.isEmpty, index <= data.count - 1 {
            viewController?.setViewControllers([data[index]], direction: .forward, animated: true)
        }
    }
    
    func previous(from index: Int) {
        if !data.isEmpty, data.count - 1 >= index {
            viewController?.setViewControllers([data[index]], direction: .reverse, animated: true)
        }
    }
}
