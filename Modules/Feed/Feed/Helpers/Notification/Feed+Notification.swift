import Foundation

extension Notification.Name {
    static let homeScrollToTop = Notification.Name("homeScrollToTop")
}
