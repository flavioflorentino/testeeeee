import AssetsKit

protocol FeedPlaceholderRetriever { }

extension FeedPlaceholderRetriever {
    func placeholderImage(_ type: ImageType) -> UIImage {
        switch type {
        case .consumer:
            return Resources.Placeholders.greenAvatarPlaceholder.image
        case .seller:
            return Resources.Placeholders.sellerAvatarPlaceholder.image
        case .action:
            return Resources.Placeholders.actionAvatarPlaceholder.image
        }
    }
}
