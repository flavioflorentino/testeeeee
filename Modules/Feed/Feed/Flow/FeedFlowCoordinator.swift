import AssetsKit
import UIKit
import UI

public protocol FeedFlowCoordinatorDelegate: AnyObject {
    func openPixRefundFlow(_ transactionId: String)
}

public protocol FeedFlowCoordinating: AnyObject {
    func start(options: FeedOptions, hasRefresh: Bool) -> FeedViewController
    func start(cardId: UUID) -> UIViewController
}

public final class FeedFlowCoordinator: FeedFlowCoordinating {
    private let actionable: ScreenActionable?
    weak var viewController: UIViewController?
    
    public weak var delegate: FeedFlowCoordinatorDelegate?

    public init(actionable: ScreenActionable? = nil) {
        self.actionable = actionable
    }
    
    @discardableResult
    public func start(options: FeedOptions, hasRefresh: Bool) -> FeedViewController {
        let controller = FeedFactory.make(options: options, hasRefresh: hasRefresh, actionable: self)
        viewController = controller
        return controller
    }
    
    public func start(cardId: UUID) -> UIViewController {
        let controller = ItemDetailFactory.make(cardId: cardId, actionable: self, delegate: self)
        controller.hidesBottomBarWhenPushed = true
        return controller
    }
}

extension FeedFlowCoordinator: ScreenActionable {
    public func screen(for type: ActionScreenType) {
        switch type {
        case let .transactionDetail(id):
            let controller = ItemDetailFactory.make(cardId: id, actionable: self, delegate: self)
            controller.hidesBottomBarWhenPushed = true
            
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case let .abuseReportDetail(type, id):
            let controller = AbuseReportDetailFactory.make(reportedType: type, reportedId: id.uuidString)
            let navigationController = UINavigationController(rootViewController: controller)
            
            viewController?.present(navigationController, animated: true)
        }
    }
}

extension FeedFlowCoordinator: ItemDetailDelegate {
    public func openPixRefundFlow(_ transactionId: String) {
        delegate?.openPixRefundFlow(transactionId)
    }
}
