import Foundation

// swiftlint:disable convenience_type
final class FeedResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: FeedResources.self).url(forResource: "FeedResources", withExtension: "bundle") else {
            return Bundle(for: FeedResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: FeedResources.self)
    }()
}
