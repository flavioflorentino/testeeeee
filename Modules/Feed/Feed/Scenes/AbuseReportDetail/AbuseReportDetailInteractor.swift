import Foundation
import Core

protocol AbuseReportDetailInteracting: AnyObject {
    func getNotificationCenter()
    func validateMessage(_ message: String?)
    func sendAbuse(with message: String?)
}

final class AbuseReportDetailInteractor {
    typealias Depedencies = HasNotificationCenter
    
    private let type: AbuseReportType
    private let reportedId: String
    
    private let service: AbuseReportDetailServicing
    private let presenter: AbuseReportDetailPresenting
    private let depedencies: Depedencies

    init(
        type: AbuseReportType,
        reportedId: String,
        service: AbuseReportDetailServicing,
        presenter: AbuseReportDetailPresenting,
        depedencies: Depedencies
    ) {
        self.type = type
        self.reportedId = reportedId
        self.service = service
        self.presenter = presenter
        self.depedencies = depedencies
    }
}

// MARK: - AbuseReportDetailInteracting
extension AbuseReportDetailInteractor: AbuseReportDetailInteracting {
    func getNotificationCenter() {
        presenter.configureScrollView(with: depedencies.notificationCenter)
    }
    
    func validateMessage(_ message: String?) {
        presenter.presentInput(valid: isReportValid(message))
    }
    
    func sendAbuse(with message: String?) {
        guard let message = message, isReportValid(message) else {
            return presenter.presentInput(valid: false)
        }
        
        presenter.switchLoadingState(shouldDisplay: true)
        
        service.sendAbuse(type: type, reportedId: reportedId, message: message) { [weak self] result in
            switch result {
            case .success:
                self?.presenter.sendAbuseSuccessed()
            case .failure:
                self?.presenter.presentReportError()
            }
        }
    }
    
    // MARK: - Private Methods
    private func isReportValid(_ report: String?) -> Bool {
        report?.trimmingCharacters(in: .whitespaces).isEmpty == false
    }
}
