import AssetsKit
import Foundation

protocol AbuseReportDetailPresenting: AnyObject {
    var viewController: AbuseReportDetailDisplaying? { get set }
    func switchLoadingState(shouldDisplay: Bool)
    func presentInput(valid: Bool)
    func presentReportError()
    func sendAbuseSuccessed()
    func configureScrollView(with notificationCenter: NotificationCenter)
}

final class AbuseReportDetailPresenter {
    weak var viewController: AbuseReportDetailDisplaying?
}

// MARK: - AbuseReportDetailPresenting
extension AbuseReportDetailPresenter: AbuseReportDetailPresenting {
    func presentInput(valid: Bool) {
        viewController?.switchButtonState(to: valid)
        
        if !valid {
            viewController?.switchInputStateTo(editing: true)
        }
    }
    
    func presentReportError() {
        viewController?.showError(
            image: Resources.Illustrations.iluError.image,
            title: Strings.AbuseReport.Error.title,
            message: Strings.AbuseReport.Error.message,
            titleButton: Strings.AbuseReport.Error.button
        )
    }
    
    func sendAbuseSuccessed() {
        viewController?.showAbuseSuccess(
            Resources.Icons.icoBigSuccess.image,
            title: Strings.thanksForYourHelp,
            message: Strings.letsReviewYourReport,
            titleButton: Strings.okIGotIt
        )
    }
    
    func switchLoadingState(shouldDisplay: Bool) {
        viewController?.switchInputStateTo(editing: false)
        viewController?.switchLoadingState(shouldDisplay)
    }
    
    func switchButtonStateTo(enabled: Bool) {
        viewController?.switchButtonState(to: enabled)
    }
    
    func configureScrollView(with notificationCenter: NotificationCenter) {
        viewController?.configureScrollView(with: notificationCenter)
    }
}
