import Core
import Foundation

protocol AbuseReportDetailServicing {
    func sendAbuse(
        type: AbuseReportType,
        reportedId: String,
        message: String?,
        _ completion: @escaping (Result<NoContent, ApiError>) -> Void
    )
}

final class AbuseReportDetailService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - AbuseReportDetailServicing
extension AbuseReportDetailService: AbuseReportDetailServicing {
    func sendAbuse(
        type: AbuseReportType,
        reportedId: String,
        message: String?,
        _ completion: @escaping (Result<NoContent, ApiError>) -> Void
    ) {
        let request = Api<NoContent>(
            endpoint: AbuseReportDetailServiceEndpoint.reportAbuse(type: type.rawValue, reportedId: reportedId, message: message)
        )

        request.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
