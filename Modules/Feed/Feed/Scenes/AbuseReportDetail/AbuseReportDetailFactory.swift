import UIKit

public enum AbuseReportType: String {
    case transactionalCard = "SOCIAL_FEED_TRANSACTIONAL_CARD"
    case comment = "SOCIAL_FEED_COMMENT"
}

public enum AbuseReportDetailFactory {
    public static func make(reportedType: AbuseReportType, reportedId: String) -> UIViewController {
        let container = DependencyContainer()
        let service: AbuseReportDetailServicing = AbuseReportDetailService(dependencies: container)
        let presenter: AbuseReportDetailPresenting = AbuseReportDetailPresenter()
        let interactor = AbuseReportDetailInteractor(
            type: reportedType,
            reportedId: reportedId,
            service: service,
            presenter: presenter,
            depedencies: container
        )
        let viewController = AbuseReportDetailViewController(interactor: interactor)

        presenter.viewController = viewController

        return viewController
    }
}
