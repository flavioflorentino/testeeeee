import AssetsKit
import UI
import UIKit
import SnapKit

protocol AbuseReportDetailDisplaying: AnyObject {
    func switchButtonState(to enabled: Bool)
    func switchLoadingState(_ shouldDisplay: Bool)
    func switchInputStateTo(editing: Bool)
    func showError(image: UIImage?, title: String?, message: String?, titleButton: String?)
    func showAbuseSuccess(_ image: UIImage?, title: String?, message: String?, titleButton: String)
    func configureScrollView(with notificationCenter: NotificationCenter)
}

final class AbuseReportDetailViewController: ViewController<AbuseReportDetailInteracting, UIView> {
    typealias Localizable = Strings.AbuseReport.Detail
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.text, Localizable.title)
        return label
    }()
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle(type: .default))
            .with(\.textColor, Colors.grayscale600.color)
        label.text = Localizable.description
        return label
    }()
    private lazy var inputTextField: UIPPFloatingTextField = {
        let inputTextField = UIPPFloatingTextField()
        inputTextField.placeholder = Localizable.placeholder
        inputTextField.delegate = self
        inputTextField.addTarget(self, action: #selector(handleTextFieldChange), for: .editingChanged)
        return inputTextField
    }()
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        return stackView
    }()
    private lazy var sendButton: UIButton = {
        let button = UIButton(type: .custom)
        button.buttonStyle(DangerButtonStyle(icon: (name: .infoCircle, alignment: .left)))
        button.setTitle(Localizable.actionTitle, for: .normal)
        button.addTarget(self, action: #selector(handleSendButtonTap), for: .touchUpInside)
        return button
    }()
    
    private lazy var containerView = UIView()
    private lazy var scrollView = ScrollView(containerView)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.getNotificationCenter()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        containerView.addSubviews(stackView, sendButton)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(descriptionLabel)
        stackView.addArrangedSubview(inputTextField)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        containerView.snp.makeConstraints {
            $0.bottom.greaterThanOrEqualTo(scrollView.visibleAreaLayoutGuide.snp.bottom).inset(Spacing.base02)
        }
        
        stackView.snp.makeConstraints {
            $0.top.equalTo(containerView.compatibleSafeArea.top).inset(Spacing.base05)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.lessThanOrEqualTo(sendButton.snp.top).inset(-Spacing.base01)
        }
        
        sendButton.snp.makeConstraints {
            $0.leading.bottom.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        stackView.setSpacing(Spacing.base01, after: titleLabel)
        stackView.setSpacing(Spacing.base04, after: descriptionLabel)
    }
    
    override func configureViews() {
        title = Localizable.pageTitle
        view.backgroundColor = Colors.backgroundPrimary.color
        
        navigationController?.navigationBar.navigationStyle(LargeNavigationStyle())
        extendedLayoutIncludesOpaqueBars = true
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: Resources.Icons.icoClose.image,
            style: .plain,
            target: self,
            action: #selector(pressCloseButtonItem)
        )
    }
}

@objc
private extension AbuseReportDetailViewController {
    func handleSendButtonTap() {
        interactor.sendAbuse(with: inputTextField.text)
    }
    
    func handleTextFieldChange() {
        interactor.validateMessage(inputTextField.text)
    }
    
    func pressCloseButtonItem() {
        dismiss(animated: true)
    }
}

// MARK: - UITextFieldDelegate
extension AbuseReportDetailViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return false
    }
}

// MARK: - AbuseReportDetailDisplaying
extension AbuseReportDetailViewController: AbuseReportDetailDisplaying {
    func showError(image: UIImage?, title: String?, message: String?, titleButton: String?) {
        endState(
            model: StatefulErrorViewModel(
                image: image,
                content: (title: title, description: message),
                button: (image: nil, title: titleButton)
            )
        )
    }
    
    func showAbuseSuccess(_ image: UIImage?, title: String?, message: String?, titleButton: String) {
        let primaryButtonAction = ApolloAlertAction(title: titleButton, dismissOnCompletion: true, completion: {
            self.dismiss(animated: true) {
                self.dismiss(animated: true)
            }
        })
        
        showApolloAlert(
            image: image,
            title: title,
            subtitle: message,
            primaryButtonAction: primaryButtonAction,
            dismissCompletion: {
                self.dismiss(animated: true) {
                    self.dismiss(animated: true)
                }
            }
        )
    }
    
    func switchInputStateTo(editing: Bool) {
        if editing {
            inputTextField.becomeFirstResponder()
        } else {
            inputTextField.endEditing(true)
        }
    }
        
    func switchButtonState(to enabled: Bool) {
        sendButton.isEnabled = enabled
    }
    
    func switchLoadingState(_ shouldDisplay: Bool) {
        shouldDisplay ? beginState() : endState()
    }
    
    func configureScrollView(with notificationCenter: NotificationCenter) {
        scrollView.keyboardHandler = KeyboardHandler(notificationCenter: notificationCenter)
    }
}

// MARK: - StatefulTransitionViewing
extension AbuseReportDetailViewController: StatefulTransitionViewing {
    func didTryAgain() {
        interactor.sendAbuse(with: inputTextField.text)
    }
}
