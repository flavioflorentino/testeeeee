import Core

enum AbuseReportDetailServiceEndpoint {
    case reportAbuse(type: String, reportedId: String, message: String? = nil)
}

extension AbuseReportDetailServiceEndpoint: ApiEndpointExposable {
    var path: String {
        "abuse"
    }
    
    var method: HTTPMethod {
        .post
    }
    
    var body: Data? {
        switch self {
        case let .reportAbuse(type, reportedId, message):
            return [
                "type": type,
                "reported_id": reportedId,
                "message": message
            ].toData()
        }
    }
    
    var isTokenNeeded: Bool {
        false
    }
}
