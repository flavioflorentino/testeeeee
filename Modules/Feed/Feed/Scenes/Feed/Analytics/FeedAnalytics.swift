import AnalyticsModule
import Foundation

enum FeedAnalytics: AnalyticsKeyProtocol {
    case buttonTapped(type: ButtonType)
    case cardButtonTapped(buttonInfo: ButtonInfo, cardInfo: CardInfo)
    case cardTapped(cardInfo: CardInfo)
    case cardLikeInteracted(interactionType: InteractionType, cardInfo: CardInfo)
    case cardViewed(cardInfo: CardInfo)
    case feedDetailOptionInteraction(optionType: OptionInteractionType, cardId: String)
    case screenViewed
    case tabViewed(tabType: TabType)
    
    var properties: [String: Any] {
        switch self {
        case let .tabViewed(tabType):
            return [
                "tab_name": tabType.rawValue,
                "screen_name": ScreenName.begin,
                "business_context": ScreenName.feed
            ]
        case .screenViewed:
            return [
                "screen_name": ScreenName.transation,
                "business_context": ScreenName.transation
            ]
        case let .buttonTapped(buttonType):
            return [
                "button_name": buttonType.rawValue,
                "screen_name": ScreenName.transation,
                "business_context": ScreenName.transation
            ]
        case let .cardButtonTapped(buttonInfo, cardInfo):
            var eventProperties: [String: Any] = [
                "interaction_type": InteractionType.cardButtonClicked.rawValue,
                "button_name": buttonInfo.name,
                "card_position": cardInfo.position,
                "card_id": cardInfo.id,
                "card_type": cardInfo.itemType.rawValue
            ]

            if let buttonHyperlink = buttonInfo.hyperlink {
                eventProperties["button_hyperlink"] = buttonHyperlink
            }

            return eventProperties
        case let .cardTapped(cardInfo):
            var eventProperties: [String: Any] = [
                "interaction_type": InteractionType.cardClicked.rawValue,
                "card_position": cardInfo.position,
                "card_id": cardInfo.id,
                "card_type": cardInfo.itemType.rawValue
            ]

            if let cardTitle = cardInfo.title {
                eventProperties["card_title"] = cardTitle
            }

            if let cardMessage = cardInfo.message {
                eventProperties["card_message"] = cardMessage
            }

            if let carrouselPosition = cardInfo.carousel?.position {
                eventProperties["carrousel_position"] = carrouselPosition
            }
            
            if let bannerId = cardInfo.informational?.id {
                eventProperties["banner_id"] = bannerId
            }
            
            if let deeplink = cardInfo.deeplink {
                eventProperties["deeplink"] = deeplink
            }

            return eventProperties
        case let .cardLikeInteracted(interactionType, cardInfo):
            return [
                "interaction_type": interactionType.rawValue,
                "card_position": cardInfo.position,
                "card_id": cardInfo.id,
                "card_type": cardInfo.itemType.rawValue
            ]
        case let .cardViewed(cardInfo):
            var eventProperties: [String: Any] = [
                "interaction_type": InteractionType.cardViewed.rawValue,
                "card_position": cardInfo.position,
                "card_id": cardInfo.id,
                "card_type": cardInfo.itemType.rawValue
            ]

            if let carrouselPosition = cardInfo.carousel?.position {
                eventProperties["carrousel_position"] = carrouselPosition
            }
            
            if let bannerID = cardInfo.informational?.id {
                eventProperties["banner_id"] = bannerID
            }

            return eventProperties
        case let .feedDetailOptionInteraction(optionType, cardId):
            return [
                "interaction_type": optionType.rawValue,
                "card_id": cardId
            ]
        }
    }
    
    var name: String {
        switch self {
        case .tabViewed:
            return "Tab Viewed"
        case .screenViewed:
            return "Screen Viewed"
        case .buttonTapped:
            return "Button Clicked"
        case .cardButtonTapped, .cardTapped, .cardLikeInteracted, .cardViewed, .feedDetailOptionInteraction:
            return "Feed Card Interacted"
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.eventTracker])
    }
}

extension FeedAnalytics {
    enum TabType: String, CaseIterable {
        case all = "TODAS"
        case mine = "MINHAS"
    }
    
    enum ScreenName {
        static let begin = "INICIO"
        static let transation = "TRANSACAO"
        static let feed = "FEED"
    }
    
    enum InteractionType: String {
        case like = "LIKE"
        case dislike = "DISLIKE"
        case cardClicked = "CLICAR_CARD"
        case cardViewed = "CARD_IMPRESSO"
        case cardButtonClicked = "CLICAR_BOTOES_CARD"
        case profileViewed = "VISUALIZAR_PERFIL"
    }
    
    enum OptionInteractionType: String {
        case returnPayment = "RETURN_PAYMENT"
        case makePrivate = "MAKE_PRIVATE"
        case enableNotification = "ENABLE_NOTIFICATIONS"
        case disableNotification = "DISABLE_NOTIFICATIONS"
        case hideTransaction = "HIDE_TRANSACTION"
        case viewReceipt = "VIEW_RECEIPT"
        case reportCardSpam = "DENUNCIAR_CARD_SPAM"
        case reportCardContent = "DENUNCIAR_CARD_CONTEUDO"
        case reportCommentSpam = "DENUNCIAR_COMENTARIO_SPAM"
        case reportCommentContent = "DENUNCIAR_COMENTARIO_CONTEUDO"
    }
    
    enum ButtonType: String {
        case back = "VOLTAR"
        case options = "OPCOES"
    }
    
    struct CardInfo {
        let id: String
        let position: Int
        let itemType: ItemType
        let title: String?
        let message: String?
        let deeplink: String?
        let carousel: CarouselInfo?
        let informational: InformatinalInfo?
        
        init(
            id: String,
            position: Int,
            itemType: ItemType,
            title: String? = nil,
            message: String? = nil,
            deeplink: String? = nil,
            carousel: CarouselInfo? = nil,
            informational: InformatinalInfo? = nil
        ) {
            self.id = id
            self.position = position
            self.itemType = itemType
            self.title = title
            self.message = message
            self.deeplink = deeplink
            self.carousel = carousel
            self.informational = informational
        }
    }
    
    struct CarouselInfo {
        let id: String
        let position: Int
    }
    
    struct InformatinalInfo {
        let id: String
    }
    
    struct ButtonInfo {
        let name: String
        let hyperlink: String?
    }
        
    struct UserInfo {
        let id: String
        let type: String
    }
}
