import Core
import Foundation

final class ItemsProfileContainerService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    private let consumerId: String
    
    init(consumerId: String, dependencies: Dependencies) {
        self.consumerId = consumerId
        self.dependencies = dependencies
    }
}

extension ItemsProfileContainerService: ItemsContainerServicing {
    func listItems(lastItem: Int?, _ completion: @escaping (Result<ItemData, ApiError>) -> Void) {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        
        let request = Api<ItemData>(endpoint: FeedServiceEndpoint.profile(lastItem: lastItem, consumerId: consumerId))
        request.shouldUseDefaultDateFormatter = false

        request.execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
