import Core
import Foundation

final class ItemsPersonalContainerService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension ItemsPersonalContainerService: ItemsContainerServicing {
    func listItems(lastItem: Int?, _ completion: @escaping (Result<ItemData, ApiError>) -> Void) {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        
        let request = Api<ItemData>(endpoint: FeedServiceEndpoint.personal(lastItem: lastItem))
        request.shouldUseDefaultDateFormatter = false

        request.execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
