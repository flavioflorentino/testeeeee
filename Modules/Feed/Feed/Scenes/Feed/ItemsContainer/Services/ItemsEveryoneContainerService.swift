import Core
import Foundation

final class ItemsEveryoneContainerService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    private let adapter: LegacyBannerAdaptering
    
    init(adapter: LegacyBannerAdaptering, dependencies: Dependencies) {
        self.adapter = adapter
        self.dependencies = dependencies
    }
}

extension ItemsEveryoneContainerService: ItemsContainerServicing {
    func listItems(lastItem: Int?, _ completion: @escaping (Result<ItemData, ApiError>) -> Void) {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        
        let request = Api<ItemData>(endpoint: FeedServiceEndpoint.everyone(lastItem: lastItem))
        request.shouldUseDefaultDateFormatter = false

        request.execute(jsonDecoder: decoder) { [weak self] result in
            guard case let .success(value) = result.map(\.model), lastItem == nil, !value.data.isEmpty else {
                self?.dependencies.mainQueue.async { completion(result.map(\.model)) }
                return
            }
            
            self?.adapter.listBanners(with: value.data, { items in
                let responseValue = ItemData(lastItem: value.lastItem, data: items)
                completion(.success(responseValue))
            })
        }
    }
}
