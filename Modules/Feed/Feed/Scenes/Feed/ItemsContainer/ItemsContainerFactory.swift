import UIKit

enum ItemsContainerType {
    case all
    case mine
}

enum ItemsContainerFactory {
    static func make(
        service: ItemsContainerServicing,
        presenter: ItemsContainerPresenting,
        isOpenFollowersAndFollowings: Bool
    ) -> UIViewController {
        let container = DependencyContainer()
        let interactor = ItemsContainerInteractor(
            presenter: presenter,
            service: service,
            dependencies: container,
            isOpenFollowersAndFollowings: isOpenFollowersAndFollowings
        )

        let viewController = ItemsContainerViewController(interactor: interactor)

        presenter.viewController = viewController

        return viewController
    }
}
