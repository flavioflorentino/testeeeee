import SnapKit
import UI
import UIKit
import AnalyticsModule

protocol ItemsContainerDisplaying: AnyObject {
    func initialItems(_ items: [ItemsContainerDataSource])
    func addItems(_ items: [ItemsContainerDataSource])
    func restoreInitialState()
    func startLoading()
    func startFooterLoading()
    func stopLoading()
    func stopFooterLoading()
    func endRefreshing()
    func updateFeedbackFooter(for state: FeedbackState)
    func showEmptyState(from configuration: FeedbackStateConfigurable)
    func showErrorState(from configuration: FeedbackStateConfigurable)
}

private extension ItemsContainerViewController.Layout {
    enum Size {
        static let estimatedRowHeight: CGFloat = 60.0
    }
}

final class ItemsContainerViewController: ViewController<ItemsContainerInteracting, UIView>, ItemsContainerTableViewing {
    fileprivate enum Layout { }
    
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = Colors.branding600.color
        refreshControl.addTarget(self, action: #selector(changeValueRefreshControl), for: .valueChanged)
        return refreshControl
    }()
    
    private lazy var footerView: FeedbackFooterView = {
        let view = FeedbackFooterView()
        view.delegate = self
        return view
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.estimatedRowHeight = 44.0
        tableView.showsVerticalScrollIndicator = false
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.delegate = self
        
        tableView.register(TransactionalViewCell.self, forCellReuseIdentifier: String(describing: TransactionalViewCell.self))
        tableView.register(InformationalViewCell.self, forCellReuseIdentifier: String(describing: InformationalViewCell.self))
        tableView.register(CarouselViewCell.self, forCellReuseIdentifier: String(describing: CarouselViewCell.self))
        return tableView
    }()
    
    private lazy var dataSource: TableViewDataSource<Int, ItemsContainerDataSource> = {
        let dataSource = TableViewDataSource<Int, ItemsContainerDataSource>(view: tableView)
        dataSource.automaticReloadData = false
        
        dataSource.itemProvider = { [weak self] tableView, indexPath, item -> UITableViewCell? in
            self?.configureItemProvider(tableView, indexPath: indexPath, item: item)
        }
        
        return dataSource
    }()

    // MARK: - ItemsContainerTableViewing Properties
    var tableItems: UITableView { tableView }

    var isRefreshControlEnable = false {
        didSet {
            guard isRefreshControlEnable else { return }
            tableView.refreshControl = refreshControl
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = dataSource
        tableView.delegate = self
        tableView.prefetchDataSource = self
        interactor.loadData()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupTableHeaderAndFooterView()
    }
    
    // MARK: - ViewConfiguration
    override func buildViewHierarchy() {
        view.addSubview(tableView)
    }
    
    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    // MARK: - ItemsContainerTableViewing Methods
    func reload() {
        interactor.loadData()
    }
    
    // MARK: - Private Methods
    @objc
    private func changeValueRefreshControl() {
        interactor.refreshData()
    }
    
    private func configureItemProvider(
        _ tableView: UITableView,
        indexPath: IndexPath,
        item: ItemsContainerDataSource
    ) -> UITableViewCell? {
        switch item {
        case let .transactional(interactor, presenter):
            return reusableCell(TransactionalViewCell.self, tableView: tableView, indexPath: indexPath) { cell in
                presenter.view = cell
                cell?.setupInteractor(interactor)
            }
        case let .informational(viewModel):
            return reusableCell(InformationalViewCell.self, tableView: tableView, indexPath: indexPath) { cell in
                viewModel.inputs.setupOutputs(cell)
                cell?.setupViewModel(viewModel)
            }
        case let .carousel(viewModel):
            return reusableCell(CarouselViewCell.self, tableView: tableView, indexPath: indexPath) { cell in
                viewModel.inputs.setupOutputs(cell)
                cell?.setupViewModel(viewModel)
            }
        case .unknown:
            return UITableViewCell()
        }
    }
    
    private func reusableCell<Cell: UITableViewCell>(
        _ type: Cell.Type,
        tableView: UITableView,
        indexPath: IndexPath,
        _ completion: @escaping (_ cell: Cell?) -> Void
    ) -> Cell? {
        let identifier = String(describing: Cell.self)
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? Cell
        completion(cell)
        return cell
    }

    private func setupTableHeaderAndFooterView() {
        if let headerView = tableView.tableHeaderView {
            updateTableHeaderOrFooterHeight(headerView)
            tableView.tableHeaderView = headerView
        }

        if let footerView = tableView.tableFooterView {
            updateTableHeaderOrFooterHeight(footerView)
            tableView.tableFooterView = footerView
        }

        tableView.layoutIfNeeded()
    }

    private func updateTableHeaderOrFooterHeight(_ headerOrFooter: UIView) {
        let targetSize = CGSize(width: headerOrFooter.bounds.width, height: UIView.layoutFittingCompressedSize.height)
        let size = headerOrFooter.systemLayoutSizeFitting(targetSize)

        guard headerOrFooter.frame.height != size.height else {
            return
        }

        headerOrFooter.frame.size.height = size.height
    }
}

extension ItemsContainerViewController: ItemsContainerDisplaying {
    func initialItems(_ items: [ItemsContainerDataSource]) {
        dataSource.remove(section: 0)
        dataSource.add(items: items, to: 0)
        
        tableView.reloadData()
    }
    
    func addItems(_ items: [ItemsContainerDataSource]) {
        guard let data = dataSource.data[0] else { return }

        let startIndex = data.count
        let endIndex = data.count + items.count
        let indexPaths = (startIndex..<endIndex).map { IndexPath(row: $0, section: 0) }

        dataSource.add(items: items, to: 0)

        if #available(iOS 11.0, *) {
            tableView.performBatchUpdates {
                self.tableView.insertRows(at: indexPaths, with: .automatic)
            }
        } else {
            tableView.beginUpdates()
            tableView.insertRows(at: indexPaths, with: .automatic)
            tableView.endUpdates()
        }
    }
    
    func restoreInitialState() {
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.tableView.tableHeaderView = nil
            self?.tableView.layoutIfNeeded()
        }
    }
    
    func startLoading() {
        beginState()
    }
    
    func startFooterLoading() {
        interactor.footerAction(from: .loading)
    }
    
    func stopLoading() {
        endState()
    }
    
    func stopFooterLoading() {
        tableView.tableFooterView = nil
    }
    
    func endRefreshing() {
        refreshControl.endRefreshing()
    }
    
    func updateFeedbackFooter(for state: FeedbackState) {
        footerView.state = state
        tableView.tableFooterView = footerView
    }
    
    func showEmptyState(from configuration: FeedbackStateConfigurable) {
        configureStateView(from: configuration)
    }
    
    func showErrorState(from configuration: FeedbackStateConfigurable) {
        configureStateView(from: configuration)
    }
        
    // MARK: - Private Methods
    private func configureStateView(from configuration: FeedbackStateConfigurable) {
        let stateView = FeedbackStateFactory.make(for: configuration, delegate: self)
        stateView.isHidden = true
        
        tableView.tableHeaderView = stateView
        updateTableHeaderOrFooterHeight(stateView)
        tableView.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.3) {
            stateView.isHidden = false
        }
    }
}

extension ItemsContainerViewController: UITableViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentSizeHeight = scrollView.contentSize.height
        
        if offsetY > contentSizeHeight - scrollView.frame.size.height {
            interactor.loadMoreData()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        interactor.willDisplayCell(at: indexPath.row)
    }
}

extension ItemsContainerViewController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        interactor.prefetchingDataIfNeeded(from: indexPaths)
    }
}

extension ItemsContainerViewController: FeedbackStateDelegate {
    func primaryAction(from configuration: FeedbackStateConfigurable) {
        interactor.feedbackAction(from: configuration)
    }
}

extension ItemsContainerViewController: FeedbackFooterDelegate {
    func handleAction(for state: FeedbackState?) {
        interactor.footerAction(from: state)
    }
}

extension ItemsContainerViewController: StatefulTransitionViewing { }
