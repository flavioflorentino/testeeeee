import Core
import Foundation

protocol ItemsContainerServicing: AnyObject {
    func listItems(lastItem: Int?, _ completion: @escaping (Result<ItemData, ApiError>) -> Void)
}
