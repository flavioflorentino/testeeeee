import Foundation
import UIKit

protocol ItemsContainerTableViewing: AnyObject {
    var tableItems: UITableView { get }
    var isRefreshControlEnable: Bool { get set }
    func reload()
}

extension ItemsContainerTableViewing {
    var isRefreshControlEnable: Bool { false }
}
