import AssetsKit
import UI
import UIKit

extension CarouselItemViewCell.Layout {
    enum Size {
        static let imageHeight: CGFloat = 125
    }
}

final class CarouselItemViewCell: UICollectionViewCell {
    fileprivate enum Layout {}
    
    private lazy var containerView = CardView()
    
    private lazy var roundedContainerView: UIView = {
        let view = UIView()
        view
            .viewStyle(RoundedViewStyle(cornerRadius: .medium))
            .with(\.clipsToBounds, true)
        return view
    }()
    
    private lazy var bannerImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.showActivityIndicator = true
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        bannerImageView.cancelRequest()
    }
    
    func setupViewModel(_ viewModel: CarouselItemViewModeling) {
        viewModel.inputs.configure()
    }
    
    func setupCell(_ item: Banner) {
        bannerImageView.setImage(url: item.imageURL)
        titleLabel.text = item.text
    }
}

extension CarouselItemViewCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(containerView)
        containerView.addSubview(roundedContainerView)
        roundedContainerView.addSubview(bannerImageView)
        roundedContainerView.addSubview(titleLabel)
    }
    
    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base01)
            $0.leading.trailing.equalToSuperview()
        }
        
        roundedContainerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        bannerImageView.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.imageHeight)
            $0.top.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(bannerImageView.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.lessThanOrEqualToSuperview().offset(-Spacing.base01)
        }
        
        titleLabel.setContentHuggingPriority(.defaultHigh, for: .vertical)
    }
    
    func configureViews() {
        contentView.backgroundColor = Colors.backgroundPrimary.color
    }
}

extension CarouselItemViewCell: CarouselItemViewModelOutputs {
    func setup(imageUrl: URL, text: String) {
        bannerImageView.setImage(url: imageUrl, placeholder: Resources.Placeholders.actionAvatarPlaceholder.image)
        titleLabel.text = text
        
        setupAccessibility()
    }
}

extension CarouselItemViewCell: AccessibilityConfiguration {
    func buildAccessibilityElements() {
        containerView.isAccessibilityElement = true
    }
    
    func configureAccessibilityAttributes() {
        containerView.accessibilityLabel = titleLabel.text
    }
}
