import Foundation

protocol CarouselItemViewModelInputs: AnyObject {
    func configure()
    func setupOutput(_ output: CarouselItemViewModelOutputs?)
}

protocol CarouselItemViewModelOutputs: AnyObject {
    func setup(imageUrl: URL, text: String)
}

protocol CarouselItemViewModeling: AnyObject {
    var inputs: CarouselItemViewModelInputs { get }
    var outputs: CarouselItemViewModelOutputs? { get set }
}

final class CarouselItemViewModel: CarouselItemViewModeling {
    weak var outputs: CarouselItemViewModelOutputs?
    
    var inputs: CarouselItemViewModelInputs { self }
    
    private let item: Banner
    
    init(item: Banner) {
        self.item = item
    }
}

extension CarouselItemViewModel: CarouselItemViewModelInputs {
    func configure() {
        outputs?.setup(imageUrl: item.imageURL, text: item.text)
    }
    
    func setupOutput(_ output: CarouselItemViewModelOutputs?) {
        outputs = output
    }
}
