import Foundation
import AnalyticsModule

protocol CarouselViewModelInputs: AnyObject {
    func setupView()
    func setupOutputs(_ outputs: CarouselViewModelOutputs?)
    func didSelectItem(from index: Int)
    func willDisplayCell(at index: Int)
}

protocol CarouselViewModelOutputs: AnyObject {
    func setupListItems(_ viewModels: [CarouselItemViewModeling])
}

protocol CarouselViewModeling: AnyObject {
    var inputs: CarouselViewModelInputs { get }
    var outputs: CarouselViewModelOutputs? { get set }
}

final class CarouselViewModel: CarouselViewModeling {
    typealias Dependencies = HasAnalytics
    
    var inputs: CarouselViewModelInputs { self }
    weak var outputs: CarouselViewModelOutputs?
    
    private let cardId: UUID
    private let banners: [Banner]
    private let cardPosition: Int
    private let dependencies: Dependencies
    
    private weak var actionable: Actionable?
    
    init(cardId: UUID, banners: [Banner], cardPosition: Int, dependencies: Dependencies, actionable: Actionable? = nil) {
        self.cardId = cardId
        self.banners = banners
        self.cardPosition = cardPosition
        self.dependencies = dependencies
        self.actionable = actionable
    }
}

extension CarouselViewModel: CarouselViewModelInputs {
    func setupView() {
        let viewModels = banners.map { CarouselItemViewModel(item: $0) }
        outputs?.setupListItems(viewModels)
    }
    
    func setupOutputs(_ outputs: CarouselViewModelOutputs?) {
        self.outputs = outputs
    }
    
    func didSelectItem(from index: Int) {
        guard banners.indices.contains(index) else { return }
        
        let bannerSelected = banners[index]
        
        guard case let .deeplink(action) = bannerSelected.action.data else { return }
        
        let event = FeedAnalytics.cardTapped(
            cardInfo: .init(
                id: cardId.uuidString,
                position: cardPosition,
                itemType: .carousel,
                title: bannerSelected.text,
                message: bannerSelected.description,
                deeplink: action.url.absoluteString,
                carousel: .init(id: bannerSelected.id.uuidString, position: index)
            )
        )
        
        dependencies.analytics.log(event)
        actionable?.deeplink(action.url)
    }
    
    func willDisplayCell(at index: Int) {
        let bannerSelected = banners[index]
        
        let event = FeedAnalytics.cardViewed(
            cardInfo: .init(
                id: cardId.uuidString,
                position: cardPosition,
                itemType: .carousel,
                carousel: .init(id: bannerSelected.id.uuidString, position: index)
            )
        )
        
        dependencies.analytics.log(event)
    }
}
