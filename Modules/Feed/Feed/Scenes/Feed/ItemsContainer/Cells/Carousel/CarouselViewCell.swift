import SnapKit
import UI
import UIKit

extension CarouselViewCell.Layout {
    enum Size {
        static let item = CGSize(width: 185.0, height: 230.0)
    }
}

final class CarouselViewCell: UITableViewCell {
    fileprivate enum Layout {}
    
    private lazy var containerView = UIView()
    
    private lazy var collectionViewFlowLayout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = Layout.Size.item
        layout.minimumInteritemSpacing = Spacing.base01
        return layout
    }()
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewFlowLayout)
        collectionView.register(
            CarouselItemViewCell.self,
            forCellWithReuseIdentifier: String(
                describing: CarouselItemViewCell.self
            )
        )
        collectionView.backgroundColor = Colors.backgroundPrimary.color
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.contentInset = .init(top: .zero, left: Spacing.base02, bottom: .zero, right: Spacing.base02)
        collectionView.delegate = self
        return collectionView
    }()
    
    private lazy var dataSource: CollectionViewDataSource<Int, CarouselItemViewModeling> = {
        let dataSource = CollectionViewDataSource<Int, CarouselItemViewModeling>(view: collectionView)
        dataSource.itemProvider = { collectionView, indexPath, viewModel -> UICollectionViewCell? in
            let identifier = String(describing: CarouselItemViewCell.self)
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as? CarouselItemViewCell
            
            viewModel.inputs.setupOutput(cell)
            cell?.setupViewModel(viewModel)
            
            return cell
        }
        
        return dataSource
    }()
    
    private var viewModel: CarouselViewModeling?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
        setupAccessibility()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        collectionView.scrollToItem(at: .init(item: 0, section: 0), at: .centeredHorizontally, animated: false)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViewModel(_ viewModel: CarouselViewModeling) {
        self.viewModel = viewModel
        collectionView.dataSource = dataSource
        self.viewModel?.inputs.setupView()
    }
}

extension CarouselViewCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(containerView)
        containerView.addSubview(collectionView)
    }
    
    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        collectionView.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.item)
            $0.edges.equalToSuperview()
        }
    }
    
    func configureViews() {
        selectionStyle = .none
        contentView.backgroundColor = Colors.backgroundPrimary.color
    }
}

extension CarouselViewCell: CarouselViewModelOutputs {
    func setupListItems(_ viewModels: [CarouselItemViewModeling]) {
        dataSource.remove(section: 0)
        dataSource.add(items: viewModels, to: 0)
    }
}

extension CarouselViewCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel?.inputs.didSelectItem(from: indexPath.item)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        viewModel?.inputs.willDisplayCell(at: indexPath.item)
    }
}

extension CarouselViewCell: AccessibilityConfiguration {
    func buildAccessibilityElements() {
        containerView.accessibilityElements = [collectionView]
    }
}
