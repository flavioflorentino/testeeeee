import AssetsKit
import SnapKit
import UI
import UIKit

extension InformationalViewCell.Layout {
    enum Size {
        static let imageSize: CGSize = .init(width: 136.0, height: 136.0)
    }
}

final class InformationalViewCell: UITableViewCell {
    fileprivate enum Layout {}
    
    private lazy var containerView: CardView = {
        let view = CardView()
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(containerAction)))
        return view
    }()
    
    private lazy var roundedContainerView: UIView = {
        let view = UIView()
        view
            .viewStyle(RoundedViewStyle(cornerRadius: .medium))
            .with(\.backgroundColor, .groupedBackgroundSecondary())
            .with(\.clipsToBounds, true)
        return view
    }()
    
    private lazy var bannerImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.backgroundColor, .groupedBackgroundSecondary())
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.backgroundColor, .groupedBackgroundSecondary())
            .with(\.textColor, Colors.grayscale600.color)
        return label
    }()
    
    private lazy var linkButton: UIButton = {
        let button = UIButton(type: .custom)
        button.addTarget(self, action: #selector(linkButtonAction), for: .touchUpInside)
        button
            .buttonStyle(LinkButtonStyle(size: .small))
            .with(\.contentHorizontalAlignment, .left)
        return button
    }()
    
    private var viewModel: InformationalViewModeling?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        bannerImageView.cancelRequest()
        
        bannerImageView.image = nil
        titleLabel.text = nil
        descriptionLabel.text = nil
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        viewModel?.inputs.updateStylesIfNeeded()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        guard #available(iOS 13, *), traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) else {
            return
        }
        
        viewModel?.inputs.updateStylesIfNeeded()
    }
    
    func setupViewModel(_ viewModel: InformationalViewModeling) {
        self.viewModel = viewModel
        self.viewModel?.inputs.setupView()
    }
    
    // MARK: - Private Methods
    @objc
    private func linkButtonAction() {
        viewModel?.inputs.primaryAction()
    }
    
    @objc
    private func containerAction() {
        viewModel?.inputs.action()
    }
}

extension InformationalViewCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(containerView)
        containerView.addSubview(roundedContainerView)
        
        roundedContainerView.addSubview(bannerImageView)
        roundedContainerView.addSubview(titleLabel)
        roundedContainerView.addSubview(descriptionLabel)
        roundedContainerView.addSubview(linkButton)
    }
    
    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.top.bottom.equalToSuperview().inset(Spacing.base01)
        }
        
        roundedContainerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        bannerImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.imageSize)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.centerY.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.leading.equalToSuperview().inset(Spacing.base02)
            $0.trailing.equalTo(bannerImageView.snp.leading).offset(-Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalToSuperview().inset(Spacing.base02)
            $0.trailing.equalTo(bannerImageView.snp.leading).offset(-Spacing.base02)
        }
        
        linkButton.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base00)
            $0.leading.equalToSuperview().inset(Spacing.base02)
            $0.trailing.equalTo(bannerImageView.snp.leading).offset(-Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base01)
        }
    }
    
    func configureViews() {
        selectionStyle = .none
        contentView.backgroundColor = Colors.backgroundPrimary.color
    }
}

extension InformationalViewCell: InformationalViewModelOutputs {
    func configureView(_ title: String, description: String?, buttonTitle: String?, imageUrl: URL) {
        bannerImageView.setImage(url: imageUrl, placeholder: Resources.Placeholders.actionAvatarPlaceholder.image)
                
        titleLabel.text = title
        descriptionLabel.text = description
        linkButton.setTitle(buttonTitle, for: .normal)
        linkButton.buttonStyle(LinkButtonStyle(size: .small))
        
        setupAccessibility()
    }
    
    func updateStylesIfNeeded() {
        containerView.setBackgroundColor(Colors.groupedBackgroundSecondary.color)
    }
}

extension InformationalViewCell: AccessibilityConfiguration {
    func buildAccessibilityElements() {
        contentView.isAccessibilityElement = false
        containerView.isAccessibilityElement = true
    }
    
    func configureAccessibilityAttributes() {
        let titleText = titleLabel.text ?? String()
        let descriptionText = descriptionLabel.text ?? String()
        let linkText = linkButton.titleLabel?.text ?? String()
        
        containerView.accessibilityLabel = "\(titleText), \(descriptionText)"
        containerView.accessibilityTraits = .allowsDirectInteraction
        
        containerView.accessibilityCustomActions = [
            UIAccessibilityCustomAction(
                name: linkText,
                target: self,
                selector: #selector(linkButtonAction)
            )
        ]
    }
}
