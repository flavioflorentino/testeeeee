import Foundation
import UI
import AnalyticsModule

protocol InformationalViewModelInputs: AnyObject {
    func setupView()
    func setupOutputs(_ outputs: InformationalViewModelOutputs?)
    func action()
    func primaryAction()
    func updateStylesIfNeeded()
}

protocol InformationalViewModelOutputs: AnyObject {
    func configureView(_ title: String, description: String?, buttonTitle: String?, imageUrl: URL)
    func updateStylesIfNeeded()
}

protocol InformationalViewModeling: AnyObject {
    var inputs: InformationalViewModelInputs { get }
    var outputs: InformationalViewModelOutputs? { get set }
}

final class InformationalViewModel: InformationalViewModeling {
    typealias Dependencies = HasAnalytics
    
    var inputs: InformationalViewModelInputs { self }
    weak var outputs: InformationalViewModelOutputs?
    
    private let cardId: UUID
    private let banner: Banner
    private let cardPosition: Int
    private let dependencies: Dependencies
    
    private weak var actionable: Actionable?

    init(cardId: UUID, banner: Banner, cardPosition: Int, dependencies: Dependencies, actionable: Actionable? = nil) {
        self.cardId = cardId
        self.banner = banner
        self.cardPosition = cardPosition
        self.dependencies = dependencies
        self.actionable = actionable
    }
}

extension InformationalViewModel: InformationalViewModelInputs {
    func setupView() {
        outputs?.configureView(
            banner.text,
            description: banner.description,
            buttonTitle: banner.primaryAction?.text,
            imageUrl: banner.imageURL
        )
    }
    
    func setupOutputs(_ outputs: InformationalViewModelOutputs?) {
        self.outputs = outputs
    }
    
    func action() {
        guard case let .deeplink(action) = banner.action.data else {
            return
        }
        
        let event = FeedAnalytics.cardTapped(
            cardInfo: .init(
                id: cardId.uuidString,
                position: cardPosition,
                itemType: .informational,
                title: banner.text,
                message: banner.description,
                deeplink: action.url.absoluteString,
                informational: .init(id: banner.id.uuidString)
            )
        )
        
        dependencies.analytics.log(event)
        actionable?.deeplink(action.url)
    }
    
    func primaryAction() {
        guard let primaryAction = banner.primaryAction,
              case let .deeplink(action) = primaryAction.action.data else {
            return
        }
        
        let event = FeedAnalytics.cardButtonTapped(
            buttonInfo: .init(name: primaryAction.text, hyperlink: action.url.absoluteString),
            cardInfo: .init(id: cardId.uuidString, position: cardPosition, itemType: .informational)
        )
        
        dependencies.analytics.log(event)
        actionable?.deeplink(action.url)
    }
    
    func updateStylesIfNeeded() {
        outputs?.updateStylesIfNeeded()
    }
}
