import Foundation
import UI

protocol AdditionalInfoPresenting: AnyObject {
    var view: AdditionalInfoDisplaying? { get set }
    func configureItem(_ item: AdditionalInfo, hasAction: Bool)
    func updateBackgroundColorIfNeeded(_ hasAction: Bool)
}

final class AdditionalInfoPresenter {
    weak var view: AdditionalInfoDisplaying?
}

extension AdditionalInfoPresenter: AdditionalInfoPresenting {
    func configureItem(_ item: AdditionalInfo, hasAction: Bool) {
        let ranges = item.text.attributes.compactMap {
            NSRange(location: $0.startIndex, length: $0.startIndex.distance(to: $0.endIndex))
        }
        
        view?.configureAttributesRanges(ranges)
        
        let image = UIImage(
            iconName: iconName(item.type),
            font: Typography.icons(.large).font(),
            color: statusColor(item.status),
            size: Sizing.ImageView.xSmall.rawValue
        )
        
        view?.configureImage(image)
        view?.configureDescription(item.text.value)
        
        configureContainerBackgroundColor(hasAction)
        configureActions(item)
    }
    
    func updateBackgroundColorIfNeeded(_ hasAction: Bool) {
        guard hasAction else {
            view?.configureBackgroundColorWhenNotExistsAction()
            return
        }

        view?.configureBackgroundColorWhenExistsAction()
    }
    
    // MARK: - Private Methods
    private func configureActions(_ item: AdditionalInfo) {
        if let primaryAction = item.primaryAction {
            view?.configurePrimaryAction(primaryAction.text)
        }
        
        if let secondaryAction = item.secondaryAction {
            view?.configureSecondaryAction(secondaryAction.text)
        }
        
        if item.action != nil {
            view?.configureContainerAction()
            
            if item.primaryAction == nil && item.secondaryAction == nil {
                let image = UIImage(
                    iconName: Iconography.angleRight.rawValue,
                    font: Typography.icons(.medium).font(),
                    color: Colors.branding600.color,
                    size: Sizing.ImageView.xSmall.rawValue,
                    edgeInsets: UIEdgeInsets(top: Spacing.base00, left: .zero, bottom: .zero, right: .zero)
                )
                
                view?.configureAccessoryImage(image)
            }
        }
    }
    
    private func configureContainerBackgroundColor(_ hasAction: Bool) {
        guard hasAction else {
            view?.configureContainerBackgroundColorWhenNotExistsAction()
            return
        }

        view?.configureContainerBackgroundColorWhenExistsAction()
    }
    
    private func statusColor(_ type: AdditionalStatus) -> UIColor {
        switch type {
        case .success:
            return Colors.success600.color
        case .informative:
            return Colors.neutral600.color
        case .warning:
            return Colors.warning600.color
        case .error:
            return Colors.critical600.color
        }
    }
    
    private func iconName(_ type: AdditionalType) -> String {
        switch type {
        case .bill:
            return Iconography.bill.rawValue
        case .checkCircle:
            return Iconography.checkCircle.rawValue
        case .leftCircle:
            return Iconography.arrowCircleLeft.rawValue
        case .exclamation:
            return Iconography.exclamationTriangle.rawValue
        case .money:
            return Iconography.moneyWithdraw.rawValue
        case .negativeCircle:
            return Iconography.timesCircle.rawValue
        case .clock:
            return Iconography.clockThree.rawValue
        case .calendar:
            return Iconography.calender.rawValue
        }
    }
}
