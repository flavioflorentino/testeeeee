import UI
import UIKit

protocol TransactionalPresenting: AnyObject {
    var view: TransactionalDisplaying? { get set }
    
    func createActionLogView(from actionLog: ActionLog, hasAction: Bool, delegate: ActionLogDelegate?)
    func createUserTestimonialView(from userTestimonial: UserTestimonial, hasAction: Bool)
    func createAdditionalInfoView(
        from additionalInfo: AdditionalInfo,
        hasAction: Bool,
        actionable: Actionable?,
        delegate: AdditionalInfoDelegate?
    )
    func createPostDetailView(from postDetail: PostDetail, hasAction: Bool)
    func createSocialActionView(from socialAction: SocialAction, hasAction: Bool, delegate: SocialActionDelegate?)
    
    func configureBackgroundColorWhenNotExistsAction()
    func configureBackgroundColorWhenExistsAction()
    
    func configureAccessibility(
        actionLog: ActionLog?,
        userTestimonial: UserTestimonial?,
        additionalInfo: AdditionalInfo?,
        postDetail: PostDetail?
    )
}

final class TransactionalPresenter {
    typealias Dependencies = HasCurrentDate & HasTimeZone
    
    weak var view: TransactionalDisplaying?
    
    private var customActions: [UIAccessibilityCustomAction] = [] {
        didSet {
            view?.configureAccessibilityCustomActions(customActions)
        }
    }
    
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension TransactionalPresenter: TransactionalPresenting {
    func createActionLogView(from actionLog: ActionLog, hasAction: Bool, delegate: ActionLogDelegate?) {
        let uiView = ActionLogFactory.make(item: actionLog, hasAction: hasAction, delegate: delegate)
        view?.configureView(uiView)
    }
    
    func createUserTestimonialView(from userTestimonial: UserTestimonial, hasAction: Bool) {
        let uiView = UserTestimonialFactory.make(item: userTestimonial, hasAction: hasAction)
        view?.configureView(uiView)
    }
    
    func createAdditionalInfoView(
        from additionalInfo: AdditionalInfo,
        hasAction: Bool,
        actionable: Actionable?,
        delegate: AdditionalInfoDelegate?
    ) {
        let uiView = AdditionalInfoFactory.make(
            item: additionalInfo,
            hasAction: hasAction,
            delegate: delegate
        )
        
        uiView.accessibilityDelegate = self
        view?.configureView(uiView)
    }
    
    func createPostDetailView(from postDetail: PostDetail, hasAction: Bool) {
        let uiView = PostDetailFactory.make(item: postDetail, hasAction: hasAction)
        view?.configureView(uiView)
    }
    
    func createSocialActionView(from socialAction: SocialAction, hasAction: Bool, delegate: SocialActionDelegate?) {
        let uiView = SocialActionFactory.make(item: socialAction, hasAction: hasAction, delegate: delegate)
        uiView.accessibilityDelegate = self
        view?.configureView(uiView)
    }
    
    func configureBackgroundColorWhenNotExistsAction() {
        view?.configureBackgroundColorWhenNotExistsAction()
    }
    
    func configureBackgroundColorWhenExistsAction() {
        view?.configureBackgroundColorWhenExistsAction()
    }
    
    func configureAccessibility(
        actionLog: ActionLog?,
        userTestimonial: UserTestimonial?,
        additionalInfo: AdditionalInfo?,
        postDetail: PostDetail?
    ) {
        var strings = [String]()

        if let actionLog = actionLog {
            strings.append(actionLog.text.value)
        }

        if let userTestimonial = userTestimonial {
            strings.append(userTestimonial.text.value)
        }

        if let additionalInfo = additionalInfo {
            strings.append(additionalInfo.text.value)
        }

        if let postDetail = postDetail {
            let privacy = postDetail.privacy == .private ? Strings.private : Strings.public
            let value = postDetail.currency.value
            let timeAgoSinceNow = postDetail.date.timeAgoSince(
                dependencies.currentDate,
                dateFormat: "dd MMMM",
                timeZone: dependencies.timeZone
            )

            switch postDetail.currency.type {
            case .credit:
                strings.append(Strings.detailCredit(value, timeAgoSinceNow, privacy))
            case .debit:
                strings.append(Strings.detailDebit(value, timeAgoSinceNow, privacy))
            case .neutral:
                strings.append(Strings.detailNeutral(value, timeAgoSinceNow, privacy))
            }
        }

        view?.configureAccessibilityLabel(strings.joined(separator: ", "))
    }
}

extension TransactionalPresenter: AccessibilityConfigurationDelegate {
    func accessibilityCustomAction(_ customAction: UIAccessibilityCustomAction) {
        customActions.append(customAction)
    }
}
