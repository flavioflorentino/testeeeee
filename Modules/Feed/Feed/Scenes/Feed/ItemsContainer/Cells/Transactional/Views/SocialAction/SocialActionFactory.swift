import UIKit

protocol UISocialActionUpdating: AnyObject {
    func updateLiked(newNumberOfLikes: Int, isLike: Bool)
}

protocol SocialActionDelegate: AnyObject {
    func didTouchLiked(numberOfLikes: Int, isLike: Bool, uiSocialActionUpdate: UISocialActionUpdating)
    func didTouchCommented()
}

enum SocialActionFactory {
    static func make(item: SocialAction, hasAction: Bool = true, delegate: SocialActionDelegate? = nil) -> SocialActionView {
        let presenter: SocialActionPresenting = SocialActionPresenter()
        let interactor = SocialActionInteractor(item: item, presenter: presenter, hasAction: hasAction, delegate: delegate)
        let view = SocialActionView(interactor: interactor)

        presenter.view = view

        return view
    }
}
