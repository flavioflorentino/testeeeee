import SnapKit
import UI
import UIKit

protocol AdditionalInfoDisplaying: AnyObject {
    func configureImage(_ image: UIImage?)
    func configureDescription(_ text: String)
    func configurePrimaryAction(_ title: String)
    func configureSecondaryAction(_ title: String)
    func configureContainerBackgroundColorWhenNotExistsAction()
    func configureContainerBackgroundColorWhenExistsAction()
    func configureAttributesRanges(_ ranges: [NSRange])
    func configureBackgroundColorWhenNotExistsAction()
    func configureBackgroundColorWhenExistsAction()
    func configureContainerAction()
    func configureAccessoryImage(_ image: UIImage?)
}

final class AdditionalInfoView: UIView {
    private lazy var containerView = UIView()
    
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.imageStyle(RoundedImageStyle(size: .xSmall, cornerRadius: .light))
        return imageView
    }()
    
    private lazy var textLabel: InteractiveLabel = {
        let label = InteractiveLabel()
        label.delegate = self
        label.labelStyle(BodyPrimaryLabelStyle())
        return label
    }()
    
    private lazy var secondaryButton: UIButton = {
        let button = UIButton(type: .custom)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(pressSecondaryButton(_:)), for: .touchUpInside)
        return button
    }()
    
    private lazy var primaryButton: UIButton = {
        let button = UIButton(type: .custom)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(pressPrimaryButton(_:)), for: .touchUpInside)
        return button
    }()
    
    private lazy var accessoryImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    weak var accessibilityDelegate: AccessibilityConfigurationDelegate?
    
    let interactor: AdditionalInfoInteracting
    
    init(interactor: AdditionalInfoInteracting) {
        self.interactor = interactor
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        interactor.loadData()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        interactor.updateBackgroundColorIfNeeded()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)

        guard #available(iOS 13, *), traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) else {
            return
        }
        
        interactor.updateBackgroundColorIfNeeded()
    }
    
    @objc
    private func pressPrimaryButton(_ sender: UIButton) {
        interactor.primaryAction()
    }
    
    @objc
    private func pressSecondaryButton(_ sender: UIButton) {
        interactor.secondaryAction()
    }
    
    @objc
    private func pressContainer() {
        interactor.containerAction()
    }
}

extension AdditionalInfoView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(containerView)
        containerView.addSubview(iconImageView)
        containerView.addSubview(textLabel)
    }
    
    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        iconImageView.snp.makeConstraints {
            $0.top.equalTo(containerView.snp.top).offset(Spacing.base02)
            $0.leading.equalTo(containerView.snp.leading).offset(Spacing.base02)
        }
        
        textLabel.snp.makeConstraints {
            $0.height.greaterThanOrEqualTo(iconImageView.snp.height)
            $0.top.equalTo(containerView.snp.top).offset(Spacing.base02)
            $0.leading.equalTo(iconImageView.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalTo(containerView.snp.trailing).offset(-Spacing.base02)
            $0.bottom.equalTo(containerView.snp.bottom).offset(-Spacing.base02).priority(.low)
        }
    }
}

extension AdditionalInfoView: AdditionalInfoDisplaying {
    func configureImage(_ image: UIImage?) {
        iconImageView.image = image
    }
    
    func configureDescription(_ text: String) {
        textLabel.text = text
    }
    
    func configurePrimaryAction(_ title: String) {
        configurePrimaryButtonAndSetConstraints()
        primaryButton.setTitle(title, for: .normal)
        
        accessibilityDelegate?.accessibilityCustomAction(
            UIAccessibilityCustomAction(
                name: title,
                target: self,
                selector: #selector(pressPrimaryButton(_:))
            )
        )
    }
    
    func configureSecondaryAction(_ title: String) {
        configureSecondaryButtonAndSetConstraints()
        secondaryButton.setTitle(title, for: .normal)
        secondaryButton.buttonStyle(LinkButtonStyle())
        
        accessibilityDelegate?.accessibilityCustomAction(
            UIAccessibilityCustomAction(
                name: title,
                target: self,
                selector: #selector(pressSecondaryButton(_:))
            )
        )
    }
    
    func configureContainerBackgroundColorWhenNotExistsAction() {
        configureContainerView(.groupedBackgroundSecondary())
    }
    
    func configureContainerBackgroundColorWhenExistsAction() {
        configureContainerView(.groupedBackgroundTertiary())
    }
    
    func configureAttributesRanges(_ ranges: [NSRange]) {
        textLabel.ranges = ranges
    }
    
    func configureBackgroundColorWhenNotExistsAction() {
        configureBackgroundColorStyle(.groupedBackgroundTertiary())
    }
    
    func configureBackgroundColorWhenExistsAction() {
        configureBackgroundColorStyle(.groupedBackgroundSecondary())
    }
    
    func configureContainerAction() {
        containerView.isUserInteractionEnabled = true
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(pressContainer))
        containerView.addGestureRecognizer(tapGestureRecognizer)
        
        accessibilityDelegate?.accessibilityCustomAction(
            UIAccessibilityCustomAction(
                name: textLabel.text ?? String(),
                target: self,
                selector: #selector(pressContainer)
            )
        )
    }
  
    func configureAccessoryImage(_ image: UIImage?) {
        configureAccessoryImageAndSetConstraints()
        accessoryImageView.image = image
    }
  
    // MARK: - Helper Private Methods
    func configureBackgroundColorStyle(_ colorStyle: Colors.Style) {
        backgroundColor = colorStyle.rawValue
    }
    
    private func configureContainerView(_ backgroundColorStyle: Colors.Style) {
        containerView
            .viewStyle(RoundedViewStyle(cornerRadius: .medium))
            .with(\.backgroundColor, backgroundColorStyle)
        
        textLabel.backgroundColor = backgroundColorStyle.rawValue
    }
    
    private func configurePrimaryButtonAndSetConstraints() {
        containerView.addSubview(primaryButton)
        
        primaryButton.snp.makeConstraints {
            $0.top.equalTo(textLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base02).priority(.medium)
        }

        if containerView.subviews.contains(secondaryButton) {
            secondaryButton.snp.makeConstraints {
                $0.top.equalTo(primaryButton.snp.bottom).offset(Spacing.base01).priority(.high)
            }
        }
    }
    
    private func configureSecondaryButtonAndSetConstraints() {
        containerView.addSubview(secondaryButton)
        
        secondaryButton.snp.makeConstraints {
            $0.top.equalTo(textLabel.snp.bottom).offset(Spacing.base01).priority(.medium)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base01)
        }
        
        if containerView.subviews.contains(primaryButton) {
            primaryButton.snp.makeConstraints {
                $0.bottom.equalTo(secondaryButton.snp.top).offset(-Spacing.base01).priority(.high)
            }
        }
    }
    
    private func configureAccessoryImageAndSetConstraints() {
        containerView.addSubview(accessoryImageView)

        accessoryImageView.snp.makeConstraints {
            $0.size.equalTo(Sizing.ImageView.xSmall.rawValue)
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        textLabel.snp.remakeConstraints {
            $0.height.greaterThanOrEqualTo(iconImageView.snp.height)
            $0.top.equalTo(containerView.snp.top).offset(Spacing.base02)
            $0.leading.equalTo(iconImageView.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalTo(accessoryImageView.snp.leading).offset(-Spacing.base01)
            $0.bottom.equalTo(containerView.snp.bottom).offset(-Spacing.base02).priority(.low)
        }
    }
}

extension AdditionalInfoView: InteractiveLabelDelegate {
    func didSelectTextWithinTheRanges(_ text: String) {
        interactor.didSelected(from: text)
    }
}
