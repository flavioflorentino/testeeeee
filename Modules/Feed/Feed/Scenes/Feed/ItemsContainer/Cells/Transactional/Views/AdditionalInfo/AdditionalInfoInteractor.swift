import Foundation
import AnalyticsModule

protocol AdditionalInfoInteracting: AnyObject {
    func loadData()
    func primaryAction()
    func secondaryAction()
    func containerAction()
    func updateBackgroundColorIfNeeded()
    func didSelected(from text: String)
}

final class AdditionalInfoInteractor {
    private let item: AdditionalInfo
    private let presenter: AdditionalInfoPresenting
    private let hasAction: Bool
    
    private weak var delegate: AdditionalInfoDelegate?
    
    init(item: AdditionalInfo, presenter: AdditionalInfoPresenting, hasAction: Bool, delegate: AdditionalInfoDelegate?) {
        self.item = item
        self.presenter = presenter
        self.hasAction = hasAction
        self.delegate = delegate
    }
}

extension AdditionalInfoInteractor: AdditionalInfoInteracting {
    func loadData() {
        presenter.configureItem(item, hasAction: hasAction)
    }
    
    func primaryAction() {
        guard let primaryAction = item.primaryAction else {
            return
        }
        
        if case let .deeplink(action) = primaryAction.action.data {
            delegate?.didTouchPrimaryAction(for: primaryAction.text, deeplinkUrl: action.url)
        }
    }
    
    func secondaryAction() {
        guard let secondaryAction = item.secondaryAction else {
            return
        }
        
        if case let .deeplink(action) = secondaryAction.action.data {
            delegate?.didTouchSecondaryAction(for: secondaryAction.text, deeplinkUrl: action.url)
        }
    }
    
    func containerAction() {
        guard let action = item.action else {
            return
        }
        
        if case let .deeplink(action) = action.data {
            delegate?.didTouchContainerAction(for: item.text.value, deeplinkUrl: action.url)
        }
    }
    
    func updateBackgroundColorIfNeeded() {
        presenter.updateBackgroundColorIfNeeded(hasAction)
    }
    
    func didSelected(from text: String) {
        item.text.attributes.lazy.forEach {
            let range = NSRange(location: $0.startIndex, length: $0.endIndex - $0.startIndex)
            let substring = String(item.text.value[range])
            
            guard substring == text, let attributeAction = $0.action else { return }
            
            if case let .deeplink(action) = attributeAction.data {
                delegate?.didSelectText(text, deeplinkUrl: action.url)
            }
        }
    }
}
