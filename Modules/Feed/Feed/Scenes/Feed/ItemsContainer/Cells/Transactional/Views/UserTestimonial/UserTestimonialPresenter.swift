import Foundation
import UI

protocol UserTestimonialPresenting: AnyObject {
    var view: UserTestimonialDisplaying? { get set }
    func configureItem(_ item: UserTestimonial)
    func updateBackgroundColorIfNeeded(_ hasAction: Bool)
}

final class UserTestimonialPresenter {
    weak var view: UserTestimonialDisplaying?
}

extension UserTestimonialPresenter: UserTestimonialPresenting {
    func configureItem(_ item: UserTestimonial) {
        let attributedString = addAttributes(item.text.value, attributes: item.text.attributes)
        
        view?.configureStyle(BodyPrimaryLabelStyle())
        
        let textValue = item.text.value.replacingOccurrences(of: " ", with: "")
        
        if textValue.containsOnlyEmoji && textValue.emojis.count <= 5 {
            view?.configureStyle(TitleLabelStyle(type: .xLarge))
        }
        
        view?.configureDescription(attributedString)
    }
    
    func updateBackgroundColorIfNeeded(_ hasAction: Bool) {
        guard hasAction else {
            view?.configureBackgroundColorWhenNotExistsAction()
            return
        }
        
        view?.configureBackgroundColorWhenExistsAction()
    }
    
    // MARK: - Private Method
    private func addAttributes(_ text: String, attributes: [Attribute]) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: text)
        
        attributes.forEach {
            attributedString.addAttribute(
                .font,
                value: Typography.bodyPrimary(.highlight).font(),
                range: NSRange(
                    location: $0.startIndex,
                    length: $0.startIndex.distance(to: $0.endIndex)
                )
            )
        }
        
        return attributedString
    }
}
