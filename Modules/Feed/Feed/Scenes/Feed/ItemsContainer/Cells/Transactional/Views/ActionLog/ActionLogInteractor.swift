import Foundation

protocol ActionLogInteracting: AnyObject {
    func loadData()
    func updateBackgroundColorIfNeeded()
    func primaryImageAction()
    func secondaryImageAction()
    func didSelected(from text: String)
}

final class ActionLogInteractor {
    private let item: ActionLog
    private let presenter: ActionLogPresenting
    private let hasAction: Bool
    
    private weak var delegate: ActionLogDelegate?

    init(item: ActionLog, presenter: ActionLogPresenting, hasAction: Bool, delegate: ActionLogDelegate?) {
        self.item = item
        self.presenter = presenter
        self.hasAction = hasAction
        self.delegate = delegate
    }
}

// MARK: - ActionLogInteracting
extension ActionLogInteractor: ActionLogInteracting {    
    func loadData() {
        presenter.configureItem(item)
    }
    
    func updateBackgroundColorIfNeeded() {
        presenter.updateBackgroundColorIfNeeded(for: item, hasAction: hasAction)
    }
    
    func primaryImageAction() {
        guard
            let firstAction = item.images.first?.action,
            case .deeplink(let action) = firstAction.data
        else {
            return
        }
        
        delegate?.didTouchPrimaryImage(for: action.url)
    }
    
    func secondaryImageAction() {
        guard
            item.images.count > 1,
            let lastAction = item.images.last?.action,
            case .deeplink(let action) = lastAction.data
        else {
            return
        }
        
        delegate?.didTouchSecondaryImage(for: action.url)
    }
    
    func didSelected(from text: String) {
        item.text.attributes.lazy.forEach {
            let range = NSRange(location: $0.startIndex, length: $0.endIndex - $0.startIndex)
            let substring = String(item.text.value[range])
            
            guard substring == text, let attributeAction = $0.action else { return }
            
            if case let .deeplink(action) = attributeAction.data {
                delegate?.didSelectText(text, deeplinkUrl: action.url)
            }
        }
    }
}
