import UI
import UIKit

protocol SocialActionDisplaying: AnyObject {
    func configureLikeButton(title: String, icon: Iconography, color: Colors.Style)
    func configureCommentButton(title: String, icon: Iconography, color: Colors.Style)
    func configureBackgroundColorWhenNotExistsAction()
    func configureBackgroundColorWhenExistsAction()
}

private extension SocialActionView.Layout {
    enum Size {
        static let lineHeight = 1.0
    }
}

final class SocialActionView: UIView {
    fileprivate enum Layout { }
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale100.color
        return view
    }()
    
    private lazy var likeButton: IconographyButton = {
        let button = IconographyButton()
        button.addTarget(self, action: #selector(likeButtonTapped), for: .touchUpInside)
        return button
    }()
    
    private lazy var commentButton: IconographyButton = {
        let button = IconographyButton()
        button.addTarget(self, action: #selector(commentButtonTapped), for: .touchUpInside)
        return button
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [likeButton, commentButton])
        stackView.alignment = .center
        stackView.distribution = .fillEqually
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    weak var accessibilityDelegate: AccessibilityConfigurationDelegate?
    
    let interactor: SocialActionInteractor
    
    init(interactor: SocialActionInteractor) {
        self.interactor = interactor
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        interactor.loadData()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        interactor.updateBackgroundColorIfNeeded()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)

        guard #available(iOS 13, *), traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) else {
            return
        }
        
        interactor.updateBackgroundColorIfNeeded()
    }
    
    // MARK: - Private Methods
    @objc
    private func likeButtonTapped() {
        interactor.likeAction()
    }
    
    @objc
    private func commentButtonTapped() {
        interactor.commentAction()
    }
}

extension SocialActionView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(lineView)
        addSubview(stackView)
    }
    
    func setupConstraints() {
        lineView.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.lineHeight)
            $0.top.leading.trailing.equalToSuperview()
        }
        
        stackView.snp.makeConstraints {
            $0.top.equalTo(lineView.snp.bottom).offset(Spacing.base00)
            $0.leading.bottom.trailing.equalToSuperview()
        }
    }
}

extension SocialActionView: SocialActionDisplaying {
    func configureLikeButton(title: String, icon: Iconography, color: Colors.Style) {
        likeButton.properties = .init(title: title, iconName: icon, color: color)
        
        accessibilityDelegate?.accessibilityCustomAction(
            UIAccessibilityCustomAction(
                name: title,
                target: self,
                selector: #selector(likeButtonTapped)
            )
        )
    }
    
    func configureCommentButton(title: String, icon: Iconography, color: Colors.Style) {
        commentButton.properties = .init(title: title, iconName: icon, color: color)
        
        accessibilityDelegate?.accessibilityCustomAction(
            UIAccessibilityCustomAction(
                name: title,
                target: self,
                selector: #selector(commentButtonTapped)
            )
        )
    }
    
    func configureBackgroundColorWhenNotExistsAction() {
        configureBackgroundColorStyle(.groupedBackgroundTertiary())
    }
    
    func configureBackgroundColorWhenExistsAction() {
        configureBackgroundColorStyle(.groupedBackgroundSecondary())
    }
    
    // MARK: - Private Methods
    private func configureBackgroundColorStyle(_ colorStyle: Colors.Style) {
        backgroundColor = colorStyle.rawValue
    }
}
