import Foundation
import UI
import AssetsKit

protocol ActionLogPresenting: AnyObject {
    var view: ActionLogDisplaying? { get set }
    func configureItem(_ item: ActionLog)
    func updateBackgroundColorIfNeeded(for item: ActionLog, hasAction: Bool)
}

final class ActionLogPresenter {
    weak var view: ActionLogDisplaying?
}

// MARK: - ActionLogPresenting
extension ActionLogPresenter: ActionLogPresenting {
    func configureItem(_ item: ActionLog) {
        let ranges = item.text.attributes.compactMap {
            NSRange(location: $0.startIndex, length: $0.startIndex.distance(to: $0.endIndex))
        }
    
        view?.configureAttributesRanges(ranges)
        
        view?.configureDescription(item.text.value)
        
        configureImageStyles(for: item)
        
        if let firstImage = item.images.first {
            view?.configurePrimaryImage(firstImage.imageURL, imagePlaceholder: self.placeholderImage(firstImage.type))
        }
        
        if item.images.count > 1, let lastImage = item.images.last {
            view?.configureSecondaryImage(lastImage.imageURL, imagePlaceholder: self.placeholderImage(lastImage.type))
        }
    }
    
    func updateBackgroundColorIfNeeded(for item: ActionLog, hasAction: Bool) {
        configureImageStyles(for: item)
        
        guard hasAction else {
            view?.configureBackgroundColorWhenNotExistsAction()
            return
        }
        
        view?.configureBackgroundColorWhenExistsAction()
    }
        
    // MARK: - Helpers Private Methods
    private func configureImageStyles(for item: ActionLog) {
        if let firstImage = item.images.first {
            let geometricType: GeometricType = firstImage.shape == .circle ? .circle : .square
            view?.configurePrimaryImageStyle(GeometricImageStyle(type: geometricType, size: .small))
        }

        if item.images.count > 1, let lastImage = item.images.last {
            let geometricType: GeometricType = lastImage.shape == .circle ? .circle : .square
            view?.configureSecondaryImageStyle(GeometricImageStyle(type: geometricType, size: .small))
        }
    }
}

extension ActionLogPresenter: FeedPlaceholderRetriever { }
