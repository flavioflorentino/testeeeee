import UI
import UIKit

protocol ActionLogDisplaying: AnyObject {
    func configureDescription(_ text: String)
    func configurePrimaryImage(_ imageUrl: URL, imagePlaceholder: UIImage?)
    func configurePrimaryImageStyle<S: ImageStyle>(_ style: S)
    func configureSecondaryImage(_ imageUrl: URL, imagePlaceholder: UIImage?)
    func configureSecondaryImageStyle<S: ImageStyle>(_ style: S)
    func configureAttributesRanges(_ ranges: [NSRange])
    func configureBackgroundColorWhenNotExistsAction()
    func configureBackgroundColorWhenExistsAction()
}

private extension ActionLogView.Layout {
    enum Size {
        static let minimumHeight = Sizing.ImageView.small.rawValue.height
    }
}

final class ActionLogView: UIView {
    fileprivate enum Layout { }
    
    private lazy var primaryImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(primaryImageAction)
            )
        )
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    private lazy var secondaryImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(secondaryImageAction)
            )
        )
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    private lazy var descriptionLabel: InteractiveLabel = {
        let label = InteractiveLabel()
        label.delegate = self
        label.labelStyle(BodyPrimaryLabelStyle())
        return label
    }()

    let interactor: ActionLogInteracting
        
    init(interactor: ActionLogInteracting) {
        self.interactor = interactor
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        interactor.loadData()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        interactor.updateBackgroundColorIfNeeded()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        guard #available(iOS 13, *), traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) else {
            return
        }
        
        interactor.updateBackgroundColorIfNeeded()
    }
    
    // MARK: - Private Methods
    @objc
    private func primaryImageAction() {
        interactor.primaryImageAction()
    }
    
    @objc
    private func secondaryImageAction() {
        interactor.secondaryImageAction()
    }
}

extension ActionLogView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(descriptionLabel)
    }
    
    func setupConstraints() {
        setupDescriptionDefaultConstraints()
        
        descriptionLabel.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
        }
    }
}

extension ActionLogView: ActionLogDisplaying {
    func configureDescription(_ text: String) {
        descriptionLabel.text = text
    }
        
    func configurePrimaryImage(_ imageUrl: URL, imagePlaceholder: UIImage?) {
        primaryImageView.setImage(url: imageUrl, placeholder: imagePlaceholder)
        setupPrimaryImageConstraints()
    }
    
    func configurePrimaryImageStyle<S>(_ style: S) where S: ImageStyle {
        primaryImageView.imageStyle(style)
    }
    
    func configureSecondaryImage(_ imageUrl: URL, imagePlaceholder: UIImage?) {
        secondaryImageView.setImage(url: imageUrl, placeholder: imagePlaceholder)
        setupSecondaryImageConstraints()
    }
    
    func configureSecondaryImageStyle<S>(_ style: S) where S: ImageStyle {
        secondaryImageView.imageStyle(style)
    }
    
    func updateImageStyles<S>(_ style: S) where S: ImageStyle {
        primaryImageView.imageStyle(style)
        secondaryImageView.imageStyle(style)
    }
    
    func configureAttributesRanges(_ ranges: [NSRange]) {
        descriptionLabel.ranges = ranges
    }
    
    func configureBackgroundColorWhenNotExistsAction() {
        configureBackgroundColorStyle(.groupedBackgroundTertiary())
    }
    
    func configureBackgroundColorWhenExistsAction() {
        configureBackgroundColorStyle(.groupedBackgroundSecondary())
    }
    
    // MARK: - Helpers Private Methods
    private func configureBackgroundColorStyle(_ colorStyle: Colors.Style) {
        backgroundColor = colorStyle.rawValue
        descriptionLabel.backgroundColor = colorStyle.rawValue
    }
    
    private func setupPrimaryImageConstraints() {
        addSubview(primaryImageView)
        
        primaryImageView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.equalToSuperview()
        }
        
        descriptionLabel.snp.remakeConstraints {
            $0.leading.equalTo(primaryImageView.snp.trailing).offset(Spacing.base01)
        }
        
        setupDescriptionDefaultConstraints()
    }
    
    private func setupSecondaryImageConstraints() {
        addSubview(secondaryImageView)

        secondaryImageView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.equalTo(primaryImageView.snp.trailing).offset(-Spacing.base01)
        }
        
        descriptionLabel.snp.remakeConstraints {
            $0.leading.equalTo(secondaryImageView.snp.trailing).offset(Spacing.base01)
        }
        
        setupDescriptionDefaultConstraints()
    }
    
    private func setupDescriptionDefaultConstraints() {
        descriptionLabel.snp.makeConstraints {
            $0.height.greaterThanOrEqualTo(Layout.Size.minimumHeight)
            $0.top.trailing.bottom.equalToSuperview()
        }
    }
}

extension ActionLogView: InteractiveLabelDelegate {
    func didSelectTextWithinTheRanges(_ text: String) {
        interactor.didSelected(from: text)
    }
}
