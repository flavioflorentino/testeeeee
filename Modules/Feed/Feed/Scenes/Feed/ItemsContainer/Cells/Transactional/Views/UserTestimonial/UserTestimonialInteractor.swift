import Foundation

protocol UserTestimonialInteracting: AnyObject {
    func loadData()
    func updateBackgroundColorIfNeeded()
}

final class UserTestimonialInteractor {
    private let item: UserTestimonial
    private let presenter: UserTestimonialPresenting
    
    private let hasAction: Bool
    
    init(item: UserTestimonial, presenter: UserTestimonialPresenting, hasAction: Bool) {
        self.item = item
        self.presenter = presenter
        self.hasAction = hasAction
    }
}

extension UserTestimonialInteractor: UserTestimonialInteracting {
    func loadData() {
        presenter.configureItem(item)
    }
    
    func updateBackgroundColorIfNeeded() {
        presenter.updateBackgroundColorIfNeeded(hasAction)
    }
}
