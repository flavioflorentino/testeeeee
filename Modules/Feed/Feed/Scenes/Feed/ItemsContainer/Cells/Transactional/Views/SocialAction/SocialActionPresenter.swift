import Foundation
import UI

protocol SocialActionPresenting: AnyObject {
    var view: SocialActionDisplaying? { get set }
    func configureItem(_ item: SocialAction)
    func refreshItem(liked: Bool, newLikeCount: Int)
    func updateBackgroundColorIfNeeded(_ hasAction: Bool)
}

final class SocialActionPresenter {
    weak var view: SocialActionDisplaying?
}

// MARK: - SocialActionPresenting
extension SocialActionPresenter: SocialActionPresenting {
    func configureItem(_ item: SocialAction) {
        view?.configureLikeButton(
            title: item.liked ? Strings.liked(item.likeCount) : Strings.like(item.likeCount),
            icon: Iconography.heart,
            color: item.liked ? .branding600() : .grayscale400()
        )

        view?.configureCommentButton(title: Strings.comment(item.commentCount),
                                     icon: Iconography.commentAlt,
                                     color: item.commented ? .branding600() : .grayscale400())
    }
    
    func refreshItem(liked: Bool, newLikeCount: Int) {
        view?.configureLikeButton(
            title: liked ? Strings.liked(newLikeCount) : Strings.like(newLikeCount),
            icon: Iconography.heart,
            color: liked ? .branding600() : .grayscale400()
        )
    }
    
    func updateBackgroundColorIfNeeded(_ hasAction: Bool) {
        guard hasAction else {
            view?.configureBackgroundColorWhenNotExistsAction()
            return
        }
        
        view?.configureBackgroundColorWhenExistsAction()
    }
}
