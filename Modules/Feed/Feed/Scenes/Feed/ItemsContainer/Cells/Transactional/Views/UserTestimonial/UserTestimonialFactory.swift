import Foundation
import UIKit

enum UserTestimonialFactory {
    static func make(item: UserTestimonial, hasAction: Bool = true) -> UserTestimonialView {
        let presenter = UserTestimonialPresenter()
        let interactor = UserTestimonialInteractor(item: item, presenter: presenter, hasAction: hasAction)
        let view = UserTestimonialView(interactor: interactor)
        
        presenter.view = view
        
        return view
    }
}
