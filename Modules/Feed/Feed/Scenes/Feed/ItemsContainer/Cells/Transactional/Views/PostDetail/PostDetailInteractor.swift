import Foundation

protocol PostDetailInteracting: AnyObject {
    func loadData()
    func updateBackgroundColorIfNeeded()
}

final class PostDetailInteractor {
    private let item: PostDetail
    private let presenter: PostDetailPresenting
    
    private let hasAction: Bool
    
    init(item: PostDetail, presenter: PostDetailPresenting, hasAction: Bool) {
        self.item = item
        self.presenter = presenter
        self.hasAction = hasAction
    }
}

extension PostDetailInteractor: PostDetailInteracting {
    func loadData() {
        presenter.configureItem(item)
    }
    
    func updateBackgroundColorIfNeeded() {
        presenter.updateBackgroundColorIfNeeded(hasAction)
    }
}
