import Core
import Foundation

enum TransactionalServiceEndpoint {
    case like(cardId: String)
    case unlike(cardId: String)
}

extension TransactionalServiceEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case let .like(cardId), let .unlike(cardId):
            return "social-feed/cards/\(cardId)/like"
        }
    }

    var method: HTTPMethod {
        switch self {
        case .like:
            return .post
        case .unlike:
            return .delete
        }
    }
}
