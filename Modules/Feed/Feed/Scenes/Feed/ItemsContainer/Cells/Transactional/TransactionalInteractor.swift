import Foundation
import AnalyticsModule

protocol TransactionalInteracting: AnyObject {
    func setupView()
    func action()
    func updateBackgroundColorIfNeeded()
    func configureAccessibility()
}

final class TransactionalInteractor {
    typealias Dependencies = HasAnalytics

    private let item: Item
    private let cardPosition: Int
    private let presenter: TransactionalPresenting
    private let service: TransactionalServicing
    private let dependencies: Dependencies

    private weak var actionable: Actionable?

    init(
        item: Item,
        cardPosition: Int,
        presenter: TransactionalPresenting,
        service: TransactionalServicing,
        dependencies: Dependencies,
        actionable: Actionable?
    ) {
        self.item = item
        self.cardPosition = cardPosition
        self.presenter = presenter
        self.service = service
        self.dependencies = dependencies
        self.actionable = actionable
    }
}

extension TransactionalInteractor: TransactionalInteracting {
    func setupView() {
        let hasAction = item.action != nil

        if let actionLog = item.actionLog {
            presenter.createActionLogView(from: actionLog, hasAction: hasAction, delegate: self)
        }

        if let userTestimonial = item.userTestimonial {
            presenter.createUserTestimonialView(from: userTestimonial, hasAction: hasAction)
        }

        if let additionalInfo = item.additionalInfo {
            presenter.createAdditionalInfoView(from: additionalInfo, hasAction: hasAction, actionable: actionable, delegate: self)
        }

        if let postDetail = item.postDetail {
            presenter.createPostDetailView(from: postDetail, hasAction: hasAction)
        }

        if let socialAction = item.socialAction {
            presenter.createSocialActionView(from: socialAction, hasAction: hasAction, delegate: self)
        }
    }

    func action() {
        guard let containerAction = item.action else { return }
        
        if case let .deeplink(action) = containerAction.data {
            let event = FeedAnalytics.cardTapped(
                cardInfo: .init(
                    id: item.id.uuidString,
                    position: cardPosition,
                    itemType: .transactional,
                    deeplink: action.url.absoluteString
                )
            )
            
            dependencies.analytics.log(event)
            actionable?.deeplink(action.url)
        }

        if case let .screen(action) = containerAction.data, case .transactionDetail = action.type {
            let event = FeedAnalytics.cardTapped(
                cardInfo: .init(
                    id: item.id.uuidString,
                    position: cardPosition,
                    itemType: .transactional
                )
            )
            
            dependencies.analytics.log(event)
            actionable?.screen(for: .transactionDetail(id: item.id))
        }
    }

    func updateBackgroundColorIfNeeded() {
        guard item.action != nil else {
            presenter.configureBackgroundColorWhenNotExistsAction()
            return
        }

        presenter.configureBackgroundColorWhenExistsAction()
    }

    func configureAccessibility() {
        presenter.configureAccessibility(
            actionLog: item.actionLog,
            userTestimonial: item.userTestimonial,
            additionalInfo: item.additionalInfo,
            postDetail: item.postDetail
        )
    }
}

// MARK: - ActionLogDelegate
extension TransactionalInteractor: ActionLogDelegate {
    func didTouchPrimaryImage(for deeplinkUrl: URL) {
        actionable?.deeplink(deeplinkUrl)
    }

    func didTouchSecondaryImage(for deeplinkUrl: URL) {
        actionable?.deeplink(deeplinkUrl)
    }
}

// MARK: - AdditionalInfoDelegate
extension TransactionalInteractor: AdditionalInfoDelegate {
    func didTouchPrimaryAction(for text: String, deeplinkUrl: URL) {
        trackingButtonTapped(for: text, urlString: deeplinkUrl.absoluteString)
        actionable?.deeplink(deeplinkUrl)
    }

    func didTouchSecondaryAction(for text: String, deeplinkUrl: URL) {
        trackingButtonTapped(for: text, urlString: deeplinkUrl.absoluteString)
        actionable?.deeplink(deeplinkUrl)
    }

    func didTouchContainerAction(for text: String, deeplinkUrl: URL) {
        trackingButtonTapped(for: text, urlString: deeplinkUrl.absoluteString)
        actionable?.deeplink(deeplinkUrl)
    }

    // The method is implemented in AdditionalInfoDelegate and ActionLogDelegate
    func didSelectText(_ text: String, deeplinkUrl: URL) {
        actionable?.deeplink(deeplinkUrl)
    }

    // MARK: - Private Methods
    private func trackingButtonTapped(for text: String, urlString: String) {
        let event = FeedAnalytics.cardButtonTapped(
            buttonInfo: .init(name: text, hyperlink: urlString),
            cardInfo: .init(id: item.id.uuidString, position: cardPosition, itemType: item.type)
        )

        dependencies.analytics.log(event)
    }
}

extension TransactionalInteractor: SocialActionDelegate {
    func didTouchLiked(numberOfLikes: Int, isLike: Bool, uiSocialActionUpdate: UISocialActionUpdating) {
        service.likeOrUnlike(cardId: item.id.uuidString, isLike: isLike) { [weak self] _ in
            self?.didUpdateLike(isLike, numberOfLikes: numberOfLikes, uiSocialActionUpdate: uiSocialActionUpdate)
        }
    }

    func didTouchCommented() {
        actionable?.screen(for: .transactionDetail(id: item.id))
    }

    // MARK: - Private Methods
    private func didUpdateLike(_ isLike: Bool, numberOfLikes: Int, uiSocialActionUpdate: UISocialActionUpdating) {        
        let event = FeedAnalytics.cardLikeInteracted(
            interactionType: isLike ? .like : .dislike,
            cardInfo: .init(id: item.id.uuidString, position: cardPosition, itemType: item.type)
        )

        dependencies.analytics.log(event)

        var referenceUserLike = isLike
        referenceUserLike.toggle()

        let newNumberOfLikes = referenceUserLike ? numberOfLikes + 1 : numberOfLikes - 1

        uiSocialActionUpdate.updateLiked(newNumberOfLikes: newNumberOfLikes, isLike: referenceUserLike)
    }
}
