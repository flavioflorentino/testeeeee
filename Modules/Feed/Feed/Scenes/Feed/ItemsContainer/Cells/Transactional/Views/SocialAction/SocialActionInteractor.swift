import Foundation

protocol SocialActionInteracting: AnyObject {
    func loadData()
    func likeAction()
    func commentAction()
    func updateBackgroundColorIfNeeded()
}

final class SocialActionInteractor {
    private let item: SocialAction
    private let presenter: SocialActionPresenting
    private let hasAction: Bool
    
    private weak var delegate: SocialActionDelegate?
    
    init(item: SocialAction, presenter: SocialActionPresenting, hasAction: Bool, delegate: SocialActionDelegate?) {
        self.item = item
        self.presenter = presenter
        self.hasAction = hasAction
        self.delegate = delegate
    }
}

// MARK: - SocialActionInteracting
extension SocialActionInteractor: SocialActionInteracting {
    func loadData() {
        presenter.configureItem(item)
    }
    
    func likeAction() {
        delegate?.didTouchLiked(numberOfLikes: item.likeCount, isLike: item.liked, uiSocialActionUpdate: self)
    }
    
    func commentAction() {
        delegate?.didTouchCommented()
    }
    
    func updateBackgroundColorIfNeeded() {
        presenter.updateBackgroundColorIfNeeded(hasAction)
    }
}

extension SocialActionInteractor: UISocialActionUpdating {
    func updateLiked(newNumberOfLikes: Int, isLike: Bool) {
        presenter.refreshItem(liked: isLike, newLikeCount: newNumberOfLikes)
    }
}
