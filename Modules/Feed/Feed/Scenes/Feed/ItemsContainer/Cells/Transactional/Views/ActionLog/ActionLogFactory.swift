import UIKit

protocol ActionLogDelegate: AnyObject {
    func didTouchPrimaryImage(for deeplinkUrl: URL)
    func didTouchSecondaryImage(for deeplinkUrl: URL)
    func didSelectText(_ text: String, deeplinkUrl: URL)
}

enum ActionLogFactory {
    static func make(item: ActionLog, hasAction: Bool = true, delegate: ActionLogDelegate? = nil) -> ActionLogView {
        let presenter: ActionLogPresenting = ActionLogPresenter()
        let interactor = ActionLogInteractor(item: item, presenter: presenter, hasAction: hasAction, delegate: delegate)
        let view = ActionLogView(interactor: interactor)

        presenter.view = view
        
        return view
    }
}
