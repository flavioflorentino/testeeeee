import UI
import UIKit

protocol PostDetailDisplaying: AnyObject {
    func configureTimeAgo(_ text: String)
    func configureCurrency(_ currency: String, color: UIColor)
    func configurePrivacy(_ image: UIImage?, text: String)
    func configureBackgroundColorWhenNotExistsAction()
    func configureBackgroundColorWhenExistsAction()
}

private extension PostDetailView.Layout {
    enum Size {
        static let privacyImage = CGSize(width: 16.0, height: 16.0)
    }
}

final class PostDetailView: UIView {
    fileprivate enum Layout { }
    
    private lazy var currencyLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.numberOfLines, 1)
        return label
    }()
    
    private lazy var timestampLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale400())
            .with(\.numberOfLines, 1)
        return label
    }()
    
    private lazy var privacyImageView = UIImageView()
    
    private lazy var privacyLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.grayscale400.color)
            .with(\.numberOfLines, 1)
        return label
    }()
    
    let interactor: PostDetailInteracting
    
    init(interactor: PostDetailInteracting) {
        self.interactor = interactor
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        interactor.loadData()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        interactor.updateBackgroundColorIfNeeded()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)

        guard #available(iOS 13, *), traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) else {
            return
        }
        
        interactor.updateBackgroundColorIfNeeded()
    }
}

extension PostDetailView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(currencyLabel)
        addSubview(timestampLabel)
        addSubview(privacyImageView)
        addSubview(privacyLabel)
    }
    
    func setupConstraints() {
        currencyLabel.snp.makeConstraints {
            $0.top.leading.bottom.equalToSuperview()
        }
        
        timestampLabel.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.equalTo(currencyLabel.snp.trailing).offset(Spacing.base00)
            $0.trailing.equalTo(privacyImageView.snp.leading).offset(-Spacing.base00)
        }
        
        privacyImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.privacyImage)
            $0.trailing.equalTo(privacyLabel.snp.leading).offset(-Spacing.base01)
            $0.trailing.equalToSuperview().inset(Spacing.base02).priority(.medium)
            $0.centerY.equalToSuperview()
        }
        
        privacyLabel.snp.makeConstraints {
            $0.top.trailing.bottom.equalToSuperview()
        }
        
        currencyLabel.setContentHuggingPriority(.required, for: .horizontal)
    }
}

extension PostDetailView: PostDetailDisplaying {
    func configureTimeAgo(_ text: String) {
        timestampLabel.text = text
    }
    
    func configureCurrency(_ currency: String, color: UIColor) {
        currencyLabel.text = currency
        currencyLabel.textColor = color
    }
    
    func configurePrivacy(_ image: UIImage?, text: String) {
        privacyImageView.image = image
        privacyLabel.text = text
    }
    
    func configureBackgroundColorWhenNotExistsAction() {
        configureBackgroundColorStyle(.groupedBackgroundTertiary())
    }
    
    func configureBackgroundColorWhenExistsAction() {
        configureBackgroundColorStyle(.groupedBackgroundSecondary())
    }
    
    // MARK: - Private Methods
    private func configureBackgroundColorStyle(_ colorStyle: Colors.Style) {
        backgroundColor = colorStyle.rawValue
        currencyLabel.backgroundColor = colorStyle.rawValue
        timestampLabel.backgroundColor = colorStyle.rawValue
        privacyLabel.backgroundColor = colorStyle.rawValue
        privacyImageView.backgroundColor = colorStyle.rawValue
    }
}
