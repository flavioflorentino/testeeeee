import SnapKit
import UI
import UIKit

protocol TransactionalDisplaying: AnyObject {
    func configureView(_ uiView: UIView)
    func configureAccessibilityLabel(_ label: String)
    func configureBackgroundColorWhenNotExistsAction()
    func configureBackgroundColorWhenExistsAction()
    func configureAccessibilityCustomActions(_ customActions: [UIAccessibilityCustomAction])
}

final class TransactionalViewCell: UITableViewCell {
    private lazy var containerView: CardView = {
        let view = CardView()
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(containerAction)))
        view.isUserInteractionEnabled = true
        return view
    }()
    
    private lazy var roundedContainerView: UIView = {
        let view = UIView()
        view
            .viewStyle(RoundedViewStyle(cornerRadius: .medium))
            .with(\.clipsToBounds, true)
        return view
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private var interactor: TransactionalInteracting?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
        setupAccessibility()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        stackView.arrangedSubviews.forEach {
            stackView.removeArrangedSubview($0)
            $0.removeFromSuperview()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        interactor?.updateBackgroundColorIfNeeded()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)

        guard #available(iOS 13, *), traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) else {
            return
        }
        
        interactor?.updateBackgroundColorIfNeeded()
    }
    
    func setupInteractor(_ interactor: TransactionalInteracting) {
        self.interactor = interactor
        self.interactor?.setupView()
        self.interactor?.configureAccessibility()
    }
    
    // MARK: - Private Methods
    @objc
    private func containerAction() {
        interactor?.action()
    }
}

extension TransactionalViewCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(containerView)
        containerView.addSubview(roundedContainerView)
        roundedContainerView.addSubview(stackView)
    }
    
    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.top.bottom.equalToSuperview().inset(Spacing.base01)
        }
        
        roundedContainerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        stackView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base02).priority(.high)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureViews() {
        selectionStyle = .none
        contentView.backgroundColor = Colors.backgroundPrimary.color
    }
}

extension TransactionalViewCell: TransactionalDisplaying {
    func configureView(_ uiView: UIView) {
        stackView.addArrangedSubview(uiView)
    }
    
    func configureAccessibilityLabel(_ label: String) {
        containerView.accessibilityLabel = label
    }
    
    func configureBackgroundColorWhenNotExistsAction() {
        configureBackgroundColorStyle(.groupedBackgroundTertiary())
    }
    
    func configureBackgroundColorWhenExistsAction() {
        configureBackgroundColorStyle(.groupedBackgroundSecondary())
    }
    
    func configureAccessibilityCustomActions(_ customActions: [UIAccessibilityCustomAction]) {
        containerView.accessibilityCustomActions = customActions
    }
    
    // MARK: - Private Methods
    private func configureBackgroundColorStyle(_ colorStyle: Colors.Style) {
        containerView.setBackgroundColor(colorStyle.rawValue)
    }
}

extension TransactionalViewCell: AccessibilityConfiguration {
    func buildAccessibilityElements() {
        contentView.isAccessibilityElement = false
        containerView.isAccessibilityElement = true
    }
}
