import UIKit
import UI

protocol AdditionalInfoDelegate: AnyObject {
    func didTouchPrimaryAction(for text: String, deeplinkUrl: URL)
    func didTouchSecondaryAction(for text: String, deeplinkUrl: URL)
    func didTouchContainerAction(for text: String, deeplinkUrl: URL)
    func didSelectText(_ text: String, deeplinkUrl: URL)
}

enum AdditionalInfoFactory {
    static func make(item: AdditionalInfo, hasAction: Bool = true, delegate: AdditionalInfoDelegate? = nil) -> AdditionalInfoView {
        let presenter = AdditionalInfoPresenter()
        let interactor = AdditionalInfoInteractor(
            item: item,
            presenter: presenter,
            hasAction: hasAction,
            delegate: delegate
        )
        
        let view = AdditionalInfoView(interactor: interactor)
        
        presenter.view = view
        
        return view
    }
}
