import Foundation
import UI

protocol PostDetailPresenting: AnyObject {
    var view: PostDetailDisplaying? { get set }
    func configureItem(_ item: PostDetail)
    func updateBackgroundColorIfNeeded(_ hasAction: Bool)
}

final class PostDetailPresenter {
    typealias Dependencies = HasCurrentDate & HasTimeZone
    
    weak var view: PostDetailDisplaying?
    
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension PostDetailPresenter: PostDetailPresenting {
    func configureItem(_ item: PostDetail) {
        let image = UIImage(
            iconName: item.privacy == .private ? Iconography.lock.rawValue : Iconography.eye.rawValue,
            font: Typography.icons(.large).font(),
            color: Colors.grayscale400.color,
            size: Sizing.ImageView.xSmall.rawValue
        )
        
        let timeAgoSinceNow = item.date.timeAgoSince(
            dependencies.currentDate,
            dateFormat: "dd MMM",
            timeZone: dependencies.timeZone
        )
        
        view?.configureCurrency(item.currency.value, color: item.currency.type.color)
        view?.configureTimeAgo("\(Strings.separatorDate) \(timeAgoSinceNow)")
        view?.configurePrivacy(image, text: item.privacy == .private ? Strings.private : Strings.public)
    }
    
    func updateBackgroundColorIfNeeded(_ hasAction: Bool) {
        guard hasAction else {
            view?.configureBackgroundColorWhenNotExistsAction()
            return
        }
        
        view?.configureBackgroundColorWhenExistsAction()
    }
}
