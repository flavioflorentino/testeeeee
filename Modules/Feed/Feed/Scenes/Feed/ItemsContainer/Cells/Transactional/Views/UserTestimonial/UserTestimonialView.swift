import SnapKit
import UI
import UIKit

protocol UserTestimonialDisplaying: AnyObject {
    func configureStyle<S: UI.LabelStyle>(_ style: S)
    func configureDescription(_ attributedString: NSAttributedString)
    func configureBackgroundColorWhenNotExistsAction()
    func configureBackgroundColorWhenExistsAction()
}

final class UserTestimonialView: UIView {
    private lazy var textLabel: UILabel = {
        let label = UILabel()
        return label
    }()

    let interactor: UserTestimonialInteracting
    
    init(interactor: UserTestimonialInteracting) {
        self.interactor = interactor
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        interactor.loadData()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        interactor.updateBackgroundColorIfNeeded()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)

        guard #available(iOS 13, *), traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) else {
            return
        }
        
        interactor.updateBackgroundColorIfNeeded()
    }
}

extension UserTestimonialView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(textLabel)
    }
    
    func setupConstraints() {
        textLabel.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}

extension UserTestimonialView: UserTestimonialDisplaying {
    func configureDescription(_ attributedString: NSAttributedString) {
        textLabel.attributedText = attributedString
    }
    
    func configureStyle<S>(_ style: S) where S: UI.LabelStyle {
        textLabel
            .labelStyle(style)
            .with(\.textColor, .grayscale600())
    }
    
    func configureBackgroundColorWhenNotExistsAction() {
        configureBackgroundColorStyle(.groupedBackgroundTertiary())
    }
    
    func configureBackgroundColorWhenExistsAction() {
        configureBackgroundColorStyle(.groupedBackgroundSecondary())
    }
    
    // MARK: - Private Methods
    private func configureBackgroundColorStyle(_ colorStyle: Colors.Style) {
        backgroundColor = colorStyle.rawValue
        textLabel.backgroundColor = colorStyle.rawValue
    }
}
