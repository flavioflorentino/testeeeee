import Core
import Foundation
import UIKit

protocol TransactionalServicing: AnyObject {
    func likeOrUnlike(cardId: String, isLike: Bool, completion: @escaping (Result<NoContent, ApiError>) -> Void)
}

final class TransactionalService {
    typealias Dependencies = HasMainQueue
    
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - TransactionalServicing
extension TransactionalService: TransactionalServicing {
    func likeOrUnlike(cardId: String, isLike: Bool, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        let endpoint: TransactionalServiceEndpoint = isLike ? .like(cardId: cardId) : .unlike(cardId: cardId)
        let request = Api<NoContent>(endpoint: endpoint)
        
        request.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
