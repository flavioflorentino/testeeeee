import UIKit

enum PostDetailFactory {
    static func make(item: PostDetail, hasAction: Bool = true) -> PostDetailView {
        let container = DependencyContainer()
        let presenter = PostDetailPresenter(dependencies: container)
        let interactor = PostDetailInteractor(item: item, presenter: presenter, hasAction: hasAction)
        let view = PostDetailView(interactor: interactor)
        
        presenter.view = view
        
        return view
    }
}
