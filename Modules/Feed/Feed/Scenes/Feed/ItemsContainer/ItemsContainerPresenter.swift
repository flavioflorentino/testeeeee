import AnalyticsModule
import Core
import FeatureFlag
import Foundation

enum LoadingType {
    case full
    case footer
}

protocol ItemsContainerPresenting: AnyObject {
    var viewController: ItemsContainerDisplaying? { get set }
    func initialItems(_ items: [Item])
    func addItems(_ items: [Item])
    func scrollsToTop()
    func starting(loading: LoadingType)
    func stoping(loading: LoadingType)
    func endRefreshing()
    func showErrorMoreItems()
    func showNotMoreItemsExists()
    func showLoadingMoreItems()
    func showEmptyState()
    func showUnableToLoadActivities()
    func showWithoutInternetConnection()
}

final class ItemsContainerPresenter {
    typealias Dependencies =
        HasAnalytics &
        HasCurrentDate &
        HasFeatureManager &
        HasMainQueue &
        HasNotificationCenter &
        HasTimeZone
    
    private let emptyStateConfiguration: FeedbackStateConfigurable
    private let dependencies: Dependencies
    private let actionable: Actionable?
    
    weak var viewController: ItemsContainerDisplaying?
    
    init(emptyStateConfiguration: FeedbackStateConfigurable, dependencies: Dependencies, actionable: Actionable?) {
        self.emptyStateConfiguration = emptyStateConfiguration
        self.dependencies = dependencies
        self.actionable = actionable
    }
}

extension ItemsContainerPresenter: ItemsContainerPresenting {
    func endRefreshing() {
        viewController?.endRefreshing()
    }
    
    func initialItems(_ items: [Item]) {
        let itemsDataSource = createItemsContainerDataSources(from: items)
        
        viewController?.restoreInitialState()
        viewController?.initialItems(itemsDataSource)
    }
    
    func addItems(_ items: [Item]) {
        let itemsDataSource = createItemsContainerDataSources(from: items)
        viewController?.addItems(itemsDataSource)
    }
    
    func scrollsToTop() {
        dependencies.notificationCenter.post(name: .homeScrollToTop, object: nil)
    }
    
    func starting(loading: LoadingType) {
        switch loading {
        case .full:
            viewController?.startLoading()
        case .footer:
            viewController?.startFooterLoading()
        }
    }
    
    func stoping(loading: LoadingType) {
        switch loading {
        case .full:
            viewController?.stopLoading()
        case .footer:
            viewController?.stopFooterLoading()
        }
    }
    
    func showNotMoreItemsExists() {
        viewController?.updateFeedbackFooter(for: .noContent)
    }

    func showErrorMoreItems() {
        viewController?.updateFeedbackFooter(for: .error)
    }
    
    func showLoadingMoreItems() {
        viewController?.updateFeedbackFooter(for: .loading)
    }
    
    func showEmptyState() {
        viewController?.showEmptyState(from: emptyStateConfiguration)
    }

    func showUnableToLoadActivities() {
        viewController?.showErrorState(from: FeedbackErrorConfiguration.unableToLoadActivities)
    }
    
    func showWithoutInternetConnection() {
        viewController?.showErrorState(from: FeedbackErrorConfiguration.withoutInternetConnection)
    }
}

// MARK: - Private Methods
private extension ItemsContainerPresenter {
    func createItemsContainerDataSources(from items: [Item]) -> [ItemsContainerDataSource] {
        items.enumerated().lazy.map { index, item in
            switch item.type {
            case .transactional:
                return configureTransactionalInItemsContainerDataSource(from: item, index: index)
            case .informational:
                return configureInformationalInItemsContainerDataSource(from: item, index: index)
            case .carousel:
                return configureCarouselInItemsContainerDataSource(from: item, index: index)
            }
        }
    }
    
    func configureTransactionalInItemsContainerDataSource(from item: Item, index: Int) -> ItemsContainerDataSource {
        let presenter = TransactionalPresenter(dependencies: dependencies)
        let service = TransactionalService(dependencies: dependencies)
        let interactor = TransactionalInteractor(
            item: item,
            cardPosition: index,
            presenter: presenter,
            service: service,
            dependencies: dependencies,
            actionable: actionable
        )
        
        return .transactional(interactor, presenter)
    }
    
    func configureInformationalInItemsContainerDataSource(from item: Item, index: Int) -> ItemsContainerDataSource {
        guard let banner = item.banners.first else { return .unknown }
        
        let viewModel = InformationalViewModel(
            cardId: item.id,
            banner: banner,
            cardPosition: index,
            dependencies: dependencies,
            actionable: actionable
        )
        
        return .informational(viewModel)
    }
    
    func configureCarouselInItemsContainerDataSource(from item: Item, index: Int) -> ItemsContainerDataSource {
        let viewModel = CarouselViewModel(
            cardId: item.id,
            banners: item.banners,
            cardPosition: index,
            dependencies: dependencies,
            actionable: actionable
        )
        
        return .carousel(viewModel)
    }
}
