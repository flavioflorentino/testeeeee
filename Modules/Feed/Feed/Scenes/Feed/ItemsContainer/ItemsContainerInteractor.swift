import Core
import Foundation
import AnalyticsModule

enum ItemsContainerDataSource {
    case transactional(TransactionalInteractor, TransactionalPresenter)
    case informational(InformationalViewModel)
    case carousel(CarouselViewModel)
    case unknown
}

protocol ItemsContainerInteracting: AnyObject {
    func loadData()
    func refreshData()
    func loadMoreData()
    func prefetchingDataIfNeeded(from indexPaths: [IndexPath])
    
    func willDisplayCell(at index: Int)
    
    func footerAction(from state: FeedbackState?)
    func feedbackAction(from configuration: FeedbackStateConfigurable)
}

final class ItemsContainerInteractor {
    typealias Dependencies = HasAnalytics
    
    private let presenter: ItemsContainerPresenting
    private let service: ItemsContainerServicing
    private let dependencies: Dependencies
    private let isOpenFollowersAndFollowings: Bool
    
    private var isNeededLoadData = true
    private var lastItem: Int?
    private var totalItems = [Item]()
        
    private var prefetchingData = [Item]()
    
    init(
        presenter: ItemsContainerPresenting,
        service: ItemsContainerServicing,
        dependencies: Dependencies,
        isOpenFollowersAndFollowings: Bool
    ) {
        self.presenter = presenter
        self.service = service
        self.dependencies = dependencies
        self.isOpenFollowersAndFollowings = isOpenFollowersAndFollowings
    }
}

extension ItemsContainerInteractor: ItemsContainerInteracting {
    func loadData() {
        guard isOpenFollowersAndFollowings else {
            presenter.showEmptyState()
            return
        }
        
        lastItem = nil
        presenter.starting(loading: .full)
        
        service.listItems(lastItem: lastItem) { [weak self] result in
            self?.presenter.stoping(loading: .full)
            self?.processResultAndPresentItems(result)
        }
    }
    
    func refreshData() {
        lastItem = nil
        
        service.listItems(lastItem: lastItem) { [weak self] result in
            self?.presenter.endRefreshing()
            self?.processResultAndPresentItems(result)
        }
    }
    
    func loadMoreData() {
        guard isNeededLoadData else { return }
        
        guard prefetchingData.isEmpty else {
            presenter.addItems(prefetchingData)
            prefetchingData.removeAll()
            return
        }
        
        isNeededLoadData = false
        presenter.starting(loading: .footer)
        
        service.listItems(lastItem: lastItem) { [weak self] result in
            self?.presenter.stoping(loading: .footer)
            switch result {
            case .success(let value) where value.data.isEmpty:
                self?.isNeededLoadData = false
                self?.presenter.showNotMoreItemsExists()
            case .success(let value):
                self?.isNeededLoadData = true
                self?.lastItem = value.lastItem
                self?.totalItems.append(contentsOf: value.data)
                self?.presenter.addItems(value.data)
            case .failure:
                self?.isNeededLoadData = false
                self?.presenter.showErrorMoreItems()
            }
        }
    }
    
    func prefetchingDataIfNeeded(from indexPaths: [IndexPath]) {
        guard indexPaths.contains(where: indexPathIsGreater) else { return }
        
        presenter.starting(loading: .footer)
        
        service.listItems(lastItem: lastItem) { [weak self] result in
            self?.presenter.stoping(loading: .footer)
            switch result {
            case .success(let value) where value.data.isEmpty:
                self?.presenter.showNotMoreItemsExists()
            case .success(let value):
                self?.lastItem = value.lastItem
                self?.totalItems.append(contentsOf: value.data)
                self?.prefetchingData = value.data
            case .failure:
                self?.isNeededLoadData = false
                self?.presenter.showErrorMoreItems()
            }
        }
    }
    
    func willDisplayCell(at index: Int) {
        guard totalItems.indices.contains(index) else { return }
        let referenceItem = totalItems[index]
        
        guard referenceItem.type != .carousel else {
            return
        }
        
        let event = FeedAnalytics.cardViewed(
            cardInfo: .init(
                id: referenceItem.id.uuidString,
                position: index,
                itemType: referenceItem.type
            )
        )
        
        dependencies.analytics.log(event)
    }
    
    func footerAction(from state: FeedbackState?) {
        guard let newState = state else { return }
        
        switch newState {
        case .noContent:
            presenter.scrollsToTop()
        case .error:
            retryLoadMoreData()
        case .loading:
            presenter.showLoadingMoreItems()
        }
    }
    
    func feedbackAction(from configuration: FeedbackStateConfigurable) {
        if configuration is FeedbackErrorConfiguration {
            loadData()
        }
    }
}

// MARK: - Private Methods
private extension ItemsContainerInteractor {
    func processResultAndPresentItems(_ result: Result<ItemData, ApiError>) {
        switch result {
        case .success(let value) where value.data.isEmpty:
            presenter.showEmptyState()
        case .success(let value):
            lastItem = value.lastItem
            totalItems = value.data
            presenter.initialItems(value.data)
        case .failure(let error):
            buildError(from: error)
        }
    }
    
    func retryLoadMoreData() {
        presenter.starting(loading: .footer)

        service.listItems(lastItem: lastItem) { [weak self] result in
            self?.presenter.stoping(loading: .footer)
            switch result {
            case .success(let value):
                self?.presenter.addItems(value.data)
                self?.isNeededLoadData = true
            case .failure:
                self?.presenter.showErrorMoreItems()
            }
        }
    }
    
    func indexPathIsGreater(from indexPath: IndexPath) -> Bool {
        indexPath.row >= (totalItems.count - 1)
    }
    
    func buildError(from error: ApiError) {
        guard case .connectionFailure = error else {
            return presenter.showUnableToLoadActivities()
        }
        
        presenter.showWithoutInternetConnection()
    }
}
