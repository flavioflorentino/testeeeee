import Foundation
import UIKit

protocol FeedbackStateDelegate: AnyObject {
    func primaryAction(from configuration: FeedbackStateConfigurable)
}

enum FeedbackStateFactory {
    static func make(for configuration: FeedbackStateConfigurable, delegate: FeedbackStateDelegate?) -> FeedbackStateView {
        let presenter: FeedbackStatePresenting = FeedbackStatePresenter()
        let interactor = FeedbackStateInteractor(configuration: configuration, presenter: presenter, delegate: delegate)
        let view = FeedbackStateView(interactor: interactor)
        
        presenter.view = view
        
        return view
    }
}
