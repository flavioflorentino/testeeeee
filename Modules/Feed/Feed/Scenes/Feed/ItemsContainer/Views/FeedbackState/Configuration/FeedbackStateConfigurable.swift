import Foundation
import UIKit

protocol FeedbackStateConfigurable {
    var image: UIImage? { get }
    var title: String { get }
    var description: String { get }
    var buttonTitle: String? { get }
}
