import AssetsKit
import Foundation
import UIKit

enum FeedbackEmptyConfiguration {
    typealias Localizable = Strings.FeedbackState.Empty
    
    case thereIsNoActivityHereYet
    case thisProfileHasNoActivityYet
    case payBillsAndRechargeCellPhone
    case closedAccount
}

extension FeedbackEmptyConfiguration: FeedbackStateConfigurable {
    var image: UIImage? {
        switch self {
        case .thereIsNoActivityHereYet, .thisProfileHasNoActivityYet, .payBillsAndRechargeCellPhone:
            return Resources.Illustrations.iluSocialNew.image
        case .closedAccount:
            return Resources.Illustrations.iluHoldingPadlock.image
        }
    }
    
    var title: String {
        switch self {
        case .thereIsNoActivityHereYet, .thisProfileHasNoActivityYet, .payBillsAndRechargeCellPhone:
            return Localizable.noActivity
        case .closedAccount:
            return Localizable.closedAccountTitle
        }
    }
    
    var description: String {
        switch self {
        case .thereIsNoActivityHereYet:
            return Localizable.thereIsNoActivityHereYet
        case .thisProfileHasNoActivityYet:
            return Localizable.thisProfileHasNoActivityYet
        case .payBillsAndRechargeCellPhone:
            return Localizable.payBillsAndRechargeCellPhone
        case .closedAccount:
            return Localizable.closedAccountDescription
        }
    }
    
    var buttonTitle: String? {
        nil
    }
}
