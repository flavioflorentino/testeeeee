import Foundation

protocol FeedbackStatePresenting: AnyObject {
    var view: FeedbackStateDisplaying? { get set }
    func configureState(_ configuration: FeedbackStateConfigurable)
}

final class FeedbackStatePresenter {
    weak var view: FeedbackStateDisplaying?
}

// MARK: - FeedbackStatePresenting
extension FeedbackStatePresenter: FeedbackStatePresenting {
    func configureState(_ configuration: FeedbackStateConfigurable) {
        view?.configureImage(configuration.image)
        view?.configureTitle(configuration.title)
        view?.configureDescription(configuration.description)
        view?.configureButtonTitle(configuration.buttonTitle)
    }
}
