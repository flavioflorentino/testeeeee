import AssetsKit
import Foundation
import UIKit

enum FeedbackErrorConfiguration {
    typealias Localizable = Strings.FeedbackState.Error
    
    case unableToLoadActivities
    case withoutInternetConnection
}

extension FeedbackErrorConfiguration: FeedbackStateConfigurable {
    var image: UIImage? {
        switch self {
        case .unableToLoadActivities:
            return Resources.Illustrations.iluFalling.image
        case .withoutInternetConnection:
            return Resources.Illustrations.iluNoConnection.image
        }
    }
    
    var title: String {
        switch self {
        case .unableToLoadActivities:
            return Localizable.UnableToLoadActivities.title
        case .withoutInternetConnection:
            return Localizable.WithoutInternetConnection.title
        }
    }
    
    var description: String {
        switch self {
        case .unableToLoadActivities:
            return Localizable.UnableToLoadActivities.description
        case .withoutInternetConnection:
            return Localizable.WithoutInternetConnection.description
        }
    }
    
    var buttonTitle: String? {
        Localizable.buttonTitle
    }
}
