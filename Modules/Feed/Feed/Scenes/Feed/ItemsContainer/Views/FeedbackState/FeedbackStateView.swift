import SnapKit
import UI
import UIKit

protocol FeedbackStateDisplaying: AnyObject {
    func configureImage(_ image: UIImage?)
    func configureTitle(_ title: String)
    func configureDescription(_ description: String)
    func configureButtonTitle(_ title: String?)
}

final class FeedbackStateView: UIView {
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textAlignment, .center)
            .with(\.textColor, Colors.grayscale700.color)
        return label
    }()
    
    private lazy var actionButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(pressActionButton), for: .touchUpInside)
        return button
    }()
    
    let interactor: FeedbackStateInteracting
    
    init(interactor: FeedbackStateInteracting) {
        self.interactor = interactor
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        interactor.loadState()
    }
    
    @objc
    private func pressActionButton() {
        interactor.handleAction()
    }
}

extension FeedbackStateView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(imageView)
        addSubview(titleLabel)
        addSubview(descriptionLabel)
        addSubview(actionButton)
    }
    
    func setupConstraints() {
        imageView.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.size.equalTo(120)
            $0.top.equalToSuperview().offset(Spacing.base03)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        actionButton.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.bottom.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
}

extension FeedbackStateView: FeedbackStateDisplaying {
    func configureImage(_ image: UIImage?) {
        imageView.image = image
    }
    
    func configureTitle(_ title: String) {
        titleLabel.text = title
    }
    
    func configureDescription(_ description: String) {
        descriptionLabel.text = description
    }
    
    func configureButtonTitle(_ title: String?) {
        actionButton.setTitle(title, for: .normal)
        actionButton.buttonStyle(LinkButtonStyle(size: .small))
    }
}
