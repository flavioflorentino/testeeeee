import Foundation

protocol FeedbackStateInteracting: AnyObject {
    func loadState()
    func handleAction()
}

final class FeedbackStateInteractor {
    private let presenter: FeedbackStatePresenting
    private let configuration: FeedbackStateConfigurable
    
    private weak var delegate: FeedbackStateDelegate?
    
    init(configuration: FeedbackStateConfigurable, presenter: FeedbackStatePresenting, delegate: FeedbackStateDelegate?) {
        self.configuration = configuration
        self.presenter = presenter
        self.delegate = delegate
    }
}

extension FeedbackStateInteractor: FeedbackStateInteracting {
    func loadState() {
        presenter.configureState(configuration)
    }
    
    func handleAction() {
        delegate?.primaryAction(from: configuration)
    }
}
