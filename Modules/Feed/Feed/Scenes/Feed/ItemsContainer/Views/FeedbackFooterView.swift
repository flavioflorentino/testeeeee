import UI
import UIKit

enum FeedbackState {
    case loading
    case noContent
    case error
    
    var image: UIImage? {
        let size = CGSize(width: 24.0, height: 24.0)
        let edgeInsets = UIEdgeInsets(top: Spacing.base00, left: .zero, bottom: .zero, right: .zero)
        
        switch self {
        case .error:
            return UIImage(
                iconName: Iconography.confused.rawValue,
                color: Colors.grayscale400.color,
                size: size,
                edgeInsets: edgeInsets
            )
        case .noContent:
            return UIImage(
                iconName: Iconography.smile.rawValue,
                color: Colors.grayscale400.color,
                size: size,
                edgeInsets: edgeInsets
            )
        default:
            return nil
        }
    }
    
    var message: String? {
        switch self {
        case .error:
            return Strings.Feedback.problemWereUnableToLoadAllActivities
        case .noContent:
            return Strings.Feedback.seenAllOfYourActivities
        default:
            return nil
        }
    }
    
    var actionTitle: String? {
        switch self {
        case .error:
            return Strings.Feedback.tryAgain
        case .noContent:
            return Strings.Feedback.backToTop
        default:
            return nil
        }
    }
}

protocol FeedbackFooterDelegate: AnyObject {
    func handleAction(for state: FeedbackState?)
}

private extension FeedbackFooterView.Layout {
    enum Size {
        static let indicatorHeight: CGFloat = 80.0
    }
}

final class FeedbackFooterView: UIView {
    fileprivate enum Layout { }
        
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale400())
            .with(\.textAlignment, .center)
        return label
    }()

    private lazy var actionButton: UIButton = {
        let button = UIButton(type: .custom)
        button.addTarget(self, action: #selector(pressAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var activityIndicatorView: UIActivityIndicatorView = {
        let indicatorView = UIActivityIndicatorView()
        indicatorView.color = Colors.branding600.color
        return indicatorView
    }()
    
    weak var delegate: FeedbackFooterDelegate?
    
    var state: FeedbackState? {
        didSet {
            updateState()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private Methods
    private func updateState() {
        let isNoContentOrError = state == .noContent || state == .error
        updateElementState(isNoContentOrError: isNoContentOrError)
        
        if isNoContentOrError {
            imageView.image = state?.image
            messageLabel.text = state?.message
            actionButton.setTitle(state?.actionTitle, for: .normal)
            actionButton.buttonStyle(LinkButtonStyle(size: .small))
        }
        
        if case .loading = state {
            activityIndicatorView.startAnimating()
        }
    }
    
    private func updateElementState(isNoContentOrError: Bool) {
        imageView.isHidden = !isNoContentOrError
        messageLabel.isHidden = !isNoContentOrError
        actionButton.isHidden = !isNoContentOrError
        activityIndicatorView.isHidden = isNoContentOrError
    }
    
    @objc
    private func pressAction() {
        delegate?.handleAction(for: state)
    }
}

extension FeedbackFooterView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(imageView)
        addSubview(messageLabel)
        addSubview(actionButton)
        addSubview(activityIndicatorView)
    }
    
    func setupConstraints() {
        imageView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.centerX.equalToSuperview()
            $0.size.equalTo(Sizing.ImageView.xSmall.rawValue)
        }

        messageLabel.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        actionButton.snp.makeConstraints {
            $0.top.equalTo(messageLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalToSuperview().offset(-Spacing.base02)
        }

        activityIndicatorView.snp.makeConstraints {
            $0.top.greaterThanOrEqualToSuperview()
            $0.bottom.lessThanOrEqualToSuperview()
            $0.center.equalToSuperview()
            $0.size.equalTo(Layout.Size.indicatorHeight)
        }
    }
}
