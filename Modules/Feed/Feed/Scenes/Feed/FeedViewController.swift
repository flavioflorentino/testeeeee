import UI
import UIKit

public protocol FeedComponentAdaptering where Self: UIViewController {
    var scrollViews: [UIScrollView] { get }
    var scrollView: UIScrollView? { get }
    var pinnedView: UIView? { get }
    
    func refresh(_ finish: @escaping () -> Void)
}

public final class FeedViewController: UIViewController {
    // MARK: - Private properties
    private lazy var headerView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.backgroundPrimary.color
        return view
    }()
    
    private lazy var activityLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.activities
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
        return label
    }()
    
    private lazy var optionsSegmentControl: UISegmentedControl = {
        let view = UISegmentedControl(items: [Strings.all, Strings.mine])
        
        view.setTitleTextAttributes(
            [.font: Typography.bodySecondary().font(), .foregroundColor: Colors.grayscale600.color],
            for: .normal
        )
        
        view.setTitleTextAttributes(
            [.font: Typography.bodySecondary().font(), .foregroundColor: Colors.white.lightColor],
            for: .selected
        )
        
        view.addTarget(self, action: #selector(changeValue(_:)), for: .valueChanged)
        
        if #available(iOS 13.0, *) {
            view.selectedSegmentTintColor = Colors.branding600.color
        }
        
        return view
    }()
    
    private lazy var pageViewController: UIPageViewController = {
        let controller = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal)
        controller.delegate = self
        return controller
    }()
    
    private lazy var dataSource = PageViewDataSource<ItemsContainerViewController>(viewController: pageViewController)
    
    private let viewModel: FeedViewModeling
    
    // MARK: - Override Methods
    init(viewModel: FeedViewModeling) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        buildLayout()
        pageViewController.dataSource = dataSource
        viewModel.inputs.configure()
    }
    
    // MARK: - Private Methods
    @objc
    private func changeValue(_ sender: UISegmentedControl) {
        viewModel.inputs.changeValue(sender.selectedSegmentIndex)
    }
}

// MARK: - ViewConfiguration
extension FeedViewController: ViewConfiguration {
    public func buildViewHierarchy() {
        pageViewController.willMove(toParent: self)
        view.addSubview(pageViewController.view)
        
        addChild(pageViewController)
        pageViewController.didMove(toParent: self)
    }
    
    public func setupConstraints() {
        pageViewController.view.snp.makeConstraints {
            $0.top.equalToSuperview().priority(.medium)
            $0.leading.bottom.trailing.equalToSuperview()
        }
    }
    
    public func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}

// MARK: - FeedViewModelOutputs
extension FeedViewController: FeedViewModelOutputs {
    func configurePages(_ viewControllers: [UIViewController]) {
        dataSource.add(viewControllers: viewControllers)
    }
    
    func configureIndex(_ index: Int) {
        optionsSegmentControl.selectedSegmentIndex = index
    }
    
    func configureHeader() {
        view.addSubview(headerView)
        headerView.addSubview(activityLabel)
        headerView.addSubview(optionsSegmentControl)

        headerView.snp.makeConstraints {
            $0.height.equalTo(Spacing.base06)
            $0.top.equalTo(view.compatibleSafeArea.top)
            $0.leading.trailing.equalToSuperview()
        }

        activityLabel.snp.makeConstraints {
            $0.leading.equalToSuperview().inset(Spacing.base02)
            $0.centerY.equalToSuperview()
        }

        optionsSegmentControl.snp.makeConstraints {
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.centerY.equalToSuperview()
        }

        pageViewController.view.snp.makeConstraints {
            $0.top.equalTo(headerView.snp.bottom).priority(.high)
        }
    }
    
    func nextPage(_ index: Int) {
        dataSource.next(from: index)
    }
    
    func previousPage(_ index: Int) {
        dataSource.previous(from: index)
    }
}

// MARK: - UIPageViewControllerDelegate
extension FeedViewController: UIPageViewControllerDelegate {
    public func pageViewController(
        _ pageViewController: UIPageViewController,
        didFinishAnimating finished: Bool,
        previousViewControllers: [UIViewController],
        transitionCompleted completed: Bool
    ) {
        guard
            let visibleViewController = pageViewController.viewControllers?.first,
            let index = dataSource.data.firstIndex(of: visibleViewController) else {
            return
        }
        
        viewModel.inputs.setIndex(index)
    }
}

// MARK: - FeedComponentAdaptering
extension FeedViewController: FeedComponentAdaptering {
    public var scrollViews: [UIScrollView] {
        dataSource.data.compactMap { $0 as? ItemsContainerTableViewing }.map { $0.tableItems }
    }

    public var scrollView: UIScrollView? {
        (dataSource.data[optionsSegmentControl.selectedSegmentIndex] as? ItemsContainerTableViewing)?.tableItems
    }
    
    public var pinnedView: UIView? { headerView }
    
    public func refresh(_ finish: @escaping () -> Void) {
        dataSource.data.lazy.compactMap {
            $0 as? ItemsContainerTableViewing
        }
        .forEach {
            $0.reload()
        }
        
        finish()
    }
}
