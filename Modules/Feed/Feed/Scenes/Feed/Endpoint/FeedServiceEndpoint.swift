import Core

enum FeedServiceEndpoint {
    case everyone(lastItem: Int? = nil)
    case personal(lastItem: Int? = nil)
    case profile(lastItem: Int? = nil, consumerId: String)
}

extension FeedServiceEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .everyone:
            return "social-feed/social"
        case .personal:
            return "social-feed/personal"
        case let .profile(_, consumerId):
            return "social-feed/personal/\(consumerId)"
        }
    }
    
    var parameters: [String: Any] {
        switch self {
        case
            let .everyone(lastItem),
            let .personal(lastItem):
            
            guard let newLastItem = lastItem else {
                return [:]
            }
            
            return [
                "lastItem": newLastItem
            ]
            
        case let .profile(lastItem, _):
            var params: [String: Any] = [
                "privacy": "PUBLIC"
            ]
            
            guard let newLastItem = lastItem else {
                return params
            }
            
            params["lastItem"] = newLastItem
            
            return params
        }
    }
}
