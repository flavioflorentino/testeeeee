import Core
import Foundation
import AnalyticsModule
import FeatureFlag

private struct ItemContainerHelper {
    let service: ItemsContainerServicing
    let presenter: ItemsContainerPresenting
    let isOpenFollowersAndFollowings: Bool
    
    init(service: ItemsContainerServicing,
         presenter: ItemsContainerPresenting,
         isOpenFollowersAndFollowings: Bool = true) {
        self.service = service
        self.presenter = presenter
        self.isOpenFollowersAndFollowings = isOpenFollowersAndFollowings
    }
}

protocol FeedViewModelInputs: AnyObject {
    func configure()
    func changeValue(_ index: Int)
    func setIndex(_ index: Int)
}

protocol FeedViewModelOutputs: AnyObject {
    func configurePages(_ viewControllers: [UIViewController])
    func configureIndex(_ index: Int)
    func configureHeader()
    func nextPage(_ index: Int)
    func previousPage(_ index: Int)
}

protocol FeedViewModeling: AnyObject {
    var inputs: FeedViewModelInputs { get }
    var outputs: FeedViewModelOutputs? { get set }
}

final class FeedViewModel: FeedViewModeling {
    typealias Dependencies =
        HasAnalytics &
        HasCurrentDate &
        HasDeeplinkManager &
        HasFeatureManager &
        HasMainQueue &
        HasNotificationCenter &
        HasTimeZone
    
    var inputs: FeedViewModelInputs { self }
    weak var outputs: FeedViewModelOutputs?
    
    private let options: FeedOptions
    private let adapter: LegacyBannerAdaptering
    private let dependencies: Dependencies
    private let actionable: ScreenActionable?
    private let hasRefresh: Bool
    
    private var currentIndex = 0
    
    init(
        options: FeedOptions,
        adapter: LegacyBannerAdaptering,
        dependencies: Dependencies,
        actionable: ScreenActionable?,
        hasRefresh: Bool = false
    ) {
        self.options = options
        self.adapter = adapter
        self.actionable = actionable
        self.dependencies = dependencies
        self.hasRefresh = hasRefresh
    }
}

extension FeedViewModel: FeedViewModelInputs {
    func configure() {
        let itemsContainers = configureItemsContainer(from: options)
        
        let viewControllers = itemsContainers.map { value -> UIViewController in
            let viewController = ItemsContainerFactory.make(
                service: value.service,
                presenter: value.presenter,
                isOpenFollowersAndFollowings: value.isOpenFollowersAndFollowings
            )
            (viewController as? ItemsContainerTableViewing)?.isRefreshControlEnable = hasRefresh
            return viewController
        }
        
        if case .mainOptions = options {
            outputs?.configureHeader()
        }
        
        outputs?.configureIndex(currentIndex)
        outputs?.configurePages(viewControllers)
    }
    
    func changeValue(_ index: Int) {
        if index > currentIndex {
            outputs?.nextPage(index)
        }
        
        if index < currentIndex {
            outputs?.previousPage(index)
        }
        
        currentIndex = index
        sendTapChangeAnalyticsEvent(index: index)
    }
    
    func setIndex(_ index: Int) {
        currentIndex = index
        outputs?.configureIndex(currentIndex)
        
        sendTapChangeAnalyticsEvent(index: index)
    }
}

// MARK: - Private Extension
private extension FeedViewModel {
    func configureItemsContainerForMainOptions() -> [ItemContainerHelper] {
        let everyoneService = ItemsEveryoneContainerService(adapter: adapter, dependencies: dependencies)
        let everyonePresenter = ItemsContainerPresenter(
            emptyStateConfiguration: FeedbackEmptyConfiguration.payBillsAndRechargeCellPhone,
            dependencies: dependencies,
            actionable: self
        )
        
        let personalService = ItemsPersonalContainerService(dependencies: dependencies)
        let personalPresenter = ItemsContainerPresenter(
            emptyStateConfiguration: FeedbackEmptyConfiguration.payBillsAndRechargeCellPhone,
            dependencies: dependencies,
            actionable: self
        )
        
        return [
            ItemContainerHelper(service: everyoneService, presenter: everyonePresenter),
            ItemContainerHelper(service: personalService, presenter: personalPresenter)
        ]
    }
    
    func configureItemsContainerForProfile(
        _ profileId: String?,
        _ isOpenFollowersAndFollowings: Bool
    ) -> [ItemContainerHelper] {
        let emptyStateConfiguration: FeedbackEmptyConfiguration
        
        if profileId == nil {
            emptyStateConfiguration = FeedbackEmptyConfiguration.thereIsNoActivityHereYet
        } else if isOpenFollowersAndFollowings {
            emptyStateConfiguration = FeedbackEmptyConfiguration.thisProfileHasNoActivityYet
        } else {
            emptyStateConfiguration = FeedbackEmptyConfiguration.closedAccount
        }
        
        let profileService = ItemsProfileContainerService(consumerId: profileId ?? "", dependencies: dependencies)
        let profilePresenter = ItemsContainerPresenter(
            emptyStateConfiguration: emptyStateConfiguration,
            dependencies: dependencies,
            actionable: self
        )
        
        return [
            ItemContainerHelper(
                service: profileService,
                presenter: profilePresenter,
                isOpenFollowersAndFollowings: isOpenFollowersAndFollowings
            )
        ]
    }
    
    func configureItemsContainer(from options: FeedOptions) -> [ItemContainerHelper] {
        switch options {
        case .mainOptions:
            return configureItemsContainerForMainOptions()
        case let .profile(profileId, isOpenFollowersAndFollowings):
            return configureItemsContainerForProfile(profileId, isOpenFollowersAndFollowings)
        }
    }
    
    func sendTapChangeAnalyticsEvent(index: Int) {
        guard let currentTab = getTabAt(index: index) else {
            return
        }
        
        dependencies.analytics.log(FeedAnalytics.tabViewed(tabType: currentTab))
    }
    
    func getTabAt(index: Int) -> FeedAnalytics.TabType? {
        let tabsType = FeedAnalytics.TabType.allCases
        if case .mainOptions = options, index < tabsType.count, index >= 0 {
            return tabsType[index]
        }

        return nil
    }
}

extension FeedViewModel: Actionable {
    func screen(for type: ActionScreenType) {
        actionable?.screen(for: type)
    }
    
    func deeplink(_ url: URL) {
        dependencies.deeplinkManager.open(url: url)
    }
}
