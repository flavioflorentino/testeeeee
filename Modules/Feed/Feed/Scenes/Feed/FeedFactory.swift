import Foundation

public enum ActionScreenType: Equatable {
    case transactionDetail(id: UUID)
    case abuseReportDetail(type: AbuseReportType, id: UUID)
}

public enum FeedOptions {
    case mainOptions
    case profile(profileId: String?, isOpenProfile: Bool)
}

public enum FeedFactory {
    public static func make(
        options: FeedOptions,
        hasRefresh: Bool,
        actionable: ScreenActionable?
    ) -> FeedViewController {
        let container = DependencyContainer()
        
        let bannerService: LegacyBannerServicing = LegacyBannerService(dependencies: container)
        let bannerAdapter: LegacyBannerAdaptering = LegacyBannerAdapter(service: bannerService)
        
        let viewModel = FeedViewModel(
            options: options,
            adapter: bannerAdapter,
            dependencies: container,
            actionable: actionable,
            hasRefresh: hasRefresh
        )
        let viewController = FeedViewController(viewModel: viewModel)
        
        viewModel.outputs = viewController
        
        return viewController
    }
}
