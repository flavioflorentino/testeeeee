import UI
import UIKit
import AssetsKit
import AccessibilityKit

protocol ItemDetailDisplaying: AnyObject {
    func configureItemDetail(_ itemDataSource: [ItemDetailDataSource])
    func startLoading()
    func stopLoading()
    func showError(with title: String, and description: String, and buttonTitle: String)
    func showOptions(_ options: [OptionAction])
    func showPopupConfirmation(
        title: String,
        description: String,
        cancelButtonTitle: String,
        confirmationButtonTitle: String,
        optionType: OptionType
    )
    func showPopupNotification(title: String, description: String, genericButtonTitle: String)
    func setCommentAvatar(with url: URL?, and placeholder: UIImage)
    func showSnackbarError(with message: String)
    func addNewItem(_ dataSourceItem: ItemDetailDataSource)
    func refreshComment(_ dataSourceItem: ItemDetailDataSource, _ index: Int)
    func showDialog(_ image: UIImage?, title: String?, message: String?, primaryButtonAction: ApolloAlertAction)
    func showAbuseOptions(title: String, message: String, abuseOptions: [Abuse])
    func enableSendCommentButton(_ isEnabled: Bool)
}

private extension ItemDetailViewController.Layout {
    enum Size {
        static let estimatedRowHeight: CGFloat = 60
        static let abuseImageSize = CGSize(width: 48.0, height: 48.0)
    }
    
    enum Image {
        static let abuseImage = UIImage(
            iconName: Iconography.infoCircle.rawValue,
            font: Typography.icons(.large).font(),
            color: Colors.white.lightColor,
            size: Size.abuseImageSize,
            edgeInsets: UIEdgeInsets(
                top: Spacing.base00,
                left: .zero,
                bottom: .zero,
                right: .zero
            )
        )
    }
}

final class ItemDetailViewController: ViewController<ItemDetailInteracting, UIView> {
    private typealias Localizable = Strings.ItemDetail
    
    fileprivate enum Layout { }
    
    // MARK: - Private properties
    private let keyboardHandler = KeyboardHandler(notificationCenter: NotificationCenter.default)
    private let MAX_CHARS = 999
    
    private lazy var containerView = UIView()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.showsVerticalScrollIndicator = false
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = Layout.Size.estimatedRowHeight
        
        tableView.register(HeaderItemDetailViewCell.self, forCellReuseIdentifier: HeaderItemDetailViewCell.identifier)
        tableView.register(CommentViewCell.self, forCellReuseIdentifier: CommentViewCell.identifier)
        return tableView
    }()
    
    private lazy var tableViewDataSource: TableViewDataSource<Int, ItemDetailDataSource> = {
        let dataSource = TableViewDataSource<Int, ItemDetailDataSource>(view: tableView)
        dataSource.automaticReloadData = false
        
        dataSource.itemProvider = { [weak self] tableView, indexPath, item -> UITableViewCell? in
            self?.configureItemProvider(tableView, indexPath: indexPath, item: item)
        }
        
        return dataSource
    }()
    
    // swiftlint:disable:next weak_delegate
    private lazy var tableViewDelegate: TableViewDelegate = {
        let delegate = TableViewDelegate()
        delegate.editActionsProvider = { _, indexPath -> [TableViewDelegate.EditActionConfiguration]? in
            guard indexPath.row > 0 else { return nil }
            
            return [
                (style: .destructive, title: nil, image: Layout.Image.abuseImage, completion: {
                    self.interactor.handleOption(.init(type: .reportContent))
                })
            ]
        }
        return delegate
    }()

    private lazy var separatorView = SeparatorView(style: BackgroundViewStyle(color: .grayscale100()))
    
    private lazy var commentInput: CommentInputView = {
        let input = CommentInputView(inputValidator: .maxCharaceters(MAX_CHARS))
        input.delegate = self
        return input
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        keyboardHandler.onHeightChangeCompletion = { keyboardHeight in
            self.view.layoutIfNeeded()
            self.commentInput.snp.updateConstraints {
                $0.bottom.equalTo(self.view.compatibleSafeArea.bottom).inset(keyboardHeight)
            }
            self.view.layoutIfNeeded()
        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tapGesture)
        
        tableView.delegate = tableViewDelegate
        tableView.dataSource = tableViewDataSource
        interactor.fetchItemDetail()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        interactor.userOpennedScreen()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        interactor.userClosedScreen()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func buildViewHierarchy() {
        view.addSubviews(containerView, commentInput, separatorView)
        containerView.addSubview(tableView)
    }
    
    override func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(separatorView.snp.top)
        }
        
        tableView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.leading.bottom.trailing.equalToSuperview()
        }
        
        separatorView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(commentInput.snp.top).inset(-Spacing.base01)
        }
        
        commentInput.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(view.compatibleSafeArea.bottom)
        }
    }

    override func configureViews() {
        title = Localizable.title
        view.backgroundColor = Colors.groupedBackgroundSecondary.color
        
        let optionsButton = UIBarButtonItem(
            title: Localizable.options,
            style: .plain,
            target: self,
            action: #selector(getOptions)
        )
        
        navigationItem.rightBarButtonItem = optionsButton
        navigationController?.navigationBar.navigationStyle(StandardNavigationStyle())
    }
}

// MARK: - Private extension
private extension ItemDetailViewController {
    func configureItemProvider(_ tableView: UITableView, indexPath: IndexPath, item: ItemDetailDataSource) -> UITableViewCell? {
        switch item {
        case let .header(presenter, interactor):
            return buildHeaderItemDetailCell(indexPath, presenter, interactor)
        case let .comment(presenter, interactor):
            return buildCommentCell(indexPath, presenter, interactor)
        }
    }
    
    func buildCommentCell(
        _ indexPath: IndexPath,
        _ presenter: CommentPresenting,
        _ interactor: CommentInteracting
    ) -> UITableViewCell? {
        let identifier = CommentViewCell.identifier
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        
        guard let commentCell = cell as? CommentViewCell else { return cell }
        presenter.view = commentCell
        interactor.delegate = self
        commentCell.resetCell()
        commentCell.setupInteractor(interactor)
        
        return commentCell
    }
    
    func buildHeaderItemDetailCell(
        _ indexPath: IndexPath,
        _ presenter: HeaderItemDetailPresenting,
        _ interactor: HeaderItemDetailInteracting
    ) -> UITableViewCell {
        let identifier = HeaderItemDetailViewCell.identifier
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        
        guard let headerCell = cell as? HeaderItemDetailViewCell else { return cell }
        presenter.view = headerCell
        headerCell.setupInteractor(interactor)
        
        return headerCell
    }
}

// MARK: - ItemDetailDisplaying
extension ItemDetailViewController: ItemDetailDisplaying {
    func configureItemDetail(_ itemDataSource: [ItemDetailDataSource]) {
        tableViewDataSource.remove(section: 0)
        tableViewDataSource.add(items: itemDataSource, to: 0)
        tableView.reloadData()
    }
    
    func addNewItem(_ dataSourceItem: ItemDetailDataSource) {
        tableViewDataSource.add(item: dataSourceItem, to: 0)
        tableView.reloadData()
        tableView.scrollToBottom()
    }
    func refreshComment(_ dataSourceItem: ItemDetailDataSource, _ index: Int) {
        tableViewDataSource.removeItem(from: 0, at: index)
        tableViewDataSource.add(item: dataSourceItem, at: index, in: 0)
        tableView.reloadData()
    }
    
    func startLoading() {
        beginState()
    }

    func stopLoading() {
        endState()
    }
    
    func showAbuseOptions(title: String, message: String, abuseOptions: [Abuse]) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        abuseOptions.forEach { option in
            let alertAction = UIAlertAction(title: option.text, style: .default) { [weak self] _ in
                self?.interactor.handleAction(option.action)
            }
            
            alertController.addAction(alertAction)
        }
        
        let cancelAction = UIAlertAction(title: Localizable.cancel, style: .cancel)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true)
    }
    
    func showError(with title: String, and description: String, and buttonTitle: String) {
        let primaryAction = ApolloAlertAction(title: buttonTitle, completion: interactor.dismiss)
        showApolloAlert(
            title: title,
            subtitle: description,
            primaryButtonAction: primaryAction,
            dismissOnTouchOutside: false
        )
    }
    
    func showOptions(_ options: [OptionAction]) {
        let optionsAlert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        options.forEach { option in
            let style: UIAlertAction.Style = option.type == .reportContent ? .destructive : .default
            let alertAction = UIAlertAction(title: option.label, style: style) { [weak self] _ in
                self?.interactor.handleOption(option)
            }

            optionsAlert.addAction(alertAction)
        }
        
        let cancelAction = UIAlertAction(title: Localizable.cancel, style: .cancel)
        optionsAlert.addAction(cancelAction)

        present(optionsAlert, animated: true)
    }
    
    func showPopupConfirmation(
        title: String,
        description: String,
        cancelButtonTitle: String,
        confirmationButtonTitle: String,
        optionType: OptionType
    ) {
        let alert = UIAlertController(title: title, message: description, preferredStyle: .alert)
        
        let confirmationAction = UIAlertAction(title: confirmationButtonTitle, style: .default, handler: { _ in
            self.interactor.handleConfirmation(with: optionType)
        })
        alert.addAction(confirmationAction)
        
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .cancel)
        alert.addAction(cancelAction)
        present(alert, animated: true)
    }
    
    func showPopupNotification(title: String, description: String, genericButtonTitle: String) {
        let alert = UIAlertController(title: title, message: description, preferredStyle: .alert)
        let okAction = UIAlertAction(title: genericButtonTitle, style: .cancel)
        alert.addAction(okAction)
        present(alert, animated: true)
    }
    
    func setCommentAvatar(with url: URL?, and placeholder: UIImage) {
        commentInput.photo = (url: url, placeholder: placeholder)
    }
    
    func showSnackbarError(with message: String) {
        let apolloSnackbar = ApolloSnackbar(
            text: message,
            iconType: .error,
            emphasis: .high
        )
        showSnackbar(apolloSnackbar, onTopOf: containerView, duration: 3.0)
        AccessibilityManager.shared.announcement(from: .announcement, value: message)
    }
    
    func showDialog(_ image: UIImage?, title: String?, message: String?, primaryButtonAction: ApolloAlertAction) {
        showApolloAlert(image: image, title: title, subtitle: message, primaryButtonAction: primaryButtonAction)
    }
    
    func enableSendCommentButton(_ isEnabled: Bool) {
        commentInput.isSendButtonEnabled = isEnabled
    }
}

extension ItemDetailViewController: CommentInputViewDelegate {
    func sendButtonTapped(with text: String?) {
        guard let text = text else { return }
        dismissKeyboard()
        interactor.addNewComment(text)
        commentInput.text = ""
    }
    
    func inputTextChanged(to text: String?) {
        guard let text = text else { return }
        
        commentInput.isSendButtonEnabled = text.isNotEmpty
        
        if text.count >= MAX_CHARS {
            commentInput.isSendButtonEnabled = false
            interactor.loadTooLongCommentError()
        }
    }
}

// MARK: CommentInteractingDelegate
extension ItemDetailViewController: CommentInteractingDelegate {
    func retrySendComment(_ comment: Comment, at index: Int) {
        interactor.retrySendComment(comment, index: index)
    }
}

@objc
extension ItemDetailViewController {
    private func getOptions() {
        interactor.loadOptions()
    }

    func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension ItemDetailViewController: StatefulTransitionViewing { }
