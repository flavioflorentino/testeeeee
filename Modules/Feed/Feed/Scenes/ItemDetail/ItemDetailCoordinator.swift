import UIKit

enum ReceiptType: String {
    case p2p
    case pav
    case store
    case pix
    case pixReceiver
}

protocol ItemDetailCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: ItemDetailDelegate? { get set }
    func dismiss()
    func openReceipt(_ id: String, type: ReceiptType)
    func openPixRefundFlow(_ id: String)
}

final class ItemDetailCoordinator {
    typealias Dependencies = HasReceiptManager
    
    private let dependencies: Dependencies
    
    weak var viewController: UIViewController?
    weak var delegate: ItemDetailDelegate?
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - ItemDetailCoordinating
extension ItemDetailCoordinator: ItemDetailCoordinating {
    func dismiss() {
       viewController?.navigationController?.popToRootViewController(animated: true)
    }
    
    func openReceipt(_ id: String, type: ReceiptType) {
        dependencies.receiptManager.open(withId: id, receiptType: type.rawValue)
    }
    
    func openPixRefundFlow(_ id: String) {
        delegate?.openPixRefundFlow(id)
    }
}
