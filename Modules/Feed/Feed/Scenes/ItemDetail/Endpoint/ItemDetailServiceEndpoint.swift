import Core
import Foundation

enum ItemDetailServiceEndpoint {
    case cardDetail(cardId: String)
    case makePrivate(cardId: String)
    case makeHidden(cardId: String)
    case followCard(cardId: String)
    case unfollowCard(cardId: String)
    case refundPayment(transactionId: String)
    case listAbuseOptions(cardId: String)
    case likeCard(cardId: String)
    case unlikeCard(cardId: String)
    case comment(cardId: String, comment: String)
}

extension ItemDetailServiceEndpoint: ApiEndpointExposable {    
    var path: String {
        switch self {
        case let .cardDetail(cardId):
            return "social-feed/cards/\(cardId)"
        case let .makePrivate(cardId):
            return "social-feed/cards/\(cardId)/privacy?privacy=PRIVATE"
        case let .makeHidden(cardId):
            return "social-feed/cards/\(cardId)/privacy?privacy=HIDDEN"
        case let .followCard(cardId), let .unfollowCard(cardId):
            return "social-feed/cards/\(cardId)/follow"
        case let .comment(cardId, _):
            return "social-feed/cards/\(cardId)/comment"
        case let .refundPayment(transactionId):
            return "payment/p2p/\(transactionId)"
        case let .likeCard(cardId), let .unlikeCard(cardId):
            return "social-feed/cards/\(cardId)/like"
        case let .listAbuseOptions(cardId):
            return "social-feed/abuse-options/\(cardId)"
        }
    }
    
    var body: Data? {
        guard case let .comment(_, comment) = self else {
            return nil
        }
        return ["comment": comment].toData()
    }
    
    var method: HTTPMethod {
        switch self {
        case .cardDetail, .listAbuseOptions:
            return .get
        case .makeHidden, .makePrivate:
            return .patch
        case .followCard, .refundPayment, .comment, .likeCard:
            return .post
        case .unfollowCard, .unlikeCard:
            return .delete
        }
    }
    
    var isTokenNeeded: Bool {
        false
    }
}
