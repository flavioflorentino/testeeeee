import AssetsKit
import Foundation
import UI

protocol ItemDetailPresenting: AnyObject {
    var viewController: ItemDetailDisplaying? { get set }
    func startLoading()
    func stopLoading()
    func configureItemDataSource(_ itemDataSource: [ItemDetailDataSource])
    func configureError()
    func dismiss()
    func configureOptions(for itemDetailOptions: [OptionType])
    func openReceipt(type: OptionType, id: String)
    func configureMakePrivateOption(type: OptionType)
    func configureHideTransactionOption(type: OptionType)
    func configureDisableNotificationOption(type: OptionType)
    func showDialog(enableNotification: Bool)
    func openPixReturnFlow(_ cardId: String)
    func showAbuseOptions(title: String, message: String, abuseOptions: [Abuse])
    func refundPaymentConfirmation(type: OptionType)
    func showOptionGenericError()
    func showRefundSuccess()
    func configureImageCommentInput(_ urlImage: URL?)
    func addComment(with item: ItemDetailDataSource)
    func refreshCommentList(with dataSourceItem: ItemDetailDataSource, at index: Int)
    func showSnackbarError(errorType: ItemDetailErrorType)
    func showAbuseSuccess()
    func enableSendCommentButton(_ isEnabled: Bool)
}

final class ItemDetailPresenter {
    typealias Localizable = Strings.ItemDetail
    
    private let coordinator: ItemDetailCoordinating
    
    weak var viewController: ItemDetailDisplaying?
    
    init(coordinator: ItemDetailCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - ItemDetailPresenting
extension ItemDetailPresenter: ItemDetailPresenting {
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }

    func configureItemDataSource(_ itemDataSource: [ItemDetailDataSource]) {
        viewController?.configureItemDetail(itemDataSource)
    }
    
    func configureError() {
        viewController?.showError(
            with: Localizable.popupErrorTitle,
            and: Localizable.popupErrorDescription,
            and: Strings.okIGotIt
        )
    }
    
    func dismiss() {
        coordinator.dismiss()
    }
    
    func configureOptions(for itemDetailOptions: [OptionType]) {
        guard itemDetailOptions.isNotEmpty else { return }
        
        let optionsAction = itemDetailOptions.map(OptionAction.init)
        
        viewController?.showOptions(optionsAction)
    }
    
    func configureMakePrivateOption(type: OptionType) {
        viewController?.showPopupConfirmation(
            title: Localizable.MakePrivateAlert.title,
            description: Localizable.MakePrivateAlert.description,
            cancelButtonTitle: Localizable.no,
            confirmationButtonTitle: Localizable.yes,
            optionType: type
        )
    }
    
    func configureHideTransactionOption(type: OptionType) {
        viewController?.showPopupConfirmation(
            title: Localizable.HideTransactionAlert.title,
            description: Localizable.HideTransactionAlert.description,
            cancelButtonTitle: Localizable.no,
            confirmationButtonTitle: Localizable.yes,
            optionType: type
        )
    }
    
    func configureDisableNotificationOption(type: OptionType) {
        viewController?.showPopupConfirmation(
            title: Localizable.DisableNotificationAlert.title,
            description: Localizable.DisableNotificationAlert.description,
            cancelButtonTitle: Localizable.no,
            confirmationButtonTitle: Localizable.yes,
            optionType: type
        )
    }
    
    func openReceipt(type: OptionType, id: String) {
        switch type {
        case .viewReceiptP2P:
            coordinator.openReceipt(id, type: .p2p)
        case .viewReceiptBiz:
            coordinator.openReceipt(id, type: .pav)
        case .viewReceiptStore:
            coordinator.openReceipt(id, type: .store)
        case .viewReceiptPixTransaction:
            coordinator.openReceipt(id, type: .pix)
        case .viewReceiptPixRefund:
            coordinator.openReceipt(id, type: .pixReceiver)
        default:
            return
        }
    }
    
    func openPixReturnFlow(_ cardId: String) {
        coordinator.openPixRefundFlow(cardId)
    }
    
    func refundPaymentConfirmation(type: OptionType) {
        viewController?.showPopupConfirmation(
            title: Localizable.RefundPayment.title,
            description: Localizable.RefundPayment.description,
            cancelButtonTitle: Localizable.no,
            confirmationButtonTitle: Localizable.yes,
            optionType: type
        )
    }
    
    func showAbuseOptions(title: String, message: String, abuseOptions: [Abuse]) {
        viewController?.showAbuseOptions(title: title, message: message, abuseOptions: abuseOptions)
    }
    
    func showDialog(enableNotification: Bool) {
        let title = enableNotification
            ? Localizable.EnableNotificationAlert.title
            : Localizable.DisabledNotificationAlert.title
        let description = enableNotification
            ? Localizable.EnableNotificationAlert.description
            : Localizable.DisabledNotificationAlert.description
        
        viewController?.showPopupNotification(title: title, description: description, genericButtonTitle: Localizable.ok)
    }
    
    func showOptionGenericError() {
        viewController?.showPopupNotification(
            title: Localizable.GenericError.title,
            description: Localizable.GenericError.description,
            genericButtonTitle: Localizable.ok
        )
    }
    
    func showRefundSuccess() {
        viewController?.showPopupNotification(
            title: "",
            description: Localizable.RefundPayment.success,
            genericButtonTitle: Localizable.ok
        )
    }
    
    func configureImageCommentInput(_ urlImage: URL?) {
        viewController?.setCommentAvatar(with: urlImage, and: Resources.Placeholders.greenAvatarPlaceholder.image)
    }
    
    func addComment(with item: ItemDetailDataSource) {
        viewController?.addNewItem(item)
    }
    
    func refreshCommentList(with dataSourceItem: ItemDetailDataSource, at index: Int) {
        viewController?.refreshComment(dataSourceItem, index)
    }
    
    func showSnackbarError(errorType: ItemDetailErrorType) {
        viewController?.showSnackbarError(with: getErrorString(errorType))
    }
    
    func showAbuseSuccess() {
        viewController?.showDialog(
            Resources.Icons.icoBigSuccess.image,
            title: Strings.thanksForYourHelp,
            message: Strings.letsReviewYourReport,
            primaryButtonAction: ApolloAlertAction(
                title: Strings.okIGotIt,
                dismissOnCompletion: true,
                completion: { }
            )
        )
    }
    
    func enableSendCommentButton(_ isEnabled: Bool) {
        viewController?.enableSendCommentButton(isEnabled)
    }
}

// MARK: Private extension
private extension ItemDetailPresenter {
    func getErrorString(_ errorType: ItemDetailErrorType) -> String {
        switch errorType {
        case .tooLongComment:
            return Localizable.Comments.tooLongCommentError
        case .publishingComment:
            return Localizable.Comments.publishingError
        case .likeCard:
            return Localizable.Like.error
        }
    }
}
