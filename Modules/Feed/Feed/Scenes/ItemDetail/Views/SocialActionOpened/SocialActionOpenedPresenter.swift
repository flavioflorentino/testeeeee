import Foundation
import UI

protocol SocialActionOpenedPresenting: AnyObject {
    var view: SocialActionOpenedDisplaying? { get set }
    func configureItem(_ item: Like)
}

final class SocialActionOpenedPresenter {
    private typealias Localizable = Strings.SocialActionOpened
    
    weak var view: SocialActionOpenedDisplaying?
}

// MARK: - SocialActionOpenedPresenting
extension SocialActionOpenedPresenter: SocialActionOpenedPresenting {
    func configureItem(_ item: Like) {
        let socialActionText = configureTitle(with: item)
        let socialActionAcessbilityValue = "\(socialActionText) .\(item.liked ? Localizable.undoLike : Localizable.toLike)"
        view?.configureSocialActionOpened(text: socialActionText,
                                          icon: Iconography.heart,
                                          color: item.liked ? .branding600() : .grayscale400(),
                                          isLiked: item.liked)
        view?.configureAcessibility(label: socialActionAcessbilityValue)
    }
    
    // MARK: private methods
    private func configureTitle(with itemLike: Like) -> String {
        if itemLike.liked {
            let firstLike = itemLike.latestLikes.first ?? ""
            return prepareTitle(Localizable.you, firstLike, totalLikes: itemLike.total)
        }
        
        guard itemLike.latestLikes.isEmpty else {
            let firstName = itemLike.latestLikes.first ?? ""
            let secondName = itemLike.latestLikes.last ?? ""
            return prepareTitle(firstName, secondName, totalLikes: itemLike.total)
        }
        
        return Localizable.toLike
    }
    
    private func prepareTitle(_ firstName: String, _ secondName: String, totalLikes: Int) -> String {
        if totalLikes == 1 {
            return Localizable.liked(firstName)
        }
        
        if totalLikes == 2 {
            return Localizable.oneMoreLiked(firstName, secondName)
        }
        
        if totalLikes == 3 {
            return Localizable.twoMoreLiked(firstName, secondName)
        }
        
        if totalLikes > 999 {
            return Localizable.morePeopleLiked(firstName, secondName)
        }
        
        return Localizable.moreThanTwoLiked(firstName, secondName, (totalLikes - 2))
    }
}
