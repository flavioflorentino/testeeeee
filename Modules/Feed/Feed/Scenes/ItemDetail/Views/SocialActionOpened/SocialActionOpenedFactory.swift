import UIKit

protocol UISocialActionOpenedUpdating: AnyObject {
    func updateLiked(newNumberOfLikes: Int, isLike: Bool)
}

protocol SocialActionOpenedDelegate: AnyObject {
    func didTouchLiked(
        numberOfLikes: Int,
        latestLikes: [String],
        isLike: Bool,
        uiSocialActionOpenedUpdate: UISocialActionOpenedUpdating
    )
}

enum SocialActionOpenedFactory {
    static func make(item: Like, delegate: SocialActionOpenedDelegate?) -> SocialActionOpenedView {
        let presenter: SocialActionOpenedPresenting = SocialActionOpenedPresenter()
        let interactor = SocialActionOpenedInteractor(item: item, presenter: presenter, delegate: delegate)
        let view = SocialActionOpenedView(interactor: interactor)

        presenter.view = view

        return view
    }
}
