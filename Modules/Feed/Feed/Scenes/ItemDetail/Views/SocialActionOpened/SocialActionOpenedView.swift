import UI
import UIKit

protocol SocialActionOpenedDisplaying: AnyObject {
    func configureSocialActionOpened(text: String, icon: Iconography, color: Colors.Style, isLiked: Bool)
    func configureAcessibility(label: String)
}

private extension SocialActionOpenedView.Layout {
    enum Size {
        static let lineHeight = 1.0
    }
    enum Button {
        static let width = 40.0
    }
}

final class SocialActionOpenedView: UIView {
    fileprivate enum Layout { }
    
    private let interactor: SocialActionOpenedInteracting
    
    init(interactor: SocialActionOpenedInteracting) {
        self.interactor = interactor
        super.init(frame: .zero)
        buildLayout()
        setupAccessibility()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        interactor.loadData()
    }
    
    private lazy var lineView = SeparatorView(style: BackgroundViewStyle(color: .grayscale100()))
    
    private lazy var likeButton = IconographyButton()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.grayscale400.color)
        return label
    }()
    
    // MARK: - Private Methods
    @objc
    private func likeButtonTapped() {
        interactor.likeAction()
    }
}

extension SocialActionOpenedView: SocialActionOpenedDisplaying {
    func configureSocialActionOpened(text: String, icon: Iconography, color: Colors.Style, isLiked: Bool) {
        likeButton.properties = .init(iconName: icon, color: color)
        titleLabel.text = text
        titleLabel.textColor = color
    }
    
    func configureAcessibility(label: String) {
        accessibilityLabel = label
    }
}

extension SocialActionOpenedView: AccessibilityConfiguration {
    func buildAccessibilityElements() {
        isAccessibilityElement = true
    }
    func configureAccessibilityAttributes() {
        accessibilityTraits = .button
    }
}

extension SocialActionOpenedView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(lineView)
        addSubview(likeButton)
        addSubview(titleLabel)
    }
    
    func setupConstraints() {
        lineView.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.lineHeight)
            $0.top.leading.trailing.equalToSuperview()
        }
        
        likeButton.snp.makeConstraints {
            $0.top.equalTo(lineView.snp.bottom).offset(Spacing.base00)
            $0.leading.bottom.equalToSuperview()
            $0.width.equalTo(Layout.Button.width)
        }
        
        titleLabel.snp.makeConstraints {
            $0.centerY.equalTo(likeButton.snp.centerY)
            $0.leading.equalTo(likeButton.snp.trailing).offset(Spacing.base00)
            $0.trailing.equalToSuperview()
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.groupedBackgroundSecondary.color
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(likeButtonTapped))
        addGestureRecognizer(tap)
    }
}
