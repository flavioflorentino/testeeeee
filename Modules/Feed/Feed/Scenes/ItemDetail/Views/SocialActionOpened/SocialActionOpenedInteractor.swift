import Foundation

protocol SocialActionOpenedInteracting: AnyObject {
    func loadData()
    func likeAction()
}

final class SocialActionOpenedInteractor {
    private let item: Like
    private let presenter: SocialActionOpenedPresenting
    
    private weak var delegate: SocialActionOpenedDelegate?

    init(item: Like, presenter: SocialActionOpenedPresenting, delegate: SocialActionOpenedDelegate?) {
        self.item = item
        self.presenter = presenter
        self.delegate = delegate
    }
}

// MARK: - SocialActionOpenedInteracting
extension SocialActionOpenedInteractor: SocialActionOpenedInteracting {
    func loadData() {
        presenter.configureItem(item)
    }
    
    func likeAction() {
        delegate?.didTouchLiked(
            numberOfLikes: item.total,
            latestLikes: item.latestLikes,
            isLike: item.liked,
            uiSocialActionOpenedUpdate: self
        )
    }
}

extension SocialActionOpenedInteractor: UISocialActionOpenedUpdating {
    func updateLiked(newNumberOfLikes: Int, isLike: Bool) {
        let newLike = Like(liked: isLike, latestLikes: item.latestLikes, total: newNumberOfLikes)
        presenter.configureItem(newLike)
    }
}
