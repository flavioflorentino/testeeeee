import UIKit
import UI

protocol CommentInputViewDelegate: AnyObject {
    func sendButtonTapped(with text: String?)
    func inputTextChanged(to text: String?)
}

enum InputValidator {
    typealias InputValidatorStrategy = (_ text: String?, _ updatedText: String?, _ range: NSRange, _ replacementText: String) -> Bool
    case custom(InputValidatorStrategy)
    case none
    
    static func maxCharaceters(_ maxChars: Int) -> InputValidator {
        .custom { _, updatedText, _, _ in
            guard let updatedText = updatedText else {
                return false
            }
            return updatedText.count <= maxChars
        }
    }
}

private extension CommentInputView.Layout {
    enum Size {
        static let minimumHeight = Sizing.ImageView.xSmall.rawValue.height
        static let maximumInputHeight = 112
    }
}

final class CommentInputView: UIView {
    typealias Localizable = Strings.ItemDetail
    typealias Photo = (url: URL?, placeholder: UIImage?)
    fileprivate enum Layout { }
    private lazy var photoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.imageStyle(AvatarImageStyle(size: .xSmall))
                 .with(\.contentMode, .scaleAspectFill)
                 .with(\.clipsToBounds, true)
        return imageView
    }()
    
    private lazy var inputContainerView: UIView = {
        let view = UIView()
        view.viewStyle(BackgroundViewStyle(color: .white()))
            .with(\.border, .light(color: .grayscale200()))
            .with(\.backgroundColor, Colors.white.color)
            .with(\.layer.cornerRadius, CornerRadius.strong)
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(userDidTapInputContainer)
            )
        )
        return view
    }()
    
    private lazy var inputTextView: TextView = {
        let textView = TextView()
        textView.placeholderText = Strings.ItemDetail.Comments.placeholder
        textView.setPlaceholderTextStyle(BodySecondaryLabelStyle())
        textView.font = Typography.bodySecondary().font()
        textView.textColor = Colors.black.color
        textView.backgroundColor = Colors.white.color
        textView.delegate = self
        textView.isScrollEnabled = false
        return textView
    }()
    
    private lazy var inputTextScrollView = UIScrollView()
    
    private lazy var sendButton: UIButton = {
        let button = UIButton(type: .system)
        button.viewStyle(RoundedViewStyle(cornerRadius: .strong))
              .with(\.backgroundColor, Colors.branding600.color)
        button.setTitleColor(Colors.white.color, for: .normal)
        button.setTitle(Iconography.message.rawValue, for: .normal)
        button.titleLabel?.font = Typography.icons(.medium).font()
        button.addTarget(self, action: #selector(userDidTapSendButton), for: .touchUpInside)
        button.contentEdgeInsets = .init(top: 0, left: Spacing.base00 / 2, bottom: 0, right: 0)
        return button
    }()
    
    private let inputValidator: InputValidator
    
    var text: String? {
        get {
            inputTextView.text
        } set {
            inputTextView.text = newValue
        }
    }
    
    var isSendButtonEnabled: Bool {
        get {
            sendButton.isEnabled
        } set {
            sendButton.isEnabled = newValue
        }
    }
    
    var placeholderText: String? {
        get {
            inputTextView.placeholderText
        } set {
            inputTextView.placeholderText = newValue
        }
    }
    
    var photo: Photo? {
        didSet {
            downloadPhoto()
        }
    }
    
    weak var delegate: CommentInputViewDelegate?
        
    init(inputValidator: InputValidator, photo: Photo? = nil) {
        self.inputValidator = inputValidator
        self.photo = photo
        super.init(frame: .zero)
        buildLayout()
        setupAccessibility()
        downloadPhoto()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CommentInputView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubviews(photoImageView, inputContainerView, sendButton)
        inputContainerView.addSubview(inputTextScrollView)
        inputTextScrollView.addSubview(inputTextView)
    }
    
    func setupConstraints() {
        photoImageView.snp.makeConstraints {
            $0.bottom.leading.equalToSuperview().inset(Spacing.base01)
        }
        sendButton.snp.makeConstraints {
            $0.bottom.equalToSuperview().offset(-Spacing.base01)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.height.width.equalTo(Layout.Size.minimumHeight)
        }
        inputContainerView.snp.makeConstraints {
            $0.height.greaterThanOrEqualTo(Layout.Size.minimumHeight)
            $0.top.bottom.equalToSuperview().inset(Spacing.base01)
            $0.trailing.equalTo(sendButton.snp.leading).offset(-Spacing.base02)
            $0.leading.equalTo(photoImageView.snp.trailing).offset(Spacing.base01)
        }
        inputTextScrollView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base01)
            $0.leading.trailing.equalToSuperview()
            $0.height.lessThanOrEqualTo(Layout.Size.maximumInputHeight)
        }
        inputTextView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.equalToSuperview().priority(.low)
            $0.width.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.groupedBackgroundSecondary.color
    }
    
    private func downloadPhoto() {
        photoImageView.setImage(url: photo?.url, placeholder: photo?.placeholder)
    }
}

@objc
private extension CommentInputView {
    func userDidTapInputContainer() {
        inputTextView.becomeFirstResponder()
    }
    
    func userDidTapSendButton() {
        delegate?.sendButtonTapped(with: inputTextView.text)
    }
}

extension CommentInputView: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        inputContainerView.viewStyle(BackgroundViewStyle(color: .white()))
                          .with(\.border, .light(color: .branding400()))
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        inputContainerView.viewStyle(BackgroundViewStyle(color: .white()))
                          .with(\.border, .light(color: .grayscale200()))
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText: String) -> Bool {
        let text = textView.text
        let updatedText = (text as NSString?)?.replacingCharacters(in: range, with: replacementText)
        
        switch inputValidator {
        case .custom(let limitStrategy):
            return limitStrategy(text, updatedText, range, replacementText)
        case .none:
            return true
        }        
    }
    
    func textViewDidChange(_ textView: UITextView) {
        delegate?.inputTextChanged(to: textView.text)
    }
}

extension CommentInputView: AccessibilityConfiguration {
    func buildAccessibilityElements() {
        inputContainerView.isAccessibilityElement = true
    }

    func configureAccessibilityAttributes() {
        inputContainerView.accessibilityLabel = Strings.ItemDetail.Comments.placeholder
        inputContainerView.accessibilityTraits = inputTextView.accessibilityTraits
        sendButton.accessibilityLabel = Localizable.publishComment
    }
 }
