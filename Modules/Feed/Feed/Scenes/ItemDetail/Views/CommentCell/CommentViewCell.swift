import UI
import UIKit

protocol CommentDisplaying: AnyObject {
    func configureName(with name: String)
    func configureComment(with text: String)
    func configureDate(with date: String)
    func configureStateComment(with text: String)
    func configureErrorState()
    func configureImage<S>(with style: S, and imageUrl: URL, and imagePlaceholder: UIImage?) where S: ImageStyle
    func configureAcessibility(label: String)
    func clearErrorState()
}

final class CommentViewCell: UITableViewCell {
    private typealias Localizable = Strings.ItemDetail.Comments
    
    private lazy var commentImageView = UIImageView()
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.grayscale850.color)
            .with(\.textAlignment, .left)
        return label
    }()
    
    private lazy var commentLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.grayscale600.color)
            .with(\.textAlignment, .left)
        return label
    }()
    
    private lazy var stateLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
            .with(\.textColor, Colors.grayscale500.color)
            .with(\.textAlignment, .left)
        label.isHidden = true
        return label
    }()
    
    private lazy var retrySendCommentButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.tryAgain, for: .normal)
        button.buttonStyle(LinkButtonStyle(size: .small))
        button.contentHorizontalAlignment = .left
        button.addTarget(self, action: #selector(retrySendComment), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    
    private lazy var retryAcessibilityAction = UIAccessibilityCustomAction(
        name: Localizable.tryAgain,
        target: self,
        selector: #selector(retrySendComment)
    )
    
    private var interactor: CommentInteracting?

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
        setupAccessibility()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupInteractor(_ interactor: CommentInteracting) {
        self.interactor = interactor
        self.interactor?.loadData()
    }
    
    func resetCell() {
        commentImageView.image = UIImage()
        nameLabel.text = nil
        commentLabel.text = nil
        stateLabel.text = nil
        retrySendCommentButton.isHidden = true
        accessibilityCustomActions = []
    }
}

private extension CommentViewCell {
    @objc
    func retrySendComment() {
        interactor?.retrySendComment()
    }
}

extension CommentViewCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubviews(commentImageView, nameLabel, commentLabel, stateLabel, retrySendCommentButton)
    }
    
    func setupConstraints() {
        commentImageView.snp.makeConstraints {
            $0.top.leading.equalToSuperview().inset(Spacing.base02)
        }
        
        nameLabel.snp.makeConstraints {
            $0.top.trailing.equalToSuperview().inset(Spacing.base02)
            $0.leading.equalTo(commentImageView.snp.trailing).offset(Spacing.base01)
        }
        
        commentLabel.snp.makeConstraints {
            $0.top.equalTo(nameLabel.snp.bottom).offset(Spacing.base01)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.leading.equalTo(commentImageView.snp.trailing).offset(Spacing.base01)
        }
        
        stateLabel.snp.makeConstraints {
            $0.top.equalTo(commentLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalTo(commentImageView.snp.trailing).offset(Spacing.base01)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().inset(Spacing.base01)
        }
        
        retrySendCommentButton.snp.makeConstraints {
            $0.top.equalTo(commentLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalTo(commentImageView.snp.trailing).offset(Spacing.base01)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().inset(Spacing.base01)
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.groupedBackgroundSecondary.color
        selectionStyle = .none
    }
}

extension CommentViewCell: CommentDisplaying {
    func configureName(with name: String) {
        nameLabel.text = name
    }
    
    func configureComment(with text: String) {
        commentLabel.text = text
    }
    
    func configureDate(with date: String) {
        stateLabel.text = date
        stateLabel.isHidden = false
    }
    
    func configureStateComment(with text: String) {
        stateLabel.text = text
        stateLabel.isHidden = false
    }
    
    func configureImage<S>(with style: S, and imageUrl: URL, and imagePlaceholder: UIImage?) where S: ImageStyle {
        commentImageView.imageStyle(style)
        commentImageView.setImage(url: imageUrl, placeholder: imagePlaceholder)
    }
    
    func configureErrorState() {
        stateLabel.isHidden = true
        retrySendCommentButton.isHidden = false
        accessibilityCustomActions = [retryAcessibilityAction]
    }
    
    func configureAcessibility(label: String) {
        accessibilityLabel = label
    }
    
    func clearErrorState() {
        stateLabel.isHidden = false
        retrySendCommentButton.isHidden = true
        accessibilityCustomActions = []
    }
}

extension CommentViewCell: AccessibilityConfiguration {
    func buildAccessibilityElements() {
        isAccessibilityElement = true
        contentView.accessibilityElementsHidden = true
    }
}
