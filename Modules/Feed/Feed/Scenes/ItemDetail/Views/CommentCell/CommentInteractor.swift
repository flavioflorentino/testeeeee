import Foundation

enum CommentState {
    case published
    case publishing
    case error
}

protocol CommentInteractingDelegate: AnyObject {
    func retrySendComment(_ comment: Comment, at index: Int)
}

protocol CommentInteracting: AnyObject {
    var delegate: CommentInteractingDelegate? { get set }
    func loadData()
    func retrySendComment()
}

final class CommentInteractor {
    private let presenter: CommentPresenting
    private let comment: Comment
    private let state: CommentState
    private let indexCell: Int
    
    weak var delegate: CommentInteractingDelegate?

    init(presenter: CommentPresenting, comment: Comment, index: Int, state: CommentState = .published) {
        self.presenter = presenter
        self.comment = comment
        self.state = state
        self.indexCell = index
    }
}

// MARK: - CommentInteracting
extension CommentInteractor: CommentInteracting {
    func loadData() {
        presenter.configureComment(position: indexCell, comment, state: state)
    }
    
    func retrySendComment() {
        presenter.configureRetryStart()
        delegate?.retrySendComment(comment, at: indexCell)
    }
}
