import AssetsKit
import Foundation
import UI
import UIKit

protocol CommentPresenting: AnyObject {
    var view: CommentDisplaying? { get set }
    
    func configureComment(position: Int, _ comment: Comment, state: CommentState)
    func configureRetryStart()
}

protocol CommentPresentingDelegate: AnyObject {
    func getAcessbilityLabel(for comment: Comment, state: CommentState, position: Int) -> String
}

final class CommentPresenter {
    private typealias Localizable = Strings.ItemDetail
    
    typealias Dependencies = HasCurrentDate & HasTimeZone
    private let dependencies: Dependencies
    
    weak var view: CommentDisplaying?
    weak var delegate: CommentPresentingDelegate?
    
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
}

// MARK: - CommentPresenting
extension CommentPresenter: CommentPresenting {
    func configureComment(position: Int, _ comment: Comment, state: CommentState = .published) {
        view?.configureName(with: comment.username)
        view?.configureComment(with: comment.text)
        
        let geometricType: GeometricType = comment.image.shape == .circle ? .circle : .square
        let style = GeometricImageStyle(type: geometricType, size: .small)
        view?.configureImage(with: style, and: comment.image.imageURL, and: placeholderImage(comment.image.type))
        
        switch state {
        case .published:
            let timeAgoSinceNow = comment.date.timeAgoSince(
                dependencies.currentDate,
                dateFormat: "dd MMM",
                timeZone: dependencies.timeZone
            )
            view?.configureDate(with: timeAgoSinceNow)
        case .publishing:
            view?.configureStateComment(with: Localizable.Comments.publishing)
        case .error:
            view?.configureErrorState()
        }
    
        guard let delegate = self.delegate else {
            return
        }
        view?.configureAcessibility(label: delegate.getAcessbilityLabel(
            for: comment,
            state: state,
            position: position
        ))
    }
    
    func configureRetryStart() {
        view?.clearErrorState()
    }
}

extension CommentPresenter: FeedPlaceholderRetriever { }
