import Foundation

protocol HeaderItemDetailPresenting: AnyObject {
    var view: HeaderItemDetailDisplaying? { get set }
    
    func configureActionLogView(_ actionLogView: ActionLogView)
    func configureUserTestimonialView(_ userTestimonialView: UserTestimonialView)
    func configurePostDetailView(_ postDetailView: PostDetailView)
    func configureSocialActionOpenedView(_ socialActionOpenedView: SocialActionOpenedView)
    func cleanAllViews()
    func configureAcessibility(itemDetail: ItemDetail)
}

final class HeaderItemDetailPresenter {
    typealias Localizable = Strings.ItemDetail
    typealias Dependencies = HasCurrentDate & HasTimeZone
    
    weak var view: HeaderItemDetailDisplaying?
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension HeaderItemDetailPresenter: HeaderItemDetailPresenting {
    func configureActionLogView(_ actionLogView: ActionLogView) {
        view?.configureTopView(actionLogView)
    }
    
    func configureUserTestimonialView(_ userTestimonialView: UserTestimonialView) {
        view?.configureTopView(userTestimonialView)
    }
    
    func configurePostDetailView(_ postDetailView: PostDetailView) {
        view?.configureTopView(postDetailView)
    }
    
    func configureSocialActionOpenedView(_ socialActionOpenedView: SocialActionOpenedView) {
        view?.configureBottomView(socialActionOpenedView)
    }
    
    func cleanAllViews() {
        view?.cleanDetailView()
    }
    
    func configureAcessibility(itemDetail: ItemDetail) {
        var acessibilityValues: [String] = []

        if let actionLog = itemDetail.actionLog {
            acessibilityValues.append(Localizable.transactionMensage(actionLog.text.value))
        }

        if let userTestimonial = itemDetail.userTestimonial {
            acessibilityValues.append(userTestimonial.text.value)
        }

        if let postDetail = itemDetail.postDetail {
            var detailAcessibilityValues: [String] = []
            let privacyType = postDetail.privacy == .private ? Strings.private : Strings.public
            let timeAgoSinceNow = postDetail.date.timeAgoSince(
                dependencies.currentDate,
                dateFormat: "dd MMM",
                timeZone: dependencies.timeZone
            )
            
            switch postDetail.currency.type {
            case .credit:
                detailAcessibilityValues.append(Localizable.credited(getAcessibilityLabel(from: postDetail.currency.value)))
            case .neutral:
                detailAcessibilityValues.append(Localizable.neutral(getAcessibilityLabel(from: postDetail.currency.value)))
            case .debit:
                detailAcessibilityValues.append(Localizable.debited(getAcessibilityLabel(from: postDetail.currency.value)))
            }
            
            detailAcessibilityValues.append(timeAgoSinceNow)
            detailAcessibilityValues.append(Localizable.transactionPrivacy(privacyType))
            
            acessibilityValues.append(detailAcessibilityValues.joined(separator: ". "))
        }

        let acessibilityLabel = acessibilityValues.joined(separator: ". ")
        
        view?.configureAcessibility(acessibilityLabel: acessibilityLabel)
    }
    
    private func getAcessibilityLabel(from string: String) -> String {
        let brazilLocale = Locale(identifier: "pt_BR")
        let currencyFormatter = NumberFormatter()
        currencyFormatter.numberStyle = .currency
        currencyFormatter.locale = brazilLocale
        
        let spellFormatter = NumberFormatter()
        spellFormatter.numberStyle = .currencyPlural
        spellFormatter.locale = brazilLocale
        
        var stringCopy = string
        stringCopy.removeAll {
            $0 == " " || $0 == "."
        }
        
        guard let currencyDoubleValue = currencyFormatter.number(from: stringCopy) else {
            return ""
        }
        
        let currencyAcessibilityLabel = spellFormatter.string(from: currencyDoubleValue) ?? ""
        return currencyAcessibilityLabel
    }
}
