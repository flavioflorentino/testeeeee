import UI
import UIKit

protocol HeaderItemDetailDisplaying: AnyObject {
    func configureTopView(_ view: UIView)
    func configureBottomView(_ view: UIView)
    func cleanDetailView()
    func configureAcessibility(acessibilityLabel: String)
}

final class HeaderItemDetailViewCell: UITableViewCell {
    private var interactor: HeaderItemDetailInteracting?
    
    private lazy var topStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var bottomStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
        setupAccessibility()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        cleanDetailView()
    }
    
    func setupInteractor(_ interactor: HeaderItemDetailInteracting) {
        self.interactor = interactor
        self.interactor?.loadData()
    }
}

extension HeaderItemDetailViewCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubviews(topStackView, bottomStackView)
    }
    
    func setupConstraints() {
        topStackView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        bottomStackView.snp.makeConstraints {
            $0.top.equalTo(topStackView.snp.bottom).offset(Spacing.base02)
            $0.bottom.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.groupedBackgroundSecondary.color
        selectionStyle = .none
    }
}

extension HeaderItemDetailViewCell: AccessibilityConfiguration {
     func buildAccessibilityElements() {
        isAccessibilityElement = false
        contentView.isAccessibilityElement = false
        accessibilityElements = [topStackView, bottomStackView]
        topStackView.isAccessibilityElement = true
     }
 }

extension HeaderItemDetailViewCell: HeaderItemDetailDisplaying {
    func configureTopView(_ view: UIView) {
        topStackView.addArrangedSubview(view)
    }
    
    func configureBottomView(_ view: UIView) {
        bottomStackView.addArrangedSubview(view)
    }
    
    func cleanDetailView() {
        topStackView.removeAllSubviews()
        bottomStackView.removeAllSubviews()
    }
    
    func configureAcessibility(acessibilityLabel: String) {
        topStackView.accessibilityLabel = acessibilityLabel
    }
}
