import Foundation

protocol HeaderItemDetailDelegate: AnyObject {
    func didTouchPrimaryImage(for deeplinkUrl: URL)
    func didTouchSecondaryImage(for deeplinkUrl: URL)
    func didSelectText(_ text: String, deeplinkUrl: URL)
    func didTouchLiked(numberOfLikes: Int, latestLikes: [String], isLike: Bool, uiSocialActionOpenedUpdate: UISocialActionOpenedUpdating)
}

protocol HeaderItemDetailInteracting: AnyObject {
    func loadData()
}

final class HeaderItemDetailInteractor {
    private let itemDetail: ItemDetail
    private let presenter: HeaderItemDetailPresenting
    
    private weak var delegate: HeaderItemDetailDelegate?
    
    init(itemDetail: ItemDetail, presenter: HeaderItemDetailPresenting, delegate: HeaderItemDetailDelegate? = nil) {
        self.itemDetail = itemDetail
        self.presenter = presenter
        self.delegate = delegate
    }
}

extension HeaderItemDetailInteractor: HeaderItemDetailInteracting {
    func loadData() {
        presenter.cleanAllViews()
        
        if let actionLog = itemDetail.actionLog {
            let itemView = ActionLogFactory.make(item: actionLog, delegate: self)
            presenter.configureActionLogView(itemView)
        }
        
        if let userTestimonial = itemDetail.userTestimonial {
            let itemView = UserTestimonialFactory.make(item: userTestimonial)
            presenter.configureUserTestimonialView(itemView)
        }
        
        if let postDetail = itemDetail.postDetail {
            let itemView = PostDetailFactory.make(item: postDetail)
            presenter.configurePostDetailView(itemView)
        }
        
        if let socialActionOpened = itemDetail.like {
            let itemView = SocialActionOpenedFactory.make(item: socialActionOpened, delegate: self)
            presenter.configureSocialActionOpenedView(itemView)
        }
    }
}

extension HeaderItemDetailInteractor: ActionLogDelegate {
    func didTouchPrimaryImage(for deeplinkUrl: URL) {
        delegate?.didTouchPrimaryImage(for: deeplinkUrl)
    }
    
    func didTouchSecondaryImage(for deeplinkUrl: URL) {
        delegate?.didTouchSecondaryImage(for: deeplinkUrl)
    }
    
    func didSelectText(_ text: String, deeplinkUrl: URL) {
        delegate?.didSelectText(text, deeplinkUrl: deeplinkUrl)
    }
}

extension HeaderItemDetailInteractor: SocialActionOpenedDelegate {
    func didTouchLiked(
        numberOfLikes: Int,
        latestLikes: [String],
        isLike: Bool,
        uiSocialActionOpenedUpdate: UISocialActionOpenedUpdating
    ) {
        delegate?.didTouchLiked(
            numberOfLikes: numberOfLikes,
            latestLikes: latestLikes,
            isLike: isLike,
            uiSocialActionOpenedUpdate: uiSocialActionOpenedUpdate
        )
    }
}
