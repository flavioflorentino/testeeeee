import UIKit

public protocol ItemDetailDelegate: AnyObject {
    func openPixRefundFlow(_ transactionId: String)
}

public enum ItemDetailFactory {
    public static func make(cardId: UUID, actionable: ScreenActionable?, delegate: ItemDetailDelegate?) -> UIViewController {
        let container = DependencyContainer()
        let coordinator: ItemDetailCoordinating = ItemDetailCoordinator(dependencies: container)
        let service: ItemDetailServicing = ItemDetailService(dependencies: container)
        let presenter: ItemDetailPresenting = ItemDetailPresenter(coordinator: coordinator)

        let interactor = ItemDetailInteractor(
            cardId: cardId,
            presenter: presenter,
            service: service,
            dependencies: container,
            actionable: actionable
        )
        
        let viewController = ItemDetailViewController(interactor: interactor)

        coordinator.viewController = viewController
        coordinator.delegate = delegate
        presenter.viewController = viewController

        return viewController
    }
}
