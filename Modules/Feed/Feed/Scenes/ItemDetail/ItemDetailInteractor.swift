import Core
import Foundation
import AnalyticsModule

enum ItemDetailErrorType {
    case tooLongComment
    case publishingComment
    case likeCard
}

enum ItemDetailDataSource {
    case header(presenter: HeaderItemDetailPresenting, interactor: HeaderItemDetailInteracting)
    case comment(presenter: CommentPresenting, interactor: CommentInteracting)
}

protocol ItemDetailInteracting: AnyObject {
    func fetchItemDetail()
    func dismiss()
    func loadOptions()
    func handleAction(_ action: Action)
    func handleOption(_ action: OptionAction)
    func handleConfirmation(with optionType: OptionType)
    func userOpennedScreen()
    func userClosedScreen()
    func loadTooLongCommentError()
    func addNewComment(_ comment: String)
    func retrySendComment(_ comment: Comment, index: Int)
}

final class ItemDetailInteractor {
    typealias Dependencies =
        HasAnalytics &
        HasAuthenticationManager &
        HasDeeplinkManager &
        HasConsumerManager &
        HasCurrentDate &
        HasTimeZone
    private typealias Localizable = Strings.ItemDetail
    
    private let cardId: UUID
    private let presenter: ItemDetailPresenting
    private let service: ItemDetailServicing
    private let dependencies: Dependencies
    private weak var actionable: ScreenActionable?
    
    private var itemDetail: ItemDetail?
    private var itemDetailOptions = [OptionType]()

    init(
        cardId: UUID,
        presenter: ItemDetailPresenting,
        service: ItemDetailServicing,
        dependencies: Dependencies,
        actionable: ScreenActionable?
    ) {
        self.cardId = cardId
        self.presenter = presenter
        self.service = service
        self.dependencies = dependencies
        self.actionable = actionable
    }
}

// MARK: - ItemDetailInteracting
extension ItemDetailInteractor: ItemDetailInteracting {
    func userOpennedScreen() {
        dependencies.analytics.log(FeedAnalytics.screenViewed)
    }
    
    func userClosedScreen() {
        let event = FeedAnalytics.buttonTapped(type: .back)
        dependencies.analytics.log(event)
    }
    
    func fetchItemDetail() {
        presenter.startLoading()

        service.fetchItemDetail(cardId: cardId) { [ weak self] result in
            guard let self = self else { return }
            
            self.presenter.stopLoading()
            switch result {
            case let .success(itemDetail):
                self.itemDetail = itemDetail
                self.itemDetailOptions = itemDetail.options
                self.presenter.configureItemDataSource(self.buildItemDetailDataSourceConfiguration())
            case let .failure(error):
                self.buildErrorConfiguration(error)
            }
        }
    }
    
    func dismiss() {
        presenter.dismiss()
    }
    
    func loadOptions() {
        let event = FeedAnalytics.buttonTapped(type: .options)
        dependencies.analytics.log(event)
        
        presenter.configureOptions(for: itemDetailOptions)
    }
    
    func handleAction(_ action: Action) {
        switch action.data {
        case let .deeplink(action):
            dependencies.deeplinkManager.open(url: action.url)
        case let .options(action):
            presenter.showAbuseOptions(title: action.title, message: action.message, abuseOptions: action.actions)
        case let .screen(action):
            handleActionScreenType(from: action.type)
        }
    }
    
    func handleOption(_ action: OptionAction) {
        switch action.type {
        case .makePrivate:
            trackingOptionSelected(with: .makePrivate)
            presenter.configureMakePrivateOption(type: action.type)
        case .hideTransaction:
            trackingOptionSelected(with: .hideTransaction)
            presenter.configureHideTransactionOption(type: action.type)
        case .enableNotification:
            trackingOptionSelected(with: .enableNotification)
            followCard()
        case .disableNotification:
            trackingOptionSelected(with: .disableNotification)
            presenter.configureDisableNotificationOption(type: action.type)
        case .reportContent:
            trackingOptionSelected(with: .reportCardContent)
            listAbuseOptions()
        case .returnPaymentP2P:
            trackingOptionSelected(with: .returnPayment)
            presenter.refundPaymentConfirmation(type: action.type)
        case .returnPaymentPix:
            trackingOptionSelected(with: .returnPayment)
            presenter.openPixReturnFlow(cardId.uuidString)
        case .viewReceiptP2P, .viewReceiptBiz, .viewReceiptStore, .viewReceiptPixRefund, .viewReceiptPixTransaction:
            trackingOptionSelected(with: .viewReceipt)
            presenter.openReceipt(type: action.type, id: cardId.uuidString)
        }
    }
    
    func handleConfirmation(with optionType: OptionType) {
        switch optionType {
        case .makePrivate:
            makeCardPrivate()
        case .hideTransaction:
            makeCardHidden()
        case .disableNotification:
            unfollowCard()
        case .returnPaymentP2P:
            confirmRefundPaymentRequiresAuthentication()
        default:
            return
        }
    }
    
    func loadTooLongCommentError() {
        presenter.showSnackbarError(errorType: .tooLongComment)
    }
    
    func addNewComment(_ comment: String) {
        guard let newComment = createNewComment(with: comment) else {
            return presenter.showSnackbarError(errorType: .publishingComment)
        }
        let index = getNewIndex()
        presenter.addComment(with: configureCommentDataSource(from: newComment, state: .publishing, index: index))
        presenter.enableSendCommentButton(false)
        sendComment(newComment, index: index)
    }
    
    func retrySendComment(_ comment: Comment, index: Int) {
        presenter.refreshCommentList(
            with: configureCommentDataSource(from: comment, state: .publishing, index: index),
            at: index
        )
        
        sendComment(comment, index: index)
    }
}

// MARK: - HeaderItemDetailDelegate
extension ItemDetailInteractor: HeaderItemDetailDelegate {
    func didTouchPrimaryImage(for deeplinkUrl: URL) {
        dependencies.deeplinkManager.open(url: deeplinkUrl)
    }
    
    func didTouchSecondaryImage(for deeplinkUrl: URL) {
        dependencies.deeplinkManager.open(url: deeplinkUrl)
    }
    
    func didSelectText(_ text: String, deeplinkUrl: URL) {
        dependencies.deeplinkManager.open(url: deeplinkUrl)
    }
    
    func didTouchLiked(numberOfLikes: Int, latestLikes: [String], isLike: Bool, uiSocialActionOpenedUpdate: UISocialActionOpenedUpdating) {
        var referenceUserLike = isLike
        referenceUserLike.toggle()

        let cardIdString = cardId.uuidString
        let endpoint: ItemDetailServiceEndpoint = referenceUserLike ? .likeCard(cardId: cardIdString) : .unlikeCard(cardId: cardIdString)

        service.updateLikeState(from: endpoint) { [weak self] result in
            switch result {
            case .success:
                self?.didUpdateLike(
                    numberOfLikes: numberOfLikes,
                    latestLikes: latestLikes,
                    isLike: referenceUserLike,
                    uiSocialActionOpenedUpdate: uiSocialActionOpenedUpdate
                )
            case .failure:
                self?.presenter.showSnackbarError(errorType: .likeCard)
            }
        }
    }
    
    // MARK: - Private Methods
    private func didUpdateLike(
        numberOfLikes: Int,
        latestLikes: [String],
        isLike: Bool,
        uiSocialActionOpenedUpdate: UISocialActionOpenedUpdating
    ) {
        let newNumberOfLikes = isLike ? numberOfLikes + 1 : numberOfLikes - 1

        uiSocialActionOpenedUpdate.updateLiked(newNumberOfLikes: newNumberOfLikes, isLike: isLike)

        guard let detail = itemDetail else { return }

        refreshItem(newValue: detail, likeUpdated: Like(liked: isLike, latestLikes: latestLikes, total: newNumberOfLikes))
        presenter.configureItemDataSource(buildItemDetailDataSourceConfiguration())
    }
}

// MARK: - Private Extension
private extension ItemDetailInteractor {
    func createNewComment(with text: String) -> Comment? {
        guard
            let imageURL = dependencies.consumerManager.imageURL,
            let username = dependencies.consumerManager.username
        else {
            return nil
        }
        
        let imageObject = ImageObject(shape: .circle, type: .consumer, imageURL: imageURL, action: nil)
        let comment = Comment(image: imageObject, username: username, text: text, date: Date())
        return comment
    }
    
    func sendComment(_ comment: Comment, index: Int) {
        service.commentCard(cardId: cardId, comment: comment.text) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success:
                self.addCommentToRefreshList(comment)
                self.presenter.refreshCommentList(
                    with: self.configureCommentDataSource(from: comment, state: .published, index: index),
                    at: index
                )
            case .failure:
                self.presenter.showSnackbarError(errorType: .publishingComment)
                self.presenter.refreshCommentList(
                    with: self.configureCommentDataSource(from: comment, state: .error, index: index),
                    at: index
                )
            }
            
            self.presenter.enableSendCommentButton(true)
        }
    }
    
    func listAbuseOptions() {
        service.listAbuseOptions(cardId: cardId) { [weak self] result in
            switch result {
            case .success(let abuseData):
                self?.presenter.showAbuseOptions(title: abuseData.title, message: abuseData.message, abuseOptions: abuseData.data)
            case .failure:
                self?.presenter.showOptionGenericError()
            }
        }
    }
    
    func addCommentToRefreshList(_ newComment: Comment) {
        guard let itemDetail = itemDetail else { return }
        var comments = itemDetail.comments
        
        comments.append(newComment)
        refreshItem(newValue: itemDetail, newComments: comments)
    }
    
    func makeCardPrivate() {
        service.changeVisibility(cardId: cardId, isPrivate: true) { [weak self] result in
            switch result {
            case .success:
                self?.refreshItemAsPrivate()
            case .failure:
                self?.presenter.showOptionGenericError()
            }
        }
    }
    
    func makeCardHidden() {
        service.changeVisibility(cardId: cardId, isPrivate: false) { [weak self] result in
            switch result {
            case .success:
                self?.refreshItemAsHidden()
            case .failure:
                self?.presenter.showOptionGenericError()
            }
        }
    }
    
    func followCard() {
        service.changeNotifications(cardId: cardId, isFollowing: true) { [weak self] result in
            switch result {
            case .success:
                self?.presenter.showDialog(enableNotification: true)
                self?.refreshItemAfterChangeNotification(notificationIsEnabled: true)
            case .failure:
                self?.presenter.showOptionGenericError()
            }
        }
    }
    
    func unfollowCard() {
        service.changeNotifications(cardId: cardId, isFollowing: false) { [weak self] result in
            switch result {
            case .success:
                self?.presenter.showDialog(enableNotification: false)
                self?.refreshItemAfterChangeNotification(notificationIsEnabled: false)
            case .failure:
                self?.presenter.showOptionGenericError()
            }
        }
    }
    
    func confirmRefundPaymentRequiresAuthentication() {
        dependencies.authenticationManager.authenticate { [weak self] result in
            switch result {
            case .success:
                self?.handleSuccessAuthentication()
            case .failure:
                self?.presenter.showOptionGenericError()
            }
        }
    }
    
    func buildItemDetailDataSourceConfiguration() -> [ItemDetailDataSource] {
        guard let itemDetail = itemDetail else {
            return []
        }
        
        var itemsDataSource = [ItemDetailDataSource]()
        
        presenter.configureImageCommentInput(dependencies.consumerManager.imageURL)
    
        let commentsDataSource = itemDetail.comments.lazy.enumerated().map { index, comment -> ItemDetailDataSource in
            configureCommentDataSource(from: comment, state: .published, index: index)
        }
        
        let headerDetailPresenter = HeaderItemDetailPresenter(dependencies: dependencies)
        let headerDetailInteractor = HeaderItemDetailInteractor(itemDetail: itemDetail, presenter: headerDetailPresenter, delegate: self)
        
        itemsDataSource.append(.header(presenter: headerDetailPresenter, interactor: headerDetailInteractor))
        itemsDataSource.append(contentsOf: commentsDataSource)
        
        return itemsDataSource
    }
    
    func buildErrorConfiguration(_ error: ApiError) {
        presenter.configureError()
    }
    
    func handleSuccessAuthentication() {
        guard let detail = itemDetail else { return }
        presenter.startLoading()
        
        service.refundPayment(transactionId: detail.id) { [weak self] result in
            self?.presenter.stopLoading()
            switch result {
            case .success:
                self?.refreshItemAfterRefundPayment()
                self?.presenter.showRefundSuccess()
            case .failure:
                self?.presenter.showOptionGenericError()
            }
        }
    }
    
    func refreshItemAsPrivate() {
        guard let detail = itemDetail,
              let postDetailCopy = detail.postDetail else {
            return
        }
        
        let postDetailUpdated = PostDetail(privacy: .private, date: postDetailCopy.date, currency: postDetailCopy.currency)
        let newOptions = detail.options.filter { $0 != .makePrivate }
        
        refreshItem(newValue: detail, postDetailUpdated: postDetailUpdated, newOptions: newOptions)
        presenter.configureItemDataSource(buildItemDetailDataSourceConfiguration())
    }
    
    func refreshItemAsHidden() {
        guard let detail = itemDetail else { return }
        let newOptions = detail.options.filter { $0 != .hideTransaction }
        
        refreshItem(newValue: detail, newOptions: newOptions)
    }
    
    func refreshItemAfterChangeNotification(notificationIsEnabled: Bool) {
        guard let detail = itemDetail else { return }
        
        let optionToRemove: OptionType = (notificationIsEnabled ? .enableNotification : .disableNotification)
        let index = detail.options.firstIndex(of: optionToRemove)
        var newOptions = detail.options.filter { $0 != optionToRemove }
        
        let newItem: OptionType = (notificationIsEnabled ? .disableNotification : .enableNotification)
        newOptions.insert(contentsOf: [newItem], at: index ?? 0)
        
        refreshItem(newValue: detail, newOptions: newOptions)
    }
    
    func refreshItemAfterRefundPayment() {
        guard let detail = itemDetail else { return }
        let newOptions = detail.options.filter { $0 != .returnPaymentP2P }
        
        refreshItem(newValue: detail, newOptions: newOptions)
    }
    
    func refreshItem(
        newValue: ItemDetail,
        postDetailUpdated: PostDetail? = nil,
        likeUpdated: Like? = nil,
        newOptions: [OptionType] = [],
        newComments: [Comment] = []
    ) {
        itemDetail = ItemDetail(
            id: newValue.id,
            resource: newValue.resource,
            actionLog: newValue.actionLog,
            userTestimonial: newValue.userTestimonial,
            postDetail: postDetailUpdated ?? newValue.postDetail,
            like: likeUpdated ?? newValue.like,
            options: newOptions.isNotEmpty ? newOptions : newValue.options,
            comments: newComments.isNotEmpty ? newComments : newValue.comments
        )
    }
    
    func configureCommentDataSource(from comment: Comment, state: CommentState, index: Int) -> ItemDetailDataSource {
        let presenter = CommentPresenter()
        let interactor = CommentInteractor(presenter: presenter, comment: comment, index: index, state: state)
        presenter.delegate = self
        return .comment(presenter: presenter, interactor: interactor)
    }
    
    func getNewIndex() -> Int {
        let currentIndex = itemDetail?.comments.count ?? 0
        return currentIndex + 1
    }
    
    func handleActionScreenType(from screenType: ScreenType) {
        if case .abuseReportSuccess = screenType {
            presenter.showAbuseSuccess()
        }
        
        if case .abuseReportDetail = screenType {
            actionable?.screen(for: .abuseReportDetail(type: .transactionalCard, id: cardId))
        }
    }
    
    func trackingOptionSelected(with optionType: FeedAnalytics.OptionInteractionType) {
        dependencies.analytics.log(FeedAnalytics.feedDetailOptionInteraction(optionType: optionType, cardId: cardId.uuidString))
    }
}

extension ItemDetailInteractor: CommentPresentingDelegate {
    func getAcessbilityLabel(for comment: Comment, state: CommentState, position: Int) -> String {
        let commentsTotal = self.itemDetail?.comments.count ?? 0
        switch state {
        case .published:
            let timeAgoSinceNow = comment.date.timeAgoSince(
                dependencies.currentDate,
                dateFormat: "dd MMM",
                timeZone: dependencies.timeZone
            )
            return Localizable.Comments.commentedSucess(
                comment.username,
                comment.text,
                timeAgoSinceNow,
                position + 1,
                commentsTotal
            )
        case .publishing:
            return Localizable.Comments.commentedPublishing(
                comment.username,
                comment.text,
                position + 1,
                commentsTotal
            )
        case .error:
            return Localizable.Comments.commentedErrored(
                comment.username,
                comment.text,
                position + 1,
                commentsTotal
            )
        }
    }
}
