struct OptionAction {
    private typealias Localizable = Strings.ItemDetail.Options
    
    let type: OptionType
    
    var label: String {
        switch type {
        case .returnPaymentP2P, .returnPaymentPix:
            return Localizable.returnPayment
        case .enableNotification:
            return Localizable.enableNotification
        case .disableNotification:
            return Localizable.disableNotification
        case .hideTransaction:
            return Localizable.hideTransaction
        case .makePrivate:
            return Localizable.makePrivate
        case .reportContent:
            return Localizable.reportContent
        case .viewReceiptP2P, .viewReceiptBiz, .viewReceiptStore, .viewReceiptPixRefund, .viewReceiptPixTransaction:
            return Localizable.viewReceipt
        }
    }
}
