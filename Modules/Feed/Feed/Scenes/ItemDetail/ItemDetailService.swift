import Core
import Foundation

protocol ItemDetailServicing {
    func fetchItemDetail(cardId: UUID, _ completion: @escaping (Result<ItemDetail, ApiError>) -> Void)
    func changeVisibility(cardId: UUID, isPrivate: Bool, _ completion: @escaping (Result<NoContent, ApiError>) -> Void)
    func changeNotifications(cardId: UUID, isFollowing: Bool, _ completion: @escaping (Result<NoContent, ApiError>) -> Void)
    func refundPayment(transactionId: UUID, _ completion: @escaping (Result<NoContent, ApiError>) -> Void)
    func updateLikeState(from endpoint: ItemDetailServiceEndpoint, completion: @escaping (Result<NoContent, ApiError>) -> Void)
    func commentCard(cardId: UUID, comment: String, _ completion: @escaping (Result<NoContent, ApiError>) -> Void)
    func listAbuseOptions(cardId: UUID, _ completion: @escaping (Result<AbuseData, ApiError>) -> Void)
    func sendAbuse(reportedId: String, _ completion: @escaping (Result<NoContent, ApiError>) -> Void)
}

final class ItemDetailService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - ItemDetailServicing
extension ItemDetailService: ItemDetailServicing {
    func fetchItemDetail(cardId: UUID, _ completion: @escaping (Result<ItemDetail, ApiError>) -> Void) {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        
        let request = Api<ItemDetail>(endpoint: ItemDetailServiceEndpoint.cardDetail(cardId: cardId.uuidString))
        request.shouldUseDefaultDateFormatter = false

        request.execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func changeVisibility(cardId: UUID, isPrivate: Bool, _ completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        let endpoint: ItemDetailServiceEndpoint = isPrivate ? .makePrivate(cardId: cardId.uuidString) : .makeHidden(cardId: cardId.uuidString)
        
        let request = Api<NoContent>(endpoint: endpoint)
        request.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func changeNotifications(cardId: UUID, isFollowing: Bool, _ completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        let endpoint: ItemDetailServiceEndpoint = isFollowing ? .followCard(cardId: cardId.uuidString) : .unfollowCard(cardId: cardId.uuidString)
        
        let request = Api<NoContent>(endpoint: endpoint)
        request.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }

    func refundPayment(transactionId: UUID, _ completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        let endpoint = ItemDetailServiceEndpoint.refundPayment(transactionId: transactionId.uuidString)
        
        let request = Api<NoContent>(endpoint: endpoint)
        request.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func commentCard(cardId: UUID, comment: String, _ completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        let endpoint = ItemDetailServiceEndpoint.comment(cardId: cardId.uuidString, comment: comment)
        
        let request = Api<NoContent>(endpoint: endpoint)
        request.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }

    func updateLikeState(from endpoint: ItemDetailServiceEndpoint, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        let api = Api<NoContent>(endpoint: endpoint)
        
        api.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }

    func listAbuseOptions(cardId: UUID, _ completion: @escaping (Result<AbuseData, ApiError>) -> Void) {
        let request = Api<AbuseData>(endpoint: ItemDetailServiceEndpoint.listAbuseOptions(cardId: cardId.uuidString))
        
        request.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func sendAbuse(reportedId: String, _ completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        let request = Api<NoContent>(
            endpoint: AbuseReportDetailServiceEndpoint.reportAbuse(
                type: AbuseReportType.transactionalCard.rawValue,
                reportedId: reportedId,
                message: nil
            )
        )

        request.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
