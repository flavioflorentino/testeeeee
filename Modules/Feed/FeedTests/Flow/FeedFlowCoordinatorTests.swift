import XCTest
@testable import Feed

private final class FeedFlowCoordinatorDelegateSpy: FeedFlowCoordinatorDelegate {

    // MARK: - OpenPixRefundFlow
    private(set) var openPixRefundFlowCallsCount = 0
    private(set) var openPixRefundFlowReceivedInvocations: [String] = []

    func openPixRefundFlow(_ transactionId: String) {
        openPixRefundFlowCallsCount += 1
        openPixRefundFlowReceivedInvocations.append(transactionId)
    }
}

private final class ScreenActionableSpy: ScreenActionable {

    // MARK: - Screen
    private(set) var screenForCallsCount = 0
    private(set) var screenForReceivedInvocations: [ActionScreenType] = []

    func screen(for type: ActionScreenType) {
        screenForCallsCount += 1
        screenForReceivedInvocations.append(type)
    }
}

final class FeedFlowCoordinatorTests: XCTestCase {
    private let feedFlowCoordinatorDelegateSpy = FeedFlowCoordinatorDelegateSpy()
    private let viewControllerMock = ViewControllerMock()
    private lazy var navControllerMock = NavigationControllerMock(rootViewController: viewControllerMock)
        
    private lazy var sut: FeedFlowCoordinator = {
        let coordinator = FeedFlowCoordinator()
        coordinator.delegate = feedFlowCoordinatorDelegateSpy
        coordinator.viewController = viewControllerMock
        return coordinator
    }()
    
    func testStart_WhenPassingFeedOptions_ShouldCreateFeedViewController() {
        let optionsMock: FeedOptions = .mainOptions
        
        let viewControllerReceived = sut.start(options: optionsMock, hasRefresh: true)
        
        XCTAssertNotNil(viewControllerReceived)
    }
    
    func testStart_WhenPassingCardId_ShouldCreateUIViewController() {
        let cardIdMock = UUID()
        
        let viewControllerReceived = sut.start(cardId: cardIdMock)
        
        XCTAssertNotNil(viewControllerReceived)
    }
    
    func testScreen_WhenActionScreenIsTransactionDetail_ShouldPushNewViewController() {
        let actionScreenMock: ActionScreenType = .transactionDetail(id: UUID())
        sut.screen(for: actionScreenMock)
        
        XCTAssertEqual(navControllerMock.pushViewControllerCallsCount, 1)
    }
    
    func testScreen_WhenActionScreenIsAbuseReportDetail_ShouldPresentNewViewController() {
        let actionScreenMock: ActionScreenType = .abuseReportDetail(type: .transactionalCard, id: UUID())
        sut.screen(for: actionScreenMock)
        
        XCTAssertEqual(viewControllerMock.presentCallsCount, 1)
    }
    
    func testOpenPixRefundFlow_ShouldCallDelegateMethod() {
        sut.openPixRefundFlow("1")
        
        XCTAssertEqual(feedFlowCoordinatorDelegateSpy.openPixRefundFlowCallsCount, 1)
        XCTAssertEqual(feedFlowCoordinatorDelegateSpy.openPixRefundFlowReceivedInvocations.first, "1")
    }
}
