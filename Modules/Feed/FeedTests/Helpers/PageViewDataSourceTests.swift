import XCTest
@testable import Feed

private class PrimaryViewController: UIViewController { }

private class SecondaryViewController: UIViewController { }

final class PageViewDataSourceTests: XCTestCase {
    private let primaryViewControllerMock = PrimaryViewController()
    private let secondaryViewControllerMock = SecondaryViewController()
    
    private let pageViewControllerMock = UIPageViewController()
    
    private lazy var sut: PageViewDataSource = {
        let dataSource = PageViewDataSource(viewController: pageViewControllerMock)
        dataSource.add(viewControllers: [primaryViewControllerMock, secondaryViewControllerMock])
        return dataSource
    }()
    
    func testPageViewControllerViewControllerBefore_WhenViewControllerBeforeIsFirst_ShouldReturnNilViewController() {
        let viewController = sut.pageViewController(pageViewControllerMock, viewControllerBefore: primaryViewControllerMock)
        
        XCTAssertNil(viewController)
    }
    
    func testPageViewControllerViewControllerBefore_WhenViewControllerBeforeIsNotFirst_ShouldReturnViewController() {
        let viewController = sut.pageViewController(pageViewControllerMock, viewControllerBefore: secondaryViewControllerMock)
        
        XCTAssertNotNil(viewController)
        XCTAssertTrue(viewController is PrimaryViewController)
    }
    
    func testPageViewControllerViewControllerAfter_WhenViewControllerIsLast_ShouldReturnNilViewController() {
        let viewController = sut.pageViewController(pageViewControllerMock, viewControllerAfter: secondaryViewControllerMock)
        
        XCTAssertNil(viewController)
    }
    
    func testPageViewControllerViewControllerAfter_WhenViewControllerIsNotLast_ShouldReturnViewController() {
        let viewController = sut.pageViewController(pageViewControllerMock, viewControllerAfter: primaryViewControllerMock)
        
        XCTAssertNotNil(viewController)
        XCTAssertTrue(viewController is SecondaryViewController)
    }
}
