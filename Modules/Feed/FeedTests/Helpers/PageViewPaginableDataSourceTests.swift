import XCTest
@testable import Feed
 
private class PrimaryViewController: UIViewController { }
 
private class SecondaryViewController: UIViewController { }
 
final class PageViewPaginableDataSourceTests: XCTestCase {
    private let primaryViewControllerMock = PrimaryViewController()
    private let secondaryViewControllerMock = SecondaryViewController()
    
    private let pageViewControllerMock = UIPageViewController()
    
    private lazy var sut: PageViewPaginableDataSource = {
        let dataSource = PageViewPaginableDataSource(viewController: pageViewControllerMock)
        dataSource.add(viewControllers: [primaryViewControllerMock, secondaryViewControllerMock])
        return dataSource
    }()
    
    func testNext_WhenExistsAnViewControllerFromIndex_ShouldChangeToSecondaryViewController() {
        sut.next(from: 1)
        
        XCTAssertTrue(pageViewControllerMock.viewControllers?.first is SecondaryViewController)
    }
    
    func testNext_WhenNotExistsAnViewControllerFromIndex_ShouldStayOnThePrimaryViewControler() {
        sut.next(from: 2)
        
        XCTAssertTrue(pageViewControllerMock.viewControllers?.first is PrimaryViewController)
    }
    
    func testPrevious_WhenExistsPreviousViewController_ShouldChangeToSecondaryViewController() {
        sut.previous(from: 1)
        
        XCTAssertTrue(pageViewControllerMock.viewControllers?.first is SecondaryViewController)
    }
    
    func testPrevious_WhenNotExistsPreviousViewController_ShouldStayOnThePrimaryViewControle() {
        sut.next(from: 2)
        
        XCTAssertTrue(pageViewControllerMock.viewControllers?.first is PrimaryViewController)
    }
 }
