import XCTest
@testable import Feed

private class PrimaryViewController: UIViewController { }

private class SecondaryViewController: UIViewController { }

private class TertiaryViewController: UIViewController { }

final class PageViewReloadableDataSourceTests: XCTestCase {
    private let primaryViewControllerMock = PrimaryViewController()
    private let secondaryViewControllerMock = SecondaryViewController()
    private let tertiaryViewControllerMock = TertiaryViewController()
    
    private let pageViewControllerMock = UIPageViewController()
     
    private lazy var sut = PageViewReloadableDataSource(viewController: pageViewControllerMock)
    
    func testAdd_ShouldAddInDataArray() {
        sut.add(viewControllers: [primaryViewControllerMock, secondaryViewControllerMock])
        
        XCTAssertEqual(sut.data.count, 2)
    }
    
    func testUpdateFromIndex_ShouldUpdateViewControllerByIndex() {
        sut.add(viewControllers: [primaryViewControllerMock, secondaryViewControllerMock])
        sut.update(from: 0, viewController: tertiaryViewControllerMock)
        
        XCTAssertTrue(sut.data.first is TertiaryViewController)
    }
    
    func testUpdateViewControllers_ShouldChangeAllViewController() {
        sut.add(viewControllers: [primaryViewControllerMock, secondaryViewControllerMock])
        sut.update(viewControllers: [tertiaryViewControllerMock])
        
        XCTAssertEqual(sut.data.count, 1)
        XCTAssertTrue(sut.data.first is TertiaryViewController)
    }
    
    func testRemoveAtIndex_ShouldRemoveViewControllerTheIndex() {
        sut.add(viewControllers: [primaryViewControllerMock, secondaryViewControllerMock])
        sut.remove(at: 1)
        
        XCTAssertEqual(sut.data.count, 1)
        XCTAssertTrue(sut.data.first is PrimaryViewController)
    }
    
    func testRemoveAtViewController_ShouldRemoveViewControllerInData() {
        sut.add(viewControllers: [primaryViewControllerMock, secondaryViewControllerMock])
        sut.remove(at: primaryViewControllerMock)
        
        XCTAssertEqual(sut.data.count, 1)
        XCTAssertTrue(sut.data.first is SecondaryViewController)
    }
 }
