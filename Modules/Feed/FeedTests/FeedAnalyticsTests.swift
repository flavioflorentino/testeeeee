import XCTest
@testable import Feed

final class FeedAnalyticsTests: XCTestCase {
    private let mockCardPosition = 0
    private let mockCardId = UUID().uuidString
    private let mockCarrouselPosition = 1
    private let mockButtonName = "button name"
    private let mockHyperlink = "hyperlink.com"
    private let mockCardTitle = "card title"
    private let mockCardMessage = "card message"
    private let mockCardType = ItemType.transactional
    
    func testCardButtonTapped_WhenHyperLinkIsNil_ShouldNotIncludeIt() {
        let event = FeedAnalytics.cardButtonTapped(
            buttonInfo: .init(name: mockButtonName, hyperlink: nil),
            cardInfo: .init(id: mockCardId, position: mockCardPosition, itemType: mockCardType)
        ).event()
        
        let expectedParamenters: [String : Any] = [
            "interaction_type": "CLICAR_BOTOES_CARD",
            "button_name": mockButtonName,
            "card_position": mockCardPosition,
            "card_id": mockCardId,
            "card_type": mockCardType.rawValue
        ]
        
        XCTAssertTrue(NSDictionary(dictionary: expectedParamenters).isEqual(to: event.properties))
        XCTAssertEqual(event.name, "Feed Card Interacted")
    }
    
    func testCardButtonTapped_WhenHyperLinkNotNil_ShouldIncludeIt() {
        let event = FeedAnalytics.cardButtonTapped(
            buttonInfo: .init(name: mockButtonName, hyperlink: mockHyperlink),
            cardInfo: .init(id: mockCardId, position: mockCardPosition, itemType: mockCardType)
        ).event()
        
        let expectedParamenters: [String : Any] = [
            "interaction_type": "CLICAR_BOTOES_CARD",
            "button_name": mockButtonName,
            "card_position": mockCardPosition,
            "card_id": mockCardId,
            "button_hyperlink": mockHyperlink,
            "card_type": mockCardType.rawValue,
        ]
        
        XCTAssertTrue(NSDictionary(dictionary: expectedParamenters).isEqual(to: event.properties))
        XCTAssertEqual(event.name, "Feed Card Interacted")
    }
    
    func testCardTapped_WhenNilProperties_ShouldNotIncludeThem() {        
        let event = FeedAnalytics.cardTapped(
            cardInfo: .init(
                id: mockCardId,
                position: mockCardPosition,
                itemType: mockCardType
            )
        ).event()
        
        let expectedParamenters: [String : Any] = [
            "interaction_type": "CLICAR_CARD",
            "card_position": mockCardPosition,
            "card_id": mockCardId,
            "card_type": mockCardType.rawValue
        ]
        
        XCTAssertTrue(NSDictionary(dictionary: expectedParamenters).isEqual(to: event.properties))
        XCTAssertEqual(event.name, "Feed Card Interacted")
    }
    
    func testCardTapped_WhenHaveAllProperties_ShouldIncludeAllThem() {
        let event = FeedAnalytics.cardTapped(
            cardInfo: .init(
                id: mockCardId,
                position: mockCardPosition,
                itemType: mockCardType,
                title: mockCardTitle,
                message: mockCardMessage,
                carousel: .init(id: mockCardId, position: mockCarrouselPosition)
            )
        ).event()
        
        let expectedParamenters: [String : Any] = [
            "interaction_type": "CLICAR_CARD",
            "card_position": mockCardPosition,
            "carrousel_position": mockCarrouselPosition,
            "card_title": mockCardTitle,
            "card_message": mockCardMessage,
            "card_id": mockCardId,
            "card_type": mockCardType.rawValue
        ]
        
        XCTAssertTrue(NSDictionary(dictionary: expectedParamenters).isEqual(to: event.properties))
        XCTAssertEqual(event.name, "Feed Card Interacted")
    }
    
    func testCardLikeInteracted_ShouldIncludeCorrectProperties() {
        let event = FeedAnalytics.cardLikeInteracted(
            interactionType: .dislike,
            cardInfo: .init(id: mockCardId, position: mockCardPosition, itemType: mockCardType)
        )
        
        let expectedParamenters: [String : Any] = [
            "interaction_type": "DISLIKE",
            "card_position": mockCardPosition,
            "card_id": mockCardId,
            "card_type": mockCardType.rawValue
        ]
        
        XCTAssertTrue(NSDictionary(dictionary: expectedParamenters).isEqual(to: event.properties))
        XCTAssertEqual(event.name, "Feed Card Interacted")
    }
    
    func testCardViewed_WhenCarrouselPositionIsNil_ShouldNotIncludeIt() {
        let event = FeedAnalytics.cardViewed(
            cardInfo: .init(
                id: mockCardId,
                position: mockCardPosition,
                itemType: mockCardType
            )
        )
        
        let expectedParamenters: [String : Any] = [
            "interaction_type": "CARD_IMPRESSO",
            "card_position": mockCardPosition,
            "card_id": mockCardId,
            "card_type": mockCardType.rawValue
        ]
        
        XCTAssertTrue(NSDictionary(dictionary: expectedParamenters).isEqual(to: event.properties))
        XCTAssertEqual(event.name, "Feed Card Interacted")
    }
    
    func testCardViewed_WhenCarrouselPositionIsIncluded_ShouldIncludeIt() {
        let event = FeedAnalytics.cardViewed(
            cardInfo: .init(
                id: mockCardId,
                position: mockCardPosition,
                itemType: mockCardType,
                carousel: .init(id: mockCardId, position: mockCarrouselPosition)
            )
        )
        
        let expectedParamenters: [String : Any] = [
            "interaction_type": "CARD_IMPRESSO",
            "card_position": mockCardPosition,
            "carrousel_position": mockCarrouselPosition,
            "card_id": mockCardId,
            "card_type": mockCardType.rawValue
        ]
        
        XCTAssertTrue(NSDictionary(dictionary: expectedParamenters).isEqual(to: event.properties))
        XCTAssertEqual(event.name, "Feed Card Interacted")
    }
    
    func testScreenViewed_ShouldIncludeCorrectProperties() {
        let event = FeedAnalytics.screenViewed.event()
        
        let expectedParamenters: [String : Any] = [
            "screen_name": "TRANSACAO",
            "business_context": "TRANSACAO"
        ]
        
        XCTAssertTrue(NSDictionary(dictionary: expectedParamenters).isEqual(to: event.properties))
        XCTAssertEqual(event.name, "Screen Viewed")
    }
    
    func testButtonClicked_WhenButtonIsBack_ShouldIncludeCorrectProperties() {
        let event = FeedAnalytics.buttonTapped(type: .back).event()
        
        let expectedParamenters: [String : Any] = [
            "screen_name": "TRANSACAO",
            "business_context": "TRANSACAO",
            "button_name": "VOLTAR"
        ]
        
        XCTAssertTrue(NSDictionary(dictionary: expectedParamenters).isEqual(to: event.properties))
        XCTAssertEqual(event.name, "Button Clicked")
    }
    
    func testButtonClicked_WhenButtonIsOptions_ShouldIncludeCorrectProperties() {
        let event = FeedAnalytics.buttonTapped(type: .options).event()
        
        let expectedParamenters: [String : Any] = [
            "screen_name": "TRANSACAO",
            "button_name": "OPCOES",
            "business_context": "TRANSACAO"
        ]
        
        XCTAssertTrue(NSDictionary(dictionary: expectedParamenters).isEqual(to: event.properties))
        XCTAssertEqual(event.name, "Button Clicked")
    }
    
    func testTabViewed_ShouldSendCorrectProperties() {
        let event = FeedAnalytics.tabViewed(tabType: .all)
        
        let expectedParamenters: [String : Any] = [
            "tab_name": "TODAS",
            "screen_name": "INICIO",
            "business_context": "FEED"
        ]
        
        XCTAssertTrue(NSDictionary(dictionary: expectedParamenters).isEqual(to: event.properties))
        XCTAssertEqual(event.name, "Tab Viewed")
    }
}
