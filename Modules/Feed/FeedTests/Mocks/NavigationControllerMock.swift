import UIKit

public final class NavigationControllerMock: UINavigationController {
    //MARK: - popToRootViewController
    private(set) var popToRootViewControllerCallsCount = 0
    
    public override func popToRootViewController(animated: Bool) -> [UIViewController]? {
        popToRootViewControllerCallsCount += 1
        let viewControllersList = super.popToRootViewController(animated: false)
        return viewControllersList
    }
    
    //MARK: - popToRootViewController
    private(set) var pushViewControllerCallsCount = 0
    
    public override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        pushViewControllerCallsCount += 1
        super.pushViewController(viewController, animated: animated)
    }
}

