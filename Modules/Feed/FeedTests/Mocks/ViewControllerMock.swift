import Foundation
import UIKit

public final class ViewControllerMock: UIViewController {
    //MARK: - present
    private(set) var presentCallsCount = 0
    
    //MARK: - dismiss
    private(set) var dismissCallsCount = 0
    private(set) var viewControllerPresented = UIViewController()
    
    override public func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        presentCallsCount += 1
        viewControllerPresented = viewControllerToPresent
        super.present(viewControllerToPresent, animated: flag, completion: completion)
    }
    
    override public func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        dismissCallsCount += 1
        super.dismiss(animated: flag, completion: completion)
    }
}
