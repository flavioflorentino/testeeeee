import Foundation
@testable import Feed

enum DataMock {
    // MARK: - Action Log Mock
    enum ActionLogMock {
        static let withActionNotExistsInImage = ActionLog(
            images: [
                ImageObject(
                    shape: .square,
                    type: .seller,
                    imageURL: URL(string: "https://i.pravatar.cc/250?img=1")!,
                    action: nil
                ),
                ImageObject(
                    shape: .circle,
                    type: .seller,
                    imageURL: URL(string: "https://i.pravatar.cc/250?img=1")!,
                    action: nil
                )
            ],
            text: TextObject(
                value: "Esse pagamento rendeu dinheiro de volta para Dani.boy fique feliz e continue gastando.",
                attributes: [
                    Attribute(
                        startIndex: 0,
                        endIndex: 8,
                        action: nil
                    )
                ]
            )
        )
        
        static let withActionExistsInImage = ActionLog(
            images: [
                ImageObject(
                    shape: .square,
                    type: .seller,
                    imageURL: URL(string: "https://i.pravatar.cc/250?img=1")!,
                    action: Action(
                        type: .deeplink,
                        data: .deeplink(
                            DeeplinkAction(
                                url: URL(string: "https://app.picpay.com/")!
                            )
                        )
                    )
                ),
                ImageObject(
                    shape: .square,
                    type: .seller,
                    imageURL: URL(string: "https://i.pravatar.cc/250?img=1")!,
                    action: Action(
                        type: .deeplink,
                        data: .deeplink(
                            DeeplinkAction(
                                url: URL(string: "https://app.picpay.com/")!
                            )
                        )
                    )
                )
            ],
            text: TextObject(
                value: "Esse pagamento rendeu dinheiro de volta para Dani.boy fique feliz e continue gastando.",
                attributes: [
                    Attribute(
                        startIndex: 5,
                        endIndex: 14,
                        action: Action(
                            type: .deeplink,
                            data: .deeplink(
                                DeeplinkAction(
                                    url: URL(string: "https://app.picpay.com/")!
                                )
                            )
                        )
                    )
                ]
            )
        )
        
        static let withActionTextObjectIsScreen = ActionLog(
            images: [
                ImageObject(
                    shape: .square,
                    type: .seller,
                    imageURL: URL(string: "https://i.pravatar.cc/250?img=1")!,
                    action: nil
                ),
                ImageObject(
                    shape: .square,
                    type: .seller,
                    imageURL: URL(string: "https://i.pravatar.cc/250?img=1")!,
                    action: nil
                )
            ],
            text: TextObject(
                value: "Esse pagamento rendeu dinheiro de volta para Dani.boy fique feliz e continue gastando.",
                attributes: [
                    Attribute(
                        startIndex: 5,
                        endIndex: 14,
                        action: Action(
                            type: .screen,
                            data: .screen(
                                ScreenAction(
                                    type: .transactionDetail
                                )
                            )
                        )
                    )
                ]
            )
        )
        
        static let withOnlyPrimaryImage = ActionLog(
            images: [
                ImageObject(
                    shape: .square,
                    type: .seller,
                    imageURL: URL(string: "https://i.pravatar.cc/250?img=1")!,
                    action: nil
                )
            ],
            text: TextObject(
                value: "Esse pagamento rendeu dinheiro de volta para Dani.boy fique feliz e continue gastando.",
                attributes: [
                    Attribute(
                        startIndex: 0,
                        endIndex: 8,
                        action: nil
                    )
                ]
            )
        )
        
        static let withPrimaryImageTypeIsConsumer = ActionLog(
            images: [
                ImageObject(
                    shape: .square,
                    type: .consumer,
                    imageURL: URL(string: "https://i.pravatar.cc/250?img=1")!,
                    action: nil
                )
            ],
            text: TextObject(
                value: "Esse pagamento rendeu dinheiro de volta para Dani.boy fique feliz e continue gastando.",
                attributes: [
                    Attribute(
                        startIndex: 0,
                        endIndex: 8,
                        action: nil
                    )
                ]
            )
        )
        
        static let withPrimaryImageTypeIsSeller = ActionLog(
            images: [
                ImageObject(
                    shape: .square,
                    type: .seller,
                    imageURL: URL(string: "https://i.pravatar.cc/250?img=1")!,
                    action: nil
                )
            ],
            text: TextObject(
                value: "Esse pagamento rendeu dinheiro de volta para Dani.boy fique feliz e continue gastando.",
                attributes: [
                    Attribute(
                        startIndex: 0,
                        endIndex: 8,
                        action: nil
                    )
                ]
            )
        )
        
        static let withPrimaryImageTypeIsAction = ActionLog(
            images: [
                ImageObject(
                    shape: .circle,
                    type: .action,
                    imageURL: URL(string: "https://i.pravatar.cc/250?img=1")!,
                    action: nil
                )
            ],
            text: TextObject(
                value: "Esse pagamento rendeu dinheiro de volta para Dani.boy fique feliz e continue gastando.",
                attributes: [
                    Attribute(
                        startIndex: 0,
                        endIndex: 8,
                        action: nil
                    )
                ]
            )
        )
    }
    
    // MARK: - User Testimonial Mock
    enum UserTestimonialMock {
        static let fullData = UserTestimonial(
            text: TextObject(
                value: "Obrigado meu consagrado",
                attributes: [
                    Attribute(
                        startIndex: 0,
                        endIndex: 5,
                        action: nil
                    )
                ]
            )
        )
        
        static let withTextContainsOnlyEmoji = UserTestimonial(
            text: TextObject(
                value: "🙏🏻🙏🏻🙏🏻",
                attributes: []
            )
        )
        
        static let withTextContainsOnlyEmojiAndSpaces = UserTestimonial(
            text: TextObject(
                value: "🙏🏻     🙏🏻  🙏🏻    ",
                attributes: []
            )
        )
    }
    
    // MARK: - Additional Info Mock
    enum AdditionalInfoMock {
        static let fullData = AdditionalInfo(
            status: .success,
            type: .calendar,
            text: TextObject(
                value: "Esse pagamento rendeu dinheiro de volta para Dani.boy fique feliz e continue gastando.",
                attributes: []
            ),
            primaryAction: nil,
            secondaryAction: nil,
            action: nil
        )
        
        static let withPrimaryActionIsDeeplink = AdditionalInfo(
            status: .success,
            type: .calendar,
            text: TextObject(
                value: "Esse pagamento rendeu dinheiro de volta para Dani.boy fique feliz e continue gastando.",
                attributes: []
            ),
            primaryAction: ObjectAction(
                text: "Saiba Mais",
                action: Action(
                    type: .deeplink,
                    data: .deeplink(
                        DeeplinkAction(
                            url: URL(string: "https://app.picpay.com/")!
                        )
                    )
                )
            ),
            secondaryAction: nil,
            action: nil
        )
        
        static let withPrimaryActionIsScreen = AdditionalInfo(
            status: .success,
            type: .calendar,
            text: TextObject(
                value: "Esse pagamento rendeu dinheiro de volta para Dani.boy fique feliz e continue gastando.",
                attributes: []
            ),
            primaryAction: ObjectAction(
                text: "Saiba Mais",
                action: Action(
                    type: .screen,
                    data: .screen(
                        ScreenAction(
                            type: .transactionDetail
                        )
                    )
                )
            ),
            secondaryAction: nil,
            action: nil
        )
        
        static let withSecondaryActionIsDeeplink = AdditionalInfo(
            status: .success,
            type: .calendar,
            text: TextObject(
                value: "Esse pagamento rendeu dinheiro de volta para Dani.boy fique feliz e continue gastando.",
                attributes: []
            ),
            primaryAction: nil,
            secondaryAction: ObjectAction(
                text: "Saiba Mais",
                action: Action(
                    type: .deeplink,
                    data: .deeplink(
                        DeeplinkAction(
                            url: URL(string: "https://app.picpay.com/")!
                        )
                    )
                )
            ),
            action: nil
        )
        
        static let withSecondaryActionIsScreen = AdditionalInfo(
            status: .success,
            type: .calendar,
            text: TextObject(
                value: "Esse pagamento rendeu dinheiro de volta para Dani.boy fique feliz e continue gastando.",
                attributes: []
            ),
            primaryAction: nil,
            secondaryAction: ObjectAction(
                text: "Saiba Mais",
                action: Action(
                    type: .screen,
                    data: .screen(
                        ScreenAction(
                            type: .transactionDetail
                        )
                    )
                )
            ),
            action: nil
        )
        
        static let withActionIsDeeplink = AdditionalInfo(
            status: .success,
            type: .calendar,
            text: TextObject(
                value: "Esse pagamento rendeu dinheiro de volta para Dani.boy fique feliz e continue gastando.",
                attributes: [
                    Attribute(
                        startIndex: 5,
                        endIndex: 14,
                        action: Action(
                            type: .deeplink,
                            data: .deeplink(
                                DeeplinkAction(
                                    url: URL(string: "https://app.picpay.com/")!
                                )
                            )
                        )
                    )
                ]
            ),
            primaryAction: nil,
            secondaryAction: nil,
            action: Action(
                type: .deeplink,
                data: .deeplink(
                    DeeplinkAction(
                        url: URL(string: "https://app.picpay.com/")!
                    )
                )
            )
        )
        
        static let withActionIsScreen = AdditionalInfo(
            status: .success,
            type: .calendar,
            text: TextObject(
                value: "Esse pagamento rendeu dinheiro de volta para Dani.boy fique feliz e continue gastando.",
                attributes: [
                    Attribute(
                        startIndex: 5,
                        endIndex: 14,
                        action: Action(
                            type: .screen,
                            data: .screen(
                                ScreenAction(
                                    type: .transactionDetail
                                )
                            )
                        )
                    )
                ]
            ),
            primaryAction: nil,
            secondaryAction: nil,
            action: Action(
                type: .screen,
                data: .screen(
                    ScreenAction(
                        type: .transactionDetail
                    )
                )
            )
        )
    }
    
    // MARK: - Post Detail Mock
    enum PostDetailMock {
        static let withPrivacyIsPublic = PostDetail(
            privacy: .public,
            date: Date(),
            currency: Currency(
                type: .neutral,
                value: "R$ 100,00"
            )
        )
        
        static let withPrivacyIsPrivate = PostDetail(
            privacy: .private,
            date: Date(),
            currency: Currency(
                type: .neutral,
                value: "R$ 100,00"
            )
        )
        
        static let withCurrencyIsCredit = PostDetail(
            privacy: .private,
            date: Date(),
            currency: Currency(
                type: .credit,
                value: "R$ 100,00"
            )
        )
        
        static let withCurrencyIsDebit = PostDetail(
            privacy: .private,
            date: Date(),
            currency: Currency(
                type: .debit,
                value: "R$ 100,00"
            )
        )
        
        static let withLessOneMinute = PostDetail(
            privacy: .public,
            date: Date(
                timeIntervalSince1970: 1600974869
            ),
            currency: Currency(
                type: .neutral,
                value: "R$ 100,00"
            )
        )
        
        static let withOneMinuteAgo = PostDetail(
            privacy: .public,
            date: Date(
                timeIntervalSince1970: 1600974828
            ),
            currency: Currency(
                type: .neutral,
                value: "R$ 100,00"
            )
        )
        
        static let withThirtyMinuteAgo = PostDetail(
            privacy: .public,
            date: Date(
                timeIntervalSince1970: 1600973089
            ),
            currency: Currency(
                type: .neutral,
                value: "R$ 100,00"
            )
        )
        
        static let withOneHourAgo = PostDetail(
            privacy: .public,
            date: Date(
                timeIntervalSince1970: 1600971289
            ),
            currency: Currency(
                type: .neutral,
                value: "R$ 100,00"
            )
        )
        
        static let withTwentyThreeAgo = PostDetail(
            privacy: .public,
            date: Date(
                timeIntervalSince1970: 1600892089
            ),
            currency: Currency(
                type: .neutral,
                value: "R$ 100,00"
            )
        )
        
        static let withYesterday = PostDetail(
            privacy: .public,
            date: Date(
                timeIntervalSince1970: 1600888489
            ),
            currency: Currency(
                type: .neutral,
                value: "R$ 100,00"
            )
        )
        
        static let withOtherDays = PostDetail(
            privacy: .public,
            date: Date(
                timeIntervalSince1970: 1600715689
            ),
            currency: Currency(
                type: .neutral,
                value: "R$ 100,00"
            )
        )
    }
    
    // MARK: - Social Action Mock
    enum SocialActionMock {
        static let fullData = SocialAction(
            likeCount: 100,
            commentCount: 10,
            liked: true,
            commented: true
        )
        
        static let withLiked = SocialAction(
            likeCount: 100,
            commentCount: 10,
            liked: true,
            commented: true
        )
        
        static let withLike = SocialAction(
            likeCount: 100,
            commentCount: 10,
            liked: false,
            commented: false
        )
        
        static let withNoLikes = SocialAction(
            likeCount: 0,
            commentCount: 0,
            liked: false,
            commented: false
        )
        
        static let withOneLike = SocialAction(
            likeCount: 1,
            commentCount: 0,
            liked: true,
            commented: false
        )
    }
    
    // MARK: - Item Mock
    enum ItemMock {
        static let noBannersCarrousel = Item(
            id: UUID(),
            type: .carousel,
            banners: [],
            action: nil,
            actionLog: nil,
            postDetail: nil,
            socialAction: nil
        )
        
        static let fullData = Item(
            id: UUID(),
            type: .transactional,
            banners: [],
            userTestimonial: UserTestimonial(
                text: TextObject(
                    value: "Obrigado meu consagrado",
                    attributes: []
                )
            ),
            additionalInfo: AdditionalInfo(
                status: .success,
                type: .calendar,
                text: TextObject(
                    value: "Esse pagamento rendeu dinheiro de volta para Dani.boy fique feliz e continue gastando.",
                    attributes: []
                ),
                primaryAction: nil,
                secondaryAction: nil,
                action: nil
            )
        )
        
        static let withDeeplinkAction = Item(
            id: UUID(),
            type: .transactional,
            banners: [],
            action: Action(
                type: .deeplink,
                data: .deeplink(
                    DeeplinkAction(
                        url: URL(string: "https://app.picpay.com/")!
                    )
                )
            ),
            actionLog: DataMock.ActionLogMock.withActionExistsInImage,
            userTestimonial: DataMock.UserTestimonialMock.fullData,
            additionalInfo: DataMock.AdditionalInfoMock.fullData,
            postDetail: DataMock.PostDetailMock.withPrivacyIsPublic,
            socialAction: DataMock.SocialActionMock.fullData
        )
        
        static let withScreenAction = Item(
            id: UUID(),
            type: .transactional,
            banners: [],
            action: Action(
                type: .screen,
                data: .screen(
                    ScreenAction(
                        type: .transactionDetail
                    )
                )
            ),
            actionLog: DataMock.ActionLogMock.withActionExistsInImage,
            postDetail: DataMock.PostDetailMock.withPrivacyIsPublic
        )
    }
    
    // MARK: - Banner Mock
    enum BannerMock {
        static let withoutPrimaryAction = Banner(
            id: UUID(),
            text: "Compre de quem está perto",
            description: "Aproveite para pagar o que quiser com o seu cartão.",
            imageURL: URL(string: "https://i.pravatar.cc/250?img=1")!,
            action: Action(
                type: .deeplink,
                data: .deeplink(
                    DeeplinkAction(
                        url: URL(string: "https://app.picpay.com/")!
                    )
                )
            ),
            primaryAction: nil
        )
        
        static let withDeeplinkAction = Banner(
            id: UUID(),
            text: "Compre de quem está perto",
            description: "Aproveite para pagar o que quiser com o seu cartão.",
            imageURL: URL(string: "https://i.pravatar.cc/250?img=1")!,
            action: Action(
                type: .deeplink,
                data: .deeplink(
                    DeeplinkAction(
                        url: URL(string: "https://app.picpay.com/")!
                    )
                )
            ),
            primaryAction: ObjectAction(
                text: "Cadastrar cartão",
                action: Action(
                    type: .deeplink,
                    data: .deeplink(
                        DeeplinkAction(
                            url: URL(string: "https://app.picpay.com/")!
                        )
                    )
                )
            )
        )
        
        static let withScreenAction = Banner(
            id: UUID(),
            text: "Compre de quem está perto",
            description: "Aproveite para pagar o que quiser com o seu cartão.",
            imageURL: URL(string: "https://i.pravatar.cc/250?img=1")!,
            action: Action(
                type: .screen,
                data: .screen(
                    ScreenAction(
                        type: .transactionDetail
                    )
                )
            ),
            primaryAction: ObjectAction(
                text: "Cadastrar cartão",
                action: Action(
                    type: .screen,
                    data: .screen(
                        ScreenAction(
                            type: .transactionDetail
                        )
                    )
                )
            )
        )
    }
    
    // MARK: - ItemDetail Mock
    enum ItemDetailMock {
        static let fullData = ItemDetail(
            id: UUID(),
            resource: ResourceMock.fullData,
            actionLog: ActionLogMock.withOnlyPrimaryImage,
            userTestimonial: UserTestimonialMock.fullData,
            postDetail: PostDetailMock.withPrivacyIsPublic,
            like: LikeMock.noLikes,
            options: [OptionType.enableNotification],
            comments: [CommentMock.comment]
        )
        
        static let dataWithAllOptions = ItemDetail(
            id: UUID(),
            resource: ResourceMock.fullData,
            actionLog: ActionLogMock.withOnlyPrimaryImage,
            userTestimonial: UserTestimonialMock.fullData,
            postDetail: PostDetailMock.withPrivacyIsPublic,
            like: LikeMock.noLikes,
            options: [OptionType.enableNotification,
                      OptionType.returnPaymentP2P,
                      OptionType.reportContent,
                      OptionType.returnPaymentPix,
                      OptionType.disableNotification,
                      OptionType.hideTransaction,
                      OptionType.makePrivate,
                      OptionType.viewReceiptBiz,
                      OptionType.viewReceiptStore,
                      OptionType.viewReceiptP2P,
                      OptionType.viewReceiptPixRefund,
                      OptionType.viewReceiptPixTransaction],
            comments: [CommentMock.comment]
        )
        
        static let empty = ItemDetail(
            id: UUID(),
            resource: ResourceMock.fullData,
            actionLog: nil,
            userTestimonial: nil,
            postDetail: nil,
            like: nil,
            options: [],
            comments: []
        )
        
        static let detailWithoutOptions = ItemDetail(
            id: UUID(),
            resource: ResourceMock.fullData,
            actionLog: ActionLogMock.withOnlyPrimaryImage,
            userTestimonial: UserTestimonialMock.fullData,
            postDetail: PostDetailMock.withPrivacyIsPublic,
            like: LikeMock.noLikes,
            options: [],
            comments: [CommentMock.comment]
        )
    }
    
    // MARK: - Resource Mock
    enum ResourceMock {
        static let fullData = Resource(
            id: "123",
            type: "TRANSACTIONAL"
        )
    }
    
    // MARK: - Like Mock
    enum LikeMock {
        static let noLikes = Like(liked: false, latestLikes: [], total: 0)
        static let youLiked = Like(liked: true, latestLikes: [], total: 1)
    }

    // MARK: - Comment Mock
    enum CommentMock {
        static let comment = Comment(
            image: ImageObject(
                shape: .circle,
                type: .consumer,
                imageURL: URL(string: "picpay://picpay.me/primary")!,
                action: nil
            ),
            username: "teste",
            text: "teste",
            date: Date()
        )
        
        static let commentWithLessOneMinute = Comment(
            image: ImageObject(
                shape: .circle,
                type: .consumer,
                imageURL: URL(string: "picpay://picpay.me/primary")!,
                action: nil
            ),
            username: "teste",
            text: "teste",
            date: Date(
                timeIntervalSince1970: 1600974869
            )
        )
        
        static let commentWithOneMinuteAgo = Comment(
            image: ImageObject(
                shape: .square,
                type: .consumer,
                imageURL: URL(string: "picpay://picpay.me/primary")!,
                action: nil
            ),
            username: "teste",
            text: "teste",
            date: Date(
                timeIntervalSince1970: 1600974828
            )
        )
        
        static let commentWithThirtyMinuteAgo = Comment(
            image: ImageObject(
                shape: .circle,
                type: .seller,
                imageURL: URL(string: "picpay://picpay.me/primary")!,
                action: nil
            ),
            username: "teste",
            text: "teste",
            date: Date(
                timeIntervalSince1970: 1600973089
            )
        )
        
        static let commentWithOneHourAgo = Comment(
            image: ImageObject(
                shape: .circle,
                type: .action,
                imageURL: URL(string: "picpay://picpay.me/primary")!,
                action: nil
            ),
            username: "teste",
            text: "teste",
            date: Date(
                timeIntervalSince1970: 1600971289
            )
        )
        
        static let commentWithTwentyThreeAgo = Comment(
            image: ImageObject(
                shape: .circle,
                type: .consumer,
                imageURL: URL(string: "picpay://picpay.me/primary")!,
                action: nil
            ),
            username: "teste",
            text: "teste",
            date: Date(
                timeIntervalSince1970: 1600892089
            )
        )
        
        static let commentWithYesterday = Comment(
            image: ImageObject(
                shape: .circle,
                type: .consumer,
                imageURL: URL(string: "picpay://picpay.me/primary")!,
                action: nil
            ),
            username: "teste",
            text: "teste",
            date: Date(
                timeIntervalSince1970: 1600888489
            )
        )
        
        static let commentWithOtherDays = Comment(
            image: ImageObject(
                shape: .circle,
                type: .consumer,
                imageURL: URL(string: "picpay://picpay.me/primary")!,
                action: nil
            ),
            username: "teste",
            text: "teste",
            date: Date(
                timeIntervalSince1970: 1600715689
            )
        )
    }
}
