import Core
import XCTest
@testable import Feed

private final class ItemDetailDelegateSpy: ItemDetailDelegate {

    //MARK: - openPixRefundFlow
    private(set) var openPixRefundFlowCallsCount = 0
    private(set) var openPixRefundFlowReceivedInvocations: [String] = []

    func openPixRefundFlow(_ transactionId: String) {
        openPixRefundFlowCallsCount += 1
        openPixRefundFlowReceivedInvocations.append(transactionId)
    }

    //MARK: - refundPaymentP2P
    private(set) var refundPaymentP2PDataCompletionCallsCount = 0
    private(set) var refundPaymentP2PDataCompletionReceivedInvocations: [(data: [AnyHashable: Any], completion: (_ success: Bool, _ error: Error?) -> Void)] = []

    func refundPaymentP2P(data: [AnyHashable: Any], completion: @escaping (_ success: Bool, _ error: Error?) -> Void) {
        refundPaymentP2PDataCompletionCallsCount += 1
        refundPaymentP2PDataCompletionReceivedInvocations.append((data: data, completion: completion))
    }
}

private class ReceiptContractSpy: ReceiptContract {
    // MARK: - Open
    private(set) var openWithIdReceiptTypeFeedItemIdCallsCount = 0
    private(set) var openWithIdReceiptTypeFeedItemIdReceivedInvocations: [(id: String, receiptType: String, feedItemId: String?)] = []

    func open(withId id: String, receiptType: String, feedItemId: String?) {
        openWithIdReceiptTypeFeedItemIdCallsCount += 1
        openWithIdReceiptTypeFeedItemIdReceivedInvocations.append((id: id, receiptType: receiptType, feedItemId: feedItemId))
    }
}

final class ItemDetailCoordinatorTests: XCTestCase {
    private let navControllerMock = NavigationControllerMock(rootViewController: ViewControllerMock())
    private let delegateSpy = ItemDetailDelegateSpy()
    private let receiptSpy = ReceiptContractSpy()
    private lazy var containerMock = DependencyContainerMock(receiptSpy)
    
    private lazy var sut: ItemDetailCoordinating = {
        let coordinator = ItemDetailCoordinator(dependencies: containerMock)
        coordinator.viewController = navControllerMock.topViewController
        coordinator.delegate = delegateSpy
        return coordinator
    }()
    
    func testDismiss_ShouldPopToRootViewController() {
        sut.dismiss()
        
        XCTAssertEqual(navControllerMock.popToRootViewControllerCallsCount, 1)
    }

    func testOpenReceipt_ShouldOpenReceiptUsingLegacyDependency() {
        let idMock = "123"
        let receiptTypeMock = ReceiptType.p2p
        sut.openReceipt(idMock, type: receiptTypeMock)
        
        XCTAssertEqual(receiptSpy.openWithIdReceiptTypeFeedItemIdCallsCount, 1)
        XCTAssertEqual(receiptSpy.openWithIdReceiptTypeFeedItemIdReceivedInvocations.first?.id, idMock)
        XCTAssertEqual(receiptSpy.openWithIdReceiptTypeFeedItemIdReceivedInvocations.first?.receiptType, receiptTypeMock.rawValue)
    }
    
    func testOpenPixRefundFlow_ShouldOpenRefundPixFlowUsingDelegate() {
        let idMock = "123"
        sut.openPixRefundFlow(idMock)
        
        XCTAssertEqual(delegateSpy.openPixRefundFlowCallsCount, 1)
        XCTAssertEqual(delegateSpy.openPixRefundFlowReceivedInvocations[0], idMock)
    }
}
