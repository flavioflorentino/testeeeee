import AssetsKit
import UI
import XCTest
@testable import Feed

private final class ItemDetailCoordinatingSpy: ItemDetailCoordinating {
    var viewController: UIViewController?
    var delegate: ItemDetailDelegate?
    
    // MARK: dismiss
    private(set) var dismissCallsCount = 0
    
    func dismiss() {
        dismissCallsCount += 1
    }
    
    //MARK: - openReceipt
    private(set) var openReceiptTypeCallsCount = 0
    private(set) var openReceiptTypeReceivedInvocations: [(id: String, type: ReceiptType)] = []

    func openReceipt(_ id: String, type: ReceiptType) {
        openReceiptTypeCallsCount += 1
        openReceiptTypeReceivedInvocations.append((id: id, type: type))
    }

    //MARK: - openPixRefundFlow
    private(set) var openPixRefundFlowCallsCount = 0
    private(set) var openPixRefundFlowReceivedInvocations: [String] = []

    func openPixRefundFlow(_ id: String) {
        openPixRefundFlowCallsCount += 1
        openPixRefundFlowReceivedInvocations.append(id)
    }
}

private final class ItemDetailDisplaySpy: ItemDetailDisplaying {

    // MARK: - ConfigureItemDetail
    private(set) var configureItemDetailCallsCount = 0
    private(set) var configureItemDetailReceivedInvocations: [[ItemDetailDataSource]] = []

    func configureItemDetail(_ itemDataSource: [ItemDetailDataSource]) {
        configureItemDetailCallsCount += 1
        configureItemDetailReceivedInvocations.append(itemDataSource)
    }

    // MARK: - StartLoading
    private(set) var startLoadingCallsCount = 0

    func startLoading() {
        startLoadingCallsCount += 1
    }

    // MARK: - StopLoading
    private(set) var stopLoadingCallsCount = 0

    func stopLoading() {
        stopLoadingCallsCount += 1
    }

    // MARK: - ShowError
    private(set) var showErrorWithAndAndCallsCount = 0
    private(set) var showErrorWithAndAndReceivedInvocations: [(title: String, description: String, buttonTitle: String)] = []

    func showError(with title: String, and description: String, and buttonTitle: String) {
        showErrorWithAndAndCallsCount += 1
        showErrorWithAndAndReceivedInvocations.append((title: title, description: description, buttonTitle: buttonTitle))
    }

    // MARK: - ShowOptions
    private(set) var showOptionsCallsCount = 0
    private(set) var showOptionsReceivedInvocations: [[OptionAction]] = []

    func showOptions(_ options: [OptionAction]) {
        showOptionsCallsCount += 1
        showOptionsReceivedInvocations.append(options)
    }

    // MARK: - ShowPopupConfirmation
    private(set) var showPopupConfirmationTitleDescriptionCancelButtonTitleConfirmationButtonTitleOptionTypeCallsCount = 0
    private(set) var showPopupConfirmationTitleDescriptionCancelButtonTitleConfirmationButtonTitleOptionTypeReceivedInvocations: [(title: String, description: String, cancelButtonTitle: String, confirmationButtonTitle: String, optionType: OptionType)] = []

    func showPopupConfirmation(
        title: String,
        description: String,
        cancelButtonTitle: String,
        confirmationButtonTitle: String,
        optionType: OptionType
    ) {
        showPopupConfirmationTitleDescriptionCancelButtonTitleConfirmationButtonTitleOptionTypeCallsCount += 1
        showPopupConfirmationTitleDescriptionCancelButtonTitleConfirmationButtonTitleOptionTypeReceivedInvocations.append((title: title, description: description, cancelButtonTitle: cancelButtonTitle, confirmationButtonTitle: confirmationButtonTitle, optionType: optionType))
    }

    // MARK: - ShowPopupNotification
    private(set) var showPopupNotificationTitleDescriptionGenericButtonTitleCallsCount = 0
    private(set) var showPopupNotificationTitleDescriptionGenericButtonTitleReceivedInvocations: [(title: String, description: String, genericButtonTitle: String)] = []

    func showPopupNotification(title: String, description: String, genericButtonTitle: String) {
        showPopupNotificationTitleDescriptionGenericButtonTitleCallsCount += 1
        showPopupNotificationTitleDescriptionGenericButtonTitleReceivedInvocations.append((title: title, description: description, genericButtonTitle: genericButtonTitle))
    }

    // MARK: - SetCommentAvatar
    private(set) var setCommentAvatarWithAndCallsCount = 0
    private(set) var setCommentAvatarWithAndReceivedInvocations: [(url: URL?, placeholder: UIImage)] = []

    func setCommentAvatar(with url: URL?, and placeholder: UIImage) {
        setCommentAvatarWithAndCallsCount += 1
        setCommentAvatarWithAndReceivedInvocations.append((url: url, placeholder: placeholder))
    }

    // MARK: - ShowSnackbarError
    private(set) var showSnackbarErrorWithCallsCount = 0
    private(set) var showSnackbarErrorWithReceivedInvocations: [String] = []

    func showSnackbarError(with message: String) {
        showSnackbarErrorWithCallsCount += 1
        showSnackbarErrorWithReceivedInvocations.append(message)
    }

    // MARK: - AddNewItem
    private(set) var addNewItemCallsCount = 0
    private(set) var addNewItemReceivedInvocations: [ItemDetailDataSource] = []

    func addNewItem(_ dataSourceItem: ItemDetailDataSource) {
        addNewItemCallsCount += 1
        addNewItemReceivedInvocations.append(dataSourceItem)
    }

    // MARK: - RefreshComment
    private(set) var refreshCommentCallsCount = 0
    private(set) var refreshCommentReceivedInvocations: [(dataSourceItem: ItemDetailDataSource, index: Int)] = []

    func refreshComment(_ dataSourceItem: ItemDetailDataSource, _ index: Int) {
        refreshCommentCallsCount += 1
        refreshCommentReceivedInvocations.append((dataSourceItem: dataSourceItem, index: index))
    }
    
    // MARK: - ShowDialog
    private(set) var showDialogTitleMessagePrimaryButtonActionCallsCount = 0
    private(set) var showDialogTitleMessagePrimaryButtonActionReceivedInvocations: [(image: UIImage?, title: String?, message: String?, primaryButtonAction: ApolloAlertAction)] = []

    func showDialog(_ image: UIImage?, title: String?, message: String?, primaryButtonAction: ApolloAlertAction) {
        showDialogTitleMessagePrimaryButtonActionCallsCount += 1
        showDialogTitleMessagePrimaryButtonActionReceivedInvocations.append((image: image, title: title, message: message, primaryButtonAction: primaryButtonAction))
    }

    // MARK: - ShowAbuseOptions
    private(set) var showAbuseOptionsTitleMessageAbuseOptionsCallsCount = 0
    private(set) var showAbuseOptionsTitleMessageAbuseOptionsReceivedInvocations: [(title: String, message: String, abuseOptions: [Abuse])] = []

    func showAbuseOptions(title: String, message: String, abuseOptions: [Abuse]) {
        showAbuseOptionsTitleMessageAbuseOptionsCallsCount += 1
        showAbuseOptionsTitleMessageAbuseOptionsReceivedInvocations.append((title: title, message: message, abuseOptions: abuseOptions))
    }
    
    // MARK: - EnableSendCommentButton
    private(set) var enableSendCommentButtonCallsCount = 0
    private(set) var enableSendCommentButtonReceiveidInvocations: [Bool] = []
    
    func enableSendCommentButton(_ isEnabled: Bool) {
        enableSendCommentButtonCallsCount += 1
        enableSendCommentButtonReceiveidInvocations.append(isEnabled)
    }
}

private class HeaderItemDetailDelegateSpy: HeaderItemDetailDelegate {
    // MARK: - DidTouchPrimaryImage
    private(set) var didTouchPrimaryImageForCallsCount = 0
    private(set) var didTouchPrimaryImageForReceivedInvocations: [URL] = []

    func didTouchPrimaryImage(for deeplinkUrl: URL) {
        didTouchPrimaryImageForCallsCount += 1
        didTouchPrimaryImageForReceivedInvocations.append(deeplinkUrl)
    }

    // MARK: - DidTouchSecondaryImage
    private(set) var didTouchSecondaryImageForCallsCount = 0
    private(set) var didTouchSecondaryImageForReceivedInvocations: [URL] = []

    func didTouchSecondaryImage(for deeplinkUrl: URL) {
        didTouchSecondaryImageForCallsCount += 1
        didTouchSecondaryImageForReceivedInvocations.append(deeplinkUrl)
    }

    // MARK: - DidSelectText
    private(set) var didSelectTextDeeplinkUrlCallsCount = 0
    private(set) var didSelectTextDeeplinkUrlReceivedInvocations: [(text: String, deeplinkUrl: URL)] = []

    func didSelectText(_ text: String, deeplinkUrl: URL) {
        didSelectTextDeeplinkUrlCallsCount += 1
        didSelectTextDeeplinkUrlReceivedInvocations.append((text: text, deeplinkUrl: deeplinkUrl))
    }

    // MARK: - DidTouchLiked
    private(set) var didTouchLikedNumberOfLikesLatestLikesIsLikeUiSocialActionOpenedUpdateCallsCount = 0
    private(set) var didTouchLikedNumberOfLikesLatestLikesIsLikeUiSocialActionOpenedUpdateReceivedInvocations: [(numberOfLikes: Int, latestLikes: [String], isLike: Bool, uiSocialActionOpenedUpdate: UISocialActionOpenedUpdating)] = []

    func didTouchLiked(numberOfLikes: Int, latestLikes: [String], isLike: Bool, uiSocialActionOpenedUpdate: UISocialActionOpenedUpdating) {
        didTouchLikedNumberOfLikesLatestLikesIsLikeUiSocialActionOpenedUpdateCallsCount += 1
        didTouchLikedNumberOfLikesLatestLikesIsLikeUiSocialActionOpenedUpdateReceivedInvocations.append((numberOfLikes: numberOfLikes, latestLikes: latestLikes, isLike: isLike, uiSocialActionOpenedUpdate: uiSocialActionOpenedUpdate))
    }
}

final class ItemDetailPresenterTests: XCTestCase {
    private typealias Localizable = Strings.ItemDetail
    
    private let viewControllerSpy = ItemDetailDisplaySpy()
    private let coordinatorSpy = ItemDetailCoordinatingSpy()
    private let depedenciesMock = DependencyContainerMock()
    private let delegateSpy = HeaderItemDetailDelegateSpy()
    
    private lazy var sut: ItemDetailPresenting = {
        let presenter = ItemDetailPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testStartLoading_ShouldCallViewControllerMethod() {
        sut.startLoading()
        
        XCTAssertEqual(viewControllerSpy.startLoadingCallsCount, 1)
    }
    
    func testStopLoading_ShouldCallViewControllerMethod() {
        sut.stopLoading()
        
        XCTAssertEqual(viewControllerSpy.stopLoadingCallsCount, 1)
    }
    
    
    func testConfigureError_ShouldCallViewControllerMethod() {
        sut.configureError()
        
        XCTAssertEqual(viewControllerSpy.showErrorWithAndAndCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.showErrorWithAndAndReceivedInvocations.first?.title, Localizable.popupErrorTitle)
        XCTAssertEqual(viewControllerSpy.showErrorWithAndAndReceivedInvocations.first?.description, Localizable.popupErrorDescription)
        XCTAssertEqual(viewControllerSpy.showErrorWithAndAndReceivedInvocations.first?.buttonTitle, Strings.okIGotIt)
    }
    
    func testDismiss_ShouldCallCoordinatorMethod() {
        sut.dismiss()
        
        XCTAssertEqual(coordinatorSpy.dismissCallsCount, 1)
    }
    
    func testConfigureHeaderItemDetail_ShouldCallConfigureCommentView() {
        let itemDataSourceMock: [ItemDetailDataSource] = buildItemDataSource()
        
        sut.configureItemDataSource(itemDataSourceMock)
        
        XCTAssertEqual(viewControllerSpy.configureItemDetailCallsCount, 1)
    }
    
    func testConfigureOptions_WhenOptionsAreEmpty_ShouldNotCallViewControllerMethod() {
        sut.configureOptions(for: DataMock.ItemDetailMock.detailWithoutOptions.options)
        
        XCTAssertEqual(viewControllerSpy.showOptionsCallsCount, 0)
    }
    
    func testConfigureOptions_WhenItHasAllOptions_ShouldCallViewControllerMethod() {
        let optionsMock = DataMock.ItemDetailMock.dataWithAllOptions.options
        sut.configureOptions(for: optionsMock)
        
        let receivedOptions = viewControllerSpy.showOptionsReceivedInvocations.first
        let receivedOptionsTypes = receivedOptions?.map { $0.type }
        let receivedOptionsLabels = receivedOptions?.map { $0.label }
        let expectedOptionsTypes = optionsMock
        let expectedOptionsLabels = [
            Localizable.Options.enableNotification,
            Localizable.Options.returnPayment,
            Localizable.Options.reportContent,
            Localizable.Options.returnPayment,
            Localizable.Options.disableNotification,
            Localizable.Options.hideTransaction,
            Localizable.Options.makePrivate,
            Localizable.Options.viewReceipt,
            Localizable.Options.viewReceipt,
            Localizable.Options.viewReceipt,
            Localizable.Options.viewReceipt,
            Localizable.Options.viewReceipt,
        ]
        
        XCTAssertEqual(viewControllerSpy.showOptionsCallsCount, 1)
        XCTAssertEqual(receivedOptions?.count, optionsMock.count)
        XCTAssertEqual(receivedOptionsTypes, expectedOptionsTypes)
        XCTAssertEqual(receivedOptionsLabels, expectedOptionsLabels)
    }
    
    func testConfigureMakePrivateOption_ShouldCallViewControllerMethod() {
        sut.configureMakePrivateOption(type: .makePrivate)
        
        XCTAssertEqual(viewControllerSpy.showPopupConfirmationTitleDescriptionCancelButtonTitleConfirmationButtonTitleOptionTypeCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.showPopupConfirmationTitleDescriptionCancelButtonTitleConfirmationButtonTitleOptionTypeReceivedInvocations.first?.title,
                       Localizable.MakePrivateAlert.title)
        XCTAssertEqual(viewControllerSpy.showPopupConfirmationTitleDescriptionCancelButtonTitleConfirmationButtonTitleOptionTypeReceivedInvocations.first?.description,
                       Localizable.MakePrivateAlert.description)
        XCTAssertEqual(viewControllerSpy.showPopupConfirmationTitleDescriptionCancelButtonTitleConfirmationButtonTitleOptionTypeReceivedInvocations.first?.cancelButtonTitle,
                       Localizable.no)
        XCTAssertEqual(viewControllerSpy.showPopupConfirmationTitleDescriptionCancelButtonTitleConfirmationButtonTitleOptionTypeReceivedInvocations.first?.confirmationButtonTitle,
                       Localizable.yes)
        XCTAssertEqual(viewControllerSpy.showPopupConfirmationTitleDescriptionCancelButtonTitleConfirmationButtonTitleOptionTypeReceivedInvocations.first?.optionType,
                       .makePrivate)
    }
    
    func testConfigureHideTransactionOption_ShouldCallViewControllerMethod() {
        sut.configureHideTransactionOption(type: .hideTransaction)
        
        XCTAssertEqual(viewControllerSpy.showPopupConfirmationTitleDescriptionCancelButtonTitleConfirmationButtonTitleOptionTypeCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.showPopupConfirmationTitleDescriptionCancelButtonTitleConfirmationButtonTitleOptionTypeReceivedInvocations.first?.title,
                       Localizable.HideTransactionAlert.title)
        XCTAssertEqual(viewControllerSpy.showPopupConfirmationTitleDescriptionCancelButtonTitleConfirmationButtonTitleOptionTypeReceivedInvocations.first?.description,
                       Localizable.HideTransactionAlert.description)
        XCTAssertEqual(viewControllerSpy.showPopupConfirmationTitleDescriptionCancelButtonTitleConfirmationButtonTitleOptionTypeReceivedInvocations.first?.cancelButtonTitle,
                       Localizable.no)
        XCTAssertEqual(viewControllerSpy.showPopupConfirmationTitleDescriptionCancelButtonTitleConfirmationButtonTitleOptionTypeReceivedInvocations.first?.confirmationButtonTitle,
                       Localizable.yes)
        XCTAssertEqual(viewControllerSpy.showPopupConfirmationTitleDescriptionCancelButtonTitleConfirmationButtonTitleOptionTypeReceivedInvocations.first?.optionType,
                       .hideTransaction)
    }
    
    func testConfigureDisableNotificationOption_ShouldCallViewControllerMethod() {
        sut.configureDisableNotificationOption(type: .disableNotification)
        
        XCTAssertEqual(viewControllerSpy.showPopupConfirmationTitleDescriptionCancelButtonTitleConfirmationButtonTitleOptionTypeCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.showPopupConfirmationTitleDescriptionCancelButtonTitleConfirmationButtonTitleOptionTypeReceivedInvocations.first?.title,
                       Localizable.DisableNotificationAlert.title)
        XCTAssertEqual(viewControllerSpy.showPopupConfirmationTitleDescriptionCancelButtonTitleConfirmationButtonTitleOptionTypeReceivedInvocations.first?.description,
                       Localizable.DisableNotificationAlert.description)
        XCTAssertEqual(viewControllerSpy.showPopupConfirmationTitleDescriptionCancelButtonTitleConfirmationButtonTitleOptionTypeReceivedInvocations.first?.cancelButtonTitle,
                       Localizable.no)
        XCTAssertEqual(viewControllerSpy.showPopupConfirmationTitleDescriptionCancelButtonTitleConfirmationButtonTitleOptionTypeReceivedInvocations.first?.confirmationButtonTitle,
                       Localizable.yes)
        XCTAssertEqual(viewControllerSpy.showPopupConfirmationTitleDescriptionCancelButtonTitleConfirmationButtonTitleOptionTypeReceivedInvocations.first?.optionType,
                       .disableNotification)
    }
    
    func testOpenReceipt_WhenPassingIdAndTypeAndTypeIsP2P_ShouldCallCoordinatorToOpenReceipt() {
        let detailMock = DataMock.ItemDetailMock.fullData
        sut.openReceipt(type: .viewReceiptP2P, id: detailMock.id.uuidString)
        
        XCTAssertEqual(coordinatorSpy.openReceiptTypeCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.openReceiptTypeReceivedInvocations.first?.id, detailMock.id.uuidString)
        XCTAssertEqual(coordinatorSpy.openReceiptTypeReceivedInvocations.first?.type, .p2p)
    }
    
    func testOpenPixReturnFlow_ShoulCallCoordinatorToOpenPixRefundFlow() {
        let detailMock = DataMock.ItemDetailMock.fullData
        sut.openPixReturnFlow(detailMock.id.uuidString)
    
        XCTAssertEqual(coordinatorSpy.openPixRefundFlowCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.openPixRefundFlowReceivedInvocations[0], detailMock.id.uuidString)
    }

    func testOpenReceipt_WhenPassingIdAndTypeAndTypeIsPav_ShouldCallCoordinatorToOpenReceipt() {
        let detailMock = DataMock.ItemDetailMock.fullData
        sut.openReceipt(type: .viewReceiptBiz, id: detailMock.id.uuidString)
        
        XCTAssertEqual(coordinatorSpy.openReceiptTypeCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.openReceiptTypeReceivedInvocations.first?.id, detailMock.id.uuidString)
        XCTAssertEqual(coordinatorSpy.openReceiptTypeReceivedInvocations.first?.type, .pav)
    }
    
    func testOpenReceipt_WhenPassingIdAndTypeAndTypeIsStore_ShouldCallCoordinatorToOpenReceipt() {
        let detailMock = DataMock.ItemDetailMock.fullData
        sut.openReceipt(type: .viewReceiptStore, id: detailMock.id.uuidString)
        
        XCTAssertEqual(coordinatorSpy.openReceiptTypeCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.openReceiptTypeReceivedInvocations.first?.id, detailMock.id.uuidString)
        XCTAssertEqual(coordinatorSpy.openReceiptTypeReceivedInvocations.first?.type, .store)
    }
    
    func testOpenReceipt_WhenPassingIdAndTypeAndTypeIsPix_ShouldCallCoordinatorToOpenReceipt() {
        let detailMock = DataMock.ItemDetailMock.fullData
        sut.openReceipt(type: .viewReceiptPixTransaction, id: detailMock.id.uuidString)
        
        XCTAssertEqual(coordinatorSpy.openReceiptTypeCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.openReceiptTypeReceivedInvocations.first?.id, detailMock.id.uuidString)
        XCTAssertEqual(coordinatorSpy.openReceiptTypeReceivedInvocations.first?.type, .pix)
    }
    
    func testOpenReceipt_WhenPassingIdAndTypeAndTypeIsPixReceiver_ShouldCallCoordinatorToOpenReceipt() {
        let detailMock = DataMock.ItemDetailMock.fullData
        sut.openReceipt(type: .viewReceiptPixRefund, id: detailMock.id.uuidString)
        
        XCTAssertEqual(coordinatorSpy.openReceiptTypeCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.openReceiptTypeReceivedInvocations.first?.id, detailMock.id.uuidString)
        XCTAssertEqual(coordinatorSpy.openReceiptTypeReceivedInvocations.first?.type, .pixReceiver)
    }
    
    func testOpenReceipt_WhenPassingAnInvalidType_ShouldNotCallCoordinator() {
        sut.openReceipt(type: .returnPaymentP2P, id: "")
        
        XCTAssertEqual(coordinatorSpy.openReceiptTypeCallsCount, 0)
    }
    
    func testRefundPaymentConfirmation_ShouldCallViewControllerMethod() {
        sut.refundPaymentConfirmation(type: .returnPaymentP2P)
        
        XCTAssertEqual(viewControllerSpy.showPopupConfirmationTitleDescriptionCancelButtonTitleConfirmationButtonTitleOptionTypeCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.showPopupConfirmationTitleDescriptionCancelButtonTitleConfirmationButtonTitleOptionTypeReceivedInvocations.first?.title,
                       Localizable.RefundPayment.title)
        XCTAssertEqual(viewControllerSpy.showPopupConfirmationTitleDescriptionCancelButtonTitleConfirmationButtonTitleOptionTypeReceivedInvocations.first?.description,
                       Localizable.RefundPayment.description)
        XCTAssertEqual(viewControllerSpy.showPopupConfirmationTitleDescriptionCancelButtonTitleConfirmationButtonTitleOptionTypeReceivedInvocations.first?.cancelButtonTitle,
                       Localizable.no)
        XCTAssertEqual(viewControllerSpy.showPopupConfirmationTitleDescriptionCancelButtonTitleConfirmationButtonTitleOptionTypeReceivedInvocations.first?.confirmationButtonTitle,
                       Localizable.yes)
        XCTAssertEqual(viewControllerSpy.showPopupConfirmationTitleDescriptionCancelButtonTitleConfirmationButtonTitleOptionTypeReceivedInvocations.first?.optionType,
                       .returnPaymentP2P)
    }
    
    func testShowDialog_WhenEnableNotificationIsTrue_ShouldCallViewControllerMethod() {
        sut.showDialog(enableNotification: true)
        
        XCTAssertEqual(viewControllerSpy.showPopupNotificationTitleDescriptionGenericButtonTitleCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.showPopupNotificationTitleDescriptionGenericButtonTitleReceivedInvocations.first?.title,
                       Localizable.EnableNotificationAlert.title)
        XCTAssertEqual(viewControllerSpy.showPopupNotificationTitleDescriptionGenericButtonTitleReceivedInvocations.first?.description,
                       Localizable.EnableNotificationAlert.description)
        XCTAssertEqual(viewControllerSpy.showPopupNotificationTitleDescriptionGenericButtonTitleReceivedInvocations.first?.genericButtonTitle,
                       Localizable.ok)
    }
    
    func testShowDialog_WhenEnableNotificationIsFalse_ShouldCallViewControllerMethod() {
        sut.showDialog(enableNotification: false)
        
        XCTAssertEqual(viewControllerSpy.showPopupNotificationTitleDescriptionGenericButtonTitleCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.showPopupNotificationTitleDescriptionGenericButtonTitleReceivedInvocations.first?.title,
                       Localizable.DisabledNotificationAlert.title)
        XCTAssertEqual(viewControllerSpy.showPopupNotificationTitleDescriptionGenericButtonTitleReceivedInvocations.first?.description,
                       Localizable.DisabledNotificationAlert.description)
        XCTAssertEqual(viewControllerSpy.showPopupNotificationTitleDescriptionGenericButtonTitleReceivedInvocations.first?.genericButtonTitle,
                       Localizable.ok)
    }
    
    func testShowOptionGenericError_ShouldCallViewControllerMethod() {
        sut.showOptionGenericError()
        
        XCTAssertEqual(viewControllerSpy.showPopupNotificationTitleDescriptionGenericButtonTitleCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.showPopupNotificationTitleDescriptionGenericButtonTitleReceivedInvocations.first?.title,
                       Localizable.GenericError.title)
        XCTAssertEqual(viewControllerSpy.showPopupNotificationTitleDescriptionGenericButtonTitleReceivedInvocations.first?.description,
                       Localizable.GenericError.description)
        XCTAssertEqual(viewControllerSpy.showPopupNotificationTitleDescriptionGenericButtonTitleReceivedInvocations.first?.genericButtonTitle,
                       Localizable.ok)
    }
    
    func testShowRefundSuccess_ShouldCallViewControllerMethod() {
        sut.showRefundSuccess()
        
        XCTAssertEqual(viewControllerSpy.showPopupNotificationTitleDescriptionGenericButtonTitleCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.showPopupNotificationTitleDescriptionGenericButtonTitleReceivedInvocations.first?.title, "")
        XCTAssertEqual(viewControllerSpy.showPopupNotificationTitleDescriptionGenericButtonTitleReceivedInvocations.first?.description,
                       Localizable.RefundPayment.success)
        XCTAssertEqual(viewControllerSpy.showPopupNotificationTitleDescriptionGenericButtonTitleReceivedInvocations.first?.genericButtonTitle,
                       Localizable.ok)
    }
    
    func testConfigureCommentInput_ShouldCallViewControllerMethod() {
        let urlMock = URL(string: "")
        sut.configureImageCommentInput(urlMock)
        
        XCTAssertEqual(viewControllerSpy.setCommentAvatarWithAndCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.setCommentAvatarWithAndReceivedInvocations.first?.url, urlMock)
        XCTAssertEqual(viewControllerSpy.setCommentAvatarWithAndReceivedInvocations.first?.placeholder,
                       Resources.Placeholders.greenAvatarPlaceholder.image)
    }
    
    func testAddComment_ShouldCallViewControllerMethod() {
        let commentMock = DataMock.CommentMock.comment
        let commentsDataSourceMock: ItemDetailDataSource = {
            let presenterMock = CommentPresenter()
            let interactorMock = CommentInteractor(presenter: presenterMock, comment: commentMock, index: 0)
            let dataSource: ItemDetailDataSource = .comment(presenter: presenterMock, interactor: interactorMock)
            return dataSource
        }()
        
        sut.addComment(with: commentsDataSourceMock)
        
        XCTAssertEqual(viewControllerSpy.addNewItemCallsCount, 1)
    }
    
    func testEnableSendCommentButton_ShouldCallViewControllerMethod() {
        sut.enableSendCommentButton(true)
        
        XCTAssertEqual(viewControllerSpy.enableSendCommentButtonCallsCount, 1)
    }
    
    func testShowAbuseOptions_ShouldCallViewControllerMethod() {
        let actionMock = OptionsAction(title: "title", message: "message", actions: [])
        
        sut.showAbuseOptions(title: actionMock.title, message: actionMock.message, abuseOptions: actionMock.actions)
        
        XCTAssertEqual(viewControllerSpy.showAbuseOptionsTitleMessageAbuseOptionsCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.showAbuseOptionsTitleMessageAbuseOptionsReceivedInvocations.first?.title, actionMock.title)
        XCTAssertEqual(viewControllerSpy.showAbuseOptionsTitleMessageAbuseOptionsReceivedInvocations.first?.message, actionMock.message)
        XCTAssertEqual(viewControllerSpy.showAbuseOptionsTitleMessageAbuseOptionsReceivedInvocations.first?.abuseOptions, actionMock.actions)
    }
    
    func testShowSnackbarError_WhenErrorTypeIsTooLongComment_ShouldCallShowSnackbarErrorInViewController() {
        sut.showSnackbarError(errorType: .tooLongComment)
        
        XCTAssertEqual(viewControllerSpy.showSnackbarErrorWithCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.showSnackbarErrorWithReceivedInvocations.first, Localizable.Comments.tooLongCommentError)
    }
    
    func testShowSnackbarError_WhenErrorTypeIsPublishing_ShouldCallShowSnackbarErrorInViewController() {
        sut.showSnackbarError(errorType: .publishingComment)
        
        XCTAssertEqual(viewControllerSpy.showSnackbarErrorWithCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.showSnackbarErrorWithReceivedInvocations.first, Localizable.Comments.publishingError)
    }
    
    func testShowSnackbarError_WhenErrorTypeIsLikeCard_ShouldCallShowSnackbarErrorInViewController() {
        sut.showSnackbarError(errorType: .likeCard)
        
        XCTAssertEqual(viewControllerSpy.showSnackbarErrorWithCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.showSnackbarErrorWithReceivedInvocations.first, Localizable.Like.error)
    }
    
    func testShowAbuseSuccess_ShouldCallViewControllerMethod() {
        sut.showAbuseSuccess()
        
        let receivedInvocations = viewControllerSpy.showDialogTitleMessagePrimaryButtonActionReceivedInvocations.first
        
        XCTAssertEqual(viewControllerSpy.showDialogTitleMessagePrimaryButtonActionCallsCount, 1)
        XCTAssertEqual(receivedInvocations?.image, Resources.Icons.icoBigSuccess.image)
        XCTAssertEqual(receivedInvocations?.title, Strings.thanksForYourHelp)
        XCTAssertEqual(receivedInvocations?.message, Strings.letsReviewYourReport)
    }
    
    func testRefreshCommentList_ShouldCallViewControllerMethod() {
        let itemDetailMock = DataMock.ItemDetailMock.fullData
        let headerDetailPresenter = HeaderItemDetailPresenter(dependencies: depedenciesMock)
        let headerDetailInteractor = HeaderItemDetailInteractor(itemDetail: itemDetailMock, presenter: headerDetailPresenter, delegate: delegateSpy)
        let dataSourceMock: ItemDetailDataSource = .header(presenter: headerDetailPresenter, interactor: headerDetailInteractor)
        
        sut.refreshCommentList(with: dataSourceMock, at: 0)
        
        XCTAssertEqual(viewControllerSpy.refreshCommentCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.refreshCommentReceivedInvocations.first?.index, 0)
    }
    
    private func buildItemDataSource() -> [ItemDetailDataSource] {
        let itemDetailMock = DataMock.ItemDetailMock.fullData
        let commentsDataSourceMock = itemDetailMock.comments.enumerated().map { index, item -> ItemDetailDataSource in
            let presenterMock = CommentPresenter()
            let interactorMock = CommentInteractor(presenter: presenterMock, comment: item, index: index)
            return .comment(presenter: presenterMock, interactor: interactorMock)
        }
        
        let headerPresenterMock = HeaderItemDetailPresenter(dependencies: depedenciesMock)
        let headerInteractorMock = HeaderItemDetailInteractor(itemDetail: itemDetailMock, presenter: headerPresenterMock, delegate: nil)
        var itemDataSourceMock: [ItemDetailDataSource] = [.header(presenter: headerPresenterMock, interactor: headerInteractorMock)]
        itemDataSourceMock.append(contentsOf: commentsDataSourceMock)
        
        return itemDataSourceMock
    }
}
