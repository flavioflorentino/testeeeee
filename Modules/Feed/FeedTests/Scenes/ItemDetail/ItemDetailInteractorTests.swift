import Core
import AnalyticsModule
import XCTest
@testable import Feed

private class ItemDetailServicingMock: ItemDetailServicing {
    // MARK: - FetchItemDetail
    private(set) var fetchItemDetailCardIdCallsCount = 0
    private(set) var fetchItemDetailCardIdReceivedInvocations: [(cardId: UUID, completion: (Result<ItemDetail, ApiError>) -> Void)] = []
    var fetchItemDetailCardIdClosure: ((UUID, @escaping (Result<ItemDetail, ApiError>) -> Void) -> Void)?

    func fetchItemDetail(cardId: UUID, _ completion: @escaping (Result<ItemDetail, ApiError>) -> Void) {
        fetchItemDetailCardIdCallsCount += 1
        fetchItemDetailCardIdReceivedInvocations.append((cardId: cardId, completion: completion))
        fetchItemDetailCardIdClosure?(cardId, completion)
    }

    // MARK: - ChangeVisibility
    private(set) var changeVisibilityCardIdIsPrivateCallsCount = 0
    private(set) var changeVisibilityCardIdIsPrivateReceivedInvocations: [(cardId: UUID, isPrivate: Bool, completion: (Result<NoContent, ApiError>) -> Void)] = []
    var changeVisibilityCardIdIsPrivateClosure: ((UUID, Bool, @escaping (Result<NoContent, ApiError>) -> Void) -> Void)?

    func changeVisibility(cardId: UUID, isPrivate: Bool, _ completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        changeVisibilityCardIdIsPrivateCallsCount += 1
        changeVisibilityCardIdIsPrivateReceivedInvocations.append((cardId: cardId, isPrivate: isPrivate, completion: completion))
        changeVisibilityCardIdIsPrivateClosure?(cardId, isPrivate, completion)
    }

    // MARK: - ChangeNotifications
    private(set) var changeNotificationsCardIdIsFollowingCallsCount = 0
    private(set) var changeNotificationsCardIdIsFollowingReceivedInvocations: [(cardId: UUID, isFollowing: Bool, completion: (Result<NoContent, ApiError>) -> Void)] = []
    var changeNotificationsCardIdIsFollowingClosure: ((UUID, Bool, @escaping (Result<NoContent, ApiError>) -> Void) -> Void)?

    func changeNotifications(cardId: UUID, isFollowing: Bool, _ completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        changeNotificationsCardIdIsFollowingCallsCount += 1
        changeNotificationsCardIdIsFollowingReceivedInvocations.append((cardId: cardId, isFollowing: isFollowing, completion: completion))
        changeNotificationsCardIdIsFollowingClosure?(cardId, isFollowing, completion)
    }

    // MARK: - RefundPayment
    private(set) var refundPaymentTransactionIdCallsCount = 0
    private(set) var refundPaymentTransactionIdReceivedInvocations: [(transactionId: UUID, completion: (Result<NoContent, ApiError>) -> Void)] = []
    var refundPaymentTransactionIdClosure: ((UUID, @escaping (Result<NoContent, ApiError>) -> Void) -> Void)?

    func refundPayment(transactionId: UUID, _ completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        refundPaymentTransactionIdCallsCount += 1
        refundPaymentTransactionIdReceivedInvocations.append((transactionId: transactionId, completion: completion))
        refundPaymentTransactionIdClosure?(transactionId, completion)
    }

    // MARK: - UpdateLikeState
    private(set) var updateLikeStateFromCompletionCallsCount = 0
    private(set) var updateLikeStateFromCompletionReceivedInvocations: [(endpoint: ItemDetailServiceEndpoint, completion: (Result<NoContent, ApiError>) -> Void)] = []
    var updateLikeStateFromCompletionClosure: ((ItemDetailServiceEndpoint, @escaping (Result<NoContent, ApiError>) -> Void) -> Void)?

    func updateLikeState(from endpoint: ItemDetailServiceEndpoint, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        updateLikeStateFromCompletionCallsCount += 1
        updateLikeStateFromCompletionReceivedInvocations.append((endpoint: endpoint, completion: completion))
        updateLikeStateFromCompletionClosure?(endpoint, completion)
    }

    // MARK: - CommentCard
    private(set) var commentCardCardIdCommentCallsCount = 0
    private(set) var commentCardCardIdCommentReceivedInvocations: [(cardId: UUID, comment: String, completion: (Result<NoContent, ApiError>) -> Void)] = []
    var commentCardCardIdCommentClosure: ((UUID, String, @escaping (Result<NoContent, ApiError>) -> Void) -> Void)?

    func commentCard(cardId: UUID, comment: String, _ completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        commentCardCardIdCommentCallsCount += 1
        commentCardCardIdCommentReceivedInvocations.append((cardId: cardId, comment: comment, completion: completion))
        commentCardCardIdCommentClosure?(cardId, comment, completion)
    }

    // MARK: - ListAbuseOptions
    private(set) var listAbuseOptionsCardIdCallsCount = 0
    private(set) var listAbuseOptionsCardIdReceivedInvocations: [(cardId: UUID, completion: (Result<AbuseData, ApiError>) -> Void)] = []
    var listAbuseOptionsCardIdClosure: ((UUID, @escaping (Result<AbuseData, ApiError>) -> Void) -> Void)?

    func listAbuseOptions(cardId: UUID, _ completion: @escaping (Result<AbuseData, ApiError>) -> Void) {
        listAbuseOptionsCardIdCallsCount += 1
        listAbuseOptionsCardIdReceivedInvocations.append((cardId: cardId, completion: completion))
        listAbuseOptionsCardIdClosure?(cardId, completion)
    }

    // MARK: - SendAbuse
    private(set) var sendAbuseReportedIdCallsCount = 0
    private(set) var sendAbuseReportedIdReceivedInvocations: [(reportedId: String, completion: (Result<NoContent, ApiError>) -> Void)] = []
    var sendAbuseReportedIdClosure: ((String, @escaping (Result<NoContent, ApiError>) -> Void) -> Void)?

    func sendAbuse(reportedId: String, _ completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        sendAbuseReportedIdCallsCount += 1
        sendAbuseReportedIdReceivedInvocations.append((reportedId: reportedId, completion: completion))
        sendAbuseReportedIdClosure?(reportedId, completion)
    }
}

private class ItemDetailPresentingSpy: ItemDetailPresenting {
    var viewController: ItemDetailDisplaying?

    // MARK: - StartLoading
    private(set) var startLoadingCallsCount = 0

    func startLoading() {
        startLoadingCallsCount += 1
    }

    // MARK: - StopLoading
    private(set) var stopLoadingCallsCount = 0

    func stopLoading() {
        stopLoadingCallsCount += 1
    }

    // MARK: - ConfigureItemDataSource
    private(set) var configureItemDataSourceCallsCount = 0
    private(set) var configureItemDataSourceReceivedInvocations: [[ItemDetailDataSource]] = []

    func configureItemDataSource(_ itemDataSource: [ItemDetailDataSource]) {
        configureItemDataSourceCallsCount += 1
        configureItemDataSourceReceivedInvocations.append(itemDataSource)
    }

    // MARK: - ConfigureError
    private(set) var configureErrorCallsCount = 0

    func configureError() {
        configureErrorCallsCount += 1
    }

    // MARK: - Dismiss
    private(set) var dismissCallsCount = 0

    func dismiss() {
        dismissCallsCount += 1
    }

    // MARK: - ConfigureOptions
    private(set) var configureOptionsForCallsCount = 0
    private(set) var configureOptionsForReceivedInvocations: [[OptionType]] = []

    func configureOptions(for itemDetailOptions: [OptionType]) {
        configureOptionsForCallsCount += 1
        configureOptionsForReceivedInvocations.append(itemDetailOptions)
    }

    // MARK: - OpenReceipt
    private(set) var openReceiptTypeIdCallsCount = 0
    private(set) var openReceiptTypeIdReceivedInvocations: [(type: OptionType, id: String)] = []

    func openReceipt(type: OptionType, id: String) {
        openReceiptTypeIdCallsCount += 1
        openReceiptTypeIdReceivedInvocations.append((type: type, id: id))
    }

    // MARK: - ConfigureMakePrivateOption
    private(set) var configureMakePrivateOptionTypeCallsCount = 0
    private(set) var configureMakePrivateOptionTypeReceivedInvocations: [OptionType] = []

    func configureMakePrivateOption(type: OptionType) {
        configureMakePrivateOptionTypeCallsCount += 1
        configureMakePrivateOptionTypeReceivedInvocations.append(type)
    }

    // MARK: - ConfigureHideTransactionOption
    private(set) var configureHideTransactionOptionTypeCallsCount = 0
    private(set) var configureHideTransactionOptionTypeReceivedInvocations: [OptionType] = []

    func configureHideTransactionOption(type: OptionType) {
        configureHideTransactionOptionTypeCallsCount += 1
        configureHideTransactionOptionTypeReceivedInvocations.append(type)
    }

    // MARK: - ConfigureDisableNotificationOption
    private(set) var configureDisableNotificationOptionTypeCallsCount = 0
    private(set) var configureDisableNotificationOptionTypeReceivedInvocations: [OptionType] = []

    func configureDisableNotificationOption(type: OptionType) {
        configureDisableNotificationOptionTypeCallsCount += 1
        configureDisableNotificationOptionTypeReceivedInvocations.append(type)
    }

    // MARK: - ShowDialog
    private(set) var showDialogEnableNotificationCallsCount = 0
    private(set) var showDialogEnableNotificationReceivedInvocations: [Bool] = []

    func showDialog(enableNotification: Bool) {
        showDialogEnableNotificationCallsCount += 1
        showDialogEnableNotificationReceivedInvocations.append(enableNotification)
    }

    // MARK: - OpenPixReturnFlow
    private(set) var openPixReturnFlowCallsCount = 0
    private(set) var openPixReturnFlowReceivedInvocations: [String] = []

    func openPixReturnFlow(_ cardId: String) {
        openPixReturnFlowCallsCount += 1
        openPixReturnFlowReceivedInvocations.append(cardId)
    }

    // MARK: - ShowAbuseOptions
    private(set) var showAbuseOptionsTitleMessageAbuseOptionsCallsCount = 0
    private(set) var showAbuseOptionsTitleMessageAbuseOptionsReceivedInvocations: [(title: String, message: String, abuseOptions: [Abuse])] = []

    func showAbuseOptions(title: String, message: String, abuseOptions: [Abuse]) {
        showAbuseOptionsTitleMessageAbuseOptionsCallsCount += 1
        showAbuseOptionsTitleMessageAbuseOptionsReceivedInvocations.append((title: title, message: message, abuseOptions: abuseOptions))
    }

    // MARK: - RefundPaymentConfirmation
    private(set) var refundPaymentConfirmationTypeCallsCount = 0
    private(set) var refundPaymentConfirmationTypeReceivedInvocations: [OptionType] = []

    func refundPaymentConfirmation(type: OptionType) {
        refundPaymentConfirmationTypeCallsCount += 1
        refundPaymentConfirmationTypeReceivedInvocations.append(type)
    }

    // MARK: - ShowOptionGenericError
    private(set) var showOptionGenericErrorCallsCount = 0

    func showOptionGenericError() {
        showOptionGenericErrorCallsCount += 1
    }

    // MARK: - ShowRefundSuccess
    private(set) var showRefundSuccessCallsCount = 0

    func showRefundSuccess() {
        showRefundSuccessCallsCount += 1
    }

    // MARK: - ConfigureImageCommentInput
    private(set) var configureImageCommentInputCallsCount = 0
    private(set) var configureImageCommentInputReceivedInvocations: [URL?] = []

    func configureImageCommentInput(_ urlImage: URL?) {
        configureImageCommentInputCallsCount += 1
        configureImageCommentInputReceivedInvocations.append(urlImage)
    }
    
    // MARK: - ShowAbuseSuccess
    private(set) var showAbuseSuccessCallsCount = 0

    func showAbuseSuccess() {
        showAbuseSuccessCallsCount += 1
    }

    // MARK: - AddComment
    private(set) var addCommentWithCallsCount = 0
    private(set) var addCommentWithReceivedInvocations: [ItemDetailDataSource] = []

    func addComment(with item: ItemDetailDataSource) {
        addCommentWithCallsCount += 1
        addCommentWithReceivedInvocations.append(item)
    }

    // MARK: - RefreshCommentList
    private(set) var refreshCommentListWithAtCallsCount = 0
    private(set) var refreshCommentListWithAtReceivedInvocations: [(dataSourceItem: ItemDetailDataSource, index: Int)] = []

    func refreshCommentList(with dataSourceItem: ItemDetailDataSource, at index: Int) {
        refreshCommentListWithAtCallsCount += 1
        refreshCommentListWithAtReceivedInvocations.append((dataSourceItem: dataSourceItem, index: index))
    }

    // MARK: - ShowSnackbarError
    private(set) var showSnackbarErrorErrorTypeCallsCount = 0
    private(set) var showSnackbarErrorErrorTypeReceivedInvocations: [ItemDetailErrorType] = []

    func showSnackbarError(errorType: ItemDetailErrorType) {
        showSnackbarErrorErrorTypeCallsCount += 1
        showSnackbarErrorErrorTypeReceivedInvocations.append(errorType)
    }
    // MARK: - EnableSendCommentButton
    private(set) var enableSendCommentButtonCallsCount = 0
    private(set) var enableSendCommentButtonReceiveidInvocations: [Bool] = []
    
    func enableSendCommentButton(_ isEnabled: Bool) {
        enableSendCommentButtonCallsCount += 1
        enableSendCommentButtonReceiveidInvocations.append(isEnabled)
    }
}

private final class ItemDetailDelegateSpy: ItemDetailDelegate {
    //MARK: - openPixRefundFlow
    private(set) var openPixRefundFlowCallsCount = 0
    private(set) var openPixRefundFlowReceivedInvocations: [String] = []

    func openPixRefundFlow(_ transactionId: String) {
        openPixRefundFlowCallsCount += 1
        openPixRefundFlowReceivedInvocations.append(transactionId)
    }

    //MARK: - refundPaymentP2P
    private(set) var refundPaymentP2PDataCompletionCallsCount = 0
    private(set) var refundPaymentP2PDataCompletionReceivedInvocations: [(data: [AnyHashable: Any], completion: (_ success: Bool, _ error: Error?) -> Void)] = []
    var successExpected: Bool = false
    var errorExpected: Error? = nil

    func refundPaymentP2P(data: [AnyHashable: Any], completion: @escaping (_ success: Bool, _ error: Error?) -> Void) {
        refundPaymentP2PDataCompletionCallsCount += 1
        refundPaymentP2PDataCompletionReceivedInvocations.append((data: data, completion: completion))
        
        completion(successExpected, errorExpected)
    }
}

private final class ScreenActionableSpy: ScreenActionable {
    // MARK: - Screen
    private(set) var screenForCallsCount = 0
    private(set) var screenForReceivedInvocations: [ActionScreenType] = []
    func screen(for type: ActionScreenType) {
        screenForCallsCount += 1
        screenForReceivedInvocations.append(type)
    }
}

private final class AuthenticationContractMock: AuthenticationContract {
    private var underlyingIsAuthenticated: Bool?
    
    var isAuthenticated: Bool {
        get { return underlyingIsAuthenticated ?? false }
        set(value) { underlyingIsAuthenticated = value }
    }

    // MARK: - Authenticate
    private(set) var authenticateCallsCount = 0
    private(set) var authenticateReceivedInvocations: [AuthenticationResult] = []
    var authenticateClosure: ((@escaping AuthenticationResult) -> Void)?

    func authenticate(_ completion: @escaping AuthenticationResult) {
        authenticateCallsCount += 1
        authenticateReceivedInvocations.append(completion)
        authenticateClosure?(completion)
    }

    private(set) var disableBiometricAuthenticationCallCount = 0
    func disableBiometricAuthentication() {
        disableBiometricAuthenticationCallCount += 1
    }
}

private final class DeeplinkContractSpy: DeeplinkContract {
    // MARK: - Open
    private(set) var openUrlCallsCount = 0
    private(set) var openUrlReceivedInvocations: [URL] = []
    var openUrlReturnValue: Bool = true

    func open(url: URL) -> Bool {
        openUrlCallsCount += 1
        openUrlReceivedInvocations.append(url)
        return openUrlReturnValue
    }
}

private final class FeedConsumerContractMock: FeedConsumerContract {
    private var underlyingConsumerId: Int?
    var consumerId: Int {
        get { return underlyingConsumerId ?? 0 }
        set(value) { underlyingConsumerId = value }
    }

    private var underlyingName: String?
    var name: String {
        get { return underlyingName ?? "" }
        set(value) { underlyingName = value }
    }

    var username: String?
    var imageURL: URL?
}

final class ItemDetailInteractorTests: XCTestCase {
    private let authenticationMock = AuthenticationContractMock()
    private let deeplinkSpy = DeeplinkContractSpy()
    
    private var consumerMock: FeedConsumerContractMock = {
        let consumer = FeedConsumerContractMock()
        consumer.username = "teste"
        consumer.imageURL = URL(string: "https://i.pravatar.cc/250?img=1")!
        return consumer
    }()

    private let cardId = UUID()
    private let serviceMock = ItemDetailServicingMock()
    private let presenterSpy = ItemDetailPresentingSpy()
    private let actionableSpy = ScreenActionableSpy()
    private let analyticsSpy = AnalyticsSpy()
    private lazy var containerMock = DependencyContainerMock(
        authenticationMock,
        deeplinkSpy,
        consumerMock,
        analyticsSpy
    )

    private lazy var sut = ItemDetailInteractor(
        cardId: cardId,
        presenter: presenterSpy,
        service: serviceMock,
        dependencies: containerMock,
        actionable: actionableSpy
    )
        
    func testFetchItemDetail_WhenSuccess_ShouldCallConfigureItemDetailOnPresent() {
        serviceMock.fetchItemDetailCardIdClosure = { _, completion in
            completion(.success(DataMock.ItemDetailMock.fullData))
        }
        
        sut.fetchItemDetail()
        
        XCTAssertEqual(serviceMock.fetchItemDetailCardIdCallsCount, 1)
        XCTAssertEqual(presenterSpy.startLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.configureErrorCallsCount, 0)
        XCTAssertEqual(presenterSpy.configureItemDataSourceCallsCount, 1)
        XCTAssertEqual(presenterSpy.configureImageCommentInputCallsCount, 1)
    }
    
    func testFetchItemDetail_WhenFailure_ShouldCallConfigureErrorPresent() {
        serviceMock.fetchItemDetailCardIdClosure = { _, completion in
            completion(.failure(.connectionFailure))
        }
        
        sut.fetchItemDetail()
        
        XCTAssertEqual(serviceMock.fetchItemDetailCardIdCallsCount, 1)
        XCTAssertEqual(presenterSpy.startLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.configureErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.configureItemDataSourceCallsCount, 0)
        XCTAssertEqual(presenterSpy.configureImageCommentInputCallsCount, 0)
    }
    
    func testDismiss_ShouldCallPresenterMethodToDismiss() {
        sut.dismiss()
        
        XCTAssertEqual(presenterSpy.dismissCallsCount, 1)
    }
    
    func testLoadOptions_WhenHasItemDetail_ShouldCallPresenterMethod() {
        serviceMock.fetchItemDetailCardIdClosure = { _, completion in
            completion(.success(DataMock.ItemDetailMock.fullData))
        }
        
        sut.fetchItemDetail()
        sut.loadOptions()
        
        let expectedEvent = FeedAnalytics.buttonTapped(type: .options).event()
        
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(presenterSpy.configureOptionsForCallsCount, 1)
        XCTAssertEqual(presenterSpy.configureOptionsForReceivedInvocations.first, [.enableNotification])
    }
    
    func testLoadOptions_WhenItemDetailIsNil_ShouldSendEventButNotCallPresenterMethod() {
        sut.loadOptions()
        
        let expectedEvent = FeedAnalytics.buttonTapped(type: .options).event()
        
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(presenterSpy.configureOptionsForCallsCount, 1)
    }
    
    func testHandleAction_WhenActionIsOptions_ShouldCallShowAbuseOptions() {
        sut.handleAction(
            Action(
                type: .options,
                data: .options(
                    OptionsAction(
                        title: "Title Option",
                        message: "Description Option",
                        actions: [
                            Abuse(
                                id: UUID(),
                                text: "Option 1",
                                action: Action(type: .screen, data: .screen(ScreenAction(type: .abuseReportDetail)))
                            )
                        ]
                    )
                )
            )
        )
        
        XCTAssertEqual(presenterSpy.showAbuseOptionsTitleMessageAbuseOptionsCallsCount, 1)
        XCTAssertEqual(presenterSpy.showAbuseOptionsTitleMessageAbuseOptionsReceivedInvocations.first?.title, "Title Option")
        XCTAssertEqual(presenterSpy.showAbuseOptionsTitleMessageAbuseOptionsReceivedInvocations.first?.message, "Description Option")
        XCTAssertEqual(presenterSpy.showAbuseOptionsTitleMessageAbuseOptionsReceivedInvocations.first?.abuseOptions.count, 1)
    }
    
    func testHandleAction_WhenActionIsDeeplink_ShouldCallOpenDeeplinkManager() {
        sut.handleAction(
            Action(
                type: .deeplink,
                data: .deeplink(
                    DeeplinkAction(
                        url: URL(string: "http://picpay.com")!
                    )
                )
            )
        )
        
        XCTAssertEqual(deeplinkSpy.openUrlCallsCount, 1)
        XCTAssertEqual(deeplinkSpy.openUrlReceivedInvocations.first, URL(string: "http://picpay.com"))
    }
    
    func testHandleAction_WhenActionIsScreenAndAbuseReportSuccess_ShouldCallShowAbuseSuccess() {
        sut.handleAction(
            Action(
                type: .screen,
                data: .screen(
                    ScreenAction(
                        type: .abuseReportSuccess
                    )
                )
            )
        )
        
        XCTAssertEqual(presenterSpy.showAbuseSuccessCallsCount, 1)
    }
    
    func testHandleAction_WhenActionIsScreenAndAbuseReportDetail_ShouldCallActionableScreen() {
        sut.handleAction(
            Action(
                type: .screen,
                data: .screen(
                    ScreenAction(
                        type: .abuseReportDetail
                    )
                )
            )
        )
        
        XCTAssertEqual(actionableSpy.screenForCallsCount, 1)
        XCTAssertEqual(actionableSpy.screenForReceivedInvocations.first, .abuseReportDetail(type: .transactionalCard, id: cardId))
    }
    
    
    func testHandleOption_WhenOptionIsMakePrivate_ShouldCallPresenterMethod() {
        serviceMock.fetchItemDetailCardIdClosure = { _, completion in
            completion(.success(DataMock.ItemDetailMock.fullData))
        }
        
        sut.fetchItemDetail()
        
        let optionActionMock = OptionAction(type: .makePrivate)
        let expectedEvent = FeedAnalytics.feedDetailOptionInteraction(optionType: .makePrivate, cardId: cardId.uuidString).event()
                
        sut.handleOption(optionActionMock)
        
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(presenterSpy.configureMakePrivateOptionTypeCallsCount, 1)
    }
    
    func testHandleOption_WhenOptionIsHideTransaction_ShouldCallPresenterMethod() {
        serviceMock.fetchItemDetailCardIdClosure = { _, completion in
            completion(.success(DataMock.ItemDetailMock.fullData))
        }
        
        sut.fetchItemDetail()
        let optionActionMock = OptionAction(type: .hideTransaction)
        let expectedEvent = FeedAnalytics.feedDetailOptionInteraction(optionType: .hideTransaction, cardId: cardId.uuidString).event()
        
        sut.handleOption(optionActionMock)
        
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(presenterSpy.configureHideTransactionOptionTypeCallsCount, 1)
    }
    
    func testHandleOption_WhenOptionIsEnableNotification_ShouldCallService() {
        serviceMock.fetchItemDetailCardIdClosure = { _, completion in
            completion(.success(DataMock.ItemDetailMock.fullData))
        }
        
        sut.fetchItemDetail()
                
        serviceMock.changeNotificationsCardIdIsFollowingClosure = { _, _, completion in
            completion(.success(NoContent()))
        }
        
        let optionActionMock = OptionAction(type: .enableNotification)
        let expectedEvent = FeedAnalytics.feedDetailOptionInteraction(optionType: .enableNotification, cardId: cardId.uuidString).event()
        
        sut.handleOption(optionActionMock)
        
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(serviceMock.changeNotificationsCardIdIsFollowingCallsCount, 1)
    }
    
    func testHandleOption_WhenOptionIsReportContentAndSuccessFromService_ShouldCallShowAbuseOptions() {
        serviceMock.fetchItemDetailCardIdClosure = { _, completion in
            completion(.success(DataMock.ItemDetailMock.fullData))
        }
        sut.fetchItemDetail()
        
        serviceMock.listAbuseOptionsCardIdClosure = { _, completion in
            completion(
                .success(
                    AbuseData(
                        title: "Option Title",
                        message: "Option Description",
                        data: [
                            Abuse(id: UUID(), text: "Option 1", action: Action(type: .screen, data: .screen(ScreenAction(type: .abuseReportDetail))))
                        ]
                    )
                )
            )
        }
        
        sut.handleOption(OptionAction(type: .reportContent))
        let expectedEvent = FeedAnalytics.feedDetailOptionInteraction(optionType: .reportCardContent, cardId: cardId.uuidString).event()
        
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(presenterSpy.showAbuseOptionsTitleMessageAbuseOptionsCallsCount, 1)
        XCTAssertEqual(presenterSpy.showAbuseOptionsTitleMessageAbuseOptionsReceivedInvocations.first?.title, "Option Title")
        XCTAssertEqual(presenterSpy.showAbuseOptionsTitleMessageAbuseOptionsReceivedInvocations.first?.message, "Option Description")
        XCTAssertEqual(presenterSpy.showAbuseOptionsTitleMessageAbuseOptionsReceivedInvocations.first?.abuseOptions.count, 1)
    }
    
    func testHandleOption_WhenOptionIsReportContentAndFailureFromService_ShouldCallShowOptionGenericError() {
        serviceMock.listAbuseOptionsCardIdClosure = { _, completion in
            completion(.failure(.connectionFailure))
        }
        
        sut.handleOption(OptionAction(type: .reportContent))
        
        XCTAssertEqual(presenterSpy.showOptionGenericErrorCallsCount, 1)
    }
    
    func testHandleOption_WhenOptionIsDisableNotification_ShouldCallPresenterMethod() {
        serviceMock.fetchItemDetailCardIdClosure = { _, completion in
            completion(.success(DataMock.ItemDetailMock.fullData))
        }
        
        sut.fetchItemDetail()
        
        let optionActionMock = OptionAction(type: .disableNotification)
        let expectedEvent = FeedAnalytics.feedDetailOptionInteraction(optionType: .disableNotification, cardId: cardId.uuidString).event()
        
        sut.handleOption(optionActionMock)
        
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(presenterSpy.configureDisableNotificationOptionTypeCallsCount, 1)
    }
    
    func testHandleOption_WhenOptionIsReturnPaymentP2P_ShouldCallPresenterMethod() {
        serviceMock.fetchItemDetailCardIdClosure = { _, completion in
            completion(.success(DataMock.ItemDetailMock.fullData))
        }
        
        sut.fetchItemDetail()
        
        let optionActionMock = OptionAction(type: .returnPaymentP2P)
        let expectedEvent = FeedAnalytics.feedDetailOptionInteraction(optionType: .returnPayment, cardId: cardId.uuidString).event()
        
        sut.handleOption(optionActionMock)
        
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(presenterSpy.refundPaymentConfirmationTypeCallsCount, 1)
    }
    
    func testHandleOption_WhenOptionIsReturnPaymentPix_ShouldCallPresenterMethod() {
        serviceMock.fetchItemDetailCardIdClosure = { _, completion in
            completion(.success(DataMock.ItemDetailMock.fullData))
        }
        
        sut.fetchItemDetail()
        
        let optionActionMock = OptionAction(type: .returnPaymentPix)
        let expectedEvent = FeedAnalytics.feedDetailOptionInteraction(optionType: .returnPayment, cardId: cardId.uuidString).event()
        
        sut.handleOption(optionActionMock)
        
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(presenterSpy.openPixReturnFlowCallsCount, 1)
    }
    
    func testHandleOption_WhenOptionIsViewReceiptP2P_ShouldCallPresenterMethod() {
        serviceMock.fetchItemDetailCardIdClosure = { _, completion in
            completion(.success(DataMock.ItemDetailMock.fullData))
        }
        
        sut.fetchItemDetail()
        
        let optionActionMock = OptionAction(type: .viewReceiptP2P)
        let expectedEvent = FeedAnalytics.feedDetailOptionInteraction(optionType: .viewReceipt, cardId: cardId.uuidString).event()
        
        sut.handleOption(optionActionMock)
        
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(presenterSpy.openReceiptTypeIdCallsCount, 1)
        XCTAssertEqual(presenterSpy.openReceiptTypeIdReceivedInvocations.first?.id, cardId.uuidString)
        XCTAssertEqual(presenterSpy.openReceiptTypeIdReceivedInvocations.first?.type.rawValue, OptionType.viewReceiptP2P.rawValue)
    }
    
    func testHandleOption_WhenOptionIsViewReceiptBiz_ShouldCallPresenterMethod() {
        serviceMock.fetchItemDetailCardIdClosure = { _, completion in
            completion(.success(DataMock.ItemDetailMock.fullData))
        }
        
        sut.fetchItemDetail()
        
        let optionActionMock = OptionAction(type: .viewReceiptBiz)
        let expectedEvent = FeedAnalytics.feedDetailOptionInteraction(optionType: .viewReceipt, cardId: cardId.uuidString).event()
        
        sut.handleOption(optionActionMock)
        
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(presenterSpy.openReceiptTypeIdCallsCount, 1)
        XCTAssertEqual(presenterSpy.openReceiptTypeIdReceivedInvocations.first?.id, cardId.uuidString)
        XCTAssertEqual(presenterSpy.openReceiptTypeIdReceivedInvocations.first?.type.rawValue, OptionType.viewReceiptBiz.rawValue)
    }
    
    func testHandleOption_WhenOptionIsViewReceiptStore_ShouldCallPresenterMethod() {
        serviceMock.fetchItemDetailCardIdClosure = { _, completion in
            completion(.success(DataMock.ItemDetailMock.fullData))
        }
        
        sut.fetchItemDetail()
        
        let optionActionMock = OptionAction(type: .viewReceiptStore)
        let expectedEvent = FeedAnalytics.feedDetailOptionInteraction(optionType: .viewReceipt, cardId: cardId.uuidString).event()
        
        sut.handleOption(optionActionMock)
        
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(presenterSpy.openReceiptTypeIdCallsCount, 1)
        XCTAssertEqual(presenterSpy.openReceiptTypeIdReceivedInvocations.first?.id, cardId.uuidString)
        XCTAssertEqual(presenterSpy.openReceiptTypeIdReceivedInvocations.first?.type.rawValue, OptionType.viewReceiptStore.rawValue)
    }
    
    func testHandleOption_WhenOptionIsViewReceiptPixRefund_ShouldCallPresenterMethod() {
        serviceMock.fetchItemDetailCardIdClosure = { _, completion in
            completion(.success(DataMock.ItemDetailMock.fullData))
        }
        
        sut.fetchItemDetail()
        
        let optionActionMock = OptionAction(type: .viewReceiptPixRefund)
        let expectedEvent = FeedAnalytics.feedDetailOptionInteraction(optionType: .viewReceipt, cardId: cardId.uuidString).event()
        
        sut.handleOption(optionActionMock)
        
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(presenterSpy.openReceiptTypeIdCallsCount, 1)
        XCTAssertEqual(presenterSpy.openReceiptTypeIdReceivedInvocations.first?.id, cardId.uuidString)
        XCTAssertEqual(presenterSpy.openReceiptTypeIdReceivedInvocations.first?.type.rawValue, OptionType.viewReceiptPixRefund.rawValue)
    }
    
    func testHandleOption_WhenOptionIsViewReceiptPixTransaction_ShouldCallPresenterMethod() {
        serviceMock.fetchItemDetailCardIdClosure = { _, completion in
            completion(.success(DataMock.ItemDetailMock.fullData))
        }
        
        sut.fetchItemDetail()
        
        let optionActionMock = OptionAction(type: .viewReceiptPixTransaction)
        let expectedEvent = FeedAnalytics.feedDetailOptionInteraction(optionType: .viewReceipt, cardId: cardId.uuidString).event()
        
        sut.handleOption(optionActionMock)
        
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(presenterSpy.openReceiptTypeIdCallsCount, 1)
        XCTAssertEqual(presenterSpy.openReceiptTypeIdReceivedInvocations.first?.id, cardId.uuidString)
        XCTAssertEqual(presenterSpy.openReceiptTypeIdReceivedInvocations.first?.type.rawValue, OptionType.viewReceiptPixTransaction.rawValue)
    }
    
    func testMakeCardPrivate_WhenSuccess_ShouldCallRefreshItemOnPresent() {
        serviceMock.fetchItemDetailCardIdClosure = { _, completion in
            completion(.success(DataMock.ItemDetailMock.fullData))
        }
        
        sut.fetchItemDetail()
        
        serviceMock.changeVisibilityCardIdIsPrivateClosure = { _, _, completion in
            completion(.success(NoContent()))
        }
        
        sut.handleConfirmation(with: .makePrivate)
        
        XCTAssertEqual(serviceMock.changeVisibilityCardIdIsPrivateCallsCount, 1)
        XCTAssertEqual(serviceMock.changeVisibilityCardIdIsPrivateReceivedInvocations.first?.cardId.uuidString, cardId.uuidString)
        XCTAssertEqual(serviceMock.changeVisibilityCardIdIsPrivateReceivedInvocations.first?.isPrivate, true)
        XCTAssertEqual(presenterSpy.configureItemDataSourceCallsCount, 2)
        XCTAssertEqual(presenterSpy.showOptionGenericErrorCallsCount, 0)
    }
    
    func testMakeCardPrivate_WhenFailure_ShouldCallGenericErrorOnPresent() {
        serviceMock.fetchItemDetailCardIdClosure = { _, completion in
            completion(.success(DataMock.ItemDetailMock.fullData))
        }
        
        sut.fetchItemDetail()
        
        serviceMock.changeVisibilityCardIdIsPrivateClosure = { _, _, completion in
            completion(.failure(.connectionFailure))
        }
        
        sut.handleConfirmation(with: .makePrivate)
        
        XCTAssertEqual(serviceMock.changeVisibilityCardIdIsPrivateCallsCount, 1)
        XCTAssertEqual(serviceMock.changeVisibilityCardIdIsPrivateReceivedInvocations.first?.cardId.uuidString, cardId.uuidString)
        XCTAssertEqual(serviceMock.changeVisibilityCardIdIsPrivateReceivedInvocations.first?.isPrivate, true)
        XCTAssertEqual(presenterSpy.configureItemDataSourceCallsCount, 1)
        XCTAssertEqual(presenterSpy.showOptionGenericErrorCallsCount, 1)
    }
    
    func testMakeCardHidden_WhenSuccess_ShouldCallRefreshItemOnPresent() {
        serviceMock.fetchItemDetailCardIdClosure = { _, completion in
            completion(.success(DataMock.ItemDetailMock.fullData))
        }
        
        sut.fetchItemDetail()
        
        serviceMock.changeVisibilityCardIdIsPrivateClosure = { _, _, completion in
            completion(.success(NoContent()))
        }
        
        sut.handleConfirmation(with: .hideTransaction)
        
        XCTAssertEqual(serviceMock.changeVisibilityCardIdIsPrivateCallsCount, 1)
        XCTAssertEqual(serviceMock.changeVisibilityCardIdIsPrivateReceivedInvocations.first?.cardId.uuidString, cardId.uuidString)
        XCTAssertEqual(serviceMock.changeVisibilityCardIdIsPrivateReceivedInvocations.first?.isPrivate, false)
        XCTAssertEqual(presenterSpy.showOptionGenericErrorCallsCount, 0)
    }
    
    func testMakeCardHidden_WhenFailure_ShouldCallGenericErrorOnPresent() {
        serviceMock.fetchItemDetailCardIdClosure = { _, completion in
            completion(.success(DataMock.ItemDetailMock.fullData))
        }
        
        sut.fetchItemDetail()
        
        serviceMock.changeVisibilityCardIdIsPrivateClosure = { _, _, completion in
            completion(.failure(.connectionFailure))
        }
        
        sut.handleConfirmation(with: .hideTransaction)
        
        XCTAssertEqual(serviceMock.changeVisibilityCardIdIsPrivateCallsCount, 1)
        XCTAssertEqual(serviceMock.changeVisibilityCardIdIsPrivateReceivedInvocations.first?.cardId.uuidString, cardId.uuidString)
        XCTAssertEqual(serviceMock.changeVisibilityCardIdIsPrivateReceivedInvocations.first?.isPrivate, false)
        XCTAssertEqual(presenterSpy.showOptionGenericErrorCallsCount, 1)
    }
    
    func testFollowCard_WhenSuccess_ShouldCallRefreshItemOnPresent() {
        serviceMock.fetchItemDetailCardIdClosure = { _, completion in
            completion(.success(DataMock.ItemDetailMock.fullData))
        }
        
        sut.fetchItemDetail()
        
        serviceMock.changeNotificationsCardIdIsFollowingClosure = { _, _, completion in
            completion(.success(NoContent()))
        }
        
        sut.handleOption(OptionAction(type: .enableNotification))
        
        XCTAssertEqual(serviceMock.changeNotificationsCardIdIsFollowingCallsCount, 1)
        XCTAssertEqual(serviceMock.changeNotificationsCardIdIsFollowingReceivedInvocations.first?.cardId.uuidString, cardId.uuidString)
        XCTAssertEqual(serviceMock.changeNotificationsCardIdIsFollowingReceivedInvocations.first?.isFollowing, true)
        XCTAssertEqual(presenterSpy.showDialogEnableNotificationCallsCount, 1)
        XCTAssertEqual(presenterSpy.showDialogEnableNotificationReceivedInvocations[0], true)
        XCTAssertEqual(presenterSpy.showOptionGenericErrorCallsCount, 0)
    }
    
    func testFollowCard_WhenFailure_ShouldCallGenericErrorOnPresent() {
        serviceMock.fetchItemDetailCardIdClosure = { _, completion in
            completion(.success(DataMock.ItemDetailMock.fullData))
        }
        
        sut.fetchItemDetail()
        
        serviceMock.changeNotificationsCardIdIsFollowingClosure = { _, _, completion in
            completion(.failure(.connectionFailure))
        }
        
        sut.handleOption(OptionAction(type: .enableNotification))
        
        XCTAssertEqual(serviceMock.changeNotificationsCardIdIsFollowingCallsCount, 1)
        XCTAssertEqual(serviceMock.changeNotificationsCardIdIsFollowingReceivedInvocations.first?.cardId.uuidString, cardId.uuidString)
        XCTAssertEqual(serviceMock.changeNotificationsCardIdIsFollowingReceivedInvocations.first?.isFollowing, true)
        XCTAssertEqual(presenterSpy.showDialogEnableNotificationCallsCount, 0)
        XCTAssertEqual(presenterSpy.showOptionGenericErrorCallsCount, 1)
    }
    
    func testUnfollowCard_WhenSuccess_ShouldCallRefreshItemOnPresent() {
        serviceMock.fetchItemDetailCardIdClosure = { _, completion in
            completion(.success(DataMock.ItemDetailMock.fullData))
        }
        
        sut.fetchItemDetail()
        
        serviceMock.changeNotificationsCardIdIsFollowingClosure = { _, _, completion in
            completion(.success(NoContent()))
        }
        
        sut.handleConfirmation(with: .disableNotification)
        
        XCTAssertEqual(serviceMock.changeNotificationsCardIdIsFollowingCallsCount, 1)
        XCTAssertEqual(serviceMock.changeNotificationsCardIdIsFollowingReceivedInvocations.first?.cardId.uuidString, cardId.uuidString)
        XCTAssertEqual(serviceMock.changeNotificationsCardIdIsFollowingReceivedInvocations.first?.isFollowing, false)
        XCTAssertEqual(presenterSpy.showDialogEnableNotificationCallsCount, 1)
        XCTAssertEqual(presenterSpy.showDialogEnableNotificationReceivedInvocations[0], false)
        XCTAssertEqual(presenterSpy.showOptionGenericErrorCallsCount, 0)
    }
    
    func testUnfollowCard_WhenFailure_ShouldCallGenericErrorOnPresent() {
        serviceMock.fetchItemDetailCardIdClosure = { _, completion in
            completion(.success(DataMock.ItemDetailMock.fullData))
        }
        
        sut.fetchItemDetail()
        
        serviceMock.changeNotificationsCardIdIsFollowingClosure = { _, _, completion in
            completion(.failure(.connectionFailure))
        }
        
        sut.handleConfirmation(with: .disableNotification)
        
        XCTAssertEqual(serviceMock.changeNotificationsCardIdIsFollowingCallsCount, 1)
        XCTAssertEqual(serviceMock.changeNotificationsCardIdIsFollowingReceivedInvocations.first?.cardId.uuidString, cardId.uuidString)
        XCTAssertEqual(serviceMock.changeNotificationsCardIdIsFollowingReceivedInvocations.first?.isFollowing, false)
        XCTAssertEqual(presenterSpy.showDialogEnableNotificationCallsCount, 0)
        XCTAssertEqual(presenterSpy.showOptionGenericErrorCallsCount, 1)
    }
    
    func testConfirmRefundPaymentRequiresAuthentication_WhenAuthAndRefundAreSuccess_ShouldCallRefreshItemOnPresent() {
        authenticationMock.authenticateClosure = { completion in
            completion(.success(String()))
        }
        
        serviceMock.fetchItemDetailCardIdClosure = { _, completion in
            completion(.success(DataMock.ItemDetailMock.fullData))
        }
        
        sut.fetchItemDetail()
        
        serviceMock.refundPaymentTransactionIdClosure = { _, completion in
            completion(.success(NoContent()))
        }
        
        sut.handleConfirmation(with: .returnPaymentP2P)
        
        XCTAssertEqual(serviceMock.refundPaymentTransactionIdCallsCount, 1)
        XCTAssertEqual(presenterSpy.startLoadingCallsCount, 2) // 2 por conta do fetchItemDetails
        XCTAssertEqual(presenterSpy.stopLoadingCallsCount, 2) // 2 por conta do fetchItemDetails
        XCTAssertEqual(presenterSpy.showRefundSuccessCallsCount, 1)
        XCTAssertEqual(presenterSpy.showOptionGenericErrorCallsCount, 0)
    }
    
    func testConfirmRefundPaymentRequiresAuthentication_WhenFailure_ShouldCallGenericErrorOnPresent() {
        authenticationMock.authenticateClosure = { completion in
            completion(.failure(.cancelled))
        }
        
        serviceMock.fetchItemDetailCardIdClosure = { _, completion in
            completion(.success(DataMock.ItemDetailMock.fullData))
        }
        
        sut.fetchItemDetail()
        sut.handleConfirmation(with: .returnPaymentP2P)

        XCTAssertEqual(authenticationMock.authenticateCallsCount, 1)
        XCTAssertEqual(presenterSpy.showOptionGenericErrorCallsCount, 1)
    }
    
    func testHandleConfirmation_WhenPassingAnInvalideType_ShouldNotCallGenericErrorOnPresent() {
        sut.handleConfirmation(with: .viewReceiptBiz)
        
        XCTAssertEqual(presenterSpy.showOptionGenericErrorCallsCount, 0)
    }
    
    func testConfirmRefundPaymentRequiresAuthentication_WhenAuthIsSuccessAndRefundFailure_ShouldCallRefreshItemOnPresent() {
        authenticationMock.authenticateClosure = { completion in
            completion(.success(String()))
        }
        
        serviceMock.fetchItemDetailCardIdClosure = { _, completion in
            completion(.success(DataMock.ItemDetailMock.fullData))
        }
        
        sut.fetchItemDetail()
        
        serviceMock.refundPaymentTransactionIdClosure = { _, completion in
            completion(.failure(.cancelled))
        }
        
        sut.handleConfirmation(with: .returnPaymentP2P)
        
        XCTAssertEqual(serviceMock.refundPaymentTransactionIdCallsCount, 1)
        XCTAssertEqual(presenterSpy.startLoadingCallsCount, 2) // 2 por conta do fetchItemDetails
        XCTAssertEqual(presenterSpy.stopLoadingCallsCount, 2) // 2 por conta do fetchItemDetails
        XCTAssertEqual(presenterSpy.showRefundSuccessCallsCount, 0)
        XCTAssertEqual(presenterSpy.showOptionGenericErrorCallsCount, 1)
    }
    
    func testUserOpennedScreen_ShouldSendAnalyticsEvent() {
        sut.userOpennedScreen()
        
        let expectedEvent = FeedAnalytics.screenViewed.event()
        
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }
    
    func testUserClosedScreen_ShouldSendAnalyticsEvent() {
        sut.userClosedScreen()
        
        let expectedEvent = FeedAnalytics.buttonTapped(type: .back).event()
        
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }

    func testLoadTooLongCommentError_ShouldCallPresenterMethod() {
        sut.loadTooLongCommentError()
        
        XCTAssertEqual(presenterSpy.showSnackbarErrorErrorTypeCallsCount, 1)
        XCTAssertEqual(presenterSpy.showSnackbarErrorErrorTypeReceivedInvocations.first, .tooLongComment)
    }
    
    func testAddNewComment_WhenIsSuccess_ShouldCallRefreshListWithNewItem() {
        serviceMock.commentCardCardIdCommentClosure = { _, _, completion in
            completion(.success(NoContent()))
        }
        
        sut.addNewComment("comment")
        
        XCTAssertEqual(serviceMock.commentCardCardIdCommentCallsCount, 1)
        XCTAssertEqual(presenterSpy.enableSendCommentButtonCallsCount, 2) // 1 chamada pra desabilitar e após o sucesso/erro habilitar o botão
        XCTAssertEqual(presenterSpy.refreshCommentListWithAtCallsCount, 1)
        XCTAssertEqual(presenterSpy.showSnackbarErrorErrorTypeCallsCount, 0)
    }
    
    func testAddNewComment_WhenIsFailure_ShouldCallShowSnackbarError() {
        serviceMock.commentCardCardIdCommentClosure = { _, _, completion in
            completion(.failure(.cancelled))
        }
        
        sut.addNewComment("comment")
        
        XCTAssertEqual(serviceMock.commentCardCardIdCommentCallsCount, 1)
        XCTAssertEqual(presenterSpy.enableSendCommentButtonCallsCount, 2) // 1 chamada pra desabilitar e após o sucesso/erro habilitar o botão
        XCTAssertEqual(presenterSpy.refreshCommentListWithAtCallsCount, 1)
        XCTAssertEqual(presenterSpy.showSnackbarErrorErrorTypeCallsCount, 1)
    }
    
    func testAddNewComment_WhenHasAnInvalidUsername_ShouldCallShowSnackbarError() {
        let consumerNil = FeedConsumerContractMock()
        consumerMock = consumerNil
        
        sut.addNewComment("comment")
        
        XCTAssertEqual(serviceMock.commentCardCardIdCommentCallsCount, 0)
        XCTAssertEqual(presenterSpy.enableSendCommentButtonCallsCount, 0)
        XCTAssertEqual(presenterSpy.refreshCommentListWithAtCallsCount, 0)
        XCTAssertEqual(presenterSpy.showSnackbarErrorErrorTypeCallsCount, 1)
    }
    
    func testRetrySendComment_WhenIsSuccess_ShouldCallRefreshListWithNewItem() {
        let commentMock = DataMock.CommentMock.comment
        
        serviceMock.commentCardCardIdCommentClosure = { _, _, completion in
            completion(.success(NoContent()))
        }
        
        sut.retrySendComment(commentMock, index: 0)
        
        XCTAssertEqual(serviceMock.commentCardCardIdCommentCallsCount, 1)
        XCTAssertEqual(presenterSpy.refreshCommentListWithAtCallsCount, 2)
        XCTAssertEqual(presenterSpy.showSnackbarErrorErrorTypeCallsCount, 0)
    }
    
    func testRetrySendComment_WhenIsFailure_ShouldCallShowSnackbarError() {
        let commentMock = DataMock.CommentMock.comment
        
        serviceMock.commentCardCardIdCommentClosure = { _, _, completion in
            completion(.failure(.cancelled))
        }
        
        sut.retrySendComment(commentMock, index: 0)
        
        XCTAssertEqual(serviceMock.commentCardCardIdCommentCallsCount, 1)
        XCTAssertEqual(presenterSpy.refreshCommentListWithAtCallsCount, 2)
        XCTAssertEqual(presenterSpy.showSnackbarErrorErrorTypeCallsCount, 1)
    }
}
