import XCTest
@testable import Feed

private class HeaderItemDetailPresentingSpy: HeaderItemDetailPresenting {
    var view: HeaderItemDetailDisplaying?

    // MARK: - ConfigureActionLogView
    private(set) var configureActionLogViewCallsCount = 0
    private(set) var configureActionLogViewReceivedInvocations: [ActionLogView] = []

    func configureActionLogView(_ actionLogView: ActionLogView) {
        configureActionLogViewCallsCount += 1
        configureActionLogViewReceivedInvocations.append(actionLogView)
    }

    // MARK: - ConfigureUserTestimonialView
    private(set) var configureUserTestimonialViewCallsCount = 0
    private(set) var configureUserTestimonialViewReceivedInvocations: [UserTestimonialView] = []

    func configureUserTestimonialView(_ userTestimonialView: UserTestimonialView) {
        configureUserTestimonialViewCallsCount += 1
        configureUserTestimonialViewReceivedInvocations.append(userTestimonialView)
    }

    // MARK: - ConfigurePostDetailView
    private(set) var configurePostDetailViewCallsCount = 0
    private(set) var configurePostDetailViewReceivedInvocations: [PostDetailView] = []

    func configurePostDetailView(_ postDetailView: PostDetailView) {
        configurePostDetailViewCallsCount += 1
        configurePostDetailViewReceivedInvocations.append(postDetailView)
    }

    // MARK: - ConfigureSocialActionOpenedView
    private(set) var configureSocialActionOpenedViewCallsCount = 0
    private(set) var configureSocialActionOpenedViewReceivedInvocations: [SocialActionOpenedView] = []

    func configureSocialActionOpenedView(_ socialActionOpenedView: SocialActionOpenedView) {
        configureSocialActionOpenedViewCallsCount += 1
        configureSocialActionOpenedViewReceivedInvocations.append(socialActionOpenedView)
    }

    // MARK: - CleanAllViews
    private(set) var cleanAllViewsCallsCount = 0

    func cleanAllViews() {
        cleanAllViewsCallsCount += 1
    }

    // MARK: - ConfigureAcessibility
    private(set) var configureAcessibilityItemDetailCallsCount = 0
    private(set) var configureAcessibilityItemDetailReceivedInvocations: [ItemDetail] = []

    func configureAcessibility(itemDetail: ItemDetail) {
        configureAcessibilityItemDetailCallsCount += 1
        configureAcessibilityItemDetailReceivedInvocations.append(itemDetail)
    }
}

private class HeaderItemDetailDelegateSpy: HeaderItemDetailDelegate {
    // MARK: - DidTouchPrimaryImage
    private(set) var didTouchPrimaryImageForCallsCount = 0
    private(set) var didTouchPrimaryImageForReceivedInvocations: [URL] = []

    func didTouchPrimaryImage(for deeplinkUrl: URL) {
        didTouchPrimaryImageForCallsCount += 1
        didTouchPrimaryImageForReceivedInvocations.append(deeplinkUrl)
    }

    // MARK: - DidTouchSecondaryImage
    private(set) var didTouchSecondaryImageForCallsCount = 0
    private(set) var didTouchSecondaryImageForReceivedInvocations: [URL] = []

    func didTouchSecondaryImage(for deeplinkUrl: URL) {
        didTouchSecondaryImageForCallsCount += 1
        didTouchSecondaryImageForReceivedInvocations.append(deeplinkUrl)
    }

    // MARK: - DidSelectText
    private(set) var didSelectTextDeeplinkUrlCallsCount = 0
    private(set) var didSelectTextDeeplinkUrlReceivedInvocations: [(text: String, deeplinkUrl: URL)] = []

    func didSelectText(_ text: String, deeplinkUrl: URL) {
        didSelectTextDeeplinkUrlCallsCount += 1
        didSelectTextDeeplinkUrlReceivedInvocations.append((text: text, deeplinkUrl: deeplinkUrl))
    }

    // MARK: - DidTouchLiked
    private(set) var didTouchLikedNumberOfLikesLatestLikesIsLikeUiSocialActionOpenedUpdateCallsCount = 0
    private(set) var didTouchLikedNumberOfLikesLatestLikesIsLikeUiSocialActionOpenedUpdateReceivedInvocations: [(numberOfLikes: Int, latestLikes: [String], isLike: Bool, uiSocialActionOpenedUpdate: UISocialActionOpenedUpdating)] = []

    func didTouchLiked(numberOfLikes: Int, latestLikes: [String], isLike: Bool, uiSocialActionOpenedUpdate: UISocialActionOpenedUpdating) {
        didTouchLikedNumberOfLikesLatestLikesIsLikeUiSocialActionOpenedUpdateCallsCount += 1
        didTouchLikedNumberOfLikesLatestLikesIsLikeUiSocialActionOpenedUpdateReceivedInvocations.append((numberOfLikes: numberOfLikes, latestLikes: latestLikes, isLike: isLike, uiSocialActionOpenedUpdate: uiSocialActionOpenedUpdate))
    }
}

final class HeaderItemDetailInteractorTests: XCTestCase {
    private let presenterSpy = HeaderItemDetailPresentingSpy()
    private let delegateSpy = HeaderItemDetailDelegateSpy()
    
    private lazy var sut = HeaderItemDetailInteractor(
        itemDetail: DataMock.ItemDetailMock.fullData,
        presenter: presenterSpy,
        delegate: delegateSpy
    )
    
    func testLoadData_WhenHasItemDetail_ShouldCallConfigureItemDetailOnPresent() {
        sut.loadData()

        XCTAssertEqual(presenterSpy.cleanAllViewsCallsCount, 1)
        XCTAssertEqual(presenterSpy.configureActionLogViewCallsCount, 1)
        XCTAssertEqual(presenterSpy.configureUserTestimonialViewCallsCount, 1)
        XCTAssertEqual(presenterSpy.configurePostDetailViewCallsCount, 1)
        XCTAssertEqual(presenterSpy.configureSocialActionOpenedViewCallsCount, 1)
    }

    func testLoadData_WhenDoesHaveNotItemDetail_ShouldCallConfigureItemDetailOnPresent() {
        sut = HeaderItemDetailInteractor(
            itemDetail: DataMock.ItemDetailMock.empty,
            presenter: presenterSpy,
            delegate: delegateSpy
        )
        
        sut.loadData()

        XCTAssertEqual(presenterSpy.cleanAllViewsCallsCount, 1)
        XCTAssertEqual(presenterSpy.configureActionLogViewCallsCount, 0)
        XCTAssertEqual(presenterSpy.configureUserTestimonialViewCallsCount, 0)
        XCTAssertEqual(presenterSpy.configurePostDetailViewCallsCount, 0)
        XCTAssertEqual(presenterSpy.configureSocialActionOpenedViewCallsCount, 0)
    }
}
