import XCTest
@testable import Feed

private class HeaderItemDetailDisplayingSpy: HeaderItemDetailDisplaying {

    // MARK: - ConfigureTopView
    private(set) var configureTopViewCallsCount = 0
    private(set) var configureTopViewReceivedInvocations: [UIView] = []

    func configureTopView(_ view: UIView) {
        configureTopViewCallsCount += 1
        configureTopViewReceivedInvocations.append(view)
    }

    // MARK: - ConfigureBottomView
    private(set) var configureBottomViewCallsCount = 0
    private(set) var configureBottomViewReceivedInvocations: [UIView] = []

    func configureBottomView(_ view: UIView) {
        configureBottomViewCallsCount += 1
        configureBottomViewReceivedInvocations.append(view)
    }

    // MARK: - CleanDetailView
    private(set) var cleanDetailViewCallsCount = 0

    func cleanDetailView() {
        cleanDetailViewCallsCount += 1
    }

    // MARK: - ConfigureAcessibility
    private(set) var configureAcessibilityAcessibilityLabelCallsCount = 0
    private(set) var configureAcessibilityAcessibilityLabelReceivedInvocations: [String] = []

    func configureAcessibility(acessibilityLabel: String) {
        configureAcessibilityAcessibilityLabelCallsCount += 1
        configureAcessibilityAcessibilityLabelReceivedInvocations.append(acessibilityLabel)
    }
}

final class HeaderItemDetailPresenterTests: XCTestCase {
    private let containerMock = DependencyContainerMock()
    private let viewSpy = HeaderItemDetailDisplayingSpy()
    
    private lazy var sut: HeaderItemDetailPresenting = {
        let presenter = HeaderItemDetailPresenter(dependencies: containerMock)
        presenter.view = viewSpy
        return presenter
    }()
    
    func testConfigureActionLog_ShouldCallConfigureView() {
        let presenter = ActionLogPresenter()
        
        let interactor = ActionLogInteractor(
            item: DataMock.ActionLogMock.withActionExistsInImage,
            presenter: presenter,
            hasAction: true,
            delegate: nil
        )
        
        let actionLogView = ActionLogView(interactor: interactor)
        
        sut.configureActionLogView(actionLogView)
        
        XCTAssertEqual(viewSpy.configureTopViewCallsCount, 1)
        XCTAssertTrue(viewSpy.configureTopViewReceivedInvocations.first is ActionLogView)
    }

    func testConfigureUserTestimonial_ShouldCallConfigureView() {
        let presenter = UserTestimonialPresenter()
        
        let interactor = UserTestimonialInteractor(
            item: DataMock.UserTestimonialMock.fullData,
            presenter: presenter,
            hasAction: true
        )
        
        let userTestimonialView = UserTestimonialView(interactor: interactor)
        
        sut.configureUserTestimonialView(userTestimonialView)

        XCTAssertEqual(viewSpy.configureTopViewCallsCount, 1)
        XCTAssertTrue(viewSpy.configureTopViewReceivedInvocations.first is UserTestimonialView)
    }

    func testConfigurePostDetail_ShouldCallConfigureView() {
        let presenter = PostDetailPresenter(dependencies: containerMock)
        
        let interactor = PostDetailInteractor(
            item: DataMock.PostDetailMock.withCurrencyIsDebit,
            presenter: presenter,
            hasAction: true
        )
        
        let postDetailView = PostDetailView(interactor: interactor)
        
        sut.configurePostDetailView(postDetailView)

        XCTAssertEqual(viewSpy.configureTopViewCallsCount, 1)
        XCTAssertTrue(viewSpy.configureTopViewReceivedInvocations.first is PostDetailView)
    }

    func testConfigureSocialAction_ShouldCallConfigureView() {
        let presenter = SocialActionOpenedPresenter()
        
        let interactor = SocialActionOpenedInteractor(
            item: DataMock.LikeMock.noLikes,
            presenter: presenter,
            delegate: nil
        )
        
        let socialActionOpenedView = SocialActionOpenedView(interactor: interactor)
        
        sut.configureSocialActionOpenedView(socialActionOpenedView)

        XCTAssertEqual(viewSpy.configureBottomViewCallsCount, 1)
        XCTAssertTrue(viewSpy.configureBottomViewReceivedInvocations.first is SocialActionOpenedView)
    }
    
    func testCleanAllViews_ShouldCallCleanDetailView() {
        sut.cleanAllViews()
        
        XCTAssertEqual(viewSpy.cleanDetailViewCallsCount, 1)
    }
}
