import XCTest
@testable import Feed

private final class CommentPresentingSpy: CommentPresenting {
    var view: CommentDisplaying?

    // MARK: - ConfigureComment
    private(set) var configureCommentPositionStateCallsCount = 0
    private(set) var configureCommentPositionStateReceivedInvocations: [(position: Int, comment: Comment, state: CommentState)] = []

    func configureComment(position: Int, _ comment: Comment, state: CommentState) {
        configureCommentPositionStateCallsCount += 1
        configureCommentPositionStateReceivedInvocations.append((position: position, comment: comment, state: state))
    }
    
    // MARK: - ConfigureRetryStart
    private(set) var configureRetryStartCallsCount = 0

    func configureRetryStart() {
        configureRetryStartCallsCount += 1
    }
}
private final class CommentInteractingDelegateSpy: CommentInteractingDelegate {

    // MARK: - RetrySendComment
    private(set) var retrySendCommentAtCallsCount = 0
    private(set) var retrySendCommentAtReceivedInvocations: [(comment: Comment, index: Int)] = []

    func retrySendComment(_ comment: Comment, at index: Int) {
        retrySendCommentAtCallsCount += 1
        retrySendCommentAtReceivedInvocations.append((comment: comment, index: index))
    }
}

final class CommentInteractorTests: XCTestCase {
    private let presenterSpy = CommentPresentingSpy()
    private let commentMock = DataMock.CommentMock.comment
    private let delegateSpy = CommentInteractingDelegateSpy()
    
    private lazy var sut: CommentInteractor = {
        let interactor = CommentInteractor(presenter: presenterSpy, comment: commentMock, index: 0)
        interactor.delegate = delegateSpy
        return interactor
    }()
    
    func testLoadData_ShouldCallPresenterMethodToConfigureView() {
        sut.loadData()
        
        XCTAssertEqual(presenterSpy.configureCommentPositionStateCallsCount, 1)
        XCTAssertEqual(presenterSpy.configureCommentPositionStateReceivedInvocations[0].comment, commentMock)
    }
    
    func testRetrySendComment_ShouldCallDelegateMethod() {
        sut.retrySendComment()
        
        XCTAssertEqual(delegateSpy.retrySendCommentAtCallsCount, 1)
        XCTAssertEqual(delegateSpy.retrySendCommentAtReceivedInvocations.first?.comment, commentMock)
    }
}
