import AssetsKit
import UI
import XCTest
@testable import Feed

private final class CommentDisplayingSpy: CommentDisplaying {

    // MARK: - ConfigureName
    private(set) var configureNameWithCallsCount = 0
    private(set) var configureNameWithReceivedInvocations: [String] = []

    func configureName(with name: String) {
        configureNameWithCallsCount += 1
        configureNameWithReceivedInvocations.append(name)
    }

    // MARK: - ConfigureComment
    private(set) var configureCommentWithCallsCount = 0
    private(set) var configureCommentWithReceivedInvocations: [String] = []

    func configureComment(with text: String) {
        configureCommentWithCallsCount += 1
        configureCommentWithReceivedInvocations.append(text)
    }

    // MARK: - ConfigureDate
    private(set) var configureDateWithCallsCount = 0
    private(set) var configureDateWithReceivedInvocations: [String] = []

    func configureDate(with date: String) {
        configureDateWithCallsCount += 1
        configureDateWithReceivedInvocations.append(date)
    }

    // MARK: - ConfigureStateComment
    private(set) var configureStateCommentWithCallsCount = 0
    private(set) var configureStateCommentWithReceivedInvocations: [String] = []

    func configureStateComment(with text: String) {
        configureStateCommentWithCallsCount += 1
        configureStateCommentWithReceivedInvocations.append(text)
    }

    // MARK: - ConfigureErrorState
    private(set) var configureErrorStateCallsCount = 0

    func configureErrorState() {
        configureErrorStateCallsCount += 1
    }

    // MARK: - ConfigureImage<S>
    private(set) var configureImageWithAndAndCallsCount = 0
    private(set) var configureImageWithAndAndReceivedInvocations: [(style: Any, imageUrl: URL, imagePlaceholder: UIImage?)] = []

    func configureImage<S>(with style: S, and imageUrl: URL, and imagePlaceholder: UIImage?) where S: ImageStyle {
        configureImageWithAndAndCallsCount += 1
        configureImageWithAndAndReceivedInvocations.append((style: style, imageUrl: imageUrl, imagePlaceholder: imagePlaceholder))
    }
    
    // MARK: - ConfigureAcessibility
    private(set) var configureAcessibilityLabelCallsCount = 0
    private(set) var configureAcessibilityLabelReceivedInvocations: [String] = []

    func configureAcessibility(label: String) {
        configureAcessibilityLabelCallsCount += 1
        configureAcessibilityLabelReceivedInvocations.append(label)
    }

    // MARK: - ClearErrorState
    private(set) var clearErrorStateCallsCount = 0

    func clearErrorState() {
        clearErrorStateCallsCount += 1
    }
}

final class CommentPresenterTests: XCTestCase {
    private var dateMock = Date(timeIntervalSince1970: 1600974889)
    private lazy var dependencyContainerMock = DependencyContainerMock(dateMock, TimeZone(identifier: "America/Sao_Paulo")!)
    
    private let viewSpy = CommentDisplayingSpy()
    
    private lazy var sut: CommentPresenting = {
        let presenter = CommentPresenter(dependencies: dependencyContainerMock)
        presenter.view = viewSpy
        return presenter
    }()
    
    func testConfigureComment_ShouldConfigureLessOneMinute() {
        sut.configureComment(position: 0, DataMock.CommentMock.commentWithLessOneMinute, state: .published)
        
        XCTAssertEqual(viewSpy.configureNameWithCallsCount, 1)
        XCTAssertEqual(viewSpy.configureNameWithReceivedInvocations.first, DataMock.CommentMock.commentWithLessOneMinute.username)
        XCTAssertEqual(viewSpy.configureCommentWithCallsCount, 1)
        XCTAssertEqual(viewSpy.configureCommentWithReceivedInvocations.first, DataMock.CommentMock.commentWithLessOneMinute.text)
        XCTAssertEqual(viewSpy.configureImageWithAndAndCallsCount, 1)
        XCTAssertEqual(viewSpy.configureImageWithAndAndReceivedInvocations.first?.imageUrl, DataMock.CommentMock.commentWithLessOneMinute.image.imageURL)
        XCTAssertEqual(viewSpy.configureImageWithAndAndReceivedInvocations.first?.imagePlaceholder, Resources.Placeholders.greenAvatarPlaceholder.image)
        XCTAssertEqual(viewSpy.configureDateWithCallsCount, 1)
        XCTAssertEqual(viewSpy.configureDateWithReceivedInvocations.first, "Agora há pouco")
        XCTAssertEqual(viewSpy.configureStateCommentWithCallsCount, 0)
        XCTAssertEqual(viewSpy.configureErrorStateCallsCount, 0)
    }
    
    func testConfigureComment_ShouldConfigureOneMinuteAgo() {
        sut.configureComment(position: 0, DataMock.CommentMock.commentWithOneMinuteAgo, state: .published)
        
        XCTAssertEqual(viewSpy.configureNameWithCallsCount, 1)
        XCTAssertEqual(viewSpy.configureNameWithReceivedInvocations.first, DataMock.CommentMock.commentWithOneMinuteAgo.username)
        XCTAssertEqual(viewSpy.configureCommentWithCallsCount, 1)
        XCTAssertEqual(viewSpy.configureCommentWithReceivedInvocations.first, DataMock.CommentMock.commentWithOneMinuteAgo.text)
        XCTAssertEqual(viewSpy.configureImageWithAndAndCallsCount, 1)
        XCTAssertEqual(viewSpy.configureImageWithAndAndReceivedInvocations.first?.imageUrl, DataMock.CommentMock.commentWithOneMinuteAgo.image.imageURL)
        XCTAssertEqual(viewSpy.configureImageWithAndAndReceivedInvocations.first?.imagePlaceholder, Resources.Placeholders.greenAvatarPlaceholder.image)
        XCTAssertEqual(viewSpy.configureDateWithCallsCount, 1)
        XCTAssertEqual(viewSpy.configureDateWithReceivedInvocations.first, "Há 1 minuto")
        XCTAssertEqual(viewSpy.configureStateCommentWithCallsCount, 0)
        XCTAssertEqual(viewSpy.configureErrorStateCallsCount, 0)
    }
    
    func testConfigureComment_ShouldConfigureThirtyMinuteAgo() {
        sut.configureComment(position: 0, DataMock.CommentMock.commentWithThirtyMinuteAgo, state: .published)
        
        XCTAssertEqual(viewSpy.configureNameWithCallsCount, 1)
        XCTAssertEqual(viewSpy.configureNameWithReceivedInvocations.first, DataMock.CommentMock.commentWithThirtyMinuteAgo.username)
        XCTAssertEqual(viewSpy.configureCommentWithCallsCount, 1)
        XCTAssertEqual(viewSpy.configureCommentWithReceivedInvocations.first, DataMock.CommentMock.commentWithThirtyMinuteAgo.text)
        XCTAssertEqual(viewSpy.configureImageWithAndAndCallsCount, 1)
        XCTAssertEqual(viewSpy.configureImageWithAndAndReceivedInvocations.first?.imageUrl, DataMock.CommentMock.commentWithThirtyMinuteAgo.image.imageURL)
        XCTAssertEqual(viewSpy.configureImageWithAndAndReceivedInvocations.first?.imagePlaceholder, Resources.Placeholders.sellerAvatarPlaceholder.image)
        XCTAssertEqual(viewSpy.configureDateWithCallsCount, 1)
        XCTAssertEqual(viewSpy.configureDateWithReceivedInvocations.first, "Há 30 minutos")
        XCTAssertEqual(viewSpy.configureStateCommentWithCallsCount, 0)
        XCTAssertEqual(viewSpy.configureErrorStateCallsCount, 0)
    }
    
    func testConfigureComment_ShouldConfigureOneHourAgo() {
        sut.configureComment(position: 0, DataMock.CommentMock.commentWithOneHourAgo, state: .published)
        
        XCTAssertEqual(viewSpy.configureNameWithCallsCount, 1)
        XCTAssertEqual(viewSpy.configureNameWithReceivedInvocations.first, DataMock.CommentMock.commentWithOneHourAgo.username)
        XCTAssertEqual(viewSpy.configureCommentWithCallsCount, 1)
        XCTAssertEqual(viewSpy.configureCommentWithReceivedInvocations.first, DataMock.CommentMock.commentWithOneHourAgo.text)
        XCTAssertEqual(viewSpy.configureImageWithAndAndCallsCount, 1)
        XCTAssertEqual(viewSpy.configureImageWithAndAndReceivedInvocations.first?.imageUrl, DataMock.CommentMock.commentWithOneHourAgo.image.imageURL)
        XCTAssertEqual(viewSpy.configureImageWithAndAndReceivedInvocations.first?.imagePlaceholder, Resources.Placeholders.actionAvatarPlaceholder.image)
        XCTAssertEqual(viewSpy.configureDateWithCallsCount, 1)
        XCTAssertEqual(viewSpy.configureDateWithReceivedInvocations.first, "Há 1 hora")
        XCTAssertEqual(viewSpy.configureStateCommentWithCallsCount, 0)
        XCTAssertEqual(viewSpy.configureErrorStateCallsCount, 0)
    }
    
    func testConfigureComment_ShouldConfigureTwentyThreeAgo() {
        sut.configureComment(position: 0, DataMock.CommentMock.commentWithTwentyThreeAgo, state: .published)
        
        XCTAssertEqual(viewSpy.configureNameWithCallsCount, 1)
        XCTAssertEqual(viewSpy.configureNameWithReceivedInvocations.first, DataMock.CommentMock.commentWithTwentyThreeAgo.username)
        XCTAssertEqual(viewSpy.configureCommentWithCallsCount, 1)
        XCTAssertEqual(viewSpy.configureCommentWithReceivedInvocations.first, DataMock.CommentMock.commentWithTwentyThreeAgo.text)
        XCTAssertEqual(viewSpy.configureImageWithAndAndCallsCount, 1)
        XCTAssertEqual(viewSpy.configureImageWithAndAndReceivedInvocations.first?.imageUrl, DataMock.CommentMock.commentWithTwentyThreeAgo.image.imageURL)
        XCTAssertEqual(viewSpy.configureImageWithAndAndReceivedInvocations.first?.imagePlaceholder, Resources.Placeholders.greenAvatarPlaceholder.image)
        XCTAssertEqual(viewSpy.configureDateWithCallsCount, 1)
        XCTAssertEqual(viewSpy.configureDateWithReceivedInvocations.first, "Há 23 horas")
        XCTAssertEqual(viewSpy.configureStateCommentWithCallsCount, 0)
        XCTAssertEqual(viewSpy.configureErrorStateCallsCount, 0)
    }
    
    func testConfigureComment_ShouldConfigureFromYesterday() {
        sut.configureComment(position: 0, DataMock.CommentMock.commentWithYesterday, state: .published)
        
        XCTAssertEqual(viewSpy.configureNameWithCallsCount, 1)
        XCTAssertEqual(viewSpy.configureNameWithReceivedInvocations.first, DataMock.CommentMock.commentWithYesterday.username)
        XCTAssertEqual(viewSpy.configureCommentWithCallsCount, 1)
        XCTAssertEqual(viewSpy.configureCommentWithReceivedInvocations.first, DataMock.CommentMock.commentWithYesterday.text)
        XCTAssertEqual(viewSpy.configureImageWithAndAndCallsCount, 1)
        XCTAssertEqual(viewSpy.configureImageWithAndAndReceivedInvocations.first?.imageUrl, DataMock.CommentMock.commentWithYesterday.image.imageURL)
        XCTAssertEqual(viewSpy.configureImageWithAndAndReceivedInvocations.first?.imagePlaceholder, Resources.Placeholders.greenAvatarPlaceholder.image)
        XCTAssertEqual(viewSpy.configureDateWithCallsCount, 1)
        XCTAssertEqual(viewSpy.configureDateWithReceivedInvocations.first, "Ontem às 16:14")
        XCTAssertEqual(viewSpy.configureStateCommentWithCallsCount, 0)
        XCTAssertEqual(viewSpy.configureErrorStateCallsCount, 0)
    }
    
    func testConfigureComment_ShouldConfigureFromOtherDays() {
        sut.configureComment(position: 0, DataMock.CommentMock.commentWithOtherDays, state: .published)
        
        XCTAssertEqual(viewSpy.configureNameWithCallsCount, 1)
        XCTAssertEqual(viewSpy.configureNameWithReceivedInvocations.first, DataMock.CommentMock.commentWithOtherDays.username)
        XCTAssertEqual(viewSpy.configureCommentWithCallsCount, 1)
        XCTAssertEqual(viewSpy.configureCommentWithReceivedInvocations.first, DataMock.CommentMock.commentWithOtherDays.text)
        XCTAssertEqual(viewSpy.configureImageWithAndAndCallsCount, 1)
        XCTAssertEqual(viewSpy.configureImageWithAndAndReceivedInvocations.first?.imageUrl, DataMock.CommentMock.commentWithOtherDays.image.imageURL)
        XCTAssertEqual(viewSpy.configureImageWithAndAndReceivedInvocations.first?.imagePlaceholder, Resources.Placeholders.greenAvatarPlaceholder.image)
        XCTAssertEqual(viewSpy.configureDateWithCallsCount, 1)
        XCTAssertEqual(viewSpy.configureDateWithReceivedInvocations.first, "21 set. às 16:14")
        XCTAssertEqual(viewSpy.configureStateCommentWithCallsCount, 0)
        XCTAssertEqual(viewSpy.configureErrorStateCallsCount, 0)
    }
    
    func testConfigureComment_WhenIsPublishing_ShouldConfigureStateComment() {
        sut.configureComment(position: 0, DataMock.CommentMock.commentWithLessOneMinute, state: .publishing)
        
        XCTAssertEqual(viewSpy.configureNameWithCallsCount, 1)
        XCTAssertEqual(viewSpy.configureNameWithReceivedInvocations.first, DataMock.CommentMock.commentWithLessOneMinute.username)
        XCTAssertEqual(viewSpy.configureCommentWithCallsCount, 1)
        XCTAssertEqual(viewSpy.configureCommentWithReceivedInvocations.first, DataMock.CommentMock.commentWithLessOneMinute.text)
        XCTAssertEqual(viewSpy.configureImageWithAndAndCallsCount, 1)
        XCTAssertEqual(viewSpy.configureImageWithAndAndReceivedInvocations.first?.imageUrl, DataMock.CommentMock.commentWithLessOneMinute.image.imageURL)
        XCTAssertEqual(viewSpy.configureImageWithAndAndReceivedInvocations.first?.imagePlaceholder, Resources.Placeholders.greenAvatarPlaceholder.image)
        XCTAssertEqual(viewSpy.configureDateWithCallsCount, 0)
        XCTAssertEqual(viewSpy.configureStateCommentWithCallsCount, 1)
        XCTAssertEqual(viewSpy.configureErrorStateCallsCount, 0)
    }
    
    func testConfigureComment_WhenIsError_ShouldConfigureErrorState() {
        sut.configureComment(position: 0, DataMock.CommentMock.commentWithLessOneMinute, state: .error)
        
        XCTAssertEqual(viewSpy.configureNameWithCallsCount, 1)
        XCTAssertEqual(viewSpy.configureNameWithReceivedInvocations.first, DataMock.CommentMock.commentWithLessOneMinute.username)
        XCTAssertEqual(viewSpy.configureCommentWithCallsCount, 1)
        XCTAssertEqual(viewSpy.configureCommentWithReceivedInvocations.first, DataMock.CommentMock.commentWithLessOneMinute.text)
        XCTAssertEqual(viewSpy.configureImageWithAndAndCallsCount, 1)
        XCTAssertEqual(viewSpy.configureImageWithAndAndReceivedInvocations.first?.imageUrl, DataMock.CommentMock.commentWithLessOneMinute.image.imageURL)
        XCTAssertEqual(viewSpy.configureImageWithAndAndReceivedInvocations.first?.imagePlaceholder, Resources.Placeholders.greenAvatarPlaceholder.image)
        XCTAssertEqual(viewSpy.configureDateWithCallsCount, 0)
        XCTAssertEqual(viewSpy.configureStateCommentWithCallsCount, 0)
        XCTAssertEqual(viewSpy.configureErrorStateCallsCount, 1)
    }
}
