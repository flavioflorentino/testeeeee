import Core
import XCTest
@testable import Feed

private enum LikesMock {
    static let toLike = Like(liked: false, latestLikes: [], total: 0)
    static let liked = Like(liked: true, latestLikes: [], total: 1)
}

private final class SocialActionOpenedPresenterSpy: SocialActionOpenedPresenting {
    var view: SocialActionOpenedDisplaying?

    //MARK: - configureItem
    private(set) var configureItemCallsCount = 0
    private(set) var itemLike: Like?

    func configureItem(_ item: Like) {
        configureItemCallsCount += 1
        itemLike = item
    }

    //MARK: - refreshItem
    private(set) var refreshItemCallsCount = 0

    func refreshItem() {
        refreshItemCallsCount += 1
    }
}

private class SocialActionOpenedDelegateSpy: SocialActionOpenedDelegate {
    // MARK: - DidTouchLiked
    private(set) var didTouchLikedNumberOfLikesLatestLikesIsLikeUiSocialActionOpenedUpdateCallsCount = 0
    private(set) var didTouchLikedNumberOfLikesLatestLikesIsLikeUiSocialActionOpenedUpdateReceivedInvocations: [(numberOfLikes: Int, latestLikes: [String], isLike: Bool, uiSocialActionOpenedUpdate: UISocialActionOpenedUpdating)] = []

    func didTouchLiked(numberOfLikes: Int, latestLikes: [String], isLike: Bool, uiSocialActionOpenedUpdate: UISocialActionOpenedUpdating) {
        didTouchLikedNumberOfLikesLatestLikesIsLikeUiSocialActionOpenedUpdateCallsCount += 1
        didTouchLikedNumberOfLikesLatestLikesIsLikeUiSocialActionOpenedUpdateReceivedInvocations.append((numberOfLikes: numberOfLikes, latestLikes: latestLikes, isLike: isLike, uiSocialActionOpenedUpdate: uiSocialActionOpenedUpdate))
    }
}

final class SocialActionOpenedInteractorTests: XCTestCase {
    private let presenterSpy = SocialActionOpenedPresenterSpy()
    private let delegateSpy = SocialActionOpenedDelegateSpy()
    
    func testLoadData_ShouldCallPresenterMethod() {
        let sut = SocialActionOpenedInteractor(
            item: LikesMock.toLike,
            presenter: presenterSpy,
            delegate: delegateSpy
        )
        
        sut.loadData()
        
        XCTAssertEqual(presenterSpy.configureItemCallsCount, 1)
        XCTAssertEqual(presenterSpy.itemLike?.liked, LikesMock.toLike.liked)
        XCTAssertEqual(presenterSpy.itemLike?.latestLikes.count, LikesMock.toLike.latestLikes.count)
        XCTAssertEqual(presenterSpy.itemLike?.total, LikesMock.toLike.total)
    }
    
    func testLikeAction_ShouldCallDidTouchLikedDelegate() {
        let sut = SocialActionOpenedInteractor(
            item: LikesMock.toLike,
            presenter: presenterSpy,
            delegate: delegateSpy
        )
        
        sut.likeAction()
        
        XCTAssertEqual(delegateSpy.didTouchLikedNumberOfLikesLatestLikesIsLikeUiSocialActionOpenedUpdateCallsCount, 1)
        XCTAssertEqual(delegateSpy.didTouchLikedNumberOfLikesLatestLikesIsLikeUiSocialActionOpenedUpdateReceivedInvocations.first?.numberOfLikes, LikesMock.toLike.total)
        XCTAssertEqual(delegateSpy.didTouchLikedNumberOfLikesLatestLikesIsLikeUiSocialActionOpenedUpdateReceivedInvocations.first?.latestLikes, LikesMock.toLike.latestLikes)
        XCTAssertEqual(delegateSpy.didTouchLikedNumberOfLikesLatestLikesIsLikeUiSocialActionOpenedUpdateReceivedInvocations.first?.isLike, LikesMock.toLike.liked)
    }
}
