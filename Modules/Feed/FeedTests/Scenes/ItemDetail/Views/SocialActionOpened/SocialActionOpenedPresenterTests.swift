import UI
import XCTest
@testable import Feed

private enum LikesMock {
    static let toLike = Like(liked: false, latestLikes: [], total: 0)
    static let onlyUserLiked = Like(liked: true, latestLikes: [], total: 1)
    static let userAndOneMoreLiked = Like(liked: true, latestLikes: ["jose"], total: 2)
    static let userAndTwoMoreLiked = Like(liked: true, latestLikes: ["jose", "maria"], total: 3)
    static let userAndMoreLiked = Like(liked: true, latestLikes: ["jose", "maria"], total: 999)
    static let userAndMoreThan999Liked = Like(liked: true, latestLikes: ["jose", "maria"], total: 1001)
    
    static let oneLiked = Like(liked: false, latestLikes: ["jose"], total: 1)
    static let twoLiked = Like(liked: false, latestLikes: ["jose", "maria"], total: 2)
    static let threeLiked = Like(liked: false, latestLikes: ["jose", "maria"], total: 3)
    static let moreThanThreeLiked = Like(liked: false, latestLikes: ["jose", "maria"], total: 999)
    static let moreThan999Liked = Like(liked: false, latestLikes: ["jose", "maria"], total: 1001)
}

private final class SocialActionOpenedDisplaySpy: SocialActionOpenedDisplaying {
    //MARK: - configureSocialActionOpened
    private(set) var configureSocialActionOpenedTextIconColorIsLikedCallsCount = 0
    private(set) var text: String?
    private(set) var icon: Iconography?
    private(set) var color: Colors.Style?
    private(set) var isLiked: Bool?

    func configureSocialActionOpened(text: String, icon: Iconography, color: Colors.Style, isLiked: Bool) {
        configureSocialActionOpenedTextIconColorIsLikedCallsCount += 1
        self.text = text
        self.icon = icon
        self.color = color
        self.isLiked = isLiked
    }
    
    // MARK: - ConfigureAcessibility
     private(set) var configureAcessibilityLabelCallsCount = 0
     private(set) var configureAcessibilityLabelReceivedInvocations: [String] = []

     func configureAcessibility(label: String) {
         configureAcessibilityLabelCallsCount += 1
         configureAcessibilityLabelReceivedInvocations.append(label)
     }
}

final class SocialActionOpenedPresenterTests: XCTestCase {
    private typealias Localizable = Strings.SocialActionOpened
    
    private let viewSpy = SocialActionOpenedDisplaySpy()
    private lazy var sut: SocialActionOpenedPresenting = {
        let presenter = SocialActionOpenedPresenter()
        presenter.view = viewSpy
        return presenter
    }()
    
    func testConfigureItem_WhenNoOneLike_ShouldCallViewMethod() {
        sut.configureItem(LikesMock.toLike)
        
        XCTAssertEqual(viewSpy.configureSocialActionOpenedTextIconColorIsLikedCallsCount, 1)
        XCTAssertEqual(viewSpy.text, Localizable.toLike)
        XCTAssertEqual(viewSpy.icon, Iconography.heart)
        XCTAssertEqual(viewSpy.isLiked, false)
    }
    
    func testConfigureItem_WhenOnlyUserLiked_ShouldCallViewMethod() {
        sut.configureItem(LikesMock.onlyUserLiked)
        
        XCTAssertEqual(viewSpy.configureSocialActionOpenedTextIconColorIsLikedCallsCount, 1)
        XCTAssertEqual(viewSpy.text, Localizable.liked(Localizable.you))
        XCTAssertEqual(viewSpy.icon, Iconography.heart)
        XCTAssertEqual(viewSpy.isLiked, true)
    }
    
    func testConfigureItem_WhenUserAndOneMoreLiked_ShouldCallViewMethod() {
        let name = LikesMock.userAndOneMoreLiked.latestLikes.first ?? ""
        sut.configureItem(LikesMock.userAndOneMoreLiked)
        
        XCTAssertEqual(viewSpy.configureSocialActionOpenedTextIconColorIsLikedCallsCount, 1)
        XCTAssertEqual(viewSpy.text, Localizable.oneMoreLiked(Localizable.you, name))
        XCTAssertEqual(viewSpy.icon, Iconography.heart)
        XCTAssertEqual(viewSpy.isLiked, true)
    }
    
    func testConfigureItem_WhenUserAndTwoMoreLiked_ShouldCallViewMethod() {
        let name = LikesMock.userAndTwoMoreLiked.latestLikes.first ?? ""
        sut.configureItem(LikesMock.userAndTwoMoreLiked)
        
        XCTAssertEqual(viewSpy.configureSocialActionOpenedTextIconColorIsLikedCallsCount, 1)
        XCTAssertEqual(viewSpy.text, Localizable.twoMoreLiked(Localizable.you, name))
        XCTAssertEqual(viewSpy.icon, Iconography.heart)
        XCTAssertEqual(viewSpy.isLiked, true)
    }
    
    func testConfigureItem_WhenUserAndMoreThanTwoLiked_ShouldCallViewMethod() {
        let name = LikesMock.userAndMoreLiked.latestLikes.first ?? ""
        sut.configureItem(LikesMock.userAndMoreLiked)
        
        XCTAssertEqual(viewSpy.configureSocialActionOpenedTextIconColorIsLikedCallsCount, 1)
        XCTAssertEqual(viewSpy.text, Localizable.moreThanTwoLiked(Localizable.you, name, 997))
        XCTAssertEqual(viewSpy.icon, Iconography.heart)
        XCTAssertEqual(viewSpy.isLiked, true)
    }
    
    func testConfigureItem_WhenUserAndMoreThan999Liked_ShouldCallViewMethod() {
        let name = LikesMock.userAndMoreLiked.latestLikes.first ?? ""
        sut.configureItem(LikesMock.userAndMoreThan999Liked)
        
        XCTAssertEqual(viewSpy.configureSocialActionOpenedTextIconColorIsLikedCallsCount, 1)
        XCTAssertEqual(viewSpy.text, Localizable.morePeopleLiked(Localizable.you, name))
        XCTAssertEqual(viewSpy.icon, Iconography.heart)
        XCTAssertEqual(viewSpy.isLiked, true)
    }
    
    func testConfigureItem_WhenOneLiked_ShouldCallViewMethod() {
        let name = LikesMock.oneLiked.latestLikes.first ?? ""
        sut.configureItem(LikesMock.oneLiked)
        
        XCTAssertEqual(viewSpy.configureSocialActionOpenedTextIconColorIsLikedCallsCount, 1)
        XCTAssertEqual(viewSpy.text, Localizable.liked(name))
        XCTAssertEqual(viewSpy.icon, Iconography.heart)
        XCTAssertEqual(viewSpy.isLiked, false)
    }
    
    func testConfigureItem_WhenTwoLiked_ShouldCallViewMethod() {
        let firstName = LikesMock.twoLiked.latestLikes.first ?? ""
        let secondName = LikesMock.twoLiked.latestLikes.last ?? ""
        sut.configureItem(LikesMock.twoLiked)
        
        XCTAssertEqual(viewSpy.configureSocialActionOpenedTextIconColorIsLikedCallsCount, 1)
        XCTAssertEqual(viewSpy.text, Localizable.oneMoreLiked(firstName, secondName))
        XCTAssertEqual(viewSpy.icon, Iconography.heart)
        XCTAssertEqual(viewSpy.isLiked, false)
    }
    
    func testConfigureItem_WhenThreeLiked_ShouldCallViewMethod() {
        let firstName = LikesMock.threeLiked.latestLikes.first ?? ""
        let secondName = LikesMock.threeLiked.latestLikes.last ?? ""
        sut.configureItem(LikesMock.threeLiked)
        
        XCTAssertEqual(viewSpy.configureSocialActionOpenedTextIconColorIsLikedCallsCount, 1)
        XCTAssertEqual(viewSpy.text, Localizable.twoMoreLiked(firstName, secondName))
        XCTAssertEqual(viewSpy.icon, Iconography.heart)
        XCTAssertEqual(viewSpy.isLiked, false)
    }
    
    func testConfigureItem_WhenMoreThanThreeLiked_ShouldCallViewMethod() {
        let firstName = LikesMock.moreThanThreeLiked.latestLikes.first ?? ""
        let secondName = LikesMock.moreThanThreeLiked.latestLikes.last ?? ""
        sut.configureItem(LikesMock.moreThanThreeLiked)
        
        XCTAssertEqual(viewSpy.configureSocialActionOpenedTextIconColorIsLikedCallsCount, 1)
        XCTAssertEqual(viewSpy.text, Localizable.moreThanTwoLiked(firstName, secondName, 997))
        XCTAssertEqual(viewSpy.icon, Iconography.heart)
        XCTAssertEqual(viewSpy.isLiked, false)
    }
    
    func testConfigureItem_WhenMoreThan999Liked_ShouldCallViewMethod() {
        let firstName = LikesMock.moreThan999Liked.latestLikes.first ?? ""
        let secondName = LikesMock.moreThan999Liked.latestLikes.last ?? ""
        sut.configureItem(LikesMock.moreThan999Liked)
        
        XCTAssertEqual(viewSpy.configureSocialActionOpenedTextIconColorIsLikedCallsCount, 1)
        XCTAssertEqual(viewSpy.text, Localizable.morePeopleLiked(firstName, secondName))
        XCTAssertEqual(viewSpy.icon, Iconography.heart)
        XCTAssertEqual(viewSpy.isLiked, false)
    }
}
