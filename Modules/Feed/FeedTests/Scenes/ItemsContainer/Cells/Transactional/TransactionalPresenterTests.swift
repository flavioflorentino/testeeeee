import UI
import XCTest
@testable import Feed

private class TransactionalDisplayingSpy: TransactionalDisplaying {
    //MARK: - configureView
    private(set) var configureViewCallsCount = 0
    private(set) var configureViewReceivedInvocations: [UIView] = []

    func configureView(_ uiView: UIView) {
        configureViewCallsCount += 1
        configureViewReceivedInvocations.append(uiView)
    }

    //MARK: - configureAccessibilityLabel
    private(set) var configureAccessibilityLabelCallsCount = 0
    private(set) var configureAccessibilityLabelReceivedInvocations: [String] = []

    func configureAccessibilityLabel(_ label: String) {
        configureAccessibilityLabelCallsCount += 1
        configureAccessibilityLabelReceivedInvocations.append(label)
    }

    //MARK: - configureBackgroundColorWhenNotExistsAction
    private(set) var configureBackgroundColorWhenNotExistsActionCallsCount = 0

    func configureBackgroundColorWhenNotExistsAction() {
        configureBackgroundColorWhenNotExistsActionCallsCount += 1
    }

    //MARK: - configureBackgroundColorWhenExistsAction
    private(set) var configureBackgroundColorWhenExistsActionCallsCount = 0

    func configureBackgroundColorWhenExistsAction() {
        configureBackgroundColorWhenExistsActionCallsCount += 1
    }

    //MARK: - configureAccessibilityCustomActions
    private(set) var configureAccessibilityCustomActionsCallsCount = 0
    private(set) var configureAccessibilityCustomActionsReceivedInvocations: [[UIAccessibilityCustomAction]] = []

    func configureAccessibilityCustomActions(_ customActions: [UIAccessibilityCustomAction]) {
        configureAccessibilityCustomActionsCallsCount += 1
        configureAccessibilityCustomActionsReceivedInvocations.append(customActions)
    }
}

final class TransactionalPresenterTests: XCTestCase {
    private let viewSpy = TransactionalDisplayingSpy()
    private let containerMock = DependencyContainerMock(Date(), TimeZone.current)
    
    private lazy var sut: TransactionalPresenter = {
        let presenter = TransactionalPresenter(dependencies: containerMock)
        presenter.view = viewSpy
        return presenter
    }()
    
    func testCreateActionLogView_ShouldCallConfigureView() {
        sut.createActionLogView(
            from: DataMock.ActionLogMock.withActionExistsInImage,
            hasAction: true,
            delegate: nil
        )
        
        XCTAssertEqual(viewSpy.configureViewCallsCount, 1)
        XCTAssertTrue(viewSpy.configureViewReceivedInvocations.first is ActionLogView)
    }
    
    func testCreateUserTestimonialView_ShouldCallConfigureView() {
        sut.createUserTestimonialView(
            from: DataMock.UserTestimonialMock.fullData,
            hasAction: true
        )
        
        XCTAssertEqual(viewSpy.configureViewCallsCount, 1)
        XCTAssertTrue(viewSpy.configureViewReceivedInvocations.first is UserTestimonialView)
    }
    
    func testCreateAdditionalInfoView_ShouldCallConfigureView() {
        sut.createAdditionalInfoView(
            from: DataMock.AdditionalInfoMock.fullData,
            hasAction: true,
            actionable: nil,
            delegate: nil
        )

        XCTAssertEqual(viewSpy.configureViewCallsCount, 1)
        XCTAssertTrue(viewSpy.configureViewReceivedInvocations.first is AdditionalInfoView)
        XCTAssertNotNil((viewSpy.configureViewReceivedInvocations.first as? AdditionalInfoView)?.accessibilityDelegate)
    }
    
    func testCreatePostDetailView_ShouldCallConfigureView() {
        sut.createPostDetailView(
            from: DataMock.PostDetailMock.withOneHourAgo,
            hasAction: true
        )
        
        XCTAssertEqual(viewSpy.configureViewCallsCount, 1)
        XCTAssertTrue(viewSpy.configureViewReceivedInvocations.first is PostDetailView)
    }
    
    func testCreateSocialActionView_ShouldCallConfigureView() {
        sut.createSocialActionView(
            from: DataMock.SocialActionMock.fullData,
            hasAction: true,
            delegate: nil
        )
        
        XCTAssertEqual(viewSpy.configureViewCallsCount, 1)
        XCTAssertTrue(viewSpy.configureViewReceivedInvocations.first is SocialActionView)
        XCTAssertNotNil((viewSpy.configureViewReceivedInvocations.first as? SocialActionView)?.accessibilityDelegate)
    }
    
    func testConfigureBackgroundColorNotContainsAction_ShouldCallConfigureBackgroundColorContainerNotExistsActionCallsCount() {
        sut.configureBackgroundColorWhenNotExistsAction()
        
        XCTAssertEqual(viewSpy.configureBackgroundColorWhenNotExistsActionCallsCount, 1)
    }
    
    func testConfigureBackgroundColorContainsAction_ShouldConfigureBackgroundColorContainerExistsActionCallsCount() {
        sut.configureBackgroundColorWhenExistsAction()
        
        XCTAssertEqual(viewSpy.configureBackgroundColorWhenExistsActionCallsCount, 1)
    }
    
    func testConfigureAccessibility_WhenPostDetailPrivacyIsPrivate_ShoudlCallConfigureAccessibilityLabel() {
        let expectedAccessibilityLabel = """
        Esse pagamento rendeu dinheiro de volta para Dani.boy fique feliz e continue gastando., \
        Obrigado meu consagrado, \
        Esse pagamento rendeu dinheiro de volta para Dani.boy fique feliz e continue gastando., \
        débito de R$ 100,00, Agora há pouco, privacidade: Privado
        """
        
        sut.configureAccessibility(
            actionLog: DataMock.ActionLogMock.withActionNotExistsInImage,
            userTestimonial: DataMock.UserTestimonialMock.fullData,
            additionalInfo: DataMock.AdditionalInfoMock.fullData,
            postDetail: DataMock.PostDetailMock.withCurrencyIsDebit
        )
        
        XCTAssertEqual(viewSpy.configureAccessibilityLabelCallsCount, 1)
        XCTAssertEqual(viewSpy.configureAccessibilityLabelReceivedInvocations.first, expectedAccessibilityLabel)
    }
    
    func testConfigureAccessibility_WhenPostDetailPrivacyIsPublic_ShoudlCallConfigureAccessibilityLabel() {
        let expectedAccessibilityLabel = """
        Esse pagamento rendeu dinheiro de volta para Dani.boy fique feliz e continue gastando., \
        Obrigado meu consagrado, \
        Esse pagamento rendeu dinheiro de volta para Dani.boy fique feliz e continue gastando., \
        débito de R$ 100,00, Agora há pouco, privacidade: Privado
        """
        
        sut.configureAccessibility(
            actionLog: DataMock.ActionLogMock.withActionNotExistsInImage,
            userTestimonial: DataMock.UserTestimonialMock.fullData,
            additionalInfo: DataMock.AdditionalInfoMock.fullData,
            postDetail: DataMock.PostDetailMock.withCurrencyIsDebit
        )
        
        XCTAssertEqual(viewSpy.configureAccessibilityLabelCallsCount, 1)
        XCTAssertEqual(viewSpy.configureAccessibilityLabelReceivedInvocations.first, expectedAccessibilityLabel)
    }
    
    func testConfigureAccessibility_WhenPostDetailCurrencyCredit_ShoudlCallConfigureAccessibilityLabel() {
        let expectedAccessibilityLabel = """
        Esse pagamento rendeu dinheiro de volta para Dani.boy fique feliz e continue gastando., \
        Obrigado meu consagrado, \
        Esse pagamento rendeu dinheiro de volta para Dani.boy fique feliz e continue gastando., \
        crédito de R$ 100,00, Agora há pouco, privacidade: Privado
        """
        
        sut.configureAccessibility(
            actionLog: DataMock.ActionLogMock.withActionNotExistsInImage,
            userTestimonial: DataMock.UserTestimonialMock.fullData,
            additionalInfo: DataMock.AdditionalInfoMock.fullData,
            postDetail: DataMock.PostDetailMock.withCurrencyIsCredit
        )
        
        XCTAssertEqual(viewSpy.configureAccessibilityLabelCallsCount, 1)
        XCTAssertEqual(viewSpy.configureAccessibilityLabelReceivedInvocations.first, expectedAccessibilityLabel)
    }
    
    func testConfigureAccessibility_WhenPostDetailCurrencyNeutral_ShoudlCallConfigureAccessibilityLabel() {
        let expectedAccessibilityLabel = """
        Esse pagamento rendeu dinheiro de volta para Dani.boy fique feliz e continue gastando., \
        Obrigado meu consagrado, \
        Esse pagamento rendeu dinheiro de volta para Dani.boy fique feliz e continue gastando., \
        valor de R$ 100,00, Agora há pouco, privacidade: Público
        """
        
        sut.configureAccessibility(
            actionLog: DataMock.ActionLogMock.withActionNotExistsInImage,
            userTestimonial: DataMock.UserTestimonialMock.fullData,
            additionalInfo: DataMock.AdditionalInfoMock.fullData,
            postDetail: DataMock.PostDetailMock.withPrivacyIsPublic
        )
        
        XCTAssertEqual(viewSpy.configureAccessibilityLabelCallsCount, 1)
        XCTAssertEqual(viewSpy.configureAccessibilityLabelReceivedInvocations.first, expectedAccessibilityLabel)
    }
    
    func testAccessibilityCustomAction_ShouldCallConfigureAccessibilityCustomActions() {
        sut.accessibilityCustomAction(.init(name: "", target: nil, selector: Selector(("fakeSelector"))))
        
        XCTAssertEqual(viewSpy.configureAccessibilityCustomActionsCallsCount, 1)
    }
}
