import XCTest
@testable import Feed

private class AdditionalInfoPresentingSpy: AdditionalInfoPresenting {
    var view: AdditionalInfoDisplaying?

    //MARK: - configureItem
    private(set) var configureItemHasActionCallsCount = 0
    private(set) var configureItemHasActionReceivedInvocations: [(item: AdditionalInfo, hasAction: Bool)] = []

    func configureItem(_ item: AdditionalInfo, hasAction: Bool) {
        configureItemHasActionCallsCount += 1
        configureItemHasActionReceivedInvocations.append((item: item, hasAction: hasAction))
    }

    //MARK: - updateBackgroundColorIfNeeded
    private(set) var updateBackgroundColorIfNeededCallsCount = 0
    private(set) var updateBackgroundColorIfNeededReceivedInvocations: [Bool] = []

    func updateBackgroundColorIfNeeded(_ hasAction: Bool) {
        updateBackgroundColorIfNeededCallsCount += 1
        updateBackgroundColorIfNeededReceivedInvocations.append(hasAction)
    }
}

private class AdditionalInfoDelegateSpy: AdditionalInfoDelegate {
    // MARK: - DidTouchPrimaryAction
    private(set) var didTouchPrimaryActionForDeeplinkUrlCallsCount = 0
    private(set) var didTouchPrimaryActionForDeeplinkUrlReceivedInvocations: [(text: String, deeplinkUrl: URL)] = []

    func didTouchPrimaryAction(for text: String, deeplinkUrl: URL) {
        didTouchPrimaryActionForDeeplinkUrlCallsCount += 1
        didTouchPrimaryActionForDeeplinkUrlReceivedInvocations.append((text: text, deeplinkUrl: deeplinkUrl))
    }

    // MARK: - DidTouchSecondaryAction
    private(set) var didTouchSecondaryActionForDeeplinkUrlCallsCount = 0
    private(set) var didTouchSecondaryActionForDeeplinkUrlReceivedInvocations: [(text: String, deeplinkUrl: URL)] = []

    func didTouchSecondaryAction(for text: String, deeplinkUrl: URL) {
        didTouchSecondaryActionForDeeplinkUrlCallsCount += 1
        didTouchSecondaryActionForDeeplinkUrlReceivedInvocations.append((text: text, deeplinkUrl: deeplinkUrl))
    }

    // MARK: - DidTouchContainerAction
    private(set) var didTouchContainerActionForDeeplinkUrlCallsCount = 0
    private(set) var didTouchContainerActionForDeeplinkUrlReceivedInvocations: [(text: String, deeplinkUrl: URL)] = []

    func didTouchContainerAction(for text: String, deeplinkUrl: URL) {
        didTouchContainerActionForDeeplinkUrlCallsCount += 1
        didTouchContainerActionForDeeplinkUrlReceivedInvocations.append((text: text, deeplinkUrl: deeplinkUrl))
    }

    // MARK: - DidSelectText
    private(set) var didSelectTextDeeplinkUrlCallsCount = 0
    private(set) var didSelectTextDeeplinkUrlReceivedInvocations: [(text: String, deeplinkUrl: URL)] = []

    func didSelectText(_ text: String, deeplinkUrl: URL) {
        didSelectTextDeeplinkUrlCallsCount += 1
        didSelectTextDeeplinkUrlReceivedInvocations.append((text: text, deeplinkUrl: deeplinkUrl))
    }
}

final class AdditionalInfoInteractorTests: XCTestCase {
    private let presenterSpy = AdditionalInfoPresentingSpy()
    private let delegateSpy = AdditionalInfoDelegateSpy()
        
    private lazy var sut = AdditionalInfoInteractor(
        item: DataMock.AdditionalInfoMock.fullData,
        presenter: presenterSpy,
        hasAction: true,
        delegate: delegateSpy
    )
    
    func testLoadData_ShouldCallConfigureItem() {
        sut.loadData()
        
        XCTAssertEqual(presenterSpy.configureItemHasActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.configureItemHasActionReceivedInvocations.first?.item, DataMock.AdditionalInfoMock.fullData)
        XCTAssertEqual(presenterSpy.configureItemHasActionReceivedInvocations.first?.hasAction, true)
    }
    
    func testPrimaryAction_WhenPrimaryActionNotExists_ShouldNotCallMethodsTheActionableAndDelegate() {
        sut.primaryAction()
        
        XCTAssertEqual(delegateSpy.didTouchPrimaryActionForDeeplinkUrlCallsCount, 0)
    }
    
    func testPrimaryAction_WhenPrimaryActionIsDeeplink_ShouldCallDeeplinkMethod() {
        sut = AdditionalInfoInteractor(
            item: DataMock.AdditionalInfoMock.withPrimaryActionIsDeeplink,
            presenter: presenterSpy,
            hasAction: true,
            delegate: delegateSpy
        )
        
        sut.primaryAction()
        
        XCTAssertEqual(delegateSpy.didTouchPrimaryActionForDeeplinkUrlCallsCount, 1)
        XCTAssertEqual(delegateSpy.didTouchPrimaryActionForDeeplinkUrlReceivedInvocations.first?.text, "Saiba Mais")
        XCTAssertEqual(delegateSpy.didTouchPrimaryActionForDeeplinkUrlReceivedInvocations.first?.deeplinkUrl, URL(string: "https://app.picpay.com/"))
    }
    
    func testSecondaryAction_WhenActionNotExists_ShouldNotCallMethodsTheActionableAndDelegate() {
        sut.secondaryAction()
        
        XCTAssertEqual(delegateSpy.didTouchSecondaryActionForDeeplinkUrlCallsCount, 0)
    }
    
    func testSecondaryAction_WhenSecondaryActionIsDeeplink_ShouldCallDeeplinkMethod() {
        sut = AdditionalInfoInteractor(
            item: DataMock.AdditionalInfoMock.withSecondaryActionIsDeeplink,
            presenter: presenterSpy,
            hasAction: true,
            delegate: delegateSpy
        )
        
        sut.secondaryAction()
        
        XCTAssertEqual(delegateSpy.didTouchSecondaryActionForDeeplinkUrlCallsCount, 1)
        XCTAssertEqual(delegateSpy.didTouchSecondaryActionForDeeplinkUrlReceivedInvocations.first?.text, "Saiba Mais")
        XCTAssertEqual(delegateSpy.didTouchSecondaryActionForDeeplinkUrlReceivedInvocations.first?.deeplinkUrl, URL(string: "https://app.picpay.com/"))
    }
    
    func testContainerAction_WhenActionNotExists_ShouldNotCallMethodsTheActionableAndDelegate() {
        sut.containerAction()
        
        XCTAssertEqual(delegateSpy.didTouchContainerActionForDeeplinkUrlCallsCount, 0)
    }
    
    func testContainerAction_WhenActionIsDeeplink_ShouldCallDeeplinkMethod() {
        sut = AdditionalInfoInteractor(
            item: DataMock.AdditionalInfoMock.withActionIsDeeplink,
            presenter: presenterSpy,
            hasAction: true,
            delegate: delegateSpy
        )
        
        sut.containerAction()
        
        XCTAssertEqual(delegateSpy.didTouchContainerActionForDeeplinkUrlCallsCount, 1)
        XCTAssertEqual(delegateSpy.didTouchContainerActionForDeeplinkUrlReceivedInvocations.first?.text, "Esse pagamento rendeu dinheiro de volta para Dani.boy fique feliz e continue gastando.")
        XCTAssertEqual(delegateSpy.didTouchContainerActionForDeeplinkUrlReceivedInvocations.first?.deeplinkUrl, URL(string: "https://app.picpay.com/"))
    }
    
    func testUpdateBackgroundColorIfNeeded_ShouldCallUpdateBackgroundColorIfNeeded() {
        sut.updateBackgroundColorIfNeeded()
        
        XCTAssertEqual(presenterSpy.updateBackgroundColorIfNeededCallsCount, 1)
        XCTAssertEqual(presenterSpy.updateBackgroundColorIfNeededReceivedInvocations.first, true)
    }
    
    func testDidSelected_WhenSelectedWordNotExistsInAttributes_ShouldNotCallMethods() {
        sut = AdditionalInfoInteractor(
            item: DataMock.AdditionalInfoMock.withActionIsDeeplink,
            presenter: presenterSpy,
            hasAction: true,
            delegate: delegateSpy
        )
        
        sut.didSelected(from: "dinheiro")
        
        XCTAssertEqual(delegateSpy.didSelectTextDeeplinkUrlCallsCount, 0)
    }
    
    func testDidSelected_WhenSelectWordIsDeeplinkAction_ShoudCallDeeplinkMethod() {
        sut = AdditionalInfoInteractor(
            item: DataMock.AdditionalInfoMock.withActionIsDeeplink,
            presenter: presenterSpy,
            hasAction: true,
            delegate: delegateSpy
        )
        
        sut.didSelected(from: "pagamento")
        
        XCTAssertEqual(delegateSpy.didSelectTextDeeplinkUrlCallsCount, 1)
        XCTAssertEqual(delegateSpy.didSelectTextDeeplinkUrlReceivedInvocations.first?.text, "pagamento")
        XCTAssertEqual(delegateSpy.didSelectTextDeeplinkUrlReceivedInvocations.first?.deeplinkUrl, URL(string: "https://app.picpay.com/"))
    }
}
