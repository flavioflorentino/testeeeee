import UI
import XCTest
@testable import Feed

private class SocialActionDisplayingSpy: SocialActionDisplaying {
    //MARK: - configureLikeButton
    private(set) var configureLikeButtonTitleIconColorCallsCount = 0
    private(set) var configureLikeButtonTitleIconColorReceivedInvocations: [(title: String, icon: Iconography, color: Colors.Style)] = []

    func configureLikeButton(title: String, icon: Iconography, color: Colors.Style) {
        configureLikeButtonTitleIconColorCallsCount += 1
        configureLikeButtonTitleIconColorReceivedInvocations.append((title: title, icon: icon, color: color))
    }

    //MARK: - configureCommentButton
    private(set) var configureCommentButtonTitleIconColorCallsCount = 0
    private(set) var configureCommentButtonTitleIconColorReceivedInvocations: [(title: String, icon: Iconography, color: Colors.Style)] = []

    func configureCommentButton(title: String, icon: Iconography, color: Colors.Style) {
        configureCommentButtonTitleIconColorCallsCount += 1
        configureCommentButtonTitleIconColorReceivedInvocations.append((title: title, icon: icon, color: color))
    }

    //MARK: - configureBackgroundColorWhenNotExistsAction
    private(set) var configureBackgroundColorWhenNotExistsActionCallsCount = 0

    func configureBackgroundColorWhenNotExistsAction() {
        configureBackgroundColorWhenNotExistsActionCallsCount += 1
    }

    //MARK: - configureBackgroundColorWhenExistsAction
    private(set) var configureBackgroundColorWhenExistsActionCallsCount = 0

    func configureBackgroundColorWhenExistsAction() {
        configureBackgroundColorWhenExistsActionCallsCount += 1
    }
}

final class SocialActionPresenterTests: XCTestCase {
    private let viewSpy = SocialActionDisplayingSpy()
    
    private lazy var sut: SocialActionPresenter = {
        let presenter = SocialActionPresenter()
        presenter.view = viewSpy
        return presenter
    }()
    
    func testConfigureItem_WhenLikedIsTrue_ShouldCallConfigureButtonLikeAndButtonComment() {
        sut.configureItem(DataMock.SocialActionMock.withLiked)
        
        XCTAssertEqual(viewSpy.configureLikeButtonTitleIconColorCallsCount, 1)
        XCTAssertEqual(viewSpy.configureLikeButtonTitleIconColorReceivedInvocations.first?.title, Strings.liked(100))
        XCTAssertEqual(viewSpy.configureCommentButtonTitleIconColorCallsCount, 1)
        XCTAssertEqual(viewSpy.configureCommentButtonTitleIconColorReceivedInvocations.first?.title, Strings.comment(10))
    }
    
    func testConfigureItem_WhenLikedIsFalse_ShouldCallConfigureButtonLikeAndButtonComment() {
        sut.configureItem(DataMock.SocialActionMock.withLike)
        
        XCTAssertEqual(viewSpy.configureLikeButtonTitleIconColorCallsCount, 1)
        XCTAssertEqual(viewSpy.configureLikeButtonTitleIconColorReceivedInvocations.first?.title, Strings.like(100))
        XCTAssertEqual(viewSpy.configureCommentButtonTitleIconColorCallsCount, 1)
        XCTAssertEqual(viewSpy.configureCommentButtonTitleIconColorReceivedInvocations.first?.title, Strings.comment(10))
    }
    
    func testRefreshItem_WhenLikedIsTrue_ShouldCallConfigureLikeButton() {
        sut.refreshItem(liked: true, newLikeCount: 10)
        
        XCTAssertEqual(viewSpy.configureLikeButtonTitleIconColorCallsCount, 1)
        XCTAssertEqual(viewSpy.configureLikeButtonTitleIconColorReceivedInvocations.first?.title, Strings.liked(10))
    }
    
    func testRefreshItem_WhenLikedIsFalse_ShouldCallConfigureLikeButton() {
        sut.refreshItem(liked: false, newLikeCount: 10)
        
        XCTAssertEqual(viewSpy.configureLikeButtonTitleIconColorCallsCount, 1)
        XCTAssertEqual(viewSpy.configureLikeButtonTitleIconColorReceivedInvocations.first?.title, Strings.like(10))
    }
    
    func testUpdateBackgroundColorIfNeeded_WhenHasActionIsTrue_ShouldCallConfigureBackgroundColorWhenExistsAction() {
        sut.updateBackgroundColorIfNeeded(true)
        
        XCTAssertEqual(viewSpy.configureBackgroundColorWhenExistsActionCallsCount, 1)
        XCTAssertEqual(viewSpy.configureBackgroundColorWhenNotExistsActionCallsCount, 0)
    }
    
    func testUpdateBackgroundColorIfNeeded_WhenHasActionIsFalse_ShouldCallConfigureBackgroundColorWhenNotExistsAction() {
        sut.updateBackgroundColorIfNeeded(false)
        
        XCTAssertEqual(viewSpy.configureBackgroundColorWhenNotExistsActionCallsCount, 1)
        XCTAssertEqual(viewSpy.configureBackgroundColorWhenExistsActionCallsCount, 0)
    }
}
