import AssetsKit
import UI
import XCTest
@testable import Feed

private class ActionLogDisplayingSpy: ActionLogDisplaying {
    //MARK: - configureDescription
    private(set) var configureDescriptionCallsCount = 0
    private(set) var configureDescriptionReceivedInvocations: [String] = []

    func configureDescription(_ text: String) {
        configureDescriptionCallsCount += 1
        configureDescriptionReceivedInvocations.append(text)
    }

    //MARK: - configurePrimaryImage
    private(set) var configurePrimaryImageImagePlaceholderCallsCount = 0
    private(set) var configurePrimaryImageImagePlaceholderReceivedInvocations: [(imageUrl: URL, imagePlaceholder: UIImage?)] = []

    func configurePrimaryImage(_ imageUrl: URL, imagePlaceholder: UIImage?) {
        configurePrimaryImageImagePlaceholderCallsCount += 1
        configurePrimaryImageImagePlaceholderReceivedInvocations.append((imageUrl: imageUrl, imagePlaceholder: imagePlaceholder))
    }

    //MARK: - configurePrimaryImageStyle<S: ImageStyle>
    private(set) var configurePrimaryImageStyleCallsCount = 0
    private(set) var configurePrimaryImageStyleReceivedInvocations: [Any] = []

    func configurePrimaryImageStyle<S: ImageStyle>(_ style: S) {
        configurePrimaryImageStyleCallsCount += 1
        configurePrimaryImageStyleReceivedInvocations.append(style)
    }

    //MARK: - configureSecondaryImage
    private(set) var configureSecondaryImageImagePlaceholderCallsCount = 0
    private(set) var configureSecondaryImageImagePlaceholderReceivedInvocations: [(imageUrl: URL, imagePlaceholder: UIImage?)] = []

    func configureSecondaryImage(_ imageUrl: URL, imagePlaceholder: UIImage?) {
        configureSecondaryImageImagePlaceholderCallsCount += 1
        configureSecondaryImageImagePlaceholderReceivedInvocations.append((imageUrl: imageUrl, imagePlaceholder: imagePlaceholder))
    }

    //MARK: - configureSecondaryImageStyle<S: ImageStyle>
    private(set) var configureSecondaryImageStyleCallsCount = 0
    private(set) var configureSecondaryImageStyleReceivedInvocations: [Any] = []

    func configureSecondaryImageStyle<S: ImageStyle>(_ style: S) {
        configureSecondaryImageStyleCallsCount += 1
        configureSecondaryImageStyleReceivedInvocations.append(style)
    }

    //MARK: - configureAttributesRanges
    private(set) var configureAttributesRangesCallsCount = 0
    private(set) var configureAttributesRangesReceivedInvocations: [[NSRange]] = []

    func configureAttributesRanges(_ ranges: [NSRange]) {
        configureAttributesRangesCallsCount += 1
        configureAttributesRangesReceivedInvocations.append(ranges)
    }

    //MARK: - configureBackgroundColorWhenNotExistsAction
    private(set) var configureBackgroundColorWhenNotExistsActionCallsCount = 0

    func configureBackgroundColorWhenNotExistsAction() {
        configureBackgroundColorWhenNotExistsActionCallsCount += 1
    }

    //MARK: - configureBackgroundColorWhenExistsAction
    private(set) var configureBackgroundColorWhenExistsActionCallsCount = 0

    func configureBackgroundColorWhenExistsAction() {
        configureBackgroundColorWhenExistsActionCallsCount += 1
    }
}

final class ActionLogPresenterTests: XCTestCase {
    private let viewSpy = ActionLogDisplayingSpy()
    
    private lazy var sut: ActionLogPresenter = {
        let presenter = ActionLogPresenter()
        presenter.view = viewSpy
        return presenter
    }()
    
    func testConfigureItem_WhenExistsOnlyPrimaryImage_ShouldConfigureViewOnlyPrimaryImage() {
        sut.configureItem(DataMock.ActionLogMock.withOnlyPrimaryImage)
        
        XCTAssertEqual(viewSpy.configureAttributesRangesCallsCount, 1)
        XCTAssertEqual(viewSpy.configureDescriptionCallsCount, 1)
        XCTAssertEqual(viewSpy.configurePrimaryImageStyleCallsCount, 1)
        XCTAssertEqual(viewSpy.configureSecondaryImageStyleCallsCount, 0)
        XCTAssertEqual(viewSpy.configurePrimaryImageImagePlaceholderCallsCount, 1)
        XCTAssertEqual(viewSpy.configureSecondaryImageImagePlaceholderCallsCount, 0)
    }
    
    func testConfigureItem_WhenImageIsConsumer_ShouldConfigurePrimarImagePlaceholder() {
        sut.configureItem(DataMock.ActionLogMock.withPrimaryImageTypeIsConsumer)
        
        XCTAssertEqual(viewSpy.configurePrimaryImageImagePlaceholderCallsCount, 1)
        XCTAssertEqual(viewSpy.configurePrimaryImageImagePlaceholderReceivedInvocations.first?.imagePlaceholder, Resources.Placeholders.greenAvatarPlaceholder.image)
    }
    
    func testConfigureItem_WhenImageIsSeller_ShouldConfigurePrimarImagePlaceholder() {
        sut.configureItem(DataMock.ActionLogMock.withPrimaryImageTypeIsSeller)
        
        XCTAssertEqual(viewSpy.configurePrimaryImageImagePlaceholderCallsCount, 1)
        XCTAssertEqual(viewSpy.configurePrimaryImageImagePlaceholderReceivedInvocations.first?.imagePlaceholder, Resources.Placeholders.sellerAvatarPlaceholder.image)
    }
    
    func testConfigureItem_WhenImageIsAction_ShouldConfigurePrimarImagePlaceholder() {
        sut.configureItem(DataMock.ActionLogMock.withPrimaryImageTypeIsAction)
        
        XCTAssertEqual(viewSpy.configurePrimaryImageImagePlaceholderCallsCount, 1)
        XCTAssertEqual(viewSpy.configurePrimaryImageImagePlaceholderReceivedInvocations.first?.imagePlaceholder, Resources.Placeholders.actionAvatarPlaceholder.image)
    }
    
    func testConfigureItem_WhenExistsPrimaryAndSecondaryImage_ShouldConfigureViewPrimaryAndSecondaryImage() {
        sut.configureItem(DataMock.ActionLogMock.withActionExistsInImage)
        
        XCTAssertEqual(viewSpy.configureAttributesRangesCallsCount, 1)
        XCTAssertEqual(viewSpy.configureDescriptionCallsCount, 1)
        XCTAssertEqual(viewSpy.configurePrimaryImageStyleCallsCount, 1)
        XCTAssertEqual(viewSpy.configureSecondaryImageStyleCallsCount, 1)
        XCTAssertEqual(viewSpy.configurePrimaryImageImagePlaceholderCallsCount, 1)
        XCTAssertEqual(viewSpy.configureSecondaryImageImagePlaceholderCallsCount, 1)
    }
    
    func testUpdateBackgroundColorIfNeeded_WhenHasActionIsTrue_ShouldCallConfigureBackgroundColorWhenExistsAction() {
        sut.updateBackgroundColorIfNeeded(for: DataMock.ActionLogMock.withActionNotExistsInImage, hasAction: true)
        
        XCTAssertEqual(viewSpy.configurePrimaryImageStyleCallsCount, 1)
        XCTAssertEqual(viewSpy.configureSecondaryImageStyleCallsCount, 1)
        XCTAssertEqual(viewSpy.configureBackgroundColorWhenExistsActionCallsCount, 1)
        XCTAssertEqual(viewSpy.configureBackgroundColorWhenNotExistsActionCallsCount, 0)
    }
    
    func testUpdateBackgroundColorIfNeeded_WhenHasActionIsFalse_ShouldCallConfigureBackgroundColorWhenNotExistsAction() {
        sut.updateBackgroundColorIfNeeded(for: DataMock.ActionLogMock.withActionNotExistsInImage, hasAction: false)
        
        XCTAssertEqual(viewSpy.configurePrimaryImageStyleCallsCount, 1)
        XCTAssertEqual(viewSpy.configureSecondaryImageStyleCallsCount, 1)
        XCTAssertEqual(viewSpy.configureBackgroundColorWhenNotExistsActionCallsCount, 1)
        XCTAssertEqual(viewSpy.configureBackgroundColorWhenExistsActionCallsCount, 0)
    }
}
