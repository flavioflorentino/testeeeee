import XCTest
@testable import Feed

private class ActionLogPresentingSpy: ActionLogPresenting {
    var view: ActionLogDisplaying?

    //MARK: - configureItem
    private(set) var configureItemCallsCount = 0
    private(set) var configureItemReceivedInvocations: [ActionLog] = []

    func configureItem(_ item: ActionLog) {
        configureItemCallsCount += 1
        configureItemReceivedInvocations.append(item)
    }

    //MARK: - updateBackgroundColorIfNeeded
    private(set) var updateBackgroundColorIfNeededForHasActionCallsCount = 0
    private(set) var updateBackgroundColorIfNeededForHasActionReceivedInvocations: [(item: ActionLog, hasAction: Bool)] = []

    func updateBackgroundColorIfNeeded(for item: ActionLog, hasAction: Bool) {
        updateBackgroundColorIfNeededForHasActionCallsCount += 1
        updateBackgroundColorIfNeededForHasActionReceivedInvocations.append((item: item, hasAction: hasAction))
    }
}

private class ActionLogDelegateSpy: ActionLogDelegate {
    // MARK: - DidTouchPrimaryImage
    private(set) var didTouchPrimaryImageForCallsCount = 0
    private(set) var didTouchPrimaryImageForReceivedInvocations: URL?

    func didTouchPrimaryImage(for deeplinkUrl: URL) {
        didTouchPrimaryImageForCallsCount += 1
        didTouchPrimaryImageForReceivedInvocations = deeplinkUrl
    }

    // MARK: - DidTouchSecondaryImage
    private(set) var didTouchSecondaryImageForCallsCount = 0
    private(set) var didTouchSecondaryImageForReceivedInvocations: URL?

    func didTouchSecondaryImage(for deeplinkUrl: URL) {
        didTouchSecondaryImageForCallsCount += 1
        didTouchSecondaryImageForReceivedInvocations = deeplinkUrl
    }

    // MARK: - DidSelectText
    private(set) var didSelectTextDeeplinkUrlCallsCount = 0
    private(set) var didSelectTextDeeplinkUrlReceivedInvocations: (text: String, deeplinkUrl: URL)?

    func didSelectText(_ text: String, deeplinkUrl: URL) {
        didSelectTextDeeplinkUrlCallsCount += 1
        didSelectTextDeeplinkUrlReceivedInvocations = (text: text, deeplinkUrl: deeplinkUrl)
    }
}

final class ActionLogInteractorTests: XCTestCase {
    private let presenterSpy = ActionLogPresentingSpy()
    private let delegateSpy = ActionLogDelegateSpy()
    
    private lazy var sut = ActionLogInteractor(
        item: DataMock.ActionLogMock.withActionNotExistsInImage,
        presenter: presenterSpy,
        hasAction: true,
        delegate: delegateSpy
    )
    
    func testLoadData_ShouldCallConfigureItem() {
        sut.loadData()
        
        XCTAssertEqual(presenterSpy.configureItemCallsCount, 1)
        XCTAssertEqual(presenterSpy.configureItemReceivedInvocations.first, DataMock.ActionLogMock.withActionNotExistsInImage)
    }
    
    func testUpdateBackgroundColorIfNeeded_ShouldCallUpdateBackgroundColorIfNeededWithIfExistsAction() {
        sut.updateBackgroundColorIfNeeded()
        
        XCTAssertEqual(presenterSpy.updateBackgroundColorIfNeededForHasActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.updateBackgroundColorIfNeededForHasActionReceivedInvocations.first?.item, DataMock.ActionLogMock.withActionNotExistsInImage)
        XCTAssertEqual(presenterSpy.updateBackgroundColorIfNeededForHasActionReceivedInvocations.first?.hasAction, true)
    }
    
    func testPrimaryImageAction_WhenActionNotExists_ShouldNotCallMethods() {
        sut.primaryImageAction()
        
        XCTAssertEqual(delegateSpy.didTouchPrimaryImageForCallsCount, 0)
    }
    
    func testPrimaryImageAction_WhenActionExists_ShouldCallDeeplinkMethod() {
        sut = ActionLogInteractor(
            item: DataMock.ActionLogMock.withActionExistsInImage,
            presenter: presenterSpy,
            hasAction: true,
            delegate: delegateSpy
        )
        
        sut.primaryImageAction()
        
        XCTAssertEqual(delegateSpy.didTouchPrimaryImageForCallsCount, 1)
    }
    
    func testSecondaryImageAction_WhenActionNotExists_ShouldNotCallMethods() {
        sut.secondaryImageAction()
        
        XCTAssertEqual(delegateSpy.didTouchSecondaryImageForCallsCount, 0)
    }
    
    func testSecondaryImageAction_WhenActionExists_ShouldCallDeeplinkMethod() {
        sut = ActionLogInteractor(
            item: DataMock.ActionLogMock.withActionExistsInImage,
            presenter: presenterSpy,
            hasAction: true,
            delegate: delegateSpy
        )
        
        sut.secondaryImageAction()
        
        XCTAssertEqual(delegateSpy.didTouchSecondaryImageForCallsCount, 1)
    }
    
    func testDidSelected_WhenSelectedWordNotExistsInAttributes_ShouldNotCallMethods() {
        sut = ActionLogInteractor(
            item: DataMock.ActionLogMock.withActionExistsInImage,
            presenter: presenterSpy,
            hasAction: true,
            delegate: delegateSpy
        )
        
        sut.didSelected(from: "dinheiro")
        
        XCTAssertEqual(delegateSpy.didSelectTextDeeplinkUrlCallsCount, 0)
    }
    
    func testDidSelected_WhenSelectWordIsDeeplinkAction_ShoudCallDeeplinkMethod() {
        sut = ActionLogInteractor(
            item: DataMock.ActionLogMock.withActionExistsInImage,
            presenter: presenterSpy,
            hasAction: true,
            delegate: delegateSpy
        )
        
        sut.didSelected(from: "pagamento")
        
        XCTAssertEqual(delegateSpy.didSelectTextDeeplinkUrlCallsCount, 1)
        XCTAssertEqual(delegateSpy.didSelectTextDeeplinkUrlReceivedInvocations?.text, "pagamento")
        XCTAssertEqual(delegateSpy.didSelectTextDeeplinkUrlReceivedInvocations?.deeplinkUrl, URL(string: "https://app.picpay.com/"))
    }
}
