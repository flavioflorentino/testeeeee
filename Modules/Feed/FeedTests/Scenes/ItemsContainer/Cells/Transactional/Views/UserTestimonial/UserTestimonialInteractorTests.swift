import XCTest
@testable import Feed

private class UserTestimonialPresentingSpy: UserTestimonialPresenting {
    var view: UserTestimonialDisplaying?

    //MARK: - configureItem
    private(set) var configureItemCallsCount = 0
    private(set) var configureItemReceivedInvocations: [UserTestimonial] = []

    func configureItem(_ item: UserTestimonial) {
        configureItemCallsCount += 1
        configureItemReceivedInvocations.append(item)
    }

    //MARK: - updateBackgroundColorIfNeeded
    private(set) var updateBackgroundColorIfNeededCallsCount = 0
    private(set) var updateBackgroundColorIfNeededReceivedInvocations: [Bool] = []

    func updateBackgroundColorIfNeeded(_ hasAction: Bool) {
        updateBackgroundColorIfNeededCallsCount += 1
        updateBackgroundColorIfNeededReceivedInvocations.append(hasAction)
    }
}

final class UserTestimonialInteractorTests: XCTestCase {
    private let presenterSpy = UserTestimonialPresentingSpy()
    
    private lazy var sut = UserTestimonialInteractor(
        item: DataMock.UserTestimonialMock.fullData,
        presenter: presenterSpy,
        hasAction: true
    )
    
    func testLoadData_ShouldCallConfigureItem() {
        sut.loadData()
        
        XCTAssertEqual(presenterSpy.configureItemCallsCount, 1)
        XCTAssertEqual(presenterSpy.configureItemReceivedInvocations.first, DataMock.UserTestimonialMock.fullData)
    }
    
    func testUpdateBackgroundColorIfNeeded_ShouldCallUpdateBackgroundColorIfNeeded() {
        sut.updateBackgroundColorIfNeeded()
        
        XCTAssertEqual(presenterSpy.updateBackgroundColorIfNeededCallsCount, 1)
        XCTAssertEqual(presenterSpy.updateBackgroundColorIfNeededReceivedInvocations.first, true)
    }
}
