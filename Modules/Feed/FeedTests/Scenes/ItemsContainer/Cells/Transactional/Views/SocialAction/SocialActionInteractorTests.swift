import Core
import XCTest
@testable import Feed

private class SocialActionPresentingSpy: SocialActionPresenting {
    var view: SocialActionDisplaying?

    //MARK: - configureItem
    private(set) var configureItemCallsCount = 0
    private(set) var configureItemReceivedInvocations: [SocialAction] = []

    func configureItem(_ item: SocialAction) {
        configureItemCallsCount += 1
        configureItemReceivedInvocations.append(item)
    }

    //MARK: - refreshItem
    private(set) var refreshItemLikedNewLikeCountCallsCount = 0
    private(set) var refreshItemLikedNewLikeCountReceivedInvocations: [(liked: Bool, newLikeCount: Int)] = []

    func refreshItem(liked: Bool, newLikeCount: Int) {
        refreshItemLikedNewLikeCountCallsCount += 1
        refreshItemLikedNewLikeCountReceivedInvocations.append((liked: liked, newLikeCount: newLikeCount))
    }

    //MARK: - updateBackgroundColorIfNeeded
    private(set) var updateBackgroundColorIfNeededCallsCount = 0
    private(set) var updateBackgroundColorIfNeededReceivedInvocations: [Bool] = []

    func updateBackgroundColorIfNeeded(_ hasAction: Bool) {
        updateBackgroundColorIfNeededCallsCount += 1
        updateBackgroundColorIfNeededReceivedInvocations.append(hasAction)
    }
}

private class SocialActionDelegateSpy: SocialActionDelegate {
    // MARK: - DidTouchLiked
    private(set) var didTouchLikedNumberOfLikesIsLikeUiSocialActionUpdateCallsCount = 0
    private(set) var didTouchLikedNumberOfLikesIsLikeUiSocialActionUpdateReceivedInvocations: [(numberOfLikes: Int, isLike: Bool, uiSocialActionUpdate: UISocialActionUpdating)] = []

    func didTouchLiked(numberOfLikes: Int, isLike: Bool, uiSocialActionUpdate: UISocialActionUpdating) {
        didTouchLikedNumberOfLikesIsLikeUiSocialActionUpdateCallsCount += 1
        didTouchLikedNumberOfLikesIsLikeUiSocialActionUpdateReceivedInvocations.append((numberOfLikes: numberOfLikes, isLike: isLike, uiSocialActionUpdate: uiSocialActionUpdate))
    }

    // MARK: - DidTouchCommented
    private(set) var didTouchCommentedCallsCount = 0

    func didTouchCommented() {
        didTouchCommentedCallsCount += 1
    }
}

final class SocialActionInteractorTests: XCTestCase {
    private let presenterSpy = SocialActionPresentingSpy()
    private let delegateSpy = SocialActionDelegateSpy()
    
    private lazy var sut = SocialActionInteractor(
        item: DataMock.SocialActionMock.fullData,
        presenter: presenterSpy,
        hasAction: true,
        delegate: delegateSpy
    )
    
    func testLoadData_ShouldCallConfigureItem() {
        sut.loadData()
        
        XCTAssertEqual(presenterSpy.configureItemCallsCount, 1)
        XCTAssertEqual(presenterSpy.configureItemReceivedInvocations.first, DataMock.SocialActionMock.fullData)
    }
    
    func testLikeAction__ShouldCallDidTouchLikedDelegate() {
        sut = SocialActionInteractor(
            item: DataMock.SocialActionMock.withLike,
            presenter: presenterSpy,
            hasAction: true,
            delegate: delegateSpy
        )
        
        sut.likeAction()
        
        XCTAssertEqual(delegateSpy.didTouchLikedNumberOfLikesIsLikeUiSocialActionUpdateCallsCount, 1)
        XCTAssertEqual(delegateSpy.didTouchLikedNumberOfLikesIsLikeUiSocialActionUpdateReceivedInvocations.first?.numberOfLikes, DataMock.SocialActionMock.withLike.likeCount)
        XCTAssertEqual(delegateSpy.didTouchLikedNumberOfLikesIsLikeUiSocialActionUpdateReceivedInvocations.first?.isLike, DataMock.SocialActionMock.withLike.liked)
    }
    
    func testLikeAction_ShouldCallDidTouchLikedDelegate() {
        sut = SocialActionInteractor(
            item: DataMock.SocialActionMock.withNoLikes,
            presenter: presenterSpy,
            hasAction: true,
            delegate: delegateSpy
        )
        
        sut.likeAction()
    
        XCTAssertEqual(delegateSpy.didTouchLikedNumberOfLikesIsLikeUiSocialActionUpdateCallsCount, 1)
        XCTAssertEqual(delegateSpy.didTouchLikedNumberOfLikesIsLikeUiSocialActionUpdateReceivedInvocations.first?.numberOfLikes, 0)
        XCTAssertEqual(delegateSpy.didTouchLikedNumberOfLikesIsLikeUiSocialActionUpdateReceivedInvocations.first?.isLike, false)
    }
    
    func testCommentAction_ShouldCallScreenActinable() {
        sut.commentAction()
        
        XCTAssertEqual(delegateSpy.didTouchCommentedCallsCount, 1)
    }
    
    func testUpdateBackgroundColorIfNeeded_ShouldCallUpdateBackgroundColorIfNeededFromPresenter() {
        sut.updateBackgroundColorIfNeeded()
        
        XCTAssertEqual(presenterSpy.updateBackgroundColorIfNeededCallsCount, 1)
        XCTAssertEqual(presenterSpy.updateBackgroundColorIfNeededReceivedInvocations.first, true)
    }
}
