import UI
import XCTest
@testable import Feed

private class PostDetailDisplayingSpy: PostDetailDisplaying {
    //MARK: - configureTimeAgo
    private(set) var configureTimeAgoCallsCount = 0
    private(set) var configureTimeAgoReceivedInvocations: [String] = []

    func configureTimeAgo(_ text: String) {
        configureTimeAgoCallsCount += 1
        configureTimeAgoReceivedInvocations.append(text)
    }

    //MARK: - configureCurrency
    private(set) var configureCurrencyColorCallsCount = 0
    private(set) var configureCurrencyColorReceivedInvocations: [(currency: String, color: UIColor)] = []

    func configureCurrency(_ currency: String, color: UIColor) {
        configureCurrencyColorCallsCount += 1
        configureCurrencyColorReceivedInvocations.append((currency: currency, color: color))
    }

    //MARK: - configurePrivacy
    private(set) var configurePrivacyTextCallsCount = 0
    private(set) var configurePrivacyTextReceivedInvocations: [(image: UIImage?, text: String)] = []

    func configurePrivacy(_ image: UIImage?, text: String) {
        configurePrivacyTextCallsCount += 1
        configurePrivacyTextReceivedInvocations.append((image: image, text: text))
    }

    //MARK: - configureBackgroundColorWhenNotExistsAction
    private(set) var configureBackgroundColorWhenNotExistsActionCallsCount = 0

    func configureBackgroundColorWhenNotExistsAction() {
        configureBackgroundColorWhenNotExistsActionCallsCount += 1
    }

    //MARK: - configureBackgroundColorWhenExistsAction
    private(set) var configureBackgroundColorWhenExistsActionCallsCount = 0

    func configureBackgroundColorWhenExistsAction() {
        configureBackgroundColorWhenExistsActionCallsCount += 1
    }
}

final class PostDetailPresenterTests: XCTestCase {
    private var dateMock = Date(timeIntervalSince1970: 1600974889)
    private lazy var dependencyContainerMock = DependencyContainerMock(dateMock, TimeZone(identifier: "America/Sao_Paulo")!)
    
    private let viewSpy = PostDetailDisplayingSpy()
    
    private lazy var sut: PostDetailPresenter = {
        let presenter = PostDetailPresenter(dependencies: dependencyContainerMock)
        presenter.view = viewSpy
        return presenter
    }()
    
    func testConfigureItem_ShouldConfigureLessOneMinute() {
        sut.configureItem(DataMock.PostDetailMock.withLessOneMinute)
        
        XCTAssertEqual(viewSpy.configureCurrencyColorCallsCount, 1)
        XCTAssertEqual(viewSpy.configureTimeAgoCallsCount, 1)
        XCTAssertEqual(viewSpy.configurePrivacyTextCallsCount, 1)
        XCTAssertEqual(viewSpy.configureCurrencyColorReceivedInvocations.first?.currency, DataMock.PostDetailMock.withLessOneMinute.currency.value)
        XCTAssertEqual(viewSpy.configureTimeAgoReceivedInvocations.first, "• Agora há pouco")
    }
    
    func testConfigureItem_ShouldConfigureOneMinuteAgo() {
        sut.configureItem(DataMock.PostDetailMock.withOneMinuteAgo)
        
        XCTAssertEqual(viewSpy.configureCurrencyColorCallsCount, 1)
        XCTAssertEqual(viewSpy.configureTimeAgoCallsCount, 1)
        XCTAssertEqual(viewSpy.configurePrivacyTextCallsCount, 1)
        XCTAssertEqual(viewSpy.configureCurrencyColorReceivedInvocations.first?.currency, DataMock.PostDetailMock.withLessOneMinute.currency.value)
        XCTAssertEqual(viewSpy.configureTimeAgoReceivedInvocations.first, "• Há 1 minuto")
    }
    
    func testConfigureItem_ShouldConfigureThirtyMinuteAgo() {
        sut.configureItem(DataMock.PostDetailMock.withThirtyMinuteAgo)
        
        XCTAssertEqual(viewSpy.configureCurrencyColorCallsCount, 1)
        XCTAssertEqual(viewSpy.configureTimeAgoCallsCount, 1)
        XCTAssertEqual(viewSpy.configurePrivacyTextCallsCount, 1)
        XCTAssertEqual(viewSpy.configureCurrencyColorReceivedInvocations.first?.currency, DataMock.PostDetailMock.withLessOneMinute.currency.value)
        XCTAssertEqual(viewSpy.configureTimeAgoReceivedInvocations.first, "• Há 30 minutos")
    }
    
    func testConfigureItem_ShouldConfigureOneHourAgo() {
        sut.configureItem(DataMock.PostDetailMock.withOneHourAgo)
        
        XCTAssertEqual(viewSpy.configureCurrencyColorCallsCount, 1)
        XCTAssertEqual(viewSpy.configureTimeAgoCallsCount, 1)
        XCTAssertEqual(viewSpy.configurePrivacyTextCallsCount, 1)
        XCTAssertEqual(viewSpy.configureCurrencyColorReceivedInvocations.first?.currency, DataMock.PostDetailMock.withLessOneMinute.currency.value)
        XCTAssertEqual(viewSpy.configureTimeAgoReceivedInvocations.first, "• Há 1 hora")
    }
    
    func testConfigureItem_ShouldConfigureTwentyThreeAgo() {
        sut.configureItem(DataMock.PostDetailMock.withTwentyThreeAgo)
        
        XCTAssertEqual(viewSpy.configureCurrencyColorCallsCount, 1)
        XCTAssertEqual(viewSpy.configureTimeAgoCallsCount, 1)
        XCTAssertEqual(viewSpy.configurePrivacyTextCallsCount, 1)
        XCTAssertEqual(viewSpy.configureCurrencyColorReceivedInvocations.first?.currency, DataMock.PostDetailMock.withLessOneMinute.currency.value)
        XCTAssertEqual(viewSpy.configureTimeAgoReceivedInvocations.first, "• Há 23 horas")
    }
    
    func testConfigureItem_ShouldConfigureFromYesterday() {
        sut.configureItem(DataMock.PostDetailMock.withYesterday)
        
        XCTAssertEqual(viewSpy.configureCurrencyColorCallsCount, 1)
        XCTAssertEqual(viewSpy.configureTimeAgoCallsCount, 1)
        XCTAssertEqual(viewSpy.configurePrivacyTextCallsCount, 1)
        XCTAssertEqual(viewSpy.configureCurrencyColorReceivedInvocations.first?.currency, DataMock.PostDetailMock.withLessOneMinute.currency.value)
        XCTAssertEqual(viewSpy.configureTimeAgoReceivedInvocations.first, "• Ontem às 16:14")
    }
    
    func testConfigureItem_ShouldConfigureFromOtherDays() {
        sut.configureItem(DataMock.PostDetailMock.withOtherDays)
        
        XCTAssertEqual(viewSpy.configureCurrencyColorCallsCount, 1)
        XCTAssertEqual(viewSpy.configureTimeAgoCallsCount, 1)
        XCTAssertEqual(viewSpy.configurePrivacyTextCallsCount, 1)
        XCTAssertEqual(viewSpy.configureCurrencyColorReceivedInvocations.first?.currency, DataMock.PostDetailMock.withLessOneMinute.currency.value)
        XCTAssertEqual(viewSpy.configureTimeAgoReceivedInvocations.first, "• 21 set. às 16:14")
    }
    
    func testConfigureItem_WhenPrivacyIsPublic_ShouldConfigureFromPublic() {
        sut.configureItem(DataMock.PostDetailMock.withPrivacyIsPublic)
        
        XCTAssertEqual(viewSpy.configurePrivacyTextCallsCount, 1)
        XCTAssertEqual(viewSpy.configurePrivacyTextReceivedInvocations.first?.text, "Público")
    }
    
    func testConfigureItem_WhenPrivacyIsPrivate_ShouldConfigureFromPrivate() {
        sut.configureItem(DataMock.PostDetailMock.withPrivacyIsPrivate)
        
        XCTAssertEqual(viewSpy.configurePrivacyTextCallsCount, 1)
        XCTAssertEqual(viewSpy.configurePrivacyTextReceivedInvocations.first?.text, "Privado")
    }
    
    func testUpdateBackgroundColorIfNeeded_WhenHasActionIsTrue_ShouldCallConfigureBackgroundColorWhenExistsAction() {
        sut.updateBackgroundColorIfNeeded(true)
        
        XCTAssertEqual(viewSpy.configureBackgroundColorWhenExistsActionCallsCount, 1)
        XCTAssertEqual(viewSpy.configureBackgroundColorWhenNotExistsActionCallsCount, 0)
    }
    
    func testUpdateBackgroundColorIfNeeded_WhenHasActionIsFalse_ShouldCallConfigureBackgroundColorWhenNotExistsAction() {
        sut.updateBackgroundColorIfNeeded(false)
        
        XCTAssertEqual(viewSpy.configureBackgroundColorWhenNotExistsActionCallsCount, 1)
        XCTAssertEqual(viewSpy.configureBackgroundColorWhenExistsActionCallsCount, 0)
    }
}
