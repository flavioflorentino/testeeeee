import UI
import XCTest
@testable import Feed

private class AdditionalInfoDisplayingSpy: AdditionalInfoDisplaying {
    //MARK: - configureImage
    private(set) var configureImageCallsCount = 0
    private(set) var configureImageReceivedInvocations: [UIImage?] = []

    func configureImage(_ image: UIImage?) {
        configureImageCallsCount += 1
        configureImageReceivedInvocations.append(image)
    }

    //MARK: - configureDescription
    private(set) var configureDescriptionCallsCount = 0
    private(set) var configureDescriptionReceivedInvocations: [String] = []

    func configureDescription(_ text: String) {
        configureDescriptionCallsCount += 1
        configureDescriptionReceivedInvocations.append(text)
    }

    //MARK: - configurePrimaryAction
    private(set) var configurePrimaryActionCallsCount = 0
    private(set) var configurePrimaryActionReceivedInvocations: [String] = []

    func configurePrimaryAction(_ title: String) {
        configurePrimaryActionCallsCount += 1
        configurePrimaryActionReceivedInvocations.append(title)
    }

    //MARK: - configureSecondaryAction
    private(set) var configureSecondaryActionCallsCount = 0
    private(set) var configureSecondaryActionReceivedInvocations: [String] = []

    func configureSecondaryAction(_ title: String) {
        configureSecondaryActionCallsCount += 1
        configureSecondaryActionReceivedInvocations.append(title)
    }

    //MARK: - configureContainerBackgroundColorWhenNotExistsAction
    private(set) var configureContainerBackgroundColorWhenNotExistsActionCallsCount = 0

    func configureContainerBackgroundColorWhenNotExistsAction() {
        configureContainerBackgroundColorWhenNotExistsActionCallsCount += 1
    }

    //MARK: - configureContainerBackgroundColorWhenExistsAction
    private(set) var configureContainerBackgroundColorWhenExistsActionCallsCount = 0

    func configureContainerBackgroundColorWhenExistsAction() {
        configureContainerBackgroundColorWhenExistsActionCallsCount += 1
    }

    //MARK: - configureAttributesRanges
    private(set) var configureAttributesRangesCallsCount = 0
    private(set) var configureAttributesRangesReceivedInvocations: [[NSRange]] = []

    func configureAttributesRanges(_ ranges: [NSRange]) {
        configureAttributesRangesCallsCount += 1
        configureAttributesRangesReceivedInvocations.append(ranges)
    }

    //MARK: - configureBackgroundColorWhenNotExistsAction
    private(set) var configureBackgroundColorWhenNotExistsActionCallsCount = 0

    func configureBackgroundColorWhenNotExistsAction() {
        configureBackgroundColorWhenNotExistsActionCallsCount += 1
    }

    //MARK: - configureBackgroundColorWhenExistsAction
    private(set) var configureBackgroundColorWhenExistsActionCallsCount = 0

    func configureBackgroundColorWhenExistsAction() {
        configureBackgroundColorWhenExistsActionCallsCount += 1
    }

    //MARK: - configureContainerAction
    private(set) var configureContainerActionCallsCount = 0

    func configureContainerAction() {
        configureContainerActionCallsCount += 1
    }

    //MARK: - configureAccessoryImage
    private(set) var configureAccessoryImageCallsCount = 0
    private(set) var configureAccessoryImageReceivedInvocations: [UIImage?] = []

    func configureAccessoryImage(_ image: UIImage?) {
        configureAccessoryImageCallsCount += 1
        configureAccessoryImageReceivedInvocations.append(image)
    }
}

final class AdditionalInfoPresenterTests: XCTestCase {
    private let viewSpy = AdditionalInfoDisplayingSpy()
    
    private lazy var sut: AdditionalInfoPresenter = {
        let presenter = AdditionalInfoPresenter()
        presenter.view = viewSpy
        return presenter
    }()
    
    func testConfigureItem_WhenPrimaryActionExists_ShouldConfigureOnlyPrimaryAction() {
        sut.configureItem(DataMock.AdditionalInfoMock.withPrimaryActionIsDeeplink, hasAction: true)
        
        XCTAssertEqual(viewSpy.configureAttributesRangesCallsCount, 1)
        XCTAssertEqual(viewSpy.configureAttributesRangesReceivedInvocations.count, 1)
        XCTAssertEqual(viewSpy.configureImageCallsCount, 1)
        XCTAssertEqual(viewSpy.configureDescriptionCallsCount, 1)
        XCTAssertEqual(viewSpy.configureContainerBackgroundColorWhenExistsActionCallsCount, 1)
        XCTAssertEqual(viewSpy.configureContainerBackgroundColorWhenNotExistsActionCallsCount, 0)
        XCTAssertEqual(viewSpy.configurePrimaryActionCallsCount, 1)
        XCTAssertEqual(viewSpy.configureSecondaryActionCallsCount, 0)
        XCTAssertEqual(viewSpy.configureContainerActionCallsCount, 0)
        XCTAssertEqual(viewSpy.configureAccessoryImageCallsCount, 0)
    }
    
    func testConfigureItem_WhenSecondaryActionExists_ShouldConfigureOnlySecondaryAction() {
        sut.configureItem(DataMock.AdditionalInfoMock.withSecondaryActionIsDeeplink, hasAction: true)
        
        XCTAssertEqual(viewSpy.configureAttributesRangesCallsCount, 1)
        XCTAssertEqual(viewSpy.configureAttributesRangesReceivedInvocations.count, 1)
        XCTAssertEqual(viewSpy.configureImageCallsCount, 1)
        XCTAssertEqual(viewSpy.configureDescriptionCallsCount, 1)
        XCTAssertEqual(viewSpy.configureContainerBackgroundColorWhenExistsActionCallsCount, 1)
        XCTAssertEqual(viewSpy.configureContainerBackgroundColorWhenNotExistsActionCallsCount, 0)
        XCTAssertEqual(viewSpy.configurePrimaryActionCallsCount, 0)
        XCTAssertEqual(viewSpy.configureSecondaryActionCallsCount, 1)
        XCTAssertEqual(viewSpy.configureContainerActionCallsCount, 0)
        XCTAssertEqual(viewSpy.configureAccessoryImageCallsCount, 0)
    }
    
    func testConfigureItem_WhenActionExists_ShouldConfigureContainerAction() {
        sut.configureItem(DataMock.AdditionalInfoMock.withActionIsDeeplink, hasAction: true)
        
        XCTAssertEqual(viewSpy.configureAttributesRangesCallsCount, 1)
        XCTAssertEqual(viewSpy.configureAttributesRangesReceivedInvocations.count, 1)
        XCTAssertEqual(viewSpy.configureImageCallsCount, 1)
        XCTAssertEqual(viewSpy.configureDescriptionCallsCount, 1)
        XCTAssertEqual(viewSpy.configureContainerBackgroundColorWhenExistsActionCallsCount, 1)
        XCTAssertEqual(viewSpy.configureContainerBackgroundColorWhenNotExistsActionCallsCount, 0)
        XCTAssertEqual(viewSpy.configurePrimaryActionCallsCount, 0)
        XCTAssertEqual(viewSpy.configureSecondaryActionCallsCount, 0)
        XCTAssertEqual(viewSpy.configureContainerActionCallsCount, 1)
        XCTAssertEqual(viewSpy.configureAccessoryImageCallsCount, 1)
    }
    
    func testConfigureItem_WhenHasActionIsTrue_ShouldCallConfigureContainerBackgroundColor() {
        sut.configureItem(DataMock.AdditionalInfoMock.fullData, hasAction: true)
        
        XCTAssertEqual(viewSpy.configureContainerBackgroundColorWhenExistsActionCallsCount, 1)
        XCTAssertEqual(viewSpy.configureContainerBackgroundColorWhenNotExistsActionCallsCount, 0)
    }
    
    func testConfigureItem_WhenHasActionIsFalse_ShouldCallConfigureContainerBackgroundColor() {
        sut.configureItem(DataMock.AdditionalInfoMock.fullData, hasAction: false)
        
        XCTAssertEqual(viewSpy.configureContainerBackgroundColorWhenNotExistsActionCallsCount, 1)
        XCTAssertEqual(viewSpy.configureContainerBackgroundColorWhenExistsActionCallsCount, 0)
    }
    
    func testUpdateBackgroundColorIfNeeded_WhenHasActionIsTrue_ShouldCallConfigureBackgroundColor() {
        sut.updateBackgroundColorIfNeeded(true)
        
        XCTAssertEqual(viewSpy.configureBackgroundColorWhenExistsActionCallsCount, 1)
        XCTAssertEqual(viewSpy.configureBackgroundColorWhenNotExistsActionCallsCount, 0)
    }
    
    func testUpdateBackgroundColorIfNeeded_WhenHasActionIsFalse_ShouldCallConfigureBackgroundColor() {
        sut.updateBackgroundColorIfNeeded(false)
        
        XCTAssertEqual(viewSpy.configureBackgroundColorWhenNotExistsActionCallsCount, 1)
        XCTAssertEqual(viewSpy.configureBackgroundColorWhenExistsActionCallsCount, 0)
    }
}
