import XCTest
@testable import Feed

private class PostDetailPresentingSpy: PostDetailPresenting {
    var view: PostDetailDisplaying?

    //MARK: - configureItem
    private(set) var configureItemCallsCount = 0
    private(set) var configureItemReceivedInvocations: [PostDetail] = []

    func configureItem(_ item: PostDetail) {
        configureItemCallsCount += 1
        configureItemReceivedInvocations.append(item)
    }

    //MARK: - updateBackgroundColorIfNeeded
    private(set) var updateBackgroundColorIfNeededCallsCount = 0
    private(set) var updateBackgroundColorIfNeededReceivedInvocations: [Bool] = []

    func updateBackgroundColorIfNeeded(_ hasAction: Bool) {
        updateBackgroundColorIfNeededCallsCount += 1
        updateBackgroundColorIfNeededReceivedInvocations.append(hasAction)
    }
}

final class PostDetailInteractorTests: XCTestCase {
    private let presenterSpy = PostDetailPresentingSpy()
    
    private lazy var sut = PostDetailInteractor(
        item: DataMock.PostDetailMock.withPrivacyIsPublic,
        presenter: presenterSpy,
        hasAction: true
    )
    
    func testLoadData_ShouldCallConfigureItem() {
        sut.loadData()
        
        XCTAssertEqual(presenterSpy.configureItemCallsCount, 1)
        XCTAssertEqual(presenterSpy.configureItemReceivedInvocations.first, DataMock.PostDetailMock.withPrivacyIsPublic)
    }
    
    func testUpdateBackgroundColorIfNeeded_ShouldCallUpdateBackgroundIfNeeded() {
        sut.updateBackgroundColorIfNeeded()
        
        XCTAssertEqual(presenterSpy.updateBackgroundColorIfNeededCallsCount, 1)
        XCTAssertEqual(presenterSpy.updateBackgroundColorIfNeededReceivedInvocations.first, true)
    }
}
