import UI
import XCTest
@testable import Feed

private class UserTestimonialDisplayingSpy: UserTestimonialDisplaying {
    //MARK: - configureStyle<S: UI.LabelStyle>
    private(set) var configureStyleCallsCount = 0
    private(set) var configureStyleReceivedInvocations: [Any] = []

    func configureStyle<S: UI.LabelStyle>(_ style: S) {
        configureStyleCallsCount += 1
        configureStyleReceivedInvocations.append(style)
    }

    //MARK: - configureDescription
    private(set) var configureDescriptionCallsCount = 0
    private(set) var configureDescriptionReceivedInvocations: [NSAttributedString] = []

    func configureDescription(_ attributedString: NSAttributedString) {
        configureDescriptionCallsCount += 1
        configureDescriptionReceivedInvocations.append(attributedString)
    }

    //MARK: - configureBackgroundColorWhenNotExistsAction
    private(set) var configureBackgroundColorWhenNotExistsActionCallsCount = 0

    func configureBackgroundColorWhenNotExistsAction() {
        configureBackgroundColorWhenNotExistsActionCallsCount += 1
    }

    //MARK: - configureBackgroundColorWhenExistsAction
    private(set) var configureBackgroundColorWhenExistsActionCallsCount = 0

    func configureBackgroundColorWhenExistsAction() {
        configureBackgroundColorWhenExistsActionCallsCount += 1
    }
}

final class UserTestimonialPresenterTests: XCTestCase {
    private let viewSpy = UserTestimonialDisplayingSpy()
    
    private lazy var sut: UserTestimonialPresenter = {
        let presenter = UserTestimonialPresenter()
        presenter.view = viewSpy
        return presenter
    }()
    
    func testConfigureItem_WhenContainsOnlyEmoji_ShouldCallConfigureStyleWithTitleLarge() {
        sut.configureItem(DataMock.UserTestimonialMock.withTextContainsOnlyEmoji)
        
        XCTAssertEqual(viewSpy.configureStyleCallsCount, 2)
        XCTAssertTrue(viewSpy.configureStyleReceivedInvocations.last is TitleLabelStyle)
        XCTAssertEqual(viewSpy.configureDescriptionCallsCount, 1)
    }
    
    func testConfigureItem_WhenContainsOnlyEmojiWithSpaces_ShouldCallConfigureStyleWithTitleLarge() {
        sut.configureItem(DataMock.UserTestimonialMock.withTextContainsOnlyEmojiAndSpaces)
        
        XCTAssertEqual(viewSpy.configureStyleCallsCount, 2)
        XCTAssertTrue(viewSpy.configureStyleReceivedInvocations.last is TitleLabelStyle)
        XCTAssertEqual(viewSpy.configureDescriptionCallsCount, 1)
    }
    
    func testConfigureItem_WhenNotContainsOnlyEmoji_ShouldCallConfigureStyleWithBodyPrimary() {
        sut.configureItem(DataMock.UserTestimonialMock.fullData)
        
        XCTAssertEqual(viewSpy.configureStyleCallsCount, 1)
        XCTAssertTrue(viewSpy.configureStyleReceivedInvocations.first is BodyPrimaryLabelStyle)
        XCTAssertEqual(viewSpy.configureDescriptionCallsCount, 1)
    }
    
    func testUpdateBackgroundColorIfNeeded_WhenHasActionIsTrue_ShouldCallConfigureBackgroundColorWhenExistsAction() {
        sut.updateBackgroundColorIfNeeded(true)
        
        XCTAssertEqual(viewSpy.configureBackgroundColorWhenExistsActionCallsCount, 1)
    }
    
    func testUpdateBackgroundColorIfNeeded_WhenHasActionIsFalse_ShouldCallConfigureBackgroundColorWhenNotExistsAction() {
        sut.updateBackgroundColorIfNeeded(false)
        
        XCTAssertEqual(viewSpy.configureBackgroundColorWhenNotExistsActionCallsCount, 1)
    }
}
