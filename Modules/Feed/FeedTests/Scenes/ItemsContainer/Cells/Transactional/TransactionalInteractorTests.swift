import AnalyticsModule
import Core
import XCTest
@testable import Feed

private class TransactionalPresentingSpy: TransactionalPresenting {
    var view: TransactionalDisplaying?

    // MARK: - CreateActionLogView
    private(set) var createActionLogViewFromHasActionDelegateCallsCount = 0
    private(set) var createActionLogViewFromHasActionDelegateReceivedInvocations: [(actionLog: ActionLog, hasAction: Bool, delegate: ActionLogDelegate?)] = []

    func createActionLogView(from actionLog: ActionLog, hasAction: Bool, delegate: ActionLogDelegate?) {
        createActionLogViewFromHasActionDelegateCallsCount += 1
        createActionLogViewFromHasActionDelegateReceivedInvocations.append((actionLog: actionLog, hasAction: hasAction, delegate: delegate))
    }

    // MARK: - CreateUserTestimonialView
    private(set) var createUserTestimonialViewFromHasActionCallsCount = 0
    private(set) var createUserTestimonialViewFromHasActionReceivedInvocations: [(userTestimonial: UserTestimonial, hasAction: Bool)] = []

    func createUserTestimonialView(from userTestimonial: UserTestimonial, hasAction: Bool) {
        createUserTestimonialViewFromHasActionCallsCount += 1
        createUserTestimonialViewFromHasActionReceivedInvocations.append((userTestimonial: userTestimonial, hasAction: hasAction))
    }

    // MARK: - CreateAdditionalInfoView
    private(set) var createAdditionalInfoViewFromHasActionActionableDelegateCallsCount = 0
    private(set) var createAdditionalInfoViewFromHasActionActionableDelegateReceivedInvocations: [(additionalInfo: AdditionalInfo, hasAction: Bool, actionable: Actionable?, delegate: AdditionalInfoDelegate?)] = []

    func createAdditionalInfoView(from additionalInfo: AdditionalInfo, hasAction: Bool, actionable: Actionable?, delegate: AdditionalInfoDelegate?) {
        createAdditionalInfoViewFromHasActionActionableDelegateCallsCount += 1
        createAdditionalInfoViewFromHasActionActionableDelegateReceivedInvocations.append((additionalInfo: additionalInfo, hasAction: hasAction, actionable: actionable, delegate: delegate))
    }

    // MARK: - CreatePostDetailView
    private(set) var createPostDetailViewFromHasActionCallsCount = 0
    private(set) var createPostDetailViewFromHasActionReceivedInvocations: [(postDetail: PostDetail, hasAction: Bool)] = []

    func createPostDetailView(from postDetail: PostDetail, hasAction: Bool) {
        createPostDetailViewFromHasActionCallsCount += 1
        createPostDetailViewFromHasActionReceivedInvocations.append((postDetail: postDetail, hasAction: hasAction))
    }

    // MARK: - CreateSocialActionView
    private(set) var createSocialActionViewFromHasActionDelegateCallsCount = 0
    private(set) var createSocialActionViewFromHasActionDelegateReceivedInvocations: [(socialAction: SocialAction, hasAction: Bool, delegate: SocialActionDelegate?)] = []

    func createSocialActionView(from socialAction: SocialAction, hasAction: Bool, delegate: SocialActionDelegate?) {
        createSocialActionViewFromHasActionDelegateCallsCount += 1
        createSocialActionViewFromHasActionDelegateReceivedInvocations.append((socialAction: socialAction, hasAction: hasAction, delegate: delegate))
    }

    // MARK: - ConfigureBackgroundColorWhenNotExistsAction
    private(set) var configureBackgroundColorWhenNotExistsActionCallsCount = 0

    func configureBackgroundColorWhenNotExistsAction() {
        configureBackgroundColorWhenNotExistsActionCallsCount += 1
    }

    // MARK: - ConfigureBackgroundColorWhenExistsAction
    private(set) var configureBackgroundColorWhenExistsActionCallsCount = 0

    func configureBackgroundColorWhenExistsAction() {
        configureBackgroundColorWhenExistsActionCallsCount += 1
    }

    // MARK: - ConfigureAccessibility
    private(set) var configureAccessibilityActionLogUserTestimonialAdditionalInfoPostDetailCallsCount = 0
    private(set) var configureAccessibilityActionLogUserTestimonialAdditionalInfoPostDetailReceivedInvocations: [(actionLog: ActionLog?, userTestimonial: UserTestimonial?, additionalInfo: AdditionalInfo?, postDetail: PostDetail?)] = []

    func configureAccessibility(actionLog: ActionLog?, userTestimonial: UserTestimonial?, additionalInfo: AdditionalInfo?, postDetail: PostDetail?) {
        configureAccessibilityActionLogUserTestimonialAdditionalInfoPostDetailCallsCount += 1
        configureAccessibilityActionLogUserTestimonialAdditionalInfoPostDetailReceivedInvocations.append((actionLog: actionLog, userTestimonial: userTestimonial, additionalInfo: additionalInfo, postDetail: postDetail))
    }
}

private class ActionableSpy: Actionable {
    // MARK: - Deeplink
    private(set) var deeplinkCallsCount = 0
    private(set) var deeplinkReceivedInvocations: [URL] = []

    func deeplink(_ url: URL) {
        deeplinkCallsCount += 1
        deeplinkReceivedInvocations.append(url)
    }

    // MARK: - Screen
    private(set) var screenForCallsCount = 0
    private(set) var screenForReceivedInvocations: [ActionScreenType] = []

    func screen(for type: ActionScreenType) {
        screenForCallsCount += 1
        screenForReceivedInvocations.append(type)
    }
}

private class TransactionalServicingMock: TransactionalServicing {
    // MARK: - LikeOrUnlike
    private(set) var likeOrUnlikeCardIdIsLikeCompletionCallsCount = 0
    private(set) var likeOrUnlikeCardIdIsLikeCompletionReceivedInvocations: [(cardId: String, isLike: Bool, completion: (Result<NoContent, ApiError>) -> Void)] = []
    var likeOrUnlikeCardIdIsLikeCompletionClosure: ((String, Bool, @escaping (Result<NoContent, ApiError>) -> Void) -> Void)?

    func likeOrUnlike(cardId: String, isLike: Bool, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        likeOrUnlikeCardIdIsLikeCompletionCallsCount += 1
        likeOrUnlikeCardIdIsLikeCompletionReceivedInvocations.append((cardId: cardId, isLike: isLike, completion: completion))
        likeOrUnlikeCardIdIsLikeCompletionClosure?(cardId, isLike, completion)
    }
}

private class UISocialActionUpdatingSpy: UISocialActionUpdating {

    // MARK: - UpdateLiked
    private(set) var updateLikedNewNumberOfLikesIsLikeCallsCount = 0
    private(set) var updateLikedNewNumberOfLikesIsLikeReceivedInvocations: [(newNumberOfLikes: Int, isLike: Bool)] = []

    func updateLiked(newNumberOfLikes: Int, isLike: Bool) {
        updateLikedNewNumberOfLikesIsLikeCallsCount += 1
        updateLikedNewNumberOfLikesIsLikeReceivedInvocations.append((newNumberOfLikes: newNumberOfLikes, isLike: isLike))
    }
}

final class TransactionalInteractorTests: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    private let presenterSpy = TransactionalPresentingSpy()
    private let actionableSpy = ActionableSpy()
    private let itemMock = DataMock.ItemMock.withDeeplinkAction
    private let serviceMock = TransactionalServicingMock()
    private let socialActionUpdatingSpy = UISocialActionUpdatingSpy()
    private lazy var depedenciesMock = DependencyContainerMock(analyticsSpy)
    private let mockDeeplinkURL = "https://app.picpay.com/"
    private lazy var sut = makeSut(item: itemMock)
    
    func testSetupView_ShouldConfigureTransactionalCard() {
        sut.setupView()
    
        XCTAssertEqual(presenterSpy.createActionLogViewFromHasActionDelegateCallsCount, 1)
        XCTAssertEqual(presenterSpy.createUserTestimonialViewFromHasActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.createAdditionalInfoViewFromHasActionActionableDelegateCallsCount, 1)
        XCTAssertEqual(presenterSpy.createPostDetailViewFromHasActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.createSocialActionViewFromHasActionDelegateCallsCount, 1)
    }
    
    func testAction_WhenItemNotActionContainer_ShouldNotExistsAction() {
        sut = makeSut(item: DataMock.ItemMock.fullData)
        
        sut.action()
        
        XCTAssertTrue(analyticsSpy.events.isEmpty)
        XCTAssertEqual(actionableSpy.deeplinkCallsCount, 0)
        XCTAssertEqual(actionableSpy.screenForCallsCount, 0)
    }
    
    func testAction_WhenItemActionIsDeeplink_ShouldCallActionableDeeplinkAndAnalytics() {
        sut.action()
        
        let expectedEvent = FeedAnalytics.cardTapped(
            cardInfo: .init(
                id: itemMock.id.uuidString,
                position: 0,
                itemType: .transactional,
                deeplink: mockDeeplinkURL
            )
        ).event()
        
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(actionableSpy.deeplinkCallsCount, 1)
    }
    
    func testAction_WhenItemActionIsScreen_ShouldCallActionableScreenAndAnalytics() {
        let itemWithScreenActionMock = DataMock.ItemMock.withScreenAction
        sut = makeSut(item: itemWithScreenActionMock)
        
        sut.action()
        
        let expectedEvent = FeedAnalytics.cardTapped(
            cardInfo: .init(
                id: itemWithScreenActionMock.id.uuidString,
                position: 0,
                itemType: .transactional
            )
        ).event()
        
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(actionableSpy.screenForCallsCount, 1)
    }
    
    func testUpdateBackgroundColorIfNeeded_WhenItemNotActionContainer_ShouldCallConfigureBackgroundColorContainsAction() {
        sut.updateBackgroundColorIfNeeded()
        
        XCTAssertEqual(presenterSpy.configureBackgroundColorWhenExistsActionCallsCount, 1)
    }
    
    func testUpdateBackgroundColorIfNeeded_WhenItemContainsActionContainer_ShouldCallConfigureBackgroundColorNotContainsAction() {
        sut = makeSut(item: DataMock.ItemMock.fullData)
        
        sut.updateBackgroundColorIfNeeded()
        
        XCTAssertEqual(presenterSpy.configureBackgroundColorWhenNotExistsActionCallsCount, 1)
    }
    
    func testConfigureAccessibility_ShouldCallConfigureAccessibilityItems() {
        sut.configureAccessibility()
        
        XCTAssertEqual(presenterSpy.configureAccessibilityActionLogUserTestimonialAdditionalInfoPostDetailCallsCount, 1)
    }
    
    func testUserLikedCard_WhenIsLike_ShouldSendLikeEvent() {
        let mockNumberOfLines = 10
        serviceMock.likeOrUnlikeCardIdIsLikeCompletionClosure = { _, _, completion in
            completion(.success(NoContent()))
        }
        sut.didTouchLiked(numberOfLikes: mockNumberOfLines, isLike: true, uiSocialActionUpdate: socialActionUpdatingSpy)
        
        let expectedEvent = FeedAnalytics.cardLikeInteracted(
            interactionType: .like,
            cardInfo: .init(
                id: itemMock.id.uuidString,
                position: 0,
                itemType: .transactional
            )
        ).event()
        
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }
    
    func testUserLikedCard_WhenIsDislike_ShouldSendLikeEvent() {
        let mockNumberOfLines = 10
        serviceMock.likeOrUnlikeCardIdIsLikeCompletionClosure = { _, _, completion in
            completion(.success(NoContent()))
        }
        sut.didTouchLiked(numberOfLikes: mockNumberOfLines, isLike: false, uiSocialActionUpdate: socialActionUpdatingSpy)
        
        let expectedEvent = FeedAnalytics.cardLikeInteracted(
            interactionType: .dislike,
            cardInfo: .init(
                id: itemMock.id.uuidString,
                position: 0,
                itemType: .transactional
            )
        ).event()
        
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }
    
    func testDidTouchPrimaryImage_ShouldCallActionable() throws {
        let mockURL = try XCTUnwrap(URL(string: mockDeeplinkURL))
        sut.didTouchPrimaryImage(for: mockURL)
        
        XCTAssertEqual(actionableSpy.deeplinkReceivedInvocations, [mockURL])
    }
    
    func testDidTouchSecondaryImage_ShouldCallActionable() throws {
        let mockURL = try XCTUnwrap(URL(string: mockDeeplinkURL))
        sut.didTouchSecondaryImage(for: mockURL)
        
        XCTAssertEqual(actionableSpy.deeplinkReceivedInvocations, [mockURL])
    }
    
    func testDidTouchPrimaryAction_ShouldCallActionableAndTracking() throws {
        let mockURL = try XCTUnwrap(URL(string: mockDeeplinkURL))
        let mockButtonName = "buttonName"
        sut.didTouchPrimaryAction(for: mockButtonName, deeplinkUrl: mockURL)
        
        let expectedEvent = FeedAnalytics.cardButtonTapped(
            buttonInfo: FeedAnalytics.ButtonInfo(name: mockButtonName, hyperlink: mockDeeplinkURL),
            cardInfo: FeedAnalytics.CardInfo(id: itemMock.id.uuidString, position: 0, itemType: .transactional)
        ).event()
        
        XCTAssertEqual(actionableSpy.deeplinkReceivedInvocations, [mockURL])
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }
    
    func testDidTouchSecondaryAction_ShouldCallActionableAndTracking() throws {
        let mockURL = try XCTUnwrap(URL(string: mockDeeplinkURL))
        let mockButtonName = "buttonName"
        sut.didTouchSecondaryAction(for: mockButtonName, deeplinkUrl: mockURL)
        
        let expectedEvent = FeedAnalytics.cardButtonTapped(
            buttonInfo: FeedAnalytics.ButtonInfo(name: mockButtonName, hyperlink: mockDeeplinkURL),
            cardInfo: FeedAnalytics.CardInfo(id: itemMock.id.uuidString, position: 0, itemType: .transactional)
        ).event()
        
        XCTAssertEqual(actionableSpy.deeplinkReceivedInvocations, [mockURL])
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }
    
    func testDidTouchContainerAction_ShouldCallActionableAndTracking() throws {
        let mockURL = try XCTUnwrap(URL(string: mockDeeplinkURL))
        let mockButtonName = "buttonName"
        sut.didTouchContainerAction(for: mockButtonName, deeplinkUrl: mockURL)
        
        let expectedEvent = FeedAnalytics.cardButtonTapped(
            buttonInfo: FeedAnalytics.ButtonInfo(name: mockButtonName, hyperlink: mockDeeplinkURL),
            cardInfo: FeedAnalytics.CardInfo(id: itemMock.id.uuidString, position: 0, itemType: .transactional)
        ).event()
        
        XCTAssertEqual(actionableSpy.deeplinkReceivedInvocations, [mockURL])
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }
    
    func testDidSelectText_ShouldCallActionable() throws {
        let selectedText = "textPart"
        let mockURL = try XCTUnwrap(URL(string: mockDeeplinkURL))
        sut.didSelectText(selectedText, deeplinkUrl: mockURL)
        
        XCTAssertEqual(actionableSpy.deeplinkReceivedInvocations, [mockURL])
    }
    
    func testDidTouchCommented_ShouldCallActionableWithScreen() {
        sut.didTouchCommented()
        
        XCTAssertEqual(actionableSpy.screenForReceivedInvocations, [.transactionDetail(id: itemMock.id)])
    }

    private func makeSut(item: Item, index: Int = 0) -> TransactionalInteractor {
        TransactionalInteractor(
            item: item,
            cardPosition: index,
            presenter: presenterSpy,
            service: serviceMock,
            dependencies: depedenciesMock,
            actionable: actionableSpy
        )
    }
}
