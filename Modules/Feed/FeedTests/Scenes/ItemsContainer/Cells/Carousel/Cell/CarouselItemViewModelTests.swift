import XCTest
@testable import Feed

private class CarouselItemViewModelOutputsSpy: CarouselItemViewModelOutputs {
    //MARK: - setup
    private(set) var setupImageUrlTextCallsCount = 0
    private(set) var setupImageUrlTextReceivedInvocations: [(imageUrl: URL, text: String)] = []

    func setup(imageUrl: URL, text: String) {
        setupImageUrlTextCallsCount += 1
        setupImageUrlTextReceivedInvocations.append((imageUrl: imageUrl, text: text))
    }
}

final class CarouselItemViewModelTests: XCTestCase {
    private let outputsSpy = CarouselItemViewModelOutputsSpy()
    private lazy var sut = CarouselItemViewModel(item: DataMock.BannerMock.withDeeplinkAction)
    
    func testConfigure_ShouldCallConfigure() {
        sut.inputs.setupOutput(outputsSpy)
        sut.inputs.configure()
        
        XCTAssertEqual(outputsSpy.setupImageUrlTextCallsCount, 1)
        XCTAssertEqual(outputsSpy.setupImageUrlTextReceivedInvocations.first?.imageUrl, DataMock.BannerMock.withDeeplinkAction.imageURL)
        XCTAssertEqual(outputsSpy.setupImageUrlTextReceivedInvocations.first?.text, DataMock.BannerMock.withDeeplinkAction.text)
    }
    
    func testSetupOutput_ShouldOutputSetter() {
        sut.inputs.setupOutput(outputsSpy)
        
        XCTAssertNotNil(sut.outputs)
    }
}
