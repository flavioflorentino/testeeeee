import XCTest
import AnalyticsModule
@testable import Feed

private class CarouselViewModelOutputsSpy: CarouselViewModelOutputs {
    //MARK: - setupListItems
    private(set) var setupListItemsCallsCount = 0
    private(set) var setupListItemsReceivedInvocations: [[CarouselItemViewModeling]] = []

    func setupListItems(_ viewModels: [CarouselItemViewModeling]) {
        setupListItemsCallsCount += 1
        setupListItemsReceivedInvocations.append(viewModels)
    }
}

private class ActionableSpy: Actionable {
    // MARK: - Deeplink
    private(set) var deeplinkCallsCount = 0
    private(set) var deeplinkReceivedInvocations: [URL] = []

    func deeplink(_ url: URL) {
        deeplinkCallsCount += 1
        deeplinkReceivedInvocations.append(url)
    }

    // MARK: - Screen
    private(set) var screenForCallsCount = 0
    private(set) var screenForReceivedInvocations: [ActionScreenType] = []

    func screen(for type: ActionScreenType) {
        screenForCallsCount += 1
        screenForReceivedInvocations.append(type)
    }
}

final class CarouselViewModelTests: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    private let actionableSpy = ActionableSpy()
    private let outputsSpy = CarouselViewModelOutputsSpy()
    private let cardIdMock = UUID()
    private lazy var dependenciesMock = DependencyContainerMock(analyticsSpy)
    
    private lazy var sut = makeSut(banners: DataMock.BannerMock.withDeeplinkAction, DataMock.BannerMock.withScreenAction)
    
    func testSetupView_ShouldCallSetupListItems() {
        sut.inputs.setupOutputs(outputsSpy)
        sut.setupView()
        
        XCTAssertEqual(outputsSpy.setupListItemsCallsCount, 1)
        XCTAssertEqual(outputsSpy.setupListItemsReceivedInvocations.first?.count, 2)
    }
    
    func testSetupOutputs_ShouldOutputSetter() {
        sut.inputs.setupOutputs(outputsSpy)
        
        XCTAssertNotNil(sut.outputs)
    }
    
    func testDidSelectedItem_WhenBannerActionIsDeeplink_ShouldCallActionableDeeplinkAndDelegate() {
        let mockBanner = DataMock.BannerMock.withDeeplinkAction
        let mockItemPosition = 1
        
        sut = makeSut(banners: mockBanner, itemPosition: mockItemPosition)
        
        sut.inputs.didSelectItem(from: 0)
        
        let expectedEvent = FeedAnalytics.cardTapped(
            cardInfo: .init(
                id: cardIdMock.uuidString,
                position: mockItemPosition,
                itemType: .carousel,
                title: mockBanner.text,
                message: mockBanner.description,
                deeplink: "https://app.picpay.com/",
                carousel: .init(id: mockBanner.id.uuidString, position: 0)
            )
        ).event()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(actionableSpy.deeplinkCallsCount, 1)
        XCTAssertEqual(actionableSpy.screenForCallsCount, 0)
    }
    
    func testDidSelectedItem_WhenBannerActionIsScreen_ShouldNotCallActionableScreenAndDeeplink() {
        sut.inputs.didSelectItem(from: 1)
        
        XCTAssertEqual(actionableSpy.deeplinkCallsCount, 0)
        XCTAssertEqual(actionableSpy.screenForCallsCount, 0)
    }
    
    func testDidSelectItem_WhenBannersDontContaintsIndex_ShouldNotCallActionableAndAnalytics() {
        sut.inputs.didSelectItem(from: 3)
        
        XCTAssertEqual(actionableSpy.deeplinkCallsCount, 0)
        XCTAssertEqual(actionableSpy.screenForCallsCount, 0)
        XCTAssertEqual(analyticsSpy.logCalledCount, 0)
    }
    
    func testWillDisplayCell_ShouldCallDelegate() {
        let mockItemPosition = 1
        let mockItem = DataMock.BannerMock.withDeeplinkAction
        
        sut = makeSut(banners: mockItem, itemPosition: mockItemPosition)
        sut.willDisplayCell(at: 0)
        
        let expectedEvent = FeedAnalytics.cardViewed(
            cardInfo: .init(
                id: cardIdMock.uuidString,
                position: mockItemPosition,
                itemType: .carousel,
                carousel: .init(id: mockItem.id.uuidString, position: 0)
            )
        ).event()

        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }
    
    private func makeSut(banners: Banner..., itemPosition: Int = 0) -> CarouselViewModel {
        CarouselViewModel(
            cardId: cardIdMock,
            banners: banners,
            cardPosition: itemPosition,
            dependencies: dependenciesMock,
            actionable: actionableSpy
        )
    }
}
