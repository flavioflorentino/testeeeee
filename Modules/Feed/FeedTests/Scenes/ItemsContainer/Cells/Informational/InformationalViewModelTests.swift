import XCTest
import AnalyticsModule
@testable import Feed

private class InformationalViewModelOutputsSpy: InformationalViewModelOutputs {
    //MARK: - configureView
    private(set) var configureViewDescriptionButtonTitleImageUrlCallsCount = 0
    private(set) var configureViewDescriptionButtonTitleImageUrlReceivedInvocations: [(title: String, description: String?, buttonTitle: String?, imageUrl: URL)] = []

    func configureView(_ title: String, description: String?, buttonTitle: String?, imageUrl: URL) {
        configureViewDescriptionButtonTitleImageUrlCallsCount += 1
        configureViewDescriptionButtonTitleImageUrlReceivedInvocations.append((title: title, description: description, buttonTitle: buttonTitle, imageUrl: imageUrl))
    }

    //MARK: - updateStylesIfNeeded
    private(set) var updateStylesIfNeededCallsCount = 0

    func updateStylesIfNeeded() {
        updateStylesIfNeededCallsCount += 1
    }
}

private class ActionableSpy: Actionable {
    // MARK: - Deeplink
    private(set) var deeplinkCallsCount = 0
    private(set) var deeplinkReceivedInvocations: [URL] = []

    func deeplink(_ url: URL) {
        deeplinkCallsCount += 1
        deeplinkReceivedInvocations.append(url)
    }

    // MARK: - Screen
    private(set) var screenForCallsCount = 0
    private(set) var screenForReceivedInvocations: [ActionScreenType] = []

    func screen(for type: ActionScreenType) {
        screenForCallsCount += 1
        screenForReceivedInvocations.append(type)
    }
}

final class InformationalViewModelTests: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    private let actionableSpy = ActionableSpy()
    private let outputsSpy = InformationalViewModelOutputsSpy()
    private let cardIdMock = UUID()
    private let bannerMock = DataMock.BannerMock.withDeeplinkAction
    private lazy var dependenciesMock = DependencyContainerMock(analyticsSpy)
    private lazy var sut = makeSut(banner: bannerMock)
    
    func testSetupView_ShouldCallConfigureView() throws {
        sut.inputs.setupOutputs(outputsSpy)
        sut.inputs.setupView()
        
        let outputInvocation = try XCTUnwrap(outputsSpy.configureViewDescriptionButtonTitleImageUrlReceivedInvocations.first)
        
        XCTAssertEqual(outputsSpy.configureViewDescriptionButtonTitleImageUrlCallsCount, 1)
        XCTAssertEqual(outputInvocation.title, DataMock.BannerMock.withDeeplinkAction.text)
        XCTAssertEqual(outputInvocation.description, DataMock.BannerMock.withDeeplinkAction.description)
        XCTAssertEqual(outputInvocation.buttonTitle, DataMock.BannerMock.withDeeplinkAction.primaryAction?.text)
        XCTAssertEqual(outputInvocation.imageUrl, DataMock.BannerMock.withDeeplinkAction.imageURL)
    }
    
    func testSetupOutputs_ShouldOutputSetter() {
        sut.inputs.setupOutputs(outputsSpy)
        
        XCTAssertNotNil(sut.outputs)
    }
    
    func testAction_WhenBannerIsDeeplink_ShouldCallActionableDeeplinkAndAnalaytics() {
        sut.inputs.action()
        
        let expectedEvent = FeedAnalytics.cardTapped(
            cardInfo: .init(
                id: cardIdMock.uuidString,
                position: 0,
                itemType: .informational,
                title: bannerMock.text,
                message: bannerMock.description,
                deeplink: "https://app.picpay.com/",
                informational: .init(id: bannerMock.id.uuidString)
            )
        ).event()
        
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(actionableSpy.deeplinkCallsCount, 1)
        XCTAssertEqual(actionableSpy.screenForCallsCount, 0)
    }
    
    func testAction_WhenBannerIsScreen_ShouldNotCallActionableAndAnalytics() {
        sut = makeSut(banner: DataMock.BannerMock.withScreenAction)
        
        sut.inputs.action()
        
        XCTAssertNil(analyticsSpy.event)
        XCTAssertEqual(actionableSpy.deeplinkCallsCount, 0)
        XCTAssertEqual(actionableSpy.screenForCallsCount, 0)
    }
    
    func testPrimaryAction_WhenBannerIsDeeplink_ShouldCallActionableDeeplinkAndAnalytics() {
        sut.inputs.primaryAction()
        
        let expectedEvent = FeedAnalytics.cardButtonTapped(
            buttonInfo: .init(
                name: bannerMock.primaryAction?.text ?? String(),
                hyperlink: "https://app.picpay.com/"
            ),
            cardInfo: .init(
                id: cardIdMock.uuidString,
                position: 0,
                itemType: .informational
            )
        ).event()
        
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(actionableSpy.deeplinkCallsCount, 1)
        XCTAssertEqual(actionableSpy.screenForCallsCount, 0)
    }
    
    func testPrimaryAction_WhenBannerIsScreen_ShouldNotCallActionableAndAnalytics() {
        sut = makeSut(banner: DataMock.BannerMock.withScreenAction)
        sut.inputs.primaryAction()

        XCTAssertEqual(analyticsSpy.logCalledCount, 0)
        XCTAssertEqual(actionableSpy.deeplinkCallsCount, 0)
        XCTAssertEqual(actionableSpy.screenForCallsCount, 0)
    }
    
    func testPrimaryAction_WhenPrimaryActionIsNil_ShouldNotCallActionableAndAnalytics() {
        sut = makeSut(banner: DataMock.BannerMock.withoutPrimaryAction)
        sut.inputs.primaryAction()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 0)
        XCTAssertEqual(actionableSpy.deeplinkCallsCount, 0)
        XCTAssertEqual(actionableSpy.screenForCallsCount, 0)
    }
    
    func testUpdateStylesIfNeeded_ShouldCallUpdateStylesIfNeeded() {
        sut.inputs.setupOutputs(outputsSpy)
        sut.inputs.updateStylesIfNeeded()
        
        XCTAssertEqual(outputsSpy.updateStylesIfNeededCallsCount, 1)
    }
    
    private func makeSut(banner: Banner, itemPosition: Int = 0) -> InformationalViewModel {
        InformationalViewModel(
            cardId: cardIdMock,
            banner: banner,
            cardPosition: itemPosition,
            dependencies: dependenciesMock,
            actionable: actionableSpy
        )
    }
}
