import XCTest
@testable import Feed

private class FeedbackStateConfigurableMock: FeedbackStateConfigurable {
    var image: UIImage? = UIImage()
    var title = "Title"
    var description = "Description"
    var buttonTitle: String? = "ButtonTitle"
}

private class FeedbackStateDisplayingSpy: FeedbackStateDisplaying {
    //MARK: - configureImage
    private(set) var configureImageCallsCount = 0
    private(set) var configureImageReceivedInvocations: [UIImage?] = []

    func configureImage(_ image: UIImage?) {
        configureImageCallsCount += 1
        configureImageReceivedInvocations.append(image)
    }

    //MARK: - configureTitle
    private(set) var configureTitleCallsCount = 0
    private(set) var configureTitleReceivedInvocations: [String] = []

    func configureTitle(_ title: String) {
        configureTitleCallsCount += 1
        configureTitleReceivedInvocations.append(title)
    }

    //MARK: - configureDescription
    private(set) var configureDescriptionCallsCount = 0
    private(set) var configureDescriptionReceivedInvocations: [String] = []

    func configureDescription(_ description: String) {
        configureDescriptionCallsCount += 1
        configureDescriptionReceivedInvocations.append(description)
    }

    //MARK: - configureButtonTitle
    private(set) var configureButtonTitleCallsCount = 0
    private(set) var configureButtonTitleReceivedInvocations: [String?] = []

    func configureButtonTitle(_ title: String?) {
        configureButtonTitleCallsCount += 1
        configureButtonTitleReceivedInvocations.append(title)
    }
}

final class FeedbackStatePresenterTests: XCTestCase {
    private let configurationMock = FeedbackStateConfigurableMock()
    private let viewSpy = FeedbackStateDisplayingSpy()
    
    private lazy var sut: FeedbackStatePresenter = {
        let presenter = FeedbackStatePresenter()
        presenter.view = viewSpy
        return presenter
    }()
    
    func testConfigureState_ShouldConfigureFeedback() {
        sut.configureState(configurationMock)
        
        XCTAssertEqual(viewSpy.configureImageCallsCount, 1)
        XCTAssertEqual(viewSpy.configureImageReceivedInvocations.first, configurationMock.image)
        XCTAssertEqual(viewSpy.configureTitleCallsCount, 1)
        XCTAssertEqual(viewSpy.configureTitleReceivedInvocations.first, configurationMock.title)
        XCTAssertEqual(viewSpy.configureDescriptionCallsCount, 1)
        XCTAssertEqual(viewSpy.configureDescriptionReceivedInvocations.first, configurationMock.description)
        XCTAssertEqual(viewSpy.configureButtonTitleCallsCount, 1)
        XCTAssertEqual(viewSpy.configureButtonTitleReceivedInvocations.first, configurationMock.buttonTitle)
    }
}
