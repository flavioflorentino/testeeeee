import AssetsKit
import XCTest
@testable import Feed

final class FeedbackEmptyConfigurationTests: XCTestCase {
    private typealias Localizable = Strings.FeedbackState.Empty
    
    private lazy var sut = FeedbackEmptyConfiguration.self
    
    func testThereIsNoActivityHereYet_ShouldConfigurationFeedbackEmpty() {
        XCTAssertEqual(sut.thereIsNoActivityHereYet.image, Resources.Illustrations.iluSocialNew.image)
        XCTAssertEqual(sut.thereIsNoActivityHereYet.title, Localizable.noActivity)
        XCTAssertEqual(sut.thereIsNoActivityHereYet.description, Localizable.thereIsNoActivityHereYet)
        XCTAssertNil(sut.thereIsNoActivityHereYet.buttonTitle)
    }
    
    func testThisProfileHasNoActivityYet_ShouldConfigurationFeedbackEmpty() {
        XCTAssertEqual(sut.thisProfileHasNoActivityYet.image, Resources.Illustrations.iluSocialNew.image)
        XCTAssertEqual(sut.thisProfileHasNoActivityYet.title, Localizable.noActivity)
        XCTAssertEqual(sut.thisProfileHasNoActivityYet.description, Localizable.thisProfileHasNoActivityYet)
        XCTAssertNil(sut.thisProfileHasNoActivityYet.buttonTitle)
    }
    
    func testPayMerchantsBillsTopUpYourCellPhoneAndMore_ShouldConfigurationFeedbackEmpty() {
        XCTAssertEqual(sut.payBillsAndRechargeCellPhone.image, Resources.Illustrations.iluSocialNew.image)
        XCTAssertEqual(sut.payBillsAndRechargeCellPhone.title, Localizable.noActivity)
        XCTAssertEqual(sut.payBillsAndRechargeCellPhone.description, Localizable.payBillsAndRechargeCellPhone)
        XCTAssertNil(sut.payBillsAndRechargeCellPhone.buttonTitle)
    }
}
