import AssetsKit
import XCTest
@testable import Feed

final class FeedbackErrorConfigurationTests: XCTestCase {
    private typealias Localizable = Strings.FeedbackState.Error
    
    private lazy var sut = FeedbackErrorConfiguration.self
    
    func testUnableToLoadActivities_ShouldConfigurationFeedbackError() {
        XCTAssertEqual(sut.unableToLoadActivities.image,  Resources.Illustrations.iluFalling.image)
        XCTAssertEqual(sut.unableToLoadActivities.title,  Localizable.UnableToLoadActivities.title)
        XCTAssertEqual(sut.unableToLoadActivities.description,  Localizable.UnableToLoadActivities.description)
        XCTAssertEqual(sut.unableToLoadActivities.buttonTitle,  Localizable.buttonTitle)
    }
    
    func testWithoutInternetConnection_ShouldConfigurationFeedbackError() {
        XCTAssertEqual(sut.withoutInternetConnection.image,  Resources.Illustrations.iluNoConnection.image)
        XCTAssertEqual(sut.withoutInternetConnection.title,  Localizable.WithoutInternetConnection.title)
        XCTAssertEqual(sut.withoutInternetConnection.description,  Localizable.WithoutInternetConnection.description)
        XCTAssertEqual(sut.withoutInternetConnection.buttonTitle,  Localizable.buttonTitle)
    }
}
