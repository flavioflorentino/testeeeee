import XCTest
@testable import Feed

private class FeedbackStateConfigurableMock: FeedbackStateConfigurable {
    var image: UIImage? = UIImage()
    var title = "Title"
    var description = "Description"
    var buttonTitle: String? = "ButtonTitle"
}

private class FeedbackStatePresentingSpy: FeedbackStatePresenting {
    var view: FeedbackStateDisplaying?

    //MARK: - configureState
    private(set) var configureStateCallsCount = 0
    private(set) var configureStateReceivedInvocations: [FeedbackStateConfigurable] = []

    func configureState(_ configuration: FeedbackStateConfigurable) {
        configureStateCallsCount += 1
        configureStateReceivedInvocations.append(configuration)
    }
}

private class FeedbackStateDelegateSpy: FeedbackStateDelegate {
    //MARK: - primaryAction
    private(set) var primaryActionFromCallsCount = 0
    private(set) var primaryActionFromReceivedInvocations: [FeedbackStateConfigurable] = []

    func primaryAction(from configuration: FeedbackStateConfigurable) {
        primaryActionFromCallsCount += 1
        primaryActionFromReceivedInvocations.append(configuration)
    }
}

final class FeedbackStateInteractorTests: XCTestCase {
    private let configurationMock = FeedbackStateConfigurableMock()
    private let presenterSpy = FeedbackStatePresentingSpy()
    private let delegateSpy = FeedbackStateDelegateSpy()
    
    private lazy var sut = FeedbackStateInteractor(
        configuration: configurationMock,
        presenter: presenterSpy,
        delegate: delegateSpy
    )
    
    func testLoadState_ShouldCallConfigureState() {
        sut.loadState()
        
        XCTAssertEqual(presenterSpy.configureStateCallsCount, 1)
        XCTAssertEqual(presenterSpy.configureStateReceivedInvocations.first?.image, configurationMock.image)
        XCTAssertEqual(presenterSpy.configureStateReceivedInvocations.first?.title, configurationMock.title)
        XCTAssertEqual(presenterSpy.configureStateReceivedInvocations.first?.description, configurationMock.description)
        XCTAssertEqual(presenterSpy.configureStateReceivedInvocations.first?.buttonTitle, configurationMock.buttonTitle)
    }
    
    func testHandleAction_ShouldCallPrimaryAction() {
        sut.handleAction()
        
        XCTAssertEqual(delegateSpy.primaryActionFromCallsCount, 1)
        XCTAssertEqual(delegateSpy.primaryActionFromReceivedInvocations.first?.image, configurationMock.image)
        XCTAssertEqual(delegateSpy.primaryActionFromReceivedInvocations.first?.title, configurationMock.title)
        XCTAssertEqual(delegateSpy.primaryActionFromReceivedInvocations.first?.description, configurationMock.description)
        XCTAssertEqual(delegateSpy.primaryActionFromReceivedInvocations.first?.buttonTitle, configurationMock.buttonTitle)
    }
}
