import Core
import AnalyticsModule
import XCTest
@testable import Feed

private class ItemsContainerServicingMock: ItemsContainerServicing {
    //MARK: - listItems
    private(set) var listItemsLastItemCallsCount = 0
    private(set) var listItemsLastItemReceivedInvocations: [(lastItem: Int?, completion: (Result<ItemData, ApiError>) -> Void)] = []
    var listItemsLastItemClosure: ((Int?, @escaping (Result<ItemData, ApiError>) -> Void) -> Void)?

    func listItems(lastItem: Int?, _ completion: @escaping (Result<ItemData, ApiError>) -> Void) {
        listItemsLastItemCallsCount += 1
        listItemsLastItemReceivedInvocations.append((lastItem: lastItem, completion: completion))
        listItemsLastItemClosure?(lastItem, completion)
    }
}

private class ItemsContainerPresentingSpy: ItemsContainerPresenting {
    var viewController: ItemsContainerDisplaying?

    // MARK: - InitialItems
    private(set) var initialItemsCallsCount = 0
    private(set) var initialItemsReceivedInvocations: [[Item]] = []

    func initialItems(_ items: [Item]) {
        initialItemsCallsCount += 1
        initialItemsReceivedInvocations.append(items)
    }

    // MARK: - AddItems
    private(set) var addItemsCallsCount = 0
    private(set) var addItemsReceivedInvocations: [[Item]] = []

    func addItems(_ items: [Item]) {
        addItemsCallsCount += 1
        addItemsReceivedInvocations.append(items)
    }

    // MARK: - ScrollsToTop
    private(set) var scrollsToTopCallsCount = 0

    func scrollsToTop() {
        scrollsToTopCallsCount += 1
    }

    // MARK: - Starting
    private(set) var startingLoadingCallsCount = 0
    private(set) var startingLoadingReceivedInvocations: [LoadingType] = []

    func starting(loading: LoadingType) {
        startingLoadingCallsCount += 1
        startingLoadingReceivedInvocations.append(loading)
    }

    // MARK: - Stoping
    private(set) var stopingLoadingCallsCount = 0
    private(set) var stopingLoadingReceivedInvocations: [LoadingType] = []

    func stoping(loading: LoadingType) {
        stopingLoadingCallsCount += 1
        stopingLoadingReceivedInvocations.append(loading)
    }

    // MARK: - EndRefreshing
    private(set) var endRefreshingCallsCount = 0

    func endRefreshing() {
        endRefreshingCallsCount += 1
    }

    // MARK: - ShowErrorMoreItems
    private(set) var showErrorMoreItemsCallsCount = 0

    func showErrorMoreItems() {
        showErrorMoreItemsCallsCount += 1
    }

    // MARK: - ShowNotMoreItemsExists
    private(set) var showNotMoreItemsExistsCallsCount = 0

    func showNotMoreItemsExists() {
        showNotMoreItemsExistsCallsCount += 1
    }

    // MARK: - ShowLoadingMoreItems
    private(set) var showLoadingMoreItemsCallsCount = 0

    func showLoadingMoreItems() {
        showLoadingMoreItemsCallsCount += 1
    }

    // MARK: - ShowEmptyState
    private(set) var showEmptyStateCallsCount = 0

    func showEmptyState() {
        showEmptyStateCallsCount += 1
    }

    // MARK: - ShowUnableToLoadActivities
    private(set) var showUnableToLoadActivitiesCallsCount = 0

    func showUnableToLoadActivities() {
        showUnableToLoadActivitiesCallsCount += 1
    }

    // MARK: - ShowWithoutInternetConnection
    private(set) var showWithoutInternetConnectionCallsCount = 0

    func showWithoutInternetConnection() {
        showWithoutInternetConnectionCallsCount += 1
    }
}

private class ActionableSpy: Actionable {
    // MARK: - Deeplink
    private(set) var deeplinkCallsCount = 0
    private(set) var deeplinkReceivedInvocations: [URL] = []

    func deeplink(_ url: URL) {
        deeplinkCallsCount += 1
        deeplinkReceivedInvocations.append(url)
    }

    // MARK: - Screen
    private(set) var screenForCallsCount = 0
    private(set) var screenForReceivedInvocations: [ActionScreenType] = []

    func screen(for type: ActionScreenType) {
        screenForCallsCount += 1
        screenForReceivedInvocations.append(type)
    }
}

private class FeedbackStateConfigurableMock: FeedbackStateConfigurable {
    var image: UIImage?
    
    private var underlyingTitle: String?
    var title: String {
        get { return underlyingTitle ?? String() }
        set(value) { underlyingTitle = value }
    }

    private var underlyingDescription: String?
    var description: String {
        get { return underlyingDescription ?? String() }
        set(value) { underlyingDescription = value }
    }

    var buttonTitle: String?
}

final class ItemsContainerInteractorTests: XCTestCase {
    private let presenterSpy = ItemsContainerPresentingSpy()
    private let serviceMock = ItemsContainerServicingMock()
    private let analyticsSpy = AnalyticsSpy()
    private lazy var containerMock = DependencyContainerMock(analyticsSpy)
    
    private lazy var sut = ItemsContainerInteractor(
        presenter: presenterSpy,
        service: serviceMock,
        dependencies: containerMock,
        isOpenFollowersAndFollowings: true
    )
    
    func testLoadData_WhenReceiveSuccessFromServiceAndReturnAnArrayItems_ShouldCallInitialItems() {
        serviceMock.listItemsLastItemClosure = { _, completion in
            completion(.success(ItemData(lastItem: 0000000000001, data: [DataMock.ItemMock.fullData, DataMock.ItemMock.fullData])))
        }
        
        sut.loadData()
        
        XCTAssertEqual(presenterSpy.startingLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.startingLoadingReceivedInvocations.first, .full)
        XCTAssertEqual(presenterSpy.stopingLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.stopingLoadingReceivedInvocations.first, .full)
        XCTAssertEqual(presenterSpy.initialItemsCallsCount, 1)
        XCTAssertEqual(presenterSpy.initialItemsReceivedInvocations.first?.count, 2)
    }
    
    func testLoadData_WhenReceiveSuccessFromServiceAndReturnAnEmptyArray_ShouldCallShowEmptyState() {
        serviceMock.listItemsLastItemClosure = { _, completion in
            completion(.success(ItemData(lastItem: 0000000000001, data: [])))
        }
        
        sut.loadData()
        
        XCTAssertEqual(presenterSpy.startingLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.startingLoadingReceivedInvocations.first, .full)
        XCTAssertEqual(presenterSpy.stopingLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.stopingLoadingReceivedInvocations.first, .full)
        XCTAssertEqual(presenterSpy.showEmptyStateCallsCount, 1)
    }
    
    func testLoadData_WhenReceiveFailureFromServiceWithAnyError_ShouldCallShowErrorStateUnableToLoadActivities() throws {
        serviceMock.listItemsLastItemClosure = { _, completion in
            completion(.failure(.serverError))
        }
        
        sut.loadData()
        
        XCTAssertEqual(presenterSpy.showUnableToLoadActivitiesCallsCount, 1)
    }
    
    func testLoadData_WhenReceiveFailureFromServiceWithConnectionFailureError_ShouldCallShowErrorStateWithoutInternetConnection() throws {
        serviceMock.listItemsLastItemClosure = { _, completion in
            completion(.failure(.connectionFailure))
        }
        
        sut.loadData()
        
        XCTAssertEqual(presenterSpy.showWithoutInternetConnectionCallsCount, 1)
    }
    
    func testRefreshData_WhenReceiveSuccessFromServiceAndReturnAnArrayItems_ShouldCallInitialItems() {
        serviceMock.listItemsLastItemClosure = { _, completion in
            completion(.success(ItemData(lastItem: 0000000000001, data: [DataMock.ItemMock.fullData, DataMock.ItemMock.fullData])))
        }
        
        sut.refreshData()
        
        XCTAssertEqual(presenterSpy.endRefreshingCallsCount, 1)
        XCTAssertEqual(presenterSpy.initialItemsCallsCount, 1)
        XCTAssertEqual(presenterSpy.initialItemsReceivedInvocations.first?.count, 2)
    }
    
    func testRefreshData_WhenReceiveSuccessFromServiceAndReturnEmptyArray_ShouldCallShowEmptyState() {
        serviceMock.listItemsLastItemClosure = { _, completion in
            completion(.success(ItemData(lastItem: 0000000000001, data: [])))
        }
        
        sut.refreshData()
        
        XCTAssertEqual(presenterSpy.endRefreshingCallsCount, 1)
        XCTAssertEqual(presenterSpy.showEmptyStateCallsCount, 1)
    }
    
    func testRefreshData_WhenReceiveFailureFromServiceWithAnyError_ShouldCallShowErrorStateUnableToLoadActivities() throws {
        serviceMock.listItemsLastItemClosure = { _, completion in
            completion(.failure(.serverError))
        }
        
        sut.refreshData()
        
        XCTAssertEqual(presenterSpy.showUnableToLoadActivitiesCallsCount, 1)
    }
    
    func testRefreshData_WhenReceiveFailureFromServiceWithConnectionFailureError_ShouldCallShowErrorStateWithoutInternetConnection() throws {
        serviceMock.listItemsLastItemClosure = { _, completion in
            completion(.failure(.connectionFailure))
        }
        
        sut.refreshData()
        
        XCTAssertEqual(presenterSpy.showWithoutInternetConnectionCallsCount, 1)
    }
    
    func testLoadMoreData_WhenPrefetchingDataNotEmpty_ShouldReturnAnArrayPrefetchItems() {
        serviceMock.listItemsLastItemClosure = { _, completion in
            completion(.success(ItemData(lastItem: 0000000000001, data: [DataMock.ItemMock.fullData, DataMock.ItemMock.fullData])))
        }
        
        sut.prefetchingDataIfNeeded(from: [IndexPath(row: 2, section: 0)])
        sut.loadMoreData()
        
        XCTAssertEqual(presenterSpy.addItemsCallsCount, 1)
        XCTAssertNotEqual(serviceMock.listItemsLastItemCallsCount, 2)
    }
    
    func testLoadMoreData_WhenIsNeededLoadDataEqualFalse_ShouldNotAddMoreItems() {
        serviceMock.listItemsLastItemClosure = { _, completion in
            completion(.failure(.connectionFailure))
        }
        
        sut.prefetchingDataIfNeeded(from: [IndexPath(row: 2, section: 0)])
        sut.loadMoreData()
        
        XCTAssertNotEqual(serviceMock.listItemsLastItemCallsCount, 2)
        XCTAssertEqual(presenterSpy.addItemsCallsCount, 0)
    }
    
    func testLoadMoreData_WhenReceiveSuccessFromServiceAndReturnAnArrayItems_ShouldCallAddItems() {
        serviceMock.listItemsLastItemClosure = { _, completion in
            completion(.success(ItemData(lastItem: 0000000000001, data: [DataMock.ItemMock.fullData, DataMock.ItemMock.fullData])))
        }
        
        sut.loadMoreData()
        
        XCTAssertEqual(presenterSpy.startingLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.startingLoadingReceivedInvocations.first, .footer)
        XCTAssertEqual(presenterSpy.stopingLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.stopingLoadingReceivedInvocations.first, .footer)
        XCTAssertEqual(presenterSpy.addItemsCallsCount, 1)
        XCTAssertEqual(presenterSpy.addItemsReceivedInvocations.first?.count, 2)
    }
    
    func testLoadMoreData_WhenReceiveSuccessFromServiceAndReturnEmptyArray_ShouldCallShowNotMoreItemsExists() {
        serviceMock.listItemsLastItemClosure = { _, completion in
            completion(.success(ItemData(lastItem: 0000000000001, data: [])))
        }
        
        sut.loadMoreData()
        
        XCTAssertEqual(presenterSpy.startingLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.startingLoadingReceivedInvocations.first, .footer)
        XCTAssertEqual(presenterSpy.stopingLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.stopingLoadingReceivedInvocations.first, .footer)
        XCTAssertEqual(presenterSpy.showNotMoreItemsExistsCallsCount, 1)
    }
    
    func testLoadMoreData_WhenReceiveFailureFromService_ShouldCallShowErrorMoreitems() {
        serviceMock.listItemsLastItemClosure = { _, completion in
            completion(.failure(.connectionFailure))
        }
        
        sut.loadMoreData()
        
        XCTAssertEqual(presenterSpy.startingLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.startingLoadingReceivedInvocations.first, .footer)
        XCTAssertEqual(presenterSpy.stopingLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.stopingLoadingReceivedInvocations.first, .footer)
        XCTAssertEqual(presenterSpy.showErrorMoreItemsCallsCount, 1)
    }
    
    func testPrefetchingDataIfNeeded_WhenIndexPathIsLessThanTotalItems_ShouldNotCallListItemsService() {
        serviceMock.listItemsLastItemClosure = { _, completion in
            completion(.success(ItemData(lastItem: 0000000000001, data: [DataMock.ItemMock.fullData, DataMock.ItemMock.fullData])))
        }
        
        sut.loadData()
        sut.prefetchingDataIfNeeded(from: [IndexPath(row: 0, section: 0)])
        
        XCTAssertNotEqual(serviceMock.listItemsLastItemCallsCount, 2)
    }
    
    func testPrefetchingDataIfNeeded_WhenReceiveSuccessFromServiceAndReturnAnArrayItems_ShouldMakePrefetchingItems() {
        serviceMock.listItemsLastItemClosure = { _, completion in
            completion(.success(ItemData(lastItem: 0000000000001, data: [DataMock.ItemMock.fullData, DataMock.ItemMock.fullData])))
        }
        
        sut.prefetchingDataIfNeeded(from: [IndexPath(row: 1, section: 0)])
        
        XCTAssertEqual(presenterSpy.startingLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.startingLoadingReceivedInvocations.first, .footer)
        XCTAssertEqual(presenterSpy.stopingLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.stopingLoadingReceivedInvocations.first, .footer)
        
        sut.loadMoreData()
        
        XCTAssertEqual(presenterSpy.addItemsCallsCount, 1)
    }
    
    func testPrefetchingDataIfNeeded_WhenReceiveSuccessFromServiceAndReturnEmptyArray_ShouldCallShowNotMoreItemsExists() {
        serviceMock.listItemsLastItemClosure = { _, completion in
            completion(.success(ItemData(lastItem: 0000000000001, data: [])))
        }
        
        sut.prefetchingDataIfNeeded(from: [IndexPath(row: 1, section: 0)])
        
        XCTAssertEqual(presenterSpy.startingLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.startingLoadingReceivedInvocations.first, .footer)
        XCTAssertEqual(presenterSpy.stopingLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.stopingLoadingReceivedInvocations.first, .footer)
        XCTAssertEqual(presenterSpy.showNotMoreItemsExistsCallsCount, 1)
    }
    
    func testPrefetchingDataIfNeeded_WhenReceiveFailureFromService_ShouldCallShowErrorMoreItems() {
        serviceMock.listItemsLastItemClosure = { _, completion in
            completion(.failure(.connectionFailure))
        }
        
        sut.prefetchingDataIfNeeded(from: [IndexPath(row: 1, section: 0)])
    
        XCTAssertEqual(presenterSpy.startingLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.startingLoadingReceivedInvocations.first, .footer)
        XCTAssertEqual(presenterSpy.stopingLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.stopingLoadingReceivedInvocations.first, .footer)
        XCTAssertEqual(presenterSpy.showErrorMoreItemsCallsCount, 1)
    }
    
    func testFooterAction_WhenFeedbackStateIsNil_ShouldNotCallAnyMethods() {
        serviceMock.listItemsLastItemClosure = { _, completion in
            completion(.success(ItemData(lastItem: 0000000000001, data: [])))
        }
        
        sut.footerAction(from: nil)
        
        XCTAssertEqual(presenterSpy.scrollsToTopCallsCount, 0)
        
        XCTAssertEqual(presenterSpy.startingLoadingCallsCount, 0)
        XCTAssertEqual(presenterSpy.stopingLoadingCallsCount, 0)
        XCTAssertEqual(serviceMock.listItemsLastItemCallsCount, 0)
        
        XCTAssertEqual(presenterSpy.showLoadingMoreItemsCallsCount, 0)
    }
    
    func testFooterAction_WhenFeedbackStateIsNoContent_ShouldCallScrollsToTop() {
        sut.footerAction(from: .noContent)
        
        XCTAssertEqual(presenterSpy.scrollsToTopCallsCount, 1)
    }
    
    func testFooterAction_WhenFeedbackStateIsError_ShouldCallListItems() {
        serviceMock.listItemsLastItemClosure = { _, completion in
            completion(.success(ItemData(lastItem: 0000000000001, data: [DataMock.ItemMock.fullData, DataMock.ItemMock.fullData])))
        }
        
        sut.footerAction(from: .error)
        
        XCTAssertEqual(presenterSpy.startingLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.startingLoadingReceivedInvocations.first, .footer)
        XCTAssertEqual(presenterSpy.stopingLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.stopingLoadingReceivedInvocations.first, .footer)
        XCTAssertEqual(serviceMock.listItemsLastItemCallsCount, 1)
    }
    
    func testFooterAction_WhenFeedbackStateIsLoading_ShouldCallShowLoadingMoreItems() {
        sut.footerAction(from: .loading)
        
        XCTAssertEqual(presenterSpy.showLoadingMoreItemsCallsCount, 1)
    }
    
    func testFeedbackAction_WhenFeedbackStateConfigurableIsFeedbackError_ShouldCallListItems() {
        serviceMock.listItemsLastItemClosure = { _, completion in
            completion(.success(ItemData(lastItem: 0000000000001, data: [DataMock.ItemMock.fullData, DataMock.ItemMock.fullData])))
        }
        
        sut.feedbackAction(from: FeedbackErrorConfiguration.withoutInternetConnection)
        
        XCTAssertEqual(presenterSpy.startingLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.startingLoadingReceivedInvocations.first, .full)
        XCTAssertEqual(presenterSpy.stopingLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.stopingLoadingReceivedInvocations.first, .full)
        XCTAssertEqual(serviceMock.listItemsLastItemCallsCount, 1)
    }
    
    func testWillDisplayCell_WhenCardTypeIsNotCarrousel_ShouldSendEvent() throws {
        let itemsMock = [DataMock.ItemMock.fullData, DataMock.ItemMock.fullData]

        serviceMock.listItemsLastItemClosure = { _, completion in
            completion(.success(.init(lastItem: 0, data: itemsMock)))
        }
        sut.loadData()
        sut.willDisplayCell(at: 1)
        
        let expectedEvent = FeedAnalytics.cardViewed(
            cardInfo: .init(
                id: itemsMock[1].id.uuidString,
                position: 1,
                itemType: .transactional
            )
        ).event()

        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }

    func testWillDisplayCell_WhenCardTypeIsCarrousel_ShouldNotSendEvent() throws {
        let itemMock = DataMock.ItemMock.noBannersCarrousel

        serviceMock.listItemsLastItemClosure = { _, completion in
            completion(.success(.init(lastItem: 0, data: [itemMock])))
        }
        sut.loadData()
        sut.willDisplayCell(at: 0)

        XCTAssertTrue(analyticsSpy.events.isEmpty)
    }
}
