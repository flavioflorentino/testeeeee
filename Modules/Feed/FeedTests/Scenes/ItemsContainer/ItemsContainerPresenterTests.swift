import FeatureFlag
import XCTest
@testable import Feed

private class ItemsContainerDisplayingSpy: ItemsContainerDisplaying {
    // MARK: - InitialItems
    private(set) var initialItemsCallsCount = 0
    private(set) var initialItemsReceivedInvocations: [[ItemsContainerDataSource]] = []

    func initialItems(_ items: [ItemsContainerDataSource]) {
        initialItemsCallsCount += 1
        initialItemsReceivedInvocations.append(items)
    }

    // MARK: - AddItems
    private(set) var addItemsCallsCount = 0
    private(set) var addItemsReceivedInvocations: [[ItemsContainerDataSource]] = []

    func addItems(_ items: [ItemsContainerDataSource]) {
        addItemsCallsCount += 1
        addItemsReceivedInvocations.append(items)
    }

    // MARK: - RestoreInitialState
    private(set) var restoreInitialStateCallsCount = 0

    func restoreInitialState() {
        restoreInitialStateCallsCount += 1
    }

    // MARK: - StartLoading
    private(set) var startLoadingCallsCount = 0

    func startLoading() {
        startLoadingCallsCount += 1
    }

    // MARK: - StartFooterLoading
    private(set) var startFooterLoadingCallsCount = 0

    func startFooterLoading() {
        startFooterLoadingCallsCount += 1
    }

    // MARK: - StopLoading
    private(set) var stopLoadingCallsCount = 0

    func stopLoading() {
        stopLoadingCallsCount += 1
    }

    // MARK: - StopFooterLoading
    private(set) var stopFooterLoadingCallsCount = 0

    func stopFooterLoading() {
        stopFooterLoadingCallsCount += 1
    }

    // MARK: - EndRefreshing
    private(set) var endRefreshingCallsCount = 0

    func endRefreshing() {
        endRefreshingCallsCount += 1
    }

    // MARK: - UpdateFeedbackFooter
    private(set) var updateFeedbackFooterForCallsCount = 0
    private(set) var updateFeedbackFooterForReceivedInvocations: [FeedbackState] = []

    func updateFeedbackFooter(for state: FeedbackState) {
        updateFeedbackFooterForCallsCount += 1
        updateFeedbackFooterForReceivedInvocations.append(state)
    }

    // MARK: - ShowEmptyState
    private(set) var showEmptyStateFromCallsCount = 0
    private(set) var showEmptyStateFromReceivedInvocations: [FeedbackStateConfigurable] = []

    func showEmptyState(from configuration: FeedbackStateConfigurable) {
        showEmptyStateFromCallsCount += 1
        showEmptyStateFromReceivedInvocations.append(configuration)
    }

    // MARK: - ShowErrorState
    private(set) var showErrorStateFromCallsCount = 0
    private(set) var showErrorStateFromReceivedInvocations: [FeedbackStateConfigurable] = []

    func showErrorState(from configuration: FeedbackStateConfigurable) {
        showErrorStateFromCallsCount += 1
        showErrorStateFromReceivedInvocations.append(configuration)
    }
}

private class NotificationCenterMock: NotificationCenter {
    // MARK: - post
    private(set) var postNameObjectCallsCount = 0
    private(set) var postNameObjectReceivedInvocations: [(name: NSNotification.Name, object: Any?)] = []
    
    override func post(name aName: NSNotification.Name, object anObject: Any?) {
        postNameObjectCallsCount += 1
        postNameObjectReceivedInvocations.append((name: aName, object: anObject))
    }
}

private class ActionableSpy: Actionable {
    // MARK: - Deeplink
    private(set) var deeplinkCallsCount = 0
    private(set) var deeplinkReceivedInvocations: [URL] = []

    func deeplink(_ url: URL) {
        deeplinkCallsCount += 1
        deeplinkReceivedInvocations.append(url)
    }
    
    // MARK: - Screen
    private(set) var screenForCallsCount = 0
    private(set) var screenForReceivedInvocations: [ActionScreenType] = []

    func screen(for type: ActionScreenType) {
        screenForCallsCount += 1
        screenForReceivedInvocations.append(type)
    }
}

private class FeedbackStateConfigurableMock: FeedbackStateConfigurable {
    var image: UIImage?
    
    private var underlyingTitle: String?
    
    var title: String {
        get { return underlyingTitle ?? String() }
        set { underlyingTitle = newValue }
    }

    private var underlyingDescription: String?
    
    var description: String {
        get { return underlyingDescription ?? String() }
        set { underlyingDescription = newValue }
    }

    var buttonTitle: String?
}

final class ItemsContainerPresenterTests: XCTestCase {
    private let notificationCenterMock = NotificationCenterMock()
    private let featureManagerMock = FeatureManagerMock()
    private let feedbackStateConfigurableMock = FeedbackStateConfigurableMock()
    private let actionableSpy = ActionableSpy()
    
    private let viewControllerSpy = ItemsContainerDisplayingSpy()
    
    private lazy var containerMock = DependencyContainerMock(notificationCenterMock, featureManagerMock)
    
    private lazy var sut: ItemsContainerPresenter = {
        let presenter = ItemsContainerPresenter(
            emptyStateConfiguration: feedbackStateConfigurableMock,
            dependencies: containerMock,
            actionable: actionableSpy
        )
        presenter.viewController = viewControllerSpy
        return presenter
    }()
        
    func testEndRefreshing_ShouldCallEndRefreshing() {
        sut.endRefreshing()
        
        XCTAssertEqual(viewControllerSpy.endRefreshingCallsCount, 1)
    }
    
    func testInitialItems_ShouldCallRestoreInitialStateAndInitialItems() {
        sut.initialItems(
            [
                DataMock.ItemMock.withDeeplinkAction,
                DataMock.ItemMock.withScreenAction
            ]
        )
        
        XCTAssertEqual(viewControllerSpy.restoreInitialStateCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.initialItemsCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.initialItemsReceivedInvocations.first?.count, 2)
    }
    
    func testAddItems_ShouldCallAddItems() {
        sut.addItems([DataMock.ItemMock.withScreenAction])
        
        XCTAssertEqual(viewControllerSpy.addItemsCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.addItemsReceivedInvocations.first?.count, 1)
    }
    
    func testScrollsToTop_ShouldCallNotificationCenterPost() {
        sut.scrollsToTop()
        
        XCTAssertEqual(notificationCenterMock.postNameObjectCallsCount, 1)
        XCTAssertEqual(notificationCenterMock.postNameObjectReceivedInvocations.first?.name, .homeScrollToTop)
        XCTAssertNil(notificationCenterMock.postNameObjectReceivedInvocations.first?.object)
    }
        
    func testStarting_WhenLoadingTypeIsFull_ShouldCallStopLoading() {
        sut.starting(loading: .full)
        
        XCTAssertEqual(viewControllerSpy.startLoadingCallsCount, 1)
    }
    
    func testStarting_WhenLoadingTypeIsFooter_ShouldCallStopFooterLoading() {
        sut.starting(loading: .footer)
        
        XCTAssertEqual(viewControllerSpy.startFooterLoadingCallsCount, 1)
    }
    
    func testStoping_WhenLoadingTypeIsFull_ShouldCallStopLoading() {
        sut.stoping(loading: .full)
        
        XCTAssertEqual(viewControllerSpy.stopLoadingCallsCount, 1)
    }
    
    func testStoping_WhenLoadingTypeIsFooter_ShouldCallStopFooterLoading() {
        sut.stoping(loading: .footer)
        
        XCTAssertEqual(viewControllerSpy.stopFooterLoadingCallsCount, 1)
    }
    
    func testShowNotMoreItemsExists_ShouldCallUpdateFeedbackFooterForNoContent() {
        sut.showNotMoreItemsExists()
        
        XCTAssertEqual(viewControllerSpy.updateFeedbackFooterForCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.updateFeedbackFooterForReceivedInvocations.first, .noContent)
    }
    
    func testShowErrorMoreItems_ShouldCallUpdateFeedbackFooterForError() {
        sut.showErrorMoreItems()
        
        XCTAssertEqual(viewControllerSpy.updateFeedbackFooterForCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.updateFeedbackFooterForReceivedInvocations.first, .error)
    }
    
    func testShowLoadingMoreItems_ShouldCallUpdateFeedbackFooterForLoading() {
        sut.showLoadingMoreItems()
        
        XCTAssertEqual(viewControllerSpy.updateFeedbackFooterForCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.updateFeedbackFooterForReceivedInvocations.first, .loading)
    }
    
    func testShowEmptyState_ShouldCallShowEmptyStateWithContextTitleAndDescription() {
        feedbackStateConfigurableMock.description = "Show Empty State"
        feedbackStateConfigurableMock.title = "Title Empty State"
        
        sut.showEmptyState()
        
        XCTAssertEqual(viewControllerSpy.showEmptyStateFromCallsCount, 1)
        XCTAssertNil(viewControllerSpy.showEmptyStateFromReceivedInvocations.first?.buttonTitle)
        XCTAssertEqual(viewControllerSpy.showEmptyStateFromReceivedInvocations.first?.title, feedbackStateConfigurableMock.title)
        XCTAssertEqual(viewControllerSpy.showEmptyStateFromReceivedInvocations.first?.description, feedbackStateConfigurableMock.description)
        XCTAssertNil(viewControllerSpy.showEmptyStateFromReceivedInvocations.first?.image)
    }
    
    func testShowUnableToLoadActivities_ShouldCallShowErrorState() {
        sut.showUnableToLoadActivities()
        
        XCTAssertEqual(viewControllerSpy.showErrorStateFromCallsCount, 1)
        XCTAssertTrue(
            try XCTUnwrap(viewControllerSpy.showErrorStateFromReceivedInvocations.first as? FeedbackErrorConfiguration) == .unableToLoadActivities
        )
    }
    
    func testShowWithoutInternetConnection_ShouldCallShowErrorState() throws {
        sut.showWithoutInternetConnection()
        
        XCTAssertEqual(viewControllerSpy.showErrorStateFromCallsCount, 1)
        XCTAssertTrue(
            try XCTUnwrap(viewControllerSpy.showErrorStateFromReceivedInvocations.first as? FeedbackErrorConfiguration) == .withoutInternetConnection
        )
    }
}
