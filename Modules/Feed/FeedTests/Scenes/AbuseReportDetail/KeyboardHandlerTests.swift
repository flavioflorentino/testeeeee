import XCTest
@testable import Feed

private struct NotificationItem: Equatable {
    let name: String?
    let observer: KeyboardHandler?
    
    static func == (lhs: NotificationItem, rhs: NotificationItem) -> Bool {
        return lhs.name == rhs.name && lhs.observer === rhs.observer
    }
}

private final class NotificationCenterMock: NotificationCenter {
    
    //MARK: - addObserver
    private(set) var addObserverSelectorNameObjectCallsCount = 0
    private(set) var addObserverSelectorNameObjectReceivedInvocations: [(observer: Any, aSelector: Selector, aName: NSNotification.Name?, anObject: Any?)] = []
    
    override func addObserver(_ observer: Any, selector aSelector: Selector, name aName: NSNotification.Name?, object anObject: Any?) {
        super.addObserver(observer, selector: aSelector, name: aName, object: anObject)
        addObserverSelectorNameObjectCallsCount += 1
        addObserverSelectorNameObjectReceivedInvocations.append((observer: observer, aSelector: aSelector, aName: aName, anObject: anObject))
    }
}

final class KeyboardHandlerTests: XCTestCase {
    private let notificationCenterMock = NotificationCenterMock()
    private lazy var sut = KeyboardHandler(notificationCenter: notificationCenterMock)
    
    func testHandleHeight_ShouldSubscribeToObserverCenter() throws {
        let expectedObservers = [
            NotificationItem(name: UIWindow.keyboardWillShowNotification.rawValue, observer: sut),
            NotificationItem(name: UIWindow.keyboardWillHideNotification.rawValue, observer: sut)
        ]
        
        let subscribedObservers: [NotificationItem] = try notificationCenterMock.addObserverSelectorNameObjectReceivedInvocations.map {
            let observerObject = try XCTUnwrap($0.observer as? KeyboardHandler)
            return NotificationItem(name: $0.aName?.rawValue, observer: observerObject)
        }

        XCTAssertEqual(notificationCenterMock.addObserverSelectorNameObjectCallsCount, 2)
        XCTAssertEqual(subscribedObservers, expectedObservers)
    }
    
    func testHandleHeight_KeyboardWillShow_ShouldCallCompletion() throws {
        let mockedKeyboardHeight: CGFloat = 200
        let heightChangedExpectation = expectation(description: "height changed")
        sut.onHeightChangeCompletion = { height in
            XCTAssertEqual(mockedKeyboardHeight, height)
            heightChangedExpectation.fulfill()
        }
        notificationCenterMock.post(name: UIWindow.keyboardWillShowNotification,
                                    object: nil,
                                    userInfo: mockKeyboardUserInfo(keyboardHeight: mockedKeyboardHeight))
        wait(for: [heightChangedExpectation], timeout: 1)
    }
    
    func testHandleHeight_KeyboardWillShowWithNilUserInfo_ShouldNotCallCompletion() {
        let heightChangedExpectation = expectation(description: "height changed")
        heightChangedExpectation.isInverted = true
        sut.onHeightChangeCompletion = { _ in
            heightChangedExpectation.fulfill()
        }
        notificationCenterMock.post(name: UIWindow.keyboardWillShowNotification, object: nil, userInfo: nil)
        wait(for: [heightChangedExpectation], timeout: 1)
    }
    
    func testHandleHeight_KeyboardWillShowWithWrongUserInfo_ShouldNotCallCompletion() {
        let mockedKeyboardHeight: CGFloat = 200
        let heightChangedExpectation = expectation(description: "height changed")
        heightChangedExpectation.isInverted = true
        sut.onHeightChangeCompletion = { _ in
            heightChangedExpectation.fulfill()
        }
        notificationCenterMock.post(name: UIWindow.keyboardWillShowNotification,
                                    object: nil,
                                    userInfo: mockKeyboardUserInfo(keyboardHeight: mockedKeyboardHeight, includeAnimationInfo: false))
        wait(for: [heightChangedExpectation], timeout: 1)
    }
    
    func testHandleHeight_KeyboardWillHide_ShouldCallCompletion() throws {
        let heightChangedExpectation = expectation(description: "height changed")
        sut.onHeightChangeCompletion = { height in
            XCTAssertEqual(height,.zero)
            heightChangedExpectation.fulfill()
        }
        notificationCenterMock.post(name: UIWindow.keyboardWillHideNotification, object: nil, userInfo: mockKeyboardUserInfo())
        wait(for: [heightChangedExpectation], timeout: 1)
    }
    
    func testHandleHeight_KeyboardWillHideWithNilUserInfo_ShouldNotCallCompletion() {
        let heightChangedExpectation = expectation(description: "height changed")
        heightChangedExpectation.isInverted = true
        sut.onHeightChangeCompletion = { _ in
            heightChangedExpectation.fulfill()
        }
        notificationCenterMock.post(name: UIWindow.keyboardWillHideNotification, object: nil, userInfo: nil)
        wait(for: [heightChangedExpectation], timeout: 1)
    }
    
    func testHandleHeight_KeyboardWillHideWithWrongUserInfo_ShouldNotCallCompletion() {
        let heightChangedExpectation = expectation(description: "height changed")
        heightChangedExpectation.isInverted = true
        sut.onHeightChangeCompletion = { _ in
            heightChangedExpectation.fulfill()
        }
        notificationCenterMock.post(name: UIWindow.keyboardWillHideNotification,
                                    object: nil,
                                    userInfo: mockKeyboardUserInfo(includeAnimationInfo: false))
        wait(for: [heightChangedExpectation], timeout: 1)
    }
    
    private func mockKeyboardUserInfo(keyboardHeight: CGFloat? = nil, includeAnimationInfo: Bool = true) -> [AnyHashable : Any] {
        
        let mockAnimationDuration = NSNumber(value: 1)
        let mockAnimationCurve = NSNumber(value: 2)
        let mockUserInfo: [(AnyHashable, Any)] = includeAnimationInfo ? [
            (UIResponder.keyboardAnimationDurationUserInfoKey, mockAnimationDuration),
            (UIResponder.keyboardAnimationCurveUserInfoKey, mockAnimationCurve)
        ] : []
        
        guard let mockedKeyboardHeight = keyboardHeight else {
            return Dictionary(uniqueKeysWithValues: mockUserInfo)
        }
        
        let mockKeyboardSize = CGSize(width: .zero, height: mockedKeyboardHeight)
        let mockKeyboardFrameValue = NSValue(cgRect: CGRect(origin: .zero, size: mockKeyboardSize))
        let mockKeyboardFrameInfo = (UIResponder.keyboardFrameEndUserInfoKey, mockKeyboardFrameValue)
        let extendedUserInfoMock = mockUserInfo + [mockKeyboardFrameInfo]
        return Dictionary(uniqueKeysWithValues: extendedUserInfoMock)
    }
}
