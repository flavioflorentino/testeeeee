import AssetsKit
import Core
import XCTest
@testable import Feed

private class AbuseReportDetailDisplayingSpy: AbuseReportDetailDisplaying {
    // MARK: - SwitchButtonState
    private(set) var switchButtonStateToCallsCount = 0
    private(set) var switchButtonStateToReceivedInvocations: [Bool] = []

    func switchButtonState(to enabled: Bool) {
        switchButtonStateToCallsCount += 1
        switchButtonStateToReceivedInvocations.append(enabled)
    }

    // MARK: - SwitchLoadingState
    private(set) var switchLoadingStateCallsCount = 0
    private(set) var switchLoadingStateReceivedInvocations: [Bool] = []

    func switchLoadingState(_ shouldDisplay: Bool) {
        switchLoadingStateCallsCount += 1
        switchLoadingStateReceivedInvocations.append(shouldDisplay)
    }

    // MARK: - SwitchInputStateTo
    private(set) var switchInputStateToEditingCallsCount = 0
    private(set) var switchInputStateToEditingReceivedInvocations: [Bool] = []

    func switchInputStateTo(editing: Bool) {
        switchInputStateToEditingCallsCount += 1
        switchInputStateToEditingReceivedInvocations.append(editing)
    }

    // MARK: - ShowError
    private(set) var showErrorImageTitleMessageTitleButtonCallsCount = 0
    private(set) var showErrorImageTitleMessageTitleButtonReceivedInvocations: [(image: UIImage?, title: String?, message: String?, titleButton: String?)] = []

    func showError(image: UIImage?, title: String?, message: String?, titleButton: String?) {
        showErrorImageTitleMessageTitleButtonCallsCount += 1
        showErrorImageTitleMessageTitleButtonReceivedInvocations.append((image: image, title: title, message: message, titleButton: titleButton))
    }

    // MARK: - ShowAbuseSuccess
    private(set) var showAbuseSuccessTitleMessageTitleButtonCallsCount = 0
    private(set) var showAbuseSuccessTitleMessageTitleButtonReceivedInvocations: [(image: UIImage?, title: String?, message: String?, titleButton: String)] = []

    func showAbuseSuccess(_ image: UIImage?, title: String?, message: String?, titleButton: String) {
        showAbuseSuccessTitleMessageTitleButtonCallsCount += 1
        showAbuseSuccessTitleMessageTitleButtonReceivedInvocations.append((image: image, title: title, message: message, titleButton: titleButton))
    }

    // MARK: - ConfigureScrollView
    private(set) var configureScrollViewWithCallsCount = 0
    private(set) var configureScrollViewWithReceivedInvocations: [NotificationCenter] = []

    func configureScrollView(with notificationCenter: NotificationCenter) {
        configureScrollViewWithCallsCount += 1
        configureScrollViewWithReceivedInvocations.append(notificationCenter)
    }
}

final class AbuseReportDetailPresenterTests: XCTestCase {
    
    private let viewControllerSpy = AbuseReportDetailDisplayingSpy()
    private lazy var sut: AbuseReportDetailPresenter = {
        let sut = AbuseReportDetailPresenter()
        sut.viewController = viewControllerSpy
        return sut
    }()
    
    func testPresentInput_WhenIsValid_ShoudEnableButton() {
        sut.presentInput(valid: true)
        
        XCTAssertEqual(viewControllerSpy.switchButtonStateToCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.switchButtonStateToReceivedInvocations, [true])
    }
    
    func testPresentInput_WhenIsNotValid_ShoudDisableButtonAndFocusOnTextField() {
        sut.presentInput(valid: false)
        
        XCTAssertEqual(viewControllerSpy.switchButtonStateToCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.switchButtonStateToReceivedInvocations, [false])
        XCTAssertEqual(viewControllerSpy.switchInputStateToEditingCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.switchInputStateToEditingReceivedInvocations, [true])
    }
    
    func testSwitchLoadingState_WhenShouldDisplay_ShouldShowLoadingAndDismissTextField() {
        sut.switchLoadingState(shouldDisplay: true)
        
        XCTAssertEqual(viewControllerSpy.switchLoadingStateCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.switchLoadingStateReceivedInvocations, [true])
        XCTAssertEqual(viewControllerSpy.switchInputStateToEditingCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.switchInputStateToEditingReceivedInvocations, [false])
    }
    
    func testSwitchLoadingState_WhenShouldNotDisplay_ShouldHideLoadingAndDismissTextField() {
        sut.switchLoadingState(shouldDisplay: false)
        
        XCTAssertEqual(viewControllerSpy.switchLoadingStateCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.switchLoadingStateReceivedInvocations, [false])
        XCTAssertEqual(viewControllerSpy.switchInputStateToEditingCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.switchInputStateToEditingReceivedInvocations, [false])
    }
    
    func testSwitchButtonStateTo_WhenIsEnabled_ShouldEnableButton() {
        sut.switchButtonStateTo(enabled: true)
        
        XCTAssertEqual(viewControllerSpy.switchButtonStateToCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.switchButtonStateToReceivedInvocations, [true])
    }
    
    func testSwitchButtonStateTo_WhenIsNotEnabled_ShouldDisableButton() {
        sut.switchButtonStateTo(enabled: false)
        
        XCTAssertEqual(viewControllerSpy.switchButtonStateToCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.switchButtonStateToReceivedInvocations, [false])
    }
    
    func testPresentReportError_ShouldShowError() {
        sut.presentReportError()
        
        XCTAssertEqual(viewControllerSpy.showErrorImageTitleMessageTitleButtonCallsCount, 1)
    }
    
    func testConfigureScrollView_ShouldConfigureViewControllerScroll() {
        let notificationCenterMock = NotificationCenter()
        sut.configureScrollView(with: notificationCenterMock)
        
        XCTAssertEqual(viewControllerSpy.configureScrollViewWithCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.configureScrollViewWithReceivedInvocations, [notificationCenterMock])
    }
    
    func testSendAbuseSuccess_ShouldShowSendAbuseSuccessInViewController() {
        sut.sendAbuseSuccessed()
        
        let receivedInvocations = viewControllerSpy.showAbuseSuccessTitleMessageTitleButtonReceivedInvocations.first
        
        XCTAssertEqual(viewControllerSpy.showAbuseSuccessTitleMessageTitleButtonCallsCount, 1)
        XCTAssertEqual(receivedInvocations?.title, Strings.thanksForYourHelp)
        XCTAssertEqual(receivedInvocations?.message, Strings.letsReviewYourReport)
        XCTAssertEqual(receivedInvocations?.titleButton, Strings.okIGotIt)
        XCTAssertEqual(receivedInvocations?.image, Resources.Icons.icoBigSuccess.image)
    }
}
