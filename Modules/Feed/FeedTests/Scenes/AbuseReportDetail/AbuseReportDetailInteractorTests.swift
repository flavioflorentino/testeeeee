import XCTest
import Core
@testable import Feed

private class AbuseReportDetailServicingMock: AbuseReportDetailServicing {
    // MARK: - SendAbuse
    private(set) var sendAbuseTypeReportedIdMessageCallsCount = 0
    private(set) var sendAbuseTypeReportedIdMessageReceivedInvocations: [(type: AbuseReportType, reportedId: String, message: String?, completion: (Result<NoContent, ApiError>) -> Void)] = []
    var sendAbuseTypeReportedIdMessageClosure: ((AbuseReportType, String, String?, @escaping (Result<NoContent, ApiError>) -> Void) -> Void)?

    func sendAbuse(
        type: AbuseReportType,
        reportedId: String,
        message: String?,
        _ completion: @escaping (Result<NoContent, ApiError>) -> Void
    ) {
        sendAbuseTypeReportedIdMessageCallsCount += 1
        sendAbuseTypeReportedIdMessageReceivedInvocations.append((type: type, reportedId: reportedId, message: message, completion: completion))
        sendAbuseTypeReportedIdMessageClosure?(type, reportedId, message, completion)
    }
}

private class AbuseReportDetailPresentingSpy: AbuseReportDetailPresenting {
    var viewController: AbuseReportDetailDisplaying?

    // MARK: - SwitchLoadingState
    private(set) var switchLoadingStateShouldDisplayCallsCount = 0
    private(set) var switchLoadingStateShouldDisplayReceivedInvocations: [Bool] = []

    func switchLoadingState(shouldDisplay: Bool) {
        switchLoadingStateShouldDisplayCallsCount += 1
        switchLoadingStateShouldDisplayReceivedInvocations.append(shouldDisplay)
    }

    // MARK: - PresentInput
    private(set) var presentInputValidCallsCount = 0
    private(set) var presentInputValidReceivedInvocations: [Bool] = []

    func presentInput(valid: Bool) {
        presentInputValidCallsCount += 1
        presentInputValidReceivedInvocations.append(valid)
    }

    // MARK: - PresentReportError
    private(set) var presentReportErrorCallsCount = 0

    func presentReportError() {
        presentReportErrorCallsCount += 1
    }

    // MARK: - SendAbuseSuccessed
    private(set) var sendAbuseSuccessedCallsCount = 0

    func sendAbuseSuccessed() {
        sendAbuseSuccessedCallsCount += 1
    }

    // MARK: - ConfigureScrollView
    private(set) var configureScrollViewWithCallsCount = 0
    private(set) var configureScrollViewWithReceivedInvocations: [NotificationCenter] = []

    func configureScrollView(with notificationCenter: NotificationCenter) {
        configureScrollViewWithCallsCount += 1
        configureScrollViewWithReceivedInvocations.append(notificationCenter)
    }
}

final class AbuseReportDetailInteractorTests: XCTestCase {
    private let reportedIdMock = UUID().uuidString
    private let serviceMock = AbuseReportDetailServicingMock()
    private let presenterSpy = AbuseReportDetailPresentingSpy()
    private let notificationCenterMock = NotificationCenter()
    private lazy var containerMock = DependencyContainerMock(notificationCenterMock)
    
    private lazy var sut = AbuseReportDetailInteractor(
        type: .transactionalCard,
        reportedId: reportedIdMock,
        service: serviceMock,
        presenter: presenterSpy,
        depedencies: containerMock
    )
    
    private let validReport = "valid report"
    private let emptyReport = "  "
    private let nilReport: String? = nil
    
    func testValidateMessage_WhenInputIsValid_ShouldPresentValidInput() {
        sut.validateMessage(validReport)
        
        XCTAssertEqual(presenterSpy.presentInputValidCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentInputValidReceivedInvocations, [true])
    }
    
    func testValidateMessage_WhenInputIsEmpty_ShouldPresentInvalidInput() {
        sut.validateMessage(emptyReport)
        
        XCTAssertEqual(presenterSpy.presentInputValidCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentInputValidReceivedInvocations, [false])
    }
    
    func testValidateMessage_WhenInputIsNil_ShouldPresentInvalidInput() {
        sut.validateMessage(nilReport)
        
        XCTAssertEqual(presenterSpy.presentInputValidCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentInputValidReceivedInvocations, [false])
    }
    
    func testSendAbuse_WhenInputIsValid_ShouldPresentLoadingAndCallService() {
        sut.sendAbuse(with: validReport)
        
        XCTAssertEqual(presenterSpy.switchLoadingStateShouldDisplayCallsCount, 1)
        XCTAssertEqual(presenterSpy.switchLoadingStateShouldDisplayReceivedInvocations, [true])
    }
    
    func testSendAbuse_WhenInputIsEmpty_ShouldPresentInvalidInput() {
        sut.sendAbuse(with: emptyReport)
        
        XCTAssertEqual(presenterSpy.presentInputValidCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentInputValidReceivedInvocations, [false])
    }
    
    func testSendAbuse_WhenInputIsNil_ShouldPresentInvalidInput() {
        sut.sendAbuse(with: nilReport)
        
        XCTAssertEqual(presenterSpy.presentInputValidCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentInputValidReceivedInvocations, [false])
    }
    
    func testSendAbuse_WhenServiceReturnSuccess_ShouldCallDelegate() {
        serviceMock.sendAbuseTypeReportedIdMessageClosure = { _, _, _, completion in
            completion(.success(NoContent()))
        }
        
        sut.sendAbuse(with: validReport)
        
        XCTAssertEqual(presenterSpy.sendAbuseSuccessedCallsCount, 1)
    }
    
    func testSendAbuse_WhenServiceFails_ShouldPresentError() {
        serviceMock.sendAbuseTypeReportedIdMessageClosure = { _, _, _, completion in
            completion(.failure(.connectionFailure))
        }
        
        sut.sendAbuse(with: validReport)
        
        XCTAssertEqual(presenterSpy.presentReportErrorCallsCount, 1)
    }
    
    func testGetNotificationCenter_ShouldPresentScrollConfiguration() {
        sut.getNotificationCenter()
        
        XCTAssertEqual(presenterSpy.configureScrollViewWithCallsCount, 1)
        XCTAssertEqual(presenterSpy.configureScrollViewWithReceivedInvocations, [notificationCenterMock])
    }
}
