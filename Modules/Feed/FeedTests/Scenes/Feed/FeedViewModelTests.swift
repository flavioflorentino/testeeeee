import XCTest
import AnalyticsModule
@testable import Feed

private class FeedViewModelOutputsSpy: FeedViewModelOutputs {
    //MARK: - configurePages
    private(set) var configurePagesCallsCount = 0
    private(set) var configurePagesReceivedInvocations: [[UIViewController]] = []

    func configurePages(_ viewControllers: [UIViewController]) {
        configurePagesCallsCount += 1
        configurePagesReceivedInvocations.append(viewControllers)
    }

    //MARK: - configureIndex
    private(set) var configureIndexCallsCount = 0
    private(set) var configureIndexReceivedInvocations: [Int] = []

    func configureIndex(_ index: Int) {
        configureIndexCallsCount += 1
        configureIndexReceivedInvocations.append(index)
    }

    //MARK: - configureHeader
    private(set) var configureHeaderCallsCount = 0

    func configureHeader() {
        configureHeaderCallsCount += 1
    }

    //MARK: - nextPage
    private(set) var nextPageCallsCount = 0
    private(set) var nextPageReceivedInvocations: [Int] = []

    func nextPage(_ index: Int) {
        nextPageCallsCount += 1
        nextPageReceivedInvocations.append(index)
    }

    //MARK: - previousPage
    private(set) var previousPageCallsCount = 0
    private(set) var previousPageReceivedInvocations: [Int] = []

    func previousPage(_ index: Int) {
        previousPageCallsCount += 1
        previousPageReceivedInvocations.append(index)
    }
}

private class LegacyBannerAdapteringMock: LegacyBannerAdaptering {
    // MARK: - ListBanners
    private(set) var listBannersWithCallsCount = 0
    private(set) var listBannersWithReceivedInvocations: [(items: [Item], onSuccess: ([Item]) -> Void)] = []
    var listBannersWithClosure: (([Item], @escaping ([Item]) -> Void) -> Void)?

    func listBanners(with items: [Item], _ onSuccess: @escaping ([Item]) -> Void) {
        listBannersWithCallsCount += 1
        listBannersWithReceivedInvocations.append((items: items, onSuccess: onSuccess))
        listBannersWithClosure?(items, onSuccess)
    }
}

private class ActionableSpy: Actionable {
    // MARK: - Deeplink
    private(set) var deeplinkCallsCount = 0
    private(set) var deeplinkReceivedInvocations: [URL] = []

    func deeplink(_ url: URL) {
        deeplinkCallsCount += 1
        deeplinkReceivedInvocations.append(url)
    }

    // MARK: - Screen
    private(set) var screenForCallsCount = 0
    private(set) var screenForReceivedInvocations: [ActionScreenType] = []

    func screen(for type: ActionScreenType) {
        screenForCallsCount += 1
        screenForReceivedInvocations.append(type)
    }
}

// MARK: - FeedViewModelTests
final class FeedViewModelTests: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    private lazy var containerMock = DependencyContainerMock(analyticsSpy)
    private let outputSpy = FeedViewModelOutputsSpy()
    private let actionableSpy = ActionableSpy()
    private let interactionTypeMock = FeedAnalytics.InteractionType.like
    private let adapterMock = LegacyBannerAdapteringMock()
    private lazy var sut: FeedViewModel = makeSut(options: .mainOptions)

    func testConfigure_WhenOptionsIsEveryoneAndPersonal_ShouldConfigureItemsContainer() {
        sut = makeSut(options: .mainOptions)
        
        sut.inputs.configure()
        
        XCTAssertEqual(outputSpy.configurePagesCallsCount, 1)
        XCTAssertEqual(outputSpy.configurePagesReceivedInvocations.first?.count, 2)
        XCTAssertEqual(outputSpy.configureIndexCallsCount, 1)
        XCTAssertEqual(outputSpy.configureIndexReceivedInvocations.first, 0)
        XCTAssertEqual(outputSpy.configureHeaderCallsCount, 1)
    }
    
    func testConfigure_WhenOptionsIsProfileAndIsMyProfileIsFalse_ShouldConfigureItemsContainer() {
        sut = makeSut(options: .profile(profileId: "123", isOpenProfile: true))
        
        sut.inputs.configure()
        
        XCTAssertEqual(outputSpy.configurePagesCallsCount, 1)
        XCTAssertEqual(outputSpy.configurePagesReceivedInvocations.first?.count, 1)
        XCTAssertEqual(outputSpy.configureIndexCallsCount, 1)
        XCTAssertEqual(outputSpy.configureIndexReceivedInvocations.first, 0)
        XCTAssertEqual(outputSpy.configureHeaderCallsCount, 0)
    }
    
    func testConfigure_WhenOptionsIsProfileAndIsMyProfileIsTrue_ShouldConfigureItemsContainer() {
        sut = makeSut(options: .profile(profileId: nil, isOpenProfile: true))
        
        sut.inputs.configure()
        
        XCTAssertEqual(outputSpy.configurePagesCallsCount, 1)
        XCTAssertEqual(outputSpy.configurePagesReceivedInvocations.first?.count, 1)
        XCTAssertEqual(outputSpy.configureIndexCallsCount, 1)
        XCTAssertEqual(outputSpy.configureIndexReceivedInvocations.first, 0)
        XCTAssertEqual(outputSpy.configureHeaderCallsCount, 0)
    }

    func testChangeValue_WhenCurrentIndexIsGreaterAndIsNotMainOptions_ShouldCallNextPageAndNotSendAnalytics() {
        let sut = makeSut(options: .profile(profileId: "123", isOpenProfile: true))
        
        sut.inputs.changeValue(1)
        
        XCTAssertNil(analyticsSpy.event)
        XCTAssertEqual(analyticsSpy.logCalledCount, 0)
        XCTAssertEqual(outputSpy.nextPageCallsCount, 1)
        XCTAssertEqual(outputSpy.nextPageReceivedInvocations.first, 1)
    }
    
    func testChangeValue_WhenCurrentIndexIsGreaterAndIsMainOptionsAndIndexIsInvalid_ShouldCallNextPageAndSendAnalytics() {
        sut.inputs.changeValue(2)
        
        XCTAssertNil(analyticsSpy.event)
        XCTAssertEqual(analyticsSpy.logCalledCount, 0)
        XCTAssertEqual(outputSpy.nextPageCallsCount, 1)
        XCTAssertEqual(outputSpy.nextPageReceivedInvocations.first, 2)
    }
    
    func testChangeValue_WhenCurrentIndexIsGreaterAndIsMainOptionsAndIndexIsValid_ShouldCallNextPageAndSendAnalytics() {
        sut.inputs.changeValue(1)

        let expectedEvent = FeedAnalytics.tabViewed(tabType: .mine).event()

        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(outputSpy.nextPageCallsCount, 1)
        XCTAssertEqual(outputSpy.nextPageReceivedInvocations.first, 1)
    }
    
    func testChangeValue_WhenCurrentIndexIsLessAndIsNotMainOptions_ShouldCallPreviousPageAndNotSendAnalytics() {
        let sut = makeSut(options: .profile(profileId: "123", isOpenProfile: true))
        
        sut.inputs.setIndex(1)
        sut.inputs.changeValue(0)

        XCTAssertNil(analyticsSpy.event)
        XCTAssertEqual(analyticsSpy.logCalledCount, 0)
        XCTAssertEqual(outputSpy.previousPageCallsCount, 1)
        XCTAssertEqual(outputSpy.previousPageReceivedInvocations.first, 0)
    }
    
    func testChangeValue_WhenCurrentIndexIsLessAndIsMainOptionsAndIndexIsInvalid_ShouldCallPreviousPageAndSendAnalytics() {
        sut.inputs.setIndex(1)
        sut.inputs.changeValue(-1)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1) // 1 because of setIndex call
        XCTAssertEqual(outputSpy.previousPageCallsCount, 1)
        XCTAssertEqual(outputSpy.previousPageReceivedInvocations.first, -1)
    }
    
    func testChangeValue_WhenCurrentIndexIsLessAndIsMainOptionsAndIndexIsValid_ShouldCallPreviousPageAndSendAnalytics() {
        sut.inputs.setIndex(1)
        sut.inputs.changeValue(0)
        
        let expectedEvent = FeedAnalytics.tabViewed(tabType: .all).event()

        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 2) //2 because of setIndex call
        XCTAssertEqual(outputSpy.previousPageCallsCount, 1)
        XCTAssertEqual(outputSpy.previousPageReceivedInvocations.first, 0)
    }
    
    func testSetIndex_WhenOptionsIsNotMainOptions_ShouldConfigureANewIndexAndNotSendAnalytics() {
        let sut = makeSut(options: .profile(profileId: "123", isOpenProfile: true))
        
        sut.inputs.setIndex(1)
        
        XCTAssertNil(analyticsSpy.event)
        XCTAssertEqual(analyticsSpy.logCalledCount, 0)
        XCTAssertEqual(outputSpy.configureIndexCallsCount, 1)
        XCTAssertEqual(outputSpy.configureIndexReceivedInvocations.first, 1)
    }
    
    func testSetIndex_WhenIsMainOptionsAndIndexIsValid_ShouldConfigureANewIndexAndSendAnalytics() {
        sut.inputs.setIndex(1)
        
        let expectedEvent = FeedAnalytics.tabViewed(tabType: .mine).event()
        
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(outputSpy.configureIndexCallsCount, 1)
        XCTAssertEqual(outputSpy.configureIndexReceivedInvocations.first, 1)
    }
    
    func testSetIndex_WhenIsMainOptionsAndIndexIsNotValid_ShouldConfigureANewIndexAndNotSendAnalytics() {
        sut.inputs.setIndex(2)
        
        XCTAssertNil(analyticsSpy.event)
        XCTAssertEqual(analyticsSpy.logCalledCount, 0)
        XCTAssertEqual(outputSpy.configureIndexCallsCount, 1)
        XCTAssertEqual(outputSpy.configureIndexReceivedInvocations.first, 2)
    }
    
    private func makeSut(options: FeedOptions) -> FeedViewModel {
        let viewModel = FeedViewModel(
            options: options,
            adapter: adapterMock,
            dependencies: containerMock,
            actionable: actionableSpy,
            hasRefresh: false
        )
        viewModel.outputs = outputSpy
        return viewModel
    }
}
