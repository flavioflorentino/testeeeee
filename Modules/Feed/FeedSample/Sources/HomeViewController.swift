import Core
import Feed
import SnapKit
import UI
import UIKit

class HomeViewController: UIViewController {
    private lazy var openReportDetailButton = makeButton(
        title: "Abrir Detalhes da Denuncia",
        selector: #selector(handleReportDetailTap)
    )
    
    private lazy var openScreenButton = makeButton(
        title: "Abrir Feed!",
        selector: #selector(pressButton)
    )
    
    private lazy var openDetailScreenButton = makeButton(
        title: "Abrir Detalhe!",
        selector: #selector(pressDetailButton)
    )
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [openReportDetailButton, openScreenButton, openDetailScreenButton])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private let dependencies = DependencyContainer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildLayout()
        
        dependencies.keychain.set(key: KeychainKey.hostBaseUrl, value: "https://gateway.service.ppay.me/")
    }
    
    private func makeButton(title: String, selector: Selector) -> UIButton {
        let button = UIButton(type: .custom)
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(title, for: .normal)
        button.addTarget(self, action: selector, for: .touchUpInside)
        return button
    }
}

@objc
private extension HomeViewController {
    func handleReportDetailTap(_ sender: UIButton) {
        present(AbuseReportDetailFactory.make(reportedType: .transactionalCard, reportedId: UUID().uuidString), animated: true)
    }
    
    func pressButton(_ sender: UIButton) {
        let viewController = FeedFactory.make(options: .mainOptions, hasRefresh: true, actionable: self)
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func pressDetailButton(_ sender: UIButton) {
        navigationController?.pushViewController(ItemDetailFactory.make(cardId: UUID(), actionable: nil, delegate: self), animated: true)
    }
}

extension HomeViewController: ViewConfiguration {
    func buildViewHierarchy() {
        view.addSubview(stackView)
    }
    
    func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.bottom.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}

extension HomeViewController: ScreenActionable {    
    func screen(for type: ActionScreenType) {
        print("[PP] - Actionable: \(type)")
    }
}

extension HomeViewController: ItemDetailDelegate {
    func openPixRefundFlow(_ transactionId: String) { }
    func refundPaymentP2P(data: [AnyHashable : Any], completion: @escaping (_ success: Bool, _ error: Error?) -> Void) { }
}
