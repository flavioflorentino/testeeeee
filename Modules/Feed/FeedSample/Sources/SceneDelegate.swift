import UIKit
import UI
import Feed

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        if let windowScene = scene as? UIWindowScene {
            let window = UIWindow(windowScene: windowScene)
            window.rootViewController = StyledNavigationController(rootViewController: HomeViewController())
            self.window = window
            
            FeedSetup.inject(instance: ConsumerSample())
            window.makeKeyAndVisible()
        }
    }
}

struct ConsumerSample: FeedConsumerContract {
    var consumerId: Int = 0
    var name: String = "SampleUser"
    var username: String? = "SampleUser"
    var imageURL: URL? = URL(string: "www.sample.com")
}
