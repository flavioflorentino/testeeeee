import Core
import Foundation

enum LoginEndpoint {
    case pj(_ cnpj: String, email: String, password: String)
}

extension LoginEndpoint: ApiEndpointExposable {
    var path: String {
        "/oauth/access-token"
    }

    var method: HTTPMethod {
        .post
    }

    var body: Data? {
        guard case let .pj(cnpj, email, password) = self else {
            return nil
        }
        return [
            "client_id": 1,
            "client_secret": "FtNgh1R9EPeH5wZEs7x3",
            "grant_type": "password",
            "password": password,
            "username": [
                "cnpj": cnpj.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(),
                "email": email
            ]
        ].toData()
    }
}
