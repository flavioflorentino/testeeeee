import FactorAuthentication
import UIKit
import Core

extension Notification.Name {
    static let factorAuthenticationTokenUpdate = Notification.Name("TokenUpdate")
}

class HomeViewController: UIViewController {
    typealias Dependencies = HasKeychainManager
    private let dependencies: Dependencies = DependencyContainer()
    private lazy var coordinator = FactorAuthenticationCoordinator(
        from: self,
        delegate: self,
        dataSource: self)
    private lazy var actionButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(didTapAction(_:)), for: .touchUpInside)
        button.setTitle("Logar", for: .normal)
        button.setTitle("Deslogar", for: .selected)
        button.isSelected = dependencies.keychain.getData(key: KeychainKey.token) != nil
        return button
    }()
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        return tableView
    }()

    private let items = FactorAuthenticationFlow.allCases
    private var selectedFlow: FactorAuthenticationFlow = .pixOptinSMS

    init() {
        super.init(nibName: nil, bundle: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(updateToken),
            name: .factorAuthenticationTokenUpdate,
            object: nil
        )
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        buildViewHierarchy()
        setupConstraints()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if dependencies.keychain.getData(key: KeychainKey.token) == nil {
            presentLogin()
        }
    }

    func configureViews() {
        view.backgroundColor = .white
    }

    func buildViewHierarchy() {
        view.addSubview(actionButton)
        view.addSubview(tableView)
    }

    func setupConstraints() {
        actionButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 24.0).isActive = true
        actionButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true

        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: actionButton.bottomAnchor, constant: 32.0).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
}

@objc
private extension HomeViewController {
    func didTapAction(_ sender: UIButton) {
        if dependencies.keychain.getData(key: KeychainKey.token) == nil {
            presentLogin()
        } else {
            dependencies.keychain.clearAll()
            updateToken()
        }
    }

    func presentLogin() {
        present(LoginViewController(), animated: true)
    }

    func updateToken() {
        actionButton.isSelected = dependencies.keychain.getData(key: KeychainKey.token) != nil
    }
}

extension HomeViewController: FactorAuthenticationDelegate {
    func factorAuthenticationWasClosed() {
        print("Foi fechado")
    }

    func factorAuthenticationWasCompleted(with hash: String) {
        print(hash)
    }
}

extension HomeViewController: FactorAuthenticationDataSource {
    var flow: FactorAuthenticationFlow {
        selectedFlow
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedFlow = items[indexPath.row]
        coordinator.start()
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        let item = items[indexPath.row]
        cell.textLabel?.text = item.rawValue
        return cell
    }
}
