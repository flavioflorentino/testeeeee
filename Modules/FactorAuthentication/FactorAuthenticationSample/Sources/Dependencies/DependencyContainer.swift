import AnalyticsModule
import Core

typealias Dependencies =
    HasMainQueue &
    HasAnalytics &
    HasKeychainManager

final class DependencyContainer: Dependencies {
    lazy var mainQueue = DispatchQueue.main
    lazy var analytics: AnalyticsProtocol = Analytics.shared
    lazy var keychain: KeychainManagerContract = KeychainManager()
}
