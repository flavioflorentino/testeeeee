import Foundation

struct Auth: Decodable {
    let accessToken: String

    private enum CodingKeys: String, CodingKey {
        case data
        case accessToken
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let data = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        accessToken = try data.decode(String.self, forKey: .accessToken)
    }
}
