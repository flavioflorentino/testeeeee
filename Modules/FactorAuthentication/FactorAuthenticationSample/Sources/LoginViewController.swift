import Core
import Foundation
import UIKit

class LoginViewController: UIViewController {
    typealias Dependencies = HasKeychainManager
    private let dependencies: Dependencies = DependencyContainer()
    private lazy var appTypeControl: UISegmentedControl = {
        let segmentedControl = UISegmentedControl(items: ["PJ", "PF"])
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        segmentedControl.addTarget(self, action: #selector(didChangeControl(_:)), for: .valueChanged)
        segmentedControl.selectedSegmentIndex = 0
        return segmentedControl
    }()
    private lazy var cnpjTextField: UITextField = {
        let textField = UITextField()
        textField.borderStyle = .roundedRect
        textField.placeholder = "CNPJ"
        return textField
    }()
    private lazy var emailTextField: UITextField = {
        let textField = UITextField()
        textField.borderStyle = .roundedRect
        textField.placeholder = "E-Mail"
        return textField
    }()
    private lazy var passwordTextField: UITextField = {
        let textField = UITextField()
        textField.borderStyle = .roundedRect
        textField.placeholder = "Senha"
        return textField
    }()
    private lazy var fieldsStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            cnpjTextField,
            emailTextField,
            passwordTextField
        ])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 24.0
        return stackView
    }()
    private lazy var loginButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(didTapLogin(_:)), for: .touchUpInside)
        button.setTitle("Entrar", for: .normal)
        return button
    }()

    init() {
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        buildViewHierarchy()
        setupConstraints()
    }

    func configureViews() {
        view.backgroundColor = .white
        cnpjTextField.becomeFirstResponder()
    }

    func buildViewHierarchy() {
        view.addSubview(appTypeControl)
        view.addSubview(fieldsStackView)
        view.addSubview(loginButton)
    }

    func setupConstraints() {
        appTypeControl.topAnchor.constraint(equalTo: view.topAnchor, constant: 24.0).isActive = true
        appTypeControl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true

        fieldsStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 32.0).isActive = true
        fieldsStackView.topAnchor.constraint(equalTo: appTypeControl.bottomAnchor, constant: 32.0).isActive = true
        fieldsStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -32.0).isActive = true

        loginButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24.0).isActive = true
        loginButton.topAnchor.constraint(equalTo: fieldsStackView.bottomAnchor, constant: 32.0).isActive = true
        loginButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -24.0).isActive = true
        loginButton.bottomAnchor.constraint(lessThanOrEqualTo: view.bottomAnchor, constant: -32.0).isActive = true
    }
}

@objc
private extension LoginViewController {
    func didChangeControl(_ sender: UISegmentedControl) {
        if cnpjTextField.isEditing, sender.selectedSegmentIndex == 1 {
            emailTextField.becomeFirstResponder()
        } else if emailTextField.isEditing, sender.selectedSegmentIndex == 0 {
            cnpjTextField.becomeFirstResponder()
        }
        UIView.animate(withDuration: 0.2) {
            self.cnpjTextField.isHidden = sender.selectedSegmentIndex != 0
        }
    }

    func didTapLogin(_ sender: UIButton) {
        // Ainda só realizei o login para o PJ :P
        guard appTypeControl.selectedSegmentIndex == 0,
              let cnpj = cnpjTextField.text,
              let email = emailTextField.text,
              let password = passwordTextField.text else {
            return
        }
        let decoder = JSONDecoder(.convertFromSnakeCase)
        let endpoint = LoginEndpoint.pj(cnpj, email: email, password: password)
        updateLoginButton(isEnable: false)
        Api<Auth>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            guard let self = self else { return }
            DispatchQueue.main.async {
                let result = result.map(\.model)
                switch result {
                case .success(let response):
                    let token = response.accessToken
                    self.dependencies.keychain.set(key: KeychainKey.token, value: token)
                    NotificationCenter.default.post(name: .factorAuthenticationTokenUpdate, object: nil)
                    self.dismiss(animated: true)
                case .failure:
                    self.updateLoginButton(isEnable: true)
                }
            }
        }
    }

    func updateLoginButton(isEnable: Bool) {
        UIView.animate(withDuration: 0.2) {
            self.loginButton.alpha = isEnable ? 1.0 : 0.3
            self.loginButton.isUserInteractionEnabled = isEnable ? true : false
        }
    }
}
