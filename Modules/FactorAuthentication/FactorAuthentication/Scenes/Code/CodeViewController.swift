import AssetsKit
import SnapKit
import UI
import UIKit

protocol CodeDisplaying: AnyObject {
    func displayNavigationTitle(_ title: String)
    func displayTitle(_ title: String)
    func displayDescription(_ description: NSAttributedString)
    func displayPlaceholder(_ placeholder: String)
    func displayTime(_ title: NSAttributedString)
    func displayWaiting(_ title: NSAttributedString)
    func displayResend()
    func displayCodeLoading()
    func setResendTitle(_ title: NSAttributedString)
    func displayCodeError(_ messsage: String)
    func dismissCodeError()
    func displayResendError(_ message: String)
    func displayKeyboard()
    func dismissKeyboard()
    func displayNetworkBanner()
    func dismissNetworkBanner()
}

extension CodeViewController.Layout {
    enum Animation {
        static let time = 0.3
    }
}

final class CodeViewController: ViewController<CodeInteracting, UIView> {
    fileprivate enum Layout {}
    private lazy var keyboardHandler = KeyboardHandler()
    private lazy var scrollView = UIScrollView()
    private lazy var containerView = UIView()

    private lazy var closeButton: UIButton = {
        let button = UIButton()
        button.setImage(Resources.Icons.icoClose.image, for: .normal)
        button.addTarget(self, action: #selector(didTapCloseButton), for: .touchUpInside)
        return button
    }()

    private lazy var networkBanner: NetworkBannerView = {
        let banner = NetworkBannerView()
        banner.isHidden = true
        return banner
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, .grayscale750())
        return label
    }()
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale750())
        return label
    }()
    private lazy var codeLoadingView = UIActivityIndicatorView(style: .gray)
    private lazy var codeTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholderColor = Colors.grayscale400.color
        textField.titleColor = Colors.grayscale400.color
        textField.selectedTitleColor = Colors.branding600.color
        textField.textColor = Colors.grayscale750.color
        textField.keyboardType = .numberPad
        if #available(iOS 12.0, *) {
            textField.textContentType = .oneTimeCode
        }
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField.rightView = codeLoadingView
        textField.rightViewMode = .always
        return textField
    }()
    private lazy var errorLabel: UILabel = {
        let label = UILabel()
        label.isHidden = true
        label.labelStyle(CaptionLabelStyle())
            .with(\.textColor, .critical900())
        return label
    }()
    private lazy var codeStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [codeTextField, errorLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    private lazy var containerStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            networkBanner,
            titleLabel,
            SpacerView(size: Spacing.base02),
            descriptionLabel,
            SpacerView(size: Spacing.base03),
            codeStackView
        ])
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        return stackView
    }()

    private lazy var actionLoading = UIActivityIndicatorView(style: .gray)
    private lazy var actionButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(LinkButtonStyle())
            .with(\.typography, .bodyPrimary())
            .with(\.textAlignment, .center)
        button.addTarget(self, action: #selector(didTapResend), for: .touchUpInside)
        return button
    }()

    private let codeMask = CustomStringMask(mask: "000000", maskTokenSet: [.decimalDigits])

    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.trackStarted()
        interactor.prepareForReceiveCode()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.setHidesBackButton(true, animated: false)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.setHidesBackButton(false, animated: false)
    }

    override func buildViewHierarchy() {
        containerView.addSubview(containerStackView)
        containerView.addSubview(actionButton)
        actionButton.addSubview(actionLoading)
        scrollView.addSubview(containerView)
        view.addSubview(scrollView)
    }

    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.top.equalTo(view.compatibleSafeArea.top)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom)
        }

        containerView.snp.makeConstraints {
            $0.leading.top.trailing.bottom.equalToSuperview()
            $0.height.equalToSuperview().priority(.low)
            $0.width.equalToSuperview()
        }

        containerStackView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.top.equalToSuperview().offset(Spacing.base02)
        }

        actionLoading.snp.makeConstraints {
            $0.trailing.centerY.equalToSuperview()
        }

        actionButton.snp.makeConstraints {
            $0.top.greaterThanOrEqualTo(containerStackView.snp.bottom).offset(Spacing.base04)
            $0.bottom.equalToSuperview().inset(Spacing.base02)
            $0.centerX.equalToSuperview()
        }
    }

    override func configureViews() {
        view.backgroundColor = .backgroundPrimary()
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: closeButton)
        networkBanner.setup { [weak self] _ in
            self?.dismissNetworkBanner()
        }
        keyboardHandler.handle { [weak self] keyboardSize in
            guard let self = self else { return }
            self.scrollView.snp.updateConstraints {
                $0.bottom.equalTo(self.view.compatibleSafeArea.bottom).inset(keyboardSize)
            }
            self.view.layoutIfNeeded()
        }
    }
}

// MARK: - CodeDisplaying
extension CodeViewController: CodeDisplaying {
    func displayNavigationTitle(_ title: String) {
        self.title = title
    }

    func displayTitle(_ title: String) {
        titleLabel.text = title
    }

    func displayDescription(_ description: NSAttributedString) {
        descriptionLabel.attributedText = description
    }

    func displayPlaceholder(_ placeholder: String) {
        codeTextField.placeholder = placeholder
        codeTextField.title = placeholder
        codeTextField.selectedTitle = placeholder
    }

    func setResendTitle(_ title: NSAttributedString) {
        actionButton.setAttributedTitle(title, for: .normal)
    }

    func displayTime(_ title: NSAttributedString) {
        actionButton.setAttributedTitle(title, for: .disabled)
        actionButton.isEnabled = false
        mustDisplayWaiting(false)
    }

    func displayWaiting(_ title: NSAttributedString) {
        actionButton.setAttributedTitle(title, for: .disabled)
        actionButton.isEnabled = false
        mustDisplayWaiting(true)
    }

    func displayResend() {
        actionButton.isEnabled = true
        mustDisplayWaiting(false)
    }

    func displayCodeLoading() {
        codeLoadingView.startAnimating()
    }

    func displayCodeError(_ messsage: String) {
        errorStatus(true, with: messsage)
    }

    func dismissCodeError() {
        errorStatus(false)
    }

    func displayResendError(_ message: String) {
        dialogError(with: message, primaryAction: { [weak self] in
            self?.interactor.resendCode()
        })
    }

    func displayKeyboard() {
        codeTextField.isUserInteractionEnabled = true
        codeTextField.becomeFirstResponder()
    }

    func dismissKeyboard() {
        codeTextField.isUserInteractionEnabled = false
        codeTextField.resignFirstResponder()
    }

    func displayNetworkBanner() {
        networkBanner(isHidden: false)
    }

    func dismissNetworkBanner() {
        networkBanner(isHidden: true)
    }
}

@objc
private extension CodeViewController {
    func didTapCloseButton(_ sender: UIButton) {
        interactor.close()
    }

    func didTapResend(_ sender: UIButton) {
        interactor.resendCode()
    }

    func textFieldDidChange(_ textField: UITextField) {
        codeTextField.text = codeMask.maskedText(from: textField.text)
        interactor.verifyCode(codeTextField.text)
    }

    func errorStatus(_ hasError: Bool, with message: String = "") {
        if hasError {
            codeTextField.lineColor = Colors.critical900.color
            codeTextField.selectedLineColor = Colors.critical900.color
            codeTextField.titleColor = Colors.critical900.color
            codeTextField.selectedTitleColor = Colors.critical900.color
        } else {
            codeTextField.lineColor = Colors.grayscale400.color
            codeTextField.selectedLineColor = Colors.branding300.color
            codeTextField.titleColor = Colors.grayscale400.color
            codeTextField.selectedTitleColor = Colors.branding600.color
        }

        codeLoadingView.stopAnimating()
        errorLabel.isHidden = !hasError
        errorLabel.text = message

        UIView.animate(withDuration: Layout.Animation.time) {
            self.view.layoutIfNeeded()
        }
    }

    func mustDisplayWaiting(_ isLoading: Bool) {
        if isLoading {
            actionLoading.startAnimating()
        } else {
            actionLoading.stopAnimating()
        }
        let loadingSize = Spacing.base01 + actionLoading.size.width
        let rightMargin = isLoading ? loadingSize : 0
        actionButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: rightMargin)
    }

    func networkBanner(isHidden: Bool) {
        UIView.animate(withDuration: Layout.Animation.time) {
            self.networkBanner.isHidden = isHidden
        }
    }
}
