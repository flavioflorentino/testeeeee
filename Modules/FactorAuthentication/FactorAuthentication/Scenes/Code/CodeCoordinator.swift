import UIKit

enum CodeAction: Equatable {
    case close
    case complete(_ hash: String)
}

protocol CodeCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: CodeAction)
}

final class CodeCoordinator {
    weak var viewController: UIViewController?
    private weak var delegate: FactorAuthenticationCoordinating?

    init(delegate: FactorAuthenticationCoordinating?) {
        self.delegate = delegate
    }
}

// MARK: - CodeCoordinating
extension CodeCoordinator: CodeCoordinating {
    func perform(action: CodeAction) {
        switch action {
        case .close:
            delegate?.close()
        case .complete(let hash):
            delegate?.complete(with: hash)
        }
    }
}
