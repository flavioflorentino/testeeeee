import Core
import Foundation

protocol CodeServicing {
    func retrieveStep(completion: @escaping (Result<StepResponse, ApiError>) -> Void)
    func verify(_ code: String, completion: @escaping (Result<VerifyCodeResponse, ApiError>) -> Void)
}

final class CodeService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    private let dataSource: FactorAuthenticationDataSource

    init(dependencies: Dependencies,
         dataSource: FactorAuthenticationDataSource) {
        self.dependencies = dependencies
        self.dataSource = dataSource
    }
}

// MARK: - CodeServicing
extension CodeService: CodeServicing {
    func retrieveStep(completion: @escaping (Result<StepResponse, ApiError>) -> Void) {
        let decoder = JSONDecoder(.convertFromSnakeCase)
        let endpoint = FactorAuthenticationEndpoint.create(dataSource.flow, dataSource.phoneNumber)
        Api<StepResponse>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }

    func verify(_ code: String, completion: @escaping (Result<VerifyCodeResponse, ApiError>) -> Void) {
        let decoder = JSONDecoder(.convertFromSnakeCase)
        let endpoint = FactorAuthenticationEndpoint.verify(code)
        Api<VerifyCodeResponse>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
