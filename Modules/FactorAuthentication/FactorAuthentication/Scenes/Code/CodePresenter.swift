import Core
import Foundation
import UI

protocol CodePresenting: AnyObject {
    var viewController: CodeDisplaying? { get set }
    func didNextStep(action: CodeAction)
    func displayCode(response: CodeResponse)
    func displayTime(time: Int)
    func displayResendLoading()
    func displayResend()
    func displayCodeLoading()
    func displayCodeError(message: String)
    func dismissCodeError()
    func displayResendError(message: String)
    func displayKeyboard()
    func dismissKeyboard()
    func displayNetworkBanner()
    func dismissNetworkBanner()
}

final class CodePresenter {
    private let coordinator: CodeCoordinating
    weak var viewController: CodeDisplaying?

    init(coordinator: CodeCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - CodePresenting
extension CodePresenter: CodePresenting {
    func didNextStep(action: CodeAction) {
        coordinator.perform(action: action)
    }

    func displayCode(response: CodeResponse) {
        viewController?.displayNavigationTitle(response.navigationTitle)
        viewController?.displayTitle(response.title)
        viewController?.displayDescription(formatWithBold(with: response.description))
        viewController?.displayPlaceholder(response.placeholder)
        let resendTitle = format(message: response.resendTitle,
                                 color: Colors.branding600.color,
                                 underlined: true)
        viewController?.setResendTitle(resendTitle)
        viewController?.displayKeyboard()
    }

    func displayTime(time: Int) {
        let attributeMessage = format(message: Strings.Code.timer(time))
        viewController?.displayTime(attributeMessage)
    }

    func displayResendLoading() {
        let attributeMessage = format(message: Strings.Code.wait)
        viewController?.displayWaiting(attributeMessage)
        viewController?.dismissKeyboard()
    }

    func displayResend() {
        viewController?.displayResend()
    }

    func displayCodeLoading() {
        viewController?.displayCodeLoading()
        viewController?.dismissKeyboard()
    }

    func displayCodeError(message: String) {
        viewController?.displayCodeError(message)
    }

    func dismissCodeError() {
        viewController?.dismissCodeError()
    }

    func displayResendError(message: String) {
        viewController?.displayResendError(message)
    }

    func displayKeyboard() {
        viewController?.displayKeyboard()
    }

    func dismissKeyboard() {
        viewController?.dismissKeyboard()
    }

    func displayNetworkBanner() {
        viewController?.displayNetworkBanner()
    }

    func dismissNetworkBanner() {
        viewController?.dismissNetworkBanner()
    }
}

// MARK: - Private Methods
private extension CodePresenter {
    func formatWithBold(with message: String) -> NSAttributedString {
        let message = message
            .replacingOccurrences(of: "<br/>", with: "\n")
            .replacingOccurrences(of: "<b>", with: "[b]")
            .replacingOccurrences(of: "</b>", with: "[b]")
        let attributeMessage = message.attributedStringWithFont(
            primary: Typography.bodyPrimary().font(),
            secondary: Typography.bodyPrimary(.highlight).font(),
            separator: "[b]")

        return attributeMessage
    }

    func format(message: String,
                color: UIColor = Colors.grayscale500.color,
                underlined: Bool = false) -> NSAttributedString {
        let attr: [NSAttributedString.Key: Any] = [
            .foregroundColor: color,
            .underlineStyle: underlined
        ]
        return NSAttributedString(string: message, attributes: attr)
    }
}
