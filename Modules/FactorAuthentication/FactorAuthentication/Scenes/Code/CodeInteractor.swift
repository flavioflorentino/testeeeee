import AnalyticsModule
import Core
import Foundation

protocol CodeInteracting: AnyObject {
    func trackStarted()
    func close()
    func prepareForReceiveCode()
    func resendCode()
    func verifyCode(_ code: String?)
}

final class CodeInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    private let service: CodeServicing
    private let presenter: CodePresenting
    private let dataSource: FactorAuthenticationDataSource

    private let codeSize = 6
    private var codeResponse: CodeResponse
    private var currentTimer: Timer?
    private let startedDate = Date()
    private var isTypingLogSent = false
    private let timerProvider: Timer.Type

    init(dependencies: Dependencies,
         service: CodeServicing,
         presenter: CodePresenting,
         dataSource: FactorAuthenticationDataSource,
         codeResponse: CodeResponse,
         timerProvider: Timer.Type = Timer.self) {
        self.dependencies = dependencies
        self.service = service
        self.presenter = presenter
        self.dataSource = dataSource
        self.codeResponse = codeResponse
        self.timerProvider = timerProvider
    }
}

// MARK: - CodeInteracting
extension CodeInteractor: CodeInteracting {
    func trackStarted() {
        trackEvent(.methodChosen)
    }

    func close() {
        trackEvent(.canceled)
        presenter.didNextStep(action: .close)
    }

    func prepareForReceiveCode() {
        presenter.displayCode(response: codeResponse)
        prepareTimer()
    }

    func resendCode() {
        presenter.displayResendLoading()
        service.retrieveStep { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let response):
                self.handleResendSuccess(response)
            case .failure(let error):
                self.handleResendFailure(error)
            }
        }
    }

    func verifyCode(_ code: String?) {
        guard let code = code else {
            return
        }
        if !isTypingLogSent {
            isTypingLogSent = true
            let timeInterval = Date().timeIntervalSince(startedDate)
            let seconds = Int(timeInterval.truncatingRemainder(dividingBy: 60))
            trackEvent(.codeDeliveryTime(seconds))
        }
        guard code.count == codeSize else {
            return
        }
        presenter.displayCodeLoading()
        service.verify(code) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let response):
                self.trackEvent(.success)
                let action = CodeAction.complete(response.hash)
                self.presenter.didNextStep(action: action)
            case .failure(let error):
                self.handleCodeFailure(error)
            }
        }
    }
}

private extension CodeInteractor {
    func prepareTimer() {
        if currentTimer?.isValid == true {
            currentTimer?.invalidate()
        }
        var time = codeResponse.timeout
        presenter.displayTime(time: time)
        currentTimer = timerProvider.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
            guard time > 1 else {
                self.presenter.displayResend()
                timer.invalidate()
                return
            }
            time -= 1
            self.presenter.displayTime(time: time)
        }
    }

    func handleResendSuccess(_ response: StepResponse) {
        if case .code = response.type {
            trackEvent(.codeResended)
            handleResend(code: response.code)
        }
    }

    func handleResend(code: CodeResponse?) {
        guard let code = code else {
            handleResendFailure(.unknown(nil))
            return
        }

        codeResponse = code
        prepareForReceiveCode()
    }

    func handleResendFailure(_ error: ApiError) {
        switch error {
        case .connectionFailure, .timeout:
            trackEvent(.error(.requestFailed))
            presenter.displayNetworkBanner()
        case .badRequest(let body):
            trackError(body.code)
            let errorMessage = body.handleBadRequest(expectedKey: "flow")
                ?? Strings.Dialog.Error.Message.default
            presenter.displayResendError(message: errorMessage)
        default:
            trackEvent(.error(.generic))
            presenter.displayResendError(message: Strings.Dialog.Error.Message.default)
        }
    }

    func handleCodeFailure(_ error: ApiError) {
        switch error {
        case .connectionFailure, .timeout:
            trackEvent(.error(.requestFailed))
            presenter.displayNetworkBanner()
        case .badRequest(let body):
            trackError(body.code)
            let errorMessage = body.handleBadRequest(expectedKey: "code")
                ?? Strings.Code.Error.Message.default
            presenter.displayCodeError(message: errorMessage)
        default:
            trackEvent(.error(.generic))
            presenter.displayCodeError(message: Strings.Code.Error.Message.default)
        }
        presenter.displayKeyboard()
    }

    func trackError(_ code: String) {
        let error = BadRequestErrors(rawValue: code)
        switch error {
        case .expired:
            trackEvent(.codeExpired)
        case .invalid:
            trackEvent(.codeIncorrect)
        case .generic:
            trackEvent(.error(.generic))
        }
    }

    func trackEvent(_ event: CodeEvents) {
        let event = Tracker(flow: dataSource.flow, eventType: event)
        dependencies.analytics.log(event)
    }
}
