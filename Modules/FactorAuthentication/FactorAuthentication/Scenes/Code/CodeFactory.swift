import UIKit

enum CodeFactory {
    static func make(delegate: FactorAuthenticationCoordinating?,
                     dataSource: FactorAuthenticationDataSource,
                     codeResponse: CodeResponse) -> CodeViewController {
        let container = DependencyContainer()
        let service: CodeServicing = CodeService(dependencies: container, dataSource: dataSource)
        let coordinator: CodeCoordinating = CodeCoordinator(delegate: delegate)
        let presenter: CodePresenting = CodePresenter(coordinator: coordinator)
        let interactor = CodeInteractor(dependencies: container, service: service, presenter: presenter, dataSource: dataSource, codeResponse: codeResponse)
        let viewController = CodeViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
