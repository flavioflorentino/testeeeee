import UIKit

enum PasswordFactory {
    static func make(delegate: FactorAuthenticationCoordinating?,
                     dataSource: FactorAuthenticationDataSource,
                     passwordResponse: PasswordResponse) -> PasswordViewController {
        let container = DependencyContainer()
        let service: PasswordServicing = PasswordService(dependencies: container)
        let coordinator: PasswordCoordinating = PasswordCoordinator(delegate: delegate)
        let presenter: PasswordPresenting = PasswordPresenter(coordinator: coordinator)
        let interactor = PasswordInteractor(service: service,
                                            presenter: presenter,
                                            dependencies: container,
                                            passwordResponse: passwordResponse,
                                            dataSource: dataSource)
        let viewController = PasswordViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
