import Core
import Foundation
import UI

protocol PasswordPresenting: AnyObject {
    var viewController: PasswordDisplaying? { get set }
    func didNextStep(action: PasswordAction)
    func displayPassword(response: PasswordResponse)
    func enableActionButton()
    func disableActionButton()
    func startLoading()
    func stopLoading()
    func displayKeyboard()
    func dismissKeyboard()
    func displayError(message: String)
    func dismissError()
    func displayNetworkBanner()
    func dismissNetworkBanner()
}

final class PasswordPresenter {
    private let coordinator: PasswordCoordinating
    weak var viewController: PasswordDisplaying?

    init(coordinator: PasswordCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - PasswordPresenting
extension PasswordPresenter: PasswordPresenting {
    func didNextStep(action: PasswordAction) {
        coordinator.perform(action: action)
    }

    func displayPassword(response: PasswordResponse) {
        viewController?.displayNavigationTitle(response.navigationTitle)
        viewController?.displayDescription(formatWithBold(with: response.description))
        viewController?.displayPlaceholder(response.placeholder)
        viewController?.setActionTitle(response.confirmTitle)
        viewController?.disableActionButton()
        viewController?.displayKeyboard()
    }

    func enableActionButton() {
        viewController?.enableActionButton()
    }

    func disableActionButton() {
        viewController?.disableActionButton()
    }

    func startLoading() {
        viewController?.startLoading()
    }

    func stopLoading() {
        viewController?.stopLoading()
    }

    func displayKeyboard() {
        viewController?.displayKeyboard()
    }

    func dismissKeyboard() {
        viewController?.dismissKeyboard()
    }

    func displayError(message: String) {
        viewController?.displayError(message)
    }

    func dismissError() {
        viewController?.dismissError()
    }

    func displayNetworkBanner() {
        viewController?.displayNetworkBanner()
    }

    func dismissNetworkBanner() {
        viewController?.dismissNetworkBanner()
    }
}

// MARK: - Private Methods
private extension PasswordPresenter {
    func formatWithBold(with message: String) -> NSAttributedString {
        let message = message
            .replacingOccurrences(of: "<br/>", with: "\n")
            .replacingOccurrences(of: "<b>", with: "[b]")
            .replacingOccurrences(of: "</b>", with: "[b]")
        let attributeMessage = message.attributedStringWithFont(
            primary: Typography.bodyPrimary().font(),
            secondary: Typography.bodyPrimary(.highlight).font(),
            separator: "[b]")

        return attributeMessage
    }
}
