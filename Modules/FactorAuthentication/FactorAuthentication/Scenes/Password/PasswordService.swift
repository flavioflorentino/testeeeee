import Core
import Foundation

protocol PasswordServicing {
    func verify(_ password: String, completion: @escaping (Result<VerifyCodeResponse, ApiError>) -> Void)
}

final class PasswordService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - PasswordServicing
extension PasswordService: PasswordServicing {
    func verify(_ password: String, completion: @escaping (Result<VerifyCodeResponse, ApiError>) -> Void) {
        let decoder = JSONDecoder(.convertFromSnakeCase)
        let endpoint = FactorAuthenticationEndpoint.verify(password)
        Api<VerifyCodeResponse>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
