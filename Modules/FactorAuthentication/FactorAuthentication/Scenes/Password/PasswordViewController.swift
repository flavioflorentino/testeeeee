import AssetsKit
import UI
import UIKit
import SnapKit

protocol PasswordDisplaying: AnyObject {
    func displayNavigationTitle(_ title: String)
    func displayDescription(_ description: NSAttributedString)
    func displayPlaceholder(_ placeholder: String)
    func setActionTitle(_ title: String)
    func enableActionButton()
    func disableActionButton()
    func startLoading()
    func stopLoading()
    func displayKeyboard()
    func dismissKeyboard()
    func displayError(_ message: String)
    func dismissError()
    func displayNetworkBanner()
    func dismissNetworkBanner()
}

private extension PasswordViewController.Layout {
    enum Animation {
        static let time = 0.3
    }
}

final class PasswordViewController: ViewController<PasswordInteracting, UIView> {
    fileprivate enum Layout { }
    private lazy var keyboardHandler = KeyboardHandler()
    private lazy var scrollView = UIScrollView()
    private lazy var containerView = UIView()

    private lazy var closeButton: UIButton = {
        let button = UIButton()
        button.setImage(Resources.Icons.icoClose.image, for: .normal)
        button.addTarget(self, action: #selector(didTapCloseButton), for: .touchUpInside)
        return button
    }()

    private lazy var networkBanner: NetworkBannerView = {
        let view = NetworkBannerView()
        view.isHidden = true
        return view
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale750())
        return label
    }()
    private lazy var passwordToggleButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(Resources.Icons.icoPasswordInvisible.image, for: .normal)
        button.setImage(Resources.Icons.icoPasswordVisible.image, for: .highlighted)
        button.addTarget(self, action: #selector(didUnsecurePassword(_:)), for: .touchDown)
        button.addTarget(self, action: #selector(didSecurePassword(_:)), for: [.touchUpInside, .touchDragInside])
        return button
    }()
    private lazy var passwordTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholderColor = Colors.grayscale400.color
        textField.titleColor = Colors.grayscale400.color
        textField.selectedTitleColor = Colors.branding600.color
        textField.textColor = Colors.grayscale750.color
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField.isSecureTextEntry = true
        textField.rightView = passwordToggleButton
        textField.rightViewMode = .always
        return textField
    }()
    private lazy var errorLabel: UILabel = {
        let label = UILabel()
        label.isHidden = true
        label.labelStyle(CaptionLabelStyle())
            .with(\.textColor, .critical900())
        return label
    }()
    private lazy var passwordStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [passwordTextField, errorLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    private lazy var containerStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            networkBanner,
            descriptionLabel,
            SpacerView(size: Spacing.base03),
            passwordStackView
        ])
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        return stackView
    }()

    private lazy var actionButton: UIButton = {
        let button = UIButton(type: .custom)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTapAction(_:)), for: .touchUpInside)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.trackStarted()
        interactor.prepareInfo()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.setHidesBackButton(true, animated: false)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.setHidesBackButton(false, animated: false)
    }

    override func buildViewHierarchy() {
        containerView.addSubview(containerStackView)
        containerView.addSubview(actionButton)
        scrollView.addSubview(containerView)
        view.addSubview(scrollView)
    }

    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.top.equalTo(view.compatibleSafeArea.top)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom)
        }

        containerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.height.equalToSuperview().priority(.low)
            $0.width.equalToSuperview()
        }

        containerStackView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.top.equalToSuperview().offset(Spacing.base02)
        }

        actionButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.top.greaterThanOrEqualTo(containerStackView.snp.bottom).offset(Spacing.base04)
            $0.bottom.equalToSuperview().inset(Spacing.base06)
            $0.height.equalTo(Spacing.base06)
        }
    }

    override func configureViews() {
        view.backgroundColor = .backgroundPrimary()
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: closeButton)
        networkBanner.setup { [weak self] _ in
            self?.dismissNetworkBanner()
        }
        keyboardHandler.handle { [weak self] keyboardSize in
            guard let self = self else { return }
            self.scrollView.snp.updateConstraints {
                $0.bottom.equalTo(self.view.compatibleSafeArea.bottom).inset(keyboardSize)
            }
            self.view.layoutIfNeeded()
        }
    }
}

// MARK: - PasswordDisplaying
extension PasswordViewController: PasswordDisplaying {
    func displayNavigationTitle(_ title: String) {
        self.title = title
    }

    func displayDescription(_ description: NSAttributedString) {
        descriptionLabel.attributedText = description
    }

    func displayPlaceholder(_ placeholder: String) {
        passwordTextField.placeholder = placeholder
        passwordTextField.title = placeholder
        passwordTextField.selectedTitle = placeholder
    }

    func setActionTitle(_ title: String) {
        actionButton.setTitle(title, for: .normal)
    }

    func enableActionButton() {
        actionButton.isEnabled = true
    }

    func disableActionButton() {
        actionButton.isEnabled = false
    }

    func startLoading() {
        actionButton.startLoading()
    }

    func stopLoading() {
        actionButton.stopLoading()
    }

    func displayKeyboard() {
        passwordTextField.isUserInteractionEnabled = true
        passwordTextField.becomeFirstResponder()
    }

    func dismissKeyboard() {
        passwordTextField.isUserInteractionEnabled = false
        passwordTextField.resignFirstResponder()
    }

    func displayError(_ message: String) {
        errorStatus(true, with: message)
    }

    func dismissError() {
        errorStatus(false)
    }

    func displayNetworkBanner() {
        networkBanner(isHidden: false)
    }

    func dismissNetworkBanner() {
        networkBanner(isHidden: true)
    }
}

@objc
private extension PasswordViewController {
    func didTapCloseButton(_ sender: UIButton) {
        interactor.close()
    }

    func didUnsecurePassword(_ sender: UIButton) {
        interactor.trackPasswordExhibition()
        passwordTextField.isSecureTextEntry = false
    }

    func didSecurePassword(_ sender: UIButton) {
        passwordTextField.isSecureTextEntry = true
    }

    func textFieldDidChange(_ textField: UITextField) {
        interactor.validate(password: passwordTextField.text)
    }

    func didTapAction(_ sender: UIButton) {
        interactor.confirm(password: passwordTextField.text)
    }

    func errorStatus(_ hasError: Bool, with message: String = "") {
        if hasError {
            passwordTextField.lineColor = Colors.critical900.color
            passwordTextField.selectedLineColor = Colors.critical900.color
            passwordTextField.titleColor = Colors.critical900.color
            passwordTextField.selectedTitleColor = Colors.critical900.color
        } else {
            passwordTextField.lineColor = Colors.grayscale400.color
            passwordTextField.selectedLineColor = Colors.branding300.color
            passwordTextField.titleColor = Colors.grayscale400.color
            passwordTextField.selectedTitleColor = Colors.branding600.color
        }

        errorLabel.isHidden = !hasError
        errorLabel.text = message

        UIView.animate(withDuration: Layout.Animation.time) {
            self.view.layoutIfNeeded()
        }
    }

    func networkBanner(isHidden: Bool) {
        UIView.animate(withDuration: Layout.Animation.time) {
            self.networkBanner.isHidden = isHidden
        }
    }
}
