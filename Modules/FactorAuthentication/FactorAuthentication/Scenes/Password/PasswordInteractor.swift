import AnalyticsModule
import Core
import Foundation

protocol PasswordInteracting: AnyObject {
    func trackStarted()
    func close()
    func prepareInfo()
    func validate(password: String?)
    func confirm(password: String?)
    func trackPasswordExhibition()
}

final class PasswordInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let service: PasswordServicing
    private let presenter: PasswordPresenting
    private let passwordResponse: PasswordResponse
    private let dataSource: FactorAuthenticationDataSource

    init(service: PasswordServicing,
         presenter: PasswordPresenting,
         dependencies: Dependencies,
         passwordResponse: PasswordResponse,
         dataSource: FactorAuthenticationDataSource) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.passwordResponse = passwordResponse
        self.dataSource = dataSource
    }
}

// MARK: - PasswordInteracting
extension PasswordInteractor: PasswordInteracting {
    func trackStarted() {
        trackEvent(.methodChosen)
    }
    
    func close() {
        trackEvent(.canceled)
        presenter.didNextStep(action: .close)
    }

    func prepareInfo() {
        presenter.displayPassword(response: passwordResponse)
    }

    func validate(password: String?) {
        guard let password = password, !password.isEmpty else {
            presenter.disableActionButton()
            return
        }
        presenter.enableActionButton()
    }

    func confirm(password: String?) {
        guard let password = password else {
            return
        }
        presenter.dismissKeyboard()
        presenter.startLoading()
        service.verify(password) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let response):
                self.trackEvent(.success)
                let action = PasswordAction.complete(response.hash)
                self.presenter.didNextStep(action: action)
            case .failure(let error):
                self.handleFailure(error)
            }
        }
    }
    
    func trackPasswordExhibition() {
        trackEvent(.passwordExhibition)
    }
}

// MARK: - Private Methods
private extension PasswordInteractor {
    func handleFailure(_ error: ApiError) {
        switch error {
        case .connectionFailure, .timeout:
            trackEvent(.error(.requestFailed))
            presenter.displayNetworkBanner()
        case .badRequest(let body):
            trackError(body.code)
            let errorMessage = body.handleBadRequest(expectedKey: "code")
                ?? Strings.Password.Error.Message.default
            presenter.displayError(message: errorMessage)
        default:
            trackEvent(.error(.generic))
            presenter.displayError(message: Strings.Password.Error.Message.default)
        }
        presenter.displayKeyboard()
        presenter.stopLoading()
    }
    
    func trackError(_ code: String) {
        let error = BadRequestErrors(rawValue: code)
        switch error {
        case .invalid:
            trackEvent(.passwordIncorrect)
        default:
            trackEvent(.error(.generic))
        }
    }
    
    func trackEvent(_ event: PasswordEvents) {
        let event = Tracker(flow: dataSource.flow, eventType: event)
        dependencies.analytics.log(event)
    }
}
