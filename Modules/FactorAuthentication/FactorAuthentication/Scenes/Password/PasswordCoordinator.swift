import UIKit

enum PasswordAction: Equatable {
    case close
    case complete(_ hash: String)
}

protocol PasswordCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: PasswordAction)
}

final class PasswordCoordinator {
    weak var viewController: UIViewController?
    private weak var delegate: FactorAuthenticationCoordinating?

    init(delegate: FactorAuthenticationCoordinating?) {
        self.delegate = delegate
    }
}

// MARK: - PasswordCoordinating
extension PasswordCoordinator: PasswordCoordinating {
    func perform(action: PasswordAction) {
        switch action {
        case .close:
            delegate?.close()
        case .complete(let hash):
            delegate?.complete(with: hash)
        }
    }
}
