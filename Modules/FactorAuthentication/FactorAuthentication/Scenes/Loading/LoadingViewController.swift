import AssetsKit
import SnapKit
import UI
import UIKit

protocol LoadingDisplaying: AnyObject {
    func displayLoading()
    func displayError()
}

extension LoadingViewController.Layout {
    enum Alert {
        static let backgroundColor = UIColor.black.withAlphaComponent(0.15)
    }
}

final class LoadingViewController: ViewController<LoadingInteracting, UIView> {
    fileprivate enum Layout {}

    private lazy var loader: UIActivityIndicatorView = {
        let loader = UIActivityIndicatorView()
        loader.color = Colors.black.lightColor
        // We can't change the size with constraints, so we need to scale the size
        loader.transform = CGAffineTransform(scaleX: 2, y: 2)
        return loader
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.Loading.title
        label.typography = .bodyPrimary()
        label.textAlignment = .center
        label.textColor = Colors.grayscale700.lightColor
        label.numberOfLines = 2
        return label
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.retrieveStep()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func buildViewHierarchy() {
        view.addSubview(loader)
        view.addSubview(titleLabel)
    }

    override func configureViews() {
        view.backgroundColor = .backgroundPrimary()
    }

    override func setupConstraints() {
        loader.snp.makeConstraints {
            $0.center.equalToSuperview()
        }

        titleLabel.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base04)
            $0.top.equalTo(loader.snp.bottom).offset(Spacing.base03)
        }
    }
}

// MARK: - LoadingDisplaying
extension LoadingViewController: LoadingDisplaying {
    func displayLoading() {
        loader.startAnimating()
    }

    func displayError() {
        dialogError(with: Strings.Dialog.Error.Message.default,
                    primaryAction: { [weak self] in
                        self?.interactor.retrieveStep()
                    }, secondaryAction: { [weak self] in
                        self?.interactor.close()
                    })
    }
}
