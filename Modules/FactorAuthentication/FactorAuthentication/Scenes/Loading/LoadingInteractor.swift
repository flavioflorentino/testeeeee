import AnalyticsModule
import Core
import Foundation

protocol LoadingInteracting: AnyObject {
    func close()
    func retrieveStep()
}

final class LoadingInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    private let service: LoadingServicing
    private let presenter: LoadingPresenting
    private let dataSource: FactorAuthenticationDataSource

    init(dependencies: Dependencies,
         service: LoadingServicing,
         presenter: LoadingPresenting,
         dataSource: FactorAuthenticationDataSource) {
        self.dependencies = dependencies
        self.service = service
        self.presenter = presenter
        self.dataSource = dataSource
    }
}

// MARK: - LoadingInteracting
extension LoadingInteractor: LoadingInteracting {
    func close() {
        trackEvent(.canceled)
        presenter.didNextStep(action: .close)
    }

    func retrieveStep() {
        trackEvent(.started)
        presenter.displayLoading()
        service.retrieveStep { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let response):
                self.handleSuccess(response)
            case .failure:
                self.trackEvent(.error(.requestFailed))
                self.presenter.displayError()
            }
        }
    }
}

private extension LoadingInteractor {
    func handleSuccess(_ response: StepResponse) {
        switch response.type {
        case .code:
            handle(code: response.code)
        case .password:
            handle(password: response.password)
        }
    }

    func handle(code: CodeResponse?) {
        guard let codeResponse = code else {
            presenter.displayError()
            return
        }
        presenter.didNextStep(action: .code(response: codeResponse))
    }

    func handle(password: PasswordResponse?) {
        guard let passwordResponse = password else {
            presenter.displayError()
            return
        }
        presenter.didNextStep(action: .password(response: passwordResponse))
    }

    func trackEvent(_ event: LoadingEvents) {
        let event = Tracker(flow: dataSource.flow, eventType: event)
        dependencies.analytics.log(event)
    }
}
