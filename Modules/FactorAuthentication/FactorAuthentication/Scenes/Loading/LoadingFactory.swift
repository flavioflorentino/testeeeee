import UIKit

enum LoadingFactory {
    static func make(delegate: FactorAuthenticationCoordinating,
                     dataSource: FactorAuthenticationDataSource) -> LoadingViewController {
        let container = DependencyContainer()
        let service: LoadingServicing = LoadingService(dependencies: container, dataSource: dataSource)
        let coordinator: LoadingCoordinating = LoadingCoordinator(delegate: delegate, dataSource: dataSource)
        let presenter: LoadingPresenting = LoadingPresenter(coordinator: coordinator)
        let interactor = LoadingInteractor(dependencies: container, service: service, presenter: presenter, dataSource: dataSource)
        let viewController = LoadingViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
