import Foundation

protocol LoadingPresenting: AnyObject {
    var viewController: LoadingDisplaying? { get set }
    func didNextStep(action: LoadingAction)
    func displayLoading()
    func displayError()
}

final class LoadingPresenter {
    private let coordinator: LoadingCoordinating
    weak var viewController: LoadingDisplaying?

    init(coordinator: LoadingCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - LoadingPresenting
extension LoadingPresenter: LoadingPresenting {
    func didNextStep(action: LoadingAction) {
        coordinator.perform(action: action)
    }

    func displayLoading() {
        viewController?.displayLoading()
    }

    func displayError() {
        viewController?.displayError()
    }
}
