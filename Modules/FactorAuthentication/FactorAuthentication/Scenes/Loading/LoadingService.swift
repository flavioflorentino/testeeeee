import Core
import Foundation

protocol LoadingServicing {
    func retrieveStep(completion: @escaping (Result<StepResponse, ApiError>) -> Void)
}

final class LoadingService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    private let dataSource: FactorAuthenticationDataSource

    init(dependencies: Dependencies, dataSource: FactorAuthenticationDataSource) {
        self.dependencies = dependencies
        self.dataSource = dataSource
    }
}

// MARK: - LoadingServicing
extension LoadingService: LoadingServicing {
    func retrieveStep(completion: @escaping (Result<StepResponse, ApiError>) -> Void) {
        let decoder = JSONDecoder(.convertFromSnakeCase)
        let endpoint = FactorAuthenticationEndpoint.create(dataSource.flow, dataSource.phoneNumber)
        Api<StepResponse>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
