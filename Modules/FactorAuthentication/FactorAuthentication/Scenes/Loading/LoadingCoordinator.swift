import UIKit

enum LoadingAction: Equatable {
    case close
    case code(response: CodeResponse)
    case password(response: PasswordResponse)
}

protocol LoadingCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: LoadingAction)
}

final class LoadingCoordinator {
    weak var viewController: UIViewController?
    private weak var delegate: FactorAuthenticationCoordinating?
    private let dataSource: FactorAuthenticationDataSource

    init(delegate: FactorAuthenticationCoordinating,
         dataSource: FactorAuthenticationDataSource) {
        self.delegate = delegate
        self.dataSource = dataSource
    }
}

// MARK: - LoadingCoordinating
extension LoadingCoordinator: LoadingCoordinating {
    func perform(action: LoadingAction) {
        switch action {
        case .close:
            delegate?.close()
        case .code(let response):
            let controller = CodeFactory.make(delegate: delegate,
                                              dataSource: dataSource,
                                              codeResponse: response)
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case .password(let response):
            let controller = PasswordFactory.make(delegate: delegate,
                                                  dataSource: dataSource,
                                                  passwordResponse: response)
            viewController?.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
