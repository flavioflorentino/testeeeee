import UI
import UIKit

public protocol FactorAuthenticationDelegate: AnyObject {
    func factorAuthenticationWasClosed()
    func factorAuthenticationWasCompleted(with hash: String)
}

public protocol FactorAuthenticationDataSource: AnyObject {
    var flow: FactorAuthenticationFlow { get }
    var phoneNumber: String? { get }
}

public extension FactorAuthenticationDataSource {
    var phoneNumber: String? { nil }
}

protocol FactorAuthenticationCoordinating: AnyObject {
    func start()
    func close()
    func complete(with hash: String)
}

public final class FactorAuthenticationCoordinator {
    private let originViewController: UIViewController
    private let navigationController = UINavigationController()
    private weak var delegate: FactorAuthenticationDelegate?
    private let dataSource: FactorAuthenticationDataSource

    public init(from originViewController: UIViewController,
                delegate: FactorAuthenticationDelegate,
                dataSource: FactorAuthenticationDataSource) {
        self.originViewController = originViewController
        self.delegate = delegate
        self.dataSource = dataSource
    }
}

extension FactorAuthenticationCoordinator: FactorAuthenticationCoordinating {
    public func start() {
        startLoading()
    }

    func close() {
        navigationController.dismiss(animated: true) {
            self.delegate?.factorAuthenticationWasClosed()
        }
    }

    func complete(with hash: String) {
        navigationController.dismiss(animated: true) {
            self.delegate?.factorAuthenticationWasCompleted(with: hash)
        }
    }
}

private extension FactorAuthenticationCoordinator {
    func startLoading() {
        let controller = LoadingFactory.make(delegate: self, dataSource: dataSource)
        navigationController.pushViewController(controller, animated: false)
        if #available(iOS 13.0, *) {
            navigationController.isModalInPresentation = true
        }
        originViewController.present(navigationController, animated: true)
    }
}
