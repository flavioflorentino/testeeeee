import Foundation

// swiftlint:disable convenience_type
final class FactorAuthenticationResources {    
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: FactorAuthenticationResources.self)
                .url(forResource: "FactorAuthenticationResources", withExtension: "bundle") else {
            return Bundle(for: FactorAuthenticationResources.self)
        }
        return Bundle(url: url) ?? Bundle(for: FactorAuthenticationResources.self)
    }()
}
