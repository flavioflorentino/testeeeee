import Core
import Foundation

enum FactorAuthenticationEndpoint {
    case create(_ flow: FactorAuthenticationFlow, _ phoneNumber: String? = nil)
    case verify(_ code: String)
}

extension FactorAuthenticationEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .create:
            return "/fa/create"
        case .verify:
            return "/fa/send"
        }
    }

    var method: HTTPMethod {
        .post
    }

    var body: Data? {
        switch self {
        case let .create(flow, phoneNumber):
            var body = ["flow": flow.rawValue]

            if let phoneNumber = phoneNumber {
                body["phone_number"] = phoneNumber
            }

            return body.toData()
        case .verify(let code):
            return ["code": code].toData()
        }
    }
}
