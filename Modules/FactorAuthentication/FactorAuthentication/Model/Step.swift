import Foundation

struct StepResponse: Decodable {
    let type: FactorAuthenticationStep
    let code: CodeResponse?
    let password: PasswordResponse?
}

struct CodeResponse: Decodable, Equatable {
    let navigationTitle: String
    let title: String
    let description: String
    let placeholder: String
    let resendTitle: String
    let timeout: Int
}

struct PasswordResponse: Decodable, Equatable {
    let navigationTitle: String
    let description: String
    let placeholder: String
    let confirmTitle: String
}
