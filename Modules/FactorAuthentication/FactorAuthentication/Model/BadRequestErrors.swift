import Foundation

enum BadRequestErrors {
    case expired
    case invalid
    case generic

    init(rawValue: String) {
        switch rawValue {
        case "fa_expired":
            self = .expired
        case "fa_invalid":
            self = .invalid
        default:
            self = .generic
        }
    }
}
