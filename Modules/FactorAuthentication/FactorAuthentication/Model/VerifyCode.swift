import Foundation

struct VerifyCodeResponse: Decodable {
    let hash: String
}
