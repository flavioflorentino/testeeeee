import Foundation

enum FactorAuthenticationStep: String, Decodable {
    case code
    case password
}
