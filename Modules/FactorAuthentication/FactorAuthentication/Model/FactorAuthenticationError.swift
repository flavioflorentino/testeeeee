import Foundation

struct AuthorizationError: Decodable {
    let meta: AuthorizationMetaError
}

struct AuthorizationMetaError: Decodable {
    let code: Int
    let errorType: String
    let errorMessage: String
}

struct FactorAuthenticationPJError: Decodable {
    let code: String
    let message: String
    let errors: [String: [String]]

    enum CodingKeys: String, CodingKey {
        case code
        case message
        case errors = "errorsMessage"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.code = try container.decode(String.self, forKey: .code)
        self.message = try container.decode(String.self, forKey: .message)
        self.errors = try container.decodeIfPresent([String: [String]].self, forKey: .errors) ?? [:]
    }
}
