import AssetsKit
import UI
import UIKit

extension UIViewController {
    func dialogError(with message: String,
                     primaryAction: @escaping () -> Void,
                     secondaryAction: @escaping () -> Void = {}) {
        let alert = PopupViewController(
            title: Strings.Dialog.Error.title,
            description: message,
            image: Resources.Illustrations.iluError.image
        )
        alert.hideCloseButton = true
        let fillButton = PopupAction(title: Strings.Dialog.Error.Try.again, style: .fill, completion: primaryAction)
        let linkButton = PopupAction(title: Strings.Dialog.Error.skip, style: .link, completion: secondaryAction)
        alert.addAction(fillButton)
        alert.addAction(linkButton)
        showPopup(alert)
    }
}
