import UIKit

final class KeyboardHandler: NSObject {
    private var completion: (CGFloat) -> Void = { _ in }

    func handle(completion: @escaping (CGFloat) -> Void) {
        self.completion = completion
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow(_:)),
            name: UIWindow.keyboardWillShowNotification,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide(_:)),
            name: UIWindow.keyboardWillHideNotification,
            object: nil)
    }

    @objc
    private func keyboardWillShow(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo,
            let keyboardSize = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            else {
                return
        }
        updateKeyboardConstraint(userInfo: userInfo, inset: keyboardSize.size.height)
    }

    @objc
    private func keyboardWillHide(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo else {
            return
        }
        updateKeyboardConstraint(userInfo: userInfo)
    }

    private func updateKeyboardConstraint(userInfo: [AnyHashable: Any], inset: CGFloat = 0.0) {
        guard let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue,
            let animationCurve = (userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber)?.uintValue
            else {
                return
        }

        UIView.animate(withDuration: animationDuration,
                       delay: 0,
                       options: UIView.AnimationOptions(rawValue: animationCurve),
                       animations: {
                        self.completion(inset)
                       }, completion: nil)
    }
}
