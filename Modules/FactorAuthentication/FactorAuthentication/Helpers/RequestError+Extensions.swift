import Core
import Foundation

extension RequestError {
    func handleBadRequest(expectedKey key: String) -> String? {
        let decoder = JSONDecoder(.convertFromSnakeCase)
        guard let data = jsonData,
              let body = try? decoder.decode(FactorAuthenticationPJError.self,
                                             from: data),
              let value = body.errors[key]?.first,
              value.isNotEmpty else {
            return nil
        }
        return value
    }
}
