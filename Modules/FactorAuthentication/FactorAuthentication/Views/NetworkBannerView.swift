import SnapKit
import UI
import UIKit

extension NetworkBannerView.Layout {
    enum Buttons {
        static let size: CGFloat = 16.0
    }
}

final class NetworkBannerView: UIView, ViewConfiguration {
    fileprivate struct Layout {}

    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.critical400.color
        view.cornerRadius = .light
        return view
    }()

    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.typography = .bodySecondary()
        label.numberOfLines = 0
        label.textColor = Colors.white.lightColor
        label.text = Strings.Network.unavailable
        return label
    }()

    private lazy var closeButton: UIButton = {
        let image = Assets.icoCloseBigGreen.image.withRenderingMode(.alwaysTemplate)
        let button = UIButton()
        button.setImage(image, for: .normal)
        button.tintColor = Colors.white.lightColor
        button.addTarget(self, action: #selector(didTapClose), for: .touchUpInside)
        return button
    }()

    private var handle: (UIView) -> Void = { _ in }

    init() {
        super.init(frame: .zero)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func buildViewHierarchy() {
        containerView.addSubview(messageLabel)
        containerView.addSubview(closeButton)
        addSubview(containerView)
    }

    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.leading.top.trailing.equalToSuperview()
            $0.bottom.equalToSuperview().offset(-Spacing.base03)
        }

        messageLabel.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.top.greaterThanOrEqualToSuperview().offset(Spacing.base02)
            $0.trailing.equalTo(closeButton.snp.leading).offset(-Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base02)
            $0.centerY.equalToSuperview()
        }

        closeButton.snp.makeConstraints {
            $0.trailing.equalToSuperview().offset(-Spacing.base01)
            $0.centerY.equalToSuperview()
            $0.size.equalTo(Layout.Buttons.size)
        }
    }

    func setup(action completion: @escaping (UIView) -> Void) {
        handle = completion
    }

    @objc
    private func didTapClose() {
        handle(self)
    }
}
