import Foundation

public enum FactorAuthenticationFlow: String, CaseIterable {
    case pixOptinEmail = "PIX_OPTIN_EMAIL"
    case pixOptinSMS = "PIX_OPTIN_SMS"
    case pixCashOut = "PIX_CASH_OUT"
    case pixCashOutChargeback = "PIX_CASH_OUT_CHARGEBACK"
    case sellerOptinWhatsapp = "SELLER_OPTIN_WHATSAPP"
}
