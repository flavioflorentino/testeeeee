import Foundation

enum ErrorType: Equatable {
    case requestFailed
    case generic

    var name: String {
        switch self {
        case .requestFailed:
            return "request-failed"
        case .generic:
            return "generic"
        }
    }
}
