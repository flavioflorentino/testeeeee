import Foundation

enum PasswordEvents: TrackerEventProtocol, Equatable {
    case methodChosen
    case passwordExhibition
    case passwordIncorrect
    case canceled
    case error(_ type: ErrorType)
    case success

    var name: String {
        switch self {
        case .methodChosen:
            return "Method Chosen"
        case .passwordExhibition:
            return "Password Exhibition"
        case .passwordIncorrect:
            return "Password Incorrect"
        case .canceled:
            return "Canceled"
        case .error:
            return "Error"
        case .success:
            return "Success"
        }
    }

    var properties: [String: Any] {
        guard case .error(let error) = self else {
            return [:]
        }
        return ["error_type": error.name]
    }
}
