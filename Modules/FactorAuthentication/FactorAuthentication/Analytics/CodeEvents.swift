import Foundation

enum CodeEvents: TrackerEventProtocol, Equatable {
    case methodChosen
    case codeDeliveryTime(_ time: Int)
    case codeResended
    case codeExpired
    case codeIncorrect
    case canceled
    case error(_ type: ErrorType)
    case success

    var name: String {
        switch self {
        case .methodChosen:
            return "Method Chosen"
        case .codeDeliveryTime:
            return "Code Delivery Time"
        case .codeResended:
            return "Code Resended"
        case .codeExpired:
            return "Code Expired"
        case .codeIncorrect:
            return "Code Incorrect"
        case .canceled:
            return "Canceled"
        case .error:
            return "Error"
        case .success:
            return "Success"
        }
    }

    var properties: [String: Any] {
        switch self {
        case .codeDeliveryTime(let time):
            return ["code_delivery_time": time]
        case .error(let error):
            return ["error_type": error.name]
        default:
            return [:]
        }
    }
}
