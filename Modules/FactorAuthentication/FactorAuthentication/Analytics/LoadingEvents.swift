import Foundation

enum LoadingEvents: TrackerEventProtocol, Equatable {
    case started
    case error(_ type: ErrorType)
    case canceled

    var name: String {
        switch self {
        case .started:
            return "Started"
        case .error:
            return "Error"
        case .canceled:
            return "Canceled"
        }
    }

    var properties: [String: Any] {
        if case .error(let error) = self {
            return ["error_type": error.name]
        }
        return [:]
    }
}
