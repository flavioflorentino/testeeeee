import AnalyticsModule
import Foundation

fileprivate extension FactorAuthenticationFlow {
    var event: String {
        switch self {
        case .pixOptinEmail, .pixOptinSMS:
            return "Pix-Optin"
        case .pixCashOut, .pixCashOutChargeback:
            return "Pix-CashOut"
        case .sellerOptinWhatsapp:
            return "WhatsApp-Optin"
        }
    }
    
    var method: String {
        switch self {
        case .pixOptinEmail:
            return "email"
        default:
            return "sms"
        }
    }
}

protocol TrackerEventProtocol {
    var name: String { get }
    var properties: [String: Any] { get }
}

struct Tracker: AnalyticsKeyProtocol {
    var flow: FactorAuthenticationFlow
    var eventType: TrackerEventProtocol

    private var properties: [String: Any] {
        var properties: [String: Any] = ["flow": flow.event, "fa_method": flow.method]
        properties.merge(eventType.properties) { lhs, _ in lhs }
        return properties
    }

    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(
            "FA - \(eventType.name)",
            properties: properties,
            providers: [.mixPanel]
        )
    }
}
