import XCTest
@testable import FactorAuthentication

private final class FactorAuthenticationDelegateSpy: FactorAuthenticationDelegate {
    private(set) var factorAuthenticationWasClosedCallsCount = 0

    func factorAuthenticationWasClosed() {
        factorAuthenticationWasClosedCallsCount += 1
    }

    private(set) var factorAuthenticationWasCompletedWithCallsCount = 0
    private(set) var factorAuthenticationWasCompletedHash: String?

    func factorAuthenticationWasCompleted(with hash: String) {
        factorAuthenticationWasCompletedWithCallsCount += 1
        factorAuthenticationWasCompletedHash = hash
    }
}

final class FactorAuthenticationCoordinatorTests: XCTestCase {
    private let navigationSpy = NavigationControllerSpy(rootViewController: UIViewController())
    private let delegateSpy = FactorAuthenticationDelegateSpy()
    private let dataSourceSpy = FactorAuthenticationPJMock.Setup.dataSource
    private lazy var sut: FactorAuthenticationCoordinating = FactorAuthenticationCoordinator(
        from: navigationSpy,
        delegate: delegateSpy,
        dataSource: dataSourceSpy
    )

    func testStart_WhenReceiveStart_ShouldStartModule() {
        sut.start()
        XCTAssertEqual(navigationSpy.presentCallsCount, 1)
        XCTAssert(navigationSpy.presentViewController is LoadingViewController)
    }

    func testClose_WhenReceiveClose_ShouldDismissModuleAndNotifyDelegate() {
        sut.close()
        XCTAssertEqual(delegateSpy.factorAuthenticationWasClosedCallsCount, 1)
    }

    func testComplete_WhenReceiveComplete_ShouldDismissModuleAndNotifyDelegate() {
        let hash = "8BB0CF6EB9B17D0F7D22B456F1212"
        sut.complete(with: hash)
        XCTAssertEqual(delegateSpy.factorAuthenticationWasCompletedWithCallsCount, 1)
        XCTAssertEqual(delegateSpy.factorAuthenticationWasCompletedHash, hash)
    }
}
