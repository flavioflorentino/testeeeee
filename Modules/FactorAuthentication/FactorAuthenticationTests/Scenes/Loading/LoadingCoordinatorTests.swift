import XCTest
@testable import FactorAuthentication

final class LoadingCoordinatorTests: XCTestCase {
    private let navigationSpy = NavigationControllerSpy(rootViewController: UIViewController())
    private let delegateSpy = FactorAuthenticationCoordinatingSpy()
    private let dataSourceSpy = FactorAuthenticationPJMock.Setup.dataSource
    private lazy var sut: LoadingCoordinating = {
        let coordinator = LoadingCoordinator(delegate: delegateSpy, dataSource: dataSourceSpy)
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()

    func testPerform_WhenReceiveActionCloseFromPresenter_ShouldCallDelegateToDismissFlow() {
        sut.perform(action: .close)

        XCTAssertEqual(delegateSpy.closeCallsCount, 1)
    }

    func testPerform_WhenReceiveActionCodeFromPresenter_ShouldPushAController() throws {
        let response = FactorAuthenticationPJMock.Step.Success.codeResponse
        let code = try XCTUnwrap(response.code)

        sut.perform(action: .code(response: code))
        XCTAssertEqual(navigationSpy.pushViewControllerCallsCount, 2)
        XCTAssert(navigationSpy.pushViewController is CodeViewController)
    }
    
    func testPerform_WhenReceiveActionPasswordFromPresenter_ShouldPushAController() throws {
        let response = FactorAuthenticationPJMock.Step.Success.passwordResponse
        let password = try XCTUnwrap(response.password)
        
        sut.perform(action: .password(response: password))
        XCTAssertEqual(navigationSpy.pushViewControllerCallsCount, 2)
        XCTAssert(navigationSpy.pushViewController is PasswordViewController)
    }
}
