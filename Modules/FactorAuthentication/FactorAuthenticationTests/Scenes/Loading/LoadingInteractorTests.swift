import AnalyticsModule
import Core
import Foundation
import XCTest
@testable import FactorAuthentication

private final class LoadingServiceMock: LoadingServicing {
    var retrieveStepResult: Result<StepResponse, ApiError>?

    func retrieveStep(completion: @escaping (Result<StepResponse, ApiError>) -> Void) {
        guard let retrieveStepResult = retrieveStepResult else {
            XCTFail("No retrieveStepResult provided")
            return
        }
        completion(retrieveStepResult)
    }
}

private final class LoadingPresenterSpy: LoadingPresenting {
    var viewController: LoadingDisplaying?

    private(set) var actionCallsCount = 0
    private(set) var actionSelected: LoadingAction?

    func didNextStep(action: LoadingAction) {
        actionSelected = action
        actionCallsCount += 1
    }

    private(set) var loadingCallsCount = 0

    func displayLoading() {
        loadingCallsCount += 1
    }

    private(set) var messageCallsCount = 0

    func displayError() {
        messageCallsCount += 1
    }
}

final class LoadingInteractorTests: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    private lazy var dependenciesMock = DependencyContainerMock(analyticsSpy)
    private let serviceMock = LoadingServiceMock()
    private let presenterSpy = LoadingPresenterSpy()
    private let dataSourceSpy = FactorAuthenticationPJMock.Setup.dataSource
    private lazy var sut = LoadingInteractor(
        dependencies: dependenciesMock,
        service: serviceMock,
        presenter: presenterSpy,
        dataSource: dataSourceSpy
    )

    func testClose_WhenReceiveCloseFromViewController_ShouldDismissFlow() {
        let track = FactorAuthenticationPJMock.Tracker.trackerLoading(.canceled)
        sut.close()

        XCTAssertEqual(presenterSpy.actionCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionSelected, .close)
        XCTAssertTrue(analyticsSpy.equals(to: track.event()))
    }

    func testRetrieveStep_WhenReceiveRetrieveStepFromViewController_ShouldPushCodeController() throws {
        let response = FactorAuthenticationPJMock.Step.Success.codeResponse
        let code = try XCTUnwrap(response.code)

        serviceMock.retrieveStepResult = .success(response)
        retrieveStepDefaultTest()
        XCTAssertEqual(presenterSpy.actionCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionSelected, .code(response: code))
    }

    func testRetrieveStep_WhenReceiveRetrieveStepFromViewController_ShouldDisplayACodeError() {
        let response = FactorAuthenticationPJMock.Step.Success.codeResponseWithCodeNil
        serviceMock.retrieveStepResult = .success(response)
        retrieveStepDefaultTest()
        XCTAssertEqual(presenterSpy.actionCallsCount, 0)
    }

    func testRetrieveStep_WhenReceiveRetrieveStepFromViewController_ShouldDisplayUnknownError() {
        let response = FactorAuthenticationPJMock.Step.Success.codeResponseWithCodeNil
        serviceMock.retrieveStepResult = .success(response)
        retrieveStepDefaultTest()

        XCTAssertEqual(presenterSpy.messageCallsCount, 1)
    }

    func testRetrieveStep_WhenReceiveRetrieveStepFromViewController_ShouldDisplayConnectionFailureError() {
        let errorEvent = FactorAuthenticationPJMock.Tracker.trackerLoading(.error(.requestFailed))
        let response = FactorAuthenticationPJMock.Step.Error.connectionFailure
        serviceMock.retrieveStepResult = .failure(response)
        retrieveStepDefaultTest(errorEvent: errorEvent)

        XCTAssertEqual(presenterSpy.messageCallsCount, 1)
    }

    func testRetrieveStep_WhenReceiveRetrieveStepFromViewController_ShouldDisplayTimeoutError() {
        let errorEvent = FactorAuthenticationPJMock.Tracker.trackerLoading(.error(.requestFailed))
        let response = FactorAuthenticationPJMock.Step.Error.timeout
        serviceMock.retrieveStepResult = .failure(response)
        retrieveStepDefaultTest(errorEvent: errorEvent)

        XCTAssertEqual(presenterSpy.messageCallsCount, 1)
    }

    func testRetrieveStep_WhenReceiveRetrieveStepFromViewController_ShouldDisplayBadRequestError() {
        let errorEvent = FactorAuthenticationPJMock.Tracker.trackerLoading(.error(.requestFailed))
        let response = FactorAuthenticationPJMock.Step.Error.badRequest
        serviceMock.retrieveStepResult = .failure(response)
        retrieveStepDefaultTest(errorEvent: errorEvent)

        XCTAssertEqual(presenterSpy.messageCallsCount, 1)
    }

    func testRetrieveStep_WhenReceiveRetrieveStepFromViewController_ShouldDisplayDefaultError() {
        let errorEvent = FactorAuthenticationPJMock.Tracker.trackerLoading(.error(.requestFailed))
        let response = FactorAuthenticationPJMock.Step.Error.default
        serviceMock.retrieveStepResult = .failure(response)
        retrieveStepDefaultTest(errorEvent: errorEvent)

        XCTAssertEqual(presenterSpy.messageCallsCount, 1)
    }
    
    func testRetrieveStep_WhenReceiveRetrieveStepFromViewController_ShouldPushPasswordController() throws {
        let response = FactorAuthenticationPJMock.Step.Success.passwordResponse
        let password = try XCTUnwrap(response.password)
        
        serviceMock.retrieveStepResult = .success(response)
        retrieveStepDefaultTest()
        XCTAssertEqual(presenterSpy.actionCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionSelected, .password(response: password))
    }
    
    func testRetrieveStep_WhenReceiveRetrieveStepFromViewController_ShouldDisplayAPasswordError() {
        let response = FactorAuthenticationPJMock.Step.Success.passwordResponseWithCodeNil
        serviceMock.retrieveStepResult = .success(response)
        retrieveStepDefaultTest()
        XCTAssertEqual(presenterSpy.actionCallsCount, 0)
    }

    private func retrieveStepDefaultTest(errorEvent: Tracker? = nil) {
        let startedEvent = FactorAuthenticationPJMock.Tracker.trackerLoading(.started)
        sut.retrieveStep()

        if let errorEvent = errorEvent {
            XCTAssertTrue(analyticsSpy.equals(to: startedEvent.event(), errorEvent.event()))
        } else {
            XCTAssertTrue(analyticsSpy.equals(to: startedEvent.event()))
        }
        XCTAssertEqual(presenterSpy.loadingCallsCount, 1)
    }
}
