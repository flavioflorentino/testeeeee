import XCTest
@testable import FactorAuthentication

private final class LoadingCoordinatorSpy: LoadingCoordinating {
    var viewController: UIViewController?

    private(set) var actionCallsCount = 0
    private(set) var actionSelected: LoadingAction?

    func perform(action: LoadingAction) {
        actionSelected = action
        actionCallsCount += 1
    }
}

private final class LoadingDisplaySpy: LoadingDisplaying {
    private(set) var loadingCallsCount = 0

    func displayLoading() {
        loadingCallsCount += 1
    }

    private(set) var messageCallsCount = 0

    func displayError() {
        messageCallsCount += 1
    }
}

final class LoadingPresenterTests: XCTestCase {
    private let coordinatorSpy = LoadingCoordinatorSpy()
    private let displaySpy = LoadingDisplaySpy()
    private lazy var sut: LoadingPresenting = {
        let presenter = LoadingPresenter(coordinator: coordinatorSpy)
        presenter.viewController = displaySpy
        return presenter
    }()

    func testDidNextStep_WhenReceiveDidNextStepFromInteractor_ShouldDismissFlow() {
        sut.didNextStep(action: .close)
        XCTAssertEqual(coordinatorSpy.actionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.actionSelected, .close)
    }

    func testDidNextStep_WhenReceiveDidNextStepFromInteractor_ShouldPushCodeController() throws {
        let response = FactorAuthenticationPJMock.Step.Success.codeResponse
        let code = try XCTUnwrap(response.code)

        sut.didNextStep(action: .code(response: code))
        XCTAssertEqual(coordinatorSpy.actionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.actionSelected, .code(response: code))
    }

    func testDisplayLoading_WhenReceiveDisplayLoadingFromInteractor_ShouldDisplayLoading() {
        sut.displayLoading()

        XCTAssertEqual(displaySpy.loadingCallsCount, 1)
    }

    func testDisplayError_WhenReceiveDisplayErrorFromInteractor_ShouldDisplayAnError() {
        sut.displayError()

        XCTAssertEqual(displaySpy.messageCallsCount, 1)
    }
}
