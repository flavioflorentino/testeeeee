import AnalyticsModule
import Core
import Foundation
import XCTest
@testable import FactorAuthentication

private final class CodeServiceMock: CodeServicing {
    var retrieveStepResult: Result<StepResponse, ApiError>?

    func retrieveStep(completion: @escaping (Result<StepResponse, ApiError>) -> Void) {
        guard let retrieveStepResult = retrieveStepResult else {
            XCTFail("No retrieveStepResult provided")
            return
        }
        completion(retrieveStepResult)
    }

    var verifyCodeResult: Result<VerifyCodeResponse, ApiError>?

    func verify(_ code: String, completion: @escaping (Result<VerifyCodeResponse, ApiError>) -> Void) {
        guard let verifyCodeResult = verifyCodeResult else {
            XCTFail("No verifyCodeResult provided")
            return
        }
        completion(verifyCodeResult)
    }
}

private final class CodePresenterSpy: CodePresenting {
    var viewController: CodeDisplaying?

    private(set) var actionCallsCount = 0
    private(set) var actionSelected: CodeAction?

    func didNextStep(action: CodeAction) {
        actionSelected = action
        actionCallsCount += 1
    }

    private(set) var displayCodeCallsCount = 0
    private(set) var codeResponse: CodeResponse?

    func displayCode(response: CodeResponse) {
        displayCodeCallsCount += 1
        codeResponse = response
    }

    private(set) var displayTimeCallsCount = 0
    private(set) var time: Int?

    func displayTime(time: Int) {
        displayTimeCallsCount += 1
        self.time = time
    }

    private(set) var displayResendLoadingCallsCount = 0

    func displayResendLoading() {
        displayResendLoadingCallsCount += 1
    }

    private(set) var displayResendCallsCount = 0

    func displayResend() {
        displayResendCallsCount += 1
    }

    private(set) var displayCodeLoadingCallsCount = 0

    func displayCodeLoading() {
        displayCodeLoadingCallsCount += 1
    }

    private(set) var displayCodeErrorCallsCount = 0
    private(set) var codeErrorMessage: String?

    func displayCodeError(message: String) {
        displayCodeErrorCallsCount += 1
        codeErrorMessage = message
    }

    private(set) var dismissCodeErrorCallsCount = 0

    func dismissCodeError() {
        dismissCodeErrorCallsCount += 1
    }

    private(set) var displayResendErrorCallsCount = 0
    private(set) var resendErrorMessage: String?

    func displayResendError(message: String) {
        displayResendErrorCallsCount += 1
        resendErrorMessage = message
    }

    private(set) var displayKeyboardCallsCount = 0

    func displayKeyboard() {
        displayKeyboardCallsCount += 1
    }

    private(set) var dismissKeyboardCallsCount = 0

    func dismissKeyboard() {
        dismissKeyboardCallsCount += 1
    }

    private(set) var displayNetworkBannerCallsCount = 0

    func displayNetworkBanner() {
        displayNetworkBannerCallsCount += 1
    }

    private(set) var dismissNetworkBannerCallsCount = 0

    func dismissNetworkBanner() {
        dismissNetworkBannerCallsCount += 1
    }
}

final class CodeInteractorTests: XCTestCase {
    private let analyticsMock = AnalyticsSpy()
    private lazy var dependenciesMock = DependencyContainerMock(analyticsMock)
    private let serviceMock = CodeServiceMock()
    private let presenterSpy = CodePresenterSpy()
    private let dataSourceSpy = FactorAuthenticationPJMock.Setup.dataSource
    private let codeResponse = FactorAuthenticationPJMock.Code.Success.codeResponseDefault
    private let codeResponseTimeout1 = FactorAuthenticationPJMock.Code.Success.codeResponseTimeout1
    private lazy var sut = CodeInteractor(
        dependencies: dependenciesMock,
        service: serviceMock,
        presenter: presenterSpy,
        dataSource: dataSourceSpy,
        codeResponse: codeResponse,
        timerProvider: TimerMock.self
    )
    private lazy var sutTimeout1 = CodeInteractor(
        dependencies: dependenciesMock,
        service: serviceMock,
        presenter: presenterSpy,
        dataSource: dataSourceSpy,
        codeResponse: codeResponseTimeout1,
        timerProvider: TimerMock.self
    )

    func testTrackStarted_WhenReceiveTrackStartedFromViewController_ShouldTrackAnEvent() {
        let event = FactorAuthenticationPJMock.Tracker.trackerCode(.methodChosen)
        sut.trackStarted()

        XCTAssertTrue(analyticsMock.equals(to: event.event()))
    }

    func testClose_WhenReceiveCloseFromViewController_ShouldDismissFlow() {
        let event = FactorAuthenticationPJMock.Tracker.trackerCode(.canceled)
        sut.close()

        XCTAssertEqual(presenterSpy.actionCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionSelected, .close)
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
    }

    func testPrepareForReceiveCode_WhenReceivePrepareForReceiveCodeFromViewController_ShouldDisplayInformations() {
        sut.prepareForReceiveCode()
        TimerMock.currentTimer?.fire()

        XCTAssertEqual(presenterSpy.displayCodeCallsCount, 1)
        XCTAssertEqual(presenterSpy.codeResponse, codeResponse)
        // It was called twice because the timer was fired
        XCTAssertEqual(presenterSpy.displayTimeCallsCount, 2)
        // It is the same as the timeout - 1 because it was passed once in the block
        XCTAssertEqual(presenterSpy.time, codeResponse.timeout - 1)
    }

    func testPrepareForReceiveCode_WhenReceivePrepareForReceiveCodeFromViewController_ShouldDisplayResend() {
        sutTimeout1.prepareForReceiveCode()
        TimerMock.currentTimer?.fire()

        XCTAssertEqual(presenterSpy.displayCodeCallsCount, 1)
        XCTAssertEqual(presenterSpy.codeResponse, codeResponseTimeout1)
        XCTAssertEqual(presenterSpy.displayTimeCallsCount, 1)
        XCTAssertEqual(presenterSpy.time, codeResponseTimeout1.timeout)
        XCTAssertEqual(presenterSpy.displayResendCallsCount, 1)
    }

    func testPrepareForReceiveCode_WhenReceivePrepareForReceiveCodeFromViewController_ShouldInvalidateTimer() {
        sut.prepareForReceiveCode()
        // It was called twice because it must create the current Timer
        TimerMock.setUp()
        TimerMock.timerIsValid = true
        sut.prepareForReceiveCode()

        XCTAssertEqual(TimerMock.invalidateCallsCount, 1)
    }

    func testResendCode_WhenReceiveResendCodeFromViewController_ShouldResendCode() throws {
        let event = FactorAuthenticationPJMock.Tracker.trackerCode(.codeResended)
        let response = FactorAuthenticationPJMock.Step.Success.codeResponse

        serviceMock.retrieveStepResult = .success(response)
        sut.resendCode()

        XCTAssertEqual(presenterSpy.displayResendLoadingCallsCount, 1)
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.displayCodeCallsCount, 1)
        XCTAssertEqual(presenterSpy.codeResponse, codeResponse)
        XCTAssertEqual(presenterSpy.displayTimeCallsCount, 1)
        XCTAssertEqual(presenterSpy.time, codeResponse.timeout)
    }

    func testResendCode_WhenReceiveResendCodeFromViewController_ShouldDisplayUnknownError() {
        let response = FactorAuthenticationPJMock.Step.Success.codeResponseWithCodeNil

        serviceMock.retrieveStepResult = .success(response)
        sut.resendCode()

        XCTAssertEqual(presenterSpy.displayResendLoadingCallsCount, 1)
    }

    func testResendCode_WhenReceiveResendCodeFromViewController_ShouldDisplayConnectionFailureError() {
        let event = FactorAuthenticationPJMock.Tracker.trackerCode(.error(.requestFailed))
        let response = FactorAuthenticationPJMock.Step.Error.connectionFailure

        serviceMock.retrieveStepResult = .failure(response)
        sut.resendCode()

        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.displayResendLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayNetworkBannerCallsCount, 1)
    }

    func testResendCode_WhenReceiveResendCodeFromViewController_ShouldDisplayTimeoutError() {
        let event = FactorAuthenticationPJMock.Tracker.trackerCode(.error(.requestFailed))
        let response = FactorAuthenticationPJMock.Step.Error.timeout

        serviceMock.retrieveStepResult = .failure(response)
        sut.resendCode()

        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.displayResendLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayNetworkBannerCallsCount, 1)
    }

    func testResendCode_WhenReceiveResendCodeFromViewController_ShouldDisplayBadRequestErrorAndTrackExpired() {
        let event = FactorAuthenticationPJMock.Tracker.trackerCode(.codeExpired)
        let response = FactorAuthenticationPJMock.Step.Error.badRequestExpired

        serviceMock.retrieveStepResult = .failure(response)
        sut.resendCode()

        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.displayResendLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayResendErrorCallsCount, 1)
    }

    func testResendCode_WhenReceiveResendCodeFromViewController_ShouldDisplayBadRequestErrorAndTrackInvalid() {
        let event = FactorAuthenticationPJMock.Tracker.trackerCode(.codeIncorrect)
        let response = FactorAuthenticationPJMock.Step.Error.badRequestInvalid

        serviceMock.retrieveStepResult = .failure(response)
        sut.resendCode()

        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.displayResendLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayResendErrorCallsCount, 1)
    }

    func testResendCode_WhenReceiveResendCodeFromViewController_ShouldDisplayBadRequestErrorAndTrackGeneric() {
        let event = FactorAuthenticationPJMock.Tracker.trackerCode(.error(.generic))
        let response = FactorAuthenticationPJMock.Step.Error.badRequest

        serviceMock.retrieveStepResult = .failure(response)
        sut.resendCode()

        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.displayResendLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayResendErrorCallsCount, 1)
    }

    func testResendCode_WhenReceiveResendCodeFromViewController_ShouldDisplayDefaultError() {
        let event = FactorAuthenticationPJMock.Tracker.trackerCode(.error(.generic))
        let response = FactorAuthenticationPJMock.Step.Error.default

        serviceMock.retrieveStepResult = .failure(response)
        sut.resendCode()

        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.displayResendLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayResendErrorCallsCount, 1)
    }

    func testVerifyCode_WhenReceiveVerifyCodeNilFromViewController_ShouldDoNothing() {
        sut.verifyCode(nil)

        XCTAssertEqual(presenterSpy.displayCodeLoadingCallsCount, 0)
        XCTAssertEqual(presenterSpy.actionCallsCount, 0)
        XCTAssertNil(presenterSpy.actionSelected)
    }

    func testVerifyCode_WhenReceiveVerifyCodeFromViewController_ShouldCompleteFlow() {
        let typingEvent = FactorAuthenticationPJMock.Tracker.trackerCode(.codeDeliveryTime(0))
        let succesEvent = FactorAuthenticationPJMock.Tracker.trackerCode(.success)
        let response = FactorAuthenticationPJMock.Code.Success.verifyCodeResponse

        serviceMock.verifyCodeResult = .success(response)
        sut.verifyCode("123456")

        XCTAssertTrue(analyticsMock.equals(to: typingEvent.event(), succesEvent.event()))
        XCTAssertEqual(presenterSpy.displayCodeLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionSelected, .complete("8BB0CF6EB9B17D0F7D22B456F1212"))
    }

    func testVerifyCode_WhenReceiveVerifyCodeFromViewController_ShouldDisplayConnectionFailureError() {
        let typingEvent = FactorAuthenticationPJMock.Tracker.trackerCode(.codeDeliveryTime(0))
        let errorEvent = FactorAuthenticationPJMock.Tracker.trackerCode(.error(.requestFailed))
        let response = FactorAuthenticationPJMock.Code.Error.connectionFailure

        serviceMock.verifyCodeResult = .failure(response)
        sut.verifyCode("123456")

        XCTAssertTrue(analyticsMock.equals(to: typingEvent.event(), errorEvent.event()))
        XCTAssertEqual(presenterSpy.displayCodeLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayNetworkBannerCallsCount, 1)
    }

    func testVerifyCode_WhenReceiveVerifyCodeFromViewController_ShouldDisplayTimeoutError() {
        let typingEvent = FactorAuthenticationPJMock.Tracker.trackerCode(.codeDeliveryTime(0))
        let errorEvent = FactorAuthenticationPJMock.Tracker.trackerCode(.error(.requestFailed))
        let response = FactorAuthenticationPJMock.Code.Error.timeout

        serviceMock.verifyCodeResult = .failure(response)
        sut.verifyCode("123456")

        XCTAssertTrue(analyticsMock.equals(to: typingEvent.event(), errorEvent.event()))
        XCTAssertEqual(presenterSpy.displayCodeLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayNetworkBannerCallsCount, 1)
    }

    func testVerifyCode_WhenReceiveVerifyCodeFromViewController_ShouldDisplayBadRequestErrorAndTrackExpired() {
        let typingEvent = FactorAuthenticationPJMock.Tracker.trackerCode(.codeDeliveryTime(0))
        let errorEvent = FactorAuthenticationPJMock.Tracker.trackerCode(.codeExpired)
        let response = FactorAuthenticationPJMock.Code.Error.badRequestExpired

        serviceMock.verifyCodeResult = .failure(response)
        sut.verifyCode("123456")

        XCTAssertTrue(analyticsMock.equals(to: typingEvent.event(), errorEvent.event()))
        XCTAssertEqual(presenterSpy.displayCodeLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayCodeErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.codeErrorMessage, "Tivemos um problema na validação. Digite o código novamente.")
    }

    func testVerifyCode_WhenReceiveVerifyCodeFromViewController_ShouldDisplayBadRequestErrorAndTrackInvalid() {
        let typingEvent = FactorAuthenticationPJMock.Tracker.trackerCode(.codeDeliveryTime(0))
        let errorEvent = FactorAuthenticationPJMock.Tracker.trackerCode(.codeIncorrect)
        let response = FactorAuthenticationPJMock.Code.Error.badRequestInvalid

        serviceMock.verifyCodeResult = .failure(response)
        sut.verifyCode("123456")

        XCTAssertTrue(analyticsMock.equals(to: typingEvent.event(), errorEvent.event()))
        XCTAssertEqual(presenterSpy.displayCodeLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayCodeErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.codeErrorMessage, "Tivemos um problema na validação. Digite o código novamente.")
    }

    func testVerifyCode_WhenReceiveVerifyCodeFromViewController_ShouldDisplayBadRequestErrorAndTrackGeneric() {
        let typingEvent = FactorAuthenticationPJMock.Tracker.trackerCode(.codeDeliveryTime(0))
        let errorEvent = FactorAuthenticationPJMock.Tracker.trackerCode(.error(.generic))
        let response = FactorAuthenticationPJMock.Code.Error.badRequest

        serviceMock.verifyCodeResult = .failure(response)
        sut.verifyCode("123456")

        XCTAssertTrue(analyticsMock.equals(to: typingEvent.event(), errorEvent.event()))
        XCTAssertEqual(presenterSpy.displayCodeLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayCodeErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.codeErrorMessage, "Tivemos um problema na validação. Digite o código novamente.")
    }

    func testVerifyCode_WhenReceiveVerifyCodeFromViewController_ShouldDisplayDefaultError() {
        let typingEvent = FactorAuthenticationPJMock.Tracker.trackerCode(.codeDeliveryTime(0))
        let errorEvent = FactorAuthenticationPJMock.Tracker.trackerCode(.error(.generic))
        let response = FactorAuthenticationPJMock.Code.Error.default

        serviceMock.verifyCodeResult = .failure(response)
        sut.verifyCode("123456")

        XCTAssertTrue(analyticsMock.equals(to: typingEvent.event(), errorEvent.event()))
        XCTAssertEqual(presenterSpy.displayCodeLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayCodeErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.codeErrorMessage, "Tivemos um problema na validação. Digite o código novamente.")
    }

    func testVerifyCode_WhenReceiveVerifyCodeFromViewController_ShouldDoNothing() {
        sut.verifyCode("1234567")

        XCTAssertEqual(presenterSpy.displayCodeLoadingCallsCount, 0)
        XCTAssertEqual(presenterSpy.actionCallsCount, 0)
        XCTAssertNil(presenterSpy.actionSelected)
    }
}
