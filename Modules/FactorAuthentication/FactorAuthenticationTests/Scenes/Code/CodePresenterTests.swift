import XCTest
@testable import FactorAuthentication

final class CodeCoordinatorSpy: CodeCoordinating {
    var viewController: UIViewController?

    private(set) var actionCallsCount = 0
    private(set) var actionSelected: CodeAction?

    func perform(action: CodeAction) {
        actionSelected = action
        actionCallsCount += 1
    }
}

final class CodeDisplaySpy: CodeDisplaying {
    private(set) var displayNavigationTitleCallsCount = 0
    private(set) var navigationTitle: String?

    func displayNavigationTitle(_ title: String) {
        displayNavigationTitleCallsCount += 1
        navigationTitle = title
    }

    private(set) var displayTitleCallsCount = 0
    private(set) var title: String?

    func displayTitle(_ title: String) {
        displayTitleCallsCount += 1
        self.title = title
    }

    private(set) var displayDescriptionCallsCount = 0
    private(set) var description: NSAttributedString?

    func displayDescription(_ description: NSAttributedString) {
        displayDescriptionCallsCount += 1
        self.description = description
    }

    private(set) var displayPlaceholderCallsCount = 0
    private(set) var placeholder: String?

    func displayPlaceholder(_ placeholder: String) {
        displayPlaceholderCallsCount += 1
        self.placeholder = placeholder
    }

    private(set) var displayTimeCallsCount = 0
    private(set) var time: NSAttributedString?

    func displayTime(_ title: NSAttributedString) {
        displayTimeCallsCount += 1
        time = title
    }

    private(set) var displayWaitingCallsCount = 0
    private(set) var waiting: NSAttributedString?

    func displayWaiting(_ title: NSAttributedString) {
        displayWaitingCallsCount += 1
        waiting = title
    }

    private(set) var displayResendCallsCount = 0

    func displayResend() {
        displayResendCallsCount += 1
    }

    private(set) var displayCodeLoadingCallsCount = 0

    func displayCodeLoading() {
        displayCodeLoadingCallsCount += 1
    }

    private(set) var setResendTitleCallsCount = 0
    private(set) var resendTitle: NSAttributedString?

    func setResendTitle(_ title: NSAttributedString) {
        setResendTitleCallsCount += 1
        resendTitle = title
    }

    private(set) var displayCodeErrorCallsCount = 0
    private(set) var codeError: String?

    func displayCodeError(_ messsage: String) {
        displayCodeErrorCallsCount += 1
        codeError = messsage
    }

    private(set) var dismissCodeErrorCallsCount = 0

    func dismissCodeError() {
        dismissCodeErrorCallsCount += 1
    }

    private(set) var displayResendErrorCallsCount = 0
    private(set) var resendError: String?

    func displayResendError(_ message: String) {
        displayResendErrorCallsCount += 1
        resendError = message
    }

    private(set) var displayKeyboardCallsCount = 0

    func displayKeyboard() {
        displayKeyboardCallsCount += 1
    }

    private(set) var dismissKeyboardCallsCount = 0

    func dismissKeyboard() {
        dismissKeyboardCallsCount += 1
    }

    private(set) var displayNetworkBannerCallsCount = 0

    func displayNetworkBanner() {
        displayNetworkBannerCallsCount += 1
    }

    private(set) var dismissNetworkBannerCallsCount = 0

    func dismissNetworkBanner() {
        dismissNetworkBannerCallsCount += 1
    }
}

final class CodePresenterTests: XCTestCase {
    private let coordinatorSpy = CodeCoordinatorSpy()
    private let displaySpy = CodeDisplaySpy()
    private lazy var sut: CodePresenting = {
        let presenter = CodePresenter(coordinator: coordinatorSpy)
        presenter.viewController = displaySpy
        return presenter
    }()

    private let hashMock = "1234567890"

    func testDidNextStep_WhenReceiveDidNextStepFromInteractor_ShouldDismissFlow() {
        sut.didNextStep(action: .close)
        XCTAssertEqual(coordinatorSpy.actionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.actionSelected, .close)
    }

    func testDidNextStep_WhenReceiveDidNextStepFromInteractor_ShouldCompleteFlow() {
        sut.didNextStep(action: .complete(hashMock))

        XCTAssertEqual(coordinatorSpy.actionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.actionSelected, .complete(hashMock))
    }

    func testDisplayCode_WhenReceiveDisplayCodeFromInteractor_ShouldDisplayInformation() throws {
        let response = FactorAuthenticationPJMock.Step.Success.codeResponse
        let code = try XCTUnwrap(response.code)

        sut.displayCode(response: code)

        XCTAssertEqual(displaySpy.displayNavigationTitleCallsCount, 1)
        XCTAssertEqual(displaySpy.navigationTitle, code.navigationTitle)
        XCTAssertEqual(displaySpy.displayTitleCallsCount, 1)
        XCTAssertEqual(displaySpy.title, code.title)
        XCTAssertEqual(displaySpy.displayDescriptionCallsCount, 1)
        XCTAssertEqual(displaySpy.description?.string, code.description)
        XCTAssertEqual(displaySpy.displayPlaceholderCallsCount, 1)
        XCTAssertEqual(displaySpy.placeholder, code.placeholder)
        XCTAssertEqual(displaySpy.setResendTitleCallsCount, 1)
        XCTAssertEqual(displaySpy.resendTitle?.string, code.resendTitle)
        XCTAssertEqual(displaySpy.displayKeyboardCallsCount, 1)
    }

   func testDisplayTime_WhenReceiveDisplayTimeFromInteractor_ShouldDisplayTime() {
       let time = 60
       sut.displayTime(time: time)

        XCTAssertEqual(displaySpy.displayTimeCallsCount, 1)
        XCTAssertEqual(displaySpy.time?.string, "Não recebeu? Aguarde \(time)s")
    }

   func testDisplayResendLoading_WhenReceiveDisplayResendLoadingFromInteractor_ShouldDisplayResendLoading() {
       sut.displayResendLoading()

        XCTAssertEqual(displaySpy.displayWaitingCallsCount, 1)
        XCTAssertEqual(displaySpy.waiting?.string, "Aguarde...")
        XCTAssertEqual(displaySpy.dismissKeyboardCallsCount, 1)
    }

    func testDisplayResend_WhenReceiveDisplayResendFromInteractor_ShouldDisplayResend() {
        sut.displayResend()
        XCTAssertEqual(displaySpy.displayResendCallsCount, 1)
    }

    func testDisplayCodeLoading_WhenReceiveDisplayCodeLoadingFromInteractor_ShouldDisplayCodeLoading() {
        sut.displayCodeLoading()
        XCTAssertEqual(displaySpy.displayCodeLoadingCallsCount, 1)
        XCTAssertEqual(displaySpy.dismissKeyboardCallsCount, 1)
    }

    func testDisplayCodeError_WhenReceiveDisplayCodeErrorFromInteractor_ShouldDisplayCodeError() {
        let message = "Mensagem de erro"
        sut.displayCodeError(message: message)
        XCTAssertEqual(displaySpy.displayCodeErrorCallsCount, 1)
        XCTAssertEqual(displaySpy.codeError, message)
    }

    func testDismissCodeError_WhenReceiveDismissCodeErrorFromInteractor_ShouldDismissCodeError() {
        sut.dismissCodeError()
        XCTAssertEqual(displaySpy.dismissCodeErrorCallsCount, 1)
    }

    func testDisplayResendError_WhenReceiveDisplayResendErrorFromInteractor_ShouldDisplayResendError() {
        let message = "Mensagem de erro"
        sut.displayResendError(message: message)
        XCTAssertEqual(displaySpy.displayResendErrorCallsCount, 1)
        XCTAssertEqual(displaySpy.resendError, message)
    }

    func testDisplayKeyboard_WhenReceiveDisplayKeyboardFromInteractor_ShouldDisplayKeyboard() {
        sut.displayKeyboard()
        XCTAssertEqual(displaySpy.displayKeyboardCallsCount, 1)
    }

    func testDismissKeyboard_WhenReceiveDismissKeyboardFromInteractor_ShouldDismissKeyboard() {
        sut.dismissKeyboard()
        XCTAssertEqual(displaySpy.dismissKeyboardCallsCount, 1)
    }

    func testDisplayNetworkBanner_WhenReceiveDisplayNetworkBannerFromInteractor_ShouldDisplayWithoutNetworkBanner() {
        sut.displayNetworkBanner()
        XCTAssertEqual(displaySpy.displayNetworkBannerCallsCount, 1)
    }

    func testDismissNetworkBanner_WhenReceiveDismissNeworkBannerFromInteractor_ShouldDismissWithoutNetworkBanner() {
        sut.dismissNetworkBanner()
        XCTAssertEqual(displaySpy.dismissNetworkBannerCallsCount, 1)
    }
}
