import XCTest
@testable import FactorAuthentication

private final class PasswordCoordinatorSpy: PasswordCoordinating {
    var viewController: UIViewController?

    private(set) var actionCallsCount = 0
    private(set) var actionSelected: PasswordAction?

    func perform(action: PasswordAction) {
        actionSelected = action
        actionCallsCount += 1
    }
}

private final class PasswordDisplaySpy: PasswordDisplaying {
    private(set) var displayNavigationTitleCallsCount = 0
    private(set) var navigationTitle: String?
    
    func displayNavigationTitle(_ title: String) {
        displayNavigationTitleCallsCount += 1
        navigationTitle = title
    }
    
    private(set) var displayDescriptionCallsCount = 0
    private(set) var description: String?
    
    func displayDescription(_ description: NSAttributedString) {
        displayDescriptionCallsCount += 1
        self.description = description.string
    }
    
    private(set) var displayPlaceholderCallsCount = 0
    private(set) var placeholder: String?
    
    func displayPlaceholder(_ placeholder: String) {
        displayPlaceholderCallsCount += 1
        self.placeholder = placeholder
    }
    
    private(set) var setActionTitleCallsCount = 0
    private(set) var actionTitle: String?
    
    func setActionTitle(_ title: String) {
        setActionTitleCallsCount += 1
        actionTitle = title
    }
    
    private(set) var enableActionButtonCallsCount = 0
    
    func enableActionButton() {
        enableActionButtonCallsCount += 1
    }
    
    private(set) var disableActionButtonCallsCount = 0
    
    func disableActionButton() {
        disableActionButtonCallsCount += 1
    }
    
    private(set) var startLoadingCallsCount = 0
    
    func startLoading() {
        startLoadingCallsCount += 1
    }
    
    private(set) var stopLoadingCallsCount = 0
    
    func stopLoading() {
        stopLoadingCallsCount += 1
    }
    
    private(set) var displayKeyboardCallsCount = 0
    
    func displayKeyboard() {
        displayKeyboardCallsCount += 1
    }
    
    private(set) var dismissKeyboardCallsCount = 0
    
    func dismissKeyboard() {
        dismissKeyboardCallsCount += 1
    }
    
    private(set) var displayErrorCallsCount = 0
    private(set) var errorMessage: String?
    
    func displayError(_ message: String) {
        displayErrorCallsCount += 1
        errorMessage = message
    }
    
    private(set) var dismissErrorCallsCount = 0
    
    func dismissError() {
        dismissErrorCallsCount += 1
    }
    
    private(set) var displayNetworkBannerCallsCount = 0
    
    func displayNetworkBanner() {
        displayNetworkBannerCallsCount += 1
    }
    
    private(set) var dismissNetworkBannerCallsCount = 0
    
    func dismissNetworkBanner() {
        dismissNetworkBannerCallsCount += 1
    }
}

final class PasswordPresenterTests: XCTestCase {
    private let coordinatorSpy = PasswordCoordinatorSpy()
    private let displaySpy = PasswordDisplaySpy()
    private let passwordResponse = FactorAuthenticationPJMock.Password.Success.passwordResponse
    private lazy var sut: PasswordPresenting = {
        let presenter = PasswordPresenter(coordinator: coordinatorSpy)
        presenter.viewController = displaySpy
        return presenter
    }()

    private let hashMock = "1234567890"

    func testDidNextStep_WhenReceiveDidNextStepFromInteractor_ShouldDismissFlow() {
        sut.didNextStep(action: .close)
        XCTAssertEqual(coordinatorSpy.actionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.actionSelected, .close)
    }

    func testDidNextStep_WhenReceiveDidNextStepFromInteractor_ShouldCompleteFlow() {
        sut.didNextStep(action: .complete(hashMock))

        XCTAssertEqual(coordinatorSpy.actionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.actionSelected, .complete(hashMock))
    }
    
    func testDisplayPassword_WhenReceiveDisplayPasswordFromInteractor_ShouldDisplayInformation() {
        let response = passwordResponse
        sut.displayPassword(response: response)
        
        XCTAssertEqual(displaySpy.navigationTitle, response.navigationTitle)
        XCTAssertEqual(displaySpy.displayNavigationTitleCallsCount, 1)
        XCTAssertEqual(displaySpy.description, response.description)
        XCTAssertEqual(displaySpy.displayDescriptionCallsCount, 1)
        XCTAssertEqual(displaySpy.placeholder, response.placeholder)
        XCTAssertEqual(displaySpy.displayPlaceholderCallsCount, 1)
        XCTAssertEqual(displaySpy.actionTitle, response.confirmTitle)
        XCTAssertEqual(displaySpy.setActionTitleCallsCount, 1)
        XCTAssertEqual(displaySpy.disableActionButtonCallsCount, 1)
        XCTAssertEqual(displaySpy.displayKeyboardCallsCount, 1)
    }
    
    func testEnableActionButton_WhenReceiveEnableActionButtonFromInteractor_ShouldEnableActionButton() {
        sut.enableActionButton()
        
        XCTAssertEqual(displaySpy.enableActionButtonCallsCount, 1)
    }
    
    func testDisableActionButton_WhenReceiveDisableActionButtonFromInteractor_ShouldDisableActionButton() {
        sut.disableActionButton()
        
        XCTAssertEqual(displaySpy.disableActionButtonCallsCount, 1)
    }
    
    func testStartLoading_WhenReceiveStartLoadingFromInteractor_ShouldStartLoading() {
        sut.startLoading()
        
        XCTAssertEqual(displaySpy.startLoadingCallsCount, 1)
    }
    
    func testStopLoading_WhenReceiveStopLoadingFromInteractor_ShouldStopLoading() {
        sut.stopLoading()
        
        XCTAssertEqual(displaySpy.stopLoadingCallsCount, 1)
    }
    
    func testDisplayKeyboard_WhenReceiveDisplayKeyboardFromInteractor_ShouldDisplayKeyboard() {
        sut.displayKeyboard()
        XCTAssertEqual(displaySpy.displayKeyboardCallsCount, 1)
    }
    
    func testDismissKeyboard_WhenReceiveDismissKeyboardFromInteractor_ShouldDismissKeyboard() {
        sut.dismissKeyboard()
        XCTAssertEqual(displaySpy.dismissKeyboardCallsCount, 1)
    }
    
    func testDisplayError_WhenReceiveDisplayErrorFromInteractor_ShouldDisplayError() {
        let messageError = "mensagem de erro"
        sut.displayError(message: messageError)
        
        XCTAssertEqual(displaySpy.displayErrorCallsCount, 1)
        XCTAssertEqual(displaySpy.errorMessage, messageError)
    }
    
    func testDismissError_WhenReceiveDismissErrorFromInteractor_ShouldDismissError() {
        sut.dismissError()
        
        XCTAssertEqual(displaySpy.dismissErrorCallsCount, 1)
    }

    func testDisplayNetworkBanner_WhenReceiveDisplayNetworkBannerFromInteractor_ShouldDisplayWithoutNetworkBanner() {
        sut.displayNetworkBanner()
        XCTAssertEqual(displaySpy.displayNetworkBannerCallsCount, 1)
    }

    func testDismissNetworkBanner_WhenReceiveDismissNeworkBannerFromInteractor_ShouldDismissWithoutNetworkBanner() {
        sut.dismissNetworkBanner()
        XCTAssertEqual(displaySpy.dismissNetworkBannerCallsCount, 1)
    }
}
