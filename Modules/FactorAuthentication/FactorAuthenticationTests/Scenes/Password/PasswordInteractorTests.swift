import AnalyticsModule
import Core
import Foundation
import XCTest
@testable import FactorAuthentication

private final class PasswordServiceMock: PasswordServicing {
    var verifyResult: Result<VerifyCodeResponse, ApiError>?

    func verify(_ password: String, completion: @escaping (Result<VerifyCodeResponse, ApiError>) -> Void) {
        guard let verifyResult = verifyResult else {
            XCTFail("No verifyResult provided")
            return
        }
        completion(verifyResult)
    }
}

private final class PasswordPresenterSpy: PasswordPresenting {
    var viewController: PasswordDisplaying?

    private(set) var actionCallsCount = 0
    private(set) var actionSelected: PasswordAction?

    func didNextStep(action: PasswordAction) {
        actionSelected = action
        actionCallsCount += 1
    }

    private(set) var displayPasswordCallsCount = 0
    private(set) var passwordResponse: PasswordResponse?

    func displayPassword(response: PasswordResponse) {
        displayPasswordCallsCount += 1
        passwordResponse = response
    }

    private(set) var enableActionButtonCallsCount = 0

    func enableActionButton() {
        enableActionButtonCallsCount += 1
    }

    private(set) var disableActionButtonCallsCount = 0

    func disableActionButton() {
        disableActionButtonCallsCount += 1
    }

    private(set) var startLoadingCallsCount = 0

    func startLoading() {
        startLoadingCallsCount += 1
    }

    private(set) var stopLoadingCallsCount = 0

    func stopLoading() {
        stopLoadingCallsCount += 1
    }

    private(set) var displayKeyboardCallsCount = 0

    func displayKeyboard() {
        displayKeyboardCallsCount += 1
    }

    private(set) var dismissKeyboardCallsCount = 0

    func dismissKeyboard() {
        dismissKeyboardCallsCount += 1
    }

    private(set) var displayErrorCallsCount = 0
    private(set) var errorMessage: String?

    func displayError(message: String) {
        displayErrorCallsCount += 1
        errorMessage = message
    }

    private(set) var dismissErrorCallsCount = 0

    func dismissError() {
        dismissErrorCallsCount += 1
    }

    private(set) var displayNetworkBannerCallsCount = 0

    func displayNetworkBanner() {
        displayNetworkBannerCallsCount += 1
    }

    private(set) var dismissNetworkBannerCallsCount = 0

    func dismissNetworkBanner() {
        dismissNetworkBannerCallsCount += 1
    }
}

final class PasswordInteractorTests: XCTestCase {
    private let analyticsMock = AnalyticsSpy()
    private lazy var dependenciesMock = DependencyContainerMock(analyticsMock)
    private let serviceMock = PasswordServiceMock()
    private let presenterSpy = PasswordPresenterSpy()
    private let dataSourceSpy = FactorAuthenticationPJMock.Setup.dataSource
    private let passwordResponse = FactorAuthenticationPJMock.Password.Success.passwordResponse
    private lazy var sut = PasswordInteractor(
        service: serviceMock,
        presenter: presenterSpy,
        dependencies: dependenciesMock,
        passwordResponse: passwordResponse,
        dataSource: dataSourceSpy
    )
    
    func testTrackStarted_WhenReceiveTrackStartedFromViewController_ShouldTrackAnEvent() {
        let event = FactorAuthenticationPJMock.Tracker.trackerPassword(.methodChosen)
        sut.trackStarted()
        
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
    }

    func testClose_WhenReceiveCloseFromViewController_ShouldDismissFlow() {
        let event = FactorAuthenticationPJMock.Tracker.trackerPassword(.canceled)
        sut.close()

        XCTAssertEqual(presenterSpy.actionCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionSelected, .close)
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
    }

    func testPrepareInfo_WhenReceivePrepareInfoFromViewController_ShouldDisplayInformation() {
        sut.prepareInfo()

        XCTAssertEqual(presenterSpy.displayPasswordCallsCount, 1)
        XCTAssertEqual(presenterSpy.passwordResponse, passwordResponse)
    }

    func testValidate_WhenReceiveValidateWithEmptyPasswordFromViewController_ShouldDisableActionButton() {
        let password = ""
        sut.validate(password: password)

        XCTAssertEqual(presenterSpy.disableActionButtonCallsCount, 1)
    }
    
    func testValidate_WhenReceiveValidateWithNilPasswordFromViewController_ShouldDisableActionButton() {
        sut.validate(password: nil)
        
        XCTAssertEqual(presenterSpy.disableActionButtonCallsCount, 1)
    }
    
    func testValidate_WhenReceiveValidateWithPasswordFromViewController_ShouldEnableActionButton() {
        let password = "123456"
        sut.validate(password: password)
        
        XCTAssertEqual(presenterSpy.enableActionButtonCallsCount, 1)
    }
    
    func testConfirm_WhenReceiveConfirmWithNilPasswordFromViewController_ShouldDoNothing() {
        sut.confirm(password: nil)
        
        XCTAssertEqual(presenterSpy.dismissKeyboardCallsCount, 0)
        XCTAssertEqual(presenterSpy.startLoadingCallsCount, 0)
        XCTAssertEqual(presenterSpy.stopLoadingCallsCount, 0)
        XCTAssertEqual(presenterSpy.displayNetworkBannerCallsCount, 0)
        XCTAssertEqual(presenterSpy.displayErrorCallsCount, 0)
        XCTAssertEqual(presenterSpy.displayKeyboardCallsCount, 0)
        XCTAssertEqual(presenterSpy.stopLoadingCallsCount, 0)
    }
    
    func testConfirm_WhenReceiveConfirmWithPasswordFromViewController_ShouldCompleteFlow() {
        let event = FactorAuthenticationPJMock.Tracker.trackerPassword(.success)
        let response = FactorAuthenticationPJMock.Password.Success.verifyCodeResponse
        serviceMock.verifyResult = .success(response)
        let password = "123456"
        sut.confirm(password: password)
        
        XCTAssertEqual(presenterSpy.dismissKeyboardCallsCount, 1)
        XCTAssertEqual(presenterSpy.startLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionSelected, .complete("8BB0CF6EB9B17D0F7D22B456F1212"))
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
    }
    
    func testConfirm_WhenReceiveConfirmWithPasswordFromViewController_ShouldDisplayConnectionFailureError() {
        let event = FactorAuthenticationPJMock.Tracker.trackerPassword(.error(.requestFailed))
        let response = FactorAuthenticationPJMock.Password.Error.connectionFailure
        serviceMock.verifyResult = .failure(response)
        let password = "123456"
        sut.confirm(password: password)
        
        XCTAssertEqual(presenterSpy.displayKeyboardCallsCount, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayNetworkBannerCallsCount, 1)
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
    }
    
    func testConfirm_WhenReceiveConfirmWithPasswordFromViewController_ShouldDisplayTimeoutError() {
        let event = FactorAuthenticationPJMock.Tracker.trackerPassword(.error(.requestFailed))
        let response = FactorAuthenticationPJMock.Password.Error.timeout
        serviceMock.verifyResult = .failure(response)
        let password = "123456"
        sut.confirm(password: password)
        
        XCTAssertEqual(presenterSpy.displayKeyboardCallsCount, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayNetworkBannerCallsCount, 1)
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
    }
    
    func testConfirm_WhenReceiveConfirmWithPasswordFromViewController_ShouldDisplayBadRequestErrorAndTrackExpired() {
        let event = FactorAuthenticationPJMock.Tracker.trackerPassword(.passwordIncorrect)
        let response = FactorAuthenticationPJMock.Password.Error.badRequestInvalid
        serviceMock.verifyResult = .failure(response)
        let password = "123456"
        sut.confirm(password: password)
        
        XCTAssertEqual(presenterSpy.displayKeyboardCallsCount, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.errorMessage, "Opa, falha na validação. Digite sua senha novamente.")
        XCTAssertEqual(presenterSpy.displayErrorCallsCount, 1)
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
    }
    
    func testConfirm_WhenReceiveConfirmWithPasswordFromViewController_ShouldDisplayBadRequestErrorAndTrackGeneric() {
        let event = FactorAuthenticationPJMock.Tracker.trackerPassword(.error(.generic))
        let response = FactorAuthenticationPJMock.Password.Error.badRequest
        serviceMock.verifyResult = .failure(response)
        let password = "123456"
        sut.confirm(password: password)
        
        XCTAssertEqual(presenterSpy.displayKeyboardCallsCount, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.errorMessage, "Opa, falha na validação. Digite sua senha novamente.")
        XCTAssertEqual(presenterSpy.displayErrorCallsCount, 1)
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
    }
    
    func testConfirm_WhenReceiveConfirmWithPasswordFromViewController_ShouldDisplayDefaultError() {
        let event = FactorAuthenticationPJMock.Tracker.trackerPassword(.error(.generic))
        let response = FactorAuthenticationPJMock.Password.Error.default
        serviceMock.verifyResult = .failure(response)
        let password = "123456"
        sut.confirm(password: password)
        
        XCTAssertEqual(presenterSpy.displayKeyboardCallsCount, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.errorMessage, "Opa, falha na validação. Digite sua senha novamente.")
        XCTAssertEqual(presenterSpy.displayErrorCallsCount, 1)
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
    }
    
    func testTrackPasswordExhibition_WhenReceiveTrackPasswordExhibitionFromViewController_ShouldTrackPasswordExhibition() {
        let event = FactorAuthenticationPJMock.Tracker.trackerPassword(.passwordExhibition)
        sut.trackPasswordExhibition()
        
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
    }
}
