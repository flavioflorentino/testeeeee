import XCTest
@testable import FactorAuthentication

final class PasswordCoordinatorTests: XCTestCase {
    private let navigationSpy = NavigationControllerSpy(rootViewController: UIViewController())
    private let delegateSpy = FactorAuthenticationCoordinatingSpy()
    private lazy var sut: PasswordCoordinating = {
        let coordinator = PasswordCoordinator(delegate: delegateSpy)
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()

    func testPerform_WhenReceiveActionCloseFromPresenter_ShouldCallDelegateToDismissFlow() {
        sut.perform(action: .close)

        XCTAssertEqual(delegateSpy.closeCallsCount, 1)
    }

    func testPerform_WhenReceiveActionCompleteFromPresenter_ShouldCallDelegateToCompleteFlow() {
        let hash = "8BB0CF6EB9B17D0F7D22B456F1212"
        sut.perform(action: .complete(hash))

        XCTAssertEqual(delegateSpy.completeCallsCount, 1)
        XCTAssertEqual(delegateSpy.completeHash, hash)
    }
}
