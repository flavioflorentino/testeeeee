import Core
import Foundation
import XCTest
@testable import FactorAuthentication

final class FactorAuthenticationDataSourceSpy: FactorAuthenticationDataSource {
    var phoneNumber: String?
    var flow: FactorAuthenticationFlow

    init(flow: FactorAuthenticationFlow) {
        self.flow = flow
    }
}
