import AnalyticsModule
import Core
import Foundation
import XCTest
@testable import FactorAuthentication

enum FactorAuthenticationPJMock {
    enum Setup {}
    enum Step {}
    enum Code {}
    enum Password {}
    enum Tracker {}
}

extension FactorAuthenticationPJMock.Setup {
    static let dataSource = FactorAuthenticationDataSourceSpy(flow: .pixOptinEmail)
}

extension FactorAuthenticationPJMock.Step {
    enum Success {
        static let codeResponse = StepResponse(
            type: .code,
            code: FactorAuthenticationPJMock.Code.Success.codeResponseDefault,
            password: nil
        )
        static let codeResponseWithCodeNil = StepResponse(
            type: .code,
            code: nil,
            password: nil
        )
        static let passwordResponse = StepResponse(
            type: .password,
            code: nil,
            password: FactorAuthenticationPJMock.Password.Success.passwordResponse
        )
        static let passwordResponseWithCodeNil = StepResponse(
            type: .password,
            code: nil,
            password: nil
        )
    }
    enum Error {
        static let timeout = ApiError.timeout
        static let connectionFailure = ApiError.connectionFailure
        static let badRequest = ApiError.badRequest(body: RequestError())
        static var badRequestInvalid: ApiError {
            var requestError = RequestError()
            requestError.code = "fa_invalid"
            return ApiError.badRequest(body: requestError)
        }
        static var badRequestExpired: ApiError {
            var requestError = RequestError()
            requestError.code = "fa_expired"
            return ApiError.badRequest(body: requestError)
        }
        static let `default` = ApiError.bodyNotFound
    }
}

extension FactorAuthenticationPJMock.Code {
    enum Success {
        static let codeResponseDefault = CodeResponse(
            navigationTitle: "navigationTitle",
            title: "title",
            description: "description",
            placeholder: "placeholder",
            resendTitle: "resendTitle",
            timeout: 60
        )
        static let codeResponseTimeout1 = CodeResponse(
            navigationTitle: "navigationTitle",
            title: "title",
            description: "description",
            placeholder: "placeholder",
            resendTitle: "resendTitle",
            timeout: 1
        )
        static let verifyCodeResponse = VerifyCodeResponse(hash: "8BB0CF6EB9B17D0F7D22B456F1212")
    }
    enum Error {
        static let timeout = ApiError.timeout
        static let connectionFailure = ApiError.connectionFailure
        static let badRequest = ApiError.badRequest(body: RequestError())
        static var badRequestInvalid: ApiError {
            var requestError = RequestError()
            requestError.code = "fa_invalid"
            return ApiError.badRequest(body: requestError)
        }
        static var badRequestExpired: ApiError {
            var requestError = RequestError()
            requestError.code = "fa_expired"
            return ApiError.badRequest(body: requestError)
        }
        static let `default` = ApiError.bodyNotFound
    }
}

extension FactorAuthenticationPJMock.Tracker {
    static func trackerCode(_ event: CodeEvents) -> Tracker {
        Tracker(flow: .pixOptinEmail, eventType: event)
    }

    static func trackerLoading(_ event: LoadingEvents) -> Tracker {
        Tracker(flow: .pixOptinEmail, eventType: event)
    }
    
    static func trackerPassword(_ event: PasswordEvents) -> Tracker {
        Tracker(flow: .pixOptinEmail, eventType: event)
    }
}

extension FactorAuthenticationPJMock.Password {
    enum Success {
        static let passwordResponse = PasswordResponse(
            navigationTitle: "navigationTitle",
            description: "description",
            placeholder: "placeholder",
            confirmTitle: "confirmTitle"
        )
        static let verifyCodeResponse = VerifyCodeResponse(hash: "8BB0CF6EB9B17D0F7D22B456F1212")
    }
    enum Error {
        static let timeout = ApiError.timeout
        static let connectionFailure = ApiError.connectionFailure
        static let badRequest = ApiError.badRequest(body: RequestError())
        static var badRequestInvalid: ApiError {
            var requestError = RequestError()
            requestError.code = "fa_invalid"
            return ApiError.badRequest(body: requestError)
        }
        static let `default` = ApiError.bodyNotFound
    }
}
