import XCTest
@testable import FactorAuthentication

final class EventsTests: XCTestCase {
    func testTrackerPixCashOut() {
        let tracker = Tracker(flow: .pixCashOut, eventType: CodeEvents.success)
        let event = tracker.event()
        XCTAssertEqual("Pix-CashOut", event.properties["flow"] as? String)
        XCTAssertEqual("sms", event.properties["fa_method"] as? String)
        XCTAssertEqual([.mixPanel], event.providers)
    }
    
    func testTrackerPixCashOutChargeback() {
        let tracker = Tracker(flow: .pixCashOutChargeback, eventType: CodeEvents.success)
        let event = tracker.event()
        XCTAssertEqual("Pix-CashOut", event.properties["flow"] as? String)
        XCTAssertEqual("sms", event.properties["fa_method"] as? String)
        XCTAssertEqual([.mixPanel], event.providers)
    }
    
    func testTrackerPixOptinEmail() {
        let tracker = Tracker(flow: .pixOptinEmail, eventType: CodeEvents.success)
        let event = tracker.event()
        XCTAssertEqual("Pix-Optin", event.properties["flow"] as? String)
        XCTAssertEqual("email", event.properties["fa_method"] as? String)
        XCTAssertEqual([.mixPanel], event.providers)
    }
    
    func testTrackerPixOptinSMS() {
        let tracker = Tracker(flow: .pixOptinSMS, eventType: CodeEvents.success)
        let event = tracker.event()
        XCTAssertEqual("Pix-Optin", event.properties["flow"] as? String)
        XCTAssertEqual("sms", event.properties["fa_method"] as? String)
        XCTAssertEqual([.mixPanel], event.providers)
    }
}
