import XCTest
@testable import FactorAuthentication

final class FactorAuthenticationEndpointTests: XCTestCase {
    func testEndpointCreate() {
        let flow = FactorAuthenticationFlow.pixOptinEmail
        let endpoint = FactorAuthenticationEndpoint.create(flow)
        XCTAssertEqual(endpoint.path, "/fa/create")
        XCTAssertEqual(endpoint.method, .post)
        XCTAssertEqual(endpoint.customHeaders, [:])
        XCTAssertEqual(endpoint.body, ["flow": flow.rawValue].toData())
    }

    func testEndpointCreateWithPhoneNumber() {
        let flow = FactorAuthenticationFlow.sellerOptinWhatsapp
        let phoneNumber = "(11) 12345-1234"
        let endpoint = FactorAuthenticationEndpoint.create(flow, phoneNumber)
        XCTAssertEqual(endpoint.path, "/fa/create")
        XCTAssertEqual(endpoint.method, .post)
        XCTAssertEqual(endpoint.customHeaders, [:])
        XCTAssertEqual(endpoint.body, ["flow": flow.rawValue, "phone_number": phoneNumber].toData())
    }

    func testEndpointVerifyCode() {
        let code = "123456"
        let endpoint = FactorAuthenticationEndpoint.verify(code)
        XCTAssertEqual(endpoint.path, "/fa/send")
        XCTAssertEqual(endpoint.method, .post)
        XCTAssertEqual(endpoint.body, ["code": code].toData())
    }
}
