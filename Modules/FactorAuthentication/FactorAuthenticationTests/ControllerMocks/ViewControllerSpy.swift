@testable import FactorAuthentication

final class ViewControllerSpy: UIViewController {
    private(set) var presentViewControllerCallsCount = 0
    private(set) var presentViewController: UIViewController?

    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        super.present(viewControllerToPresent, animated: flag, completion: completion)
        presentViewController = viewControllerToPresent
        presentViewControllerCallsCount += 1
    }

    private(set) var dismissCallsCount = 0

    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        super.dismiss(animated: flag, completion: completion)
        dismissCallsCount += 1
    }
}
