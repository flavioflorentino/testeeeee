import Foundation
@testable import FactorAuthentication

final class FactorAuthenticationCoordinatingSpy: FactorAuthenticationCoordinating {
    private(set) var startCallsCount = 0

    func start() {
        startCallsCount += 1
    }

    private(set) var closeCallsCount = 0

    func close() {
        closeCallsCount += 1
    }

    private(set) var completeCallsCount = 0
    private(set) var completeHash: String?

    func complete(with hash: String) {
        completeHash = hash
        completeCallsCount += 1
    }
}
