import XCTest
@testable import FactorAuthentication

final class FactorAuthenticationPJErrorTests: XCTestCase {
    func testFactorAuthenticationPJErrorDecode_WhenTryToDecodeError_ShouldDecodeWithErrorsMessage() throws {
        let json = """
            {
             "code": "codigo",
             "message": "mensagem",
             "errorsMessage": {
              "code": [
               "code1",
               "code2"
              ]
             }
            }
        """
        let data = try XCTUnwrap(json.data(using: .utf8))
        let decode = try JSONDecoder().decode(FactorAuthenticationPJError.self, from: data)
        XCTAssertEqual(decode.code, "codigo")
        XCTAssertEqual(decode.message, "mensagem")
        XCTAssertEqual(decode.errors, ["code": ["code1", "code2"]])
    }

    func testFactorAuthenticationPJErrorDecode_WhenTryToDecodeError_ShouldDecodeWithoutErrorsMessage() throws {
        let json = """
            {
             "code": "codigo",
             "message": "mensagem"
            }
        """
        let data = try XCTUnwrap(json.data(using: .utf8))
        let decode = try JSONDecoder().decode(FactorAuthenticationPJError.self, from: data)
        XCTAssertEqual(decode.code, "codigo")
        XCTAssertEqual(decode.message, "mensagem")
        XCTAssertEqual(decode.errors, [:])
    }
}
