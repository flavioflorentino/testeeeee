import Core
import Foundation
import XCTest
@testable import FactorAuthentication

final class RequestErrorExtensionsTests: XCTestCase {
    func testHandleBadRequest_WhenReceiveABadRequestError_ShouldReturnCode() throws {
        let json = """
            {
             "code": "codigo",
             "message": "mensagem",
             "errorsMessage": {
              "code": [
               "code1",
               "code2"
              ]
             }
            }
        """
        let data = try XCTUnwrap(json.data(using: .utf8))
        let requestError = RequestError.initialize(with: data)
        let response = requestError.handleBadRequest(expectedKey: "code")
        XCTAssertEqual(response, "code1")
    }

    func testHandleBadRequest_WhenReceiveABadRequestError_ShouldReturnNil() throws {
        let json = """
            {
             "code": "codigo",
             "message": "mensagem"
            }
        """
        let data = try XCTUnwrap(json.data(using: .utf8))
        let requestError = RequestError.initialize(with: data)
        let response = requestError.handleBadRequest(expectedKey: "code")
        XCTAssertNil(response)
    }
}
