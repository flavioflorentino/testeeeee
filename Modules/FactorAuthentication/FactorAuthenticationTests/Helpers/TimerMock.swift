import Foundation

final class TimerMock: Timer {
    private var block: ((Timer) -> Void)?
    private(set) static var currentTimer: TimerMock?

    static var timerIsValid = false

    override var isValid: Bool {
        TimerMock.timerIsValid
    }

    static func setUp() {
        timerIsValid = false
        invalidateCallsCount = 0
    }

    override func fire() {
        block?(self)
    }

    private(set) static var invalidateCallsCount = 0

    override func invalidate() {
        TimerMock.invalidateCallsCount += 1
    }

    override class func scheduledTimer(withTimeInterval interval: TimeInterval, repeats: Bool, block: @escaping (Timer) -> Void) -> Timer {
        let timer = TimerMock()
        timer.block = block
        currentTimer = timer
        return timer
    }
}
