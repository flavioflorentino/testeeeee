import AnalyticsModule
import Core
import Foundation
import XCTest
@testable import FactorAuthentication

final class DependencyContainerMock: Dependencies {
    lazy var mainQueue: DispatchQueue = resolve()
    lazy var analytics: AnalyticsProtocol = resolve()
    lazy var keychain: KeychainManagerContract = resolve()

    private let dependencies: [Any]

    init(_ dependencies: Any...) {
        self.dependencies = dependencies
    }
}

extension DependencyContainerMock {
    func resolve<T>() -> T {
        let resolved = dependencies.compactMap { $0 as? T }

        switch resolved.first {
        case .none:
            fatalError("DependencyContainerMock could not resolve dependency: \(T.self)\n")
        case .some where resolved.count > 1:
            fatalError("DependencyContainerMock resolved mutiple dependencies for: \(T.self)\n")
        case .some(let mock):
            return mock
        }
    }

    func resolve() -> DispatchQueue {
        let resolved = dependencies.compactMap { $0 as? DispatchQueue }.first
        return resolved ?? DispatchQueue(label: "DependencyContainerMock")
    }

    func resolve() -> AnalyticsProtocol {
        let resolved = dependencies.compactMap { $0 as? AnalyticsProtocol }.first
        return resolved ?? AnalyticsSpy()
    }
}
