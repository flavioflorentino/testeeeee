import XCTest
@testable import AnalyticsModule

final class ThirdPartyTrackerMock: ThirdPartyTracker, ThirdPartyTrackerTime {
    typealias TrackedEvent = (String, [String: Any])
    private(set) var lastTrackedEvent: TrackedEvent?
    private(set) var lastTimedEventName: String?

    func track(_ event: String, properties: [String : Any]) {
        lastTrackedEvent = (event, properties: properties)
    }

    func time(_ event: String) {
       lastTimedEventName = event
    }
}

final class EventSanitizerSpy: EventSanitizing {
    private (set) var cleanNameCount = 0
    private (set) var cleanPropertiesCount = 0

    func clean(name: String) -> String {
        cleanNameCount += 1
        return name
    }

    func clean(properties: [String : Any]) -> [String : Any] {
        cleanPropertiesCount += 1
        return properties
    }
}

final class AnalyticsTests: XCTestCase {
    private lazy var sut: Analytics = .init()

    override func setUp() {
        super.setUp()
        AnalyticsLogger.shared.clean()
    }

    func testAnalytics_WhenRegisteringTracker_ShouldStoreTracker() throws {
        let trackerMock = ThirdPartyTrackerMock()
        let event = AnalyticsEvent("a event", properties: [:], providers: [.appsFlyer])

        sut.register(tracker: trackerMock, for: .appsFlyer)
        sut.log(event)

        let logs = AnalyticsLogger.shared.getLogs()
        let log = try XCTUnwrap(logs.first)

        XCTAssertEqual(logs.count, 1)
        XCTAssertEqual(log.provider, String(describing: AnalyticsProvider.appsFlyer).capitalized)
    }

    func testAnalytics_WhenLoggingEvent_ShouldLogToEachEventProviderTrack() throws {
        let appflyerTracker = ThirdPartyTrackerMock()
        let mixpanelTracker = ThirdPartyTrackerMock()
        let firebaseTracker = ThirdPartyTrackerMock()
        let event = AnalyticsEvent("a event", properties: [:], providers: [.appsFlyer, .mixPanel, .firebase])

        sut.register(tracker: appflyerTracker, for: .appsFlyer)
        sut.register(tracker: mixpanelTracker, for: .mixPanel)
        sut.register(tracker: firebaseTracker, for: .firebase)
        sut.log(event)

        let logs = AnalyticsLogger.shared.getLogs()
        XCTAssertEqual(logs.count, 3)

        let logsProviders = logs.compactMap { $0.provider }
        let exptectedProviders = [
            String(describing: AnalyticsProvider.appsFlyer).capitalized,
            String(describing: AnalyticsProvider.mixPanel).capitalized,
            String(describing: AnalyticsProvider.firebase).capitalized
        ]

        XCTAssertEqual(exptectedProviders, logsProviders)
    }

    func testAnalytics_WhenRegisteringTrackerSanitizer_ShouldStoreTrackerAndSanitizer() {
        let trackerMock = ThirdPartyTrackerMock()
        let sanitizerSpy = EventSanitizerSpy()
        let event = AnalyticsEvent("a event", properties: [:], providers: [.appsFlyer])

        sut.register(tracker: trackerMock, for: .appsFlyer, with: sanitizerSpy)
        sut.log(event)

        XCTAssertEqual(sanitizerSpy.cleanNameCount, 1)
        XCTAssertEqual(sanitizerSpy.cleanPropertiesCount, 1)
        XCTAssertEqual(AnalyticsLogger.shared.getLogs().count, 1)
    }

    func testAnalytics_WhenLoggingEventForProviderWithSanitizer_ShouldSanitizeEventBeforeLogging() throws {
        let trackerMock = ThirdPartyTrackerMock()
        let event = AnalyticsEvent(
            "a event-with    invalid name",
            properties: ["invalid-property-key": "a value"],
            providers: [.appsFlyer]
        )

        sut.register(tracker: trackerMock, for: .appsFlyer, with: FirebaseSanitizer())
        sut.log(event)

        let loggedEvent = try XCTUnwrap(AnalyticsLogger.shared.getLogs().first)
        let eventName = try XCTUnwrap(loggedEvent.event.keys.first)
        let eventProperties = try XCTUnwrap(loggedEvent.event.values.first)

        XCTAssertEqual(eventName, "a_eventwith_invalid_name")
        XCTAssert(eventProperties.isEqual(to: ["invalidpropertykey": "a value"]))
    }
}

private extension Dictionary where Key == String, Value: Any {
    func isEqual(to other: [String: Any]) -> Bool {
        NSDictionary(dictionary: self).isEqual(to: other)
    }
}
