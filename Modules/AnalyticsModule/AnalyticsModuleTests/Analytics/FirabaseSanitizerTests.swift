import XCTest
@testable import AnalyticsModule

final class FirebaseSanitizerTests: XCTestCase {
    private lazy var sut: FirebaseSanitizer = FirebaseSanitizer.init()

    func testCleanName_WhenContainsUppercaseChars_ShouldLowecaseName() {
        let invalidName = "NameWithUppercase"

        let cleanName = sut.clean(name: invalidName)

        XCTAssertEqual(cleanName, "namewithuppercase")
    }

    func testCleanName_WhenContainsDashChar_ShouldRemoveDashes() {
        let invalidName = "Name-With-Dashes"

        let cleanName = sut.clean(name: invalidName)

        XCTAssertEqual(cleanName, "namewithdashes")
    }
    
    func testCleanName_WhenContainsSpaces_ShouldReplaceWithUnderscore() {
        let invalidName = "Name With    Spaces"

        let cleanName = sut.clean(name: invalidName)

        XCTAssertEqual(cleanName, "name_with_spaces")
    }
    
    func testCleanName_WhenBigName_ShouldCropToMaxSize() {
        let invalidName = "NameWithALongNumberOfCharacteresMoreThanForty"

        let cleanName = sut.clean(name: invalidName)

        XCTAssertEqual(cleanName, "namewithalongnumberofcharacteresmorethan")
    }

    func testCleanName_WhenSmallName_ShouldNotCrop() {
        let invalidName = "SmallName"

        let cleanName = sut.clean(name: invalidName)

        XCTAssertEqual(cleanName, "smallname")
    }

    func testCleanName_WhenNameEmptyString_ShouldReturnName() {
        let invalidName = ""

        let cleanName = sut.clean(name: invalidName)

        XCTAssertEqual(cleanName, "")
    }

    func testCleanProperties_WhenKeyContainsUppercaseChars_ShouldLowecaseProperties() {
        let invalidProperties: [String: Any] = ["PropertiesWithUppercase": "a value"]
        let expectedProperties: [String: Any] = ["propertieswithuppercase": "a value"]

        let cleanProperties = sut.clean(properties: invalidProperties)

        XCTAssert(cleanProperties.isEqual(to: expectedProperties))
    }

    func testCleanProperties_WhenContainsDashChar_ShouldRemoveDashes() {
        let invalidProperties: [String: Any] = ["Key-With-Dashes": "a value"]
        let expectedProperties: [String: Any] = ["keywithdashes": "a value"]

        let cleanProperties = sut.clean(properties: invalidProperties)

        XCTAssert(cleanProperties.isEqual(to: expectedProperties))
    }

    func testCleanProperties_WhenContainsSpaces_ShouldReplaceWithUnderscore() {
        let invalidProperties: [String: Any] = ["Key With     Spaces": "a value"]
        let expectedProperties: [String: Any] = ["key_with_spaces": "a value"]

        let cleanProperties = sut.clean(properties: invalidProperties)

        XCTAssert(cleanProperties.isEqual(to: expectedProperties))
    }

    func testCleanProperties_WhenBigProperties_ShouldCropToMaxSize() {
        let invalidProperties: [String: Any] = ["PropertiesKeyWithALongNumberOfCharacteresMoreThanForty": "a value"]
        let expectedProperties: [String: Any] = ["propertieskeywithalongnumberofcharactere": "a value"]

        let cleanProperties = sut.clean(properties: invalidProperties)

        XCTAssert(cleanProperties.isEqual(to: expectedProperties))
    }

    func testCleanProperties_WhenSmallProperties_ShouldNotCrop() {
        let invalidProperties: [String: Any] = ["smallkey": "a value"]
        let expectedProperties: [String: Any] = ["smallkey": "a value"]

        let cleanProperties = sut.clean(properties: invalidProperties)

        XCTAssert(cleanProperties.isEqual(to: expectedProperties))
    }

    func testCleanProperties_WhenPropertiesEmptyString_ShouldReturnProperties() {
        let invalidProperties: [String: Any] = ["": "a value"]
        let expectedProperties: [String: Any] = ["": "a value"]

        let cleanProperties = sut.clean(properties: invalidProperties)

        XCTAssert(cleanProperties.isEqual(to: expectedProperties))
    }

    func testCleanProperties_WhenBigPropertiesValue_ShouldCropValueToMaxSize() {
        let invalidProperties: [String: Any] = [
            "key": "a really big value with more than one hundred chars said twice a really big value with more than one hundred chars"
        ]
        let expectedProperties: [String: Any] = [
            "key": "a really big value with more than one hundred chars said twice a really big value with more than one"
        ]

        let cleanProperties = sut.clean(properties: invalidProperties)

        XCTAssert(cleanProperties.isEqual(to: expectedProperties))
    }
}

private extension Dictionary where Key == String, Value: Any {
    func isEqual(to other: [String: Any]) -> Bool {
        NSDictionary(dictionary: self).isEqual(to: other)
    }
}
