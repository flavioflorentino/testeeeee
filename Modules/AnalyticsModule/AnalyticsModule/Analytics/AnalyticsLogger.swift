import Foundation

public typealias AnalyticsLoggerEvent = [String: [String: Any]]
public struct AnalyticsLoggerTrack {
    public let provider: String
    public let event: AnalyticsLoggerEvent
    
    public init(provider: String, event: AnalyticsLoggerEvent) {
        self.provider = provider
        self.event = event
    }
}

public protocol AnalyticsLoggerProtocol {
    func clean()
    func getLogs() -> [AnalyticsLoggerTrack]
    func info(with eventName: String, properties: [String: Any], from provider: String)
    func info(with eventName: String, properties: [String: Any], from provider: AnalyticsProvider)
}

public extension AnalyticsLoggerProtocol {
    func info(with eventName: String, properties: [String: Any], from provider: AnalyticsProvider) {
        let providerName = String(describing: provider).capitalized
        info(with: eventName, properties: properties, from: providerName)
    }
}

@objcMembers
public final class AnalyticsLogger: NSObject, AnalyticsLoggerProtocol {
    public static let shared = AnalyticsLogger()

    // Logs are added from various threads concurrently, we implement read/write pattern using this
    // queue to ensure writes are thread safe.
    private let syncQueue = DispatchQueue(label: "com.picpay.analyticslogger.queue", attributes: .concurrent)
    // Reads and writes to this array must ensure thread safety by using `syncQueue`
    private var logs: [AnalyticsLoggerTrack] = []

    public func clean() {
        syncQueue.async(flags: .barrier) { [weak self] in
            self?.logs.removeAll()
        }
    }
    
    public func getLogs() -> [AnalyticsLoggerTrack] {
        syncQueue.sync { self.logs }
    }
    
    public func info(with eventName: String, properties: [String: Any], from provider: String) {
        let eventLog = AnalyticsLoggerTrack(provider: provider, event: [eventName: properties])

        syncQueue.async(flags: .barrier) { [weak self] in
            self?.logs.append(eventLog)
        }

        #if DEBUG
        NSLog("[Analytics] - '\(provider)' event: '\(eventName)' with properties: \(properties)")
        #endif
    }
}
