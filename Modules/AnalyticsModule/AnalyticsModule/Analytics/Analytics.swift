public protocol HasAnalytics {
    var analytics: AnalyticsProtocol { get }
}

public protocol AnalyticsProtocol {
    func log(_ eventKey: AnalyticsKeyProtocol)
    func time(_ eventKey: AnalyticsKeyProtocol)
    func register(tracker: ThirdPartyTracker, for provider: AnalyticsProvider, with sanitizer: EventSanitizing?)
    func reset()
}

public extension AnalyticsProtocol {
    func register(tracker: ThirdPartyTracker, for provider: AnalyticsProvider) {
        register(tracker: tracker, for: provider, with: nil)
    }
}

public final class Analytics: AnalyticsProtocol {
    private var trackers: [AnalyticsProvider: ThirdPartyTracker] = [:]
    private var sanitizers: [AnalyticsProvider: EventSanitizing] = [:]

    public static let shared = Analytics()

    internal init() { }

    public func register(tracker: ThirdPartyTracker, for provider: AnalyticsProvider, with sanitizer: EventSanitizing? = nil) {
        if let sanitizer = sanitizer {
            sanitizers[provider] = sanitizer
        }
        trackers[provider] = tracker
    }

    public func log(_ eventKey: AnalyticsKeyProtocol) {
        let event = eventKey.event()
        for provider in event.providers {
            guard let tracker = trackers[provider] else { continue }

            let sanitizer = sanitizers[provider]
            let cleanName = sanitizer?.clean(name: event.name) ?? event.name
            let cleanProperties = sanitizer?.clean(properties: event.properties) ?? event.properties

            #if DEBUG
            AnalyticsLogger.shared.info(with: cleanName, properties: cleanProperties, from: provider)
            #else
            tracker.track(cleanName, properties: cleanProperties)
            #endif
        }
    }
    
    public func time(_ eventKey: AnalyticsKeyProtocol) {
        let event = eventKey.event()
        for provider in event.providers {
            if let tracker = trackers[provider] as? ThirdPartyTrackerTime {
                #if DEBUG
                AnalyticsLogger.shared.info(with: event.name, properties: event.properties, from: provider)
                #else
                tracker.time(event.name)
                #endif
            }
        }
    }

    public func reset() {
        for tracker in trackers.values {
            if let resetableTracker = tracker as? ThirdPartyTrackerReseting {
                resetableTracker.reset()
            }
        }
    }
}
