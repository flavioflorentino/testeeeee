import Foundation

public protocol ThirdPartyTracker {
    func track(_ event: String, properties: [String: Any])
}

public protocol ThirdPartyTrackerTime {
    func time(_ event: String)
}

public protocol ThirdPartyTrackerReseting {
    func reset()
}

public protocol AppsFlyerContract: ThirdPartyTracker {}
public protocol MixPanelContract: ThirdPartyTracker, ThirdPartyTrackerTime, ThirdPartyTrackerReseting {}
public protocol FirebaseContract: ThirdPartyTracker {}
public protocol EventTrackerContract: ThirdPartyTracker, ThirdPartyTrackerReseting {}
