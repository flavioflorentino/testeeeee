import Foundation

@objcMembers
public class OBJCAnalytics: NSObject {
    public static func log(name: String, properties: [String: Any]) {
        let event = AnalyticsEvent(name, properties: properties, providers: [.mixPanel, .firebase, .appsFlyer])
        Analytics.shared.log(event)
    }
    
    public static func logFirebase(name: String, properties: [String: Any]) {
        let event = AnalyticsEvent(name, properties: properties, providers: [.firebase])
        Analytics.shared.log(event)
    }

    public static func logMixpanel(name: String, properties: [String: Any]) {
        let event = AnalyticsEvent(name, properties: properties, providers: [.mixPanel])
        Analytics.shared.log(event)
    }
}
