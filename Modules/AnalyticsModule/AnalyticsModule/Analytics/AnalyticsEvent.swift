public protocol AnalyticsEventProtocol {
    var name: String { get set }
    var properties: [String: Any] { get set }
    var providers: [AnalyticsProvider] { get set }
}

extension AnalyticsEventProtocol {
    private var providers: [AnalyticsProvider] { [.firebase] }
}

public protocol AnalyticsKeyProtocol {
    func event() -> AnalyticsEventProtocol
}

public struct AnalyticsEvent: AnalyticsEventProtocol, AnalyticsKeyProtocol {
    public var name: String
    public var properties: [String: Any]
    public var providers: [AnalyticsProvider]
    
    public init(
        _ name: String,
        properties: [String: Any] = [:],
        providers: [AnalyticsProvider] = [.eventTracker]
    ) {
        self.name = name
        self.properties = properties
        self.providers = providers
    }
    
    public func event() -> AnalyticsEventProtocol { self }
}
