import Foundation

public protocol EventSanitizing {
    func clean(name: String) -> String
    func clean(properties: [String: Any]) -> [String: Any]
}

public struct FirebaseSanitizer: EventSanitizing {
    private let maxNameCharacters = 40
    private let maxValueCharacters = 100

    public init() {}

    public func clean(name: String) -> String {
        sanitize(name, maxCharacters: maxNameCharacters)
    }

    public func clean(properties: [String: Any]) -> [String: Any] {
        properties.reduce(into: [:]) { result, kv in
            let key = sanitize(kv.key, maxCharacters: maxNameCharacters)
            let value = crop("\(kv.value)", to: maxValueCharacters)
            result[key] = value
        }
    }
}

private extension FirebaseSanitizer {
    func sanitize(_ string: String, maxCharacters: Int) -> String {
        let cleanString = string
            .data(using: .ascii, allowLossyConversion: true)
            .flatMap { String(data: $0, encoding: .ascii) }
            .map { $0.lowercased() }
            .map { $0.replacingOccurrences(of: "-", with: "") }
            .map { replaceSpacesToUnderscore($0) }
            .map { crop($0, to: maxCharacters) }

        return cleanString ?? ""
    }

    func replaceSpacesToUnderscore(_ string: String) -> String {
        do {
            let regex = try NSRegularExpression(pattern: "\\s+", options: .caseInsensitive)
            let range = NSRange(location: 0, length: string.count)
            return regex.stringByReplacingMatches(in: string, range: range, withTemplate: "_")
        } catch {
            return string.replacingOccurrences(of: " ", with: "_")
        }
    }
    
    func crop(_ string: String, to size: Int) -> String {
        guard size >= 0 else {
            return string
        }

        return String(string.prefix(size))
    }
 }
