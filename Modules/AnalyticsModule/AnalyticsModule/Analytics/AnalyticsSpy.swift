#if DEBUG
import Foundation

public final class AnalyticsSpy: AnalyticsProtocol {
    public private(set) var event: AnalyticsEventProtocol?
    public private(set) var events: [AnalyticsEventProtocol] = []
    public private(set) var logCalledCount = 0
    public private(set) var timeCalledCount = 0
    public private(set) var registerCalledCount = 0
    public private(set) var resetCalledCount = 0
    
    public init() {}

    public func log(_ event: AnalyticsKeyProtocol) {
        self.event = event.event()
        self.events.append(event.event())
        self.logCalledCount += 1
    }
    
    public func time(_ event: AnalyticsKeyProtocol) {
        self.event = event.event()
        self.events.append(event.event())
        timeCalledCount += 1
    }

    public func register(tracker: ThirdPartyTracker, for: AnalyticsProvider, with: EventSanitizing?) {
        registerCalledCount += 1
    }

    public func reset() {
        self.event = nil
        self.events.removeAll()
        resetCalledCount += 1
    }

    public func equals(to: AnalyticsEventProtocol) -> Bool {
        guard let event = event else {
            return false
        }
        return compare(first: event, second: to)
    }
    
    public func equals(to event: AnalyticsEventProtocol...) -> Bool {
        events.elementsEqual(event, by: compare(first:second:))
    }

    public func value(forKey key: String) -> String? {
        guard let value = event?.properties[key] else {
            return nil
        }
    
        return "\(value)"
    }
    
    private func compare(first: AnalyticsEventProtocol, second: AnalyticsEventProtocol) -> Bool {
        first.name == second.name &&
        NSDictionary(dictionary: first.properties).isEqual(to: second.properties) &&
        first.providers == second.providers
    }
}

#endif
