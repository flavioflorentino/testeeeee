import AppTrackingTransparency
import Foundation

public typealias AppTrackingAuthorizationCompletion = (AppTrackingAuthorizationStatus) -> Void

public enum AppTrackingAuthorizationStatus {
    case notDetermined
    case authorized
    case denied
    case restricted
    case unknown
}

public enum AppTracking {
    public static func requestAuthorization(completion: @escaping AppTrackingAuthorizationCompletion = { _ in }) {
        if #available(iOS 14, *) {
            ATTrackingManager.requestTrackingAuthorization { status in
                switch status {
                case .authorized:
                    // Tracking authorization dialog was shown and we are authorized
                    // Now that we are authorized we can get the IDFA
                    completion(.authorized)
                case .denied:
                    // Tracking authorization dialog was shown and permission is denied
                    completion(.denied)
                case .notDetermined:
                    // Tracking authorization dialog has not been shown
                    completion(.notDetermined)
                case .restricted:
                    completion(.restricted)
                @unknown default:
                    completion(.unknown)
                }
            }
        } else {
            completion(.authorized)
        }
    }
}
