public enum AnalyticsProvider: CaseIterable {
    case appsFlyer
    case mixPanel
    case firebase
    case eventTracker
}
