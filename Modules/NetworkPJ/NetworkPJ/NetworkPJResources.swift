import Foundation

// swiftlint:disable convenience_type
final class NetworkPJResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: NetworkPJResources.self).url(forResource: "NetworkPJResources", withExtension: "bundle") else {
            return Bundle(for: NetworkPJResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: NetworkPJResources.self)
    }()
}
