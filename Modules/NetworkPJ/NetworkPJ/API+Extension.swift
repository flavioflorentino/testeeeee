import Core

public extension ApiEndpointExposable {
    func headerWith(pin: String) -> [String: String] {
        ["pin": pin]
    }
}

public extension Core.Api {
    func bizExecute<T: Decodable>(
        jsonDecoder: JSONDecoder = JSONDecoder(),
        completion: @escaping Completion
    ) where E == ResponseBiz<T> {
        execute(jsonDecoder: jsonDecoder) { result in
            switch result {
            case .success(let response):
                self.handleBizSuccess(response: response, completion: completion)
            case .failure(let apiError):
                self.handleApiError(apiError: apiError, completion: completion)
            }
        }
    }
}

private extension Core.Api {
    func handleBizSuccess<T: Decodable>(response: Success, completion: Completion) where E == ResponseBiz<T> {
        guard response.data != nil else {
            completion(.failure(.bodyNotFound))
            return
        }
        
        completion(.success(response))
    }
    
    func handleApiError(apiError: ApiError, completion: Completion) {
        if let data = apiError.requestError?.jsonData,
            let errorBiz = try? JSONDecoder().decode(ErrorBiz.self, from: data) {
            completion(.failure(.badRequest(body: errorBiz.error)))
            return
        }
    }
}
