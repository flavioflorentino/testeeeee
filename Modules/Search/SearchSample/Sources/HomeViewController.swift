import UIKit
import UI
import Search
import SearchKit

class HomeViewController: UIViewController {

    private lazy var p2pSearchButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(LinkButtonStyle())
        button.setTitle("Busca Pessoas", for: .normal)
        button.addTarget(self, action: #selector(goToPeopleSearch), for: .touchUpInside)
        return button
    }()

    init() {
        super.init(nibName: nil, bundle: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc
    private func goToPeopleSearch() {
        let viewController = P2PSearchFactory.make()
        navigationController?.pushViewController(viewController, animated: true)
    }
}

extension HomeViewController: ViewConfiguration {
    func buildViewHierarchy() {
        view.addSubview(p2pSearchButton)
    }

    func setupConstraints() {
        p2pSearchButton.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }

    func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}
