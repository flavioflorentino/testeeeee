import AnalyticsModule
import Core
import UIKit

public final class SearchFactory {
    public typealias Dependencies = HasAnalytics & HasMainQueue

    // MARK: - Properties
    private let dependencies: Dependencies

    // MARK: - Initialization
    public init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

public extension SearchFactory {
    func makeSearchCoordinator(
        title: String?,
        searchType: SearchType,
        shouldShowContacts: Bool = true,
        navigationController: UINavigationController = UINavigationController(),
        analyticsData: SearchAnalyticsData? = nil
    ) -> SearchCoordinating {
        SearchCoordinator(
            title: title,
            searchType: searchType,
            shouldShowContacts: shouldShowContacts,
            navigationController: navigationController,
            analyticsData: analyticsData,
            controllerFactory: self
        )
    }
}

// MARK: - SearchListControllerFactory
extension SearchFactory: SearchListControllerFactory {
    func makeSearchListController(
        searchType: SearchType,
        shouldShowContacts: Bool,
        analyticsData: SearchAnalyticsData?,
        didSelectAction: ((SearchSelection) -> Void)?
    ) -> Controller {
        let presenter = SearchListPresenter()
        presenter.didSelect = didSelectAction

        let interactor = SearchListInteractor(
            service: SearchService(dependencies: dependencies),
            presenter: presenter,
            searchType: searchType,
            emptySearchReturnsContacts: shouldShowContacts,
            contactsPermission: SearchListContactsPermissionWrapper(),
            dependency: dependencies,
            analyticsData: analyticsData
        )

        let viewController = SearchListViewController(interactor: interactor)
        presenter.display = viewController

        return viewController
    }
}
