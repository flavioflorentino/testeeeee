import Core
import Foundation

// MARK: - SearchType
public enum SearchType: String {
    case consumers
    case contacts
}

public protocol SearchServicing {
    @discardableResult
    func search(
        term: String?,
        type: SearchType,
        page: Int,
        _ completion: @escaping (Result<[SearchResult], ApiError>) -> Void
    ) -> URLSessionTask?
}
