import Core

struct SearchServiceEndpoint {
    // MARK: - Properties
    private let term: String?
    private let group: String
    private let page: Int

    // MARK: - Initialization
    private init(term: String?, group: String, page: Int) {
        self.term = term
        self.group = group.uppercased()
        self.page = page
    }

    static func search(term: String? = nil, group: String, page: Int = 0) -> SearchServiceEndpoint {
        .init(term: term, group: group, page: page)
    }
}

// MARK: - Parameters
private extension SearchServiceEndpoint {
    enum Parameter: String {
        case term
        case group
        case page
    }
}

// MARK: - ApiEndpointExposable
extension SearchServiceEndpoint: ApiEndpointExposable {
    var path: String { "search" }

    var parameters: [String: Any] {
        var parameters: [String: Any] = [
            Parameter.group.rawValue: group,
            Parameter.page.rawValue: page
        ]

        if let term = term {
            parameters[Parameter.term.rawValue] = term
        }

        return parameters
    }
}
