import Core
import Foundation

struct SearchService {
    typealias Dependencies = HasMainQueue

    // MARK: - Properties
    private let dependencies: Dependencies

    // MARK: - Initialization
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - SearchServicing
extension SearchService: SearchServicing {
    @discardableResult
    func search(
        term: String?,
        type: SearchType,
        page: Int,
        _ completion: @escaping (Result<[SearchResult], ApiError>) -> Void
    ) -> URLSessionTask? {
        let endpoint = SearchServiceEndpoint.search(term: term, group: type.rawValue, page: page)
        return Api<[SearchResponseModel]>(endpoint: endpoint).execute(jsonDecoder: .init(.convertFromSnakeCase)) { result in
            self.dependencies.mainQueue.async {
                completion(result.map {
                    $0.model.flatMap(\.rows)
                })
            }
        }
    }
}

// MARK: - Helper models
private struct SearchResponseModel: Decodable {
    let rows: [SearchResult]
}
