import Foundation

// swiftlint:disable convenience_type
final class SearchResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: SearchResources.self).url(forResource: "SearchResources", withExtension: "bundle") else {
            return Bundle(for: SearchResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: SearchResources.self)
    }()
}