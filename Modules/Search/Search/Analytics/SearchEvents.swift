import AnalyticsModule

enum SearchEvents {
    case cardContactsPermission(origin: String)
    case paymentItemAccessed(type: String, name: String, id: String, section: (name: String, position: Int))
    case searchAccessed(section: String)
    case searchResultViewed(isEmpty: Bool, query: String, section: String)
}

// MARK: - Private
private extension SearchEvents {
    var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }

    var name: String {
        switch self {
        case .cardContactsPermission:
            return "Card Contacts Permission"
        case .paymentItemAccessed:
            return "Payment Item Accessed"
        case .searchAccessed:
            return "Pagar - Search Accessed"
        case .searchResultViewed:
            return "Pagar - Search Result Viewed"
        }
    }

    var properties: [String: Any] {
        switch self {
        case let .cardContactsPermission(origin):
            return ["origin": origin]
        case let .paymentItemAccessed(type, name, id, section):
            return [
                "type": type,
                "item_name": name,
                "id": id,
                "section": section.name,
                "section_position": section.position
            ]
        case let .searchAccessed(section):
            return ["section": section]
        case let .searchResultViewed(isEmpty, query, section):
            return [
                "tab": section,
                "query": query,
                "is_empty": isEmpty,
                "section": section
            ]
        }
    }
}

// MARK: - AnalyticsKeyProtocol
extension SearchEvents: AnalyticsKeyProtocol {
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
