public struct SearchAnalyticsData {
    let cardContactsPermissionOrigin: String
    let paymentItemAccessedSection: String
    let searchAccessedSection: String
    let searchResultViewedSection: String

    public init(
        cardContactsPermissionOrigin: String,
        paymentItemAccessedSection: String,
        searchAccessedSection: String,
        searchResultViewedSection: String
    ) {
        self.cardContactsPermissionOrigin = cardContactsPermissionOrigin
        self.paymentItemAccessedSection = paymentItemAccessedSection
        self.searchAccessedSection = searchAccessedSection
        self.searchResultViewedSection = searchResultViewedSection
    }
}
