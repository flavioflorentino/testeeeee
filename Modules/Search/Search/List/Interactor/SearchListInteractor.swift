import AnalyticsModule
import Core
import Foundation
import PermissionsKit

final class SearchListInteractor {
    typealias Dependency = HasAnalytics

    // MARK: - Properties
    private let dependency: Dependency
    private let service: SearchServicing
    private let presenter: SearchListPresenting
    private let searchType: SearchType
    private let emptySearchReturnsContacts: Bool
    private let minimumCharactersToSearch: Int
    private let contactsPermission: SearchListContactsPermission
    private let analyticsData: SearchAnalyticsData?

    private var currentPage: Int = 0
    private var lastSearchTerm: String?
    private var lastSearchType: SearchType?

    private var timer: Timer? {
        didSet {
            oldValue?.invalidate()
            previousRequest = nil
        }
    }

    private weak var previousRequest: URLSessionTask? {
        didSet {
            oldValue?.cancel()
        }
    }

    // MARK: - Initialization
    init(
        service: SearchServicing,
        presenter: SearchListPresenting,
        searchType: SearchType,
        emptySearchReturnsContacts: Bool,
        minimumCharactersToSearch: Int = 3,
        contactsPermission: SearchListContactsPermission,
        dependency: Dependency,
        analyticsData: SearchAnalyticsData?
    ) {
        self.service = service
        self.presenter = presenter
        self.searchType = searchType
        self.emptySearchReturnsContacts = emptySearchReturnsContacts
        self.minimumCharactersToSearch = minimumCharactersToSearch
        self.contactsPermission = contactsPermission
        self.analyticsData = analyticsData
        self.dependency = dependency
    }
}

// MARK: - SearchListInteracting
extension SearchListInteractor: SearchListInteracting {
    func loadData() {
        handleEmptySearchTerm()
    }

    func search(term: String, delaying: Bool) {
        guard term.isNotEmpty else {
            timer = nil
            return handleEmptySearchTerm()
        }

        guard term.count >= minimumCharactersToSearch else {
            return
        }

        presenter.showLoading()
        if delaying {
            timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { [weak self] _ in
                self?.search(term: term)
            }
        } else {
            search(term: term)
        }
    }

    func didStartTypeToSearch() {
        guard let analyticsData = analyticsData else { return }

        dependency.analytics.log(SearchEvents.searchAccessed(section: analyticsData.searchAccessedSection))
    }

    func loadMoreResults() {
        guard let term = lastSearchTerm, let type = lastSearchType else { return }

        previousRequest = service.search(term: term, type: type, page: currentPage + 1) { [weak self] result in
            switch result {
            case let .success(values):
                self?.currentPage += 1
                self?.presenter.showMore(results: values.filter { $0.type != .unknown })
            case .failure:
                break
            }
        }
    }

    func requestContactsPermission() {
        guard contactsPermission.status != .authorized else {
            return presenter.hideLoading()
        }

        presenter.showLoading()
        contactsPermission.requestContacts { [weak self] status in
            self?.handleContactsPermissionStatusDidChange(to: status)
        }

        guard let analyticsData = analyticsData else { return }
        dependency.analytics.log(SearchEvents.cardContactsPermission(origin: analyticsData.cardContactsPermissionOrigin))
    }

    func didSelect(result: SearchResult, at index: IndexPath) {
        presenter.didSelect(selection: .single(result))

        guard let analyticsData = analyticsData else { return }
        let section = "\(analyticsData.paymentItemAccessedSection) - \(paymentItemAccessedSectionSuffix(for: result))"
        dependency.analytics.log(
            SearchEvents.paymentItemAccessed(
                type: paymentItemType(for: result),
                name: paymentItemAccessedName(for: result),
                id: paymentItemAccessedId(for: result),
                section: (name: section, position: index.row)
            )
        )
    }
}

// MARK: - Private
private extension SearchListInteractor {
    func search(term: String) {
        lastSearchTerm = term
        lastSearchType = searchType
        currentPage = 0

        let type = searchType
        previousRequest = service.search(term: term, type: searchType, page: currentPage) { [weak self] result in
            switch result {
            case let .success(values):
                let values = values.filter { $0.type != .unknown }
                self?.presenter.show(results: values, type: type)
                self?.sendSearchResultEvent(term: term, values: values)
            case let .failure(error):
                switch error {
                case .cancelled:
                    break
                default:
                    self?.presenter.show(error: error, type: type)
                }
            }
        }
    }

    func sendSearchResultEvent(term: String, values: [SearchResult]) {
        guard let analyticsData = analyticsData else { return }
        dependency.analytics.log(
            SearchEvents.searchResultViewed(
                isEmpty: values.isEmpty,
                query: term,
                section: analyticsData.searchResultViewedSection
            )
        )
    }

    func paymentItemAccessedSectionSuffix(for item: SearchResult) -> String {
        switch item.type {
        case .consumer:
            return "resultados"
        case .contact:
            return "contatos"
        default:
            return ""
        }
    }

    func paymentItemAccessedName(for item: SearchResult) -> String {
        switch item {
        case let .consumer(data), let .contact(data):
            return data.name
        default:
            return ""
        }
    }

    func paymentItemType(for item: SearchResult) -> String {
        switch item.type {
        case .contact, .consumer:
            return "consumer"
        default:
            return ""
        }
    }

    func paymentItemAccessedId(for item: SearchResult) -> String {
        switch item {
        case let .consumer(data), let .contact(data):
            return data.id
        default:
            return ""
        }
    }

    func handleEmptySearchTerm() {
        guard emptySearchReturnsContacts else {
            return
        }

        guard contactsPermission.status == .authorized else {
            return presenter.showContactsPermissionWarning()
        }

        presenter.showLoading()
        requestContacts()
    }

    func requestContacts() {
        lastSearchTerm = nil
        lastSearchType = .contacts
        currentPage = 0

        previousRequest = service.search(term: nil, type: .contacts, page: currentPage) { [weak self] result in
            switch result {
            case let .success(values):
                self?.presenter.show(results: values.filter { $0.type != .unknown }, type: .contacts)
            case let .failure(error):
                switch error {
                case .cancelled:
                    break
                default:
                    self?.presenter.show(error: error, type: .contacts)
                }
            }
        }
    }

    func handleContactsPermissionStatusDidChange(to status: PermissionStatus) {
        guard status == .authorized else {
            return presenter.showContactsPermissionWarning()
        }

        requestContacts()
    }
}
