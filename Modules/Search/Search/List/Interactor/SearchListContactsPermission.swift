import PermissionsKit

protocol SearchListContactsPermission {
    var status: PermissionStatus { get }

    func requestContacts(completion: @escaping Permission.Callback)
}

// MARK: - Wrapper
final class SearchListContactsPermissionWrapper {
    // MARK: - Properties
    private lazy var permission: Permission = .contacts

    // MARK: - Initialization
    init() { }
}

extension SearchListContactsPermissionWrapper: SearchListContactsPermission {
    var status: PermissionStatus { permission.status }

    func requestContacts(completion: @escaping Permission.Callback) {
        permission.request { completion($0) }
    }
}
