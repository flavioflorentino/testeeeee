import Core

public protocol SearchListInteracting {
    func loadData()
    func didStartTypeToSearch()
    func search(term: String, delaying: Bool)
    func loadMoreResults()

    func requestContactsPermission()

    func didSelect(result: SearchResult, at index: IndexPath)
}
