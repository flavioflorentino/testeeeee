import Foundation
import SnapKit
import UI
import UIKit

// MARK: - Layout
private extension SearchListHeaderView.Layout {
    enum ProBadge {
        static let size = CGSize(width: 28.0, height: 14.0)
        static let cornerRadius: CGFloat = 2.8
    }

    enum VerifiedBadge {
        static let size = CGSize(width: 17.0, height: 17.0)
        static let cornerRadius: CGFloat = size.height / 2.0
        static let margin: CGFloat = 2.0
    }
}

final class SearchListHeaderView: UICollectionReusableView {
    fileprivate enum Layout { }

    // MARK: - Properties
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.backgroundColor, .backgroundPrimary())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .left)
        label.numberOfLines = 1
        label.clipsToBounds = true

        return label
    }()

    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setup(title: String) {
        titleLabel.text = title
    }
}

// MARK: - ViewConfiguration
extension SearchListHeaderView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(titleLabel)
    }

    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }

    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
}
