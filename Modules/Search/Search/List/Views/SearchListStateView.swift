import Foundation
import SnapKit
import UI
import UIKit

// MARK: - Layout
private extension SearchListStateView.Layout {
    enum ProBadge {
        static let size = CGSize(width: 28.0, height: 14.0)
        static let cornerRadius: CGFloat = 2.8
    }

    enum VerifiedBadge {
        static let size = CGSize(width: 17.0, height: 17.0)
        static let cornerRadius: CGFloat = size.height / 2.0
        static let margin: CGFloat = 2.0
    }

    enum ActionButton {
        static let width: CGFloat = 168.0
    }
}

final class SearchListStateView: UIView {
    fileprivate enum Layout { }

    enum State {
        case normal
        case message(value: String)
        case error(message: String, buttonText: String? = nil, buttonAction: (() -> Void)? = nil)
        case warning(view: UIView)
        case loading
    }

    // MARK: - Properties
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let indicator = createActivityIndicatorView()
        indicator.color = Colors.grayscale400.color
        indicator.hidesWhenStopped = true
        indicator.isHidden = true

        return indicator
    }()

    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [messageLabel, actionButton])
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = Spacing.base02
        stackView.isHidden = true

        return stackView
    }()

    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.backgroundColor, .backgroundPrimary())
            .with(\.textColor, .grayscale400())
            .with(\.textAlignment, .center)
        label.numberOfLines = 1
        label.clipsToBounds = true
        label.isHidden = true

        return label
    }()

    private lazy var actionButton: UIButton = {
        let button = UIButton(type: .custom)
        button
            .buttonStyle(SecondaryButtonStyle(size: .small))
            .with(\.backgroundColor, (color: .backgroundPrimary(), state: .normal))
            .with(\.borderColor, (color: .branding400(), state: .normal))
            .with(\.textColor, (color: .branding400(), state: .normal))
        button.addTarget(self, action: #selector(didTapAtActionButton), for: .touchUpInside)

        button.isHidden = true

        return button
    }()

    private var warningContainerTopConstraint: Constraint?

    private lazy var warningContainerView: UIView = {
        let view = UIView()
        view.isHidden = true

        return view
    }()

    private var buttonAction: (() -> Void)?

    var state: State = .normal {
        didSet {
            stateChanged()
        }
    }

    var sectionHeight: CGFloat = 0.0 {
        didSet {
            warningContainerTopConstraint?.update(offset: sectionHeight)
        }
    }

    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - ViewConfiguration
extension SearchListStateView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(warningContainerView)
        addSubview(stackView)
        addSubview(activityIndicator)
    }

    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }

    func setupConstraints() {
        activityIndicator.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.top.leading.greaterThanOrEqualToSuperview().offset(Spacing.base02).priority(999)
            $0.bottom.trailing.lessThanOrEqualToSuperview().offset(-Spacing.base02).priority(999)
        }

        stackView.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.top.leading.greaterThanOrEqualToSuperview().offset(Spacing.base02).priority(999)
            $0.bottom.trailing.lessThanOrEqualToSuperview().offset(-Spacing.base02).priority(999)
        }

        actionButton.snp.makeConstraints {
            $0.width.equalTo(Layout.ActionButton.width)
        }

        warningContainerView.snp.makeConstraints {
            warningContainerTopConstraint = $0.top.equalToSuperview().offset(sectionHeight).constraint
            $0.leading.trailing.equalToSuperview()
            $0.bottom.lessThanOrEqualToSuperview()
        }
    }
}

// MARK: - Private
private extension SearchListStateView {
    func createActivityIndicatorView() -> UIActivityIndicatorView {
        if #available(iOS 13.0, *) {
            return UIActivityIndicatorView(style: .large)
        }

        return UIActivityIndicatorView(style: .whiteLarge)
    }

    @objc
    func didTapAtActionButton() {
        buttonAction?()
    }

    // MARK: - State handler
    func stateChanged() {
        resetState()

        switch state {
        case let .message(value):
            showMessage(value)
        case .loading:
            activityIndicator.startAnimating()
        case let .error(message, buttonText, action):
            showError(message, buttonText: buttonText, action: action)
        case let .warning(view):
            showWarning(view)
        default:
            break
        }
    }

    func resetState() {
        activityIndicator.stopAnimating()

        stackView.isHidden = true
        messageLabel.text = nil
        messageLabel.isHidden = true
        actionButton.setTitle(nil, for: .normal)
        actionButton.isHidden = true
        buttonAction = nil

        warningContainerView.isHidden = true
        warningContainerView.subviews.forEach { $0.removeFromSuperview() }
    }

    func showMessage(_ message: String) {
        messageLabel.text = message
        messageLabel.isHidden = false
        stackView.isHidden = false
    }

    func showError(_ message: String, buttonText: String?, action: (() -> Void)?) {
        messageLabel.text = message
        messageLabel.isHidden = false

        if let buttonText = buttonText {
            actionButton.setTitle(buttonText, for: .normal)
            actionButton.isHidden = false

            buttonAction = action
        }

        stackView.isHidden = false
    }

    func showWarning(_ view: UIView) {
        warningContainerView.addSubview(view)
        view.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.bottom.lessThanOrEqualToSuperview()
        }

        warningContainerView.isHidden = false
    }
}
