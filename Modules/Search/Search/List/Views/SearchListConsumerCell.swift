import AssetsKit
import Foundation
import SnapKit
import UI
import UIKit

// MARK: - Layout
private extension SearchListConsumerCell.Layout {
    enum ProBadge {
        static let size = CGSize(width: 28.0, height: 14.0)
    }

    enum VerifiedBadge {
        static let size = CGSize(width: 17.0, height: 17.0)
        static let margin: CGFloat = 2.0
    }
}

final class SearchListConsumerCell: UICollectionViewCell {
    fileprivate enum Layout { }

    // MARK: - Properties
    private lazy var containerStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [avatarContainerView, labelsContainerStackView])
        stackView.alignment = .center
        stackView.axis = .horizontal
        stackView.distribution = .fillProportionally
        stackView.spacing = Spacing.base02

        return stackView
    }()

    private lazy var avatarContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.backgroundPrimary.color

        return view
    }()

    private lazy var avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView
            .imageStyle(AvatarImageStyle(size: .medium))
            .with(\.backgroundColor, .backgroundPrimary())

        return imageView
    }()

    private lazy var proBadgeImageView: UIImageView = {
        let imageView = UIImageView(image: Resources.Icons.icoProBadge.image)
        imageView.isHidden = true

        return imageView
    }()

    private lazy var verifiedBadgeImageView: UIImageView = {
        let imageView = UIImageView(image: Resources.Icons.icoVerifiedBadge.image)
        imageView.isHidden = true

        return imageView
    }()

    private lazy var labelsContainerStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [usernameLabel, nameLabel])
        stackView.alignment = .leading
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.spacing = Spacing.base00

        return stackView
    }()

    private lazy var usernameLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(CaptionLabelStyle(type: .highlight))
            .with(\.backgroundColor, .backgroundPrimary())
            .with(\.textColor, .black())
        label.numberOfLines = 1
        label.clipsToBounds = true

        return label
    }()

    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(CaptionLabelStyle())
            .with(\.backgroundColor, .backgroundPrimary())
            .with(\.textColor, .grayscale600())
        label.numberOfLines = 1
        label.clipsToBounds = true

        return label
    }()

    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        avatarImageView.image = nil
        proBadgeImageView.isHidden = true
        verifiedBadgeImageView.isHidden = true
        usernameLabel.text = nil
        nameLabel.text = nil
    }
}

// MARK: - ViewConfiguration
extension SearchListConsumerCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(containerStackView)

        avatarContainerView.addSubview(avatarImageView)
        avatarContainerView.addSubview(proBadgeImageView)
        avatarContainerView.addSubview(verifiedBadgeImageView)
    }

    func configureViews() {
        contentView.backgroundColor = Colors.backgroundPrimary.color
    }

    func setupConstraints() {
        containerStackView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        avatarImageView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        proBadgeImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.ProBadge.size)
            $0.centerX.equalTo(avatarImageView.snp.centerX)
            $0.centerY.equalTo(avatarImageView.snp.bottom)
        }

        verifiedBadgeImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.VerifiedBadge.size)
            $0.trailing.equalTo(avatarContainerView.snp.trailing).offset(Layout.VerifiedBadge.margin)
            $0.bottom.equalTo(avatarContainerView.snp.bottom).offset(Layout.VerifiedBadge.margin)
        }
    }
}

// MARK: - Setup
extension SearchListConsumerCell {
    func setup(consumer: SearchResult.ConsumerData) {
        avatarImageView.setImage(
            url: consumer.smallImageUrl,
            placeholder: Resources.Placeholders.greenAvatarPlaceholder.image
        )

        proBadgeImageView.isHidden = consumer.isVerified || !consumer.isPro
        verifiedBadgeImageView.isHidden = !consumer.isVerified

        usernameLabel.text = consumer.username
        nameLabel.text = consumer.name
    }
}
