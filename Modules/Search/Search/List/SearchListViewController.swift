import Core
import Foundation
import PermissionsKit
import SnapKit
import UI
import UIKit

private extension SearchListViewController.Layout {
    enum Row {
        static let height: CGFloat = 72.0
    }

    enum SearchBar {
        static let height: CGFloat = 40.0
    }

    enum Section {
        static let height: CGFloat = 42.0
    }
}

final class SearchListViewController: ViewController<SearchListInteracting, UIView>, SearchListController {
    fileprivate enum Layout { }

    private enum Section {
        case main
    }

    // MARK: - Properties
    private var sectionTitle: String?

    private lazy var sectionHeaderViewIdentifier = String(describing: SearchListHeaderView.self)
    private lazy var consumerCellIdentifier = String(describing: SearchListConsumerCell.self)
    private lazy var searchDataSource = CollectionViewDataSource<Section, SearchResult>(view: collectionView)

    private lazy var searchBarContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale050.color

        return view
    }()

    private lazy var searchBar: SearchComponentTextField = {
        let searchBar = SearchComponentTextFieldFactory.make()
        searchBar.attributedPlaceholder = NSAttributedString(
            string: Strings.searchConsumerPlaceholder,
            attributes: [.foregroundColor: Colors.grayscale300.color]
        )
        searchBar.backgroundColor = Colors.backgroundPrimary.color
        searchBar.addTarget(self, action: #selector(searchEditingDidBegin), for: .editingDidBegin)
        searchBar.addTarget(self, action: #selector(searchEditingChanged), for: .editingChanged)

        return searchBar
    }()

    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical

        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = Colors.backgroundPrimary.color
        collectionView.delegate = self

        collectionView.register(SearchListConsumerCell.self, forCellWithReuseIdentifier: consumerCellIdentifier)
        collectionView.register(
            SearchListHeaderView.self,
            forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
            withReuseIdentifier: sectionHeaderViewIdentifier
        )

        return collectionView
    }()

    private lazy var searchListStateView: SearchListStateView = {
        let stateView = SearchListStateView()
        stateView.sectionHeight = Layout.Section.height

        return stateView
    }()

    var didSelect: ((SearchResult) -> Void)?

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        updateNavigationBarAppearance()
        configureKeyboardHideWhenTappingAround()

        collectionView.dataSource = searchDataSource
        searchDataSource.add(section: .main)
        searchDataSource.itemProvider = { [weak self] collection, indexPath, item in
            let cell = collection.dequeueReusableCell(
                withReuseIdentifier: self?.reuseIdentifierForCell(at: indexPath, value: item) ?? "",
                for: indexPath
            )
            self?.configureCell(index: indexPath.row, value: item, cell: cell)

            return cell
        }

        searchDataSource.supplementaryViewProvider = { [weak self] collectionView, kind, indexPath in
            self?.supplementaryView(collection: collectionView, kind, indexPath)
        }

        interactor.loadData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    // MARK: - Setup
    override func buildViewHierarchy() {
        view.addSubview(searchBarContainerView)
        view.addSubview(collectionView)

        searchBarContainerView.addSubview(searchBar)
        collectionView.backgroundView = searchListStateView
    }

    override func setupConstraints() {
        searchBarContainerView.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top)
            $0.leading.trailing.equalToSuperview()
        }

        searchBar.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.trailing.bottom.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Layout.SearchBar.height)
        }

        collectionView.snp.makeConstraints {
            $0.top.equalTo(searchBarContainerView.snp.bottom)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(view.compatibleSafeArea.bottom)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.grayscale050.color
    }
}

// MARK: - SearchListDisplay
extension SearchListViewController: SearchListDisplay {
    func showLoading() {
        sectionTitle = nil
        searchDataSource.update(items: [], from: .main)
        searchListStateView.state = .loading
    }

    func hideLoading() {
        searchListStateView.state = .normal
    }

    func show(results: [SearchResult], sectionTitle: String) {
        searchListStateView.state = .normal
        self.sectionTitle = sectionTitle
        searchDataSource.update(items: results, from: .main)
    }

    func showMore(results: [SearchResult]) {
        searchDataSource.add(items: results, to: .main)
    }

    func showContactsPermissionWarning() {
        let setup: PermissionRequestViewSetup = .listItem(
            requestType: .contacts(usesOldImage: true),
            authorizeHandler: { [weak self] in
                self?.requestContactsPermission()
            }
        )

        searchListStateView.state = .warning(view: PermissionRequestView(setup: setup))
        sectionTitle = Strings.contacts.uppercased()
        searchDataSource.update(items: [], from: .main)
    }

    func showNoResultMessage(text: String) {
        sectionTitle = nil
        searchDataSource.update(items: [], from: .main)
        searchListStateView.state = .message(value: text)
    }

    func show(error message: String, buttonTitle: String?) {
        sectionTitle = nil
        searchDataSource.update(items: [], from: .main)
        searchListStateView.state = .error(message: message, buttonText: buttonTitle, buttonAction: { [weak self] in
            self?.interactor.search(term: self?.searchBar.text ?? "", delaying: false)
        })
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension SearchListViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        CGSize(width: collectionView.bounds.width, height: Layout.Row.height)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let item = searchDataSource.item(at: indexPath) else { return }
        interactor.didSelect(result: item, at: indexPath)
    }

    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        referenceSizeForHeaderInSection section: Int
    ) -> CGSize {
        guard sectionTitle != nil else {
            return .zero
        }

        return CGSize(width: collectionView.bounds.width, height: Layout.Section.height)
    }
}

// MARK: - Private
private extension SearchListViewController {
    func configureKeyboardHideWhenTappingAround() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
    }

    @objc
    func dismissKeyboard() {
        view.endEditing(true)
    }

    func updateNavigationBarAppearance() {
        navigationController?.navigationBar.barTintColor = Colors.grayscale050.color
    }

    func reuseIdentifierForCell(at index: IndexPath, value: SearchResult) -> String {
        switch value {
        case .contact,
             .consumer:
            return consumerCellIdentifier
        default:
            return ""
        }
    }

    func configureCell(index: Int, value: SearchResult, cell: UICollectionViewCell) {
        switch value {
        case let .contact(data),
             let .consumer(data):
            guard let cell = cell as? SearchListConsumerCell else { return }

            cell.setup(consumer: data)
        default:
            break
        }
    }

    func supplementaryView(collection: UICollectionView, _ kind: String, _ indexPath: IndexPath) -> UICollectionReusableView? {
        guard let view = collection
            .dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: sectionHeaderViewIdentifier, for: indexPath)
            as? SearchListHeaderView else {
            return nil
        }

        view.setup(title: (sectionTitle ?? "").uppercased())
        return view
    }

    func requestContactsPermission() {
        interactor.requestContactsPermission()
    }

    // MARK: - Actions
    @objc
    func searchEditingDidBegin() {
        interactor.didStartTypeToSearch()
    }

    @objc
    func searchEditingChanged() {
        interactor.search(term: searchBar.text ?? "", delaying: true)
    }
}
