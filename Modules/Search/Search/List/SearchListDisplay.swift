protocol SearchListDisplay: AnyObject {
    func showLoading()
    func hideLoading()

    func show(results: [SearchResult], sectionTitle: String)
    func showMore(results: [SearchResult])

    func showContactsPermissionWarning()
    func showNoResultMessage(text: String)
    func show(error message: String, buttonTitle: String?)
}
