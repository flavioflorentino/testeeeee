import UIKit

protocol SearchListControllerFactory {
    typealias Controller = UIViewController & SearchListController

    func makeSearchListController(
        searchType: SearchType,
        shouldShowContacts: Bool,
        analyticsData: SearchAnalyticsData?,
        didSelectAction: ((SearchSelection) -> Void)?
    ) -> Controller
}
