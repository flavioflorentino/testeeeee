import Core

final class SearchListPresenter {
    weak var display: SearchListDisplay?

    var didSelect: ((SearchSelection) -> Void)?
}

// MARK: - SearchListPresenting
extension SearchListPresenter: SearchListPresenting {
    func showLoading() {
        display?.showLoading()
    }

    func hideLoading() {
        display?.hideLoading()
    }

    func show(results: [SearchResult], type: SearchType) {
        guard results.isNotEmpty else {
            return handleEmptySearch(for: type)
        }

        display?.show(results: results, sectionTitle: sectionTitle(from: type))
    }

    func showMore(results: [SearchResult]) {
        display?.showMore(results: results)
    }

    func showContactsPermissionWarning() {
        display?.showContactsPermissionWarning()
    }

    func show(error: ApiError, type: SearchType) {
        display?.show(error: Strings.errorWhenSearching, buttonTitle: Strings.tryAgain)
    }

    func didSelect(selection: SearchSelection) {
        didSelect?(selection)
    }
}

private extension SearchListPresenter {
    func sectionTitle(from type: SearchType) -> String {
        switch type {
        case .contacts:
            return Strings.contacts.uppercased()
        default:
            return Strings.results.uppercased()
        }
    }

    func handleEmptySearch(for type: SearchType) {
        guard type != .contacts else {
            display?.hideLoading()
            return
        }

        display?.showNoResultMessage(text: Strings.noResults)
    }
}
