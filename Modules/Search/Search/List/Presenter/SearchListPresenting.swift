import Core

public protocol SearchListPresenting {
    func showLoading()
    func hideLoading()

    func show(results: [SearchResult], type: SearchType)
    func showMore(results: [SearchResult])

    func showContactsPermissionWarning()
    func show(error: ApiError, type: SearchType)

    func didSelect(selection: SearchSelection)
}
