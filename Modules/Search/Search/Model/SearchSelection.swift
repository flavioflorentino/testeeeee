public enum SearchSelection {
    case single(_ value: SearchResult)
    case multiple(_ values: [SearchResult])
}
