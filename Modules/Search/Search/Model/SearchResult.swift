import Foundation

public enum SearchResult {
    case contact(data: ConsumerData)
    case consumer(data: ConsumerData)
    case unknown(type: String)
}

public extension SearchResult {
    var type: `Type` {
        switch self {
        case .contact:
            return .contact
        case .consumer:
            return .consumer
        default:
            return .unknown
        }
    }
}

// MARK: - Nested
public extension SearchResult {
    enum `Type`: String {
        case contact
        case consumer
        case unknown
    }

    struct ConsumerData {
        public let id: String
        public let username: String
        public let name: String
        public let smallImageUrl: URL?
        public let largeImageUrl: URL?
        public let isVerified: Bool
        public let isPro: Bool
    }
}

// MARK: - Decodable
extension SearchResult: Decodable {
    private enum CodingKeys: CodingKey {
        case type
        case data
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let typeString = try container.decode(String.self, forKey: .type)

        guard let type = `Type`(rawValue: typeString) else {
            self = .unknown(type: typeString)
            return
        }

        switch type {
        case .contact:
            let nestedContainer = try container.nestedContainer(keyedBy: ConsumerData.CodingKeys.self, forKey: .data)
            self = .contact(data: try ConsumerData(from: nestedContainer))
        case .consumer:
            let nestedContainer = try container.nestedContainer(keyedBy: ConsumerData.CodingKeys.self, forKey: .data)
            self = .consumer(data: try ConsumerData(from: nestedContainer))
        default:
            self = .unknown(type: typeString)
        }
    }
}

extension SearchResult.ConsumerData: Decodable {
    fileprivate enum CodingKeys: String, CodingKey {
        case id
        case username
        case name
        case imageUrlLarge
        case imageUrlSmall
        case verified
        case pro
    }

    fileprivate init(from container: KeyedDecodingContainer<CodingKeys>) throws {
        let id = try container.decode(String.self, forKey: .id)
        let name = try container.decode(String.self, forKey: .name)
        let username = try container.decode(String.self, forKey: .username)

        let smallImageUrl = try? container.decodeIfPresent(URL.self, forKey: .imageUrlSmall)
        let largeImageUrl = try? container.decodeIfPresent(URL.self, forKey: .imageUrlLarge)

        let isVerified = try container.decodeIfPresent(Bool.self, forKey: .verified) ?? false
        let isPro = try container.decodeIfPresent(Bool.self, forKey: .pro) ?? false

        self.init(
            id: id,
            username: username,
            name: name,
            smallImageUrl: smallImageUrl,
            largeImageUrl: largeImageUrl,
            isVerified: isVerified,
            isPro: isPro
        )
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        try self.init(from: container)
    }
}
