import Foundation
import SearchCore
import SearchKit
import Core

protocol P2PSearchInteracting: AnyObject {
    func viewWillAppear()
    func search(term: String)
    func tryAgain()
    func cancel()
    func selectResult(indexPath: IndexPath)
}

final class P2PSearchInteractor: P2PSearchInteracting {
    typealias Dependencies = SearchDependencies
    private let dependencies: Dependencies

    private let service: P2PSearchServicing
    private let presenter: P2PSearchPresenting
    private let builder: P2PSearchViewModelBuildable

    private var sections: [SearchSection] = []
    private let minimumNumberOfCharactersToSearch = 3
    private var currentTerm = String()

    init(service: P2PSearchServicing,
         presenter: P2PSearchPresenting,
         builder: P2PSearchViewModelBuildable,
         dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.builder = builder
    }

    func viewWillAppear() {
        presenter.presentTitle()
        presenter.presentSearchBarPlaceholder()
    }

    func search(term: String) {
        guard shouldPerformSearch(term: term) else { return }
        currentTerm = term
        presenter.presentState(.loading)
        service.search(term: currentTerm) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let sections):
                sections.isEmpty ? self.showEmptyState() : self.showResults(sections)
            case .failure(let error):
                self.showError(error)
            }
        }
    }

    func cancel() { }

    func tryAgain() {
        search(term: currentTerm)
    }

    func selectResult(indexPath: IndexPath) { }
}

private extension P2PSearchInteractor {
    func showEmptyState() {
        let emptyState: SearchErrorView.Style = .emptyState(viewModel: self.createEmptyStateViewModel())
        self.presenter.presentState(.error(style: emptyState))
    }

    func showResults(_ sections: [SearchSection]) {
        self.sections = sections
        let sectionViewModels = self.builder.build(from: sections)
        self.presenter.presentState(.results(sectionViewModels))
    }

    func showError(_ error: ApiError) {
        switch error {
        case .connectionFailure:
            self.presenter.presentState(.error(style: .noConnection))
        default:
            self.presenter.presentState(.error(style: .generalError))
        }
    }

    func shouldPerformSearch(term: String) -> Bool {
        term.count >= minimumNumberOfCharactersToSearch
    }

    func createEmptyStateViewModel() -> SearchErrorView.EmptyStateViewModel {
        .init(title: Strings.Results.EmptyState.title,
              description: Strings.Results.EmptyState.description)
    }
}
