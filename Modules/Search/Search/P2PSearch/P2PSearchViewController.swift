import UI
import UIKit
import SearchKit

enum P2PSearchDisplayState {
    case loading
    case results([SearchResultSectionViewModel])
    case error(style: SearchErrorView.Style)
}

protocol P2PSearchDisplaying: AnyObject {
    func displayState(_ state: P2PSearchDisplayState)
    func displayTitle(_ title: String?)
    func displaySearchBarPlaceholder(_ placeholder: String?)
}

final class P2PSearchViewController: ViewController<P2PSearchInteracting, UIView> {
    fileprivate enum Layout { }

    private lazy var searchBarContainer = SearchBarContainer()
    private lazy var tableView = SearchResultTableView()
    private lazy var loadingView = SearchResultLoadingView()
    private lazy var errorView = SearchErrorView()
    private lazy var backButton: UIBarButtonItem = {
        let backButton = UIBarButtonItem()
        backButton.title = String()
        backButton.tintColor = Colors.branding600.color
        return backButton
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        buildLayout()
        bindEvents()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        interactor.viewWillAppear()
    }

    override func buildViewHierarchy() {
        view.addSubviews(searchBarContainer, tableView, loadingView, errorView)
    }
    
    override func setupConstraints() {
        searchBarContainer.snp.makeConstraints {
            $0.top.equalTo(view.snp.topMargin)
            $0.leading.trailing.equalToSuperview()
        }

        tableView.snp.makeConstraints {
            $0.top.equalTo(searchBarContainer.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }

        loadingView.snp.makeConstraints {
            $0.top.equalTo(searchBarContainer.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }

        errorView.snp.makeConstraints {
            $0.top.equalTo(searchBarContainer.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }

    override func configureViews() {
        tableView.delegate = self
        searchBarContainer.delegate = self

        view.backgroundColor = Colors.backgroundPrimary.color

        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
}

private extension P2PSearchViewController {
    func bindEvents() {
        errorView.tryAgainAction = { [weak self] in
            self?.interactor.tryAgain()
        }
    }

    func resetScreenState() {
        [loadingView, errorView, tableView].forEach { $0.isHidden = true }
        tableView.display(sections: [])
    }
}

private extension P2PSearchViewController.Layout {
    // swiftlint:disable nesting
    enum Animation {
        static var separatorDisplayOffsetThreshold: CGFloat {
            typealias Section = SearchResultSectionView.Layout
            return (Section.topVerticalPadding + Section.labelTopSpacing) * 0.93
        }
    }
}

extension P2PSearchViewController: P2PSearchDisplaying {
    func displayState(_ state: P2PSearchDisplayState) {
        resetScreenState()
        switch state {
        case .results(let results):
            tableView.isHidden = false
            tableView.display(sections: results)
        case .loading:
            loadingView.isHidden = false
        case .error(let style):
            errorView.isHidden = false
            errorView.style = style
        }
    }

    func displayTitle(_ title: String?) {
        self.title = title
    }

    func displaySearchBarPlaceholder(_ placeholder: String?) {
        self.searchBarContainer.placeholderText = placeholder
    }
}

extension P2PSearchViewController: ApolloSearchBarDelegate {
    public func didChangeText(_ newText: String?) {
        interactor.search(term: newText ?? "")
    }

    public func didCancel() {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        interactor.cancel()
    }

    public func didBeginEditing() {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
}

extension P2PSearchViewController: SearchResultTableViewDelegate {
    public func didSelectRowAt(indexPath: IndexPath) {
        interactor.selectResult(indexPath: indexPath)
    }

    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let searchBar = searchBarContainer
        let currentOffset = scrollView.contentOffset.y
        let offsetThreshold = Layout.Animation.separatorDisplayOffsetThreshold
        currentOffset < offsetThreshold ? searchBar.hideSeparator() : searchBar.showSeparator()
    }
}
