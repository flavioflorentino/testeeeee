import UIKit

enum P2PSearchAction {
}

protocol P2PSearchCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: P2PSearchAction)
}

final class P2PSearchCoordinator {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - P2PSearchCoordinating
extension P2PSearchCoordinator: P2PSearchCoordinating {
    func perform(action: P2PSearchAction) {
    }
}
