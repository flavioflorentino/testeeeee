import Foundation
import AssetsKit
import SearchKit
import SearchCore

protocol P2PSearchViewModelBuildable {
    func build(from sections: [SearchSection]) -> [SearchResultSectionViewModel]
}

final class P2PSearchViewModelBuilder: P2PSearchViewModelBuildable {
    func build(from sections: [SearchSection]) -> [SearchResultSectionViewModel] {
        sections.map(SearchResultSectionViewModel.init)
    }
}

private extension SearchResultSectionViewModel {
    init(section: SearchSection) {
        self.init(title: section.sectionTitle,
                  actionTitle: section.actionType?.rawValue,
                  items: .init(row: section.rows))
    }
}

private extension Array where Element == SearchResultSectionViewModel.SearchResultItemViewModel {
    init(row: SectionRow) {
        self.init()
        switch row {
        case let .recents(items), let .list(items):
            let listItem = items.map(SearchResultCellViewModel.init)
            listItem.forEach { self.append(.list(item: $0)) }
        case let .carousel(items):
            let carouselItems = items.map(SearchResultCarouselCellViewModel.init)
            self.append(.carousel(items: carouselItems, shouldRenderActionButton: false))
        case let .secondaryCarousel(items):
            let carouselItems = items.map(SecondaryCarouselCellViewModel.init)
            self.append(.secondaryCarousel(items: carouselItems))
        case let .info(item):
            let helpItem = item.map(SearchInformationalCardCellViewModel.init)
            self.append(.help(item: helpItem))
        }
    }
}

private extension SearchResultCellViewModel {
    init(row: SearchResultListRow) {
        let style = Style(type: row.type, isVerified: row.isVerified, isPro: row.isPro)
        self.init(style: style,
                  profileImageUrl: row.avatarImageUrl,
                  title: row.title,
                  description: row.body,
                  footer: row.footer)
    }
}

private extension SearchResultCellViewModel.Style {
    init(type: SearchableType, isVerified: Bool, isPro: Bool) {
        switch type {
        case .consumer:
            self = .people(isVerified: isVerified, isPro: isPro)
        case .places:
            self = .store
        case .subscription:
            self = .subscription
        case .store:
            self = .store
        }
    }
}

private extension SearchResultCarouselCellViewModel {
    init(row: SearchResultCarouselRow) {
        self.init(profileImageUrl: row.avatarImageUrl, titleText: row.title, descriptionText: row.footer)
    }
}

private extension SecondaryCarouselCellViewModel {
    init(row: SearchSecondaryCarouselRow) {
        self.init(title: row.title, highlight: row.body, icon: row.type.icon)
    }
}

private extension SearchResultServicesType {
    var icon: UIImage {
        switch self {
        case .boleto:
            return Resources.Icons.icoBarcode.image
        case .p2m:
            return Resources.Icons.icoP2m.image
        case .pix:
            return Resources.Icons.icoPixOutline.image
        }
    }
}

private extension SearchInformationalCardCellViewModel {
    init(row: SearchResultInfoRow) {
        self.init(title: row.title)
    }
}
