import AnalyticsModule
import Core
import FeatureFlag
import Foundation

typealias SearchDependencies = HasMainQueue & HasFeatureManager & HasAnalytics & HasNoDependency

protocol HasNoDependency {}

final class DependencyContainer: SearchDependencies {
    lazy var mainQueue = DispatchQueue.main
    lazy var featureManager: FeatureManagerContract = FeatureManager.shared
    lazy var analytics: AnalyticsProtocol = Analytics.shared
}
