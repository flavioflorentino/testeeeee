import Foundation

protocol P2PSearchPresenting: AnyObject {
    var viewController: P2PSearchDisplaying? { get set }
    func presentState(_ state: P2PSearchDisplayState)
    func presentTitle()
    func presentSearchBarPlaceholder()
}

final class P2PSearchPresenter: P2PSearchPresenting {
    private let coordinator: P2PSearchCoordinating
    weak var viewController: P2PSearchDisplaying?

    init(coordinator: P2PSearchCoordinating) {
        self.coordinator = coordinator
    }

    func presentState(_ state: P2PSearchDisplayState) {
        viewController?.displayState(state)
    }

    func presentTitle() {
        viewController?.displayTitle(Strings.P2P.title)
    }

    func presentSearchBarPlaceholder() {
        viewController?.displaySearchBarPlaceholder(Strings.P2P.SearchBar.placeholder)
    }
}
