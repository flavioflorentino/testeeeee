import UIKit

public enum P2PSearchFactory {
    public static func make() -> UIViewController {
        let container = DependencyContainer()
        let service: P2PSearchServicing = P2PSearchService(dependencies: container)
        let coordinator: P2PSearchCoordinating = P2PSearchCoordinator(dependencies: container)
        let presenter: P2PSearchPresenting = P2PSearchPresenter(coordinator: coordinator)
        let builder: P2PSearchViewModelBuildable = P2PSearchViewModelBuilder()
        let interactor = P2PSearchInteractor(service: service,
                                             presenter: presenter,
                                             builder: builder,
                                             dependencies: container)
        let viewController = P2PSearchViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
