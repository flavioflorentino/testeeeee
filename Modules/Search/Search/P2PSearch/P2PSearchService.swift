import Core
import Foundation
import SearchCore

protocol P2PSearchServicing {
    func search(term: String, completion: @escaping ((Result<[SearchSection], ApiError>) -> Void))
}

final class P2PSearchService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    private var pendingRequest: DispatchWorkItem?
    private let debounceTime = 0.5

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension P2PSearchService: P2PSearchServicing {
    func search(term: String, completion: @escaping ((Result<[SearchSection], ApiError>) -> Void)) {
        pendingRequest?.cancel()

        let request = DispatchWorkItem { [weak self] in
            self?.dependencies.mainQueue.asyncAfter(deadline: .now() + 1.0) {
                //TODO Backend integration
                completion(.success(SearchSectionMockFactory.make()))
            }
        }

        pendingRequest = request

        self.dependencies.mainQueue.asyncAfter(deadline: .now() + debounceTime,
                                               execute: request)
    }
}
