import Foundation
import UI
import UIKit

final class SearchCoordinator {
    // MARK: - Properties
    private let navigationController: UINavigationController
    private let title: String?
    private let searchType: SearchType
    private let shouldShowContacts: Bool
    private let controllerFactory: SearchListControllerFactory
    private let analyticsData: SearchAnalyticsData?

    private var firstControllerParentObserver: NSKeyValueObservation?

    private var children: [Coordinating] = []

    var childViewController: [UIViewController] = []
    var viewController: UIViewController?

    var didFinishFlow: ((SearchSelection?) -> Void)?

    // MARK: - Initialization
    init(
        title: String?,
        searchType: SearchType,
        shouldShowContacts: Bool,
        navigationController: UINavigationController,
        analyticsData: SearchAnalyticsData?,
        controllerFactory: SearchListControllerFactory
    ) {
        self.title = title
        self.searchType = searchType
        self.shouldShowContacts = shouldShowContacts
        self.navigationController = navigationController
        self.analyticsData = analyticsData
        self.controllerFactory = controllerFactory
    }
}

// MARK: - SearchCoordinating
extension SearchCoordinator: SearchCoordinating {
    func start() {
        let viewController = controllerFactory.makeSearchListController(
            searchType: searchType,
            shouldShowContacts: shouldShowContacts,
            analyticsData: analyticsData
        ) { [weak self] selection in
            self?.didFinishFlow?(selection)
        }

        viewController.title = title
        viewController.hidesBottomBarWhenPushed = true

        navigationController.pushViewController(viewController, animated: true)

        guard firstControllerParentObserver == nil else { return }

        firstControllerParentObserver = createParentObserver(for: viewController) { [weak self] newParent in
            self?.firstScreenParentDidChange(to: newParent)
        }
    }
}

// MARK: - Private
private extension SearchCoordinator {
    func createParentObserver(
        for controller: UIViewController,
        didChange: @escaping (UIViewController?) -> Void
    ) -> NSKeyValueObservation {
        controller.observe(\.parent, options: .new) { _, values in
            guard let parent = values.newValue else { return }
            didChange(parent)
        }
    }

    func firstScreenParentDidChange(to parent: UIViewController?) {
        guard parent == nil else { return }
        didFinishFlow?(nil)
    }
}
