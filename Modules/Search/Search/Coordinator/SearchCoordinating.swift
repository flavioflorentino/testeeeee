import UI

public protocol SearchCoordinating: Coordinating {
    var didFinishFlow: ((SearchSelection?) -> Void)? { get set }
}
