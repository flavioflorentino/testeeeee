import Foundation
@testable import Search
import SearchCore
import SearchKit
import XCTest

private final class P2PSearchPresenterSpy: P2PSearchPresenting {
    var viewController: P2PSearchDisplaying?

    private(set) var presentStateCallCount = 0
    private(set) var presentTitleCallCount = 0
    private(set) var presentSearchBarPlaceholderCallCount = 0

    private(set) var states = [P2PSearchDisplayState]()

    func presentState(_ state: P2PSearchDisplayState) {
        presentStateCallCount += 1
        states.append(state)
    }

    func presentTitle() {
        presentTitleCallCount += 1
    }

    func presentSearchBarPlaceholder() {
        presentSearchBarPlaceholderCallCount += 1
    }
}

final class P2PSearchInteractorTests: XCTestCase {
    private let presenter = P2PSearchPresenterSpy()
    private let service = P2PSearchServiceMock()
    private let builder = P2PSearchViewModelBuilderMock()

    private lazy var dependencies = DependencyContainerMock()
    private lazy var interactor = P2PSearchInteractor(
        service: service,
        presenter: presenter,
        builder: builder,
        dependencies: dependencies
    )

    private let invalidTerm = "ab"
    private let validTerm = "abc"

    func testViewWillAppear_ShouldPresentTitle() {
        interactor.viewWillAppear()

        XCTAssertEqual(presenter.presentTitleCallCount, 1)
        XCTAssertEqual(presenter.presentSearchBarPlaceholderCallCount, 1)
    }

    func testSearch_WithTermLessThanThreeCharactersLong_ShouldNotPerformSearch() {
        interactor.search(term: invalidTerm)

        XCTAssertEqual(presenter.presentStateCallCount, 0)
        XCTAssertEqual(service.searchCallCount, 0)
    }

    func testSearch_WithValidTermAndServiceError_ShouldPerformSearchAndPresentError() {
        service.searchResult = .failure(.serverError)

        interactor.search(term: validTerm)

        XCTAssertEqual(presenter.presentStateCallCount, 2)
        XCTAssertEqual(service.searchCallCount, 1)
        let expectedStates: [P2PSearchDisplayState] = [.loading, .error(style: .generalError)]
        XCTAssertEqual(presenter.states, expectedStates)
    }

    func testSearch_WithValidTermAndEmptyResults_ShouldPerformSearchAndPresentEmptyState() {
        service.searchResult = .success([])
        builder.results = []

        interactor.search(term: validTerm)

        XCTAssertEqual(presenter.presentStateCallCount, 2)
        XCTAssertEqual(service.searchCallCount, 1)
        let expectedStates: [P2PSearchDisplayState] = [.loading, .error(style: .emptyState())]
        XCTAssertEqual(presenter.states, expectedStates)
    }

    func testSearch_WithValidTermAndResults_ShouldPerformSearchAndPresentResults() {
        service.searchResult = .success(SearchSectionMockFactory.make())
        let results = SearchResultSectionViewModelMockFactory.make()
        builder.results = results

        interactor.search(term: validTerm)

        XCTAssertEqual(presenter.presentStateCallCount, 2)
        XCTAssertEqual(service.searchCallCount, 1)
        let expectedStates: [P2PSearchDisplayState] = [.loading, .results(results)]
        XCTAssertEqual(presenter.states, expectedStates)
    }
}

extension P2PSearchDisplayState: Equatable {
    public static func == (lhs: P2PSearchDisplayState, rhs: P2PSearchDisplayState) -> Bool {
        switch (lhs, rhs) {
        case (.loading, .loading):
            return true
        case (.results, .results):
            return true
        case let (.error(lhsStyle), .error(rhsStyle)):
            return lhsStyle == rhsStyle
        default:
            return false
        }
    }
}

extension SearchErrorView.Style: Equatable {
    public static func == (lhs: SearchErrorView.Style, rhs: SearchErrorView.Style) -> Bool {
        switch (lhs, rhs) {
        case (.noConnection, .noConnection):
            return true
        case (.generalError, .generalError):
            return true
        case (.emptyState, .emptyState):
            return true
        case (.waitingInput, .waitingInput):
            return true
        default:
            return false
        }
    }
}
