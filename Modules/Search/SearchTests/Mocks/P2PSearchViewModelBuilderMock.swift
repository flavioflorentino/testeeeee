import Core
import Foundation
import SearchCore
import SearchKit
@testable import Search

final class P2PSearchViewModelBuilderMock: P2PSearchViewModelBuildable {
    var results: [SearchResultSectionViewModel] = []

    func build(from sections: [SearchSection]) -> [SearchResultSectionViewModel] {
        results
    }
}
