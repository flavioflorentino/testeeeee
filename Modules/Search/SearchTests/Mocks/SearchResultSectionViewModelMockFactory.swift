import Foundation
import SearchKit

final class SearchResultSectionViewModelMockFactory {
    typealias SectionViewModel = SearchResultSectionViewModel
    private typealias ItemViewModel = SearchResultSectionViewModel.SearchResultItemViewModel

    static func make() -> [SectionViewModel] {
        return [listSection(), carouselSection()]
    }

    private static func listSection() -> SectionViewModel {
        SectionViewModel(title: nil, actionTitle: nil, items: listItems())
    }

    private static func listItems() -> [ItemViewModel] {
        let cell1 = SearchResultCellViewModel(style: .store,
                                              profileImageUrl: nil,
                                              title: "title1",
                                              description: "desc1",
                                              footer: nil)
        let cell2 = SearchResultCellViewModel(style: .subscription,
                                              profileImageUrl: nil,
                                              title: "title2",
                                              description: "desc2",
                                              footer: "footer2")

        return [.list(item: cell1), .list(item: cell2)]
    }

    private static func carouselSection() -> SectionViewModel {
        SectionViewModel(title: "Locais", actionTitle: nil, items: listItems())
    }

    private static func carouselItems() -> ItemViewModel {
        let cell1 = SearchResultCarouselCellViewModel(profileImageUrl: nil,
                                                      titleText: "carousel title 1",
                                                      descriptionText: "carousel desc 1")
        let cell2 = SearchResultCarouselCellViewModel(profileImageUrl: nil,
                                                      titleText: "carousel title 2",
                                                      descriptionText: "carousel desc 2")
        return .carousel(items: [cell1, cell2], shouldRenderActionButton: false)
    }
}
