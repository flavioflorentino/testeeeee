import Core
import AnalyticsModule
import FeatureFlag
@testable import Search

final class DependencyContainerMock: SearchDependencies {
    lazy var analytics: AnalyticsProtocol = resolve()
    lazy var featureManager: FeatureManagerContract = resolve()
    lazy var mainQueue: DispatchQueue = resolve()

    private let dependencies: [Any]

    init(_ dependencies: Any...) {
        self.dependencies = dependencies
    }

    func resolve<T>() -> T {
        let resolved = dependencies.compactMap { $0 as? T }

        switch resolved.first {
        case .none:
            fatalError("DependencyContainerMock could not resolve dependency: \(T.self)\n")
        case .some where resolved.count > 1:
            fatalError("DependencyContainerMock resolved mutiple dependencies for: \(T.self)\n")
        case .some(let mock):
            return mock
        }
    }

    func resolve() -> DispatchQueue {
        dependencies.compactMap { $0 as? DispatchQueue }.first ?? DispatchQueue(label: "DependencyContainerMock")
    }
}
