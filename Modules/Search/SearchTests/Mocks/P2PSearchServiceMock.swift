@testable import Search
import SearchCore
import Core
import Foundation

final class P2PSearchServiceMock: P2PSearchServicing {
    private(set) var searchCallCount = 0

    private(set) var userId: String?
    var searchResult: Result<[SearchSection], ApiError> = .failure(.unknown(nil))

    func search(term: String, completion: @escaping ((Result<[SearchSection], ApiError>) -> Void)) {
        searchCallCount += 1
        completion(searchResult)
    }
}
