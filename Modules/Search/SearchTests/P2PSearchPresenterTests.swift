@testable import Search
import UI
import XCTest

private final class P2PSearchCoordinatorSpy: P2PSearchCoordinating {
    var viewController: UIViewController?

    private(set) var performCallsCount = 0
    private(set) var action: P2PSearchAction?

    func perform(action: P2PSearchAction) {
        //TODO Write function body when navigation is integrated
    }
}

private final class P2PSearchViewControllerSpy: P2PSearchDisplaying {
    private(set) var displayStateCallCount = 0
    private(set) var displayTitleCallCount = 0
    private(set) var displaySearchBarPlaceholderCallCount = 0

    private(set) var stateReceived: P2PSearchDisplayState?
    private(set) var titleReceived: String?
    private(set) var placeholderReceived: String?

    func displayState(_ state: P2PSearchDisplayState) {
        displayStateCallCount += 1
        stateReceived = state
    }

    func displayTitle(_ title: String?) {
        displayTitleCallCount += 1
        titleReceived = title
    }

    func displaySearchBarPlaceholder(_ placeholder: String?) {
        displaySearchBarPlaceholderCallCount += 1
        placeholderReceived = placeholder
    }
}

final class P2PSearchPresenterTests: XCTestCase {
    private let coordinatorSpy = P2PSearchCoordinatorSpy()
    private let viewControllerSpy = P2PSearchViewControllerSpy()
    private lazy var presenter: P2PSearchPresenting = {
        let presenter = P2PSearchPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()

    func testPresentState_ShouldDisplayState() {
        let state: P2PSearchDisplayState = .loading

        presenter.presentState(state)

        XCTAssertEqual(viewControllerSpy.displayStateCallCount, 1)
        XCTAssertEqual(viewControllerSpy.stateReceived, state)
    }

    func testPresentTitle_ShouldDisplayTitle() {
        presenter.presentTitle()

        XCTAssertEqual(viewControllerSpy.displayTitleCallCount, 1)
        XCTAssertEqual(viewControllerSpy.titleReceived, Strings.P2P.title)
    }

    func testPresentSearchBarPlaceholder_ShouldDisplaySearchBarPlaceholder() {
        presenter.presentSearchBarPlaceholder()

        XCTAssertEqual(viewControllerSpy.displaySearchBarPlaceholderCallCount, 1)
        XCTAssertEqual(viewControllerSpy.placeholderReceived, Strings.P2P.SearchBar.placeholder)
    }
}
