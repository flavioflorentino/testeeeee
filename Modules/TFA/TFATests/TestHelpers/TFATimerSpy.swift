@testable import TFA
import Foundation

final class TFATimerProtocolSpy: TFATimerProtocol {
    weak var delegate: TFATimerDelegate?
    private(set) var callStartCount = 0
    private(set) var callStopCount = 0

    func start() {
        callStartCount += 1
    }

    func stop() {
        callStopCount += 1
    }
}
