import UIKit

final class NavigationControllerSpy: UINavigationController {
    private(set) var callPushViewControllerCount = 0
    private(set) var callPopViewControllerCount = 0
    private(set) var callPopToRootViewControllerCount = 0
    private(set) var callPresentViewControllerCount = 0
    private(set) var callSetViewControllersCount = 0
    private(set) var callDismissViewControllerCount = 0
    private(set) var pushedViewController: UIViewController?
    private(set) var viewController: UIViewController?
    private(set) var settedViewControllers: [UIViewController]?
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        callPushViewControllerCount += 1
        pushedViewController = viewController
    }
    
    override func popViewController(animated: Bool) -> UIViewController? {
        callPopViewControllerCount += 1
        return nil
    }
    
    override func popToRootViewController(animated: Bool) -> [UIViewController]? {
        callPopToRootViewControllerCount += 1
        return nil
    }
    
    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        callPresentViewControllerCount += 1
        viewController = viewControllerToPresent
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        callDismissViewControllerCount += 1
    }
    
    override func setViewControllers(_ viewControllers: [UIViewController], animated: Bool) {
        callSetViewControllersCount += 1
        settedViewControllers = viewControllers
    }
}
