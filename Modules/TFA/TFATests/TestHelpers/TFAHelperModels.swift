import Core
import Foundation

struct TFAHelperModels {
    func attempLimitErrorJson() -> RequestError {
        let data: [String: Any] = [
            "short_message" : "Limite atingido",
            "data" : [
                "retry_time" : 10
            ],
            "code" : "form_limit",
            "message" : "Você excedeu o limite de tentativas. Aguarde 02:52 minutos para tentar novamente."
        ]
        let jsonData = try? JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
        return RequestError.initialize(with: jsonData)
    }
    
    func invalidDataErrorJson() -> RequestError {
        let data: [String: Any] = [
            "short_message" : "Ops!",
            "message" : "Dados inválidos",
            "code" : "4401"
        ]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
        return RequestError.initialize(with: jsonData)
    }
}
