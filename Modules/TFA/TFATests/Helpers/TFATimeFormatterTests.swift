import Foundation
@testable import TFA
import XCTest

final class TFATimeFormatterTests: XCTestCase {
    private lazy var sut = TFATimeFormatter()

    func testFormatString_OneSecond_ShouldFormatString() {
        let timeString = sut.formatString(from: 1)
        let expectedString = "00:01"

        XCTAssertEqual(timeString, expectedString)
    }

    func testFormatString_TwelveSeconds_ShouldFormatString() {
        let timeString = sut.formatString(from: 12)
        let expectedString = "00:12"

        XCTAssertEqual(timeString, expectedString)
    }

    func testFormatString_OneMinuteAndFive_ShouldFormatString() {
        let timeString = sut.formatString(from: 65)
        let expectedString = "01:05"

        XCTAssertEqual(timeString, expectedString)
    }

    func testFormatString_OneMinuteFifteen_ShouldFormatString() {
        let timeString = sut.formatString(from: 75)
        let expectedString = "01:15"

        XCTAssertEqual(timeString, expectedString)
    }

    func testFormatString_TwoMinutesAndTwentySeven_ShouldFormatString() {
        let timeString = sut.formatString(from: 147)
        let expectedString = "02:27"

        XCTAssertEqual(timeString, expectedString)
    }

    func testFormatString_FiveMinutes_ShouldFormatString() {
        let timeString = sut.formatString(from: 300)
        let expectedString = "05:00"

        XCTAssertEqual(timeString, expectedString)
    }

}
