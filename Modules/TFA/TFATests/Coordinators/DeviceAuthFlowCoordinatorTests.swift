import IdentityValidation
@testable import TFA
import XCTest

final class DeviceAuthFlowCoordinatorTests: XCTestCase {
    private let navigationSpy = NavigationControllerSpy()
    private let presenterController = UIViewController()
    
    private var finishedWithSuccess: Bool?
    
    private lazy var sut = DeviceAuthFlowCoordinator(
        tfaId: "123456",
        presenterController: presenterController,
        navigationController: navigationSpy,
        onFinish: finishBlock(_:)
    )

    func testStart_ShouldPresentOnboarding() {
        sut.start()
        
        XCTAssertEqual(navigationSpy.callSetViewControllersCount, 1)
        XCTAssertTrue(navigationSpy.settedViewControllers?.first is DeviceAuthOnboardingViewController)
    }
    
    func testOnboardingClose_WhenValidationIsNotComplete_ShouldDismissTFAFlowWithoutSuccess() throws {
        sut.onboardingClose()
        
        XCTAssertEqual(navigationSpy.callDismissViewControllerCount, 1)
        let validationResult = try XCTUnwrap(finishedWithSuccess)
        XCTAssertFalse(validationResult)
    }
    
    func testOnboardingShowHelp_ShouldPresentInfoViewController() {
        sut.onboardingShowHelp()
        
        XCTAssertEqual(navigationSpy.callPushViewControllerCount, 1)
        XCTAssertTrue(navigationSpy.pushedViewController is DeviceAuthInfoViewController)
    }
    
    func testInfoDidGoBack_ShouldPopViewController() {
        sut.infoDidGoBack()
        
        XCTAssertEqual(navigationSpy.callPopViewControllerCount, 1)
    }
    
    func testOnboardingNextStep_ShouldPresentDeviceAuthOptionsViewController() {
        sut.onboardingNextStep()
        
        XCTAssertEqual(navigationSpy.callPushViewControllerCount, 1)
        XCTAssertTrue(navigationSpy.pushedViewController is DeviceAuthOptionsViewController)
    }
    
    func testOptionsDidGoBack_ShouldPopViewCOntroller() {
        sut.optionsDidGoBack()
        
        XCTAssertEqual(navigationSpy.callPopViewControllerCount, 1)
    }
    
    func testOptionsDidCloseFlow_ShouldDismissTFAFlowWithoutSuccess() throws {
        sut.optionsDidCloseFlow()
        
        XCTAssertEqual(navigationSpy.callDismissViewControllerCount, 1)
        let validationResult = try XCTUnwrap(finishedWithSuccess)
        XCTAssertFalse(validationResult)
    }
    
    func testDidSelectValidationId_ShouldPresentIdentityValidationCoordinator() {
        let selfieData = DeviceAuthSelfieFlowResponse.Selfie(token: "abc", flow: "TFA")
        let selfieValidationData = DeviceAuthSelfieFlowResponse(selfie: selfieData)
        let authOption = DeviceAuthOption(flow: .validationID, firstStep: .selfie, iconUrl: "", title: "Biometria")
        
        sut.didSelectValidationId(validationData: selfieValidationData, option: authOption)
        
        XCTAssertNotNil(sut.identityValidationCoordinator)
        XCTAssertEqual(navigationSpy.callPresentViewControllerCount, 1)
    }
    
    func testDidSelectOption_WhenFirstStepIsUserData_ShouldPresentPersonalDataValidationViewController() {
        sut.didSelectOption(option: DeviceAuthOption(flow: .sms, firstStep: .userData, iconUrl: "", title: ""))
        
        XCTAssertEqual(navigationSpy.callPushViewControllerCount, 1)
        XCTAssertTrue(navigationSpy.pushedViewController is PersonalDataValidationViewController)
    }
    
    func testValidationDidGoBack_ShouldPopViewController() {
        sut.validationDidGoBack()

        XCTAssertEqual(navigationSpy.callPopToRootViewControllerCount, 1)
    }
    
    func testReceivedNextStep_WhenNextStepIsBankData_ShouldPushBankAccountViewController() {
        sut.receivedNextStep(forFlow: .sms, step: .bankData)
        
        XCTAssertEqual(navigationSpy.callPushViewControllerCount, 1)
        XCTAssertTrue(navigationSpy.pushedViewController is BankAccountValidationViewController)
    }
    
    func testReceivedNextStep_WhenNextStepIsPhoneNumber_ShouldPushPhoneNumberViewController() {
        sut.receivedNextStep(forFlow: .sms, step: .phoneNumber)
        
        XCTAssertEqual(navigationSpy.callPushViewControllerCount, 1)
        XCTAssertTrue(navigationSpy.pushedViewController is PhoneNumberValidationViewController)
    }
    
    func testReceivedNextStep_WhenNextStepIsUserData_ShouldPushPersonalDataViewController() {
        sut.receivedNextStep(forFlow: .sms, step: .userData)
        
        XCTAssertEqual(navigationSpy.callPushViewControllerCount, 1)
        XCTAssertTrue(navigationSpy.pushedViewController is PersonalDataValidationViewController)
    }
}

private extension DeviceAuthFlowCoordinatorTests {
    func finishBlock(_ completed: Bool) {
        self.finishedWithSuccess = completed
    }
}
