import UI
@testable import TFA
import XCTest

private final class CodeValidationCoordinatorSpy: CodeValidationCoordinating {
    var delegate: DeviceAuthFlowStepping?
    var viewController: UIViewController?
    
    private(set) var performActionCallCount = 0
    private(set) var actionPerformed: CodeValidationAction?
    
    func perform(action: CodeValidationAction) {
        performActionCallCount += 1
        actionPerformed = action
    }
}

private final class CodeValidationViewControllerSpy: CodeValidationDisplaying {
    private(set) var displayLoadingCallCount = 0
    private(set) var loadingState = false
    private(set) var displayRetryButtonEnabledCallCount = 0
    private(set) var isButtonEnabled = false
    private(set) var displayAttributedDescriptionCallCount = 0
    private(set) var attributedStringDisplayed: NSAttributedString?
    private(set) var displayDescriptionCallCount = 0
    private(set) var descriptionStringDisplayed: String?
    
    private(set) var displayAlertCallCount = 0
    private(set) var displayCancelAuthorizationAlertCallCount = 0
    private(set) var displayAttempLimitErrorCallCount = 0
    private(set) var displayedErrorTitle = ""
    private(set) var displayedErrorMessage = ""
    private(set) var retryTimeDisplayed = 0
    
    private(set) var displayRetryButtonDisabledCallCount = 0
    private(set) var retryButtonDisabledAttributedText: NSAttributedString?
    
    func displayLoading(_ isLoading: Bool) {
        displayLoadingCallCount += 1
        loadingState = isLoading
    }
    
    func displayCancelAuthorizationAlert() {
        displayCancelAuthorizationAlertCallCount += 1
    }
    
    func displayAttempLimitError(with title: String, message: String, retryTime: Int) {
        displayAttempLimitErrorCallCount += 1
        displayedErrorTitle = title
        displayedErrorMessage = message
        retryTimeDisplayed = retryTime
    }
    
    func displayAlert(title: String, message: String) {
        displayAlertCallCount += 1
        displayedErrorTitle = title
        displayedErrorMessage = message
    }
    
    func displayAttributedDescription(_ attributedText: NSAttributedString) {
        displayAttributedDescriptionCallCount += 1
        attributedStringDisplayed = attributedText
    }
    
    func displayDescription(_ descriptionText: String) {
        displayDescriptionCallCount += 1
        descriptionStringDisplayed = descriptionText
    }
    
    func displayRetryButtonEnabled() {
        displayRetryButtonEnabledCallCount += 1
    }
    
    func displayRetryButtonDisabled(with attributedText: NSAttributedString) {
        displayRetryButtonDisabledCallCount += 1
        retryButtonDisabledAttributedText = attributedText
    }
}

final class CodeValidationPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = CodeValidationCoordinatorSpy()
    private lazy var viewControllerSpy = CodeValidationViewControllerSpy()
    
    private lazy var sut: CodeValidationPresenter = {
        let presenter = CodeValidationPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testDidNextStep_WhenActionIsBack_ShouldPerformActionOnCoordinator() {
        sut.didNextStep(action: .back)
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.actionPerformed, .back)
    }
    
    func testPresentLoading_WhenCalledFromInteractor_ShouldDisplayLoadingOnViewController() {
        sut.presentLoading(true)
        
        XCTAssertEqual(viewControllerSpy.displayLoadingCallCount, 1)
        XCTAssertTrue(viewControllerSpy.loadingState)
    }
    
    func testPresentGoBackAlert_WhenCalledFromInteractor_ShouldDisplayCancelAuthorizationAlert() {
        sut.presentGoBackAlert()
        
        XCTAssertEqual(viewControllerSpy.displayCancelAuthorizationAlertCallCount, 1)
    }
    
    func testPresentRetryButtonEnabled_ShouldDisplayRetryButtonEnableOnViewController() {
        sut.presentRetryButtonEnabled()
        
        XCTAssertEqual(viewControllerSpy.displayRetryButtonEnabledCallCount, 1)
    }
    
    func testPresentRetryButtonUpdatedWithRemainingTIme_ShouldDisplayRetryButtonDisabledWithAttributedText() {
        let formattedTime = DropLeadingTFATimeFormatter().formatString(from: TimeInterval(10))
        let buttonText = Strings.Code.didNotReceived(formattedTime)
        
        let expectedAttributedText = buttonText.attributedString(
            highlights: [formattedTime],
            attrs: [.font: Typography.bodyPrimary(.highlight).font()]
        )
        
        sut.presentRetryButtonUpdated(with: 10)
        
        XCTAssertEqual(viewControllerSpy.displayRetryButtonDisabledCallCount, 1)
        XCTAssertEqual(viewControllerSpy.retryButtonDisabledAttributedText, expectedAttributedText)
    }
    
    func testPresentInfoForDestination_WhenFlowIsAuthorizedDeviceOrDevice_ShouldNotCallDisplayDescription() {
        sut.presentInfoFor(destination: "****88991", withFlow: .authorizedDevice)
        sut.presentInfoFor(destination: "****88991", withFlow: .device)
        
        XCTAssertEqual(viewControllerSpy.displayDescriptionCallCount, 0)
        XCTAssertEqual(viewControllerSpy.displayAttributedDescriptionCallCount, 0)
    }
    
    func testPresentInfoForDestination_WhenDestinationIsEmptyAndFlowIsEmail_ShouldCallDisplayDescription() {
        sut.presentInfoFor(destination: "", withFlow: .email)
        
        XCTAssertEqual(viewControllerSpy.displayDescriptionCallCount, 1)
        XCTAssertEqual(viewControllerSpy.descriptionStringDisplayed, Strings.Code.emailNotificationSent)
        XCTAssertEqual(viewControllerSpy.displayAttributedDescriptionCallCount, 0)
    }
    
    func testPresentInfoForDestination_WhenDestinationIsEmptyAndFlowIsSMS_ShouldCallDisplayDescription() {
        sut.presentInfoFor(destination: "", withFlow: .sms)
        
        XCTAssertEqual(viewControllerSpy.displayDescriptionCallCount, 1)
        XCTAssertEqual(viewControllerSpy.descriptionStringDisplayed, Strings.Code.smsNotificationSent)
        XCTAssertEqual(viewControllerSpy.displayAttributedDescriptionCallCount, 0)
    }
    
    func testPresentInfoForDestination_WhenDestinationIsNotEmptyAndFlowIsSMS_ShouldCallDisplayAttributedDescription() {
        sut.presentInfoFor(destination: "****88991", withFlow: .sms)
        
        let title = Strings.Code.smsNotificationWithPhoneMask("****88991")
        let expectedAttributedText = title.attributedString(
            highlights: ["****88991"],
            attrs: [.font: Typography.bodyPrimary(.highlight).font()]
        )
        
        XCTAssertEqual(viewControllerSpy.displayAttributedDescriptionCallCount, 1)
        XCTAssertEqual(viewControllerSpy.attributedStringDisplayed, expectedAttributedText)
        XCTAssertEqual(viewControllerSpy.displayDescriptionCallCount, 0)
    }
    
    func testPresentInfoForDestination_WhenDestinationIsNotEmptyAndFlowIsEmail_ShouldCallDisplayAttributedDescription() {
        sut.presentInfoFor(destination: "****@***il.com", withFlow: .email)
        
        let title = Strings.Code.emailNotificationWithEmailMask("****@***il.com")
        let expectedAttributedText = title.attributedString(
            highlights: ["****@***il.com"],
            attrs: [.font: Typography.bodyPrimary(.highlight).font()]
        )
        
        XCTAssertEqual(viewControllerSpy.displayAttributedDescriptionCallCount, 1)
        XCTAssertEqual(viewControllerSpy.attributedStringDisplayed, expectedAttributedText)
        XCTAssertEqual(viewControllerSpy.displayDescriptionCallCount, 0)
    }
    
    func testPresentErrorAlert_WhenCalledFromInteractor_ShouldDisplayErrorAlertOnViewController() {
        sut.presentErrorAlert(title: "Ops!", message: "Algo não está certo!?")

        XCTAssertEqual(viewControllerSpy.displayAlertCallCount, 1)
        XCTAssertEqual(viewControllerSpy.displayedErrorTitle, "Ops!")
        XCTAssertEqual(viewControllerSpy.displayedErrorMessage, "Algo não está certo!?")
    }

    func testPresentAttemptLimitError_WhenCalledFromInteractor_ShouldDisplayAttemptLimitError() {
        let error = TFAHelperModels().attempLimitErrorJson()
        sut.presentAttempLimitError(tfaError: error, retryTime: 180)

        XCTAssertEqual(viewControllerSpy.displayAttempLimitErrorCallCount, 1)
        XCTAssertEqual(viewControllerSpy.displayedErrorTitle, "Limite atingido")
        XCTAssertEqual(
            viewControllerSpy.displayedErrorMessage,
            "Você excedeu o limite de tentativas. Aguarde 02:52 minutos para tentar novamente."
        )
        XCTAssertEqual(viewControllerSpy.retryTimeDisplayed, 180)
    }
}
