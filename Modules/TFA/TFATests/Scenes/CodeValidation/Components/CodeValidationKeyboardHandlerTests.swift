import Foundation
@testable import TFA
import UI
import XCTest

private final class NotificationCenterSpy: NotificationCenter {
    private(set) var addObserverCallCount = 0
    private(set) var addObserverWithSelectorNameObjectReceivedInvocations:
        [(observer: Any, selector: Selector, name: NSNotification.Name?, object: Any?)] = []
    
    override func addObserver(
        _ observer: Any,
        selector aSelector: Selector,
        name aName: NSNotification.Name?,
        object anObject: Any?
    ) {
        super.addObserver(observer, selector: aSelector, name: aName, object: anObject)
        addObserverCallCount += 1
        addObserverWithSelectorNameObjectReceivedInvocations.append(
            (observer: observer, selector: aSelector, name: aName, object: anObject)
        )
    }
}

final class CodeValidationKeyboardHandlerTests: XCTestCase {
    private let notificationCenterSpy = NotificationCenterSpy()
  
    private lazy var sut: CodeValidationKeyboardHandler = {
        CodeValidationKeyboardHandler(
            scrollView: UIScrollView(),
            contentBottomMargin: Spacing.base05,
            notificationCenter: notificationCenterSpy
        )
    }()
    
    func testRegisterForKeyboardNotifications_ShouldAddObservers() {
        let expectedObservers: [String] = [
            UIResponder.keyboardWillShowNotification.rawValue,
            UIResponder.keyboardWillHideNotification.rawValue
        ]
        
        sut.registerForKeyboardNotifications()
        
        let subscribedObserversNames: [String] = notificationCenterSpy.addObserverWithSelectorNameObjectReceivedInvocations.map {
            return $0.name?.rawValue ?? ""
        }
        
        XCTAssertEqual(notificationCenterSpy.addObserverCallCount, 2)
        XCTAssertEqual(subscribedObserversNames, expectedObservers)
    }
    
    func testKeyboardWillShowNotification_WithCorrectUserInfo_ShouldCallShowHideCompletion() {
        sut.registerForKeyboardNotifications()
        
        let mockedKeyboardHeight: CGFloat = 200
        let keyboardShowHideExpectation = expectation(description: "keyboard changed")
        
        sut.onShowHideCompletion = { keyboardHeight in
            XCTAssertEqual(mockedKeyboardHeight, keyboardHeight)
            keyboardShowHideExpectation.fulfill()
        }
        
        notificationCenterSpy.post(
            name: UIResponder.keyboardWillShowNotification,
            object: nil,
            userInfo: mockKeyboardUserInfo(keyboardHeight: mockedKeyboardHeight)
        )
        
        wait(for: [keyboardShowHideExpectation], timeout: 1)
    }
    
    func testKeyboardWillShowNotification_WithNilUserInfo_ShouldNotCallShowHideCompletion() {
        sut.registerForKeyboardNotifications()
        
        let keyboardShowHideExpectation = expectation(description: "keyboard changed")
        keyboardShowHideExpectation.isInverted = true
        
        sut.onShowHideCompletion = { keyboardHeight in
            keyboardShowHideExpectation.fulfill()
        }
        
        notificationCenterSpy.post(name: UIResponder.keyboardWillShowNotification, object: nil, userInfo: nil)
      
        wait(for: [keyboardShowHideExpectation], timeout: 1)
    }

    func testKeyboardWillShowNotification_WithWrongUserInfo_ShouldNotCallShowHideCompletion() {
        sut.registerForKeyboardNotifications()

        let mockedKeyboardHeight: CGFloat = 200
        let keyboardShowHideExpectation = expectation(description: "keyboard changed")
        keyboardShowHideExpectation.isInverted = true

        sut.onShowHideCompletion = { _ in
            keyboardShowHideExpectation.fulfill()
        }

        notificationCenterSpy.post(
            name: UIResponder.keyboardWillShowNotification,
            object: nil,
            userInfo: mockKeyboardUserInfo(keyboardHeight: mockedKeyboardHeight, includeAnimationInfo: false)
        )
        
        wait(for: [keyboardShowHideExpectation], timeout: 1)
    }
    
    func testKeyboardWillHideNotification_WithCorrectUserInfo_ShouldCallShowHideCompletion() throws {
        sut.registerForKeyboardNotifications()
    
        let keyboardShowHideExpectation = expectation(description: "keyboard changed")
        
        sut.onShowHideCompletion = { keyboardHeight in
            XCTAssertEqual(keyboardHeight, .zero)
            keyboardShowHideExpectation.fulfill()
        }
        
        notificationCenterSpy.post(name: UIResponder.keyboardWillHideNotification, object: nil, userInfo: mockKeyboardUserInfo())
        
        wait(for: [keyboardShowHideExpectation], timeout: 1)
    }
    
    func testKeyboardWillHideNotification_WithNilUserInfo_ShouldNotCallShowHideCompletion() {
        sut.registerForKeyboardNotifications()
        
        let keyboardShowHideExpectation = expectation(description: "keyboard changed")
        keyboardShowHideExpectation.isInverted = true
        
        sut.onShowHideCompletion = { keyboardHeight in
            keyboardShowHideExpectation.fulfill()
        }
        
        notificationCenterSpy.post(name: UIResponder.keyboardWillHideNotification, object: nil, userInfo: nil)
      
        wait(for: [keyboardShowHideExpectation], timeout: 1)
    }
    
    private func mockKeyboardUserInfo(keyboardHeight: CGFloat? = nil, includeAnimationInfo: Bool = true) -> [AnyHashable : Any] {
        
        let mockAnimationDuration = NSNumber(value: 1)
        let mockAnimationCurve = NSNumber(value: 2)
        let mockUserInfo: [(AnyHashable, Any)] = includeAnimationInfo ? [
            (UIResponder.keyboardAnimationDurationUserInfoKey, mockAnimationDuration),
            (UIResponder.keyboardAnimationCurveUserInfoKey, mockAnimationCurve)
        ] : []
        
        guard let mockedKeyboardHeight = keyboardHeight else {
            return Dictionary(uniqueKeysWithValues: mockUserInfo)
        }
        
        let mockKeyboardSize = CGSize(width: .zero, height: mockedKeyboardHeight)
        let mockKeyboardFrameValue = NSValue(cgRect: CGRect(origin: .zero, size: mockKeyboardSize))
        let mockKeyboardFrameInfo = (UIResponder.keyboardFrameEndUserInfoKey, mockKeyboardFrameValue)
        let extendedUserInfoMock = mockUserInfo + [mockKeyboardFrameInfo]
        return Dictionary(uniqueKeysWithValues: extendedUserInfoMock)
    }

}
