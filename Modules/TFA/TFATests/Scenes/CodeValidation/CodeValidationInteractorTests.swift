import Core
@testable import TFA
import XCTest

private final class CodeValidationServiceMock: CodeValidationServicing {
    var getNewCodeExpectedResult: Result<ValidationCode, ApiError> = .failure(.serverError)
    var validateCodeExpectedResult: Result<DeviceAuthValidationResponse, ApiError> = .failure(.serverError)
    
    func getNewCode(requestData: DeviceAuthRequestData, completion: @escaping (Result<ValidationCode, ApiError>) -> Void) {
        completion(getNewCodeExpectedResult)
    }
    
    func validateCodeForNextStep(
        code: String,
        requestData: DeviceAuthRequestData,
        completion: @escaping (Result<DeviceAuthValidationResponse, ApiError>) -> Void
    ) {
        completion(validateCodeExpectedResult)
    }
}

private final class CodeValidationPresenterSpy: CodeValidationPresenting {
    var viewController: CodeValidationDisplaying?
    
    private(set) var presentGoBackAlertCallCount = 0
    private(set) var didNextStepCallCount = 0
    private(set) var didNextStepAction: CodeValidationAction?
    private(set) var presentLoadingCallCount = 0
    private(set) var loadingState = false
    private(set) var presentRetryButtonEnabledCallCount = 0
    private(set) var presentInfoForCallCount = 0
    private(set) var codeDestination: String?
    private(set) var validationFlow: DeviceAuthFlow?
    private(set) var presentErrorAlertCallCount = 0
    private(set) var presentAttempLimitErrorCallCount = 0
    private(set) var presentedErrorTitle = ""
    private(set) var presentedErrorMessage = ""
    private(set) var attemptLimitErrorRetryTime = 0
    private(set) var presentRetryButtonUpdatedCallCount = 0
    private(set) var retryButtonRemainingTime = 0
    
    func didNextStep(action: CodeValidationAction) {
        didNextStepCallCount += 1
        didNextStepAction = action
    }
    
    func presentLoading(_ isLoading: Bool) {
        presentLoadingCallCount += 1
        loadingState = isLoading
    }
    
    func presentRetryButtonEnabled() {
        presentRetryButtonEnabledCallCount += 1
    }
    
    func presentInfoFor(destination: String, withFlow flow: DeviceAuthFlow) {
        presentInfoForCallCount += 1
        codeDestination = destination
        validationFlow = flow
    }
    
    func presentAttempLimitError(tfaError: RequestError, retryTime: Int) {
        presentAttempLimitErrorCallCount += 1
        presentedErrorTitle = tfaError.title
        presentedErrorMessage = tfaError.message
        attemptLimitErrorRetryTime = retryTime
    }
    
    func presentErrorAlert(title: String, message: String) {
        presentErrorAlertCallCount += 1
        presentedErrorTitle = title
        presentedErrorMessage = message
    }
    
    func presentGoBackAlert() {
        presentGoBackAlertCallCount += 1
    }
    
    func presentRetryButtonUpdated(with remainingTime: Int) {
        presentRetryButtonUpdatedCallCount += 1
        retryButtonRemainingTime = remainingTime
    }
}

final class CodeValidationInteractorTests: XCTestCase {
    private lazy var serviceMock = CodeValidationServiceMock()
    private lazy var presenterSpy = CodeValidationPresenterSpy()
    private let timerSpy = TFATimerProtocolSpy()
    
    private lazy var sut = CodeValidationInteractor(
        service: serviceMock,
        presenter: presenterSpy,
        deviceAuthRequestData: DeviceAuthRequestData(flow: .sms, step: .userData, tfaId: "abc"),
        timer: timerSpy
    )
    
    func testLoadDefaultInfo_ShouldPresentInfoForEmptyDestination() {
        sut.loadDefaultInfo()
        
        XCTAssertEqual(presenterSpy.presentInfoForCallCount, 1)
        XCTAssertEqual(presenterSpy.codeDestination, "")
        XCTAssertEqual(presenterSpy.validationFlow, .sms)
    }
    
    func testGoBackStarted_ShouldPresentGoBackAlert() {
        sut.goBackStarted()
        
        XCTAssertEqual(presenterSpy.presentGoBackAlertCallCount, 1)
    }
    
    func testGoBackConfirmed_ShouldCallDidNextStepOnPresenterAndStopTimer() {
        sut.goBackConfirmed()
        
        XCTAssertEqual(presenterSpy.didNextStepCallCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepAction, .back)
        XCTAssertEqual(timerSpy.callStopCount, 1)
    }
    
    func testLoadCode_WhenServiceSucceeds_ShouldPresentDestinationInfo() {
        serviceMock.getNewCodeExpectedResult = .success(.init(retryTime: 60, sentTo: "*2****3712", channel: "SMS"))
        sut.loadCode()
        
        XCTAssertEqual(presenterSpy.presentInfoForCallCount, 1)
        XCTAssertEqual(presenterSpy.codeDestination, "*2****3712")
        XCTAssertEqual(presenterSpy.validationFlow, .sms)
    }
    
    func testLoadCode_WhenServiceHasFailed_ShouldNotPresentDestinationInfo() {
        sut.loadCode()
        
        XCTAssertEqual(presenterSpy.presentLoadingCallCount, 2)
        XCTAssertFalse(presenterSpy.loadingState)
        XCTAssertEqual(presenterSpy.presentInfoForCallCount, 0)
        XCTAssertNil(presenterSpy.codeDestination)
    }

    func testLoadCode_WhenValidationErrorHappens_ShouldPresentErrorAlert() {
        var badRequestError = RequestError()
        badRequestError.title = "Requisição inválida"
        badRequestError.message = "Verifique os dados informados."

        serviceMock.getNewCodeExpectedResult = .failure(.badRequest(body: badRequestError))
        sut.loadCode()

        XCTAssertEqual(presenterSpy.presentErrorAlertCallCount, 1)
        XCTAssertEqual(presenterSpy.presentedErrorTitle, "Requisição inválida")
        XCTAssertEqual(presenterSpy.presentedErrorMessage, "Verifique os dados informados.")
    }

    func testLoadCode_WhenAttempLimitReached_ShouldPresentAttemptLimitError() {
        let attempLimitError = TFAHelperModels().attempLimitErrorJson()

        serviceMock.getNewCodeExpectedResult = .failure(.tooManyRequests(body: attempLimitError))
        sut.loadCode()

        XCTAssertEqual(presenterSpy.presentAttempLimitErrorCallCount, 1)
        XCTAssertEqual(presenterSpy.presentedErrorTitle, "Limite atingido")
        XCTAssertEqual(
            presenterSpy.presentedErrorMessage, "Você excedeu o limite de tentativas. Aguarde 02:52 minutos para tentar novamente."
        )
        XCTAssertEqual(presenterSpy.attemptLimitErrorRetryTime, 10)
    }
    
    func testValidateCode_WhenCodeIsLenghtIsInvalid_ShoudlNotTryToValidate() {
        sut.validateCode("")
        
        XCTAssertEqual(presenterSpy.presentLoadingCallCount, 0)
    }
    
    func testValidateCode_WhenCodeIsInvalid_ShoudlPresentError() {
        serviceMock.validateCodeExpectedResult = .failure(.unauthorized(body: TFAHelperModels().invalidDataErrorJson()))
        sut.validateCode("1234")
        
        XCTAssertEqual(presenterSpy.presentLoadingCallCount, 2)
        XCTAssertEqual(presenterSpy.presentErrorAlertCallCount, 1)
        XCTAssertEqual(presenterSpy.presentedErrorTitle, "Ops!")
        XCTAssertEqual(presenterSpy.presentedErrorMessage, "Dados inválidos")
    }
    
    func testValidateCode_WhenAttempLimitReached_ShouldPresentAttemptLimitError() {
        let attempLimitError = TFAHelperModels().attempLimitErrorJson()

        serviceMock.getNewCodeExpectedResult = .failure(.tooManyRequests(body: attempLimitError))
        sut.loadCode()

        XCTAssertEqual(presenterSpy.presentAttempLimitErrorCallCount, 1)
        XCTAssertEqual(presenterSpy.presentedErrorTitle, "Limite atingido")
        XCTAssertEqual(
            presenterSpy.presentedErrorMessage, "Você excedeu o limite de tentativas. Aguarde 02:52 minutos para tentar novamente."
        )
        XCTAssertEqual(presenterSpy.attemptLimitErrorRetryTime, 10)
    }
    
    func testValidateCode_WhenServiceSucceeds_ShouldStopTimerAndPresentNextStep() {
        serviceMock.validateCodeExpectedResult = .success(.init(nextStep: .final))
        sut.validateCode("1234")
        
        XCTAssertEqual(presenterSpy.didNextStepCallCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepAction, .nextStep(flow: .sms, step: .final))
        XCTAssertEqual(timerSpy.callStopCount, 1)
    }
    
    func testTimerTic_WhenCountdownTimeIsEqualOrLessThanZero_ShouldPresentRetryButtonEnabled() {
        sut.startTimerForNewRequest(-1)

        sut.timerTic()

        XCTAssertEqual(timerSpy.callStopCount, 1)
        XCTAssertEqual(presenterSpy.presentRetryButtonEnabledCallCount, 1)
    }

    func testTimerTic_WhenCountdownTimeIsGreaterThanZero_ShouldPresentRetryButtonUpdatedWithRemainingTime() {
        sut.startTimerForNewRequest(10)

        sut.timerTic()

        XCTAssertEqual(presenterSpy.presentRetryButtonUpdatedCallCount, 1)
        XCTAssertEqual(presenterSpy.retryButtonRemainingTime, 9)
    }
}
