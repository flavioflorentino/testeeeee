import AnalyticsModule
import Foundation
@testable import TFA
import XCTest

private final class DeviceAuthInfoPresenterSpy: DeviceAuthInfoPresenting {
    private(set) var callGoBackCount = 0
    
    func goBack() {
        callGoBackCount += 1
    }
}


final class DeviceAuthInfoInteractorTests: XCTestCase {
    private let analytics = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(analytics)
    
    private lazy var presenterSpy = DeviceAuthInfoPresenterSpy()
    private lazy var sut = DeviceAuthInfoInteractor(presenter: presenterSpy, dependencies: dependencies)
    
    func testSendAnalytics_ShouldSendTheExpectedEvent() {
        sut.sendAnalytics()
        
        let expectedEvent = TFATracker(eventType: InfoEvent.helpAccessed).event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testBack_ShouldCallPresenterGoBack() {
        sut.back()
        
        XCTAssertEqual(presenterSpy.callGoBackCount, 1)
    }
}
