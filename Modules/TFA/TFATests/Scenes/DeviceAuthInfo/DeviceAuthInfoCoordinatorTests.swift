@testable import TFA
import XCTest

private final class DeviceAuthInfoCoordinatorDelegateSpy: DeviceAuthInfoCoordinatorDelegate {
    private(set) var infoDidGoBackCallCount = 0
    
    func infoDidGoBack() {
        infoDidGoBackCallCount += 1
    }
}

final class DeviceAuthInfoCoordinatorTests: XCTestCase {
    private let delegateSpy = DeviceAuthInfoCoordinatorDelegateSpy()
    
    private lazy var sut: DeviceAuthInfoCoordinator = {
        let coordinator = DeviceAuthInfoCoordinator()
        coordinator.delegate = delegateSpy
        return coordinator
    }()
    
    func testPerform_WhenActionIsGoBack_ShouldCallDelegateWithDidGoBackMethod() {
        sut.perform(action: .goBack)
        
        XCTAssertEqual(delegateSpy.infoDidGoBackCallCount, 1)
    }
}
