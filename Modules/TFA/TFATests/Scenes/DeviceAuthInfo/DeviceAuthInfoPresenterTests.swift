@testable import TFA
import XCTest

private final class DeviceAuthInfoCoordinatorSpy: DeviceAuthInfoCoordinating {
    var viewController: UIViewController?
    var delegate: DeviceAuthInfoCoordinatorDelegate?
    
    private(set) var performActionCallCount = 0
    private(set) var actionPerformed: DeviceAuthInfoAction?
    
    func perform(action: DeviceAuthInfoAction) {
        performActionCallCount += 1
        actionPerformed = action
    }
}

final class DeviceAuthInfoPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = DeviceAuthInfoCoordinatorSpy()
    
    private lazy var sut = DeviceAuthInfoPresenter(coordinator: coordinatorSpy)
    
    func testGoBack_ShouldCallCoordinatorWithGoBackAction() {
        sut.goBack()
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.actionPerformed, .goBack)
    }
}
