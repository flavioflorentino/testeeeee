@testable import TFA
import XCTest

private final class ValidationSuccessCoordinatorDelegateSpy: ValidationSuccessCoordinatorDelegate {
    private(set) var successDidEnterCallCount = 0
    
    func successDidEnter() {
        successDidEnterCallCount += 1
    }
}

final class ValidationSuccessCoordinatorTests: XCTestCase {
    private let delegateSpy = ValidationSuccessCoordinatorDelegateSpy()

    private lazy var sut: ValidationSuccessCoordinator = {
        let coordinator = ValidationSuccessCoordinator()
        coordinator.delegate = delegateSpy
        return coordinator
    }()
    
    func testPerform_WhenActionIsEnter_ShouldCallDelegateSuccessDidEnter() {
        sut.perform(action: .enter)
        
        XCTAssertEqual(delegateSpy.successDidEnterCallCount, 1)
    }
}
