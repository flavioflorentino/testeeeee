@testable import TFA
import XCTest

private final class ValidationSuccessPresenterSpy: ValidationSuccessPresenting {
    private(set) var didNextStepCallCount = 0
    private(set) var nextStepAction: ValidationSuccessAction?
    
    func didNextStep(action: ValidationSuccessAction) {
        didNextStepCallCount += 1
        nextStepAction = action
    }
}

final class ValidationSuccessInteractorTests: XCTestCase {
    private lazy var presenterSpy = ValidationSuccessPresenterSpy()
    private lazy var timerSpy = TFATimerProtocolSpy()
    
    private lazy var sut = ValidationSuccessInteractor(
        presenter: presenterSpy,
        deviceAuthRequestData: DeviceAuthRequestData(flow: .sms, step: .userData, tfaId: "abc")
    )
    
    func testDidEnter_ShouldCallPresenterDidNextStepWithEnterAcrion() {
        sut.didEnter()
        
        XCTAssertEqual(presenterSpy.didNextStepCallCount, 1)
        XCTAssertEqual(presenterSpy.nextStepAction, .enter)
    }
}
