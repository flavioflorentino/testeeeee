@testable import TFA
import XCTest

private final class ValidationSuccessCoordinatorSpy: ValidationSuccessCoordinating {
    var viewController: UIViewController?
    var delegate: ValidationSuccessCoordinatorDelegate?
    
    private(set) var actionCalled: ValidationSuccessAction?
    private(set) var performActionCallCount = 0
    
    func perform(action: ValidationSuccessAction) {
        performActionCallCount += 1
        actionCalled = action
    }
}

final class ValidationSuccessPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = ValidationSuccessCoordinatorSpy()
    private lazy var sut = ValidationSuccessPresenter(coordinator: coordinatorSpy)
    
    func testDidNextStep_WhenActionIsEnter_ShouldCallCoordinatorEnterAction() {
        sut.didNextStep(action: .enter)
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.actionCalled, .enter)
    }
}
