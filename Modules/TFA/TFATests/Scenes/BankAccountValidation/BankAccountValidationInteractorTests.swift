import Core
@testable import TFA
import XCTest

private final class BankAccountValidationServiceMock: BankAccountValidationServicing {
    var getBankAccountExpectedResult: Result<TFABankAccount, ApiError> = .failure(.serverError)
    var validateBankAccountExpectedResult: Result<DeviceAuthValidationResponse, ApiError> = .failure(.serverError)
    
    func getBankAccountData(
        requestData: DeviceAuthRequestData,
        completion: @escaping (Result<TFABankAccount, ApiError>) -> Void
    ) {
        completion(getBankAccountExpectedResult)
    }
    
    func validateBankAccountData(
        bankAccount: AccountData,
        requestData: DeviceAuthRequestData,
        completion: @escaping (Result<DeviceAuthValidationResponse, ApiError>) -> Void
    ) {
        completion(validateBankAccountExpectedResult)
    }
}

private final class BankAccountValidationPresenterSpy: BankAccountValidationPresenting {
    var viewController: BankAccountValidationDisplaying?
    private(set) var didNextStepCallCount = 0
    private(set) var nextStepAction: BankAccountValidationAction?
    private(set) var presentGoBackAlertCallCount = 0
    private(set) var presentBankInfoCallCount = 0
    private(set) var presentedBankInfo: TFABankAccount?
    private(set) var presentLoadingCallCount = 0
    private(set) var updateAgencyNumberFieldCallCount = 0
    private(set) var updateAgencyDigitFieldCallCount = 0
    private(set) var updateAccountNumberFieldCallCount = 0
    private(set) var updateAccountDigitFieldCallCount = 0
    private(set) var validationErrorMessage: String?
    private(set) var shouldEnableContinueButtonCallCount = 0
    private(set) var continueButtonEnabled = false
    
    private(set) var presentAttemptLimitErrorCallCount = 0
    private(set) var attemptLimitError: RequestError?
    private(set) var attemptLimitErrorRetryTime = 0
    
    private(set) var presentErrorAlertCallCount = 0
    private(set) var presentedErrorTitle: String?
    private(set) var presentedErrorMessage: String?
    
    private(set) var presentContinueButtonUpdatedCallCount = 0
    private(set) var continueButtonremainingTime = 0
    private(set) var presentFormValidationEnabledCallCount = 0
    
    func didNextStep(action: BankAccountValidationAction) {
        didNextStepCallCount += 1
        nextStepAction = action
    }
    
    func presentBankInfo(bankAccountInfo: TFABankAccount) {
        presentBankInfoCallCount += 1
        presentedBankInfo = bankAccountInfo
    }
    
    func presentLoading(_ isLoading: Bool) {
        presentLoadingCallCount += 1
    }
    
    func presentGoBackAlert() {
        presentGoBackAlertCallCount += 1
    }
    
    func updateAgencyNumberField(withError message: String?) {
        updateAgencyNumberFieldCallCount += 1
        validationErrorMessage = message
    }
    
    func updateAgencyDigitNumberField(withError message: String?) {
        updateAgencyDigitFieldCallCount += 1
        validationErrorMessage = message
    }
    
    func updateAccountNumberField(withError message: String?) {
        updateAccountNumberFieldCallCount += 1
        validationErrorMessage = message
    }
    
    func updateAccountDigitNumberField(withError message: String?) {
        updateAccountDigitFieldCallCount += 1
        validationErrorMessage = message
    }
    
    func shouldEnableContinueButton(_ isEnabled: Bool) {
        shouldEnableContinueButtonCallCount += 1
        continueButtonEnabled = isEnabled
    }
    
    func presentAttempLimitError(tfaError: RequestError, retryTime: Int) {
        presentAttemptLimitErrorCallCount += 1
        attemptLimitError = tfaError
        attemptLimitErrorRetryTime = retryTime
    }
    
    func presentErrorAlert(title: String, message: String) {
        presentErrorAlertCallCount += 1
        presentedErrorTitle = title
        presentedErrorMessage = message
    }
    
    func presentContinueButtonUpdated(with remainingTime: Int) {
        presentContinueButtonUpdatedCallCount += 1
        continueButtonremainingTime = remainingTime
    }
    
    func presentFormValidationEnabled() {
        presentFormValidationEnabledCallCount += 1
    }
}

final class BankAccountValidationInteractorTests: XCTestCase {
    private lazy var serviceMock = BankAccountValidationServiceMock()
    private lazy var presenterSpy = BankAccountValidationPresenterSpy()
    private lazy var timerSpy = TFATimerProtocolSpy()
    
    private lazy var sut = BankAccountValidationInteractor(
        service: serviceMock,
        presenter: presenterSpy,
        deviceAuthRequestData: DeviceAuthRequestData(flow: .sms, step: .userData, tfaId: "abc"),
        timer: timerSpy
    )
    
    func testBackButtonWasTapped_WhenCalledFromInteractor_ShouldPresentGoBackAlert() {
        sut.backButtonWasTapped()
        
        XCTAssertEqual(presenterSpy.presentGoBackAlertCallCount, 1)
    }
    
    func testGoBack_WhenCalledFromInteractor_ShouldCallDidNextStepOnPresenter() {
        sut.goBack()
        
        XCTAssertEqual(presenterSpy.didNextStepCallCount, 1)
        XCTAssertEqual(presenterSpy.nextStepAction, .back)
    }
    
    func testLoadBankInfo_WhenResponseIsSuccess_ShouldCallPresentBankInfoOnPresenter() {
        serviceMock.getBankAccountExpectedResult = .success(.init(bank: .init(bankName: "Caixa", imageUrl: nil, agencyDv: false)))
        sut.loadBankInfo()
        
        XCTAssertEqual(presenterSpy.presentBankInfoCallCount, 1)
        XCTAssertEqual(presenterSpy.presentLoadingCallCount, 2)
        XCTAssertEqual(presenterSpy.presentedBankInfo?.bank.bankName, "Caixa")
    }
    
    func testCheckAgencyNumber_WhenAgencyIsValid_ShouldCallUpdateAgencyNumberFieldWithoutError() {
        sut.checkAgencyNumber("1234")
        
        XCTAssertEqual(presenterSpy.updateAgencyNumberFieldCallCount, 1)
        XCTAssertNil(presenterSpy.validationErrorMessage)
    }
    
    func testCheckAgencyNumber_WhenAgencyIsInvalid_ShouldCallUpdateAgencyNumberFieldWithError() {
        sut.checkAgencyNumber("")
        
        XCTAssertEqual(presenterSpy.updateAgencyNumberFieldCallCount, 1)
        XCTAssertEqual(presenterSpy.validationErrorMessage, Strings.BankAccount.agencyRequired)
    }
    
    func testCheckAgencyDigit_WhenDigitIsValid_ShouldCallUpdateAgencyDigitFieldWithoutError() {
        sut.checkAgencyDigit("1")
        
        XCTAssertEqual(presenterSpy.updateAgencyDigitFieldCallCount, 1)
        XCTAssertNil(presenterSpy.validationErrorMessage)
    }
    
    func testCheckAgencyDigit_WhenDigitIsInvalid_ShouldCallUpdateAgencyDigitFieldWithError() {
        sut.checkAgencyDigit("13")
        
        XCTAssertEqual(presenterSpy.updateAgencyDigitFieldCallCount, 1)
        XCTAssertEqual(presenterSpy.validationErrorMessage, Strings.BankAccount.digitRequired)
    }
    
    func testCheckAccountNumber_WhenAccountIsValid_ShouldCallUpdateAccountNumberFieldWithoutError() {
        sut.checkAccountNumber("1234")
        
        XCTAssertEqual(presenterSpy.updateAccountNumberFieldCallCount, 1)
        XCTAssertNil(presenterSpy.validationErrorMessage)
    }
    
    func testCheckAccountNumber_WhenAccountIsInvalid_ShouldCallUpdateAccountNumberFieldWithError() {
        sut.checkAccountNumber("")

        XCTAssertEqual(presenterSpy.updateAccountNumberFieldCallCount, 1)
        XCTAssertEqual(presenterSpy.validationErrorMessage, Strings.BankAccount.accountNumberRequired)
    }

    func testCheckAccountDigit_WhenDigitIsValid_ShouldCallUpdateAccountDigitFieldWithoutError() {
        sut.checkAccountDigit("1")

        XCTAssertEqual(presenterSpy.updateAccountDigitFieldCallCount, 1)
        XCTAssertNil(presenterSpy.validationErrorMessage)
    }

    func testCheckAccountDigit_WhenDigitIsInvalid_ShouldCallUpdateAccountDigitFieldWithError() {
        sut.checkAccountDigit("13")

        XCTAssertEqual(presenterSpy.updateAccountDigitFieldCallCount, 1)
        XCTAssertEqual(presenterSpy.validationErrorMessage, Strings.BankAccount.digitRequired)
    }
    
    func testValidateForm_WhenServerHasError_ShouldPresentsGenericError() {
        serviceMock.validateBankAccountExpectedResult = .failure(.serverError)
        sut.validateForm()

        XCTAssertEqual(presenterSpy.presentErrorAlertCallCount, 1)
        XCTAssertEqual(presenterSpy.presentedErrorTitle, Strings.Alerts.genericErrorTitle)
        XCTAssertEqual(presenterSpy.presentedErrorMessage, Strings.Alerts.genericErrorMessage)
    }

    func testValidateForm_WhenInputIsInvalid_ShouldPresentNotAuthorizedError() {
        var notAuthorizedError = RequestError()
        notAuthorizedError.title = "Dados inválidos"
        notAuthorizedError.message = "Verifique os dados informados."

        serviceMock.validateBankAccountExpectedResult = .failure(.unauthorized(body: notAuthorizedError))
        sut.validateForm()

        XCTAssertEqual(presenterSpy.presentErrorAlertCallCount, 1)
        XCTAssertEqual(presenterSpy.presentedErrorTitle, "Dados inválidos")
        XCTAssertEqual(presenterSpy.presentedErrorMessage, "Verifique os dados informados.")
    }

    func testValidateForm_WhenAttempLimitReached_ShouldPresentAttemptLimitError() {
        let attempLimitError = TFAHelperModels().attempLimitErrorJson()

        serviceMock.validateBankAccountExpectedResult = .failure(.tooManyRequests(body: attempLimitError))
        sut.validateForm()

        XCTAssertNotNil(presenterSpy.attemptLimitError)
        XCTAssertEqual(presenterSpy.presentAttemptLimitErrorCallCount, 1)
        XCTAssertEqual(presenterSpy.attemptLimitError?.title, "Limite atingido")
        XCTAssertEqual(
            presenterSpy.attemptLimitError?.message,
            "Você excedeu o limite de tentativas. Aguarde 02:52 minutos para tentar novamente."
        )
        XCTAssertEqual(presenterSpy.attemptLimitError?.code, "form_limit")
        XCTAssertEqual(presenterSpy.attemptLimitErrorRetryTime, 10)
    }

    func testValidateForm_WhenInputIsValid_ShouldCallNextStepWithReceivedStep() {
        serviceMock.validateBankAccountExpectedResult = .success(DeviceAuthValidationResponse(nextStep: .phoneNumber))

        sut.validateForm()

        XCTAssertEqual(presenterSpy.didNextStepCallCount, 1)
        XCTAssertEqual(presenterSpy.nextStepAction, .nextStep(flow: .sms, step: .phoneNumber))
    }

    func testTimerTic_WhenCountdownTimeIsEqualOrLessThanZero_ShouldPresentFormValidationEnabled() {
        sut.startRetryValidationTimer(retryTime: -1)

        sut.timerTic()

        XCTAssertEqual(timerSpy.callStopCount, 1)
        XCTAssertEqual(presenterSpy.presentFormValidationEnabledCallCount, 1)
    }

    func testTimerTic_WhenCountdownTimeIsGreaterThanZero_ShouldPresentContinueButtonUpdatedWithRemainingTime() {
        sut.startRetryValidationTimer(retryTime: 10)

        sut.timerTic()

        XCTAssertEqual(presenterSpy.presentContinueButtonUpdatedCallCount, 1)
        XCTAssertEqual(presenterSpy.continueButtonremainingTime, 9)
    }
}
