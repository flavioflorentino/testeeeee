@testable import TFA
import XCTest

private final class BankAccountValidationCoordinatorSpy: BankAccountValidationCoordinating {
    var viewController: UIViewController?
    var delegate: DeviceAuthFlowStepping?
    
    private(set) var actionCalled: BankAccountValidationAction?
    private(set) var performActionCallCount = 0
    
    func perform(action: BankAccountValidationAction) {
        performActionCallCount += 1
        actionCalled = action
    }
}

private final class BankAccountValidationViewControllerSpy: BankAccountValidationDisplaying {
    private(set) var displayCancelAuthorizationAlertCallCount = 0
    private(set) var displayLoadingCallCount = 0
    private(set) var displayBankInfoCallCount = 0
    private(set) var bankInfoDisplayed: TFABankInfo?
    private(set) var updateAgencyFieldErrorMessageCallCount = 0
    private(set) var updateAgencyDigitFieldErrorMessageCallCount = 0
    private(set) var updateAccountFieldErrorMessageCallCount = 0
    private(set) var updateAccountDigitFieldErrorMessageCallCount = 0
    private(set) var validationErrorMessage: String?
    private(set) var enableContinueButtonCallCount = 0
    private(set) var isContinueButtonEnabled = false
    private(set) var displayAttemptLimitErrorCallCount = 0
    private(set) var displayedErrorTitle = ""
    private(set) var displayedErrorMessage = ""
    private(set) var displayAlertCallCount = 0
    private(set) var retryTimeDisplayed = 0
    private(set) var displayCountdownTimerCallCount = 0
    private(set) var countdownRemainingTime = ""
    private(set) var enableValidationFormCallCount = 0
    
    func displayLoading(_ isLoading: Bool) {
        displayLoadingCallCount += 1
    }
    
    func displayBankInfo(bankInfo: TFABankInfo) {
        displayBankInfoCallCount += 1
        bankInfoDisplayed = bankInfo
    }
    
    func displayCancelAuthorizationAlert() {
        displayCancelAuthorizationAlertCallCount += 1
    }
    
    func updateAgencyFieldErrorMessage(_ message: String?) {
        updateAgencyFieldErrorMessageCallCount += 1
        validationErrorMessage = message
    }
    
    func updateAgencyDigitFieldErrorMessage(_ message: String?) {
        updateAgencyDigitFieldErrorMessageCallCount += 1
        validationErrorMessage = message
    }
    
    func updateAccountFieldErrorMessage(_ message: String?) {
        updateAccountFieldErrorMessageCallCount += 1
        validationErrorMessage = message
    }
    
    func updateAccountDigitFieldErrorMessage(_ message: String?) {
        updateAccountDigitFieldErrorMessageCallCount += 1
        validationErrorMessage = message
    }
    
    func enableContinueButton(_ enable: Bool) {
        enableContinueButtonCallCount += 1
        isContinueButtonEnabled = enable
    }
    
    func displayAttempLimitError(with title: String, message: String, retryTime: Int) {
        displayAttemptLimitErrorCallCount += 1
        displayedErrorTitle = title
        displayedErrorMessage = message
        retryTimeDisplayed = retryTime
    }
    
    func displayAlert(title: String, message: String) {
        displayAlertCallCount += 1
        displayedErrorTitle = title
        displayedErrorMessage = message
    }
    
    func displayCountdownTimer(with remainingTime: String) {
        displayCountdownTimerCallCount += 1
        countdownRemainingTime = remainingTime
    }
    
    func enableFormValidation() {
        enableValidationFormCallCount += 1
    }
}

final class BankAccountValidationPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = BankAccountValidationCoordinatorSpy()
    private lazy var viewControllerSpy = BankAccountValidationViewControllerSpy()
    
    private lazy var sut: BankAccountValidationPresenter = {
        let presenter = BankAccountValidationPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testPresentGoBackAlert_ShouldDisplayCancelAuthorizationAlertOnViewController() {
        sut.presentGoBackAlert()
        
        XCTAssertEqual(viewControllerSpy.displayCancelAuthorizationAlertCallCount, 1)
    }
    
    func testPresentLoading_ShouldDisplayLoadingOnViewController() {
        sut.presentLoading(true)
        
        XCTAssertEqual(viewControllerSpy.displayLoadingCallCount, 1)
    }
    
    func testPresentBankInfo_ShouldDisplayInfoOnViewController() {
        sut.presentBankInfo(bankAccountInfo: TFABankAccount(bank: TFABankInfo(bankName: "Caixa", imageUrl: nil, agencyDv: false)))
        
        XCTAssertEqual(viewControllerSpy.displayBankInfoCallCount, 1)
        XCTAssertEqual(viewControllerSpy.bankInfoDisplayed?.bankName, "Caixa")
    }
    
    func testDidNextStep_WhenActionIsBank_ShouldCallCoordinatorWithBackAction() {
        sut.didNextStep(action: .back)
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.actionCalled, .back)
    }
    
    func testUpdateAgencyNumberField_WhenErrorIsNil_ShouldCallUpdateAgencyErrorMessageOnViewControllerWithoutError() {
        sut.updateAgencyNumberField(withError: nil)
        
        XCTAssertEqual(viewControllerSpy.updateAgencyFieldErrorMessageCallCount, 1)
        XCTAssertNil(viewControllerSpy.validationErrorMessage)
    }
    
    func testUpdateAgencyNumberField_WhenErrorIsNotNil_ShouldCallUpdateAgencyErrorMessageOnViewControllerWithError() {
        sut.updateAgencyNumberField(withError: "errorMessage")
        
        XCTAssertEqual(viewControllerSpy.updateAgencyFieldErrorMessageCallCount, 1)
        XCTAssertNotNil(viewControllerSpy.validationErrorMessage)
        XCTAssertEqual(viewControllerSpy.validationErrorMessage, "errorMessage")
    }
    
    func testUpdateAgencyDigitNumberField_WhenErrorIsNil_ShouldCallUpdateAgencyDigitErrorMessageOnViewControllerWithoutError() {
        sut.updateAgencyDigitNumberField(withError: nil)
        
        XCTAssertEqual(viewControllerSpy.updateAgencyDigitFieldErrorMessageCallCount, 1)
        XCTAssertNil(viewControllerSpy.validationErrorMessage)
    }
    
    func testUpdateAgencyDigitNumberField_WhenErrorIsNotNil_ShouldCallUpdateAgencyDigitErrorMessageOnViewControllerWithError() {
        sut.updateAgencyDigitNumberField(withError: "errorMessage")
        
        XCTAssertEqual(viewControllerSpy.updateAgencyDigitFieldErrorMessageCallCount, 1)
        XCTAssertNotNil(viewControllerSpy.validationErrorMessage)
        XCTAssertEqual(viewControllerSpy.validationErrorMessage, "errorMessage")
    }
    
    func testUpdateAccountNumberField_WhenErrorIsNil_ShouldCallUpdateAccountErrorMessageOnViewControllerWithoutError() {
        sut.updateAccountNumberField(withError: nil)
        
        XCTAssertEqual(viewControllerSpy.updateAccountFieldErrorMessageCallCount, 1)
        XCTAssertNil(viewControllerSpy.validationErrorMessage)
    }
    
    func testUpdateAccountNumberField_WhenErrorIsNotNil_ShouldCallUpdateAccountErrorMessageOnViewControllerWithError() {
        sut.updateAccountNumberField(withError: "errorMessage")
        
        XCTAssertEqual(viewControllerSpy.updateAccountFieldErrorMessageCallCount, 1)
        XCTAssertNotNil(viewControllerSpy.validationErrorMessage)
        XCTAssertEqual(viewControllerSpy.validationErrorMessage, "errorMessage")
    }
    
    func testUpdateAccountDigitNumberField_WhenErrorIsNil_ShouldCallUpdateAccountDigitErrorMessageOnViewControllerWithoutError() {
        sut.updateAccountDigitNumberField(withError: nil)
        
        XCTAssertEqual(viewControllerSpy.updateAccountDigitFieldErrorMessageCallCount, 1)
        XCTAssertNil(viewControllerSpy.validationErrorMessage)
    }
    
    func testUpdateAccountDigitNumberField_WhenErrorIsNotNil_ShouldCallUpdateAccountDigitErrorMessageOnViewControllerWithError() {
        sut.updateAccountDigitNumberField(withError: "errorMessage")
        
        XCTAssertEqual(viewControllerSpy.updateAccountDigitFieldErrorMessageCallCount, 1)
        XCTAssertNotNil(viewControllerSpy.validationErrorMessage)
        XCTAssertEqual(viewControllerSpy.validationErrorMessage, "errorMessage")
    }
    
    func testShouldEnableContinueButton_WhenEnableIsTrue_ShouldDisplayButtonEnabled() {
        sut.shouldEnableContinueButton(true)

        XCTAssertEqual(viewControllerSpy.enableContinueButtonCallCount, 1)
        XCTAssertTrue(viewControllerSpy.isContinueButtonEnabled)
    }

    func testPresentErrorAlert_WhenCalledFromViewModel_ShouldDisplayErrorAlertOnViewController() {
        sut.presentErrorAlert(title: "Ops!", message: "Algo não está certo!?")

        XCTAssertEqual(viewControllerSpy.displayAlertCallCount, 1)
        XCTAssertEqual(viewControllerSpy.displayedErrorTitle, "Ops!")
        XCTAssertEqual(viewControllerSpy.displayedErrorMessage, "Algo não está certo!?")
    }

    func testPresentAttemptLimitError_WhenCalledFromViewModel_ShouldDisplayAttemptLimitError() {
        let error = TFAHelperModels().attempLimitErrorJson()
        sut.presentAttempLimitError(tfaError: error, retryTime: 180)

        XCTAssertEqual(viewControllerSpy.displayAttemptLimitErrorCallCount, 1)
        XCTAssertEqual(viewControllerSpy.displayedErrorTitle, "Limite atingido")
        XCTAssertEqual(
            viewControllerSpy.displayedErrorMessage,
            "Você excedeu o limite de tentativas. Aguarde 02:52 minutos para tentar novamente."
        )
        XCTAssertEqual(viewControllerSpy.retryTimeDisplayed, 180)
    }

    func testPresentContinueButtonUpdated_ShouldDisplayContinueButtonWithRemainingTime() {
        sut.presentContinueButtonUpdated(with: 100)

        XCTAssertEqual(viewControllerSpy.displayCountdownTimerCallCount, 1)
        XCTAssertEqual(viewControllerSpy.countdownRemainingTime, "Aguarde 01:40")
    }
    
    func testPresentFormValidationEnabled_ShouldEnableFormOnViewController() {
        sut.presentFormValidationEnabled()
        
        XCTAssertEqual(viewControllerSpy.enableValidationFormCallCount, 1)
    }
}
