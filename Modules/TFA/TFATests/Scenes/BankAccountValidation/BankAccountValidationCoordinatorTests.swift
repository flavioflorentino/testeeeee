@testable import TFA
import UI
import XCTest

private final class BankAccountValidationCoordinatorDelegateSpy: DeviceAuthFlowStepping {
    private(set) var validationDidGoBackCallCount = 0
    private(set) var receivedNextStepCallCount = 0
    private(set) var validationFlow: DeviceAuthFlow?
    private(set) var validationStep: DeviceAuthStep?
    
    func validationDidGoBack() {
        validationDidGoBackCallCount += 1
    }
    
    func receivedNextStep(forFlow flow: DeviceAuthFlow, step: DeviceAuthStep) {
        receivedNextStepCallCount += 1
        validationFlow = flow
        validationStep = step
    }
}

final class BankAccountValidationCoordinatorTests: XCTestCase {
    private let delegateSpy = BankAccountValidationCoordinatorDelegateSpy()

    private lazy var sut: BankAccountValidationCoordinator = {
        let coordinator = BankAccountValidationCoordinator()
        coordinator.delegate = delegateSpy
        return coordinator
    }()
    
    func testPerform_WhenActionIsBack_ShouldCallValidationDigGoBackOnDelegate() {
        sut.perform(action: .back)
        
        XCTAssertEqual(delegateSpy.validationDidGoBackCallCount, 1)
    }
    
    func testPerform_WhenActionIsNextStep_ShouldCallDidValidateAccountDataOnDelegate() {
        sut.perform(action: .nextStep(flow: .sms, step: .code))
        
        XCTAssertEqual(delegateSpy.receivedNextStepCallCount, 1)
        XCTAssertEqual(delegateSpy.validationFlow, .sms)
        XCTAssertEqual(delegateSpy.validationStep, .code)
    }
}
