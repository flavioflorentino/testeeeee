@testable import TFA
import UI
import XCTest

private final class PhoneNumberValidationCoordinatorSpy: PhoneNumberValidationCoordinating {
    var delegate: DeviceAuthFlowStepping?
    var viewController: UIViewController?
    
    private(set) var performActionCallCount = 0
    private(set) var actionPerformed: PhoneNumberValidationAction?
    
    func perform(action: PhoneNumberValidationAction) {
        performActionCallCount += 1
        actionPerformed = action
    }
}

private final class PhoneNumberValidationViewControllerSpy: PhoneNumberValidationDisplaying {
    private(set) var displayLoadingCallCount = 0
    private(set) var loadingState = false
    private(set) var updatePhoneNumberFieldErrorMessageCallCount = 0
    private(set) var phoneNumberErrorMessage: String?
    private(set) var enableContinueButtonCallCount = 0
    private(set) var isButtonEnabled = false
    private(set) var displayAttributedDescriptionCallCount = 0
    private(set) var attributedStringDisplayed: NSAttributedString?
    private(set) var displayAlertCallCount = 0
    private(set) var displayCancelAuthorizationAlertCallCount = 0
    private(set) var displayAttempLimitErrorCallCount = 0
    private(set) var displayedErrorTitle = ""
    private(set) var displayedErrorMessage = ""
    private(set) var retryTimeDisplayed = 0
    private(set) var displayCountdownTimerCallCount = 0
    private(set) var countdownRemainingTime = ""
    private(set) var enableValidationFormCallCount = 0
    
    func displayLoading(_ isLoading: Bool) {
        displayLoadingCallCount += 1
        loadingState = isLoading
    }
    
    func updatePhoneNumberFieldErrorMessage(_ message: String?) {
        updatePhoneNumberFieldErrorMessageCallCount += 1
        phoneNumberErrorMessage = message
    }
    
    func enableContinueButton(_ enable: Bool) {
        enableContinueButtonCallCount += 1
        isButtonEnabled = enable
    }
    
    func displayAttributedDescription(_ attributedString: NSAttributedString) {
        displayAttributedDescriptionCallCount += 1
        attributedStringDisplayed = attributedString
    }
    
    func displayCancelAuthorizationAlert() {
        displayCancelAuthorizationAlertCallCount += 1
    }
    
    func displayAttempLimitError(with title: String, message: String, retryTime: Int) {
        displayAttempLimitErrorCallCount += 1
        displayedErrorTitle = title
        displayedErrorMessage = message
        retryTimeDisplayed = retryTime
    }
    
    func displayAlert(title: String, message: String) {
        displayAlertCallCount += 1
        displayedErrorTitle = title
        displayedErrorMessage = message
    }
    
    func displayCountdownTimer(with remainingTime: String) {
        displayCountdownTimerCallCount += 1
        countdownRemainingTime = remainingTime
    }
    
    func enableFormValidation() {
        enableValidationFormCallCount += 1
    }
}

final class PhoneNumberValidationPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = PhoneNumberValidationCoordinatorSpy()
    private lazy var viewControllerSpy = PhoneNumberValidationViewControllerSpy()
    
    private lazy var sut: PhoneNumberValidationPresenter = {
        let presenter = PhoneNumberValidationPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testDidNextStep_WhenActionIsBack_ShouldPerformActionOnCoordinator() {
        sut.didNextStep(action: .back)
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.actionPerformed, .back)
    }
    
    func testPresentLoading_WhenCalledFromInteractor_ShouldDisplayLoadingOnViewController() {
        sut.presentLoading(true)
        
        XCTAssertEqual(viewControllerSpy.displayLoadingCallCount, 1)
        XCTAssertTrue(viewControllerSpy.loadingState)
    }
    
    func testPresentGoBackAlert_WhenCalledFromInteractor_ShouldDisplayCancelAuthorizationAlert() {
        sut.presentGoBackAlert()
        
        XCTAssertEqual(viewControllerSpy.displayCancelAuthorizationAlertCallCount, 1)
    }
    
    func testPresentHintForNumber_WhenHintIsNotNil_ShouldDisplayAttributedTextOnViewController() {
        let hint = "****9999"
        sut.presentHintForNumber(hint)
     
        let phoneDescriptionText = Strings.PhoneNumber.body
        let completeText = phoneDescriptionText + hint
        
        let attributed = completeText.attributedString(
            highlights: [hint],
            attrs: [NSAttributedString.Key.font: Typography.bodyPrimary(.highlight).font()]
        )
        
        XCTAssertEqual(viewControllerSpy.displayAttributedDescriptionCallCount, 1)
        XCTAssertEqual(viewControllerSpy.attributedStringDisplayed, attributed)
    }
    
    func testPresentHintForNumber_WhenHintNil_ShouldNotDisplayAttributedTextOnViewController() {
        sut.presentHintForNumber(nil)
        
        XCTAssertEqual(viewControllerSpy.displayAttributedDescriptionCallCount, 0)
    }
    
    func testUpdatePhoneNumberField_WhenErrorIsNotNil_ShouldDisplayErrorOnViewController() {
        sut.updatePhoneNumberField(withError: Strings.PhoneNumber.phoneFieldError)
        
        XCTAssertEqual(viewControllerSpy.updatePhoneNumberFieldErrorMessageCallCount, 1)
        XCTAssertEqual(viewControllerSpy.phoneNumberErrorMessage, Strings.PhoneNumber.phoneFieldError)
    }
    
    func testUpdatePhoneNumberField_WhenErrorIsNil_ShouldNotDisplayErrorOnViewController() {
        sut.updatePhoneNumberField(withError: nil)
        
        XCTAssertEqual(viewControllerSpy.updatePhoneNumberFieldErrorMessageCallCount, 1)
        XCTAssertNil(viewControllerSpy.phoneNumberErrorMessage)
    }
    
    func testShouldEnableContinueButton_WhenCalledFromInteractor_ShouldEnableContinueButtonOnViewController() {
        sut.shouldEnableContinueButton(true)
        
        XCTAssertEqual(viewControllerSpy.enableContinueButtonCallCount, 1)
        XCTAssertTrue(viewControllerSpy.isButtonEnabled)
    }
    
    func testPresentErrorAlert_WhenCalledFromViewModel_ShouldDisplayErrorAlertOnViewController() {
        sut.presentErrorAlert(title: "Ops!", message: "Algo não está certo!?")

        XCTAssertEqual(viewControllerSpy.displayAlertCallCount, 1)
        XCTAssertEqual(viewControllerSpy.displayedErrorTitle, "Ops!")
        XCTAssertEqual(viewControllerSpy.displayedErrorMessage, "Algo não está certo!?")
    }

    func testPresentAttemptLimitError_WhenCalledFromViewModel_ShouldDisplayAttemptLimitError() {
        let error = TFAHelperModels().attempLimitErrorJson()
        sut.presentAttempLimitError(tfaError: error, retryTime: 180)

        XCTAssertEqual(viewControllerSpy.displayAttempLimitErrorCallCount, 1)
        XCTAssertEqual(viewControllerSpy.displayedErrorTitle, "Limite atingido")
        XCTAssertEqual(
            viewControllerSpy.displayedErrorMessage,
            "Você excedeu o limite de tentativas. Aguarde 02:52 minutos para tentar novamente."
        )
        XCTAssertEqual(viewControllerSpy.retryTimeDisplayed, 180)
    }

    func testPresentContinueButtonUpdated_ShouldDisplayContinueButtonWithRemainingTime() {
        sut.presentContinueButtonUpdated(with: 100)

        XCTAssertEqual(viewControllerSpy.displayCountdownTimerCallCount, 1)
        XCTAssertEqual(viewControllerSpy.countdownRemainingTime, "Aguarde 01:40")
    }
    
    func testPresentFormValidationEnabled_ShouldEnableFormOnViewController() {
        sut.presentFormValidationEnabled()
        
        XCTAssertEqual(viewControllerSpy.enableValidationFormCallCount, 1)
    }
}
