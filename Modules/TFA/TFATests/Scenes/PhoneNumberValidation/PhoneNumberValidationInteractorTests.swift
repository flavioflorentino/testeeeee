import Core
@testable import TFA
import XCTest

private final class PhoneNumberValidationServiceMock: PhoneNumberValidationServicing {
    var getPhoneNumberExpectedResult: Result<TFAPhoneNumberResponse, ApiError> = .failure(.serverError)
    var validatePhoneNumberExpectedResult: Result<DeviceAuthValidationResponse, ApiError> = .failure(.serverError)
    
    func getPhoneNumberData(
        requestData: DeviceAuthRequestData,
        completion: @escaping (Result<TFAPhoneNumberResponse, ApiError>) -> Void
    ) {
        completion(getPhoneNumberExpectedResult)
    }
    
    func validatePhoneNumber(
        phoneNumber: String,
        requestData: DeviceAuthRequestData,
        completion: @escaping (Result<DeviceAuthValidationResponse, ApiError>) -> Void
    ) {
        completion(validatePhoneNumberExpectedResult)
    }
}

private final class PhoneNumberValidationPresenterSpy: PhoneNumberValidationPresenting {
    var viewController: PhoneNumberValidationDisplaying?
    private(set) var didNextStepCallCount = 0
    private(set) var receivedStep: PhoneNumberValidationAction?
    private(set) var presentLoadingCallCount = 0
    private(set) var loadingState = false
    private(set) var presentGoBackAlertCallCount = 0
    private(set) var presentHintForNumberCallCount = 0
    private(set) var hintPresented: String?
    private(set) var updatePhoneNumberFieldCallCount = 0
    private(set) var validationErrorMessage: String?
    private(set) var shouldEnableContinueButtonCallCount = 0
    private(set) var isButtonEnabled = false
    private(set) var presentErrorAlertCallCount = 0
    private(set) var presentAttempLimitErrorCallCount = 0
    private(set) var presentedErrorTitle = ""
    private(set) var presentedErrorMessage = ""
    private(set) var attemptLimitErrorRetryTime = 0
    private(set) var presentFormValidationEnabledCallCount = 0
    private(set) var presentContinueButtonUpdatedCallCount = 0
    private(set) var continueButtonRemainingTime = 0
    
    func didNextStep(action: PhoneNumberValidationAction) {
        didNextStepCallCount += 1
        receivedStep = action
    }
    
    func presentLoading(_ isLoading: Bool) {
        presentLoadingCallCount += 1
        loadingState = isLoading
    }
    
    func presentGoBackAlert() {
        presentGoBackAlertCallCount += 1
    }
  
    func presentHintForNumber(_ hint: String?) {
        presentHintForNumberCallCount += 1
        hintPresented = hint
    }
    
    func updatePhoneNumberField(withError message: String?) {
        updatePhoneNumberFieldCallCount += 1
        validationErrorMessage = message
    }
    
    func shouldEnableContinueButton(_ isEnabled: Bool) {
        shouldEnableContinueButtonCallCount += 1
        isButtonEnabled = isEnabled
    }
    
    func presentAttempLimitError(tfaError: RequestError, retryTime: Int) {
        presentAttempLimitErrorCallCount += 1
        presentedErrorTitle = tfaError.title
        presentedErrorMessage = tfaError.message
        attemptLimitErrorRetryTime = retryTime
    }
    
    func presentErrorAlert(title: String, message: String) {
        presentErrorAlertCallCount += 1
        presentedErrorTitle = title
        presentedErrorMessage = message
    }
    
    func presentContinueButtonUpdated(with remainingTime: Int) {
        presentContinueButtonUpdatedCallCount += 1
        continueButtonRemainingTime = remainingTime
    }
    
    func presentFormValidationEnabled() {
        presentFormValidationEnabledCallCount += 1
    }
}

final class PhoneNumberValidationInteractorTests: XCTestCase {
    private lazy var serviceMock = PhoneNumberValidationServiceMock()
    private lazy var presenterSpy = PhoneNumberValidationPresenterSpy()
    private let timerSpy = TFATimerProtocolSpy()
    
    private lazy var sut = PhoneNumberValidationInteractor(
        service: serviceMock,
        presenter: presenterSpy,
        deviceAuthRequestData: DeviceAuthRequestData(flow: .sms, step: .userData, tfaId: "abc"),
        timer: timerSpy
    )
    
    func testLoadPhoneInfo_WhenServiceReturnIsOK_ShouldPresentPhoneNumberHint() {
        serviceMock.getPhoneNumberExpectedResult = .success(.init(phone: .init(phoneNumber: "****4124")))
        sut.loadPhoneInfo()
        
        XCTAssertEqual(presenterSpy.presentLoadingCallCount, 2)
        XCTAssertFalse(presenterSpy.loadingState)
        XCTAssertEqual(presenterSpy.presentHintForNumberCallCount, 1)
        XCTAssertEqual(presenterSpy.hintPresented, "****4124")
    }
    
    func testLoadPhoneInfo_WhenServiceReturnIsError_ShouldNotPresentPhoneNumberHint() {
        sut.loadPhoneInfo()
        
        XCTAssertEqual(presenterSpy.presentLoadingCallCount, 2)
        XCTAssertFalse(presenterSpy.loadingState)
        XCTAssertEqual(presenterSpy.presentHintForNumberCallCount, 0)
        XCTAssertNil(presenterSpy.hintPresented)
    }
    
    func testBackButtonWasTapped_ShouldPresentGoBackAlert() {
        sut.backButtonWasTapped()
        
        XCTAssertEqual(presenterSpy.presentGoBackAlertCallCount, 1)
    }
    
    func testGoBack_ShouldCallDidNextStepOnPresenter() {
        sut.goBack()
        
        XCTAssertEqual(presenterSpy.didNextStepCallCount, 1)
        XCTAssertEqual(presenterSpy.receivedStep, .back)
    }
    
    func testCheckPhoneNumber_WhenPhoneIsInvalid_ShouldPresentInvalidErrorMessage() {
        sut.checkPhoneNumber("")
        
        XCTAssertEqual(presenterSpy.updatePhoneNumberFieldCallCount, 1)
        XCTAssertEqual(presenterSpy.validationErrorMessage, Strings.PhoneNumber.phoneFieldError)
    }
    
    func testCheckPhoneNumber_WhenPhoneIsValid_ShouldEnableContinueButton() {
        sut.checkPhoneNumber("(85)999991122")
        
        XCTAssertEqual(presenterSpy.updatePhoneNumberFieldCallCount, 1)
        XCTAssertEqual(presenterSpy.shouldEnableContinueButtonCallCount, 1)
        XCTAssertNil(presenterSpy.validationErrorMessage)
        XCTAssertTrue(presenterSpy.isButtonEnabled)
    }
    
    func testValidateForm_WhenValidationSucceeds_ShouldCallNextStepWithReceivedStep() {
        serviceMock.validatePhoneNumberExpectedResult = .success(.init(nextStep: .code))
        sut.validateForm()
        
        XCTAssertEqual(presenterSpy.didNextStepCallCount, 1)
        XCTAssertEqual(presenterSpy.receivedStep, .nextStep(flow: .sms, step: .code))
    }
    
    func testValidateForm_WhenServerHasError_ShouldPresentsGenericError() {
        serviceMock.validatePhoneNumberExpectedResult = .failure(.serverError)
        sut.validateForm()

        XCTAssertEqual(presenterSpy.presentErrorAlertCallCount, 1)
        XCTAssertEqual(presenterSpy.presentedErrorTitle, Strings.Alerts.genericErrorTitle)
        XCTAssertEqual(presenterSpy.presentedErrorMessage, Strings.Alerts.genericErrorMessage)
    }

    func testValidateForm_WhenInputIsInvalid_ShouldPresentNotAuthorizedError() {
        var notAuthorizedError = RequestError()
        notAuthorizedError.title = "Dados inválidos"
        notAuthorizedError.message = "Verifique os dados informados."

        serviceMock.validatePhoneNumberExpectedResult = .failure(.unauthorized(body: notAuthorizedError))
        sut.validateForm()

        XCTAssertEqual(presenterSpy.presentErrorAlertCallCount, 1)
        XCTAssertEqual(presenterSpy.presentedErrorTitle, "Dados inválidos")
        XCTAssertEqual(presenterSpy.presentedErrorMessage, "Verifique os dados informados.")
    }

    func testValidateForm_WhenAttempLimitReached_ShouldPresentAttemptLimitError() {
        let attempLimitError = TFAHelperModels().attempLimitErrorJson()

        serviceMock.validatePhoneNumberExpectedResult = .failure(.tooManyRequests(body: attempLimitError))
        sut.validateForm()

        XCTAssertEqual(presenterSpy.presentAttempLimitErrorCallCount, 1)
        XCTAssertEqual(presenterSpy.presentedErrorTitle, "Limite atingido")
        XCTAssertEqual(
            presenterSpy.presentedErrorMessage, "Você excedeu o limite de tentativas. Aguarde 02:52 minutos para tentar novamente."
        )
        XCTAssertEqual(presenterSpy.attemptLimitErrorRetryTime, 10)
    }
    
    func testTimerTic_WhenCountdownTimeIsEqualOrLessThanZero_ShouldPresentFormValidationEnabled() {
        sut.startRetryValidationTimer(retryTime: -1)

        sut.timerTic()

        XCTAssertEqual(timerSpy.callStopCount, 1)
        XCTAssertEqual(presenterSpy.presentFormValidationEnabledCallCount, 1)
    }

    func testTimerTic_WhenCountdownTimeIsGreaterThanZero_ShouldPresentContinueButtonUpdatedWithRemainingTime() {
        sut.startRetryValidationTimer(retryTime: 10)

        sut.timerTic()

        XCTAssertEqual(presenterSpy.presentContinueButtonUpdatedCallCount, 1)
        XCTAssertEqual(presenterSpy.continueButtonRemainingTime, 9)
    }
}
