@testable import TFA
import XCTest

private final class DeviceAuthOnboardingCoordinatorSpy: DeviceAuthOnboardingCoordinating {
    var viewController: UIViewController?
    var delegate: DeviceAuthOnboardingCoordinatorDelegate?
    
    private(set) var performActionCallCount = 0
    private(set) var actionCalled: DeviceAuthOnboardingAction?
    
    func perform(action: DeviceAuthOnboardingAction) {
        performActionCallCount += 1
        actionCalled = action
    }
}

final class DeviceAuthOnboardingPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = DeviceAuthOnboardingCoordinatorSpy()
    
    private lazy var sut = DeviceAuthOnboardingPresenter(coordinator: coordinatorSpy)
    
    func testDidNextStep_WhenActionIsClose_ShouldCallCoordinatorWithCloseAction() {
        sut.didNextStep(action: .close)
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.actionCalled, .close)
    }
}
