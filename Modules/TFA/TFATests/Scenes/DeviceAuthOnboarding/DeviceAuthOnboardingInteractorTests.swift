import AnalyticsModule
@testable import TFA
import XCTest

private final class DeviceAuthOnboardingPresenterSpy: DeviceAuthOnboardingPresenting {
    private(set) var callDidNextStepCount = 0
    private(set) var actionCalled: DeviceAuthOnboardingAction?
    
    func didNextStep(action: DeviceAuthOnboardingAction) {
        callDidNextStepCount += 1
        actionCalled = action
    }
}

final class DeviceAuthOnboardingInteractorTests: XCTestCase {
    private let analytics = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(analytics)
    
    private lazy var presenterSpy = DeviceAuthOnboardingPresenterSpy()
    private lazy var sut = DeviceAuthOnboardingInteractor(presenter: presenterSpy, dependencies: dependencies)
    
    func testSendAnalytics_ShouldSendTheExpectedEvent() {
        sut.sendAnalytics()
        
        let expectedEvent = TFATracker(eventType: OnboardingEvent.started).event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testCloseOnboarding_WhenCalledFromViewController_ShouldCallCloseActionOnPresenter() {
        sut.closeOnboarding()
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.actionCalled, .close)
    }
    
    func testShowHelpInfo_WhenCalledFromViewController_ShouldCallShowHelpOnPresenter() {
        sut.showHelpInfo()
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.actionCalled, .showHelp)
    }
    
    func testContinueValidation_WhenCalledFromViewController_ShouldCallContinueOnPresenter() {
        sut.continueValidation()
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.actionCalled, .continueValidation)
    }
}
