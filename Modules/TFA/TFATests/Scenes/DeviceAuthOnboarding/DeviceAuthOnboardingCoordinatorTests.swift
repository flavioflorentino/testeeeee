@testable import TFA
import XCTest

private final class DeviceAuthOnboardingCoordinatorDelegateSpy: DeviceAuthOnboardingCoordinatorDelegate {
    private(set) var onboardingCloseCallCount = 0
    private(set) var onboardingShowHelpCallCount = 0
    private(set) var onboardingNextStepCallCount = 0
    
    func onboardingClose() {
        onboardingCloseCallCount += 1
    }
    
    func onboardingShowHelp() {
        onboardingShowHelpCallCount += 1
    }
    
    func onboardingNextStep() {
        onboardingNextStepCallCount += 1
    }
}

final class DeviceAuthOnboardingCoordinatorTests: XCTestCase {
    private let delegateSpy = DeviceAuthOnboardingCoordinatorDelegateSpy()
    
    private lazy var sut: DeviceAuthOnboardingCoordinator = {
        let coordinator = DeviceAuthOnboardingCoordinator()
        coordinator.delegate = delegateSpy
        return coordinator
    }()
 
    func testPerform_WhenActionIsClose_ShouldCallDelegateCloseMethod() {
        sut.perform(action: .close)
        
        XCTAssertEqual(delegateSpy.onboardingCloseCallCount, 1)
    }
    
    func testPerform_WhenActionIsShowHelp_ShouldCallDelegateShowHelpMethod() {
        sut.perform(action: .showHelp)
        
        XCTAssertEqual(delegateSpy.onboardingShowHelpCallCount, 1)
    }
    
    func testPerform_WhenActionIsContinueValidation_ShouldCallDelegateNextStepMethod() {
        sut.perform(action: .continueValidation)
        
        XCTAssertEqual(delegateSpy.onboardingNextStepCallCount, 1)
    }
}
