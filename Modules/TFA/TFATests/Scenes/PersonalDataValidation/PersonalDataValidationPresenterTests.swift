@testable import TFA
import XCTest

private final class PersonalDataValidationCoordinatorSpy: PersonalDataValidationCoordinating {
    var viewController: UIViewController?
    var delegate: DeviceAuthFlowStepping?
    
    private(set) var actionPerformed: PersonalDataValidationAction?
    private(set) var performActionCallCount = 0
    func perform(action: PersonalDataValidationAction) {
        performActionCallCount += 1
        actionPerformed = action
    }
}

private final class PersonalDataValidationViewControllerSpy: PersonalDataValidationDisplaying {
    private(set) var displayLoadingCallCount = 0
    private(set) var loadingValue: Bool?
    private(set) var displayEmailHintCallCount = 0
    private(set) var emailHint = ""
    private(set) var updateEmailFieldErrorMessageCallCount = 0
    private(set) var updateDocumentFieldErrorMessageCallCount = 0
    private(set) var updateBirthdateFieldErrorMessageCallCount = 0
    private(set) var updateMotherNameFieldErrorMessageCallCount = 0
    private(set) var fieldErrorMessage: String?
    private(set) var enableContinueButtonCallCount = 0
    private(set) var isContinueButtonEnabled = false
    private(set) var displayAttemptLimitErrorCallCount = 0
    private(set) var displayedErrorTitle = ""
    private(set) var displayedErrorMessage = ""
    private(set) var displayAlertCallCount = 0
    private(set) var retryTimeDisplayed = 0
    private(set) var displayCountdownTimerCallCount = 0
    private(set) var countdownRemainingTime = ""
    private(set) var enableFormValidationCallCount = 0
    private(set) var displayCancelAuthorizationAlertCallCount = 0
    
    func displayLoading(_ isLoading: Bool) {
        displayLoadingCallCount += 1
        loadingValue = isLoading
    }
    
    func displayEmailHint(_ hint: String) {
        displayEmailHintCallCount += 1
        emailHint = hint
    }
    
    func updateEmailFieldErrorMessage(_ message: String?) {
        fieldErrorMessage = message
        updateEmailFieldErrorMessageCallCount += 1
    }
    
    func updateDocumentFieldErrorMessage(_ message: String?) {
        fieldErrorMessage = message
        updateDocumentFieldErrorMessageCallCount += 1
    }
    
    func updateBirthdateFieldErrorMessage(_ message: String?) {
        fieldErrorMessage = message
        updateBirthdateFieldErrorMessageCallCount += 1
    }
    
    func updateMotherNameFieldErrorMessage(_ message: String?) {
        fieldErrorMessage = message
        updateMotherNameFieldErrorMessageCallCount += 1
    }
    
    func enableContinueButton(enable: Bool) {
        enableContinueButtonCallCount += 1
        isContinueButtonEnabled = enable
    }
    
    func displayAttempLimitError(with title: String, message: String, retryTime: Int) {
        displayAttemptLimitErrorCallCount += 1
        displayedErrorTitle = title
        displayedErrorMessage = message
        retryTimeDisplayed = retryTime
    }

    func displayAlert(title: String, message: String) {
        displayAlertCallCount += 1
        displayedErrorTitle = title
        displayedErrorMessage = message
    }
    
    func displayCountdownTimer(with remainingTime: String) {
        displayCountdownTimerCallCount += 1
        countdownRemainingTime = remainingTime
    }
    
    func enableFormValidation() {
        enableFormValidationCallCount += 1
    }
    
    func displayCancelAuthorizationAlert() {
        displayCancelAuthorizationAlertCallCount += 1
    }
}

final class PersonalDataValidationPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = PersonalDataValidationCoordinatorSpy()
    private lazy var viewControllerSpy = PersonalDataValidationViewControllerSpy()
    
    private lazy var sut: PersonalDataValidationPresenter = {
        let presenter = PersonalDataValidationPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testDidNextStep_WhenActionIsBack_ShouldCallCoordinatorWithBackAction() {
        sut.didNextStep(action: .back)
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.actionPerformed, .back)
    }
    
    func testPresentLoading_ShouldCallViewControllerDisplayLoading() {
        sut.presentLoading(true)
        
        XCTAssertEqual(viewControllerSpy.displayLoadingCallCount, 1)
        XCTAssertTrue(viewControllerSpy.loadingValue ?? false)
    }
    
    func testUpdateEmailHint_ShouldCallViewControllerDisplayEmailHint() {
        sut.updateEmailHint("***@mail.com")
        
        XCTAssertEqual(viewControllerSpy.displayEmailHintCallCount, 1)
        XCTAssertEqual(viewControllerSpy.emailHint, "***@mail.com")
    }
    
    func testUpdateEmailField_WhenErrorIsNil_ShouldNotDisplayErrorOnViewController() {
        sut.updateEmailField(withError: nil)
        
        XCTAssertEqual(viewControllerSpy.updateEmailFieldErrorMessageCallCount, 1)
        XCTAssertNil(viewControllerSpy.fieldErrorMessage)
    }

    func testUpdateDocumentField_WhenErrorIsNil_ShouldNotDisplayErrorOnViewController() {
        sut.updateDocumentField(withError: nil)
        
        XCTAssertEqual(viewControllerSpy.updateDocumentFieldErrorMessageCallCount, 1)
        XCTAssertNil(viewControllerSpy.fieldErrorMessage)
    }

    func testUpdateBirthdateField_WhenErrorIsNil_ShouldNotDisplayErrorOnViewController() {
        sut.updateBirthdateField(withError: nil)
        
        XCTAssertEqual(viewControllerSpy.updateBirthdateFieldErrorMessageCallCount, 1)
        XCTAssertNil(viewControllerSpy.fieldErrorMessage)
    }

    func testUpdateMotherNameField_WhenErrorIsNil_ShouldNotDisplayErrorOnViewController() {
        sut.updateMotherNameField(withError: nil)
        
        XCTAssertEqual(viewControllerSpy.updateMotherNameFieldErrorMessageCallCount, 1)
        XCTAssertNil(viewControllerSpy.fieldErrorMessage)
    }

    func testUpdateEmailField_WhenErrorIsNotNil_ShouldDisplayErrorOnViewController() {
        sut.updateEmailField(withError: Strings.PersonalData.invalidEmail)
        
        XCTAssertEqual(viewControllerSpy.updateEmailFieldErrorMessageCallCount, 1)
        XCTAssertEqual(viewControllerSpy.fieldErrorMessage, Strings.PersonalData.invalidEmail)
    }

    func testUpdateDocumentField_WhenErrorIsNotNil_ShouldDisplayErrorOnViewController() {
        sut.updateDocumentField(withError: Strings.PersonalData.invalidCPFDocument)
        
        XCTAssertEqual(viewControllerSpy.updateDocumentFieldErrorMessageCallCount, 1)
        XCTAssertEqual(viewControllerSpy.fieldErrorMessage, Strings.PersonalData.invalidCPFDocument)
    }

    func testUpdateBirthdateField_WhenErrorIsNotNil_ShouldDisplayErrorOnViewController() {
        sut.updateBirthdateField(withError: Strings.PersonalData.birthdateRequired)
        
        XCTAssertEqual(viewControllerSpy.updateBirthdateFieldErrorMessageCallCount, 1)
        XCTAssertEqual(viewControllerSpy.fieldErrorMessage, Strings.PersonalData.birthdateRequired)
    }

    func testUpdateMotherNameField_WhenErrorIsNotNil_ShouldDisplayErrorOnViewController() {
        sut.updateMotherNameField(withError: Strings.PersonalData.motherNameRequired)
        
        XCTAssertEqual(viewControllerSpy.updateMotherNameFieldErrorMessageCallCount, 1)
        XCTAssertEqual(viewControllerSpy.fieldErrorMessage, Strings.PersonalData.motherNameRequired)
    }
    
    func testShouldEnableContinueButton_WhenEnableIsTrue_ShouldDisplayButtonEnabled() {
        sut.shouldEnableContinueButton(true)
     
        XCTAssertEqual(viewControllerSpy.enableContinueButtonCallCount, 1)
        XCTAssertTrue(viewControllerSpy.isContinueButtonEnabled)
    }

    func testPresentErrorAlert_WhenCalledFromViewModel_ShouldDisplayErrorAlertOnViewController() {
        sut.presentErrorAlert(title: "Ops!", message: "Algo não está certo!?")

        XCTAssertEqual(viewControllerSpy.displayAlertCallCount, 1)
        XCTAssertEqual(viewControllerSpy.displayedErrorTitle, "Ops!")
        XCTAssertEqual(viewControllerSpy.displayedErrorMessage, "Algo não está certo!?")
    }

    func testPresentAttemptLimitError_WhenCalledFromViewModel_ShouldDisplayAttemptLimitError() {
        let error = TFAHelperModels().attempLimitErrorJson()
        sut.presentAttempLimitError(tfaError: error, retryTime: 180)
        
        XCTAssertEqual(viewControllerSpy.displayAttemptLimitErrorCallCount, 1)
        XCTAssertEqual(viewControllerSpy.displayedErrorTitle, "Limite atingido")
        XCTAssertEqual(viewControllerSpy.displayedErrorMessage, "Você excedeu o limite de tentativas. Aguarde 02:52 minutos para tentar novamente.")
        XCTAssertEqual(viewControllerSpy.retryTimeDisplayed, 180)
    }
    
    func testPresentContinueButtonUpdated_ShouldDisplayContinueButtonWithRemainingTime() {
        sut.presentContinueButtonUpdated(with: 100)
        
        XCTAssertEqual(viewControllerSpy.displayCountdownTimerCallCount, 1)
        XCTAssertEqual(viewControllerSpy.countdownRemainingTime, "Aguarde 01:40")
    }
    
    func testPresetGoBackAlert_ShouldDisplayAlertOnViewController() {
        sut.presentGoBackAlert()
        
        XCTAssertEqual(viewControllerSpy.displayCancelAuthorizationAlertCallCount, 1)
    }
    
    func testPresentFormValidationEnabled_ShouldEnableFormOnViewController() {
        sut.presentFormValidationEnabled()
        
        XCTAssertEqual(viewControllerSpy.enableFormValidationCallCount, 1)
    }
}
