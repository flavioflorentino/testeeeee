import Core
import Foundation
@testable import TFA
import XCTest

private final class PersonalDataValidationServiceMock: PersonalDataValidationServicing {
    var getPersonalDataExpectedResult: Result<TFAConsumerData, ApiError> = .failure(.serverError)
    var validatePersonalDataExpectedResult: Result<DeviceAuthValidationResponse, ApiError> = .failure(.serverError)
    
    func getPersonalData(requestData: DeviceAuthRequestData, completion: @escaping (Result<TFAConsumerData, ApiError>) -> Void) {
        completion(getPersonalDataExpectedResult)
    }

    func validatePersonalData(personalData: TFAPersonalDataInfo, requestData: DeviceAuthRequestData, completion: @escaping (Result<DeviceAuthValidationResponse, ApiError>) -> Void) {
        completion(validatePersonalDataExpectedResult)
    }
}

private final class PersonalDataValidationPresenterSpy: PersonalDataValidationPresenting {
    var viewController: PersonalDataValidationDisplaying?
    
    private(set) var nextStepReceived: PersonalDataValidationAction?
    private(set) var didNextStepCallCount = 0
    private(set) var presentLoadingCallCount = 0
    private(set) var updateEmailHintCallCount = 0
    private(set) var emailHint: String?
    private(set) var updateEmailFieldCallCount = 0
    private(set) var updateDocumentFieldCallCount = 0
    private(set) var updateBirthdateFieldCallCount = 0
    private(set) var updateMotherNameFieldCallCount = 0
    private(set) var fieldValidationErrorMessage: String?
    private(set) var shouldEnableContinueButtonCallCount = 0
    private(set) var isContinueButtonEnabled = false
    private(set) var presentedErrorTitle = ""
    private(set) var presentedErrorMessage = ""
    private(set) var presentErrorAlertCallCount = 0
    private(set) var presentAttemptLimitErrorCallCount = 0
    private(set) var attemptLimitError: RequestError?
    private(set) var attemptLimitErrorRetryTime = 0
    private(set) var presentContinueButtonUpdatedCallCount = 0
    private(set) var continueButtonremainingTime = 0
    private(set) var presentFormValidationEnabledCallCount = 0
    private(set) var presentGoBackAlertCallCount = 0
    
    func didNextStep(action: PersonalDataValidationAction) {
        didNextStepCallCount += 1
        nextStepReceived = action
    }
    
    func presentLoading(_ loading: Bool) {
        presentLoadingCallCount += 1
    }
    
    func updateEmailHint(_ hint: String) {
        updateEmailHintCallCount += 1
        emailHint = hint
    }
    
    func updateEmailField(withError message: String?) {
        updateEmailFieldCallCount += 1
        fieldValidationErrorMessage = message
    }
    
    func updateDocumentField(withError message: String?) {
        updateDocumentFieldCallCount += 1
        fieldValidationErrorMessage = message
    }
    
    func updateBirthdateField(withError message: String?) {
        updateBirthdateFieldCallCount += 1
        fieldValidationErrorMessage = message
    }
    
    func updateMotherNameField(withError message: String?) {
        updateMotherNameFieldCallCount += 1
        fieldValidationErrorMessage = message
    }
    
    func shouldEnableContinueButton(_ isEnabled: Bool) {
        shouldEnableContinueButtonCallCount += 1
        isContinueButtonEnabled = isEnabled
    }
    
    func presentAttempLimitError(tfaError: RequestError, retryTime: Int) {
        presentAttemptLimitErrorCallCount += 1
        attemptLimitError = tfaError
        attemptLimitErrorRetryTime = retryTime
    }
    
    func presentErrorAlert(title: String, message: String) {
        presentErrorAlertCallCount += 1
        presentedErrorTitle = title
        presentedErrorMessage = message
    }
    
    func presentContinueButtonUpdated(with remainingTime: Int) {
        presentContinueButtonUpdatedCallCount += 1
        continueButtonremainingTime = remainingTime
    }
    
    func presentFormValidationEnabled() {
        presentFormValidationEnabledCallCount += 1
    }
    
    func presentGoBackAlert() {
        presentGoBackAlertCallCount += 1
    }
}

final class PersonalDataValidationInteractorTests: XCTestCase {
    private lazy var serviceMock = PersonalDataValidationServiceMock()
    private lazy var presenterSpy = PersonalDataValidationPresenterSpy()
    private let timerSpy = TFATimerProtocolSpy()
    
    private lazy var sut = PersonalDataValidationInteractor(
        service: serviceMock,
        presenter: presenterSpy,
        deviceAuthRequestData: DeviceAuthRequestData(flow: .sms, step: .userData, tfaId: "abc"),
        timer: timerSpy
    )
    
    func testGoBack_ShouldCallPresenterWithGoBackAction() {
        sut.goBack()
        
        XCTAssertEqual(presenterSpy.didNextStepCallCount, 1)
        XCTAssertEqual(presenterSpy.nextStepReceived, .back)
    }
    
    func testBackButtonWasTapped_ShouldPresentGoBackWarning() {
        sut.backButtonWasTapped()
        
        XCTAssertEqual(presenterSpy.presentGoBackAlertCallCount, 1)
    }
    
    func testGetPersonalDataInfo_ShoulCallPresenterPresentLoadingTwice() {
        serviceMock.getPersonalDataExpectedResult = .success(.init(consumerData: .init(email: "***@mail.com")))
        sut.getPersonalDataInfo()
        
        XCTAssertEqual(presenterSpy.presentLoadingCallCount, 2)
    }
    
    func testEmailBeginEditing_WhenEmailIsNotNil_ShouldCallPresentUpdateEmailHint() {
        serviceMock.getPersonalDataExpectedResult = .success(.init(consumerData: .init(email: "***@mail.com")))
        sut.getPersonalDataInfo()
        sut.emailBeginEditing()
        
        XCTAssertEqual(presenterSpy.updateEmailHintCallCount, 1)
        XCTAssertNotNil(presenterSpy.emailHint)
        XCTAssertEqual(presenterSpy.emailHint, "***@mail.com")
    }
    
    func testEmailBeginEditing_WhenEmailIsNil_ShouldNotCallPresenterUpdateEmailHint() {
        sut.emailBeginEditing()
        
        XCTAssertEqual(presenterSpy.updateEmailHintCallCount, 0)
        XCTAssertNil(presenterSpy.emailHint)
    }
    
    func testCheckMotherName_WhenNameIsInvalid_ShouldPresentErrorMessage() {
        sut.checkMotherName("")
        
        XCTAssertEqual(presenterSpy.updateMotherNameFieldCallCount, 1)
        XCTAssertEqual(presenterSpy.fieldValidationErrorMessage, Strings.PersonalData.motherNameRequired)
    }
    
    func testCheckMotherName_WhenNameIsValid_ShouldNotPresentErrorMessage() {
        sut.checkMotherName("Maria")
        
        XCTAssertEqual(presenterSpy.updateMotherNameFieldCallCount, 1)
        XCTAssertNil(presenterSpy.fieldValidationErrorMessage)
    }
    
    func testCheckEmail_WhenEmailIsInvalid_ShoulPresentErrorMessage() {
        sut.checkEmail("jose@")
        
        XCTAssertEqual(presenterSpy.updateEmailFieldCallCount, 1)
        XCTAssertEqual(presenterSpy.fieldValidationErrorMessage, Strings.PersonalData.invalidEmail)
    }
    
    func testCheckEmail_WhenEmailIsValid_ShouldNotPresentErrorMessage() {
        sut.checkEmail("jose@mail.com")
        
        XCTAssertEqual(presenterSpy.updateEmailFieldCallCount, 1)
        XCTAssertEqual(presenterSpy.fieldValidationErrorMessage, nil)
    }
    
    func testCheckDocument_WhenDocumentIsInvalid_ShoulPresentErrorMessage() {
        sut.checkDocument("11111111111")
        
        XCTAssertEqual(presenterSpy.updateDocumentFieldCallCount, 1)
        XCTAssertEqual(presenterSpy.fieldValidationErrorMessage, Strings.PersonalData.invalidCPFDocument)
    }
    
    func testCheckDocument_WhenDocumentIsValid_ShouldNotPresentErrorMessage() {
        sut.checkDocument("199.213.820-63")
        
        XCTAssertEqual(presenterSpy.updateDocumentFieldCallCount, 1)
        XCTAssertEqual(presenterSpy.fieldValidationErrorMessage, nil)
    }
    
    func testCheckBirthday_WhenBirthdayIsEmpty_ShoulPresentRequiredErrorMessage() {
        sut.checkBirthday("")
        
        XCTAssertEqual(presenterSpy.updateBirthdateFieldCallCount, 1)
        XCTAssertEqual(presenterSpy.fieldValidationErrorMessage, Strings.PersonalData.birthdateRequired)
    }
    
    func testCheckBirthday_WhenBirthdayLenghtIsToSmall_ShoulPresentInvalidErrorMessage() {
        sut.checkBirthday("11/")
        
        XCTAssertEqual(presenterSpy.updateBirthdateFieldCallCount, 1)
        XCTAssertEqual(presenterSpy.fieldValidationErrorMessage, Strings.PersonalData.invalidBirthdate)
    }
    
    func testCheckBirthday_WhenBirthdayLenghtIsToLarge_ShoulPresentInvalidErrorMessage() {
        sut.checkBirthday("11/11/201121")
        
        XCTAssertEqual(presenterSpy.updateBirthdateFieldCallCount, 1)
        XCTAssertEqual(presenterSpy.fieldValidationErrorMessage, Strings.PersonalData.invalidBirthdate)
    }
    
    func testCheckBirthday_WhenBirthdayLenghtAndDateAreValid_ShoulNotPresentErrorMessage() {
        sut.checkBirthday("11/11/2000")
        
        XCTAssertEqual(presenterSpy.updateBirthdateFieldCallCount, 1)
        XCTAssertEqual(presenterSpy.fieldValidationErrorMessage, nil)
    }
    
    func testCheckBirthday_WhenBirthdayLenghtIsValidButDateIsInvalid_ShoulPresentErrorMessage() {
        sut.checkBirthday("33/15/2000")
        
        XCTAssertEqual(presenterSpy.updateBirthdateFieldCallCount, 1)
        XCTAssertEqual(presenterSpy.fieldValidationErrorMessage, Strings.PersonalData.invalidBirthdate)
    }
    
    func testCheckEmailField_WhenAtLeastOneFieldIsNotValid_ShouldNotEnableContinue() {
        sut.checkDocument("199.213.820-63")
        sut.checkBirthday("11/11/2000")
        sut.checkMotherName("")
        sut.checkEmail("jose@mail.com")
        
        XCTAssertEqual(presenterSpy.shouldEnableContinueButtonCallCount, 4)
        XCTAssertFalse(presenterSpy.isContinueButtonEnabled)
    }
    
    func testCheckEmailField_WhenAllFieldsAreValid_ShouldEnableContinue() {
        sut.checkDocument("199.213.820-63")
        sut.checkBirthday("11/11/2000")
        sut.checkMotherName("Maria")
        sut.checkEmail("jose@mail.com")
        
        XCTAssertEqual(presenterSpy.shouldEnableContinueButtonCallCount, 4)
        XCTAssertTrue(presenterSpy.isContinueButtonEnabled)
    }

    func testValidateForm_WhenServerHasError_ShouldPresentsGenericError() {
        serviceMock.validatePersonalDataExpectedResult = .failure(.serverError)
        sut.validateForm(document: "123.456.789.01", birthDate: "10/10/2000", motherName: "mae2", email: "email@mail.com")

        XCTAssertEqual(presenterSpy.presentErrorAlertCallCount, 1)
        XCTAssertEqual(presenterSpy.presentedErrorTitle, Strings.Alerts.genericErrorTitle)
        XCTAssertEqual(presenterSpy.presentedErrorMessage, Strings.Alerts.genericErrorMessage)
    }

    func testValidateForm_WhenInputIsInvalid_ShouldPresentNotAuthorizedError() {
        var notAuthorizedError = RequestError()
        notAuthorizedError.title = "Dados inválidos"
        notAuthorizedError.message = "Verifique os dados informados."

        serviceMock.validatePersonalDataExpectedResult = .failure(.unauthorized(body: notAuthorizedError))
        sut.validateForm(document: "123.456.789.01", birthDate: "10/10/2000", motherName: "mae2", email: "email@mail.com")

        XCTAssertEqual(presenterSpy.presentErrorAlertCallCount, 1)
        XCTAssertEqual(presenterSpy.presentedErrorTitle, "Dados inválidos")
        XCTAssertEqual(presenterSpy.presentedErrorMessage, "Verifique os dados informados.")
    }

    func testValidateForm_WhenAttempLimitReached_ShouldPresentAttemptLimitError() {
        let attempLimitError = TFAHelperModels().attempLimitErrorJson()

        serviceMock.validatePersonalDataExpectedResult = .failure(.tooManyRequests(body: attempLimitError))
        sut.validateForm(document: "123.456.789.01", birthDate: "10/10/2000", motherName: "mae2", email: "email@mail.com")

        XCTAssertNotNil(presenterSpy.attemptLimitError)
        XCTAssertEqual(presenterSpy.presentAttemptLimitErrorCallCount, 1)
        XCTAssertEqual(presenterSpy.attemptLimitError?.title, "Limite atingido")
        XCTAssertEqual(
            presenterSpy.attemptLimitError?.message,
            "Você excedeu o limite de tentativas. Aguarde 02:52 minutos para tentar novamente."
        )
        XCTAssertEqual(presenterSpy.attemptLimitError?.code, "form_limit")
        XCTAssertEqual(presenterSpy.attemptLimitErrorRetryTime, 10)
    }
    
    func testValidateForm_WhenInputIsValid_ShouldCallNextStepWithReceivedStep() {
        serviceMock.validatePersonalDataExpectedResult = .success(DeviceAuthValidationResponse(nextStep: .phoneNumber))
        
        sut.validateForm(document: "123.456.789-01", birthDate: "01/01/2001", motherName: "mae", email: "email")
        
        XCTAssertEqual(presenterSpy.didNextStepCallCount, 1)
        XCTAssertEqual(presenterSpy.nextStepReceived, .nextStep(flow: .sms, step: .phoneNumber))
    }
    
    func testTimerTic_WhenCountdownTimeIsEqualOrLessThanZero_ShouldPresentFormValidationEnabled() {
        sut.startRetryValidationTimer(retryTime: -1)

        sut.timerTic()

        XCTAssertEqual(timerSpy.callStopCount, 1)
        XCTAssertEqual(presenterSpy.presentFormValidationEnabledCallCount, 1)
    }

    func testTimerTic_WhenCountdownTimeIsGreaterThanZeroShouldPresentContinueButtonUpdatedWithRemainingTime() {
        sut.startRetryValidationTimer(retryTime: 10)

        sut.timerTic()

        XCTAssertEqual(presenterSpy.presentContinueButtonUpdatedCallCount, 1)
        XCTAssertEqual(presenterSpy.continueButtonremainingTime, 9)
    }
}
