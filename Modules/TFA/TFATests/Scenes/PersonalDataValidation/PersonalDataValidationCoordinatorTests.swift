import UI
@testable import TFA
import XCTest

private final class PersonalDataValidationCoordinatorDelegateSpy: DeviceAuthFlowStepping {
    private(set) var validationDidGoBackCallCount = 0
    private(set) var receivedNextStepCallCount = 0
    private(set) var validationFlow: DeviceAuthFlow?
    private(set) var validationStep: DeviceAuthStep?
    
    func validationDidGoBack() {
        validationDidGoBackCallCount += 1
    }
    
    func receivedNextStep(forFlow flow: DeviceAuthFlow, step: DeviceAuthStep) {
        receivedNextStepCallCount += 1
        validationFlow = flow
        validationStep = step
    }
}

final class PersonalDataValidationCoordinatorTests: XCTestCase {
    private let delegateSpy = PersonalDataValidationCoordinatorDelegateSpy()
    
    private lazy var sut: PersonalDataValidationCoordinator = {
        let coordinator = PersonalDataValidationCoordinator()
        coordinator.delegate = delegateSpy
        return coordinator
    }()
    
    func testPerform_WhenActionIsBack_ShouldCallDelegateValidationDidGoBack() {
        sut.perform(action: .back)
        
        XCTAssertEqual(delegateSpy.validationDidGoBackCallCount, 1)
    }
    
    func testPerform_WhenActionIsNextStep_ShouldCallDelegateDidValidatePersonalData() {
        sut.perform(action: .nextStep(flow: .sms, step: .phoneNumber))
        
        XCTAssertEqual(delegateSpy.receivedNextStepCallCount, 1)
        XCTAssertEqual(delegateSpy.validationFlow, .sms)
        XCTAssertEqual(delegateSpy.validationStep, .phoneNumber)
    }
}
