import Core
@testable import TFA
import XCTest

private final class DeviceAuthOptionsCoordinatorSpy: DeviceAuthOptionsCoordinating {
    var viewController: UIViewController?
    var delegate: DeviceAuthOptionsCoordinatorDelegate?
    private(set) var callPerformActionCount = 0
    private(set) var actionPerformed: DeviceAuthOptionsAction?
    
    func perform(action: DeviceAuthOptionsAction) {
        callPerformActionCount += 1
        actionPerformed = action
    }
}

private final class DeviceAuthOptionsViewControllerSpy: DeviceAuthOptionsDisplaying {
    private(set) var callDisplayOptionsCount = 0
    private(set) var displayedOptions: [DeviceAuthOption] = []
    private(set) var callDisplayLoadingCount = 0
    private(set) var callDisplayRetryLoadErrorCount = 0
    private(set) var callDisplayErrorCount = 0
    private(set) var callDeviceAuthBlockedErrorCount = 0
    private(set) var isLoading = false
    private(set) var blockedErrorTitle = ""
    private(set) var blockedErrorMessage = ""
    private(set) var errorTitle = ""
    private(set) var errorMessage = ""
    
    
    func displayOptions(_ options: [DeviceAuthOption]) {
        callDisplayOptionsCount += 1
        displayedOptions = options
    }
    
    func displayLoading(_ loading: Bool) {
        callDisplayLoadingCount += 1
        isLoading = loading
    }
    
    func displayRetryLoadError() {
        callDisplayRetryLoadErrorCount += 1
    }
    
    func displayDeviceAuthBlockedError(with title: String, message: String) {
        callDeviceAuthBlockedErrorCount += 1
        blockedErrorTitle = title
        blockedErrorMessage = message
    }
    
    func displayError(with title: String, message: String) {
        callDisplayErrorCount += 1
        errorTitle = title
        errorMessage = message
    }
}

final class DeviceAuthOptionsPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = DeviceAuthOptionsCoordinatorSpy()
    private lazy var viewControllerSpy = DeviceAuthOptionsViewControllerSpy()
    
    private lazy var sut: DeviceAuthOptionsPresenter = {
        let presenter = DeviceAuthOptionsPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func test_WhenCalledFromInteractor_ShouldDisplayTFAOptionsOnViewController() {
        let options = [
            DeviceAuthOption(flow: .sms, firstStep: .userData, iconUrl: "", title: "SMS"),
            DeviceAuthOption(flow: .email, firstStep: .userData, iconUrl: "", title: "Email"),
            DeviceAuthOption(flow: .creditCard, firstStep: .creditCard, iconUrl: "", title: "Cartão")
        ]
        sut.presentOptions(options)
        
        XCTAssertEqual(viewControllerSpy.callDisplayOptionsCount, 1)
        XCTAssertEqual(viewControllerSpy.displayedOptions, options)
    }

    func testPresentLoading_ShouldDisplayLoadingOnViewController() {
        sut.presentLoading(true)

        XCTAssertEqual(viewControllerSpy.callDisplayLoadingCount, 1)
        XCTAssertTrue(viewControllerSpy.isLoading)
    }

    func testPresentRetryLoadError_WhenReceiveRequestError_ShouldDisplayRetryLoadErrorAlert() {
        sut.presentRetryLoadError()

        XCTAssertEqual(viewControllerSpy.callDisplayRetryLoadErrorCount, 1)
    }

    func testPresentNextStepFor_WhenCalledFromInteractor_ShouldCallCoordinatorNextStep() {
        let option = DeviceAuthOption(flow: .sms, firstStep: .userData, iconUrl: "", title: "SMS")
        sut.presentNextStepFor(option)

        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        XCTAssertEqual(coordinatorSpy.actionPerformed, .selectedOption(option: option))
    }

    func testPresentSelfieValidation_WhenOptionIsSelfie_ShouldPerformActionOnCoordinator() {
        let selfieData = DeviceAuthSelfieFlowResponse.Selfie(token: "abc", flow: "TFA")
        let selfieValidationData = DeviceAuthSelfieFlowResponse(selfie: selfieData)
        let authOption = DeviceAuthOption(flow: .validationID, firstStep: .selfie, iconUrl: "", title: "Biometria")
        
        sut.presentSelfieValidation(with: selfieValidationData, andOption: authOption)
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        XCTAssertEqual(coordinatorSpy.actionPerformed, .validationId(validationData: selfieValidationData, option: authOption))
    }

    func testPresentSelfieValidationError_WhenCalledFromInteractor_ShouldPresentErrorOnViewController() {
        var genericError = RequestError()
        genericError.title = "Ops!"
        genericError.message = "Tivemos um problema com a solicitação, tente novamente."
        
        sut.presentSelfieValidationError(error: genericError)

        XCTAssertEqual(viewControllerSpy.callDisplayErrorCount, 1)
        XCTAssertEqual(viewControllerSpy.errorTitle, genericError.title)
        XCTAssertEqual(viewControllerSpy.errorMessage, genericError.message)
    }

    func testPresentTFABlockedError_WhenCalledFromInteractor_ShouldDisplayErrorOnViewController() {
        var blockedError = RequestError()
        blockedError.title = "Bloqueio por fraude"
        blockedError.code = "tfa_blocked_code"
        blockedError.message = "Aguarde 3 dias para tentar novamente."
        
        sut.presentTFABlockedError(blockedError)

        XCTAssertEqual(viewControllerSpy.callDeviceAuthBlockedErrorCount, 1)
        XCTAssertEqual(viewControllerSpy.blockedErrorTitle, blockedError.title)
        XCTAssertEqual(viewControllerSpy.blockedErrorMessage, blockedError.message)
    }

    func testGoBack_WhenCalledFromInteractor_ShouldCallCoordinatorBackAction() {
        sut.goBack()
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        XCTAssertEqual(coordinatorSpy.actionPerformed, .back)
    }
    
    func testCloseDeviceAuth_WhenCalledFromInteractor_ShouldCallCoordinatorCloseAction() {
        sut.closeDeviceAuth()
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        XCTAssertEqual(coordinatorSpy.actionPerformed, .close)
    }
}
