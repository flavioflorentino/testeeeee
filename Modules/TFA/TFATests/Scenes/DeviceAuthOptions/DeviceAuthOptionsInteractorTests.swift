import AnalyticsModule
import Core
import FeatureFlag
import Foundation
import SecurityModule
@testable import TFA
import XCTest

private final class LogDetectionServiceSpy: LogDetectionServicing {
    private (set) var sendLogCallCount = 0
    
    func sendLog(log: LogInfoContract) {
        sendLogCallCount += 1
    }
}

private final class DeviceAuthOptionsServiceMock: DeviceAuthOptionsServicing {
    var tfaOptionsExpectedResult: Result<DeviceAuthOptionsResponse, ApiError>?
    var startSelfieValidationExpectedResult: Result<DeviceAuthSelfieFlowResponse, ApiError>?
    
    func authOptions(
        with data: DeviceAuthRequestData,
        completion: @escaping (Result<DeviceAuthOptionsResponse, ApiError>) -> Void
    ) {
        guard let expectedResult = tfaOptionsExpectedResult else {
            return
        }
        completion(expectedResult)
    }
    
    func startSelfieValidation(
        with data: DeviceAuthRequestData,
        completion: @escaping (Result<DeviceAuthSelfieFlowResponse, ApiError>) -> Void
    ) {
        guard let expectedResult = startSelfieValidationExpectedResult else {
            return
        }
        
        completion(expectedResult)
    }
}

private final class DeviceAuthOptionsPresenterSpy: DeviceAuthOptionsPresenting {
    var viewController: DeviceAuthOptionsDisplaying?
    private(set) var presentedOptions: [DeviceAuthOption] = []
    private(set) var callPresentOptionsCount = 0
    private(set) var callPresentLoadingCount = 0
    private(set) var callPresentRetryLoadErrorCount = 0
    private(set) var callPresentBlockedErrorCount = 0
    private(set) var callPresentNextStepCount = 0
    private(set) var optionSelected: DeviceAuthOption?
    private(set) var callGoBackCount = 0
    private(set) var callCloseDeviceAuthCount = 0
    private(set) var callPresentSelfieValidationCount = 0
    private(set) var selfieValidationData: DeviceAuthSelfieFlowResponse?
    private(set) var selfieOptionData: DeviceAuthOption?
    private(set) var callPresentSelfieValidationErrorCount = 0
    private(set) var isLoading = false
    
    func presentLoading(_ loading: Bool) {
        callPresentLoadingCount += 1
        isLoading = loading
    }
    
    func presentOptions(_ options: [DeviceAuthOption]) {
        callPresentOptionsCount += 1
        presentedOptions = options
    }
    
    func presentNextStepFor(_ option: DeviceAuthOption) {
        callPresentNextStepCount += 1
        optionSelected = option
    }
    
    func presentSelfieValidation(with validationData: DeviceAuthSelfieFlowResponse, andOption option: DeviceAuthOption) {
        callPresentSelfieValidationCount += 1
        selfieValidationData = validationData
        selfieOptionData = option
    }
    
    func presentRetryLoadError() {
        callPresentRetryLoadErrorCount += 1
    }
    
    func presentSelfieValidationError(error: RequestError) {
        callPresentSelfieValidationErrorCount += 1
    }
    
    func presentTFABlockedError(_ error: RequestError) {
        callPresentBlockedErrorCount += 1
    }
    
    func goBack() {
        callGoBackCount += 1
    }
    
    func closeDeviceAuth() {
        callCloseDeviceAuthCount += 1
    }
}

final class DeviceAuthOptionsInteractorTests: XCTestCase {
    private lazy var serviceMock = DeviceAuthOptionsServiceMock()
    private lazy var presenterSpy = DeviceAuthOptionsPresenterSpy()
    private lazy var logDetectionServiceSpy = LogDetectionServiceSpy()
    
    private lazy var featureManagerMock = FeatureManagerMock()
    private lazy var analyticsMock = AnalyticsSpy()
    private lazy var mockDependencies = DependencyContainerMock(featureManagerMock, analyticsMock)
    
    private lazy var sut: DeviceAuthOptionsInteractor = {
        let requestData = DeviceAuthRequestData(flow: .sms, step: .initial, tfaId: "1")
        let interactor = DeviceAuthOptionsInteractor(
            service: serviceMock,
            presenter: presenterSpy,
            dependencies: mockDependencies,
            requestData: requestData,
            logDetectionService: logDetectionServiceSpy
        )
        
        return interactor
    }()
    
    func testLoadinOptions_WhenCalledFromViewController_ShouldPresentLoading() {
        serviceMock.tfaOptionsExpectedResult = mockedOptionsSuccess
        featureManagerMock.override(key: .featureTfaCreditcardFlowActive, with: true)
        
        sut.loadOptions()
        XCTAssertEqual(presenterSpy.callPresentLoadingCount, 2)
        XCTAssertFalse(presenterSpy.isLoading)
    }
    
    func testLoadOptions_WhenCreditCardFlowIsActiveAndServiceIsSuccess_ShouldPresentCreditCardOption() {
        serviceMock.tfaOptionsExpectedResult = mockedOptionsSuccess
        featureManagerMock.override(key: .featureTfaCreditcardFlowActive, with: true)
        
        sut.loadOptions()
        
        XCTAssertEqual(presenterSpy.callPresentOptionsCount, 1)
        XCTAssertTrue(presenterSpy.presentedOptions.contains(where: { $0.flow == .creditCard }))
    }
    
    func testLoadOptions_WhenCreditCardFlagIsInactiveAndServiceIsSuccess_ShouldNotPresentCreditCardOption() {
        featureManagerMock.override(key: .featureTfaCreditcardFlowActive, with: false)
        serviceMock.tfaOptionsExpectedResult = mockedOptionsSuccess
        
        sut.loadOptions()
        XCTAssertEqual(presenterSpy.callPresentOptionsCount, 1)
        XCTAssertFalse(presenterSpy.presentedOptions.contains(where: { $0.flow == .creditCard }))
    }
    
    func testLoadOptions_WhenServiceIsSuccess_ShouldSendAuthorizationMethodsViewEvent() {
        serviceMock.tfaOptionsExpectedResult = mockedOptionsSuccess
        featureManagerMock.override(key: .featureTfaCreditcardFlowActive, with: false)
        sut.loadOptions()
        
        let expectedEvent = AuthOptionsTracker(
            eventType: AuthOptionsEvent.authorizationMethodsViewed,
            authFlowOptions: presenterSpy.presentedOptions.map({ $0.flow })
        ).event()
        
        XCTAssertTrue(analyticsMock.equals(to: expectedEvent))
    }
    
    func testLoadOptions_WhenServiceFailsWithGenericError_ShouldPresentRetryLoadError() {
        serviceMock.tfaOptionsExpectedResult = .failure(.serverError)
        sut.loadOptions()
        
        XCTAssertEqual(presenterSpy.callPresentRetryLoadErrorCount, 1)
    }
    
    func testLoadOptions_WhenServiceFailsWithRequestError_ShouldPresentRetryLoadError() {
        serviceMock.tfaOptionsExpectedResult = .failure(.unauthorized(body: RequestError()))
        sut.loadOptions()
        
        XCTAssertEqual(presenterSpy.callPresentRetryLoadErrorCount, 1)
    }
    
    func testLoadOptions_WhenTFAIsBlocked_ShouldPresentTFABlockedError() throws {
        var blockedError = RequestError()
        blockedError.title = "Bloqueio por fraude"
        blockedError.code = "tfa_blocked_code"
        blockedError.message = "Aguarde 3 dias para tentar novamente."
        
        serviceMock.tfaOptionsExpectedResult = .failure(.unauthorized(body: blockedError))
        sut.loadOptions()
        
        XCTAssertEqual(presenterSpy.callPresentBlockedErrorCount, 1)
    }
    
    func testSelectItem_WhenCalledFromViewModel_ShouldPresentsNextStep() {
        featureManagerMock.override(key: .featureTfaCreditcardFlowActive, with: true)
        serviceMock.tfaOptionsExpectedResult = mockedOptionsSuccess
        
        sut.loadOptions()
        sut.selectedItem(IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(presenterSpy.callPresentNextStepCount, 1)
        XCTAssertEqual(presenterSpy.optionSelected?.flow, .authorizedDevice)
    }
    
    func testBack_WhenCalledFromViewController_ShouldCallPresenterGoBack() {
        sut.back()
        
        XCTAssertEqual(presenterSpy.callGoBackCount, 1)
    }
    
    func testClose_WhenCalledFromViewController_ShouldCallPresenterCloseDeviceAuth() {
        sut.close()
        
        XCTAssertEqual(presenterSpy.callCloseDeviceAuthCount, 1)
    }
    
    func testSelectItem_WhenCalledFromViewController_ShouldLogAuthorizationMethodChosenEvent() {
        featureManagerMock.override(key: .featureTfaCreditcardFlowActive, with: true)
        serviceMock.tfaOptionsExpectedResult = mockedOptionsSuccess
        
        sut.loadOptions()
        
        let expectedEvent = AuthOptionsTracker(
            eventType: AuthOptionsEvent.authorizationMethodChosen,
            authFlow: .authorizedDevice).event()
        
        sut.selectedItem(IndexPath(row: 0, section: 0))
        XCTAssertTrue(analyticsMock.equals(to: expectedEvent))
    }
    
    func testSelectSelfieValidationItem_WhenServiceIsSuccess_ShouldReceiveToken() {
        let validationResponse = DeviceAuthSelfieFlowResponse(
            selfie: DeviceAuthSelfieFlowResponse.Selfie(token: "token", flow: "TFA")
        )
        
        let expectedOption = DeviceAuthOption(flow: .validationID, firstStep: .selfie, iconUrl: "", title: "Validar Biometria")
        trySelfieValidation(with: .success(validationResponse))
        
        
        XCTAssertEqual(presenterSpy.callPresentSelfieValidationCount, 1)
        XCTAssertNotNil(presenterSpy.selfieValidationData)
        XCTAssertEqual(presenterSpy.selfieValidationData?.selfie.token, "token")
        XCTAssertEqual(presenterSpy.selfieOptionData, expectedOption)
    }
    
    func testSelectSelfieValidationItem_WhenServiceHasGenericFailure_ShouldPresentError() {
        trySelfieValidation(with: .failure(.serverError))
        
        XCTAssertEqual(presenterSpy.callPresentSelfieValidationErrorCount, 1)
        XCTAssertNil(presenterSpy.selfieValidationData)
        XCTAssertEqual(logDetectionServiceSpy.sendLogCallCount, 1)
    }
    
    func testSelectSelfieValidationItem_WhenServiceFailsWithRequestError_ShouldPresentError() {
        trySelfieValidation(with: .failure(.unauthorized(body: RequestError())))
        
        XCTAssertEqual(presenterSpy.callPresentSelfieValidationErrorCount, 1)
        XCTAssertNil(presenterSpy.selfieValidationData)
        XCTAssertEqual(logDetectionServiceSpy.sendLogCallCount, 1)
    }
}

private extension DeviceAuthOptionsInteractorTests {
    var mockedOptionsSuccess: Result<DeviceAuthOptionsResponse, ApiError> {
        .success(
            DeviceAuthOptionsResponse(options: [
                DeviceAuthOption(flow: .authorizedDevice, firstStep: .code, iconUrl: "", title: "Aparelho antigo"),
                DeviceAuthOption( flow: .creditCard, firstStep: .creditCard, iconUrl: "", title: "Cartão de Crédito"),
                DeviceAuthOption( flow: .sms, firstStep: .userData, iconUrl: "", title: "SMS"),
                DeviceAuthOption( flow: .email, firstStep: .userData, iconUrl: "", title: "Email")
            ])
        )
    }
    
    func trySelfieValidation(with selfieValidationResponse: Result<DeviceAuthSelfieFlowResponse, ApiError>?) {
        featureManagerMock.override(key: .featureTfaCreditcardFlowActive, with: false)
        
        serviceMock.tfaOptionsExpectedResult = .success(
            DeviceAuthOptionsResponse(options: [
                DeviceAuthOption(flow: .validationID, firstStep: .selfie, iconUrl: "", title: "Validar Biometria")
            ])
        )
        
        serviceMock.startSelfieValidationExpectedResult = selfieValidationResponse
        
        sut.loadOptions()
        sut.selectedItem(IndexPath(row: 0, section: 0))
    }
}
