@testable import TFA
import XCTest

private final class DeviceAuthOptionsCoordinatorDelegateSpy: DeviceAuthOptionsCoordinatorDelegate {
    private(set) var callDidGoBackCount = 0
    private(set) var callDidCloseFlowCount = 0
    private(set) var callDidSelectOptionCount = 0
    private(set) var callDidSelectValidationIdCount = 0
    private(set) var validationIdData: DeviceAuthSelfieFlowResponse?
    private(set) var optionData: DeviceAuthOption?
    
    func didSelectOption(option: DeviceAuthOption) {
        callDidSelectOptionCount += 1
        optionData = option
    }
    
    func didSelectValidationId(validationData: DeviceAuthSelfieFlowResponse, option: DeviceAuthOption) {
        callDidSelectValidationIdCount += 1
        validationIdData = validationData
        optionData = option
    }
    
    func optionsDidCloseFlow() {
        callDidCloseFlowCount += 1
    }
    
    func optionsDidGoBack() {
        callDidGoBackCount += 1
    }
}

final class DeviceAuthOptionsCoordinatorTests: XCTestCase {
    private let delegateSpy = DeviceAuthOptionsCoordinatorDelegateSpy()
    
    private lazy var sut: DeviceAuthOptionsCoordinator = {
        let coordinator = DeviceAuthOptionsCoordinator()
        coordinator.delegate = delegateSpy
        return coordinator
    }()
    
    func testPerform_WhenActionIsBack_ShouldCallDelegateDidGoBack() {
        sut.perform(action: .back)
        
        XCTAssertEqual(delegateSpy.callDidGoBackCount, 1)
    }
    
    func testPerform_WhenActionIsClose_ShouldCallDelegateDidCloseFlow() {
        sut.perform(action: .close)
        
        XCTAssertEqual(delegateSpy.callDidCloseFlowCount, 1)
    }
    
    func testPerform_WhenActionIsSelectedOption_ShouldCallDelegateDidSelectOption() {
        let authOption = DeviceAuthOption(flow: .sms, firstStep: .userData, iconUrl: "", title: "")
        sut.perform(action: .selectedOption(option: authOption))
        
        XCTAssertEqual(delegateSpy.callDidSelectOptionCount, 1)
        XCTAssertEqual(delegateSpy.optionData, authOption)
    }
    
    func testPerform_WhenActionIsValidationId_ShouldCallDelegateDidSelectValidationId() {
        let selfieData = DeviceAuthSelfieFlowResponse.Selfie(token: "def", flow: "TFA_FLOW")
        let selfieValidationData = DeviceAuthSelfieFlowResponse(selfie: selfieData)
        let authOption = DeviceAuthOption(flow: .validationID, firstStep: .selfie, iconUrl: "", title: "Biometria Facial")
        
        sut.perform(action: .validationId(validationData: selfieValidationData, option: authOption))
        
        XCTAssertEqual(delegateSpy.callDidSelectValidationIdCount, 1)
        XCTAssertEqual(delegateSpy.validationIdData?.selfie, selfieData)
        XCTAssertEqual(delegateSpy.optionData, authOption)
    }
}
