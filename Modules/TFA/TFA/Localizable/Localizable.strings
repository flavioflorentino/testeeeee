// General
"General.confirm" = "Confirmar";
"General.tryAgain" = "Tentar novamente";
"General.back" = "Voltar";
"General.understood" = "OK, entendi";
"General.continue" = "Continuar";
"General.wait" = "Aguarde %@";

//Info
"Info.header" = "Por que preciso fazer isso?";
"Info.body" = "Autorizar o aparelho é muito importante pra sua segurança. Toda vez que você entrar na sua conta PicPay usando outro aparelho celular, pediremos que você confirme algumas informações.\n\nPara entrar na sua conta, autorize seu aparelho agora.";

//Onboarding
"Onboarding.header" = "Autorize seu aparelho";
"Onboarding.body" = "Você está tentando acessar sua conta num novo aparelho. Pra sua segurança, autorize o novo acesso pra aproveitar o PicPay.";
"Onboarding.infoButtonTitle" = "Por que preciso fazer isso?";

//TFA Options
"Options.header" = "Autorização de acesso";
"Options.body" = "Escolha uma das opções abaixo para autorizar seu novo aparelho.";

//Phone Verification
"PhoneNumber.header" = "Seu celular";
"PhoneNumber.body" = "Informe o número completo do celular que você usou para criar sua conta no PicPay: ";
"PhoneNumber.phoneFieldPlaceholder" = "Número do celular";
"PhoneNumber.phoneNumberMask" = "(00) 00000-0000";
"PhoneNumber.phoneFieldError" = "Número de telefone obrigatório";

//Alerts
"Alerts.genericErrorTitle" = "Ops!";
"Alerts.genericErrorMessage" = "Ocorreu um erro, tente novamente";
"Alerts.optionsListNotLoadedAlertTitle" = "Não conseguimos carregar as opções";
"Alerts.optionsListNotLoadedAlertMessage" = "Ocorreu um erro ao tentar carregar as opções para você autorizar seu aparelho.";
"Alerts.anotherAuthorizationWay" = "Autorizar de outra maneira";
"Alerts.cardListNotLoadedAlertTitle" = "Não conseguimos carregar os seus cartões";
"Alerts.cardListNotLoadedAlertMessage" = "Não foi possível verificar os cartões de crédito da sua conta no PicPay.";

//Alerts.CancelAuthorization
"Alerts.CancelAuthorization.continue" = "Continuar autorização";
"Alerts.CancelAuthorization.title" = "Tem certeza que quer cancelar a autorização?";
"Alerts.CancelAuthorization.message" = "Ao voltar, você precisará preencher de novo todas as informações para autorizar seu aparelho.";
"Alerts.CancelAuthorization.cancel" = "Quero cancelar";

//TFACode
"Code.header" = "Digite o código de autorização";
"Code.titleTextForEmail" = "Digite o código que enviamos para seu email";
"Code.resendButtonText" = "Reenviar código";
"Code.oldDeviceNotificationSent" = "Enviamos uma notificação para o seu aparelho antigo. Insira o código recebido via PicPay no campo abaixo:";
"Code.smsNotificationSent" = "Enviamos um SMS com um código de autorização para o seu número cadastrado. Insira o código no campo abaixo para autorizar o acesso desse dispositivo.";
"Code.smsNotificationWithPhoneMask" = "Enviamos um SMS para o seu número %@ com um código de autorização. Insira o código no campo abaixo para autorizar o acesso desse dispositivo.";
"Code.emailNotificationSent" = "Enviamos um código de autorização para o e-mail informado. Insira o código no campo abaixo para autorizar o acesso desse dispositivo.";
"Code.emailNotificationWithEmailMask" = "Enviamos um código de autorização para o %@. Insira o código no campo abaixo para autorizar o acesso desse dispositivo.";
"Code.didNotReceived" = "Não recebeu? Aguarde %@s";
"Code.authorizationCodePlaceholder" = "Código de autorização";

//TFA Success
"Success.header" = "Uhul! Acesso autorizado";
"Success.body" = "Agora você pode usar o PicPay nesse aparelho com segurança. Aproveita!";
"Success.enter" = "Entrar";

//TFACreditCard
"CreditCard.header" = "Confirme os dados do seu cartão";
"CreditCard.body" = "Escolha um cartão para continuar a autenticação";
"CreditCard.optionTitle" = "Cartão com final ";

//TFACreditCardValidate
"CardValidate.header" = "Confirme os dados do seu cartão";
"CardValidate.cardNumberPlaceholder" = "Número do cartão";
"CardValidate.dueDatePlaceholder" = "Data de validade";
"CardValidate.cardNumberFieldMask" = "0000 0000 0000 0000 0000";
"CardValidate.dueDateFieldMask" = "00/00";
"CardValidate.cardNumberRequiredError" = "Campo obrigátorio";
"CardValidate.invalidDueDateError" = "Dados inválidos";

//TFAStatus
"Status.approvedTitle" = "Tudo certo!";
"Status.deniedTitle" = "Não foi possível autorizar o dispositivo";
"Status.inconclusiveTitle" = "Analisando seus dados";
"Status.waitingAnalysisTitle" = "Tente novamente mais tarde.";
"Status.approvedDescription" = "Seu novo aparelho foi autorizado. Agora você pode usar o PicPay nesse dispositivo com segurança.";
"Status.deniedDescription" = "Você pode tentar novamente daqui 7 dias.";
"Status.inconclusiveDescription" = "Não se preocupe, já recebemos suas informações. Assim que nossa equipe conferir a foto, você será avisado por e-mail e poderá acessar sua conta usando este aparelho.";
"Status.waitingAnalysisDescription" = "Não se preocupe, já recebemos seus dados, mas não concluímos a operação. Tente depois.";
"Status.understood" = "Ok, entendi";
"Status.optionsList" = "Listagem de opções";

//TFABankAccountValidate
"BankAccount.header" = "Confirme os dados da sua conta bancária";
"BankAccount.agencyPlaceholder" = "Agência";
"BankAccount.accountPlaceholder" = "Número da conta";
"BankAccount.digitPlaceholder" = "Dígito";
"BankAccount.alertTitle" = "Sua segurança é muito importante pra gente";
"BankAccount.alertMessage" = "Pedimos a confirmação da conta bancária pra ter certeza de que é você que está solicitando um novo acesso.";
"BankAccount.alertActionText" = "Ok, valeu";
"BankAccount.registeredBank" = "Banco cadastrado";
"BankAccount.agencyRequired" = "Agência obrigatório";
"BankAccount.accountNumberRequired" = "Número da conta obrigatório";
"BankAccount.digitRequired" = "Dígito obrigatório";

//TFAPersonalDataCheck
"PersonalData.header" = "Seus dados";
"PersonalData.body" = "Para continuar, confirme algumas informações sobre você.";
"PersonalData.cpfPlaceholder" = "Seu CPF";
"PersonalData.birthdatePlaceholder" = "Data de nascimento";
"PersonalData.motherNamePlaceholder" = "Nome completo da sua mãe";
"PersonalData.emailPlaceholder" = "Email cadastrado no PicPay";
"PersonalData.cpfDocumentMask" = "000.000.000-00";
"PersonalData.birthDateMask" = "00/00/0000";
"PersonalData.verifyInfo" = "Revise as informações.";
"PersonalData.birthdateRequired" = "Data de Nascimento obrigatória";
"PersonalData.invalidBirthdate" = "Data de Nascimento inválida";
"PersonalData.invalidCPFDocument" = "CPF inválido";
"PersonalData.invalidEmail" = "Email inválido";
"PersonalData.motherNameRequired" = "Nome da mãe obrigatório";
