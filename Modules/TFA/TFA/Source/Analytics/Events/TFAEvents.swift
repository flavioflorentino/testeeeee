import Foundation

enum OnboardingEvent: String, TFAEvent {
    case started = "Started"
}

enum InfoEvent: String, TFAEvent {
    case helpAccessed = "Help Accessed"
}

enum AuthOptionsEvent: String, TFAEvent {
    case authorizationMethodsViewed = "Authorization Methods Viewed"
    case authorizationMethodChosen = "Authorization Method Chosen"
}
