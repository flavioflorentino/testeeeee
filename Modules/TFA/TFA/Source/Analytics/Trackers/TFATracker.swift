import AnalyticsModule
import Foundation

protocol TFAEvent {
    var name: String { get }
}

extension TFAEvent where Self: RawRepresentable, Self.RawValue == String {
    var name: String { rawValue }
}

struct TFATracker: AnalyticsKeyProtocol {
    var eventType: TFAEvent
    
    private var name: String { eventType.name }
    
    private var properties: [String: Any] {
        [:]
    }
    
    private var providers: [AnalyticsProvider] {
        [AnalyticsProvider.mixPanel]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("Device Authorization - " + name, properties: properties, providers: providers)
    }
}
