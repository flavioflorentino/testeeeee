import AnalyticsModule
import Foundation

struct AuthOptionsTracker: AnalyticsKeyProtocol {
    var eventType: TFAEvent
    var authFlowOptions: [DeviceAuthFlow] = []
    var authFlow: DeviceAuthFlow?
    
    private var name: String { eventType.name }
    
    private var properties: [String: Any] {
        if let selectedFlow = authFlow {
            let methodChosen = AuthFlowEventFormatter(authFlow: selectedFlow).description
            return ["method_chosen": methodChosen]
        } else {
            let flowsDescriptions = authFlowOptions.map({ AuthFlowEventFormatter(authFlow: $0).description })
            return ["methods_showed": flowsDescriptions]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [AnalyticsProvider.mixPanel]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("Device Authorization - " + name, properties: properties, providers: providers)
    }
}
