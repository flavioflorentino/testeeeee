import Foundation

struct AuthStepEventFormatter {
    var authStep: DeviceAuthStep
    
    var description: String {
        switch authStep {
        case .userData:
            return "USER DATA"
        case .bankData:
            return "BANK DATA"
        case .phoneNumber:
            return "PHONE NUMBER"
        case .code:
            return "CODE"
        case .creditCard:
            return "CARD DATA"
        default:
            return ""
        }
    }
}

struct AuthFlowEventFormatter {
    var authFlow: DeviceAuthFlow
    
    var description: String {
        switch authFlow {
        case .authorizedDevice, .device:
            return "PREVIOUS DEVICE"
        case .sms:
            return "SMS"
        case .email:
            return "EMAIL"
        case .creditCard:
            return "CREDIT CARD"
        case .validationID:
            return "SELFIE"
        default:
            return ""
        }
    }
}
