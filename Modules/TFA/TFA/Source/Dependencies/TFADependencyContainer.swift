import AnalyticsModule
import Core
import FeatureFlag

typealias TFADependencies =
    HasMainQueue &
    HasAnalytics &
    HasFeatureManager

final class TFADependencyContainer: TFADependencies {
    lazy var mainQueue = DispatchQueue.main
    lazy var analytics: AnalyticsProtocol = Analytics.shared
    lazy var featureManager: FeatureManagerContract = FeatureManager.shared
}
