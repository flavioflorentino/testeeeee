import Foundation
import UIKit

protocol TFAPopNavigationControllerDelegate: AnyObject {
    func shouldPop() -> Bool
}

final class TFAPopNavigationController: UINavigationController {
    weak var popDelegate: TFAPopNavigationControllerDelegate?
}

extension TFAPopNavigationController: UINavigationBarDelegate {
    func navigationBar(_ navigationBar: UINavigationBar, shouldPop item: UINavigationItem) -> Bool {
        popDelegate?.shouldPop() ?? true
    }
}
