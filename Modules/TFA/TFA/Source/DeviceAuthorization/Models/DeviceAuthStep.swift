import Foundation

enum DeviceAuthStep: String, Decodable {
    case initial = "INITIAL"
    case selfie = "SELFIE"
    case userData = "DATA_CONSUMER"
    case bankData = "BANK_ACCOUNT"
    case phoneNumber = "PHONE_NUMBER"
    case creditCard = "CREDIT_CARD"
    case code = "CODE"
    case final = "FINAL"
}
