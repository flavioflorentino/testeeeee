struct DeviceAuthRequestData {
    let flow: DeviceAuthFlow
    let step: DeviceAuthStep
    let tfaId: String
}
