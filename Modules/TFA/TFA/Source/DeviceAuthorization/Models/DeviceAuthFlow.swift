import Foundation

enum DeviceAuthFlow: String, Decodable {
    case sms = "SMS"
    case validationID = "VALIDATION_ID"
    case authorizedDevice = "AUTHORIZED_DEVICE"
    case device = "DEVICE"
    case email = "EMAIL"
    case creditCard = "CREDIT_CARD"
    case options = "OPTIONS"
}
