import UI
import UIKit

final class TFANavBarBackButton: UIBarButtonItem {
    init(target: AnyObject?, action: Selector?) {
        super.init()
        self.action = action
        self.target = target
        self.image = Assets.navIconBack.image.withRenderingMode(.alwaysOriginal)
        self.style = .plain
        self.imageInsets = UIEdgeInsets(top: 0, left: -Spacing.base01, bottom: 0, right: 0)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
