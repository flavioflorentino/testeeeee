import UI
import UIKit

enum TFATextFieldFactory {
    static func defaultField() -> UIPPFloatingTextField {
        let textfield = UIPPFloatingTextField()
        textfield.defaultForm()
        textfield.titleColor = Colors.grayscale600.color
        textfield.selectedTitleColor = Colors.grayscale600.color
        textfield.textColor = Colors.grayscale900.color
        textfield.placeholderColor = Colors.grayscale300.color
        textfield.placeholderFont = Typography.bodyPrimary().font()
        textfield.font = Typography.bodyPrimary().font()
        return textfield
    }
}
