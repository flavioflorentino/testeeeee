import IdentityValidation
import UI
import UIKit

public final class DeviceAuthFlowCoordinator {
    private let navigationController: UINavigationController
    private let presenterController: UIViewController
    
    private var tfaId: String
    private var currentStep: DeviceAuthStep = .initial
    private var onFinish: ((_ validationComplete: Bool) -> Void) = { _ in }
    var identityValidationCoordinator: IDValidationCoordinatorProtocol?
    
    public init(
        tfaId: String,
        presenterController: UIViewController,
        navigationController: UINavigationController = UINavigationController(),
        onFinish: @escaping ((_ validationComplete: Bool) -> Void)
    ) {
        self.tfaId = tfaId
        self.presenterController = presenterController
        self.onFinish = onFinish
        
        self.navigationController = navigationController
        self.navigationController.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController.navigationBar.shadowImage = UIImage()
        self.navigationController.navigationBar.barTintColor = Colors.backgroundPrimary.color
        self.navigationController.modalPresentationStyle = .fullScreen
    }
    
    public func start() {
        let onboardingVC = DeviceAuthOnboardingFactory.make(coordinatorDelegate: self)
        
        navigationController.viewControllers = [onboardingVC]
        presenterController.present(navigationController, animated: true, completion: nil)
    }
}

// MARK: Private methods
private extension DeviceAuthFlowCoordinator {
    func finish(validationComplete: Bool) {
        navigationController.dismiss(animated: true)
        onFinish(validationComplete)
    }
    
    func presentNextStep(_ step: DeviceAuthStep, for flow: DeviceAuthFlow) {
        switch step {
        case .userData:
            presentPersonalDataValidation(with: flow, step: step)
        case .bankData:
            presentBankAccountValidation(with: flow, step: step)
        case .phoneNumber:
            presentPhoneNumberValidation(with: flow, step: step)
        case .code:
            presentCodeValidation(with: flow, step: step)
        case .final:
            presentValidationSuccess(with: flow, step: step)
        default:
            break
        }
    }
    
    func presentHelpInfo() {
        let helpVC = DeviceAuthInfoFactory.make(coordinatorDelegate: self)
        navigationController.pushViewController(helpVC, animated: true)
    }
    
    func presentAuthOptions() {
        let optionsVC = DeviceAuthOptionsFactory.make(
            coordinatorDelegate: self,
            deviceAuthRequestData: DeviceAuthRequestData(flow: .options, step: currentStep, tfaId: tfaId)
        )
        navigationController.pushViewController(optionsVC, animated: true)
    }
    
    func presentPersonalDataValidation(with flow: DeviceAuthFlow, step: DeviceAuthStep) {
        let personalDataVC = PersonalDataValidationFactory.make(
            coordinatorDelegate: self,
            deviceAuthRequestData: DeviceAuthRequestData(flow: flow, step: step, tfaId: tfaId)
        )
        navigationController.pushViewController(personalDataVC, animated: true)
    }
    
    func presentBankAccountValidation(with flow: DeviceAuthFlow, step: DeviceAuthStep) {
        let bankAccountVC = BankAccountValidationFactory.make(
            coordinatorDelegate: self,
            deviceAuthRequestData: DeviceAuthRequestData(flow: flow, step: step, tfaId: tfaId)
        )
        
        navigationController.pushViewController(bankAccountVC, animated: true)
    }
    
    func presentPhoneNumberValidation(with flow: DeviceAuthFlow, step: DeviceAuthStep) {
        let phoneNumberVC = PhoneNumberValidationFactory.make(
            coordinatorDelegate: self,
            deviceAuthRequestData: DeviceAuthRequestData(flow: flow, step: step, tfaId: tfaId)
        )
        navigationController.pushViewController(phoneNumberVC, animated: true)
    }
    
    func presentCodeValidation(with flow: DeviceAuthFlow, step: DeviceAuthStep) {
        let codeValidationVC = CodeValidationFactory.make(
            coordinatorDelegate: self, deviceAuthRequestData: DeviceAuthRequestData(flow: flow, step: step, tfaId: tfaId)
        )
        navigationController.pushViewController(codeValidationVC, animated: true)
    }
    
    func presentValidationSuccess(with flow: DeviceAuthFlow, step: DeviceAuthStep) {
        let successVC = ValidationSuccessFactory.make(
            coordinatorDelegate: self,
            deviceAuthRequestData: DeviceAuthRequestData(flow: flow, step: step, tfaId: tfaId)
        )
        navigationController.pushViewController(successVC, animated: true)
    }
    
    func processValidationStatus(_ status: IDValidationStatus?) {
        // todo
    }
}

// MARK: - DeviceAuthFlowProtocol
extension DeviceAuthFlowCoordinator: DeviceAuthFlowStepping {
    func validationDidGoBack() {
        navigationController.popToRootViewController(animated: true)
    }
    
    func receivedNextStep(forFlow flow: DeviceAuthFlow, step: DeviceAuthStep) {
        presentNextStep(step, for: flow)
    }
}

// MARK: Delegates
extension DeviceAuthFlowCoordinator: DeviceAuthOnboardingCoordinatorDelegate {
    func onboardingClose() {
        finish(validationComplete: false)
    }
    
    func onboardingShowHelp() {
        presentHelpInfo()
    }
    
    func onboardingNextStep() {
        presentAuthOptions()
    }
}

extension DeviceAuthFlowCoordinator: DeviceAuthInfoCoordinatorDelegate {
    func infoDidGoBack() {
        navigationController.popViewController(animated: true)
    }
}

extension DeviceAuthFlowCoordinator: DeviceAuthOptionsCoordinatorDelegate {
    func didSelectOption(option: DeviceAuthOption) {
        presentNextStep(option.firstStep, for: option.flow)
    }
    
    func didSelectValidationId(validationData: DeviceAuthSelfieFlowResponse, option: DeviceAuthOption) {
        identityValidationCoordinator = IDValidationFlowCoordinator(
            from: self.navigationController,
            token: validationData.selfie.token,
            flow: validationData.selfie.flow,
            completion: { [weak self] validationStatus in
                self?.processValidationStatus(validationStatus)
            }
        )
        
        identityValidationCoordinator?.start()
    }
    
    func optionsDidCloseFlow() {
        finish(validationComplete: false)
    }
    
    func optionsDidGoBack() {
        navigationController.popViewController(animated: true)
    }
}

extension DeviceAuthFlowCoordinator: ValidationSuccessCoordinatorDelegate {
    func successDidEnter() {
        finish(validationComplete: true)
    }
}
