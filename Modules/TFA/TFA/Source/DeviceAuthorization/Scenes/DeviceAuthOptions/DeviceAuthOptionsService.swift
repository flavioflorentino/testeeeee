import Core
import Foundation

protocol DeviceAuthOptionsServicing {
    func authOptions(
        with data: DeviceAuthRequestData,
        completion: @escaping (Result<DeviceAuthOptionsResponse, ApiError>) -> Void
    )
    
    func startSelfieValidation(
        with data: DeviceAuthRequestData,
        completion: @escaping (Result<DeviceAuthSelfieFlowResponse, ApiError>) -> Void
    )
}

final class DeviceAuthOptionsService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - DeviceAuthOptionsServicing
extension DeviceAuthOptionsService: DeviceAuthOptionsServicing {
    func authOptions(
        with data: DeviceAuthRequestData,
        completion: @escaping (Result<DeviceAuthOptionsResponse, ApiError>) -> Void
    ) {
        let api = Api<DeviceAuthOptionsResponse>(endpoint: DeviceAuthEndpoint.tfaOptions(requestData: data))
        
        api.execute { result in
            self.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func startSelfieValidation(
        with data: DeviceAuthRequestData,
        completion: @escaping (Result<DeviceAuthSelfieFlowResponse, ApiError>) -> Void
    ) {
        let api = Api<DeviceAuthSelfieFlowResponse>(endpoint: DeviceAuthEndpoint.selfieValidation(requestData: data))
        
        api.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
