import UI
import UIKit

protocol DeviceAuthOptionsDisplaying: AnyObject {
    func displayOptions(_ options: [DeviceAuthOption])
    func displayLoading(_ loading: Bool)
    func displayRetryLoadError()
    func displayDeviceAuthBlockedError(with title: String, message: String)
    func displayError(with title: String, message: String)
}

private extension DeviceAuthOptionsViewController.Layout {
    enum Height {
        static let cellEstimatedRowHeight: CGFloat = 80
    }
}

final class DeviceAuthOptionsViewController: ViewController<DeviceAuthOptionsInteracting, UIView>, LoadingViewProtocol {
    fileprivate enum Layout { }
    
    enum Section {
        case main
    }
    
    lazy var loadingView = LoadingView()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, .grayscale750())
            .with(\.textAlignment, .left)
        label.text = Strings.Options.header
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.textColor, .grayscale750())
            .with(\.textAlignment, .left)
        label.text = Strings.Options.body
        return label
    }()
    
    private lazy var tableView: UITableView = {
        let table = UITableView()
        table.backgroundColor = Colors.backgroundPrimary.color
        table.separatorStyle = .none
        table.showsVerticalScrollIndicator = false
        table.rowHeight = UITableView.automaticDimension
        table.estimatedRowHeight = Layout.Height.cellEstimatedRowHeight
        table.register(DeviceAuthOptionCell.self, forCellReuseIdentifier: DeviceAuthOptionCell.identifier)
        table.delegate = self
        return table
    }()
    
    // MARK: Handler
    
    private lazy var dataSource: TableViewDataSource<Section, DeviceAuthOption> = {
        let dataSource = TableViewDataSource<Section, DeviceAuthOption>(
            view: tableView
        ) { tableView, indexPath, option -> UITableViewCell? in
            let cell = tableView.dequeueReusableCell(
                withIdentifier: DeviceAuthOptionCell.identifier,
                for: indexPath
            ) as? DeviceAuthOptionCell
            
            cell?.tableRow = indexPath.row
            cell?.setup(for: option)
            return cell
        }
        
        dataSource.add(section: .main)
        return dataSource
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.loadOptions()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func buildViewHierarchy() {
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
        view.addSubview(tableView)
    }
    
    override func configureViews() {
        tableView.dataSource = dataSource
        navigationItem.leftBarButtonItem = TFANavBarBackButton(target: self, action: #selector(backButtonTapped(_:)))
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    override func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.topMargin.equalToSuperview().offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        tableView.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview()
        }
    }
}

// MARK: - DeviceAuthOptionsDisplaying
extension DeviceAuthOptionsViewController: DeviceAuthOptionsDisplaying {
    func displayOptions(_ options: [DeviceAuthOption]) {
        dataSource.update(items: options, from: .main)
    }
    
    func displayLoading(_ loading: Bool) {
        loading ? startLoadingView() : stopLoadingView()
    }
    
    func displayRetryLoadError() {
        let retryAction = ApolloAlertAction(title: Strings.General.tryAgain, dismissOnCompletion: true) { [weak self] in
            self?.interactor.loadOptions()
        }
        
        let backAction = ApolloAlertAction(title: Strings.General.back, dismissOnCompletion: true) { [weak self] in
            self?.interactor.close()
        }
        
        showApolloAlert(
            title: Strings.Alerts.optionsListNotLoadedAlertTitle,
            subtitle: Strings.Alerts.optionsListNotLoadedAlertMessage,
            primaryButtonAction: retryAction,
            linkButtonAction: backAction,
            dismissOnTouchOutside: false
        )
    }
    
    func displayDeviceAuthBlockedError(with title: String, message: String) {
        let okAction = ApolloAlertAction(title: Strings.General.understood, dismissOnCompletion: true) { [weak self] in
            self?.interactor.close()
        }
        
        showApolloAlert(title: title, subtitle: message, primaryButtonAction: okAction, dismissOnTouchOutside: false)
    }
    
    func displayError(with title: String, message: String) {
        let okAction = ApolloAlertAction(title: Strings.General.back) { }
        showApolloAlert(title: title, subtitle: message, primaryButtonAction: okAction, dismissOnTouchOutside: false)
    }
}

extension DeviceAuthOptionsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        interactor.selectedItem(indexPath)
    }
}

// MARK: Private methods
@objc
private extension DeviceAuthOptionsViewController {
    func backButtonTapped(_ sender: UIButton) {
        interactor.back()
    }
}
