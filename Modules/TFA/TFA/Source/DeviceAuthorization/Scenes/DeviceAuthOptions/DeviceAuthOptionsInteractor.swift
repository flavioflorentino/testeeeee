import Core
import FeatureFlag
import AnalyticsModule
import Foundation
import SecurityModule

protocol DeviceAuthOptionsInteracting: AnyObject {
    func loadOptions()
    func selectedItem(_ indexPath: IndexPath)
    func back()
    func close()
}

final class DeviceAuthOptionsInteractor {
    typealias Dependencies = HasFeatureManager & HasAnalytics
    private let dependencies: Dependencies

    private let service: DeviceAuthOptionsServicing
    private let presenter: DeviceAuthOptionsPresenting
    private var items: [DeviceAuthOption] = []
    private let requestData: DeviceAuthRequestData
    private let logDetectionService: LogDetectionServicing
    private let tfaBlockedCode = "tfa_blocked_code"

    init(
        service: DeviceAuthOptionsServicing,
        presenter: DeviceAuthOptionsPresenting,
        dependencies: Dependencies,
        requestData: DeviceAuthRequestData,
        logDetectionService: LogDetectionServicing
    ) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.requestData = requestData
        self.logDetectionService = logDetectionService
    }
}

// MARK: - DeviceAuthOptionsInteracting
extension DeviceAuthOptionsInteractor: DeviceAuthOptionsInteracting {
    func loadOptions() {
        presenter.presentLoading(true)
        service.authOptions(with: requestData) { [weak self] result in
            self?.presenter.presentLoading(false)
            switch result {
            case .success(let options):
                self?.verifyOptions(options)
            case .failure(let error):
                 guard let requestError = error.requestError else {
                     self?.presenter.presentRetryLoadError()
                     return
                 }
                 
                 self?.verifyError(requestError)
            }
        }
    }
    
    func selectedItem(_ indexPath: IndexPath) {
        logDetectionService.sendLog()
        let selectedOption = items[indexPath.row]
        
        let eventTracker = AuthOptionsTracker(
            eventType: AuthOptionsEvent.authorizationMethodChosen,
            authFlow: selectedOption.flow
        )
        dependencies.analytics.log(eventTracker)
        
        guard selectedOption.flow == .validationID else {
            presenter.presentNextStepFor(selectedOption)
            return
        }
        
        startSelfieValidation(with: selectedOption)
    }
    
    func back() {
        presenter.goBack()
    }
    
    func close() {
        presenter.closeDeviceAuth()
    }
}

// MARK: - private methods
extension DeviceAuthOptionsInteractor {
    private func verifyError(_ error: RequestError) {
        if error.code == tfaBlockedCode {
            presenter.presentTFABlockedError(error)
        } else {
            presenter.presentRetryLoadError()
        }
    }
    
    private func verifyOptions(_ authOptions: DeviceAuthOptionsResponse) {
        var filteredOptions = authOptions.options
        if !dependencies.featureManager.isActive(.featureTfaCreditcardFlowActive) {
            filteredOptions = authOptions.options.filter({ $0.flow != .creditCard })
        }

        let authFlows = filteredOptions.map({ $0.flow })
        let eventTracker = AuthOptionsTracker(
            eventType: AuthOptionsEvent.authorizationMethodsViewed,
            authFlowOptions: authFlows
        )
        
        dependencies.analytics.log(eventTracker)
        
        self.items = filteredOptions
        self.presenter.presentOptions(filteredOptions)
    }
    
    private func startSelfieValidation(with option: DeviceAuthOption) {
        let deviceAuthData = DeviceAuthRequestData(flow: option.flow, step: option.firstStep, tfaId: requestData.tfaId)
        
        presenter.presentLoading(true)
        service.startSelfieValidation(with: deviceAuthData) { [weak self] result in
            self?.presenter.presentLoading(false)
            switch result {
            case .success(let response):
                self?.presenter.presentSelfieValidation(with: response, andOption: option)
            case .failure(let error):
                guard let requestError = error.requestError else {
                    self?.presenter.presentSelfieValidationError(error: RequestError())
                    return
                }
                
                self?.presenter.presentSelfieValidationError(error: requestError)
            }
        }
    }
}
