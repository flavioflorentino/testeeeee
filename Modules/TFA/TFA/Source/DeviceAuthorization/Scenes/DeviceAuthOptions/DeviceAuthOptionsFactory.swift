import SecurityModule
import UIKit

enum DeviceAuthOptionsFactory {
    static func make(
        coordinatorDelegate: DeviceAuthOptionsCoordinatorDelegate,
        deviceAuthRequestData: DeviceAuthRequestData
    ) -> DeviceAuthOptionsViewController {
        let container = TFADependencyContainer()
        let service: DeviceAuthOptionsServicing = DeviceAuthOptionsService(dependencies: container)
        let coordinator: DeviceAuthOptionsCoordinating = DeviceAuthOptionsCoordinator()
        let presenter: DeviceAuthOptionsPresenting = DeviceAuthOptionsPresenter(coordinator: coordinator)
        
        let logDetectionService: LogDetectionServicing = LogDetectionService()
        let interactor = DeviceAuthOptionsInteractor(
            service: service,
            presenter: presenter,
            dependencies: container,
            requestData: deviceAuthRequestData,
            logDetectionService: logDetectionService
        )
        let viewController = DeviceAuthOptionsViewController(interactor: interactor)

        coordinator.viewController = viewController
        coordinator.delegate = coordinatorDelegate
        presenter.viewController = viewController

        return viewController
    }
}
