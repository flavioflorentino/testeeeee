import UIKit

enum DeviceAuthOptionsAction: Equatable {
    case selectedOption(option: DeviceAuthOption)
    case validationId(validationData: DeviceAuthSelfieFlowResponse, option: DeviceAuthOption)
    case close
    case back
}

protocol DeviceAuthOptionsCoordinatorDelegate: AnyObject {
    func didSelectOption(option: DeviceAuthOption)
    func didSelectValidationId(validationData: DeviceAuthSelfieFlowResponse, option: DeviceAuthOption)
    func optionsDidCloseFlow()
    func optionsDidGoBack()
}

protocol DeviceAuthOptionsCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: DeviceAuthOptionsCoordinatorDelegate? { get set }
    func perform(action: DeviceAuthOptionsAction)
}

final class DeviceAuthOptionsCoordinator {
    weak var viewController: UIViewController?
    weak var delegate: DeviceAuthOptionsCoordinatorDelegate?
}

// MARK: - DeviceAuthOptionsCoordinating
extension DeviceAuthOptionsCoordinator: DeviceAuthOptionsCoordinating {
    func perform(action: DeviceAuthOptionsAction) {
        switch action {
        case .close:
            delegate?.optionsDidCloseFlow()
        case .selectedOption(let option):
            delegate?.didSelectOption(option: option)
        case let .validationId(validationData, option):
            delegate?.didSelectValidationId(validationData: validationData, option: option)
        case .back:
            delegate?.optionsDidGoBack()
        }
    }
}
