import UI
import UIKit

private extension DeviceAuthOptionCell.Layout {
    enum Image {
        static let size: CGFloat = 48
    }
    
    enum Width {
        static let disclosureIndicatorWidth: CGFloat = 20
    }
    
    enum Height {
        static let separatorViewHeight: CGFloat = 1
    }
}

final class DeviceAuthOptionCell: UITableViewCell {
    fileprivate enum Layout { }
    
    var tableRow: Int? {
        didSet {
            topSeparatorView.isHidden = self.tableRow == 0 ? false : true
        }
    }
    
    private lazy var topSeparatorView: UIView = {
        let view = UIView()
        view.isHidden = true
        view.backgroundColor = Colors.grayscale100.color
        return view
    }()
    
    private lazy var deviceAuthImageView = UIImageView()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .left)
        return label
    }()
    
    private lazy var deviceAuthDisclosureIndicatorView: UIImageView = {
        let imageView = UIImageView(image: Assets.chevron.image)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var bottomSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale100.color
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addComponents()
        layoutComponents()
        selectionStyle = .none
        backgroundColor = Colors.backgroundPrimary.color
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addComponents() {
        addSubview(topSeparatorView)
        addSubview(deviceAuthImageView)
        addSubview(titleLabel)
        addSubview(deviceAuthDisclosureIndicatorView)
        addSubview(bottomSeparatorView)
    }
    
    private func layoutComponents() {
        topSeparatorView.snp.makeConstraints {
            $0.height.equalTo(Layout.Height.separatorViewHeight)
            $0.leading.trailing.top.equalToSuperview()
            $0.bottom.equalTo(deviceAuthImageView.snp.top).offset(-Spacing.base02)
        }
        
        deviceAuthImageView.snp.makeConstraints {
            $0.leading.equalToSuperview()
            $0.trailing.equalTo(titleLabel.snp.leading).offset(-Spacing.base00)
            $0.bottom.equalTo(bottomSeparatorView.snp.top).offset(-Spacing.base02)
            $0.width.height.equalTo(Layout.Image.size)
        }
        
        titleLabel.snp.makeConstraints {
            $0.centerY.equalTo(deviceAuthImageView.snp.centerY)
            $0.trailing.equalTo(deviceAuthDisclosureIndicatorView.snp.leading).offset(-Spacing.base01)
        }
        
        deviceAuthDisclosureIndicatorView.snp.makeConstraints {
            $0.centerY.equalTo(titleLabel.snp.centerY)
            $0.width.equalTo(Layout.Width.disclosureIndicatorWidth)
            $0.trailing.equalToSuperview()
        }
        
        bottomSeparatorView.snp.makeConstraints {
            $0.height.equalTo(Layout.Height.separatorViewHeight)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    func setup(for option: DeviceAuthOption) {
        titleLabel.text = option.title
        deviceAuthImageView.setImage(url: URL(string: option.iconUrl), placeholder: tfaIconFor(option.flow))
    }
}

private extension DeviceAuthOptionCell {
    func tfaIconFor(_ type: DeviceAuthFlow) -> UIImage {
        switch type {
        case .creditCard:
            return Assets.creditCard.image
        case .sms:
            return Assets.sms.image
        case .email:
            return Assets.email.image
        default:
            return Assets.smartphone.image
        }
    }
}
