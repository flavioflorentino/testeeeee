import Core
import Foundation
import UI

protocol DeviceAuthOptionsPresenting: AnyObject {
    var viewController: DeviceAuthOptionsDisplaying? { get set }
    func presentLoading(_ loading: Bool)
    func presentOptions(_ options: [DeviceAuthOption])
    func presentNextStepFor(_ option: DeviceAuthOption)
    func presentSelfieValidation(with validationData: DeviceAuthSelfieFlowResponse, andOption option: DeviceAuthOption)
    func presentRetryLoadError()
    func presentSelfieValidationError(error: RequestError)
    func presentTFABlockedError(_ error: RequestError)
    func goBack()
    func closeDeviceAuth()
}

final class DeviceAuthOptionsPresenter {
    private let coordinator: DeviceAuthOptionsCoordinating
    weak var viewController: DeviceAuthOptionsDisplaying?

    init(coordinator: DeviceAuthOptionsCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - DeviceAuthOptionsPresenting
extension DeviceAuthOptionsPresenter: DeviceAuthOptionsPresenting {
    func presentLoading(_ loading: Bool) {
        viewController?.displayLoading(loading)
    }
    
    func presentOptions(_ options: [DeviceAuthOption]) {
        viewController?.displayOptions(options)
    }
    
    func presentNextStepFor(_ option: DeviceAuthOption) {
        coordinator.perform(action: .selectedOption(option: option))
    }
    
    func presentSelfieValidation(with validationData: DeviceAuthSelfieFlowResponse, andOption option: DeviceAuthOption) {
        coordinator.perform(action: .validationId(validationData: validationData, option: option))
    }
    
    func presentRetryLoadError() {
        viewController?.displayRetryLoadError()
    }
    
    func presentSelfieValidationError(error: RequestError) {
        viewController?.displayError(with: error.title, message: error.message)
    }
    
    func presentTFABlockedError(_ error: RequestError) {
        viewController?.displayDeviceAuthBlockedError(with: error.title, message: error.message)
    }
    
    func goBack() {
        coordinator.perform(action: .back)
    }
    
    func closeDeviceAuth() {
        coordinator.perform(action: .close)
    }
}
