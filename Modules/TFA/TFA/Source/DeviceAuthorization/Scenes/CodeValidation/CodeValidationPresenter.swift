import Core
import Foundation
import UI

protocol CodeValidationPresenting: AnyObject {
    var viewController: CodeValidationDisplaying? { get set }
    func didNextStep(action: CodeValidationAction)
    func presentLoading(_ isLoading: Bool)
    func presentGoBackAlert()
    func presentRetryButtonEnabled()
    func presentRetryButtonUpdated(with remainingTime: Int)
    func presentInfoFor(destination: String, withFlow flow: DeviceAuthFlow)
    func presentAttempLimitError(tfaError: RequestError, retryTime: Int)
    func presentErrorAlert(title: String, message: String)
}

final class CodeValidationPresenter {
    private let coordinator: CodeValidationCoordinating
    weak var viewController: CodeValidationDisplaying?
    
    private let timeFormatter: TFATimeFormatterProtocol

    init(coordinator: CodeValidationCoordinating, timeFormatter: TFATimeFormatterProtocol = DropLeadingTFATimeFormatter()) {
        self.coordinator = coordinator
        self.timeFormatter = timeFormatter
    }
}

// MARK: - CodeValidationPresenting
extension CodeValidationPresenter: CodeValidationPresenting {
    func didNextStep(action: CodeValidationAction) {
        coordinator.perform(action: action)
    }
    
    func presentLoading(_ isLoading: Bool) {
        viewController?.displayLoading(isLoading)
    }
    
    func presentGoBackAlert() {
        viewController?.displayCancelAuthorizationAlert()
    }
    
    func presentRetryButtonEnabled() {
        viewController?.displayRetryButtonEnabled()
    }
    
    func presentRetryButtonUpdated(with remainingTime: Int) {
        let formattedTime = timeFormatter.formatString(from: TimeInterval(remainingTime))
        let buttonText = Strings.Code.didNotReceived(formattedTime)
        
        let attributedText = buttonText.attributedString(
            highlights: [formattedTime],
            attrs: [.font: Typography.bodyPrimary(.highlight).font()]
        )
        
        viewController?.displayRetryButtonDisabled(with: attributedText)
    }
    
    func presentInfoFor(destination: String, withFlow flow: DeviceAuthFlow) {
        guard flow != .authorizedDevice && flow != .device else {
            return
        }
        
        guard !destination.isEmpty else {
            let title = flow == .email ? Strings.Code.emailNotificationSent : Strings.Code.smsNotificationSent
            viewController?.displayDescription(title)
            return
        }
        
        let title =
            flow == .email ? Strings.Code.emailNotificationWithEmailMask(destination) :
            Strings.Code.smsNotificationWithPhoneMask(destination)
        
        let attributedText = title.attributedString(
            highlights: [destination],
            attrs: [.font: Typography.bodyPrimary(.highlight).font()]
        )
        
        viewController?.displayAttributedDescription(attributedText)
    }
    
    func presentAttempLimitError(tfaError: RequestError, retryTime: Int) {
        viewController?.displayAttempLimitError(with: tfaError.title, message: tfaError.message, retryTime: retryTime)
    }
    
    func presentErrorAlert(title: String, message: String) {
        viewController?.displayAlert(title: title, message: message)
    }
}
