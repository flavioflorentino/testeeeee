import Core
import Foundation

protocol CodeValidationServicing {
    func getNewCode(requestData: DeviceAuthRequestData, completion: @escaping (Result<ValidationCode, ApiError>) -> Void)
    func validateCodeForNextStep(
        code: String,
        requestData: DeviceAuthRequestData,
        completion: @escaping (Result<DeviceAuthValidationResponse, ApiError>) -> Void
    )
}

struct CodeValidationService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - CodeValidationServicing
extension CodeValidationService: CodeValidationServicing {
    func getNewCode(requestData: DeviceAuthRequestData, completion: @escaping (Result<ValidationCode, ApiError>) -> Void) {
        let endpoint = DeviceAuthEndpoint.getValidationCode(requestData: requestData)
        let api = Api<ValidationCode>(endpoint: endpoint)
        
        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { result in
            dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func validateCodeForNextStep(
        code: String,
        requestData: DeviceAuthRequestData,
        completion: @escaping (Result<DeviceAuthValidationResponse, ApiError>) -> Void
    ) {
        let endpoint = DeviceAuthEndpoint.validateCode(code: code, requestData: requestData)
        let api = Api<DeviceAuthValidationResponse>(endpoint: endpoint)
        
        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { result in
            dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
