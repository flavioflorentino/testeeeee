import Core
import Foundation

protocol CodeValidationInteracting: AnyObject {
    func loadDefaultInfo()
    func goBackStarted()
    func goBackConfirmed()
    func loadCode()
    func validateCode(_ code: String)
    func startTimerForNewRequest(_ retryTime: Int)
}

final class CodeValidationInteractor {
    private let service: CodeValidationServicing
    private let presenter: CodeValidationPresenting

    private let deviceAuthRequestData: DeviceAuthRequestData
    private let validCodeLength = 4
    
    private var timer: TFATimerProtocol
    private let timeLimitToValidate = 60
    private var countdownSeconds: Int
    
    init(
        service: CodeValidationServicing,
        presenter: CodeValidationPresenting,
        deviceAuthRequestData: DeviceAuthRequestData,
        timer: TFATimerProtocol = TFATimer()
    ) {
        self.service = service
        self.presenter = presenter
        self.deviceAuthRequestData = deviceAuthRequestData
        self.timer = timer
        self.countdownSeconds = timeLimitToValidate
        self.timer.delegate = self
    }
}

// MARK: - CodeValidationInteracting
extension CodeValidationInteractor: CodeValidationInteracting {
    func loadDefaultInfo() {
        presenter.presentInfoFor(destination: "", withFlow: deviceAuthRequestData.flow)
    }
    
    func goBackStarted() {
        presenter.presentGoBackAlert()
    }
    
    func goBackConfirmed() {
        timer.stop()
        presenter.didNextStep(action: .back)
    }
    
    func loadCode() {
        presenter.presentLoading(true)
        service.getNewCode(requestData: deviceAuthRequestData) { result in
            self.presenter.presentLoading(false)
            
            switch result {
            case .success(let validationCode):
                self.receivedValidationCode(validationCode)
                self.startTimerForNewRequest(validationCode.retryTime)
            case .failure(let error):
                self.parseError(error: error)
            }
        }
    }
    
    func validateCode(_ code: String) {
        guard code.count == validCodeLength else {
            return
        }
        
        presenter.presentLoading(true)
        service.validateCodeForNextStep(code: code, requestData: deviceAuthRequestData) { result in
            self.presenter.presentLoading(false)
            
            switch result {
            case .success(let response):
                self.didValidateCode(for: response.nextStep)
            case .failure(let error):
                self.parseError(error: error)
            }
        }
    }
    
    func startTimerForNewRequest(_ retryTime: Int) {
        countdownSeconds = retryTime
        timer.start()
    }
}

// MARK: - Timer Delegate
extension CodeValidationInteractor: TFATimerDelegate {
    func timerTic() {
        if countdownSeconds <= 0 {
            timer.stop()
            presenter.presentRetryButtonEnabled()
        } else {
            countdownSeconds -= 1
            presenter.presentRetryButtonUpdated(with: countdownSeconds)
        }
    }
}

// MARK: - Private Methods
private extension CodeValidationInteractor {
    func receivedValidationCode(_ code: ValidationCode) {
        presenter.presentInfoFor(destination: code.sentTo, withFlow: deviceAuthRequestData.flow)
    }
    
    func didValidateCode(for nextStep: DeviceAuthStep) {
        timer.stop()
        presenter.didNextStep(action: .nextStep(flow: deviceAuthRequestData.flow, step: nextStep))
    }
    
    func parseError(error: ApiError) {
        switch error {
        case .tooManyRequests(let requestError), .unauthorized(let requestError), .badRequest(let requestError):
            guard let retryTime = requestError.data["retryTime"] as? Int else {
                presenter.presentErrorAlert(title: requestError.title, message: requestError.message)
                return
            }
            presenter.presentAttempLimitError(tfaError: requestError, retryTime: retryTime)
        default:
            presenter.presentErrorAlert(title: Strings.Alerts.genericErrorTitle, message: Strings.Alerts.genericErrorMessage)
        }
    }
}
