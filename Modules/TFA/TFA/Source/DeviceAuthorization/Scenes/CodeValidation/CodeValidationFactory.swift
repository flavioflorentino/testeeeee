import UIKit

enum CodeValidationFactory {
    static func make(
        coordinatorDelegate: DeviceAuthFlowStepping,
        deviceAuthRequestData: DeviceAuthRequestData
    ) -> CodeValidationViewController {
        let container = TFADependencyContainer()
        let service: CodeValidationServicing = CodeValidationService(dependencies: container)
        let coordinator: CodeValidationCoordinating = CodeValidationCoordinator()
        let presenter: CodeValidationPresenting = CodeValidationPresenter(coordinator: coordinator)
        
        let interactor = CodeValidationInteractor(
            service: service,
            presenter: presenter,
            deviceAuthRequestData: deviceAuthRequestData
        )
        
        let viewController = CodeValidationViewController(interactor: interactor)

        coordinator.viewController = viewController
        coordinator.delegate = coordinatorDelegate
        presenter.viewController = viewController

        return viewController
    }
}
