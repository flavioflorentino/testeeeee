import Foundation
import UIKit

final class CodeValidationKeyboardHandler {
    var onShowHideCompletion: ((CGFloat) -> Void)?
    
    private let notificationCenter: NotificationCenter
    private let scrollView: UIScrollView
    private let contentBottomMargin: CGFloat
    
    init(scrollView: UIScrollView, contentBottomMargin: CGFloat = 0, notificationCenter: NotificationCenter = .default) {
        self.contentBottomMargin = contentBottomMargin
        self.scrollView = scrollView
        self.notificationCenter = notificationCenter
    }
    
    func registerForKeyboardNotifications() {
        notificationCenter.addObserver(
            self,
            selector: #selector(keyboardWillShow(_:)),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        
        notificationCenter.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    
    @objc
    private func keyboardWillShow(_ notification: NSNotification) {
        var inset = contentBottomMargin
        if #available(iOS 11, *) {
            inset -= scrollView.safeAreaInsets.bottom
        }
        scrollView.contentInset.bottom = inset
        scrollView.scrollIndicatorInsets = scrollView.contentInset
        
        guard let userInfo = notification.userInfo,
              let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
            return
        }
        
        updateKeyboardConstraint(userInfo: userInfo, inset: keyboardFrame.cgRectValue.size.height)
    }
    
    @objc
    private func keyboardWillHide(_ notification: NSNotification) {
        scrollView.contentInset.bottom = 0
        scrollView.scrollIndicatorInsets = scrollView.contentInset
        
        guard let userInfo = notification.userInfo else {
            return
        }
        
        updateKeyboardConstraint(userInfo: userInfo)
    }
    
    private func updateKeyboardConstraint(userInfo: [AnyHashable: Any], inset: CGFloat = 0.0) {
        guard let animationDuration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber,
              let animationCurve = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber else {
            return
        }
        
        UIView.animate(
            withDuration: animationDuration.doubleValue,
            delay: 0,
            options: UIView.AnimationOptions(rawValue: animationCurve.uintValue),
            animations: {
                self.onShowHideCompletion?(inset)
            },
            completion: nil
        )
    }
}
