import UIKit

enum CodeValidationAction: Equatable {
    case back
    case nextStep(flow: DeviceAuthFlow, step: DeviceAuthStep)
}

protocol CodeValidationCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: DeviceAuthFlowStepping? { get set }
    func perform(action: CodeValidationAction)
}

final class CodeValidationCoordinator {
    weak var viewController: UIViewController?
    weak var delegate: DeviceAuthFlowStepping?
}

// MARK: - CodeValidationCoordinating
extension CodeValidationCoordinator: CodeValidationCoordinating {
    func perform(action: CodeValidationAction) {
        switch action {
        case .back:
            delegate?.validationDidGoBack()
        case let .nextStep(flow, step):
            delegate?.receivedNextStep(forFlow: flow, step: step)
        }
    }
}
