import Foundation

struct ValidationCode: Decodable {
    let retryTime: Int
    let sentTo: String
    let channel: String?
}
