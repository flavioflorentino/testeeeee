import SnapKit
import UI
import UIKit

protocol CodeValidationDisplaying: AnyObject {
    func displayLoading(_ isLoading: Bool)
    func displayCancelAuthorizationAlert()
    func displayAttempLimitError(with title: String, message: String, retryTime: Int)
    func displayAlert(title: String, message: String)
    func displayAttributedDescription(_ attributedText: NSAttributedString)
    func displayDescription(_ descriptionText: String)
    func displayRetryButtonEnabled()
    func displayRetryButtonDisabled(with attributedText: NSAttributedString)
}

final class CodeValidationViewController: ViewController<CodeValidationInteracting, UIView>, LoadingViewProtocol {
    fileprivate enum Constants {
        static let validCodeLength = 4
    }
    lazy var loadingView = LoadingView()
    
    private lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.backgroundColor = .clear
        return scroll
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, .grayscale750())
            .with(\.textAlignment, .left)
            .with(\.text, Strings.Code.header)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.textColor, .grayscale750())
            .with(\.textAlignment, .left)
            .with(\.text, Strings.Code.oldDeviceNotificationSent)
        return label
    }()
    
    private lazy var codeTextfield: UIPPFloatingTextField = {
        let textfield = TFATextFieldFactory.defaultField()
        textfield.addTarget(self, action: #selector(checkCode), for: .editingChanged)
        textfield.keyboardType = .numberPad
        textfield.placeholder = Strings.PhoneNumber.phoneFieldPlaceholder
        textfield.delegate = self
        return textfield
    }()
    
    private lazy var retryButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.Code.resendButtonText, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(retryButtonTapped), for: .touchUpInside)
        return button
    }()
    
    private lazy var keyboardHandler = CodeValidationKeyboardHandler(scrollView: scrollView, contentBottomMargin: Spacing.base05)
    private var scrollViewBottomConstraint: Constraint?
    private var codeTextFieldBottomConstraint: Constraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.loadDefaultInfo()
        interactor.loadCode()
    }

    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(codeTextfield)
        contentView.addSubview(retryButton)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.leading.equalTo(view.compatibleSafeArea.leading)
            $0.top.equalTo(view.compatibleSafeArea.top)
            $0.trailing.equalTo(view.compatibleSafeArea.trailing)
            scrollViewBottomConstraint = $0.bottom.equalTo(view.compatibleSafeArea.bottom).constraint
        }
        
        contentView.snp.makeConstraints {
            $0.top.bottom.centerX.width.equalToSuperview()
            $0.height.equalToSuperview().priority(.medium)
        }

        titleLabel.snp.makeConstraints {
            $0.bottom.equalTo(descriptionLabel.snp.top).offset(-Spacing.base02)
            $0.top.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(codeTextfield.snp.top).offset(-Spacing.base06)
        }
        
        codeTextfield.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            codeTextFieldBottomConstraint = $0.bottom.lessThanOrEqualTo(retryButton.snp.top).offset(-Spacing.base02).constraint
        }
        
        retryButton.snp.makeConstraints {
            $0.leading.trailing.bottom.equalToSuperview().inset(Spacing.base02)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        keyboardHandler.registerForKeyboardNotifications()
        navigationItem.leftBarButtonItem = TFANavBarBackButton(target: self, action: #selector(backButtonTapped))
        
        codeTextFieldBottomConstraint?.deactivate()
        keyboardHandler.onShowHideCompletion = { self.keyboardShowHideCompletion(height: $0) }
    }
}

// MARK: - Private Methods
@objc
private extension CodeValidationViewController {
    func keyboardShowHideCompletion(height: CGFloat) {
        view.layoutIfNeeded()
        scrollView.snp.makeConstraints {
            height == 0 ?
                self.codeTextFieldBottomConstraint?.deactivate() :
                self.codeTextFieldBottomConstraint?.activate()
          
            self.scrollViewBottomConstraint?.deactivate()
            self.scrollViewBottomConstraint = $0.bottom.equalTo(self.view.compatibleSafeArea.bottom).inset(height).constraint
        }
        view.layoutIfNeeded()
    }
    
    func backButtonTapped() {
        interactor.goBackStarted()
    }
    
    func checkCode() {
        interactor.validateCode(codeTextfield.text ?? "")
    }
    
    func retryButtonTapped() {
        interactor.loadCode()
    }
    
    private func enableValidation(_ isEnabled: Bool) {
        retryButton.isEnabled = isEnabled
        retryButton.setTitle(Strings.Code.resendButtonText, for: .disabled)
        retryButton.setTitle(Strings.Code.resendButtonText, for: .normal)
        retryButton.buttonStyle(LinkButtonStyle())
        codeTextfield.isEnabled = isEnabled
    }
}

// MARK: - CodeValidationDisplaying
extension CodeValidationViewController: CodeValidationDisplaying {
    func displayLoading(_ isLoading: Bool) {
        isLoading ? startLoadingView() : stopLoadingView()
    }
    
    func displayCancelAuthorizationAlert() {
        let continueAction = ApolloAlertAction(title: Strings.Alerts.CancelAuthorization.continue, dismissOnCompletion: true) { }
        
        let cancelAction = ApolloAlertAction(
            title: Strings.Alerts.CancelAuthorization.cancel,
            dismissOnCompletion: true) {
                self.interactor.goBackConfirmed()
        }

        showApolloAlert(
            title: Strings.Alerts.CancelAuthorization.title,
            subtitle: Strings.Alerts.CancelAuthorization.message,
            primaryButtonAction: continueAction,
            linkButtonAction: cancelAction,
            dismissOnTouchOutside: false
        )
    }
    
    func displayDescription(_ descriptionText: String) {
        descriptionLabel.text = descriptionText
    }
    
    func displayAttributedDescription(_ attributedText: NSAttributedString) {
        descriptionLabel.attributedText = attributedText
    }
    
    func displayRetryButtonEnabled() {
        enableValidation(true)
    }
    
    func displayRetryButtonDisabled(with attributedText: NSAttributedString) {
        retryButton.isEnabled = false
        retryButton.buttonStyle(LinkButtonStyle())
        retryButton.setAttributedTitle(attributedText, for: .disabled)
    }
    
    func displayAttempLimitError(with title: String, message: String, retryTime: Int) {
        enableValidation(false)
        interactor.startTimerForNewRequest(retryTime)

        displayAlert(title: title, message: message)
    }
    
    func displayAlert(title: String, message: String) {
        let backAction = ApolloAlertAction(title: Strings.General.back, dismissOnCompletion: true) { }
        showApolloAlert(title: title, subtitle: message, primaryButtonAction: backAction, dismissOnTouchOutside: false)
    }
}

extension CodeValidationViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.isEmpty {
            return true
        }
        
        guard let text = textField.text else {
            return false
        }
        
        return text.count < Constants.validCodeLength
    }
}
