import Foundation

protocol DeviceAuthInfoPresenting: AnyObject {
    func goBack()
}

final class DeviceAuthInfoPresenter: DeviceAuthInfoPresenting {
    private let coordinator: DeviceAuthInfoCoordinating

    init(coordinator: DeviceAuthInfoCoordinating) {
        self.coordinator = coordinator
    }
    
    func goBack() {
        coordinator.perform(action: .goBack)
    }
}
