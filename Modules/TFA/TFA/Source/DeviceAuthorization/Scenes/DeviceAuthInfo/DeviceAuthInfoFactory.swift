import UIKit

enum DeviceAuthInfoFactory {
    static func make(coordinatorDelegate: DeviceAuthInfoCoordinatorDelegate) -> DeviceAuthInfoViewController {
        let container = TFADependencyContainer()
        let coordinator: DeviceAuthInfoCoordinating = DeviceAuthInfoCoordinator()
        let presenter: DeviceAuthInfoPresenting = DeviceAuthInfoPresenter(coordinator: coordinator)
        let interactor = DeviceAuthInfoInteractor(presenter: presenter, dependencies: container)
        let viewController = DeviceAuthInfoViewController(interactor: interactor)

        coordinator.viewController = viewController
        coordinator.delegate = coordinatorDelegate

        return viewController
    }
}
