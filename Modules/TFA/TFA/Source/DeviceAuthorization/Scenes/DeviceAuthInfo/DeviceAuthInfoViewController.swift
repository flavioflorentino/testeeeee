import UI
import UIKit

final class DeviceAuthInfoViewController: ViewController<DeviceAuthInfoInteracting, UIView> {
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textColor, .grayscale750())
            .with(\.textAlignment, .left)
        label.text = Strings.Info.header
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.textColor, .grayscale750())
            .with(\.textAlignment, .left)
        label.text = Strings.Info.body
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.sendAnalytics()
        navigationItem.leftBarButtonItem = TFANavBarBackButton(target: self, action: #selector(backButtonTapped(_:)))
    }

    override func buildViewHierarchy() {
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
    }
    
    override func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.topMargin.equalToSuperview().offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}

// MARK: Private methods
@objc
private extension DeviceAuthInfoViewController {
    func backButtonTapped(_ sender: UIButton) {
        interactor.back()
    }
}
