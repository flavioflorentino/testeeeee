import AnalyticsModule
import Foundation

protocol DeviceAuthInfoInteracting: AnyObject {
    func sendAnalytics()
    func back()
}

final class DeviceAuthInfoInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let presenter: DeviceAuthInfoPresenting

    init(presenter: DeviceAuthInfoPresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - DeviceAuthInfoInteracting
extension DeviceAuthInfoInteractor: DeviceAuthInfoInteracting {
    func sendAnalytics() {
        let eventTracker = TFATracker(eventType: InfoEvent.helpAccessed)
        dependencies.analytics.log(eventTracker)
    }
    
    func back() {
        presenter.goBack()
    }
}
