import UIKit

enum DeviceAuthInfoAction {
    case goBack
}

protocol DeviceAuthInfoCoordinatorDelegate: AnyObject {
    func infoDidGoBack()
}

protocol DeviceAuthInfoCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: DeviceAuthInfoCoordinatorDelegate? { get set }
    func perform(action: DeviceAuthInfoAction)
}

final class DeviceAuthInfoCoordinator {
    weak var viewController: UIViewController?
    weak var delegate: DeviceAuthInfoCoordinatorDelegate?
}

// MARK: - DeviceAuthInfoCoordinating
extension DeviceAuthInfoCoordinator: DeviceAuthInfoCoordinating {
    func perform(action: DeviceAuthInfoAction) {
        if case .goBack = action {
            delegate?.infoDidGoBack()
        }
    }
}
