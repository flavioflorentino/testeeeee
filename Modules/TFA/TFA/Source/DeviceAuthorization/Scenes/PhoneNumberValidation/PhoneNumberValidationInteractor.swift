import Core
import Foundation
import Validations

protocol PhoneNumberValidationInteracting: AnyObject {
    func backButtonWasTapped()
    func goBack()
    func loadPhoneInfo()
    func checkPhoneNumber(_ text: String)
    func validateForm()
    func startRetryValidationTimer(retryTime: Int)
}

final class PhoneNumberValidationInteractor {
    private let service: PhoneNumberValidationServicing
    private let presenter: PhoneNumberValidationPresenting

    private let deviceAuthRequestData: DeviceAuthRequestData
    private var validationPhoneNumber = ""
    
    private var timer: TFATimerProtocol
    private let timeLimitToValidate = 180
    private var countdownSeconds: Int
    
    init(
        service: PhoneNumberValidationServicing,
        presenter: PhoneNumberValidationPresenting,
        deviceAuthRequestData: DeviceAuthRequestData,
        timer: TFATimerProtocol = TFATimer()
    ) {
        self.service = service
        self.presenter = presenter
        self.deviceAuthRequestData = deviceAuthRequestData
        self.timer = timer
        self.countdownSeconds = timeLimitToValidate
        self.timer.delegate = self
    }
}

// MARK: - PhoneNumberValidationInteracting
extension PhoneNumberValidationInteractor: PhoneNumberValidationInteracting {
    func backButtonWasTapped() {
        presenter.presentGoBackAlert()
    }
    
    func goBack() {
        presenter.didNextStep(action: .back)
    }
    
    func loadPhoneInfo() {
        presenter.presentLoading(true)
        service.getPhoneNumberData(requestData: deviceAuthRequestData) { [weak self] result in
            self?.presenter.presentLoading(false)
            
            if case let .success(response) = result {
                self?.presenter.presentHintForNumber(response.phone.phoneNumber)
            }
        }
    }
    
    func checkPhoneNumber(_ text: String) {
        validationPhoneNumber = text
        let isValidPhone = validatePhone(validationPhoneNumber)
        presenter.updatePhoneNumberField(withError: isValidPhone ? nil : Strings.PhoneNumber.phoneFieldError)
        
        checkFieldsAndEnableContinue()
    }
    
    func validateForm() {
        presenter.presentLoading(true)
        
        service.validatePhoneNumber(
            phoneNumber: validationPhoneNumber,
            requestData: deviceAuthRequestData
        ) { [weak self] result in
            self?.presenter.presentLoading(false)
            
            switch result {
            case .success(let response):
                self?.didValidatePhoneNumber(for: response.nextStep)
            case .failure(let error):
                self?.parseError(error: error)
            }
        }
    }
    
    func startRetryValidationTimer(retryTime: Int) {
        countdownSeconds = retryTime
        timer.start()
    }
}

// MARK: - Timer Delegate
extension PhoneNumberValidationInteractor: TFATimerDelegate {
    func timerTic() {
        if countdownSeconds <= 0 {
            timer.stop()
            presenter.presentFormValidationEnabled()
        } else {
            countdownSeconds -= 1
            presenter.presentContinueButtonUpdated(with: countdownSeconds)
        }
    }
}

// MARK: - Private Methods
private extension PhoneNumberValidationInteractor {
    func validatePhone(_ phone: String) -> Bool {
        let validator = CellphoneWithDDDValidator()
        do {
            try validator.validate(phone)
            return true
        } catch {
            return false
        }
    }
    
    func checkFieldsAndEnableContinue() {
        let isValidPhone = validatePhone(validationPhoneNumber)
        presenter.shouldEnableContinueButton(isValidPhone)
    }
    
    func didValidatePhoneNumber(for nextStep: DeviceAuthStep) {
        presenter.didNextStep(action: .nextStep(flow: deviceAuthRequestData.flow, step: nextStep))
    }
    
    func parseError(error: ApiError) {
        switch error {
        case .tooManyRequests(let requestError), .unauthorized(let requestError), .badRequest(let requestError):
            guard let retryTime = requestError.data["retryTime"] as? Int else {
                presenter.presentErrorAlert(title: requestError.title, message: requestError.message)
                return
            }
            presenter.presentAttempLimitError(tfaError: requestError, retryTime: retryTime)
        default:
            presenter.presentErrorAlert(title: Strings.Alerts.genericErrorTitle, message: Strings.Alerts.genericErrorMessage)
        }
    }
}
