import Core
import Foundation

protocol PhoneNumberValidationServicing {
    func getPhoneNumberData(
        requestData: DeviceAuthRequestData,
        completion: @escaping (Result<TFAPhoneNumberResponse, ApiError>) -> Void
    )
    func validatePhoneNumber(
        phoneNumber: String,
        requestData: DeviceAuthRequestData,
        completion: @escaping (Result<DeviceAuthValidationResponse, ApiError>) -> Void
    )
}

struct PhoneNumberValidationService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - PhoneNumberValidationServicing
extension PhoneNumberValidationService: PhoneNumberValidationServicing {
    func getPhoneNumberData(
        requestData: DeviceAuthRequestData,
        completion: @escaping (Result<TFAPhoneNumberResponse, ApiError>) -> Void
    ) {
        let endpoint = DeviceAuthEndpoint.getPhoneNumberData(requestData: requestData)
        let api = Api<TFAPhoneNumberResponse>(endpoint: endpoint)
        
        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { result in
            dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func validatePhoneNumber(
        phoneNumber: String,
        requestData: DeviceAuthRequestData,
        completion: @escaping (Result<DeviceAuthValidationResponse, ApiError>) -> Void
    ) {
        let endpoint = DeviceAuthEndpoint.validatePhoneNumber(phoneNumber: phoneNumber, requestData: requestData)
        let api = Api<DeviceAuthValidationResponse>(endpoint: endpoint)
        
        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { result in
            dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
