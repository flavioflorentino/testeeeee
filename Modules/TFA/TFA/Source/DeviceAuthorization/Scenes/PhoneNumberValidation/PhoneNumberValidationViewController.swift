import UI
import UIKit

protocol PhoneNumberValidationDisplaying: AnyObject {
    func displayLoading(_ isLoading: Bool)
    func displayCancelAuthorizationAlert()
    func updatePhoneNumberFieldErrorMessage(_ message: String?)
    func enableContinueButton(_ enable: Bool)
    func displayAttributedDescription(_ attributedString: NSAttributedString)
    func displayAttempLimitError(with title: String, message: String, retryTime: Int)
    func displayAlert(title: String, message: String)
    func displayCountdownTimer(with remainingTime: String)
    func enableFormValidation()
}

final class PhoneNumberValidationViewController: ViewController<PhoneNumberValidationInteracting, UIView>, LoadingViewProtocol {
    fileprivate enum Constants {
        static let phoneNumberLength = 15
    }
    
    lazy var loadingView = LoadingView()
    
    private lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.backgroundColor = .clear
        return scroll
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, .grayscale750())
            .with(\.textAlignment, .left)
            .with(\.text, Strings.PhoneNumber.header)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.textColor, .grayscale750())
            .with(\.textAlignment, .left)
            .with(\.text, Strings.PhoneNumber.body)
        return label
    }()
    
    private lazy var phoneNumberTextfield: UIPPFloatingTextField = {
        let textfield = TFATextFieldFactory.defaultField()
        textfield.addTarget(self, action: #selector(checkPhoneNumberField), for: .editingChanged)
        textfield.keyboardType = .numberPad
        textfield.placeholder = Strings.PhoneNumber.phoneFieldPlaceholder
        textfield.delegate = self
        return textfield
    }()
    
    private lazy var continueButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.General.continue, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.isEnabled = false
        button.addTarget(self, action: #selector(didTapContinueButton), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Mask
    private lazy var phoneNumberMask: TextFieldMasker = {
        let mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cellPhoneWithDDD)
        return TextFieldMasker(textMask: mask)
    }()
    
    private lazy var keyboardScrollViewHandler = KeyboardScrollViewHandler(scrollView: scrollView)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.loadPhoneInfo()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(phoneNumberTextfield)
        contentView.addSubview(continueButton)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        contentView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.width.equalTo(view.snp.width)
        }

        titleLabel.snp.makeConstraints {
            $0.bottom.equalTo(descriptionLabel.snp.top).offset(-Spacing.base02)
            $0.top.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(phoneNumberTextfield.snp.top).offset(-Spacing.base06)
        }
        
        phoneNumberTextfield.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(continueButton.snp.top).offset(-Spacing.base06)
        }
        
        continueButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base04)
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        phoneNumberMask.bind(to: phoneNumberTextfield)
        keyboardScrollViewHandler.registerForKeyboardNotifications()
        navigationItem.leftBarButtonItem = TFANavBarBackButton(target: self, action: #selector(backButtonTapped))
    }
}

// MARK: - Private Methods
@objc
private extension PhoneNumberValidationViewController {
    func backButtonTapped() {
        interactor.backButtonWasTapped()
    }
    
    func checkPhoneNumberField() {
        interactor.checkPhoneNumber(phoneNumberTextfield.text ?? "")
    }
    
    func didTapContinueButton() {
        interactor.validateForm()
    }
    
    private func enableValidation(_ isEnabled: Bool) {
        continueButton.isEnabled = isEnabled
        continueButton.setTitle(Strings.General.continue, for: .disabled)
        continueButton.setTitle(Strings.General.continue, for: .normal)
        phoneNumberTextfield.isEnabled = isEnabled
    }
}

// MARK: - PhoneNumberValidationDisplaying
extension PhoneNumberValidationViewController: PhoneNumberValidationDisplaying {
    func displayLoading(_ isLoading: Bool) {
        isLoading ? startLoadingView() : stopLoadingView()
    }
    
    func updatePhoneNumberFieldErrorMessage(_ message: String?) {
        phoneNumberTextfield.errorMessage = message
    }
    
    func enableContinueButton(_ enable: Bool) {
        continueButton.isEnabled = enable
    }
    
    func displayAttributedDescription(_ attributedString: NSAttributedString) {
        descriptionLabel.attributedText = attributedString
    }
    
    func displayCancelAuthorizationAlert() {
        let continueAction = ApolloAlertAction(title: Strings.Alerts.CancelAuthorization.continue, dismissOnCompletion: true) { }
        
        let cancelAction = ApolloAlertAction(
            title: Strings.Alerts.CancelAuthorization.cancel,
            dismissOnCompletion: true) { [weak self] in
                self?.interactor.goBack()
        }

        showApolloAlert(
            title: Strings.Alerts.CancelAuthorization.title,
            subtitle: Strings.Alerts.CancelAuthorization.message,
            primaryButtonAction: continueAction,
            linkButtonAction: cancelAction,
            dismissOnTouchOutside: false
        )
    }
    
    func displayAttempLimitError(with title: String, message: String, retryTime: Int) {
        enableValidation(false)
        interactor.startRetryValidationTimer(retryTime: retryTime)

        displayAlert(title: title, message: message)
    }
    
    func displayAlert(title: String, message: String) {
        let backAction = ApolloAlertAction(title: Strings.General.back, dismissOnCompletion: true) { }
        showApolloAlert(title: title, subtitle: message, primaryButtonAction: backAction, dismissOnTouchOutside: false)
    }
    
    func displayCountdownTimer(with remainingTime: String) {
        continueButton.setTitle(remainingTime, for: .disabled)
    }
    
    func enableFormValidation() {
        enableValidation(true)
    }
}

extension PhoneNumberValidationViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.isEmpty {
            return true
        }
        
        guard let text = textField.text else {
            return false
        }
        
        switch textField {
        case phoneNumberTextfield:
            return text.count < Constants.phoneNumberLength
        default:
            return true
        }
    }
}
