import UIKit

enum PhoneNumberValidationAction: Equatable {
    case back
    case nextStep(flow: DeviceAuthFlow, step: DeviceAuthStep)
}

protocol PhoneNumberValidationCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: DeviceAuthFlowStepping? { get set }
    func perform(action: PhoneNumberValidationAction)
}

final class PhoneNumberValidationCoordinator {
    weak var viewController: UIViewController?
    weak var delegate: DeviceAuthFlowStepping?
}

// MARK: - PhoneNumberValidationCoordinating
extension PhoneNumberValidationCoordinator: PhoneNumberValidationCoordinating {
    func perform(action: PhoneNumberValidationAction) {
        switch action {
        case .back:
            delegate?.validationDidGoBack()
        case let .nextStep(flow, step):
            delegate?.receivedNextStep(forFlow: flow, step: step)
        }
    }
}
