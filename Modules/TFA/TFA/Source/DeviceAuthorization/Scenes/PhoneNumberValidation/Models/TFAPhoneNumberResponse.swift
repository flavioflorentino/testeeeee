import Foundation

struct TFAPhoneNumberResponse: Decodable {
    struct TFAPhoneNumber: Decodable {
        let phoneNumber: String
    }
    
    let phone: TFAPhoneNumber
}
