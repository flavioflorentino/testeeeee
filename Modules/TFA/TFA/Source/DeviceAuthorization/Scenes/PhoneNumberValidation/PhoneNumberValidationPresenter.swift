import Core
import UI
import Foundation

protocol PhoneNumberValidationPresenting: AnyObject {
    var viewController: PhoneNumberValidationDisplaying? { get set }
    func didNextStep(action: PhoneNumberValidationAction)
    func presentGoBackAlert()
    func presentLoading(_ isLoading: Bool)
    func presentHintForNumber(_ hint: String?)
    func updatePhoneNumberField(withError message: String?)
    func shouldEnableContinueButton(_ isEnabled: Bool)
    func presentAttempLimitError(tfaError: RequestError, retryTime: Int)
    func presentErrorAlert(title: String, message: String)
    func presentContinueButtonUpdated(with remainingTime: Int)
    func presentFormValidationEnabled()
}

final class PhoneNumberValidationPresenter {
    private let coordinator: PhoneNumberValidationCoordinating
    weak var viewController: PhoneNumberValidationDisplaying?

    private let timeFormatter: TFATimeFormatterProtocol
    
    init(coordinator: PhoneNumberValidationCoordinating, timeFormatter: TFATimeFormatterProtocol = TFATimeFormatter()) {
        self.coordinator = coordinator
        self.timeFormatter = timeFormatter
    }
}

// MARK: - PhoneNumberValidationPresenting
extension PhoneNumberValidationPresenter: PhoneNumberValidationPresenting {
    func didNextStep(action: PhoneNumberValidationAction) {
        coordinator.perform(action: action)
    }
    
    func presentGoBackAlert() {
        viewController?.displayCancelAuthorizationAlert()
    }
    
    func presentLoading(_ isLoading: Bool) {
        viewController?.displayLoading(isLoading)
    }
    
    func updatePhoneNumberField(withError message: String?) {
        viewController?.updatePhoneNumberFieldErrorMessage(message)
    }
    
    func shouldEnableContinueButton(_ isEnabled: Bool) {
        viewController?.enableContinueButton(isEnabled)
    }
    
    func presentHintForNumber(_ hint: String?) {
        guard let maskedNumber = hint else {
            return
        }
        
        let attrubutedString = getAttributedText(forHint: maskedNumber)
        viewController?.displayAttributedDescription(attrubutedString)
    }
    
    func presentAttempLimitError(tfaError: RequestError, retryTime: Int) {
        viewController?.displayAttempLimitError(with: tfaError.title, message: tfaError.message, retryTime: retryTime)
    }
    
    func presentErrorAlert(title: String, message: String) {
        viewController?.displayAlert(title: title, message: message)
    }
    
    func presentContinueButtonUpdated(with remainingTime: Int) {
        let formattedTime = timeFormatter.formatString(from: TimeInterval(remainingTime))
        let buttonText = Strings.General.wait(formattedTime)
        viewController?.displayCountdownTimer(with: buttonText)
    }
    
    func presentFormValidationEnabled() {
        viewController?.enableFormValidation()
    }
}

// MARK: - Private Methods
private extension PhoneNumberValidationPresenter {
    func getAttributedText(forHint hint: String) -> NSAttributedString {
        let phoneDescriptionText = Strings.PhoneNumber.body
        let completeText = phoneDescriptionText + hint
        
        return completeText.attributedString(
            highlights: [hint],
            attrs: [.font: Typography.bodyPrimary(.highlight).font()]
        )        
    }
}
