import UIKit

enum PhoneNumberValidationFactory {
    static func make(
        coordinatorDelegate: DeviceAuthFlowStepping,
        deviceAuthRequestData: DeviceAuthRequestData
    ) -> PhoneNumberValidationViewController {
        let container = TFADependencyContainer()
        let service: PhoneNumberValidationServicing = PhoneNumberValidationService(dependencies: container)
        let coordinator: PhoneNumberValidationCoordinating = PhoneNumberValidationCoordinator()
        let presenter: PhoneNumberValidationPresenting = PhoneNumberValidationPresenter(coordinator: coordinator)
        
        let interactor = PhoneNumberValidationInteractor(
            service: service,
            presenter: presenter,
            deviceAuthRequestData: deviceAuthRequestData
        )
        
        let viewController = PhoneNumberValidationViewController(interactor: interactor)

        coordinator.viewController = viewController
        coordinator.delegate = coordinatorDelegate
        presenter.viewController = viewController

        return viewController
    }
}
