import Core
import Foundation

protocol DeviceAuthOnboardingPresenting: AnyObject {
    func didNextStep(action: DeviceAuthOnboardingAction)
}

final class DeviceAuthOnboardingPresenter: DeviceAuthOnboardingPresenting {
    private let coordinator: DeviceAuthOnboardingCoordinating

    init(coordinator: DeviceAuthOnboardingCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: DeviceAuthOnboardingAction) {
        coordinator.perform(action: action)
    }
}
