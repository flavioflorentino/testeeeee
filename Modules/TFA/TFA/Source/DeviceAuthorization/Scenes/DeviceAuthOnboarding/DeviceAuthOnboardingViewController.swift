import AssetsKit
import UI
import UIKit

final class DeviceAuthOnboardingViewController: ViewController<DeviceAuthOnboardingInteracting, UIView> {
    private lazy var centerContainerView = UIView()
    private lazy var onboardingImage = UIImageView(image: Assets.tfaDevice.image)
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, .grayscale750())
            .with(\.textAlignment, .center)
        label.text = Strings.Onboarding.header
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.textColor, .grayscale750())
            .with(\.textAlignment, .center)
        label.text = Strings.Onboarding.body
        return label
    }()
    
    private lazy var continueButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.General.continue, for: .normal)
        button.addTarget(self, action: #selector(continueButtonTapped(_:)), for: .touchUpInside)
        button.buttonStyle(PrimaryButtonStyle())
        return button
    }()
    
    private lazy var informationButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.Onboarding.infoButtonTitle, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(informationButtonTapped(_:)), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.sendAnalytics()
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            image: Resources.Icons.icoClose.image.withRenderingMode(.alwaysOriginal),
            landscapeImagePhone: nil,
            style: .plain,
            target: self,
            action: #selector(closeButtonTapped(_:))
        )
    }
    
    override func buildViewHierarchy() {
        view.addSubview(centerContainerView)
        centerContainerView.addSubview(onboardingImage)
        centerContainerView.addSubview(titleLabel)
        centerContainerView.addSubview(descriptionLabel)
        centerContainerView.addSubview(continueButton)
        view.addSubview(informationButton)
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }

    override func setupConstraints() {
        centerContainerView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.centerY.equalToSuperview()
        }
        
        onboardingImage.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.centerX.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(onboardingImage.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        continueButton.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview()
        }
        
        informationButton.snp.makeConstraints {
            $0.bottom.equalToSuperview().offset(-Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
}

// MARK: Private methods
@objc
private extension DeviceAuthOnboardingViewController {
    func closeButtonTapped(_ sender: UIButton) {
        interactor.closeOnboarding()
    }
    
    func informationButtonTapped(_ sender: UIButton) {
        interactor.showHelpInfo()
    }
    
    func continueButtonTapped(_ sender: UIButton) {
        interactor.continueValidation()
    }
}
