import Foundation

enum DeviceAuthOnboardingFactory {
    static func make(coordinatorDelegate: DeviceAuthOnboardingCoordinatorDelegate) -> DeviceAuthOnboardingViewController {
        let container = TFADependencyContainer()
        let coordinator: DeviceAuthOnboardingCoordinating = DeviceAuthOnboardingCoordinator()
        let presenter: DeviceAuthOnboardingPresenting = DeviceAuthOnboardingPresenter(coordinator: coordinator)
        let viewModel = DeviceAuthOnboardingInteractor(presenter: presenter, dependencies: container)
        let viewController = DeviceAuthOnboardingViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        coordinator.delegate = coordinatorDelegate

        return viewController
    }
}
