import UIKit

enum DeviceAuthOnboardingAction {
    case close
    case showHelp
    case continueValidation
}

protocol DeviceAuthOnboardingCoordinatorDelegate: AnyObject {
    func onboardingClose()
    func onboardingShowHelp()
    func onboardingNextStep()
}

protocol DeviceAuthOnboardingCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: DeviceAuthOnboardingCoordinatorDelegate? { get set }
    func perform(action: DeviceAuthOnboardingAction)
}

final class DeviceAuthOnboardingCoordinator: DeviceAuthOnboardingCoordinating {
    weak var viewController: UIViewController?
    weak var delegate: DeviceAuthOnboardingCoordinatorDelegate?
    
    func perform(action: DeviceAuthOnboardingAction) {
        switch action {
        case .close:
            delegate?.onboardingClose()
        case .showHelp:
            delegate?.onboardingShowHelp()
        case .continueValidation:
            delegate?.onboardingNextStep()
        }
    }
}
