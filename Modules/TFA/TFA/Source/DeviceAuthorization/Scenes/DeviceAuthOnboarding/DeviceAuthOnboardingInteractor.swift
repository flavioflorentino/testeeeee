import AnalyticsModule
import Foundation

protocol DeviceAuthOnboardingInteracting: AnyObject {
    func sendAnalytics()
    func closeOnboarding()
    func showHelpInfo()
    func continueValidation()
}

final class DeviceAuthOnboardingInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let presenter: DeviceAuthOnboardingPresenting

    init(presenter: DeviceAuthOnboardingPresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

extension DeviceAuthOnboardingInteractor: DeviceAuthOnboardingInteracting {
    func sendAnalytics() {
        let eventTracker = TFATracker(eventType: OnboardingEvent.started)
        dependencies.analytics.log(eventTracker)
    }
    
    func closeOnboarding() {
        presenter.didNextStep(action: .close)
    }
    
    func showHelpInfo() {
        presenter.didNextStep(action: .showHelp)
    }
    
    func continueValidation() {
        presenter.didNextStep(action: .continueValidation)
    }
}
