import Foundation

struct AccountData {
    let agency: String
    let agencyDigit: String?
    let account: String
    let accountDigit: String
}
