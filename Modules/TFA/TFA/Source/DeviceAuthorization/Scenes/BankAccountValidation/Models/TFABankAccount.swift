import Foundation

struct TFABankAccount: Decodable {
    let bank: TFABankInfo
}

struct TFABankInfo: Decodable {
    let bankName: String
    let imageUrl: String?
    let agencyDv: Bool
}
