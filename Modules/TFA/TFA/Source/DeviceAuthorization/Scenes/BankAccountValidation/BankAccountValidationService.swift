import Core
import Foundation

protocol BankAccountValidationServicing {
    func getBankAccountData(requestData: DeviceAuthRequestData, completion: @escaping (Result<TFABankAccount, ApiError>) -> Void)
    func validateBankAccountData(
        bankAccount: AccountData,
        requestData: DeviceAuthRequestData,
        completion: @escaping (Result<DeviceAuthValidationResponse, ApiError>) -> Void
    )
}

final class BankAccountValidationService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
  
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - BankAccountValidationServicing
extension BankAccountValidationService: BankAccountValidationServicing {
    func getBankAccountData(requestData: DeviceAuthRequestData, completion: @escaping (Result<TFABankAccount, ApiError>) -> Void) {
        let endpoint = DeviceAuthEndpoint.getBankAccountInfo(requestData: requestData)
        let api = Api<TFABankAccount>(endpoint: endpoint)
        
        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func validateBankAccountData(
        bankAccount: AccountData,
        requestData: DeviceAuthRequestData,
        completion: @escaping (Result<DeviceAuthValidationResponse, ApiError>) -> Void
    ) {
        let endpoint = DeviceAuthEndpoint.validateBankAccount(bankAccount: bankAccount, requestData: requestData)
        let api = Api<DeviceAuthValidationResponse>(endpoint: endpoint)
        
        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
