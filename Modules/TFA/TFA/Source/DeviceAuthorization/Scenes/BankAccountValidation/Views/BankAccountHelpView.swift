import Foundation
import UI
import UIKit
import AssetsKit

protocol BankAccountHelpViewDelegate: AnyObject {
    func didTouchHelpButton(banckAccountView: BankAccountHelpView)
}

final class BankAccountHelpView: UIView, ViewConfiguration {
    private lazy var bankImage: UIImageView = {
        let imageView = UIImageView()
        imageView.cornerRadius = .full
        return imageView
    }()

    private lazy var bankName: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .default))
            .with(\.textColor, .grayscale700())
        return label
    }()

    private lazy var infoButton: UIButton = {
        let button = UIButton()
        button.setImage(Resources.Icons.icoQuestion.image, for: .normal)
        button.addTarget(self, action: #selector(helpButtonTapped(_:)), for: .touchUpInside)
        return button
    }()

    weak var delegate: BankAccountHelpViewDelegate?

    init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubview(bankImage)
        addSubview(bankName)
        addSubview(infoButton)
    }
    
    func setupConstraints() {
        bankImage.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base02)
            $0.trailing.equalTo(bankName.snp.leading).offset(-Spacing.base01)
            $0.size.equalTo(Sizing.ImageView.xSmall.rawValue)
        }
        
        bankName.snp.makeConstraints {
            $0.centerY.equalTo(bankImage.snp.centerY)
            $0.trailing.equalTo(infoButton.snp.leading).offset(-Spacing.base01)
        }

        infoButton.snp.makeConstraints {
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.centerY.equalTo(bankImage.snp.centerY)
            $0.width.equalTo(Spacing.base03)
            $0.height.equalTo(Spacing.base03)
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.grayscale050.color
        layer.cornerRadius = CornerRadius.medium
    }

    @objc
    private func helpButtonTapped(_ sender: UIButton) {
        delegate?.didTouchHelpButton(banckAccountView: self)
    }
    
    func setBankInfo(_ bankInfo: TFABankInfo?) {
        bankName.text = bankInfo?.bankName ?? Strings.BankAccount.registeredBank
        bankImage.setImage(url: URL(string: bankInfo?.imageUrl ?? ""), placeholder: Resources.Placeholders.bank.image)
    }
}
