import UI
import UIKit

enum BankAccountValidationAction: Equatable {
    case back
    case nextStep(flow: DeviceAuthFlow, step: DeviceAuthStep)
}

protocol BankAccountValidationCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: DeviceAuthFlowStepping? { get set }
    func perform(action: BankAccountValidationAction)
}

final class BankAccountValidationCoordinator {
    weak var viewController: UIViewController?
    weak var delegate: DeviceAuthFlowStepping?
}

// MARK: - BankAccountValidationCoordinating
extension BankAccountValidationCoordinator: BankAccountValidationCoordinating {
    func perform(action: BankAccountValidationAction) {
        switch action {
        case .back:
            delegate?.validationDidGoBack()
        case let .nextStep(flow, step):
            delegate?.receivedNextStep(forFlow: flow, step: step)
        }
    }
}
