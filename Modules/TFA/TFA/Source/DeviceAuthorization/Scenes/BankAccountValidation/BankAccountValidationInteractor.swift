import Core
import Foundation
import Validations

protocol BankAccountValidationInteracting: AnyObject {
    func backButtonWasTapped()
    func goBack()
    func loadBankInfo()
    func checkAgencyNumber(_ text: String)
    func checkAgencyDigit(_ text: String)
    func checkAccountNumber(_ text: String)
    func checkAccountDigit(_ text: String)
    func validateForm()
    func startRetryValidationTimer(retryTime: Int)
}

final class BankAccountValidationInteractor {
    private let service: BankAccountValidationServicing
    private let presenter: BankAccountValidationPresenting
    
    private let deviceAuthRequestData: DeviceAuthRequestData
    
    private var validationAgencyNumber = ""
    private var validationAgencyDigit = ""
    private var validationAccountNumber = ""
    private var validationAccountDigit = ""
  
    private var hasAgencyDigit = false
    
    private var timer: TFATimerProtocol
    private let timeLimitToValidate = 180
    private var countdownSeconds: Int
    
    init(
        service: BankAccountValidationServicing,
        presenter: BankAccountValidationPresenting,
        deviceAuthRequestData: DeviceAuthRequestData,
        timer: TFATimerProtocol = TFATimer()
    ) {
        self.service = service
        self.presenter = presenter
        self.deviceAuthRequestData = deviceAuthRequestData
        self.timer = timer
        self.countdownSeconds = timeLimitToValidate
        self.timer.delegate = self
    }
}

// MARK: - BankAccountValidationInteracting
extension BankAccountValidationInteractor: BankAccountValidationInteracting {
    func backButtonWasTapped() {
        presenter.presentGoBackAlert()
    }
    
    func goBack() {
        presenter.didNextStep(action: .back)
    }
    
    func loadBankInfo() {
        presenter.presentLoading(true)
        
        service.getBankAccountData(requestData: deviceAuthRequestData) { [weak self] result in
            self?.presenter.presentLoading(false)
            if case let .success(bankAccount) = result {
                self?.hasAgencyDigit = bankAccount.bank.agencyDv
                self?.presenter.presentBankInfo(bankAccountInfo: bankAccount)
            }
        }
    }
    
    func checkAgencyNumber(_ text: String) {
        validationAgencyNumber = text
        
        let isValidAgencyNumber = minRangeValidation(validationAgencyNumber)
        presenter.updateAgencyNumberField(withError: isValidAgencyNumber ? nil : Strings.BankAccount.agencyRequired)
        
        checkFieldsAndEnableContinue()
    }
    
    func checkAgencyDigit(_ text: String) {
        validationAgencyDigit = text
        
        let isValidAgencyDigit = minMaxRangeValidation(validationAgencyDigit)
        presenter.updateAgencyDigitNumberField(withError: isValidAgencyDigit ? nil : Strings.BankAccount.digitRequired)
        
        checkFieldsAndEnableContinue()
    }
    
    func checkAccountNumber(_ text: String) {
        validationAccountNumber = text
        
        let isValidAccountNumber = minRangeValidation(validationAccountNumber)
        presenter.updateAccountNumberField(withError: isValidAccountNumber ? nil : Strings.BankAccount.accountNumberRequired)
        
        checkFieldsAndEnableContinue()
    }
    
    func checkAccountDigit(_ text: String) {
        validationAccountDigit = text
        
        let isValidAccountDigit = minMaxRangeValidation(validationAccountDigit)
        presenter.updateAccountDigitNumberField(withError: isValidAccountDigit ? nil : Strings.BankAccount.digitRequired)
        
        checkFieldsAndEnableContinue()
    }
    
    func validateForm() {
        presenter.presentLoading(true)
        
        let account = AccountData(
            agency: validationAgencyNumber,
            agencyDigit: validationAgencyDigit, 
            account: validationAccountNumber,
            accountDigit: validationAccountDigit
        )
        
        service.validateBankAccountData(bankAccount: account, requestData: deviceAuthRequestData) { [weak self] result in
            self?.presenter.presentLoading(false)
            
            switch result {
            case .success(let response):
                self?.didValidateAccountData(for: response.nextStep)
            case .failure(let error):
                self?.parseError(error: error)
            }
        }
    }
    
    func startRetryValidationTimer(retryTime: Int) {
        countdownSeconds = retryTime
        timer.start()
    }
}

// MARK: - Timer Delegate
extension BankAccountValidationInteractor: TFATimerDelegate {
    func timerTic() {
        if countdownSeconds <= 0 {
            timer.stop()
            presenter.presentFormValidationEnabled()
        } else {
            countdownSeconds -= 1
            presenter.presentContinueButtonUpdated(with: countdownSeconds)
        }
    }
}

// MARK: - Private Methods
private extension BankAccountValidationInteractor {
    func minRangeValidation(_ validationText: String) -> Bool {
        let validator = RangeValidator(minCount: 1, maxCount: nil)
        do {
            try validator.validate(validationText)
            return true
        } catch {
            return false
        }
    }
    
    func minMaxRangeValidation(_ validationText: String) -> Bool {
        let validator = RangeValidator(minCount: 1, maxCount: 1)
        do {
            try validator.validateForInterval(validationText)
            return true
        } catch {
            return false
        }
    }
    
    func checkFieldsAndEnableContinue() {
        let isValidAgency = minRangeValidation(validationAgencyNumber)
        let isValidAgencyDigit = minMaxRangeValidation(validationAgencyDigit) || !hasAgencyDigit
        let isValidAccount = minRangeValidation(validationAccountNumber)
        let isValidAccountDigit = minMaxRangeValidation(validationAccountDigit)
        
        let enable = isValidAgency && isValidAgencyDigit && isValidAccount && isValidAccountDigit
        presenter.shouldEnableContinueButton(enable)
    }
    
    func didValidateAccountData(for nextStep: DeviceAuthStep) {
        presenter.didNextStep(action: .nextStep(flow: deviceAuthRequestData.flow, step: nextStep))
    }
    
    func parseError(error: ApiError) {
        switch error {
        case .tooManyRequests(let requestError), .unauthorized(let requestError), .badRequest(let requestError):
            guard let retryTime = requestError.data["retryTime"] as? Int else {
                presenter.presentErrorAlert(title: requestError.title, message: requestError.message)
                return
            }
            presenter.presentAttempLimitError(tfaError: requestError, retryTime: retryTime)
        default:
            presenter.presentErrorAlert(title: Strings.Alerts.genericErrorTitle, message: Strings.Alerts.genericErrorMessage)
        }
    }
}
