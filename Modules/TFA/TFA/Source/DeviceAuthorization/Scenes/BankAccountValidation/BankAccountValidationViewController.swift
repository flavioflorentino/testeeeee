import SnapKit
import UI
import UIKit

protocol BankAccountValidationDisplaying: AnyObject {
    func displayLoading(_ isLoading: Bool)
    func displayBankInfo(bankInfo: TFABankInfo)
    func displayCancelAuthorizationAlert()
    func updateAgencyFieldErrorMessage(_ message: String?)
    func updateAgencyDigitFieldErrorMessage(_ message: String?)
    func updateAccountFieldErrorMessage(_ message: String?)
    func updateAccountDigitFieldErrorMessage(_ message: String?)
    func enableContinueButton(_ enable: Bool)
    func displayAttempLimitError(with title: String, message: String, retryTime: Int)
    func displayAlert(title: String, message: String)
    func displayCountdownTimer(with remainingTime: String)
    func enableFormValidation()
}

final class BankAccountValidationViewController: ViewController<BankAccountValidationInteracting, UIView>, LoadingViewProtocol {
    fileprivate enum Constants {
        static let digitLength = 1
    }
    
    lazy var loadingView = LoadingView()
    
    private lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.backgroundColor = .clear
        return scroll
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, .grayscale750())
            .with(\.textAlignment, .left)
        label.text = Strings.BankAccount.header
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var bankAccountView: BankAccountHelpView = {
        let bankHelpView = BankAccountHelpView()
        bankHelpView.delegate = self
        return bankHelpView
    }()
    
    private lazy var agencyTextfield: UIPPFloatingTextField = {
        let textfield = TFATextFieldFactory.defaultField()
        textfield.addTarget(self, action: #selector(checkAgencyNumberTextField), for: .editingChanged)
        textfield.delegate = self
        textfield.keyboardType = .numberPad
        textfield.placeholder = Strings.BankAccount.agencyPlaceholder
        return textfield
    }()
    
    private lazy var agencyDigitTextfield: UIPPFloatingTextField = {
        let textfield = TFATextFieldFactory.defaultField()
        textfield.addTarget(self, action: #selector(checkAgencyDigitTextField), for: .editingChanged)
        textfield.delegate = self
        textfield.isHidden = true
        textfield.placeholder = Strings.BankAccount.digitPlaceholder
        return textfield
    }()
    
    private lazy var agencyStackView: UIStackView = {
        let stack = UIStackView()
        stack.alignment = .leading
        stack.axis = .horizontal
        stack.spacing = Spacing.base02
        stack.distribution = .fill
        return stack
    }()
    
    private lazy var accountTextfield: UIPPFloatingTextField = {
        let textfield = TFATextFieldFactory.defaultField()
        textfield.addTarget(self, action: #selector(checkAccountNumberTextField), for: .editingChanged)
        textfield.delegate = self
        textfield.keyboardType = .numberPad
        textfield.placeholder = Strings.BankAccount.accountPlaceholder
        return textfield
    }()
    
    private lazy var accountDigitTextfield: UIPPFloatingTextField = {
        let textfield = TFATextFieldFactory.defaultField()
        textfield.addTarget(self, action: #selector(checkAccountDigitTextField), for: .editingChanged)
        textfield.delegate = self
        textfield.placeholder = Strings.BankAccount.digitPlaceholder
        return textfield
    }()
    
    private lazy var accountStackView: UIStackView = {
        let stack = UIStackView()
        stack.alignment = .leading
        stack.axis = .horizontal
        stack.spacing = Spacing.base02
        stack.distribution = .fill
        return stack
    }()
    
    private lazy var continueButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.General.continue, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.isEnabled = false
        button.addTarget(self, action: #selector(didTapContinueButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var keyboardScrollViewHandler = KeyboardScrollViewHandler(scrollView: scrollView)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        keyboardScrollViewHandler.registerForKeyboardNotifications()
        interactor.loadBankInfo()
    }

    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(bankAccountView)
        contentView.addSubview(agencyStackView)
        agencyStackView.addArrangedSubview(agencyTextfield)
        agencyStackView.addArrangedSubview(agencyDigitTextfield)
        contentView.addSubview(accountStackView)
        accountStackView.addArrangedSubview(accountTextfield)
        accountStackView.addArrangedSubview(accountDigitTextfield)
        contentView.addSubview(continueButton)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        contentView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.width.equalTo(view.snp.width)
        }

        titleLabel.snp.makeConstraints {
            $0.bottom.equalTo(bankAccountView.snp.top).offset(-Spacing.base03)
            $0.top.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        bankAccountView.snp.makeConstraints {
            $0.bottom.equalTo(agencyStackView.snp.top).offset(-Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        agencyStackView.snp.makeConstraints {
            $0.bottom.equalTo(accountStackView.snp.top).offset(-Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        accountStackView.snp.makeConstraints {
            $0.bottom.equalTo(continueButton.snp.top).offset(-Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        agencyDigitTextfield.snp.makeConstraints {
            $0.width.equalTo(Sizing.base12)
        }
        
        accountDigitTextfield.snp.makeConstraints {
            $0.width.equalTo(Sizing.base12)
        }
        
        continueButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Sizing.Button.default.rawValue)
            $0.bottom.equalToSuperview().offset(-Spacing.base03)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        navigationItem.leftBarButtonItem = TFANavBarBackButton(target: self, action: #selector(backButtonTapped))
    }
}

// MARK: - Private methods
@objc
private extension BankAccountValidationViewController {
    func backButtonTapped() {
        interactor.backButtonWasTapped()
    }

    func checkAgencyNumberTextField() {
        interactor.checkAgencyNumber(agencyTextfield.text ?? "")
    }
    
    func checkAgencyDigitTextField() {
        interactor.checkAgencyDigit(agencyDigitTextfield.text ?? "")
    }
    
    func checkAccountNumberTextField() {
        interactor.checkAccountNumber(accountTextfield.text ?? "")
    }
    
    func checkAccountDigitTextField() {
        interactor.checkAccountDigit(accountDigitTextfield.text ?? "")
    }
    
    func didTapContinueButton() {
        view.endEditing(true)
        interactor.validateForm()
    }
    
    private func enableFields(enable: Bool) {
        agencyTextfield.isEnabled = enable
        agencyDigitTextfield.isEnabled = enable
        accountTextfield.isEnabled = enable
        accountDigitTextfield.isEnabled = enable
    }
    
    private func enableValidation(_ isEnabled: Bool) {
        continueButton.isEnabled = isEnabled
        continueButton.setTitle(Strings.General.continue, for: .disabled)
        continueButton.setTitle(Strings.General.continue, for: .normal)
        enableFields(enable: isEnabled)
    }
}

// MARK: - BankAccountValidationDisplaying
extension BankAccountValidationViewController: BankAccountValidationDisplaying {
    func displayLoading(_ isLoading: Bool) {
        isLoading ? startLoadingView() : stopLoadingView()
    }
    
    func displayBankInfo(bankInfo: TFABankInfo) {
        bankAccountView.setBankInfo(bankInfo)
        agencyDigitTextfield.isHidden = !bankInfo.agencyDv
    }
    
    func displayCancelAuthorizationAlert() {
        let continueAction = ApolloAlertAction(title: Strings.Alerts.CancelAuthorization.continue, dismissOnCompletion: true) { }
        
        let cancelAction = ApolloAlertAction(
            title: Strings.Alerts.CancelAuthorization.cancel,
            dismissOnCompletion: true) { [weak self] in
                self?.interactor.goBack()
        }

        showApolloAlert(
            title: Strings.Alerts.CancelAuthorization.title,
            subtitle: Strings.Alerts.CancelAuthorization.message,
            primaryButtonAction: continueAction,
            linkButtonAction: cancelAction,
            dismissOnTouchOutside: false
        )
    }
    
    func updateAgencyFieldErrorMessage(_ message: String?) {
        agencyTextfield.errorMessage = message
    }
    
    func updateAgencyDigitFieldErrorMessage(_ message: String?) {
        agencyDigitTextfield.errorMessage = message
    }
    
    func updateAccountFieldErrorMessage(_ message: String?) {
        accountTextfield.errorMessage = message
    }
    
    func updateAccountDigitFieldErrorMessage(_ message: String?) {
        accountDigitTextfield.errorMessage = message
    }
    
    func enableContinueButton(_ enable: Bool) {
        continueButton.isEnabled = enable
    }
    
    func displayAlert(title: String, message: String) {
        let backAction = ApolloAlertAction(title: Strings.General.back, dismissOnCompletion: true) { }
        showApolloAlert(title: title, subtitle: message, primaryButtonAction: backAction, dismissOnTouchOutside: false)
    }
    
    func displayAttempLimitError(with title: String, message: String, retryTime: Int) {
        enableValidation(false)
        interactor.startRetryValidationTimer(retryTime: retryTime)

        displayAlert(title: title, message: message)
    }
    
    func displayCountdownTimer(with remainingTime: String) {
        continueButton.setTitle(remainingTime, for: .disabled)
    }
    
    func enableFormValidation() {
        enableValidation(true)
    }
}

extension BankAccountValidationViewController: BankAccountHelpViewDelegate {
    func didTouchHelpButton(banckAccountView: BankAccountHelpView) {
        let okAction = ApolloAlertAction(title: Strings.BankAccount.alertActionText, dismissOnCompletion: true) { }
        
        showApolloAlert(
            title: Strings.BankAccount.alertTitle,
            subtitle: Strings.BankAccount.alertMessage,
            primaryButtonAction: okAction,
            dismissOnTouchOutside: false
        )
    }
}

extension BankAccountValidationViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.isEmpty {
            return true
        }
        
        guard let text = textField.text else {
            return false
        }
        
        switch textField {
        case agencyDigitTextfield, accountDigitTextfield:
            return text.count < Constants.digitLength
        default:
            return true
        }
    }
}
