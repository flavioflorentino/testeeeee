import Core
import Foundation

protocol BankAccountValidationPresenting: AnyObject {
    var viewController: BankAccountValidationDisplaying? { get set }
    func didNextStep(action: BankAccountValidationAction)
    func presentBankInfo(bankAccountInfo: TFABankAccount)
    func presentLoading(_ isLoading: Bool)
    func presentGoBackAlert()
    func updateAgencyNumberField(withError message: String?)
    func updateAgencyDigitNumberField(withError message: String?)
    func updateAccountNumberField(withError message: String?)
    func updateAccountDigitNumberField(withError message: String?)
    func shouldEnableContinueButton(_ isEnabled: Bool)
    func presentAttempLimitError(tfaError: RequestError, retryTime: Int)
    func presentErrorAlert(title: String, message: String)
    func presentContinueButtonUpdated(with remainingTime: Int)
    func presentFormValidationEnabled()
}

final class BankAccountValidationPresenter {
    private let coordinator: BankAccountValidationCoordinating
    weak var viewController: BankAccountValidationDisplaying?
    private let timeFormatter: TFATimeFormatterProtocol
    
    init(coordinator: BankAccountValidationCoordinating, timeFormatter: TFATimeFormatterProtocol = TFATimeFormatter()) {
        self.coordinator = coordinator
        self.timeFormatter = timeFormatter
    }
}

// MARK: - BankAccountValidationPresenting
extension BankAccountValidationPresenter: BankAccountValidationPresenting {
    func didNextStep(action: BankAccountValidationAction) {
        coordinator.perform(action: action)
    }
    
    func presentBankInfo(bankAccountInfo: TFABankAccount) {
        viewController?.displayBankInfo(bankInfo: bankAccountInfo.bank)
    }
    
    func presentLoading(_ isLoading: Bool) {
        viewController?.displayLoading(isLoading)
    }
    
    func presentGoBackAlert() {
        viewController?.displayCancelAuthorizationAlert()
    }
    
    func updateAgencyNumberField(withError message: String?) {
        viewController?.updateAgencyFieldErrorMessage(message)
    }
    
    func updateAgencyDigitNumberField(withError message: String?) {
        viewController?.updateAgencyDigitFieldErrorMessage(message)
    }

    func updateAccountNumberField(withError message: String?) {
        viewController?.updateAccountFieldErrorMessage(message)
    }
    
    func updateAccountDigitNumberField(withError message: String?) {
        viewController?.updateAccountDigitFieldErrorMessage(message)
    }
    
    func shouldEnableContinueButton(_ isEnabled: Bool) {
        viewController?.enableContinueButton(isEnabled)
    }
    
    func presentErrorAlert(title: String, message: String) {
        viewController?.displayAlert(title: title, message: message)
    }
    
    func presentAttempLimitError(tfaError: RequestError, retryTime: Int) {
        viewController?.displayAttempLimitError(with: tfaError.title, message: tfaError.message, retryTime: retryTime)
    }
    
    func presentContinueButtonUpdated(with remainingTime: Int) {
        let formattedTime = timeFormatter.formatString(from: TimeInterval(remainingTime))
        let buttonText = Strings.General.wait(formattedTime)
        viewController?.displayCountdownTimer(with: buttonText)
    }
    
    func presentFormValidationEnabled() {
        viewController?.enableFormValidation()
    }
}
