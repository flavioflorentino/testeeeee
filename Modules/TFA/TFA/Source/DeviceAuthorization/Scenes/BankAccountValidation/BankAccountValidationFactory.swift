import UIKit

enum BankAccountValidationFactory {
    static func make(
        coordinatorDelegate: DeviceAuthFlowStepping,
        deviceAuthRequestData: DeviceAuthRequestData
    ) -> BankAccountValidationViewController {
        let container = TFADependencyContainer()
        let service: BankAccountValidationServicing = BankAccountValidationService(dependencies: container)
        let coordinator: BankAccountValidationCoordinating = BankAccountValidationCoordinator()
        let presenter: BankAccountValidationPresenting = BankAccountValidationPresenter(coordinator: coordinator)
        
        let interactor = BankAccountValidationInteractor(
            service: service,
            presenter: presenter,
            deviceAuthRequestData: deviceAuthRequestData
        )
        
        let viewController = BankAccountValidationViewController(interactor: interactor)

        coordinator.viewController = viewController
        coordinator.delegate = coordinatorDelegate
        presenter.viewController = viewController

        return viewController
    }
}
