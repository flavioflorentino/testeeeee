import UIKit

enum ValidationSuccessFactory {
    static func make(
        coordinatorDelegate: ValidationSuccessCoordinatorDelegate,
        deviceAuthRequestData: DeviceAuthRequestData
    ) -> ValidationSuccessViewController {
        let coordinator: ValidationSuccessCoordinating = ValidationSuccessCoordinator()
        let presenter: ValidationSuccessPresenting = ValidationSuccessPresenter(coordinator: coordinator)
        let interactor = ValidationSuccessInteractor(presenter: presenter, deviceAuthRequestData: deviceAuthRequestData)
        let viewController = ValidationSuccessViewController(interactor: interactor)

        coordinator.viewController = viewController
        coordinator.delegate = coordinatorDelegate

        return viewController
    }
}
