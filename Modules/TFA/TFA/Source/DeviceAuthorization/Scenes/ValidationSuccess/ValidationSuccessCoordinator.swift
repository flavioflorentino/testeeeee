import UIKit

enum ValidationSuccessAction {
    case enter
}

protocol ValidationSuccessCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: ValidationSuccessCoordinatorDelegate? { get set }
    func perform(action: ValidationSuccessAction)
}

protocol ValidationSuccessCoordinatorDelegate: AnyObject {
    func successDidEnter()
}

final class ValidationSuccessCoordinator {
    weak var viewController: UIViewController?
    weak var delegate: ValidationSuccessCoordinatorDelegate?
}

// MARK: - ValidationSuccessCoordinating
extension ValidationSuccessCoordinator: ValidationSuccessCoordinating {
    func perform(action: ValidationSuccessAction) {
        if case .enter = action {
            delegate?.successDidEnter()
        }
    }
}
