import Core
import Foundation

protocol ValidationSuccessInteracting: AnyObject {
    func didEnter()
}

final class ValidationSuccessInteractor {
    private let presenter: ValidationSuccessPresenting

    private let deviceAuthRequestData: DeviceAuthRequestData
    
    init(presenter: ValidationSuccessPresenting, deviceAuthRequestData: DeviceAuthRequestData) {
        self.presenter = presenter
        self.deviceAuthRequestData = deviceAuthRequestData
    }
}

// MARK: - ValidationSuccessInteracting
extension ValidationSuccessInteractor: ValidationSuccessInteracting {
    func didEnter() {
        presenter.didNextStep(action: .enter)
    }
}
