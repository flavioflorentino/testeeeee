import Foundation

protocol ValidationSuccessPresenting: AnyObject {
    func didNextStep(action: ValidationSuccessAction)
}

final class ValidationSuccessPresenter {
    private let coordinator: ValidationSuccessCoordinating

    init(coordinator: ValidationSuccessCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - ValidationSuccessPresenting
extension ValidationSuccessPresenter: ValidationSuccessPresenting {
    func didNextStep(action: ValidationSuccessAction) {
        coordinator.perform(action: action)
    }
}
