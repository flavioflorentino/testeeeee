import UI
import UIKit

final class ValidationSuccessViewController: ViewController<ValidationSuccessInteracting, UIView> {
    private lazy var centerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .fill
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.spacing = Spacing.base03
        return stackView
    }()
    
    private lazy var successImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = Assets.tfaSuccess.image
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textColor, .black())
            .with(\.textAlignment, .center)
        label.text = Strings.Success.header
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        label.text = Strings.Success.body
        return label
    }()
    
    private lazy var enterButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Strings.Success.enter, for: .normal)
        button.addTarget(self, action: #selector(enterButtonTapped), for: .touchUpInside)
        return button
    }()

    override func buildViewHierarchy() {
        view.addSubview(centerStackView)
        centerStackView.addArrangedSubview(successImageView)
        centerStackView.addArrangedSubview(titleLabel)
        centerStackView.addArrangedSubview((descriptionLabel))
        centerStackView.addArrangedSubview(enterButton)
    }
    
    override func setupConstraints() {
        centerStackView.snp.makeConstraints {
            $0.centerX.centerY.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        navigationItem.hidesBackButton = true
    }
}

// MARK: - Private methods
@objc
private extension ValidationSuccessViewController {
    func enterButtonTapped() {
        interactor.didEnter()
    }
}
