import Core
import Foundation

protocol PersonalDataValidationPresenting: AnyObject {
    var viewController: PersonalDataValidationDisplaying? { get set }
    func didNextStep(action: PersonalDataValidationAction)
    func presentLoading(_ loading: Bool)
    func updateEmailHint(_ hint: String)
    func updateEmailField(withError message: String?)
    func updateDocumentField(withError message: String?)
    func updateBirthdateField(withError message: String?)
    func updateMotherNameField(withError message: String?)
    func shouldEnableContinueButton(_ isEnabled: Bool)
    func presentAttempLimitError(tfaError: RequestError, retryTime: Int)
    func presentContinueButtonUpdated(with remainingTime: Int)
    func presentErrorAlert(title: String, message: String)
    func presentFormValidationEnabled()
    func presentGoBackAlert()
}

final class PersonalDataValidationPresenter {
    private let coordinator: PersonalDataValidationCoordinating
    weak var viewController: PersonalDataValidationDisplaying?
    private let timeFormatter: TFATimeFormatterProtocol

    init(coordinator: PersonalDataValidationCoordinating, timeFormatter: TFATimeFormatterProtocol = TFATimeFormatter()) {
        self.coordinator = coordinator
        self.timeFormatter = timeFormatter
    }
}

// MARK: - PersonalDataValidationPresenting
extension PersonalDataValidationPresenter: PersonalDataValidationPresenting {
    func didNextStep(action: PersonalDataValidationAction) {
        coordinator.perform(action: action)
    }
    
    func presentLoading(_ loading: Bool) {
        viewController?.displayLoading(loading)
    }
    
    func updateEmailHint(_ hint: String) {
        viewController?.displayEmailHint(hint)
    }
    
    func updateEmailField(withError message: String?) {
        viewController?.updateEmailFieldErrorMessage(message)
    }
    
    func updateDocumentField(withError message: String?) {
        viewController?.updateDocumentFieldErrorMessage(message)
    }
    
    func updateBirthdateField(withError message: String?) {
        viewController?.updateBirthdateFieldErrorMessage(message)
    }
    
    func updateMotherNameField(withError message: String?) {
        viewController?.updateMotherNameFieldErrorMessage(message)
    }
    
    func shouldEnableContinueButton(_ isEnabled: Bool) {
        viewController?.enableContinueButton(enable: isEnabled)
    }
    
    func presentAttempLimitError(tfaError: RequestError, retryTime: Int) {
        viewController?.displayAttempLimitError(with: tfaError.title, message: tfaError.message, retryTime: retryTime)
    }
    
    func presentErrorAlert(title: String, message: String) {
        viewController?.displayAlert(title: title, message: message)
    }
    
    func presentContinueButtonUpdated(with remainingTime: Int) {
        let formattedTime = timeFormatter.formatString(from: TimeInterval(remainingTime))
        let buttonText = Strings.General.wait(formattedTime)
        viewController?.displayCountdownTimer(with: buttonText)
    }
    
    func presentFormValidationEnabled() {
        viewController?.enableFormValidation()
    }
    
    func presentGoBackAlert() {
        viewController?.displayCancelAuthorizationAlert()
    }
}
