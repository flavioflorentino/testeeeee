import Core
import Foundation
import Validations

protocol PersonalDataValidationInteracting: AnyObject {
    func goBack()
    func getPersonalDataInfo()
    func emailBeginEditing()
    func checkMotherName(_ text: String)
    func checkEmail(_ text: String)
    func checkDocument(_ text: String)
    func checkBirthday(_ text: String)
    func validateForm(document: String, birthDate: String, motherName: String, email: String)
    func startRetryValidationTimer(retryTime: Int)
    func backButtonWasTapped()
}

final class PersonalDataValidationInteractor {
    private let service: PersonalDataValidationServicing
    private let presenter: PersonalDataValidationPresenting
    
    private let deviceAuthRequestData: DeviceAuthRequestData
    private var emailHint: String?
    
    private var validationDocument = ""
    private var validationBirthdate = ""
    private var validationMotherName = ""
    private var validationEmail = ""
    
    private var timer: TFATimerProtocol
    private var countdownSeconds = 180
    
    init(
        service: PersonalDataValidationServicing,
        presenter: PersonalDataValidationPresenting,
        deviceAuthRequestData: DeviceAuthRequestData,
        timer: TFATimerProtocol = TFATimer()
    ) {
        self.service = service
        self.presenter = presenter
        self.deviceAuthRequestData = deviceAuthRequestData
        self.timer = timer
        self.timer.delegate = self
    }
}

// MARK: - PersonalDataValidationInteracting
extension PersonalDataValidationInteractor: PersonalDataValidationInteracting {
    func backButtonWasTapped() {
        presenter.presentGoBackAlert()
    }
    
    func goBack() {
        presenter.didNextStep(action: .back)
    }
    
    func getPersonalDataInfo() {
        presenter.presentLoading(true)
        service.getPersonalData(requestData: deviceAuthRequestData) { [weak self] result in
            self?.presenter.presentLoading(false)
            if case let .success(consumer) = result {
                self?.emailHint = consumer.consumerData.email
            }
        }
    }
    
    func emailBeginEditing() {
        guard let hint = emailHint else {
            return
        }
        presenter.updateEmailHint(hint)
    }
    
    func checkMotherName(_ text: String) {
        let validText = text.replacingOccurrences(of: "[^a-zA-Z\u{00C0}-\u{00FF} ]", with: "", options: .regularExpression)
        validationMotherName = validText
        let validName = isValidMotherName(validationMotherName) == nil
        
        presenter.updateMotherNameField(withError: validName ? nil : Strings.PersonalData.motherNameRequired)
        
        checkFieldsAndEnableContinue()
    }
    
    func checkEmail(_ text: String) {
        validationEmail = text
        let validEmail = isValidEmail(validationEmail)
        
        presenter.updateEmailField(
            withError: validEmail ? nil : Strings.PersonalData.invalidEmail
        )
        checkFieldsAndEnableContinue()
    }
    
    func checkDocument(_ text: String) {
        validationDocument = text
        let validDocument = isValidDocument(validationDocument)

        presenter.updateDocumentField(
            withError: validDocument ? nil : Strings.PersonalData.invalidCPFDocument
        )
        checkFieldsAndEnableContinue()
    }
    
    func checkBirthday(_ text: String) {
        validationBirthdate = text
        
        switch isValidBirthdateLenght(validationBirthdate) {
        case .incompleteData:
            presenter.updateBirthdateField(withError: Strings.PersonalData.birthdateRequired)
        case .invalidData:
            presenter.updateBirthdateField(withError: Strings.PersonalData.invalidBirthdate)
        case .none:
            isValidDate(validationBirthdate)
                ? presenter.updateBirthdateField(withError: nil)
                : presenter.updateBirthdateField(withError: Strings.PersonalData.invalidBirthdate)
        }
        
        checkFieldsAndEnableContinue()
    }
    
    func validateForm(document: String, birthDate: String, motherName: String, email: String) {
        let personalData = TFAPersonalDataInfo(cpfDocument: document, birthDate: birthDate, mothersName: motherName, email: email)
        
        presenter.presentLoading(true)
        service.validatePersonalData(personalData: personalData, requestData: deviceAuthRequestData) { [weak self] result in
            self?.presenter.presentLoading(false)
            
            switch result {
            case .success(let response):
                self?.didValidateData(for: response.nextStep)
            case .failure(let error):
                self?.parseError(error: error)
            }
        }
    }
    
    func startRetryValidationTimer(retryTime: Int) {
        countdownSeconds = retryTime
        timer.start()
    }
}

extension PersonalDataValidationInteractor: TFATimerDelegate {
    func timerTic() {
        if countdownSeconds <= 0 {
            timer.stop()
            presenter.presentFormValidationEnabled()
        } else {
            countdownSeconds -= 1
            presenter.presentContinueButtonUpdated(with: countdownSeconds)
        }
    }
}

// MARK: - Private Methods
private extension PersonalDataValidationInteractor {
    func isValidEmail(_ email: String) -> Bool {
        let validator = RegexValidator(descriptor: AccountRegexDescriptor.email)
        do {
            try validator.validate(email)
            return true
        } catch {
            return false
        }
    }
    
    func isValidMotherName(_ motherName: String) -> GenericValidationError? {
        let validator = RangeValidator(minCount: 1, maxCount: nil)
        do {
            try validator.validate(motherName)
            return nil
        } catch {
            return .incompleteData
        }
    }
    
    func isValidBirthdateLenght(_ birthdate: String) -> GenericValidationError? {
        let minRangeValidator = RangeValidator(minCount: 1, maxCount: 10)
        do {
            try minRangeValidator.validateForInterval(birthdate)
            return nil
        } catch {
            return error as? GenericValidationError
        }
    }
    
    func isValidDate(_ dateString: String) -> Bool {
        let currentDate = Date()
        
        guard
            let date = DateFormatter(style: .notLenient).date(from: dateString),
            date < currentDate else {
            return false
        }
        
        return true
    }
    
    func isValidDocument(_ document: String) -> Bool {
        let validator = CPFValidator()
        do {
            try validator.validate(document)
            return true
        } catch {
            return false
        }
    }
    
    func checkFieldsAndEnableContinue() {
        let validBirthdate = isValidBirthdateLenght(validationBirthdate) == nil && isValidDate(validationBirthdate)
        let validMotherName = isValidMotherName(validationMotherName) == nil
        let enable = isValidEmail(validationEmail) && isValidDocument(validationDocument) && validBirthdate && validMotherName
        presenter.shouldEnableContinueButton(enable)
    }
    
    func didValidateData(for nextStep: DeviceAuthStep) {
        presenter.didNextStep(action: .nextStep(flow: deviceAuthRequestData.flow, step: nextStep))
    }
    
    func parseError(error: ApiError) {
        switch error {
        case .tooManyRequests(let requestError), .unauthorized(let requestError), .badRequest(let requestError):
            guard let retryTime = requestError.data["retryTime"] as? Int else {
                presenter.presentErrorAlert(title: requestError.title, message: requestError.message)
                return
            }
            presenter.presentAttempLimitError(tfaError: requestError, retryTime: retryTime)
        default:
            presenter.presentErrorAlert(title: Strings.Alerts.genericErrorTitle, message: Strings.Alerts.genericErrorMessage)
        }
    }
}
