import Foundation

struct TFAPersonalDataInfo {
    let cpfDocument: String
    let birthDate: String
    let mothersName: String
    let email: String
}
