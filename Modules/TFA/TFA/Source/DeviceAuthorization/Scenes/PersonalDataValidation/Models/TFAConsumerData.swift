import Foundation

struct TFAConsumerData: Decodable {
    struct ConsumerData: Decodable {
        let email: String
    }
    let consumerData: ConsumerData
}
