import UI
import UIKit

enum PersonalDataValidationAction: Equatable {
    case back
    case nextStep(flow: DeviceAuthFlow, step: DeviceAuthStep)
}

protocol PersonalDataValidationCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: DeviceAuthFlowStepping? { get set }
    func perform(action: PersonalDataValidationAction)
}

final class PersonalDataValidationCoordinator {
    weak var viewController: UIViewController?
    weak var delegate: DeviceAuthFlowStepping?
}

// MARK: - PersonalDataValidationCoordinating
extension PersonalDataValidationCoordinator: PersonalDataValidationCoordinating {
    func perform(action: PersonalDataValidationAction) {
        switch action {
        case .back:
            delegate?.validationDidGoBack()
        case let .nextStep(flow, step):
            delegate?.receivedNextStep(forFlow: flow, step: step)
        }
    }
}
