import UI
import UIKit

protocol PersonalDataValidationDisplaying: AnyObject {
    func displayLoading(_ isLoading: Bool)
    func displayEmailHint(_ hint: String)
    func updateEmailFieldErrorMessage(_ message: String?)
    func updateDocumentFieldErrorMessage(_ message: String?)
    func updateBirthdateFieldErrorMessage(_ message: String?)
    func updateMotherNameFieldErrorMessage(_ message: String?)
    func enableContinueButton(enable: Bool)
    func displayAttempLimitError(with title: String, message: String, retryTime: Int)
    func displayAlert(title: String, message: String)
    func displayCancelAuthorizationAlert()
    func displayCountdownTimer(with remainingTime: String)
    func enableFormValidation()
}

// MARK: - View Section
final class PersonalDataValidationViewController: ViewController<PersonalDataValidationInteracting, UIView>, LoadingViewProtocol {
    fileprivate enum Constants {
        static let maskedCpfDocumentLength = 14
        static let maskedBirthdateLength = 10
    }
    
    lazy var loadingView = LoadingView()
    
    private lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.backgroundColor = .clear
        return scroll
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, .grayscale750())
            .with(\.textAlignment, .left)
        label.text = Strings.PersonalData.header
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.textColor, .grayscale750())
            .with(\.textAlignment, .left)
        label.text = Strings.PersonalData.body
        return label
    }()
    
    private lazy var documentTextfield: UIPPFloatingTextField = {
        let textfield = TFATextFieldFactory.defaultField()
        textfield.placeholder = Strings.PersonalData.cpfPlaceholder
        textfield.addTarget(self, action: #selector(checkDocumentTextField), for: .editingChanged)
        textfield.keyboardType = .numberPad
        textfield.delegate = self
        return textfield
    }()
    
    private lazy var documentMasker: TextFieldMasker = {
        let mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cpf)
        let masker = TextFieldMasker(textMask: mask)
        return masker
    }()
    
    private lazy var birthdateTextfield: UIPPFloatingTextField = {
        let textfield = TFATextFieldFactory.defaultField()
        textfield.placeholder = Strings.PersonalData.birthdatePlaceholder
        textfield.addTarget(self, action: #selector(checkBirthdayTextField), for: .editingChanged)
        textfield.keyboardType = .numberPad
        textfield.delegate = self
        return textfield
    }()
    
    private lazy var birthDateMasker: TextFieldMasker = {
        let mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.date)
        let masker = TextFieldMasker(textMask: mask)
        return masker
    }()
    
    private lazy var motherNameTextfield: UIPPFloatingTextField = {
        let textfield = TFATextFieldFactory.defaultField()
        textfield.placeholder = Strings.PersonalData.motherNamePlaceholder
        textfield.addTarget(self, action: #selector(checkMotherNameTextfield), for: .editingChanged)
        textfield.delegate = self
        return textfield
    }()
    
    private lazy var emailTextfield: UIPPFloatingTextField = {
        let textfield = TFATextFieldFactory.defaultField()
        textfield.title = Strings.PersonalData.emailPlaceholder
        textfield.placeholder = Strings.PersonalData.emailPlaceholder
        textfield.keyboardType = .emailAddress
        textfield.addTarget(self, action: #selector(checkEmailTextField), for: .editingChanged)
        textfield.addTarget(self, action: #selector(emailEditingBegan), for: .editingDidBegin)
        textfield.delegate = self
        return textfield
    }()
    
    private lazy var continueButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.General.continue, for: .normal)
        button.addTarget(self, action: #selector(didTapContinueButton), for: .touchUpInside)
        button.buttonStyle(PrimaryButtonStyle())
        button.isEnabled = false
        return button
    }()

    private lazy var keyboardScrollViewHandler = KeyboardScrollViewHandler(scrollView: scrollView)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        keyboardScrollViewHandler.registerForKeyboardNotifications()
        documentMasker.bind(to: documentTextfield)
        birthDateMasker.bind(to: birthdateTextfield)
        
        interactor.getPersonalDataInfo()
    }

    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(documentTextfield)
        contentView.addSubview(birthdateTextfield)
        contentView.addSubview(motherNameTextfield)
        contentView.addSubview(emailTextfield)
        contentView.addSubview(continueButton)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalTo(view)
        }
        
        contentView.snp.makeConstraints {
            $0.edges.equalTo(scrollView)
            $0.width.equalTo(view.snp.width)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(contentView.snp.top).offset(Spacing.base02)
            $0.leading.trailing.equalTo(contentView).inset(Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalTo(contentView).inset(Spacing.base02)
        }

        documentTextfield.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalTo(contentView).inset(Spacing.base02)
        }
        
        birthdateTextfield.snp.makeConstraints {
            $0.top.equalTo(documentTextfield.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalTo(contentView).inset(Spacing.base02)
        }
        
        motherNameTextfield.snp.makeConstraints {
            $0.top.equalTo(birthdateTextfield.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalTo(contentView).inset(Spacing.base02)
        }
        
        emailTextfield.snp.makeConstraints {
            $0.top.equalTo(motherNameTextfield.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalTo(contentView).inset(Spacing.base02)
        }
        
        continueButton.snp.makeConstraints {
            $0.bottom.equalTo(contentView.snp.bottom).offset(-Spacing.base02)
            $0.leading.trailing.equalTo(contentView).inset(Spacing.base02)
            $0.top.equalTo(emailTextfield.snp.bottom).offset(Spacing.base04)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        navigationItem.leftBarButtonItem = TFANavBarBackButton(target: self, action: #selector(backButtonTapped))
    }
}

// MARK: - Private Methods
@objc
private extension PersonalDataValidationViewController {
    func backButtonTapped() {
        interactor.backButtonWasTapped()
    }
    
    private func emailEditingBegan() {
        interactor.emailBeginEditing()
    }
    
    func checkMotherNameTextfield() {
        interactor.checkMotherName(motherNameTextfield.text ?? "")
    }
    
    func checkDocumentTextField() {
        interactor.checkDocument(documentTextfield.text ?? "")
    }
    
    func checkBirthdayTextField() {
        interactor.checkBirthday(birthdateTextfield.text ?? "")
    }
    
    func checkEmailTextField() {
        interactor.checkEmail(emailTextfield.text ?? "")
    }
    
    func didTapContinueButton() {
        view?.endEditing(true)
        viewModel.validateForm(
            document: documentTextfield.text ?? "",
            birthDate: birthdateTextfield.text ?? "",
            motherName: motherNameTextfield.text ?? "",
            email: emailTextfield.text ?? ""
        )
    }
    
    private func enableFields(enable: Bool) {
        documentTextfield.isEnabled = enable
        birthdateTextfield.isEnabled = enable
        motherNameTextfield.isEnabled = enable
        emailTextfield.isEnabled = enable
    }
    
    private func enableValidation(_ isEnabled: Bool) {
        continueButton.isEnabled = isEnabled
        continueButton.setTitle(Strings.General.continue, for: .disabled)
        continueButton.setTitle(Strings.General.continue, for: .normal)
        enableFields(enable: isEnabled)
    }
}

// MARK: - PersonalDataValidationDisplaying
extension PersonalDataValidationViewController: PersonalDataValidationDisplaying {
    func displayLoading(_ isLoading: Bool) {
        isLoading ? startLoadingView() : stopLoadingView()
    }
    
    func displayEmailHint(_ hint: String) {
        emailTextfield.setTitleVisible(true)
        emailTextfield.placeholder = hint
    }
    
    func updateEmailFieldErrorMessage(_ message: String?) {
        emailTextfield.errorMessage = message
    }
    
    func updateDocumentFieldErrorMessage(_ message: String?) {
        documentTextfield.errorMessage = message
    }
    
    func updateBirthdateFieldErrorMessage(_ message: String?) {
        birthdateTextfield.errorMessage = message
    }
    
    func updateMotherNameFieldErrorMessage(_ message: String?) {
        motherNameTextfield.errorMessage = message
    }
    
    func enableContinueButton(enable: Bool) {
        continueButton.isEnabled = enable
    }
    
    func displayAlert(title: String, message: String) {
        let backAction = ApolloAlertAction(title: Strings.General.back, dismissOnCompletion: true) { }
        showApolloAlert(title: title, subtitle: message, primaryButtonAction: backAction, dismissOnTouchOutside: false)
    }
    
    func displayAttempLimitError(with title: String, message: String, retryTime: Int) {
        enableValidation(false)
        interactor.startRetryValidationTimer(retryTime: retryTime)
        
        displayAlert(title: title, message: message)
    }
    
    func displayCountdownTimer(with remainingTime: String) {
        continueButton.setTitle(remainingTime, for: .disabled)
    }
    
    func enableFormValidation() {
        enableValidation(true)
    }
    
    func displayCancelAuthorizationAlert() {
        let continueAction = ApolloAlertAction(title: Strings.Alerts.CancelAuthorization.continue, dismissOnCompletion: true) { }
        
        let cancelAction = ApolloAlertAction(
            title: Strings.Alerts.CancelAuthorization.cancel,
            dismissOnCompletion: true) { [weak self] in
                self?.interactor.goBack()
        }

        showApolloAlert(
            title: Strings.Alerts.CancelAuthorization.title,
            subtitle: Strings.Alerts.CancelAuthorization.message,
            primaryButtonAction: continueAction,
            linkButtonAction: cancelAction,
            dismissOnTouchOutside: false
        )
    }
}

// MARK: - UItextFieldDelegate
extension PersonalDataValidationViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.isEmpty {
            return true
        }
        
        guard let text = textField.text else {
            return false
        }
        
        switch textField {
        case documentTextfield:
            return text.count < Constants.maskedCpfDocumentLength
        case birthdateTextfield:
            return text.count < Constants.maskedBirthdateLength
        default:
            return true
        }
    }
}
