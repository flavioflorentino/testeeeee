import Core
import Foundation

protocol PersonalDataValidationServicing {
    func getPersonalData(requestData: DeviceAuthRequestData, completion: @escaping (Result<TFAConsumerData, ApiError>) -> Void)
    func validatePersonalData(
        personalData: TFAPersonalDataInfo,
        requestData: DeviceAuthRequestData,
        completion: @escaping (Result<DeviceAuthValidationResponse, ApiError>) -> Void
    )
}

final class PersonalDataValidationService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - PersonalDataValidationServicing
extension PersonalDataValidationService: PersonalDataValidationServicing {
    func getPersonalData(requestData: DeviceAuthRequestData, completion: @escaping (Result<TFAConsumerData, ApiError>) -> Void) {
        let endpoint = DeviceAuthEndpoint.getPersonalData(requestData: requestData)
        let api = Api<TFAConsumerData>(endpoint: endpoint)
        
        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func validatePersonalData(
        personalData: TFAPersonalDataInfo,
        requestData: DeviceAuthRequestData,
        completion: @escaping (Result<DeviceAuthValidationResponse, ApiError>) -> Void
    ) {
        let endpoint = DeviceAuthEndpoint.validatePersonalData(personalData: personalData, requestData: requestData)
        let api = Api<DeviceAuthValidationResponse>(endpoint: endpoint)
        
        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
