import UIKit

enum PersonalDataValidationFactory {
    static func make(
        coordinatorDelegate: DeviceAuthFlowStepping,
        deviceAuthRequestData: DeviceAuthRequestData
    ) -> PersonalDataValidationViewController {
        let container = TFADependencyContainer()
        let service: PersonalDataValidationServicing = PersonalDataValidationService(dependencies: container)
        let coordinator: PersonalDataValidationCoordinating = PersonalDataValidationCoordinator()
        let presenter: PersonalDataValidationPresenting = PersonalDataValidationPresenter(coordinator: coordinator)
        
        let interactor = PersonalDataValidationInteractor(
            service: service,
            presenter: presenter,
            deviceAuthRequestData: deviceAuthRequestData
        )
        
        let viewController = PersonalDataValidationViewController(interactor: interactor)

        coordinator.viewController = viewController
        coordinator.delegate = coordinatorDelegate
        presenter.viewController = viewController

        return viewController
    }
}
