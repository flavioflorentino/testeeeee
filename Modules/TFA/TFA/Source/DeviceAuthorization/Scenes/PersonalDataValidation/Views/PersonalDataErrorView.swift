import UI
import UIKit

final class PersonalDataErrorView: UIView, ViewConfiguration {
    private lazy var errorTextLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
        label.textColor = Colors.white.lightColor
        label.textAlignment = .left
        label.text = Strings.PersonalData.verifyInfo
        return label
    }()
    
    init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubview(errorTextLabel)
    }
    
    func setupConstraints() {
        errorTextLabel.snp.makeConstraints {
            $0.top.leading.trailing.bottom.equalTo(self).inset(Spacing.base01)
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.critical400.color
        layer.cornerRadius = CornerRadius.light
    }
}
