import Core
import Foundation

enum DeviceAuthEndpoint {
    case tfaOptions(requestData: DeviceAuthRequestData)
    case selfieValidation(requestData: DeviceAuthRequestData)
    case getPersonalData(requestData: DeviceAuthRequestData)
    case validatePersonalData(personalData: TFAPersonalDataInfo, requestData: DeviceAuthRequestData)
    case getBankAccountInfo(requestData: DeviceAuthRequestData)
    case validateBankAccount(bankAccount: AccountData, requestData: DeviceAuthRequestData)
    case getPhoneNumberData(requestData: DeviceAuthRequestData)
    case validatePhoneNumber(phoneNumber: String, requestData: DeviceAuthRequestData)
    case getValidationCode(requestData: DeviceAuthRequestData)
    case validateCode(code: String, requestData: DeviceAuthRequestData)
}

extension DeviceAuthEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .tfaOptions(let requestData),
            .selfieValidation(let requestData),
            .getPersonalData(let requestData),
            .validatePersonalData(_, let requestData),
            .getBankAccountInfo(let requestData),
            .validateBankAccount(_, let requestData),
            .getPhoneNumberData(let requestData),
            .validatePhoneNumber(_, let requestData),
            .getValidationCode(let requestData),
            .validateCode(_, let requestData):
            return configureEndpoint(with: requestData)
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .validatePersonalData, .validateBankAccount, .validatePhoneNumber, .validateCode:
            return .post
        default:
            return .get
        }
    }
    
    var body: Data? {
        switch self {
        case let .validatePersonalData(personalData, _):
            return personalDataBody(personalData)
        case let .validateBankAccount(bankAccount, _):
            return bankAccountDataBody(bankAccount)
        case let .validatePhoneNumber(phoneNumber, _):
            return phoneNumberDataBody(phoneNumber)
        case let .validateCode(code, _):
            return validateCodeDataBody(code)
        default:
            return nil
        }
    }
    
    var isTokenNeeded: Bool {
        false
    }
    
    private func configureEndpoint(with requestData: DeviceAuthRequestData) -> String {
        "auth/tfa/v2/\(requestData.tfaId)/flow/\(requestData.flow.rawValue)/step/\(requestData.step.rawValue)"
    }
    
    private func personalDataBody(_ personalData: TFAPersonalDataInfo) -> Data? {
        let formData: [String: Any] = [
            "form_data": [
                "cpf": personalData.cpfDocument,
                "mother_name": personalData.mothersName,
                "email": personalData.email,
                "birth_date": personalData.birthDate
            ]
        ]
        
        return formData.toData()
    }
    
    private func bankAccountDataBody(_ accountData: AccountData) -> Data? {
        var bankAccount: [String: Any] = [
            "account": accountData.account,
            "account_dv": accountData.accountDigit,
            "agency": accountData.agency
        ]

        if let agencyDigit = accountData.agencyDigit,
            agencyDigit.isNotEmpty {
            bankAccount["agency_dv"] = agencyDigit
        }

        let accountData: [String: Any] = ["bank_account": bankAccount]
        return accountData.toData()
    }
    
    private func phoneNumberDataBody(_ phoneNumber: String) -> Data? {
        let phoneNumberData: [String: Any] = [
            "phone": [
                "phone_number": phoneNumber
            ]
        ]
        
        return phoneNumberData.toData()
    }
    
    private func validateCodeDataBody(_ code: String) -> Data? {
        ["code": code].toData()
    }
}
