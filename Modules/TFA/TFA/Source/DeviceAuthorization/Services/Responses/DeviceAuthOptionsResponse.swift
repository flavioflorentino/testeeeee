import Foundation

struct DeviceAuthOption: Decodable, Equatable {
    enum CodingKeys: String, CodingKey {
        case flow
        case firstStep = "first_step"
        case iconUrl = "icon_url"
        case title
    }
    
    let flow: DeviceAuthFlow
    let firstStep: DeviceAuthStep
    let iconUrl: String
    let title: String
}

struct DeviceAuthOptionsResponse: Decodable {
    let options: [DeviceAuthOption]
}
