import Foundation

struct DeviceAuthValidationResponse: Decodable {
    let nextStep: DeviceAuthStep
}
