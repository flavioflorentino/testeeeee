import Foundation

struct DeviceAuthSelfieFlowResponse: Decodable, Equatable {
    struct Selfie: Decodable, Equatable {
        let token: String
        let flow: String
    }
    
    let selfie: Selfie
}
