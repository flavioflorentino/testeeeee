import Foundation

protocol DeviceAuthFlowStepping: AnyObject {
    func validationDidGoBack()
    func receivedNextStep(forFlow flow: DeviceAuthFlow, step: DeviceAuthStep)
}
