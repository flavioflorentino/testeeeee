import Foundation

protocol TFATimerDelegate: AnyObject {
    func timerTic()
}

protocol TFATimerProtocol {
    var delegate: TFATimerDelegate? { get set }
    func start()
    func stop()
}

final class TFATimer: TFATimerProtocol {
    private let timeInterval: TimeInterval = 1
    private let repeats = true

    private(set) var timer: Timer?
    weak var delegate: TFATimerDelegate?

    func start() {
        configureTimer()
        timer?.fire()
    }

    func stop() {
        timer?.invalidate()
    }
}

private extension TFATimer {
    func configureTimer() {
        timer = Timer.scheduledTimer(
            timeInterval: timeInterval,
            target: self,
            selector: #selector(runTimedCode),
            userInfo: nil,
            repeats: repeats
        )
    }

    @objc
    func runTimedCode() {
        delegate?.timerTic()
    }
}
