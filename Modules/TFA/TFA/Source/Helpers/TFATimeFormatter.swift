import Foundation

protocol TFATimeFormatterProtocol {
    func formatString(from interval: TimeInterval) -> String
}

struct TFATimeFormatter: TFATimeFormatterProtocol {
    func formatString(from interval: TimeInterval) -> String {
        let dateFormatter = DateComponentsFormatter()
        dateFormatter.allowedUnits = [.minute, .second]
        dateFormatter.zeroFormattingBehavior = .pad

        return dateFormatter.string(from: interval) ?? ""
    }
}

struct DropLeadingTFATimeFormatter: TFATimeFormatterProtocol {
    func formatString(from interval: TimeInterval) -> String {
        let dateFormatter = DateComponentsFormatter()
        dateFormatter.allowedUnits = [.minute, .second]
        dateFormatter.zeroFormattingBehavior = .dropLeading

        return dateFormatter.string(from: interval) ?? ""
    }
}
