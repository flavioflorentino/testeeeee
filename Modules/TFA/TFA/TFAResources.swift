import Foundation

// swiftlint:disable convenience_type
final class TFAResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: TFAResources.self).url(forResource: "TFAResources", withExtension: "bundle") else {
            return Bundle(for: TFAResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: TFAResources.self)
    }()
}
