import Foundation

public extension URL {
    var queryParameters: [String: String?] {
        guard
            let urlComponents = URLComponents(url: self, resolvingAgainstBaseURL: true),
            let queryItems = urlComponents.queryItems
            else {
                return [:]
        }
    
        return Dictionary(queryItems.map { ($0.name, $0.value) }) { _, last in last }
    }
}
