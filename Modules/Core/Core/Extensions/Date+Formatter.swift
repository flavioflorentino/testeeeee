import Foundation

public extension Date {
    /// "dd"
    static var dd: DateFormatter {
        let df = DateFormatter()
        df.dateFormat = "dd"
        df.locale = Locale(identifier: "pt_BR")
        return df
    }
    
    /// "MMMM"
    static var mmmm: DateFormatter {
        let df = DateFormatter()
        df.dateFormat = "MMMM"
        df.locale = Locale(identifier: "pt_BR")
        return df
    }
    
    /// "dd 'de' MMMM", e.g.: "16 de Novembro"
    func ddDeMMMM() -> String {
        let day = Date.dd.string(from: self)
        let month = Date.mmmm.string(from: self)
        return "\(day) de \(month.capitalized)"
    }
}
