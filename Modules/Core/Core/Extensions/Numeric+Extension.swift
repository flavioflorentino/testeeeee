import Foundation

public extension Numeric {
    func toCurrencyString(minFractionDigits: Int = 2, maxFractionDigits: Int = 2) -> String? {
        NumberFormatter.toCurrencyString(with: self,
                                         minFractionDigits: minFractionDigits,
                                         maxFractionDigits: maxFractionDigits)
    }
}
