import Foundation

public enum DateFormatterStyle {
    case notLenient
}

public extension DateFormatter {
    convenience init(style: DateFormatterStyle) {
        self.init()

        switch style {
        case .notLenient:
            configureNotLenient()
        }
    }

    private func configureNotLenient() {
        dateFormat = "dd/MM/yyyy"
        calendar = Calendar(identifier: .iso8601)
        timeZone = TimeZone(secondsFromGMT: 0)
        isLenient = false
    }
}
