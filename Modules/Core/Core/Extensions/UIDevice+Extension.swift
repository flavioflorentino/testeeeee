import UIKit

public extension UIDevice {
    typealias Dependencies = HasKeychainManagerPersisted

    @objc var installationId: String? {
        guard let installationId = KVStore().stringFor(.installation_id), !installationId.isEmpty else {
            let installationId = identifierForVendor?.uuidString
            if let id = installationId {
                KVStore().setString(id, with: .installation_id)
            }
            return installationId
        }
        
        return installationId
    }
    
    @objc var deviceModel: String {
         var systemInfo = utsname()
         uname(&systemInfo)

         return Mirror(reflecting: systemInfo.machine).children
             .compactMap { $0.value as? Int8 }
             .filter { $0 != 0 }
             .map { String(UnicodeScalar(UInt8($0))) }
             .reduce(into: "") { $0 += $1 }
     }
    
    func picpayDeviceId(dependencies: Dependencies) -> String? {
        let keychainPersisted = dependencies.keychainWithPersistent
        guard let uId = keychainPersisted.getData(key: KeychainKey.ppDeviceId) else {
            let newId = generateUuid()
            keychainPersisted.set(key: KeychainKey.ppDeviceId, value: newId)
            return newId
        }
        
        return uId
    }
    
    private func generateUuid() -> String {
        let uuidRef = CFUUIDCreate(nil)
        let uuidStringRef: CFString = CFUUIDCreateString(nil, uuidRef)
        return uuidStringRef as String
    }
}
