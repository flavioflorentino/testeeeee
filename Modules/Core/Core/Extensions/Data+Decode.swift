import Foundation

public extension Data {
    func decoded<D: Decodable>() -> D? {
        do {
            let jsonDecoder = JSONDecoder()
            let decoded = try jsonDecoder.decode(D.self, from: self)
            return decoded
        } catch {
            debugPrint("decode Error: \(error)")
            return nil
        }
    }
}
