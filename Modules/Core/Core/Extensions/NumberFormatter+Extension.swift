import Foundation

public extension NumberFormatter {
    static func toCurrencyString<T: Numeric>(
        with value: T,
        minFractionDigits: Int = 2,
        maxFractionDigits: Int = 2
    ) -> String? {
        let formatter = NumberFormatter()
        formatter.usesGroupingSeparator = true
        formatter.minimumFractionDigits = minFractionDigits
        formatter.maximumFractionDigits = maxFractionDigits
        formatter.numberStyle = .currency
        formatter.locale = Locale(identifier: "pt_BR")
        return formatter.string(for: value)
    }
    
    static func toString<T: NSNumber>(
        with value: T,
        minFractionDigits: Int = 2,
        maxFractionDigits: Int = 2,
        currencyDecimalSeparator: String = ",",
        currencyGroupingSeparator: String = ".",
        decimalSeparator: String = ".",
        groupingSeparator: String = "",
        numberStle: NumberFormatter.Style = .currency
    ) -> String? {
        let formatter = NumberFormatter()
        formatter.usesGroupingSeparator = true
        formatter.minimumFractionDigits = minFractionDigits
        formatter.maximumFractionDigits = maxFractionDigits
        formatter.currencyDecimalSeparator = currencyDecimalSeparator
        formatter.currencyGroupingSeparator = currencyGroupingSeparator
        formatter.decimalSeparator = decimalSeparator
        formatter.groupingSeparator = groupingSeparator
        formatter.numberStyle = numberStle
        formatter.locale = Locale(identifier: "pt_BR")
        return formatter.string(for: value)
    }
    
    static func toNumber(from value: String, defaultValue: Double = 0.0) -> Double {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        formatter.locale = Locale(identifier: "pt_BR")
        
        let sanitizedValue = value.replacingOccurrences(of: "R$", with: "")
        
        return formatter.number(from: sanitizedValue)?.doubleValue ?? defaultValue
    }
}
