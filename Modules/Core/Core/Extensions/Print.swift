import Foundation

// Override the swift print function to prevent logging on release version
public func debugPrint(_ items: @autoclosure () -> Any, separator: String = " ", terminator: String = "\n") {
    #if DEBUG
        Swift.debugPrint(items(), separator: separator, terminator: terminator)
    #endif
}

func print(_ items: @autoclosure () -> Any, separator: String = " ", terminator: String = "\n") {
    #if DEBUG
        Swift.print(items(), separator: separator, terminator: terminator)
    #endif
}
