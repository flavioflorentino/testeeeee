import Foundation

public enum AppType {
    case pf, pj
}

@objc
public class Environment: NSObject {
    @objc public static var apiUrl: String? {
        var urlString: String? = Environment.get("PICPAY_API_HOST")
        
        #if DEBUG
            urlString = UserDefaults.standard.string(forKey: "USER_DEFINED_HOST") ?? Environment.get("PICPAY_API_HOST")
        #endif
        
        return urlString
    }
    
    @objc public static let showBetaWarning: Bool = Environment.get("SHOW_BETA_WARNING") ?? false
    
    @objc public static let appStoreId: String? = Environment.get("APP_STORE_ID")
    
    public class func get<T>(_ name: String, bundle: Bundle = Bundle.main) -> T? {
        guard let enviromentSetting = bundle.infoDictionary?["EnviromentSetting"] as? [String: AnyObject],
            let key = enviromentSetting[name] else {
            return nil
        }
        
        return key as? T
    }

    public static let appType: AppType = {
        let identifier = Bundle.main.bundleIdentifier ?? String()
        #if DEBUG
        return identifier == "com.picpay.PicPay-Development" ? .pf : .pj
        #else
        return identifier == "com.picpay.PicPay" ? .pf : .pj
        #endif
    }()
}
