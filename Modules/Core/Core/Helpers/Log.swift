import Foundation

internal func debugDescription(_ request: URLRequest) -> String{
    var d = "----------------- REQUEST -------------------"
    d += "\n" + request.debugDescription
    d += "\n=========== REQUEST END ============"
    return d
}

enum Domain: String {
    case app = "App"
    case view = "View"
    case layout = "Layout"
    case controller = "Controller"
    case routing = "Routing"
    case service = "Service"
    case networkRequest = "Network Request"
    case networkResponse = "Network Response"
    case model = "Model"
    case cache = "Cache"
    case db = "DB"
    case io = "IO"
    
    static func custom(_ value: String) -> Domain {
        return Domain(rawValue: value) ?? .app
    }
}

enum Level: Int {
    case error
    case warning
    case important
    case info
    case debug
    case verbose
    case noise
    
    static func custom(_ value: Int) -> Level {
        return Level(rawValue: value) ?? .warning
    }
}

struct Log {
    // MARK: - Error
    static func error(_ domain: Domain,_ message: String?,
                            _ file: String = #file,
                            _ line: Int = #line,
                            _ function: String = #function){
        Log.error([domain], message, file, line, function)
    }
    
    static func error(_ domain: [Domain],_ message: String?,
                            _ file: String = #file,
                            _ line: Int = #line,
                            _ function: String = #function){
        Log.log(domain, .error, message, file, line, function)
    }
    
    // MARK: - Info
    static func info(domain: [String], message: String?,
                            file: String = #file,
                            line: Int = #line,
                            function: String = #function){
        var domains:[Domain] = []
        for rawValue in domain{
            domains.append(Domain(rawValue: rawValue) ?? .app)
        }
        Log.info(domains, message, file, line, function)
    }
    
    static func info(_ domain: Domain,_ message: String?,
                            _ file: String = #file,
                            _ line: Int = #line,
                            _ function: String = #function){
        Log.info([domain], message, file, line, function)
    }
    
    static func info(_ domain: [Domain],_ message: String?,
                            _ file: String = #file,
                            _ line: Int = #line,
                            _ function: String = #function){
        Log.log(domain, .info, message, file, line, function)
    }
    
    // MARK: - Debug
    static func debug(_ domain: Domain,_ message: String?,
                             _ file: String = #file,
                             _ line: Int = #line,
                             _ function: String = #function){
        Log.debug([domain], message, file, line, function)
    }
    
    static func debug(_ domain: [Domain],_ message: String?,
                             _ file: String = #file,
                             _ line: Int = #line,
                             _ function: String = #function){
        Log.log(domain, .debug, message, file, line, function)
    }
    
    
    
    static func log(_ domain: [Domain],
                    _ level: Level,
                    _ message: String?,
                    _ file: String = #file,
                    _ line: Int = #line,
                    _ function: String = #function) {
        
        #if DEBUG
            print("📁 Files: \(file)")
            print(message ?? "")
        #endif
    }
}
