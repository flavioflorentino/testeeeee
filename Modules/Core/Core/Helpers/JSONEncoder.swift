import Foundation

public func prepareBody<T: Encodable>(with payload: T, strategy: JSONEncoder.KeyEncodingStrategy = .useDefaultKeys) -> Data? {
    let jsonEncoder = JSONEncoder()
    jsonEncoder.outputFormatting = .prettyPrinted
    jsonEncoder.keyEncodingStrategy = strategy

    do {
        return try jsonEncoder.encode(payload)
    } catch {
        print("Failure to prepare card payload. \(error)")
        return nil
    }
}
