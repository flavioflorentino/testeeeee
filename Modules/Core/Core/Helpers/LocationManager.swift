import CoreLocation
import Foundation

public protocol LocationManagerContract {
    var authorizedLocation: CLLocation? { get }
    
    var lastAvailableLocation: CLLocation? { get }
}

public struct LocationDescriptor {
    public let latitude: String
    public let longitude: String
    public let accuracy: String
    public let locationTimestamp: String
    public let currentTimestamp: String
    
    public init?(_ location: CLLocation?, currentTimestamp: Date = Date()) {
        guard let location = location else {
            return nil
        }
        latitude = String(format: "%.6f", location.coordinate.latitude)
        longitude = String(format: "%.6f", location.coordinate.longitude)
        accuracy = "\(location.horizontalAccuracy)"
        locationTimestamp = String(format: "%0.0f", location.timestamp.timeIntervalSince1970 * 1000)
        self.currentTimestamp = String(format: "%0.0f", currentTimestamp.timeIntervalSince1970 * 1000)
    }
}

public final class LocationHeadersBridge: NSObject {
    @objc public var latitude: NSString {
        return LocationHeaders.latitude.toNSString()
    }
    @objc public var longitude: NSString {
        return LocationHeaders.longitude.toNSString()
    }
    @objc public var accuracy: NSString {
        return LocationHeaders.accuracy.toNSString()
    }
    @objc public var locationTimestamp: NSString {
        return LocationHeaders.locationTimestamp.toNSString()
    }
    @objc public var currentTimestamp: NSString {
        return LocationHeaders.currentTimestamp.toNSString()
    }
}

public enum LocationHeaders: String {
    case latitude = "latitude"
    case longitude = "longitude"
    case accuracy = "gps_acc"
    case locationTimestamp = "location_timestamp"
    case currentTimestamp = "current_timestamp"
    
    func toNSString() -> NSString {
        return self.rawValue as NSString
    }
}

public final class LocationManagerObjcWrapper: NSObject {
    @objc public var location: CLLocation? {
        return LocationManager.shared.authorizedLocation
    }
}

public class LocationManager: NSObject, CLLocationManagerDelegate, LocationManagerContract {
    public static let shared = LocationManager()
    
    private var location: CLLocation?
    
    public var authorizedLocation: CLLocation? {
        get {
            return isAuthorized() ? location : nil
        }
    }
    
    public var lastAvailableLocation: CLLocation? {
        return LocationManager.shared.authorizedLocation ?? LocationManager.shared.cachedLocation
    }
    
    private var cachedLocation: CLLocation? {
        if let cachedLocations = KVStore().dictionaryFor(.locationCoordinates) as? [String: Double],
            let lat = cachedLocations[LocationHeaders.latitude.rawValue], let lon = cachedLocations[LocationHeaders.longitude.rawValue] {
            return CLLocation(latitude: lat, longitude: lon)
        }
        return nil
    }
    
    private var locationManager: CLLocationManager?
    
    override private init() {
        super.init()
        
        OperationQueue.main.addOperation {
            self.locationManager = CLLocationManager()
            self.locationManager?.desiredAccuracy = kCLLocationAccuracyBest
            self.locationManager?.distanceFilter = CLLocationDistance(100)
            self.locationManager?.delegate = self
            
            self.locationManager?.startUpdatingLocation()
        }
    }

    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        location = locations.first
        saveCurrentLocationOnCache()
        // Stop the location manager and restart after 15 seconds
        locationManager?.stopUpdatingLocation()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 15) { [weak self] in
            self?.locationManager?.startUpdatingLocation()
        }
    }
    
    private func saveCurrentLocationOnCache() {
        guard let location = authorizedLocation else {
            return
        }
        var values = [String: Double]()
        values[LocationHeaders.latitude.rawValue] = location.coordinate.latitude
        values[LocationHeaders.longitude.rawValue] = location.coordinate.longitude
        KVStore().set(value: values, with: .locationCoordinates)
    }
    
    private func isAuthorized() -> Bool {
        let authorizationStatus = CLLocationManager.authorizationStatus()
        let authorizedWhenInUse = authorizationStatus == .authorizedWhenInUse
        let authorizedAlways = authorizationStatus == .authorizedAlways
        
        return authorizedWhenInUse || authorizedAlways
    }
}
