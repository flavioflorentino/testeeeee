import Foundation

// swiftlint:disable convenience_type
final class CoreResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: CoreResources.self).url(forResource: "CoreResources", withExtension: "bundle") else {
            return Bundle(for: CoreResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: CoreResources.self)
    }()
}
