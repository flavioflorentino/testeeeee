import Foundation
import KeychainAccess

public protocol KeychainKeyable {
    var rawValue: String { get }
}

public enum KeychainKey: String, KeychainKeyable {
    case token = "token"                                       // used: Core, Card, PCI, verdinho swift
    case ppDeviceId = "pp_device_id"                           // used: Core, test verdinho swift
    case shouldPin = "should_pin"                              // used: Core, verdinho swift
    case pinDomains = "pin_domains"                            // used: Core, verdinho swift
    case hostBaseUrl = "host_base_url"                         // used: Core, VendingMachine, verdinho swift, verdinho obj-c
    case authToken = "auth_token"                              // authToken is used to keep user password   // used: Verdinho swift, verdinho obj-c
    case tempAuthToken = "temp_auth_token"                     // used: Verdinho obj-c
    case tempAuthTokenStatus = "temp_auth_token_status"        // used: Verdinho obj-c
}

public enum KeychainError: Error {
    case generic(String)
    var localizedDescription: String {
        guard case let .generic(error) = self else {
            return ""
        }
        return error
    }
}

public protocol KeychainManagerContract {
    func getData(key: KeychainKeyable) -> String?
    func getModelData<T: Codable>(key: KeychainKeyable) -> T?
    func getInt(key: KeychainKeyable) -> Int?
    
    func set(key: KeychainKeyable, value: String)
    func set<T: Codable>(key: KeychainKeyable, value: T)
    
    func setValueWithBiometry(key: KeychainKeyable, value: String) throws
    func getValueWithBiometry(key: KeychainKeyable, authenticationPrompt: String) throws -> String?
    
    @discardableResult
    func clearValue(key: KeychainKeyable) -> Bool
    func clearAll()
}

public final class KeychainManager: KeychainManagerContract {
    private var keychain = KeychainPP()
    private var identifier: String

    public init(identifier: String? = Bundle.main.bundleIdentifier, isPersistent: Bool = false) {
        self.identifier = identifier ?? ""
        guard isPersistent else {
            self.keychain = KeychainPP(identifier: identifier)
            return
        }
        self.keychain = KeychainPP(persistent: isPersistent)
    }
    
    // MARK: GET FUNCTIONS
    public func getData(key: KeychainKeyable) -> String? {
        keychain.getData(key: key)
    }

    public func getModelData<T: Codable>(key: KeychainKeyable) -> T? {
        guard let hash = keychain.getData(key: key) else {
            return nil
        }

        let data = Data(hash.utf8)
        let decoder = JSONDecoder()

        guard let model = try? decoder.decode(T.self, from: data) else {
            return nil
        }
        
        return model
    }
    
    public func getInt(key: KeychainKeyable) -> Int? {
        keychain.getIntData(key: key)
    }
    
    // MARK: SET FUNCTIONS
    public func set(key: KeychainKeyable, value: String) {
        keychain.set(key: key, value: value)
    }
    
    public func setValueWithBiometry(key: KeychainKeyable, value: String) throws {
        let keychainAccess = Keychain(service: identifier)
        do {
            let policy: AuthenticationPolicy
            if #available(iOS 11.3, *) {
                policy = .biometryCurrentSet
            } else { // Fallback on earlier versions
                policy = .userPresence
            }
            try keychainAccess
                .accessibility(.whenPasscodeSetThisDeviceOnly, authenticationPolicy: policy)
                .set(value, key: key.rawValue)
        } catch let error as Status {
            throw KeychainError.generic(error.description)
        } catch {
            throw KeychainError.generic(error.localizedDescription)
        }
    }
    
    public func getValueWithBiometry(key: KeychainKeyable, authenticationPrompt: String) throws -> String? {
        do {
            let keychainAccess = Keychain(service: identifier)
            return try keychainAccess
                .authenticationPrompt(authenticationPrompt)
                .get(key.rawValue)
        } catch let error as Status {
            throw KeychainError.generic(error.description)
        } catch {
            throw KeychainError.generic(error.localizedDescription)
        }
    }

    public func set<T: Codable>(key: KeychainKeyable, value: T) {
        let enconder = JSONEncoder()
        guard
            let model = try? enconder.encode(value),
            let hash = String(data: model, encoding: .utf8) else {
                return
        }
        
        keychain.set(key: key, value: hash)
    }
    
    // MARK: CLEAR FUNCTIONS
    public func clearValue(key: KeychainKeyable) -> Bool {
        keychain.clearValue(key: key)
    }
    
    public func clearAll() {
        keychain.clearKeychain()
    }
}
