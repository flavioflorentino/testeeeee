import Foundation

public final class KeychainManagerMock: KeychainManagerContract {
    private var identifierSelected: String = "main-identifier"
    private lazy var override: [String: [String: Any]] = {
        [identifierSelected: [:]]
    }()
    
    public init(isPersistent: Bool = false) {
        guard isPersistent else {
            identifierSelected = "main-identifier"
            return
        }
        identifierSelected = "main-identifier-persistent"
    }

    public func getData(key: KeychainKeyable) -> String? {
        guard let value = override[identifierSelected]?[key.rawValue] as? String else {
            return nil
        }
        
        return value
    }

    public func getModelData<T: Codable>(key: KeychainKeyable) -> T? {
        guard let hash = override[identifierSelected]?[key.rawValue] as? String else {
            return nil
        }
        
        let data = Data(hash.utf8)
        let decoder = JSONDecoder()

        guard let model = try? decoder.decode(T.self, from: data) else {
            return nil
        }
               
        return model
    }
    
    public func getInt(key: KeychainKeyable) -> Int? {
        guard let value = override[identifierSelected]?[key.rawValue] as? String, let intValue = Int(value) else {
            return nil
        }
               
        return intValue
    }
    
    public func set(key: KeychainKeyable, value: String) {
        override[identifierSelected]?[key.rawValue] = value
    }

    public func set<T: Codable>(key: KeychainKeyable, value: T) {
        let enconder = JSONEncoder()
        guard
            let model = try? enconder.encode(value),
            let hash = String(data: model, encoding: .utf8) else {
                return
        }
        
        override[identifierSelected]?[key.rawValue] = hash
    }
    
    public func clearValue(key: KeychainKeyable) -> Bool {
        override[identifierSelected]?.removeValue(forKey: key.rawValue)
        return true
    }

    public func clearAll() {
        override[identifierSelected]?.removeAll()
    }
    
    public func setValueWithBiometry(key: KeychainKeyable, value: String) throws {
        set(key: key, value: value)
    }
    
    public func getValueWithBiometry(key: KeychainKeyable, authenticationPrompt: String) throws -> String? {
        getData(key: key)
    }
}
