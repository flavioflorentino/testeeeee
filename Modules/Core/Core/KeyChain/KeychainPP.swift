import Foundation
import Security
import KeychainAccess

final class KeychainPP {
    // Keychain key = currentUserAccount
    private let currentUserAccount: String
    private let defaultService: String
    private let keychainAccess: Keychain
    
    // Verify if contains old key of keychain.
    // variable `defaultService` represent old key who store all keys in one dictionary
    //
    // TODO: Remove in future versions, after remove old implementation
    //
    // *OBS*: check with you tech manager about impacts in old version, user who have old versions of app
    private var hasMigrated: Bool {
        keychainAccess[data: defaultService] == nil
    }
    
    convenience init() {
        self.init(identifier: nil)
    }
    
    convenience init(identifier: String?) {
        let bundleIdentifier = Bundle.main.bundleIdentifier ?? ""
        self.init(identifier ?? bundleIdentifier, defaultService: identifier ?? bundleIdentifier)
    }
    
    /// Presistent Keychain won't be deleted after users logout or delete/reinstall the app
    convenience init(persistent: Bool) {
        guard persistent else {
            self.init()
            return
        }
        let identifier = Bundle.main.bundleIdentifier ?? ""
        self.init(identifier, defaultService: identifier + "-persistent")
    }
    
    private init(_ currentUserAccount: String, defaultService: String) {
        self.currentUserAccount = currentUserAccount
        self.defaultService = defaultService
        self.keychainAccess = Keychain(service: defaultService)
    }
    
    @discardableResult
    func set(rawKey: String, value: String?) -> Bool {
        if isRunningAtUnitTests {
            UserDefaults.standard.set(value, forKey: "keychain_\(rawKey)")
            return true
        }
        
        if hasMigrated {
            keychainAccess[rawKey] = value
            return true
        }
        
        var query: [CFString: Any] = [
            kSecClass: kSecClassGenericPassword,
            kSecAttrAccount: currentUserAccount,
            kSecAttrService: defaultService
        ]
        
        if var allDataDict = getAllKeychainValuesDict() {
            allDataDict[rawKey] = value
            let data = NSKeyedArchiver.archivedData(withRootObject: allDataDict)
            let attributes = [kSecValueData: data] as CFDictionary
            return SecItemUpdate(query as CFDictionary, attributes) == noErr
        }
        
        let data = NSKeyedArchiver.archivedData(withRootObject: [rawKey: value])
        query[kSecValueData] = data
        return SecItemAdd(query as CFDictionary, nil) == noErr
    }
    
    @discardableResult
    func set(key: KeychainKeyable, value: String?) -> Bool {
        set(rawKey: key.rawValue, value: value)
    }

    func getIntData(key: KeychainKeyable) -> Int? {
        intValue(for: key.rawValue)
    }

    func getData(key: KeychainKeyable) -> String? {
        value(for: key.rawValue)
    }
    
    func value(for key: String) -> String? {
        if isRunningAtUnitTests {
            return UserDefaults.standard.string(forKey: "keychain_\(key)")
        }
        
        if hasMigrated {
            return keychainAccess[key]
        }
        
        if let dict = getAllKeychainValuesDict(), dict.keys.contains(key), let value = dict[key] as? String {
            return value
        }
        
        return nil
    }
    
    func intValue(for key: String) -> Int? {
        if isRunningAtUnitTests {
            return UserDefaults.standard.integer(forKey: "keychain_\(key)")
        }
        
        if hasMigrated {
            guard let value = keychainAccess[key] else {
                return nil
            }
            return Int(value)
        }
        
        guard
            let dict = getAllKeychainValuesDict(),
            dict.keys.contains(key),
            let value = dict[key] as? String
            else {
                return nil
        }
        
        return Int(value)
    }
    
    private func migrateDictionaryToKeysOfKeychain(_ dict: [String: Any]) {
        // Turns the data dictionary into a keychain item
        // Previously I was saving the entire dictionary in just one key, that key was the bundle id of the application.
        // So it would be enough to take the item from a key to get all the data.
        for (key, value) in dict {
            if let value = value as? String {
                keychainAccess[key] = value
            }
            if let value = value as? Data {
                keychainAccess[data: key] = value
            }
        }
        
        // remove all itens on old key in keychain
        keychainAccess[data: defaultService] = nil
    }
    
    private func getAllKeychainValuesDict(useKeychainPP: Bool = false) -> [String: Any?]? {
        let query = [
            kSecClass: kSecClassGenericPassword,
            kSecAttrAccount: currentUserAccount,
            kSecAttrService: defaultService,
            kSecReturnData: kCFBooleanTrue as Any,
            kSecMatchLimit: kSecMatchLimitOne
        ] as CFDictionary
        
        var dataTypeRef: AnyObject?
        let status = SecItemCopyMatching(query, &dataTypeRef)
        if status == noErr, let dataTypeRefData = dataTypeRef as? Data, let dict = NSKeyedUnarchiver.unarchiveObject(with: dataTypeRefData) as? [String: Any] {
            migrateDictionaryToKeysOfKeychain(dict)
            return dict
        }
        return nil
    }
    
    func clearValue(key: KeychainKeyable) -> Bool {
        clearValue(key: key.rawValue)
    }
    
    private func clearValue(key: String) -> Bool {
        if isRunningAtUnitTests {
            UserDefaults.standard.setValue(nil, forKey: "keychain_\(key)")
            return true
        }
        
        if hasMigrated {
            keychainAccess[key] = nil
            return true
        }
        
        let query: [CFString: Any] = [
            kSecClass: kSecClassGenericPassword,
            kSecAttrAccount: currentUserAccount,
            kSecAttrService: defaultService
        ]
        
        guard var dict = getAllKeychainValuesDict(), dict.keys.contains(key) else {
            return true
        }
        
        dict.removeValue(forKey: key)
        let data = NSKeyedArchiver.archivedData(withRootObject: dict)
        let attributes = [kSecValueData: data] as CFDictionary
        return SecItemUpdate(query as CFDictionary, attributes) == noErr
    }
    
    @discardableResult
    func clearKeychain() -> Bool {
        if isRunningAtUnitTests {
            UserDefaults.standard.dictionaryRepresentation()
                .filter { $0.key.contains("keychain_") }
                .forEach { UserDefaults.standard.setValue(nil, forKey: $0.key) }
            
            return true
        }
        
        if hasMigrated {
            /// Auth key has especials attributes for protection
            keychainAccess[KeychainKey.authToken.rawValue] = nil
            
            let allKeys = keychainAccess.allKeys()
            for key in allKeys {
                keychainAccess[key] = nil
            }
            return true
        }
        
        let query = [kSecClass: kSecClassGenericPassword, kSecAttrAccount: currentUserAccount, kSecAttrService: defaultService] as CFDictionary
        return SecItemDelete(query) == noErr
    }
    
    private var isRunningAtUnitTests: Bool {
        ProcessInfo.processInfo.environment["XCTestConfigurationFilePath"] != nil
    }
}
