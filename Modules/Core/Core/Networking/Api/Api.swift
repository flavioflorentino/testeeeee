import UIKit

public struct NoContent: Codable {
    public init() {}
}

public class Api<E: Decodable> {
    public typealias Completion = ApiCompletion.Completion<E>
    public typealias Success = ApiCompletion.Success<E>
    public typealias Dependencies = HasKeychainManager & HasKeychainManagerPersisted
    private typealias Result = ApiCompletion.ApiResult<E>

    private let endpoint: ApiEndpointExposable
    private let locationManager: LocationManagerContract
    private let dependencies: Dependencies = DependencyContainer()
    
    public var shouldUseDefaultDateFormatter = true

    public init(endpoint: ApiEndpointExposable, locationManager: LocationManagerContract = LocationManager.shared) {
        self.endpoint = endpoint
        self.locationManager = locationManager
    }

    @discardableResult
    public func execute(
        session: URLSessionable = URLSession(configuration: .default, delegate: URLSessionPinning(), delegateQueue: nil),
        jsonDecoder: JSONDecoder = JSONDecoder(),
        _ completion: @escaping Completion
    ) -> URLSessionTask? {
        execute(
            session: session,
            jsonDecoder: jsonDecoder,
            completionType: ApiCompletionSetup.Default,
            completion
        )
    }
    
    @discardableResult
    public func execute(
        session: URLSessionable = URLSession(configuration: .default, delegate: URLSessionPinning(), delegateQueue: nil),
        jsonDecoder: JSONDecoder = JSONDecoder(),
        completionType: ApiCompletion.Type,
        _ completion: @escaping Completion
    ) -> URLSessionTask? {
        let apiCompletion = completionType.init(completion)
        
        let request: URLRequest
        do {
            try request = makeRequest()
        } catch {
            apiCompletion.run(Result.failure(.unknown(error)))
            return nil
        }
        
        let task = session.dataTask(with: request) { responseBody, response, error in
            self.handle(
                request: request,
                responseBody: responseBody,
                response: response,
                error: error,
                jsonDecoder: jsonDecoder,
                completion: apiCompletion
            )
        }
        
        task.resume()
        
        return task
    }
    
    private func makeRequest() throws -> URLRequest {
        var urlComponent = URLComponents(string: endpoint.absoluteStringUrl)
        if endpoint.method == .get && endpoint.parameters.isNotEmpty {
            urlComponent?.queryItems = self.endpoint.parameters
                .map { URLQueryItem(name: $0.key, value: "\($0.value)") }
        }
        
        guard let url = urlComponent?.url else {
            throw ApiError.malformedRequest("Unable to parse url")
        }
        
        var request = URLRequest(url: url)
        request.addValue(endpoint.contentType.rawValue, forHTTPHeaderField: "Content-Type")
        
        request.httpMethod = endpoint.method.rawValue
        if endpoint.shouldAppendBody, let body = endpoint.body {
            request.httpBody = body
        }
        
        if endpoint.isTokenNeeded {
            let token = dependencies.keychain.getData(key: KeychainKey.token)
            request.addValue(token ?? "", forHTTPHeaderField: "Token")
            request.addValue(token == nil ? "" : "Bearer \(token ?? "")",
                             forHTTPHeaderField: "Authorization")
        }
        
        let allHeaders = defaultRequestHeaders().merging(endpoint.customHeaders) { current, _ in current }
        allHeaders.forEach { header in
            request.addValue(header.value, forHTTPHeaderField: header.key)
        }
        
        Log.info(.networkRequest, request.cURLDescription())
        
        return request
    }
    
    private func handle(
        request: URLRequest,
        responseBody: Data?,
        response: URLResponse?,
        error: Error?,
        jsonDecoder: JSONDecoder,
        completion: ApiCompletion
    ) {
        Log.info(.networkResponse, "\(response?.debugDescription ?? "")\n\(prettyPrint(responseBody))")
        
        if let error = error as NSError?,
            error.code == NSURLErrorCancelled {
            completion.run(Result.failure(.cancelled))
            return
        }
        
        guard let httpResponse = response as? HTTPURLResponse else {
            completion.run(Result.failure(.connectionFailure))
            return
        }
        
        let status = HTTPStatusCode(rawValue: httpResponse.statusCode) ?? .processing
        let result = evaluateResult(status: status, jsonDecoder: jsonDecoder, responseBody: responseBody)
        completion.run(result, response: httpResponse, request: request)
    }
    
    private func evaluateResult(
        status: HTTPStatusCode,
        jsonDecoder: JSONDecoder,
        responseBody: Data?
    ) -> Result {
        let result: Result
        switch status {
        case .ok, .created, .accepted, .noContent:
            result = handleSuccess(responseBody: responseBody, jsonDecoder: jsonDecoder)
        case .badRequest, .unprocessableEntity, .preconditionFailed, .preconditionRequired:
            let picPayError = RequestError.initialize(with: responseBody)
            result = .failure(.badRequest(body: picPayError))
        case .unauthorized:
            let picPayError = RequestError.initialize(with: responseBody)
            result = .failure(.unauthorized(body: picPayError))
        case .notFound:
            let picPayError = RequestError.initialize(with: responseBody)
            result = .failure(.notFound(body: picPayError))
        case .tooManyRequests:
            let picPayError = RequestError.initialize(with: responseBody)
            result = .failure(.tooManyRequests(body: picPayError))
        case .requestTimeout:
            result = .failure(.timeout)
        case .internalServerError, .badGateway, .serviceUnavailable:
            result = .failure(.serverError)
        case .upgradeRequired:
            result = .failure(.upgradeRequired)
        default:
            let picPayError = RequestError.initialize(with: responseBody)
            result = .failure(.otherErrors(body: picPayError))
        }
        
        return result
    }
}

extension Api {
    private func handleSuccess(responseBody: Data?, jsonDecoder: JSONDecoder) -> Result {
        switch E.self {
        case is Data.Type:
            return contentData(responseBody)
        case is NoContent.Type:
            return decodeNoContentData()
        default:
            return decodeContentData(responseBody, jsonDecoder: jsonDecoder)
        }
    }
    
    private func contentData(_ responseBody: Data?) -> Result {
        guard let response = responseBody as? E else {
            return .failure(.bodyNotFound)
        }

        let success = Success(model: response, data: responseBody)
        return .success(success)
    }

    private func decodeContentData(_ responseBody: Data?, jsonDecoder: JSONDecoder) -> Result {
        do {
            if shouldUseDefaultDateFormatter {
                jsonDecoder.dateDecodingStrategy = .formatted(DecodableDateFormatter())
            }
            
            let decoded = try jsonDecoder.decode(E.self, from: responseBody ?? Data())
            let result = Success(model: decoded, data: responseBody)
            return .success(result)
        } catch {
            Log.error(.networkResponse, "Unable to parse json \(error)")
            return .failure(.decodeError(error))
        }
    }
    
    private func decodeNoContentData() -> Result {
        guard let emptyJsonData = "{}".data(using: .utf8) else {
            return .failure(ApiError.bodyNotFound)
        }
        
        do {
            let decoded = try JSONDecoder().decode(E.self, from: emptyJsonData)
            let result = Success(model: decoded, data: nil)
            return .success(result)
        } catch {
            return .failure(.decodeError(error))
        }
    }
}

extension Api {
    private func prettyPrint(_ jsonData: Data?) -> String {
        guard let data = jsonData else {
            return ""
        }
        
        do {
            let jsonObj = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
            guard JSONSerialization.isValidJSONObject(jsonObj) else {
                return String(data: data, encoding: .utf8) ?? ""
            }
            
            let data = try JSONSerialization.data(withJSONObject: jsonObj, options: .prettyPrinted)
            
            return String(data: data, encoding: .utf8) ?? ""
        } catch {
            return ""
        }
    }
}

extension Api {
    private func defaultRequestHeaders() -> [String: String] {
        var headers: [String: String] = [:]
        
        let versionNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String ?? ""
        let buildNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String ?? ""
        
        headers["app_version"] = versionNumber
        headers["device_os"] = "ios"
        headers["app_version_b"] = buildNumber
        headers["device_model"] = UIDevice.current.deviceModel
        headers["device_id"] = UIDevice.current.picpayDeviceId(dependencies: dependencies)
        headers["installation_id"] = UIDevice.current.installationId
        headers["timezone"] = TimeZone.current.identifier
        headers[CorrelationId.headerField] = CorrelationId.id
        headers["new_api_ios"] = "true"
        
        guard let location = LocationDescriptor(locationManager.authorizedLocation) else {
            return headers
        }
        
        headers[LocationHeaders.latitude.rawValue] = location.latitude
        headers[LocationHeaders.longitude.rawValue] = location.longitude
        headers[LocationHeaders.accuracy.rawValue] = location.accuracy
        headers[LocationHeaders.locationTimestamp.rawValue] = location.locationTimestamp
        headers[LocationHeaders.currentTimestamp.rawValue] = location.currentTimestamp
        
        return headers
    }
}

private extension URLRequest {
    func cURLDescription() -> String {
        guard
            let url = self.url,
            let method = self.httpMethod
            else {
                return "$ curl command could not be created"
        }
        
        var components = ["$ curl -v"]
        
        components.append("-X \(method)")
        
        for header in self.allHTTPHeaderFields ?? [:] {
            let escapedValue = header.value.replacingOccurrences(of: "\"", with: "\\\"")
            components.append("-H \"\(header.key): \(escapedValue)\"")
        }
        
        if let httpBodyData = self.httpBody {
            let httpBody = String(decoding: httpBodyData, as: UTF8.self)
            var escapedBody = httpBody.replacingOccurrences(of: "\\\"", with: "\\\\\"")
            escapedBody = escapedBody.replacingOccurrences(of: "\"", with: "\\\"")
            
            components.append("-d \"\(escapedBody)\"")
        }
        
        components.append("\"\(url.absoluteString)\"")
        
        return components.joined(separator: " \\\n\t")
    }
}
