import Foundation

public protocol ApiEndpointExposable {
    var baseURL: URL { get }
    var path: String { get }
    var method: HTTPMethod { get }
    var parameters: [String: Any] { get }
    var body: Data? { get }
    var isTokenNeeded: Bool { get }
    var customHeaders: [String: String] { get }
    var absoluteStringUrl: String { get }
    var shouldAppendBody: Bool { get }
    var contentType: ContentType { get }
}

public extension ApiEndpointExposable {
    typealias Dependencies = HasKeychainManager
    var baseURL: URL {
        #if DEBUG
        let dependencies: Dependencies = DependencyContainer()
        if let userDefinedHost = dependencies.keychain.getData(key: KeychainKey.hostBaseUrl), let userUrl = URL(string: userDefinedHost) {
            return userUrl
        }
        #endif
        
        guard let apiUrl = Environment.apiUrl, let url = URL(string: apiUrl) else {
            fatalError("You need to define the api url")
        }
        return url
    }

    var absoluteStringUrl: String {
        let basePathString = baseURL.absoluteString
        let safeBasePath = basePathString.hasSuffix("/") ? String(basePathString.dropLast()) : basePathString
        let safePath = path.starts(with: "/") || path.isEmpty ? path : "/\(path)"
        return "\(safeBasePath)\(safePath)"
    }
    
    var shouldAppendBody: Bool { method != .get && method != .delete && body != nil }

    var method: HTTPMethod { .get }
    
    var parameters: [String: Any] { [:] }
    
    var body: Data? { nil }
    
    var isTokenNeeded: Bool { true }
    
    var customHeaders: [String: String] { [:] }

    var contentType: ContentType { .applicationJson }
}
