import Dispatch

public protocol HasMainQueue {
    var mainQueue: DispatchQueue { get }
}

public protocol HasDispatchGroup {
    var dispatchGroup: DispatchGroup { get }
}

public protocol HasGlobalQueue {
    var globalQueue: DispatchQueue { get }
}
