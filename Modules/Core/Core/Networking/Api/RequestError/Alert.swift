import Foundation

public struct Alert: Decodable {
    public var title: NSAttributedString?
    public var text: NSAttributedString?
    public var textType: TextType
    public var image: Image?
    public var buttonsJson: [[String: Any]]
    public var showCloseButton: Bool
    public var dismissWithGesture: Bool
    public var dismissOnTouchBackground: Bool
    
    public var overlayMaskAlpha: Double?

    enum RootKeys: String, CodingKey {
        case title
        case text
        case image
        case buttons
        case dismissWithGesture
        case showCloseButton
        case dismissOnTouchBackground
    }
    
    enum TextKeys: String, CodingKey {
        case value, type
    }
    
    public init(from decoder: Decoder) throws {
        let rootContainer = try decoder.container(keyedBy: RootKeys.self)
        
        let _title = try rootContainer.decode(String.self, forKey: .title)
        title = NSAttributedString(string: _title)
        
        let textContainer = try rootContainer.nestedContainer(keyedBy: TextKeys.self, forKey: .text)
        
        let _text = try textContainer.decode(String.self, forKey: .value)
        text = NSAttributedString(string: _text)
        textType = try textContainer.decode(TextType.self, forKey: .type)
        
        let _buttonsJson = try rootContainer.decodeIfPresent([Any].self, forKey: .buttons)
        
        buttonsJson = (_buttonsJson as? [[String: Any]]) ?? []
        
        image = try rootContainer.decodeIfPresent(Image.self, forKey: .image)
        
        dismissOnTouchBackground = try rootContainer.decodeIfPresent(Bool.self, forKey: .dismissOnTouchBackground) ?? false
        showCloseButton = try rootContainer.decodeIfPresent(Bool.self, forKey: .showCloseButton) ?? false
        dismissWithGesture = try rootContainer.decodeIfPresent(Bool.self, forKey: .dismissWithGesture) ?? false
    }
}
