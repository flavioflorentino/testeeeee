import Foundation

public struct RequestError: Decodable, Error {
    public var title: String
    public var message: String
    public var code: String
    public var messageId: Int?
    public var field: String?
    public var data: [String: Any] = [:]
    public var alert: Alert?
    public var errors: [RequestError] = []
    public var alertJson: [String: Any] = [:]
    public var jsonData: Data?
    
    enum RootKeys: String, CodingKey {
        case message
        case shortMessage
        case descriptionPt
        case title
        case picpayCode
        case code
        case messageId
        case field
        case data
        case ui = "_ui"
        case error = "Error"
        case errors
    }
    
    enum UIKeys: String, CodingKey {
        case alert
    }
    
    enum ErrorLegacyKeys: String, CodingKey {
        case descriptionPt
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: RootKeys.self)
        
        title = try container.decodeIfPresent(String.self, forKey: .shortMessage) ?? "Erro na requisição"
        data = try container.decodeIfPresent([String: Any].self, forKey: .data) ?? [:]
        field = try container.decodeIfPresent(String.self, forKey: .field)
        errors = try container.decodeIfPresent([RequestError].self, forKey: .errors) ?? []
        
        var _message = "Tente novamente mais tarde"
        if let m = try? container.decodeIfPresent(String.self, forKey: .message) {
            _message = m
        } else if let m = try? container.decodeIfPresent(String.self, forKey: .descriptionPt) {
            _message = m
        } else if let legacyErrorContainer = try? container.nestedContainer(keyedBy: ErrorLegacyKeys.self, forKey: .error), let m = try? legacyErrorContainer.decodeIfPresent(String.self, forKey: .descriptionPt) {
            _message = m
        }
        
        message = _message
        
        if let _code = try? container.decode(String.self, forKey: .code) {
            code = _code
        } else if let _code = try? container.decode(Int.self, forKey: .code) {
            code = String(_code)
        } else {
            code = "Empty code"
        }
        
        let uiContainer = try? container.nestedContainer(keyedBy: UIKeys.self, forKey: .ui)
        alert = try uiContainer?.decodeIfPresent(Alert.self, forKey: .alert)
        
        alertJson = try uiContainer?.decodeIfPresent([String: Any].self, forKey: .alert) ?? [:]
    }
    
    public init() {
        message = "Ocorreu um erro ao carregar as informações, tente novamente."
        title = "Problema na conexão"
        code = "Empty code"
    }
    
    init(title: String, message: String, code: String = "Empty code") {
        self.title = title
        self.message = message
        self.code = code
    }
    
    public static func initialize(with responseBody: Data?) -> RequestError {
        do {
            let jsonDecoder = JSONDecoder()
            jsonDecoder.dateDecodingStrategy = .formatted(DecodableDateFormatter())
            jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
            var decoded = try jsonDecoder.decode(RequestError.self, from: responseBody ?? Data())
            decoded.jsonData = responseBody
            return decoded
        } catch {
            #if DEBUG
            print("Decode PicPayError failure \(error))")
            #endif
            return RequestError()
        }
    }
}
