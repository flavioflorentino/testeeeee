import Foundation

public enum TextType: String, Decodable {
    case plain
    case html
}
