import Foundation

public enum ImageStyleType: String, Decodable {
    case round
    case square
    case standard
}

public enum ImageSourceType: String, Decodable {
    case asset
    case url
}

public struct Image: Decodable {
    private let value: String
    private let styleType: ImageStyleType
    private let sourceType: ImageSourceType
    
    enum CodingKeys: String, CodingKey {
        case url
        case asset
        case type
        case sourceType
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        var _value = ""
        if let url = try? container.decodeIfPresent(String.self, forKey: .url) {
            _value = url
        } else if let asset = try? container.decodeIfPresent(String.self, forKey: .asset) {
            _value = asset
        }
        
        value = _value
        styleType = try container.decodeIfPresent(ImageStyleType.self, forKey: .type) ?? .standard
        sourceType = try container.decodeIfPresent(ImageSourceType.self, forKey: .sourceType) ?? .url
    }
    
    init(name: String, style: ImageStyleType = .standard, source: ImageSourceType = .url) {
        styleType = style
        sourceType = source
        value = name
    }
}
