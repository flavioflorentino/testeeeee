import Foundation

public extension JSONDecoder {
    convenience init(_ decodingStrategy: JSONDecoder.KeyDecodingStrategy) {
        self.init()
        keyDecodingStrategy = decodingStrategy
    }
}
