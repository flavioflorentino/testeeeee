import Foundation

public extension JSONDecoder.KeyDecodingStrategy {
    static var lowercased: Self = .custom { keys in
        keys.last?.stringValue.lowercased() ?? ""
    }
}

extension String: CodingKey {
    public init?(stringValue: String) {
        self.init(stringLiteral: stringValue)
    }
    
    public var stringValue: String {
        self
    }
    
    public init?(intValue: Int) {
        self.init(intValue)
    }

    public var intValue: Int? {
        Int(self)
    }
}
