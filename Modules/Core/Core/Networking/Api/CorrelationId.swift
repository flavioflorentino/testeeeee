import Foundation

@objcMembers
public final class CorrelationId: NSObject {
    public static var id: String {
        UUID().uuidString
    }
    
    public static var headerField: String {
        "X-Request-ID"
    }
}
