import Foundation

/// Use this class to inject a custom default ApiCompletion interceptor
public enum ApiCompletionSetup {
    public static var Default: ApiCompletion.Type = ApiCompletion.self
    
    public static func inject(type defaultCompletion: ApiCompletion.Type) {
        ApiCompletionSetup.Default = defaultCompletion
    }
}

/// Subclass this to implement a custom ApiCompletion interceptor.
/// This class do not ensure the generic types at build time, use it only with the Core.Api.execute method.
open class ApiCompletion {
    public typealias Success<E: Decodable> = (model: E, data: Data?)
    public typealias ApiResult<E: Decodable> = Result<Success<E>, ApiError>
    public typealias Completion<E: Decodable> = (ApiResult<E>) -> Void

    private var completion: Any
    
    /// Do not create an ApiCompletion manually.
    public required init<E: Decodable>(_ completion: @escaping Completion<E>) {
        self.completion = completion
    }

    /// Override this func to implement a custom ApiCompletion interceptor handler
    open func intercept<E: Decodable>(_ result: ApiResult<E>, response: HTTPURLResponse? = nil, request: URLRequest? = nil) {
        guard let typedCompletion = completion as? Completion<E> else {
            assertionFailure("""
            ApiCompletion.execute(..) called with a mismatch generic type.
            Do not create or execute ApiCompletion's manually.
            """)
            return
        }
        typedCompletion(result)
    }
    
    internal func run<E: Decodable>(_ result: ApiResult<E>, response: HTTPURLResponse? = nil, request: URLRequest? = nil) {
        intercept(result, response: response, request: request)
    }
}
