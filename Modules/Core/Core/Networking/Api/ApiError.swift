import Foundation

public enum ApiError: Error {
    case notFound(body: RequestError)
    case unauthorized(body: RequestError)
    case badRequest(body: RequestError)
    case tooManyRequests(body: RequestError)
    case otherErrors(body: RequestError)
    case connectionFailure
    case cancelled
    case serverError
    case timeout
    case bodyNotFound
    case upgradeRequired
    case malformedRequest(_: String?)
    case decodeError(_: Error)
    case unknown(_: Error?)
    
    public var requestError: RequestError? {
        switch self {
        case .badRequest(let requestError),
             .notFound(let requestError),
             .unauthorized(let requestError),
             .otherErrors(let requestError),
             .tooManyRequests(let requestError):
            return requestError
        case .timeout:
            return RequestError(title: Strings.Error.title, message: Strings.Error.timeout)
        case .connectionFailure:
            return RequestError(title: Strings.Error.title, message: Strings.Error.connectionFailure)
        default:
            return nil
        }
    }
}
