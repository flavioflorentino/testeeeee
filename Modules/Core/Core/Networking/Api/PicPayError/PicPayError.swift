import Foundation

public struct PicPayError: Decodable {
    public var message: String
    public var title: String?
    public var picpayCode: String?
    public var messageId: Int?
    public var field: String?
    
    public var jsonData: Data?
    
    enum CodingKeys: String, CodingKey {
        case message, title, picpayCode, messageId, field
    }
    
    init() {
        message = String()
    }
    
    internal static func parse(with data: Data?) -> PicPayError? {
        guard let jsonData = data else {
            return nil
        }
        
        do {
            let decoder = JSONDecoder()
            let error = try decoder.decode(PicPayError.self, from: jsonData)
            return error
        } catch {
            print("Unable to parse json data to PicPayError \(error)")
        }
        
        return nil
    }
}
