import Foundation
import UIKit

public protocol AuthenticationManagerContract {
    func logout(fromContext context: UIViewController?, completion: ((Error?) -> Void)?)
}
