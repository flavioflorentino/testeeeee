import Foundation

public protocol TokenManagerContract {
    func updateToken(_ token: String)
    var token: String { get }
}
