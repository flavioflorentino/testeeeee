public protocol ProfileContract {
    func open(withId id: Int, image: String, isPro: Bool, username: String)
}
