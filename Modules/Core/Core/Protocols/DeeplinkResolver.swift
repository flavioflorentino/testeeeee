public protocol DeeplinkResolver {
    func canHandle(url: URL, isAuthenticated: Bool) -> DeeplinkResolverResult
    func open(url: URL, isAuthenticated: Bool) -> Bool
}

public enum DeeplinkResolverResult {
    case handleable
    case onlyWithAuth
    case notHandleable
}
