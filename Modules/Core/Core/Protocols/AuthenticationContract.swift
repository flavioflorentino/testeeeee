public typealias AuthenticationResult = (Result<String, AuthenticationStatus>) -> Void

public enum AuthenticationStatus: Error, Equatable {
    case nilPassword
    case cancelled
}

public protocol AuthenticationContract {
    var isAuthenticated: Bool { get }
    func authenticate(_ completion: @escaping AuthenticationResult)
    func disableBiometricAuthentication()
}
