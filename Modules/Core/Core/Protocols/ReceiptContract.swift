public protocol ReceiptContract {
    func open(withId id: String, receiptType: String, feedItemId: String?)
}

public extension ReceiptContract {
    func open(withId id: String, receiptType: String) {
        open(withId: id, receiptType: receiptType, feedItemId: nil)
    }
}
