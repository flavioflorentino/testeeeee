public protocol DeeplinkContract {
    @discardableResult
    func open(url: URL) -> Bool
}
