import Foundation

public protocol PinningHandlerProvider {
    func handle(
        challenge: URLAuthenticationChallenge,
        completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void
    )
}

@objc
public final class PinningHandler: NSObject, PinningHandlerProvider {
    private let configuration: PinningConfigurationApiProtocol
    
    @objc
    override public convenience init() {
        self.init(configuration: PinningConfigurationApi())
    }

    public init(configuration: PinningConfigurationApiProtocol) {
        self.configuration = configuration
        super.init()
    }

    @objc
    public func handle(
        challenge: URLAuthenticationChallenge,
        completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void
    ) {
        let hostName = NSString(string: challenge.protectionSpace.host)
        let pinDomain = configuration.shouldPinDomain(hostName: NSString(string: hostName)) == PinResponse.yes
        
        // Verify if by config flag the pin is enabled and if the host is inside of the domains decided to be pinned.
        guard configuration.shouldPin && pinDomain else {
            debugPrint("[PinningHandler] don't have to pin or isn't domain that have to pin, host:  \(hostName)")
            completionHandler(.performDefaultHandling, nil)
            return
        }
        
        guard let serverTrust = challenge.protectionSpace.serverTrust else {
            debugPrint("[PinningHandler] Couldn't get the server trust")
            completionHandler(.cancelAuthenticationChallenge, nil)
            return
        }
        
        let isServerTrusted: Bool = configuration.verifyTrustedServer(
            authenticationMethod: NSString(string: challenge.protectionSpace.authenticationMethod),
            serverTrust: serverTrust,
            hostDomain: hostName
            ) == PinResponse.yes
        
        guard
            isServerTrusted,
            configuration.shouldTrustServer(hostDomain: hostName, serverTrust: serverTrust) == .yes
            else {
                debugPrint("[PinningHandler] Server isn't trusted")
                completionHandler(.cancelAuthenticationChallenge, nil)
                return
        }
        
        let credential = URLCredential(trust: serverTrust)
        completionHandler(.useCredential, credential)
    }
}
