import Foundation

public final class URLSessionPinning: NSObject, URLSessionDelegate {
    private let pinningHandler: PinningHandlerProvider
    
    public init(pinningHandler: PinningHandlerProvider = PinningHandler()) {
        self.pinningHandler = pinningHandler
    }
    
    public func urlSession(
        _ session: URLSession,
        didReceive challenge: URLAuthenticationChallenge,
        completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void
    ) {
        pinningHandler.handle(challenge: challenge, completionHandler: completionHandler)
    }
}
