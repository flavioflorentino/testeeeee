import Foundation
import Security

/// Used instead of Bool for security purpose
@objc
public enum PinResponse: Int {
    case yes = 10
    case no = 11
}

@objc
public protocol PinningConfigurationApiProtocol: AnyObject {
    /// Verify in keychain if should pin the requests  or not
    @objc var shouldPin: Bool { get }
    
    /// Get the list of domains that the app should pin the requests from keychain
    var domainsTrusted: [String] { get }
    
    /// Verify the type of authentication method, policies of ssl and avaliate the server trust
    @objc
    func verifyTrustedServer(authenticationMethod: NSString, serverTrust: SecTrust?, hostDomain: NSString) -> PinResponse
    
    /// Verify if the host certificate is one of the ones that we can trust
    @objc
    func shouldTrustServer(hostDomain: NSString, serverTrust: SecTrust?) -> PinResponse
    
    /// Verify if the host should pass by pinning verification
    @objc
    func shouldPinDomain(hostName: NSString) -> PinResponse
}

public final class PinningConfigurationApi: NSObject, PinningConfigurationApiProtocol {
    private let keychain: KeychainManagerContract
    private let certificatesNames = ["AmazonRootCA1", "AmazonRootCA2", "AmazonRootCA3", "AmazonRootCA4", "SFSRootCAG2"]
    
    public init(keychain: KeychainManagerContract = KeychainManager()) {
        self.keychain = keychain
    }
    
    @objc public var shouldPin: Bool {
        keychain.getInt(key: KeychainKey.shouldPin) == PinResponse.yes.rawValue
    }
    
    public var domainsTrusted: [String] {
        guard let data = keychain.getData(key: KeychainKey.pinDomains) else {
            debugPrint("[PinningConfigurationApi] Couldn't retrieve domains from keychain")
            return []
        }
        
        return data.components(separatedBy: ",")
    }
  
    private func getCertificates() -> [Data] {
        let bundle = Bundle(for: type(of: self)) // path for certificates in Core
        
        let coreCertificates = certificatesNames.compactMap { name in
            getCertificate(name: name, bundle: bundle)
        }
        
        // Get certificates from app
        let appCertificates = certificatesNames.compactMap { getCertificate(name: $0) }
        
        return coreCertificates + appCertificates
    }
    
    private func getCertificate(name: String, bundle: Bundle = CoreResources.resourcesBundle) -> Data? {
        guard let filePath = bundle.path(forResource: name, ofType: "cer") else {
            return nil
        }
        do {
            return try Data(contentsOf: URL(fileURLWithPath: filePath))
        } catch {
            debugPrint("[PinningConfigurationApi] Problems in getting certificate data, error: \(error)")
            return nil
        }
    }
    
    @objc
    public func verifyTrustedServer(authenticationMethod: NSString, serverTrust: SecTrust?, hostDomain: NSString) -> PinResponse {
        guard
            let serverTrust = serverTrust, authenticationMethod.isEqual(to: NSURLAuthenticationMethodServerTrust),
            var result = SecTrustResultType(rawValue: 0)
            else {
                return PinResponse.no
        }
        
        let policies = SecPolicyCreateSSL(true, hostDomain as CFString)
        SecTrustSetPolicies(serverTrust, policies)
        SecTrustEvaluate(serverTrust, &result)
        
        return (result == SecTrustResultType.proceed || result == SecTrustResultType.unspecified) ? .yes : .no
    }
    
    @objc
    public func shouldTrustServer(hostDomain: NSString, serverTrust: SecTrust?) -> PinResponse {
        guard let serverTrust = serverTrust else {
            return PinResponse.no
        }
        let rootIndex = SecTrustGetCertificateCount(serverTrust) - 1
        let certificates = getCertificates()
        
        guard let serverCertificate = SecTrustGetCertificateAtIndex(serverTrust, rootIndex) else {
            return PinResponse.no
        }
        let serverCertificateData = SecCertificateCopyData(serverCertificate)
        
        return certificates.contains(serverCertificateData as Data) ? .yes : .no
    }
    
    @objc
    public func shouldPinDomain(hostName: NSString) -> PinResponse {
        let hostNameString = String(hostName)
        var containsDomain = PinResponse.no
        for domain in domainsTrusted where hostNameString.contains(domain) {
            containsDomain = PinResponse.yes
        }
        
        return containsDomain
    }
    
    // MARK: - Alamofire
    
    public func generateSecCertificates() -> [SecCertificate] {
        let certificates = getCertificates()
        
        let secCertificates: [SecCertificate] = certificates.compactMap { data in
            guard let certificate = SecCertificateCreateWithData(nil, data as CFData) else {
                debugPrint("[PinningConfigurationApi] Error in making certificate: \(data)")
                return nil
            }
            return certificate
        }
        return secCertificates
    }
    
    private var isRunningAtUnitTests: Bool {
        ProcessInfo.processInfo.environment["XCTestConfigurationFilePath"] != nil
    }
}
