import Foundation

typealias CoreDependencies = HasKeychainManager & HasKeychainManagerPersisted

final class DependencyContainer: CoreDependencies {    
    lazy var keychain: KeychainManagerContract = KeychainManager()
    lazy var keychainWithPersistent: KeychainManagerContract = KeychainManager(isPersistent: true)
}
