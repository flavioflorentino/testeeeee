public protocol HasLocationManager {
    var locationManager: LocationManagerContract { get }
}

public protocol HasKeychainManager {
    var keychain: KeychainManagerContract { get }
}

public protocol HasKeychainManagerPersisted {
    var keychainWithPersistent: KeychainManagerContract { get }
}
