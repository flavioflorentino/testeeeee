import Foundation

public protocol KVKeyContract: CustomStringConvertible {
}

public protocol KVStoreContract {
    func dumbObjCForKey(_ key: KVKeyContract) -> Any?
    func objectForKey(_ key: KVKeyContract) -> Any?
    func boolFor(_ key: KVKeyContract) -> Bool
    func intFor(_ key: KVKeyContract) -> Int
    func dictionaryFor(_ key: KVKeyContract) -> [String: Any]?
    func arrayFor(_ key: KVKeyContract) -> [Any]?
    func stringFor(_ key: KVKeyContract) -> String?

    func set(value: Any, with key: KVKeyContract)
    func setInt(_ value: Int, with key: KVKeyContract)
    func setBool(_ value: Bool, with key: KVKeyContract)
    func setString(_ value: String, with key: KVKeyContract)
    func setData(_ value: Data, with key: KVKeyContract)
    func setDate(_ value: Date, with key: KVKeyContract)

    func removeObjectFor(_ key: KVKeyContract)

    func setFirstTimeOnlyEvent(_ event: String)
    func getFirstTimeOnlyEvent(_ event: String) -> Bool
}

#if DEBUG

// MARK: - Mock
public final class KVStoreMock {
    // MARK: - Properties
    private lazy var storage: KVStoreManager = KVStoreManagerMock()

    // MARK: - Initialization
    public init() { }

    public func set(value: Any, with key: String) {
        save(value: value, key: key)
    }
}

extension KVStoreMock: KVStoreContract {
    public func dumbObjCForKey(_ key: KVKeyContract) -> Any? {
        objectForKey(key)
    }

    public func objectForKey(_ key: KVKeyContract) -> Any? {
        retrieve(key)
    }

    public func boolFor(_ key: KVKeyContract) -> Bool {
        retrieve(key) ?? false
    }

    public func intFor(_ key: KVKeyContract) -> Int {
        retrieve(key) ?? 0
    }

    public func dictionaryFor(_ key: KVKeyContract) -> [String : Any]? {
        retrieve(key)
    }

    public func arrayFor(_ key: KVKeyContract) -> [Any]? {
        retrieve(key)
    }

    public func stringFor(_ key: KVKeyContract) -> String? {
        retrieve(key)
    }

    public func set(value: Any, with key: KVKeyContract) {
        save(value: value, key: key)
    }

    public func setInt(_ value: Int, with key: KVKeyContract) {
        save(value: value, key: key)
    }

    public func setBool(_ value: Bool, with key: KVKeyContract) {
        save(value: value, key: key)
    }

    public func setString(_ value: String, with key: KVKeyContract) {
        save(value: value, key: key)
    }

    public func setData(_ value: Data, with key: KVKeyContract) {
        save(value: value, key: key)
    }

    public func setDate(_ value: Date, with key: KVKeyContract) {
        save(value: value, key: key)
    }

    public func removeObjectFor(_ key: KVKeyContract) {
        storage.removeObject(forKey: key.description)
    }

    public func setFirstTimeOnlyEvent(_ event: String) {
        storage.save(true, forKey: event)
    }

    public func getFirstTimeOnlyEvent(_ event: String) -> Bool {
        retrieve(event) ?? false
    }
}

private extension KVStoreMock {
    func retrieve<T>(_ key: KVKeyContract) -> T? {
        storage.object(forKey: key.description) as? T
    }

    func retrieve<T>(_ key: String) -> T? {
        storage.object(forKey: key) as? T
    }

    func save<T>(value: T, key: KVKeyContract) {
        save(value: value, key: key.description)
    }

    func save<T>(value: T, key: String) {
        storage.save(value, forKey: key.description)
    }
}

#endif
