import Foundation

@objc
public class KVStore: NSObject {
    private var storage: KVStoreManager
    
    @objc
    public override init() {
        self.storage = UserDefaults.standard
    }
    
    @objc
    public convenience init(storage: KVStoreManager) {
        self.init()
        self.storage = storage
    }
    
    // Mark - Public Methods
    @objc
    public func dumbObjCForKey(_ key: KVKey) -> Any? {
        return objectForKey(key)
    }

    public func retrieveObject<T: Decodable>(type: T.Type, key: KVKey) throws -> T {
       
        guard let data = retrieve(key) as? Data else {
            throw KVStoreError.dataIsInvalid
        }
        
        return try JSONDecoder().decode(type, from: data)
    }
    
    public func saveObject<T: Encodable>(value: T, key: KVKey) throws {
        let data = try JSONEncoder().encode(value)
        
        save(value: data, with: key)
    }
    
    public func objectForKey(_ key: KVKey) -> Any? {
        return retrieve(key)
    }
    
    @objc
    public func boolFor(_ key: KVKey) -> Bool {
        return retrieve(key) as? Bool ?? false
    }
    
    @objc
    public func intFor(_ key: KVKey) -> Int {
        return retrieve(key) as? Int ?? 0
    }
    
    @objc
    public func dictionaryFor(_ key: KVKey) -> [String :Any]? {
        return retrieve(key) as? [String: Any]
    }
    
    @objc
    public func arrayFor(_ key: KVKey) -> [Any]? {
        return retrieve(key) as? [Any]
    }
    
    @objc
    public func stringFor(_ key: KVKey) -> String? {
        return retrieve(key) as? String
    }

    @objc
    public func set(value: Any, with key: KVKey) {
        save(value: value, with: key)
    }
    
    @objc
    public func setInt(_ value: Int, with key: KVKey) {
        save(value: value, with: key)
    }
    
    @objc
    public func setBool(_ value: Bool, with key: KVKey) {
        save(value: value, with: key)
    }
    
    @objc
    public func setString(_ value: String, with key: KVKey) {
        save(value: value, with: key)
    }
    
    @objc
    public func setData(_ value: Data, with key: KVKey) {
        save(value: value, with: key)
    }
    
    @objc
    public func setDate(_ value: Date, with key: KVKey) {
        save(value: value, with: key)
    }
    
    @objc
    public func removeObjectFor(_ key: KVKey) {
        remove(key)
    }
    
    // Mark - Private Methods
    private func save(value: Any, with key: KVKey) {
        storage.save(value, forKey: describing(key))
    }

    private func save(value: Any, with key: KVKeyContract) {
        storage.save(value, forKey: key.description)
    }
    
    private func retrieve(_ key: KVKey) -> Any? {
        return storage.object(forKey: describing(key))
    }

    private func retrieve<T>(_ key: KVKeyContract) -> T? {
        storage.object(forKey: key.description) as? T
    }
    
    private func remove(_ key: KVKey) {
        storage.removeObject(forKey: describing(key))
    }

    private func remove(_ key: KVKeyContract) {
        storage.removeObject(forKey: key.description)
    }
}

// MARK: - KVStoreContract
extension KVStore: KVStoreContract {
    public func dumbObjCForKey(_ key: KVKeyContract) -> Any? {
        objectForKey(key)
    }

    public func objectForKey(_ key: KVKeyContract) -> Any? {
        retrieve(key)
    }

    public func boolFor(_ key: KVKeyContract) -> Bool {
        retrieve(key) ?? false
    }

    public func intFor(_ key: KVKeyContract) -> Int {
        retrieve(key) ?? 0
    }
    public func dictionaryFor(_ key: KVKeyContract) -> [String: Any]? {
        retrieve(key)
    }

    public func arrayFor(_ key: KVKeyContract) -> [Any]? {
        retrieve(key)
    }

    public func stringFor(_ key: KVKeyContract) -> String? {
        retrieve(key)
    }

    public func set(value: Any, with key: KVKeyContract) {
        save(value: value, with: key)
    }

    public func setInt(_ value: Int, with key: KVKeyContract) {
        save(value: value, with: key)
    }

    public func setBool(_ value: Bool, with key: KVKeyContract) {
        save(value: value, with: key)
    }

    public func setString(_ value: String, with key: KVKeyContract) {
        save(value: value, with: key)
    }

    public func setData(_ value: Data, with key: KVKeyContract) {
        save(value: value, with: key)
    }

    public func setDate(_ value: Date, with key: KVKeyContract) {
        save(value: value, with: key)
    }

    public func removeObjectFor(_ key: KVKeyContract) {
        remove(key)
    }
}

extension KVStore {
    // Objective-C don't functions inside ENUMs, that's why
    // we need this workaround
    private func describing(_ key: KVKey) -> String {
        return ObjectiveCWorkAround.enumDescriptionFor(key)
    }
    
    private class ObjectiveCWorkAround {
        static func enumDescriptionFor(_ key: KVKey) -> String {
            
            switch key {
            case .userToken:
                #if DEBUG
                return "\(String(describing: key))-Development"
                #else
                return String(describing: key)
                #endif
                
            case .locationAlwaysWithWhenInUse:
                return "permission.requestedLocationAlwaysWithWhenInUse"
            default:
                return String(describing: key)
            }
        }
    }
}

enum KVStoreError: Error {
    case dataIsInvalid
}

public extension KVStore {
    @objc
    func setFirstTimeOnlyEvent(_ event: String) {
        storage.save(true, forKey: event)
    }

    @objc
    func getFirstTimeOnlyEvent(_ event: String) -> Bool {
        return (storage.object(forKey: event) as? Bool) ?? false
    }
}

public extension KVStore {
    @objc
    func defineBrAsADefaultLocale() {
        storage.save(["br"], forKey: "AppleLanguage")
    }
}

//This is class is now used just to migration Token from UserDefaults to Keychain
//Now we use just Keychain to retrieve the User Token
@objc
class Token: NSObject {
    //I know!
    static let SuiteName = "group.com.picpay.PicPay"
    static let storage = KVStore(storage: UserDefaults.init(suiteName: SuiteName)!)
    
    @objc
    static func delete() {
        KVStore().removeObjectFor(.userToken)
        storage.removeObjectFor(.userToken)
    }
    
    static func setClearCache(to value: Bool) {
        storage.setBool(value, with: .clearCache)
    }
    
    @objc
    static func get() -> String? {
        return storage.stringFor(.userToken)
    }
}
