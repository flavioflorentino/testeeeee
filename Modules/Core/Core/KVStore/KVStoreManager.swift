import Foundation

@objc
public protocol KVStoreManager {
    func save(_ value: Any, forKey defaultName: String)
    func object(forKey defaultName: String) -> Any?
    func removeObject(forKey defaultName: String)
}

@objc
extension UserDefaults: KVStoreManager { }

public extension UserDefaults {
    func save(_ value: Any, forKey defaultName: String) {
        self.setValue(value, forKey: defaultName)
    }
}

#if DEBUG

public final class KVStoreManagerMock: KVStoreManager {
    private lazy var values: [String: Any] = [:]

    public init() { }

    public func save(_ value: Any, forKey defaultName: String) {
        values[defaultName] = value
    }

    public func object(forKey defaultName: String) -> Any? {
        values[defaultName]
    }

    public func removeObject(forKey defaultName: String) {
        values.removeValue(forKey: defaultName)
    }
}

#endif
