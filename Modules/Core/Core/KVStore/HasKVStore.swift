public protocol HasKVStore {
    var kvStore: KVStoreContract { get }
}
