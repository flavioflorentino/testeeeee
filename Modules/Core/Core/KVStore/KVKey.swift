import Foundation

@objc
public enum KVKey: Int {
    
    //MARK: - PPAuth
    case use_touch_id
    case touch_id_asked
    case isTouchIdKeychainBug
    case needUpdateBiometricAuthPin
    
    //MARK: - PPContatcList
    case identifyContactsCache
    case localContactsHash
    case remoteContactHash
    
    //MARK: - CreditCardManager
    case defaultCreditCardId
    case defaultDebitCardId
    
    //MARK: - Token
    case userToken
    case clearCache
    
    //MARK: - ConsumerManager
    case isPhoneVerified
    case authInfoUpdated
    case getAllConsumerData
    case onboardSocial
    case isBalanceHidden
    
    //MARK: - ConsumerConstants
    case showCreditPaymentMethodIntro
    
    //MARK: - DGBoletoFormViewModel
    case lastBoleto
    
    //MARK: - DGRechargeRecentsManager
    case DGRecents
    
    //MARK: - DGTVRechargeViewModel
    case tvRecharges
    
    //MARK: - CardsFormViewModel
    case cardsStatus
    
    //MARK: - CashbackReceipWidgetItem
    case cashbackPopup
    
    //MARK: - WSConsumer
    case userName
    case email
    case email_confirmed
    case email_pending
    
    //MARK: - AppParameters
    case verifiedNumber
    case notFirstTimeOnApp
    case hasAtLeastOneFeedOnce
    case phoneVerificationCallDelay
    case isEnabledPhoneVerificationCall
    case phoneVerificationSMSDelay
    case phoneVerificationWhatsAppDelay
    case phoneVerificationSentDate
    case phoneVerificationDDD
    case phoneVerificationNumber
    case isRegisterUsernameStep
    case askedForRegistrationRemoteNotification
    case installation_id
    case isRemoteConfigLoaded
    
    //Esse typo já estava, se for corrigir faça uma migração do antigo pro novo
    case privacyCconfig
    case privacyConfigSuggestionPromptCount
    case lastChosenPrivacyConfig
    case useBalance
    case useBalanceAlreadySetted
    
    // Mark - NewTransactionViewController
    case installmentsPopupShow
    
    //MARK: - UnloggedViewController
    case developmentApiHost
    
    //MARK: - PaymentViewController
    case installmentPopupShowedPAV
    
    //MARK: - DynamicLinkHelper
    case firebase_dynamic_link_deep_link
    case firebase_dynamic_link_referral_code
    
    //MARK: - PermissionLocationAlways
    case locationAlwaysWithWhenInUse
    
    //MARK: - Savings
    case savings_onboarding_saw
    
    //MARK: - Keychain
    case alreadyOpenApp
    
    // MARK: - Offer cards
    case listOfferCardsWalletDismissed
    
    //MARK: - Adyen3ds
    case viewedWarning3dsTransaction
    
    // MARK: - Credit PicPay
    case hasPresentedCreditPicpayOfferAlert
    
    // MARK: - Location coordinates
    case locationCoordinates
    
    //MARK: - DeepLink Storage
    case savedDeeplink
    
    // MARK - Student Account
    case sawBenefits
    case studentAccountActivePopup
    case hasStudentAccountActive
    
    //MARK: ATM24
    case atm24OfferAlert
    
    //MARK: Feature Manager
    case featureFeed
    case featureSearchMain
    case featureSearchStore
    case featureSearchLocale
    case featureSearchMainLocale
    case featureSearchSuggestions
    case featureSearchConsumers
    
    //MARK: Registration
    case unfinishedRegistrationCache
    case showAppTour
    
    //MARK: MGM
    case mgmIncentive
    case isFromReferralRecommendation
    case inviterName
    
    // MARK: PIX
    case isPixCashoutWelcomeVisualized
    case isPixKeyManagementWelcomeVisualized
    case isPixAvailableFeedbackVisualized
    case isPixModalHomeVisualized
    case lastPix
    
    // MARK : PixKeyManager
    case showedPreRegisteredFlow
    
    // MARK: Review Privacy
    case isUserReviewPrivacy
    
    // MARK: Store
    case featureShowStoreTooltipQuantity
    case featureShowSettingsTooltipQuantity
    
    // MARK: Direct Message
    case hasPresentedBetaAlert

    // MARK: Keychain Migration
    case hasMigratedAuthToken
}

// Helper extension to be used only by PPAnalytics
enum KVKeyAnalytics {
    //case "FT tela transação P2P"
    //"FT tela transação Pagamento Fixo"
    //FT Notificacoes
    //FT notifications
    //"FT tela transação PAV"
    //"Respondeu requisição de contatos do iOS"
    //"Permissao autorizada - Contatos"
    //"Permissao autorizada - Camera"
    //"Permissao autorizada - Localizacao"
    //"FT Inicio"
    //"ID Validation - Acessou FT"
    //"FT Carteira"
    //"FT Pagar - " name
    //"FT Ajustes"
    //"FT saque"
    //"Permissao autorizada - Notificacao"
}

// TL;DR: Shit to work on Obj-c
// When a enum with @objc is used on objective-c, at run time the cases are converted
// to Int, so.. when String(describing: ) is used, the return is the name of the enum,
// in this case, KVKey. When KVStore cease to be used on obj-c classes, this extension
// should be deleted. Meanwhile, we will have to deal with it.
extension KVKey: CustomStringConvertible {
    public var description: String {
        switch self {
        case .use_touch_id: return "use_touch_id"
        case .touch_id_asked: return "touch_id_asked"
        case .isTouchIdKeychainBug: return "isTouchIdKeychainBug"
        case .needUpdateBiometricAuthPin: return "needUpdateBiometricAuthPin"
        case .identifyContactsCache: return "identifyContactsCache"
        case .localContactsHash: return "localContactsHash"
        case .remoteContactHash: return "remoteContactHash"
        case .defaultCreditCardId: return "defaultCreditCardId"
        case .userToken: return "userToken"
        case .clearCache: return "clearCache"
        case .isPhoneVerified: return "isPhoneVerified"
        case .authInfoUpdated: return "authInfoUpdated"
        case .getAllConsumerData: return "getAllConsumerData"
        case .onboardSocial: return "onboardSocial"
        case .isBalanceHidden: return "isBalanceHidden"
        case .showCreditPaymentMethodIntro: return "showCreditPaymentMethodIntro"
        case .lastBoleto: return "lastBoleto"
        case .DGRecents: return "DGRecents"
        case .tvRecharges: return "tvRecharges"
        case .cardsStatus: return "cardsStatus"
        case .cashbackPopup: return "cashbackPopup"
        case .userName: return "userName"
        case .email: return "email"
        case .email_confirmed: return "email_confirmed"
        case .email_pending: return "email_pending"
        case .verifiedNumber: return "verifiedNumber"
        case .notFirstTimeOnApp: return "notFirstTimeOnApp"
        case .hasAtLeastOneFeedOnce: return "hasAtLeastOneFeedOnce"
        case .phoneVerificationCallDelay: return "phoneVerificationCallDelay"
        case .isEnabledPhoneVerificationCall: return "isEnabledPhoneVerificationCall"
        case .phoneVerificationSMSDelay: return "phoneVerificationSMSDelay"
        case .phoneVerificationWhatsAppDelay: return "phoneVerificationWhatsAppDelay"
        case .phoneVerificationSentDate: return "phoneVerificationSentDate"
        case .phoneVerificationDDD: return "phoneVerificationDDD"
        case .phoneVerificationNumber: return "phoneVerificationNumber"
        case .isRegisterUsernameStep: return "isRegisterUsernameStep"
        case .askedForRegistrationRemoteNotification: return "askedForRegistrationRemoteNotification"
        case .installation_id: return "installation_id"
        case .isRemoteConfigLoaded: return "remoteConfigLoaded"
        case .privacyCconfig: return "privacyCconfig"
        case .privacyConfigSuggestionPromptCount: return "privacyConfigSuggestionPromptCount"
        case .lastChosenPrivacyConfig: return "lastChosenPrivacyConfig"
        case .useBalance: return "useBalance"
        case .useBalanceAlreadySetted: return "useBalanceAlreadySetted"
        case .installmentsPopupShow: return "installmentsPopupShow"
        case .developmentApiHost: return "developmentApiHost"
        case .installmentPopupShowedPAV: return "installmentPopupShowedPAV"
        case .firebase_dynamic_link_deep_link: return "firebase_dynamic_link_deep_link"
        case .firebase_dynamic_link_referral_code: return "firebase_dynamic_link_referral_code"
        case .locationAlwaysWithWhenInUse: return "locationAlwaysWithWhenInUse"
        case .savings_onboarding_saw: return "savings_onboarding_saw"
        case .alreadyOpenApp: return "alreadyOpenApp"
        case .listOfferCardsWalletDismissed: return "list_offer_cards_wallet_dismissed"
        case .viewedWarning3dsTransaction: return "viewedWarning3dsTransaction"
        case .hasPresentedCreditPicpayOfferAlert: return "has_presented_credit_picpay_offer_alert"
        case .locationCoordinates: return "locationCoordinates"
        case .defaultDebitCardId: return "default_debit_cardId"
        case .savedDeeplink: return "savedDeeplink"
        case .sawBenefits: return "sawBenefits"
        case .studentAccountActivePopup: return "studentAccountActivePopup"
        case .hasStudentAccountActive: return "hasStudentAccountActive"
        case .atm24OfferAlert: return "atm24OfferAlert"
            
        case .featureFeed: return "feature_feed"
        case .featureSearchMain: return "feature_search_main"
        case .featureSearchStore: return "feature_search_store"
        case .featureSearchLocale: return "feature_search_locale"
        case .featureSearchMainLocale: return "feature_search_main_locale"
        case .featureSearchSuggestions: return "feature_search_suggestions"
        case .featureSearchConsumers: return "feature_search_consumers"
            
        case .unfinishedRegistrationCache: return "unfinishedRegistrationCache"
        case .showAppTour: return "showAppTour"
        case .mgmIncentive: return "mgmIncentive"
        case .isFromReferralRecommendation: return "isFromReferralRecommendation"
        case .inviterName: return "inviterName"
            
        case .isPixKeyManagementWelcomeVisualized: return "isPixKeyManagementWelcomeVisualized"
        case .isPixModalHomeVisualized: return "isPixModalHomeVisualized"
        case .isPixCashoutWelcomeVisualized: return "isPixCashoutWelcomeVisualized"
        case .lastPix: return "lastPix"
            
        case .showedPreRegisteredFlow: return "showedPreRegisteredFlow"
        case .isPixAvailableFeedbackVisualized: return "isPixAvailableFeedbackVisualized"
            
        case .isUserReviewPrivacy: return "isUserReviewPrivacy"
        
        case .featureShowStoreTooltipQuantity: return "featureShowStoreTooltipQuantity"
        case .featureShowSettingsTooltipQuantity: return "featureShowSettingsTooltipQuantity"
            
        case .hasPresentedBetaAlert: return "hasPresentedBetaAlert"
        case .hasMigratedAuthToken: return "hasMigratedAuthToken"
        }
    }
}

extension KVKey: KVKeyContract { }
