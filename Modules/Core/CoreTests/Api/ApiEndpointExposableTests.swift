import XCTest
@testable import Core

private final class ApiEndpointMock: ApiEndpointExposable {
    let givenBaseURL: URL
    let pathString: String

    init(baseURL: URL, pathString: String) {
        self.givenBaseURL = baseURL
        self.pathString = pathString
    }

    var path: String { pathString }

    var baseURL: URL { givenBaseURL }
}

final class ApiEndpointExposableTests: XCTestCase {
    func testAbsoluteStringUrl_WhenBasePathEndsWithSlashPathStartWithSlash_ShouldReturnCorrectUrl() throws {
        let url = try XCTUnwrap(URL(string: "http://some.niceuri.com/"))
        let endpoint = ApiEndpointMock(
            baseURL: url,
            pathString: "/api/dosomestuff"
        )

        XCTAssertEqual(endpoint.absoluteStringUrl, "http://some.niceuri.com/api/dosomestuff")
    }

    func testAbsoluteStringUrl_WhenBasePathEndsWithoutSlashPathStartWithSlash_ShouldReturnCorrectUrl() throws {
        let url = try XCTUnwrap(URL(string: "http://some.niceuri.com"))
        let endpoint = ApiEndpointMock(
            baseURL: url,
            pathString: "/api/dosomestuff"
        )

        XCTAssertEqual(endpoint.absoluteStringUrl, "http://some.niceuri.com/api/dosomestuff")
    }

    func testAbsoluteStringUrl_WhenBasePathEndsWithSlashPathDontStartWithSlash_ShouldReturnCorrectUrl() throws {
        let url = try XCTUnwrap(URL(string: "http://some.niceuri.com/"))
        let endpoint = ApiEndpointMock(
            baseURL: url,
            pathString: "api/dosomestuff"
        )

        XCTAssertEqual(endpoint.absoluteStringUrl, "http://some.niceuri.com/api/dosomestuff")
    }

    func testAbsoluteStringUrl_WhenBasePathEndsWithoutSlashPathDontStartWithSlash_ShouldReturnCorrectUrl() throws {
        let url = try XCTUnwrap(URL(string: "http://some.niceuri.com"))
        let endpoint = ApiEndpointMock(
            baseURL: url,
            pathString: "api/dosomestuff"
        )

        XCTAssertEqual(endpoint.absoluteStringUrl, "http://some.niceuri.com/api/dosomestuff")
    }
    
    func testAbsoluteStringUrl_WhenBasePathEndsWithoutSlashPathDontStartWithSlashAndQueryParams_ShouldReturnCorrectUrl() throws {
        let url = try XCTUnwrap(URL(string: "http://some.niceuri.com"))
        let endpoint = ApiEndpointMock(
            baseURL: url,
            pathString: "/someapth/withstuff/andparams?external_id=bill-1001344&external_key=123"
        )
        
        XCTAssertEqual(
            endpoint.absoluteStringUrl,
            "http://some.niceuri.com/someapth/withstuff/andparams?external_id=bill-1001344&external_key=123"
        )
    }
    func testAbsoluteStringUrl_WhenBasePathEndWithSlashAndPathIsEmpty_ShouldKeepBasePathWithoutTrailingSlash() throws {
        let url = try XCTUnwrap(URL(string: "http://some.niceuri.com/"))
        let endpoint = ApiEndpointMock(
            baseURL: url,
            pathString: ""
        )

        XCTAssertEqual(
            endpoint.absoluteStringUrl,
            "http://some.niceuri.com"
        )
    }
    func testAbsoluteStringUrl_WhenBasePathEndWithoutSlashAndPathIsEmpty_ShouldKeepBasePathWithoutTrailingSlash() throws {
        let url = try XCTUnwrap(URL(string: "http://some.niceuri.com"))
        let endpoint = ApiEndpointMock(
            baseURL: url,
            pathString: ""
        )

        XCTAssertEqual(
            endpoint.absoluteStringUrl,
            "http://some.niceuri.com"
        )
    }
}
