import XCTest
@testable import Core

class ContentTypeTests: XCTestCase {
    func testMultipartFormData_WhenBoundaryIsNotNil_ShouldReturnMultipartFormDataWithBoundary() {
        let expectedResult = "multipart/form-data; boundary=XYZ"
        
        let type = ContentType.multipartFormData(boundary: "XYZ")
        
        XCTAssertEqual(type.rawValue, expectedResult)
    }
    
    func testMultipartFormData_WhenBoundaryIsNil_ShouldReturnDefaultMultipartFormData() {
        let expectedResult = "multipart/form-data"
        
        let type = ContentType.multipartFormData()
        
        XCTAssertEqual(type.rawValue, expectedResult)
    }
    
    func testContentType_WhenRawValueIsInvoked_ShouldReturnDefaultContentTypeValue() {
        XCTAssertEqual(ContentType.applicationAtomXml.rawValue, "application/atom+xml")
        XCTAssertEqual(ContentType.applicationFormUrlEncoded.rawValue, "application/x-www-form-urlencoded")
        XCTAssertEqual(ContentType.applicationJson.rawValue, "application/json")
        XCTAssertEqual(ContentType.applicationOctetStream.rawValue, "application/octet-stream")
        XCTAssertEqual(ContentType.applicationSvgXml.rawValue, "application/svg+xml")
        XCTAssertEqual(ContentType.applicationXhtmlXml.rawValue, "application/xhtml+xml")
        XCTAssertEqual(ContentType.applicationXml.rawValue, "application/xml")
        XCTAssertEqual(ContentType.textHtml.rawValue, "text/html")
        XCTAssertEqual(ContentType.textPlain.rawValue, "text/plain")
        XCTAssertEqual(ContentType.textXml.rawValue, "text/xml")
        XCTAssertEqual(ContentType.wildcard.rawValue, "*/*")
    }
}
