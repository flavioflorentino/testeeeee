import XCTest
@testable import Core

final class CorrelationIdTest: XCTestCase {
    func testHeaderField_ShouldRetunrHeaderName() {
        let expectedResult = CorrelationId.headerField
        
        XCTAssertEqual(expectedResult, "X-Request-ID")
    }
    
    
    func testId_ShouldReturnDifferentValuePerCall() {
        var expectedOriginal: [String] = []
        for _ in 0..<5 {
            expectedOriginal.append(CorrelationId.id)
        }
        
        let expectedUnique = Set(expectedOriginal)
        
        XCTAssertEqual(expectedOriginal.count, 5)
        XCTAssertEqual(expectedUnique.count, 5)
    }
}
