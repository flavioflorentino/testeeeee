import XCTest
@testable import Core

fileprivate struct SomeCodable: Codable{
    let id: Int
    let genericObject: [String:AnyCodable]
}

fileprivate struct Seeds {
    static let rawElementsJson : String = """
    {
        "int": 12345,
        "string": "Foo Bar",
        "double": 28.787833,
        "bool": true
    }
    """
    
    static let nestedObjectJson : String = """
    {
        "dictionary": {
            "int": 12345,
            "string": "Foo Bar",
            "double": 28.787833,
            "bool": true
        }
    }
    """
    
    static let nestedArrayJson : String = """
    {
       "array": [
           12345,
           "Foo Bar",
           28.787833,
           true
       ]
    }
    """
    
    static let arrayRawElementsJson: String = """
    [
        12345,
        "Foo Bar",
        28.787833,
        true
    ]
    """
    
    static let arrayNestedObjectJson: String = """
    [
        {
            "int": 12345,
            "string": "Foo Bar",
            "double": 28.787833,
            "bool": true
        },
        12345,
        "Foo Bar",
        28.787833,
        true
    ]
    """
    
    static let arrayNestedArrayJson: String = """
       [
           [
               12345,
               "Foo Bar",
               28.787833,
               true
           ],
           12345,
           "Foo Bar",
           28.787833,
           true
       ]
       """
    
    static let complexObject: String = """
    {
        "id": 12345,
        "name": "Foo",
        "logged": true,
        "value": 1.50,
        "items": ["Foo", "Bar"],
        "dict": {
            "string": "Foo",
            "int": 1,
            "bool": true,
            "double": 3.2455453
        },
        "array": [{
            "name": "Foo",
            "age": 30
        }, {
            "name": "Bar",
            "age": 28
        }]
    }
    """
    
    static let someCodable: String = """
    {
        "id": 1111,
        "genericObject": {
            "int": 12345,
            "string": "Foo Bar",
            "double": 28.787833,
            "bool": false
        }
    }
    """
    
    static let jsonWithNullObject: String = """
       {
           "id": 1111,
           "string": null
       }
       """
}

class AnyCodableTests: XCTestCase {
    
    // Object
    
    func testInitCoder_WhenObjectWithRawValues_ShouldBeDecoded() throws{
        let jsonData = try XCTUnwrap(Seeds.rawElementsJson.data(using: .utf8))
        
        let sut = try? JSONDecoder().decode(AnyCodable.self, from: jsonData)
        
        let value = try XCTUnwrap(sut?.value)
        let dictionary = try XCTUnwrap(value as? [String: Any])
        
        XCTAssertEqual("Foo Bar", dictionary["string"] as? String)
        XCTAssertEqual(12345, dictionary["int"] as? Int)
        XCTAssertEqual(28.787833, dictionary["double"] as? Double)
        XCTAssertEqual(true, dictionary["bool"] as? Bool)
    }
    
    func testEncode_WhenObjectWithRawValues_ShouldBeEncoded() throws{
        let jsonData = try XCTUnwrap(Seeds.rawElementsJson.data(using: .utf8))
        
        let decoded = try? JSONDecoder().decode(AnyCodable.self, from: jsonData)
        let sut = try? JSONEncoder().encode(decoded)
        
        let unwrappedSut = try XCTUnwrap(sut)
        
        let redecoded = try? JSONSerialization.jsonObject(with: unwrappedSut, options: [])
        let object = try XCTUnwrap(redecoded as? [String:Any])
        
        XCTAssertEqual(12345, object["int"] as? Int)
        XCTAssertEqual("Foo Bar", object["string"] as? String)
        XCTAssertEqual(28.787833, object["double"] as? Double)
        XCTAssertEqual(true, object["bool"] as? Bool)
    }
    
    func testInitCoder_WhenObjectWithNestedObjectValues_ShouldBeDecoded() throws{
        let jsonData = try XCTUnwrap(Seeds.nestedObjectJson.data(using: .utf8))
        
        let sut = try? JSONDecoder().decode(AnyCodable.self, from: jsonData)
        
        let value = try XCTUnwrap(sut?.value)
        let dictionary = try XCTUnwrap(value as? [String: Any])
        let nestedDictionary = try XCTUnwrap(dictionary["dictionary"] as? [String: Any])
        
        XCTAssertEqual("Foo Bar", nestedDictionary["string"] as? String)
        XCTAssertEqual(12345, nestedDictionary["int"] as? Int)
        XCTAssertEqual(28.787833, nestedDictionary["double"] as? Double)
        XCTAssertEqual(true, nestedDictionary["bool"] as? Bool)
    }
    
    func testEncode_WhenObjectWithNestedObjectValues_ShouldBeEncoded() throws{
        let jsonData = try XCTUnwrap(Seeds.nestedObjectJson.data(using: .utf8))
        
        let decoded = try? JSONDecoder().decode(AnyCodable.self, from: jsonData)
        let sut = try? JSONEncoder().encode(decoded)
        
        let unwrappedSut = try XCTUnwrap(sut)
        
        let redecoded = try? JSONSerialization.jsonObject(with: unwrappedSut, options: [])
        let object = try XCTUnwrap(redecoded as? [String:Any])
        let nestedObject = try XCTUnwrap(object["dictionary"] as? [String:Any])
        
        XCTAssertEqual(12345, nestedObject["int"] as? Int)
        XCTAssertEqual("Foo Bar", nestedObject["string"] as? String)
        XCTAssertEqual(28.787833, nestedObject["double"] as? Double)
        XCTAssertEqual(true, nestedObject["bool"] as? Bool)
    }
    
    func testInitCoder_WhenObjectWithNestedArrayValues_ShouldBeDecoded() throws{
        let jsonData = try XCTUnwrap(Seeds.nestedArrayJson.data(using: .utf8))
        
        let sut = try? JSONDecoder().decode(AnyCodable.self, from: jsonData)
        
        let value = try XCTUnwrap(sut?.value)
        let dictionary = try XCTUnwrap(value as? [String: Any])
        let nestedArray = try XCTUnwrap(dictionary["array"] as? [Any])
        
        XCTAssertEqual(12345, nestedArray[0] as? Int)
        XCTAssertEqual("Foo Bar", nestedArray[1] as? String)
        XCTAssertEqual(28.787833, nestedArray[2] as? Double)
        XCTAssertEqual(true, nestedArray[3] as? Bool)
    }
    
    func testEncode_WhenObjectWithNestedArrayValues_ShouldBeEncoded() throws{
        let jsonData = try XCTUnwrap(Seeds.nestedArrayJson.data(using: .utf8))
        
        let decoded = try? JSONDecoder().decode(AnyCodable.self, from: jsonData)
        let sut = try? JSONEncoder().encode(decoded)
        
        let unwrappedSut = try XCTUnwrap(sut)
        
        let redecoded = try? JSONSerialization.jsonObject(with: unwrappedSut, options: [])
        let object = try XCTUnwrap(redecoded as? [String:Any])
        let nestedArray = try XCTUnwrap(object["array"] as? [Any])
        
        XCTAssertEqual(12345, nestedArray[0] as? Int)
        XCTAssertEqual("Foo Bar", nestedArray[1] as? String)
        XCTAssertEqual(28.787833, nestedArray[2] as? Double)
        XCTAssertEqual(true, nestedArray[3] as? Bool)
    }
    
    // Array
    
    func testInitCoder_WhenArrayWithRawValues_ShouldBeDecoded() throws{
        let jsonData = try XCTUnwrap(Seeds.arrayRawElementsJson.data(using: .utf8))
        
        let sut = try? JSONDecoder().decode(AnyCodable.self, from: jsonData)
        let value = try XCTUnwrap(sut?.value)
        let array = try XCTUnwrap(value as? [Any])
        
        XCTAssertEqual(12345, array[0] as? Int)
        XCTAssertEqual("Foo Bar", array[1] as? String)
        XCTAssertEqual(28.787833, array[2] as? Double)
        XCTAssertEqual(true, array[3] as? Bool)
    }
    
    func testEncode_WhenArrayWithRawValues_ShouldBeEncoded() throws{
        let jsonData = try XCTUnwrap(Seeds.arrayRawElementsJson.data(using: .utf8))
        
        let decoded = try? JSONDecoder().decode(AnyCodable.self, from: jsonData)
        let sut = try? JSONEncoder().encode(decoded)
        
        let unwrappedSut = try XCTUnwrap(sut)
        
        let redecoded = try? JSONSerialization.jsonObject(with: unwrappedSut, options: [])
        let array = try XCTUnwrap(redecoded as? [Any])
        
        XCTAssertEqual(12345, array[0] as? Int)
        XCTAssertEqual("Foo Bar", array[1] as? String)
        XCTAssertEqual(28.787833, array[2] as? Double)
        XCTAssertEqual(true, array[3] as? Bool)
    }
    
    func testInitCoder_WhenArrayWithNestedObjectValues_ShouldBeDecoded() throws{
        let jsonData = try XCTUnwrap(Seeds.arrayNestedObjectJson.data(using: .utf8))
        
        let sut = try? JSONDecoder().decode(AnyCodable.self, from: jsonData)
        let value = try XCTUnwrap(sut?.value)
        let array = try XCTUnwrap(value as? [Any])
        let nestedDictionary = try XCTUnwrap(array[0] as? [String:Any])
        
        XCTAssertEqual(12345, array[1] as? Int)
        XCTAssertEqual("Foo Bar", array[2] as? String)
        XCTAssertEqual(28.787833, array[3] as? Double)
        XCTAssertEqual(true, array[4] as? Bool)
        
        XCTAssertEqual("Foo Bar", nestedDictionary["string"] as? String)
        XCTAssertEqual(12345, nestedDictionary["int"] as? Int)
        XCTAssertEqual(28.787833, nestedDictionary["double"] as? Double)
        XCTAssertEqual(true, nestedDictionary["bool"] as? Bool)
    }
    
    func testEncode_WhenArrayWithNestedObjectValues_ShouldBeEncoded() throws{
        let jsonData = try XCTUnwrap(Seeds.arrayNestedObjectJson.data(using: .utf8))
        
        let decoded = try? JSONDecoder().decode(AnyCodable.self, from: jsonData)
        let sut = try? JSONEncoder().encode(decoded)
        
        let unwrappedSut = try XCTUnwrap(sut)
        
        let redecoded = try? JSONSerialization.jsonObject(with: unwrappedSut, options: [])
        let array = try XCTUnwrap(redecoded as? [Any])
        let nestedDictionary = try XCTUnwrap(array[0] as? [String:Any])
        
        XCTAssertEqual(12345, array[1] as? Int)
        XCTAssertEqual("Foo Bar", array[2] as? String)
        XCTAssertEqual(28.787833, array[3] as? Double)
        XCTAssertEqual(true, array[4] as? Bool)
        
        XCTAssertEqual("Foo Bar", nestedDictionary["string"] as? String)
        XCTAssertEqual(12345, nestedDictionary["int"] as? Int)
        XCTAssertEqual(28.787833, nestedDictionary["double"] as? Double)
        XCTAssertEqual(true, nestedDictionary["bool"] as? Bool)
    }
    
    func testInitCoder_WhenArrayWithNestedArrayValues_ShouldBeDecoded() throws{
        let jsonData = try XCTUnwrap(Seeds.arrayNestedArrayJson.data(using: .utf8))
        
        let sut = try? JSONDecoder().decode(AnyCodable.self, from: jsonData)
        let value = try XCTUnwrap(sut?.value)
        let array = try XCTUnwrap(value as? [Any])
        let nestedArray = try XCTUnwrap(array[0] as? [Any])
        
        XCTAssertEqual(12345, array[1] as? Int)
        XCTAssertEqual("Foo Bar", array[2] as? String)
        XCTAssertEqual(28.787833, array[3] as? Double)
        XCTAssertEqual(true, array[4] as? Bool)
        
        XCTAssertEqual(12345, nestedArray[0] as? Int)
        XCTAssertEqual("Foo Bar", nestedArray[1] as? String)
        XCTAssertEqual(28.787833, nestedArray[2] as? Double)
        XCTAssertEqual(true, nestedArray[3] as? Bool)
    }
    
    func testEncode_WhenArraytWithNestedArrayValues_ShouldBeEncoded() throws{
        let jsonData = try XCTUnwrap(Seeds.arrayNestedArrayJson.data(using: .utf8))
        
        let decoded = try? JSONDecoder().decode(AnyCodable.self, from: jsonData)
        let sut = try? JSONEncoder().encode(decoded)
        
        let unwrappedSut = try XCTUnwrap(sut)
        
        let redecoded = try? JSONSerialization.jsonObject(with: unwrappedSut, options: [])
        let array = try XCTUnwrap(redecoded as? [Any])
        let nestedArray = try XCTUnwrap(array[0] as? [Any])
        
        XCTAssertEqual(12345, array[1] as? Int)
        XCTAssertEqual("Foo Bar", array[2] as? String)
        XCTAssertEqual(28.787833, array[3] as? Double)
        XCTAssertEqual(true, array[4] as? Bool)
        
        XCTAssertEqual(12345, nestedArray[0] as? Int)
        XCTAssertEqual("Foo Bar", nestedArray[1] as? String)
        XCTAssertEqual(28.787833, nestedArray[2] as? Double)
        XCTAssertEqual(true, nestedArray[3] as? Bool)
    }
    
    // Complex Object
    
    func testInitCoder_WhenComplexObject_ShouldBeDecoded() throws{
        let jsonData = try XCTUnwrap(Seeds.complexObject.data(using: .utf8))
        
        let sut = try? JSONDecoder().decode(AnyCodable.self, from: jsonData)
        let value = try XCTUnwrap(sut?.value)
        let object = try XCTUnwrap(value as? [String:Any])
        let dictionary = try XCTUnwrap(object["dict"] as? [String:Any])
        let array = try XCTUnwrap(object["array"] as? [Any])
        let nestedArrayObject1 = try XCTUnwrap(array[0] as? [String:Any])
        let nestedArrayObject2 = try XCTUnwrap(array[1] as? [String:Any])
        
        XCTAssertEqual(12345, object["id"] as? Int)
        XCTAssertEqual("Foo", object["name"] as? String)
        XCTAssertEqual(true, object["logged"] as? Bool)
        XCTAssertEqual(1.50, object["value"] as? Double)
        
        XCTAssertEqual("Foo", dictionary["string"] as? String)
        XCTAssertEqual(1, dictionary["int"] as? Int)
        XCTAssertEqual(true, dictionary["bool"] as? Bool)
        XCTAssertEqual(3.2455453, dictionary["double"] as? Double)
    
        XCTAssertEqual("Foo", nestedArrayObject1["name"] as? String)
        XCTAssertEqual(30, nestedArrayObject1["age"] as? Int)
        
        XCTAssertEqual("Bar", nestedArrayObject2["name"] as? String)
        XCTAssertEqual(28, nestedArrayObject2["age"] as? Int)
    }
    
    func testEncode_WhenComplexObject_ShouldBeEncoded() throws{
        let jsonData = try XCTUnwrap(Seeds.complexObject.data(using: .utf8))
        
        let decoded = try? JSONDecoder().decode(AnyCodable.self, from: jsonData)
        let sut = try? JSONEncoder().encode(decoded)
        
        let unwrappedSut = try XCTUnwrap(sut)
        let redecoded = try? JSONSerialization.jsonObject(with: unwrappedSut, options: [])
        
        let object = try XCTUnwrap(redecoded as? [String:Any])
        let dictionary = try XCTUnwrap(object["dict"] as? [String:Any])
        let array = try XCTUnwrap(object["array"] as? [Any])
        let nestedArrayObject1 = try XCTUnwrap(array[0] as? [String:Any])
        let nestedArrayObject2 = try XCTUnwrap(array[1] as? [String:Any])
        
        XCTAssertEqual(12345, object["id"] as? Int)
        XCTAssertEqual("Foo", object["name"] as? String)
        XCTAssertEqual(true, object["logged"] as? Bool)
        XCTAssertEqual(1.50, object["value"] as? Double)
        
        XCTAssertEqual("Foo", dictionary["string"] as? String)
        XCTAssertEqual(1, dictionary["int"] as? Int)
        XCTAssertEqual(true, dictionary["bool"] as? Bool)
        XCTAssertEqual(3.2455453, dictionary["double"] as? Double)
        
        XCTAssertEqual("Foo", nestedArrayObject1["name"] as? String)
        XCTAssertEqual(30, nestedArrayObject1["age"] as? Int)
        
        XCTAssertEqual("Bar", nestedArrayObject2["name"] as? String)
        XCTAssertEqual(28, nestedArrayObject2["age"] as? Int)
    }
    
    // Collection - Dictionary
    
    func testInitCoder_WhenObjectWithRawValuesDecodedInDictionary_ShouldBeDecoded() throws{
        let jsonData = try XCTUnwrap(Seeds.rawElementsJson.data(using: .utf8))
        
        let sut = try? JSONDecoder().decode([String:AnyCodable].self, from: jsonData)
        
        XCTAssertNotNil(sut)
    }
    
    func testEncode_WhenObjectWithRawValuesDecodedInDictionary_ShouldBeEncoded() throws{
        let jsonData = try XCTUnwrap(Seeds.rawElementsJson.data(using: .utf8))
        
        let decoded = try? JSONDecoder().decode([String:AnyCodable].self, from: jsonData)
        let sut = try? JSONEncoder().encode(decoded)
        
        XCTAssertNotNil(sut)
    }
    
    func testInitCoder_WhenObjectWithNestedObjectValuesDecodedInDictionary_ShouldBeDecoded() throws{
        let jsonData = try XCTUnwrap(Seeds.nestedObjectJson.data(using: .utf8))
        
        let sut = try? JSONDecoder().decode([String:AnyCodable].self, from: jsonData)
        
        XCTAssertNotNil(sut)
    }
    
    func testEncode_WhenObjectWithNestedObjectValuesDecodedInDictionary_ShouldBeEncoded() throws{
        let jsonData = try XCTUnwrap(Seeds.nestedObjectJson.data(using: .utf8))
        
        let decoded = try? JSONDecoder().decode([String:AnyCodable].self, from: jsonData)
        let sut = try? JSONEncoder().encode(decoded)
        
        XCTAssertNotNil(sut)
    }
    
    func testInitCoder_WhenObjectWithNestedArrayValuesDecodedInDictionary_ShouldBeDecoded() throws{
        let jsonData = try XCTUnwrap(Seeds.nestedArrayJson.data(using: .utf8))
        
        let sut = try? JSONDecoder().decode([String:AnyCodable].self, from: jsonData)
        
        XCTAssertNotNil(sut)
    }
    
    func testEncode_WhenObjectWithNestedArrayValuesDecodedInDictionary_ShouldBeEncoded() throws{
        let jsonData = try XCTUnwrap(Seeds.nestedArrayJson.data(using: .utf8))
        
        let decoded = try? JSONDecoder().decode([String:AnyCodable].self, from: jsonData)
        let sut = try? JSONEncoder().encode(decoded)
        
        XCTAssertNotNil(sut)
    }
    
    func testInitCoder_WhenComplexObjectDecodedInDictionary_ShouldBeDecoded() throws{
        let jsonData = try XCTUnwrap(Seeds.complexObject.data(using: .utf8))
        
        let sut = try? JSONDecoder().decode([String:AnyCodable].self, from: jsonData)
        
        XCTAssertNotNil(sut)
    }
    
    func testEncode_WhenComplexObjectDecodedInDictionary_ShouldBeEncoded() throws{
        let jsonData = try XCTUnwrap(Seeds.complexObject.data(using: .utf8))
        
        let decoded = try? JSONDecoder().decode([String:AnyCodable].self, from: jsonData)
        let sut = try? JSONEncoder().encode(decoded)
        
        XCTAssertNotNil(sut)
    }
    
    // Collection - Array
    
    func testInitCoder_WhenArrayWithRawValuesDecodedInArray_ShouldBeDecoded() throws{
        let jsonData = try XCTUnwrap(Seeds.arrayRawElementsJson.data(using: .utf8))
        
        let sut = try? JSONDecoder().decode([AnyCodable].self, from: jsonData)
        
        XCTAssertNotNil(sut)
    }
    
    func testEncode_WhenArrayWithRawValuesDecodedInArray_ShouldBeEncoded() throws{
        let jsonData = try XCTUnwrap(Seeds.arrayRawElementsJson.data(using: .utf8))
        
        let decoded = try? JSONDecoder().decode([AnyCodable].self, from: jsonData)
        let sut = try? JSONEncoder().encode(decoded)
        
        XCTAssertNotNil(sut)
    }
    
    func testInitCoder_WhenArrayWithNestedObjectValuesDecodedInArray_ShouldBeDecoded() throws{
        let jsonData = try XCTUnwrap(Seeds.arrayNestedObjectJson.data(using: .utf8))
        
        let sut = try? JSONDecoder().decode([AnyCodable].self, from: jsonData)
        
        XCTAssertNotNil(sut)
    }
    
    func testEncode_WhenArrayWithNestedObjectValuesDecodedInArray_ShouldBeEncoded() throws{
        let jsonData = try XCTUnwrap(Seeds.arrayNestedObjectJson.data(using: .utf8))
        
        let decoded = try? JSONDecoder().decode([AnyCodable].self, from: jsonData)
        let sut = try? JSONEncoder().encode(decoded)
        
        XCTAssertNotNil(sut)
    }
    
    func testInitCoder_WhenArrayWithNestedArrayValuesDecodedInArray_ShouldBeDecoded() throws{
        let jsonData = try XCTUnwrap(Seeds.arrayNestedArrayJson.data(using: .utf8))
        
        let sut = try? JSONDecoder().decode([AnyCodable].self, from: jsonData)
        
        XCTAssertNotNil(sut)
    }
    
    func testEncode_WhenArraytWithNestedArrayValuesDecodedInArray_ShouldBeEncoded() throws{
        let jsonData = try XCTUnwrap(Seeds.arrayNestedArrayJson.data(using: .utf8))
        
        let decoded = try? JSONDecoder().decode([AnyCodable].self, from: jsonData)
        let sut = try? JSONEncoder().encode(decoded)
        
        XCTAssertNotNil(sut)
    }
    
    
    // Custom Object
    
    func testInitCoder_WhenCustomObject_ShouldBeDecoded() throws{
        let jsonData = try XCTUnwrap(Seeds.someCodable.data(using: .utf8))
        
        let sut = try? JSONDecoder().decode(SomeCodable.self, from: jsonData)
        
        XCTAssertEqual(1111, sut?.id)
        XCTAssertEqual(12345, sut?.genericObject["int"]?.value as? Int)
        XCTAssertEqual("Foo Bar", sut?.genericObject["string"]?.value as? String)
        XCTAssertEqual(28.787833, sut?.genericObject["double"]?.value as? Double)
        XCTAssertEqual(false, sut?.genericObject["bool"]?.value as? Bool)
    }
    
    func testEncode_WhenCustomObject_ShouldBeEncoded() throws{
        let jsonData = try XCTUnwrap(Seeds.someCodable.data(using: .utf8))
        
        let decoded = try? JSONDecoder().decode(SomeCodable.self, from: jsonData)
        let sut = try? JSONEncoder().encode(decoded)
        
        let unwrappedSut = try XCTUnwrap(sut)
        let redecoded = try? JSONSerialization.jsonObject(with: unwrappedSut, options: [])
        
        let object = try XCTUnwrap(redecoded as? [String:Any])
        let genericObject = try XCTUnwrap(object["genericObject"] as? [String:Any])
        
        XCTAssertEqual(1111, object["id"] as? Int)
        XCTAssertEqual(12345, genericObject["int"] as? Int)
        XCTAssertEqual("Foo Bar", genericObject["string"] as? String)
        XCTAssertEqual(28.787833, genericObject["double"] as? Double)
        XCTAssertEqual(false, genericObject["bool"] as? Bool)
    }
    
    
    // Init Value
    
    func testInitWithValue_WhenPassingADictionaryGettingValues_ShouldRetriveCorrect() throws{
        let dicionary : [String: Any] = ["foo" : "bar"]
        let sut = AnyCodable(value: dicionary)
        let casted = sut.value as? [String: Any]
        
        let unwrappedCasted = try XCTUnwrap(casted)
        let fooValue = unwrappedCasted["foo"]
        
        let unwrappedFooValue = try XCTUnwrap(fooValue)
        let unwrappedFooValueString = try XCTUnwrap(unwrappedFooValue as? String)
        
        XCTAssertEqual(unwrappedFooValueString, "bar")
    }
    
    // null
    
    func testIniCoder_WhenObjectWithNullValue_ShouldBeDecoded() throws{
         let jsonData = try XCTUnwrap(Seeds.jsonWithNullObject.data(using: .utf8))
         
         let sut = try? JSONDecoder().decode(AnyCodable.self, from: jsonData)
         
         XCTAssertNotNil(sut)
     }
}
