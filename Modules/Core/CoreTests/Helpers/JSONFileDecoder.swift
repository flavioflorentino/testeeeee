import XCTest

final class JSONFileDecoder<T: Decodable> {
    private let decoder = JSONDecoder()

    @discardableResult
    func load(
        resource: String,
        typeDecoder: JSONDecoder.KeyDecodingStrategy = .useDefaultKeys
    ) throws -> T {
        decoder.keyDecodingStrategy = typeDecoder
        let bundle = Bundle(for: type(of: self))
        let path = try XCTUnwrap(bundle.path(forResource: resource, ofType: "json"))
        let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
        return try decoder.decode(T.self, from: data)
    }
}
