import XCTest
@testable import Core

//2019/12/30 e 2019/30/12
class DateExtensionTest: XCTestCase {
    func testDate_firstFormatter() {
        let thomas = "2014-07-08T17:11:00.208Z"
        let formatter = Date.firstFormatter
        let date = formatter.date(from: thomas)
        XCTAssertNotNil(date)
    }
    
    func testDate_secondFormatter() {
        let miroslav = "2014/07/08"
        let formatter = Date.secondFormatter
        let date = formatter.date(from: miroslav)
        XCTAssertNotNil(date)
    }
    
    func testDate_thirdFormatter() {
        let toni = "2014-07-08T17:24:00.000Z"
        let formatter = Date.thirdFormatter
        let date = formatter.date(from: toni)
        XCTAssertNotNil(date)
    }
    
    func testDate_forthFormatter() {
        let toni = "08/07/2014"
        let formatter = Date.forthFormatter
        let date = formatter.date(from: toni)
        XCTAssertNotNil(date)
    }
    
    func testDate_fifthFormatter() {
        let sami = "2014-07-08"
        let formatter = Date.fifthFormatter
        let date = formatter.date(from: sami)
        XCTAssertNotNil(date)
    }
    
    func testDecodableDateFormatter() {
        let thomas = "2014-07-08T17:11:00.208Z"
        let miroslav = "2014/07/08"
        let toni = "2014-07-08T17:24:00.000Z"
        let tonii = "08/07/2014"
        let sami = "2014-07-08"
        let andre = "2014-07-08T18:14:00.000Z"
        let andree = "2014-07-08T18:24:00.000Z"
        let oscar = "2014-07-08T18:35:00.000Z"
        let dates = [thomas, miroslav, toni, tonii, sami, andre, andree, oscar]
        let formatter = DecodableDateFormatter()
        let result = dates.compactMap({ formatter.date(from: $0) })
        XCTAssertTrue(result.count == 8)
    }
    
    func testDecodableDateFormatter_fail() {
        let bestYearEver = "2020"
        let formatter = DecodableDateFormatter()
        let result = formatter.date(from: bestYearEver)
        XCTAssertNil(result)
    }
    
    func testDate_ddDeMMMM() {
        let thomas = "2014-07-08T17:11:00.208Z"
        let formatter = Date.firstFormatter
        let date = formatter.date(from: thomas)
        XCTAssertEqual(date?.ddDeMMMM(), "08 de Julho")
    }
}
