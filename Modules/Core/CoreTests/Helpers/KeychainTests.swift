import XCTest
@testable import Core

// MARK: FUNCTIONS AND MOCK ENUMS

private enum KeychainKeyableMock: String, KeychainKeyable {
    case apiAuthIndex = "api_auth_index"
    case apiAuth = "api_auth"
    case userRegister = "user_register"
    case singUpRegister = "singUp_register"
    case registerStatus = "register_status"
}

private enum KeychainKeyMock: KeychainKeyable {
    case cvv(String)
    case userIdLogout
    case tokenLogout
    
    public var rawValue: String {
        switch self {
        case .cvv(let cardId):
            return "cvv_card_\(cardId)"
        case .userIdLogout:
            return "user_id_logout"
        case .tokenLogout:
            return "token_logout"
        }
    }
}

private enum PPAuthTempTokenStatusMock: UInt {
    case PPAuthTempTokenStatusInexistent
    case PPAuthTempTokenStatusLogin
    case PPAuthTempTokenStatusRegister
}

class KeychainTests: XCTestCase {
    typealias Dependencies = HasKeychainManager
    let dependencies: Dependencies = DependencyContainerMock(KeychainManagerMock())
    
    override func setUp() {
        super.setUp()
        dependencies.keychain.clearAll()
    }
    
    private func getTempAuthTokenStatusEnum(string: String) -> PPAuthTempTokenStatusMock {
        switch string {
        case "login":
            return .PPAuthTempTokenStatusLogin
        case "register":
            return .PPAuthTempTokenStatusRegister
        default:
            return .PPAuthTempTokenStatusInexistent
        }
    }
    
    // MARK: TEST FUCNTIONS
    
    func testSetToken() {
        dependencies.keychain.set(key: KeychainKey.token, value: "tokentest")
        XCTAssertEqual(dependencies.keychain.getData(key: KeychainKey.token), "tokentest")
    }
    
    func testNullifyToken() {
        dependencies.keychain.clearValue(key: KeychainKey.token)
        //Setting nil to a dictionary value on keychain is returning NSNull instead of nil
        XCTAssertNil(dependencies.keychain.getData(key: KeychainKey.token))
    }
    
    func testSetUserId() {
        dependencies.keychain.set(key: KeychainKey.authToken, value: "1885")
        XCTAssertEqual(dependencies.keychain.getData(key: KeychainKey.authToken), "1885")
    }
    
    func testNullifyUserId() {
        dependencies.keychain.clearValue(key: KeychainKey.authToken)
        XCTAssertNil(dependencies.keychain.getData(key: KeychainKey.authToken))
    }
    
    func testUserIdLogout() {
        dependencies.keychain.set(key: KeychainKeyMock.userIdLogout, value: "1885")
        XCTAssertEqual(dependencies.keychain.getData(key: KeychainKeyMock.userIdLogout), "1885")
    }
    
    func testNullifyUserIdLogout() {
        dependencies.keychain.clearValue(key: KeychainKeyMock.userIdLogout)
        XCTAssertNil(dependencies.keychain.getData(key: KeychainKeyMock.userIdLogout))
    }
    
    func testSetTokenLogout() {
        dependencies.keychain.set(key: KeychainKeyMock.tokenLogout, value: "1885")
        XCTAssertEqual(dependencies.keychain.getData(key: KeychainKeyMock.tokenLogout), "1885")
    }
    
    func testNullifyTokenLogout() {
        dependencies.keychain.clearValue(key: KeychainKeyMock.tokenLogout)
        XCTAssertNil(dependencies.keychain.getData(key: KeychainKeyMock.tokenLogout))
    }
    
    func testSetTempUserId() {
        dependencies.keychain.set(key: KeychainKey.tempAuthToken, value: "1435")
        XCTAssertEqual(dependencies.keychain.getData(key: KeychainKey.tempAuthToken), "1435")
    }
    
    func testNullifyTempUserId() {
        dependencies.keychain.clearValue(key: KeychainKey.tempAuthToken)
        XCTAssertNil(dependencies.keychain.getData(key: KeychainKey.tempAuthToken))
    }
    
    func testSetTempUserIdStatusInexistent() {
        dependencies.keychain.set(key: KeychainKey.tempAuthTokenStatus, value: "inexistent")
        guard let tempTokenStatus = try? XCTUnwrap(dependencies.keychain.getData(key: KeychainKey.tempAuthTokenStatus)) else {
            XCTFail("I Can`t Unwrap value")
            return
        }
        XCTAssertEqual(getTempAuthTokenStatusEnum(string: tempTokenStatus), .PPAuthTempTokenStatusInexistent)
    }
    
    func testSetTempUserIdStatusLogin() {
        dependencies.keychain.set(key: KeychainKey.tempAuthTokenStatus, value: "login")
        guard let tempTokenStatus = try? XCTUnwrap(dependencies.keychain.getData(key: KeychainKey.tempAuthTokenStatus)) else {
            XCTFail("I Can`t Unwrap value")
            return
        }
        XCTAssertEqual(getTempAuthTokenStatusEnum(string: tempTokenStatus), .PPAuthTempTokenStatusLogin)
    }
    
    func testSetTempUserIdStatusRegister() {
        dependencies.keychain.set(key: KeychainKey.tempAuthTokenStatus, value: "register")
        guard let tempTokenStatus = try? XCTUnwrap(dependencies.keychain.getData(key: KeychainKey.tempAuthTokenStatus)) else {
            XCTFail("I Can`t Unwrap value")
            return
        }
        XCTAssertEqual(getTempAuthTokenStatusEnum(string: tempTokenStatus), .PPAuthTempTokenStatusRegister)
    }
    
    func testNullifyTempUserIdStatus() {
        dependencies.keychain.clearValue(key: KeychainKey.tempAuthTokenStatus)
        XCTAssertNil(dependencies.keychain.getData(key: KeychainKey.tempAuthTokenStatus))
    }
    
    func testSetPPDeviceId() {
        dependencies.keychain.set(key: KeychainKey.ppDeviceId, value: "test-876343-test")
        XCTAssertEqual(dependencies.keychain.getData(key: KeychainKey.ppDeviceId), "test-876343-test")
    }
    
    func testNullifyPPDeviceId() {
        dependencies.keychain.clearValue(key: KeychainKey.ppDeviceId)
        XCTAssertNil(dependencies.keychain.getData(key: KeychainKey.ppDeviceId))
    }
    
    func testClearValue_WhenKeyIsKeychainKeyable_ShouldClearOneValue() {
        dependencies.keychain.set(key: KeychainKeyableMock.registerStatus, value: "test-123456789")
        dependencies.keychain.set(key: KeychainKeyableMock.apiAuth, value: "test-temp-123456789")
        let result = dependencies.keychain.clearValue(key: KeychainKeyableMock.registerStatus)
        XCTAssertTrue(result)
        XCTAssertNil(dependencies.keychain.getData(key: KeychainKeyableMock.registerStatus))
        XCTAssertNotNil(dependencies.keychain.getData(key: KeychainKeyableMock.apiAuth))
    }
    
    func testClearValue_WhenKeyIsKeyChainKey_ShouldClearOneValue() {
        dependencies.keychain.set(key: KeychainKey.token, value: "test-123456789")
        dependencies.keychain.set(key: KeychainKey.tempAuthToken, value: "test-temp-123456789")
        let result = dependencies.keychain.clearValue(key: KeychainKey.token)
        XCTAssertTrue(result)
        XCTAssertNil(dependencies.keychain.getData(key: KeychainKey.token))
        XCTAssertNotNil(dependencies.keychain.getData(key: KeychainKey.tempAuthToken))
    }
    
    func testClearKeychain() {
        dependencies.keychain.clearAll()
        XCTAssertNil(dependencies.keychain.getData(key: KeychainKey.token))
        XCTAssertNil(dependencies.keychain.getData(key: KeychainKey.authToken))
        XCTAssertNil(dependencies.keychain.getData(key: KeychainKey.tempAuthToken))
        XCTAssertNil(dependencies.keychain.getData(key: KeychainKey.tempAuthTokenStatus))
        XCTAssertNil(dependencies.keychain.getData(key: KeychainKey.ppDeviceId))
    }
}
