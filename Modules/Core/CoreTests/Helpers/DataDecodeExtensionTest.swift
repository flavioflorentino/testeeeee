import XCTest
@testable import Core

class DataDecodeTest: XCTestCase {
    private let jsonDataValid = """
    {
    "value": true
    }
    """.data(using: .utf8)!
    
    private let jsonDataInvalid = """
    {"}
    """.data(using: .utf8)!
    
    private struct Object: Decodable {
        let value: Bool
    }
    
    func testDataDecoded_withSuccess() {
        let result: Object? = jsonDataValid.decoded()
        XCTAssertNotNil(result)
        XCTAssertEqual(result?.value, true)
    }
    
    func testDataDecoded_withError() {
        let result: Object? = jsonDataInvalid.decoded()
        XCTAssertNil(result)
    }
}
