import Foundation

final class SessionDelegateDummy: NSObject, URLAuthenticationChallengeSender {
    func use(_ credential: URLCredential, for challenge: URLAuthenticationChallenge) { }
    func continueWithoutCredential(for challenge: URLAuthenticationChallenge) { }
    func cancel(_ challenge: URLAuthenticationChallenge) { }
}

final class URLProtectionSpaceFake: URLProtectionSpace {
    private var internalServerTrust: SecTrust?
    
    init(trust: SecTrust?, hostName: String = "testePinning") {
        internalServerTrust = trust
        super.init(
            host: hostName,
            port: 443,
            protocol: "",
            realm: "",
            authenticationMethod: NSURLAuthenticationMethodServerTrust
        )
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var serverTrust: SecTrust? {
        internalServerTrust
    }
}

final class PinningTrust {
    private let certificateNames: [String]
    
    init(certificateNames: [String] = ["picpay.com", "Amazon", "AmazonRootCA1"]) {
        self.certificateNames = certificateNames
    }
    
    private var certificates: [SecCertificate] {
        let bundle = Bundle(for: type(of: self))
        return certificateNames
            .compactMap { bundle.path(forResource: $0, ofType: "cer") }
            .compactMap { try? Data(contentsOf: URL(fileURLWithPath: $0)) }
            .compactMap { SecCertificateCreateWithData(nil, $0 as CFData) }
    }
    
    func getTrust(hostName: String = "") -> SecTrust? {
        var optionalTrust: SecTrust?
        let status = SecTrustCreateWithCertificates(certificates as CFArray, nil, &optionalTrust)
        
        guard status == errSecSuccess, let trust = optionalTrust else {
            return nil
        }
        
        //date time for 10/02/2020
        let verifyTime: CFAbsoluteTime = 603122637
        let verifyDate: CFDate = CFDateCreate(nil, verifyTime)
        SecTrustSetVerifyDate(trust, verifyDate)
        return trust
    }
}
