import XCTest
import CommonCrypto
@testable import Core

final class PinningConfigurationApiTests: XCTestCase {
    typealias Dependencies = HasKeychainManager
    private lazy var sut: PinningConfigurationApi = {
        PinningConfigurationApi(keychain: dependencyMock.keychain)
    }()
    private let sessionDelegate = SessionDelegateDummy()
    private lazy var dependencyMock: Dependencies = DependencyContainerMock(KeychainManagerMock())
    
    override func setUp() {
        super.setUp()
        dependencyMock.keychain.clearAll()
    }
    
    func testPinningConfigurationApiWhenShouldTrustServerIsTrustedShouldReturnYes() {
        let pinningTrust = PinningTrust()
        let trust = pinningTrust.getTrust(hostName: "gateway.service.ppay.me")
        
        let response = sut.shouldTrustServer(hostDomain: "gateway.service.ppay.me", serverTrust: trust)
        let expected: PinResponse = .yes
        
        XCTAssertEqual(response.rawValue, expected.rawValue)
    }
    
    func testPinningConfigurationApiWhenShouldTrustServerIsNotTrustedShouldReturnNo() {
        let pinningTrust = PinningTrust(certificateNames: ["ProxymanCA"])
        let trust = pinningTrust.getTrust(hostName: "gateway.service.ppay.me")
        
        let response = sut.shouldTrustServer(hostDomain: "gateway.service.ppay.me", serverTrust: trust)
        
        XCTAssertEqual(response, .no)
    }
    
    func testPinningConfigurationApiWhenShouldPinDomainDomainIsInTheListShouldReturnYes() {
        let domains = "gateway.service.ppay.me,picpay.com"
        setKeychainDomains(domains: domains)
        
        let result = self.sut.shouldPinDomain(hostName: "gateway.service.ppay.me")
        let expected: PinResponse = .yes
        XCTAssertEqual(result.rawValue, expected.rawValue)
    }
    
    func testPinningConfigurationApiWhenShouldPinDomainDomainIsNotInTheListShouldReturnNo() {
        let domains = "gateway.service.ppay.me, picpay.com"
        setKeychainDomains(domains: domains)
        
        let result = self.sut.shouldPinDomain(hostName: "teste.pinning.com")
        
        XCTAssertEqual(result, .no)
    }
    
    func testPinningConfigurationApiWhenVerifyTrustedServerIsValidShouldReturnYes() {
        let trustPinning = PinningTrust()
        let hostName = "ws8734652387.picpay.com"
        
        guard let trust = trustPinning.getTrust(hostName: hostName) else {
            XCTFail()
            return
        }
        
        let space = URLProtectionSpaceFake(trust: trust, hostName: hostName)
        let credential = URLCredential(trust: trust)
        let challenge = URLAuthenticationChallenge(
            protectionSpace: space,
            proposedCredential: credential,
            previousFailureCount: 0,
            failureResponse: nil,
            error: nil,
            sender: sessionDelegate
        )
        let isTrusted = sut.verifyTrustedServer(
            authenticationMethod: NSString(string: challenge.protectionSpace.authenticationMethod),
            serverTrust: trust,
            hostDomain: NSString(string: hostName)
        )
        
        let expected = PinResponse.yes
        
        XCTAssertEqual(isTrusted.rawValue, expected.rawValue)
    }
    
    func testPinningConfigurationApiWhenVerifyTrustedServerIsNotValidShouldReturnNo() {
        let trustPinning = PinningTrust(certificateNames: ["ProxymanCA"])
        let hostName = "ws8734652387.picpay.com"
        
        guard let trust = trustPinning.getTrust(hostName: hostName) else {
            XCTFail()
            return
        }
        
        let space = URLProtectionSpaceFake(trust: trust, hostName: hostName)
        let credential = URLCredential(trust: trust)
        let challenge = URLAuthenticationChallenge(
            protectionSpace: space,
            proposedCredential: credential,
            previousFailureCount: 0,
            failureResponse: nil,
            error: nil,
            sender: sessionDelegate
        )
        
        let isTrusted = sut.verifyTrustedServer(
            authenticationMethod: NSString(string: challenge.protectionSpace.authenticationMethod),
            serverTrust: trust,
            hostDomain: NSString(string: hostName)
        )
        
        XCTAssertEqual(isTrusted, PinResponse.no)
    }
    
    func testPinningConfigurationApiWhenGenerateSecCertificatesShouldReturnValidData() {
        func md5(data: NSData) -> String {
            var hash = [UInt8](repeating: 0, count: Int(CC_MD5_DIGEST_LENGTH))
            CC_MD5(data.bytes, CC_LONG(data.length), &hash)
            return hash.map { String(format: "%02hhx", $0) }.joined()
        }
        let hashes = sut.generateSecCertificates()
            .compactMap { SecCertificateCopyData($0) as NSData }
            .map { md5(data: $0) }
        let certificateHashes = Set(hashes)
        let referenceHashes = Set([
            "43c6bfaeecfead2f18c6886830fcc8e6",
            "c8e58dcea842e27ac02a5c7c9e26bf66",
            "a0d4ef0bf7b5d849952aecf5c4fc8187",
            "173574af7b611cebf4f93ce2ee40f9a2",
            "89bc27d5eb178d066a69d5fd8947b4cd"
        ])
        XCTAssertEqual(certificateHashes, referenceHashes)
    }
    
    private func setKeychainDomains(domains: String) {
        dependencyMock.keychain.set(key: KeychainKey.pinDomains, value: domains)
    }
}
