import XCTest
@testable import Core

private final class PinningConfigurationMock: PinningConfigurationApiProtocol {
    private let isServerTrusted: PinResponse
    private let serverIsVerified: PinResponse
    private let isDomainPinned: PinResponse
    
    let shouldPin: Bool
    let domainsTrusted: [String]
    
    init(shouldPin: Bool, isServerTrusted: PinResponse, serverIsVerified: PinResponse, isDomainPinned: PinResponse){
        self.isServerTrusted = isServerTrusted
        self.serverIsVerified = serverIsVerified
        self.isDomainPinned = isDomainPinned
        
        self.shouldPin = shouldPin
        domainsTrusted = []
    }
    
    func verifyTrustedServer(authenticationMethod: NSString, serverTrust: SecTrust?, hostDomain: NSString) -> PinResponse {
        serverIsVerified
    }
    
    func shouldTrustServer(hostDomain: NSString, serverTrust: SecTrust?) -> PinResponse {
        isServerTrusted
    }
    
    func shouldPinDomain(hostName: NSString) -> PinResponse {
        isDomainPinned
    }
}

final class PinningHandlerTests: XCTestCase {
    let trustHelper = PinningTrust()
    
   func testHandleWhenAllValidationsAreTrueShouldUseCredential() {
       let pinningConfig = PinningConfigurationMock(
           shouldPin: true,
           isServerTrusted: .yes,
           serverIsVerified: .yes,
           isDomainPinned: .yes
       )
       let delegate = SessionDelegateDummy()
       let sut = PinningHandler(configuration: pinningConfig)

       guard let trust = trustHelper.getTrust() else {
           XCTFail()
           return
       }

       let space = URLProtectionSpaceFake(trust: trust)
       let challenge = URLAuthenticationChallenge(
           protectionSpace: space,
           proposedCredential: nil,
           previousFailureCount: 0,
           failureResponse: nil,
           error: nil,
           sender: delegate
       )

       let completionHandler: ChallengeHandler = { disposition, credential in
           XCTAssertNotNil(credential)
           XCTAssert(URLSession.AuthChallengeDisposition.useCredential == disposition)
       }

       sut.handle(challenge: challenge, completionHandler: completionHandler)

   }

   func testHandleWhenServerIsNotTrustedShouldCancel() {
       let pinningConfig = PinningConfigurationMock(
           shouldPin: true,
           isServerTrusted: .no,
           serverIsVerified: .yes,
           isDomainPinned: .yes
       )
       let delegate = SessionDelegateDummy()
       let sut = PinningHandler(configuration: pinningConfig)

       guard let trust = trustHelper.getTrust() else {
           XCTFail()
           return
       }

       let space = URLProtectionSpaceFake(trust: trust)
       let challenge = URLAuthenticationChallenge(
           protectionSpace: space,
           proposedCredential: nil,
           previousFailureCount: 0,
           failureResponse: nil,
           error: nil,
           sender: delegate
       )

       let completionHandler: ChallengeHandler = { disposition, credential in
           XCTAssertNil(credential, "[testHandleWhenServerIsNotTrustedShouldCancel] credential should be nil")
           XCTAssert(
               URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge == disposition,
               "[testHandleWhenServerIsNotTrustedShouldCancel] disposition should be cancel"
           )
       }

       sut.handle(challenge: challenge, completionHandler: completionHandler)
   }

   func testHandleWhenIsNotPinningDomainShouldUseDefaultApproach() {
       let pinningConfig = PinningConfigurationMock(
           shouldPin: true,
           isServerTrusted: .yes,
           serverIsVerified: .yes,
           isDomainPinned: .no
       )
       let delegate = SessionDelegateDummy()
       let sut = PinningHandler(configuration: pinningConfig)
       guard let trust = trustHelper.getTrust() else {
           XCTFail()
           return
       }

       let space = URLProtectionSpaceFake(trust: trust)
       let challenge = URLAuthenticationChallenge(
           protectionSpace: space,
           proposedCredential: nil,
           previousFailureCount: 0,
           failureResponse: nil,
           error: nil,
           sender: delegate
       )

       let completionHandler: ChallengeHandler = { disposition, credential in
           XCTAssertNil(credential)
           XCTAssert(URLSession.AuthChallengeDisposition.performDefaultHandling == disposition)
       }

       sut.handle(challenge: challenge, completionHandler: completionHandler)
   }

   func testHandleWhenNotPinningShouldUseDefaultApproach() {
       let pinningConfig = PinningConfigurationMock(
           shouldPin: false,
           isServerTrusted: .yes,
           serverIsVerified: .yes,
           isDomainPinned: .yes
       )
       let delegate = SessionDelegateDummy()
       let sut = PinningHandler(configuration: pinningConfig)
       guard let trust = trustHelper.getTrust() else {
           XCTFail()
           return
       }

       let space = URLProtectionSpaceFake(trust: trust)
       let challenge = URLAuthenticationChallenge(
           protectionSpace: space,
           proposedCredential: nil,
           previousFailureCount: 0,
           failureResponse: nil,
           error: nil,
           sender: delegate
       )

       let completionHandler: ChallengeHandler = { disposition, credential in
           XCTAssertNil(credential)
           XCTAssert(URLSession.AuthChallengeDisposition.performDefaultHandling == disposition)
       }

       sut.handle(challenge: challenge, completionHandler: completionHandler)
   }
    
    func testHandleWhenServerTrustIsNilShouldCancel() {
        let pinningConfig = PinningConfigurationMock(
            shouldPin: true,
            isServerTrusted: .yes,
            serverIsVerified: .yes,
            isDomainPinned: .yes
        )
        let delegate = SessionDelegateDummy()
        let sut = PinningHandler(configuration: pinningConfig)
        
        let space = URLProtectionSpaceFake(trust: nil)
        let challenge = URLAuthenticationChallenge(
            protectionSpace: space,
            proposedCredential: nil,
            previousFailureCount: 0,
            failureResponse: nil,
            error: nil,
            sender: delegate
        )
        
        let completionHandler: ChallengeHandler = { disposition, credential in
            XCTAssertNil(credential)
            XCTAssert(URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge == disposition)
        }
        
        sut.handle(challenge: challenge, completionHandler: completionHandler)
    }
}
