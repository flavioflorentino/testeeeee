import XCTest
@testable import Core

typealias ChallengeHandler = (URLSession.AuthChallengeDisposition, URLCredential?) -> Void

private final class PinningHandlerSpy: PinningHandlerProvider {
    private (set) var challenge: URLAuthenticationChallenge?
    
    func handle(challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        self.challenge = challenge
        completionHandler(.cancelAuthenticationChallenge, nil)
    }
}

final class URLSessionPinningTests: XCTestCase {
    private let handlerSpy = PinningHandlerSpy()
    private lazy var sut = URLSessionPinning(pinningHandler: handlerSpy)
    private let sessionDelegate = SessionDelegateDummy()
    
    func testUrlSessionWhenReceiveChallengeShouldHaveTheSameChallengeAsPinningHandlerProvider() {
        let session = URLSession()
        let space = URLProtectionSpace(host: "mock", port: 443, protocol: "", realm: "", authenticationMethod: NSURLAuthenticationMethodHTTPBasic)
        let challenge = URLAuthenticationChallenge(protectionSpace: space, proposedCredential: nil, previousFailureCount: 0, failureResponse: nil, error: nil, sender: sessionDelegate)
        
        let expectation = self.expectation(description: "calledHandler")
        let completionHandler: ChallengeHandler = { _,_ in
            expectation.fulfill()
        }

        sut.urlSession(session, didReceive: challenge, completionHandler: completionHandler)
        XCTAssertTrue(handlerSpy.challenge === challenge)
        wait(for: [expectation], timeout: 1)
    }
}
