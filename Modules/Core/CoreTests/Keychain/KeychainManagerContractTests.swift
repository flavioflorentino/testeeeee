import Foundation
import XCTest
@testable import Core

private final class KeychainManagerContractTests: XCTestCase {
    typealias Dependencies = HasKeychainManager
    private let dependencies: Dependencies = DependencyContainerMock(KeychainManagerMock())
    
    private enum KeychainKeyableMock: String, KeychainKeyable {
        case userRegister = "user_register"
    }
    
    override func setUp() {
        super.setUp()
        dependencies.keychain.clearAll()
    }
    
    func testSaveValue_WhenReceiveActionWithKeychainKey_ShouldSaveValueInDictOnKeyChain() {
        dependencies.keychain.set(key: KeychainKey.token, value: "tokenTeste")
        XCTAssertEqual(getValue(key: KeychainKey.token), "tokenTeste")
    }
    
    func testSaveValue_WhenReceiveActionWithKeychainKeyable_ShouldSaveValueInDictOnKeyChain() {
        dependencies.keychain.set(key: KeychainKeyableMock.userRegister, value: "userRegister")
        XCTAssertEqual(getValue(key: KeychainKeyableMock.userRegister), "userRegister")
    }
    
    func testGetValueString_WhenReceiveAction_ShouldReturnStringValue() {
        addValue(value: nil)
        let value = dependencies.keychain.getData(key: KeychainKey.token)
        XCTAssertEqual(value, "tokenTeste")
    }
    
    func testGetValueInt_WhenReceiveAction_ShouldReturnIntValue() {
        addValue(value: "123")
        let value = dependencies.keychain.getInt(key: KeychainKey.token)
        XCTAssertEqual(value, 123)
    }
    
    func testGetValueCodable_WhenNotExistValue_ShouldReturnNil() {
        dependencies.keychain.clearAll()
        let value: ModelMock? = dependencies.keychain.getModelData(key: KeychainKeyableMock.userRegister)
        XCTAssertNil(value)
    }
    
    func testGetValueCodable_WhenReceiveAction_ShouldReturnCodableValue() {
        let model = ModelMock(name: "nome teste", password: "senha mega forte")
        dependencies.keychain.set(key: KeychainKeyableMock.userRegister, value: model)
        let value: ModelMock? = dependencies.keychain.getModelData(key: KeychainKeyableMock.userRegister)
        XCTAssertEqual(value?.name, "nome teste")
        XCTAssertEqual(value?.password, "senha mega forte")
    }
    
    func testGetValueCodable_WhenModelIsInvalid_ShouldReturnNil() {
        let model = ModelMock(name: "nome teste", password: "senha mega forte")
        dependencies.keychain.set(key: KeychainKeyableMock.userRegister, value: model)
        let value: ModelInvalidMock? = dependencies.keychain.getModelData(key: KeychainKeyableMock.userRegister)
        XCTAssertNil(value)
    }
    
    func testClearValue_WhenReceiveClearValueKeychainKeyable_ShouldReturnTrueValue() {
        let result = dependencies.keychain.clearValue(key: KeychainKeyableMock.userRegister)
        let value = dependencies.keychain.getData(key: KeychainKeyableMock.userRegister)
        XCTAssertTrue(result)
        XCTAssertNil(value)
    }

    func testClearValue_WhenReceiveClearValueKeychainKey_ShouldReturnTrueValue() {
        let result = dependencies.keychain.clearValue(key: KeychainKey.token)
        let value = dependencies.keychain.getData(key: KeychainKey.token)
        XCTAssertTrue(result)
        XCTAssertNil(value)
    }

    func testClearValue_WhenReceiveClearValueKeychainKey_ShouldReturnFalseValue() {
        let result = dependencies.keychain.clearValue(key: KeychainKey.token)
        XCTAssertTrue(result)
    }

    func testClearValue_WhenReceiveClearValueKeychainKeyable_ShouldReturnFalseValue() {
        let result = dependencies.keychain.clearValue(key: KeychainKeyableMock.userRegister)
        XCTAssertTrue(result)
    }
    
    func testClearAllValues_WhenReceiveAction_ShouldReturnTrueValue() {
        dependencies.keychain.clearAll()
        let value = getValue(key: KeychainKey.token)
        XCTAssertEqual(value, "")
    }
}

private extension KeychainManagerContractTests {
    func addValue(value: String? = nil) {
        dependencies.keychain.set(key: KeychainKey.token, value: value ?? "tokenTeste")
    }
    
    func getValue(key: KeychainKeyable) -> String {
        guard let value = dependencies.keychain.getData(key: key) else {
            return ""
        }
        
        return value
    }
}

private struct ModelMock: Codable {
    let name: String
    let password: String
}

private struct ModelInvalidMock: Codable {
    let invalidName: String
    let invalidMail: String
}
