@testable import Core
import XCTest

private struct DecodeArrays: Decodable {
    public let json: [String: Any]

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        json = try container.decode([String: Any].self, forKey: .data)
    }

    private enum CodingKeys: String, CodingKey {
        case data
    }
}

final class DecodeArraysTests: XCTestCase {
    private let decoder = JSONFileDecoder<DecodeArrays>()
    
    // MARK: - Decode
    func testDecode_WhenDecodingJsonWithArrayInsideArrays_ShouldParseTheDataAndNoThrowAnError() throws {
        XCTAssertNoThrow(try decoder.load(resource: "receiptResponseMock", typeDecoder: .convertFromSnakeCase))
    }
}
