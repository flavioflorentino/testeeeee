@testable import Core
import XCTest

final class DateFormatterExtensionTests: XCTestCase {
    // MARK: - NotLenientFormatter

    func testNotLenientFormatter_whenStringIsNotADate_DateObjectShouldBeNil() {
        let string = "test"

        XCTAssertNil(DateFormatter(style: .notLenient).date(from: string))
    }

    func testNotLenientFormatter_whenStringIsMissingYear_DateObjectShouldBeNil() {
        let strings = ["", "0", "01", "01/", "01/0", "01/01", "01/01/"]

        strings.forEach {
            XCTAssertNil(DateFormatter(style: .notLenient).date(from: $0))
        }
    }

    func testNotLenientFormatter_whenStringIsNotMissingYear_DateObjectShouldNotBeNil() {
        let strings = ["01/01/2", "01/01/20", "01/01/200", "01/01/2000"]

        strings.forEach {
            XCTAssertNotNil(DateFormatter(style: .notLenient).date(from: $0))
        }
    }

    func testNotLenientFormatter_whenStringFormatIsIncorrect_DateObjectShouldBeNil() {
        let strings = ["2000-12-21", "5 de Abril", "12/21/2000", "2014-11-15T18:32:17+00:00"]

        strings.forEach {
            XCTAssertNil(DateFormatter(style: .notLenient).date(from: $0))
        }
    }

    func testNotLenientFormatter_whenStringFormatIsCloseToCorrect_DateObjectShouldNotBeNil() {
        let strings = ["21-12-2000", "21/12-2000", "21-DEC-2000", "21/December/2000"]

        strings.forEach {
            XCTAssertNotNil(DateFormatter(style: .notLenient).date(from: $0))
        }
    }

    func testNotLenientFormatter_whenStringIsNotAValidDate_DateObjectShouldBeNil() {
        let strings = ["32/01/1783", "30/02/2020", "29/02/2001", "10/13/1999"]

        strings.forEach {
            XCTAssertNil(DateFormatter(style: .notLenient).date(from: $0))
        }
    }

    func testNotLenientFormatter_whenStringIsAValidDate_DateObjectShouldNotBeNil() {
        let strings = ["01/12/1949", "02/12/1949", "01/12/1952", "01/12/1953", "11/10/1998", "04/09/1752"]

        strings.forEach {
            XCTAssertNotNil(DateFormatter(style: .notLenient).date(from: $0))
        }
    }
}
