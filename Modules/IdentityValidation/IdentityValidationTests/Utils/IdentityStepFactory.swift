@testable import IdentityValidation

struct IdentityStepFactory {
    var label = ""
    var detail = ""
    var status = IDValidationStatus.notStarted
    var type = IdentityStepType.initial
    var icons = StepIcons(darkMode: "", lightMode: "")
    var waitTime: Int = 0

    func make() -> IdentityStep {
        return IdentityStep(
            label: label,
            detail: detail,
            status: status,
            type: type,
            icons: icons,
            waitTime: waitTime
        )
    }
}
