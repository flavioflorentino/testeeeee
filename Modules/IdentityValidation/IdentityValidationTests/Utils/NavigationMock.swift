import UIKit

final class NavigationMock: UINavigationController {
    private(set) var isPushViewControllerCalled = false
    private(set) var isPopViewControllerCalled = false
    private(set)var isPresentViewControllerCalled = false
    private(set) var isDismissViewControllerCalled = false

    private(set) var popedViewController: UIViewController?
    private(set) var pushedViewController: UIViewController?
    private(set) var viewControllerPresented: UIViewController?

    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        pushedViewController = viewController
        isPushViewControllerCalled = true
        super.pushViewController(viewController, animated: false)
    }

    @discardableResult
    override func popViewController(animated: Bool) -> UIViewController? {
        isPopViewControllerCalled = true
        popedViewController = super.popViewController(animated: false)
        return popedViewController
    }

    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        viewControllerPresented = viewControllerToPresent
        isPresentViewControllerCalled = true
        super.present(viewControllerToPresent, animated: false)
        completion?()
    }

    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        isDismissViewControllerCalled = true
        super.dismiss(animated: false)
        completion?()
    }
}
