@testable import IdentityValidation

final class IdentityValidationCoordinatingSpy: IdentityValidationCoordinating {
    private(set) var callPresentNextStepCount = 0
    private(set) var callEndWithStatusCount = 0
    private(set) var callDismissCount = 0

    func presentNextStep(_ step: IdentityStep) {
        callPresentNextStepCount += 1
    }

    func endWithStatus(_ status: IDValidationStatus?) {
        callEndWithStatusCount += 1
    }

    func dismiss() {
        callDismissCount += 1
    }
}
