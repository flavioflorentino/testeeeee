import AnalyticsModule
import Core
import XCTest
@testable import IdentityValidation

private class StatusServicingMock: StatusServicing {
    private(set) var callRequestCount = 0
    private(set) var callWaitForTimeCount = 0
    private(set) var timeReceived = 0
    
    var stepExpectedResult: Result<IdentityStep, ApiError>?

    func step(token: String, flow: String, _ completion: @escaping ((Result<IdentityStep, ApiError>) -> Void)) {
        callRequestCount += 1
        guard let expectedResult = stepExpectedResult else {
            return
        }
        completion(expectedResult)
    }
    
    func waitForTime(_ time: Int, completion: @escaping () -> Void) {
        callWaitForTimeCount += 1
        timeReceived = time
        completion()
    }
}

private class StatusPresentingSpy: StatusPresenting {
    var viewController: StatusDisplay?
    private(set) var callSetupTextsCount = 0
    private(set) var callPresentLoadingButtonCount = 0
    private(set) var isButtonLoading = false
    private(set) var callSetupIconCount = 0
    private(set) var callDismissWithCurrentStepCount = 0
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    private(set) var callDismissWithNextStepCount = 0
    private(set) var callPresentErrorCount = 0
    private(set) var callPresentNextStepCount = 0

    func setupTexts() {
        callSetupTextsCount += 1
    }

    func presentLoadingButton(_ isLoading: Bool) {
        callPresentLoadingButtonCount += 1
        isButtonLoading = isLoading
    }
    
    func setupIcon(isDarkMode: Bool) {
        callSetupIconCount += 1
    }

    func dismissWithCurrentStep() {
        callDismissWithCurrentStepCount += 1
    }

    func dismissWithNextStep(_ nextStep: IdentityStep) {
        callDismissWithNextStepCount += 1
    }

    func startLoading() {
        callStartLoadingCount += 1
    }

    func stopLoading() {
        callStopLoadingCount += 1
    }

    func presentError(message: String?) {
        callPresentErrorCount += 1
    }
    
    func presentNextStep(_ step: IdentityStep) {
        callPresentNextStepCount += 1
    }
}

final class StatusViewModelTests: XCTestCase {
    private let serviceMock = StatusServicingMock()
    private let presenterSpy = StatusPresentingSpy()
    
    private let analytics = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(analytics)

    private lazy var sut = createSut()

    func testLoadInfo_WhenWaitTimeIsZero_ShouldCallSetupTextsWithoutLoading() {
        sut.loadInfo()

        XCTAssertEqual(presenterSpy.callSetupTextsCount, 1)
        XCTAssertEqual(presenterSpy.callPresentLoadingButtonCount, 0)
    }
    
    func testLoadInfo_WhenWaitTimeIsBiggerThanZeroAndTypeIsOnHold_ShouldCallSetupTextAndLoadingButton() {
        sut = createSut(type: .onHold, waitTime: 10)
        sut.loadInfo()

        XCTAssertEqual(presenterSpy.callSetupTextsCount, 1)
        XCTAssertEqual(presenterSpy.callPresentLoadingButtonCount, 2)
        XCTAssertFalse(presenterSpy.isButtonLoading)
    }
    
    func testLoadInfo_WhenWaitTimeIsBiggerThanZeroAndTypeIsOnHold_ShouldWaitForTimeAndRemoveLoadingFromButton() {
        sut = createSut(type: .onHold, waitTime: 10)
        sut.loadInfo()

        XCTAssertEqual(serviceMock.callWaitForTimeCount, 1)
        XCTAssertEqual(serviceMock.timeReceived, 10)
        XCTAssertEqual(presenterSpy.callPresentLoadingButtonCount, 2)
        XCTAssertFalse(presenterSpy.isButtonLoading)
    }
    
    func testLoadInfo_WhenStepIsOnHold_ShouldSendOnHoldStatusEvent() {
        sut = createSut(type: .onHold)
        sut.loadInfo()

        let properties: [IdentityValidationEventProperty] = [
            .init(name: .screenName, value: IdentityValidationStatusEventProperty.onHold),
            .init(name: .flow, value: "TFA")
        ]
        
        let expectedEvent = IdentityValidationTracker(
            eventType: IdentityValidationEventType.screenViewed,
            eventProperties: properties
        ).event()
        
        XCTAssertEqual(presenterSpy.callSetupTextsCount, 1)
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testLoadInfo_WhenStepIsProcessing_ShouldSendProcessingStatusEvent() {
        sut = createSut(type: .processing)
        sut.loadInfo()

        let properties: [IdentityValidationEventProperty] = [
            .init(name: .screenName, value: IdentityValidationStatusEventProperty.processing),
            .init(name: .flow, value: "TFA")
        ]
        
        let expectedEvent = IdentityValidationTracker(
            eventType: IdentityValidationEventType.screenViewed,
            eventProperties: properties
        ).event()
        
        XCTAssertEqual(presenterSpy.callSetupTextsCount, 1)
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testLoadInfo_WhenStepIsDone_ShouldSendDoneStatusEvent() {
        sut = createSut(type: .done)
        sut.loadInfo()

        let properties: [IdentityValidationEventProperty] = [
            .init(name: .screenName, value: IdentityValidationStatusEventProperty.done),
            .init(name: .flow, value: "TFA")
        ]
        
        let expectedEvent = IdentityValidationTracker(
            eventType: IdentityValidationEventType.screenViewed,
            eventProperties: properties
        ).event()
        
        XCTAssertEqual(presenterSpy.callSetupTextsCount, 1)
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }

    func testSetupIcon_ShouldCallSetupIcon() {
        sut.setupIcon(isDarkMode: true)

        XCTAssertEqual(presenterSpy.callSetupIconCount, 1)
    }

    func testHandleButtonTap_WhenStepTypeIsNotOnHold_ShouldCallDismissWithCurrentStep() {
        sut.handleButtonTap()

        XCTAssertEqual(presenterSpy.callDismissWithCurrentStepCount, 1)
    }

    func testHandleButtonTap_WhenStepTypeIsOnHold_ShouldStartAndStopLoading() {
        sut = createSut(type: .onHold)
        serviceMock.stepExpectedResult = .failure(.serverError)
        sut.handleButtonTap()

        XCTAssertEqual(presenterSpy.callStartLoadingCount, 1)
        XCTAssertEqual(presenterSpy.callStopLoadingCount, 1)
    }

    func testHandleButtonTap_WhenStepTypeIsOnHold_WhenResultIsSuccessAndStepIsOfEndFlowType_ShouldCallDismissWithNextStep() {
        sut = createSut(type: .onHold)
        let step = IdentityStepFactory(type: .alternative).make()
        serviceMock.stepExpectedResult = .success(step)
        sut.handleButtonTap()

        XCTAssertEqual(presenterSpy.callDismissWithNextStepCount, 1)
    }

    func testHandleButtonTap_WhenStepTypeIsOnHold_WhenResultIsFailure_ShouldPresentError() {
        sut = createSut(type: .onHold)
        serviceMock.stepExpectedResult = .failure(.serverError)
        sut.handleButtonTap()

        XCTAssertEqual(presenterSpy.callPresentErrorCount, 1)
    }

    func testRetryNextStep_ShouldCallServiceStepRequest() {
        sut.retryNextStep()

        XCTAssertEqual(serviceMock.callRequestCount, 1)
    }
    
    func testHandleButtonTap_WhenStepTypeIsOnHold_WhenResultIsSuccessAndStepIsNotOfEndFlowType_ShouldCallPresentNextStep() {
        sut = createSut(type: .onHold)
        let step = IdentityStepFactory(type: .documentFront).make()
        serviceMock.stepExpectedResult = .success(step)
        sut.handleButtonTap()

        XCTAssertEqual(presenterSpy.callPresentNextStepCount, 1)
    }
}

private extension StatusViewModelTests {
    func createSut(type: IdentityStepType = .processing, waitTime: Int = 0) -> StatusViewModel {
        let step = IdentityStepFactory(
            label: "LABEL TEXT",
            detail: "DETAIL TEXT",
            type: type,
            icons: StepIcons(darkMode: "https://url.dark.icon",
                             lightMode: "https://url.light.icon"),
            waitTime: waitTime
        ).make()
        
        return StatusViewModel(service: serviceMock,
                               presenter: presenterSpy,
                               serviceToken: "TOLKIEN 🧙‍♂️",
                               flow: "TFA",
                               step: step,
                               dependencies: dependencies
        )
    }
}
