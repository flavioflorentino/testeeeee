import XCTest
@testable import IdentityValidation
import UI

private class StatusDisplaySpy: StatusDisplay {
    var loadingView = LoadingView()
    private(set) var callDisplayIconCount = 0
    private(set) var callDisplayTextsCount = 0
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    private(set) var callShowErrorCount = 0
    private(set) var callRetryNextStepCount = 0

    private(set) var callDisplayLoadingButtonCount = 0
    private(set) var isButtonLoading = false
    private(set) var buttonLoadingText = ""
    
    private(set) var iconUrl: URL?
    private(set) var title: String?
    private(set) var descriptionText: String?
    private(set) var buttonTitle: String?

    func displayIcon(iconUrl: URL?) {
        callDisplayIconCount += 1
        self.iconUrl = iconUrl
    }

    func displayTexts(title: String, description: String, buttonTitle: String) {
        callDisplayTextsCount += 1
        self.title = title
        self.descriptionText = description
        self.buttonTitle = buttonTitle
    }

    func showError(_ controller: PopupViewController) {
        callShowErrorCount += 1
    }

    func retryNextStep() {
        callRetryNextStepCount += 1
    }

    func startLoadingView() {
        callStartLoadingCount += 1
    }

    func stopLoadingView() {
        callStopLoadingCount += 1
    }
    
    func displayLoadingButton(_ isLoading: Bool, text: String) {
        callDisplayLoadingButtonCount += 1
        isButtonLoading = isLoading
        buttonLoadingText = text
    }
}

private class StatusCoordinatingSpy: StatusCoordinating {
    private(set) var callDismissActionCount = 0
    private(set) var callNextStepActionCount = 0
    private(set) var status: IDValidationStatus?
    private(set) var step: IdentityStep?

    func perform(action: StatusAction) {
        switch action {
        case .dismiss(let status):
            callDismissActionCount += 1
            self.status = status
        case .nextStep(let step):
            callNextStepActionCount += 1
            self.step = step
        }
    }
}

final class StatusPresenterTests: XCTestCase {
    private let controllerSpy = StatusDisplaySpy()
    private let coordinatorSpy = StatusCoordinatingSpy()

    private lazy var sut = createSut()

    func testSetupTexts_ShouldDisplayTitle() {
        sut.setupTexts()

        XCTAssertEqual(controllerSpy.callDisplayTextsCount, 1)
        XCTAssertEqual(controllerSpy.title, "LABEL TEXT")
    }

    func testSetupTexts_ShouldDisplayDescription() {
        sut.setupTexts()

        XCTAssertEqual(controllerSpy.callDisplayTextsCount, 1)
        XCTAssertEqual(controllerSpy.descriptionText, "DETAIL TEXT")
    }

    func testSetupTexts_WhenStepTypeIsNotOnHold_ShouldDisplayButtonTitle() {
        sut.setupTexts()

        XCTAssertEqual(controllerSpy.callDisplayTextsCount, 1)
        XCTAssertEqual(controllerSpy.buttonTitle, Strings.Status.Button.ok)
    }

    func testSetupTexts_WhenStepTypeIsOnHold_ShouldDisplayButtonTitle() {
        sut = createSut(type: .onHold)
        sut.setupTexts()

        XCTAssertEqual(controllerSpy.callDisplayTextsCount, 1)
        XCTAssertEqual(controllerSpy.buttonTitle, Strings.Status.Button.continue)
    }

    func testSetupIntroIcon_WhenIsDarkMode_ShouldDisplayIcon() {
        sut.setupIcon(isDarkMode: true)

        XCTAssertEqual(controllerSpy.callDisplayIconCount, 1)
        XCTAssertEqual(controllerSpy.iconUrl, URL(string: "https://url.dark.icon"))
    }

    func testSetupIntroIcon_WhenIsNotDarkMode_ShouldDisplayIcon() {
        sut.setupIcon(isDarkMode: false)

        XCTAssertEqual(controllerSpy.callDisplayIconCount, 1)
        XCTAssertEqual(controllerSpy.iconUrl, URL(string: "https://url.light.icon"))
    }

    func testDismissWithCurrentStep_ShouldPerformDismissActionWithCurrentStepStatus() {
        sut.dismissWithCurrentStep()

        XCTAssertEqual(coordinatorSpy.callDismissActionCount, 1)
        XCTAssertEqual(coordinatorSpy.status, .notStarted)
    }

    func testDismissWithNextStep_ShouldPerformDismissActionWithNewStepStatus() {
        let step = IdentityStepFactory(status: .rejected).make()
        sut.dismissWithNextStep(step)

        XCTAssertEqual(coordinatorSpy.callDismissActionCount, 1)
        XCTAssertEqual(coordinatorSpy.status, .rejected)
    }

    func testStartLoading_ShouldCallStartLoadingView() {
        sut.startLoading()

        XCTAssertEqual(controllerSpy.callStartLoadingCount, 1)
    }

    func testStopLoading_ShouldCallStopLoadingView() {
        sut.stopLoading()

        XCTAssertEqual(controllerSpy.callStopLoadingCount, 1)
    }

    func testPresentError_ShouldCallShowError() {
        sut.presentError(message: nil)

        XCTAssertEqual(controllerSpy.callShowErrorCount, 1)
    }
    
    func testPresentNextStep_ShouldPerformNextStepActionWithNewStep() {
        let step = IdentityStepFactory(type: .documentFront).make()
        sut.presentNextStep(step)
        
        XCTAssertEqual(coordinatorSpy.callNextStepActionCount, 1)
        XCTAssertEqual(coordinatorSpy.step?.type, .documentFront)
    }
    
    func testPresentLoadingButton_WhenLoadingIsTrue_ShouldDisplayLoadingOnButton() {
        sut.presentLoadingButton(true)
        
        XCTAssertEqual(controllerSpy.callDisplayLoadingButtonCount, 1)
        XCTAssertTrue(controllerSpy.isButtonLoading)
        XCTAssertEqual(controllerSpy.buttonLoadingText, Strings.Status.Button.verifying)
    }
    
    func testPresentLoadingButton_WhenLoadingIsFalse_ShouldRemoveLoadingFromButton() {
        sut.presentLoadingButton(false)
        
        XCTAssertEqual(controllerSpy.callDisplayLoadingButtonCount, 1)
        XCTAssertFalse(controllerSpy.isButtonLoading)
        XCTAssertEqual(controllerSpy.buttonLoadingText, Strings.Status.Button.continue)
    }
}

private extension StatusPresenterTests {
    func createSut(type: IdentityStepType = .processing) -> StatusPresenter {
        let step = IdentityStepFactory(
            label: "LABEL TEXT",
            detail: "DETAIL TEXT",
            type: type,
            icons: StepIcons(darkMode: "https://url.dark.icon",
                             lightMode: "https://url.light.icon")
        ).make()
        let presenter = StatusPresenter(coordinator: coordinatorSpy, step: step)
        presenter.viewController = controllerSpy
        return presenter
    }
}
