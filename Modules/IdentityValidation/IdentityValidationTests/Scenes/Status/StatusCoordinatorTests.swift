import XCTest
@testable import IdentityValidation

final class StatusCoordinatorTests: XCTestCase {
    private let delegateSpy = IdentityValidationCoordinatingSpy()

    private lazy var sut = StatusCoordinator(coordinatorDelegate: delegateSpy)

    func testPerform_WhenActionIsDismiss_ShouldCallEndWithStatus() {
        sut.perform(action: .dismiss(nil))

        XCTAssertEqual(delegateSpy.callEndWithStatusCount, 1)
    }
    
    func testPerform_WhenActionIsNextStep_ShouldCall() {
        sut.perform(action: .nextStep(IdentityStepFactory(status: .approved).make()))
        
        XCTAssertEqual(delegateSpy.callPresentNextStepCount, 1)
    }
}
