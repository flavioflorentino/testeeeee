@testable import IdentityValidation
import XCTest

final class DocumentCaptureIntroCoordinatorTests: XCTestCase {
    private let delegateSpy = DocumentCaptureFlowCoordinatingSpy()

    private lazy var sut = DocumentCaptureIntroCoordinator(delegate: delegateSpy)

    func testPerform_WhenActionIsInstructions_CallDelegateNavigateToInstructions() {
        sut.perform(action: .instructions)

        XCTAssertEqual(delegateSpy.navigateToInstructionsCallCount, 1)
    }

    func testPerform_WhenActionIsClose_CallDelegateClose() {
        sut.perform(action: .close)

        XCTAssertEqual(delegateSpy.closeCallCount, 1)
    }
}
