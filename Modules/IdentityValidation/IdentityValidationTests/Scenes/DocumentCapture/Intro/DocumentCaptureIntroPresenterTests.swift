@testable import IdentityValidation
import XCTest

private class DocumentCaptureIntroCoordinatingSpy: DocumentCaptureIntroCoordinating {
    private(set) var instructionsActionCallCount = 0
    private(set) var closeActionCallCount = 0

    func perform(action: DocumentCaptureIntroAction) {
        switch action {
        case .instructions:
            instructionsActionCallCount += 1
        default:
            closeActionCallCount += 1
        }
    }
}

private class DocumentCaptureIntroDisplayingSpy: DocumentCaptureIntroDisplaying {
    private(set) var displayIconCallCount = 0
    private(set) var displayTextsCallCount = 0

    private(set) var iconUrl: URL?

    func displayIcon(iconUrl: URL?, fallbackIcon: UIImage) {
        displayIconCallCount += 1
        self.iconUrl = iconUrl
    }

    func displayTexts(title: String, description: String, buttonTitle: String) {
        displayTextsCallCount += 1
    }
}

final class DocumentCaptureIntroPresenterTests: XCTestCase {
    private let coordinatorSpy = DocumentCaptureIntroCoordinatingSpy()
    private let viewControllerSpy = DocumentCaptureIntroDisplayingSpy()

    private lazy var sut: DocumentCaptureIntroPresenter = {
        let step = IdentityStepFactory(
            label: "LABEL TEXT",
            detail: "DETAIL TEXT",
            icons: StepIcons(darkMode: "https://url.dark.icon",
                             lightMode: "https://url.light.icon")
        ).make()
        let presenter = DocumentCaptureIntroPresenter(coordinator: coordinatorSpy, step: step)
        presenter.viewController = viewControllerSpy
        return presenter
    }()

    func testSetupIntroIcon_WhenDarkModeIsTrue_CallDisplayIcon() {
        sut.setupIntroIcon(isDarkMode: true)

        XCTAssertEqual(viewControllerSpy.displayIconCallCount, 1)
        XCTAssertEqual(viewControllerSpy.iconUrl, URL(string: "https://url.dark.icon"))
    }

    func testSetupIntroIcon_WhenDarkModeIsFalse_CallDisplayIcon() {
        sut.setupIntroIcon(isDarkMode: false)

        XCTAssertEqual(viewControllerSpy.displayIconCallCount, 1)
        XCTAssertEqual(viewControllerSpy.iconUrl, URL(string: "https://url.light.icon"))
    }

    func testSetupTexts_CallDisplayTexts() {
        sut.setupTexts()

        XCTAssertEqual(viewControllerSpy.displayTextsCallCount, 1)
    }

    func testNextScene_CallCoordinatorInstructionsAction() {
        sut.nextScene()

        XCTAssertEqual(coordinatorSpy.instructionsActionCallCount, 1)
    }

    func testLater_CallCoordinatorCloseAction() {
        sut.later()

        XCTAssertEqual(coordinatorSpy.closeActionCallCount, 1)
    }
}
