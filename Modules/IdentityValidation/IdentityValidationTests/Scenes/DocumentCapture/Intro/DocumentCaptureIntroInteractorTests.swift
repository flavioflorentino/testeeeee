import AnalyticsModule
@testable import IdentityValidation
import XCTest

private class DocumentCaptureIntroPresentingSpy: DocumentCaptureIntroPresenting {
    var viewController: DocumentCaptureIntroDisplaying?
    private(set) var setupIntroIconCallCount = 0
    private(set) var setupTextsCallCount = 0
    private(set) var nextSceneCallCount = 0
    private(set) var laterCallCount = 0

    func setupIntroIcon(isDarkMode: Bool) {
        setupIntroIconCallCount += 1
    }

    func setupTexts() {
        setupTextsCallCount += 1
    }

    func nextScene() {
        nextSceneCallCount += 1
    }

    func later() {
        laterCallCount += 1
    }
}

final class DocumentCaptureIntroInteractorTests: XCTestCase {
    private let analytics = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(analytics)
    private let presenterSpy = DocumentCaptureIntroPresentingSpy()

    private lazy var sut = DocumentCaptureIntroInteractor(presenter: presenterSpy, dependencies: dependencies, flow: "TFA")

    func testSetupTexts_CallPresenterSetupTexts() {
        sut.setupTexts()

        XCTAssertEqual(presenterSpy.setupTextsCallCount, 1)
    }

    func testSetupTexts_ShouldTrackAppearEvent() {
        sut.setupTexts()

        let properties: [IdentityValidationEventProperty] = [
            .init(name: .screenName, value: IdentityValidationDocumentEventProperty.intro),
            .init(name: .flow, value: "TFA")
        ]
        
        let expectedEvent = IdentityValidationTracker(
            eventType: IdentityValidationEventType.screenViewed,
            eventProperties: properties
        ).event()
        
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }

    func testSetupIcon_CallPresenterSetupIcon() {
        sut.setupIcon(isDarkMode: false)

        XCTAssertEqual(presenterSpy.setupIntroIconCallCount, 1)
    }

    func testNextScene_CallPresenterNextScene() {
        sut.nextScene()

        XCTAssertEqual(presenterSpy.nextSceneCallCount, 1)
    }

    func testLater_CallPresenterLater() {
        sut.later()

        XCTAssertEqual(presenterSpy.laterCallCount, 1)
    }
}
