@testable import IdentityValidation

final class DocumentCaptureFlowCoordinatingSpy: DocumentCaptureFlowCoordinating {
    private(set) var navigateToInstructionsCallCount = 0
    private(set) var showCameraCallCount = 0
    private(set) var closeCallCount = 0

    func navigateToInstructions() {
        navigateToInstructionsCallCount += 1
    }

    func showCamera() {
        showCameraCallCount += 1
    }

    func close() {
        closeCallCount += 1
    }
}
