@testable import IdentityValidation
import XCTest

final class DocumentCaptureFlowCoordinatorTests: XCTestCase {
    private let navController = NavigationMock(rootViewController: UIViewController())
    private let step = IdentityStepFactory(type: .documentFront).make()
    private let delegateSpy = IdentityValidationCoordinatingSpy()

    private lazy var sut = DocumentCaptureFlowCoordinator(
        with: navController,
        serviceToken: "Tolkien 🧙‍♂️",
        flow: "PASSWORD_RESET",
        step: step,
        coordinatorDelegate: delegateSpy
    )

    func testStart_ShouldPushIntroController() throws {
        sut.start()

        XCTAssertTrue(navController.isPushViewControllerCalled)
        let pushedController = try XCTUnwrap(navController.pushedViewController)
        XCTAssertTrue(pushedController is DocumentCaptureIntroViewController)
    }

    func testNavigateToInstructions_ShouldPushInstructionsController() throws {
        sut.navigateToInstructions()

        XCTAssertTrue(navController.isPushViewControllerCalled)
        let pushedController = try XCTUnwrap(navController.pushedViewController)
        XCTAssertTrue(pushedController is DocumentCaptureInstructionsViewController)
    }

    func testClose_ShouldPushCaptureController() throws {
        sut.showCamera()

        let presentedNavigation = try XCTUnwrap(navController.viewControllerPresented as? UINavigationController)
        let pushedController = try XCTUnwrap(presentedNavigation.viewControllers.first)
        XCTAssertTrue(pushedController is CaptureViewController)
    }

    func testClose_ShouldCallDelegateDismiss() {
        sut.close()

        XCTAssertEqual(delegateSpy.callDismissCount, 1)
    }

    func testCloseCamera_ShouldDismissNavigationController() {
        sut.closeCamera()

        XCTAssertTrue(navController.isDismissViewControllerCalled)
    }

    func testFinished_WhenNextStepIsDocumentBack_ShouldPresentCaptureController() throws {
        sut.finished(with: IdentityStepFactory(type: .documentBack).make())

        let presentedNavigation = try XCTUnwrap(navController.viewControllerPresented as? UINavigationController)
        let pushedController = try XCTUnwrap(presentedNavigation.viewControllers.first)
        XCTAssertTrue(pushedController is CaptureViewController)
    }

    func testFinished_WhenNextStepIsOnHold_ShouldCallDelegatePresenterNextStep() throws {
        sut.finished(with: IdentityStepFactory(type: .onHold).make())

        XCTAssertEqual(delegateSpy.callPresentNextStepCount, 1)
    }
}
