@testable import IdentityValidation
import XCTest

final class DocumentCaptureInstructionsCoordinatorTests: XCTestCase {
    private let delegateSpy = DocumentCaptureFlowCoordinatingSpy()

    private lazy var sut = DocumentCaptureInstructionsCoordinator(delegate: delegateSpy)

    func testPerform_WhenActionIsInstructions_ShouldCallDelegateNavigateToInstructions() {
        sut.perform(action: .nextScene)

        XCTAssertEqual(delegateSpy.showCameraCallCount, 1)
    }

    func testPerform_WhenActionIsClose_ShouldCallDelegateClose() {
        sut.perform(action: .close)

        XCTAssertEqual(delegateSpy.closeCallCount, 1)
    }
}
