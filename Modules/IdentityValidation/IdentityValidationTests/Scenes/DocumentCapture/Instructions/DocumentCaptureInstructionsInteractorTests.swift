import AnalyticsModule
@testable import IdentityValidation
import XCTest

private class DocumentCaptureInstructionsPresentingSpy: DocumentCaptureInstructionsPresenting {
    var viewController: DocumentCaptureInstructionsDisplaying?
    private(set) var setupInstructionsCallCount = 0
    private(set) var nextSceneCallCount = 0
    private(set) var laterCallCount = 0

    func setupInstructions() {
        setupInstructionsCallCount += 1
    }

    func nextScene() {
        nextSceneCallCount += 1
    }

    func later() {
        laterCallCount += 1
    }
}

final class DocumentCaptureInstructionsInteractorTests: XCTestCase {
    private let analytics = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(analytics)
    private let presenterSpy = DocumentCaptureInstructionsPresentingSpy()

    private lazy var sut = DocumentCaptureInstructionsInteractor(presenter: presenterSpy, dependencies: dependencies, flow: "TFA")

    func testSetupInstructions_ShouldCallPresenterSetupInstructions() {
        sut.setupInstructions()

        XCTAssertEqual(presenterSpy.setupInstructionsCallCount, 1)
    }

    func testSetupInstructions_ShouldTrackAppearEvent() {
        sut.setupInstructions()

        let properties: [IdentityValidationEventProperty] = [
            .init(name: .screenName, value: IdentityValidationDocumentEventProperty.tips),
            .init(name: .flow, value: "TFA")
        ]
        
        let expectedEvent = IdentityValidationTracker(
            eventType: IdentityValidationEventType.screenViewed,
            eventProperties: properties
        ).event()
        
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }

    func testNextScene_ShouldCallPresenterNextScene() {
        sut.nextScene()

        XCTAssertEqual(presenterSpy.nextSceneCallCount, 1)
    }

    func testLater_ShouldCallPresenterLater() {
        sut.later()

        XCTAssertEqual(presenterSpy.laterCallCount, 1)
    }
}
