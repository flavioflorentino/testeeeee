import AssetsKit
@testable import IdentityValidation
import XCTest

private class DocumentCaptureInstructionsCoordinatingSpy: DocumentCaptureInstructionsCoordinating {
    private(set) var action:  DocumentCaptureInstructionsAction?
    private(set) var performActionCallCount = 0

    func perform(action: DocumentCaptureInstructionsAction) {
        self.action = action
        performActionCallCount += 1
    }
}

private class DocumentCaptureInstructionsDisplayingSpy: DocumentCaptureInstructionsDisplaying {
    private(set) var displayTextsCallCount = 0
    private(set) var setInstructionRowsCallCount = 0
    private(set) var rows: [(UIImage, String)]?

    func displayTexts(title: String, mainButtonTitle: String, secondaryButtonTitle: String) {
        displayTextsCallCount += 1
    }

    func setInstructionRows(_ rows: [(UIImage, String)]) {
        self.rows = rows
        setInstructionRowsCallCount += 1
    }
}

final class DocumentCaptureInstructionsPresenterTests: XCTestCase {
    private let coordinatorSpy = DocumentCaptureInstructionsCoordinatingSpy()
    private let controllerSpy = DocumentCaptureInstructionsDisplayingSpy()

    private lazy var sut: DocumentCaptureInstructionsPresenter = {
        let presenter = DocumentCaptureInstructionsPresenter(coordinator: coordinatorSpy)
        presenter.viewController = controllerSpy
        return presenter
    }()

    func testSetupInstructions_ShouldCallControllerDisplayTexts() {
        sut.setupInstructions()

        XCTAssertEqual(controllerSpy.displayTextsCallCount, 1)
        XCTAssertEqual(controllerSpy.rows?[0].0, Resources.Icons.icoDocument.image)
        XCTAssertEqual(controllerSpy.rows?[0].1, Strings.DocumentCapture.Instructions.first)
        XCTAssertEqual(controllerSpy.rows?[1].0, Resources.Icons.icoPhoto.image)
        XCTAssertEqual(controllerSpy.rows?[1].1, Strings.DocumentCapture.Instructions.second)
        XCTAssertEqual(controllerSpy.rows?[2].0, Resources.Icons.icoSearch.image)
        XCTAssertEqual(controllerSpy.rows?[2].1, Strings.DocumentCapture.Instructions.third)
    }

    func testSetupInstructions_ShouldCallControllerSetInstructionRows() {
        sut.setupInstructions()

        XCTAssertEqual(controllerSpy.setInstructionRowsCallCount, 1)
    }

    func testNextScene_ShouldCallCoordinatorPerformNextSceneAction() {
        sut.nextScene()

        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .nextScene)
    }

    func testLater_ShouldCallCoordinatorPerformCloseAction() {
        sut.later()

        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .close)
    }
}
