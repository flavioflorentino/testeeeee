import XCTest
@testable import IdentityValidation
import UI

private final class LoaderDisplaySpy: LoaderDisplay {
    private(set) var callShowPopupControllerCount = 0

    func showPopupController(with properties: PopupProperties) {
        callShowPopupControllerCount += 1
    }
}

private final class LoaderCoordinatingSpy: LoaderCoordinating {
    private(set) var callNextStepActionCount = 0
    private(set) var callCloseActionCount = 0

    func perform(action: LoaderAction) {
        switch action {
        case .nextStep:
            callNextStepActionCount += 1
        case .close:
            callCloseActionCount += 1
        }
    }
}

final class LoaderPresenterTests: XCTestCase {
    private let coordinatorSpy = LoaderCoordinatingSpy()
    private let controllerSpy = LoaderDisplaySpy()

    private lazy var sut: LoaderPresenter = {
        let presenter = LoaderPresenter(coordinator: coordinatorSpy)
        presenter.viewController = controllerSpy
        return presenter
    }()

    func testHandleNextStep_ShouldCallCoordinatorNextStep() {
        let step = IdentityStepFactory().make()
        sut.handleNextStep(step)

        XCTAssertEqual(coordinatorSpy.callNextStepActionCount, 1)
    }

    func testPresentError_ShouldCallControllerShowError() {
        sut.presentError(message: nil)

        XCTAssertEqual(controllerSpy.callShowPopupControllerCount, 1)
    }

    func testClose_ShouldPerformCoordinatorCloseAction() {
        sut.close()

        XCTAssertEqual(coordinatorSpy.callCloseActionCount, 1)
    }
}
