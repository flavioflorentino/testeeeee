import Core
import XCTest
@testable import IdentityValidation

private final class LoaderServicingMock: LoaderServicing {
    var isSuccess = false

    func step(token: String, flow: String, _ completion: @escaping ((Result<IdentityStep, ApiError>) -> Void)) {
        if isSuccess {
            completion(.success(IdentityStepFactory().make()))
        } else {
            completion(.failure(.serverError))
        }
    }
}

private final class LoaderPresentingSpy: LoaderPresenting {
    var viewController: LoaderDisplay?
    private(set) var callHandleNextStepCount = 0
    private(set) var callPresentErrorCount = 0
    private(set) var callCloseCount = 0

    func handleNextStep(_ step: IdentityStep) {
        callHandleNextStepCount += 1
    }

    func presentError(message: String?) {
        callPresentErrorCount += 1
    }

    func close() {
        callCloseCount += 1
    }
}

final class LoaderViewModelTests: XCTestCase {
    private let serviceMock = LoaderServicingMock()
    private let presenterSpy = LoaderPresentingSpy()
    
    private lazy var sut = LoaderViewModel(
        serviceToken: "I_IS_TOKEN",
        flow: "TFA",
        service: serviceMock,
        presenter: presenterSpy
    )

    func testLoadStep_WhenResultIsSuccess_ShouldCallPresenterHandleNextStep() {
        serviceMock.isSuccess = true
        sut.loadStep()

        XCTAssertEqual(presenterSpy.callHandleNextStepCount, 1)
    }

    func testLoadStep_WhenResultIsFailure_ShouldCallPresenterPresentError() {
        sut.loadStep()

        XCTAssertEqual(presenterSpy.callPresentErrorCount, 1)
    }

    func testClose_ShouldCallPresenterClose() {
        sut.close()

        XCTAssertEqual(presenterSpy.callCloseCount, 1)
    }
}
