import XCTest
@testable import IdentityValidation

final class LoaderCoordinatorTests: XCTestCase {
    private let delegateSpy = IdentityValidationCoordinatingSpy()

    private lazy var sut = LoaderCoordinator(coordinatorDelegate: delegateSpy)

    func testPerform_WhenActionIsNextStep_ShouldCallPresentLoadedStep() {
        let step = IdentityStepFactory().make()
        sut.perform(action: .nextStep(step))

        XCTAssertEqual(delegateSpy.callPresentNextStepCount, 1)
    }

    func testPerform_WhenActionIsClose_ShouldCallDismiss() {
        sut.perform(action: .close)

        XCTAssertEqual(delegateSpy.callDismissCount, 1)
    }
}
