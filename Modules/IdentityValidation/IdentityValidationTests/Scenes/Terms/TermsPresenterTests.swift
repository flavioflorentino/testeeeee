import XCTest
@testable import IdentityValidation
import UI

private final class TermsDisplaySpy: TermsDisplay {
    var loadingView: LoadingView = LoadingView()

    private(set) var callStartLoadingViewCount = 0
    private(set) var callDisplayWebPageCount = 0
    private(set) var callStopLoadingViewCount = 0
    private(set) var callDisplayTitleCount = 0

    func startLoadingView() {
        callStartLoadingViewCount += 1
    }

    func displayWebPage(_ request: URLRequest) {
        callDisplayWebPageCount += 1
    }

    func stopLoadingView() {
        callStopLoadingViewCount += 1
    }

    func displayTitle(_ title: String?) {
        callDisplayTitleCount += 1
    }
}

final class TermsPresenterTests: XCTestCase {
    private let viewControllerSpy = TermsDisplaySpy()

    private lazy var sut: TermsPresenter = {
        let presenter = TermsPresenter()
        presenter.viewController = viewControllerSpy
        return presenter
    }()

    func testLoadWebPage_ShouldStartLoadingView() throws {
        let url = try XCTUnwrap(URL(string: "https://www.picpay.com"))
        sut.loadWebPage(for: URLRequest(url: url))

        XCTAssertEqual(viewControllerSpy.callStartLoadingViewCount, 1)
    }

    func testLoadWebPage_ShouldDisplayWebPage() throws {
        let url = try XCTUnwrap(URL(string: "https://www.picpay.com"))
        sut.loadWebPage(for: URLRequest(url: url))

        XCTAssertEqual(viewControllerSpy.callDisplayWebPageCount, 1)
    }

    func testWebPageFinishedLoading_ShouldStopLoadingView() {
        sut.webPageFinishedLoading()

        XCTAssertEqual(viewControllerSpy.callStopLoadingViewCount, 1)
    }

    func testDisplayPageTitle_ShouldDisplayTitle() {
        sut.displayPageTitle(nil)

        XCTAssertEqual(viewControllerSpy.callDisplayTitleCount, 1)
    }
}
