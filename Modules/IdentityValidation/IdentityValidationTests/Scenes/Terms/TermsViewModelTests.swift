import XCTest
@testable import IdentityValidation
import WebKit

private final class TermsPresentingSpy: TermsPresenting {
    var viewController: TermsDisplay?
    private(set) var callLoadWebPageCount = 0
    private(set) var callWebPageFinishedLoadingCount = 0
    @objc private(set) var callDisplayPageTitleCount = 0

    private(set) var request: URLRequest?

    func loadWebPage(for request: URLRequest) {
        callLoadWebPageCount += 1
        self.request = request
    }

    func webPageFinishedLoading() {
        callWebPageFinishedLoadingCount += 1
    }

    func displayPageTitle(_ title: String?) {
        callDisplayPageTitleCount += 1
    }
}

final class TermsViewModelTests: XCTestCase {
    private let presenterSpy = TermsPresentingSpy()

    private lazy var sut = TermsViewModel(presenter: presenterSpy)

    func testLoadWebPage_ShouldCallLoadWebPage() {
        sut.loadWebPage()

        XCTAssertEqual(presenterSpy.callLoadWebPageCount, 1)
    }

    func testLoadWebPage_ShouldCreateURLResquest() {
        sut.loadWebPage()

        let expectedUrlString = "https://picpay.com/app_webviews/terms/"
        XCTAssertEqual(presenterSpy.request?.url?.absoluteString, expectedUrlString)
    }

    func testWebViewDidFinishLoading_ShouldCallWebPageFinishedLoading() {
        sut.webViewDidFinishLoading(WKWebView())

        XCTAssertEqual(presenterSpy.callWebPageFinishedLoadingCount, 1)
    }
}
