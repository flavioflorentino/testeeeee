@testable import IdentityValidation
import XCTest

final class FacialBiometricFlowCoordinatorTests: XCTestCase {
    private let navController = NavigationMock(rootViewController: UIViewController())
    private let step = IdentityStepFactory(type: .selfie).make()
    private let delegateSpy = IdentityValidationCoordinatingSpy()

    private lazy var sut = FacialBiometricFlowCoordinator(
        with: navController,
        serviceToken: "TOKEN",
        flow: "TFA",
        step: step,
        coordinatorDelegate: delegateSpy
    )
}

// MARK: - CoordinatorProtocol
extension FacialBiometricFlowCoordinatorTests {
    func testStart_ShouldPushIntroViewController() throws {
        sut.start()

        let controller = try XCTUnwrap(navController.pushedViewController)
        XCTAssertTrue(controller is FacialBiometricIntroViewController)
    }
}

// MARK: - FacialBiometricFlowCoordinating
extension FacialBiometricFlowCoordinatorTests {
    func testNavigateToInstructionsScene_ShouldPushInstructionsController() throws {
        sut.navigateToInstructionsScene()

        let controller = try XCTUnwrap(navController.pushedViewController)
        XCTAssertTrue(controller is FacialBiometricInstructionsViewController)
    }

    func testNavigateToTerms_ShouldPushTermsController() throws {
        sut.navigateToTerms()

        let controller = try XCTUnwrap(navController.pushedViewController)
        XCTAssertTrue(controller is TermsViewController)
    }

    func testShowCamera_() throws {
        sut.showCamera()

        let presentedNavigation = try XCTUnwrap(navController.viewControllerPresented as? UINavigationController)
        let pushedController = try XCTUnwrap(presentedNavigation.viewControllers.first)
        XCTAssertTrue(pushedController is CaptureViewController)
    }

    func testCloseCamera_ShouldDismissNavigationController() {
        sut.closeCamera()

        XCTAssertTrue(navController.isDismissViewControllerCalled)
    }

    func testFinished_ShouldCallDelegatePresentNextStep() {
        sut.finished(with: IdentityStepFactory().make())

        XCTAssertTrue(navController.isDismissViewControllerCalled)
        XCTAssertEqual(delegateSpy.callPresentNextStepCount, 1)
    }

    func testClose_ShouldCallDelegateDismiss() {
        sut.close()

        XCTAssertEqual(delegateSpy.callDismissCount, 1)
    }
}
