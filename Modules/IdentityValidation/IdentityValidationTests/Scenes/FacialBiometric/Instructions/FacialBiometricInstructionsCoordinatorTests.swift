@testable import IdentityValidation
import XCTest

final class FacialBiometricInstructionsCoordinatorTests: XCTestCase {
    private let delegateSpy = FacialBiometricFlowCoordinatingSpy()

    private lazy var sut = FacialBiometricInstructionsCoordinator(delegate: delegateSpy)

    func testPerform_WhenActionIsNextScene_ShouldCallShowCamera() {
        sut.perform(action: .nextScene)

        XCTAssertEqual(delegateSpy.callShowCameraCount , 1)
    }

    func testPerform_WhenActionIsClose_ShouldCallClose() {
        sut.perform(action: .close)

        XCTAssertEqual(delegateSpy.callCloseCount , 1)
    }
}
