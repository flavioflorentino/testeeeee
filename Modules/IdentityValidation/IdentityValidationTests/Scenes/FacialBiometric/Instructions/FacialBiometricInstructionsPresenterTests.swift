import AssetsKit
@testable import IdentityValidation
import XCTest

private final class FacialBiometricInstructionsDisplaySpy: FacialBiometricInstructionsDisplay {
    private(set) var callAddInstructionRowCount = 0
    private(set) var callDisplayTextsCount = 0
    private(set) var rows: [(icon: UIImage, text: String)]?

    func setInstructionRows(_ rows: [(UIImage, String)]) {
        callAddInstructionRowCount += 1
        self.rows = rows
    }

    func displayTexts(title: String, mainButtonTitle: String, secondaryButtonTitle: String) {
        callDisplayTextsCount += 1
    }
}

private final class FacialBiometricInstructionsCoordinatingSpy: FacialBiometricInstructionsCoordinating {
    private(set) var nextSceneActionCount = 0
    private(set) var closeActionCount = 0

    func perform(action: FacialBiometricInstructionsAction) {
        switch action {
        case .nextScene:
            nextSceneActionCount += 1
        case .close:
            closeActionCount += 1
        }
    }
}

final class FacialBiometricInstructionsPresenterTests: XCTestCase {
    private let controllerSpy = FacialBiometricInstructionsDisplaySpy()
    private let coordinatorSpy = FacialBiometricInstructionsCoordinatingSpy()

    private lazy var sut: FacialBiometricInstructionsPresenter = {
        let presenter = FacialBiometricInstructionsPresenter(coordinator: coordinatorSpy)
        presenter.display = controllerSpy
        return presenter
    }()

    func testSetupInstructions_ShouldAddTexts() {
        sut.setupInstructions()

        XCTAssertEqual(controllerSpy.callDisplayTextsCount, 1)
    }

    func testSetupInstructions_ShouldAddCorrectAssets() {
        sut.setupInstructions()

        XCTAssertEqual(controllerSpy.callAddInstructionRowCount, 1)
        XCTAssertEqual(controllerSpy.rows?[0].0, Resources.Icons.icoLamp.image)
        XCTAssertEqual(controllerSpy.rows?[0].1, Strings.FacialBiometric.Instructions.first)
        XCTAssertEqual(controllerSpy.rows?[1].0, Resources.Icons.icoCapGlass.image)
        XCTAssertEqual(controllerSpy.rows?[1].1, Strings.FacialBiometric.Instructions.second)
        XCTAssertEqual(controllerSpy.rows?[2].0, Resources.Icons.icoFaceSmile.image)
        XCTAssertEqual(controllerSpy.rows?[2].1, Strings.FacialBiometric.Instructions.third)
    }

    func testNextScene_ShouldPerformNextSceneAction() {
        sut.nextScene()

        XCTAssertEqual(coordinatorSpy.nextSceneActionCount, 1)
    }

    func testLater_ShouldPerformCloseAction() {
        sut.later()

        XCTAssertEqual(coordinatorSpy.closeActionCount, 1)
    }
}
