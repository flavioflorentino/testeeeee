import AnalyticsModule
@testable import IdentityValidation
import XCTest

private final class FacialBiometricInstructionsPresentingSpy: FacialBiometricInstructionsPresenting {
    var display: FacialBiometricInstructionsDisplay?
    private(set) var callNextSceneCount = 0
    private(set) var callLaterCount = 0
    private(set) var callSetupInstructionsCount = 0

    func nextScene() {
        callNextSceneCount += 1
    }

    func later() {
        callLaterCount += 1
    }

    func setupInstructions() {
        callSetupInstructionsCount += 1
    }
}

final class FacialBiometricInstructionsViewModelTests: XCTestCase {
    private let presenterSpy = FacialBiometricInstructionsPresentingSpy()
    private let analytics = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(analytics)

    private lazy var sut = FacialBiometricInstructionsViewModel(presenter: presenterSpy, dependencies: dependencies, flow: "TFA")

    func testSetupInstructions_ShouldTrackAppearEvent() {
        sut.setupInstructions()

        let properties: [IdentityValidationEventProperty] = [
            .init(name: .screenName, value: IdentityValidationSelfieEventProperty.tips),
            .init(name: .flow, value: "TFA")
        ]
        
        let expectedEvent = IdentityValidationTracker(
            eventType: IdentityValidationEventType.screenViewed,
            eventProperties: properties
        ).event()
        
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }

    func testSetupInstructions_ShouldCallSetupInstructions() {
        sut.setupInstructions()

        XCTAssertEqual(presenterSpy.callSetupInstructionsCount, 1)
    }

    func testNextScene_ShouldCallNextScene() {
        sut.nextScene()

        XCTAssertEqual(presenterSpy.callNextSceneCount, 1)
    }

    func testLater_ShouldCallLater() {
        sut.later()

        XCTAssertEqual(presenterSpy.callLaterCount, 1)
    }
}
