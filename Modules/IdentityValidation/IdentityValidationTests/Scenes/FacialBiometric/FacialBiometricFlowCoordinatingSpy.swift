@testable import IdentityValidation

final class FacialBiometricFlowCoordinatingSpy: FacialBiometricFlowCoordinating {
    private(set) var callNavigateToInstructionsSceneCount = 0
    private(set) var callNavigateToTermsCount = 0
    private(set) var callShowCameraCount = 0
    private(set) var callCloseCameraCount = 0
    private(set) var callFinishedCount = 0
    private(set) var callCloseCount = 0

    func navigateToInstructionsScene() {
        callNavigateToInstructionsSceneCount += 1
    }

    func navigateToTerms() {
        callNavigateToTermsCount += 1
    }

    func showCamera() {
        callShowCameraCount += 1
    }

    func closeCamera() {
        callCloseCameraCount += 1
    }

    func finished(with nextStep: IdentityStep) {
        callFinishedCount += 1
    }

    func close() {
        callCloseCount += 1
    }
}
