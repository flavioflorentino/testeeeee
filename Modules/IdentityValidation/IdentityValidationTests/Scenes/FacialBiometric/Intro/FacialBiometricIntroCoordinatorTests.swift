@testable import IdentityValidation
import XCTest

final class FacialBiometricIntroCoordinatorTests: XCTestCase {
    private let delegateSpy = FacialBiometricFlowCoordinatingSpy()

    private lazy var sut = FacialBiometricIntroCoordinator(delegate: delegateSpy)

    func testPerform_WhenActionIsInstructions_ShouldCallNavigateToInstructions() {
        sut.perform(action: .instructions)

        XCTAssertEqual(delegateSpy.callNavigateToInstructionsSceneCount, 1)
    }

    func testPerform_WhenActionIsReadTerms_ShouldCallNavigateToTerms() {
        sut.perform(action: .readTerms)

        XCTAssertEqual(delegateSpy.callNavigateToTermsCount, 1)
    }

    func testPerform_WhenActionIsClose_ShouldCallClose() {
        sut.perform(action: .close)

        XCTAssertEqual(delegateSpy.callCloseCount, 1)
    }
}
