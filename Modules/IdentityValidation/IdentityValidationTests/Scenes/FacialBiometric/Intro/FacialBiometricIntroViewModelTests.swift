import AnalyticsModule
@testable import IdentityValidation
import XCTest

private final class FacialBiometricIntroPresenterSpy: FacialBiometricIntroPresenting {
    var viewController: FacialBiometricIntroDisplay?
    private(set) var didCallSetupIntroIcon = 0
    private(set) var didCallSetupTexts = 0
    private(set) var didCallSetupAttributedTerms = 0
    private(set) var didCallLater = 0
    private(set) var didCallNextScene = 0
    private(set) var didCallReadTerms = 0

    private(set) var isDarkMode: Bool?

    func setupIntroIcon(isDarkMode: Bool) {
        didCallSetupIntroIcon += 1
        self.isDarkMode = isDarkMode
    }

    func setupTexts() {
        didCallSetupTexts += 1
    }
    
    func setupAttributedTerms() {
        didCallSetupAttributedTerms += 1
    }
    
    func nextScene() {
        didCallNextScene += 1
    }
    
    func later() {
        didCallLater += 1
    }
    
    func readTerms() {
        didCallReadTerms += 1
    }
}

final class FacialBiometricIntroViewModelTest: XCTestCase {
    private let presenterSpy = FacialBiometricIntroPresenterSpy()
    private let analytics = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(analytics)

    private lazy var sut: FacialBiometricIntroViewModel = {
        let viewModel = FacialBiometricIntroViewModel(presenter: presenterSpy, dependencies: dependencies, flow: "TFA")
        return viewModel
    }()

    func testLoadData_ShouldCallSetupTexts() {
        sut.loadData()
        
        XCTAssertEqual(presenterSpy.didCallSetupTexts, 1)
    }
    
    func testLoadData_ShouldCallSetupAttributedTerms() {
        sut.loadData()
        
        XCTAssertEqual(presenterSpy.didCallSetupAttributedTerms, 1)
    }

    func testLoadData_ShouldTrackAppearEvent() {
        sut.loadData()

        let properties: [IdentityValidationEventProperty] = [
            .init(name: .screenName, value: IdentityValidationSelfieEventProperty.intro),
            .init(name: .flow, value: "TFA")
        ]
        
        let expectedEvent = IdentityValidationTracker(
            eventType: IdentityValidationEventType.screenViewed,
            eventProperties: properties
        ).event()
        
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }

    func testSetupIcon_WhenDarkModeIsTrue_ShouldCallSetupIntroIcon() {
        sut.setupIcon(isDarkMode: true)

        XCTAssertEqual(presenterSpy.didCallSetupIntroIcon, 1)
        XCTAssertEqual(presenterSpy.isDarkMode, true)
    }

    func testSetupIcon_WhenDarkModeIsFalse_ShouldCallSetupIntroIcon() {
        sut.setupIcon(isDarkMode: false)

        XCTAssertEqual(presenterSpy.didCallSetupIntroIcon, 1)
        XCTAssertEqual(presenterSpy.isDarkMode, false)
    }
    
    func testNextScene_ShouldCallNextScene() {
        sut.nextScene()
        
        XCTAssertEqual(presenterSpy.didCallNextScene, 1)
    }
    
    func testLater_ShouldCallLater() {
        sut.later()
        
        XCTAssertEqual(presenterSpy.didCallLater, 1)
    }

    func testReadTerms_ShouldCallReadTerms() {
        sut.readTerms()

        XCTAssertEqual(presenterSpy.didCallReadTerms, 1)
    }
    
    func testReadTerms_ShouldLogTermsEvent() {
        sut.readTerms()

        let properties: [IdentityValidationEventProperty] = [
            .init(name: .screenName, value: IdentityValidationSelfieEventProperty.intro),
            .init(name: .flow, value: "TFA"),
            .init(name: .buttonName, value: IdentityValidationButtonNameProperty.termsOfUse)
        ]
        
        let expectedEvent = IdentityValidationTracker(
            eventType: IdentityValidationEventType.buttonClicked,
            eventProperties: properties
        ).event()
        
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
}
