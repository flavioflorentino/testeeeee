import AssetsKit
@testable import IdentityValidation
import XCTest

private final class FacialBiometricIntroCoordinatorSpy: FacialBiometricIntroCoordinating {
    var viewController: UIViewController?
    private(set) var didCallInstructionsAction = 0
    private(set) var didCallReadTermsAction = 0
    private(set) var didCallCloseAction = 0

    func perform(action: FacialBiometricIntroAction) {
        switch action {
        case .instructions:
            didCallInstructionsAction += 1
        case .readTerms:
            didCallReadTermsAction += 1
        case .close:
            didCallCloseAction += 1
        }
    }
}

private final class FacialBiometricIntroViewControllerSpy: FacialBiometricIntroDisplay {
    private(set) var didCallDisplayIcon = 0
    private(set) var didCallDisplayTexts = 0
    private(set) var didCallDisplayTerms = 0

    private(set) var iconUrl: URL?
    private(set) var fallbackIcon: UIImage?
    private(set) var title: String?
    private(set) var description: String?
    private(set) var terms: String?

    func displayIcon(iconUrl: URL?, fallbackIcon: UIImage) {
        didCallDisplayIcon += 1
        self.iconUrl = iconUrl
        self.fallbackIcon = fallbackIcon
    }

    func displayTexts(title: String, description: String, buttonTitle: String) {
        didCallDisplayTexts += 1
        self.title = title
        self.description = description
    }

    func displayTerms(terms: NSAttributedString) {
        didCallDisplayTerms += 1
        self.terms = terms.string
    }
}

final class FacialBiometricIntroPresenterTests: XCTestCase {
    private let coordinatorSpy = FacialBiometricIntroCoordinatorSpy()
    private let viewControllerSpy = FacialBiometricIntroViewControllerSpy()

    private lazy var sut: FacialBiometricIntroPresenter = {
        let step = IdentityStepFactory(
            label: "LABEL TEXT",
            detail: "DETAIL TEXT",
            icons: StepIcons(darkMode: "https://url.dark.icon",
                             lightMode: "https://url.light.icon")
        ).make()
        let presenter = FacialBiometricIntroPresenter(coordinator: coordinatorSpy, step: step)
        presenter.viewController = viewControllerSpy

        return presenter
    }()

    func testSetupIntroIcon_ShouldHaveFallbackImage() {
        sut.setupIntroIcon(isDarkMode: true)

        XCTAssertEqual(viewControllerSpy.didCallDisplayIcon, 1)
        XCTAssertEqual(viewControllerSpy.fallbackIcon, Resources.Illustrations.iluTakingSelfie.image)
    }

    func testSetupIntroIcon_WhenDarkModeIsTrue_ShouldDisplayIcon() {
        sut.setupIntroIcon(isDarkMode: true)

        XCTAssertEqual(viewControllerSpy.didCallDisplayIcon, 1)
        XCTAssertEqual(viewControllerSpy.iconUrl, URL(string: "https://url.dark.icon"))
    }

    func testSetupIntroIcon_WhenDarkModeIsFalse_ShouldDisplayIcon() {
        sut.setupIntroIcon(isDarkMode: false)

        XCTAssertEqual(viewControllerSpy.didCallDisplayIcon, 1)
        XCTAssertEqual(viewControllerSpy.iconUrl, URL(string: "https://url.light.icon"))
    }
    
    func testSetupTexts_ShouldDisplayTitle() {
        sut.setupTexts()
        
        XCTAssertEqual(viewControllerSpy.didCallDisplayTexts, 1)
        XCTAssertEqual(viewControllerSpy.title, "LABEL TEXT")
    }
    
    func testSetupTexts_ShouldDisplayDescription() {
        sut.setupTexts()
        
        XCTAssertEqual(viewControllerSpy.didCallDisplayTexts, 1)
        XCTAssertEqual(viewControllerSpy.description, "DETAIL TEXT")
    }

    func testSetupAttributedTerms_ShouldDisplayTerms() {
        sut.setupAttributedTerms()

        XCTAssertEqual(viewControllerSpy.didCallDisplayTerms, 1)
        XCTAssertEqual(viewControllerSpy.terms, Strings.FacialBiometric.Intro.terms)
    }

    func testNextScene_ShouldCallPerformWithInstructionsAction() {
        sut.nextScene()

        XCTAssertEqual(coordinatorSpy.didCallInstructionsAction, 1)
    }

    func testReadTerms_ShouldCallPerformWithReadTermsAction() {
        sut.readTerms()

        XCTAssertEqual(coordinatorSpy.didCallReadTermsAction, 1)
    }

    func testLater_ShouldCallPerformWithCloseAction() {
        sut.later()

        XCTAssertEqual(coordinatorSpy.didCallCloseAction, 1)
    }
}
