import XCTest
@testable import IdentityValidation
import UI

private final class PreviewCoordinatingSpy: PreviewCoordinating {
    private(set) var callRetakePictureAction = 0
    private(set) var callNextStepAction = 0
    private(set) var callCloseAction = 0

    func perform(action: PreviewAction) {
        switch action {
        case .retakePicture:
            callRetakePictureAction += 1
        case .nextStep:
            callNextStepAction += 1
        case .close:
            callCloseAction += 1
        }
    }
}

private final class PreviewDisplaySpy: PreviewDisplay {
    var loadingView = LoadingView()

    private(set) var callDisplayPreviewImageCount = 0
    private(set) var callSetupConfirmationViewCount = 0
    private(set) var callSetupUploadErrorViewCount = 0
    private(set) var callDisplayErrorCount = 0
    private(set) var callStartLoadingViewCount = 0
    private(set) var callStopLoadingViewCount = 0

    private(set) var infoText = ""
    private(set) var retakeButtonTitle = ""
    private(set) var acceptButtonTitle = ""
    private(set) var tryAgainButtonTitle = ""
    private(set) var errorMessage = ""

    func displayPreviewImage(_ image: UIImage) {
        callDisplayPreviewImageCount += 1
    }

    func setupConfirmationView(infoText: String, retakeButtonTitle: String, acceptButtonTitle: String) {
        callSetupConfirmationViewCount += 1

        self.infoText = infoText
        self.retakeButtonTitle = retakeButtonTitle
        self.acceptButtonTitle = acceptButtonTitle
    }

    func setupUploadErrorView(tryAgainButtonTitle: String) {
        callSetupUploadErrorViewCount += 1

        self.tryAgainButtonTitle = tryAgainButtonTitle
    }

    func displayError(_ message: String) {
        callDisplayErrorCount += 1

        errorMessage = message
    }

    func startLoadingView() {
        callStartLoadingViewCount += 1
    }

    func stopLoadingView() {
        callStopLoadingViewCount += 1
    }
}

final class PreviewPresenterTests: XCTestCase {
    private let coordinatorSpy = PreviewCoordinatingSpy()
    private let controllerSpy = PreviewDisplaySpy()

    private lazy var sut: PreviewPresenter = {
        let presenter = PreviewPresenter(coordinator: coordinatorSpy)
        presenter.viewController = controllerSpy
        return presenter
    }()

    func testPreviewImage_ShouldDisplayPreviewImage() {
        sut.previewImage(UIImage())

        XCTAssertEqual(controllerSpy.callDisplayPreviewImageCount, 1)
    }

    func testDisplayTexts_WhenTypeIsSelfie_ShouldSetupConfirmationView() {
        sut.displayTexts(for: .selfie)

        XCTAssertEqual(controllerSpy.callSetupConfirmationViewCount, 1)
        XCTAssertEqual(controllerSpy.infoText, Strings.Preview.Selfie.info)
        XCTAssertEqual(controllerSpy.retakeButtonTitle, Strings.Preview.Button.Retake.text)
        XCTAssertEqual(controllerSpy.acceptButtonTitle, Strings.Preview.Button.Accept.text)
    }

    func testDisplayTexts_WhenTypeIsDocumentFront_ShouldSetupConfirmationView() {
        sut.displayTexts(for: .documentFront)

        XCTAssertEqual(controllerSpy.callSetupConfirmationViewCount, 1)
        XCTAssertEqual(controllerSpy.infoText, Strings.Preview.Document.Front.info)
    }

    func testDisplayTexts_WhenTypeIsDocumentBack_ShouldSetupConfirmationView() {
        sut.displayTexts(for: .documentBack)

        XCTAssertEqual(controllerSpy.callSetupConfirmationViewCount, 1)
        XCTAssertEqual(controllerSpy.infoText, Strings.Preview.Document.Back.info)
    }

    func testDisplayTexts_ShouldSetupUploadErrorView() {
        sut.displayTexts(for: .selfie)

        XCTAssertEqual(controllerSpy.callSetupConfirmationViewCount, 1)
        XCTAssertEqual(controllerSpy.tryAgainButtonTitle, Strings.Preview.Button.Retake.text)
    }

    func testRetakePicture_ShouldPerformRetakePictureAction() {
        sut.retakePicture()

        XCTAssertEqual(coordinatorSpy.callRetakePictureAction, 1)
    }

    func testDisplayError_WhenMessageIsNil_ShouldDisplayError() {
        sut.displayError(message: nil)

        XCTAssertEqual(controllerSpy.callDisplayErrorCount, 1)
        XCTAssertEqual(controllerSpy.errorMessage, Strings.Error.message)
    }

    func testDisplayError_WhenMessageIsNotNil_ShouldDisplayError() {
        let errorMessage = "Error Message"
        sut.displayError(message: errorMessage)

        XCTAssertEqual(controllerSpy.callDisplayErrorCount, 1)
        XCTAssertEqual(controllerSpy.errorMessage, errorMessage)
    }

    func testSetLoadingState_WhenTrue_ShouldStartLoading() {
        sut.setLoadingState(true)

        XCTAssertEqual(controllerSpy.callStartLoadingViewCount, 1)
    }

    func testSetLoadingState_WhenFalse_ShouldStopLoading() {
        sut.setLoadingState(false)

        XCTAssertEqual(controllerSpy.callStopLoadingViewCount, 1)
    }

    func testNextStep_ShouldPerformNextStepAction() {
        let step = IdentityStepFactory().make()
        sut.nextStep(step)

        XCTAssertEqual(coordinatorSpy.callNextStepAction, 1)
    }

    func testClose_ShouldPerformCloseAction() {
        sut.close()

        XCTAssertEqual(coordinatorSpy.callCloseAction, 1)
    }
}
