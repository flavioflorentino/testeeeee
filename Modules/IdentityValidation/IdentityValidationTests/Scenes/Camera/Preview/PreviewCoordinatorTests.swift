import XCTest
@testable import IdentityValidation

final class PreviewCoordinatorTests: XCTestCase {
    private let delegateSpy = CameraFlowCoordinatingSpy()

    private lazy var sut = PreviewCoordinator(delegate: delegateSpy)

    func testPerform_WhenActionIsRetakePicture_ShouldCallReturnToCamera() {
        sut.perform(action: .retakePicture)

        XCTAssertEqual(delegateSpy.callReturnToCameraCount, 1)
    }

    func testPerform_WhenActionIsNextStep_ShouldCallFinished() {
        let step = IdentityStepFactory().make()
        sut.perform(action: .nextStep(step))

        XCTAssertEqual(delegateSpy.callFinishedCount, 1)
    }

    func testPerform_WhenActionIsClose_ShouldCallClose() {
        sut.perform(action: .close)

        XCTAssertEqual(delegateSpy.callCloseCount, 1)
    }
}
