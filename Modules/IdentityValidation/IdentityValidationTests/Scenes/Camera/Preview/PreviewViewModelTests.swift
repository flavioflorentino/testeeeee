import AnalyticsModule
import AssetsKit
import Core
import XCTest
@testable import IdentityValidation

private final class PreviewServicingMock: PreviewServicing {
    var shouldUploadSuccessfully = true

    func uploadImage(token: String, flow: String, imageData: Data, completion: @escaping (Result<IdentityStep, ApiError>) -> Void) {
        if shouldUploadSuccessfully {
            let setp = IdentityStepFactory().make()
            completion(.success(setp))
        } else {
            completion(.failure(.bodyNotFound))
        }
    }
}

private final class PreviewPresentingSpy: PreviewPresenting {
    var viewController: PreviewDisplay?
    private(set) var callPreviewImageCount = 0
    private(set) var callDisplayTextsCount = 0
    private(set) var callRetakePictureCount = 0
    private(set) var callDisplayErrorCount = 0
    private(set) var callSetLoadingStateCount = 0
    private(set) var callNextStepCount = 0
    private(set) var callCloseCount = 0

    private(set) var errorMessage: String?

    func previewImage(_ image: UIImage) {
        callPreviewImageCount += 1
    }

    func displayTexts(for type: CameraType) {
        callDisplayTextsCount += 1
    }

    func retakePicture() {
        callRetakePictureCount += 1
    }

    func displayError(message: String?) {
        callDisplayErrorCount += 1
        errorMessage = message
    }

    func setLoadingState(_ loading: Bool) {
        callSetLoadingStateCount += 1
    }

    func nextStep(_ step: IdentityStep) {
        callNextStepCount += 1
    }

    func close() {
        callCloseCount += 1
    }
}

final class PreviewViewModelTests: XCTestCase {
    private let serviceMock = PreviewServicingMock()
    private let presenterSpy = PreviewPresentingSpy()
    private let analytics = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(analytics)

    private lazy var sut = createSut()

    func testSetupPreview_ShouldCallPreviewImage() {
        sut.setupPreview()

        XCTAssertEqual(presenterSpy.callPreviewImageCount, 1)
    }

    func testSetupPreview_ShouldCallDisplayTexts() {
        sut.setupPreview()

        XCTAssertEqual(presenterSpy.callDisplayTextsCount, 1)
    }

    func testRetakePicture_ShouldCallRetakePicture() {
        sut.retakePicture()

        XCTAssertEqual(presenterSpy.callRetakePictureCount, 1)
    }

    func testAcceptPicture_WhenImageDataIsNil_ShouldCallDisplayError() {
        sut.acceptPicture()

        XCTAssertEqual(presenterSpy.callDisplayErrorCount, 1)
        XCTAssertEqual(presenterSpy.errorMessage, Strings.Preview.Error.text)
    }

    func testAcceptPicture_WhenImageDataIsNotNil_ShouldSetLoadingStateTwice() {
        sut = createSut(with: Resources.Illustrations.iluTakingSelfie.image)
        sut.acceptPicture()

        XCTAssertEqual(presenterSpy.callSetLoadingStateCount, 2)
    }

    func testAcceptPicture_WhenImageDataIsNotNil_WhenUploadSuccess_ShouldPresentNextStep() {
        sut = createSut(with: Resources.Illustrations.iluTakingSelfie.image)
        sut.acceptPicture()
        
        serviceMock.shouldUploadSuccessfully = true
        
        XCTAssertEqual(presenterSpy.callNextStepCount, 1)
    }
    
    func testAcceptPicture_WhenImageDataIsNotNil_WhenUploadFailure_ShouldCallDisplayError() {
        sut = createSut(with: Resources.Illustrations.iluTakingSelfie.image)
        serviceMock.shouldUploadSuccessfully = false
        sut.acceptPicture()

        XCTAssertEqual(presenterSpy.callDisplayErrorCount, 1)
        XCTAssertNil(presenterSpy.errorMessage)
    }
    
    func testAcceptPicture_WhenTypeIsSelfie_WhenUploadIsFailure_ShouldTrackError() {
        sut = createSut(with: Resources.Illustrations.iluTakingSelfie.image, type: .selfie)
        serviceMock.shouldUploadSuccessfully = false
        sut.acceptPicture()
        
        let screenViewedProperties: [IdentityValidationEventProperty] = [
            .init(name: .screenName, value: IdentityValidationSelfieEventProperty.error),
            .init(name: .flow, value: "TFA")
        ]
        
        let screenViewedEvent = IdentityValidationTracker(
            eventType: IdentityValidationEventType.screenViewed,
            eventProperties: screenViewedProperties
        ).event()
        
        let dialogViewedProperties: [IdentityValidationEventProperty] = [
            .init(name: .screenName, value: IdentityValidationSelfieEventProperty.error),
            .init(name: .flow, value: "TFA"),
            .init(name: .dialogName, value: IdentityValidationDialogNameProperty.error),
            .init(name: .dialogText, value: "")
        ]
        
        let dialogViewedEvent = IdentityValidationTracker(
            eventType: IdentityValidationEventType.dialogViewed,
            eventProperties: dialogViewedProperties
        ).event()
        
        XCTAssertTrue(analytics.equals(to: screenViewedEvent, dialogViewedEvent))
    }
    
    func testAcceptPicture_WhenTypeIsDocumentFront_WhenUploadIsFailure_ShouldTrackError() {
        sut = createSut(with: Resources.Illustrations.iluTakingSelfie.image, type: .documentFront)
        serviceMock.shouldUploadSuccessfully = false
        sut.acceptPicture()
        
        let screenViewedProperties: [IdentityValidationEventProperty] = [
            .init(name: .screenName, value: IdentityValidationDocumentEventProperty.frontCameraError),
            .init(name: .flow, value: "TFA")
        ]
        
        let screenViewedEvent = IdentityValidationTracker(
            eventType: IdentityValidationEventType.screenViewed,
            eventProperties: screenViewedProperties
        ).event()
        
        let dialogViewedProperties: [IdentityValidationEventProperty] = [
            .init(name: .screenName, value: IdentityValidationDocumentEventProperty.frontCameraError),
            .init(name: .flow, value: "TFA"),
            .init(name: .dialogName, value: IdentityValidationDialogNameProperty.error),
            .init(name: .dialogText, value: "")
        ]
        
        let dialogViewedEvent = IdentityValidationTracker(
            eventType: IdentityValidationEventType.dialogViewed,
            eventProperties: dialogViewedProperties
        ).event()
        
        XCTAssertTrue(analytics.equals(to: screenViewedEvent, dialogViewedEvent))
    }
    
    func testAcceptPicture_WhenTypeIsDocumentBack_WhenUploadIsFailure_ShouldTrackError() {
        sut = createSut(with: Resources.Illustrations.iluTakingSelfie.image, type: .documentBack)
        serviceMock.shouldUploadSuccessfully = false
        sut.acceptPicture()
        
        let screenViewedProperties: [IdentityValidationEventProperty] = [
            .init(name: .screenName, value: IdentityValidationDocumentEventProperty.backCameraError),
            .init(name: .flow, value: "TFA")
        ]
        
        let screenViewedEvent = IdentityValidationTracker(
            eventType: IdentityValidationEventType.screenViewed,
            eventProperties: screenViewedProperties
        ).event()
        
        let dialogViewedProperties: [IdentityValidationEventProperty] = [
            .init(name: .screenName, value: IdentityValidationDocumentEventProperty.backCameraError),
            .init(name: .flow, value: "TFA"),
            .init(name: .dialogName, value: IdentityValidationDialogNameProperty.error),
            .init(name: .dialogText, value: "")
        ]
        
        let dialogViewedEvent = IdentityValidationTracker(
            eventType: IdentityValidationEventType.dialogViewed,
            eventProperties: dialogViewedProperties
        ).event()
        
        XCTAssertTrue(analytics.equals(to: screenViewedEvent, dialogViewedEvent))
    }
    

    func testSetupPreview_WhenCameraTypeIsSelfie_ShouldTrackSendEvent() {
        sut.setupPreview()

        let properties: [IdentityValidationEventProperty] = [
            .init(name: .screenName, value: IdentityValidationSelfieEventProperty.send),
            .init(name: .flow, value: "TFA")
        ]
        
        let expectedEvent = IdentityValidationTracker(
            eventType: IdentityValidationEventType.screenViewed,
            eventProperties: properties
        ).event()
        
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }

    func testSetupPreview_WhenCameraTypeIsDocumentFront_ShouldTrackSendEvent() {
        sut = createSut(type: .documentFront)
        sut.setupPreview()

        let properties: [IdentityValidationEventProperty] = [
            .init(name: .screenName, value: IdentityValidationDocumentEventProperty.frontCameraSend),
            .init(name: .flow, value: "TFA")
        ]
        
        let expectedEvent = IdentityValidationTracker(
            eventType: IdentityValidationEventType.screenViewed,
            eventProperties: properties
        ).event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }

    func testSetupPreview_WhenCameraTypeIsDocumentBack_ShouldTrackSendEvent() {
        sut = createSut(type: .documentBack)
        sut.setupPreview()

        let properties: [IdentityValidationEventProperty] = [
            .init(name: .screenName, value: IdentityValidationDocumentEventProperty.backCameraSend),
            .init(name: .flow, value: "TFA")
        ]
        
        let expectedEvent = IdentityValidationTracker(
            eventType: IdentityValidationEventType.screenViewed,
            eventProperties: properties
        ).event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }

    func testTryAgain_ShouldCallRetakePicture() {
        sut.tryAgain()

        XCTAssertEqual(presenterSpy.callRetakePictureCount, 1)
    }

    func testClose_ShouldCallClose() {
        sut.close()

        XCTAssertEqual(presenterSpy.callCloseCount, 1)
    }
}

private extension PreviewViewModelTests {
    func createSut(with image: UIImage = UIImage(), type: CameraType = .selfie) -> PreviewViewModel {
        return PreviewViewModel(
            serviceToken: "TOKEN",
            flow: "TFA",
            type: type,
            previewImage: image,
            service: serviceMock,
            presenter: presenterSpy,
            dependencies: dependencies
        )
    }
}
