import AssetsKit
import XCTest
@testable import IdentityValidation
import UI

private final class CaptureCoordinatingSpy: CaptureCoordinating {
    private(set) var callCloseAction = 0
    private(set) var callPreviewAction = 0
    private(set) var callSettingsAction = 0

    func perform(action: CaptureAction) {
        switch action {
        case .close:
            callCloseAction += 1
        case .preview:
            callPreviewAction += 1
        case .settings:
            callSettingsAction += 1
        }
    }
}

private final class CaptureDisplaySpy: CaptureDisplay {
    var loadingView = LoadingView()
    private(set) var callShowTipCount = 0
    private(set) var tipText: String?
    private(set) var callShowAttributedTipCount = 0
    private(set) var callAddCameraMaskCount = 0
    private(set) var maskHandler: CameraMaskHandler?
    private(set) var callShowPermissionPopupControllerCount = 0
    private(set) var callShowErrorPopupControllerCount = 0
    private(set) var callStartLoadingViewCount = 0
    private(set) var callStopLoadingViewCount = 0

    private(set) var isSettingsAction = false

    func showTip(text: String) {
        callShowTipCount += 1
        tipText = text
    }

    func showAttributedTip(_ string: NSAttributedString) {
        callShowAttributedTipCount += 1
    }

    func addCameraMask(with handler: CameraMaskHandler) {
        callAddCameraMaskCount += 1
        maskHandler = handler
    }

    func showPermissionPopupController(with properties: PopupProperties) {
        callShowPermissionPopupControllerCount += 1
    }

    func showErrorPopupController(with properties: PopupProperties) {
        callShowErrorPopupControllerCount += 1
    }

    func startLoadingView() {
        callStartLoadingViewCount += 1
    }

    func stopLoadingView() {
        callStopLoadingViewCount += 1
    }
}

final class CapturePresenterTests: XCTestCase {
    private let coordinatorSpy = CaptureCoordinatingSpy()
    private let controllerSpy = CaptureDisplaySpy()

    private lazy var sut: CapturePresenter = {
        let presenter = CapturePresenter(coordinator: coordinatorSpy)
        presenter.viewController = controllerSpy
        return presenter
    }()

    func testDisplayTexts_WhenTypeIsSelfie_ShouldCallShowTip() {
        sut.displayTexts(for: .selfie)

        XCTAssertEqual(controllerSpy.callStartLoadingViewCount, 1)
        XCTAssertEqual(controllerSpy.callShowTipCount, 1)
        XCTAssertEqual(controllerSpy.tipText, Strings.Camera.Selfie.Tip.text)
    }

    func testDisplayTexts_WhenTypeIsDocumentFront_ShouldCallShowAttributedTip() {
        sut.displayTexts(for: .documentFront)

        XCTAssertEqual(controllerSpy.callStartLoadingViewCount, 1)
        XCTAssertEqual(controllerSpy.callShowAttributedTipCount, 1)
    }

    func testDisplayTexts_WhenTypeIsDocumentBack_ShouldCallShowAttributedTip() {
        sut.displayTexts(for: .documentBack)

        XCTAssertEqual(controllerSpy.callStartLoadingViewCount, 1)
        XCTAssertEqual(controllerSpy.callShowAttributedTipCount, 1)
    }

    func testDisplayCameraMask_WhenTypeIsSelfie_ShouldCallShowTip() throws {
        sut.displayCameraMask(for: .selfie)

        XCTAssertEqual(controllerSpy.callStopLoadingViewCount, 1)
        XCTAssertEqual(controllerSpy.callAddCameraMaskCount, 1)
        let handler = try XCTUnwrap(controllerSpy.maskHandler)
        XCTAssertTrue(handler is SelfieMaskHandler)
    }

    func testDisplayCameraMask_WhenTypeIsDocumentFront_ShouldCallShowTip() throws {
        sut.displayCameraMask(for: .documentFront)

        XCTAssertEqual(controllerSpy.callStopLoadingViewCount, 1)
        XCTAssertEqual(controllerSpy.callAddCameraMaskCount, 1)
        let handler = try XCTUnwrap(controllerSpy.maskHandler)
        XCTAssertTrue(handler is DocumentMaskHandler)
    }

    func testDisplayCameraMask_WhenTypeIsDocumentBack_ShouldCallShowTip() throws {
        sut.displayCameraMask(for: .documentBack)

        XCTAssertEqual(controllerSpy.callStopLoadingViewCount, 1)
        XCTAssertEqual(controllerSpy.callAddCameraMaskCount, 1)
        let handler = try XCTUnwrap(controllerSpy.maskHandler)
        XCTAssertTrue(handler is DocumentMaskHandler)
    }

    func testDisplayPermissionDenied_ShouldShowPermissionPopupController() {
        sut.displayPermissionDenied()

        XCTAssertEqual(controllerSpy.callShowPermissionPopupControllerCount, 1)
    }

    func testDisplayError_ShouldShowErrorPopupController() {
        sut.displayError(message: nil)

        XCTAssertEqual(controllerSpy.callShowErrorPopupControllerCount, 1)
    }

    func testShowPreview_ShouldPerformPreviewAction() {
        sut.showPreview(with: UIImage())

        XCTAssertEqual(coordinatorSpy.callPreviewAction, 1)
    }

    func testOpenSettings_ShouldPerformSettingsAction() {
        sut.openSettings()

        XCTAssertEqual(coordinatorSpy.callSettingsAction, 1)
    }

    func testClose_ShouldPerformCloseAction() {
        sut.close()

        XCTAssertEqual(coordinatorSpy.callCloseAction, 1)
    }
}
