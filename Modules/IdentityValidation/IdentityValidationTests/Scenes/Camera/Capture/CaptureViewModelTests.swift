import AnalyticsModule
import XCTest
@testable import IdentityValidation

private final class CameraManagerProtocolMock: CameraManagerProtocol {
    var showPrepareError = false
    var throwDisplayPreviewError = false
    var shouldCaptureSuccessfully = true
    private(set) var callDisplayPreviewCount = 0
    private(set) var callStartRunningCameraCount = 0
    private(set) var callStopRunningCameraCount = 0

    func prepare(camera: CameraManager.CameraType, completion: @escaping (Error?) -> Void) {
        if showPrepareError {
            completion(CameraManager.CameraControllerError.invalidInput)
        } else {
            completion(nil)
        }
    }

    func displayPreview(on view: UIView) throws {
        if throwDisplayPreviewError {
            throw CameraManager.CameraControllerError.invalidInput
        } else {
            callDisplayPreviewCount += 1
        }
    }

    func captureImage(completion: @escaping (Result<UIImage, Error>) -> Void) {
        if shouldCaptureSuccessfully {
            completion(.success(UIImage()))
        } else {
            completion(.failure(CameraManager.CameraControllerError.invalidInput))
        }
    }

    func startRunningCamera() {
        callStartRunningCameraCount += 1
    }

    func stopRunningCamera() {
        callStopRunningCameraCount += 1
    }
}

private final class CapturePresentingSpy: CapturePresenting {
    var viewController: CaptureDisplay?
    private(set) var callDisplayTextsCount = 0
    private(set) var callDisplayCameraMaskCount = 0
    private(set) var callDisplayErrorCount = 0
    private(set) var callDisplayPermissionDeniedCount = 0
    private(set) var callShowPreviewCount = 0
    private(set) var callOpenSettingsCount = 0
    private(set) var callCloseCount = 0

    func displayTexts(for type: CameraType) {
        callDisplayTextsCount += 1
    }

    func displayCameraMask(for type: CameraType) {
        callDisplayCameraMaskCount += 1
    }

    func displayPermissionDenied() {
        callDisplayPermissionDeniedCount += 1
    }

    func displayError(message: String?) {
        callDisplayErrorCount += 1
    }

    func showPreview(with image: UIImage) {
        callShowPreviewCount += 1
    }

    func openSettings() {
        callOpenSettingsCount += 1
    }

    func close() {
        callCloseCount += 1
    }
}

private final class CaptureServicingMock: CaptureServicing {
    var authorizationStatus = DevicePermissionStatus.authorized
    private(set) var callRequestCameraPermissionCount = 0

    func checkCameraPermission(completion: @escaping (DevicePermissionStatus) -> Void) {
        completion(authorizationStatus)
    }

    func requestCameraPermission(completion: ((Bool) -> Void)?) {
        callRequestCameraPermissionCount += 1
    }
}

final class CaptureViewModelTests: XCTestCase {
    private let cameraManagerMock = CameraManagerProtocolMock()
    private let presenterSpy = CapturePresentingSpy()
    private let serviceMock = CaptureServicingMock()
    private let analytics = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(analytics)

    private lazy var sut = createSut(with: .selfie)

    func testSetupCameraView_ShouldDisplayTexts() {
        sut.setupCameraView()

        XCTAssertEqual(presenterSpy.callDisplayTextsCount, 1)
    }

    func testSetupCameraView_WhenCameraTypeIsSelfie_ShouldTrackAppearEvent() {
        sut.setupCameraView()

        let properties: [IdentityValidationEventProperty] = [
            .init(name: .screenName, value: IdentityValidationSelfieEventProperty.camera),
            .init(name: .flow, value: "TFA")
        ]
        
        let expectedEvent = IdentityValidationTracker(
            eventType: IdentityValidationEventType.screenViewed,
            eventProperties: properties
        ).event()
        
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }

    func testSetupCameraView_WhenCameraTypeIsDocumentFront_ShouldTrackAppearEvent() {
        sut = createSut(with: .documentFront)
        sut.setupCameraView()

        let properties: [IdentityValidationEventProperty] = [
            .init(name: .screenName, value: IdentityValidationDocumentEventProperty.frontCamera),
            .init(name: .flow, value: "TFA")
        ]
        
        let expectedEvent = IdentityValidationTracker(
            eventType: IdentityValidationEventType.screenViewed,
            eventProperties: properties
        ).event()
        
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }

    func testSetupCameraView_WhenCameraTypeIsDocumentBack_ShouldTrackAppearEvent() {
        sut = createSut(with: .documentBack)
        sut.setupCameraView()

        let properties: [IdentityValidationEventProperty] = [
            .init(name: .screenName, value: IdentityValidationDocumentEventProperty.backCamera),
            .init(name: .flow, value: "TFA")
        ]
        
        let expectedEvent = IdentityValidationTracker(
            eventType: IdentityValidationEventType.screenViewed,
            eventProperties: properties
        ).event()
        
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }

    func testSetupCameraMask_ShouldDisplayCameraMask() {
        sut.setupCameraMask()

        XCTAssertEqual(presenterSpy.callDisplayCameraMaskCount, 1)
    }

    func testCheckCameraAuthorization_WhenIsAuthorized_WhenShouldNotConfigureCamera_ShouldStartRunningCamera() {
        sut.checkCameraAuthorization(placeholderView: UIView())
        sut.checkCameraAuthorization(placeholderView: UIView())

        XCTAssertEqual(cameraManagerMock.callStartRunningCameraCount, 1)
    }

    func testCheckCameraAuthorization_WhenIsAuthorized_WhenShouldConfigureCamera_WhenErrorIsNotNil_ShouldDisplayError() {
        cameraManagerMock.showPrepareError = true
        sut.checkCameraAuthorization(placeholderView: UIView())

        XCTAssertEqual(presenterSpy.callDisplayErrorCount, 1)
    }

    func testCheckCameraAuthorization_WhenIsAuthorized_WhenShouldConfigureCamera_WhenErrorIsNil_WhenDisplayPreviewThrows_ShouldDisplayError() {
        cameraManagerMock.throwDisplayPreviewError = true
        sut.checkCameraAuthorization(placeholderView: UIView())

        XCTAssertEqual(presenterSpy.callDisplayErrorCount, 1)
    }

    func testCheckCameraAuthorization_WhenIsAuthorized_WhenShouldConfigureCamera_WhenErrorIsNil_ShouldDisplayPreview() {
        sut.checkCameraAuthorization(placeholderView: UIView())

        XCTAssertEqual(cameraManagerMock.callDisplayPreviewCount, 1)
    }

    func testCheckCameraAuthorization_WhenIsNotDetermined_() {
        serviceMock.authorizationStatus = .notDetermined
        sut.checkCameraAuthorization(placeholderView: UIView())

        XCTAssertEqual(serviceMock.callRequestCameraPermissionCount, 1)
    }

    func testCheckCameraAuthorization_WhenIsDenied_ShouldDisplayPermissionDenied() {
        serviceMock.authorizationStatus = .denied
        sut.checkCameraAuthorization(placeholderView: UIView())

        XCTAssertEqual(presenterSpy.callDisplayPermissionDeniedCount, 1)
    }

    func testCheckCameraAuthorization_WhenIsDisabled_ShouldDisplayPermissionDenied() {
        serviceMock.authorizationStatus = .disabled
        sut.checkCameraAuthorization(placeholderView: UIView())

        XCTAssertEqual(presenterSpy.callDisplayPermissionDeniedCount, 1)
    }

    func testTakePhoto_ShouldStopCamera() {
        sut.takePhoto()

        XCTAssertEqual(cameraManagerMock.callStopRunningCameraCount, 1)
    }

    func testTakePhoto_WhenCaptureIsSuccess_ShouldShowPreview() {
        sut.takePhoto()

        XCTAssertEqual(presenterSpy.callShowPreviewCount, 1)
    }

    func testTakePhoto_WhenCaptureIsFailure_ShouldDisplayError() {
        cameraManagerMock.shouldCaptureSuccessfully = false
        sut.takePhoto()

        XCTAssertEqual(presenterSpy.callDisplayErrorCount, 1)
    }

    func testStopCamera_ShouldStopCamera() {
        sut.stopCamera()

        XCTAssertEqual(cameraManagerMock.callStopRunningCameraCount, 1)
    }

    func testOpenSettings_ShouldCallPresenterOpenSettings() {
        sut.openSettings()

        XCTAssertEqual(presenterSpy.callOpenSettingsCount, 1)
    }

    func testClose_ShouldCallClose() {
        sut.close()

        XCTAssertEqual(presenterSpy.callCloseCount, 1)
    }
}

private extension CaptureViewModelTests {
    func createSut(with cameraType: CameraType) -> CaptureViewModel {
        CaptureViewModel(
            presenter: presenterSpy,
            service: serviceMock,
            type: cameraType,
            dependencies: dependencies,
            flow: "TFA",
            cameraManager: cameraManagerMock
        )
    }
}
