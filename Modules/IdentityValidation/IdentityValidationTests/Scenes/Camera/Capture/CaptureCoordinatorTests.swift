import XCTest
@testable import IdentityValidation

private final class ApplicationSpy: UIApplicationContract {
    private(set) var callOpenUrlCount = 0

    func open(
        _ url: URL,
        options: [UIApplication.OpenExternalURLOptionsKey : Any],
        completionHandler completion: ((Bool) -> Void)?)
    {
        callOpenUrlCount += 1
    }

    func canOpenURL(_ url: URL) -> Bool {
        true
    }
}

final class CaptureCoordinatorTests: XCTestCase {
    private let delegateSpy = CameraFlowCoordinatingSpy()
    private let applicationSpy = ApplicationSpy()

    private lazy var sut = CaptureCoordinator(delegate: delegateSpy, application: applicationSpy)

    func testPerform_WhenActionIsClose_ShouldCallClose() {
        sut.perform(action: .close)

        XCTAssertEqual(delegateSpy.callCloseCount, 1)
    }

    func testPerform_WhenActionSettings_ShouldCallAplication() {
        sut.perform(action: .settings)

        XCTAssertEqual(applicationSpy.callOpenUrlCount, 1)
    }

    func testPerform_WhenActionPreview_ShouldCallNavigateToPreview() {
        sut.perform(action: .preview(UIImage()))

        XCTAssertEqual(delegateSpy.callNavigateToPreviewCount, 1)
    }
}
