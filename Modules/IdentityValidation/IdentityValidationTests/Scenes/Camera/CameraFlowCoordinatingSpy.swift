@testable import IdentityValidation
import UIKit

final class CameraFlowCoordinatingSpy: CameraFlowCoordinating {
    private(set) var callNavigateToPreviewCount = 0
    private(set) var callReturnToCameraCount = 0
    private(set) var callFinishedCount = 0
    private(set) var callCloseCount = 0

    func navigateToPreview(with image: UIImage) {
        callNavigateToPreviewCount += 1
    }

    func returnToCamera() {
        callReturnToCameraCount += 1
    }

    func finished(with nextStep: IdentityStep) {
        callFinishedCount += 1
    }

    func close() {
        callCloseCount += 1
    }
}
