import XCTest
@testable import IdentityValidation

private class CameraFlowCoordinatorDelegateSpy: CameraFlowCoordinatorDelegate {
    private(set) var callFinishedCount = 0
    private(set) var callCloseCameraCount = 0

    func finished(with nextStep: IdentityStep) {
        callFinishedCount += 1
    }

    func closeCamera() {
        callCloseCameraCount += 1
    }
}

final class CameraFlowCoordinatorTests: XCTestCase {
    private let delegateSpy = CameraFlowCoordinatorDelegateSpy()
    private let originController = NavigationMock(rootViewController: UIViewController())
    private let navController = NavigationMock(rootViewController: UIViewController())

    private lazy var sut = CameraFlowCoordinator(
        serviceToken: "TOKEN",
        flow: "TFA",
        type: .selfie,
        delegate: delegateSpy,
        originController: originController,
        navigationController: navController
    )

    func testStart_ShouldPresentCaptureController() throws {
        sut.start()

        XCTAssertTrue(originController.isPresentViewControllerCalled)
        let controller = try XCTUnwrap(navController.pushedViewController)
        XCTAssertTrue(controller is CaptureViewController)
    }

    func testNavigateToPreview_ShouldPushPreviewController() throws {
        sut.navigateToPreview(with: UIImage())

        let controller = try XCTUnwrap(navController.pushedViewController)
        XCTAssertTrue(controller is PreviewViewController)
    }

    func testReturnToCamera_ShouldPopViewController() {
        sut.returnToCamera()

        XCTAssertTrue(navController.isPopViewControllerCalled)
    }

    func testFinished_ShouldCallDelegateFinished() {
        sut.finished(with: IdentityStepFactory().make())

        XCTAssertEqual(delegateSpy.callFinishedCount, 1)
    }

    func testClose_ShouldCallCloseCamera() {
        sut.close()

        XCTAssertEqual(delegateSpy.callCloseCameraCount, 1)
    }
}
