import AnalyticsModule
import XCTest
@testable import IdentityValidation

final class IDValidationFlowCoordinatorTests: XCTestCase {
    private let originController = NavigationMock(rootViewController: UIViewController())
    private let navController = NavigationMock(rootViewController: UIViewController())
    private var completionCalledCount = 0
    private var validationStatus: IDValidationStatus?

    private let analytics = AnalyticsSpy()
    private lazy var mockedDependencies = DependencyContainerMock(analytics)
    
    private lazy var sut = IDValidationFlowCoordinator(
        from: originController,
        token: "BEST_TOKEN_BR",
        flow: "TFA",
        completion: completionHandler,
        navigationController: navController
    )

    // MARK: - IDValidationCoordinatorProtocol

    func testStart_ShouldPresentLoaderController() throws {
        sut.start()

        XCTAssertTrue(originController.isPresentViewControllerCalled)
        let controller = try XCTUnwrap(navController.pushedViewController)
        XCTAssertTrue(controller is LoaderViewController)
    }

    // MARK: - IdentityValidationCoordinating

    func testPresentNextStep_WhenTypeIsSelfie_ShouldStartFacialBiometricCoordinator() throws {
        let step = IdentityStepFactory(type: .selfie).make()
        sut.presentNextStep(step)

        let controller = try XCTUnwrap(navController.pushedViewController)
        XCTAssertTrue(controller is FacialBiometricIntroViewController)
    }

    func testPresentNextStep_WhenTypeIsDocumentFront_ShouldStartDocumentCaptureCoordinator() throws {
        let step = IdentityStepFactory(type: .documentFront).make()
        sut.presentNextStep(step)

        let controller = try XCTUnwrap(navController.pushedViewController)
        XCTAssertTrue(controller is DocumentCaptureIntroViewController)
    }

    func testPresentNextStep_WhenTypeIsDocumentBack_ShouldStartDocumentCaptureCoordinator() throws {
        let step = IdentityStepFactory(type: .documentBack).make()
        sut.presentNextStep(step)

        let controller = try XCTUnwrap(navController.pushedViewController)
        XCTAssertTrue(controller is DocumentCaptureIntroViewController)
    }

    func testPresentNextStep_WhenTypeIsDone_ShouldPushStatusController() throws {
        let step = IdentityStepFactory(type: .done).make()
        sut.presentNextStep(step)

        let controller = try XCTUnwrap(navController.pushedViewController)
        XCTAssertTrue(controller is StatusViewController)
    }

    func testPresentNextStep_WhenTypeIsProcessing_ShouldPushStatusController() throws {
        let step = IdentityStepFactory(type: .processing).make()
        sut.presentNextStep(step)

        let controller = try XCTUnwrap(navController.pushedViewController)
        XCTAssertTrue(controller is StatusViewController)
    }

    func testPresentNextStep_WhenTypeIsOnHold_ShouldPushStatusController() throws {
        let step = IdentityStepFactory(type: .onHold).make()
        sut.presentNextStep(step)

        let controller = try XCTUnwrap(navController.pushedViewController)
        XCTAssertTrue(controller is StatusViewController)
    }

    func testPresentNextStep_WhenTypeIsAlternative_ShouldDismissWithStatus() {
        let step = IdentityStepFactory(status: .approved, type: .alternative).make()
        sut.presentNextStep(step)

        XCTAssertTrue(originController.isDismissViewControllerCalled)
        XCTAssertEqual(completionCalledCount, 1)
        XCTAssertEqual(validationStatus, .approved)
    }

    func testEndWithStatus_ShouldDismissWithStatus() {
        sut.endWithStatus(.inconclusive)

        XCTAssertTrue(originController.isDismissViewControllerCalled)
        XCTAssertEqual(completionCalledCount, 1)
        XCTAssertEqual(validationStatus, .inconclusive)
    }

    func testDismiss_ShouldCallCompletionBlockWithStatusNil() {
        sut.dismiss()

        XCTAssertTrue(originController.isDismissViewControllerCalled)
        XCTAssertEqual(completionCalledCount, 1)
        XCTAssertNil(validationStatus)
    }
    
    func testDismiss_ShouldLogNullStatusEvent() {
        sut.dependencies = mockedDependencies
        sut.dismiss()

        XCTAssertNil(validationStatus)
        
        let properties: [IdentityValidationEventProperty] = [
            .init(name: .status, value: "NULL"),
            .init(name: .flow, value: "TFA")
        ]
        
        let expectedEvent = IdentityValidationTracker(
            eventType: IdentityValidationEventType.idValidationClosed,
            eventProperties: properties
        ).event()
        
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testEndWithStatus_ShouldLogCorrectStatusEvent() {
        sut.dependencies = mockedDependencies
        sut.endWithStatus(.approved)
        
        let properties: [IdentityValidationEventProperty] = [
            .init(name: .status, value: IDValidationStatus.approved.rawValue),
            .init(name: .flow, value: "TFA")
        ]
        
        let expectedEvent = IdentityValidationTracker(
            eventType: IdentityValidationEventType.idValidationClosed,
            eventProperties: properties
        ).event()
        
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
}

private extension IDValidationFlowCoordinatorTests {
    func completionHandler(_ status: IDValidationStatus?) {
        completionCalledCount += 1
        validationStatus = status
    }
}
