import XCTest
@testable import IdentityValidation

final class LocalMock<T: Decodable> {
    func pureLocalMock(_ fileName: String) -> T? {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        guard
            let data = data(fileName),
            let item = try? decoder.decode(T.self, from: data)
            else {
                return nil
        }
        return item
    }
    
    private func data(_ fileName: String) -> Data? {
        guard
            let file = Bundle(for: type(of: self)).path(forResource: fileName, ofType: "json"),
            let data = try? Data(contentsOf: URL(fileURLWithPath: file))
            else {
                return nil
        }
        return data
    }
}
