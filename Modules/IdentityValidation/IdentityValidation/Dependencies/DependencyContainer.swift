import Foundation
import AnalyticsModule
import Core

typealias Dependencies =
    HasMainQueue &
    HasAnalytics

protocol HasAnalytics {
    var analytics: AnalyticsProtocol { get }
}

final class DependencyContainer: Dependencies {
    lazy var mainQueue = DispatchQueue.main
    lazy var analytics: AnalyticsProtocol = Analytics.shared
}
