import AVFoundation
import Core

enum DevicePermissionStatus: String {
    case authorized = "Authorized"
    case denied = "Denied"
    case disabled = "Disabled"
    case notDetermined = "Not Determined"

    internal init?(string: String?) {
        guard let string = string else {
            return nil
        }
        self.init(rawValue: string)
    }
}

protocol CameraPermissionHandling {
    var cameraAuthorizationStatus: DevicePermissionStatus { get }
    func requestCamera(_ completion: @escaping (Bool) -> Void)
}

struct PermissionHandler: CameraPermissionHandling {
    static let cameraUsageDescription = "NSCameraUsageDescription"

    var cameraAuthorizationStatus: DevicePermissionStatus {
        let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)

        switch status {
        case .authorized:
            return .authorized
        case .notDetermined:
            return .notDetermined
        default:
            return .denied
        }
    }

    func requestCamera(_ completion: @escaping (Bool) -> Void) {
        guard Bundle.main.object(forInfoDictionaryKey: PermissionHandler.cameraUsageDescription) != nil else {
            debugPrint("WARNING: \(PermissionHandler.cameraUsageDescription) not found in Info.plist")
            return
        }

        guard !ProcessInfo.processInfo.environment.keys.contains("XCTestConfigurationFilePath") else {
            return
        }

        AVCaptureDevice.requestAccess(for: AVMediaType.video) { authorized in
            completion(authorized)
        }
    }
}
