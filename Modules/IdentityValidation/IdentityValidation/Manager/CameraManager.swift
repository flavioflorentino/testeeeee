import AVFoundation
import UIKit

protocol CameraManagerProtocol {
    func prepare(camera: CameraManager.CameraType, completion: @escaping (Error?) -> Void)
    func displayPreview(on view: UIView) throws
    func captureImage(completion: @escaping (Result<UIImage, Error>) -> Void)
    func startRunningCamera()
    func stopRunningCamera()
}

final class CameraManager: NSObject, CameraManagerProtocol {
    private let cameraQueue: DispatchQueue
    private let captureSession: AVCaptureSession
    private let previewLayer: AVCaptureVideoPreviewLayer

    private weak var previewView: UIView?

    private var currentCamera: CameraType?
    private var photoOutput: AVCaptureOutput?
    private var frontCamera: AVCaptureDevice?
    private var frontCameraInput: AVCaptureDeviceInput?
    private var backCamera: AVCaptureDevice?
    private var backCameraInput: AVCaptureDeviceInput?

    private var photoCaptureCompletionBlock: ((Result<UIImage, Error>) -> Void)?

    init(queue: DispatchQueue = DispatchQueue(label: "cameraManager")) {
        self.cameraQueue = queue
        captureSession = AVCaptureSession()
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        previewLayer.connection?.videoOrientation = .portrait
        super.init()
    }

    func prepare(camera: CameraManager.CameraType, completion: @escaping (Error?) -> Void) {
        cameraQueue.async { [weak self] in
            var prepareError: Error?

            do {
                self?.configureCaptureDevices()
                try self?.configureDeviceInputs(for: camera)
                self?.configurePhotoOutput()
            } catch {
                prepareError = error
            }

            DispatchQueue.main.async {
                completion(prepareError)
            }
        }
    }

    func displayPreview(on view: UIView) throws {
        guard captureSession.isRunning else {
            throw CameraControllerError.captureSessionNotRunning
        }

        view.layer.insertSublayer(previewLayer, at: 0)
        previewView = view
        previewLayer.frame = view.frame
    }

    func captureImage(completion: @escaping (Result<UIImage, Error>) -> Void) {
        photoCaptureCompletionBlock = completion

        guard captureSession.isRunning else {
            completion(.failure(CameraControllerError.captureSessionNotRunning))
            return
        }

        let newCapturePhotoSettings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecJPEG])
        if let capturePhotoOutput = photoOutput as? AVCapturePhotoOutput {
            cameraQueue.async {
                capturePhotoOutput.capturePhoto(with: newCapturePhotoSettings, delegate: self)
            }
        } else {
            completion(.failure(CameraControllerError.photoOutputIsNil))
        }
    }

    func startRunningCamera() {
        if !captureSession.isRunning {
            captureSession.startRunning()            
        }
    }

    func stopRunningCamera() {
        captureSession.stopRunning()
    }
}

extension CameraManager: AVCapturePhotoCaptureDelegate {
    // swiftlint:disable:next function_parameter_count
    func photoOutput(
            _ captureOutput: AVCapturePhotoOutput,
            didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?,
            previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?,
            resolvedSettings: AVCaptureResolvedPhotoSettings,
            bracketSettings: AVCaptureBracketedStillImageSettings?,
            error: Swift.Error?
        ) {
        if let error = error {
            DispatchQueue.main.async {
                self.photoCaptureCompletionBlock?(.failure(error))
            }
            return
        }

        guard
            let buffer = photoSampleBuffer,
            let data = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: buffer, previewPhotoSampleBuffer: nil),
            let image = UIImage(data: data)
            else {
                DispatchQueue.main.async {
                    self.photoCaptureCompletionBlock?(.failure(CameraControllerError.delegateError))
                }
                return
        }

        DispatchQueue.main.async {
            self.photoCaptureCompletionBlock?(.success(image))
        }
    }
}

private extension CameraManager {
    func configureCaptureDevices() {
        frontCamera = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front)
        backCamera = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back)
    }

    func configureDeviceInputs(for camera: CameraType) throws {
        switch camera {
        case .front:
            try configureFrontCamera()

        case .back:
            try configureBackCamera()
        }
    }

    func configureFrontCamera() throws {
        guard let frontCamera = frontCamera else { throw CameraControllerError.cameraNotAvailable }

        let newFrontCameraInput = try AVCaptureDeviceInput(device: frontCamera)
        frontCameraInput = newFrontCameraInput
        try addNewCaptureInput(newFrontCameraInput)
        currentCamera = .front
    }

    func configureBackCamera() throws {
        guard let backCamera = backCamera else { throw CameraControllerError.cameraNotAvailable }

        try backCamera.lockForConfiguration()
        backCamera.focusMode = .continuousAutoFocus
        backCamera.exposureMode = .continuousAutoExposure
        backCamera.unlockForConfiguration()

        let newBackCameraInput = try AVCaptureDeviceInput(device: backCamera)
        backCameraInput = newBackCameraInput
        try addNewCaptureInput(newBackCameraInput)
        currentCamera = .back
    }

    func configurePhotoOutput() {
        let newPhotoOutput = AVCapturePhotoOutput()
        photoOutput = newPhotoOutput

        if captureSession.canAddOutput(newPhotoOutput) {
            captureSession.addOutput(newPhotoOutput)
        }
        captureSession.startRunning()
    }

    func addNewCaptureInput(_ input: AVCaptureInput) throws {
        if captureSession.canAddInput(input) {
            captureSession.addInput(input)
        } else {
            throw CameraControllerError.invalidCaptureInput
        }
    }
}

extension CameraManager {
    enum CameraControllerError: Swift.Error {
        case captureSessionNotRunning
        case invalidInput
        case photoOutputIsNil
        case delegateError
        case cameraNotAvailable
        case invalidCaptureInput
    }

    enum CameraType {
        case front
        case back
    }
}
