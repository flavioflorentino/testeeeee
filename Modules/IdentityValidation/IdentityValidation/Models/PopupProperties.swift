import Foundation
import UI

struct PopupProperties {
    let type: PopupType
    let title: String
    let message: String
    let mainButtonTitle: String
    let secondaryButtonTitle: String?
}
