import Foundation

enum IdentityStepType: String, Decodable {
    case initial = "INITIAL"
    case questions = "QUESTIONS"
    case selfie = "SELFIE"
    case documentBack = "BACK_DOCUMENT"
    case documentFront = "FRONT_DOCUMENT"
    case processing = "PROCESSING"
    case done = "DONE"
    case alternative = "ALTERNATIVE"
    case onHold = "ON_HOLD"
}
