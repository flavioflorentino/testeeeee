import Foundation

struct StepIcons: Decodable {
    var darkMode: String
    var lightMode: String
}
