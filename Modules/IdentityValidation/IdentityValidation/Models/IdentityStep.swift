import Foundation

struct IdentityStep: Decodable {
    let label: String
    let detail: String
    let status: IDValidationStatus?
    let type: IdentityStepType
    let icons: StepIcons
    let waitTime: Int?
}
