import Foundation

public enum IDValidationStatus: String, Decodable {
    case notStarted = "NOT_STARTED"
    case notVerified = "NOT_VERIFIED"
    case notCreated = "NOT_CREATED"
    case approved = "APPROVED"
    case verified = "VERIFIED"
    case toVerify = "TO_VERIFY"
    case rejected = "REJECTED"
    case undefined = "UNDEFINED"
    case pending = "PENDING"
    case disabled = "DISABLED"
    case inconclusive = "INCONCLUSIVE"
}
