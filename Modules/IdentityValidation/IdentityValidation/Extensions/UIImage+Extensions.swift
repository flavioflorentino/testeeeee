import UIKit
import Foundation

extension UIImage {
    @objc
    func resizeImage(targetSize: CGSize) -> UIImage? {
        let newSize = createNewSize(targetSize: targetSize)
        let rect = CGRect(origin: .zero, size: CGSize(width: newSize.width, height: newSize.height))

        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        draw(in: rect)

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage
    }
    
    private func createNewSize(targetSize: CGSize) -> CGSize {
        let widthRatio = targetSize.width / size.width
        let heightRatio = targetSize.height / size.height
        
        if widthRatio > heightRatio {
            return CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            return CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
        }
    }
}
