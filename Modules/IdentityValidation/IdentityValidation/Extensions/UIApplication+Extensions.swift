import UIKit
import Foundation

protocol UIApplicationContract {
    func open(_ url: URL, options: [UIApplication.OpenExternalURLOptionsKey: Any], completionHandler completion: ((Bool) -> Void)?)
    func canOpenURL(_ url: URL) -> Bool
}

extension UIApplicationContract {
    func open(_ url: URL) {
        open(url, options: [:], completionHandler: nil)
    }
}

extension UIApplication: UIApplicationContract { }
