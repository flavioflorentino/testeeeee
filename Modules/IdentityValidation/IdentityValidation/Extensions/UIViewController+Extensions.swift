import UI
import UIKit

extension UIViewController {
    func showPopupController(with properties: PopupProperties,
                             mainButtonAction: (() -> Void)?,
                             secondaryButtonAction: (() -> Void)? = nil) {
        let popupController = PopupViewController(
            title: properties.title,
            description: properties.message,
            preferredType: properties.type
        )
        popupController.hideCloseButton = true

        let mainButtonPopupAction = PopupAction(title: properties.mainButtonTitle,
                                                style: .fill) { [weak popupController] in
            popupController?.dismiss(animated: true) {
                mainButtonAction?()
            }
        }
        popupController.addAction(mainButtonPopupAction)

        if let secondaryButtonTitle = properties.secondaryButtonTitle {
            let secondaryButtonPopupAction = PopupAction(title: secondaryButtonTitle,
                                                         style: .link) { [weak popupController] in
                popupController?.dismiss(animated: true) {
                    secondaryButtonAction?()
                }
            }
            popupController.addAction(secondaryButtonPopupAction)
        }

        showPopup(popupController)
    }
}
