import Core
import Foundation
import WebKit

protocol TermsViewModelInputs: AnyObject {
    func loadWebPage()
    func webViewDidFinishLoading(_ webView: WKWebView)
    func webViewDidReceiveChallenge(
        _ challenge: URLAuthenticationChallenge,
        completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void
    )
}

final class TermsViewModel {
    private let presenter: TermsPresenting

    init(presenter: TermsPresenting) {
        self.presenter = presenter
    }
}

extension TermsViewModel: TermsViewModelInputs {
    func loadWebPage() {
        guard
            let termsUrl = URL(string: "https://picpay.com/app_webviews/terms/")
            else {
                return
        }
        presenter.loadWebPage(for: URLRequest(url: termsUrl))
    }

    func webViewDidFinishLoading(_ webView: WKWebView) {
        presenter.webPageFinishedLoading()

        webView.evaluateJavaScript("document.title") { value, _ in
            self.presenter.displayPageTitle(value as? String)
        }
    }

    func webViewDidReceiveChallenge(
        _ challenge: URLAuthenticationChallenge,
        completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void
    ) {
        let pinningHandler = PinningHandler()
        pinningHandler.handle(challenge: challenge, completionHandler: completionHandler)
    }
}
