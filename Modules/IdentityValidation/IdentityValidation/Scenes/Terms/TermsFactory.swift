import Foundation

enum TermsFactory {
    static func make() -> TermsViewController {
        let presenter = TermsPresenter()
        let viewModel = TermsViewModel(presenter: presenter)
        let viewController = TermsViewController(viewModel: viewModel)

        presenter.viewController = viewController

        return viewController
    }
}
