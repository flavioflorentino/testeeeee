import Foundation

protocol TermsPresenting: AnyObject {
    var viewController: TermsDisplay? { get set }
    func loadWebPage(for request: URLRequest)
    func webPageFinishedLoading()
    func displayPageTitle(_ title: String?)
}

final class TermsPresenter: TermsPresenting {
    weak var viewController: TermsDisplay?

    func loadWebPage(for request: URLRequest) {
        viewController?.startLoadingView()
        viewController?.displayWebPage(request)
    }

    func webPageFinishedLoading() {
        viewController?.stopLoadingView()
    }

    func displayPageTitle(_ title: String?) {
        viewController?.displayTitle(title)
    }
}
