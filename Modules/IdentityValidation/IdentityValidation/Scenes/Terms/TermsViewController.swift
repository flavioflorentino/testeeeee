import UI
import UIKit
import WebKit

protocol TermsDisplay: AnyObject, LoadingViewProtocol {
    func displayWebPage(_ request: URLRequest)
    func displayTitle(_ title: String?)
}

final class TermsViewController: ViewController<TermsViewModelInputs, UIView> {
    fileprivate enum Layout { }

    private lazy var webView: WKWebView = {
        let webView = WKWebView()
        webView.isMultipleTouchEnabled = true
        webView.navigationDelegate = self
        return webView
    }()

    lazy var loadingView = LoadingView()

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadWebPage()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func buildViewHierarchy() {
        view.addSubview(webView)
    }
    
    override func setupConstraints() {
        webView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}

extension TermsViewController: TermsDisplay {
    func displayWebPage(_ request: URLRequest) {
        webView.load(request)
    }

    func displayTitle(_ title: String?) {
        self.title = title
    }
}

extension TermsViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation?) {
        viewModel.webViewDidFinishLoading(webView)
    }

    func webView(
        _ webView: WKWebView,
        didReceive challenge: URLAuthenticationChallenge,
        completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void
    ) {
        viewModel.webViewDidReceiveChallenge(challenge, completionHandler: completionHandler)
    }
}
