import UIKit
import UI
import UIKit

protocol FacialBiometricIntroDisplay: AnyObject {
    func displayIcon(iconUrl: URL?, fallbackIcon: UIImage)
    func displayTexts(title: String, description: String, buttonTitle: String)
    func displayTerms(terms: NSAttributedString)
}

extension FacialBiometricIntroViewController.Layout {
    enum Size {
        static let image: CGFloat = 130
    }
}

final class FacialBiometricIntroViewController: ViewController<FacialBiometricIntroViewModelInputs, UIView>, FacialBiometricIntroDisplay {
    fileprivate enum Layout { }

    private var isUserInterfaceStyleDark: Bool {
        if #available(iOS 12.0, *) {
            return traitCollection.userInterfaceStyle == .dark
        }
        return false
    }

    private lazy var infoView: IdentityInfoView = {
        let infoView = IdentityInfoView()
        infoView.mainButtonAction = { [weak self] in self?.didTapNext() }
        return infoView
    }()

    private lazy var laterButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Strings.General.later, for: .normal)
        button.addTarget(self, action: #selector(didTapLater), for: .touchUpInside)
        button.buttonStyle(LinkButtonStyle())
        return button
    }()
    
    private lazy var termsLabel: UILabel = {
        let label = UILabel()
        label.isUserInteractionEnabled = true
        label.addGestureRecognizer(termsGestureRecognizer)
        label
            .labelStyle(CaptionLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var termsGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapTermsOfUse))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func buildViewHierarchy() {
        view.addSubview(infoView)
        view.addSubview(laterButton)
        view.addSubview(termsLabel)
    }
    
    override func setupConstraints() {
        infoView.snp.makeConstraints {
            $0.centerY.equalToSuperview().offset(-Spacing.base10)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        laterButton.snp.makeConstraints {
            $0.top.equalTo(infoView.snp.bottom).offset(Spacing.base01)
            $0.centerX.equalToSuperview()
        }
        
        termsLabel.snp.makeConstraints {
            $0.top.equalTo(laterButton.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalTo(infoView)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        viewModel.setupIcon(isDarkMode: isUserInterfaceStyleDark)
    }

    // MARK: - FacialBiometricIntroDisplay

    func displayIcon(iconUrl: URL?, fallbackIcon: UIImage) {
        infoView.setIconImage(from: iconUrl, fallbackIcon: fallbackIcon)
    }

    func displayTexts(title: String, description: String, buttonTitle: String) {
        infoView.configureView(with: title, description: description, buttonTitle: buttonTitle)
    }

    func displayTerms(terms: NSAttributedString) {
        termsLabel.attributedText = terms
    }
}

private extension FacialBiometricIntroViewController {
    func didTapNext() {
        viewModel.nextScene()
    }

    @objc
    func didTapLater() {
        viewModel.later()
    }

    @objc
    func didTapTermsOfUse() {
        viewModel.readTerms()
    }
}
