import Foundation

enum FacialBiometricIntroAction {
    case instructions
    case readTerms
    case close
}

protocol FacialBiometricIntroCoordinating {
    func perform(action: FacialBiometricIntroAction)
}

final class FacialBiometricIntroCoordinator: FacialBiometricIntroCoordinating {
    private weak var delegate: FacialBiometricFlowCoordinating?
    
    init(delegate: FacialBiometricFlowCoordinating?) {
        self.delegate = delegate
    }
    
    func perform(action: FacialBiometricIntroAction) {
        switch action {
        case .instructions:
            delegate?.navigateToInstructionsScene()
        case .readTerms:
            delegate?.navigateToTerms()
        case .close:
            delegate?.close()
        }
    }
}
