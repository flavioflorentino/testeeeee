import AssetsKit
import Foundation
import UI
import UIKit

protocol FacialBiometricIntroPresenting: AnyObject {
    var viewController: FacialBiometricIntroDisplay? { get set }
    func setupIntroIcon(isDarkMode: Bool)
    func setupTexts()
    func setupAttributedTerms()
    func nextScene()
    func readTerms()
    func later()
}

final class FacialBiometricIntroPresenter: FacialBiometricIntroPresenting {
    private let coordinator: FacialBiometricIntroCoordinating
    private let step: IdentityStep
    weak var viewController: FacialBiometricIntroDisplay?
    
    private lazy var underlineAttributed: [NSAttributedString.Key: Any] = {
        [.foregroundColor: Colors.branding400.color, .underlineStyle: NSUnderlineStyle.single.rawValue]
    }()
    
    init(coordinator: FacialBiometricIntroCoordinating, step: IdentityStep) {
        self.coordinator = coordinator
        self.step = step
    }

    func setupIntroIcon(isDarkMode: Bool) {
        let iconStringUrl = isDarkMode ? step.icons.darkMode : step.icons.lightMode
        viewController?.displayIcon(iconUrl: URL(string: iconStringUrl),
                                    fallbackIcon: Resources.Illustrations.iluTakingSelfie.image)
    }

    func setupTexts() {
        viewController?.displayTexts(title: step.label,
                                     description: step.detail,
                                     buttonTitle: Strings.General.takePhoto)
    }
    
    func setupAttributedTerms() {
        let attributedText = NSMutableAttributedString(string: Strings.FacialBiometric.Intro.terms)
        let attributedRange = (attributedText.string as NSString).range(of: Strings.FacialBiometric.Intro.termsLink)
        
        attributedText.addAttributes(underlineAttributed, range: attributedRange)
        
        viewController?.displayTerms(terms: attributedText)
    }
    
    func nextScene() {
        coordinator.perform(action: .instructions)
    }
    
    func readTerms() {
        coordinator.perform(action: .readTerms)
    }

    func later() {
        coordinator.perform(action: .close)
    }
}
