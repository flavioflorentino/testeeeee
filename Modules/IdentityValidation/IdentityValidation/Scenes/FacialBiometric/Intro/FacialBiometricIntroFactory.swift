import Foundation

enum FacialBiometricIntroFactory {
    static func make(
        step: IdentityStep,
        flow: String,
        delegate: FacialBiometricFlowCoordinating?
    ) -> FacialBiometricIntroViewController {
        let dependencyContainer = DependencyContainer()
        let coordinator = FacialBiometricIntroCoordinator(delegate: delegate)
        let presenter = FacialBiometricIntroPresenter(coordinator: coordinator, step: step)
        let viewModel = FacialBiometricIntroViewModel(presenter: presenter, dependencies: dependencyContainer, flow: flow)
        let controller = FacialBiometricIntroViewController(viewModel: viewModel)
        
        presenter.viewController = controller

        return controller
    }
}
