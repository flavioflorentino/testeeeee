import Foundation

protocol FacialBiometricIntroViewModelInputs: AnyObject {
    func loadData()
    func setupIcon(isDarkMode: Bool)
    func nextScene()
    func later()
    func readTerms()
}

final class FacialBiometricIntroViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    private let flow: String
    
    private let presenter: FacialBiometricIntroPresenting

    init(presenter: FacialBiometricIntroPresenting, dependencies: Dependencies, flow: String) {
        self.presenter = presenter
        self.dependencies = dependencies
        self.flow = flow
    }
}

extension FacialBiometricIntroViewModel: FacialBiometricIntroViewModelInputs {
    func loadData() {
        presenter.setupTexts()
        presenter.setupAttributedTerms()
        sendScreenTrack()
    }

    func setupIcon(isDarkMode: Bool) {
        presenter.setupIntroIcon(isDarkMode: isDarkMode)
    }
    
    func nextScene() {
        presenter.nextScene()
    }
    
    func later() {
        presenter.later()
    }
    
    func readTerms() {
        sendTermsEvent()
        presenter.readTerms()
    }
}

private extension FacialBiometricIntroViewModel {
    func sendScreenTrack() {
        let event = IdentityValidationEventType.screenViewed
        let properties: [IdentityValidationEventProperty] = [
            .init(name: .screenName, value: IdentityValidationSelfieEventProperty.intro),
            .init(name: .flow, value: flow)
        ]
        
        let tracker = IdentityValidationTracker(eventType: event, eventProperties: properties)
        dependencies.analytics.log(tracker)
    }
    
    func sendTermsEvent() {
        let event = IdentityValidationEventType.buttonClicked
        let properties: [IdentityValidationEventProperty] = [
            .init(name: .screenName, value: IdentityValidationSelfieEventProperty.intro),
            .init(name: .flow, value: flow),
            .init(name: .buttonName, value: IdentityValidationButtonNameProperty.termsOfUse)
        ]
        
        let tracker = IdentityValidationTracker(eventType: event, eventProperties: properties)
        dependencies.analytics.log(tracker)
    }
}
