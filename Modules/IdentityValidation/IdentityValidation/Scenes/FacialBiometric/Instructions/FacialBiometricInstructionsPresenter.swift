import AssetsKit
import Foundation

protocol FacialBiometricInstructionsPresenting: AnyObject {
    var display: FacialBiometricInstructionsDisplay? { get set }
    func nextScene()
    func later()
    func setupInstructions()
}

final class FacialBiometricInstructionsPresenter {
    private let coordinator: FacialBiometricInstructionsCoordinating
    weak var display: FacialBiometricInstructionsDisplay?
    
    init(coordinator: FacialBiometricInstructionsCoordinating) {
        self.coordinator = coordinator
    }
}

extension FacialBiometricInstructionsPresenter: FacialBiometricInstructionsPresenting {
    func setupInstructions() {
        let instructions = Strings.FacialBiometric.Instructions.self

        display?.displayTexts(title: instructions.title,
                              mainButtonTitle: Strings.General.takePhoto,
                              secondaryButtonTitle: Strings.General.later)

        display?.setInstructionRows([
            (Resources.Icons.icoLamp.image, instructions.first),
            (Resources.Icons.icoCapGlass.image, instructions.second),
            (Resources.Icons.icoFaceSmile.image, instructions.third)
        ])
    }
    
    func nextScene() {
        coordinator.perform(action: .nextScene)
    }

    func later() {
        coordinator.perform(action: .close)
    }
}
