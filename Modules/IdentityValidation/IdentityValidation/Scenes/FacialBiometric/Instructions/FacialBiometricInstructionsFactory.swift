import Foundation

enum FacialBiometricInstructionsFactory {
    static func make(delegate: FacialBiometricFlowCoordinating?, flow: String) -> FacialBiometricInstructionsViewController {
        let dependencyContainer = DependencyContainer()
        let coordinator = FacialBiometricInstructionsCoordinator(delegate: delegate)
        let presenter = FacialBiometricInstructionsPresenter(coordinator: coordinator)
        let viewModel = FacialBiometricInstructionsViewModel(presenter: presenter, dependencies: dependencyContainer, flow: flow)
        let viewController = FacialBiometricInstructionsViewController(viewModel: viewModel)

        presenter.display = viewController

        return viewController
    }
}
