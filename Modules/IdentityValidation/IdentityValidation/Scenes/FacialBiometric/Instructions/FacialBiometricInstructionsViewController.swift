// swiftlint:disable type_name
import UI
import UIKit

protocol FacialBiometricInstructionsDisplay: AnyObject {
    func displayTexts(title: String, mainButtonTitle: String, secondaryButtonTitle: String)
    func setInstructionRows(_ rows: [(UIImage, String)])
}

final class FacialBiometricInstructionsViewController: ViewController<FacialBiometricInstructionsViewModelInputs, IdentityInstructionsView> {
    fileprivate enum Layout { }

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.setupInstructions()
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        rootView.mainButtonAction = { [weak self] in self?.didTapNext() }
        rootView.secondaryButtonAction = { [weak self] in self?.didTapLater() }
    }
}

private extension FacialBiometricInstructionsViewController {
    func didTapNext() {
        viewModel.nextScene()
    }

    func didTapLater() {
        viewModel.later()
    }
}

extension FacialBiometricInstructionsViewController: FacialBiometricInstructionsDisplay {
    func displayTexts(title: String, mainButtonTitle: String, secondaryButtonTitle: String) {
        rootView.configureView(with: title, mainButtonTitle: mainButtonTitle, secondaryButtonTitle: secondaryButtonTitle)
    }

    func setInstructionRows(_ rows: [(UIImage, String)]) {
        rootView.addInstructionRows(rows)
    }
}
