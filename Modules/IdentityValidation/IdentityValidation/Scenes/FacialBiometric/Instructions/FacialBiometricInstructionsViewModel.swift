import Foundation

protocol FacialBiometricInstructionsViewModelInputs: AnyObject {
    func setupInstructions()
    func nextScene()
    func later()
}

final class FacialBiometricInstructionsViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let flow: String
    private let presenter: FacialBiometricInstructionsPresenting

    init(presenter: FacialBiometricInstructionsPresenting, dependencies: Dependencies, flow: String) {
        self.presenter = presenter
        self.dependencies = dependencies
        self.flow = flow
    }
}

extension FacialBiometricInstructionsViewModel: FacialBiometricInstructionsViewModelInputs {
    func setupInstructions() {
        sendScreenTrack()
        presenter.setupInstructions()
    }
    
    func nextScene() {
        presenter.nextScene()
    }

    func later() {
        presenter.later()
    }
}

private extension FacialBiometricInstructionsViewModel {
    func sendScreenTrack() {
        let event = IdentityValidationEventType.screenViewed
        let properties: [IdentityValidationEventProperty] = [
            .init(name: .screenName, value: IdentityValidationSelfieEventProperty.tips),
            .init(name: .flow, value: flow)
        ]
        
        let tracker = IdentityValidationTracker(eventType: event, eventProperties: properties)
        dependencies.analytics.log(tracker)
    }
}
