import Foundation

enum FacialBiometricInstructionsAction {
    case nextScene
    case close
}

protocol FacialBiometricInstructionsCoordinating: AnyObject {
    func perform(action: FacialBiometricInstructionsAction)
}

final class FacialBiometricInstructionsCoordinator {
    private weak var delegate: FacialBiometricFlowCoordinating?

    init(delegate: FacialBiometricFlowCoordinating?) {
        self.delegate = delegate
    }
}

extension FacialBiometricInstructionsCoordinator: FacialBiometricInstructionsCoordinating {
    func perform(action: FacialBiometricInstructionsAction) {
        switch action {
        case .nextScene:
            delegate?.showCamera()
        case .close:
            delegate?.close()
        }
    }
}
