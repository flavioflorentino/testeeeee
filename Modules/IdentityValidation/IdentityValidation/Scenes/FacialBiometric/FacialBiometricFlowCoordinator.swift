import UIKit
import UI

protocol FacialBiometricFlowCoordinating: AnyObject {
    func navigateToInstructionsScene()
    func navigateToTerms()
    func showCamera()
    func closeCamera()
    func finished(with nextStep: IdentityStep)
    func close()
}

final class FacialBiometricFlowCoordinator {
    private let navigationController: UINavigationController
    private let step: IdentityStep
    private let serviceToken: String
    private let flow: String
    private var cameraCoordinator: CoordinatorProtocol?
    private weak var coordinatorDelegate: IdentityValidationCoordinating?

    init(
        with navigationController: UINavigationController,
        serviceToken: String,
        flow: String,
        step: IdentityStep,
        coordinatorDelegate: IdentityValidationCoordinating?
    ) {
        self.navigationController = navigationController
        self.serviceToken = serviceToken
        self.flow = flow
        self.step = step
        self.coordinatorDelegate = coordinatorDelegate
    }
}

extension FacialBiometricFlowCoordinator: CoordinatorProtocol {
    func start() {
        let controller = FacialBiometricIntroFactory.make(step: step, flow: flow, delegate: self)
        navigationController.pushViewController(controller, animated: true)
    }
}

extension FacialBiometricFlowCoordinator: FacialBiometricFlowCoordinating {
    func navigateToInstructionsScene() {
        let instructionsController = FacialBiometricInstructionsFactory.make(delegate: self, flow: flow)
        navigationController.pushViewController(instructionsController, animated: true)
    }

    func navigateToTerms() {
        let termsController = TermsFactory.make()
        navigationController.pushViewController(termsController, animated: true)
    }

    func showCamera() {
        cameraCoordinator = CameraFlowCoordinator(
            serviceToken: serviceToken,
            flow: flow,
            type: .selfie,
            delegate: self,
            originController: navigationController
        )
        cameraCoordinator?.start()
    }

    func close() {
        coordinatorDelegate?.dismiss()
    }
}

extension FacialBiometricFlowCoordinator: CameraFlowCoordinatorDelegate {
    func closeCamera() {
        navigationController.dismiss(animated: true)
        cameraCoordinator = nil
    }

    func finished(with nextStep: IdentityStep) {
        navigationController.dismiss(animated: true) {
            self.coordinatorDelegate?.presentNextStep(nextStep)
        }
    }
}
