import Foundation

enum LoaderFactory {
    static func make(
        serviceToken: String,
        flow: String,
        coordinatorDelegate: IdentityValidationCoordinating?
    ) -> LoaderViewController {
        let container = DependencyContainer()
        let service = LoaderService(dependencies: container)
        let coordinator = LoaderCoordinator(coordinatorDelegate: coordinatorDelegate)
        let presenter = LoaderPresenter(coordinator: coordinator)
        let viewModel = LoaderViewModel(serviceToken: serviceToken, flow: flow, service: service, presenter: presenter)
        let viewController = LoaderViewController(viewModel: viewModel)

        presenter.viewController = viewController

        return viewController
    }
}
