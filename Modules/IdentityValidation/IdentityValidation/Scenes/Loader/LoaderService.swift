import Foundation
import Core

protocol LoaderServicing {
    func step(token: String, flow: String, _ completion: @escaping ((Result<IdentityStep, ApiError>) -> Void))
}

final class LoaderService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension LoaderService: LoaderServicing {
    func step(token: String, flow: String, _ completion: @escaping ((Result<IdentityStep, ApiError>) -> Void)) {
        let endpoint = stepEndpoint(token: token, flow: flow)
        let api = Api<IdentityStep>(endpoint: endpoint)
        
        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}

private extension LoaderService {
    func stepEndpoint(token: String, flow: String) -> IdentityValidationEndpoint {
        IdentityValidationEndpoint(
            path: "verifications/identity/step",
            method: .post,
            contentType: .applicationJson,
            customHeaders: [
                "Token": token,
                "flow": flow
            ],
            body: nil
        )
    }
}
