import Foundation
import UI

protocol LoaderDisplay: AnyObject {
    func showPopupController(with properties: PopupProperties)
}

final class LoaderViewController: ViewController<LoaderViewModelInputs, LoadingView>, LoaderDisplay {
    fileprivate enum Layout { }

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadStep()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    // MARK: - LoaderDisplay

    func showPopupController(with properties: PopupProperties) {
        showPopupController(
            with: properties,
            mainButtonAction: { [weak self] in
                self?.viewModel.loadStep()
            },
            secondaryButtonAction: { [weak self] in
                self?.viewModel.close()
            }
        )
    }
}
