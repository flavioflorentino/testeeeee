import Foundation
import Core

protocol LoaderViewModelInputs: AnyObject {
    func loadStep()
    func close()
}

final class LoaderViewModel: LoaderViewModelInputs {
    private let service: LoaderServicing
    private let presenter: LoaderPresenting
    private let serviceToken: String
    private let flow: String

    init(serviceToken: String, flow: String, service: LoaderServicing, presenter: LoaderPresenting) {
        self.serviceToken = serviceToken
        self.flow = flow
        self.service = service
        self.presenter = presenter
    }

    // MARK: - LoaderViewModelInputs

    func loadStep() {
        service.step(token: serviceToken, flow: flow) { [weak self] result in
            self?.handleStepResult(result)
        }
    }

    func close() {
        presenter.close()
    }
}

private extension LoaderViewModel {
    func handleStepResult(_ result: Result<IdentityStep, ApiError>) {
        switch result {
        case .success(let step):
            presenter.handleNextStep(step)

        case .failure(let error):
            presenter.presentError(message: error.requestError?.message)
        }
    }
}
