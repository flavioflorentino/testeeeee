import Foundation

enum LoaderAction {
    case nextStep(IdentityStep)
    case close
}

protocol LoaderCoordinating: AnyObject {
    func perform(action: LoaderAction)
}

final class LoaderCoordinator {
    private weak var coordinatorDelegate: IdentityValidationCoordinating?

    init(coordinatorDelegate: IdentityValidationCoordinating?) {
        self.coordinatorDelegate = coordinatorDelegate
    }
}

extension LoaderCoordinator: LoaderCoordinating {
    func perform(action: LoaderAction) {
        switch action {
        case .nextStep(let step):
            coordinatorDelegate?.presentNextStep(step)
        case .close:
            coordinatorDelegate?.dismiss()
        }
    }
}
