import Foundation
import UI

protocol LoaderPresenting: AnyObject {
    var viewController: LoaderDisplay? { get set }
    func handleNextStep(_ step: IdentityStep)
    func presentError(message: String?)
    func close()
}

final class LoaderPresenter {
    weak var viewController: LoaderDisplay?
    private let coordinator: LoaderCoordinating

    init(coordinator: LoaderCoordinating) {
        self.coordinator = coordinator
    }
}

extension LoaderPresenter: LoaderPresenting {
    func handleNextStep(_ step: IdentityStep) {
        coordinator.perform(action: .nextStep(step))
    }

    func presentError(message: String?) {
        let popupProperties = PopupProperties(type: .text,
                                              title: Strings.Loader.Error.title,
                                              message: message ?? Strings.Loader.Error.message,
                                              mainButtonTitle: Strings.General.tryAgain,
                                              secondaryButtonTitle: Strings.General.close)
        viewController?.showPopupController(with: popupProperties)
    }

    func close() {
        coordinator.perform(action: .close)
    }
}
