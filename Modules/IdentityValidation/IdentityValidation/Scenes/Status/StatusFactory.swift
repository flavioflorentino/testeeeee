import Foundation

enum StatusFactory {
    static func make(
        delegate: IdentityValidationCoordinating?,
        serviceToken: String,
        flow: String,
        step: IdentityStep
    ) -> StatusViewController {
        let container = DependencyContainer()
        let service = StatusService(dependencies: container)
        let coordinator = StatusCoordinator(coordinatorDelegate: delegate)
        let presenter = StatusPresenter(coordinator: coordinator, step: step)
        let viewModel = StatusViewModel(
            service: service,
            presenter: presenter,
            serviceToken: serviceToken,
            flow: flow,
            step: step,
            dependencies: container
        )
        let viewController = StatusViewController(viewModel: viewModel)

        presenter.viewController = viewController

        return viewController
    }
}
