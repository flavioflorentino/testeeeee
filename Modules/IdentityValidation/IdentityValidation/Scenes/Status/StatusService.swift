import Foundation
import Core

protocol StatusServicing {
    func step(token: String, flow: String, _ completion: @escaping ((Result<IdentityStep, ApiError>) -> Void))
    func waitForTime(_ time: Int, completion: @escaping () -> Void)
}

final class StatusService: StatusServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    func step(token: String, flow: String, _ completion: @escaping ((Result<IdentityStep, ApiError>) -> Void)) {
        let endpoint = stepEndpoint(token: token, flow: flow)
        let api = Api<IdentityStep>(endpoint: endpoint)
        
        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func waitForTime(_ time: Int, completion: @escaping () -> Void) {
        dependencies.mainQueue.asyncAfter(deadline: DispatchTime.now() + TimeInterval(time), execute: completion)
    }
}

private extension StatusService {
    func stepEndpoint(token: String, flow: String) -> IdentityValidationEndpoint {
        IdentityValidationEndpoint(
            path: "verifications/identity/step/on-hold",
            method: .post,
            contentType: .applicationJson,
            customHeaders: [
                "Token": token,
                "flow": flow
            ],
            body: nil
        )
    }
}
