import Foundation
import UI

protocol StatusPresenting: AnyObject {
    var viewController: StatusDisplay? { get set }
    func setupTexts()
    func presentLoadingButton(_ isLoading: Bool)
    func setupIcon(isDarkMode: Bool)
    func dismissWithCurrentStep()
    func dismissWithNextStep(_ nextStep: IdentityStep)
    func startLoading()
    func stopLoading()
    func presentError(message: String?)
    func presentNextStep(_ step: IdentityStep)
}

final class StatusPresenter: StatusPresenting {
    private let coordinator: StatusCoordinating
    private let step: IdentityStep
    weak var viewController: StatusDisplay?

    init(coordinator: StatusCoordinating, step: IdentityStep) {
        self.coordinator = coordinator
        self.step = step
    }

    func setupTexts() {
        let buttonTitle = step.type == .onHold ? Strings.Status.Button.continue : Strings.Status.Button.ok
        viewController?.displayTexts(title: step.label, description: step.detail, buttonTitle: buttonTitle)
    }
    
    func presentLoadingButton(_ isLoading: Bool) {
        let buttonTitle = isLoading ? Strings.Status.Button.verifying : Strings.Status.Button.continue
        viewController?.displayLoadingButton(isLoading, text: buttonTitle)
    }

    func setupIcon(isDarkMode: Bool) {
        let iconStringUrl = isDarkMode ? step.icons.darkMode : step.icons.lightMode
        viewController?.displayIcon(iconUrl: URL(string: iconStringUrl))
    }

    func dismissWithCurrentStep() {
        coordinator.perform(action: .dismiss(step.status))
    }

    func dismissWithNextStep(_ nextStep: IdentityStep) {
        coordinator.perform(action: .dismiss(nextStep.status))
    }

    func startLoading() {
        viewController?.startLoadingView()
    }

    func stopLoading() {
        viewController?.stopLoadingView()
    }

    func presentError(message: String?) {
        viewController?.showError(createPopupController(message: message))
    }
    
    func presentNextStep(_ step: IdentityStep) {
        coordinator.perform(action: .nextStep(step))
    }
}

private extension StatusPresenter {
    func createPopupController(message: String?) -> PopupViewController {
        let popupController = PopupViewController(
            title: Strings.Status.Error.title,
            description: message ?? Strings.Status.Error.message,
            preferredType: .text
        )

        let tryAgainAction = PopupAction(title: Strings.General.tryAgain, style: .fill) { [weak self, weak popupController] in
            popupController?.dismiss(animated: true) {
                self?.viewController?.retryNextStep()
            }
        }

        let closeAction = PopupAction(title: Strings.General.close, style: .link) { [weak self, weak popupController] in
            popupController?.dismiss(animated: true) {
                self?.coordinator.perform(action: .dismiss(self?.step.status))
            }
        }

        popupController.addAction(tryAgainAction)
        popupController.addAction(closeAction)

        return popupController
    }
}
