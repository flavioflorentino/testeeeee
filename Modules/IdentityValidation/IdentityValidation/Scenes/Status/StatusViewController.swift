import UI
import UIKit

protocol StatusDisplay: AnyObject, LoadingViewProtocol {
    func displayIcon(iconUrl: URL?)
    func displayTexts(title: String, description: String, buttonTitle: String)
    func showError(_ controller: PopupViewController)
    func retryNextStep()
    func displayLoadingButton(_ isLoading: Bool, text: String)
}

private extension StatusViewController.Layout {
    enum Alpha {
        static let loadingBackground: CGFloat = 0.75
    }

    enum Size {
        static let image: CGFloat = 130
    }
}

final class StatusViewController: ViewController<StatusViewModelInputs, UIView>, StatusDisplay {
    fileprivate enum Layout { }

    private var isUserInterfaceStyleDark: Bool {
        if #available(iOS 12.0, *) {
            return traitCollection.userInterfaceStyle == .dark
        }
        return false
    }

    private lazy var infoView: IdentityInfoView = {
        let infoView = IdentityInfoView()
        infoView.mainButtonAction = { [weak self] in self?.didTapContinue() }
        return infoView
    }()

    lazy var loadingView: LoadingView = {
        let loadingView = LoadingView()
        loadingView.backgroundColor = Colors.black.lightColor.withAlphaComponent(Layout.Alpha.loadingBackground)
        return loadingView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadInfo()
    }

    override func buildViewHierarchy() {
        view.addSubview(infoView)
    }

    override func setupConstraints() {
        infoView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base04)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        viewModel.setupIcon(isDarkMode: isUserInterfaceStyleDark)
    }

    // MARK: - StatusDisplay

    func displayIcon(iconUrl: URL?) {
        infoView.setIconImage(from: iconUrl)
    }

    func displayTexts(title: String, description: String, buttonTitle: String) {
        infoView.configureView(with: title,
                               description: description,
                               buttonTitle: buttonTitle)
    }

    func showError(_ controller: PopupViewController) {
        showPopup(controller)
    }

    func retryNextStep() {
        viewModel.retryNextStep()
    }
    
    func displayLoadingButton(_ isLoading: Bool, text: String) {
        infoView.showLoadingButton(isLoading, text: text)
    }
}

@objc
private extension StatusViewController {
    func didTapContinue() {
        viewModel.handleButtonTap()
    }
}
