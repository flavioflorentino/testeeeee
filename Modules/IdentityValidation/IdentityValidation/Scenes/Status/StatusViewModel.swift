import AnalyticsModule
import Foundation

protocol StatusViewModelInputs: AnyObject {
    func loadInfo()
    func setupIcon(isDarkMode: Bool)
    func handleButtonTap()
    func retryNextStep()
}

final class StatusViewModel: StatusViewModelInputs {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let service: StatusServicing
    private let presenter: StatusPresenting
    private let step: IdentityStep
    private let serviceToken: String
    private let flow: String

    init(
        service: StatusServicing,
        presenter: StatusPresenting,
        serviceToken: String,
        flow: String,
        step: IdentityStep,
        dependencies: Dependencies
    ) {
        self.service = service
        self.presenter = presenter
        self.step = step
        self.serviceToken = serviceToken
        self.flow = flow
        self.dependencies = dependencies
    }
    
    func loadInfo() {
        trackEvent(step: step.type)
        presenter.setupTexts()
        
        guard
            step.type == .onHold,
            let waitingTime = step.waitTime,
            waitingTime > 0 else {
            return
        }
        
        presenter.presentLoadingButton(true)
        service.waitForTime(waitingTime) { [weak self] in
            self?.presenter.presentLoadingButton(false)
        }
    }

    func setupIcon(isDarkMode: Bool) {
        presenter.setupIcon(isDarkMode: isDarkMode)
    }

    func handleButtonTap() {
        guard step.type != .onHold else {
            getNextStep()
            return
        }

        presenter.dismissWithCurrentStep()
    }

    func retryNextStep() {
        getNextStep()
    }
}

private extension StatusViewModel {
    func getNextStep() {
        presenter.startLoading()

        service.step(token: serviceToken, flow: flow) { [weak self] result in
            self?.presenter.stopLoading()
            
            switch result {
            case .success(let step):
                self?.verifyNextStep(step)

            case .failure(let error):
                self?.presenter.presentError(message: error.requestError?.message)
            }
        }
    }
    
    func verifyNextStep(_ step: IdentityStep) {
        switch step.type {
        case .done, .onHold, .processing, .alternative:
            presenter.dismissWithNextStep(step)
        default:
            presenter.presentNextStep(step)
        }
    }
    
    func trackEvent(step: IdentityStepType) {
        switch step {
        case .done:
            sendScreenTrack(with: .done)
        case .processing:
            sendScreenTrack(with: .processing)
        case .onHold:
            sendScreenTrack(with: .onHold)
        default:
            break
        }
    }
    
    func sendScreenTrack(with status: IdentityValidationStatusEventProperty) {
        let event = IdentityValidationEventType.screenViewed
        let properties: [IdentityValidationEventProperty] = [
            .init(name: .screenName, value: status),
            .init(name: .flow, value: flow)
        ]
        
        let tracker = IdentityValidationTracker(eventType: event, eventProperties: properties)
        dependencies.analytics.log(tracker)
    }
}
