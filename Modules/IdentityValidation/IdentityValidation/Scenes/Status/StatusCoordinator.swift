import Foundation

enum StatusAction {
    case dismiss(IDValidationStatus?)
    case nextStep(_ step: IdentityStep)
}

protocol StatusCoordinating: AnyObject {
    func perform(action: StatusAction)
}

final class StatusCoordinator: StatusCoordinating {
    private weak var coordinatorDelegate: IdentityValidationCoordinating?

    init(coordinatorDelegate: IdentityValidationCoordinating?) {
        self.coordinatorDelegate = coordinatorDelegate
    }

    func perform(action: StatusAction) {
        switch action {
        case let .dismiss(status):
            coordinatorDelegate?.endWithStatus(status)
        case let .nextStep(step):
            coordinatorDelegate?.presentNextStep(step)
        }
    }
}
