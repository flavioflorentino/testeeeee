import UI
import UIKit

protocol DocumentCaptureFlowCoordinating: AnyObject {
    func navigateToInstructions()
    func showCamera()
    func close()
}

final class DocumentCaptureFlowCoordinator: CoordinatorProtocol, DocumentCaptureFlowCoordinating {
    private let navigationController: UINavigationController
    private var step: IdentityStep
    private let serviceToken: String
    private let flow: String
    private var cameraCoordinator: CoordinatorProtocol?
    private weak var coordinatorDelegate: IdentityValidationCoordinating?

    init(
        with navigationController: UINavigationController,
        serviceToken: String,
        flow: String,
        step: IdentityStep,
        coordinatorDelegate: IdentityValidationCoordinating?
    ) {
        self.navigationController = navigationController
        self.serviceToken = serviceToken
        self.flow = flow
        self.step = step
        self.coordinatorDelegate = coordinatorDelegate
    }

    func start() {
        let controller = DocumentCaptureIntroFactory.make(step: step, delegate: self, flow: flow)
        navigationController.pushViewController(controller, animated: true)
    }

    // MARK: - DocumentCaptureFlowCoordinating

    func navigateToInstructions() {
        let controller = DocumentCaptureInstructionsFactory.make(delegate: self, flow: flow)
        navigationController.pushViewController(controller, animated: true)
    }

    func showCamera() {
        let cameraType: CameraType = step.type == .documentFront ? .documentFront : .documentBack
        cameraCoordinator = CameraFlowCoordinator(
            serviceToken: serviceToken,
            flow: flow,
            type: cameraType,
            delegate: self,
            originController: navigationController
        )
        cameraCoordinator?.start()
    }

    func close() {
        coordinatorDelegate?.dismiss()
    }
}

extension DocumentCaptureFlowCoordinator: CameraFlowCoordinatorDelegate {
    func closeCamera() {
        navigationController.dismiss(animated: true)
        cameraCoordinator = nil
    }

    func finished(with nextStep: IdentityStep) {
        navigationController.dismiss(animated: true) {
            self.shouldShowCameraOrDismiss(nextStep)
        }
    }
}

private extension DocumentCaptureFlowCoordinator {
    func shouldShowCameraOrDismiss(_ nextStep: IdentityStep) {
        if nextStep.type == .documentBack {
            step = nextStep
            showCamera()
        } else {
            coordinatorDelegate?.presentNextStep(nextStep)
        }
    }
}
