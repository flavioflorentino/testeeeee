import UI
import UIKit

protocol DocumentCaptureIntroDisplaying: AnyObject {
    func displayIcon(iconUrl: URL?, fallbackIcon: UIImage)
    func displayTexts(title: String, description: String, buttonTitle: String)
}

final class DocumentCaptureIntroViewController: ViewController<DocumentCaptureIntroInteracting, UIView>, DocumentCaptureIntroDisplaying {
    private var isUserInterfaceStyleDark: Bool {
        if #available(iOS 12.0, *) {
            return traitCollection.userInterfaceStyle == .dark
        }
        return false
    }

    private lazy var infoView: IdentityInfoView = {
        let infoView = IdentityInfoView()
        infoView.mainButtonAction = { [weak self] in self?.didTapNext() }
        return infoView
    }()

    private lazy var laterButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Strings.General.later, for: .normal)
        button.addTarget(self, action: #selector(didTapLater), for: .touchUpInside)
        button.buttonStyle(LinkButtonStyle())
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.setupTexts()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        interactor.setupIcon(isDarkMode: isUserInterfaceStyleDark)
    }

    override func buildViewHierarchy() {
        view.addSubview(infoView)
        view.addSubview(laterButton)
    }

    override func setupConstraints() {
        infoView.snp.makeConstraints {
            $0.centerY.equalToSuperview().offset(-Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        laterButton.snp.makeConstraints {
            $0.top.equalTo(infoView.snp.bottom).offset(Spacing.base01)
            $0.centerX.equalToSuperview()
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }

    // MARK: - DocumentCaptureIntroDisplaying

    func displayIcon(iconUrl: URL?, fallbackIcon: UIImage) {
        infoView.setIconImage(from: iconUrl, fallbackIcon: fallbackIcon)
    }

    func displayTexts(title: String, description: String, buttonTitle: String) {
        infoView.configureView(with: title, description: description, buttonTitle: buttonTitle)
    }
}

private extension DocumentCaptureIntroViewController {
    func didTapNext() {
        interactor.nextScene()
    }

    @objc
    func didTapLater() {
        interactor.later()
    }
}
