import Foundation

enum DocumentCaptureIntroFactory {
    static func make(
        step: IdentityStep,
        delegate: DocumentCaptureFlowCoordinating?,
        flow: String
    ) -> DocumentCaptureIntroViewController {
        let dependencyContainer = DependencyContainer()
        let coordinator = DocumentCaptureIntroCoordinator(delegate: delegate)
        let presenter = DocumentCaptureIntroPresenter(coordinator: coordinator, step: step)
        let interactor = DocumentCaptureIntroInteractor(presenter: presenter, dependencies: dependencyContainer, flow: flow)
        let viewController = DocumentCaptureIntroViewController(interactor: interactor)

        presenter.viewController = viewController

        return viewController
    }
}
