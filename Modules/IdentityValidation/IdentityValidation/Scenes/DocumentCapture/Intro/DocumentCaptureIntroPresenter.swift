import AssetsKit
import Foundation

protocol DocumentCaptureIntroPresenting: AnyObject {
    var viewController: DocumentCaptureIntroDisplaying? { get set }
    func setupIntroIcon(isDarkMode: Bool)
    func setupTexts()
    func nextScene()
    func later()
}

final class DocumentCaptureIntroPresenter: DocumentCaptureIntroPresenting {
    private let coordinator: DocumentCaptureIntroCoordinating
    private let step: IdentityStep
    weak var viewController: DocumentCaptureIntroDisplaying?

    init(coordinator: DocumentCaptureIntroCoordinating, step: IdentityStep) {
        self.coordinator = coordinator
        self.step = step
    }

    // MARK: - DocumentCaptureIntroPresenting

    func setupIntroIcon(isDarkMode: Bool) {
        let iconStringUrl = isDarkMode ? step.icons.darkMode : step.icons.lightMode
        viewController?.displayIcon(iconUrl: URL(string: iconStringUrl),
                                    fallbackIcon: Resources.Illustrations.iluDoc.image)
    }

    func setupTexts() {
        viewController?.displayTexts(title: step.label,
                                     description: step.detail,
                                     buttonTitle: Strings.General.advance)
    }

    func nextScene() {
        coordinator.perform(action: .instructions)
    }

    func later() {
        coordinator.perform(action: .close)
    }
}
