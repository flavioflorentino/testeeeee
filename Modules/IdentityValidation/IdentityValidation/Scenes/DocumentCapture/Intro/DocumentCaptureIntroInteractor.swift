import Foundation

protocol DocumentCaptureIntroInteracting: AnyObject {
    func setupTexts()
    func setupIcon(isDarkMode: Bool)
    func nextScene()
    func later()
}

final class DocumentCaptureIntroInteractor: DocumentCaptureIntroInteracting {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let flow: String
    private let presenter: DocumentCaptureIntroPresenting

    init(presenter: DocumentCaptureIntroPresenting, dependencies: Dependencies, flow: String) {
        self.presenter = presenter
        self.dependencies = dependencies
        self.flow = flow
    }

    // MARK: - DocumentCaptureIntroInteracting

    func setupTexts() {
        sendScreenTrack()
        presenter.setupTexts()
    }

    func setupIcon(isDarkMode: Bool) {
        presenter.setupIntroIcon(isDarkMode: isDarkMode)
    }

    func nextScene() {
        presenter.nextScene()
    }

    func later() {
        presenter.later()
    }
}

private extension DocumentCaptureIntroInteractor {
    func sendScreenTrack() {
        let event = IdentityValidationEventType.screenViewed
        let properties: [IdentityValidationEventProperty] = [
            .init(name: .screenName, value: IdentityValidationDocumentEventProperty.intro),
            .init(name: .flow, value: flow)
        ]
        
        let tracker = IdentityValidationTracker(eventType: event, eventProperties: properties)
        dependencies.analytics.log(tracker)
    }
}
