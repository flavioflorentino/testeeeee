import Foundation

enum DocumentCaptureIntroAction {
    case instructions
    case close
}

protocol DocumentCaptureIntroCoordinating: AnyObject {
    func perform(action: DocumentCaptureIntroAction)
}

final class DocumentCaptureIntroCoordinator: DocumentCaptureIntroCoordinating {
    private weak var delegate: DocumentCaptureFlowCoordinating?

    init(delegate: DocumentCaptureFlowCoordinating?) {
        self.delegate = delegate
    }

    func perform(action: DocumentCaptureIntroAction) {
        switch action {
        case .instructions:
            delegate?.navigateToInstructions()
        case .close:
            delegate?.close()
        }
    }
}
