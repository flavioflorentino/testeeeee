import Foundation

protocol DocumentCaptureInstructionsInteracting: AnyObject {
    func setupInstructions()
    func nextScene()
    func later()
}

final class DocumentCaptureInstructionsInteractor: DocumentCaptureInstructionsInteracting {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let flow: String
    private let presenter: DocumentCaptureInstructionsPresenting

    init(presenter: DocumentCaptureInstructionsPresenting, dependencies: Dependencies, flow: String) {
        self.presenter = presenter
        self.dependencies = dependencies
        self.flow = flow
    }

    // MARK: - DocumentCaptureInstructionsInteracting

    func setupInstructions() {
        sendScreenTrack()
        presenter.setupInstructions()
    }

    func nextScene() {
        presenter.nextScene()
    }

    func later() {
        presenter.later()
    }
}

private extension DocumentCaptureInstructionsInteractor {
    func sendScreenTrack() {
        let event = IdentityValidationEventType.screenViewed
        let properties: [IdentityValidationEventProperty] = [
            .init(name: .screenName, value: IdentityValidationDocumentEventProperty.tips),
            .init(name: .flow, value: flow)
        ]
        
        let tracker = IdentityValidationTracker(eventType: event, eventProperties: properties)
        dependencies.analytics.log(tracker)
    }
}
