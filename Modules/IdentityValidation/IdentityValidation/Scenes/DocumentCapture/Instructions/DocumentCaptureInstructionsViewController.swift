// swiftlint:disable type_name
import UI
import UIKit

protocol DocumentCaptureInstructionsDisplaying: AnyObject {
    func displayTexts(title: String, mainButtonTitle: String, secondaryButtonTitle: String)
    func setInstructionRows(_ rows: [(UIImage, String)])
}

final class DocumentCaptureInstructionsViewController: ViewController<DocumentCaptureInstructionsInteracting, IdentityInstructionsView> {
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.setupInstructions()
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        rootView.mainButtonAction = { [weak self] in self?.didTapNext() }
        rootView.secondaryButtonAction = { [weak self] in self?.didTapLater() }
    }
}

extension DocumentCaptureInstructionsViewController: DocumentCaptureInstructionsDisplaying {
    func displayTexts(title: String, mainButtonTitle: String, secondaryButtonTitle: String) {
        rootView.configureView(with: title, mainButtonTitle: mainButtonTitle, secondaryButtonTitle: secondaryButtonTitle)
    }

    func setInstructionRows(_ rows: [(UIImage, String)]) {
        rootView.addInstructionRows(rows)
    }
}

private extension DocumentCaptureInstructionsViewController {
    func didTapNext() {
        interactor.nextScene()
    }

    func didTapLater() {
        interactor.later()
    }
}
