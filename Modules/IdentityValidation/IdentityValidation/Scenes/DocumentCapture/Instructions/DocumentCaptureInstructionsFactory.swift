import Foundation

enum DocumentCaptureInstructionsFactory {
    static func make(delegate: DocumentCaptureFlowCoordinating?, flow: String) -> DocumentCaptureInstructionsViewController {
        let dependencyContainer = DependencyContainer()
        let coordinator = DocumentCaptureInstructionsCoordinator(delegate: delegate)
        let presenter = DocumentCaptureInstructionsPresenter(coordinator: coordinator)
        let interactor = DocumentCaptureInstructionsInteractor(presenter: presenter, dependencies: dependencyContainer, flow: flow)
        let viewController = DocumentCaptureInstructionsViewController(interactor: interactor)

        presenter.viewController = viewController

        return viewController
    }
}
