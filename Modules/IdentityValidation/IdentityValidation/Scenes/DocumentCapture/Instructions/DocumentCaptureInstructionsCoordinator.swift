import Foundation

enum DocumentCaptureInstructionsAction {
    case nextScene
    case close
}

protocol DocumentCaptureInstructionsCoordinating: AnyObject {
    func perform(action: DocumentCaptureInstructionsAction)
}

final class DocumentCaptureInstructionsCoordinator: DocumentCaptureInstructionsCoordinating {
    private weak var delegate: DocumentCaptureFlowCoordinating?

    init(delegate: DocumentCaptureFlowCoordinating?) {
        self.delegate = delegate
    }

    func perform(action: DocumentCaptureInstructionsAction) {
        switch action {
        case .nextScene:
            delegate?.showCamera()
        case .close:
            delegate?.close()
        }
    }
}
