import AssetsKit
import Foundation

protocol DocumentCaptureInstructionsPresenting: AnyObject {
    var viewController: DocumentCaptureInstructionsDisplaying? { get set }
    func setupInstructions()
    func nextScene()
    func later()
}

final class DocumentCaptureInstructionsPresenter: DocumentCaptureInstructionsPresenting {
    private let coordinator: DocumentCaptureInstructionsCoordinating
    weak var viewController: DocumentCaptureInstructionsDisplaying?

    init(coordinator: DocumentCaptureInstructionsCoordinating) {
        self.coordinator = coordinator
    }

    // MARK: - DocumentCaptureInstructionsPresenting

    func setupInstructions() {
        let instructions = Strings.DocumentCapture.Instructions.self

        viewController?.displayTexts(title: instructions.title,
                                     mainButtonTitle: Strings.General.takePhoto,
                                     secondaryButtonTitle: Strings.General.later)

        viewController?.setInstructionRows([
            (Resources.Icons.icoDocument.image, instructions.first),
            (Resources.Icons.icoPhoto.image, instructions.second),
            (Resources.Icons.icoSearch.image, instructions.third)
        ])
    }

    func nextScene() {
        coordinator.perform(action: .nextScene)
    }

    func later() {
        coordinator.perform(action: .close)
    }
}
