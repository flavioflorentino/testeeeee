import UIKit

protocol CameraMaskHandler {
    func addMask(in view: UIView, maskView: UIView)
}
