import UI
import UIKit

private extension DocumentMaskHandler.Layout {
    enum Dimensions {
        static let cornerLine: CGFloat = UIScreen.main.bounds.width * 0.309_33
        static let cornerLineOffset: CGFloat = 3
        static let lineWidth: CGFloat = 6
    }
}

struct DocumentMaskHandler: CameraMaskHandler {
    fileprivate enum Layout {}

    func addMask(in view: UIView, maskView: UIView) {
        guard view.layer.sublayers?.contains(where: { $0 is CAShapeLayer }) == false else {
            return
        }
        view.layoutIfNeeded()

        let overlayPath = UIBezierPath(rect: view.bounds)
        let maskPath = UIBezierPath(rect: maskView.frame)
        overlayPath.append(maskPath)
        overlayPath.usesEvenOddFillRule = true

        let fillLayer = CAShapeLayer()
        fillLayer.path = overlayPath.cgPath
        fillLayer.fillRule = .evenOdd
        fillLayer.fillColor = Colors.black.lightColor.withAlphaComponent(0.7).cgColor

        fillLayer.addSublayer(topLeftLayer(forPath: maskPath))
        fillLayer.addSublayer(topRightLayer(forPath: maskPath))
        fillLayer.addSublayer(bottomLeftLayer(forPath: maskPath))
        fillLayer.addSublayer(bottomRightLayer(forPath: maskPath))

        let position: UInt32 = 1
        view.layer.insertSublayer(fillLayer, at: position)
    }

    private func topLeftLayer(forPath path: UIBezierPath) -> CAShapeLayer {
        let point = CGPoint(x: path.bounds.minX, y: path.bounds.minY)
        let topLeftPath = UIBezierPath()

        topLeftPath.move(to: point)
        topLeftPath.addLine(to: CGPoint(x: point.x + Layout.Dimensions.cornerLine, y: point.y))
        topLeftPath.move(to: CGPoint(x: point.x, y: point.y - Layout.Dimensions.cornerLineOffset))
        topLeftPath.addLine(to: CGPoint(x: point.x, y: point.y + Layout.Dimensions.cornerLine))
        return shapeLayer(forPath: topLeftPath)
    }

    private func topRightLayer(forPath path: UIBezierPath) -> CAShapeLayer {
        let point = CGPoint(x: path.bounds.maxX, y: path.bounds.minY)
        let topRightPath = UIBezierPath()

        topRightPath.move(to: point)
        topRightPath.addLine(to: CGPoint(x: point.x - Layout.Dimensions.cornerLine, y: point.y))
        topRightPath.move(to: CGPoint(x: point.x, y: point.y - Layout.Dimensions.cornerLineOffset))
        topRightPath.addLine(to: CGPoint(x: point.x, y: point.y + Layout.Dimensions.cornerLine))
        return shapeLayer(forPath: topRightPath)
    }

    private func bottomLeftLayer(forPath path: UIBezierPath) -> CAShapeLayer {
        let point = CGPoint(x: path.bounds.minX, y: path.bounds.maxY)
        let bottomLeftPath = UIBezierPath()

        bottomLeftPath.move(to: point)
        bottomLeftPath.addLine(to: CGPoint(x: point.x + Layout.Dimensions.cornerLine, y: point.y))
        bottomLeftPath.move(to: CGPoint(x: point.x, y: point.y + Layout.Dimensions.cornerLineOffset))
        bottomLeftPath.addLine(to: CGPoint(x: point.x, y: point.y - Layout.Dimensions.cornerLine))
        return shapeLayer(forPath: bottomLeftPath)
    }

    private func bottomRightLayer(forPath path: UIBezierPath) -> CAShapeLayer {
        let point = CGPoint(x: path.bounds.maxX, y: path.bounds.maxY)
        let bottomRightPath = UIBezierPath()

        bottomRightPath.move(to: point)
        bottomRightPath.addLine(to: CGPoint(x: point.x - Layout.Dimensions.cornerLine, y: point.y))
        bottomRightPath.move(to: CGPoint(x: point.x, y: point.y + Layout.Dimensions.cornerLineOffset))
        bottomRightPath.addLine(to: CGPoint(x: point.x, y: point.y - Layout.Dimensions.cornerLine))
        return shapeLayer(forPath: bottomRightPath)
    }

    private func shapeLayer(forPath path: UIBezierPath) -> CAShapeLayer {
        let corner = CAShapeLayer()
        corner.strokeColor = Colors.success400.color.cgColor
        corner.path = path.cgPath
        corner.lineWidth = Layout.Dimensions.lineWidth
        corner.fillColor = UIColor.clear.cgColor
        return corner
    }
}
