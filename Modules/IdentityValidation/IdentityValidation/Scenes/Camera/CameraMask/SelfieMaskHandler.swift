import UI
import UIKit

private extension SelfieMaskHandler.Layout {
    enum Ratio {
        static let maskHeight: CGFloat = 0.563_37
    }
}

struct SelfieMaskHandler: CameraMaskHandler {
    fileprivate enum Layout {}

    func addMask(in view: UIView, maskView: UIView) {
        guard view.layer.sublayers?.contains(where: { $0 is CAShapeLayer }) == false else {
            return
        }
        view.layoutIfNeeded()

        let ovalHeight = view.frame.height * Layout.Ratio.maskHeight
        let ovalRect = CGRect(origin: maskView.frame.origin,
                              size: CGSize(width: maskView.frame.width,
                                           height: ovalHeight))

        let maskPath = UIBezierPath(ovalIn: ovalRect)
        let overlayPath = UIBezierPath(rect: view.bounds)
        overlayPath.append(maskPath)
        overlayPath.usesEvenOddFillRule = true

        let position: UInt32 = 1

        let fillLayer = CAShapeLayer()
        fillLayer.path = overlayPath.cgPath
        fillLayer.fillRule = .evenOdd
        fillLayer.fillColor = Colors.black.lightColor.withAlphaComponent(0.7).cgColor
        view.layer.insertSublayer(fillLayer, at: position)
    }
}
