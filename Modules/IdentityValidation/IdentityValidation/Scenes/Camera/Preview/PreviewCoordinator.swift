import Foundation

enum PreviewAction {
    case retakePicture
    case nextStep(IdentityStep)
    case close
}

protocol PreviewCoordinating: AnyObject {
    func perform(action: PreviewAction)
}

final class PreviewCoordinator {
    private weak var delegate: CameraFlowCoordinating?

    init(delegate: CameraFlowCoordinating?) {
        self.delegate = delegate
    }
}

extension PreviewCoordinator: PreviewCoordinating {
    func perform(action: PreviewAction) {
        switch action {
        case .retakePicture:
            delegate?.returnToCamera()

        case .nextStep(let step):
            delegate?.finished(with: step)

        case .close:
            delegate?.close()
        }
    }
}
