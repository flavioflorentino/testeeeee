import UIKit
import Foundation
import UIKit

protocol PreviewPresenting: AnyObject {
    var viewController: PreviewDisplay? { get set }
    func previewImage(_ image: UIImage)
    func displayTexts(for type: CameraType)
    func retakePicture()
    func displayError(message: String?)
    func setLoadingState(_ loading: Bool)
    func nextStep(_ step: IdentityStep)
    func close()
}

final class PreviewPresenter {
    private let coordinator: PreviewCoordinating
    weak var viewController: PreviewDisplay?

    init(coordinator: PreviewCoordinating) {
        self.coordinator = coordinator
    }
}

extension PreviewPresenter: PreviewPresenting {
    func previewImage(_ image: UIImage) {
        viewController?.displayPreviewImage(image)
    }

    func displayTexts(for type: CameraType) {
        let infoText: String

        switch type {
        case .selfie:
            infoText = Strings.Preview.Selfie.info
        case .documentFront:
            infoText = Strings.Preview.Document.Front.info
        case .documentBack:
            infoText = Strings.Preview.Document.Back.info
        }

        viewController?.setupConfirmationView(
            infoText: infoText,
            retakeButtonTitle: Strings.Preview.Button.Retake.text,
            acceptButtonTitle: Strings.Preview.Button.Accept.text
        )

        viewController?.setupUploadErrorView(tryAgainButtonTitle: Strings.Preview.Button.Retake.text)
    }

    func retakePicture() {
        coordinator.perform(action: .retakePicture)
    }

    func displayError(message: String?) {
        viewController?.displayError(message ?? Strings.Error.message)
    }

    func setLoadingState(_ loading: Bool) {
        if loading {
            self.viewController?.startLoadingView()
        } else {
            self.viewController?.stopLoadingView()
        }
    }

    func nextStep(_ step: IdentityStep) {
        coordinator.perform(action: .nextStep(step))
    }

    func close() {
        coordinator.perform(action: .close)
    }
}
