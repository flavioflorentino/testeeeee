import AssetsKit
import UI
import UIKit

protocol PreviewDisplay: AnyObject, LoadingViewProtocol {
    func displayPreviewImage(_ image: UIImage)
    func setupConfirmationView(infoText: String, retakeButtonTitle: String, acceptButtonTitle: String)
    func setupUploadErrorView(tryAgainButtonTitle: String)
    func displayError(_ message: String)
}

private extension PreviewViewController.Layout {
    enum Alpha {
        static let loadingBackground: CGFloat = 0.75
    }

    enum Size {
        static let closeButton: CGFloat = 32
    }
}

final class PreviewViewController: ViewController<PreviewViewModelInputs, UIView> {
    fileprivate enum Layout { }

    private lazy var previewImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()

    private lazy var closeButton: UIButton = {
        let button = UIButton()
        button.setImage(Resources.Icons.icoClose.image, for: .normal)
        button.addTarget(self, action: #selector(didTapClose), for: .touchUpInside)
        button.accessibilityLabel = Strings.General.Button.Close.Accessibility.text
        return button
    }()

    private lazy var confirmationView: PreviewConfirmationView = {
        let confirmationView = PreviewConfirmationView()
        confirmationView.delegate = self
        return confirmationView
    }()

    private lazy var uploadErrorView: PreviewUploadErrorView = {
        let errorView = PreviewUploadErrorView()
        errorView.delegate = self
        return errorView
    }()

    private lazy var messageErrorView = PreviewMessageErrorView()

    lazy var loadingView: LoadingView = {
        let loadingView = LoadingView()
        loadingView.setTextColor(Colors.white.lightColor)
        loadingView.backgroundColor = Colors.black.lightColor.withAlphaComponent(Layout.Alpha.loadingBackground)
        return loadingView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.setupPreview()
    }

    override func buildViewHierarchy() {
        view.addSubview(previewImageView)
        view.addSubview(closeButton)
        view.addSubview(confirmationView)
        view.addSubview(uploadErrorView)
        view.addSubview(messageErrorView)
    }
    
    override func setupConstraints() {
        previewImageView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        closeButton.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top).offset(Spacing.base03)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.size.equalTo(Layout.Size.closeButton)
        }

        confirmationView.snp.makeConstraints {
            $0.leading.trailing.bottom.equalToSuperview()
        }

        uploadErrorView.snp.makeConstraints {
            $0.leading.trailing.bottom.equalToSuperview()
        }

        messageErrorView.snp.makeConstraints {
            $0.top.equalTo(closeButton.snp.bottom).offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base03)
            $0.trailing.equalTo(closeButton.snp.trailing)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        setState(error: false)
    }
}

private extension PreviewViewController {
    func setState(error: Bool) {
        closeButton.isHidden = !error
        uploadErrorView.isHidden = !error
        messageErrorView.isHidden = !error
        confirmationView.isHidden = error
    }

    @objc
    func didTapClose() {
        viewModel.close()
    }
}

extension PreviewViewController: PreviewConfirmationViewDelegate {
    func didTapRetakeButton() {
        viewModel.retakePicture()
    }

    func didTapAcceptButton() {
        viewModel.acceptPicture()
    }
}

extension PreviewViewController: PreviewUploadErrorDelegate {
    func didTapTryAgainButton() {
        viewModel.tryAgain()
    }
}

extension PreviewViewController: PreviewDisplay {
    func displayPreviewImage(_ image: UIImage) {
        previewImageView.image = image
    }

    func setupConfirmationView(infoText: String, retakeButtonTitle: String, acceptButtonTitle: String) {
        confirmationView.setup(
            informationText: infoText,
            retakeButtonTitle: retakeButtonTitle,
            acceptButtonTitle: acceptButtonTitle
        )
    }

    func setupUploadErrorView(tryAgainButtonTitle: String) {
        uploadErrorView.setup(tryAgainButtonTitle: tryAgainButtonTitle)
    }

    func displayError(_ message: String) {
        setState(error: true)
        messageErrorView.setup(errorMessage: message)
    }
}
