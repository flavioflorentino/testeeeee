import UIKit
import Core

protocol PreviewViewModelInputs: AnyObject {
    func setupPreview()
    func close()
    func retakePicture()
    func acceptPicture()
    func tryAgain()
}

final class PreviewViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let previewImage: UIImage
    private let presenter: PreviewPresenting
    private let service: PreviewServicing
    private let type: CameraType
    private let serviceToken: String
    private let flow: String

    private let imageSize = CGSize(width: 720, height: 1_280)
    private let imageQuality: CGFloat = 1

    init(
        serviceToken: String,
        flow: String,
        type: CameraType,
        previewImage: UIImage,
        service: PreviewServicing,
        presenter: PreviewPresenting,
        dependencies: Dependencies
    ) {
        self.serviceToken = serviceToken
        self.flow = flow
        self.previewImage = previewImage
        self.presenter = presenter
        self.service = service
        self.type = type
        self.dependencies = dependencies
    }
}

extension PreviewViewModel: PreviewViewModelInputs {
    func setupPreview() {
        trackSendEvent()
        presenter.previewImage(previewImage)
        presenter.displayTexts(for: type)
    }

    func retakePicture() {
        presenter.retakePicture()
    }

    func acceptPicture() {
        guard let imageData = prepareImageForUpload() else {
            presenter.displayError(message: Strings.Preview.Error.text)
            return
        }

        presenter.setLoadingState(true)

        service.uploadImage(token: serviceToken, flow: flow, imageData: imageData) { [weak self] result in
            self?.handleUploadResult(result)
        }
    }

    func tryAgain() {
        presenter.retakePicture()
    }

    func close() {
        presenter.close()
    }
}

private extension PreviewViewModel {
    func prepareImageForUpload() -> Data? {
        guard
            let imageForUpload = previewImage.resizeImage(targetSize: imageSize),
            let imageData = imageForUpload.jpegData(compressionQuality: imageQuality)
            else {
                return nil
        }
        return imageData
    }

    func handleUploadResult(_ result: Result<IdentityStep, ApiError>) {
        presenter.setLoadingState(false)

        switch result {
        case .success(let step):
            presenter.nextStep(step)

        case .failure(let error):
            trackErrorEvent(message: error.requestError?.message)
            presenter.displayError(message: error.requestError?.message)
        }
    }

    func trackSendEvent() {
        switch type {
        case .selfie:
            trackScreenViewed(screenName: IdentityValidationSelfieEventProperty.send)
        case .documentFront:
            trackScreenViewed(screenName: IdentityValidationDocumentEventProperty.frontCameraSend)
        case .documentBack:
            trackScreenViewed(screenName: IdentityValidationDocumentEventProperty.backCameraSend)
        }
    }
    
    func trackErrorEvent(message: String?) {
        switch type {
        case .selfie:
            trackScreenViewed(screenName: IdentityValidationSelfieEventProperty.error)
            trackDialogViewed(screenName: IdentityValidationSelfieEventProperty.error, message: message)
        case .documentFront:
            trackScreenViewed(screenName: IdentityValidationDocumentEventProperty.frontCameraError)
            trackDialogViewed(screenName: IdentityValidationDocumentEventProperty.frontCameraError, message: message)
        case .documentBack:
            trackScreenViewed(screenName: IdentityValidationDocumentEventProperty.backCameraError)
            trackDialogViewed(screenName: IdentityValidationDocumentEventProperty.backCameraError, message: message)
        }
    }
    
    func trackScreenViewed(screenName: IdentityValidationPropertyValue) {
        let properties: [IdentityValidationEventProperty] = [
            .init(name: .screenName, value: screenName),
            .init(name: .flow, value: flow)
        ]
        let tracker = IdentityValidationTracker(eventType: IdentityValidationEventType.screenViewed, eventProperties: properties)
        dependencies.analytics.log(tracker)
    }
    
    func trackDialogViewed(screenName: IdentityValidationPropertyValue, message: String?) {
        let properties: [IdentityValidationEventProperty] = [
            .init(name: .screenName, value: screenName),
            .init(name: .flow, value: flow),
            .init(name: .dialogName, value: IdentityValidationDialogNameProperty.error),
            .init(name: .dialogText, value: message ?? "")
        ]
        let tracker = IdentityValidationTracker(eventType: IdentityValidationEventType.dialogViewed, eventProperties: properties)
        dependencies.analytics.log(tracker)
    }
}
