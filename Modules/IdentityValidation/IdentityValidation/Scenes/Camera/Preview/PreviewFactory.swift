import Foundation
import UIKit

enum PreviewFactory {
    static func make(
        serviceToken: String,
        flow: String,
        cameraType: CameraType,
        previewImage: UIImage,
        delegate: CameraFlowCoordinating?
    ) -> PreviewViewController {
        let dependencyContainer = DependencyContainer()
        let service = PreviewService(dependencies: DependencyContainer())
        let coordinator = PreviewCoordinator(delegate: delegate)
        let presenter = PreviewPresenter(coordinator: coordinator)
        let viewModel = PreviewViewModel(
            serviceToken: serviceToken,
            flow: flow,
            type: cameraType,
            previewImage: previewImage,
            service: service,
            presenter: presenter,
            dependencies: dependencyContainer
        )
        let viewController = PreviewViewController(viewModel: viewModel)

        presenter.viewController = viewController

        return viewController
    }
}
