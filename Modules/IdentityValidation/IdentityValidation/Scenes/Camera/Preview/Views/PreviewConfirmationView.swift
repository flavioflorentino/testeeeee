import UIKit
import UI
import UIKit

protocol PreviewConfirmationViewDelegate: AnyObject {
    func didTapRetakeButton()
    func didTapAcceptButton()
}

private extension PreviewConfirmationView.Layout {
    enum Alpha {
        static let background: CGFloat = 0.75
    }
}

final class PreviewConfirmationView: UIView, ViewConfiguration {
    fileprivate enum Layout { }

    weak var delegate: PreviewConfirmationViewDelegate?

    private lazy var informationLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .white(.light))
        return label
    }()

    private lazy var retakeButton: UIButton = {
        let button = UIButton(type: .custom)
        button.addTarget(self, action: #selector(retakeButtonTapped(_:)), for: .touchUpInside)
        return button
    }()

    private lazy var acceptButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(acceptButtonTapped(_:)), for: .touchUpInside)
        button.buttonStyle(PrimaryButtonStyle())
        return button
    }()

    private lazy var buttonsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fillEqually
        stackView.alignment = .center
        stackView.spacing = Spacing.base02
        return stackView
    }()

    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func buildViewHierarchy() {
        addSubview(informationLabel)
        addSubview(buttonsStackView)
        buttonsStackView.addArrangedSubview(retakeButton)
        buttonsStackView.addArrangedSubview(acceptButton)
    }

    func setupConstraints() {
        informationLabel.snp.makeConstraints {
            $0.top.leading.equalToSuperview().offset(Spacing.base03)
            $0.trailing.equalToSuperview().offset(-Spacing.base03)
        }

        buttonsStackView.snp.makeConstraints {
            $0.top.equalTo(informationLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalTo(informationLabel)
            $0.bottom.equalTo(compatibleSafeArea.bottom).offset(-Spacing.base03)
        }
    }

    func configureViews() {
        backgroundColor = Colors.black.lightColor.withAlphaComponent(Layout.Alpha.background)
    }

    func setup(informationText: String, retakeButtonTitle: String, acceptButtonTitle: String) {
        informationLabel.text = informationText
        acceptButton.setTitle(acceptButtonTitle, for: .normal)
        retakeButton.setTitle(retakeButtonTitle, for: .normal)
        retakeButton.buttonStyle(LinkButtonStyle())
    }
}

@objc
private extension PreviewConfirmationView {
    func retakeButtonTapped(_ sender: UIButton) {
        delegate?.didTapRetakeButton()
    }

    func acceptButtonTapped(_ sender: UIButton) {
        delegate?.didTapAcceptButton()
    }
}
