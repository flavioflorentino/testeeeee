import UIKit
import UI
import UIKit

final class PreviewMessageErrorView: UIView, ViewConfiguration {
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(CaptionLabelStyle(type: .highlight))
            .with(\.textColor, .white(.light))
        return label
    }()

    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func buildViewHierarchy() {
        addSubview(messageLabel)
    }

    func setupConstraints() {
        messageLabel.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(Spacing.base02)
        }
    }

    func configureViews() {
        backgroundColor = Colors.critical400.color
    }

    func setup(errorMessage: String) {
        messageLabel.text = errorMessage
    }
}
