import UIKit
import UI
import UIKit

protocol PreviewUploadErrorDelegate: AnyObject {
    func didTapTryAgainButton()
}

private extension PreviewUploadErrorView.Layout {
    enum Alpha {
        static let background: CGFloat = 0.75
    }
}

final class PreviewUploadErrorView: UIView, ViewConfiguration {
    fileprivate enum Layout { }

    weak var delegate: PreviewUploadErrorDelegate?

    private lazy var tryAgainButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(tryAgainButtonTapped(_:)), for: .touchUpInside)
        button.buttonStyle(PrimaryButtonStyle())
        return button
    }()

    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func buildViewHierarchy() {
        addSubview(tryAgainButton)
    }

    func setupConstraints() {
        tryAgainButton.snp.makeConstraints {
            $0.top.leading.equalToSuperview().offset(Spacing.base03)
            $0.trailing.equalToSuperview().offset(-Spacing.base03)
            $0.bottom.equalTo(compatibleSafeArea.bottom).offset(-Spacing.base03)
        }
    }

    func configureViews() {
        backgroundColor = Colors.black.lightColor.withAlphaComponent(Layout.Alpha.background)
    }

    func setup(tryAgainButtonTitle: String) {
        tryAgainButton.setTitle(tryAgainButtonTitle, for: .normal)
    }
}

@objc
private extension PreviewUploadErrorView {
    func tryAgainButtonTapped(_ sender: UIButton) {
        delegate?.didTapTryAgainButton()
    }
}
