import Core
import Foundation

protocol PreviewServicing {
    func uploadImage(
        token: String,
        flow: String,
        imageData: Data,
        completion: @escaping (Result<IdentityStep, ApiError>) -> Void
    )
}

final class PreviewService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension PreviewService: PreviewServicing {
    func uploadImage(
        token: String,
        flow: String,
        imageData: Data,
        completion: @escaping (Result<IdentityStep, ApiError>) -> Void
    ) {
        let boundary = MultipartImageData.generateBoundary()
        let bodyData = MultipartImageData(imageData: imageData, boundary: boundary).build()
        let endpoint = imageEndpoint(token: token, flow: flow, boundary: boundary, body: bodyData)
        let api = Api<IdentityStep>(endpoint: endpoint)
        
        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}

private extension PreviewService {
    func imageEndpoint(token: String, flow: String, boundary: String, body: Data) -> IdentityValidationEndpoint {
        IdentityValidationEndpoint(
            path: "verifications/identity/step/image",
            method: .post,
            contentType: .multipartFormData(boundary: boundary),
            customHeaders: [
                "Token": token,
                "flow": flow
            ],
            body: body
        )
    }
}
