import UIKit
import Foundation

protocol CaptureViewModelInputs: AnyObject {
    func setupCameraView()
    func setupCameraMask()
    func checkCameraAuthorization(placeholderView: UIView)
    func takePhoto()
    func stopCamera()
    func openSettings()
    func close()
}

final class CaptureViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let cameraManager: CameraManagerProtocol
    private let presenter: CapturePresenting
    private let service: CaptureServicing
    private let type: CameraType
    private let flow: String
    
    private var shouldConfigureCamera = true

    init(
        presenter: CapturePresenting,
        service: CaptureServicing,
        type: CameraType,
        dependencies: Dependencies,
        flow: String,
        cameraManager: CameraManagerProtocol = CameraManager()
    ) {
        self.cameraManager = cameraManager
        self.presenter = presenter
        self.service = service
        self.type = type
        self.dependencies = dependencies
        self.flow = flow
    }
}

extension CaptureViewModel: CaptureViewModelInputs {
    func setupCameraView() {
        trackAppearEvent()
        presenter.displayTexts(for: type)
    }

    func setupCameraMask() {
        presenter.displayCameraMask(for: type)
    }

    func checkCameraAuthorization(placeholderView: UIView) {
        service.checkCameraPermission { authorizationStatus in
            switch authorizationStatus {
            case .authorized:
                self.shouldConfigureCamera(placeholderView: placeholderView)
            case .notDetermined:
                self.requestCameraPermission(view: placeholderView)
            case .denied, .disabled:
                self.presenter.displayPermissionDenied()
            }
        }
    }

    func takePhoto() {
        cameraManager.captureImage { [weak self] result in
            self?.stopCamera()

            switch result {
            case .success(let image):
                self?.presenter.showPreview(with: image)
            case .failure(let error):
                self?.presenter.displayError(message: error.localizedDescription)
            }
        }
    }

    func stopCamera() {
        cameraManager.stopRunningCamera()
    }

    func openSettings() {
        presenter.openSettings()
    }

    func close() {
        presenter.close()
    }
}

private extension CaptureViewModel {
    func requestCameraPermission(view: UIView) {
        service.requestCameraPermission(completion: nil)
    }

    func shouldConfigureCamera(placeholderView: UIView) {
        if shouldConfigureCamera {
            configureCamera(placeholderView: placeholderView)
        } else {
            cameraManager.startRunningCamera()
        }
    }

    func configureCamera(placeholderView: UIView) {
        let cameraType: CameraManager.CameraType

        switch type {
        case .selfie:
            cameraType = .front
        case .documentFront, .documentBack:
            cameraType = .back
        }

        cameraManager.prepare(camera: cameraType) { [weak self] error in
            self?.handleCameraPrepare(error, view: placeholderView)
        }

        shouldConfigureCamera = false
    }

    func handleCameraPrepare(_ error: Error?, view: UIView) {
        guard error == nil else {
            presenter.displayError(message: nil)
            return
        }

        do {
            try cameraManager.displayPreview(on: view)
        } catch {
            presenter.displayError(message: nil)
        }
    }

    func trackAppearEvent() {
        switch type {
        case .selfie:
            trackScreenViewedEvent(screenName: IdentityValidationSelfieEventProperty.camera)
        case .documentFront:
            trackScreenViewedEvent(screenName: IdentityValidationDocumentEventProperty.frontCamera)
        case .documentBack:
            trackScreenViewedEvent(screenName: IdentityValidationDocumentEventProperty.backCamera)
        }
    }
    
    func trackScreenViewedEvent(screenName: IdentityValidationPropertyValue) {
        let properties: [IdentityValidationEventProperty] = [
            .init(name: .screenName, value: screenName),
            .init(name: .flow, value: flow)
        ]
        let tracker = IdentityValidationTracker(eventType: IdentityValidationEventType.screenViewed, eventProperties: properties)
        dependencies.analytics.log(tracker)
    }
}
