import AssetsKit
import UI
import UIKit

protocol CapturePresenting: AnyObject {
    var viewController: CaptureDisplay? { get set }
    func displayTexts(for type: CameraType)
    func displayCameraMask(for type: CameraType)
    func displayPermissionDenied()
    func displayError(message: String?)
    func showPreview(with image: UIImage)
    func openSettings()
    func close()
}

final class CapturePresenter {
    private let coordinator: CaptureCoordinating
    weak var viewController: CaptureDisplay?

    init(coordinator: CaptureCoordinating) {
        self.coordinator = coordinator
    }
}

extension CapturePresenter: CapturePresenting {
    func displayTexts(for type: CameraType) {
        viewController?.startLoadingView()

        switch type {
        case .selfie:
            viewController?.showTip(text: Strings.Camera.Selfie.Tip.text)

        case .documentFront:
            let attributedString = createAttributedString(text: Strings.Camera.Document.Front.Tip.text,
                                                          highlight: Strings.Camera.Document.Front.Tip.highlight)
            viewController?.showAttributedTip(attributedString)

        case .documentBack:
            let attributedString = createAttributedString(text: Strings.Camera.Document.Back.Tip.text,
                                                          highlight: Strings.Camera.Document.Back.Tip.highlight)
            viewController?.showAttributedTip(attributedString)
        }
    }

    func displayCameraMask(for type: CameraType) {
        viewController?.stopLoadingView()

        switch type {
        case .selfie:
            viewController?.addCameraMask(with: SelfieMaskHandler())
        case .documentFront, .documentBack:
            viewController?.addCameraMask(with: DocumentMaskHandler())
        }
    }

    func displayPermissionDenied() {
        let popupProperties = PopupProperties(type: .image(Resources.Icons.icoCameraPermission.image),
                                              title: Strings.Camera.Permission.title,
                                              message: Strings.Camera.Permission.description,
                                              mainButtonTitle: Strings.Camera.Button.settings,
                                              secondaryButtonTitle: Strings.General.later)
        viewController?.showPermissionPopupController(with: popupProperties)
    }

    func displayError(message: String?) {
        let popupProperties = PopupProperties(type: .text,
                                              title: Strings.Error.title,
                                              message: message ?? Strings.Error.message,
                                              mainButtonTitle: Strings.General.close,
                                              secondaryButtonTitle: nil)
        viewController?.showErrorPopupController(with: popupProperties)
    }

    func showPreview(with image: UIImage) {
        coordinator.perform(action: .preview(image))
    }

    func openSettings() {
        coordinator.perform(action: .settings)
    }

    func close() {
        coordinator.perform(action: .close)
    }
}

private extension CapturePresenter {
    func createAttributedString(text: String, highlight: String) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: text)
        let attributedRange = (attributedString.string as NSString).range(of: highlight)

        attributedString.addAttributes([.font: Typography.bodyPrimary(.highlight).font()], range: attributedRange)

        return attributedString
    }
}
