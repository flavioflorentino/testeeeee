import AssetsKit
import UI
import UIKit

protocol CaptureDisplay: AnyObject, LoadingViewProtocol {
    func showTip(text: String)
    func showAttributedTip(_ string: NSAttributedString)
    func addCameraMask(with handler: CameraMaskHandler)
    func showPermissionPopupController(with properties: PopupProperties)
    func showErrorPopupController(with properties: PopupProperties)
}

private extension CaptureViewController.Layout {
    enum Size {
        static let closeButton: CGFloat = 32
        static let shotButton: CGFloat = 64
    }

    enum Radius {
        static let shotButton = Size.shotButton / 2
    }
}

final class CaptureViewController: ViewController<CaptureViewModelInputs, UIView>, CaptureDisplay {
    fileprivate enum Layout {}

    private lazy var maskView = UIView()

    private lazy var closeButton: UIButton = {
        let button = UIButton()
        button.setImage(Resources.Icons.icoClose.image, for: .normal)
        button.addTarget(self, action: #selector(didTapClose), for: .touchUpInside)
        button.accessibilityLabel = Strings.General.Button.Close.Accessibility.text
        return button
    }()

    private lazy var tipLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .white(.light))
            .with(\.textAlignment, .center)
        return label
    }()

    private lazy var shotButton: UIButton = {
        let button = UIButton()
        button.setImage(Resources.Icons.icoCamera.image, for: .normal)
        button.backgroundColor = Colors.branding400.color
        button.layer.cornerRadius = Layout.Radius.shotButton
        button.addTarget(self, action: #selector(didTapTake), for: .touchUpInside)
        button.accessibilityLabel = Strings.Camera.Button.Shot.Accessibility.text
        return button
    }()

    lazy var loadingView = LoadingView()

    override var prefersStatusBarHidden: Bool { true }

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(checkCameraAuthorization),
            name: UIApplication.didBecomeActiveNotification,
            object: nil
        )
        viewModel.setupCameraView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        setNeedsStatusBarAppearanceUpdate()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkCameraAuthorization()
        viewModel.setupCameraMask()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel.stopCamera()
    }

    override func buildViewHierarchy() {
        view.addSubview(maskView)
        view.addSubview(closeButton)
        view.addSubview(tipLabel)
        view.addSubview(shotButton)
    }

    override func setupConstraints() {
        closeButton.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top).offset(Spacing.base03)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.size.equalTo(Layout.Size.closeButton)
        }

        tipLabel.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base04)
            $0.trailing.equalToSuperview().offset(-Spacing.base04)
            $0.top.equalTo(closeButton.snp.bottom).offset(Spacing.base02)
        }

        maskView.snp.makeConstraints {
            $0.top.equalTo(tipLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalTo(tipLabel)
        }

        shotButton.snp.makeConstraints {
            $0.top.equalTo(maskView.snp.bottom).offset(Spacing.base02)
            $0.centerX.equalToSuperview()
            $0.size.equalTo(Layout.Size.shotButton)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).offset(-Spacing.base05)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        maskView.backgroundColor = .clear
    }

    // MARK: - CaptureDisplay

    func showTip(text: String) {
        tipLabel.text = text
    }

    func showAttributedTip(_ string: NSAttributedString) {
        tipLabel.attributedText = string
    }

    func addCameraMask(with handler: CameraMaskHandler) {
        handler.addMask(in: view, maskView: maskView)
    }

    func showPermissionPopupController(with properties: PopupProperties) {
        showPopupController(
            with: properties,
            mainButtonAction: { [weak self] in
                self?.viewModel.openSettings()
            },
            secondaryButtonAction: { [weak self] in
                self?.viewModel.close()
            }
        )
    }

    func showErrorPopupController(with properties: PopupProperties) {
        showPopupController(with: properties) { [weak self] in
            self?.viewModel.close()
        }
    }
}

@objc
private extension CaptureViewController {
    func didTapClose() {
        viewModel.close()
    }

    func didTapTake() {
        viewModel.takePhoto()
    }

    func checkCameraAuthorization() {
        viewModel.checkCameraAuthorization(placeholderView: view)
    }
}
