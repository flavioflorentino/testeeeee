import UIKit

enum CaptureAction {
    case close
    case settings
    case preview(UIImage)
}

protocol CaptureCoordinating: AnyObject {
    func perform(action: CaptureAction)
}

final class CaptureCoordinator {
    private let application: UIApplicationContract
    private weak var delegate: CameraFlowCoordinating?

    init(delegate: CameraFlowCoordinating?, application: UIApplicationContract = UIApplication.shared) {
        self.application = application
        self.delegate = delegate
    }
}

extension CaptureCoordinator: CaptureCoordinating {
    func perform(action: CaptureAction) {
        switch action {
        case .close:
            delegate?.close()

        case .settings:
            openDeviceSettings()

        case .preview(let image):
            delegate?.navigateToPreview(with: image)
        }
    }
}

private extension CaptureCoordinator {
    func openDeviceSettings() {
        guard
            let url = URL(string: UIApplication.openSettingsURLString),
            application.canOpenURL(url)
            else {
                return
        }
        application.open(url)
    }
}
