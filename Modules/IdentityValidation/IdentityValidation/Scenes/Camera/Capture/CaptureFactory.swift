import Foundation

enum CaptureFactory {
    static func make(cameraType: CameraType, delegate: CameraFlowCoordinating?, flow: String) -> CaptureViewController {
        let dependencyContainer = DependencyContainer()
        let service = CaptureService(dependencies: DependencyContainer())
        let coordinator = CaptureCoordinator(delegate: delegate)
        let presenter = CapturePresenter(coordinator: coordinator)
        let viewModel = CaptureViewModel(
            presenter: presenter,
            service: service,
            type: cameraType,
            dependencies: dependencyContainer,
            flow: flow
        )
        let viewController = CaptureViewController(viewModel: viewModel)

        presenter.viewController = viewController

        return viewController
    }
}
