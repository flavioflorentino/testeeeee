import Core
import Foundation

protocol CaptureServicing {
    func checkCameraPermission(completion: @escaping (DevicePermissionStatus) -> Void)
    func requestCameraPermission(completion: ((Bool) -> Void)?)
}

final class CaptureService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    private let cameraHandler: CameraPermissionHandling

    init(dependencies: Dependencies, cameraHandler: CameraPermissionHandling = PermissionHandler()) {
        self.dependencies = dependencies
        self.cameraHandler = cameraHandler
    }
}

extension CaptureService: CaptureServicing {
    func checkCameraPermission(completion: @escaping (DevicePermissionStatus) -> Void) {
        dependencies.mainQueue.async {
            completion(self.cameraHandler.cameraAuthorizationStatus)
        }
    }

    func requestCameraPermission(completion: ((Bool) -> Void)?) {
        cameraHandler.requestCamera { [weak self] authorized in
            self?.dependencies.mainQueue.async { completion?(authorized) }
        }
    }
}
