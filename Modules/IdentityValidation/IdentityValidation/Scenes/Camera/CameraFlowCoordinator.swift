import UIKit

enum CameraType {
    case selfie
    case documentFront
    case documentBack
}

protocol CameraFlowCoordinating: AnyObject {
    func navigateToPreview(with image: UIImage)
    func returnToCamera()
    func finished(with nextStep: IdentityStep)
    func close()
}

protocol CameraFlowCoordinatorDelegate: AnyObject {
    func finished(with nextStep: IdentityStep)
    func closeCamera()
}

final class CameraFlowCoordinator {
    private let originController: UIViewController
    private let navigationController: UINavigationController
    private let type: CameraType
    private let serviceToken: String
    private let flow: String
    private weak var coordinatorDelegate: CameraFlowCoordinatorDelegate?

    init(
        serviceToken: String,
        flow: String,
        type: CameraType,
        delegate: CameraFlowCoordinatorDelegate?,
        originController: UIViewController,
        navigationController: UINavigationController = UINavigationController()
    ) {
        self.serviceToken = serviceToken
        self.flow = flow
        self.type = type
        self.coordinatorDelegate = delegate
        self.originController = originController
        self.navigationController = navigationController
    }
}

extension CameraFlowCoordinator: CoordinatorProtocol {
    func start() {
        let captureController = CaptureFactory.make(cameraType: type, delegate: self, flow: flow)
        navigationController.pushViewController(captureController, animated: false)
        navigationController.modalPresentationStyle = .fullScreen
        originController.present(navigationController, animated: true)
    }
}

extension CameraFlowCoordinator: CameraFlowCoordinating {
    func navigateToPreview(with image: UIImage) {
        let previewController = PreviewFactory.make(
            serviceToken: serviceToken,
            flow: flow,
            cameraType: type,
            previewImage: image,
            delegate: self
        )
        navigationController.pushViewController(previewController, animated: true)
    }

    func returnToCamera() {
        navigationController.popViewController(animated: true)
    }

    func finished(with nextStep: IdentityStep) {
        coordinatorDelegate?.finished(with: nextStep)
    }

    func close() {
        coordinatorDelegate?.closeCamera()
    }
}
