import Foundation

public protocol IDValidationCoordinatorProtocol: CoordinatorProtocol {}

public protocol CoordinatorProtocol: AnyObject {
    func start()
}
