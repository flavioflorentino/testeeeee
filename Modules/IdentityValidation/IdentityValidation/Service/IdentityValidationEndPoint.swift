import Foundation
import Core

struct IdentityValidationEndpoint: ApiEndpointExposable {
    var path: String
    var method: HTTPMethod
    var contentType: ContentType
    var customHeaders: [String: String]
    var body: Data?
    var isTokenNeeded: Bool { false }
}
