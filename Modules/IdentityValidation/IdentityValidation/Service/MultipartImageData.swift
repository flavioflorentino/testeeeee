import Foundation

struct MultipartImageData {
    let imageData: Data
    let boundary: String

    private static let range = 0..<1_000_000_000

    private let name = "image"
    private let mimetype = "image/jpg"
    private let fileName = String(format: "imagem-%08x.jpg", Int.random(in: range))

    static func generateBoundary() -> String {
        String(format: "picpay.boundary.%08x%08x", Int.random(in: range), Int.random(in: range))
    }

    func build() -> Data {
        var body = Data()

        body.appendString("--\(boundary)\r\n")
        body.appendString("Content-Disposition: form-data; name=\"\(name)\"; filename=\"\(fileName)\"\r\n")
        body.appendString("Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageData)
        body.appendString("\r\n")
        body.appendString("--\(boundary)--\r\n")

        return body
    }
}

private extension Data {
    mutating func appendString(_ string: String) {
        if let data = string.data(using: .utf8, allowLossyConversion: true) {
            append(data)
        }
    }
}
