import Foundation

// swiftlint:disable convenience_type
final class IdentityValidationResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: IdentityValidationResources.self).url(forResource: "IdentityValidationResources", withExtension: "bundle") else {
            return Bundle(for: IdentityValidationResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: IdentityValidationResources.self)
    }()
}
