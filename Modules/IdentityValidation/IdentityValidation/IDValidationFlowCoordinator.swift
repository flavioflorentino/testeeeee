import AnalyticsModule
import UIKit
import UI

protocol IdentityValidationCoordinating: AnyObject {
    func presentNextStep(_ step: IdentityStep)
    func dismiss()
    func endWithStatus(_ status: IDValidationStatus?)
}

public final class IDValidationFlowCoordinator {
    typealias Dependencies = HasAnalytics
    var dependencies: Dependencies = DependencyContainer()
    
    private var currentCoordinator: CoordinatorProtocol?

    private let originViewController: UIViewController
    private let navigationController: UINavigationController
    private let userToken: String
    private let flow: String
    private var completion: ((_ status: IDValidationStatus?) -> Void)?

    public init(
        from viewController: UIViewController,
        token: String,
        flow: String,
        completion: ((_ status: IDValidationStatus?) -> Void)?,
        navigationController: UINavigationController = UINavigationController()
    ) {
        self.originViewController = viewController
        self.navigationController = navigationController
        self.userToken = token
        self.flow = flow
        self.completion = completion
    }
}

extension IDValidationFlowCoordinator: IDValidationCoordinatorProtocol {
    public func start() {
        let loaderController = LoaderFactory.make(
            serviceToken: userToken,
            flow: flow,
            coordinatorDelegate: self
        )
        navigationController.pushViewController(loaderController, animated: false)
        if #available(iOS 13.0, *) {
            navigationController.isModalInPresentation = true
        }
        originViewController.present(navigationController, animated: true)
    }
}

extension IDValidationFlowCoordinator: IdentityValidationCoordinating {
    func presentNextStep(_ step: IdentityStep) {
        currentCoordinator = nil

        switch step.type {
        case .selfie:
            presentSelfie(step)

        case .documentFront, .documentBack:
            presentDocument(step)

        case .done, .processing, .onHold:
            presentStatus(step)

        default:
            endValidationFlow(with: step.status)
        }
    }

    func endWithStatus(_ status: IDValidationStatus?) {
        endValidationFlow(with: status)
    }

    func dismiss() {
        endValidationFlow(with: nil)
    }
}

private extension IDValidationFlowCoordinator {
    func presentSelfie(_ step: IdentityStep) {
        currentCoordinator = FacialBiometricFlowCoordinator(
            with: navigationController,
            serviceToken: userToken,
            flow: flow,
            step: step,
            coordinatorDelegate: self
        )
        currentCoordinator?.start()
    }

    func presentDocument(_ step: IdentityStep) {
        currentCoordinator = DocumentCaptureFlowCoordinator(
            with: navigationController,
            serviceToken: userToken,
            flow: flow,
            step: step,
            coordinatorDelegate: self
        )
        currentCoordinator?.start()
    }

    func presentStatus(_ step: IdentityStep) {
        let statusController = StatusFactory.make(delegate: self, serviceToken: userToken, flow: flow, step: step)
        navigationController.pushViewController(statusController, animated: true)
    }

    func endValidationFlow(with status: IDValidationStatus?) {
        trackStatus(status)
        originViewController.dismiss(animated: true) {
            self.completion?(status)
        }
    }
    
    private func trackStatus(_ status: IDValidationStatus?) {
        let event = IdentityValidationEventType.idValidationClosed
        let properties: [IdentityValidationEventProperty] = [
            .init(name: .status, value: status?.rawValue ?? "NULL"),
            .init(name: .flow, value: flow)
        ]
        
        let tracker = IdentityValidationTracker(eventType: event, eventProperties: properties)
        dependencies.analytics.log(tracker)
    }
}
