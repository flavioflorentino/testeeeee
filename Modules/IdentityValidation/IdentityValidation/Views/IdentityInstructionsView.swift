import UIKit
import UI

final class IdentityInstructionsView: UIView, ViewConfiguration {
    fileprivate enum Layout { }

    var mainButtonAction: (() -> Void)?
    var secondaryButtonAction: (() -> Void)?

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        return label
    }()

    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base05
        return stackView
    }()

    private lazy var mainButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(didTapMainButton), for: .touchUpInside)
        button.buttonStyle(PrimaryButtonStyle())
        return button
    }()

    private lazy var secondaryButton: UIButton = {
        let button = UIButton(type: .custom)
        button.addTarget(self, action: #selector(didTapSecondaryButton), for: .touchUpInside)
        return button
    }()

    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func buildViewHierarchy() {
        addSubview(titleLabel)
        addSubview(stackView)
        addSubview(mainButton)
        addSubview(secondaryButton)
    }

    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        stackView.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base05)
            $0.leading.trailing.equalTo(titleLabel)
            $0.centerY.equalToSuperview().offset(-Spacing.base05)
        }

        mainButton.snp.makeConstraints {
            $0.top.equalTo(stackView.snp.bottom).offset(Spacing.base06)
            $0.leading.trailing.equalTo(stackView)
        }

        secondaryButton.snp.makeConstraints {
            $0.top.equalTo(mainButton.snp.bottom).offset(Spacing.base01)
            $0.centerX.equalToSuperview()
        }
    }

    func configureView(with title: String, mainButtonTitle: String, secondaryButtonTitle: String) {
        titleLabel.text = title
        mainButton.setTitle(mainButtonTitle, for: .normal)
        secondaryButton.setTitle(secondaryButtonTitle, for: .normal)
        secondaryButton.buttonStyle(LinkButtonStyle())
    }

    func addInstructionRows(_ rows: [(UIImage, String)]) {
        rows.forEach { icon, text in
            let row = InstructionRowView()
            row.configureRow(with: icon, text: text)
            stackView.addArrangedSubview(row)
        }
    }
}

@objc
private extension IdentityInstructionsView {
    func didTapMainButton() {
        mainButtonAction?()
    }

    func didTapSecondaryButton() {
        secondaryButtonAction?()
    }
}
