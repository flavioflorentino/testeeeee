import UI
import UIKit

extension IdentityInfoView.Layout {
    enum Size {
        static let imageHeight: CGFloat = 130
    }
}

final class IdentityInfoView: UIView, ViewConfiguration {
    fileprivate enum Layout { }

    var mainButtonAction: (() -> Void)?

    private lazy var iconImageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFit
        return image
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        return label
    }()

    private lazy var mainButton: UIButton = {
        let button = UIButton(type: .custom)
        button.addTarget(self, action: #selector(didTapMainButton), for: .touchUpInside)
        button.buttonStyle(PrimaryButtonStyle())
        return button
    }()

    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base03
        return stackView
    }()

    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func buildViewHierarchy() {
        addSubview(stackView)
        stackView.addArrangedSubview(iconImageView)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(descriptionLabel)
        stackView.addArrangedSubview(mainButton)
    }

    func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        iconImageView.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.imageHeight)
        }
    }

    func setIconImage(from url: URL?, fallbackIcon: UIImage? = nil) {
        iconImageView.setImage(url: url) { [weak self] image in
            self?.iconImageView.image = image ?? fallbackIcon
        }
    }

    func configureView(with title: String, description: String, buttonTitle: String) {
        titleLabel.text = title
        descriptionLabel.text = description
        mainButton.setTitle(buttonTitle, for: .normal)
    }
    
    func showLoadingButton(_ isLoading: Bool, text: String) {
        mainButton.setTitle(text, for: isLoading ? .disabled : .normal)
        isLoading ? mainButton.startLoading() : mainButton.stopLoading()
    }
}

private extension IdentityInfoView {
    @objc
    func didTapMainButton() {
        mainButtonAction?()
    }
}
