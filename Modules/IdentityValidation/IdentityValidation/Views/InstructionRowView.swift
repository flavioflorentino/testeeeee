import UIKit
import UI
import UIKit

extension InstructionRowView.Layout {
    enum Size {
        static let icon: CGFloat = 40.0
    }
}

final class InstructionRowView: UIView, ViewConfiguration {
    fileprivate enum Layout { }
    
    private lazy var iconImageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale600())
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func buildViewHierarchy() {
        addSubview(iconImageView)
        addSubview(descriptionLabel)
    }

    func setupConstraints() {
        iconImageView.snp.makeConstraints {
            $0.leading.centerY.equalToSuperview()
            $0.size.equalTo(Layout.Size.icon)
        }

        descriptionLabel.snp.makeConstraints {
            $0.top.bottom.trailing.equalToSuperview()
            $0.leading.equalTo(iconImageView.snp.trailing).offset(Spacing.base02)
        }
    }

    func configureViews() {
        backgroundColor = .clear
    }
    
    func configureRow(with icon: UIImage, text: String) {
        iconImageView.image = icon
        descriptionLabel.text = text
    }
}
