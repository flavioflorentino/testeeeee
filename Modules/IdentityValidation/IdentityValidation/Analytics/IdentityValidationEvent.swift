import Foundation

protocol IdentityValidationEvent {
    var name: String { get }
}

extension IdentityValidationEvent where Self: RawRepresentable, Self.RawValue == String {
    var name: String { rawValue }
}

enum IdentityValidationEventType: String, IdentityValidationEvent {
    case userNavigated = "User Navigated"
    case buttonClicked = "Button Clicked"
    case screenViewed = "Screen Viewed"
    case idValidationClosed = "Validation ID Status Closed"
    case dialogViewed = "Dialog Viewed"
}
