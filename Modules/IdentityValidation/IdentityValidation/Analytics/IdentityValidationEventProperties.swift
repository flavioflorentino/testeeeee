import Foundation

struct IdentityValidationEventProperty {
    enum Name: String {
        case screenName = "screen_name"
        case buttonName = "button_name"
        case flow = "flow"
        case status = "status"
        case dialogName = "dialog_name"
        case dialogText = "dialog_text"
    }
    
    var name: Name
    var value: IdentityValidationPropertyValue
}

protocol IdentityValidationPropertyValue {
    var name: String { get }
}

extension IdentityValidationPropertyValue where Self: RawRepresentable, Self.RawValue == String {
    var name: String { rawValue }
}

enum IdentityValidationSelfieEventProperty: String, IdentityValidationPropertyValue {
    case intro = "VALIDAR_IDENTIDADE_SELFIE_INTRO"
    case tips = "VALIDAR_IDENTIDADE_SELFIE_DICAS"
    case camera = "VALIDAR_IDENTIDADE_SELFIE_CAMERA"
    case send = "VALIDAR_IDENTIDADE_SELFIE_ENVIAR"
    case error = "VALIDAR_IDENTIDADE_SELFIE_ERROR"
}

enum IdentityValidationDocumentEventProperty: String, IdentityValidationPropertyValue {
    case intro = "VALIDAR_IDENTIDADE_DOCUMENTO_INTRO"
    case tips = "VALIDAR_IDENTIDADE_DOCUMENTO_DICAS"
    case frontCamera = "VALIDAR_IDENTIDADE_DOCUMENTO_CAMERA_FRONTAL"
    case frontCameraSend = "VALIDAR_IDENTIDADE_DOCUMENTO_CAMERA_FRONTAL_ENVIAR"
    case frontCameraError = "VALIDAR_IDENTIDADE_DOCUMENTO_CAMERA_FRONTAL_ERROR"
    case backCamera = "VALIDAR_IDENTIDADE_DOCUMENTO_CAMERA_TRASEIRA"
    case backCameraSend = "VALIDAR_IDENTIDADE_DOCUMENTO_CAMERA_TRASEIRA_ENVIAR"
    case backCameraError = "VALIDAR_IDENTIDADE_DOCUMENTO_CAMERA_TRASEIRA_ERROR"
}

enum IdentityValidationStatusEventProperty: String, IdentityValidationPropertyValue {
    case processing = "VALIDAR_IDENTIDADE_STATUS_PROCESSING"
    case done = "VALIDAR_IDENTIDADE_STATUS_DONE"
    case onHold = "VALIDAR_IDENTIDADE_STATUS_ONHOLD"
}

enum IdentityValidationButtonNameProperty: String, IdentityValidationPropertyValue {
    case termsOfUse = "TERMOS_DE_USO"
}

enum IdentityValidationDialogNameProperty: String, IdentityValidationPropertyValue {
    case error = "ERROR"
}

extension String: IdentityValidationPropertyValue {
    var name: String { self }
}
