import AnalyticsModule
import Foundation

struct IdentityValidationTracker: AnalyticsKeyProtocol {
    var eventType: IdentityValidationEvent
    var eventProperties: [IdentityValidationEventProperty]

    private var properties: [String: Any] {
        eventProperties.reduce(into: [String: Any]()) { result, property in
            result[property.name.rawValue] = property.value.name
        }
    }

    private var name: String { eventType.name }

    private var providers: [AnalyticsProvider] {
        [.eventTracker]
    }

    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
