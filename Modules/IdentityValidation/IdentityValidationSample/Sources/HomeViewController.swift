import IdentityValidation
import UI

class HomeViewController: UIViewController {
    private var flowCoordinator: IDValidationCoordinatorProtocol?

    private lazy var button: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("Iniciar", for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTouchButton), for: .touchUpInside)
        return button
    }()
    
    init() {
        super.init(nibName: nil, bundle: nil)
        
        view.addSubview(button)
        
        button.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base04)
            $0.trailing.equalToSuperview().offset(-Spacing.base04)
        }

        title = "Identity Validation"
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    func didTouchButton() {
        guard let navigationController = navigationController else {
            return
        }

        flowCoordinator = IDValidationFlowCoordinator(from: navigationController,
                                                      token: "",
                                                      flow: "TFA",
                                                      completion: completionHandler)
        flowCoordinator?.start()
    }

    func completionHandler(_ status: IDValidationStatus?) {
        if let status = status {
            print("Finished with status: \(status)")
        } else {
            print("Finished without status")
        }
    }
}
