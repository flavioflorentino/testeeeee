@testable import Address
import Core
import XCTest

final class AddressListServiceMock: AddressListServicing {
    var getAddressListSuccess: Bool = false
    var removeAddressSuccess: Bool = false
    
    func getAddressList(completion: @escaping (Result<[Address], ApiError>) -> Void) {
        guard getAddressListSuccess else {
            completion(.failure(ApiError.timeout))
            return
        }
        
        do {
            let model = try JSONFileDecoder<[Address]>().load(resource: "addressList")
            completion(.success(model))
        } catch {
            completion(.failure(ApiError.connectionFailure))
        }
    }
    
    func removeAddress(id: Int, completion: @escaping (Result<Void, ApiError>) -> Void) {
        guard removeAddressSuccess else {
            completion(.failure(ApiError.timeout))
            return
        }
        
        completion(.success(()))
    }
}

final class AddressListPresenterSpy: AddressListPresenting {
    private(set) var callPresentLoadingAddressList = 0
    private(set) var callPresentReloadingAddressList = 0
    private(set) var callUpdateAddressList = 0
    private(set) var callAskAddressAction = 0
    private(set) var callPresenterError = 0
    private(set) var callDidNextStep = 0
    
    private(set) var model: [Address]?
    private(set) var errorTitle: String?
    private(set) var errorMessage: String?
    private(set) var action: AddressListAction?
    
    var viewController: AddressListDisplay?
    
    func presentLoadingAddressList() {
        callPresentLoadingAddressList += 1
    }
    
    func presentReloadingAddressList() {
        callPresentReloadingAddressList += 1
    }
    
    func askAddressAction() {
        callAskAddressAction += 1
    }
    
    func updateAddressList(model: [Address]) {
        callUpdateAddressList += 1
        self.model = model
    }
    
    func presenterError(title: String, message: String) {
        callPresenterError += 1
        errorTitle = title
        errorMessage = message
    }
    
    func didNextStep(action: AddressListAction) {
        callDidNextStep += 1
        self.action = action
    }
}

final class AddressListViewModelTest: XCTestCase {
    private let serviceMock = AddressListServiceMock()
    private let presenterSpy = AddressListPresenterSpy()
    
    private lazy var sut = AddressListViewModel(
        service: serviceMock,
        presenter: presenterSpy
    )
    
    func testUpdateView_ShouldCallStartLoadAddressList() {
        sut.updateView()
        
        XCTAssertEqual(presenterSpy.callPresentLoadingAddressList, 1)
    }
    
    func testUpdateView_WhenGetAddressListSuccess_ShouldCallStartLoadAddressList() {
        serviceMock.getAddressListSuccess = true
        sut.updateView()
        
        XCTAssertEqual(presenterSpy.callUpdateAddressList, 1)
        XCTAssertEqual(presenterSpy.model?.count, 2)
    }
    
    func testUpdateView_WhenGetAddressListFailure_ShouldCallPresenterError() {
        serviceMock.getAddressListSuccess = false
        sut.updateView()
        
        XCTAssertEqual(presenterSpy.callPresenterError, 1)
        XCTAssertEqual(presenterSpy.errorTitle, ApiError.timeout.requestError?.title)
        XCTAssertEqual(presenterSpy.errorMessage, ApiError.timeout.requestError?.message)
    }
    
    func testReloadAddress_ShouldCallStartReloadAddressList() {
        sut.reloadAddress()
        
        XCTAssertEqual(presenterSpy.callPresentReloadingAddressList, 1)
    }
    
    func testReloadAddress_WhenGetAddressListSuccess_ShouldCallStartLoadAddressList() {
        serviceMock.getAddressListSuccess = true
        sut.updateView()
        
        XCTAssertEqual(presenterSpy.callUpdateAddressList, 1)
        XCTAssertEqual(presenterSpy.model?.count, 2)
    }
    
    func testReloadAddress_WhenGetAddressListFailure_ShouldCallPresenterError() {
        serviceMock.getAddressListSuccess = false
        sut.updateView()
        
        XCTAssertEqual(presenterSpy.callPresenterError, 1)
        XCTAssertEqual(presenterSpy.errorTitle, ApiError.timeout.requestError?.title)
        XCTAssertEqual(presenterSpy.errorMessage, ApiError.timeout.requestError?.message)
    }
    
    func testEditAddress_WhenAddressValid_ShouldCallDidNextStep() {
        serviceMock.getAddressListSuccess = true
        
        sut.updateView()
        sut.didTapRow(index: IndexPath(row: 0, section: 0))
        sut.editAddress()
        
        XCTAssertEqual(presenterSpy.callDidNextStep, 1)
        XCTAssertEqual(presenterSpy.action, .openEditAddress(id: presenterSpy.model?[0].id ?? 0))
    }
    
    func testEditAddress_WhenAddressInvalid_ShouldCallPresenterError() {
        serviceMock.getAddressListSuccess = true
               
        sut.updateView()
        sut.editAddress()
        
        XCTAssertEqual(presenterSpy.callPresenterError, 1)
        XCTAssertEqual(presenterSpy.errorTitle, Strings.Error.title)
        XCTAssertEqual(presenterSpy.errorMessage, Strings.Error.notFoundAddress)
    }
    
    func testRemoveAddress_WhenAddressInvalid_ShouldCallPresenterError() {
        serviceMock.getAddressListSuccess = true
               
        sut.updateView()
        sut.removeAddress()
        
        XCTAssertEqual(presenterSpy.callPresenterError, 1)
        XCTAssertEqual(presenterSpy.errorTitle, Strings.Error.title)
        XCTAssertEqual(presenterSpy.errorMessage, Strings.Error.notFoundAddress)
    }
    
    func testRemoveAddress_WhenAddressValidAndRemoveAddressSuccess_ShouldCallUpdateAddressList() {
        serviceMock.getAddressListSuccess = true
        serviceMock.removeAddressSuccess = true
        
        sut.updateView()
        sut.didTapRow(index: IndexPath(row: 0, section: 0))
        sut.removeAddress()
        
        XCTAssertEqual(presenterSpy.callUpdateAddressList, 2)
        XCTAssertEqual(presenterSpy.model?.count, 1)
    }
    
    func testRemoveAddress_WhenAddressValidAndRemoveAddressSuccess_ShouldCallStartLoadAddressList() {
        serviceMock.getAddressListSuccess = true
        serviceMock.removeAddressSuccess = true
        
        sut.updateView()
        sut.didTapRow(index: IndexPath(row: 0, section: 0))
        sut.removeAddress()
        
        XCTAssertEqual(presenterSpy.callPresentLoadingAddressList, 2)
    }
    
    func testRemoveAddress_WhenAddressValidAndRemoveAddressFailure_ShouldCallPresenterError() {
        serviceMock.getAddressListSuccess = true
        serviceMock.removeAddressSuccess = false
        
        sut.updateView()
        sut.didTapRow(index: IndexPath(row: 0, section: 0))
        sut.removeAddress()
        
        XCTAssertEqual(presenterSpy.callPresenterError, 1)
        XCTAssertEqual(presenterSpy.errorTitle, ApiError.timeout.requestError?.title)
        XCTAssertEqual(presenterSpy.errorMessage, ApiError.timeout.requestError?.message)
    }
    
    func testDidTapRow_WhenAddressInvalid_ShouldCallPresenterError() {
        serviceMock.getAddressListSuccess = true
        
        sut.updateView()
        sut.didTapRow(index: IndexPath(row: 4, section: 0))
        
        XCTAssertEqual(presenterSpy.callPresenterError, 1)
        XCTAssertEqual(presenterSpy.errorTitle, Strings.Error.title)
        XCTAssertEqual(presenterSpy.errorMessage, Strings.Error.notFoundAddress)
    }
    
    func testDidTapRow_WhenAddressValid_ShouldCallAskAddressAction() {
        serviceMock.getAddressListSuccess = true
        
        sut.updateView()
        sut.didTapRow(index: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(presenterSpy.callAskAddressAction, 1)
    }
    
    func testOpenAddAddress_ShouldCallDidNextStep() {
        serviceMock.getAddressListSuccess = true
        
        sut.openAddAddress()
        
        XCTAssertEqual(presenterSpy.callDidNextStep, 1)
        XCTAssertEqual(presenterSpy.action, .openAddAddress)
    }
}
