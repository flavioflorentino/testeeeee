@testable import Address
import UI
import XCTest

final class AddressListDisplaySpy: AddressListDisplay {
    private(set) var callDisplayFooter = 0
    private(set) var callRemoveFooter = 0
    private(set) var callDisplayEmptyView = 0
    private(set) var callRemoveEmptyView = 0
    private(set) var callDisplayItems = 0
    private(set) var callShowScreenLoading = 0
    private(set) var callShowRefreshLoading = 0
    private(set) var callHideLoading = 0
    private(set) var callDisplayActionSheet = 0
    private(set) var callDisplayErrorPopup = 0

    private(set) var data: [AddressCell]?
    private(set) var errorTitle: String?
    private(set) var errorMessage: String?
    
    func displayFooter() {
        callDisplayFooter += 1
    }
    
    func removeFooter() {
        callRemoveFooter += 1
    }
    
    func displayEmptyView() {
        callDisplayEmptyView += 1
    }
    
    func removeEmptyView() {
        callRemoveEmptyView += 1
    }
    
    func displayItems(data: [AddressCell]) {
        callDisplayItems += 1
        self.data = data
    }
    
    func showScreenLoading() {
        callShowScreenLoading += 1
    }
    
    func showRefreshLoading() {
        callShowRefreshLoading += 1
    }
    
    func hideLoading() {
        callHideLoading += 1
    }
    
    func displayActionSheet() {
        callDisplayActionSheet += 1
    }
    
    func displayErrorPopup(title: String, message: String) {
        callDisplayErrorPopup += 1
        self.errorTitle = title
        self.errorMessage = message
    }
}

final class AddressListCoordinatorSpy: AddressListCoordinating {
    private(set) var callPerform = 0
    
    private(set) var action: AddressListAction?
    
    var viewController: UIViewController?
    
    func perform(action: AddressListAction) {
        callPerform += 1
        self.action = action
    }
}

class AddressListPresenterTest: XCTestCase {
    private let displaySpy = AddressListDisplaySpy()
    private let coordinatorSpy = AddressListCoordinatorSpy()
    
    private lazy var sut: AddressListPresenting = {
        let sut = AddressListPresenter(coordinator: coordinatorSpy)
        sut.viewController = displaySpy
        
        return sut
    }()
    
    private func createModel(isEmpty: Bool = false) throws -> [Address] {
        guard isEmpty else {
            return try JSONFileDecoder<[Address]>().load(resource: "addressList")
        }
        
        return []
    }
    
    func testPresentLoadingAddressList_ShouldCallShowScreenLoading() {
        sut.presentLoadingAddressList()
        
        XCTAssertEqual(displaySpy.callShowScreenLoading, 1)
    }
    
    func testPresentReloadingAddressList_ShouldCallShowRefreshLoading() {
        sut.presentReloadingAddressList()
        
        XCTAssertEqual(displaySpy.callShowRefreshLoading, 1)
    }
    
    func testAskAddressAction_ShouldCallDisplayActionSheet() {
        sut.askAddressAction()
        
        XCTAssertEqual(displaySpy.callDisplayActionSheet, 1)
    }
    
    func testPresenterError_ShouldCallHideLoading() {
        sut.presenterError(title: "Opss!!", message: "Error")
        
        XCTAssertEqual(displaySpy.callHideLoading, 1)
    }
    
    func testPresenterError_ShouldCallDisplayErrorPopup() {
        let title = "Opss!!"
        let message = "Error"
        sut.presenterError(title: title, message: message)
        
        XCTAssertEqual(displaySpy.callDisplayErrorPopup, 1)
        XCTAssertEqual(displaySpy.errorTitle, title)
        XCTAssertEqual(displaySpy.errorMessage, message)
    }
    
    func testDidNextStep_WhenActionOpenAddAddress_ShouldCallPerform() {
        sut.didNextStep(action: .openAddAddress)
        
        XCTAssertEqual(coordinatorSpy.callPerform, 1)
        XCTAssertEqual(coordinatorSpy.action, .openAddAddress)
    }
    
    func testDidNextStep_WhenActionOpenEditAddress_ShouldCallPerform() {
        sut.didNextStep(action: .openEditAddress(id: 10))
        
        XCTAssertEqual(coordinatorSpy.callPerform, 1)
        XCTAssertEqual(coordinatorSpy.action, .openEditAddress(id: 10))
    }
    
    func testUpdateAddressList_ShouldCallHideLoading() throws {
        let model = try XCTUnwrap(createModel())
        sut.updateAddressList(model: model)
        
        XCTAssertEqual(displaySpy.callHideLoading, 1)
    }
    
    func testUpdateAddressList_WhenItemFull_ShouldCallDisplayFooter() throws {
        let model = try XCTUnwrap(createModel(isEmpty: false))
        sut.updateAddressList(model: model)
        
        XCTAssertEqual(displaySpy.callDisplayFooter, 1)
    }
    
    func testUpdateAddressList_WhenItemFull_ShouldCallRemoveEmptyView() throws {
        let model = try XCTUnwrap(createModel(isEmpty: false))
        sut.updateAddressList(model: model)
        
        XCTAssertEqual(displaySpy.callRemoveEmptyView, 1)
    }
    
    func testUpdateAddressList_WhenItemEmpty_ShouldCallRemoveFooter() throws {
        let model = try XCTUnwrap(createModel(isEmpty: true))
        sut.updateAddressList(model: model)
        
        XCTAssertEqual(displaySpy.callRemoveFooter, 1)
    }
    
    func testUpdateAddressList_WhenItemEmpty_ShouldCallDisplayEmptyView() throws {
        let model = try XCTUnwrap(createModel(isEmpty: true))
        sut.updateAddressList(model: model)
        
        XCTAssertEqual(displaySpy.callDisplayEmptyView, 1)
    }
    
    func testUpdateAddressList_WhenItemEmpty_ShouldCallDisplayItems() throws {
        let model = try XCTUnwrap(createModel(isEmpty: true))
        sut.updateAddressList(model: model)
        
        XCTAssertEqual(displaySpy.callDisplayItems, 1)
        XCTAssertEqual(displaySpy.data?.count, 0)
    }
    
    func testUpdateAddressList_WhenItemFull_ShouldCallDisplayItems() throws {
        let model = try XCTUnwrap(createModel(isEmpty: false))
        sut.updateAddressList(model: model)
        
        XCTAssertEqual(displaySpy.callDisplayItems, 1)
        XCTAssertEqual(displaySpy.data?.count, 2)
    }
}
