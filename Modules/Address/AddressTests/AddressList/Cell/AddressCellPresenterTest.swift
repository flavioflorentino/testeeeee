@testable import Address
import XCTest

final class AddressCellPresenterDisplaySpy: AddressCellPresenterDisplay {
    private(set) var callDisplayTitle = 0
    private(set) var callDisplayDescription = 0
    private(set) var callDisplayImage = 0
    private(set) var callCheckboxIsHidden = 0
    private(set) var callDisplayCheckbox = 0
    
    private(set) var title: String?
    private(set) var description: String?
    private(set) var image: UIImage?
    private(set) var checkboxIsHidden: Bool?
    private(set) var checkboxImage: UIImage?
    
    func displayTitle(with text: String) {
        callDisplayTitle += 1
        self.title = text
    }
    
    func displayCheckbox(with image: UIImage, isHidden: Bool) {
        callDisplayCheckbox += 1
        checkboxImage = image
        checkboxIsHidden = isHidden
    }
    
    func displayDescription(with text: String) {
        callDisplayDescription += 1
        self.description = text
    }
    
    func displayImage(with image: UIImage) {
        callDisplayImage += 1
        self.image = image
    }
}

final class AddressCellPresenterTest: XCTestCase {
    private let viewSpy = AddressCellPresenterDisplaySpy()
    
    private func createModel(
        addressType: AddressType = .home,
        city: String = "Serra",
        isSelectable: Bool = false,
        isSelected: Bool = false
    ) -> AddressCell {
        AddressCell(isSelectable: isSelectable,
                    isSelected: isSelected,
                    addressType: addressType,
                    addressName: "addressName",
                    addressComplement: "addressComplement",
                    streetName: "streetName",
                    streetNumber: "72",
                    city: city,
                    state: "ES",
                    zipCode: "29168665")
    }
    
    private lazy var sut: AddressCellPresenting = {
        let sut = AddressCellPresenter()
        sut.view = viewSpy
        
        return sut
    }()
    
    func testSetup_ShouldCallDisplayTitle() {
        let model = createModel()
        sut.setup(model: model)
        
        XCTAssertEqual(viewSpy.callDisplayTitle, 1)
        XCTAssertEqual(viewSpy.title, model.addressName)
    }
    
    func testSetup_ShouldCallDisplayDescription() {
        let model = createModel()
        sut.setup(model: model)
        
        XCTAssertEqual(viewSpy.callDisplayDescription, 1)
        XCTAssertEqual(viewSpy.description, "streetName, 72, addressComplement - Serra, ES - 29168665")
    }
    
    func testSetup_WhenCityEmpty_ShouldCallDisplayDescription() {
        let model = createModel(city: "")
        sut.setup(model: model)
        
        XCTAssertEqual(viewSpy.callDisplayDescription, 1)
        XCTAssertEqual(viewSpy.description, "streetName, 72, addressComplement - ES - 29168665")
    }
    
    func testSetup_WhenHousingTypeIsHome_ShouldCallDisplayImage() {
        let model = createModel(addressType: .home)
        sut.setup(model: model)
        
        XCTAssertEqual(viewSpy.callDisplayImage, 1)
        XCTAssertEqual(viewSpy.image, Assets.icHome.image)
    }
    
    func testSetup_WhenHousingTypeIsBuilding_ShouldCallDisplayImage() {
        let model = createModel(addressType: .building)
        sut.setup(model: model)
        
        XCTAssertEqual(viewSpy.callDisplayImage, 1)
        XCTAssertEqual(viewSpy.image, Assets.icBuilding.image)
    }
    
    func testSetup_WhenHousingTypeIsMountain_ShouldCallDisplayImage() {
        let model = createModel(addressType: .mountain)
        sut.setup(model: model)
        
        XCTAssertEqual(viewSpy.callDisplayImage, 1)
        XCTAssertEqual(viewSpy.image, Assets.icMountain.image)
    }
    
    func testSetup_WhenIsSelectableTrue_ShouldCallCallDisplayCheckbox() {
        let model = createModel(isSelectable: true)
        sut.setup(model: model)
        
        XCTAssertEqual(viewSpy.callDisplayCheckbox, 1)
        XCTAssertEqual(viewSpy.checkboxIsHidden, false)
    }
    
    func testSetup_WhenIsSelectableFalse_ShouldCallCallDisplayCheckbox() {
        let model = createModel(isSelectable: false)
        sut.setup(model: model)
        
        XCTAssertEqual(viewSpy.callDisplayCheckbox, 1)
        XCTAssertEqual(viewSpy.checkboxIsHidden, true)
    }
    
    func testSetup_WhenIsSelectedTrue_ShouldCallDisplayCheckbox() {
        let model = createModel(isSelected: true)
        sut.setup(model: model)
        
        XCTAssertEqual(viewSpy.callDisplayCheckbox, 1)
        XCTAssertEqual(viewSpy.checkboxImage, Assets.icCheckboxEnable.image)
    }
    
    func testSetup_WhenIsSelectedFalse_ShouldCallDisplayCheckbox() {
        let model = createModel(isSelected: false)
        sut.setup(model: model)
        
        XCTAssertEqual(viewSpy.callDisplayCheckbox, 1)
        XCTAssertEqual(viewSpy.checkboxImage, Assets.icCheckboxDisable.image)
    }
}
