@testable import Address
import XCTest

final class SelectAddressCoordinatorSpy: SelectAddressCoordinating {
    private(set) var callPerform = 0
    
    private(set) var action: SelectAddressAction?
    
    var viewController: UIViewController?
    
    func perform(action: SelectAddressAction) {
        callPerform += 1
        self.action = action
    }
}

final class SelectAddressDisplaySpy: SelectAddressDisplay {
    private(set) var callDisplayFooter = 0
    private(set) var callRemoveHeader = 0
    private(set) var callDisplayHeader = 0
    private(set) var callDisplayItems = 0
    private(set) var callShowScreenLoading = 0
    private(set) var callShowRefreshLoading = 0
    private(set) var callHideLoading = 0
    private(set) var callDisplayErrorPopup = 0
    
    private(set) var headerTitle: String?
    private(set) var headerDescription: String?
    private(set) var headerImage: Bool?
    
    private(set) var data: [AddressCell]?
    
    private(set) var errorTitle: String?
    private(set) var errorMessage: String?
    
    func displayFooter() {
        callDisplayFooter += 1
    }
    
    func removeHeader() {
        callRemoveHeader += 1
    }
    
    func displayHeader(title: String, description: String, imageIsHidden: Bool) {
        callDisplayHeader += 1
        headerTitle = title
        headerDescription = description
        headerImage = imageIsHidden
    }
    
    func displayItems(data: [AddressCell]) {
        callDisplayItems += 1
        self.data = data
    }
    
    func showScreenLoading() {
        callShowScreenLoading += 1
    }
    
    func showRefreshLoading() {
        callShowRefreshLoading += 1
    }
    
    func hideLoading() {
        callHideLoading += 1
    }
    
    func displayErrorPopup(title: String, message: String) {
        callDisplayErrorPopup += 1
        errorTitle = title
        errorMessage = message
    }    
}

class SelectAddressPresenterTest: XCTestCase {
    private let displaySpy = SelectAddressDisplaySpy()
    private let coordinatorSpy = SelectAddressCoordinatorSpy()
    
    private lazy var sut: SelectAddressPresenting = {
        let sut = SelectAddressPresenter(coordinator: coordinatorSpy)
        sut.viewController = displaySpy
        
        return sut
    }()
    
    private func createModel() throws -> [Address] {
        try JSONFileDecoder<[Address]>().load(resource: "addressList")
    }
    
    func testPresentLoadingAddressList_ShouldCallShowScreenLoading() {
        sut.presentLoadingAddressList()
        
        XCTAssertEqual(displaySpy.callShowScreenLoading, 1)
    }
    
    func testPresentReloadingAddressList_ShouldCallShowRefreshLoading() {
        sut.presentReloadingAddressList()
        
        XCTAssertEqual(displaySpy.callShowRefreshLoading, 1)
    }
    
    func testPresenterError_ShouldCallHideLoading() {
        let title = "title"
        let message = "message"
        sut.presentError(title: title, message: message)
        
        XCTAssertEqual(displaySpy.callHideLoading, 1)
    }
    
    func testPresenterError_ShouldCallDisplayErrorPopup() {
        let title = "title"
        let message = "message"
        sut.presentError(title: title, message: message)
        
        XCTAssertEqual(displaySpy.callDisplayErrorPopup, 1)
        XCTAssertEqual(displaySpy.errorTitle, title)
        XCTAssertEqual(displaySpy.errorMessage, message)
    }
    
    func testUpdateAddressList_ShouldCallHideLoading() throws {
        let model = try XCTUnwrap(createModel())
        sut.updateAddressList(data: model, selectedId: nil, header: nil)
        
        XCTAssertEqual(displaySpy.callHideLoading, 1)
    }
    
    func testUpdateAddressList_ShouldCallDisplayFooter() throws {
        let model = try XCTUnwrap(createModel())
        sut.updateAddressList(data: model, selectedId: nil, header: nil)
        
        XCTAssertEqual(displaySpy.callDisplayFooter, 1)
    }
    
    func testUpdateAddressList_WhenHeaderNil_ShouldCallRemoveHeader() throws {
        let model = try XCTUnwrap(createModel())
        sut.updateAddressList(data: model, selectedId: nil, header: nil)
        
        XCTAssertEqual(displaySpy.callRemoveHeader, 1)
    }
    
    func testUpdateAddressList_WhenHeaderNotNil_ShouldCallRemoveHeader() throws {
        let model = try XCTUnwrap(createModel())
        let header = SelectAddress.Header(showImage: true, title: "title", description: "description")
        sut.updateAddressList(data: model, selectedId: nil, header: header)
        
        XCTAssertEqual(displaySpy.callDisplayHeader, 1)
        XCTAssertEqual(displaySpy.headerTitle, header.title)
        XCTAssertEqual(displaySpy.headerDescription, header.description)
        XCTAssertEqual(displaySpy.headerImage, !header.showImage)
    }
    
    func testUpdateAddressList_WhenSelectedIdNil_ShouldCallDisplayItems() throws {
        let model = try XCTUnwrap(createModel())
        sut.updateAddressList(data: model, selectedId: nil, header: nil)
        
        XCTAssertEqual(displaySpy.callDisplayItems, 1)
        XCTAssertEqual(displaySpy.data?.count, 2)
        XCTAssertEqual(displaySpy.data?.first?.isSelected, false)
    }
    
    func testUpdateAddressList_WhenSelectedIdNotNil_ShouldCallDisplayItems() throws {
        let model = try XCTUnwrap(createModel())
        sut.updateAddressList(data: model, selectedId: 2, header: nil)
        
        XCTAssertEqual(displaySpy.callDisplayItems, 1)
        XCTAssertEqual(displaySpy.data?.count, 2)
        XCTAssertEqual(displaySpy.data?.first?.isSelected, true)
    }
    
    func testDidNextStep_WhenActionOpenAddAddress_ShouldCallPerform() {
        sut.didNextStep(action: .openAddAddress)
        
        XCTAssertEqual(coordinatorSpy.callPerform, 1)
        switch coordinatorSpy.action {
        case .openAddAddress:
            XCTAssert(true)
        default:
            XCTFail("Not Call OpenAddAddress")
        }
    }
    
    func testDidNextStep_WhenActionAddressSelected_ShouldCallPerform() throws {
        let model = try XCTUnwrap(createModel())
        sut.didNextStep(action: .addressSelected(address: model[0]))
        
        XCTAssertEqual(coordinatorSpy.callPerform, 1)
        switch coordinatorSpy.action {
        case .addressSelected(let address):
            XCTAssertNotNil(address)
        default:
            XCTFail("Not Call AddressSelected")
        }
    }
}
