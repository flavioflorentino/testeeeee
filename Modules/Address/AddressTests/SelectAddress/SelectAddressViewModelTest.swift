@testable import Address
import Core
import XCTest

final class SelectAddressServiceMock: SelectAddressServicing {
    var getAddressListSuccess: Bool = false
    
    func getAddressList(completion: @escaping (Result<[Address], ApiError>) -> Void) {
        guard getAddressListSuccess else {
            completion(.failure(ApiError.timeout))
            return
        }
        
        do {
            let model = try JSONFileDecoder<[Address]>().load(resource: "addressList")
            completion(.success(model))
        } catch {
            completion(.failure(ApiError.connectionFailure))
        }
    }
}

final class SelectAddressPresenterSpy: SelectAddressPresenting {
    private(set) var callPresentLoadingAddressList = 0
    private(set) var callPresentReloadingAddressList = 0
    private(set) var callUpdateAddressList = 0
    private(set) var callPresentError = 0
    private(set) var callDidNextStep = 0
    
    private(set) var data: [Address]?
    private(set) var selectedId: Int?
    private(set) var header: SelectAddress.Header?
    private(set) var errorTitle: String?
    private(set) var errorMessage: String?
    private(set) var action: SelectAddressAction?
    
    var viewController: SelectAddressDisplay?
    
    func presentLoadingAddressList() {
        callPresentLoadingAddressList += 1
    }
    
    func presentReloadingAddressList() {
        callPresentReloadingAddressList += 1
    }
    
    func updateAddressList(data: [Address], selectedId: Int?, header: SelectAddress.Header?) {
        callUpdateAddressList += 1
        self.data = data
        self.selectedId = selectedId
        self.header = header
    }
    
    func presentError(title: String, message: String) {
        callPresentError += 1
        errorTitle = title
        errorMessage = message
    }
    
    func didNextStep(action: SelectAddressAction) {
        callDidNextStep += 1
        self.action = action
    }
}

class SelectAddressViewModelTest: XCTestCase {
    private let serviceMock = SelectAddressServiceMock()
    private let presenterSpy = SelectAddressPresenterSpy()
    
    private lazy var sut = SelectAddressViewModel(
        model: createModel(),
        service: serviceMock,
        presenter: presenterSpy
    )
    
    private func createModel() -> SelectAddress {
        SelectAddress(selectedId: 0, header: SelectAddress.Header(showImage: true,
                                                                  title: "title",
                                                                  description: "description"))
    }
    
    func testUpdateView_ShouldCallStartLoadAddressList() {
        sut.updateView()
        
        XCTAssertEqual(presenterSpy.callPresentLoadingAddressList, 1)
    }
    
    func testUpdateView_WhenGetAddressListSuccess_ShouldCallStartLoadAddressList() {
        serviceMock.getAddressListSuccess = true
        sut.updateView()
        
        XCTAssertEqual(presenterSpy.callUpdateAddressList, 1)
        XCTAssertEqual(presenterSpy.data?.count, 2)
        XCTAssertEqual(presenterSpy.selectedId, 0)
        XCTAssertEqual(presenterSpy.header?.title, "title")
        XCTAssertEqual(presenterSpy.header?.description, "description")
        XCTAssertEqual(presenterSpy.header?.showImage, true)
    }
    
    func testUpdateView_WhenGetAddressListFailure_ShouldCallPresentError() {
        serviceMock.getAddressListSuccess = false
        sut.updateView()
        
        XCTAssertEqual(presenterSpy.callPresentError, 1)
        XCTAssertEqual(presenterSpy.errorTitle, ApiError.timeout.requestError?.title)
        XCTAssertEqual(presenterSpy.errorMessage, ApiError.timeout.requestError?.message)
    }
    
    func testReloadAddress_ShouldCallStartReloadAddressList() {
        sut.reloadAddress()
        
        XCTAssertEqual(presenterSpy.callPresentReloadingAddressList, 1)
    }
    
    func testReloadAddress_WhenGetAddressListSuccess_ShouldCallStartLoadAddressList() {
        serviceMock.getAddressListSuccess = true
        sut.updateView()
        
        XCTAssertEqual(presenterSpy.callUpdateAddressList, 1)
        XCTAssertEqual(presenterSpy.data?.count, 2)
        XCTAssertEqual(presenterSpy.selectedId, 0)
        XCTAssertEqual(presenterSpy.header?.title, "title")
        XCTAssertEqual(presenterSpy.header?.description, "description")
        XCTAssertEqual(presenterSpy.header?.showImage, true)
    }
    
    func testReloadAddress_WhenGetAddressListFailure_ShouldCallPresentError() {
        serviceMock.getAddressListSuccess = false
        sut.updateView()
        
        XCTAssertEqual(presenterSpy.callPresentError, 1)
        XCTAssertEqual(presenterSpy.errorTitle, ApiError.timeout.requestError?.title)
        XCTAssertEqual(presenterSpy.errorMessage, ApiError.timeout.requestError?.message)
    }
    
    func testOpenAddAddress_WhenGetAddressListFailure_ShouldCallPresenterError() {
        sut.openAddAddress()
        
        XCTAssertEqual(presenterSpy.callDidNextStep, 1)
        switch presenterSpy.action {
        case .openAddAddress:
            XCTAssert(true)
        default:
            XCTFail("Not Call OpenAddAddress")
        }
    }
    
    func testDidTapRow_WhenAddressInvalid_ShouldCallPresentError() {
        serviceMock.getAddressListSuccess = true
        
        sut.updateView()
        sut.didTapRow(index: IndexPath(row: 4, section: 0))
        
        XCTAssertEqual(presenterSpy.callPresentError, 1)
        XCTAssertEqual(presenterSpy.errorTitle, Strings.Error.title)
        XCTAssertEqual(presenterSpy.errorMessage, Strings.Error.notFoundAddress)
    }
    
    func testDidTapRow_WhenAddressValid_ShouldCallAskAddressAction() {
        serviceMock.getAddressListSuccess = true
        
        sut.updateView()
        sut.didTapRow(index: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(presenterSpy.callDidNextStep, 1)
        switch presenterSpy.action {
        case .addressSelected(let address):
            XCTAssertNotNil(address)
        default:
            XCTFail("Not Call Address Selected")
        }
    }
}
