@testable import Address
import XCTest

private struct ItemSearchableSpy: ItemSearchble {
    func getId() -> Int { 1 }
    func getSelectionValue() -> String { "selectionValue" }
    func getTitle() -> String { "titleValue" }
}

private class SelectRegionCoordinatingSpy: SelectRegionCoordinating {
    var viewController: UIViewController?
    var didSelectRegionAction: (ItemSearchble) -> Void = { _ in }
    
    // MARK: - didSelect
    private(set) var didSelectModelCallsCount = 0
    private(set) var didSelectModelReceivedInvocations: [ItemSearchble] = []
    
    func didSelect(model: ItemSearchble) {
        didSelectModelCallsCount += 1
        didSelectModelReceivedInvocations.append(model)
    }
}

private class SelectRegionDisplaySpy: SelectRegionDisplay {
    // MARK: - displayTitle
    private(set) var displayTitleTitleCallsCount = 0
    private(set) var displayTitleTitleReceivedInvocations: [String] = []
    
    func displayTitle(title: String) {
        displayTitleTitleCallsCount += 1
        displayTitleTitleReceivedInvocations.append(title)
    }
    
    // MARK: - displaySearchBarTitle
    private(set) var displaySearchBarTitleTitleCallsCount = 0
    private(set) var displaySearchBarTitleTitleReceivedInvocations: [String] = []
    
    func displaySearchBarTitle(title: String) {
        displaySearchBarTitleTitleCallsCount += 1
        displaySearchBarTitleTitleReceivedInvocations.append(title)
    }
    
    // MARK: - showScreenLoading
    private(set) var showScreenLoadingCallsCount = 0
    
    func showScreenLoading() {
        showScreenLoadingCallsCount += 1
    }
    
    // MARK: - displayItems
    private(set) var displayItemsDataCallsCount = 0
    private(set) var displayItemsDataReceivedInvocations: [[ItemSearchble]] = []
    
    func displayItems(data: [ItemSearchble]) {
        displayItemsDataCallsCount += 1
        displayItemsDataReceivedInvocations.append(data)
    }
    
    // MARK: - enableSearchBar
    private(set) var enableSearchBarCallsCount = 0
    
    func enableSearchBar() {
        enableSearchBarCallsCount += 1
    }
    
    // MARK: - hideLoading
    private(set) var hideLoadingCallsCount = 0
    
    func hideLoading() {
        hideLoadingCallsCount += 1
    }
    
    // MARK: - displayErrorPopup
    private(set) var displayErrorPopupTitleMessageCallsCount = 0
    private(set) var displayErrorPopupTitleMessageReceivedInvocations: [(title: String, message: String)] = []
    
    func displayErrorPopup(title: String, message: String) {
        displayErrorPopupTitleMessageCallsCount += 1
        displayErrorPopupTitleMessageReceivedInvocations.append((title: title, message: message))
    }
}

class SelectRegionPresenterTest: XCTestCase {
    private let displaySpy = SelectRegionDisplaySpy()
    private let coordinatorSpy = SelectRegionCoordinatingSpy()
    
    private lazy var sut: SelectRegionPresenting = {
        let sut = SelectRegionPresenter(coordinator: coordinatorSpy)
        sut.viewController = displaySpy
        
        return sut
    }()
    
    func testPresentUpdateTitle_WhenPassTitle_ShouldCallDisplayTitle() {
        let newTitle = "New Title"
        sut.updateTitle(title: newTitle)
        
        XCTAssertEqual(displaySpy.displayTitleTitleCallsCount, 1)
        XCTAssertEqual(displaySpy.displayTitleTitleReceivedInvocations.last, newTitle)
    }
    
    func testPresentUpdateSearchBarTitle_WhenPassTitle_ShouldCallDisplaySearchBarTitle() {
        let newTitle = "New Search Bar Title"
        sut.updateSearchBarTitle(title: newTitle)
        
        XCTAssertEqual(displaySpy.displaySearchBarTitleTitleCallsCount, 1)
        XCTAssertEqual(displaySpy.displaySearchBarTitleTitleReceivedInvocations.last, newTitle)
    }
    
    func testDidSelect_WhenPassModel_ShouldCallDidSelectItem() {
        let itemSpy = ItemSearchableSpy()
        sut.didSelect(model: itemSpy)
        
        XCTAssertEqual(coordinatorSpy.didSelectModelCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.didSelectModelReceivedInvocations.last?.getId(), itemSpy.getId())
        XCTAssertEqual(coordinatorSpy.didSelectModelReceivedInvocations.last?.getTitle(), itemSpy.getTitle())
        XCTAssertEqual(coordinatorSpy.didSelectModelReceivedInvocations.last?.getSelectionValue(), itemSpy.getSelectionValue())
    }
    
    func testEnableSearch_ShouldCallEnableSearchBar() {
        sut.enableSearch()
        XCTAssertEqual(displaySpy.enableSearchBarCallsCount, 1)
    }
    
    func testUpdateDataList_WhenPassListOfData_ShouldDisplayItems() {
        let itemSpy1 = ItemSearchableSpy()
        let itemSpy2 = ItemSearchableSpy()
        sut.updateDataList(data: [itemSpy1, itemSpy2])
        
        XCTAssertEqual(displaySpy.displayItemsDataCallsCount, 1)
        XCTAssertEqual(displaySpy.displayItemsDataReceivedInvocations.last?.count, 2)
    }
    
    func testPresentErro_WhenPassTitleAndMessage_ShouldDisplayErrorPopup() {
        let title = "title 1"
        let message = "message 1"
        sut.presentError(title: title, message: message)
        
        XCTAssertEqual(displaySpy.displayErrorPopupTitleMessageCallsCount, 1)
        XCTAssertEqual(displaySpy.displayErrorPopupTitleMessageReceivedInvocations.last?.title, title)
        XCTAssertEqual(displaySpy.displayErrorPopupTitleMessageReceivedInvocations.last?.message, message)
    }
}
