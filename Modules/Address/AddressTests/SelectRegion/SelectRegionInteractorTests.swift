@testable import Address
import Core
import XCTest

private class SelectCityServicingMock: SelectCityServicing {
    var getCityListSuccess: Bool = false
    
    func getCities(uf: String, completion: @escaping (Result<[City], ApiError>) -> Void) {
        guard getCityListSuccess else {
            completion(.failure(ApiError.timeout))
            return
        }
        
        let city1 = City(id: 1, type: "", name: "Recife", abbreviation: "abbreviation")
        let city2 = City(id: 2, type: "", name: "São Paulo", abbreviation: "abbreviation")
        let listCities = [city1, city2]
        
        completion(.success(listCities))
    }
}

private class CityRegionSpy: RegionFactory<City> {
    var uf = "SP"
    let service: SelectCityServicing
    
    init(service: SelectCityServicing, dependencies: AddressDependencies = DependencyContainer()) {
        self.service = service
        
        super.init(dependencies: dependencies)
    }
    
    override func getScreenTitle() -> String {
        Strings.SelectCity.title
    }
    
    override func getSearchBarTitle() -> String {
        Strings.SelectRegion.Search.placeholder
    }
    
    override func getData(completion: @escaping (Result<[City], ApiError>) -> Void) {
        service.getCities(uf: uf, completion: completion)
    }
}

private class SelectRegionPresentingSpy: SelectRegionPresenting {
    var viewController: SelectRegionDisplay?
    
    // MARK: - updateTitle
    private(set) var updateTitleTitleCallsCount = 0
    private(set) var updateTitleTitleReceivedInvocations: [String] = []
    
    func updateTitle(title: String) {
        updateTitleTitleCallsCount += 1
        updateTitleTitleReceivedInvocations.append(title)
    }
    
    // MARK: - updateSearchBarTitle
    private(set) var updateSearchBarTitleTitleCallsCount = 0
    private(set) var updateSearchBarTitleTitleReceivedInvocations: [String] = []
    
    func updateSearchBarTitle(title: String) {
        updateSearchBarTitleTitleCallsCount += 1
        updateSearchBarTitleTitleReceivedInvocations.append(title)
    }
    
    // MARK: - presentLoadingDataList
    private(set) var presentLoadingDataListCallsCount = 0
    
    func presentLoadingDataList() {
        presentLoadingDataListCallsCount += 1
    }
    
    // MARK: - enableSearch
    private(set) var enableSearchCallsCount = 0
    
    func enableSearch() {
        enableSearchCallsCount += 1
    }
    
    // MARK: - updateDataList
    private(set) var updateDataListDataCallsCount = 0
    private(set) var updateDataListDataReceivedInvocations: [[ItemSearchble]] = []
    
    func updateDataList(data: [ItemSearchble]) {
        updateDataListDataCallsCount += 1
        updateDataListDataReceivedInvocations.append(data)
    }
    
    // MARK: - presentError
    private(set) var presentErrorTitleMessageCallsCount = 0
    private(set) var presentErrorTitleMessageReceivedInvocations: [(title: String, message: String)] = []
    
    func presentError(title: String, message: String) {
        presentErrorTitleMessageCallsCount += 1
        presentErrorTitleMessageReceivedInvocations.append((title: title, message: message))
    }
    
    // MARK: - didSelect
    private(set) var didSelectModelCallsCount = 0
    private(set) var didSelectModelReceivedInvocations: [ItemSearchble] = []
    
    func didSelect(model: ItemSearchble) {
        didSelectModelCallsCount += 1
        didSelectModelReceivedInvocations.append(model)
    }
}


class SelectRegionInteractorTest: XCTestCase {
    private let seviceMock = SelectCityServicingMock()
    private lazy var cityRegionSpy = CityRegionSpy(service: seviceMock)
    private let presenterSpy = SelectRegionPresentingSpy()
    
    private lazy var sut = SelectRegionInteractor(
        regionType: cityRegionSpy, presenter: presenterSpy
    )
    
    func testConfigureViews_ShouldPresentTitleAndSearchBarTitle() {
        sut.configureView()
        
        XCTAssertEqual(presenterSpy.updateTitleTitleCallsCount, 1)
        XCTAssertEqual(presenterSpy.updateSearchBarTitleTitleCallsCount, 1)
    }
    
    func testDidSelect_WhenPassCity_ShouldCallDidSelectMode() {
        let cityName = "city test"
        sut.didSelect(model: City(id: 1, type: "", name: cityName, abbreviation: "abbreviation"))
        
        XCTAssertEqual(presenterSpy.didSelectModelCallsCount, 1)
        XCTAssertEqual(presenterSpy.didSelectModelReceivedInvocations.last?.getTitle(), cityName)
    }
    
    func testRequestData_ShouldCallUpdateDataList() throws {
        seviceMock.getCityListSuccess = true
        sut.requestData()
        
        XCTAssertEqual(presenterSpy.updateDataListDataCallsCount, 1)
        XCTAssertEqual(presenterSpy.enableSearchCallsCount, 1)
        let cities = try XCTUnwrap(presenterSpy.updateDataListDataReceivedInvocations.last)
        XCTAssertEqual(cities.count, 2)
    }
    
    func testFilterData_WhenPassSomeValidInputText_ShouldCallUpdateDataList() throws {
        seviceMock.getCityListSuccess = true
        sut.requestData()
        let inputText = "Recife"
        sut.filterData(with: inputText)
        
        let cities = try  XCTUnwrap(presenterSpy.updateDataListDataReceivedInvocations.last)
        XCTAssertEqual(cities.count, 1)
        XCTAssertEqual(cities.first?.getTitle(), inputText)
    }
    
    func testFilterData_WhenPassEmptyInputText_ShouldCallUpdateDataList() throws {
        seviceMock.getCityListSuccess = true
        sut.requestData()
        let inputText = ""
        sut.filterData(with: inputText)
        
        let cities = try  XCTUnwrap(presenterSpy.updateDataListDataReceivedInvocations.last)
        XCTAssertEqual(cities.count, 2)
    }
}
