import Foundation

// swiftlint:disable convenience_type
final class AddressResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: AddressResources.self).url(forResource: "AddressResources", withExtension: "bundle") else {
            return Bundle(for: AddressResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: AddressResources.self)
    }()
}
