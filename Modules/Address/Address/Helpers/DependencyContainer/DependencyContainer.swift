import AnalyticsModule
import Core
import Foundation

typealias AddressDependencies = HasNoDependency & HasMainQueue & HasAnalytics

final class DependencyContainer: AddressDependencies {
    lazy var mainQueue = DispatchQueue.main
    lazy var analytics: AnalyticsProtocol = Analytics.shared
}
