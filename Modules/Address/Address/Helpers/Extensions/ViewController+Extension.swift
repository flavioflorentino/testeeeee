import UI

extension UIViewController {
    func showErrorPopup(
        title: String,
        message: String,
        buttonTitle: String = Strings.Button.ok,
        closeCompletion: (() -> Void)? = nil
    ) {
        let popupViewController = PopupViewController(
            title: title,
            description: message,
            preferredType: .image(Assets.icSad.image)
        )
        
        let confirmAction = PopupAction(title: buttonTitle,
                                        style: .fill,
                                        completion: closeCompletion)
        popupViewController.didCloseDismiss = closeCompletion
        popupViewController.addAction(confirmAction)
        
        showPopup(popupViewController)
    }
}
