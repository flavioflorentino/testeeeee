import Foundation
import UI
import UIKit

private extension SelectAddressView.Layout {
    enum Size {
        static let image = CGSize(width: 80, height: 80)
    }
}

class SelectAddressView: UIView, ViewConfiguration {
    fileprivate enum Layout { }
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Spacing.base01
        stackView.axis = .vertical

        return stackView
    }()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.icHome.image
        imageView.contentMode = .scaleAspectFit
        
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.text = Strings.EmptyAddress.addAddress
        label
            .labelStyle(TitleLabelStyle(type: .large))
            .with(\.textColor, .grayscale750())
        label.numberOfLines = 2
        
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.text = Strings.EmptyAddress.explanationeUsed
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale400())

        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundSecondary.color
    }
    
    func buildViewHierarchy() {
        stackView.addArrangedSubview(imageView)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(descriptionLabel)
        
        addSubview(stackView)
    }
    
    func setupConstraints() {
        imageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.image)
        }
        
        stackView.snp.makeConstraints {
            $0.top.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.bottom.equalToSuperview().offset(-Spacing.base02)
        }
    }
    
    func configureView(title: String, description: String, imageIsHidden: Bool) {
        imageView.isHidden = imageIsHidden
        titleLabel.text = title
        descriptionLabel.text = description
    }
}
