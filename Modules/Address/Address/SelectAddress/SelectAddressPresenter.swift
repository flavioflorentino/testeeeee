import Foundation

protocol SelectAddressPresenting: AnyObject {
    var viewController: SelectAddressDisplay? { get set }
    func presentLoadingAddressList()
    func presentReloadingAddressList()
    func updateAddressList(data: [Address], selectedId: Int?, header: SelectAddress.Header?)
    func presentError(title: String, message: String)
    func didNextStep(action: SelectAddressAction)
}

final class SelectAddressPresenter {
    private let coordinator: SelectAddressCoordinating
    weak var viewController: SelectAddressDisplay?

    init(coordinator: SelectAddressCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - SelectAddressPresenting
extension SelectAddressPresenter: SelectAddressPresenting {
    func presentLoadingAddressList() {
        viewController?.showScreenLoading()
    }
    
    func presentReloadingAddressList() {
        viewController?.showRefreshLoading()
    }
    
    func updateAddressList(data: [Address], selectedId: Int?, header: SelectAddress.Header?) {
        viewController?.hideLoading()
        viewController?.displayFooter()
        configureHeader(header)
        updateItems(model: data, selectedId: selectedId)
    }
    
    func presentError(title: String, message: String) {
        viewController?.hideLoading()
        viewController?.displayErrorPopup(title: title, message: message)
    }
    
    func didNextStep(action: SelectAddressAction) {
        coordinator.perform(action: action)
    }
    
    private func updateItems(model: [Address], selectedId: Int?) {
        let cells = model.map {
            AddressCell(isSelectable: true,
                        isSelected: $0.id == selectedId,
                        addressType: $0.addressType,
                        addressName: $0.addressName,
                        addressComplement: $0.addressComplement,
                        streetName: $0.streetName,
                        streetNumber: $0.streetNumber,
                        city: $0.city,
                        state: $0.state,
                        zipCode: $0.zipCode)
        }
        
        viewController?.displayItems(data: cells)
    }
    
    private func configureHeader(_ header: SelectAddress.Header?) {
        guard let header = header else {
            viewController?.removeHeader()
            return
        }
        
        viewController?.displayHeader(title: header.title, description: header.description, imageIsHidden: !header.showImage)
    }
}
