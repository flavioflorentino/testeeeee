import Core
import Foundation

protocol SelectAddressServicing {
    func getAddressList(completion: @escaping (Result<[Address], ApiError>) -> Void)
}

final class SelectAddressService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - SelectAddressServicing
extension SelectAddressService: SelectAddressServicing {
    func getAddressList(completion: @escaping (Result<[Address], ApiError>) -> Void) {
        let api = Api<[Address]>(endpoint: AddressEndpoint.consumerAddress)
        
        api.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
