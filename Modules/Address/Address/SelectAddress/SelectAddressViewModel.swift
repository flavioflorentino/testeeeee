import Core
import Foundation

protocol SelectAddressViewModelInputs: AnyObject {
    func updateView()
    func openAddAddress()
    func reloadAddress()
    func didTapRow(index: IndexPath)
}

final class SelectAddressViewModel {
    private let service: SelectAddressServicing
    private let presenter: SelectAddressPresenting
    private let model: SelectAddress
    private var adresses: [Address]
    
    init(model: SelectAddress, service: SelectAddressServicing, presenter: SelectAddressPresenting) {
        self.model = model
        self.service = service
        self.presenter = presenter
        self.adresses = []
    }
}

// MARK: - SelectAddressViewModelInputs
extension SelectAddressViewModel: SelectAddressViewModelInputs {
    func updateView() {
        presenter.presentLoadingAddressList()
        loadAddressList()
    }
    
    func reloadAddress() {
        presenter.presentReloadingAddressList()
        loadAddressList()
    }
    
    func openAddAddress() {
        presenter.didNextStep(action: .openAddAddress)
    }
    
    func didTapRow(index: IndexPath) {
        let row = index.row
        guard adresses.indices.contains(row) else {
            errorNotFoundAdrress()
            return
        }
        
        presenter.didNextStep(action: .addressSelected(address: adresses[row]))
    }
    
    private func loadAddressList() {
        service.getAddressList { [weak self] result in
            switch result {
            case .success(let adresses):
                self?.adresses = adresses
                self?.requestSuccess()
            case .failure(let error):
                self?.requestError(error: error)
            }
        }
    }
    
    private func errorNotFoundAdrress() {
        presenter.presentError(title: Strings.Error.title, message: Strings.Error.notFoundAddress)
    }
    
    private func requestSuccess() {
        presenter.updateAddressList(data: adresses, selectedId: model.selectedId, header: model.header)
    }
    
    private func requestError(error: ApiError) {
        let requestError = error.requestError ?? RequestError()
        presenter.presentError(title: requestError.title, message: requestError.message)
    }
}
