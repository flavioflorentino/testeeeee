import UIKit

protocol SelectAddressCoordinatorDelegate: AnyObject {
    func onAddressSelected(_ address: Address)
    func onNewAddressAdded(_ address: Address)
}

enum SelectAddressAction {
    case openAddAddress
    case addressSelected(address: Address)
}

protocol SelectAddressCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: SelectAddressAction)
}

final class SelectAddressCoordinator {
    private weak var delegate: SelectAddressCoordinatorDelegate?
    weak var viewController: UIViewController?
    
    init(delegate: SelectAddressCoordinatorDelegate?) {
        self.delegate = delegate
    }
}

// MARK: - SelectAddressCoordinating
extension SelectAddressCoordinator: SelectAddressCoordinating {
    func perform(action: SelectAddressAction) {
        // TODO: Open AddAddress
    }
}
