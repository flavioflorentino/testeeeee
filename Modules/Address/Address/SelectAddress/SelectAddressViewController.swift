import UI
import UIKit

protocol SelectAddressDisplay: AnyObject {
    func displayFooter()
    func removeHeader()
    func displayHeader(title: String, description: String, imageIsHidden: Bool)
    func displayItems(data: [AddressCell])
    func showScreenLoading()
    func showRefreshLoading()
    func hideLoading()
    func displayErrorPopup(title: String, message: String)
}

private extension SelectAddressViewController.Layout {
    enum Size {
        static let estimatedRowHeight: CGFloat = 80.0
    }
}

final class SelectAddressViewController: ViewController<SelectAddressViewModelInputs, UIView>, LoadingViewProtocol {
    fileprivate enum Layout { }

    lazy var loadingView = LoadingView()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorInset = .zero
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.estimatedRowHeight = Layout.Size.estimatedRowHeight
        tableView.rowHeight = UITableView.automaticDimension
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        tableView.register(
            AddressCellView.self,
            forCellReuseIdentifier: String(describing: AddressCellView.self)
        )

        return tableView
    }()
    
    private lazy var tableViewHeader = SelectAddressView()
    
    private lazy var tableViewFooter: AddNewAddressView = {
        let frame = CGRect(origin: .zero, size: CGSize(width: tableView.frame.size.width, height: Layout.Size.estimatedRowHeight))
        let footer = AddNewAddressView(frame: frame)
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapFooter))
        footer.addGestureRecognizer(gestureRecognizer)
        
        return footer
    }()
    
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(didPullRefresh), for: .valueChanged)
        
        return refreshControl
    }()
    
    private lazy var dataSource: TableViewDataSource<Int, AddressCell> = {
        let dataSource = TableViewDataSource<Int, AddressCell>(view: tableView)
        dataSource.add(section: 0)
        dataSource.itemProvider = { tableView, indexPath, item -> UITableViewCell? in
            let cellIdentifier = String(describing: AddressCellView.self)
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? AddressCellView
            cell?.setupView(model: item)
            
            return cell
        }
        
        return dataSource
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.updateView()
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundSecondary.color
        tableView.dataSource = dataSource
    }
    
    override func buildViewHierarchy() {
        tableView.addSubview(refreshControl)
        view.addSubview(tableView)
        tableView.tableHeaderView = tableViewHeader
    }
    
    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        tableViewHeader.snp.makeConstraints {
            $0.top.equalTo(tableView)
            $0.leading.trailing.equalTo(tableView).inset(Spacing.base02)
            $0.centerX.equalTo(tableView)
            $0.height.greaterThanOrEqualTo(Layout.Size.estimatedRowHeight)
        }
    }
}

@objc
private extension SelectAddressViewController {
    func didTapFooter() {
        viewModel.openAddAddress()
    }
    
    func didPullRefresh() {
        viewModel.reloadAddress()
    }
}

// MARK: SelectAddressDisplay
extension SelectAddressViewController: SelectAddressDisplay {
    func displayFooter() {
        tableView.tableFooterView = tableViewFooter
    }
    
    func removeHeader() {
        tableView.tableHeaderView = nil
    }
    
    func displayHeader(title: String, description: String, imageIsHidden: Bool) {
        tableViewHeader.configureView(title: title, description: description, imageIsHidden: imageIsHidden)
        view.layoutIfNeeded()
        tableView.reloadData()
    }
    
    func displayItems(data: [AddressCell]) {
        dataSource.update(items: data, from: 0)
    }
    
    func showScreenLoading() {
        startLoadingView()
    }
    
    func showRefreshLoading() {
        refreshControl.beginRefreshing()
    }
    
    func hideLoading() {
        stopLoadingView()
        refreshControl.endRefreshing()
    }
    
    func displayErrorPopup(title: String, message: String) {
        showErrorPopup(title: title, message: message)
    }
}

extension SelectAddressViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        viewModel.didTapRow(index: indexPath)
    }
}

extension SelectAddressViewController: AddressListEmptyViewDelegate {
    func didTapAddAddress() {
        viewModel.openAddAddress()
    }
}
