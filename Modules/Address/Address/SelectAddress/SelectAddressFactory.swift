import Foundation
import UIKit

enum SelectAddressFactory {
    static func make(model: SelectAddress, delegate: SelectAddressCoordinatorDelegate?) -> UIViewController {
        let container = DependencyContainer()
        let service: SelectAddressServicing = SelectAddressService(dependencies: container)
        let coordinator: SelectAddressCoordinating = SelectAddressCoordinator(delegate: delegate)
        let presenter: SelectAddressPresenting = SelectAddressPresenter(coordinator: coordinator)
        let viewModel = SelectAddressViewModel(model: model, service: service, presenter: presenter)
        let viewController = SelectAddressViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
