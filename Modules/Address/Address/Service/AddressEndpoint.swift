import Core
import Foundation

public enum AddressEndpoint {
    case consumerAddress
    case removeAddress(id: Int)
    case countries
    case states
    case cities(uf: String)
    case searchBy(cep: String)
}

extension AddressEndpoint: ApiEndpointExposable {
    public var path: String {
        switch self {
        case .countries:
            return "address/countries"
        case .states:
            return "address/states"
        case .cities(let uf):
            return "address/states/\(uf)/cities"
        case .consumerAddress:
            return "address/addresses/consumer"
        case .removeAddress(let id):
            return "address/addresses/consumer/address/\(id)"
        case .searchBy(let cep):
            return "address/search?zipcode=\(cep)"
        }
    }
    
    public var method: HTTPMethod {
        switch self {
        case .consumerAddress, .countries, .states, .cities, .searchBy:
             return .get
        case .removeAddress:
            return .delete
        }
    }
}
