import UI
import UIKit

protocol SelectRegionDisplay: AnyObject {
    func displayTitle(title: String)
    func displaySearchBarTitle(title: String)
    func showScreenLoading()
    func displayItems(data: [ItemSearchble])
    func enableSearchBar()
    func hideLoading()
    func displayErrorPopup(title: String, message: String)
}

public final class SelectRegionViewController: ViewController<SelectRegionInteracting, UIView>, LoadingViewProtocol {
    public lazy var loadingView = LoadingView()
    
    private lazy var searchController: UISearchController = {
        let searchController = UISearchController()
        searchController.searchBar.delegate = self
        searchController.searchBar.isUserInteractionEnabled = false
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        return searchController
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        
        return tableView
    }()
    
    private lazy var dataSource: TableViewDataSource<Int, ItemSearchble> = {
        let dataSource = TableViewDataSource<Int, ItemSearchble>(view: tableView)
        dataSource.add(section: 0)
        dataSource.itemProvider = { _, _, item -> UITableViewCell? in
            let cell = UITableViewCell()
            cell.textLabel?.text = item.getTitle()
            return cell
        }
        
        return dataSource
    }()
    
    public func setCustomSearchBarColor(color: UIColor) {
        searchController.searchBar.backgroundColor = color
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        interactor.requestData()
        extendedLayoutIncludesOpaqueBars = true
        
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = false
        } else {
            tableView.tableHeaderView = searchController.searchBar
        }
    }
    
    override public func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalTo(view.compatibleSafeArea.edges)
        }
    }
    
    override public func configureViews() {
        view.backgroundColor = Colors.backgroundSecondary.color
        tableView.dataSource = dataSource
        interactor.configureView()
    }
    
    override public func buildViewHierarchy() {
        view.addSubview(tableView)
    }
}

// MARK: SelectRegionDisplay
extension SelectRegionViewController: SelectRegionDisplay {
    func displaySearchBarTitle(title: String) {
        searchController.searchBar.placeholder = title
    }
    
    func displayTitle(title: String) {
        self.title = title
    }
    
    func enableSearchBar() {
        searchController.searchBar.isUserInteractionEnabled = true
    }
    
    func showScreenLoading() {
        startLoadingView()
    }
    
    func displayItems(data: [ItemSearchble]) {
        dataSource.update(items: data, from: 0)
    }
    
    func hideLoading() {
        stopLoadingView()
    }
    
    func displayErrorPopup(title: String, message: String) {
        showErrorPopup(title: title, message: message)
    }
}

// MARK: UITableViewDelegate
extension SelectRegionViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let item = dataSource.data[indexPath.section]?[indexPath.row] {
            interactor.didSelect(model: item)
        }
    }
}

// MARK: UISearchControllerDelegate
extension SelectRegionViewController: UISearchBarDelegate {
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let text = searchBar.text else {
            return
        }
        interactor.filterData(with: text)
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        interactor.filterData(with: "")
    }
}
