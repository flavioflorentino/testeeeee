import Core
import Foundation

public class CountryRegion: RegionFactory<Country> {
    let service: SelectCountryService
    
    override init(dependencies: AddressDependencies = DependencyContainer()) {
        self.service = SelectCountryService(dependencies: dependencies)
        
        super.init(dependencies: dependencies)
    }
    
    override func getScreenTitle() -> String {
        Strings.SelectCountry.title
    }
    
    override func getSearchBarTitle() -> String {
        Strings.SelectRegion.Search.placeholder
    }
    
    override func getData(completion: @escaping (Result<[Country], ApiError>) -> Void) {
        service.getCountries(completion: completion)
    }
}
