import Core
import Foundation

protocol SelectCountryServicing {
    func getCountries(completion: @escaping (Result<[Country], ApiError>) -> Void)
}

final class SelectCountryService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - SelectCountryServicing
extension SelectCountryService: SelectCountryServicing {
    func getCountries(completion: @escaping (Result<[Country], ApiError>) -> Void) {
        let api = Api<[Country]>(endpoint: AddressEndpoint.countries)
        
        api.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
