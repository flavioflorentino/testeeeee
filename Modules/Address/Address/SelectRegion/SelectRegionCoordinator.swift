import UIKit

protocol SelectRegionCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var didSelectRegionAction: (ItemSearchble) -> Void { get set }
    func didSelect(model: ItemSearchble)
}

final class SelectRegionCoordinator {
    weak var viewController: UIViewController?
    var didSelectRegionAction: (ItemSearchble) -> Void = { _ in }
    
    init(didSelectRegionAction: @escaping (ItemSearchble) -> Void) {
        self.didSelectRegionAction = didSelectRegionAction
    }
}

// MARK: - SelectRegionCoordinating
extension SelectRegionCoordinator: SelectRegionCoordinating {
    func didSelect(model: ItemSearchble) {
        didSelectRegionAction(model)
        viewController?.navigationController?.popViewController(animated: true)
    }
}
