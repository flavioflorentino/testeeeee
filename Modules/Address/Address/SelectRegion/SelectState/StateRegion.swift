import Core
import Foundation

public class StateRegion: RegionFactory<State> {
    let service: SelectStateService
    
    override init(dependencies: AddressDependencies = DependencyContainer()) {
        self.service = SelectStateService(dependencies: dependencies)
        
        super.init(dependencies: dependencies)
    }
    
    override func getScreenTitle() -> String {
        Strings.SelectState.title
    }
    
    override func getSearchBarTitle() -> String {
        Strings.SelectRegion.Search.placeholder
    }
    
    override func getData(completion: @escaping (Result<[State], ApiError>) -> Void) {
        service.getStates(completion: completion)
    }
}
