import Core
import Foundation

protocol SelectStateServicing {
    func getStates(completion: @escaping (Result<[State], ApiError>) -> Void)
}

final class SelectStateService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - SelectStateServicing
extension SelectStateService: SelectStateServicing {
    func getStates(completion: @escaping (Result<[State], ApiError>) -> Void) {
        let api = Api<[State]>(endpoint: AddressEndpoint.states)
        
        api.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
