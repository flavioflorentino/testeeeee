import Core
import Foundation

public class CityRegion: RegionFactory<City> {
    let uf: String
    let service: SelectCityService
    
    init(uf: String, dependencies: AddressDependencies = DependencyContainer()) {
        self.uf = uf
        self.service = SelectCityService(dependencies: dependencies)
        
        super.init(dependencies: dependencies)
    }
    
    override func getScreenTitle() -> String {
        Strings.SelectCity.title
    }
    
    override func getSearchBarTitle() -> String {
        Strings.SelectRegion.Search.placeholder
    }
    
    override func getData(completion: @escaping (Result<[City], ApiError>) -> Void) {
        service.getCities(uf: uf, completion: completion)
    }
}
