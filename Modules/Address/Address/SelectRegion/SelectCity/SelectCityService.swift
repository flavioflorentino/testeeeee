import Core
import Foundation

protocol SelectCityServicing {
    func getCities(uf: String, completion: @escaping (Result<[City], ApiError>) -> Void)
}

final class SelectCityService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - SelectCityServicing
extension SelectCityService: SelectCityServicing {
    func getCities(uf: String, completion: @escaping (Result<[City], ApiError>) -> Void) {
        let api = Api<[City]>(endpoint: AddressEndpoint.cities(uf: uf))
        
        api.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
