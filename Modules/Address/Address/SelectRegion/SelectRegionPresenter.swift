import Foundation

protocol SelectRegionPresenting: AnyObject {
    var viewController: SelectRegionDisplay? { get set }
    func updateTitle(title: String)
    func updateSearchBarTitle(title: String)
    func presentLoadingDataList()
    func enableSearch()
    func updateDataList(data: [ItemSearchble])
    func presentError(title: String, message: String)
    func didSelect(model: ItemSearchble)
}

final class SelectRegionPresenter {
    private let coordinator: SelectRegionCoordinating
    weak var viewController: SelectRegionDisplay?
    
    init(coordinator: SelectRegionCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - SelectRegionPresenting
extension SelectRegionPresenter: SelectRegionPresenting {
    func updateTitle(title: String) {
        viewController?.displayTitle(title: title)
    }
    
    func updateSearchBarTitle(title: String) {
        viewController?.displaySearchBarTitle(title: title)
    }
    
    func didSelect(model: ItemSearchble) {
        coordinator.didSelect(model: model)
    }
    
    func enableSearch() {
        viewController?.enableSearchBar()
    }
    
    func presentLoadingDataList() {
        viewController?.showScreenLoading()
    }
    
    func updateDataList(data: [ItemSearchble]) {
        viewController?.displayItems(data: data)
        viewController?.hideLoading()
    }
    
    func presentError(title: String, message: String) {
        viewController?.hideLoading()
        viewController?.displayErrorPopup(title: title, message: message)
    }
}
