import Foundation
import UIKit

public protocol ItemSearchble {
    func getId() -> Int
    func getSelectionValue() -> String
    func getTitle() -> String
}

public enum SelectRegionFactory {
    public enum RegionType {
        case country
        case state
        case city(uf: String)
    }
    
    public static func make(type: RegionType, completion: @escaping (ItemSearchble) -> Void) -> SelectRegionViewController {
        switch type {
        case .country:
            return make(regionType: CountryRegion(), completion: completion)
        case .state:
            return make(regionType: StateRegion(), completion: completion)
        case .city(let uf):
            return make(regionType: CityRegion(uf: uf), completion: completion)
        }
    }
    
    static func make<T: ItemSearchble>(regionType: RegionFactory<T>, completion: @escaping (ItemSearchble) -> Void) -> SelectRegionViewController {
        let coordinator: SelectRegionCoordinating = SelectRegionCoordinator(didSelectRegionAction: completion)
        let presenter: SelectRegionPresenting = SelectRegionPresenter(coordinator: coordinator)
        let interactor = SelectRegionInteractor<T>(regionType: regionType, presenter: presenter)
        let viewController = SelectRegionViewController(interactor: interactor)
        
        coordinator.viewController = viewController
        coordinator.didSelectRegionAction = completion
        presenter.viewController = viewController
        
        return viewController
    }
}
