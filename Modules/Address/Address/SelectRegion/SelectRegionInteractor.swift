import Core
import Foundation

public protocol SelectRegionInteracting: AnyObject {
    func configureView()
    func requestData()
    func filterData(with text: String)
    func didSelect(model: ItemSearchble)
}

final class SelectRegionInteractor<T: ItemSearchble> {
    private let regionType: RegionFactory<T>
    private let presenter: SelectRegionPresenting
    private var data: [T] = []
    
    init(regionType: RegionFactory<T>, presenter: SelectRegionPresenting) {
        self.regionType = regionType
        self.presenter = presenter
    }
}

// MARK: - SelectRegionInteractor
extension SelectRegionInteractor: SelectRegionInteracting {
    func configureView() {
        presenter.updateTitle(title: regionType.getScreenTitle())
        presenter.updateSearchBarTitle(title: regionType.getSearchBarTitle())
    }
    
    func didSelect(model: ItemSearchble) {
        presenter.didSelect(model: model)
    }
    
    func filterData(with text: String) {
        if text.isEmpty {
            presenter.updateDataList(data: data)
        } else {
            let filteredData = data.filter({ $0.getTitle().lowercased().contains(text.lowercased()) })
            presenter.updateDataList(data: filteredData)
        }
    }
    
    func requestData() {
        presenter.presentLoadingDataList()
        regionType.getData { [weak self] result in
            switch result {
            case .success(let data):
                self?.data = data
                self?.presenter.updateDataList(data: data)
                self?.presenter.enableSearch()
            case .failure(let error):
                self?.requestError(error: error)
            }
        }
    }
    
    private func requestError(error: ApiError) {
        let requestError = error.requestError ?? RequestError()
        presenter.presentError(title: requestError.title, message: requestError.message)
    }
}
