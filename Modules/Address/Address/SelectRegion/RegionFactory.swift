import Core

public class RegionFactory<T: ItemSearchble> {
    typealias Dependencies = HasMainQueue
    let dependencies: AddressDependencies
    
    init(dependencies: AddressDependencies) {
        self.dependencies = dependencies
    }
    
    func getSearchBarTitle() -> String { "" }
    func getScreenTitle() -> String { "" }
    func getData(completion: @escaping (Result<[T], ApiError>) -> Void) {}
}
