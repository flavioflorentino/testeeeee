import Foundation

struct AddressCell {
    let isSelectable: Bool
    let isSelected: Bool
    let addressType: AddressType
    let addressName: String
    let addressComplement: String
    let streetName: String
    let streetNumber: String
    let city: String
    let state: String
    let zipCode: String
}
