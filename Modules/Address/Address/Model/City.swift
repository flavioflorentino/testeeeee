import Foundation

public struct City: Decodable {
    public let id: Int
    public let type: String?
    public let name: String
    public let abbreviation: String?
}

extension City: ItemSearchble {
    public func getId() -> Int {
        self.id
    }
    
    public func getTitle() -> String {
        self.name
    }
    
    public func getSelectionValue() -> String {
        self.name
    }
}
