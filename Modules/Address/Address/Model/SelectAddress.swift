import Foundation

struct SelectAddress {
    let selectedId: Int?
    let header: Header?
}

extension SelectAddress {
    struct Header {
        let showImage: Bool
        let title: String
        let description: String
    }
}
