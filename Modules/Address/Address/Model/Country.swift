import Foundation

public struct Country: Decodable {
    public let id: Int
    public let initials: String
    public let name: String
}

extension Country: ItemSearchble {
    public func getId() -> Int {
        self.id
    }
    
    public func getTitle() -> String {
        self.name
    }
    
    public func getSelectionValue() -> String {
        self.name
    }
}
