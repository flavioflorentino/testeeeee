import Foundation

struct Address: Decodable {
    let id: Int
    let addressType: AddressType
    let country: String
    let state: String
    let zipCode: String
    let city: String
    let neighborhood: String
    let addressName: String
    let addressComplement: String
    let streetName: String
    let streetNumber: String
}

extension Address {
    private enum CodingKeys: String, CodingKey {
        case id, country, state, city, neighborhood
        case addressName = "address_name"
        case zipCode = "zip_code"
        case addressComplement = "address_complement"
        case streetName = "street_name"
        case streetNumber = "street_number"
        case addressType = "icon_name"
    }
    
    enum HousingType: String, Codable {
        case home
        case building
        case mountain
    }
}
