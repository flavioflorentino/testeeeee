import Foundation

enum AddressType: String, Codable {
    case home
    case building
    case mountain
}
