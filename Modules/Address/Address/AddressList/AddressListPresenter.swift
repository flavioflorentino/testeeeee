import Foundation
import UI

protocol AddressListPresenting: AnyObject {
    var viewController: AddressListDisplay? { get set }
    func presentLoadingAddressList()
    func presentReloadingAddressList()
    func askAddressAction()
    func updateAddressList(model: [Address])
    func presenterError(title: String, message: String)
    func didNextStep(action: AddressListAction)
}

final class AddressListPresenter {
    private let coordinator: AddressListCoordinating
    weak var viewController: AddressListDisplay?

    init(coordinator: AddressListCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - AddressListPresenting
extension AddressListPresenter: AddressListPresenting {
    func presentLoadingAddressList() {
        viewController?.showScreenLoading()
    }
    
    func presentReloadingAddressList() {
        viewController?.showRefreshLoading()
    }
    
    func askAddressAction() {
        viewController?.displayActionSheet()
    }
    
    func updateAddressList(model: [Address]) {
        viewController?.hideLoading()
        
        model.isEmpty ? viewController?.displayEmptyView() : viewController?.removeEmptyView()
        model.isEmpty ? viewController?.removeFooter() : viewController?.displayFooter()
        
        updateItems(model: model)
    }
    
    func presenterError(title: String, message: String) {
        viewController?.hideLoading()
        viewController?.displayErrorPopup(title: title, message: message)
    }
    
    func didNextStep(action: AddressListAction) {
        coordinator.perform(action: action)
    }
    
    private func updateItems(model: [Address]) {
        let cells = model.map {
            AddressCell(isSelectable: false,
                        isSelected: false,
                        addressType: $0.addressType,
                        addressName: $0.addressName,
                        addressComplement: $0.addressComplement,
                        streetName: $0.streetName,
                        streetNumber: $0.streetNumber,
                        city: $0.city,
                        state: $0.state,
                        zipCode: $0.zipCode)
        }
        
        viewController?.displayItems(data: cells)
    }
}
