import SnapKit
import UI

protocol AddressListDisplay: AnyObject {
    func displayFooter()    
    func displayItems(data: [AddressCell])
    func showScreenLoading()
    func showRefreshLoading()
    func hideLoading()
    func displayActionSheet()
    func displayEmptyView()
    func removeFooter()
    func removeEmptyView()
    func displayErrorPopup(title: String, message: String)
}

final class AddressListViewController: ViewController<AddressListViewModelInputs, UIView>, LoadingViewProtocol {
    lazy var loadingView = LoadingView()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorInset = .zero
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableView.automaticDimension
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        tableView.register(
            AddressCellView.self,
            forCellReuseIdentifier: String(describing: AddressCellView.self)
        )

        return tableView
    }()
    
    private lazy var tableViewFooter: AddNewAddressView = {
        let frame = CGRect(origin: .zero, size: CGSize(width: tableView.frame.size.width, height: 80))
        let footer = AddNewAddressView(frame: frame)
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapFooter))
        footer.addGestureRecognizer(gestureRecognizer)
        
        return footer
    }()
    
    private lazy var emptyView: AddressListEmptyView = {
        let frame: CGRect
        if #available(iOS 11.0, *) {
            frame = view.safeAreaLayoutGuide.layoutFrame
        } else {
            frame = view.frame
        }
        
        let emptyView = AddressListEmptyView(frame: frame)
        emptyView.delegate = self
        
        return emptyView
    }()
    
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(didPullRefresh), for: .valueChanged)
        
        return refreshControl
    }()
    
    private lazy var dataSource: TableViewDataSource<Int, AddressCell> = {
        let dataSource = TableViewDataSource<Int, AddressCell>(view: tableView)
        dataSource.add(section: 0)
        dataSource.itemProvider = { tableView, indexPath, item -> UITableViewCell? in
            let cellIdentifier = String(describing: AddressCellView.self)
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? AddressCellView
            cell?.setupView(model: item)
            
            return cell
        }
        
        return dataSource
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.updateView()
    }

    override func configureViews() {
        title = Strings.AddressList.title
        view.backgroundColor = Colors.backgroundSecondary.color
        tableView.dataSource = dataSource
    }
    
    override func buildViewHierarchy() {
        tableView.addSubview(refreshControl)
        view.addSubview(tableView)
    }
    
    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
}
@objc
private extension AddressListViewController {
    func didTapFooter() {
        viewModel.openAddAddress()
    }
    
    func didPullRefresh() {
        viewModel.reloadAddress()
    }
}
// MARK: AddressListDisplay
extension AddressListViewController: AddressListDisplay {
    func displayFooter() {
        tableView.tableFooterView = tableViewFooter
    }
    
    func displayEmptyView() {
        tableView.tableHeaderView = emptyView
    }
    
    func removeFooter() {
        tableView.tableFooterView = nil
    }
    
    func removeEmptyView() {
        tableView.tableHeaderView = nil
    }
    
    func displayItems(data: [AddressCell]) {
        dataSource.update(items: data, from: 0)
    }
    
    func showScreenLoading() {
        startLoadingView()
    }
    
    func showRefreshLoading() {
        refreshControl.beginRefreshing()
    }
    
    func hideLoading() {
        stopLoadingView()
        refreshControl.endRefreshing()
    }
    
    func displayActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let editAction = UIAlertAction(title: Strings.Button.editAddress, style: .default) { [weak self] _ in
            self?.viewModel.editAddress()
        }
        
        let removeAction = UIAlertAction(title: Strings.Button.removeAddress, style: .destructive) { [weak self] _ in
            self?.viewModel.removeAddress()
        }
        
        let closeAction = UIAlertAction(title: Strings.Button.cancel, style: .cancel, handler: nil)
        
        actionSheet.addAction(editAction)
        actionSheet.addAction(removeAction)
        actionSheet.addAction(closeAction)
        
        present(actionSheet, animated: true)
    }
    
    func displayErrorPopup(title: String, message: String) {
        showErrorPopup(title: title, message: message)
    }
}

extension AddressListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        viewModel.didTapRow(index: indexPath)
    }
}

extension AddressListViewController: AddressListEmptyViewDelegate {
    func didTapAddAddress() {
        viewModel.openAddAddress()
    }
}
