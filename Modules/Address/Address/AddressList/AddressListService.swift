import Core
import Foundation

protocol AddressListServicing {
    func getAddressList(completion: @escaping (Result<[Address], ApiError>) -> Void)
    func removeAddress(id: Int, completion: @escaping (Result<Void, ApiError>) -> Void)
}

final class AddressListService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - AddressListServicing
extension AddressListService: AddressListServicing {
    func getAddressList(completion: @escaping (Result<[Address], ApiError>) -> Void) {
        let api = Api<[Address]>(endpoint: AddressEndpoint.consumerAddress)
        
        api.execute { [weak self] result in            
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func removeAddress(id: Int, completion: @escaping (Result<Void, ApiError>) -> Void) {
        let api = Api<NoContent>(endpoint: AddressEndpoint.removeAddress(id: id))
        
        api.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case .success:
                    completion(.success(()))
                case .failure(let apiError):
                    completion(.failure(apiError))
                }
            }
        }
    }
}
