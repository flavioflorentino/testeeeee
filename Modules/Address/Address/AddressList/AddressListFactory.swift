import Foundation

public enum AddressListFactory {
    public static func make() -> UIViewController {
        let container = DependencyContainer()
        let service: AddressListServicing = AddressListService(dependencies: container)
        let coordinator: AddressListCoordinating = AddressListCoordinator()
        let presenter: AddressListPresenting = AddressListPresenter(coordinator: coordinator)
        let viewModel = AddressListViewModel(service: service, presenter: presenter)
        let viewController = AddressListViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
