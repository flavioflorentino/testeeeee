import Foundation
import SnapKit
import UI

private extension AddNewAddressView.Layout {
    enum Size {
        static let button = Sizing.ImageView.medium.rawValue
        static let image = CGSize(width: Spacing.base03, height: Spacing.base03)
        static let borderWidth: CGFloat = 1
    }
}

final class AddNewAddressView: UIView, ViewConfiguration {
    fileprivate enum Layout { }
    
    private lazy var buttonView: UIView = {
        let view = UIView()
        view.layer.borderWidth = Layout.Size.borderWidth
        view.layer.borderColor = Colors.grayscale100.color.cgColor
        view.layer.cornerRadius = Layout.Size.button.width / 2
        view.clipsToBounds = true
        
        return view
    }()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.icPlus.image
        imageView.imageStyle(AvatarImageStyle(size: .xSmall))
        imageView.contentMode = .scaleAspectFit
        
        return imageView
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.Button.addNewAddress
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale950())
        label.numberOfLines = 2
        
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Spacing.base02
        stackView.axis = .horizontal

        return stackView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundSecondary.color
    }
    
    func buildViewHierarchy() {
        buttonView.addSubview(imageView)
        
        stackView.addArrangedSubview(buttonView)
        stackView.addArrangedSubview(titleLabel)
        addSubview(stackView)
    }
    
    func setupConstraints() {
        buttonView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.button)
        }
        
        imageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.image)
            $0.centerX.centerY.equalToSuperview()
        }
        
        stackView.snp.makeConstraints {
            $0.top.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.bottom.equalToSuperview().offset(-Spacing.base02)
        }
    }
}
