import SnapKit
import UI

protocol AddressListEmptyViewDelegate: AnyObject {
    func didTapAddAddress()
}

private extension AddressListEmptyView.Layout {
    enum Size {
        static let image = CGSize(width: 100, height: 100)
    }
}

final class AddressListEmptyView: UIView, ViewConfiguration {
    fileprivate enum Layout { }
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        
        return view
    }()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.icHome.image
        imageView.contentMode = .scaleAspectFit
        
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 2
        label.text = Strings.EmptyAddress.addAddress
        label
            .labelStyle(TitleLabelStyle(type: .large))
            .with(\.textColor, .grayscale750())

        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.text = Strings.EmptyAddress.explanationeUsed
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale400())

        return label
    }()
    
    private lazy var addButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Strings.Button.addAddress, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(tapAddButton), for: .touchUpInside)
        
        return button
    }()
    
    weak var delegate: AddressListEmptyViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureViews() {
        backgroundColor = .clear
    }
    
    func buildViewHierarchy() {
        containerView.addSubview(imageView)
        containerView.addSubview(titleLabel)
        containerView.addSubview(descriptionLabel)
        containerView.addSubview(addButton)
        
        addSubview(containerView)
    }
    
    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.center.leading.trailing.equalToSuperview()
        }
        
        imageView.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.size.equalTo(Layout.Size.image)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        addButton.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.bottom.trailing.equalToSuperview().offset(-Spacing.base02)
        }
    }
}

@objc
private extension AddressListEmptyView {
    func tapAddButton() {
        delegate?.didTapAddAddress()
    }
}
