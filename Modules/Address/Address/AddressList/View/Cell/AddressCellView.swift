import Foundation
import SnapKit
import UI

private extension AddressCellView.Layout {
    enum Size {
        static let image = Sizing.ImageView.medium.rawValue
        static let checkbox = CGSize(width: Spacing.base03, height: Spacing.base03)
        static let line: CGFloat = 1
    }
}

final class AddressCellView: UITableViewCell, ViewConfiguration {
    fileprivate enum Layout { }
    
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Spacing.base02
        stackView.alignment = .center

        return stackView
    }()
    
    private lazy var addressView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale100.color
        view.layer.cornerRadius = Layout.Size.image.width / 2
        view.clipsToBounds = true
        
        return view
    }()
    
    private lazy var checkboxImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.icCheckboxDisable.image
        imageView.contentMode = .scaleAspectFit
        imageView.isHidden = true
        
        return imageView
    }()
    
    private lazy var addressImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.imageStyle(AvatarImageStyle(size: .xSmall))
        imageView.contentMode = .scaleAspectFit
        
        return imageView
    }()

    private lazy var separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale100.color
        
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale950())
        label.numberOfLines = 2
        
        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())

        return label
    }()

    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Spacing.base00
        stackView.axis = .vertical

        return stackView
    }()

    private lazy var presenter: AddressCellPresenting = {
        let presenter = AddressCellPresenter()
        presenter.view = self

        return presenter
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configureViews() {
        backgroundColor = Colors.backgroundSecondary.color
    }

    func buildViewHierarchy() {
        addressView.addSubview(addressImageView)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(descriptionLabel)
        
        rootStackView.addArrangedSubview(addressView)
        rootStackView.addArrangedSubview(stackView)
        rootStackView.addArrangedSubview(checkboxImageView)
        
        contentView.addSubview(rootStackView)
        contentView.addSubview(separatorView)
    }

    func setupConstraints() {
        rootStackView.snp.makeConstraints {
            $0.top.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        addressView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.image)
        }
        
        checkboxImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.checkbox)
        }
        
        addressImageView.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
        
        separatorView.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.line)
            $0.top.equalTo(rootStackView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }

    func setupView(model: AddressCell) {
        presenter.setup(model: model)
    }
}

extension AddressCellView: AddressCellPresenterDisplay {
    func displayTitle(with text: String) {
        titleLabel.text = text
    }

    func displayDescription(with text: String) {
        descriptionLabel.text = text
    }

    func displayImage(with image: UIImage) {
        addressImageView.image = image
    }
    
    func displayCheckbox(with image: UIImage, isHidden: Bool) {
        checkboxImageView.image = image
        checkboxImageView.isHidden = isHidden
    }
}
