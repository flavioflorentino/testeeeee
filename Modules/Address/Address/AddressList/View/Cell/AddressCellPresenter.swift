import Foundation
import UI

protocol AddressCellPresenting {
    var view: AddressCellPresenterDisplay? { get set }
    func setup(model: AddressCell)
}

protocol AddressCellPresenterDisplay: AnyObject {
    func displayTitle(with text: String)
    func displayDescription(with text: String)
    func displayImage(with image: UIImage)
    func displayCheckbox(with image: UIImage, isHidden: Bool)
}

final class AddressCellPresenter: AddressCellPresenting {
    weak var view: AddressCellPresenterDisplay?

    func setup(model: AddressCell) {
        view?.displayTitle(with: model.addressName)
        setupDescription(model: model)
        setupImage(housingType: model.addressType)
        setupCheckbox(isSelectable: model.isSelectable, isSelected: model.isSelected)
    }

    private func setupImage(housingType: AddressType) {
        let asset: ImageAsset
        switch housingType {
        case .home:
            asset = Assets.icHome
        case .building:
            asset = Assets.icBuilding
        case .mountain:
            asset = Assets.icMountain
        }

        view?.displayImage(with: asset.image)
    }
    
    private func setupDescription(model: AddressCell) {
        let array: [(String, Separator)] = [
            (model.streetName, .comma),
            (model.streetNumber, .comma),
            (model.addressComplement, .hyphen),
            (model.city, .comma),
            (model.state, .hyphen),
            (model.zipCode, .none)
        ]
        
        let description = descriptionAddress(array: array)
        view?.displayDescription(with: description)
    }
    
    private func descriptionAddress(array: [(description: String, separator: Separator)]) -> String {
        var description = ""
        
        for info in array where !info.description.isEmpty {
            description += info.description + info.separator.value
        }
        
        return description
            .trimmingCharacters(in: .whitespacesAndNewlines)
            .trimmingCharacters(in: CharacterSet(charactersIn: Separator.comma.value))
            .trimmingCharacters(in: CharacterSet(charactersIn: Separator.hyphen.value))
    }
    
    private func setupCheckbox(isSelectable: Bool, isSelected: Bool) {
        let image = isSelected ? Assets.icCheckboxEnable.image : Assets.icCheckboxDisable.image
        view?.displayCheckbox(with: image, isHidden: !isSelectable)
    }
}

extension AddressCellPresenter {
    enum Separator: String {
        case none = ""
        case comma = ","
        case hyphen = "-"
        
        var value: String {
            switch self {
            case .comma:
                return ", "
            case .hyphen:
                return " - "
            case .none:
                return ""
            }
        }
    }
}
