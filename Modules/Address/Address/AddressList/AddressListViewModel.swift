import Core
import Foundation

protocol AddressListViewModelInputs: AnyObject {
    func updateView()
    func reloadAddress()
    func editAddress()
    func removeAddress()
    func openAddAddress()
    func didTapRow(index: IndexPath)
}

final class AddressListViewModel {
    private let service: AddressListServicing
    private let presenter: AddressListPresenting
    private var selectedAddress: Address?
    private var model: [Address]
    
    init(service: AddressListServicing, presenter: AddressListPresenting) {
        self.model = []
        self.service = service
        self.presenter = presenter
    }
}

// MARK: - AddressListViewModelInputs
extension AddressListViewModel: AddressListViewModelInputs {
    func updateView() {
        presenter.presentLoadingAddressList()
        loadAddressList()
    }
    
    func reloadAddress() {
        presenter.presentReloadingAddressList()
        loadAddressList()
    }
    
    func editAddress() {
        guard let id = selectedAddress?.id else {
            errorNotFoundAdrress()
            return
        }
        
        presenter.didNextStep(action: .openEditAddress(id: id))
    }
    
    func removeAddress() {
        guard let id = selectedAddress?.id else {
            errorNotFoundAdrress()
            return
        }
        
        presenter.presentLoadingAddressList()
        service.removeAddress(id: id) { [weak self] result in
            switch result {
            case .success:
                self?.removeAddress(id: id)
            case .failure(let error):
                self?.requestError(error: error)
            }
        }
    }
    
    func openAddAddress() {
        presenter.didNextStep(action: .openAddAddress)
    }
    
    func didTapRow(index: IndexPath) {
        let row = index.row
        guard model.indices.contains(row) else {
            errorNotFoundAdrress()
            return
        }
        
        selectedAddress = model[row]
        presenter.askAddressAction()
    }
    
    private func removeAddress(id: Int) {
        let filter = model.filter { $0.id == id }
        model = filter
        presenter.updateAddressList(model: model)
    }
    
    private func loadAddressList() {
        service.getAddressList { [weak self] result in
            switch result {
            case .success(let model):
                self?.model = model
                self?.presenter.updateAddressList(model: model)
            case .failure(let error):
                self?.requestError(error: error)
            }
        }
    }
    
    private func errorNotFoundAdrress() {
        presenter.presenterError(title: Strings.Error.title, message: Strings.Error.notFoundAddress)
    }
    
    private func requestError(error: ApiError) {
        let requestError = error.requestError ?? RequestError()
        presenter.presenterError(title: requestError.title, message: requestError.message)
    }
}
