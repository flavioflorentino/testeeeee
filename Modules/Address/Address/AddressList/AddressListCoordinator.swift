import UIKit

enum AddressListAction: Equatable {
    case openEditAddress(id: Int)
    case openAddAddress
}

protocol AddressListCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: AddressListAction)
}

final class AddressListCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - AddressListCoordinating
extension AddressListCoordinator: AddressListCoordinating {
    func perform(action: AddressListAction) {
        // TODO: Open AddAddress
    }
}
