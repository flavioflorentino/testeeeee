enum ScreenNames {
    case negotiated
    
    var name: String {
        switch self {
        case .negotiated:
            return "Loan Installment Negotiated"
        }
    }
}
