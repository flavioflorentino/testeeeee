import AnalyticsModule

enum NegotiatedInstallmentEvent: AnalyticsKeyProtocol, Equatable {
    case didEnter(screen: ScreenNames)
    case didTap
    
    private var name: String {
        switch self {
        case .didTap:
            return "Loan Installment Help Clicked"
        case .didEnter:
            return "Screen Viewed"
        }
    }
    
    private var properties: [String: Any] {
        if case let .didEnter(screen) = self {
            return ["screen_name": screen.name]
        }
        return [:]
    }

    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.mixPanel])
    }
}
