import AnalyticsModule

enum LoanDocumentsEvent: AnalyticsKeyProtocol, Equatable {
    case didView(hasContractUnavailable: Bool)
    case didSelect(status: ContractStatus)
    case didTap
    case didFail
    
    private var name: String {
        switch self {
        case .didView:
            return "List Viewed"
        case .didSelect:
            return "Selected"
        case .didTap:
            return "Help Clicked"
        case .didFail:
            return "List Error"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .didView(hasContractUnavailable):
            return ["has_contract_unavailable": hasContractUnavailable]
        case let .didSelect(status):
            return ["status": status]
        default:
            return [:]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("Loan Contract \(name)", properties: properties, providers: [.mixPanel])
    }
}

extension LoanDocumentsEvent {
    enum ContractStatus: String {
        case available = "Available"
        case unavailable = "Unavailable"
        case completed = "Completed"
    }
}
