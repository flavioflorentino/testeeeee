import AnalyticsModule

enum LoanContractEvent: AnalyticsKeyProtocol, Equatable {
    case didTap(origin: TapOrigin)
    
    private var name: String {
        switch self {
        case let .didTap(origin):
            return "Loan Contract \(origin) Clicked"
        }
    }

    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, providers: [.mixPanel])
    }
}

extension LoanContractEvent {
    enum TapOrigin: String {
        case share = "Share"
        case show = "Show"
    }
}
