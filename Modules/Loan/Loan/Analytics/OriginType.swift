public enum OriginType: Equatable {
    case settingsMenu
    case myLoanAgreements
    case installments
    case payment
    case wallet
    case hub
    case paymentEmail
    case deepLink
    case charge(params: [String: String?])
    
    var name: String {
        switch self {
        case .settingsMenu:
            return "Settings"
        case .myLoanAgreements:
            return "Contract Selection"
        case .installments:
            return "Monitoring"
        case .payment:
            return "Payment"
        case .wallet:
            return "Wallet"
        case .paymentEmail:
            return "Payment Email"
        case .deepLink:
            return "Deeplink"
        case .charge:
            return "Installments"
        case .hub:
            return "Hub"
        }
    }
    
    var value: String {
        switch self {
        case .settingsMenu:
            return "Ajuda"
        case .myLoanAgreements:
            return "Seleção de Contratos"
        case .installments:
            return "Acompanhamento"
        case .payment:
            return "Pagamento"
        case .wallet:
            return "Carteira"
        case .hub:
            return "Hub"
        default:
            return ""
        }
    }

    var params: [String: String?] {
        if case let .charge(params) = self {
            return params
        } else { return [:] }
    }
}
