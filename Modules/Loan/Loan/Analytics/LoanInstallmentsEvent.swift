import Foundation
import AnalyticsModule

enum LoanInstallmentsEvent: AnalyticsKeyProtocol, Equatable {
    case didView(origin: OriginType)
    case didTap(status: InstallmentStatus)

    private var name: String {
        switch self {
        case .didView:
            return "Loan Installment Viewed"
        case .didTap:
            return "Loan Installment Clicked"
        }
    }

    private var properties: [String: Any] {
        switch self {
        case let .didView(origin):
            return origin.params as [String: Any]
        case let .didTap(status):
            return ["status": status.trackingValue]
        }
    }

    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.mixPanel])
    }
}
