import AnalyticsModule

public enum LoanEvent: AnalyticsKeyProtocol, Equatable {
    case didEnter(to: OriginType, from: OriginType, params: [String: String] = [:])
    case didCopy(to: OriginType)
    case didSend(to: OriginType)
    
    private var name: String {
        switch self {
        case let .didEnter(to, _, _):
            return "Lending \(to.name) Accessed"
        case let .didCopy(to):
            return "Lending \(to.name) Copy"
        case let .didSend(to):
            return "Lending \(to.name) Sent"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .didEnter(_, from, params):
            var parameters = params
            parameters["origin"] = from.value
            return parameters
        case .didCopy, .didSend:
            return [:]
        }
    }
    
    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.mixPanel])
    }
}
