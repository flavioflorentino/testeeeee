import UI

public enum LoanFlow {
    case loanAgreements
    case loanAgreementsTrack
    case payment(contractId: String, installments: [Installment])
    case installments(contractId: String)
}

public protocol LoanFlowCoordinating: Coordinating {
    func start(flow: LoanFlow, didFinish: @escaping () -> Void)
    func close()
}

extension LoanFlowCoordinating {
    func start(flow: LoanFlow) {
        start(flow: flow, didFinish: {})
    }
}

public final class LoanCoordinator: LoanFlowCoordinating {
    public var childViewController: [UIViewController] = []
    public var viewController: UIViewController?
    private let navigationController: UINavigationController
    private var childCoordinators: [Coordinating] = []
    
    private let origin: OriginType
    var didFinish: () -> Void = {}
    
    public init(with navigationController: UINavigationController, from origin: OriginType) {
        self.navigationController = navigationController
        self.origin = origin
    }
    
    public func start(flow: LoanFlow, didFinish: @escaping () -> Void = {}) {
        self.didFinish = didFinish
        
        switch flow {
        case .loanAgreements:
            startLoanAgreementsFlow()
        case .loanAgreementsTrack:
            startLoanAgreementsTrackFlow()
        case let .payment(contractId, installments):
            startPaymentFlow(contractId: contractId, installments: installments)
        case let .installments(contractId):
            startLoanInstallments(contractId: contractId)
        }
    }
    
    public func close() {
        navigationController.popViewController(animated: true)
        childCoordinators = []
        didFinish()
    }
    
    deinit {
        childCoordinators = []
    }
}

private extension LoanCoordinator {
    func startLoanAgreementsFlow() {
        let loans = LoanAgreementsCoordinator(from: navigationController, delegate: self)
        loans.perform(action: .myLoanAgreements(origin: origin))
        childCoordinators.append(loans)
    }
    
    func startLoanAgreementsTrackFlow() {
        let loanAgreementsTrack = LoanAgreementsTrackCoordinator(from: navigationController, delegate: self)
        loanAgreementsTrack.perform(action: .myLoanDocuments)
        childCoordinators.append(loanAgreementsTrack)
    }
    
    func startPaymentFlow(contractId: String, installments: [Installment]) {
        let payment = PaymentFlowCoordinator(from: navigationController, delegate: self)
        payment.perform(action: .paymentMethod(contractId: contractId, installments: installments))
        childCoordinators.append(payment)
    }

    func startLoanInstallments(contractId: String) {
        let loans = LoanAgreementsCoordinator(from: navigationController, delegate: self)
        loans.perform(action: .installments(contractId: contractId, origin: self.origin))
        childCoordinators.append(loans)
    }
}
