public enum InstallmentStatus: String, Decodable {
    case overdue = "OVERDUED"
    case open = "OPEN"
    case closeToExpire = "CLOSE_TO_EXPIRE"
    case paid = "PAID"
    case paidOverdued = "PAID_OVERDUED"
    case paidInAdvance = "PAID_IN_ADVANCE"
    case renegotiated = "RENEGOTIATED"
}

extension InstallmentStatus {
    var description: String {
        switch self {
        case .overdue:
            return Strings.Installments.Status.overdue
        case .paid, .paidOverdued, .paidInAdvance:
            return Strings.Installments.Status.paid
        case .open:
            return Strings.Installments.Status.open
        case .closeToExpire:
            return Strings.Installments.Status.closeToExpire
        case .renegotiated:
            return Strings.Installments.Status.renegotiated
        }
    }
    
    var trackingValue: String {
        switch self {
        case .overdue:
            return "Atrasado"
        case .closeToExpire:
            return "Em dia"
        case .open:
            return "Antecipação"
        case .renegotiated:
            return "Negociada"
        default:
            return String()
        }
    }
}
