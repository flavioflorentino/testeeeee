import Foundation

public enum LoanTrackStatus: String, CaseIterable, Decodable {
    case active = "ACTIVE"
    case completed = "COMPLETED"
    
    var description: String {
        switch self {
        case .active:
            return  Strings.LoanTrackStatus.Description.active
        case .completed:
            return Strings.LoanTrackStatus.Description.completed
        }
    }
    
    var situation: String {
        switch self {
        case .active:
            return Strings.LoanTrackStatus.Situation.active
        case .completed:
            return Strings.LoanTrackStatus.Situation.completed
        }
    }
}
