import Foundation

struct LoanContractPDFResponse: Decodable {
    let url: String
    
    enum CodingKeys: String, CodingKey {
        case url = "file_url"
    }
}
