struct SendBankSlipByEmailResponse: Decodable {
    let destinationEmail: String
}
