import Foundation
import AssetsKit

public enum PaymentMethod: CaseIterable {
    case picpay
    case bankSlip
    
    var title: String {
        switch self {
        case .picpay:
            return Strings.Payment.PaymentMethod.Picpay.title
        case .bankSlip:
            return Strings.Payment.PaymentMethod.BankSlip.title
        }
    }
    
    var description: String {
        Strings.Payment.PaymentMethod.description
    }
    
    var image: UIImage {
        switch self {
        case .picpay:
            return Resources.Icons.icoPicpayGreenSquare.image
        case .bankSlip:
            return Resources.Icons.icoBarCodeSquared.image
        }
    }
}
