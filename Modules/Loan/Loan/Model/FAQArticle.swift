public enum FAQArticle: String {
    case payInAdvance = "360049938952"
    case bankSlip = "360054130271"
    case negotiated = "360050393091"
    case contracts = "360049939212"
    case installments = "360010562732"
}
