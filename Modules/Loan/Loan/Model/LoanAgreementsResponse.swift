import Foundation

struct LoanAgreementsResponse: Decodable {
    let loans: [LoanAgreement]
}
