import Foundation

struct LoanAgreement: Decodable {
    let id: String
    let bankId: String
    let loanGoal: LoanGoal
    let totalInstallments: Int
    let totalInstallmentsAlreadyPaid: Int
    let status: String
    
    enum CodingKeys: String, CodingKey {
        case bankId, loanGoal, totalInstallments, totalInstallmentsAlreadyPaid, status
        case id = "contractId"
    }
}
