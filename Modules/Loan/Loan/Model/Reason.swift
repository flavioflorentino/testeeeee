import Foundation
import UIKit
import UI

public enum LoanReason: String, CaseIterable, Decodable {
    case bills = "BILLS"
    case build = "BUILD"
    case business = "BUSINESS"
    case car = "CAR"
    case travel = "TRAVEL"
    case debt = "DEBITS"
    case health = "MEDICAL"
    case shopping = "SHOPPING"
    case education = "EDUCATION"
    case other = "OTHERS"
    
    var description: String {
        switch self {
        case .bills:
            return "Contas atrasadas"
        case .build:
            return "Reformas ou consertos"
        case .business:
            return "Investir no meu negócio"
        case .car:
            return "Comprar um carro"
        case .travel:
            return "Viagem"
        case .debt:
            return "Dívidas"
        case .health:
            return "Despesas médicas"
        case .shopping:
            return "Compras"
        case .education:
            return "Educação"
        case .other:
            return "Outros"
        }
    }
    
    var imageName: String {
        switch self {
        case .bills:
            return Iconography.receiptAlt.rawValue
        case .build:
            return Iconography.screw.rawValue
        case .business:
            return Iconography.storeAlt.rawValue
        case .car:
            return Iconography.car.rawValue
        case .travel:
            return Iconography.plane.rawValue
        case .debt:
            return Iconography.bill.rawValue
        case .health:
            return Iconography.medkit.rawValue
        case .shopping:
            return Iconography.shoppingCartAlt.rawValue
        case .education:
            return Iconography.bookOpen.rawValue
        case .other:
            return Iconography.wallet.rawValue
        }
    }
}
