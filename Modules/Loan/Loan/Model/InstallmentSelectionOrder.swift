public enum InstallmentSelectionOrder: String, Decodable {
    case topDown = "TOP_DOWN"
    case bottomUp = "BOTTOM_UP"
    case both = "BOTH"
    case singleDirection = "SINGLE_DIRECTION"
}
