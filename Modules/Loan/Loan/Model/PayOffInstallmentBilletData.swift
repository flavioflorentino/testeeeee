struct PayOffInstallmentBilletData {
    let billetBarCode: String
    let billetPlaceholder: String
    let userBalance: String?
    let installment: String
    let dueDate: String
    let status: InstallmentStatus
}
