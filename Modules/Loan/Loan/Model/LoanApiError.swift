public enum LoanApiError: String, Equatable {
    case timeExceeded = "PERSONAL_CREDIT_0022"
}
