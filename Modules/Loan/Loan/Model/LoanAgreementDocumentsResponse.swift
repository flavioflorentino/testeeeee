import Foundation

struct LoanAgreementDocumentsResponse: Decodable {
    let loans: [LoanAgreementDocument]
}
