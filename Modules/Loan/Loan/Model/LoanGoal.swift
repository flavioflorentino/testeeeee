import Foundation

struct LoanGoal: Decodable, Equatable {
    let category: LoanReason
    let description: String?
}
