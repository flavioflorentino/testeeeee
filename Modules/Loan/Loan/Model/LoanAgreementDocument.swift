import Foundation

struct LoanAgreementDocument: Decodable, Equatable {
    let bankId: String
    let contractEnd: String?
    let id: String
    let documentSent: Bool
    let loanGoal: LoanGoal
    let status: LoanTrackStatus
    let totalInstallments: Int
    let totalInstallmentsAlreadyPaid: Int

    enum CodingKeys: String, CodingKey {
        case id = "contractId"
        case bankId, loanGoal, totalInstallments, totalInstallmentsAlreadyPaid
        case status, contractEnd, documentSent
    }
}
