import Foundation

struct BankSlip: Decodable, Equatable {
    let code: String
    let value: String
    let dueDate: Date
    
    enum CodingKeys: String, CodingKey {
        case value = "slipValue"
        case dueDate = "slipDueDate"
        case code = "slipDigitableLine"
    }
}
