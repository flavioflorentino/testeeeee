import Foundation

struct InstallmentsResponse: Decodable {
    let outstandingBalance: Double
    let discountValueTotal: Double?
    let pendingList: [Installment]
    let paidList: [Installment]
    let selectionOrder: InstallmentSelectionOrder
    
    enum CodingKeys: String, CodingKey {
        case outstandingBalance, discountValueTotal, selectionOrder
        case pendingList = "listOpenInstallment"
        case paidList = "listPaidInstallment"
    }
}

public struct Installment: Decodable, Equatable {
    let id: String
    let originalValue: Double
    let value: Double
    let penaltyValue: Double?
    let order: Int
    let status: InstallmentStatus
    let dueDate: Date
    let paymentDate: Date?
    let daysOverDue: Int?
    let isPayable: Bool
    let discountValue: Double?
    
    enum CodingKeys: String, CodingKey {
        case id, originalValue, value, penaltyValue
        case order, status, dueDate, paymentDate
        case daysOverDue, discountValue
        case isPayable = "payable"
    }
}
