import Core
import UI
import UIKit

public protocol LoanLegacySetupContract: AnyObject {
    var billet: LoanBilletContract? { get set }
    var recharge: LoanRechargeMethodsContract? { get set }
    var consumer: LoanConsumerContract? { get set }
    var faq: LoanFAQContract? { get set }
    var auth: AuthenticationContract? { get set }
    var customerSupport: LoanCustomerSupportContract? { get set }
    var navigationInstance: NavigationContract? { get set }
}

public final class LoanLegacySetup: LoanLegacySetupContract {
    public static let shared = LoanLegacySetup()
    
    public var billet: LoanBilletContract?
    public var recharge: LoanRechargeMethodsContract?
    public var consumer: LoanConsumerContract?
    public var faq: LoanFAQContract?
    public var auth: AuthenticationContract?
    public var customerSupport: LoanCustomerSupportContract?
    public var navigationInstance: NavigationContract?

    private init() {}
    
    public func inject(instance: Any) {
        switch instance {
        case let instance as LoanBilletContract:
            billet = instance
        case let instance as LoanRechargeMethodsContract:
            recharge = instance
        case let instance as LoanConsumerContract:
            consumer = instance
        case let instance as LoanFAQContract:
            faq = instance
        case let instance as AuthenticationContract:
            auth = instance
        case let instance as LoanCustomerSupportContract:
            customerSupport = instance
        case let instance as NavigationContract:
            navigationInstance = instance
        default:
            assertionFailure("🚨\nAttempted to inject unexpected depedency: \(instance)\n")
        }
    }
}
