protocol HasSafariView {
    var safari: SafariViewDependency { get }
}

public protocol HasLegacy {
    var legacy: LoanLegacySetupContract { get }
}
