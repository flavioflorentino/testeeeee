import UI
import UIKit

public struct LoanAlertSettings {
    let imageAsset: UIImage
    let title: String
    let subtitle: String
    let okButtonTitle: String
    var cancelTitle: String = Strings.Generic.notNow
    var hideCloseButton = true
    var okAction: (() -> Void) = {}
    var hasCancel = true
    var cancelAction: (() -> Void) = {}
}

public struct LoanAlertHelper {
    enum Accessibility: String {
        case alertOkButton
        case alertCancelButton
    }

    static let shared = LoanAlertHelper()

    private init() {}

    func createAlert(with settings: LoanAlertSettings, presentIn viewController: UIViewController) {
        let popupViewController = PopupViewController(title: settings.title,
                                                      preferredType: .image(settings.imageAsset))
        popupViewController.hideCloseButton = settings.hideCloseButton

        let subtitle = settings.subtitle.attributedStringWith(normalFont: Typography.bodyPrimary().font(),
                                                              highlightFont: Typography.bodyPrimary(.highlight).font(),
                                                              normalColor: Colors.grayscale700.color,
                                                              highlightColor: Colors.grayscale700.color,
                                                              textAlignment: .center,
                                                              underline: false,
                                                              lineHeight: 24)
        popupViewController.setAttributeDescription(subtitle)

        let confirmAction = PopupAction(title: settings.okButtonTitle,
                                        style: .fill,
                                        completion: settings.okAction)
        confirmAction.accessibilityIdentifier = Accessibility.alertOkButton.rawValue
        popupViewController.addAction(confirmAction)

        if settings.hasCancel {
            let cancelAction = PopupAction(title: settings.cancelTitle,
                                           style: .link,
                                           completion: settings.cancelAction)
            cancelAction.accessibilityIdentifier = Accessibility.alertCancelButton.rawValue
            popupViewController.addAction(cancelAction)
        }

        viewController.showPopup(popupViewController)
    }
}
