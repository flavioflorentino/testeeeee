import Foundation
import Core

public struct InstallmentsDeeplinkResolver: DeeplinkResolver {
    public typealias Dependencies = HasLegacy
    private let dependencies: Dependencies

    public init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    public func canHandle(url: URL, isAuthenticated: Bool) -> DeeplinkResolverResult {
        guard url.path.contains(LoanDeeplinkPath.charging.rawValue) else {
            return .notHandleable
        }
        guard isAuthenticated else {
            return .onlyWithAuth
        }
        return .handleable
    }

    public func open(url: URL, isAuthenticated: Bool) -> Bool {
        guard canHandle(url: url, isAuthenticated: isAuthenticated) == .handleable else { return false }
        let contractIdIndex = 4

        guard url.pathComponents.indices.contains(contractIdIndex) else { return false }
        let contractId = url.pathComponents[contractIdIndex]

        return open(contractId: contractId, params: url.queryParameters)
    }
}
extension InstallmentsDeeplinkResolver {
    private func open(contractId: String, params: [String: String?]) -> Bool {
        guard let navigationController = dependencies.legacy.navigationInstance?.getCurrentNavigation()
        else {
            return false
        }
        let coordinator = LoanCoordinator(with: navigationController, from: .charge(params: params))
        dependencies.legacy.navigationInstance?.updateCurrentCoordinating(coordinator)
        coordinator.start(flow: .installments(contractId: contractId)) {
            dependencies.legacy.navigationInstance?.updateCurrentCoordinating(nil)
        }
        return true
    }
}
