import Foundation

enum LoanDeeplinkPath: String {
    case charging = "/personal-credit/aftersales/contracts"
    case documents = "/personal-credit/xp/aftersales/contracts"
}
