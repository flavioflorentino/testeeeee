import Foundation
import Core

public struct MyLoanDocumentsDeeplinkResolver: DeeplinkResolver {
    public typealias Dependencies = HasLegacy
    private let dependencies: Dependencies

    public init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    public func canHandle(url: URL, isAuthenticated: Bool) -> DeeplinkResolverResult {
        guard LoanDeeplinkPath.documents.rawValue == url.path else {
            return .notHandleable
        }
        guard isAuthenticated else {
            return .onlyWithAuth
        }
        return .handleable
    }
    
    public func open(url: URL, isAuthenticated: Bool) -> Bool {
        guard canHandle(url: url, isAuthenticated: isAuthenticated) == .handleable
        else {
            return false
        }
        return open()
    }
}

extension MyLoanDocumentsDeeplinkResolver {
    private func open() -> Bool {
        guard let navigationController = dependencies.legacy.navigationInstance?.getCurrentNavigation() else { return false }
        let coordinator = LoanCoordinator(with: navigationController, from: .deepLink)
        dependencies.legacy.navigationInstance?.updateCurrentCoordinating(coordinator)
        coordinator.start(flow: .loanAgreementsTrack) {
            dependencies.legacy.navigationInstance?.updateCurrentCoordinating(nil)
        }
        return true
    }
}
