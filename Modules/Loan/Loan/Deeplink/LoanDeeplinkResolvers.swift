import Foundation
import Core

public enum LoanDeeplinkResolvers {
    public static func make() -> [DeeplinkResolver] {
        let dependencies = LoanDependencyContainer()
        let resolvers: [DeeplinkResolver] = [
            MyLoanDocumentsDeeplinkResolver(dependencies: dependencies),
            InstallmentsDeeplinkResolver(dependencies: dependencies)
        ]
        return resolvers
    }
}
