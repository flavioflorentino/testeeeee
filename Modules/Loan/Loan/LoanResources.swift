import Foundation

// swiftlint:disable convenience_type
final class LoanResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: LoanResources.self).url(forResource: "LoanResources", withExtension: "bundle") else {
            return Bundle(for: LoanResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: LoanResources.self)
    }()
}
