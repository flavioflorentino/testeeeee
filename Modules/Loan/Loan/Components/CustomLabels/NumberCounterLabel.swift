import UIKit
import UI

typealias OptionalFormatBlock = ((Double) -> NSAttributedString)

final class NumberCounterLabel: UILabel {
    private var previousValue: Double = 0
    private var currentValue: Double = 0
    private var duration: Double = 0
    private var animationStartDate = Date()
    private var displayLink: CADisplayLink?
    var customFormatBlock: OptionalFormatBlock?
    
    func updateValue(from: Double, to: Double, animationDuration: Double? = nil) {
        displayLink?.invalidate()
        displayLink = nil
        previousValue = from
        currentValue = to
        duration = animationDuration ?? 2.0
        animationStartDate = Date()
        setupDisplayLink()
    }
    
    private func setupDisplayLink() {
        displayLink = CADisplayLink(target: self, selector: #selector(handleUpdate))
        displayLink?.add(to: .current, forMode: .common)
        displayLink?.isPaused = false
    }
    
    @objc
    private func handleUpdate() {
        let timeNow = Date()
        let elapsedTime = timeNow.timeIntervalSince(animationStartDate)
        if elapsedTime > duration {
            displayLink?.isPaused = true
            setTextValue(value: currentValue)
        } else {
            let percentage = elapsedTime / duration
            var totalValue: Double = 0
            if currentValue > previousValue {
                totalValue = previousValue + percentage * (currentValue - previousValue)
            } else {
                totalValue = previousValue - percentage * (previousValue - currentValue)
            }
            setTextValue(value: totalValue)
        }
    }
    
    private func setTextValue(value: Double) {
        attributedText = customFormatBlock?(value) ?? NSAttributedString(string: "")
    }
}
