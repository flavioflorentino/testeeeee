import UIKit

public typealias BilletCompletion = (Result<UIViewController, BilletWrapperError>) -> Void

public enum BilletWrapperError: Error {
    case unableToStart
    case unknown
}

public protocol LoanBilletContract {
    func openBilletFlow(
        withBarcode barcode: String,
        placeholder: String,
        origin: String,
        completion: @escaping BilletCompletion
    )
}
