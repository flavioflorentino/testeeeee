public protocol LoanConsumerContract {
    var consumerBalance: String { get }
    var consumerId: Int { get }
    var consumerEmail: String { get }
}
