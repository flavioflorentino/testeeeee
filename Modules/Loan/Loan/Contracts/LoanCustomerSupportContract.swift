import Foundation

public protocol LoanCustomerSupportContract {
    func show(article: FAQArticle)
    func push(article: FAQArticle)
}
