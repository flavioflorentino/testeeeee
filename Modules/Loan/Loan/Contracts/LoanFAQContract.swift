import UIKit

public typealias FAQResult = Result<FAQWebView, FAQError>

public protocol FAQWebView: UIViewController {}

public enum FAQError: Error, Equatable {
    case emptyUrl
    case couldNotOpen(_ url: String)
    case viewControllerType
    
    var localizedDescription: String {
        switch self {
        case .emptyUrl:
            return "It was not possible to open an empty URL"
        case let .couldNotOpen(url):
            return "It was not possible to open current URL: \(url)"
        case .viewControllerType:
            return "ViewController is not FAQWebView"
        }
    }
}

public protocol LoanFAQContract {
    func controller(withTitle title: String?, faqUrl: String) -> FAQResult
}

public extension LoanFAQContract {
    func controller(faqUrl: String) -> FAQResult {
        controller(withTitle: nil, faqUrl: faqUrl)
    }
}
