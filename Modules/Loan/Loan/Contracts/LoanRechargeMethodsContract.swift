import UIKit

public protocol LoanRechargeMethodsContract {
    func openRechargeLoadView() -> UIViewController
}
