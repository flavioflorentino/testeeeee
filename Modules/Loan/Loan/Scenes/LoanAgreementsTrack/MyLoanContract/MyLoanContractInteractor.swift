import Core
import Foundation
import AnalyticsModule

protocol MyLoanContractInteracting: AnyObject {
    func fillFields()
    func pop()
    func openContractPDF()
    func shareContractPDF()
    func fetch()
}

final class MyLoanContractInteractor {
    typealias Dependencies = HasSafariView & HasAnalytics
    private let dependencies: Dependencies
    private let service: MyLoanContractServicing
    private let presenter: MyLoanContractPresenting
    private let document: LoanAgreementDocument
    
    private let download: MyLoanContractDownloading
    private var urlString: String?
    private var filePath: URL?
    
    init(with document: LoanAgreementDocument, service: MyLoanContractServicing, presenter: MyLoanContractPresenting, dependencies: Dependencies, download: MyLoanContractDownloading) {
        self.service = service
        self.presenter = presenter
        self.document = document
        self.dependencies = dependencies
        self.download = download
    }
}

// MARK: - MyLoanContractInteracting
extension MyLoanContractInteractor: MyLoanContractInteracting {
    func pop() {
        presenter.pop()
    }

    func fillFields() {
        presenter.presentFilledFields(document: document)
    }
    
    func openContractPDF() {
        open()
    }
    
    func shareContractPDF() {
        share()
    }
    
    func fetch() {
        getURLString()
    }
}

private extension MyLoanContractInteractor {
    func getURLString() {
        service.fetchContractPDF(with: document.id) { [weak self] result in
            switch result {
            case .success(let link):
                self?.urlString = link.url
            case .failure:
                self?.urlString = nil
            }
        }
    }
    
    func open() {
        dependencies.analytics.log(LoanContractEvent.didTap(origin: .show))
        guard let urlString = urlString else {
            presenter.presentErrorWhenOpen()
            return
        }
        presenter.openSafari(with: urlString)
    }
    
    func share() {
        dependencies.analytics.log(LoanContractEvent.didTap(origin: .share))
        guard urlString != nil else {
            presenter.presentErrorWhenShare()
            return
        }
        handleFilePath()
    }
    
    func handleFilePath() {
        if let filePath = filePath,
           FileManager.default.fileExists(atPath: filePath.path) {
            presenter.openShare(withURL: filePath)
        } else {
            getFilePath()
        }
    }

    func getFilePath() {
        guard let urlString = urlString else { return }
        download.savePDFInDevice(urlString: urlString) { [weak self] filePath  in
            guard let filePath = filePath else {
                self?.presenter.presentErrorWhenShare()
                return }
            self?.filePath = filePath
            self?.presenter.openShare(withURL: filePath)
        }
    }
}
