import Foundation
import Core

protocol MyLoanContractServicing {
    func fetchContractPDF(with contractID: String, completion: @escaping (Result<LoanContractPDFResponse, ApiError>) -> Void)
}

final class MyLoanContractService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - MyLoanContractServicing
extension MyLoanContractService: MyLoanContractServicing {    
    func fetchContractPDF(with contractID: String, completion: @escaping (Result<LoanContractPDFResponse, ApiError>) -> Void) {
        let api = Api<LoanContractPDFResponse>(endpoint: LoanAgreementsTrackServiceEndpoint.download(contractId: contractID))
        api.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
