import Foundation
import SafariServices

protocol MyLoanContractPresenting: AnyObject {
    var viewController: MyLoanContractDisplaying? { get set }
    func pop()
    func presentFilledFields(document: LoanAgreementDocument)
    func openShare(withURL: URL)
    func presentErrorWhenOpen()
    func presentErrorWhenShare()
    func openSafari(with urlString: String)
}

final class MyLoanContractPresenter {
    private let coordinator: MyLoanContractCoordinating
    weak var viewController: MyLoanContractDisplaying?

    init(coordinator: MyLoanContractCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - MyLoanContractPresenting
extension MyLoanContractPresenter: MyLoanContractPresenting {
    func presentErrorWhenOpen() {
        viewController?.displayErrorWhenOpen()
    }
    
    func presentErrorWhenShare() {
        viewController?.displayErrorWhenShare()
    }
    
    func openShare(withURL: URL) {
        viewController?.showActivityShare(url: withURL)
    }
    
    func pop() {
        coordinator.perform(action: .pop)
    }
    
    func presentFilledFields(document: LoanAgreementDocument) {
        viewController?.displayFilledFields(document: document)
    }
    
    func openSafari(with url: String) {
        coordinator.perform(action: .openSafari(url: url))
    }
}
