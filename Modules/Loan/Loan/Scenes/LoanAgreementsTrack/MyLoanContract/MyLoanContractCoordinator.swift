import SafariServices
import Core

enum MyLoanContractAction: Equatable {
    case pop
    case openSafari(url: String)
}

protocol MyLoanContractCoordinating: AnyObject {
    func perform(action: MyLoanContractAction)
}

final class MyLoanContractCoordinator {
    typealias Dependencies = HasSafariView
    private let dependencies: Dependencies
    
    var viewController: UIViewController?
    weak var delegate: LoanAgreementsTrackCoordinating?
    
    init(with delegate: LoanAgreementsTrackCoordinating, dependencies: Dependencies) {
        self.delegate = delegate
        self.dependencies = dependencies
    }
}

// MARK: - MyLoanContractCoordinating
extension MyLoanContractCoordinator: MyLoanContractCoordinating {
    func perform(action: MyLoanContractAction) {
        switch action {
        case .pop:
            delegate?.perform(action: .pop)
        case let .openSafari(url):
            openSafari(withURL: url)
        }
    }
    
    func openSafari(withURL url: String) {
        guard
            case let .success(controller) = dependencies.safari.controller(for: url)
        else { return }
        delegate?.perform(action: .present(controller))
    }
}
