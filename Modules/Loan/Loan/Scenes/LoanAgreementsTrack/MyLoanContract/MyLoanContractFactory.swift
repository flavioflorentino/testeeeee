import UIKit

enum MyLoanContractFactory {
    static func make(with document: LoanAgreementDocument, delegate: LoanAgreementsTrackCoordinating) -> MyLoanContractViewController {
        let container = LoanDependencyContainer()
        let service: MyLoanContractServicing = MyLoanContractService(dependencies: container)
        let coordinator: MyLoanContractCoordinating = MyLoanContractCoordinator(with: delegate, dependencies: container)
        let presenter: MyLoanContractPresenting = MyLoanContractPresenter(coordinator: coordinator)
        let download = MyLoanContractDownload(dependencies: container)
        let interactor = MyLoanContractInteractor(with: document, service: service, presenter: presenter, dependencies: container, download: download)
        let viewController = MyLoanContractViewController(interactor: interactor)

        presenter.viewController = viewController

        return viewController
    }
}
