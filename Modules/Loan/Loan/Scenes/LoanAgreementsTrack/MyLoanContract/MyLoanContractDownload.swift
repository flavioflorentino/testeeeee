import Foundation
import Core

protocol MyLoanContractDownloading {
    func savePDFInDevice (urlString: String, completion: @escaping (URL?) -> Void)
}

final class MyLoanContractDownload {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}
    
extension MyLoanContractDownload: MyLoanContractDownloading {
    func savePDFInDevice (urlString: String, completion: @escaping (URL?) -> Void) {
        DispatchQueue.global().async {
            let pdfName = Strings.MyLoanContract.File.name
            let documentsDirectory = FileManager.default.temporaryDirectory
            let filePath = documentsDirectory.appendingPathComponent(pdfName)
            guard let url = URL(string: urlString) else { return }
            
            do {
                let data = try Data(contentsOf: url)
                try data.write(to: filePath, options: .atomic)
                self.dependencies.mainQueue.async {
                    completion(filePath)
                }
            } catch {
                completion(nil)
                print(error.localizedDescription)
            }
        }
    }
}
