import UI
import UIKit
import AssetsKit
import Core

private typealias Localizable = Strings.MyLoanContract

protocol MyLoanContractDisplaying: AnyObject {
    func displayFilledFields(document: LoanAgreementDocument)
    func displayErrorWhenShare()
    func displayErrorWhenOpen()
    func showActivityShare(url: URL)
}

private extension MyLoanContractViewController.Layout {
    enum Size {
        static let imageSize = 144
    }
    
    enum Height {
        static let separatorView = 1
    }
}

final class MyLoanContractViewController: ViewController<MyLoanContractInteracting, UIView> {
    fileprivate enum Layout { }
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.alwaysBounceVertical = true
        scrollView.showsVerticalScrollIndicator = false
        return scrollView
    }()
    
    private lazy var contentView = UIView()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = Resources.Illustrations.iluListChecked.image
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.title
        label
            .labelStyle(TitleLabelStyle(type: .large))
            .with(\.textColor, Colors.grayscale900.color)
        label.textAlignment = .center
        return label
    }()
    
    private lazy var topSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale100.color
        return view
    }()
    
    private lazy var detailLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.details
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, Colors.grayscale700.color)
        return label
    }()
    
    private lazy var reasonStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fill
        return stackView
    }()
    
    private lazy var reasonTitleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.reason
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale700.color)
        return label
    }()
    
    private lazy var reasonDescriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale700.color)
        label.textAlignment = .right
        return label
    }()
    
    private lazy var statusStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fill
        return stackView
    }()
    
    private lazy var statusTitleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.status
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale700.color)
        return label
    }()
    
    private lazy var statusDescriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale700.color)
        label.textAlignment = .right
        return label
    }()
    
    private lazy var bottomSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale100.color
        return view
    }()
    
    private lazy var showButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Localizable.Button.show, for: .normal)
        button.addTarget(self, action: #selector(showButtonTapped), for: .touchUpInside)
        return button
    }()
    
    private lazy var shareButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.Button.share, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(shareButtonTapped), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = Localizable.Navigation.title
        interactor.fillFields()
        interactor.fetch()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        
        scrollView.addSubview(contentView)
        
        contentView.addSubview(imageView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(topSeparatorView)
        contentView.addSubview(detailLabel)
        contentView.addSubview(reasonStackView)
        contentView.addSubview(statusStackView)
        contentView.addSubview(bottomSeparatorView)
        contentView.addSubview(showButton)
        contentView.addSubview(shareButton)
        
        reasonStackView.addArrangedSubview(reasonTitleLabel)
        reasonStackView.addArrangedSubview(reasonDescriptionLabel)
        
        statusStackView.addArrangedSubview(statusTitleLabel)
        statusStackView.addArrangedSubview(statusDescriptionLabel)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        contentView.snp.makeConstraints {
            $0.edges.width.equalToSuperview()
            $0.height.equalTo(view.compatibleSafeArea.height).priority(.low)
        }
        
        imageView.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalTo(contentView).offset(Spacing.base07)
            $0.height.equalTo(Layout.Size.imageSize)
        }
        
        titleLabel.snp.makeConstraints {
            $0.leading.trailing.equalTo(contentView).inset(Spacing.base01)
            $0.centerX.equalToSuperview()
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base03)
        }
        
        topSeparatorView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base03)
            $0.height.equalTo(Layout.Height.separatorView)
        }
        
        detailLabel.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.top.equalTo(topSeparatorView.snp.bottom).offset(Spacing.base02)
        }
        
        reasonStackView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.top.equalTo(detailLabel.snp.bottom).offset(Spacing.base02)
        }
        
        statusStackView.snp.makeConstraints {
            $0.trailing.leading.equalToSuperview().inset(Spacing.base02)
            $0.top.equalTo(reasonStackView.snp.bottom).offset(Spacing.base01)
        }
        
        bottomSeparatorView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.top.equalTo(statusStackView.snp.bottom).offset(Spacing.base02)
            $0.height.equalTo(Layout.Height.separatorView)
        }
        
        showButton.snp.makeConstraints {
            $0.top.greaterThanOrEqualTo(bottomSeparatorView.snp.bottom).offset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        shareButton.snp.makeConstraints {
            $0.top.equalTo(showButton.snp.bottom).offset(Spacing.base02)
            $0.centerX.equalToSuperview()
            $0.bottom.equalToSuperview().offset(-Spacing.base03)
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
    }
    
    private func setupNavigationBar() {
        navigationController?.navigationBar.navigationStyle(StandardNavigationStyle())
        navigationController?.setupLoanAppearance()
    }
}

// MARK: - MyLoanContractDisplaying
extension MyLoanContractViewController: MyLoanContractDisplaying {
    func displayErrorWhenShare() {
        alertDialog(okAction: interactor.shareContractPDF)
    }
    
    func displayErrorWhenOpen() {
        alertDialog(okAction: interactor.openContractPDF)
    }
    
    func displayFilledFields(document: LoanAgreementDocument) {
        reasonDescriptionLabel.text = document.loanGoal.description
        statusDescriptionLabel.text = document.status.situation
    }
    
    func showActivityShare(url: URL) {
        let activityController = UIActivityViewController(activityItems: [url], applicationActivities: nil)
        activityController.popoverPresentationController?.sourceView = view
        present(activityController, animated: true)
    }
}

// MARK: - Helper Methods
extension MyLoanContractViewController {
    @objc
    func showButtonTapped() {
        interactor.openContractPDF()
    }
    
    @objc
    func shareButtonTapped() {
        interactor.shareContractPDF()
    }
    
    private func alertDialog(okAction: @escaping (() -> Void)) {
        let primaryButtonAction = ApolloAlertAction(title: Strings.Generic.tryAgain) {
            self.interactor.fetch()
            okAction()
        }
        
        let linkButtonAction = ApolloAlertAction(title: Strings.Generic.notNow) {
            self.interactor.pop()
        }
        
        showApolloAlert(type: .sticker(style: .error),
                        title: Strings.Generic.title,
                        subtitle: Strings.Generic.message,
                        primaryButtonAction: primaryButtonAction,
                        linkButtonAction: linkButtonAction)
    }
}
