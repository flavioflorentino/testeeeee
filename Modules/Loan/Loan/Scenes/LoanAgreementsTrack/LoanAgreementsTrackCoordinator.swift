import Foundation
import UI
import UIKit

enum LoanAgreementsTrackAction: Equatable {
    case myLoanDocuments
    case myLoanContract(document: LoanAgreementDocument)
    case close
    case pop
    case present(_ controller: UIViewController)
}

protocol LoanAgreementsTrackCoordinating: Coordinating {
    func perform(action: LoanAgreementsTrackAction)
}

final class LoanAgreementsTrackCoordinator: LoanAgreementsTrackCoordinating {
    var childViewController: [UIViewController] = []
    var viewController: UIViewController?
    var navigationController: UINavigationController
    private weak var delegate: LoanFlowCoordinating?
    
    init(from navigationController: UINavigationController, delegate: LoanFlowCoordinating) {
        self.delegate = delegate
        self.navigationController = navigationController
    }
    
    func perform(action: LoanAgreementsTrackAction) {
        switch action {
        case .close:
            delegate?.close()
        case .pop:
            navigationController.popViewController(animated: true)
        case let .present(controller):
            navigationController.present(controller, animated: true)
        default:
            handleFlow(action)
        }
    }
    
    private func handleFlow(_ action: LoanAgreementsTrackAction) {
        let controller: UIViewController
        switch action {
        case .myLoanDocuments:
            controller = MyLoanDocumentsFactory.make(delegate: self)
        case let .myLoanContract(document):
            controller = MyLoanContractFactory.make(with: document, delegate: self)
        default:
            return
        }
        navigationController.pushViewController(controller, animated: true)
    }
}
