import UIKit

enum MyLoanDocumentsAction: Equatable {
    case myLoanContract(document: LoanAgreementDocument)
    case pop
    case openFAQ(article: FAQArticle)
}

protocol MyLoanDocumentsCoordinating: AnyObject {
    func perform(action: MyLoanDocumentsAction)
}

final class MyLoanDocumentsCoordinator {
    typealias Dependencies = HasLegacy
    private let dependencies: Dependencies
    var viewController: UIViewController?
    weak var delegate: LoanAgreementsTrackCoordinating?
    
    init(with delegate: LoanAgreementsTrackCoordinating, dependencies: Dependencies) {
        self.delegate = delegate
        self.dependencies = dependencies
    }
}

// MARK: - LoanDocumentsCoordinating
extension MyLoanDocumentsCoordinator: MyLoanDocumentsCoordinating {
    func perform(action: MyLoanDocumentsAction) {
        switch action {
        case let .myLoanContract(contract):
            delegate?.perform(action: .myLoanContract(document: contract))
        case .pop:
            delegate?.perform(action: .pop)
        case let .openFAQ(article):
            dependencies.legacy.customerSupport?.show(article: article)
        }
    }
}
