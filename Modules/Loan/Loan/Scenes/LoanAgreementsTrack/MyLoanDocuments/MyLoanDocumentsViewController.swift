import UI
import UIKit
import AssetsKit

protocol MyLoanDocumentsDisplaying: AnyObject {
    func displayItems(data: [MyLoanDocumentViewModel])
    func displayError()
    func startLoading()
    func stopLoading()
    func displayConectionError()
    func displayLoanInfoView()
    func displayFeedbackDialog()
}

private extension MyLoanDocumentsViewController.Layout {
    enum Height {
        static let tableViewRow: CGFloat = 96
        static let separatorView = 1
    }
    
    enum Size {
        static let infoImageSize = 20
        static let infoCloseButton = CGSize(width: 72, height: 24)
    }
    
    enum Duration {
        static let animationTime = 0.5
    }
}

final class MyLoanDocumentsViewController: ViewController<MyLoanDocumentsInteracting, UIView> {
    fileprivate enum Layout { }
    
    enum AccessibilityIdentifier: String {
        case documentsTableView
    }
    
    private lazy var FAQBarButtonItem: UIBarButtonItem = {
        let label = UILabel()
        label
            .labelStyle(IconLabelStyle(type: .medium))
            .with(\.textColor, .branding600())
            .with(\.text, Iconography.questionCircle.rawValue)
            .with(\.isUserInteractionEnabled, true)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(FAQButtonTapped))
        label.addGestureRecognizer(tapGesture)
        let barButton = UIBarButtonItem(customView: label)
        return barButton
    }()
        
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.MyLoanDocuments.title
        label
            .labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, Colors.black.color)
        label.isHidden = true
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base03
        return stackView
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.MyLoanDocuments.description
        label.isHidden = true
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, Colors.grayscale700.color)
        return label
    }()
    
    private lazy var loanInfoView: UIView = {
       let view = UIView()
        view.layer.borderWidth = 1
        view.layer.borderColor = Colors.grayscale100.color.cgColor
        view.layer.cornerRadius = 8
        view.clipsToBounds = true
        view.isHidden = true
        return view
    }()
    
    private lazy var loanInfoIconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = Resources.Icons.icoBlueInfo.image
        return imageView
    }()
    
    private lazy var loanInfoLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.MyLoanDocuments.information
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.grayscale600.color)
        return label
    }()
    
    private lazy var loanInfoCloseButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.MyLoanDocuments.InformationCloseButton.text, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(infoCloseButtonTapped), for: .touchUpInside)
        return button
    }()
    
    private lazy var separatorView: UIView = {
       let view = UIView()
        view.backgroundColor = Colors.grayscale100.color
        return view
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .singleLine
        tableView.separatorInset = .zero
        tableView.rowHeight = Layout.Height.tableViewRow
        tableView.accessibilityIdentifier = AccessibilityIdentifier.documentsTableView.rawValue
        tableView.tableFooterView = UIView()
        tableView.register(MyLoanDocumentCell.self, forCellReuseIdentifier: MyLoanDocumentCell.identifier)
        return tableView
    }()
        
    private lazy var dataSource: TableViewDataSource<Int, MyLoanDocumentViewModel> = {
        let dataSource = TableViewDataSource<Int, MyLoanDocumentViewModel>(view: tableView)
        dataSource.add(section: 0)
        dataSource.itemProvider = { tableView, indexPath, item -> UITableViewCell? in
            let cell = tableView.dequeueReusableCell(withIdentifier: MyLoanDocumentCell.identifier, for: indexPath) as? MyLoanDocumentCell
            cell?.setup(with: item)
            return cell
        }
        return dataSource
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.fetch()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(titleLabel)
        view.addSubview(stackView)
        view.addSubview(separatorView)
        view.addSubview(tableView)
        
        stackView.addArrangedSubview(descriptionLabel)
        stackView.addArrangedSubview(loanInfoView)
        
        loanInfoView.addSubview(loanInfoLabel)
        loanInfoView.addSubview(loanInfoIconImageView)
        loanInfoView.addSubview(loanInfoCloseButton)
    }
    
    override func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(topLayoutGuide.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        stackView.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
                
        loanInfoIconImageView.snp.makeConstraints {
            $0.top.leading.equalToSuperview().inset(Spacing.base02)
            $0.size.equalTo(Layout.Size.infoImageSize)
        }
        
        loanInfoLabel.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base02)
            $0.leading.equalTo(loanInfoIconImageView.snp.trailing).offset(Spacing.base01)
        }
        
        loanInfoCloseButton.snp.makeConstraints {
            $0.centerY.equalTo(loanInfoLabel)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.size.equalTo(Layout.Size.infoCloseButton)
            $0.leading.greaterThanOrEqualTo(loanInfoLabel.snp.trailing).offset(Spacing.base01)
        }
        
        separatorView.snp.makeConstraints {
            $0.top.equalTo(stackView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(Layout.Height.separatorView)
        }
        
        tableView.snp.makeConstraints {
            $0.top.equalTo(separatorView.snp.bottom)
            $0.bottom.leading.trailing.equalToSuperview()
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        tableView.dataSource = dataSource
    }
    
    private func setupNavigationBar() {
        navigationController?.setupLoanAppearance()
        navigationItem.rightBarButtonItem = FAQBarButtonItem
    }
}
    
// MARK: - LoanDocumentsDisplaying
extension MyLoanDocumentsViewController: MyLoanDocumentsDisplaying {
    func displayItems(data: [MyLoanDocumentViewModel]) {
        dataSource.update(items: data, from: 0)
    }
    
    func displayError() {
        showAlertDialog(imageAsset: Resources.Illustrations.iluError.image,
                        title: Strings.Generic.title,
                        subtitle: Strings.Generic.message)
    }
    
    func displayConectionError() {
        showAlertDialog(imageAsset: Resources.Illustrations.iluNoConnectionGreenBackground.image,
                        title: Strings.Generic.ConectionError.title,
                        subtitle: Strings.Generic.ConectionError.message)
    }
    
    func displayFeedbackDialog() {
        let primaryButtonAction = ApolloAlertAction(title: Strings.Generic.ok) {}
        
        showApolloAlert(type: .sticker(style: .info),
                        title: Strings.MyLoanDocuments.Warning.title,
                        subtitle: Strings.MyLoanDocuments.Warning.subtitle,
                        primaryButtonAction: primaryButtonAction)
    }
    
    func startLoading() {
        beginState()
    }
    
    func stopLoading() {
        showHeaderLabels()
        endState()
    }
}

// MARK: - StatefulTransitionViewing
extension MyLoanDocumentsViewController: StatefulTransitionViewing {
    func statefulViewForLoading() -> StatefulViewing {
        MyLoanDocumentSkeletonView()
    }
}

// MARK: - TableView Delegate Protocol
extension MyLoanDocumentsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        interactor.didTapRow(index: indexPath)
    }
}

// MARK: - Helper Methods
extension MyLoanDocumentsViewController {
    func displayLoanInfoView() {
        loanInfoView.isHidden = false
    }
    
    @objc
    private func infoCloseButtonTapped() {
        UIView.transition(with: self.view,
                          duration: Layout.Duration.animationTime,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.loanInfoView.alpha = 0
                          }, completion: {_ in
                            self.loanInfoView.removeFromSuperview()
                          })
    }
    
    @objc
    func FAQButtonTapped() {
        interactor.openFAQ()
    }
    
    func showAlertDialog(imageAsset: UIImage, title: String, subtitle: String) {
        let primaryAlertButton = ApolloAlertAction(title: Strings.Generic.tryAgain) {
            self.interactor.fetch()
        }
        
        let linkButtonAction = ApolloAlertAction(title: Strings.Generic.notNow) {
            self.interactor.close()
        }
        
        showApolloAlert(image: imageAsset,
                        title: title,
                        subtitle: subtitle,
                        primaryButtonAction: primaryAlertButton,
                        linkButtonAction: linkButtonAction)
    }
    
    func showHeaderLabels() {
        titleLabel.isHidden = false
        descriptionLabel.isHidden = false
    }
}
