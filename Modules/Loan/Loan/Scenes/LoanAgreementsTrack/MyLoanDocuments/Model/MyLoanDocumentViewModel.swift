struct MyLoanDocumentViewModel {
    let title: String
    let status: LoanTrackStatus
    let subtitleDate: String
    let category: LoanReason
    let documentSent: Bool
    
    init(with loan: LoanAgreementDocument) {
        title = loan.loanGoal.category.description
        category = loan.loanGoal.category
        status = loan.status
        subtitleDate = loan.contractEnd ?? ""
        documentSent = loan.documentSent
    }
}
