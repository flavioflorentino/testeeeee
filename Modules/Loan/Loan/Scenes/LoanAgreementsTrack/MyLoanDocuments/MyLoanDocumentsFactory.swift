import UIKit

enum MyLoanDocumentsFactory {
    static func make(delegate: LoanAgreementsTrackCoordinating) -> MyLoanDocumentsViewController {
        let container = LoanDependencyContainer()
        let service: MyLoanDocumentsServicing = MyLoanDocumentsService(dependencies: container)
        let coordinator: MyLoanDocumentsCoordinating = MyLoanDocumentsCoordinator(with: delegate, dependencies: container)
        let presenter: MyLoanDocumentsPresenting = MyLoanDocumentsPresenter(coordinator: coordinator)
        let interactor = MyLoanDocumentsInteractor(service: service, presenter: presenter, dependencies: container)
        let viewController = MyLoanDocumentsViewController(interactor: interactor)
        
        presenter.viewController = viewController

        return viewController
    }
}
