import Foundation

protocol MyLoanDocumentsPresenting: AnyObject {
    var viewController: MyLoanDocumentsDisplaying? { get set }
    func didNextStep(action: MyLoanDocumentsAction)
    func present(loans: [LoanAgreementDocument])
    func startLoading()
    func stopLoading()
    func presentError()
    func presentConectionError()
    func close()
    func presentLoanInfoView()
    func presentFeedbackDialog()
    func openFAQ(article: FAQArticle)
}

final class MyLoanDocumentsPresenter {
    private let coordinator: MyLoanDocumentsCoordinating
    weak var viewController: MyLoanDocumentsDisplaying?

    init(coordinator: MyLoanDocumentsCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - LoanDocumentsPresenting
extension MyLoanDocumentsPresenter: MyLoanDocumentsPresenting {
    func present(loans: [LoanAgreementDocument]) {
        viewController?.displayItems(data: loans.map(MyLoanDocumentViewModel.init(with:)))
    }
    
    func didNextStep(action: MyLoanDocumentsAction) {
        coordinator.perform(action: action)
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
    
    func presentError() {
        viewController?.displayError()
    }
    
    func presentConectionError() {
        viewController?.displayConectionError()
    }
    
    func presentLoanInfoView() {
        viewController?.displayLoanInfoView()
    }
    
    func presentFeedbackDialog() {
        viewController?.displayFeedbackDialog()
    }
    
    func close() {
        coordinator.perform(action: .pop)
    }
    
    func openFAQ(article: FAQArticle) {
        coordinator.perform(action: .openFAQ(article: article))
    }
}
