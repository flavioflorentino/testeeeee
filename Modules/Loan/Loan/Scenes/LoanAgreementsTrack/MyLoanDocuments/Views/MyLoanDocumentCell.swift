import UI
import UIKit
import AssetsKit

extension MyLoanDocumentCell.Layout {
    enum Size {
        static let icon = CGSize(width: 48, height: 48)
    }
    
    enum Height {
        static let progressView: CGFloat = 8
    }
}

final class MyLoanDocumentCell: UITableViewCell {
    fileprivate enum Layout {}
    
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView
            .imageStyle(RoundedImageStyle(size: .medium, cornerRadius: .full))
            .with(\.border, .light(color: .grayscale100()))
        imageView.contentMode = .center
        return imageView
    }()
    
    private lazy var iconLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(IconLabelStyle(type: .medium))
            .with(\.textAlignment, .center)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
        return label
    }()
    
    private lazy var subtitleStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = Spacing.base00
        return stackView
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale500())
        return label
    }()
    
    private lazy var subtitleDateLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
        label.textAlignment = .left
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(with loan: MyLoanDocumentViewModel) {
        titleLabel.text = loan.title
        subtitleLabel.text = loan.status.description
        handleContractStatus(loan)
    }
    
    func handleContractStatus(_ loan: MyLoanDocumentViewModel) {
        iconLabel.text = loan.category.imageName
        switch loan.status {
        case .active:
            iconLabel.textColor = Colors.branding600.color
            subtitleDateLabel.isHidden = true
        case .completed:
            iconLabel.textColor = Colors.grayscale400.color
            subtitleDateLabel.isHidden = false
            subtitleDateLabel.text = loan.subtitleDate
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        iconImageView.image = nil
    }
}

extension MyLoanDocumentCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(iconImageView)
        contentView.addSubview(stackView)
        
        iconImageView.addSubview(iconLabel)
        
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(subtitleStackView)
        
        subtitleStackView.addArrangedSubview(subtitleLabel)
        subtitleStackView.addArrangedSubview(subtitleDateLabel)
    }
    
    func setupConstraints() {
        iconImageView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base03)
            $0.size.equalTo(Layout.Size.icon)
        }
        
        iconLabel.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.centerX.equalToSuperview()
        }
        
        stackView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalTo(iconImageView.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(Spacing.base00)
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
        selectionStyle = .none
        accessoryView = UIImageView(image: Resources.Icons.icoGreenRightArrow.image)
    }
}
