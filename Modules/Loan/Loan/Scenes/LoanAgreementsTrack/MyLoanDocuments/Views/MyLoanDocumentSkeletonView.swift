import UI
import UIKit
import SkeletonView

// MARK: - Layout
private extension MyLoanDocumentSkeletonView.Layout {
    enum Cell { }

    enum Size {
        static let line: CGFloat = 16.0
    }

    enum CornerRadius {
        static let line: CGFloat = Size.line / 2.0
    }
}

private extension MyLoanDocumentSkeletonView.Layout.Cell {
    enum Size {
        static let container: CGFloat = 96.0
        static let separatorView = 1
    }
}

final class MyLoanDocumentSkeletonView: UIView, StatefulViewing {
    fileprivate enum Layout { }

    // MARK: - Properties
    private lazy var tableHeader = TableHeader()
    
    private lazy var sectionHeaderFirstLine: UIView = {
        let view = UIView()
        view.layer.cornerRadius = Layout.CornerRadius.line
        view.clipsToBounds = true
        view.isSkeletonable = true

        return view
    }()
    
    private lazy var sectionHeaderSecondLine: UIView = {
        let view = UIView()
        view.layer.cornerRadius = Layout.CornerRadius.line
        view.clipsToBounds = true
        view.isSkeletonable = true

        return view
    }()

    private lazy var cellsStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [Cell(), Cell()])
        stackView.axis = .vertical

        return stackView
    }()

    private let gradient = SkeletonGradient(baseColor: Colors.grayscale100.color)
    
    var viewModel: StatefulViewModeling?
    weak var delegate: StatefulDelegate?

    // MARK: - Initialization
    convenience init() {
        self.init(frame: .zero)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func updateConstraints() {
        super.updateConstraints()
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        showAnimatedGradientSkeleton(usingGradient: gradient)
    }
}

// MARK: - ViewConfiguration
extension MyLoanDocumentSkeletonView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(tableHeader)
        addSubview(sectionHeaderFirstLine)
        addSubview(sectionHeaderSecondLine)
        addSubview(cellsStackView)
    }

    func setupConstraints() {
        tableHeader.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base06 * 2)
            $0.top.equalTo(snp_topMargin).offset(Spacing.base02)
        }
        
        sectionHeaderFirstLine.snp.makeConstraints {
            $0.top.equalTo(tableHeader.snp.bottom).offset(Spacing.base06)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base06 * 2)
            $0.height.equalTo(Layout.Size.line)
        }
        
        sectionHeaderSecondLine.snp.makeConstraints {
            $0.top.equalTo(sectionHeaderFirstLine.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base10 * 2)
            $0.height.equalTo(Layout.Size.line)
        }
        
        cellsStackView.snp.makeConstraints {
            $0.top.equalTo(sectionHeaderSecondLine.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
        }
    }

    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
}

// MARK: - Nested types
private extension MyLoanDocumentSkeletonView {
    final class TableHeader: UIView, ViewConfiguration {
        private lazy var firstLineView: UIView = {
            let view = UIView()
            view.layer.cornerRadius = Layout.CornerRadius.line
            view.clipsToBounds = true
            view.isSkeletonable = true

            return view
        }()
        
        private let gradient = SkeletonGradient(baseColor: Colors.grayscale100.color)
        
        // MARK: - Initialization
        convenience init() {
            self.init(frame: .zero)
        }

        override init(frame: CGRect) {
            super.init(frame: frame)

            buildLayout()
        }

        @available(*, unavailable)
        public required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }

        func buildViewHierarchy() {
            addSubview(firstLineView)
        }

        func setupConstraints() {
            firstLineView.snp.makeConstraints {
                $0.top.equalToSuperview()
                $0.leading.equalToSuperview()
                $0.trailing.equalToSuperview()
                $0.height.equalTo(Layout.Size.line)
            }
        }

        func configureViews() {
            backgroundColor = Colors.backgroundPrimary.color
        }

        override func layoutSubviews() {
            super.layoutSubviews()

            showAnimatedGradientSkeleton(usingGradient: gradient)
        }
    }
}

private extension MyLoanDocumentSkeletonView {
    final class Cell: UIView, ViewConfiguration {
        private lazy var containerView: UIView = {
            let view = UIView()
            view.backgroundColor = Colors.backgroundPrimary.color
            view.isSkeletonable = true

            return view
        }()
        
        private lazy var topSeparatorView: UIView = {
           let view = UIView()
            view.backgroundColor = Colors.grayscale100.color
            
            return view
        }()
        
        private let gradient = SkeletonGradient(baseColor: Colors.grayscale100.color)
        
        private lazy var iconImageView: UIImageView = {
            let imageView = UIImageView()
            imageView.imageStyle(RoundedImageStyle(size: .medium, cornerRadius: .full))
            imageView.isSkeletonable = true
            
            return imageView
        }()

        private lazy var linesContainerView = UIView()

        private lazy var firstLineView: UIView = {
            let view = UIView()
            view.layer.cornerRadius = Layout.CornerRadius.line
            view.clipsToBounds = true
            view.isSkeletonable = true

            return view
        }()

        private lazy var secondLineView: UIView = {
            let view = UIView()
            view.layer.cornerRadius = Layout.CornerRadius.line
            view.clipsToBounds = true
            view.isSkeletonable = true

            return view
        }()
        
        private lazy var bottomSeparatorView: UIView = {
           let view = UIView()
            view.backgroundColor = Colors.grayscale100.color
            
            return view
        }()

        // MARK: - Initialization
        convenience init() {
            self.init(frame: .zero)
        }

        override init(frame: CGRect) {
            super.init(frame: frame)

            buildLayout()
        }

        @available(*, unavailable)
        public required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }

        func buildViewHierarchy() {
            addSubview(topSeparatorView)
            addSubview(containerView)
            addSubview(bottomSeparatorView)
            
            containerView.addSubview(iconImageView)
            containerView.addSubview(linesContainerView)

            linesContainerView.addSubview(firstLineView)
            linesContainerView.addSubview(secondLineView)
        }

        func setupConstraints() {
            topSeparatorView.snp.makeConstraints {
                $0.top.equalToSuperview()
                $0.leading.trailing.equalToSuperview()
                $0.height.equalTo(Layout.Cell.Size.separatorView)
            }
            
            containerView.snp.makeConstraints {
                $0.leading.trailing.bottom.equalToSuperview()
                $0.top.equalTo(topSeparatorView.snp.bottom)
                $0.height.equalTo(Layout.Cell.Size.container)
            }
            
            iconImageView.snp.makeConstraints {
                $0.leading.equalToSuperview().offset(Spacing.base03)
                $0.centerY.equalToSuperview()
            }
            
            linesContainerView.snp.makeConstraints {
                $0.leading.equalTo(iconImageView.snp.trailing).offset(Spacing.base02)
                $0.trailing.equalToSuperview().offset(-Spacing.base05)
                $0.centerY.equalToSuperview()
            }
            
            firstLineView.snp.makeConstraints {
                $0.top.leading.trailing.equalToSuperview()
                $0.height.equalTo(Layout.Size.line)
            }
            
            secondLineView.snp.makeConstraints {
                $0.top.equalTo(firstLineView.snp.bottom).offset(Spacing.base02)
                $0.bottom.leading.equalToSuperview()
                $0.trailing.equalToSuperview().offset(-Spacing.base08)
                $0.height.equalTo(Layout.Size.line)
            }
            
            bottomSeparatorView.snp.makeConstraints {
                $0.leading.trailing.equalToSuperview()
                $0.top.equalTo(containerView.snp.bottom)
                $0.height.equalTo(Layout.Cell.Size.separatorView)
            }
        }

        func configureViews() {
            backgroundColor = Colors.backgroundPrimary.color
        }

        override func layoutSubviews() {
            super.layoutSubviews()
            
            firstLineView.showAnimatedGradientSkeleton(usingGradient: gradient)
            secondLineView.showAnimatedGradientSkeleton(usingGradient: gradient)
            iconImageView.showAnimatedGradientSkeleton(usingGradient: gradient)
        }
    }
}
