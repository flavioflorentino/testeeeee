import Foundation
import Core
import AnalyticsModule

protocol MyLoanDocumentsInteracting: AnyObject {
    func fetch()
    func didTapRow(index: IndexPath)
    func close()
    func openFAQ()
}

final class MyLoanDocumentsInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let service: MyLoanDocumentsServicing
    private let presenter: MyLoanDocumentsPresenting
    private var loans: [LoanAgreementDocument]
    
    init(service: MyLoanDocumentsServicing, presenter: MyLoanDocumentsPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.loans = []
        self.dependencies = dependencies
    }
}

// MARK: - LoanDocumentsInteracting
extension MyLoanDocumentsInteractor: MyLoanDocumentsInteracting {
    func fetch() {
        requestLoanAgreementDocumentsList()
    }
    
    func didTapRow(index: IndexPath) {
        guard index.row >= 0 && index.row < loans.count else { return }
        let selectedLoanDocument = loans[index.row]
        setupAnalytics(with: selectedLoanDocument)
        selectedLoanDocument.documentSent ?
            presenter.didNextStep(action: .myLoanContract(document: selectedLoanDocument)) :
            presenter.presentFeedbackDialog()
    }
    
    func openFAQ() {
        presenter.openFAQ(article: .contracts)
    }
    
    func close() {
        presenter.close()
    }
}

private extension MyLoanDocumentsInteractor {
    func requestLoanAgreementDocumentsList() {
        presenter.startLoading()
        service.getLoanAgreementsToTrack { [weak self] result in
            guard let self = self else { return }
            self.presenter.stopLoading()
            switch result {
            case .success(let response):
                self.loans = response.loans
                self.presenter.present(loans: self.loans)
                self.loanInfoViewIfNeeded()
            case .failure(let error):
                self.dependencies.analytics.log(LoanDocumentsEvent.didFail)
                self.handleError(error: error)
            }
        }
    }
    
    private func loanInfoViewIfNeeded() {
        guard loans.contains(where: { !$0.documentSent }) else {
            dependencies.analytics.log(LoanDocumentsEvent.didView(hasContractUnavailable: false))
            return
        }
        dependencies.analytics.log(LoanDocumentsEvent.didView(hasContractUnavailable: true))
        presenter.presentLoanInfoView()
    }
    
    private func handleError(error: ApiError) {
        switch error {
        case .connectionFailure:
            presenter.presentConectionError()
        default:
            presenter.presentError()
        }
    }
    
    func setupAnalytics(with selectedLoanDocument: LoanAgreementDocument) {
        guard selectedLoanDocument.status == .completed else {
            let documentSent = selectedLoanDocument.documentSent
            dependencies.analytics.log(LoanDocumentsEvent.didSelect(status: documentSent ? .available : .unavailable))
            return
        }
        
        dependencies.analytics.log(LoanDocumentsEvent.didSelect(status: .completed))
    }
}
