import Core
import Foundation

protocol MyLoanDocumentsServicing {
    func getLoanAgreementsToTrack(completion: @escaping (Result<LoanAgreementDocumentsResponse, ApiError>) -> Void)
}

struct MyLoanDocumentsService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - LoanDocumentsServicing
extension MyLoanDocumentsService: MyLoanDocumentsServicing {    
    func getLoanAgreementsToTrack(completion: @escaping (Result<LoanAgreementDocumentsResponse, ApiError>) -> Void) {
        let api = Api<LoanAgreementDocumentsResponse>(endpoint: LoanAgreementsTrackServiceEndpoint.loanDocumentsList)

        api.execute { result in
            dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
