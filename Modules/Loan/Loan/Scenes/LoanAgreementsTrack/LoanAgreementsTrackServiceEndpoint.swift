import Core
import Foundation

enum LoanAgreementsTrackServiceEndpoint {
    case loanDocumentsList
    case download(contractId: String)
}

extension LoanAgreementsTrackServiceEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .loanDocumentsList:
            return "personal-credit-aftersales/customer-service/v2/contracts"
        case let .download(contractID):
            return "personal-credit-aftersales/customer-service/v1/contracts/\(contractID)/documents/url"
        }
    }
}
