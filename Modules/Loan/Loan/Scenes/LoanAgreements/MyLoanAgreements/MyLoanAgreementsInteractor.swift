import AnalyticsModule
import Core
import Foundation

protocol MyLoanAgreementsInteracting: AnyObject {
    func trackEnterView()
    func fetch()
    func didTapRow(index: IndexPath)
    func back()
    func didTapFAQ()
}

final class MyLoanAgreementsInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let service: MyLoanAgreementsServicing
    private let presenter: MyLoanAgreementsPresenting
    private var loans: [LoanAgreement]
    private var origin: OriginType
    
    init(service: MyLoanAgreementsServicing,
         presenter: MyLoanAgreementsPresenting,
         from origin: OriginType,
         dependencies: Dependencies) {
        self.dependencies = dependencies
        self.service = service
        self.presenter = presenter
        self.loans = []
        self.origin = origin
    }
}

extension MyLoanAgreementsInteractor: MyLoanAgreementsInteracting {
    func trackEnterView() {
        dependencies.analytics.log(LoanEvent.didEnter(to: .myLoanAgreements, from: origin))
    }
    
    func fetch() {
        requestLoanAgreementsList()
    }
    
    func didTapRow(index: IndexPath) {
        guard loans.indices.contains(index.row) else { return }
        let selectedLoan = loans[index.row]
        presenter.didNextStep(action: .installments(contractId: selectedLoan.id))
    }
    
    func back() {
        presenter.back()
    }
    
    func didTapFAQ() {
        presenter.openFAQ(.installments)
    }
}

private extension MyLoanAgreementsInteractor {
    func requestLoanAgreementsList() {
        presenter.startLoading()
        
        service.getLoanAgreements { [weak self] result in
            self?.presenter.stopLoading()
            
            switch result {
            case .success(let response):
                let loans = response.loans
                self?.loans = loans
                self?.presenter.present(loans: loans)
            case .failure:
                self?.presenter.presentError()
            }
        }
    }
}
