import UI
import UIKit
import AssetsKit

extension LoanAgreementCell.Layout {
    enum Size {
        static let icon = CGSize(width: 48, height: 48)
    }
    
    enum Height {
        static let progressView: CGFloat = 8
    }
}

final class LoanAgreementCell: UITableViewCell {
    fileprivate enum Layout {}
    
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView
            .imageStyle(RoundedImageStyle(size: .medium, cornerRadius: .full))
            .with(\.border, .light(color: .grayscale100()))
        imageView.contentMode = .center
        return imageView
    }()
    
    private lazy var iconLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(IconLabelStyle(type: .medium))
            .with(\.textColor, .branding600())
            .with(\.textAlignment, .center)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
        return label
    }()
    
    private lazy var installmentsLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale500())
        return label
    }()
    
    private lazy var progressView: UIProgressView = {
        let view = UIProgressView(progressViewStyle: .bar)
        view.progressTintColor = Colors.branding600.color
        view.trackTintColor = Colors.grayscale200.color
        view.layer.cornerRadius = Layout.Height.progressView / 2
        view.clipsToBounds = true
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(with loan: LoanAgreementViewModel) {
        iconLabel.text = loan.category.imageName
        titleLabel.text = loan.title
        installmentsLabel.text = loan.installmentsInfo
        progressView.setProgress(loan.installmentPercentage, animated: true)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        iconImageView.image = nil
    }
}

extension LoanAgreementCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(iconImageView)
        contentView.addSubview(stackView)
        
        iconImageView.addSubview(iconLabel)
        
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(installmentsLabel)
        stackView.addArrangedSubview(progressView)
        
        if #available(iOS 11, *) {
            stackView.setCustomSpacing(Spacing.base00, after: titleLabel)
            stackView.setCustomSpacing(Spacing.base01, after: installmentsLabel)
        }
    }
    
    func setupConstraints() {
        iconImageView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base03)
            $0.size.equalTo(Layout.Size.icon)
        }
        
        iconLabel.snp.makeConstraints {
            $0.centerX.centerY.equalToSuperview()
        }
        
        stackView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalTo(iconImageView.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        progressView.snp.makeConstraints {
            $0.height.equalTo(Layout.Height.progressView)
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
        selectionStyle = .none
        accessoryView = UIImageView(image: Resources.Icons.icoGreenRightArrow.image)
    }
}
