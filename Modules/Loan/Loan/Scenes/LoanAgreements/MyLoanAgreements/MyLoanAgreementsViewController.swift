import UI
import UIKit
import AssetsKit

protocol MyLoanAgreementsDisplay: AnyObject {
    func displayItems(data: [LoanAgreementViewModel])
    func displayEmptyState()
    func displayError()
    func startLoading()
    func stopLoading()
}

private extension MyLoanAgreementsViewController.Layout {
    enum Height {
        static let tableViewRow: CGFloat = 96
        static let separatorView = 1
    }
}

final class MyLoanAgreementsViewController: ViewController<MyLoanAgreementsInteracting, UIView>, LoadingViewProtocol {
    fileprivate enum Layout { }
    
    enum AccessibilityIdentifier: String {
        case loansTableView
    }
    
    lazy var loadingView: LoadingView = {
        let loading = LoadingView()
        loading.text = Strings.Generic.Loading.text
        return loading
    }()
    
    private lazy var FAQBarButtonItem: UIBarButtonItem = {
        let label = UILabel()
        label
            .labelStyle(IconLabelStyle(type: .medium))
            .with(\.textColor, .branding600())
            .with(\.text, Iconography.questionCircle.rawValue)
            .with(\.isUserInteractionEnabled, true)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(FAQButtonTapped))
        label.addGestureRecognizer(tapGesture)
        let barButton = UIBarButtonItem(customView: label)
        return barButton
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.MyLoanAgreements.title
        label
            .labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, Colors.black.color)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.MyLoanAgreements.description
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, Colors.grayscale700.color)
        return label
    }()
    
    private lazy var separatorView: UIView = {
       let view = UIView()
        view.backgroundColor = Colors.grayscale100.color
        return view
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .singleLine
        tableView.separatorInset = .zero
        tableView.rowHeight = Layout.Height.tableViewRow
        tableView.accessibilityIdentifier = AccessibilityIdentifier.loansTableView.rawValue
        tableView.tableFooterView = UIView()
        tableView.register(LoanAgreementCell.self, forCellReuseIdentifier: LoanAgreementCell.identifier)
        return tableView
    }()
    
    private lazy var dataSource: TableViewDataSource<Int, LoanAgreementViewModel> = {
        let dataSource = TableViewDataSource<Int, LoanAgreementViewModel>(view: tableView)
        dataSource.add(section: 0)
        dataSource.itemProvider = { tableView, indexPath, item -> UITableViewCell? in
            let cell = tableView.dequeueReusableCell(withIdentifier: LoanAgreementCell.identifier, for: indexPath) as? LoanAgreementCell
            cell?.setup(with: item)
            return cell
        }
        return dataSource
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.fetch()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
        interactor.trackEnterView()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
        view.addSubview(separatorView)
        view.addSubview(tableView)
    }
    
    override func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(topLayoutGuide.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        separatorView.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(Layout.Height.separatorView)
        }
        
        tableView.snp.makeConstraints {
            $0.top.equalTo(separatorView.snp.bottom)
            $0.bottom.leading.trailing.equalToSuperview()
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        tableView.dataSource = dataSource
    }
    
    private func setupNavigationBar() {
        navigationController?.setupLoanAppearance()
        navigationItem.rightBarButtonItem = FAQBarButtonItem
    }
}

extension MyLoanAgreementsViewController: MyLoanAgreementsDisplay {
    func displayItems(data: [LoanAgreementViewModel]) {
        dataSource.update(items: data, from: 0)
    }
    
    func displayEmptyState() {
        descriptionLabel.text = Strings.MyLoanAgreements.emptyText
        separatorView.isHidden = true
    }
    
    func displayError() {
        let primaryButtonAction = ApolloAlertAction(title: Strings.Generic.tryAgain) {
            self.interactor.fetch()
        }
        
        let linkButtonAction = ApolloAlertAction(title: Strings.Generic.notNow) {
            self.interactor.back()
        }
        
        showApolloAlert(type: .sticker(style: .error),
                        title: Strings.Generic.title,
                        subtitle: Strings.Generic.message,
                        primaryButtonAction: primaryButtonAction,
                        linkButtonAction: linkButtonAction)
    }
    
    func startLoading() {
        startLoadingView()
    }
    
    func stopLoading() {
        stopLoadingView()
    }
}

extension MyLoanAgreementsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        interactor.didTapRow(index: indexPath)
    }
}

@objc
private extension MyLoanAgreementsViewController {
    func FAQButtonTapped() {
        interactor.didTapFAQ()
    }
}
