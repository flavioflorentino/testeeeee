import Foundation

struct LoanAgreementViewModel {
    let title: String
    let category: LoanReason
    let installmentsInfo: String
    let totalInstallmentsPaid: Int
    let totalInstallments: Int
    let installmentPercentage: Float
    
    init(with loan: LoanAgreement) {
        title = loan.loanGoal.category.description
        category = loan.loanGoal.category
        installmentsInfo = Strings.MyLoanAgreements.installmentInfo(loan.totalInstallmentsAlreadyPaid, loan.totalInstallments)
        totalInstallmentsPaid = loan.totalInstallmentsAlreadyPaid
        totalInstallments = loan.totalInstallments
        installmentPercentage = Float(totalInstallmentsPaid) / Float(totalInstallments)
    }
}
