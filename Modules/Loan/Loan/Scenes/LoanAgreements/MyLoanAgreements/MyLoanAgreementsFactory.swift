enum MyLoanAgreementsFactory {
    static func make(delegate: LoanAgreementsCoordinating, from origin: OriginType) -> MyLoanAgreementsViewController {
        let dependencies = LoanDependencyContainer()
        let service: MyLoanAgreementsServicing = MyLoanAgreementsService(dependencies: dependencies)
        let coordinator: MyLoanAgreementsCoordinating = MyLoanAgreementsCoordinator(delegate: delegate, dependencies: dependencies)
        let presenter: MyLoanAgreementsPresenting = MyLoanAgreementsPresenter(coordinator: coordinator)
        let interactor = MyLoanAgreementsInteractor(service: service, presenter: presenter, from: origin, dependencies: dependencies)
        let viewController = MyLoanAgreementsViewController(interactor: interactor)

        presenter.viewController = viewController

        return viewController
    }
}
