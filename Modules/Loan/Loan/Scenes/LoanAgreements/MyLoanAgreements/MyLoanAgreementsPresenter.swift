import Foundation

protocol MyLoanAgreementsPresenting: AnyObject {
    var viewController: MyLoanAgreementsDisplay? { get set }
    func present(loans: [LoanAgreement])
    func didNextStep(action: MyLoanAgreementsAction)
    func startLoading()
    func stopLoading()
    func presentError()
    func back()
    func openFAQ(_ article: FAQArticle)
}

final class MyLoanAgreementsPresenter {
    private let coordinator: MyLoanAgreementsCoordinating
    weak var viewController: MyLoanAgreementsDisplay?

    init(coordinator: MyLoanAgreementsCoordinating) {
        self.coordinator = coordinator
    }
}

extension MyLoanAgreementsPresenter: MyLoanAgreementsPresenting {
    func present(loans: [LoanAgreement]) {
        guard !loans.isEmpty else {
            viewController?.displayEmptyState()
            return
        }
        viewController?.displayItems(data: loans.map { LoanAgreementViewModel(with: $0) })
    }
    
    func didNextStep(action: MyLoanAgreementsAction) {
        coordinator.perform(action: action)
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
    
    func presentError() {
        viewController?.displayError()
    }
    
    func back() {
        coordinator.perform(action: .back)
    }
    
    func openFAQ(_ article: FAQArticle) {
        coordinator.perform(action: .openFAQ(article))
    }
}
