import Core
import Foundation

protocol MyLoanAgreementsServicing {
    func getLoanAgreements(completion: @escaping (Result<LoanAgreementsResponse, ApiError>) -> Void)
}

final class MyLoanAgreementsService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension MyLoanAgreementsService: MyLoanAgreementsServicing {
    func getLoanAgreements(completion: @escaping (Result<LoanAgreementsResponse, ApiError>) -> Void) {
        let api = Api<LoanAgreementsResponse>(endpoint: LoanAgreementsServiceEndpoint.loansList)

        api.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
