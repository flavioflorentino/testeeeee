import UIKit

enum MyLoanAgreementsAction: Equatable {
    case installments(contractId: String)
    case back
    case openFAQ(_ article: FAQArticle)
}

protocol MyLoanAgreementsCoordinating: AnyObject {
    func perform(action: MyLoanAgreementsAction)
}

final class MyLoanAgreementsCoordinator {
    private weak var delegate: LoanAgreementsCoordinating?
    typealias Dependencies = HasLegacy
    private let dependencies: Dependencies
    
    init(delegate: LoanAgreementsCoordinating, dependencies: Dependencies) {
        self.delegate = delegate
        self.dependencies = dependencies
    }
}

extension MyLoanAgreementsCoordinator: MyLoanAgreementsCoordinating {
    func perform(action: MyLoanAgreementsAction) {
        switch action {
        case let .installments(contractId):
            delegate?.perform(action: .installments(contractId: contractId, origin: .myLoanAgreements))
        case .back:
            delegate?.perform(action: .pop)
        case let .openFAQ(article):
            dependencies.legacy.customerSupport?.push(article: article)
        }
    }
}
