import Core
import Foundation

protocol HireAdvancePaymentServicing {
}

final class HireAdvancePaymentService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - HireAdvancePaymentServicing
extension HireAdvancePaymentService: HireAdvancePaymentServicing {
}
