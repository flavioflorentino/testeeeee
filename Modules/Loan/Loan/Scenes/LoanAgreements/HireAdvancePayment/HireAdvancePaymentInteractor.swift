import Foundation
import AnalyticsModule

protocol HireAdvancePaymentInteracting: AnyObject {
    func fetchPayment()
    func nextStep()
}

final class HireAdvancePaymentInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    private let presenter: HireAdvancePaymentPresenting
    private let contractId: String
    private let installments: [Installment]
    private let valueToPay: Double
    private let totalDiscount: Double

    init(presenter: HireAdvancePaymentPresenting,
         dependencies: Dependencies,
         contractId: String,
         installments: [Installment]) {
        self.presenter = presenter
        self.dependencies = dependencies
        self.installments = installments
        self.contractId = contractId
        self.valueToPay = installments.reduce(0.00) { $0 + $1.value }
        self.totalDiscount = installments.reduce(0.00) { $0 + ($1.discountValue ?? 0.00) }
    }
}

// MARK: - HireAdvancePaymentInteracting
extension HireAdvancePaymentInteractor: HireAdvancePaymentInteracting {
    func fetchPayment() {
        if totalDiscount > 0.00 {
            presenter.presentDiscountLabel(totalDiscount: totalDiscount)
        }
        
        presenter.presentPayment(installments: installments, valueToPay: valueToPay)
    }

    func nextStep() {
        presenter.openPaymentMethods(contractId: contractId, installments: installments)
    }
}
