import UIKit

enum HireAdvancePaymentFactory {
    static func make(contractId: String,
                     installments: [Installment],
                     delegate: LoanAgreementsCoordinating) -> HireAdvancePaymentViewController {
        let container = LoanDependencyContainer()
        let coordinator: HireAdvancePaymentCoordinating = HireAdvancePaymentCoordinator(delegate: delegate)
        let presenter: HireAdvancePaymentPresenting = HireAdvancePaymentPresenter(coordinator: coordinator)
        let interactor = HireAdvancePaymentInteractor(presenter: presenter,
                                                      dependencies: container,
                                                      contractId: contractId,
                                                      installments: installments)
        let viewController = HireAdvancePaymentViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
