import UI
import UIKit
import AssetsKit

extension HireAdvancePaymentCell.Layout {}

final class HireAdvancePaymentCell: UITableViewCell {
    fileprivate enum Layout {}

    private lazy var orderLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale700.color)
        return label
    }()

    private lazy var dueDateLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale700.color)
        return label
    }()

    private lazy var originalValueLabel = UILabel()

    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale700.color)
            .with(\.textAlignment, .right)
        return label
    }()

    private lazy var bottomSeparatorView = SeparatorView()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - SetupCell
extension HireAdvancePaymentCell {
    func setup(with installment: HireAdvancePaymentViewModel) {
        orderLabel.text = installment.order
        dueDateLabel.text = installment.dueDate
        valueLabel.text = installment.value
        originalValueLabel.attributedText = installment.originalValueDescription
    }
}

// MARK: - ViewConfiguration
extension HireAdvancePaymentCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(orderLabel)
        contentView.addSubview(dueDateLabel)
        contentView.addSubview(originalValueLabel)
        contentView.addSubview(valueLabel)
        contentView.addSubview(bottomSeparatorView)
    }

    func setupConstraints() {
        orderLabel.snp.makeConstraints {
            $0.top.bottom.leading.equalToSuperview().inset(Spacing.base02)
        }

        dueDateLabel.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base02)
            $0.leading.equalTo(orderLabel.snp.trailing).offset(Spacing.base00)
        }

        originalValueLabel.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base02)
            $0.trailing.equalTo(valueLabel.snp.leading).offset(-Spacing.base01)
        }

        valueLabel.snp.makeConstraints {
            $0.top.bottom.trailing.equalToSuperview().inset(Spacing.base02)
        }

        bottomSeparatorView.snp.makeConstraints {
            $0.bottom.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }

    func configureViews() {
        selectionStyle = .none
    }
}
