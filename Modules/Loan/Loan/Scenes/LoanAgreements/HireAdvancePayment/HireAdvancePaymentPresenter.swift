import Foundation
import UI

protocol HireAdvancePaymentPresenting: AnyObject {
    var viewController: HireAdvancePaymentDisplaying? { get set }
    func presentPayment(installments: [Installment], valueToPay: Double)
    func openPaymentMethods(contractId: String, installments: [Installment])
    func presentDiscountLabel(totalDiscount: Double)
}

final class HireAdvancePaymentPresenter {
    private let coordinator: HireAdvancePaymentCoordinating
    weak var viewController: HireAdvancePaymentDisplaying?

    init(coordinator: HireAdvancePaymentCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - HireAdvancePaymentPresenting
extension HireAdvancePaymentPresenter: HireAdvancePaymentPresenting {
    func presentPayment(installments: [Installment], valueToPay: Double) {
        viewController?.displayValue(valueToPay)
        viewController?.displayHeaderTitle(installmentsCount: installments.count)
        viewController?.updateInstallments(installments: installments.map { HireAdvancePaymentViewModel(installment: $0) })
    }
    
    func presentDiscountLabel(totalDiscount: Double) {
        viewController?.displayDiscountLabel(formatDiscountValueDescription(value: totalDiscount))
    }
    
    func openPaymentMethods(contractId: String, installments: [Installment]) {
        coordinator.perform(action: .payment(contractId: contractId, installments: installments))
    }
}

extension HireAdvancePaymentPresenter {
    private func formatDiscountValueDescription(value: Double) -> NSAttributedString {
        let currencyString = value.toCurrencyString() ?? ""
        let discountValueRawValue = Strings.HireAdvancePayment.Total.discount(currencyString)

        return discountValueRawValue.attributedStringWith(
            normalFont: Typography.bodyPrimary().font(),
            highlightFont: Typography.bodyPrimary(.highlight).font(),
            normalColor: Colors.grayscale500.color,
            highlightColor: Colors.grayscale500.color,
            underline: false
        )
    }
}
