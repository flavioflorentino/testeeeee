import UIKit

enum HireAdvancePaymentAction: Equatable {
    case payment(contractId: String, installments: [Installment])
}

protocol HireAdvancePaymentCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: HireAdvancePaymentAction)
}

final class HireAdvancePaymentCoordinator {
    weak var viewController: UIViewController?
    private weak var delegate: LoanAgreementsCoordinating?

    init(delegate: LoanAgreementsCoordinating) {
        self.delegate = delegate
    }
}

// MARK: - HireAdvancePaymentCoordinating
extension HireAdvancePaymentCoordinator: HireAdvancePaymentCoordinating {
    func perform(action: HireAdvancePaymentAction) {
        if case let .payment(contractId, installments) = action {
            delegate?.perform(action: .payment(contractId: contractId, installments: installments))
        }
    }
}
