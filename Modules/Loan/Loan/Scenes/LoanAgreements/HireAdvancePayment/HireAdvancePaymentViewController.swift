import UI
import UIKit
import AssetsKit

protocol HireAdvancePaymentDisplaying: AnyObject {
    func displayValue(_ value: Double)
    func displayDiscountLabel(_ value: NSAttributedString)
    func displayHeaderTitle(installmentsCount: Int)
    func updateInstallments(installments: [HireAdvancePaymentViewModel])
}

private extension HireAdvancePaymentViewController.Layout {
    enum Height {
        static let headerRow: CGFloat = 44
        static let tableViewEstimatedRowHeight: CGFloat = 96
        static let separatorView = 1
    }

    enum Size {
        static let imageSize = CGSize(width: 128, height: 128)
    }
}

final class HireAdvancePaymentViewController: ViewController<HireAdvancePaymentInteracting, UIView> {
    fileprivate enum Layout { }

    enum AccessibilityIdentifier: String {
        case installmentsTableView
    }

    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsVerticalScrollIndicator = false
        return scrollView
    }()

    private lazy var contentView = UIView()

    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = Resources.Illustrations.iluGirlCoinCircledBackground.image
        return imageView
    }()

    private lazy var headerTitleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, Colors.black.color)
        label.textAlignment = .center
        return label
    }()

    private lazy var headerStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = Spacing.base01
        stack.alignment = .center
        return stack
    }()
    
    private lazy var currencyView = UIView()

    private lazy var valueView: CurrencyField = {
        let value = CurrencyField()
        value.fontCurrency = Typography.title(.small).font()
        value.fontValue = Typography.currency(.large).font()
        value.textColor = Colors.neutral600.color
        value.spacing = Spacing.base00
        value.positionCurrency = .top
        value.isEnabled = false
        return value
    }()

    private lazy var discountLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.grayscale500.color)
        label.isHidden = true
        return label
    }()

    private lazy var installmentsLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.HireAdvancePayment.TableView.title
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.grayscale700.color)
        return label
    }()

    private lazy var installmentsTableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.separatorInset = .zero
        tableView.allowsSelection = false
        tableView.estimatedRowHeight = Layout.Height.tableViewEstimatedRowHeight
        tableView.accessibilityIdentifier = AccessibilityIdentifier.installmentsTableView.rawValue
        tableView.tableFooterView = UIView()
        tableView.register(HireAdvancePaymentCell.self, forCellReuseIdentifier: HireAdvancePaymentCell.identifier)
        return tableView
    }()

    private lazy var nextButton: UIButton = {
       let button = UIButton()
        button.setTitle(Strings.HireAdvancePayment.Button.title, for: .normal)
        button.addTarget(self, action: #selector(nextStep), for: .touchUpInside)
        button.buttonStyle(PrimaryButtonStyle())
        return button
    }()

    private lazy var installmentsDataSource: TableViewDataSource<Int, HireAdvancePaymentViewModel> = {
        let dataSource = TableViewDataSource<Int, HireAdvancePaymentViewModel>(view: installmentsTableView)
        dataSource.add(section: 0)
        dataSource.itemProvider = { tableView, indexPath, item -> UITableViewCell? in
            let cell = tableView.dequeueReusableCell(withIdentifier: HireAdvancePaymentCell.identifier, for: indexPath) as? HireAdvancePaymentCell
            cell?.setup(with: item)
            return cell
        }
        return dataSource
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.fetchPayment()
        installmentsTableView.dataSource = installmentsDataSource
    }

    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(imageView)
        contentView.addSubview(headerTitleLabel)
        contentView.addSubview(headerStackView)
        currencyView.addSubview(valueView)
        headerStackView.addArrangedSubview(currencyView)
        headerStackView.addArrangedSubview(discountLabel)
        contentView.addSubview(installmentsLabel)
        contentView.addSubview(installmentsTableView)
        contentView.addSubview(nextButton)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        contentView.snp.makeConstraints {
            $0.edges.width.equalToSuperview()
            $0.height.equalTo(view.compatibleSafeArea.height).priority(.low)
        }

        imageView.snp.makeConstraints {
            $0.top.equalTo(contentView.snp.top).offset(Spacing.base03)
            $0.size.equalTo(Layout.Size.imageSize)
            $0.centerX.equalToSuperview()
        }

        headerTitleLabel.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.centerX.equalToSuperview()
        }

        headerStackView.snp.makeConstraints {
            $0.top.equalTo(headerTitleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        valueView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        installmentsLabel.snp.makeConstraints {
            $0.top.equalTo(headerStackView.snp.bottom).offset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        installmentsTableView.snp.makeConstraints {
            $0.top.equalTo(installmentsLabel.snp.bottom)
            $0.leading.trailing.equalToSuperview()
        }

        nextButton.snp.makeConstraints {
            $0.top.equalTo(installmentsTableView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).offset(-Spacing.base02)
        }
    }

    override func configureViews() {
        navigationController?.setupLoanAppearance()
        navigationItem.title = Strings.HireAdvancePayment.Navigation.title
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}

// MARK: - HireAdvancePaymentDisplaying
extension HireAdvancePaymentViewController: HireAdvancePaymentDisplaying {
    func displayValue(_ value: Double) {
        valueView.value = value
    }

    func displayHeaderTitle(installmentsCount: Int) {
        headerTitleLabel.text = installmentsCount > 1
            ? Strings.HireAdvancePayment.ManyInstallments.title(installmentsCount)
            : Strings.HireAdvancePayment.SingleInstallment.title
    }

    func displayDiscountLabel(_ value: NSAttributedString) {
        discountLabel.isHidden = false
        discountLabel.attributedText = value
    }

    func updateInstallments(installments: [HireAdvancePaymentViewModel]) {
        installmentsDataSource.add(items: installments, to: 0)
    }
}

@objc
private extension HireAdvancePaymentViewController {
    func nextStep() {
        interactor.nextStep()
    }
}
