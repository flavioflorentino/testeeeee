import UI
import UIKit

final class HireAdvancePaymentViewModel {
    let order: String
    let dueDate: String
    let value: String?

    private let originalValue: String?
    private let hasDiscount: Bool

    private let dateFormatter: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "MMM yyyy"
        df.locale = Locale(identifier: "pt_BR_POSIX")
        return df
    }()

    var originalValueDescription: NSAttributedString? {
        guard hasDiscount, let originalValue = originalValue else { return nil }

        let originalValueAttributedString = NSAttributedString(
            string: originalValue,
            attributes: [
                .font: Typography.bodyPrimary().font(),
                .foregroundColor: Colors.grayscale500.color,
                .strikethroughStyle: NSUnderlineStyle.single.rawValue,
                .strikethroughColor: Colors.grayscale500.color
            ]
        )
        return originalValueAttributedString
    }

    init(installment: Installment) {
        order = "\(installment.order)ª"
        dueDate = dateFormatter
            .string(from: installment.dueDate)
            .uppercased()
            .replacingOccurrences(of: ".", with: "")
        value = installment.value.toCurrencyString()
        originalValue = installment.originalValue.toCurrencyString()
        hasDiscount = installment.originalValue > installment.value
    }
}
