import Core
import Foundation

enum LoanAgreementsServiceEndpoint {
    case loansList
    case installmentsList(contractId: String)
    case verifyWorkday
}

extension LoanAgreementsServiceEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .loansList:
            return "personal-credit/customer-service/contracts"
        case let .installmentsList(contractId):
            return "personal-credit/customer-service/contracts/\(contractId)/installments/"
        case .verifyWorkday:
            return "personal-credit-aftersales/customer-service/v1/verify-workday"
        }
    }
}
