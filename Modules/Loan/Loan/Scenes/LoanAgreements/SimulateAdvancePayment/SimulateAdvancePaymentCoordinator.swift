import UIKit

enum SimulateAdvancePaymentAction: Equatable {
    case hireAdvancePayment(contractId: String, installments: [Installment])
    case pop
    case openFAQ(_ article: FAQArticle)
}

protocol SimulateAdvancePaymentCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: SimulateAdvancePaymentAction)
}

final class SimulateAdvancePaymentCoordinator {
    weak var viewController: UIViewController?
    typealias Dependencies = HasLegacy
    private let dependencies: Dependencies

    private weak var delegate: LoanAgreementsCoordinating?
    init(delegate: LoanAgreementsCoordinating, dependencies: Dependencies) {
        self.delegate = delegate
        self.dependencies = dependencies
    }
}

// MARK: - SimulateAdvancePaymentCoordinating
extension SimulateAdvancePaymentCoordinator: SimulateAdvancePaymentCoordinating {
    func perform(action: SimulateAdvancePaymentAction) {
        switch action {
        case let .hireAdvancePayment(contractId, installments):
            delegate?.perform(action: .hireAdvancePayment(contractId: contractId, installments: installments))
        case .pop:
            delegate?.perform(action: .pop)
        case let .openFAQ(article):
            dependencies.legacy.customerSupport?.push(article: article)
        }
    }
}
