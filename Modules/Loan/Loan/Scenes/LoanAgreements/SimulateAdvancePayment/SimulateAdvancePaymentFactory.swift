import UIKit

enum SimulateAdvancePaymentFactory {
    static func make(with contractId: String, installments: [Installment], bankSelectionOrder: InstallmentSelectionOrder, delegate: LoanAgreementsCoordinating) -> SimulateAdvancePaymentViewController {
        let container = LoanDependencyContainer()
        let service: SimulateAdvancePaymentServicing = SimulateAdvancePaymentService(dependencies: container)
        let coordinator: SimulateAdvancePaymentCoordinating = SimulateAdvancePaymentCoordinator(delegate: delegate, dependencies: container)
        let presenter: SimulateAdvancePaymentPresenting = SimulateAdvancePaymentPresenter(coordinator: coordinator)
        let interactor = SimulateAdvancePaymentInteractor(contractId: contractId, installments: installments, bankSelectionOrder: bankSelectionOrder, presenter: presenter, dependencies: container, service: service)
        let viewController = SimulateAdvancePaymentViewController(interactor: interactor)
        
        coordinator.viewController = viewController
        presenter.viewController = viewController
        
        return viewController      
    }
}
