import UI
import UIKit

final class SimulateAdvancePaymentViewModel {
    let order: String
    let discountValue: Double
    let originalValue: Double
    let dueDate: String
    let status: InstallmentStatus
    let valueToPay: Double
    
    let isMandatory: Bool
    let isNotSelectable: Bool
    var isSelected: Bool
    var isSelectionAvailable: Bool

    var shouldAnimateWhenExpanding = false
    var shouldAnimateWhenCompressing = false

    private let dateFormatter: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "dd MMM yyyy"
        df.locale = Locale(identifier: "pt_BR_POSIX")
        return df
    }()
    
    init(installment: Installment, isSelectionAvailable: Bool) {
        order = "\(installment.order)ª"
        dueDate = dateFormatter
            .string(from: installment.dueDate)
            .uppercased()
            .replacingOccurrences(of: ".", with: "")
        status = installment.status
        originalValue = installment.originalValue
        discountValue = installment.discountValue ?? 0.00
        valueToPay = installment.value
        isMandatory =
            installment.status == .overdue ||
            installment.status == .closeToExpire
        isSelected = isMandatory
        isNotSelectable = installment.status == .renegotiated
        self.isSelectionAvailable = isSelectionAvailable
    }
}
