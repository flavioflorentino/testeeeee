import Foundation
import UI
import UIKit
import AssetsKit

private typealias Localizable = Strings.SimulateAdvancePayment.Total

protocol SimulateAdvancePaymentPresenting: AnyObject {
    var viewController: SimulateAdvancePaymentDisplaying? { get set }
    func presentData(items: [SimulateAdvancePaymentViewModel])
    func setupFooterDescription(valueToPay value: Double,
                                previousValue: Double,
                                totalInstallments: Int,
                                previousDiscount: Double,
                                totalDiscount: Double)
    func presentNextButtonState(isNextButtonEnabled: Bool)
    func openHireAdvancePayment(contractId: String, installments: [Installment])
    func presentError()
    func presentTimeExceededWarning()
    func presentConnectionError()
    func pop()
    func openFAQ(article: FAQArticle)
}

final class SimulateAdvancePaymentPresenter {
    private let coordinator: SimulateAdvancePaymentCoordinating
    weak var viewController: SimulateAdvancePaymentDisplaying?

    init(coordinator: SimulateAdvancePaymentCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - SimulateAdvancePaymentPresenting
extension SimulateAdvancePaymentPresenter: SimulateAdvancePaymentPresenting {
    func presentData(items: [SimulateAdvancePaymentViewModel]) {
        viewController?.displayItems(data: items)
    }

    func setupFooterDescription(valueToPay value: Double,
                                previousValue: Double,
                                totalInstallments: Int,
                                previousDiscount: Double,
                                totalDiscount: Double) {
        let installments = totalInstallments <= 1 ?
            Localizable.installment("\(totalInstallments)") :
            Localizable.installments("\(totalInstallments)")

        viewController?.displayFooterDescription(installments: installments,
                                                 previousValue: previousValue,
                                                 totalValue: value,
                                                 previousDiscount: previousDiscount,
                                                 totalDiscount: totalDiscount)
    }
    
    func presentNextButtonState(isNextButtonEnabled: Bool) {
        viewController?.displayNextButtonState(isNextButtonEnabled: isNextButtonEnabled)
    }
    
    func openHireAdvancePayment(contractId: String, installments: [Installment]) {
        coordinator.perform(action: .hireAdvancePayment(contractId: contractId, installments: installments))
    }
  
    func presentError() {
        viewController?.showErrorDialog(imageAsset: Resources.Illustrations.iluError.image,
                                        title: Strings.Generic.title,
                                        subtitle: Strings.Generic.message)
    }
    
    func presentConnectionError() {
        viewController?.showErrorDialog(imageAsset: Resources.Illustrations.iluNoConnectionGreenBackground.image,
                                        title: Strings.Generic.ConectionError.title,
                                        subtitle: Strings.Generic.ConectionError.message)
    }
    
    func presentTimeExceededWarning() {
        viewController?.displayTimeExceededWarning()
    }
    
    func pop() {
        coordinator.perform(action: .pop)
    }
    
    func openFAQ(article: FAQArticle) {
        coordinator.perform(action: .openFAQ(article))
    }
}
