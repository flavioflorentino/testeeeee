import Foundation
import UI
import UIKit

private typealias Localizable = Strings.SimulateAdvancePayment.Total

private extension SimulateAdvancePaymentFooterView.Layout {
    enum Size {
        static let button = CGSize(width: 128, height: 40)
    }
}

final class SimulateAdvancePaymentFooterView: UIView {
    fileprivate enum Layout {}
    
    private lazy var separatorView = SeparatorView()
    
    lazy var titleLabel: NumberCounterLabel = {
        let label = NumberCounterLabel()
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, Colors.grayscale700.color)
        label.customFormatBlock = { value in
            NSAttributedString(string: value.toCurrencyString() ?? "")
        }
        return label
    }()
    
    lazy var subtitleLabel: NumberCounterLabel = {
        let label = NumberCounterLabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.grayscale400.color)
        label.customFormatBlock = { value in
            NSAttributedString(string: Localizable.discount(value.toCurrencyString() ?? ""))
        }
        return label
    }()
    
    lazy var installmentLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(CaptionLabelStyle())
            .with(\.textColor, Colors.grayscale700.color)
        return label
    }()
    
    lazy var continueButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Strings.Installments.Footer.button, for: .normal)
        return button
    }()
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension SimulateAdvancePaymentFooterView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(separatorView)
        addSubview(titleLabel)
        addSubview(subtitleLabel)
        addSubview(installmentLabel)
        addSubview(continueButton)
    }
    
    func setupConstraints() {
        separatorView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.leading.equalToSuperview().inset(Spacing.base02)
        }
        
        subtitleLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom)
            $0.leading.bottom.equalToSuperview().inset(Spacing.base02)
        }
        
        installmentLabel.snp.makeConstraints {
            $0.centerY.equalTo(titleLabel)
            $0.leading.equalTo(titleLabel.snp.trailing).offset(Spacing.base00)
        }
        
        continueButton.snp.makeConstraints {
            $0.top.bottom.trailing.equalToSuperview().inset(Spacing.base02)
            $0.size.equalTo(Layout.Size.button)
        }
    }
    
    func configureViews() {
        viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }
}
