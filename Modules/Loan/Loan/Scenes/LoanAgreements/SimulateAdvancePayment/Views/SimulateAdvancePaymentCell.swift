import UI
import UIKit
import AssetsKit

protocol SimulateAdvancePaymentCellDelegate: AnyObject {
    func didTapImage(from cell: UITableViewCell, status: InstallmentStatus)
}

extension SimulateAdvancePaymentCell.Layout {
    enum Size {
        static let checkIcon = CGSize(width: 20, height: 20)
        static let infoIcon = CGSize(width: 20, height: 20)
    }
    
    enum Duration {
        static let animationTime = 0.25
    }
}

final class SimulateAdvancePaymentCell: UITableViewCell {
    fileprivate enum Layout {}
    
    weak var delegate: SimulateAdvancePaymentCellDelegate?
    private var intallmentStatus: InstallmentStatus?
    
    private lazy var checkImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private lazy var orderLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
        return label
    }()
    
    private lazy var dueDateTitleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
        label.text = Strings.Installments.dueDate
        return label
    }()
    
    private lazy var dueDateLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.grayscale700.color)
        return label
    }()
    
    private lazy var dueDateStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [dueDateTitleLabel, dueDateLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base00
        stackView.alignment = .leading
        return stackView
    }()
    
    private lazy var infoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        imageView.isHidden = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapInfoImageView)))
        return imageView
    }()
        
    private lazy var statusLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
            .with(\.textColor, Colors.grayscale400.color)
        return label
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
        return label
    }()
    
    private lazy var valueStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [statusLabel, valueLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base00
        stackView.alignment = .trailing
        return stackView
    }()
    
    private lazy var wrapperInfoStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [valueStackView, infoImageView])
        stackView.axis = .horizontal
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        checkImageView.isHidden = true
        checkImageView.image = nil
        infoImageView.isHidden = true
        statusLabel.isHidden = true
    }
}

// MARK: - SetupCell
extension SimulateAdvancePaymentCell {
    func setup(with installment: SimulateAdvancePaymentViewModel) {
        orderLabel.text = installment.order
        dueDateLabel.text = installment.dueDate
        valueLabel.text = installment.valueToPay.toCurrencyString()
        intallmentStatus = installment.status
        configCell(with: installment)
        updateOrderLabelContraint()
    }
}

private extension SimulateAdvancePaymentCell {
    func configCell(with installment: SimulateAdvancePaymentViewModel) {
        if installment.isMandatory {
            setupMandatoryInstallment(with: installment)
            configureDisabledCell()
        } else if installment.isNotSelectable {
            setupNotSelectableInstallment(with: installment)
            configureDisabledCell()
        } else {
            setupSelectableInstallment(with: installment)
            installment.isSelectionAvailable ? configureEnabledCell() : configureDisabledCell()
        }
    }
    
    func setupNotSelectableInstallment(with installment: SimulateAdvancePaymentViewModel) {
        checkImageView.isHidden = true
        infoImageView.isHidden = false
        statusLabel.isHidden = false
        statusLabel.text = installment.status.description
        infoImageView.image = Resources.Icons.icoBlueInfo.image
    }
    
    func setupMandatoryInstallment(with installment: SimulateAdvancePaymentViewModel) {
        statusLabel.isHidden = false
        infoImageView.isHidden = false
        checkImageView.isHidden = false
        statusLabel.text = installment.status.description
        infoImageView.image = Resources.Icons.icoBlueInfo.image
        checkImageView.image = Resources.Icons.icoCheckBoxDisabled.image
    }
    
    func setupSelectableInstallment(with installment: SimulateAdvancePaymentViewModel) {
        if installment.isSelectionAvailable {
            checkImageView.isHidden = false
            checkImageView.image = installment.isSelected ?
                Resources.Icons.icoCheckBoxEnabled.image :
                Resources.Icons.icoCheckBoxEmpty.image
            expansionAnimation(with: installment)
        } else {
            checkImageView.isHidden = true
            compressionAnimation(with: installment)
        }
    }
    
    func expansionAnimation(with installment: SimulateAdvancePaymentViewModel) {
        if installment.shouldAnimateWhenExpanding {
            imageExpansionAnimation()
            constraintAnimation()
            installment.shouldAnimateWhenExpanding = false
        }
    }
    
    func compressionAnimation(with installment: SimulateAdvancePaymentViewModel) {
        if installment.shouldAnimateWhenCompressing {
            constraintAnimation()
            installment.shouldAnimateWhenCompressing = false
        }
    }
    
    func configureDisabledCell() {
        backgroundColor = Colors.grayscale050.color
        orderLabel.textColor = Colors.grayscale300.color
        dueDateTitleLabel.textColor = Colors.grayscale400.color
        dueDateLabel.textColor = Colors.grayscale400.color
        valueLabel.textColor = Colors.grayscale400.color
    }
    
    func configureEnabledCell() {
        backgroundColor = Colors.backgroundPrimary.color
        orderLabel.textColor = Colors.grayscale700.color
        dueDateTitleLabel.textColor = Colors.grayscale500.color
        dueDateLabel.textColor = Colors.grayscale700.color
        valueLabel.textColor = Colors.neutral600.color
    }
}
// MARK: - Animation
private extension SimulateAdvancePaymentCell {
    func imageExpansionAnimation() {
        checkImageView.center.x = 0
        checkImageView.transform = .init(scaleX: 0, y: 0)
        checkImageView.alpha = 0
        
        UIView.animate(withDuration: Layout.Duration.animationTime,
                       delay: 0,
                       options: .curveEaseIn,
                       animations: {
                        self.checkImageView.center.x = self.checkImageView.frame.midX
                        self.checkImageView.transform = .init(scaleX: 1, y: 1)
                        self.checkImageView.alpha = 1
                       })
    }
    
    func constraintAnimation() {
        UIView.animate(withDuration: Layout.Duration.animationTime,
                       delay: 0,
                       options: .curveLinear,
                       animations: {
                        self.updateOrderLabelContraint()
                        self.layoutIfNeeded()
                       })
    }
}

// MARK: - ViewConfiguration
extension SimulateAdvancePaymentCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(checkImageView)
        contentView.addSubview(orderLabel)
        contentView.addSubview(dueDateStackView)
        contentView.addSubview(wrapperInfoStackView)
    }
    
    func setupConstraints() {
        checkImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.checkIcon)
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
        }
        
        orderLabel.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalTo(checkImageView.snp.trailing).offset(Spacing.base02)
        }
        
        dueDateStackView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base02)
            $0.centerY.equalToSuperview()
            $0.leading.equalTo(orderLabel.snp.trailing).offset(Spacing.base02)
        }
        
        wrapperInfoStackView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base02)
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        infoImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.infoIcon)
        }
    }
    
    func updateOrderLabelContraint() {
        orderLabel.snp.remakeConstraints {
            $0.centerY.equalToSuperview()
            if checkImageView.isHidden {
                $0.leading.equalToSuperview().offset(Spacing.base02)
            } else {
                $0.leading.equalTo(checkImageView.snp.trailing).offset(Spacing.base02)
            }
        }
    }
    
    func configureViews() {
        selectionStyle = .none
    }
}

@objc
extension SimulateAdvancePaymentCell {
    private func didTapInfoImageView() {
        guard let intallmentStatus = intallmentStatus else { return }
        delegate?.didTapImage(from: self, status: intallmentStatus)
    }
}
