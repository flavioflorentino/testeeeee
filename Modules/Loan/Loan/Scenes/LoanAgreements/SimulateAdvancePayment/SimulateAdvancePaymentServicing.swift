import Foundation
import Core

protocol SimulateAdvancePaymentServicing {
    func verifyWorkday(completion: @escaping (Result<NoContent, ApiError>) -> Void)
}

struct SimulateAdvancePaymentService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - SimulateAdvancePaymentServicing
extension SimulateAdvancePaymentService: SimulateAdvancePaymentServicing {
    func verifyWorkday(completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        let api = Api<NoContent>(endpoint: LoanAgreementsServiceEndpoint.verifyWorkday)
        
        api.execute { result in
            self.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
