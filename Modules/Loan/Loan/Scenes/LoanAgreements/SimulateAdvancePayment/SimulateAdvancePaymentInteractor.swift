import Foundation
import AnalyticsModule
import Core

enum Orientation {
    case downwards
    case upwards
}

protocol SimulateAdvancePaymentInteracting: AnyObject {
    func createData()
    func didTapRow(index: IndexPath)
    var newInstallments: [SimulateAdvancePaymentViewModel] { get set }
    var userSelectionOrder: Orientation? { get set }
    func fetch()
    func pop()
    func didTapFAQ(article: FAQArticle)
}

final class SimulateAdvancePaymentInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    private let service: SimulateAdvancePaymentServicing
    private let presenter: SimulateAdvancePaymentPresenting
    private let installments: [Installment]
    private let contractId: String
    private var previousValue: Double = 0
    private var previousDiscount: Double = 0
    var newInstallments: [SimulateAdvancePaymentViewModel] = []
    private var initialIsSelectableRow: [Bool]
    private var firstIndexOfSelectableRow: Int?
    private var lastIndexOfSelectableRow: Int?
    private var bankSelectionOrder: InstallmentSelectionOrder
    var userSelectionOrder: Orientation?
    
    init(contractId: String,
         installments: [Installment],
         bankSelectionOrder: InstallmentSelectionOrder,
         presenter: SimulateAdvancePaymentPresenting,
         dependencies: Dependencies,
         service: SimulateAdvancePaymentServicing) {
        self.presenter = presenter
        self.installments = installments
        self.bankSelectionOrder = bankSelectionOrder
        self.dependencies = dependencies
        self.contractId = contractId
        self.service = service
        initialIsSelectableRow = Array(repeating: false, count: installments.count)
    }
}

// MARK: - SimulateAdvancePaymentInteracting
extension SimulateAdvancePaymentInteractor: SimulateAdvancePaymentInteracting {
    func createData() {
        getFirstAndLastInitialSelectableRows()
        fillNewArrayOfInstallments()
        setupSelectableRowsAccordingToOrientation()
        displayData()
        displayTotalToBePaid()
    }
    
    func didTapRow(index: IndexPath) {
        guard index.row >= 0 && index.row < newInstallments.count else { return }
        let tappedRow = newInstallments[index.row]
        guard tappedRow.isSelectionAvailable else { return }
        setupSelectableRowsAccordingToOrientation() // Should be called here again due to unit test
        handleSelectionAndDeselection(index: index)
        displayData()
        displayTotalToBePaid()
    }
    
    func fetch() {
        verifyWorkday()
    }
    
    func pop() {
        presenter.pop()
    }
    
    func didTapFAQ(article: FAQArticle) {
        presenter.openFAQ(article: article)
    }
}

// Private Methods
private extension SimulateAdvancePaymentInteractor {
    func verifyWorkday() {
        service.verifyWorkday { [weak self] result in
            switch result {
            case .success:
                self?.openHireAdvancePayment()
            case .failure(let error):
                guard error.requestError?.code == LoanApiError.timeExceeded.rawValue else {
                    self?.handleError(error: error)
                    return
                }
                self?.presenter.presentTimeExceededWarning()
            }
        }
    }
    
    func handleError(error: ApiError) {
        switch error {
        case .connectionFailure:
            presenter.presentConnectionError()
        default:
            presenter.presentError()
        }
    }
    
    func openHireAdvancePayment() {
        var installmentsToHireAdvancePayment: [Installment] = []
        for i in 0..<newInstallments.count where newInstallments[i].isSelected {
            installmentsToHireAdvancePayment.append(installments[i])
        }
        presenter.openHireAdvancePayment(contractId: contractId,
                                         installments: installmentsToHireAdvancePayment)
    }
    
    func handleSelectionAndDeselection(index: IndexPath) {
        let tappedRow = newInstallments[index.row]
        tappedRow.isSelected ?
            setupDeselection(from: index.row) :
            setupSelectionAvailability(from: index.row)
        tappedRow.isSelected.toggle()
    }
    
    func displayData() {
        presenter.presentData(items: newInstallments)
    }
    
    func displayTotalToBePaid() {
        let selectedInstallments = newInstallments.filter { $0.isSelected }
        let totalToPay = selectedInstallments.reduce(0.00) { $0 + $1.valueToPay }
        let totalDiscount = selectedInstallments.reduce(0.00) { $0 + $1.discountValue }
        let totalInstallments = selectedInstallments.count
        let thereIsSelectedInstallments = totalInstallments > 0
        
        presenter.setupFooterDescription(valueToPay: totalToPay,
                                         previousValue: previousValue,
                                         totalInstallments: totalInstallments,
                                         previousDiscount: previousDiscount,
                                         totalDiscount: totalDiscount)

        presenter.presentNextButtonState(isNextButtonEnabled: thereIsSelectedInstallments)
        previousValue = totalToPay
        previousDiscount = totalDiscount
    }
    
    // MARK: - Create initial data
    func getFirstAndLastInitialSelectableRows() {
        let isInstallmentOpenedArray = installments.map { $0.status == .open }
        guard let firstIndex = isInstallmentOpenedArray.firstIndex(of: true),
              let lastIndex = isInstallmentOpenedArray.lastIndex(of: true) else { return }
        
        initialIsSelectableRow[firstIndex] = true
        initialIsSelectableRow[lastIndex] = true
    }
    
    func fillNewArrayOfInstallments() {
        for i in 0..<installments.count {
            newInstallments.append(
                SimulateAdvancePaymentViewModel(installment: installments[i],
                                                isSelectionAvailable: initialIsSelectableRow[i]))
        }
    }
    
    func setupSelectableRowsAccordingToOrientation() {
        if firstIndexOfSelectableRow == nil && lastIndexOfSelectableRow == nil {
            let selectableInstallment = newInstallments.map { !$0.isMandatory && !$0.isNotSelectable }
            guard let firstIndex = selectableInstallment.firstIndex(of: true),
                  let lastIndex = selectableInstallment.lastIndex(of: true) else { return }
            
            firstIndexOfSelectableRow = firstIndex
            lastIndexOfSelectableRow = lastIndex
            
            switch bankSelectionOrder {
            case .topDown:
                userSelectionOrder = .downwards
                newInstallments[firstIndex].isSelectionAvailable = true
                newInstallments[lastIndex].isSelectionAvailable = false
            case .bottomUp:
                userSelectionOrder = .upwards
                newInstallments[firstIndex].isSelectionAvailable = false
                newInstallments[lastIndex].isSelectionAvailable = true
            case .singleDirection:
                newInstallments[firstIndex].isSelectionAvailable = true
                newInstallments[lastIndex].isSelectionAvailable = true
            case .both:
                newInstallments[firstIndex].isSelectionAvailable = true
                newInstallments[lastIndex].isSelectionAvailable = true
            }
        }
    }
    
    // MARK: - Selection Case
    func setupSelectionAvailability(from tappedRowIndex: Int) {
        guard newInstallments.count > 1 else { return }
        
        switch bankSelectionOrder {
        case .topDown:
            selectTopDown(from: tappedRowIndex)
        case .bottomUp:
            selectBottomUp(from: tappedRowIndex)
        case .singleDirection:
            setupUserSelectionOrderIfNeeded(at: tappedRowIndex)
            guard let userSelectionOrder = userSelectionOrder else { return }
            switch userSelectionOrder {
            case .downwards:
                selectTopDown(from: tappedRowIndex)
            case .upwards:
                selectBottomUp(from: tappedRowIndex)
            }
        case .both:
            selectTopDown(from: tappedRowIndex)
            selectBottomUp(from: tappedRowIndex)
        }
    }
    
    func selectTopDown(from tappedRowIndex: Int) {
        switch tappedRowIndex {
        case 0:
            performActionsWhenSelecting(at: 1)
        case newInstallments.count - 1:
            break
        default:
            let previousRow = tappedRowIndex - 1
            let nextRow = tappedRowIndex + 1
            
            if newInstallments[previousRow].isSelected || newInstallments[previousRow].isNotSelectable {
                performActionsWhenSelecting(at: nextRow)
            }
        }
    }
    
    func selectBottomUp(from tappedRowIndex: Int) {
        switch tappedRowIndex {
        case 0:
            break
        case newInstallments.count - 1:
            let lastIndex = newInstallments.count - 1
            performActionsWhenSelecting(at: lastIndex - 1)
        default:
            let previousRow = tappedRowIndex - 1
            let nextRow = tappedRowIndex + 1
            
            if newInstallments[nextRow].isSelected {
                performActionsWhenSelecting(at: previousRow)
            }
        }
    }
    
    func performActionsWhenSelecting(at index: Int) {
        if !newInstallments[index].isMandatory &&
            !newInstallments[index].isNotSelectable &&
            !newInstallments[index].isSelectionAvailable {
            newInstallments[index].isSelectionAvailable = true
            newInstallments[index].shouldAnimateWhenExpanding = true
            newInstallments[index].shouldAnimateWhenCompressing = true
        }
    }
    
    func setupUserSelectionOrderIfNeeded(at tappedRowIndex: Int) {
        guard let lastIndex = lastIndexOfSelectableRow,
              let firstIndex = firstIndexOfSelectableRow else { return }
        
        if  userSelectionOrder == nil && firstIndex != lastIndex {
            if tappedRowIndex == firstIndex &&
                !newInstallments[lastIndex].isSelected {
                userSelectionOrder = .downwards
                newInstallments[lastIndex].isSelectionAvailable = false
                newInstallments[lastIndex].shouldAnimateWhenExpanding = true
                newInstallments[lastIndex].shouldAnimateWhenCompressing = true
            }
            
            if tappedRowIndex == lastIndex &&
                !newInstallments[firstIndex].isSelected {
                userSelectionOrder = .upwards
                newInstallments[firstIndex].isSelectionAvailable = false
                newInstallments[firstIndex].shouldAnimateWhenExpanding = true
                newInstallments[firstIndex].shouldAnimateWhenCompressing = true
            }
        }
    }
    
    // MARK: - Deselection Case
    func setupDeselection(from tappedRowIndex: Int) {
        let totalUnselected =
            newInstallments.filter { !$0.isSelected && !$0.isMandatory && !$0.isNotSelectable }.count
        finalIndexToDeselectRows(totalUnselected: totalUnselected) { [weak self] firstUnselectedIndex, lastUnselectedIndex in
            guard let self = self,
                  let firstUnselectedIndex = firstUnselectedIndex,
                  let lastUnselectedIndex = lastUnselectedIndex else {
                return }
            
            switch self.bankSelectionOrder {
            case .topDown:
                self.disableCloseRows(from: tappedRowIndex + 1, to: firstUnselectedIndex + 1, by: 1)
            case .bottomUp:
                self.disableCloseRows(from: tappedRowIndex - 1, to: lastUnselectedIndex - 1, by: -1)
            case .singleDirection:
                guard let userSelectionOrder = self.userSelectionOrder else { return }
                switch userSelectionOrder {
                case .downwards:
                    self.disableCloseRows(from: tappedRowIndex + 1, to: firstUnselectedIndex + 1, by: 1)
                case .upwards:
                    self.disableCloseRows(from: tappedRowIndex - 1, to: lastUnselectedIndex - 1, by: -1)
                }
                self.enableSelectableRowIfNeeded(tappedRow: tappedRowIndex)
            case .both:
                self.deselectInBothOrientation(from: tappedRowIndex, totalUnselected: totalUnselected)
            }
        }
    }
    
    // Single Direction
    func finalIndexToDeselectRows(totalUnselected: Int, completion: @escaping (Int?, Int?) -> Void) {
        switch totalUnselected {
        case 0:
            guard let firstIndex = firstIndexOfSelectableRow,
                  let lastIndex = lastIndexOfSelectableRow else {
                completion(nil, nil)
                return }
            completion(lastIndex, firstIndex) // the index is actually sent in reverse
        default:
            getFirstAndLastIndexNotSelected { firstIndex, lastIndex in
                guard let firstIndex = firstIndex,
                      let lastIndex = lastIndex else {
                    completion(nil, nil)
                    return }
                completion(firstIndex, lastIndex)
            }
        }
    }
    
    func enableSelectableRowIfNeeded(tappedRow: Int) {
        guard let firstIndex = firstIndexOfSelectableRow,
              let lastIndex = lastIndexOfSelectableRow else {
            return }
        
        guard let userOrder = userSelectionOrder else { return }
        switch userOrder {
        case .downwards:
            if tappedRow == firstIndex {
                enableRow(at: lastIndex)
            }
        case .upwards:
            if tappedRow == lastIndex {
                enableRow(at: firstIndex)
            }
        }
    }
    
    func enableRow(at index: Int) {
        userSelectionOrder = nil
        newInstallments[index].isSelectionAvailable = true
        addAnimationEffectIfNeeded(at: index)
    }
    
    func addAnimationEffectIfNeeded(at index: Int) {
        if !newInstallments[index].shouldAnimateWhenCompressing &&
            !newInstallments[index].shouldAnimateWhenExpanding {
            newInstallments[index].shouldAnimateWhenExpanding = true
            newInstallments[index].shouldAnimateWhenCompressing = true
        }
    }
    
    // Both Direction
    func deselectInBothOrientation(from tappedRowIndex: Int, totalUnselected: Int) {
        guard let firstIndex = firstIndexOfSelectableRow,
              let lastIndex = lastIndexOfSelectableRow else {
            return }
        
        let totalUnselectedUntilTappedRow = getAmountOfUnselected(from: firstIndex, to: tappedRowIndex)
        let totalUnselectedFromTappedRow = getAmountOfUnselected(from: tappedRowIndex, to: lastIndex)
        
        if totalUnselectedUntilTappedRow == 0 && totalUnselectedFromTappedRow != 0 {
            disableCloseRowsAccordingToOrientation(from: tappedRowIndex,
                                                   totalUnselected: totalUnselected,
                                                   orientation: .downwards)
        }
        
        if totalUnselectedUntilTappedRow != 0 && totalUnselectedFromTappedRow == 0 {
            disableCloseRowsAccordingToOrientation(from: tappedRowIndex,
                                                   totalUnselected: totalUnselected,
                                                   orientation: .upwards)
        }
    }
    
    func disableCloseRowsAccordingToOrientation(from tappedRowIndex: Int,
                                                totalUnselected: Int,
                                                orientation: Orientation) {
        getFirstAndLastIndexNotSelected { firstIndex, lastIndex in
            guard let firstIndex = firstIndex,
                  let lastIndex = lastIndex else {
                return }
            var index, increment: Int
            switch orientation {
            case .downwards:
                index = firstIndex
                increment = 1
            case .upwards:
                index = lastIndex
                increment = -1
            }
            let finalIndex = totalUnselected == 1 ? index : index + increment
            disableCloseRows(from: tappedRowIndex + increment, to: finalIndex, by: increment)
        }
    }
    
    // MARK: - Helper deselection methods
    func getFirstAndLastIndexNotSelected(completion: (Int?, Int?) -> Void) {
        let isNotSelectedArray = newInstallments.map {
            !$0.isSelected && !$0.isMandatory && !$0.isNotSelectable
        }
        guard let lastUnselectedIndex = isNotSelectedArray.lastIndex(of: true),
              let firstUnselectedIndex = isNotSelectedArray.firstIndex(of: true) else {
            completion(nil, nil)
            return }
        completion(firstUnselectedIndex, lastUnselectedIndex)
    }
    
    func disableCloseRows(from start: Int, to end: Int, by interval: Int) {
        for i in stride(from: start, to: end, by: interval) {
            newInstallments[i].isSelectionAvailable = false
            newInstallments[i].isSelected = false
        }
    }
    
    func getAmountOfUnselected(from start: Int, to end: Int) -> Int {
        var count: Int = 0
        for i in start...end where
            !newInstallments[i].isMandatory &&
            !newInstallments[i].isNotSelectable &&
            !newInstallments[i].isSelected {
            count += 1
        }
        return count
    }
}
