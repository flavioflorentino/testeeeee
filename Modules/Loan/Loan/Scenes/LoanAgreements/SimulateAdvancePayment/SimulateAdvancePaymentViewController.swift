import UI
import UIKit
import AssetsKit

private typealias Localizable = Strings.SimulateAdvancePayment

protocol SimulateAdvancePaymentDisplaying: AnyObject {
    func displayItems(data: [SimulateAdvancePaymentViewModel])
    func displayFooterDescription(installments: String,
                                  previousValue: Double,
                                  totalValue: Double,
                                  previousDiscount: Double,
                                  totalDiscount: Double)
    func displayNextButtonState(isNextButtonEnabled: Bool)
    func displayTimeExceededWarning()
    func showErrorDialog(imageAsset: UIImage, title: String, subtitle: String)
}

private extension SimulateAdvancePaymentViewController.Layout {
    enum Height {
        static let tableEstimatedRowHeight: CGFloat = 72
        static let separatorView = 1
    }
}

final class SimulateAdvancePaymentViewController: ViewController<SimulateAdvancePaymentInteracting, UIView> {
    fileprivate enum Layout { }
    
    private lazy var FAQBarButtonItem: UIBarButtonItem = {
        let label = UILabel()
        label
            .labelStyle(IconLabelStyle(type: .medium))
            .with(\.textColor, .branding600())
            .with(\.text, Iconography.questionCircle.rawValue)
            .with(\.isUserInteractionEnabled, true)
        let tapGesture =
            UITapGestureRecognizer(target: self, action: #selector(didTapPayInAdvanceFAQ))
        label.addGestureRecognizer(tapGesture)
        let barButton = UIBarButtonItem(customView: label)
        return barButton
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.title
        label
            .labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, Colors.black.color)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.description
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, Colors.grayscale700.color)
        return label
    }()
    
    private lazy var separatorView = SeparatorView()
    
    private lazy var footerView: SimulateAdvancePaymentFooterView = {
        let footerView = SimulateAdvancePaymentFooterView()
        footerView.continueButton.addTarget(self, action: #selector(didTapFooterbutton), for: .touchUpInside)
        return footerView
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .singleLine
        tableView.separatorInset = .zero
        tableView.rowHeight = Layout.Height.tableEstimatedRowHeight
        tableView.tableFooterView = UIView()
        tableView.register(SimulateAdvancePaymentCell.self, forCellReuseIdentifier: SimulateAdvancePaymentCell.identifier)
        return tableView
    }()    
        
    private lazy var dataSource: TableViewDataSource<Int, SimulateAdvancePaymentViewModel> = {
        let dataSource = TableViewDataSource<Int, SimulateAdvancePaymentViewModel>(view: tableView)
        dataSource.add(section: 0)
        dataSource.itemProvider = { tableView, indexPath, item -> UITableViewCell? in
            let cell = tableView.dequeueReusableCell(withIdentifier: SimulateAdvancePaymentCell.identifier, for: indexPath) as? SimulateAdvancePaymentCell
            
            cell?.delegate = self
            cell?.setup(with: item)
            return cell
        }
        return dataSource
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        interactor.createData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
    }

    override func buildViewHierarchy() {
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
        view.addSubview(separatorView)
        view.addSubview(tableView)
        view.addSubview(footerView)
    }
    
    override func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(topLayoutGuide.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        separatorView.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(Layout.Height.separatorView)
        }
        
        tableView.snp.makeConstraints {
            $0.top.equalTo(separatorView.snp.bottom)
            $0.trailing.leading.equalToSuperview()
        }
        
        footerView.snp.makeConstraints {
            $0.top.equalTo(tableView.snp.bottom)
            $0.trailing.leading.equalToSuperview()
            $0.bottom.equalTo(bottomLayoutGuide.snp.top)
        }
    }

    override func configureViews() {
        super.configureViews()
        view.backgroundColor = Colors.backgroundPrimary.color
        tableView.dataSource = dataSource
    }
    
    private func setupNavigationBar() {
        navigationController?.setupLoanAppearance()
        navigationItem.rightBarButtonItem = FAQBarButtonItem
    }
}

@objc
private extension SimulateAdvancePaymentViewController {
    func didTapFooterbutton() {
        interactor.fetch()
    }
    
    func didTapPayInAdvanceFAQ() {
        self.interactor.didTapFAQ(article: .payInAdvance)
    }
}

// MARK: - SimulateAdvancePaymentDisplaying
extension SimulateAdvancePaymentViewController: SimulateAdvancePaymentDisplaying {
    func showErrorDialog(imageAsset: UIImage, title: String, subtitle: String) {
        let alertData = LoanAlertSettings(imageAsset: imageAsset,
                                          title: title,
                                          subtitle: subtitle,
                                          okButtonTitle: Strings.Generic.tryAgain,
                                          okAction: { self.interactor.fetch() },
                                          cancelAction: { self.interactor.pop() })
        LoanAlertHelper.shared.createAlert(with: alertData, presentIn: self)
    }
    
    func displayTimeExceededWarning() {
        let alertData = LoanAlertSettings(imageAsset: Resources.Illustrations.iluWarning.image,
                                          title: Localizable.Warning.TimeExceeded.title,
                                          subtitle: Localizable.Warning.TimeExceeded.description,
                                          okButtonTitle: Strings.Generic.ok,
                                          cancelTitle: Localizable.Warning.TimeExceeded.HelpButton.title,
                                          okAction: {},
                                          hasCancel: true,
                                          cancelAction: {
                                            self.interactor.didTapFAQ(article: .bankSlip)
                                          })
            
        LoanAlertHelper.shared.createAlert(with: alertData, presentIn: self)
    }
    
    func displayItems(data: [SimulateAdvancePaymentViewModel]) {
        dataSource.update(items: data, from: 0)
    }
    
    func displayFooterDescription(installments: String,
                                  previousValue: Double,
                                  totalValue: Double,
                                  previousDiscount: Double,
                                  totalDiscount: Double) {
        footerView.titleLabel.updateValue(from: previousValue, to: totalValue, animationDuration: 0.3)
        footerView.subtitleLabel.updateValue(from: previousDiscount, to: totalDiscount, animationDuration: 0.3)
        footerView.installmentLabel.text = installments
    }
    
    func displayNextButtonState(isNextButtonEnabled: Bool) {
        footerView.continueButton.isEnabled = isNextButtonEnabled
    }
}

// MARK: - UITableViewDelegate
extension SimulateAdvancePaymentViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        interactor.didTapRow(index: indexPath)
    }
}

// MARK: - UITableViewDelegate
extension SimulateAdvancePaymentViewController: SimulateAdvancePaymentCellDelegate {
    func didTapImage(from cell: UITableViewCell, status: InstallmentStatus) {
        switch status {
        case .overdue:
            showInfoDialog(title: Localizable.Warning.Overdue.title,
                           subtitle: Localizable.Warning.Overdue.description)
        case .closeToExpire:
            showInfoDialog(title: Localizable.Warning.CloseToExpire.title,
                           subtitle: Localizable.Warning.CloseToExpire.description)
        case .renegotiated:
            showInfoDialog(title: Localizable.Warning.Renegotiated.title,
                           subtitle: Localizable.Warning.Renegotiated.description)
        default:
            break
        }
    }
}

// MARK: - Helper Methods
private extension SimulateAdvancePaymentViewController {
    func showInfoDialog(title: String, subtitle: String) {
        let alertData = LoanAlertSettings(imageAsset: Resources.Illustrations.iluPersonInfo.image,
                                          title: title,
                                          subtitle: subtitle,
                                          okButtonTitle: Strings.Generic.ok,
                                          hasCancel: false)
        
        LoanAlertHelper.shared.createAlert(with: alertData, presentIn: self)
    }
}
