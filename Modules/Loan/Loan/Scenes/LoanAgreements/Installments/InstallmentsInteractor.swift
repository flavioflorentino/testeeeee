import Foundation
import AnalyticsModule
import FeatureFlag

protocol InstallmentsInteracting: AnyObject {
    func setupAnalytics()
    func fetchInstallments()
    func close()
    func pop()
    func didTapRow(index: IndexPath)
    func setupIndexSegmentControl()
    func didTapNext()
    func showFooterViewIfNeeded(indexSegmentControl: CGFloat)
    func openFAQ()
}

final class InstallmentsInteractor {
    typealias Dependencies = HasAnalytics & HasFeatureManager
    private let dependencies: Dependencies
    
    private let service: InstallmentsServicing
    private let presenter: InstallmentsPresenting
    private let contractId: String
    private var pendingInstallments: [Installment]
    private var origin: OriginType
    private var discountValueTotal: Double
    private var currentIndexSegmentControl: CGFloat = 0
    private var installmentSelectionOrder: InstallmentSelectionOrder?
    
    private let type = "type"
    private let paid = "paid"
    
    init(service: InstallmentsServicing,
         presenter: InstallmentsPresenting,
         from origin: OriginType,
         dependencies: Dependencies,
         contractId: String
    ) {
        self.dependencies = dependencies
        self.service = service
        self.presenter = presenter
        self.contractId = contractId
        self.pendingInstallments = []
        self.discountValueTotal = 0
        self.origin = origin
    }
}

extension InstallmentsInteractor: InstallmentsInteracting {
    func setupAnalytics() {
        dependencies.analytics.log(LoanInstallmentsEvent.didView(origin: origin))
        dependencies.analytics.log(LoanEvent.didEnter(to: .installments, from: origin))
    }
    
    func fetchInstallments() {
        presenter.startLoading()
        
        service.getInstallments(of: contractId) { [weak self] response in
            guard let self = self else { return }
            self.presenter.stopLoading()
            
            guard case let .success(response) = response else {
                self.presenter.presentError()
                return
            }
            
            self.pendingInstallments = response.pendingList
            self.discountValueTotal = response.discountValueTotal ?? 0.00                                   
            self.installmentSelectionOrder = response.selectionOrder
            
            self.handleInstallmentsToBeDisplayed(response)
            self.presenter.setupDescription(valueToPay: response.outstandingBalance)
            self.verifyPaymentFeatureIsActivated()
            self.presenter.presentTotalDiscount(value: self.discountValueTotal)
            self.showFooterViewIfNeeded(indexSegmentControl: self.currentIndexSegmentControl)
        }
    }
    
    func handleInstallmentsToBeDisplayed(_ response: InstallmentsResponse) {
        if response.pendingList.isEmpty {
            self.presenter.displayOpenEmptyView()
        } else {
            self.presenter.presentPendingList(response.pendingList)
        }
        
        if response.paidList.isEmpty {
            self.presenter.displayPaidEmptyView()
        } else {
            self.presenter.presentPaidList(response.paidList)
        }
    }
    
    func setupIndexSegmentControl() {
        if case let .charge(params) = origin {
            currentIndexSegmentControl = params[type] == paid ? 1 : 0
        }
        presenter.setTabSegmentControl(index: Int(currentIndexSegmentControl))
    }
    
    func didTapRow(index: IndexPath) {
        guard pendingInstallments.indices.contains(index.row) else { return }
        
        let selectedInstallment = pendingInstallments[index.row]
        
        if selectedInstallment.isPayable {
            presenter.openPaymentMethods(contractId: contractId, installment: selectedInstallment)
        } else {
            verifyStatus(installment: selectedInstallment)
        }
    }
    
    func close() {
        presenter.close()
    }
    
    func pop() {
        presenter.pop()
    }
    
    func openFAQ() {
        presenter.openFAQ(.installments)
    }

    func didTapNext() {
        guard let installmentSelectionOrder = installmentSelectionOrder else { return }
        presenter.openSimulateAdvancePayment(contractId: contractId,
                                             installments: pendingInstallments,
                                             selectionOrder: installmentSelectionOrder)
    }
    
    func showFooterViewIfNeeded(indexSegmentControl: CGFloat) {
        currentIndexSegmentControl = indexSegmentControl
        if dependencies.featureManager.isActive(.isAnticipateLoanInstallmentsAvailable), indexSegmentControl != 1, discountValueTotal != 0.00 {
            presenter.presentFooterViewIfNeeded(shouldShow: true)
        } else {
            presenter.presentFooterViewIfNeeded(shouldShow: false)
        }
    }
    
    func verifyPaymentFeatureIsActivated() {
        if dependencies.featureManager.isActive(.opsFeatureLoanPayment) {
            presenter.presentPaymentFeature()
        }
    }
}

private extension InstallmentsInteractor {
    func verifyStatus(installment: Installment) {
        if installment.status == .renegotiated {
            dependencies.analytics.log(LoanInstallmentsEvent.didTap(status: .renegotiated))
            presenter.openNegotiatedInstallment(installment: installment)
        } else {
            handleFeatureFlag()
        }
    }
    
    func handleFeatureFlag() {
        if dependencies.featureManager.isActive(.isAnticipateLoanInstallmentsAvailable) {
            presenter.showPayInAdvanceIsAvailableWarning()
        } else {
            presenter.showPayInAdvanceIsNotAvailableWarning()
        }
    }
}
