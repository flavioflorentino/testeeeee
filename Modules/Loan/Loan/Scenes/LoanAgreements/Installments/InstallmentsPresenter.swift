import Core
import FeatureFlag
import Foundation
import UI
import UIKit
import AssetsKit

protocol InstallmentsPresenting: AnyObject {
    var viewController: InstallmentsDisplay? { get set }
    func setupDescription(valueToPay value: Double)
    func presentPaidList(_ paidList: [Installment])
    func presentPendingList(_ pendingList: [Installment])
    func presentPaymentFeature()
    func startLoading()
    func stopLoading()
    func openPaymentMethods(contractId: String, installment: Installment)
    func showPayInAdvanceIsNotAvailableWarning()
    func showPayInAdvanceIsAvailableWarning()
    func close()
    func pop()
    func presentError()
    func setTabSegmentControl(index: Int)
    func openSimulateAdvancePayment(contractId: String,
                                    installments: [Installment],
                                    selectionOrder: InstallmentSelectionOrder)
    func presentTotalDiscount(value: Double)
    func presentFooterViewIfNeeded(shouldShow: Bool)
    func openNegotiatedInstallment(installment: Installment)
    func displayPaidEmptyView()
    func displayOpenEmptyView()
    func openFAQ(_ article: FAQArticle)
}

final class InstallmentsPresenter: InstallmentsPresenting {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    private let coordinator: InstallmentsCoordinating
    weak var viewController: InstallmentsDisplay?
    
    init(coordinator: InstallmentsCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
    
    func setupDescription(valueToPay value: Double) {
        guard let valueFormatted = value.toCurrencyString() else { return }
        
        let attributed = Strings.Installments.description(valueFormatted).attributedStringWith(
            normalFont: Typography.title(.small).font(),
            highlightFont: Typography.title(.small).font(),
            normalColor: Colors.grayscale700.color,
            highlightColor: Colors.neutral600.color,
            underline: false
        )
        
        viewController?.displayDescription(text: attributed)
    }
    
    func presentTotalDiscount(value: Double) {
        guard let valueFormatted = value.toCurrencyString() else { return }
        viewController?.displayTotalDiscount(text: valueFormatted)
    }
    
    func presentPaidList(_ paidList: [Installment]) {
        let paidInstallments = paidList.map {
            InstallmentViewModel(with: $0)
        }
        
        viewController?.displayPaid(data: paidInstallments)
    }
    
    func presentPendingList(_ pendingList: [Installment]) {
        let pendingInstallments = pendingList.map {
            InstallmentViewModel(with: $0)
        }
        
        viewController?.displayPending(data: pendingInstallments)
    }
    
    func presentPaymentFeature() {
        viewController?.enablePaymentFeature()
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
    
    func openPaymentMethods(contractId: String, installment: Installment) {
        coordinator.perform(action: .payment(contractId: contractId, installment: installment))
    }
    
    func openSimulateAdvancePayment(contractId: String,
                                    installments: [Installment],
                                    selectionOrder: InstallmentSelectionOrder) {
        coordinator.perform(action: .simulateAdvancePayment(contractId: contractId,
                                                            installments: installments,
                                                            selectionOrder: selectionOrder))
    }
    
    func showPayInAdvanceIsNotAvailableWarning() {
        let warning = ApolloFeedbackViewContent(image: Resources.Illustrations.iluTool.image,
                                                title: Strings.PayInAdvance.Warning.title,
                                                description: NSAttributedString(string: Strings.PayInAdvance.Warning.subtitle),
                                                primaryButtonTitle: Strings.PayInAdvance.Warning.dismissButtonTitle,
                                                secondaryButtonTitle: Strings.PayInAdvance.Warning.supportButtonTitle)
        
        coordinator.perform(action: .payInAdvanceWarning(warning, article: .payInAdvance))
    }
    
    func showPayInAdvanceIsAvailableWarning() {
        viewController?.displayPayInAdvanceIsAvailable()
    }
    
    func close() {
        coordinator.perform(action: .close)
    }
    
    func pop() {
        coordinator.perform(action: .pop)
    }
    
    func presentError() {
        viewController?.displayError()
    }
    
    func setTabSegmentControl(index: Int) {
        viewController?.displayTabSegmentControl(index: index)
    }
    
    func presentFooterViewIfNeeded(shouldShow: Bool) {
        viewController?.displayFooterViewAnimation(showFooterView: shouldShow)
    }
    
    func openNegotiatedInstallment(installment: Installment) {
        coordinator.perform(action: .openNegotiatedInstallment(installment: installment))
    }
    
    func displayPaidEmptyView() {
        viewController?.displayEmptyPaidView(message: Strings.Installments.EmptyView.noPaidInstallmentMessage,
                                             image: Resources.Illustrations.iluEmptyCircledBackground.image)
    }
    
    func displayOpenEmptyView() {
        viewController?.displayEmptyOpenView(message: Strings.Installments.EmptyView.noOpenInstallmentMessage,
                                             image: Resources.Illustrations.iluPartyCone.image)
    }
    
    func openFAQ(_ article: FAQArticle) {
        coordinator.perform(action: .openFAQ(article: article))
    }
}
