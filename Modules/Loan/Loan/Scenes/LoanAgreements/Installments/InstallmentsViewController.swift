import UI
import UIKit
import AssetsKit

private typealias Localizables = Strings.PayInAdvance.Warning

protocol InstallmentsDisplay: AnyObject {
    func displayPending(data: [InstallmentViewModel])
    func displayPaid(data: [InstallmentViewModel])
    func displayDescription(text: NSAttributedString)
    func startLoading()
    func stopLoading()
    func enablePaymentFeature()
    func displayError()
    func displayTabSegmentControl(index: Int)
    func displayTotalDiscount(text: String)
    func displayPayInAdvanceIsAvailable()
    func displayFooterViewAnimation(showFooterView: Bool)
    func displayEmptyPaidView(message: String, image: UIImage)
    func displayEmptyOpenView(message: String, image: UIImage)
}

extension InstallmentsViewController.Layout {
    enum Height {
        static let headerRow: CGFloat = 44
        static let tableViewEstimatedRowHeight: CGFloat = 96
        static let separatorView = 1
    }
    
    enum Duration {
        static let animation = 0.3
    }
}

final class InstallmentsViewController: ViewController<InstallmentsInteracting, UIView>, LoadingViewProtocol {
    fileprivate enum Layout { }
    
    enum AccessibilityIdentifier: String {
        case installmentsTableView
    }
    
    lazy var loadingView: LoadingView = {
        let loading = LoadingView()
        loading.text = Strings.Generic.Loading.text
        return loading
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.Installments.title
        label
            .labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, Colors.black.color)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var FAQBarButtonItem: UIBarButtonItem = {
        let label = UILabel()
        label
            .labelStyle(IconLabelStyle(type: .medium))
            .with(\.textColor, .branding600())
            .with(\.text, Iconography.questionCircle.rawValue)
            .with(\.isUserInteractionEnabled, true)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(FAQButtonTapped))
        label.addGestureRecognizer(tapGesture)
        let barButton = UIBarButtonItem(customView: label)
        return barButton
    }()
    
    private lazy var separatorView = SeparatorView()
    
    private lazy var headlineLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.Installments.Tabs.title
        label.textColor = Colors.grayscale700.color
        label.font = UIFont.boldSystemFont(ofSize: 16.0)
        return label
    }()
    
    private lazy var selectedListSegmentControl: CustomSegmentedControl = {
        let items = [Strings.Installments.Tabs.first, Strings.Installments.Tabs.second]
        let segmentedControl = CustomSegmentedControl(buttonTitles: items)
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        segmentedControl.textColor = Colors.grayscale400.color
        segmentedControl.selectorTextColor = Colors.branding600.color
        segmentedControl.selectorViewColor = Colors.branding600.color
        segmentedControl.backgroundColor = Colors.backgroundPrimary.color
        segmentedControl.selectorIsFullSize = false
        segmentedControl.addTarget(self, action: #selector(didTapSegmentControl), for: .valueChanged)
        return segmentedControl
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = Colors.backgroundPrimary.color
        scrollView.isPagingEnabled = true
        scrollView.delegate = self
        scrollView.showsHorizontalScrollIndicator = false
        return scrollView
    }()
    
    private lazy var stackViewContentView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [pendingInstallmentsTableView, paidInstallmentsTableView])
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    private lazy var pendingInstallmentsTableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .singleLine
        tableView.separatorInset = .zero
        tableView.allowsSelection = false
        tableView.estimatedRowHeight = Layout.Height.tableViewEstimatedRowHeight
        tableView.accessibilityIdentifier = AccessibilityIdentifier.installmentsTableView.rawValue
        tableView.tableFooterView = UIView()
        tableView.register(InstallmentCell.self, forCellReuseIdentifier: InstallmentCell.identifier)
        return tableView
    }()
    
    private lazy var paidInstallmentsTableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .singleLine
        tableView.separatorInset = .zero
        tableView.allowsSelection = false
        tableView.estimatedRowHeight = Layout.Height.tableViewEstimatedRowHeight
        tableView.accessibilityIdentifier = AccessibilityIdentifier.installmentsTableView.rawValue
        tableView.tableFooterView = UIView()
        tableView.register(InstallmentCell.self, forCellReuseIdentifier: InstallmentCell.identifier)
        return tableView
    }()
    
    private lazy var pendingInstallmentsDataSource: TableViewDataSource<Int, InstallmentViewModel> = {
        let dataSource = TableViewDataSource<Int, InstallmentViewModel>(view: pendingInstallmentsTableView)
        dataSource.add(section: 0)
        dataSource.itemProvider = { tableView, indexPath, item -> UITableViewCell? in
            let cell = tableView.dequeueReusableCell(withIdentifier: InstallmentCell.identifier, for: indexPath) as? InstallmentCell
            cell?.setup(with: item)
            cell?.configureAccessoryView()
            return cell
        }
        return dataSource
    }()
    
    private lazy var paidInstallmentsDataSource: TableViewDataSource<Int, InstallmentViewModel> = {
        let dataSource = TableViewDataSource<Int, InstallmentViewModel>(view: paidInstallmentsTableView)
        dataSource.add(section: 0)
        dataSource.itemProvider = { tableView, indexPath, item -> UITableViewCell? in
            let cell = tableView.dequeueReusableCell(withIdentifier: InstallmentCell.identifier, for: indexPath) as? InstallmentCell
            cell?.setup(with: item)
            return cell
        }
        return dataSource
    }()
    
    private lazy var footerView: InstallmentFooterView = {
        let footerView = InstallmentFooterView()
        footerView.button.addTarget(self, action: #selector(didTapFooterButton), for: .touchUpInside)
        return footerView
    }()
    
    lazy var emptyPaidInstallmentsView: EmptyInstallmentsListView = {
        let view = EmptyInstallmentsListView()
        view.isHidden = true
        return view
    }()
    
    lazy var emptyOpenInstallmentsView: EmptyInstallmentsListView = {
        let view = EmptyInstallmentsListView()
        view.isHidden = true
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setupLoanAppearance()
        navigationItem.rightBarButtonItem = FAQBarButtonItem
        
        interactor.setupAnalytics()
        interactor.setupIndexSegmentControl()
        interactor.fetchInstallments()
        
        pendingInstallmentsTableView.dataSource = pendingInstallmentsDataSource
        paidInstallmentsTableView.dataSource = paidInstallmentsDataSource
    }

    override func buildViewHierarchy() {
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
        view.addSubview(headlineLabel)
        view.addSubview(selectedListSegmentControl)
        view.addSubview(separatorView)
        view.addSubview(scrollView)
        view.addSubview(footerView)
        
        pendingInstallmentsTableView.addSubview(emptyOpenInstallmentsView)
        paidInstallmentsTableView.addSubview(emptyPaidInstallmentsView)
        
        scrollView.addSubview(stackViewContentView)
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    override func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(topLayoutGuide.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.trailing.leading.equalToSuperview().inset(Spacing.base02)
        }
        
        headlineLabel.snp.makeConstraints {
            $0.centerY.equalTo(selectedListSegmentControl)
            $0.leading.equalToSuperview().offset(Spacing.base02)
        }
        
        selectedListSegmentControl.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.height.equalTo(Layout.Height.headerRow)
        }
        
        separatorView.snp.makeConstraints {
            $0.top.equalTo(selectedListSegmentControl.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(Layout.Height.separatorView)
        }
        
        scrollView.snp.makeConstraints {
            $0.top.equalTo(separatorView.snp.bottom)
            $0.trailing.bottom.leading.equalToSuperview()
        }
        
        stackViewContentView.snp.makeConstraints {
            $0.edges.height.equalToSuperview()
        }
        
        pendingInstallmentsTableView.snp.makeConstraints {
            $0.height.width.equalTo(scrollView)
        }
        
        emptyOpenInstallmentsView.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
        
        emptyPaidInstallmentsView.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        segmentControlValueChanged()
    }
}

extension InstallmentsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        interactor.didTapRow(index: indexPath)
    }
}

@objc
private extension InstallmentsViewController {
    func didTapSegmentControl() {
        segmentControlValueChanged()
    }
    
    func didTapFooterButton() {
        interactor.didTapNext()
    }

    func FAQButtonTapped() {
        interactor.openFAQ()
    }
}

extension InstallmentsViewController {
    func segmentControlValueChanged() {
        let selectedIndex = CGFloat(selectedListSegmentControl.selectedIndex)
        let contentOffset = CGPoint(x: selectedIndex * scrollView.bounds.width, y: 0)
        scrollView.setContentOffset(contentOffset, animated: true)
        
        interactor.showFooterViewIfNeeded(indexSegmentControl: selectedIndex)
    }
}

extension InstallmentsViewController: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        guard scrollView == self.scrollView else { return }
        
        let pagesOffset = Array(stride(from: 0, to: scrollView.contentSize.width, by: scrollView.bounds.width))
        if let index = pagesOffset.firstIndex(of: scrollView.contentOffset.x) {
            selectedListSegmentControl.setIndex(index: Int(index))
            interactor.showFooterViewIfNeeded(indexSegmentControl: CGFloat(index))
        }
    }
}

extension InstallmentsViewController: InstallmentsDisplay {
    func displayTabSegmentControl(index: Int) {
        selectedListSegmentControl.setIndex(index: index)
    }
    
    func displayDescription(text: NSAttributedString) {
        descriptionLabel.attributedText = text
    }
    
    func displayTotalDiscount(text: String) {
        footerView.subtitle.text = Strings.Installments.Footer.subtitle(text)
    }
    
    func displayPending(data: [InstallmentViewModel]) {
        pendingInstallmentsDataSource.add(items: data, to: 0)
    }
    
    func displayPaid(data: [InstallmentViewModel]) {
        paidInstallmentsDataSource.add(items: data, to: 0)
    }
    
    func displayError() {
        let primaryButtonAction = ApolloAlertAction(title: Strings.Generic.tryAgain) {
            self.interactor.fetchInstallments()
        }
        
        let linkButtonAction = ApolloAlertAction(title: Strings.Generic.notNow) {
            self.interactor.pop()
        }
        
        showApolloAlert(type: .sticker(style: .error),
                        title: Strings.Generic.title,
                        subtitle: Strings.Generic.message,
                        primaryButtonAction: primaryButtonAction,
                        linkButtonAction: linkButtonAction)
    }
    
    func startLoading() {
        startLoadingView()
    }
    
    func stopLoading() {
        stopLoadingView()
    }
    
    func enablePaymentFeature() {
        pendingInstallmentsTableView.delegate = self
        pendingInstallmentsTableView.allowsSelection = true
    }
    
    func displayFooterViewAnimation(showFooterView: Bool) {
        UIView.transition(with: self.view,
                          duration: Layout.Duration.animation,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.updateFooterConstraints(to: showFooterView)
                            self.footerView.opacity = Opacity.Style(rawValue: showFooterView ? 1 : 0)
                          })
    }

    func displayPayInAdvanceIsAvailable() {
        let primaryAlertButton = ApolloAlertAction(title: Localizables.WhenAnticipationIsAvailable.continueButtonTitle) {
            self.interactor.didTapNext()
        }
        
        let linkButtonAction = ApolloAlertAction(title: Localizables.dismissButtonTitle) {}
        
        showApolloAlert(type: .sticker(style: .warning),
                        title: Localizables.WhenAnticipationIsAvailable.title,
                        subtitle: Localizables.WhenAnticipationIsAvailable.subtitle,
                        primaryButtonAction: primaryAlertButton,
                        linkButtonAction: linkButtonAction)
    }
    
    func displayEmptyPaidView(message: String, image: UIImage) {
        emptyPaidInstallmentsView.messageLabel.text = message
        emptyPaidInstallmentsView.imageView.image = image
        emptyPaidInstallmentsView.isHidden = false
    }
    
    func displayEmptyOpenView(message: String, image: UIImage) {
        emptyOpenInstallmentsView.messageLabel.text = message
        emptyOpenInstallmentsView.imageView.image = image
        emptyOpenInstallmentsView.isHidden = false
    }
}

private extension InstallmentsViewController {
    func updateFooterConstraints(to showFooterView: Bool) {
        scrollView.snp.remakeConstraints {
            $0.top.equalTo(separatorView.snp.bottom)
            $0.trailing.leading.equalToSuperview()
            if showFooterView {
                $0.bottom.equalTo(footerView.snp.top)
            } else {
                $0.bottom.equalToSuperview()
            }
        }
        
        footerView.snp.remakeConstraints {
            $0.leading.trailing.equalToSuperview()
            if showFooterView {
                $0.bottom.equalTo(bottomLayoutGuide.snp.top)
            } else {
                $0.top.equalToSuperview()
            }
        }
    }
}
