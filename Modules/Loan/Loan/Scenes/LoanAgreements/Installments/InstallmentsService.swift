import Core
import Foundation

protocol InstallmentsServicing {
    func getInstallments(of contractId: String, completion: @escaping (Result<InstallmentsResponse, ApiError>) -> Void)
}

final class InstallmentsService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension InstallmentsService: InstallmentsServicing {
    func getInstallments(of contractId: String, completion: @escaping (Result<InstallmentsResponse, ApiError>) -> Void) {
        let decoder = JSONDecoder(.useDefaultKeys)
        decoder.dateDecodingStrategy = .formatted(Date.forthFormatter)
        
        let api = Api<InstallmentsResponse>(endpoint: LoanAgreementsServiceEndpoint.installmentsList(contractId: contractId))
        api.shouldUseDefaultDateFormatter = false
        
        api.execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
