import UI
import UIKit

struct InstallmentViewModel {
    let order: String
    let dueDate: String
    let hasDiscount: Bool
    let hasValueChanged: Bool
    let isValueUpdated: Bool
    let isPayable: Bool
    
    private let originalValue: String?
    private let value: String
    private let status: InstallmentStatus
    
    let colorState: [InstallmentStatus: DynamicColorType] = [
        .paid: Colors.branding600,
        .paidOverdued: Colors.branding600,
        .paidInAdvance: Colors.branding600,
        .closeToExpire: Colors.neutral600,
        .overdue: Colors.critical900,
        .renegotiated: Colors.warning900
    ]
    
    private let dateFormatter: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "dd MMM yyyy"
        df.locale = Locale(identifier: "pt_BR_POSIX")
        return df
    }()
    
    var originalValueDescription: NSAttributedString? {
        guard isValueUpdated, let originalValue = originalValue else { return nil }
        
        let originalValueAttributedString = NSAttributedString(
            string: originalValue,
            attributes: [
                .font: Typography.caption().font(),
                .foregroundColor: Colors.grayscale500.color,
                .strikethroughStyle: NSUnderlineStyle.single.rawValue,
                .strikethroughColor: Colors.grayscale500.color
            ]
        )
        return originalValueAttributedString
    }
    
    var valueDescription: NSAttributedString? {
        guard let valueToDisplay = isValueUpdated ? value : originalValue else { return nil }
        
        let attr = NSMutableAttributedString(string: valueToDisplay)
        attr.addAttributes(
            [.foregroundColor: (colorState[status] ?? Colors.grayscale700).color],
            range: NSRange(location: 0, length: attr.length)
        )
        return attr
    }
    
    var statusDescription: NSAttributedString {
        let attr = NSMutableAttributedString(string: status.description)
        attr.addAttributes(
            [.foregroundColor: (colorState[status] ?? Colors.grayscale700).color],
            range: NSRange(location: 0, length: attr.length)
        )
        return attr
    }
    
    init(with installment: Installment) {
        order = "\(installment.order)ª"
        dueDate = dateFormatter
            .string(from: installment.dueDate)
            .uppercased()
            .replacingOccurrences(of: ".", with: "")
        value = installment.value.toCurrencyString() ?? ""
        status = installment.status
        originalValue = installment.originalValue.toCurrencyString()
        hasDiscount = installment.originalValue != installment.value
        isPayable = installment.isPayable
        hasValueChanged = value != originalValue
        isValueUpdated = hasValueChanged &&
            (installment.status == .overdue
            || installment.status == .paid
            || installment.status == .paidInAdvance
            || installment.status == .paidOverdued)
    }
}

extension InstallmentViewModel: Equatable {
    static func == (lhs: InstallmentViewModel, rhs: InstallmentViewModel) -> Bool {
        lhs.order == rhs.order &&
        lhs.dueDate == rhs.dueDate &&
        lhs.originalValue == rhs.originalValue &&
        lhs.value == rhs.value &&
        lhs.status == rhs.status
    }
}
