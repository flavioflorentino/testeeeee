import Foundation

enum InstallmentsFactory {
    static func make(with contractId: String, from origin: OriginType, delegate: LoanAgreementsCoordinating) -> InstallmentsViewController {
        let dependencies = LoanDependencyContainer()
        let service: InstallmentsServicing = InstallmentsService(dependencies: dependencies)
        let coordinator: InstallmentsCoordinating = InstallmentsCoordinator(dependencies: dependencies, delegate: delegate)
        let presenter: InstallmentsPresenting = InstallmentsPresenter(coordinator: coordinator, dependencies: dependencies)
        let interactor = InstallmentsInteractor(service: service,
                                                presenter: presenter,
                                                from: origin,
                                                dependencies: dependencies,
                                                contractId: contractId)
        let viewController = InstallmentsViewController(interactor: interactor)
        
        presenter.viewController = viewController
        coordinator.viewController = viewController

        return viewController
    }
}
