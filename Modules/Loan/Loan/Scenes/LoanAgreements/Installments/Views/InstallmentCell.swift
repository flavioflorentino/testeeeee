import AssetsKit
import UI
import UIKit

final class InstallmentCell: UITableViewCell {
    private lazy var orderLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
        return label
    }()
    
    private lazy var dueDateTitleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle(type: .default))
        label.text = Strings.Installments.dueDate
        return label
    }()
    
    private lazy var dueDateLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.grayscale700.color)
        return label
    }()
    
    private lazy var dueDateStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [dueDateTitleLabel, dueDateLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base00
        stackView.alignment = .leading
        return stackView
    }()
    
    private lazy var statusLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle(type: .default))
        label.text = Strings.Installments.dueDate
        return label
    }()
    
    private lazy var originalValueLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle(type: .default))
            .with(\.textAlignment, .right)
        return label
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
        return label
    }()
    
    private lazy var paymentInfoStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [statusLabel, originalValueLabel, valueLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base00
        stackView.alignment = .trailing
        return stackView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension InstallmentCell {
    func setup(with installment: InstallmentViewModel) {
        orderLabel.text = installment.order
        dueDateLabel.text = installment.dueDate
        statusLabel.attributedText = installment.statusDescription
        originalValueLabel.attributedText = installment.originalValueDescription
        valueLabel.attributedText = installment.valueDescription
    }
    
    func configureAccessoryView() {
        accessoryView = UIImageView(image: Resources.Icons.icoGreenRightArrow.image)
        updateConstraintDueToAccessoryView()
    }
    
    private func updateConstraintDueToAccessoryView() {
        paymentInfoStackView.snp.updateConstraints {
            $0.trailing.equalToSuperview().offset(-Spacing.base06)
        }
    }
}

extension InstallmentCell: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(orderLabel)
        addSubview(dueDateStackView)
        addSubview(paymentInfoStackView)
    }
    
    func setupConstraints() {
        orderLabel.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
        }
        
        dueDateStackView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.centerY.equalToSuperview()
            $0.leading.equalTo(orderLabel.snp.trailing).offset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base02)
        }
        
        paymentInfoStackView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
    }
    
    func configureViews() {
        selectionStyle = .none
        accessoryView = .none
    }
}
