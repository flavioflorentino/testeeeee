import AssetsKit
import UI
import UIKit

private extension EmptyInstallmentsListView.Layout {
    enum Size {
        static let imageView = CGSize(width: 128, height: 160)
    }
}

final class EmptyInstallmentsListView: UIView {
    fileprivate enum Layout {}
    
    // MARK: - Properties
    lazy var messageLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.grayscale700.color)
            .with(\.textAlignment, .center)
        return label
    }()
    
    lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [imageView, messageLabel])
        stackView.spacing = Spacing.base03
        stackView.axis = .vertical
        return stackView
    }()

    // MARK: - Initialization
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension EmptyInstallmentsListView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(stackView)
    }
    
    func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base06)
            $0.center.equalToSuperview()
        }
        
        imageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.imageView)
        }
    }
    
    func configureViews() {
        self.backgroundColor = .white
    }
}
