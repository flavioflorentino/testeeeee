import Foundation
import UI
import UIKit
import AssetsKit

private extension InstallmentFooterView.Layout {
    enum Size {
        static let button = CGSize(width: 128, height: 40)
    }
}

final class InstallmentFooterView: UIView {
    fileprivate enum Layout {}
    
    private lazy var title: UILabel = {
        let label = UILabel()
        label.text = Strings.Installments.Footer.title
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, Colors.grayscale700.color)
        return label
    }()
    
    private lazy var separatorView = SeparatorView()
    
    lazy var subtitle: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.grayscale400.color)
        return label
    }()
    
    lazy var button: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Strings.Installments.Footer.button, for: .normal)
        return button
    }()
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension InstallmentFooterView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(title)
        addSubview(subtitle)
        addSubview(button)
        addSubview(separatorView)
    }
    
    func setupConstraints() {
        title.snp.makeConstraints {
            $0.leading.top.equalToSuperview().inset(Spacing.base02)
        }

        subtitle.snp.makeConstraints {
            $0.top.equalTo(title.snp.bottom)
            $0.bottom.leading.equalToSuperview().inset(Spacing.base02)
        }
        
        button.snp.makeConstraints {
            $0.top.bottom.trailing.equalToSuperview().inset(Spacing.base02)
            $0.size.equalTo(Layout.Size.button)
        }
        
        separatorView.snp.makeConstraints {
            $0.leading.trailing.top.equalToSuperview()
        }
    }
    
    func configureViews() {
        viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }
}
