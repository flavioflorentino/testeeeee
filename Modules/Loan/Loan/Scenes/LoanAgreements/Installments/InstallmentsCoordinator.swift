import Foundation
import Core
import UIKit
import UI

enum InstallmentsAction: Equatable {
    case close
    case pop
    case payment(contractId: String, installment: Installment)
    case openFAQ(article: FAQArticle)
    case payInAdvanceWarning(_ warning: ApolloFeedbackViewContent, article: FAQArticle)
    case simulateAdvancePayment(contractId: String,
                                installments: [Installment],
                                selectionOrder: InstallmentSelectionOrder)
    case openNegotiatedInstallment(installment: Installment)
}

protocol InstallmentsCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: InstallmentsAction)
}

final class InstallmentsCoordinator: InstallmentsCoordinating {
    weak var viewController: UIViewController?
    typealias Dependencies = HasLegacy
    private let dependencies: Dependencies
    private weak var delegate: LoanAgreementsCoordinating?
    
    init(dependencies: Dependencies, delegate: LoanAgreementsCoordinating) {
        self.dependencies = dependencies
        self.delegate = delegate
    }
    
    func perform(action: InstallmentsAction) {
        switch action {
        case .close:
            delegate?.perform(action: .close)
        case.pop:
            delegate?.perform(action: .pop)
        case let .payment(contractId, installment):
            delegate?.perform(action: .payment(contractId: contractId, installments: [installment]))
        case let .payInAdvanceWarning(warning, article):
            startPayInAdvanceWarning(warning, article: article)
        case let .openFAQ(article):
            dependencies.legacy.customerSupport?.push(article: article)
        case let .simulateAdvancePayment(contractId, installments, selectionOrder):
            delegate?.perform(action: .simulateAdvancePayment(contractId: contractId,
                                                              installments: installments,
                                                              selectionOrder: selectionOrder))
        case let .openNegotiatedInstallment(installment):
            delegate?.perform(action: .openNegotiatedInstallment(installment: installment))
        }
    }
}

extension InstallmentsCoordinator {
    func startPayInAdvanceWarning(_ warning: ApolloFeedbackViewContent, article: FAQArticle) {
        guard let navigationController = viewController?.navigationController else { return }
        let controller = ApolloFeedbackViewController(content: warning)
        controller.didTapPrimaryButton = { self.perform(action: .pop) }
        controller.didTapSecondaryButton = { self.perform(action: .openFAQ(article: article)) }
        controller.navigationItem.hidesBackButton = true
        navigationController.pushViewController(controller, animated: true)
    }
}
