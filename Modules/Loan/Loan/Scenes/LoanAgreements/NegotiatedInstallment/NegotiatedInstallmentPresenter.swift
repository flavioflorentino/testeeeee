import UI

protocol NegotiatedInstallmentPresenting: AnyObject {
    var viewController: NegotiatedInstallmentDisplaying? { get set }
    func presentData(value: Double, userEmail: String)
    func openFAQ()
}

final class NegotiatedInstallmentPresenter {
    private let coordinator: NegotiatedInstallmentCoordinating
    weak var viewController: NegotiatedInstallmentDisplaying?

    init(coordinator: NegotiatedInstallmentCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - NegotiatedInstallmentPresenting
extension NegotiatedInstallmentPresenter: NegotiatedInstallmentPresenting {
    func presentData(value: Double, userEmail: String) {
        let maskUserEmail = StringObfuscationMasker.mask(email: userEmail) ?? ""
        let valueString = value.toCurrencyString() ?? ""
        viewController?.displayData(value: valueString, userEmail: maskUserEmail)
    }
    
    func openFAQ() {
        coordinator.perform(action: .openFAQ(.negotiated))
    }
}
