import UIKit

enum NegotiatedInstallmentFactory {
    static func make(installment: Installment) -> NegotiatedInstallmentViewController {
        let container = LoanDependencyContainer()
        let coordinator: NegotiatedInstallmentCoordinating = NegotiatedInstallmentCoordinator(dependencies: container)
        let presenter: NegotiatedInstallmentPresenting = NegotiatedInstallmentPresenter(coordinator: coordinator)
        let interactor = NegotiatedInstallmentInteractor(presenter: presenter, dependencies: container, installment: installment)
        let viewController = NegotiatedInstallmentViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
