import Foundation
import UI
import UIKit
import AssetsKit

private extension NegotiatedInstallmentDisclaimerView.Layout {
    enum Size {
        static let imageSize = CGSize(width: 20, height: 20)
    }
}

final class NegotiatedInstallmentDisclaimerView: UIView {
    fileprivate enum Layout {}
    
    private lazy var textLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.NegociatedInstallment.Disclaimer.text
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.grayscale700.color)
        return label
    }()
    
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = Resources.Icons.icoBlueInfo.image
        return imageView
    }()
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension NegotiatedInstallmentDisclaimerView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(iconImageView)
        addSubview(textLabel)
    }
    
    func setupConstraints() {
        iconImageView.snp.makeConstraints {
            $0.top.leading.equalToSuperview().offset(Spacing.base02)
            $0.size.equalTo(Layout.Size.imageSize)
        }
        
        textLabel.snp.makeConstraints {
            $0.leading.equalTo(iconImageView.snp.trailing).offset(Spacing.base01)
            $0.top.bottom.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureViews() {
        viewStyle(BackgroundViewStyle(color: .grayscale050()))
            .with(\.cornerRadius, .medium)
    }
}
