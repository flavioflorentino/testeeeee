import UIKit

enum NegotiatedInstallmentAction: Equatable {
    case openFAQ(_ article: FAQArticle)
}

protocol NegotiatedInstallmentCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: NegotiatedInstallmentAction)
}

final class NegotiatedInstallmentCoordinator {
    weak var viewController: UIViewController?
    typealias Dependencies = HasLegacy
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - NegotiatedInstallmentCoordinating
extension NegotiatedInstallmentCoordinator: NegotiatedInstallmentCoordinating {
    func perform(action: NegotiatedInstallmentAction) {
        if case let .openFAQ(article) = action {
            dependencies.legacy.customerSupport?.push(article: article)
        }
    }
}
