import Foundation
import AnalyticsModule

protocol NegotiatedInstallmentInteracting: AnyObject {
    func fetch()
    func didTapFAQ()
}

final class NegotiatedInstallmentInteractor {
    typealias Dependencies = HasAnalytics & HasLegacy
    private let dependencies: Dependencies

    private let presenter: NegotiatedInstallmentPresenting
    private let installment: Installment

    init(presenter: NegotiatedInstallmentPresenting, dependencies: Dependencies, installment: Installment) {
        self.presenter = presenter
        self.dependencies = dependencies
        self.installment = installment
    }
}

// MARK: - NegotiatedInstallmentInteracting
extension NegotiatedInstallmentInteractor: NegotiatedInstallmentInteracting {
    func fetch() {
        dependencies.analytics.log(NegotiatedInstallmentEvent.didEnter(screen: .negotiated))
        
        let userEmail = dependencies.legacy.consumer?.consumerEmail
        presenter.presentData(value: installment.value, userEmail: userEmail ?? "")
    }
    
    func didTapFAQ() {
        dependencies.analytics.log(NegotiatedInstallmentEvent.didTap)
        
        presenter.openFAQ()
    }
}
