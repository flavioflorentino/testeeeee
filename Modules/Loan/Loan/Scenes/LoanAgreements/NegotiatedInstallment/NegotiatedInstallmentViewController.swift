import UI
import UIKit

private typealias Localizable = Strings.NegociatedInstallment

protocol NegotiatedInstallmentDisplaying: AnyObject {
    func displayData(value: String, userEmail: String)
}

private extension NegotiatedInstallmentViewController.Layout {
    enum Size {
        static let imageSize = CGSize(width: 167, height: 140)
    }
}

final class NegotiatedInstallmentViewController: ViewController<NegotiatedInstallmentInteracting, UIView> {
    fileprivate enum Layout { }
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.alwaysBounceVertical = true
        scrollView.showsVerticalScrollIndicator = false
        return scrollView
    }()
    
    private lazy var contentView = UIView()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = Assets.bankSlipImage.image
        return imageView
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.title
        label
            .labelStyle(TitleLabelStyle(type: .large))
            .with(\.textColor, Colors.black.color)
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var firstSeparatorView = SeparatorView()
    
    private lazy var detailLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.detail
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, Colors.grayscale700.color)
            .with(\.textAlignment, .left)
        return label
    }()
    
    private lazy var totalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fill
        return stackView
    }()
    
    private lazy var totalTitleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.total
        label.setContentHuggingPriority(.required, for: .horizontal)
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale700.color)
        return label
    }()
    
    private lazy var totalValueLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.grayscale700.color)
            .with(\.textAlignment, .right)
        return label
    }()
    
    private lazy var emailStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.alignment = .fill
        return stackView
    }()
        
    private lazy var emailTitleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.email
        label.setContentHuggingPriority(.required, for: .horizontal)
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale700.color)
        return label
    }()
    
    private lazy var emailLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.grayscale700.color)
            .with(\.textAlignment, .right)
        return label
    }()
    
    private lazy var secondSeparatorView = SeparatorView()
    
    private lazy var disclaimerView = NegotiatedInstallmentDisclaimerView()
    
    private lazy var helpLinkButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.NegociatedInstallment.helpButton, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(helpButtonTapped), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = Localizable.Navigation.title
        
        interactor.fetch()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
    }

    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(imageView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(firstSeparatorView)
        contentView.addSubview(detailLabel)
        contentView.addSubview(totalStackView)
        contentView.addSubview(emailStackView)

        contentView.addSubview(secondSeparatorView)
        contentView.addSubview(disclaimerView)
        contentView.addSubview(helpLinkButton)
        
        totalStackView.addArrangedSubview(totalTitleLabel)
        totalStackView.addArrangedSubview(totalValueLabel)
        
        emailStackView.addArrangedSubview(emailTitleLabel)
        emailStackView.addArrangedSubview(emailLabel)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        contentView.snp.makeConstraints {
            $0.edges.width.equalToSuperview()
            $0.height.equalTo(view.compatibleSafeArea.height).priority(.low)
        }
        
        imageView.snp.makeConstraints {
            $0.top.equalTo(contentView.snp.top).offset(Spacing.base04)
            $0.size.equalTo(Layout.Size.imageSize)
            $0.centerX.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base03)
            $0.centerX.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base01)
        }
    
        firstSeparatorView.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        detailLabel.snp.makeConstraints {
            $0.top.equalTo(firstSeparatorView.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
        }
        
        totalStackView.snp.makeConstraints {
            $0.top.equalTo(detailLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        emailStackView.snp.makeConstraints {
            $0.top.equalTo(totalStackView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
         
        secondSeparatorView.snp.makeConstraints {
            $0.top.equalTo(emailStackView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        disclaimerView.snp.makeConstraints {
            $0.top.equalTo(secondSeparatorView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        helpLinkButton.snp.makeConstraints {
            $0.bottom.equalToSuperview().offset(-Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.centerX.equalToSuperview()
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    private func setupNavigationBar() {
        navigationController?.setupLoanAppearance()
    }
}

// MARK: - NegotiatedInstallmentDisplaying
extension NegotiatedInstallmentViewController: NegotiatedInstallmentDisplaying {
    func displayData(value: String, userEmail: String) {
        totalValueLabel.text = value
        emailLabel.text = userEmail
    }
}

// MARK: - Helper Methods
private extension NegotiatedInstallmentViewController {
    @objc
    func helpButtonTapped() {
        interactor.didTapFAQ()
    }
}
