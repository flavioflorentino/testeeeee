import Foundation
import UIKit
import UI

private extension TimeExceededBulletView.Layout {
    enum Image {
        static let size = CGSize(width: 20, height: 22)
    }
}

final class TimeExceededBulletView: UIView {
    fileprivate enum Layout {}
    
    private lazy var stackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [iconLabel, descriptionLabel])
        stack.axis = .horizontal
        stack.distribution = .fillProportionally
        stack.alignment = .top
        stack.spacing = Spacing.base01
        return stack
    }()
    
    private lazy var iconLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(IconLabelStyle(type: .medium))
            .with(\.textColor, .neutral400())
            .with(\.textAlignment, .center)
        label.numberOfLines = 1
        label.text = Iconography.calendarAlt.rawValue
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale700())
        return label
    }()
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(description: String) {
        let attributed = description.attributedStringWith(
            normalFont: Typography.bodyPrimary().font(),
            highlightFont: Typography.bodyPrimary(.highlight).font(),
            normalColor: Colors.grayscale700.color,
            highlightColor: Colors.grayscale700.color,
            underline: false
        )
        
        descriptionLabel.attributedText = attributed
    }
}

extension TimeExceededBulletView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(stackView)
    }
    
    func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    func configureViews() {
        backgroundColor = .clear
    }
}
