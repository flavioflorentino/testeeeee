import Foundation
import UI

protocol TimeExceededPresenting: AnyObject {
    var viewController: TimeExceededDisplaying? { get set }
    func presentDescription()
    func presentBulletItems()
    func openFAQ()
    func didNextStep(action: TimeExceededAction)
}

final class TimeExceededPresenter {
    private let coordinator: TimeExceededCoordinating
    weak var viewController: TimeExceededDisplaying?

    init(coordinator: TimeExceededCoordinating) {
        self.coordinator = coordinator
    }
}

extension TimeExceededPresenter: TimeExceededPresenting {
    func presentDescription() {
        let attributedDescription = Strings.TimeExceeded.description.attributedStringWith(
            normalFont: Typography.bodyPrimary().font(),
            highlightFont: Typography.bodyPrimary(.highlight).font(),
            normalColor: Colors.grayscale700.color,
            highlightColor: Colors.grayscale700.color,
            underline: false
        )
        
        viewController?.displayDescription(attributedDescription)
    }
    
    func presentBulletItems() {
        viewController?.displayBulletItem(description: Strings.TimeExceeded.Bullet.first)
        viewController?.displayBulletItem(description: Strings.TimeExceeded.Bullet.second)
    }
    
    func openFAQ() {
        coordinator.perform(action: .openFAQ(.bankSlip))
    }
    
    func didNextStep(action: TimeExceededAction) {
        coordinator.perform(action: action)
    }
}
