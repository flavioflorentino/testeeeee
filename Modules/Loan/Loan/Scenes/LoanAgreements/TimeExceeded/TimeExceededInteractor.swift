import Foundation

protocol TimeExceededInteracting: AnyObject {
    func viewDidLoad()
    func didTapNext()
    func didTapFAQ()
}

final class TimeExceededInteractor {
    private let service: TimeExceededServicing
    private let presenter: TimeExceededPresenting

    init(service: TimeExceededServicing, presenter: TimeExceededPresenting) {
        self.service = service
        self.presenter = presenter
    }
}

extension TimeExceededInteractor: TimeExceededInteracting {
    func viewDidLoad() {
        presenter.presentDescription()
        presenter.presentBulletItems()
    }
    
    func didTapNext() {
        presenter.didNextStep(action: .close)
    }
    
    func didTapFAQ() {
        presenter.openFAQ()
    }
}
