import UI
import UIKit

protocol TimeExceededDisplaying: AnyObject {
    func displayDescription(_ description: NSMutableAttributedString)
    func displayBulletItem(description: String)
}

private extension TimeExceededViewController.Layout {
    enum Image {
        static let size = CGSize(width: 140, height: 140)
    }
}

final class TimeExceededViewController: ViewController<TimeExceededInteracting, UIView> {
    fileprivate enum Layout { }
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsVerticalScrollIndicator = false
        return scrollView
    }()
    
    private lazy var contentView = UIView()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.timeExceeded.image
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.TimeExceeded.title
        label
            .labelStyle(TitleLabelStyle(type: .large))
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var bulletStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = Spacing.base02
        stack.distribution = .fillProportionally
        return stack
    }()
    
    private lazy var nextButton: UIButton = {
       let button = UIButton()
        button.setTitle(Strings.Generic.ok, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTapNext), for: .touchUpInside)
        return button
    }()
    
    private lazy var linkButton: UIButton = {
       let button = UIButton()
        button.setTitle(Strings.TimeExceeded.HelpButton.title, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(didTapFAQ), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.viewDidLoad()
    }

    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(imageView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(bulletStackView)
        scrollView.addSubview(nextButton)
        scrollView.addSubview(linkButton)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        contentView.snp.makeConstraints {
            $0.edges.width.equalToSuperview()
            $0.height.equalTo(view.compatibleSafeArea.height).priority(.low)
        }
        
        imageView.snp.makeConstraints {
            $0.top.equalTo(contentView.snp.top).offset(Spacing.base04)
            $0.centerX.equalToSuperview()
            $0.size.equalTo(Layout.Image.size)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        bulletStackView.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        nextButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        linkButton.snp.makeConstraints {
            $0.top.equalTo(nextButton.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base04)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}

@objc
extension TimeExceededViewController {
    func didTapNext() {
        interactor.didTapNext()
    }
    
    func didTapFAQ() {
        interactor.didTapFAQ()
    }
}

extension TimeExceededViewController: TimeExceededDisplaying {
    func displayDescription(_ description: NSMutableAttributedString) {
        descriptionLabel.attributedText = description
    }
    
    func displayBulletItem(description: String) {
        let view = TimeExceededBulletView()
        view.setup(description: description)
        bulletStackView.addArrangedSubview(view)
    }
}
