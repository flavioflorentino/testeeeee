import UIKit

enum TimeExceededFactory {
    static func make() -> TimeExceededViewController {
        let dependencies = LoanDependencyContainer()
        let service: TimeExceededServicing = TimeExceededService()
        let coordinator: TimeExceededCoordinating = TimeExceededCoordinator(dependencies: dependencies)
        let presenter: TimeExceededPresenting = TimeExceededPresenter(coordinator: coordinator)
        let interactor = TimeExceededInteractor(service: service, presenter: presenter)
        let viewController = TimeExceededViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
