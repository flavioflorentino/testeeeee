import UIKit

enum TimeExceededAction {
    case openFAQ(_ article: FAQArticle)
    case close
}

protocol TimeExceededCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: TimeExceededAction)
}

final class TimeExceededCoordinator {
    typealias Dependencies = HasLegacy
    private let dependencies: Dependencies
    
    weak var viewController: UIViewController?
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension TimeExceededCoordinator: TimeExceededCoordinating {
    func perform(action: TimeExceededAction) {
        switch action {
        case let .openFAQ(article):
            dependencies.legacy.customerSupport?.push(article: article)
        case .close:
            viewController?.dismiss(animated: true)
        }
    }
}
