import Foundation
import UI
import UIKit

public enum LoanAgreementsAction: Equatable {
    case myLoanAgreements(origin: OriginType)
    case installments(contractId: String, origin: OriginType)
    case simulateAdvancePayment(contractId: String,
                                installments: [Installment],
                                selectionOrder: InstallmentSelectionOrder)
    case hireAdvancePayment(contractId: String, installments: [Installment])
    case payment(contractId: String, installments: [Installment])
    case close
    case pop
    case openNegotiatedInstallment(installment: Installment)
}

protocol LoanAgreementsCoordinating: Coordinating {
    func perform(action: LoanAgreementsAction)
}

final class LoanAgreementsCoordinator: LoanAgreementsCoordinating {
    var childViewController: [UIViewController] = []
    var viewController: UIViewController?
    var navigationController: UINavigationController
    private weak var delegate: LoanFlowCoordinating?
    
    init(from navigationController: UINavigationController, delegate: LoanFlowCoordinating) {
        self.delegate = delegate
        self.navigationController = navigationController
    }
    
    func perform(action: LoanAgreementsAction) {
        switch action {
        case .close:
            delegate?.close()
        case let .payment(contractId, installments):
            delegate?.start(flow: .payment(contractId: contractId, installments: installments))
        case .pop:
            navigationController.popViewController(animated: true)
        default:
            handleFlow(action)
        }
    }
    
    private func handleFlow(_ action: LoanAgreementsAction) {
        let controller: UIViewController
        
        switch action {
        case let .myLoanAgreements(origin):
            controller = MyLoanAgreementsFactory.make(delegate: self, from: origin)
        case let .installments(contractId, origin):
            controller = InstallmentsFactory.make(with: contractId, from: origin, delegate: self)
        case let .simulateAdvancePayment(contractId, installments, selectionOrder):
            controller = SimulateAdvancePaymentFactory.make(with: contractId,
                                                            installments: installments,
                                                            bankSelectionOrder: selectionOrder,
                                                            delegate: self)
        case let .hireAdvancePayment(contractId, installments):
            controller = HireAdvancePaymentFactory.make(contractId: contractId,
                                                        installments: installments,
                                                        delegate: self)
        case let .openNegotiatedInstallment(installment):
            controller = NegotiatedInstallmentFactory.make(installment: installment)
        default:
            return
        }
        navigationController.pushViewController(controller, animated: true)
    }
}
