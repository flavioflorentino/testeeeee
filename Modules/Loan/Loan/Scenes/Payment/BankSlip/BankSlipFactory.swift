import UIKit

enum BankSlipFactory {
    static func make(bankSlip: BankSlip,
                     installments: [Installment],
                     contractId: String,
                     delegate: PaymentFlowCoordinating) -> BankSlipViewController {
        let dependencies = LoanDependencyContainer()
        let service: BankSlipServicing = BankSlipService(dependencies: dependencies)
        let coordinator: BankSlipCoordinating = BankSlipCoordinator(delegate: delegate)
        let presenter: BankSlipPresenting = BankSlipPresenter(coordinator: coordinator)
        let interactor: BankSlipInteracting = BankSlipInteractor(service: service,
                                                                 presenter: presenter,
                                                                 bankSlip: bankSlip,
                                                                 contractId: contractId,
                                                                 installments: installments,
                                                                 dependencies: dependencies)
        let viewController = BankSlipViewController(interactor: interactor)

        presenter.viewController = viewController

        return viewController
    }
}
