import UI
import UIKit

struct BankSlipViewModel {
    private let value: String
    private let dueDate: Date
    let code: String
    
    var installmentDescription: NSAttributedString {
        let dueDateFormatted = dueDate.ddDeMMMM()
        let dueDateDescriptionRawValue = Strings.Payment.BankSlip.Details.title(dueDateFormatted, value)
        
        return dueDateDescriptionRawValue.attributedStringWith(
            normalFont: Typography.title(.small).font(),
            highlightFont: Typography.title(.small).font(),
            normalColor: Colors.black.color,
            highlightColor: Colors.neutral600.color,
            textAlignment: .center,
            underline: false
        )
    }
    
    init(bankSlip: BankSlip) {
        value = bankSlip.value
        dueDate = bankSlip.dueDate
        code = bankSlip.code
    }
}
