import Foundation

protocol BankSlipPresenting: AnyObject {
    var viewController: BankSlipDisplaying? { get set }
    func displayBankSlipInfo(bankSlip: BankSlip)
    func didNextStep(action: BankSlipAction)
    func startLoading()
    func stopLoading()
    func presentCopyCodeSucceded()
    func presentSendByEmailSucceded(email: String)
    func presentError()
    func close()
}

final class BankSlipPresenter {
    private let coordinator: BankSlipCoordinating
    weak var viewController: BankSlipDisplaying?
    
    init(coordinator: BankSlipCoordinating) {
        self.coordinator = coordinator
    }
}

extension BankSlipPresenter: BankSlipPresenting {
    func displayBankSlipInfo(bankSlip: BankSlip) {
        let viewModel = BankSlipViewModel(bankSlip: bankSlip)
        viewController?.displayInfos(description: viewModel.installmentDescription, code: viewModel.code)
    }
    
    func didNextStep(action: BankSlipAction) {
        coordinator.perform(action: action)
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
    
    func presentSendByEmailSucceded(email: String) {
        viewController?.displaySendByEmailSucceded(email: email)
    }
    
    func presentCopyCodeSucceded() {
        viewController?.displayCopyCodeSucceded()
    }
    
    func presentError() {
        viewController?.displayError()
    }
    
    func close() {
        coordinator.perform(action: .back)
    }
}
