import UI
import UIKit
import AssetsKit

private typealias Localizable = Strings.Payment.BankSlip

protocol BankSlipDisplaying: AnyObject {
    func displayInfos(description: NSAttributedString, code: String)
    func displayError()
    func displayCopyCodeSucceded()
    func displaySendByEmailSucceded(email: String)
    func startLoading()
    func stopLoading()
}

private extension BankSlipViewController.Layout {
    enum Size {
        static let imageSize = CGSize(width: 167, height: 140)
    }
    
    enum Height {
        static let copyCodeInitialHeight: CGFloat = 0
    }
    
    enum Duration {
        static let animationTime: Double = 0.2
        static let delayAnimation: Double = 2.0
        static let intervalButtonDisabled: Double = animationTime + delayAnimation
    }
}

final class BankSlipViewController: ViewController<BankSlipInteracting, UIView>, LoadingViewProtocol {
    lazy var loadingView: LoadingView = {
        let loading = LoadingView()
        loading.text = Strings.Generic.Loading.text
        return loading
    }()
    
    fileprivate enum Layout { }

    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.alwaysBounceVertical = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.isHidden = true
        return scrollView
    }()
    
    private lazy var contentView = UIView()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = Assets.bankSlipImage.image
        return imageView
    }()
    
    private lazy var bankSlipInfoLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var bankSlipCodeTitleLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.Payment.BankSlip.Code.text
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale700.color)
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var bankSlipCodeLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.black.color)
        label.textAlignment = .center
        return label
    }()
    
    private lazy var copyCodeSuccessLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = Colors.neutral600.color
        label.text = Localizable.Code.CopyCode.SuccessAlert.text
        label.textAlignment = .center
        label.font = Typography.bodySecondary().font()
        label.textColor = Colors.white.color
        label.layer.cornerRadius = CornerRadius.light
        label.layer.masksToBounds = true
        return label
    }()
    
    private lazy var copyCodeButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.Code.CopyButton.text, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTapCopy), for: .touchUpInside)
        return button
    }()
    
    private lazy var sendEmailLinkButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.SendEmail.Link.text, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(didTapSendByEmail), for: .touchUpInside)
        return button
    }()
    
    private lazy var disclaimerView: ApolloFeedbackCard = {
        let feedbackCard = ApolloFeedbackCard(description: Localizable.Disclaimer.text,
                                              iconType: .information,
                                              layoutType: .onlyText)
        return feedbackCard
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.trackEnterView()
        interactor.loadData()
    }

    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(imageView)
        contentView.addSubview(bankSlipInfoLabel)
        contentView.addSubview(bankSlipCodeTitleLabel)
        contentView.addSubview(bankSlipCodeLabel)
        contentView.addSubview(copyCodeSuccessLabel)
        contentView.addSubview(disclaimerView)
        contentView.addSubview(copyCodeButton)
        contentView.addSubview(sendEmailLinkButton)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        contentView.snp.makeConstraints {
            $0.edges.width.equalToSuperview()
            $0.height.equalTo(view.compatibleSafeArea.height).priority(.low)
        }
        
        imageView.snp.makeConstraints {
            $0.top.equalTo(contentView.snp.top).inset(Spacing.base04)
            $0.size.equalTo(Layout.Size.imageSize)
            $0.centerX.equalToSuperview()
        }
        
        bankSlipInfoLabel.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.centerX.equalToSuperview()
        }

        bankSlipCodeTitleLabel.snp.makeConstraints {
            $0.top.equalTo(bankSlipInfoLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.centerX.equalToSuperview()
        }
        
        bankSlipCodeLabel.snp.makeConstraints {
            $0.top.equalTo(bankSlipCodeTitleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.centerX.equalToSuperview()
        }
        
        copyCodeSuccessLabel.snp.makeConstraints {
            $0.bottom.leading.trailing.equalTo(bankSlipCodeLabel)
            $0.height.equalTo(Layout.Height.copyCodeInitialHeight)
        }
        
        disclaimerView.snp.makeConstraints {
            $0.top.equalTo(bankSlipCodeLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.centerX.equalToSuperview()
        }
        
        copyCodeButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.centerX.equalToSuperview()
        }
        
        sendEmailLinkButton.snp.makeConstraints {
            $0.top.equalTo(copyCodeButton.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.centerX.equalToSuperview()
            $0.bottom.lessThanOrEqualToSuperview().inset(Spacing.base02)
        }
    }

    override func configureViews() {
        navigationController?.setupLoanAppearance()
        title = Strings.Payment.BankSlip.title
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    private func updateCopyCodeSuccess(show: Bool) {
        let height = show ? bankSlipCodeLabel.frame.height : Layout.Height.copyCodeInitialHeight
        
        copyCodeSuccessLabel.snp.updateConstraints {
            $0.height.equalTo(height)
        }
        
        view.layoutIfNeeded()
    }
}

extension BankSlipViewController: BankSlipDisplaying {
    func displayInfos(description: NSAttributedString, code: String) {
        bankSlipInfoLabel.attributedText = description
        bankSlipCodeLabel.text = code
        scrollView.isHidden = false
    }
    
    func displayError() {
        let primaryButtonAction = ApolloAlertAction(title: Strings.Generic.tryAgain) {
            self.interactor.sendBankSlipByEmail()
        }
        
        let linkButtonAction = ApolloAlertAction(title: Strings.Generic.notNow) {
            self.interactor.close()
        }
        
        showApolloAlert(type: .sticker(style: .error),
                        title: Strings.Generic.title,
                        subtitle: Strings.Generic.message,
                        primaryButtonAction: primaryButtonAction,
                        linkButtonAction: linkButtonAction
        )
    }
    
    func displayCopyCodeSucceded() {
        UIView.animate(
            withDuration: 0.33,
            delay: 0,
            animations: {
                self.updateCopyCodeSuccess(show: true)
            },
            completion: { _ in
                UIView.animate(withDuration: Layout.Duration.animationTime, delay: Layout.Duration.delayAnimation, animations: {
                    self.updateCopyCodeSuccess(show: false)
                })
            }
        )
    }
    
    func displaySendByEmailSucceded(email: String) {
        let settings = LoanAlertSettings(
            imageAsset: Resources.Illustrations.iluEmailFeedback.image,
            title: Strings.Payment.BankSlip.SendEmail.Popup.title,
            subtitle: Strings.Payment.BankSlip.SendEmail.Popup.text(email),
            okButtonTitle: Strings.Payment.BankSlip.SendEmail.Popup.Button.text,
            hasCancel: false
        )
        LoanAlertHelper.shared.createAlert(with: settings, presentIn: self)
    }
    
    func startLoading() {
        startLoadingView()
    }
    
    func stopLoading() {
        stopLoadingView()
    }
}

@objc
extension BankSlipViewController {
    func didTapCopy() {
        copyCodeButton.isUserInteractionEnabled = false
        interactor.didTapCopy()
        Timer.scheduledTimer(timeInterval: Layout.Duration.intervalButtonDisabled,
                             target: self,
                             selector: #selector(enableUserInteraction),
                             userInfo: nil,
                             repeats: false)
    }
    
    func enableUserInteraction() {
        copyCodeButton.isUserInteractionEnabled = true
    }
    
    func didTapSendByEmail() {
        interactor.sendBankSlipByEmail()
    }
}
