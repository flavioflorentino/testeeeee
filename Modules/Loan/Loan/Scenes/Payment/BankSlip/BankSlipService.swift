import Core
import Foundation

typealias SendBankSlipByEmailCompletionBlock = (Result<SendBankSlipByEmailResponse, ApiError>) -> Void

protocol BankSlipServicing {
    func sendBankSlipByEmail(contractId: String, installmentIds: [String], completion: @escaping SendBankSlipByEmailCompletionBlock)
}

final class BankSlipService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension BankSlipService: BankSlipServicing {
    func sendBankSlipByEmail(contractId: String, installmentIds: [String], completion: @escaping SendBankSlipByEmailCompletionBlock) {
        let endpoint = PaymentFlowServiceEndpoint.sendBankSlipByEmail(contractId: contractId,
                                                                      installmentIds: installmentIds)
        let api = Api<SendBankSlipByEmailResponse>(endpoint: endpoint)

        api.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
