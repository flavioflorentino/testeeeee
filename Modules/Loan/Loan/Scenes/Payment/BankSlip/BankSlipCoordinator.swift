import UIKit

enum BankSlipAction {
    case back
}

protocol BankSlipCoordinating: AnyObject {
    var delegate: PaymentFlowCoordinating? { get set }
    
    func perform(action: BankSlipAction)
}

final class BankSlipCoordinator {
    weak var delegate: PaymentFlowCoordinating?
    weak var viewController: UIViewController?
    
    init(delegate: PaymentFlowCoordinating) {
        self.delegate = delegate
    }
}

extension BankSlipCoordinator: BankSlipCoordinating {
    func perform(action: BankSlipAction) {
        switch action {
        case .back:
            delegate?.perform(action: .back)
        }
    }
}
