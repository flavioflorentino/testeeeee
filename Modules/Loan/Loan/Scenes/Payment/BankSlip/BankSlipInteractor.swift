import Foundation
import AnalyticsModule

protocol BankSlipInteracting: AnyObject {
    func trackEnterView()
    func loadData()
    func didTapCopy()
    func sendBankSlipByEmail()
    func close()
}

final class BankSlipInteractor {
    private let service: BankSlipServicing
    private let presenter: BankSlipPresenting
    private var bankSlip: BankSlip
    private var installments: [Installment]
    private var contractId: String
    
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    init(service: BankSlipServicing, presenter: BankSlipPresenting, bankSlip: BankSlip, contractId: String, installments: [Installment], dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.bankSlip = bankSlip
        self.installments = installments
        self.contractId = contractId
        self.dependencies = dependencies
    }
}

extension BankSlipInteractor: BankSlipInteracting {
    func trackEnterView() {
        if installments.count == 1, let installment = installments.first {
            let event = LoanEvent.didEnter(
                to: .paymentEmail,
                from: .payment,
                params: ["type": installment.status.trackingValue]
            )
            dependencies.analytics.log(event)
        } 
    }
    
    func loadData() {
        presenter.displayBankSlipInfo(bankSlip: bankSlip)
    }
    
    func didTapCopy() {
        dependencies.analytics.log(LoanEvent.didCopy(to: .paymentEmail))
        
        UIPasteboard.general.string = bankSlip.code
        presenter.presentCopyCodeSucceded()
    }
    
    func sendBankSlipByEmail() {
        dependencies.analytics.log(LoanEvent.didSend(to: .paymentEmail))
        
        presenter.startLoading()
        
        service.sendBankSlipByEmail(contractId: contractId, installmentIds: installments.map(\.id)) { [weak self] response in
            self?.presenter.stopLoading()
            
            switch response {
            case .success(let response):
                self?.presenter.presentSendByEmailSucceded(email: response.destinationEmail)
            case .failure:
                self?.presenter.presentError()
            }
        }
    }
    
    func close() {
        presenter.close()
    }
}
