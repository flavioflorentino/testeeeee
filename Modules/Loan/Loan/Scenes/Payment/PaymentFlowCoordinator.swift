import UIKit
import UI

enum PaymentAction {
    case paymentMethod(contractId: String, installments: [Installment])
    case bankSlip(bankSlip: BankSlip, installments: [Installment], contractId: String)
    case back
}

protocol PaymentFlowCoordinating: Coordinating {
    func perform(action: PaymentAction)
}

final class PaymentFlowCoordinator {
    var childViewController: [UIViewController] = []
    var viewController: UIViewController?
    var navigationController: UINavigationController
    private weak var delegate: LoanFlowCoordinating?
    
    init(from navigationController: UINavigationController, delegate: LoanFlowCoordinating) {
        self.navigationController = navigationController
        self.delegate = delegate
    }
}

extension PaymentFlowCoordinator: PaymentFlowCoordinating {
    func perform(action: PaymentAction) {
        switch action {
        case .back:
            navigationController.popViewController(animated: true)
        default:
            handleFlow(action)
        }
    }
    
    private func handleFlow(_ action: PaymentAction) {
        let controller: UIViewController
        
        switch action {
        case let .paymentMethod(contractId, installments):
            controller = PaymentMethodFactory.make(contractId: contractId, installments: installments, delegate: self)
        case let .bankSlip(bankSlip, installments, contractId):
            controller = BankSlipFactory.make(bankSlip: bankSlip, installments: installments, contractId: contractId, delegate: self)
        default:
            return
        }
        
        navigationController.pushViewController(controller, animated: true)
    }
}
