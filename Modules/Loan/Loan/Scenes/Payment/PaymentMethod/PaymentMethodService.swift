import Core
import Foundation

typealias GetBankSlipCompletionBlock = (Result<BankSlip, ApiError>) -> Void

protocol PaymentMethodServicing {
    func getBankSlip(contractId: String, installmentIds: [String], completion: @escaping GetBankSlipCompletionBlock)
}

final class PaymentMethodService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension PaymentMethodService: PaymentMethodServicing {
    func getBankSlip(contractId: String, installmentIds: [String], completion: @escaping GetBankSlipCompletionBlock) {
        let api = Api<BankSlip>(endpoint: PaymentFlowServiceEndpoint.bankSlip(contractId: contractId, installmentIds: installmentIds))
        api.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
