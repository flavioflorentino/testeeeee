import Foundation
import UI
import UIKit
import AssetsKit

protocol PaymentMethodViewDelegate: AnyObject {
    func didSelect(_ method: PaymentMethod)
}

private extension PaymentMethodView.Layout {
    enum Size {
        static let icon = CGSize(width: 40, height: 40)
        static let arrow = CGSize(width: 24, height: 24)
    }
    
    enum Height {
        static let cardView = 80
    }
}

final class PaymentMethodView: UIView {
    fileprivate enum Layout {}
    
    enum Accessibility: String {
        case backgroundView
    }
    
    private lazy var tapGesture = UITapGestureRecognizer(target: self, action: #selector(selectPaymentMethod))
    
    private lazy var cardView: UIView = {
       let view = UIView()
        view.viewStyle(CardViewStyle())
        return view
    }()
    
    private lazy var contentStackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [titleLabel, descriptionLabel])
        stack.axis = .vertical
        stack.spacing = Spacing.base00
        stack.distribution = .fillProportionally
        return stack
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale400())
        return label
    }()
    
    private lazy var paymentMethodImage: UIImageView = {
        let imageview = UIImageView()
        imageview.contentMode = .scaleAspectFit
        return imageview
    }()
    
    private lazy var arrowImage: UIImageView = {
        let imageview = UIImageView()
        imageview.contentMode = .scaleAspectFit
        imageview.image = Resources.Icons.icoGreenRightArrow.image
        return imageview
    }()
    
    weak var delegate: PaymentMethodViewDelegate?
    
    var paymentMethod: PaymentMethod = .bankSlip {
        didSet {
            paymentMethodImage.image = paymentMethod.image
            titleLabel.text = paymentMethod.title
            descriptionLabel.text = paymentMethod.description
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        guard #available(iOS 13, *), traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) else {
            return
        }
        
        let bezierPath = UIBezierPath(roundedRect: cardView.bounds, cornerRadius: CornerRadius.medium)
        cardView
            .viewStyle(CardViewStyle(cgPath: bezierPath.cgPath))
            .with(\.backgroundColor, .groupedBackgroundSecondary())
    }
    
    init(delegate: PaymentMethodViewDelegate, frame: CGRect = .zero) {
        super.init(frame: frame)
        self.delegate = delegate
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

@objc
private extension PaymentMethodView {
    func selectPaymentMethod() {
        delegate?.didSelect(paymentMethod)
    }
}

extension PaymentMethodView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(cardView)
        cardView.addSubview(paymentMethodImage)
        cardView.addSubview(contentStackView)
        cardView.addSubview(arrowImage)
    }
    
    func setupConstraints() {
        cardView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.top.equalToSuperview().offset(Spacing.base00)
            $0.bottom.equalToSuperview().offset(-Spacing.base00)
            $0.height.equalTo(Layout.Height.cardView)
        }
        
        paymentMethodImage.snp.makeConstraints {
            $0.leading.equalTo(cardView).offset(Spacing.base02)
            $0.centerY.equalTo(cardView)
            $0.size.equalTo(Layout.Size.icon)
        }
        
        contentStackView.snp.makeConstraints {
            $0.centerY.equalTo(cardView)
            $0.leading.equalTo(paymentMethodImage.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalTo(arrowImage).offset(Spacing.base01)
        }
        
        arrowImage.snp.makeConstraints {
            $0.centerY.equalTo(cardView)
            $0.trailing.equalTo(cardView).offset(-Spacing.base03)
            $0.size.equalTo(Layout.Size.arrow)
        }
    }
    
    func configureViews() {
        backgroundColor = .clear
        accessibilityIdentifier = Accessibility.backgroundView.rawValue
        addGestureRecognizer(tapGesture)
    }
}
