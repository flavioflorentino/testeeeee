import Foundation
import UI
import UIKit

final class PaymentMethodInstallmentDetailsItemView: UIView {
    fileprivate enum Layout {}
    
    private lazy var stackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [titleLabel, valueLabel])
        stack.axis = .horizontal
        stack.alignment = .center
        stack.distribution = .fill
        return stack
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .left)
        return label
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .right)
        return label
    }()
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setItem(title: String, value: String) {
        titleLabel.text = title
        valueLabel.text = value
    }
}

extension PaymentMethodInstallmentDetailsItemView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(stackView)
    }
    
    func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}
