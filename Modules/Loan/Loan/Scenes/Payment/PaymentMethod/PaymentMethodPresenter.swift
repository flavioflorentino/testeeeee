import Foundation
import UI

protocol PaymentMethodPresenting: AnyObject {
    var viewController: PaymentMethodDisplaying? { get set }
    func presentSingleInstallmentInfo(installment: Installment)
    func presentMultipleInstallmentsInfo(installments: [Installment])
    func presentPaymentMethods(_ paymentMethods: [PaymentMethod])
    func startLoading()
    func stopLoading()
    func presentTimeExceededWarning()
    func presentError()
    func didNextStep(action: PaymentMethodAction)
}

final class PaymentMethodPresenter {
    private let coordinator: PaymentMethodCoordinating
    weak var viewController: PaymentMethodDisplaying?
    
    init(coordinator: PaymentMethodCoordinating) {
        self.coordinator = coordinator
    }
}

extension PaymentMethodPresenter: PaymentMethodPresenting {
    func presentMultipleInstallmentsInfo(installments: [Installment]) {
        let sumOfInstallmentsValues = installments.reduce(0.0) { $0 + $1.value }
        viewController?.addHeaderTitle(title: Strings.Payment.Header.installmentsValue)
        viewController?.displayValue(sumOfInstallmentsValues)
        viewController?.displayDueDate(formattedTodaysDate())
    }

    func presentSingleInstallmentInfo(installment: Installment) {
        let viewModel = PaymentMethodViewModel(installment: installment)
        viewController?.displayValue(viewModel.value)

        guard viewModel.isDetailsRequired else {
            viewController?.addHeaderTitle(title: Strings.Payment.Header.installmentValue)
            viewController?.displayDueDate(viewModel.dueDateDescription)
            return
        }
        
        viewController?.addHeaderTitle(title: Strings.Payment.Header.updatedValue)
        viewController?.addDetailsTitle()
        
        [
            (title: Strings.Payment.InstallmentDetails.originalDate, value: viewModel.originalDueDate),
            (title: Strings.Payment.InstallmentDetails.daysOverdue, value: viewModel.daysOverdueDescription),
            (title: Strings.Payment.InstallmentDetails.originalValue, value: viewModel.originalValueDescription),
            (title: Strings.Payment.InstallmentDetails.penaltyValue, value: viewModel.penaltyValueDescription)
        ]
        .forEach { row in
            viewController?.addDetailsRow(title: row.title, value: row.value)
        }
        
        viewController?.addDisclaimer()
    }
    
    func presentPaymentMethods(_ paymentMethods: [PaymentMethod]) {
        viewController?.displayPaymentMethods(paymentMethods)
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
    
    func presentTimeExceededWarning() {
        coordinator.perform(action: .timeExceeded)
    }
    
    func presentError() {
        viewController?.displayError()
    }
    
    func didNextStep(action: PaymentMethodAction) {
        coordinator.perform(action: action)
    }
}

extension PaymentMethodPresenter {
    private func formattedTodaysDate() -> NSAttributedString {
        let todaysDateFormatted = Date().ddDeMMMM()
        let todaysDateDescriptionRawValue = Strings.Payment.DueDate.text(todaysDateFormatted)
    
        return todaysDateDescriptionRawValue.attributedStringWith(
            normalFont: Typography.bodySecondary().font(),
            highlightFont: Typography.bodySecondary(.highlight).font(),
            normalColor: Colors.grayscale500.color,
            highlightColor: Colors.grayscale500.color,
            underline: false
        )
    }
}
