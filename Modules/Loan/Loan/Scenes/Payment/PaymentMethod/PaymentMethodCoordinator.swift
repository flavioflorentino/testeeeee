import UIKit

enum PaymentMethodAction: Equatable {
    case timeExceeded
    case bankSlip(bankSlip: BankSlip, installments: [Installment], contractId: String)
}

protocol PaymentMethodCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    
    func perform(action: PaymentMethodAction)
}

final class PaymentMethodCoordinator {
    private weak var delegate: PaymentFlowCoordinating?
    weak var viewController: UIViewController?
    
    init(delegate: PaymentFlowCoordinating) {
        self.delegate = delegate
    }
}

extension PaymentMethodCoordinator: PaymentMethodCoordinating {
    func perform(action: PaymentMethodAction) {
        switch action {
        case let .bankSlip(bankSlip, installments, contractId):
            delegate?.perform(action: .bankSlip(bankSlip: bankSlip, installments: installments, contractId: contractId))
        case .timeExceeded:
            let controller = TimeExceededFactory.make()
            viewController?.present(controller, animated: true)
        }
    }
}
