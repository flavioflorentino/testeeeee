import UI
import UIKit
import AssetsKit

protocol PaymentMethodDisplaying: AnyObject {
    func displayValue(_ value: Double)
    func displayDueDate(_ dueDate: NSAttributedString)
    func displayPaymentMethods(_ paymentMethods: [PaymentMethod])
    func addDetailsTitle()
    func addHeaderTitle(title: String)
    func addDetailsRow(title: String, value: String?)
    func addDisclaimer()
    func startLoading()
    func stopLoading()
    func displayError()
}

private extension PaymentMethodViewController.Layout {
    enum Height {
        static let separatorView = 1
    }
}

final class PaymentMethodViewController: ViewController<PaymentMethodInteracting, UIView>, LoadingViewProtocol {
    fileprivate enum Layout { }
    
    lazy var loadingView: LoadingView = {
        let loading = LoadingView()
        loading.text = Strings.Generic.Loading.text
        return loading
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.alwaysBounceVertical = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    private lazy var contentView = UIView()
    
    private lazy var headerTitleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, Colors.black.color)
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var headerStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = Spacing.base01
        stack.alignment = .center
        return stack
    }()
    
    private lazy var currencyView = UIView()
    
    private lazy var valueView: CurrencyField = {
        let value = CurrencyField()
        value.fontCurrency = Typography.title(.small).font()
        value.fontValue = Typography.currency(.large).font()
        value.textColor = Colors.neutral600.color
        value.spacing = Spacing.base00
        value.positionCurrency = .top
        value.isEnabled = false
        return value
    }()
    
    private lazy var dueDateLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle(type: .default))
            .with(\.textColor, Colors.grayscale500.color)
        label.isHidden = true
        return label
    }()
    
    private lazy var separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale100.color
        return view
    }()
    
    private lazy var stackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = Spacing.base01
        return stack
    }()
    
    private lazy var detailsTitleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, Colors.grayscale700.color)
        label.text = Strings.Payment.InstallmentDetails.title
        return label
    }()
    
    private lazy var disclaimerView: ApolloFeedbackCard = {
        let view = ApolloFeedbackCard(description: Strings.Payment.InstallmentDetails.disclaimer,
                                      iconType: .information,
                                      layoutType: .onlyText)
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.trackEnterView()
        interactor.loadData()
    }

    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(headerTitleLabel)
        contentView.addSubview(headerStackView)
        currencyView.addSubview(valueView)
        headerStackView.addArrangedSubview(currencyView)
        headerStackView.addArrangedSubview(dueDateLabel)
        contentView.addSubview(separatorView)
        contentView.addSubview(stackView)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        contentView.snp.makeConstraints {
            $0.edges.width.equalToSuperview()
            $0.height.equalTo(view.compatibleSafeArea.height).priority(.low)
        }
        
        headerTitleLabel.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalTo(contentView.snp.top).offset(Spacing.base04)
        }
        
        headerStackView.snp.makeConstraints {
            $0.top.equalTo(headerTitleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        valueView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        separatorView.snp.makeConstraints {
            $0.top.equalTo(headerStackView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Layout.Height.separatorView)
        }
        
        stackView.snp.makeConstraints {
            $0.top.equalTo(separatorView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.lessThanOrEqualToSuperview().inset(Spacing.base04)
        }
    }
    
    override func configureViews() {
        navigationController?.setupLoanAppearance()
        navigationItem.title = Strings.Payment.PaymentMethod.title
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}

extension PaymentMethodViewController {
    private func addCustomSpace(space: CGFloat, after view: UIView) {
        if #available(iOS 11, *) {
            stackView.setCustomSpacing(space, after: view)
        } else {
            stackView.setSpacing(space, after: view)
        }
    }
}

extension PaymentMethodViewController: PaymentMethodDisplaying {
    func displayValue(_ value: Double) {
        valueView.value = value
    }
    
    func displayDueDate(_ dueDate: NSAttributedString) {
        dueDateLabel.isHidden = false
        dueDateLabel.attributedText = dueDate
    }
    
    func displayPaymentMethods(_ paymentMethods: [PaymentMethod]) {
        paymentMethods.forEach {
            let view = PaymentMethodView(delegate: self)
            view.paymentMethod = $0
            stackView.addArrangedSubview(view)
        }
    }
    
    func addHeaderTitle(title: String) {
        headerTitleLabel.text = title
    }
    
    func addDetailsTitle() {
        stackView.addArrangedSubview(detailsTitleLabel)
        addCustomSpace(space: Spacing.base02, after: detailsTitleLabel)
    }
    
    func addDetailsRow(title: String, value: String?) {
        guard let value = value else { return }
        
        let itemView = PaymentMethodInstallmentDetailsItemView()
        itemView.setItem(title: title, value: value)
        stackView.addArrangedSubview(itemView)
    }
    
    func addDisclaimer() {
        guard let lastSubview = stackView.arrangedSubviews.last else { return }
        
        let separatorView = SeparatorView()
        
        stackView.addArrangedSubview(separatorView)
        stackView.addArrangedSubview(disclaimerView)
        
        addCustomSpace(space: Spacing.base02, after: lastSubview)
        addCustomSpace(space: Spacing.base03, after: separatorView)
        addCustomSpace(space: Spacing.base03, after: disclaimerView)
    }
    
    func startLoading() {
        startLoadingView()
    }
    
    func stopLoading() {
        stopLoadingView()
    }
    
    func displayError() {
        let primaryButtonAction = ApolloAlertAction(title: Strings.Generic.tryAgain) {
            self.interactor.didSelectPaymentMethod(.bankSlip)
        }
        
        let linkButtonAction = ApolloAlertAction(title: Strings.Generic.notNow) { }
        
        showApolloAlert(type: .sticker(style: .error),
                        title: Strings.Generic.title,
                        subtitle: Strings.Generic.message,
                        primaryButtonAction: primaryButtonAction,
                        linkButtonAction: linkButtonAction)
    }
}

extension PaymentMethodViewController: PaymentMethodViewDelegate {
    func didSelect(_ paymentMethod: PaymentMethod) {
        interactor.didSelectPaymentMethod(paymentMethod)
    }
}
