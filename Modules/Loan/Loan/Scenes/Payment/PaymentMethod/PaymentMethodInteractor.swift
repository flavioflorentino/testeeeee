import Foundation
import FeatureFlag
import AnalyticsModule

protocol PaymentMethodInteracting: AnyObject {
    func trackEnterView()
    func loadData()
    func didSelectPaymentMethod(_ paymentMethod: PaymentMethod)
}

final class PaymentMethodInteractor {
    typealias Dependencies = HasFeatureManager & HasAnalytics
    private let dependencies: Dependencies
    
    private let service: PaymentMethodServicing
    private let presenter: PaymentMethodPresenting
    private let installments: [Installment]
    private let contractId: String
    
    private var paymentMethods: [PaymentMethod] {
        var methods: [PaymentMethod] = []
        if dependencies.featureManager.isActive(.opsFeatureLoanPicpayPayment) {
            methods.append(.picpay)
        }
        if dependencies.featureManager.isActive(.opsFeatureLoanSlipPayment) {
            methods.append(.bankSlip)
        }
        return methods
    }
    
    init(service: PaymentMethodServicing,
         presenter: PaymentMethodPresenting,
         contractId: String,
         installments: [Installment],
         dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.contractId = contractId
        self.installments = installments
        self.dependencies = dependencies
    }
}

extension PaymentMethodInteractor: PaymentMethodInteracting {
    func trackEnterView() {
        if installments.count == 1, let installment = installments.first {
            let event = LoanEvent.didEnter(
                to: .payment,
                from: .installments,
                params: ["type": installment.status.trackingValue]
            )
            dependencies.analytics.log(event)
        }
    }
    
    func loadData() {
        if installments.count == 1, let installment = installments.first {
            presenter.presentSingleInstallmentInfo(installment: installment)
        } else {
            presenter.presentMultipleInstallmentsInfo(installments: installments)
        }
        presenter.presentPaymentMethods(paymentMethods)
    }
    
    func didSelectPaymentMethod(_ paymentMethod: PaymentMethod) {
        guard paymentMethod == .bankSlip else { return }
        requestBankSlipInfo()
    }
    
    func requestBankSlipInfo() {
        presenter.startLoading()

        service.getBankSlip(contractId: contractId, installmentIds: installments.map(\.id)) { [weak self] result in
            guard let self = self else { return }
            self.presenter.stopLoading()
            
            switch result {
            case .success(let bankSlip):
                self.presenter.didNextStep(action: .bankSlip(bankSlip: bankSlip, installments: self.installments, contractId: self.contractId))
            case .failure(let error):
                guard error.requestError?.code == LoanApiError.timeExceeded.rawValue else {
                    self.presenter.presentError()
                    return
                }
                
                self.presenter.presentTimeExceededWarning()
            }
        }
    }
}
