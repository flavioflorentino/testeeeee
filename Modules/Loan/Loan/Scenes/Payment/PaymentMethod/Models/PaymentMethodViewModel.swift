import Core
import UI
import UIKit

struct PaymentMethodViewModel {
    var value: Double
    var status: InstallmentStatus
    let isDetailsRequired: Bool
    
    private var dueDate: Date
    private var daysOverdue: Int?
    private var originalValue: Double
    private var penaltyValue: Double?
    
    var dueDateDescription: NSAttributedString {
        let dueDateFormatted = dueDate.ddDeMMMM()
        let dueDateDescriptionRawValue = Strings.Payment.DueDate.text(dueDateFormatted)
        
        return dueDateDescriptionRawValue.attributedStringWith(
            normalFont: Typography.bodySecondary().font(),
            highlightFont: Typography.bodySecondary(.highlight).font(),
            normalColor: Colors.grayscale500.color,
            highlightColor: Colors.grayscale500.color,
            underline: false
        )
    }
    
    var originalDueDate: String? {
        guard let daysOverdue = daysOverdue,
              let originalDueDate = Calendar.current.date(byAdding: .day,
                                                          value: daysOverdue,
                                                          to: dueDate)
        else { return nil }
        
        return Date.forthFormatter.string(from: originalDueDate)
    }
    
    var daysOverdueDescription: String? {
        guard let daysOverdue = daysOverdue else { return nil }
        return Strings.Payment.InstallmentDetails.DaysOverdue.description(abs(daysOverdue))
    }
    
    var originalValueDescription: String? {
        originalValue.toCurrencyString()
    }
    
    var penaltyValueDescription: String? {
        penaltyValue?.toCurrencyString()
    }
    
    init(installment: Installment) {
        value = installment.value
        status = installment.status
        dueDate = installment.dueDate
        daysOverdue = installment.daysOverDue
        originalValue = installment.originalValue
        penaltyValue = installment.penaltyValue
        isDetailsRequired = status == .overdue
    }
}
