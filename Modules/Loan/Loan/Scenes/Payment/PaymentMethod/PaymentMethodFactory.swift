import UIKit

enum PaymentMethodFactory {
    static func make(contractId: String, installments: [Installment], delegate: PaymentFlowCoordinating) -> PaymentMethodViewController {
        let dependencies = LoanDependencyContainer()
        let service: PaymentMethodServicing = PaymentMethodService(dependencies: dependencies)
        let coordinator: PaymentMethodCoordinating = PaymentMethodCoordinator(delegate: delegate)
        let presenter: PaymentMethodPresenting = PaymentMethodPresenter(coordinator: coordinator)
        let interactor = PaymentMethodInteractor(service: service, presenter: presenter, contractId: contractId, installments: installments, dependencies: dependencies)
        let viewController = PaymentMethodViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
