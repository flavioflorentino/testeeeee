import Core
import Foundation

enum PaymentFlowServiceEndpoint {
    case bankSlip(contractId: String, installmentIds: [String])
    case sendBankSlipByEmail(contractId: String, installmentIds: [String])
}

extension PaymentFlowServiceEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case let .bankSlip(contractId, installmentIds):
            return "personal-credit/customer-service/contracts/\(contractId)/installments/digitable-lines?\(formatQueryParams(installmentIds))"
        case let .sendBankSlipByEmail(contractId, installmentIds):
            return "personal-credit/customer-service/contracts/\(contractId)/installments/settlement-slips?\(formatQueryParams(installmentIds))"
        }
    }
    
    var method: HTTPMethod {
        .post
    }
}

extension PaymentFlowServiceEndpoint {
    private func formatQueryParams(_ ids: [String]) -> String {
        ids.map { "ids=\($0)" }.joined(separator: "&")
    }
}
