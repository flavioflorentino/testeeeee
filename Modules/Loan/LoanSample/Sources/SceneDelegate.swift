import UIKit
import Loan

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        if let windowScene = scene as? UIWindowScene {
            let window = UIWindow(windowScene: windowScene)
            let navigation = UINavigationController(rootViewController: HomeViewController())
            window.rootViewController = navigation
            self.window = window
            window.makeKeyAndVisible()
        }
    }
}
