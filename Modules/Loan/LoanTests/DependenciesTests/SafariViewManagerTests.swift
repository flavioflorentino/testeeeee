import SafariServices
import XCTest
import Foundation
@testable import Loan

private final class URLOpenerMock: URLOpener {
    var canOpenURL = false
    private(set) var url: URL?
    private(set) var canOpenURLCallCount = 0
    
    func canOpenURL(_ url: URL) -> Bool {
        self.url = url
        canOpenURLCallCount += 1
        return canOpenURL
    }
}

final class SafariViewManagerTests: XCTestCase {
    private let urlOpenerMock = URLOpenerMock()
    private lazy var sut = SafariViewManager.shared
 
    func testOpen_WhenURLIsValid_ShouldReturnSuccess() throws {
        urlOpenerMock.canOpenURL = true
        let expectedURL = try XCTUnwrap(URL(string: "https://picpay.com"))
        
        let result = sut.controller(for: expectedURL.absoluteString, urlOpener: urlOpenerMock)
        
        guard case .success = result else {
            XCTFail("Something went wrong")
            return
        }
        
        
        XCTAssertEqual(urlOpenerMock.canOpenURLCallCount, 1)
        XCTAssertEqual(urlOpenerMock.url, expectedURL)
    }
    
    func testOpen_WhenURLIsEmpty_ShouldReturnFailureEmptyUrl() {
        let result = sut.controller(for: "")
        
        guard case let .failure(error) = result else {
            XCTFail("Something went wrong")
            return
        }
        
        XCTAssertEqual(error, SafariError.emptyUrl)
    }
    
    func testOpen_WhenURLIsNil_ShouldReturnFailureNilUrl() {
        let result = sut.controller(for: nil)
        
        guard case let .failure(error) = result else {
            XCTFail("Something went wrong")
            return
        }
        
        XCTAssertEqual(error, SafariError.nilUrl)
    }
    
    func testOpen_WhenURLIsInvalid_ShouldReturnFailureCouldNotOpen() throws {
        let invalidUrl = try XCTUnwrap(URL(string: "http"))
        let result = sut.controller(for: invalidUrl.absoluteString, urlOpener: urlOpenerMock)
        
        guard case let .failure(error) = result else {
            XCTFail("Something went wrong")
            return
        }
        
        guard case let .couldNotOpen(url) = error else {
            XCTFail("Something went wrong")
            return
        }
        
        XCTAssertEqual(url, invalidUrl.absoluteString)
        XCTAssertEqual(error, SafariError.couldNotOpen(invalidUrl.absoluteString))
        XCTAssertEqual(urlOpenerMock.canOpenURLCallCount, 1)
        XCTAssertEqual(urlOpenerMock.url, invalidUrl)
    }
}
