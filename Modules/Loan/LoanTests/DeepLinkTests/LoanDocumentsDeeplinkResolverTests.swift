import Foundation
import Core
@testable import Loan
import XCTest

final class MyLoanDocumentsDeeplinkResolverTests: XCTestCase {
    private var navigationSpy = UINavigationControllerSpy()
    private lazy var dependencies = LoanSetupMock(.navigationInstance(navigation: navigationSpy))
    private lazy var sut = MyLoanDocumentsDeeplinkResolver(dependencies: LoanDependencyContainerMock(dependencies))

    func testCanHandleUrl_WhenIsValidContractsUrlAndUserIsAuthenticated_ShouldReturnHandleable() throws {
        let deeplink = "picpay://picpay/personal-credit/xp/aftersales/contracts"
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.canHandle(url: url, isAuthenticated: true)

        XCTAssertEqual(result, .handleable)
    }

    func testCanHandleUrl_WhenIsValidContractsUrlAndUserIsNotAuthenticated_ShouldReturnOnlyWithAuth() throws {
        let deeplink = "picpay://picpay/personal-credit/xp/aftersales/contracts"
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.canHandle(url: url, isAuthenticated: false)

        XCTAssertEqual(result, .onlyWithAuth)
    }

    func testCanHandleUrl_WhenIsInvalidContractsUrlAndUserIsAuthenticated_ShouldReturnNotHandleable() throws {
        let deeplink = "picpay://picpay/personal-credit/xp"
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.canHandle(url: url, isAuthenticated: true)

        XCTAssertEqual(result, .notHandleable)
    }

    func testOpenUrl_WhenIsHandleableUrl_ShouldPushMyLoanDocumentsViewController() throws {
        let deeplink = "picpay://picpay/personal-credit/xp/aftersales/contracts"

        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.open(url: url, isAuthenticated: true)

        XCTAssertTrue(result)
        XCTAssertNotNil(navigationSpy.pushedViewController as? MyLoanDocumentsViewController)
    }
}
