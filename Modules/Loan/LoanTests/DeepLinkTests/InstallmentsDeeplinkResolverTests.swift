import Foundation
import Core
@testable import Loan
import XCTest

final class InstallmentsDeeplinkResolverTests: XCTestCase {
    private var navigationSpy = UINavigationControllerSpy()
    private lazy var dependencies = LoanSetupMock(.navigationInstance(navigation: navigationSpy))
    private lazy var sut = InstallmentsDeeplinkResolver(dependencies: LoanDependencyContainerMock(dependencies))

    func testCanHandleUrl_WhenIsValidUrlAndUserIsAuthenticated_ShouldReturnHandleable() throws {
        let deeplink = "picpay://picpay/personal-credit/aftersales/contracts/CD1234567890/installments?type=close_to_expire&days=2&origin=push"
        
        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.canHandle(url: url, isAuthenticated: true)

        XCTAssertEqual(result, .handleable)
    }

    func testCanHandleUrl_WhenIsValidUrlAndUserIsNotAuthenticated_ShouldReturnOnlyWithAuth() throws {
        let deeplink = "picpay://picpay/personal-credit/aftersales/contracts/CD1234567890/installments?type=close_to_expire&days=2&origin=push"

        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.canHandle(url: url, isAuthenticated: false)

        XCTAssertEqual(result, .onlyWithAuth)
    }

    func testCanHandleUrl_WhenIsInvalidUrlAndUserIsAuthenticated_ShouldReturnNotHandleable() throws {
        let deeplink = "picpay://picpay/aftersales/"

        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.canHandle(url: url, isAuthenticated: true)

        XCTAssertEqual(result, .notHandleable)
    }

    func testOpenUrl_WhenIsHandleableUrl_ShouldPushInstallmentsViewController() throws {
        let deeplink = "picpay://picpay/personal-credit/aftersales/contracts/CD1234567890/installments?type=close_to_expire&days=2&origin=push"

        let url = try XCTUnwrap(URL(string: deeplink))

        let result = sut.open(url: url, isAuthenticated: true)

        XCTAssertTrue(result)
        XCTAssertNotNil(navigationSpy.pushedViewController as? InstallmentsViewController)
    }
}
