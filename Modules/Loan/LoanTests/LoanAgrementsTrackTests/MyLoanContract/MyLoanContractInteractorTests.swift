import AnalyticsModule
import SafariServices
import Core
import XCTest
@testable import Loan

private final class MyLoanContractServiceSpy: MyLoanContractServicing {
    let contractLink = LoanContractPDFResponse(url: "https://pdf")
    
    lazy var response: Result<LoanContractPDFResponse, ApiError> = .success(contractLink)
    
    func fetchContractPDF(with contractID: String, completion: @escaping (Result<LoanContractPDFResponse, ApiError>) -> Void) {
        completion(response)
    }
}

private final class MyLoanContractPresenterSpy: MyLoanContractPresenting {
    var viewController: MyLoanContractDisplaying?
    
    private(set) var popCallCount = 0
    private(set) var presentErrorWhenOpenCallCount = 0
    private(set) var presentErrorWhenShareCallCount = 0
    private(set) var presentFilledFieldsCallCount = 0
    private(set) var openShareCallCount = 0
    private(set) var openSafariCallCount = 0
    
    private(set) var document: LoanAgreementDocument?
    private(set) var url: URL?
    private(set) var urlString: String?
        
    func pop() {
        popCallCount += 1
    }
    
    func presentErrorWhenOpen() {
        presentErrorWhenOpenCallCount += 1
    }
    
    func presentErrorWhenShare() {
        presentErrorWhenShareCallCount += 1
    }
    
    func presentFilledFields(document: LoanAgreementDocument) {
        presentFilledFieldsCallCount += 1
        self.document = document
    }
    
    func openSafari(with urlString: String) {
        openSafariCallCount += 1
        self.urlString = urlString
    }
    
    func openShare(withURL: URL) {
        openShareCallCount += 1
        url = withURL
    }
}

final class MyLoanContractDownloadMock: MyLoanContractDownloading {
    private (set) var savePDFCallCount = 0
    private (set) var urlString: String?
    
    var url: URL?
    
    func savePDFInDevice(urlString: String, completion: @escaping (URL?) -> Void) {
        savePDFCallCount += 1
        self.urlString = urlString
        completion(url)
    } 
}

final class MyLoanContractInteractorTests: XCTestCase {
    private let serviceSpy = MyLoanContractServiceSpy()
    private let presenterSpy = MyLoanContractPresenterSpy()
    private let downloadMock = MyLoanContractDownloadMock()
    private let dependenciesSpy = LoanDependencyContainerMock(AnalyticsSpy(),
                                                              SafariViewManagerMock())
    private let dumbDocument = LoanAgreementDocument(bankId: "1a1a1a",
                                                 contractEnd: "02/02/2020",
                                                 id: "1b1b1b",
                                                 documentSent: true,
                                                 loanGoal: LoanGoal(category: .business,
                                                                    description: ""),
                                                 status: .active,
                                                 totalInstallments: 0,
                                                 totalInstallmentsAlreadyPaid: 0)

    private lazy var sut = MyLoanContractInteractor(with: dumbDocument,
                                                    service: serviceSpy,
                                                    presenter: presenterSpy,
                                                    dependencies: dependenciesSpy,
                                                    download: downloadMock)

    func testOpenContractPDF_WhenHasNotValidUrlStringFromService_ShouldPresentError() {
        serviceSpy.response = .failure(.serverError)
        sut.openContractPDF()
        
        XCTAssertEqual(presenterSpy.presentErrorWhenOpenCallCount, 1)
    }
    
    func testOpenContractPDF_WhenHasValidUrlStringFromService_ShouldOpenSafari() {
        sut.fetch()
        sut.openContractPDF()
        
        XCTAssertEqual(presenterSpy.openSafariCallCount, 1)
    }
    
    func testShareContractPDF_WhenHasNotValidUrlStringFromService_ShouldPresentError() throws {
        serviceSpy.response = .failure(.serverError)
        sut.shareContractPDF()

        XCTAssertEqual(presenterSpy.presentErrorWhenShareCallCount, 1)
    }
     
    func testShareContractPDF_WhenHasValidUrlStringFromServiceButIsNotPossibleToSavePDFOnDevice_ShouldReturn() throws {
        sut.fetch()
        sut.shareContractPDF()
        downloadMock.url = try XCTUnwrap(URL(string: "pdf"))

        XCTAssertEqual(presenterSpy.presentErrorWhenShareCallCount, 1)
        XCTAssertEqual(downloadMock.savePDFCallCount, 1)
    }
    
    func testPopViewController_WhenCalledByViewController_ShouldCallCoordinator() {
        sut.pop()

        XCTAssertEqual(presenterSpy.popCallCount, 1)
    }

    func testShareContractPDF_WhenHasValidUrlStringFromServiceAndIsPossibleToSavePDFOnDevice_ShouldOpenShare() throws {
        sut.fetch()
        downloadMock.url = try XCTUnwrap(URL(string: "https://pdf"))
        sut.shareContractPDF()

        XCTAssertEqual(presenterSpy.openShareCallCount, 1)
        XCTAssertEqual(downloadMock.savePDFCallCount, 1)
    }

    func testFillFields_WhenViewDidLoad_ShouldPresentFilledFields() {
        sut.fillFields()
        
        XCTAssertEqual(presenterSpy.presentFilledFieldsCallCount, 1)
    }
}
