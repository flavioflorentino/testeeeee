import XCTest
import Core
import SafariServices
@testable import Loan

final private class MyLoanContractCoordinatorSpy: MyLoanContractCoordinating {
    private(set) var performActionCallCount = 0
    private(set) var performedAction: MyLoanContractAction?
       
    func perform(action: MyLoanContractAction) {
        performActionCallCount += 1
        performedAction = action
    }
}

final private class MyLoanContractControllerSpy: MyLoanContractDisplaying {
    private(set) var displayFilledFieldsCallCount = 0
    private(set) var displayErrorWhenShareCallCount = 0
    private(set) var displayErrorWhenOpenCount = 0
    private(set) var showActivityShareCallCount = 0
    
    private(set) var document: LoanAgreementDocument?
    private(set) var url: URL?
    
    func displayFilledFields(document: LoanAgreementDocument) {
        displayFilledFieldsCallCount += 1
        self.document = document
    }
    
    func displayErrorWhenShare() {
        displayErrorWhenShareCallCount += 1
    }
    
    func displayErrorWhenOpen() {
        displayErrorWhenOpenCount += 1
    }
    
    func showActivityShare(url: URL) {
        showActivityShareCallCount += 1
        self.url = url
    }
}

final class MyLoanContractPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = MyLoanContractCoordinatorSpy()
    private let controllerSpy = MyLoanContractControllerSpy()

    private lazy var sut: MyLoanContractPresenter = {
        let sut = MyLoanContractPresenter(coordinator: coordinatorSpy)
        sut.viewController = controllerSpy
        return sut
    }()
    
    func testPresentErrorWhenOpen_WhenOpenPDF_ShouldDisplayErrorWhenOpen() {
        sut.presentErrorWhenOpen()
        
        XCTAssertEqual(controllerSpy.displayErrorWhenOpenCount, 1)
    }
    
    func testPresentErrorWhenShare_WhenSharePDF_ShouldDisplayErrorWhenShare() {
        sut.presentErrorWhenShare()
        
        XCTAssertEqual(controllerSpy.displayErrorWhenShareCallCount, 1)
    }
    
    func testOpen_WhenCalledByPresenter_ShouldOpenSafariViewController() throws {
        let urlString = "https://picpay.com"
        sut.openSafari(with: urlString)
        
       
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .openSafari(url: urlString))
    }
    
    func testOpenShare_WhenCalledByPresenter_ShouldShowAcitivyShare() throws {
        let dumbUrl = try XCTUnwrap(URL(string: "https://picpay.com"))
        sut.openShare(withURL: dumbUrl)
        
        XCTAssertEqual(controllerSpy.showActivityShareCallCount, 1)
    }
    
    func testPresentFilledFields_WhenViewDidLoad_ShouldDisplayFilledFields() {
        let dumbDocument = LoanAgreementDocument(bankId: "1a1a1a",
                                             contractEnd: "02/02/2020",
                                             id: "1b1b1b",
                                             documentSent: true,
                                             loanGoal: LoanGoal(category: .business,
                                                                description: ""),
                                             status: .active,
                                             totalInstallments: 0,
                                             totalInstallmentsAlreadyPaid: 0)
        sut.presentFilledFields(document: dumbDocument)
        
        XCTAssertEqual(controllerSpy.displayFilledFieldsCallCount, 1)
        XCTAssertEqual(controllerSpy.document, dumbDocument)
    }
    
    func testPop_ShouldPerformPopAction() {
        sut.pop()

        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .pop)
    }
}
