import XCTest
@testable import Loan

final class LoanAgreementsTrackCoordinatorTests: XCTestCase {
    private let navigationSpy = UINavigationControllerSpy()
    private let delegateSpy = LoanFlowCoordinatingSpy()
    private lazy var sut = LoanAgreementsTrackCoordinator(from: navigationSpy, delegate: delegateSpy)
       
    func testPerformAction_WhenActionIsMyLoanDocuments_ShouldStartMyLoanDocumentsView() {
        sut.perform(action: .myLoanDocuments)
        
        XCTAssertEqual(navigationSpy.pushCallCount, 1)
        XCTAssert(navigationSpy.pushedViewController is MyLoanDocumentsDisplaying)
    }
    
    func testPerformActionClose_ShouldClose() {
        sut.perform(action: .close)
        
        XCTAssertEqual(delegateSpy.closeCallCount, 1)
    }
}
