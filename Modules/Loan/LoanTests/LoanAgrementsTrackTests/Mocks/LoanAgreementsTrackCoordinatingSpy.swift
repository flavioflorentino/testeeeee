import UIKit
@testable import Loan

final class LoanAgreementsTrackCoordinatingSpy: LoanAgreementsTrackCoordinating {

    var childViewController: [UIViewController] = []
    var viewController: UIViewController?
    
    private(set) var performActionCallCount = 0
    private(set) var performedAction: LoanAgreementsTrackAction?
    
    func perform(action: LoanAgreementsTrackAction) {
        performActionCallCount += 1
        performedAction = action
    }
}
