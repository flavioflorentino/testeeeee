import AnalyticsModule
import Core
import XCTest
@testable import Loan

private final class MyLoanDocumentsServiceSpy: MyLoanDocumentsServicing {
    lazy var response: Result<LoanAgreementDocumentsResponse, ApiError> = .failure(.serverError)
    
    func getLoanAgreementsToTrack(completion: @escaping (Result<LoanAgreementDocumentsResponse, ApiError>) -> Void) {
        completion(response)
    }
}

private final class MyLoanDocumentsPresenterSpy: MyLoanDocumentsPresenting {
    var viewController: MyLoanDocumentsDisplaying?
    
    private(set) var presentLoanDocumentsCallCount = 0
    private(set) var didNextStepCallCount = 0
    private(set) var startLoadingCallCount = 0
    private(set) var stopLoadingCallCount = 0
    private(set) var presentErrorCallCount = 0
    private(set) var presentLoanInfoViewCallCount = 0
    private(set) var closeCallCount = 0
    private(set) var performedAction: MyLoanDocumentsAction?
    private(set) var presentConectionErrorCallCount = 0
    private(set) var presentFeedbackDialogCallCount = 0
    private(set) var presentOpenFAQCallCount = 0
    
    func didNextStep(action: MyLoanDocumentsAction) {
        didNextStepCallCount += 1
        performedAction = action
    }
    
    func present(loans: [LoanAgreementDocument]) {
        presentLoanDocumentsCallCount += 1
    }
    
    func startLoading() {
        startLoadingCallCount += 1
    }
    
    func stopLoading() {
        stopLoadingCallCount += 1
    }
    
    func presentError() {
        presentErrorCallCount += 1
    }
    
    func close() {
        closeCallCount += 1
    }
    
    func presentConectionError() {
        presentConectionErrorCallCount += 1
    }
    
    func presentLoanInfoView() {
        presentLoanInfoViewCallCount += 1
    }
    
    func presentFeedbackDialog() {
        presentFeedbackDialogCallCount += 1
    }
    
    func openFAQ(article: FAQArticle) {
        presentOpenFAQCallCount += 1
    }
}

final class MyLoanDocumentsInteractorTests: XCTestCase {
    private let serviceSpy = MyLoanDocumentsServiceSpy()
    private let presenterSpy = MyLoanDocumentsPresenterSpy()
    private let dependenciesSpy = LoanDependencyContainerMock(AnalyticsSpy())
    
    private let documentWithDocumentSentTrue = LoanAgreementDocument(bankId: "1",
                                     contractEnd: "01/02/2020",
                                     id: "1a1a1a",
                                     documentSent: true,
                                     loanGoal:
                                        LoanGoal(category: .build, description: ""),
                                     status: .active,
                                     totalInstallments: 5,
                                     totalInstallmentsAlreadyPaid: 2)
        
    
    private let documentWithDocumentSentFalse = LoanAgreementDocument(bankId: "12",
                                             contractEnd: "02/02/2020",
                                             id: "1b1b1b",
                                             documentSent: false,
                                             loanGoal:
                                                LoanGoal(category: .build,
                                                         description: ""),
                                             status: .active,
                                             totalInstallments: 5,
                                             totalInstallmentsAlreadyPaid: 2)
    
    private lazy var sut = MyLoanDocumentsInteractor(service: serviceSpy, presenter: presenterSpy, dependencies: dependenciesSpy)
    
    func testFetchLoanDocuments_WhenFetchSucceeds_ShouldPresentLoanDocuments() {
        serviceSpy.response = .success(LoanAgreementDocumentsResponse(loans: [documentWithDocumentSentTrue]))
        sut.fetch()
        
        XCTAssertEqual(presenterSpy.presentLoanDocumentsCallCount, 1)
    }
    
    func testFetchAgreementDocuments_WhenFetchError_ShouldPresentError() {
        sut.fetch()

        XCTAssertEqual(presenterSpy.presentErrorCallCount, 1)
    }
    
    func testFetchAgreementDocuments_WhenFetchConectionError_ShouldPresentConectionError() {
        serviceSpy.response = .failure(.connectionFailure)
        sut.fetch()
    
        XCTAssertEqual(self.presenterSpy.presentConectionErrorCallCount, 1)
    }
    
    func testClose_WhenCalledByViewController_ShouldCallCoordinator() {
        sut.close()
        
        XCTAssertEqual(presenterSpy.closeCallCount, 1)
    }
    
    func testSelectedLoanDocument_WhenDocumentSentIsTrue_ShouldGoToNextStep() {
        serviceSpy.response = .success(LoanAgreementDocumentsResponse(loans: [documentWithDocumentSentTrue]))
        sut.fetch()
        sut.didTapRow(index: IndexPath(row: 0, section: 0))

        XCTAssertEqual(presenterSpy.didNextStepCallCount, 1)
        XCTAssertEqual(presenterSpy.performedAction, .myLoanContract(document:documentWithDocumentSentTrue))
    }
    
    func testSelectedLoanDocument_WhenDocumentSentIsFalse_ShouldPresentFeedbackDialog() {
        serviceSpy.response = .success(LoanAgreementDocumentsResponse(loans: [documentWithDocumentSentFalse]))
        
        sut.fetch()
        sut.didTapRow(index: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(presenterSpy.presentFeedbackDialogCallCount, 1)
    }
    
    func testLoanInfoViewIfNeeded_WhenDocumentSentIsTrue_ShouldPresentLoanInfoView() {
        serviceSpy.response = .success(LoanAgreementDocumentsResponse(loans: [documentWithDocumentSentFalse]))
        sut.fetch()
    
        XCTAssertEqual(presenterSpy.presentLoanInfoViewCallCount, 1)
    }
    
    func tesOpenFaq_ShouldPerform() {
        sut.openFAQ()
        
        XCTAssertEqual(presenterSpy.presentOpenFAQCallCount, 1)
    }
}
