import XCTest
import Core
@testable import Loan

final private class MyLoanDocumentsControllerSpy: MyLoanDocumentsDisplaying {
    private(set) var displayItemsCallCount = 0
    private(set) var displayedItemsCount = 0
    private(set) var displayedLoanInfoViewCount = 0
    private(set) var displayErrorCallCount = 0
    private(set) var startLoadingCallCount = 0
    private(set) var stopLoadingCallCount = 0
    private(set) var displayConectionErrorCallCount = 0
    private(set) var displayFeedbackDialogCallCount = 0

    func displayItems(data: [MyLoanDocumentViewModel]) {
        displayItemsCallCount += 1
        displayedItemsCount = data.count
    }
    
    func displayLoanInfoView() {
        displayedLoanInfoViewCount += 1
    }
    
    func displayError() {
        displayErrorCallCount += 1
    }

    func startLoading() {
        startLoadingCallCount += 1
    }

    func stopLoading() {
        stopLoadingCallCount += 1
    }
    
    func displayConectionError() {
        displayConectionErrorCallCount += 1
    }
    
    func displayFeedbackDialog() {
        displayFeedbackDialogCallCount += 1
    }
}

final private class MyLoanDocumentsCoordinatorSpy: MyLoanDocumentsCoordinating {

    private(set) var performActionCallCount = 0
    private(set) var performedAction: MyLoanDocumentsAction?

    func perform(action: MyLoanDocumentsAction) {
        performActionCallCount += 1
        performedAction = action
    }
}

final class MyLoanDocumentsPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = MyLoanDocumentsCoordinatorSpy()
    private let controllerSpy = MyLoanDocumentsControllerSpy()
    
    private let dumbDocument = LoanAgreementDocument(bankId: "12",
                                             contractEnd: "02/02/2020",
                                             id: "1b1b1b",
                                             documentSent: true,
                                             loanGoal:
                                                LoanGoal(category: .business,
                                                         description: ""),
                                             status: .active,
                                             totalInstallments: 2,
                                             totalInstallmentsAlreadyPaid: 4)

    private lazy var sut: MyLoanDocumentsPresenter = {
        let sut = MyLoanDocumentsPresenter(coordinator: coordinatorSpy)
        sut.viewController = controllerSpy
        return sut
    }()

    func testPresent_ShouldDisplayItems() {
        sut.present(loans: [dumbDocument])

        XCTAssertEqual(controllerSpy.displayItemsCallCount, 1)
        XCTAssertEqual(controllerSpy.displayedItemsCount, 1)
    }

    func testDidNextStep_ShouldPerformAction() {
        sut.didNextStep(action: .myLoanContract(document: dumbDocument))

        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .myLoanContract(document: dumbDocument))
    }
    
    func testPresentFeedbackDialog_ShouldDisplayFeedbackDialog() {
        sut.presentFeedbackDialog()
        
        XCTAssertEqual(controllerSpy.displayFeedbackDialogCallCount, 1)
    }

    func testClose_ShouldPerformCloseAction() {
        sut.close()

        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .pop)
    }

    func testStartLoading_ShouldStartLoading() {
        sut.startLoading()

        XCTAssertEqual(controllerSpy.startLoadingCallCount, 1)
        XCTAssertEqual(controllerSpy.stopLoadingCallCount, 0)
    }

    func testStopLoading_ShouldStopLoading() {
        sut.stopLoading()

        XCTAssertEqual(controllerSpy.startLoadingCallCount, 0)
        XCTAssertEqual(controllerSpy.stopLoadingCallCount, 1)
    }

    func testPresentError_ShouldDisplayError() {
        sut.presentError()
        
        XCTAssertEqual(controllerSpy.displayErrorCallCount, 1)
    }
    
    func testPresentConectionError_ShouldDisplayConectionError() {
        sut.presentConectionError()

        XCTAssertEqual(controllerSpy.displayConectionErrorCallCount, 1)
    }
    
    func testPresentInfoLoadView_ShouldDisplayInfoLoadView() {
        sut.presentLoanInfoView()

        XCTAssertEqual(controllerSpy.displayedLoanInfoViewCount, 1)
    }
}
