import FeatureFlag
import XCTest
@testable import Core
@testable import Loan

private final class PaymentMethodServiceSpy: PaymentMethodServicing {
    var bankSlipResponse: Result<BankSlip, ApiError> = .failure(.serverError)
    var bankSlipEmailResponse: Result<SendBankSlipByEmailResponse, ApiError> = .failure(.serverError)

    func getBankSlip(contractId: String, installmentIds: [String], completion: @escaping GetBankSlipCompletionBlock) {
        completion(bankSlipResponse)
    }
}

private final class PaymentMethodPresenterSpy: PaymentMethodPresenting {
    var viewController: PaymentMethodDisplaying?
    
    private(set) var presentInstallmentInfoCallCount = 0
    private(set) var presentMultipleInstallmentsInfoCallCount = 0
    private(set) var presentPaymentMethodsCallCount = 0
    private(set) var didNextStepCallCount = 0
    private(set) var startLoadingCallCount = 0
    private(set) var stopLoadingCallCount = 0
    private(set) var presentTimeExceededWarningCallCount = 0
    private(set) var presentErrorCallCount = 0
    private(set) var performedAction: PaymentMethodAction?
    private(set) var presentedPaymentMethods: [PaymentMethod] = []
    
    func presentSingleInstallmentInfo(installment: Installment) {
        presentInstallmentInfoCallCount += 1
    }

    func presentMultipleInstallmentsInfo(installments: [Installment]) {
        presentMultipleInstallmentsInfoCallCount += 1
    }
    
    func presentPaymentMethods(_ paymentMethods: [PaymentMethod]) {
        presentPaymentMethodsCallCount += 1
        presentedPaymentMethods = paymentMethods
    }
    
    func startLoading() {
        startLoadingCallCount += 1
    }
    
    func stopLoading() {
        stopLoadingCallCount += 1
    }
    
    func presentTimeExceededWarning() {
        presentTimeExceededWarningCallCount += 1
    }
    
    func presentError() {
        presentErrorCallCount += 1
    }
    
    func didNextStep(action: PaymentMethodAction) {
        didNextStepCallCount += 1
        performedAction = action
    }
}

final class PaymentMethodInteractorTests: XCTestCase {
    private let serviceSpy = PaymentMethodServiceSpy()
    private let presenterSpy = PaymentMethodPresenterSpy()
    private lazy var dependenciesSpy = LoanDependencyContainerMock(featureManagerMock)
    private lazy var sut = PaymentMethodInteractor(
        service: serviceSpy,
        presenter: presenterSpy,
        contractId: "11111",
        installments: [newInstallment()],
        dependencies: dependenciesSpy
    )
    
    private let featureManagerMock: FeatureManagerMock = {
        let featureManagerMock = FeatureManagerMock()
        featureManagerMock.override(keys: .opsFeatureLoanPicpayPayment, with: false)
        featureManagerMock.override(keys: .opsFeatureLoanSlipPayment, with: false)
        return featureManagerMock
    }()
    
    func testLoadData_WhenHasSingleInstallment_ShouldPresentInstallmentInfo() {
        sut.loadData()
        
        XCTAssertEqual(presenterSpy.presentInstallmentInfoCallCount, 1)
        XCTAssertEqual(presenterSpy.presentMultipleInstallmentsInfoCallCount, 0)
    }

    func testLoadData_WhenHasMultipleInstallment_ShouldPresentInstallmentsInfo() {
        sut = PaymentMethodInteractor(
            service: serviceSpy,
            presenter: presenterSpy,
            contractId: "11111",
            installments: [newInstallment(), newInstallment()],
            dependencies: dependenciesSpy
        )
        sut.loadData()

        XCTAssertEqual(presenterSpy.presentMultipleInstallmentsInfoCallCount, 1)
        XCTAssertEqual(presenterSpy.presentInstallmentInfoCallCount, 0)
    }
    
    func testLoadData_ShouldPresentPaymentMethod() {
        sut.loadData()
        
        XCTAssertEqual(presenterSpy.presentPaymentMethodsCallCount, 1)
    }
    
    func testDidTapBankSlip_WhenLoadBankSlipSucceeded_ShouldLoadBankSlipInfo() {
        let bankSplip = BankSlip(code: "12345", value: "R$22,99", dueDate: Date())
        
        serviceSpy.bankSlipResponse = .success(bankSplip)
        
        sut.didSelectPaymentMethod(.bankSlip)

        XCTAssertEqual(presenterSpy.startLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepCallCount, 1)
        XCTAssertEqual(presenterSpy.presentTimeExceededWarningCallCount, 0)
        XCTAssertEqual(presenterSpy.presentErrorCallCount, 0)
    }
    
    func testDidTapBankSlip_WhenLoadBankSlipTimeExceededError_ShouldPresentTimeExceededWarning() {
        serviceSpy.bankSlipResponse = .failure(
            .otherErrors(
                body: RequestError(
                    title: "Error",
                    message: "Error",
                    code: LoanApiError.timeExceeded.rawValue
                )
            )
        )
        
        sut.didSelectPaymentMethod(.bankSlip)

        XCTAssertEqual(presenterSpy.startLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepCallCount, 0)
        XCTAssertEqual(presenterSpy.presentTimeExceededWarningCallCount, 1)
        XCTAssertEqual(presenterSpy.presentErrorCallCount, 0)
    }
    
    func testDidTapBankSlip_WhenLoadBankSlipGenericError_ShouldPresentGenericError() {
        serviceSpy.bankSlipResponse = .failure(.serverError)
        
        sut.didSelectPaymentMethod(.bankSlip)

        XCTAssertEqual(presenterSpy.startLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepCallCount, 0)
        XCTAssertEqual(presenterSpy.presentTimeExceededWarningCallCount, 0)
        XCTAssertEqual(presenterSpy.presentErrorCallCount, 1)
    }
    
    func testLoadData_WhenLoanPicpayPaymentFeatureFlagIsOn_ShouldDisplayPicpayPaymentOption() {
        featureManagerMock.override(key: .opsFeatureLoanPicpayPayment, with: true)
        sut.loadData()
        
        XCTAssertTrue(presenterSpy.presentedPaymentMethods.contains(.picpay))
    }
    
    func testLoadData_WhenLoanPicpayPaymentFeatureFlagIsOff_ShouldNotDisplayPicpayPaymentOption() {
        featureManagerMock.override(key: .opsFeatureLoanPicpayPayment, with: false)
        sut.loadData()
        
        XCTAssertFalse(presenterSpy.presentedPaymentMethods.contains(.picpay))
    }
    
    func testLoadData_WhenLoanBankSlipPaymentFeatureFlagIsOn_ShouldDisplayBankSlipOption() {
        featureManagerMock.override(key: .opsFeatureLoanSlipPayment, with: true)
        sut.loadData()
        
        XCTAssertTrue(presenterSpy.presentedPaymentMethods.contains(.bankSlip))
    }
    
    func testLoadData_WhenLoanBankSlipPaymentFeatureFlagIsOff_ShouldNotDisplayBankSlipOption() {
        featureManagerMock.override(key: .opsFeatureLoanSlipPayment, with: false)
        sut.loadData()
        
        XCTAssertFalse(presenterSpy.presentedPaymentMethods.contains(.bankSlip))
    }
    
    private func newInstallment(status: InstallmentStatus = .open) -> Installment {
        Installment(
            id: "1",
            originalValue: 1056.61,
            value: 1009.58,
            penaltyValue: 0,
            order: 1,
            status: status,
            dueDate: Date(),
            paymentDate: nil,
            daysOverDue: -28,
            isPayable: true,
            discountValue: 0
        )
    }
}
