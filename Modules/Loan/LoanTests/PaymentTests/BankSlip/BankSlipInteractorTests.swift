import AnalyticsModule
import XCTest
import AssetsKit
@testable import Core
@testable import Loan

final private class BankSlipServiceSpy: BankSlipServicing {
    var bankSlipEmailResponse: Result<SendBankSlipByEmailResponse, ApiError> = .failure(.serverError)

    func sendBankSlipByEmail(contractId: String, installmentIds: [String], completion: @escaping SendBankSlipByEmailCompletionBlock) {
        completion(bankSlipEmailResponse)
    }
}

final private class BankSlipPresenterSpy: BankSlipPresenting {
    var viewController: BankSlipDisplaying?

    private(set) var displayBankSlipInfoCallCount = 0
    private(set) var didNextStepCallCount = 0
    private(set) var startLoadingCallCount = 0
    private(set) var stopLoadingCallCount = 0
    private(set) var presentCopyCodeSuccededCallCount = 0
    private(set) var presentSendByEmailSuccededCallCount = 0
    private(set) var presentErrorCallCount = 0
    private(set) var closeCallCount = 0

    func displayBankSlipInfo(bankSlip: BankSlip) {
        displayBankSlipInfoCallCount += 1
    }

    func didNextStep(action: BankSlipAction) {
        didNextStepCallCount += 1
    }

    func startLoading() {
        startLoadingCallCount += 1
    }

    func stopLoading() {
        stopLoadingCallCount += 1
    }

    func presentCopyCodeSucceded() {
        presentCopyCodeSuccededCallCount += 1
    }

    func presentSendByEmailSucceded(email: String) {
        presentSendByEmailSuccededCallCount += 1
    }

    func presentError() {
        presentErrorCallCount += 1
    }

    func close() {
        closeCallCount += 1
    }
}

final class BankSlipInteractorTests: XCTestCase {
    private let presenterSpy = BankSlipPresenterSpy()
    private let serviceSpy = BankSlipServiceSpy()
    private let bankSlip = BankSlip(code: "1", value: "R$ 100,00", dueDate: Date())
    private let installment = Installment(id: "ID",
                                          originalValue: 22.99,
                                          value: 30.00,
                                          penaltyValue: nil,
                                          order: 2,
                                          status: .overdue,
                                          dueDate: Date(),
                                          paymentDate: nil,
                                          daysOverDue: nil,
                                          isPayable: true,
                                          discountValue: 0.0)
    private lazy var sut = BankSlipInteractor(service: serviceSpy,
                                              presenter: presenterSpy,
                                              bankSlip: bankSlip,
                                              contractId: "Id",
                                              installments: [installment],
                                              dependencies: dependencies)
    private let analytics = AnalyticsSpy()
    private lazy var dependencies = LoanDependencyContainerMock(analytics)

    func testTrackEnterView_ShoudLogEventDidEnter() throws {
        let expectedEvent = LoanEvent.didEnter(to: .paymentEmail, from: .payment, params: ["type": installment.status.trackingValue])
        sut.trackEnterView()
        try assertEvent(with: expectedEvent)
    }

    func testDidTapCopy_ShoudLogEventDidCopy() throws {
        let expectedEvent = LoanEvent.didCopy(to: .paymentEmail)
        sut.didTapCopy()
        try assertEvent(with: expectedEvent)
    }

    func testSendBankSlipByEmail_ShoudLogEventDidSend() throws {
        let expectedEvent = LoanEvent.didSend(to: .paymentEmail)
        sut.sendBankSlipByEmail()
        try assertEvent(with: expectedEvent)
    }

    func testSendBankSlipByEmail_WhenServerError_ShouldPresentError() {
        sut.sendBankSlipByEmail()

        XCTAssertEqual(presenterSpy.startLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.presentSendByEmailSuccededCallCount, 0)
        XCTAssertEqual(presenterSpy.presentErrorCallCount, 1)
    }

    func testSendBankSlipByEmail_WhenHasBankSlipByEmailResponse_ShouldPresentSendByEmailSucceded() {
        serviceSpy.bankSlipEmailResponse = .success(SendBankSlipByEmailResponse(destinationEmail: "mail@picpay.com"))
        sut.sendBankSlipByEmail()

        XCTAssertEqual(presenterSpy.startLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.presentSendByEmailSuccededCallCount, 1)
        XCTAssertEqual(presenterSpy.presentErrorCallCount, 0)
    }

    func testClose_ShouldClose() {
        sut.close()
        XCTAssertEqual(presenterSpy.closeCallCount, 1)
    }

    private func assertEvent(with expectedEvent: LoanEvent) throws {
        let analytics = try XCTUnwrap(dependencies.analytics as? AnalyticsSpy)
        XCTAssertTrue(analytics.equals(to: expectedEvent.event()))
    }
}
