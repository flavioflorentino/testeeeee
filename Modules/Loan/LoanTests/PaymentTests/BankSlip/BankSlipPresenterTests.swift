import XCTest
import Core
@testable import Loan

final private class BankSlipControllerSpy: BankSlipDisplaying {
    private(set) var displayInfosCallCount = 0
    private(set) var displayErrorCallCount = 0
    private(set) var displayCopyCodeSuccededCallCount = 0
    private(set) var displaySendByEmailSuccededCallCount = 0
    private(set) var startLoadingCallCount = 0
    private(set) var stopLoadingCallCount = 0

    func displayInfos(description: NSAttributedString, code: String) {
        displayInfosCallCount += 1
    }

    func displayError() {
        displayErrorCallCount += 1
    }

    func displayCopyCodeSucceded() {
        displayCopyCodeSuccededCallCount += 1
    }

    func displaySendByEmailSucceded(email: String) {
        displaySendByEmailSuccededCallCount += 1
    }

    func startLoading() {
        startLoadingCallCount += 1
    }

    func stopLoading() {
        stopLoadingCallCount += 1
    }
}

final private class BankSlipCoordinatorSpy: BankSlipCoordinating {
    var delegate: PaymentFlowCoordinating?

    private(set) var performActionCallCount = 0
    private(set) var performedAction: BankSlipAction?

    func perform(action: BankSlipAction) {
        performActionCallCount += 1
        performedAction = action
    }
}

final class BankSlipPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = BankSlipCoordinatorSpy()
    private let controllerSpy = BankSlipControllerSpy()

    private lazy var sut: BankSlipPresenter = {
        let sut = BankSlipPresenter(coordinator: coordinatorSpy)
        sut.viewController = controllerSpy
        return sut
    }()

    func testDisplayBankSlipInfo_ShouldDisplayInfos() {
        let dumbBankSlip = BankSlip(code: "12345", value: "R$22,99", dueDate: Date())
        sut.displayBankSlipInfo(bankSlip: dumbBankSlip)

        XCTAssertEqual(controllerSpy.displayInfosCallCount, 1)
    }

    func testDidNextStep_ShouldPerformBankSlipAction() {
        sut.didNextStep(action: .back)

        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .back)
    }

    func testStartLoading_ShouldStartLoading() {
        sut.startLoading()
        XCTAssertEqual(controllerSpy.startLoadingCallCount, 1)
    }

    func testStopLoading_ShouldStopLoading() {
        sut.stopLoading()
        XCTAssertEqual(controllerSpy.stopLoadingCallCount, 1)
    }

    func testPresentSendByEmailSucceded_ShouldDisplaySendByEmailSucceded() {
        sut.presentSendByEmailSucceded(email: "mail@picpay.com")
        XCTAssertEqual(controllerSpy.displaySendByEmailSuccededCallCount, 1)
    }

    func testPresentCopyCodeSucceded_ShouldDisplayCopyCodeSucceded() {
        sut.presentCopyCodeSucceded()
        XCTAssertEqual(controllerSpy.displayCopyCodeSuccededCallCount, 1)
    }

    func testPresentError_ShouldDisplayError() {
        sut.presentError()
        XCTAssertEqual(controllerSpy.displayErrorCallCount, 1)
    }

    func testClose_ShouldPerformBackAction() {
        sut.close()
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .back)
    }
}
