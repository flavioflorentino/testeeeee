import XCTest
@testable import Loan

private final class PaymentMethodCoordinatorDumb: PaymentMethodCoordinating {
    var viewController: UIViewController?
    
    func perform(action: PaymentMethodAction) { }
}

private final class PaymentMethodControllerSpy: PaymentMethodDisplaying {
    private(set) var displayValueCallCount = 0
    private(set) var displayDueDateCallCount = 0
    private(set) var displayPaymentMethodsCallCount = 0
    private(set) var displayDetailsTitleCallCount = 0
    private(set) var displayAddDetailsRowCallCount = 0
    private(set) var displayAddDisclaimerCallCount = 0
    private(set) var startLoadingCallCount = 0
    private(set) var stopLoadingCallCount = 0
    private(set) var displayErrorCallCount = 0
    private(set) var displayHeaderTitleCallCount = 0
    
    func displayValue(_ value: Double) {
        displayValueCallCount += 1
    }
    
    func displayDueDate(_ dueDate: NSAttributedString) {
        displayDueDateCallCount += 1
    }
    
    func displayPaymentMethods(_ paymentMethods: [PaymentMethod]) {
        displayPaymentMethodsCallCount += 1
    }
    
    func addDetailsTitle() {
        displayDetailsTitleCallCount += 1
    }
    
    func addHeaderTitle(title: String) {
        displayHeaderTitleCallCount += 1
    }
    
    func addDetailsRow(title: String, value: String?) {
        displayAddDetailsRowCallCount += 1
    }
    
    func addDisclaimer() {
        displayAddDisclaimerCallCount += 1
    }
    
    func startLoading() {
        startLoadingCallCount += 1
    }
    
    func stopLoading() {
        stopLoadingCallCount += 1
    }
    
    func displayError() {
        displayErrorCallCount += 1
    }
}

final class PaymentMethodPresenterTests: XCTestCase {
    private let coordinatorSpy = PaymentMethodCoordinatorDumb()
    private let controllerSpy = PaymentMethodControllerSpy()
    
    private lazy var sut: PaymentMethodPresenter = {
        let sut = PaymentMethodPresenter(coordinator: coordinatorSpy)
        sut.viewController = controllerSpy
        return sut
    }()
    
    func testPresentInstallmentDetails_ShouldCallDisplayValue() {
        sut.presentSingleInstallmentInfo(installment: newInstallment())
        
        XCTAssertEqual(controllerSpy.displayValueCallCount, 1)
    }
    
    func testPresentInstallmentDetails_ShouldCallDisplayDueDate() {
        sut.presentSingleInstallmentInfo(installment: newInstallment())
        
        XCTAssertEqual(controllerSpy.displayDueDateCallCount, 1)
    }
    
    func testPresentInstallmentDetails_WhenDetailsIsRequired_ShouldCallDisplayDueDate() {
        sut.presentSingleInstallmentInfo(installment: newInstallment(status: .overdue))
        
        XCTAssertEqual(controllerSpy.displayDueDateCallCount, 0)
    }
    
    func testPresentInstallmentDetails_WhenDetailsIsRequired_ShouldCallDisplayDetailsTitle() {
        sut.presentSingleInstallmentInfo(installment: newInstallment(status: .overdue))
        
        XCTAssertEqual(controllerSpy.displayDetailsTitleCallCount, 1)
    }
    
    func testPresentInstallmentDetails_WhenDetailsIsRequired_ShouldCallDisplayHeaderTitle() {
        sut.presentSingleInstallmentInfo(installment: newInstallment(status: .overdue))
        
        XCTAssertEqual(controllerSpy.displayHeaderTitleCallCount, 1)
    }
    
    func testPresentInstallmentDetails_WhenDetailsIsRequired_ShouldCallDisplayAddDetailsRow() {
        sut.presentSingleInstallmentInfo(installment: newInstallment(status: .overdue))
        
        XCTAssertEqual(controllerSpy.displayAddDetailsRowCallCount, 4)
    }
    
    func testPresentInstallmentDetails_WhenDetailsIsRequired_ShouldCallAddDisclaimer() {
        sut.presentSingleInstallmentInfo(installment: newInstallment(status: .overdue))
        
        XCTAssertEqual(controllerSpy.displayAddDisclaimerCallCount, 1)
    }
    
    func testPresentInstallmentDetails_WhenDetailsIsNotRequired_ShouldNotCallDisplayDetailsTitle() {
        sut.presentSingleInstallmentInfo(installment: newInstallment(status: .closeToExpire))
        
        XCTAssertEqual(controllerSpy.displayPaymentMethodsCallCount, 0)
    }
    
    func testPresentInstallmentDetails_WhenDetailsIsNotRequired_ShouldNotCallDisplayAddDetailsRow() {
        sut.presentSingleInstallmentInfo(installment: newInstallment(status: .closeToExpire))
        
        XCTAssertEqual(controllerSpy.displayPaymentMethodsCallCount, 0)
    }
    
    func testPresentInstallmentDetails_WhenDetailsIsNotRequired_ShouldNotCallAddDisclaimer() {
        sut.presentSingleInstallmentInfo(installment: newInstallment(status: .closeToExpire))
        
        XCTAssertEqual(controllerSpy.displayAddDisclaimerCallCount, 0)
    }
    
    private func newInstallment(status: InstallmentStatus = .open) -> Installment {
        Installment(
            id: "1",
            originalValue: 1056.61,
            value: 1009.58,
            penaltyValue: 0,
            order: 1,
            status: status,
            dueDate: Date(),
            paymentDate: nil,
            daysOverDue: -28,
            isPayable: true,
            discountValue: 0
        )
    }
}

