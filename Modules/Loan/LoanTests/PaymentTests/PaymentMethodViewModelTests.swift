import XCTest
@testable import Loan

final class PaymentMethodViewModelTests: XCTestCase {
    func testisDetailsRequired_WhenStatusOverdue_ShouldBeTrue() {
        let viewModel = PaymentMethodViewModel(installment: newInstallment(status: .overdue))
        
        XCTAssertTrue(viewModel.isDetailsRequired)
    }
    
    func testDetailsRequired_WhenStatusCloseToExpire_ShouldBeFalse() {
        let viewModel = PaymentMethodViewModel(installment: newInstallment(status: .closeToExpire))
        
        XCTAssertFalse(viewModel.isDetailsRequired)
    }
    
    private func newInstallment(status: InstallmentStatus) -> Installment {
        Installment(
            id: "1",
            originalValue: 1056.61,
            value: 1009.58,
            penaltyValue: 0,
            order: 1,
            status: status,
            dueDate: Date(),
            paymentDate: nil,
            daysOverDue: -28,
            isPayable: true,
            discountValue: 0
        )
    }
}
