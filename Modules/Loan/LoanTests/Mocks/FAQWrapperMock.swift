@testable import Loan

final class FAQWrapperMock: LoanFAQContract {
    private(set) var controllerCallCount = 0
    private(set) var title: String?
    private(set) var faqUrl: String?
    
    var expectedResult: FAQResult?
    
    func controller(withTitle title: String? = nil, faqUrl: String) -> FAQResult {
        controllerCallCount += 1
        self.title = title
        self.faqUrl = faqUrl
        
        guard let result = expectedResult else {
            fatalError("\n🚨 Please initialize expected result\n")
        }
        
        return result
    }
}
