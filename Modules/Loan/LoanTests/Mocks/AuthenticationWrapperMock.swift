import Core
@testable import Loan

final class AuthenticationWrapperMock: AuthenticationContract {
    var isAuthenticated: Bool {
        false
    }
    
    private(set) var authenticateCallCount = 0
    var expectedResult: Result<String, AuthenticationStatus>?
    
    func authenticate(_ completion: @escaping AuthenticationResult) {
        authenticateCallCount += 1
        
        guard let result = expectedResult else {
            debugPrint("\n🚨 Please initialize expected result\n")
            return
        }
        
        completion(result)
    }

    private(set) var disableBiometricAuthenticationCallCount = 0
    func disableBiometricAuthentication() {
        disableBiometricAuthenticationCallCount += 1
    }
}
