@testable import Loan

final class ConsumerWrapperMock: LoanConsumerContract {
    var consumerBalance: String = ""
    var consumerId: Int = 0
    var consumerEmail: String = "picpay@gmail.com"
}

