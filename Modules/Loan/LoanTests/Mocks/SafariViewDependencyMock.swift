import SafariServices
@testable import Loan

final class SafariViewManagerMock: SafariViewDependency {
    var error: SafariError?
    
    func controller(for urlPath: String? = "", urlOpener: URLOpener) -> Result<SFSafariViewController, SafariError> {
        guard
            error == nil,
            let url = URL(string: "https://picpay.com")
            else {
                return .failure(error ?? .nilUrl)
        }
        
        return .success(SFSafariViewController(url: url))
    }
}
