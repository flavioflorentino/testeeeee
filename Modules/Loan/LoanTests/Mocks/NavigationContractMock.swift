import UI
import UIKit

final class NavigationContractMock: NavigationContract {
    private(set) var getCurrentNavigationCallsCount = 0
    private(set) var updateCurrentCoordinatingCallsCount = 0
    private(set) var updateCurrentCoordinatingReceivedInvocations: [Coordinating?] = []

    var currentNavigationController: UINavigationController

    init(navigationSpy: UINavigationControllerSpy) {
        currentNavigationController = navigationSpy
    }

    func getCurrentNavigation() -> UINavigationController? {
        getCurrentNavigationCallsCount += 1
        return currentNavigationController
    }

    func updateCurrentCoordinating(_ coordinating: Coordinating?) {
        updateCurrentCoordinatingCallsCount += 1
        updateCurrentCoordinatingReceivedInvocations.append(coordinating)
    }
}
