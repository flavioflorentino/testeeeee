@testable import Loan

final class RechargeMethodsWrapperMock: LoanRechargeMethodsContract {
    private(set) var openRechargeLoadViewCallCount = 0
    
    func openRechargeLoadView() -> UIViewController {
        openRechargeLoadViewCallCount += 1
        return UIViewController()
    }
}
