@testable import Loan

final class LoanFlowCoordinatingSpy: LoanFlowCoordinating {
    var childViewController: [UIViewController] = []
    var viewController: UIViewController?
    
    private(set) var startFlowCallCount = 0
    private(set) var finishHireFlowCallCount = 0
    private(set) var closeCallCount = 0
    private(set) var startedFlow: LoanFlow?
    
    func start(flow: LoanFlow, didFinish: @escaping () -> Void) {
        startFlowCallCount += 1
        startedFlow = flow
    }

    func finishHireFlow() {
        finishHireFlowCallCount += 1
    }
    
    func close() {
        closeCallCount += 1
    }
}
