@testable import Loan

final class BilletWrapperMock: LoanBilletContract {
    private(set) var openBilletFlowCallCount = 0
    private(set) var barcode: String?
    private(set) var placeholder: String?
    
    var expectedResult: BilletCompletion?
    
    func openBilletFlow(withBarcode barcode: String,
                        placeholder: String,
                        origin: String,
                        completion: @escaping BilletCompletion) {
        openBilletFlowCallCount += 1
        self.barcode = barcode
        self.placeholder = placeholder
        expectedResult = completion
    }
}
