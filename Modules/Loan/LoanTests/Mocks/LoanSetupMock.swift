import Foundation
import Core
import UI
@testable import Loan

enum LegacyDependencies {
    case billet
    case recharge
    case consumer
    case faq
    case auth
    case navigationInstance(navigation: UINavigationControllerSpy)
}

final class LoanSetupMock: LoanLegacySetupContract {
    var billet: LoanBilletContract?
    var recharge: LoanRechargeMethodsContract?
    var consumer: LoanConsumerContract?
    var faq: LoanFAQContract?
    var auth: AuthenticationContract?
    var deeplink: DeeplinkContract?
    var customerSupport: LoanCustomerSupportContract?
    var navigationInstance: NavigationContract?
    
    init(_ dependencies: LegacyDependencies...) {
        dependencies.forEach {
            switch $0 {
            case .billet:
                billet = BilletWrapperMock()
            case .recharge:
                recharge = RechargeMethodsWrapperMock()
            case .consumer:
                consumer = ConsumerWrapperMock()
            case .faq:
                faq = FAQWrapperMock()
            case .auth:
                auth = AuthenticationWrapperMock()
            case let .navigationInstance(navigation):
                navigationInstance = NavigationContractMock(navigationSpy: navigation)
            }
        }
    }
}
