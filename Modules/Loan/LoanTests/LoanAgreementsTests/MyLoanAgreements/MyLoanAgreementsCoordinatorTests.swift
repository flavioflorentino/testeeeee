import XCTest
@testable import Loan

final class MyLoanAgreementsCoordinatorTests: XCTestCase {
    private let delegateSpy = LoanAgreementsCoordinatingSpy()
    private let dependenciesSpy = LoanDependencyContainerMock(LoanSetupMock(.faq))
    private lazy var sut = MyLoanAgreementsCoordinator(delegate: delegateSpy,
                                                       dependencies: dependenciesSpy)
    
    func testPerformActionMyLoanAgreements_ShouldGoToMyLoanAgreements() {
        sut.perform(action: .installments(contractId: "1"))
        
        XCTAssertEqual(delegateSpy.performActionCallCount, 1)
        XCTAssertEqual(delegateSpy.performedAction, .installments(contractId: "1", origin: .myLoanAgreements))
    }
    
    func testPerformActionBack_ShouldPop() {
        sut.perform(action: .back)
        
        XCTAssertEqual(delegateSpy.performActionCallCount, 1)
        XCTAssertEqual(delegateSpy.performedAction, .pop)
    }
}
