import AnalyticsModule
import Core
import XCTest
@testable import Loan

private final class MyLoanAgreementsServiceSpy: MyLoanAgreementsServicing {
    let loan = LoanAgreement(id: "1a1a1a",
                              bankId: "212",
                              loanGoal:
                                LoanGoal(category: .build, description: ""),
                              totalInstallments: 10,
                              totalInstallmentsAlreadyPaid: 2,
                              status: "ACTIVE")
    lazy var response: Result<LoanAgreementsResponse, ApiError> = .success(LoanAgreementsResponse(loans: [loan]))
    
    func getLoanAgreements(completion: @escaping (Result<LoanAgreementsResponse, ApiError>) -> Void) {
        completion(response)
    }
}

private final class MyLoanAgreementsPresenterSpy: MyLoanAgreementsPresenting {
    var viewController: MyLoanAgreementsDisplay?
    
    private(set) var presentLoanAgreementsCallCount = 0
    private(set) var didNextStepCallCount = 0
    private(set) var startLoadingCallCount = 0
    private(set) var stopLoadingCallCount = 0
    private(set) var presentErrorCallCount = 0
    private(set) var backCallCount = 0
    private(set) var openFAQCallCount = 0
    
    private(set) var performedAction: MyLoanAgreementsAction?
    private(set) var article: FAQArticle?
    
    func present(loans: [LoanAgreement]) {
        presentLoanAgreementsCallCount += 1
    }
    
    func didNextStep(action: MyLoanAgreementsAction) {
        didNextStepCallCount += 1
        performedAction = action
    }
    
    func startLoading() {
        startLoadingCallCount += 1
    }
    
    func stopLoading() {
        stopLoadingCallCount += 1
    }
    
    func presentError() {
        presentErrorCallCount += 1
    }
    
    func back() {
        backCallCount += 1
    }
    
    func openFAQ(_ article: FAQArticle) {
        openFAQCallCount += 1
        self.article = article
    }
}

final class MyLoanAgreementsInteractorTests: XCTestCase {
    private let serviceSpy = MyLoanAgreementsServiceSpy()
    private let presenterSpy = MyLoanAgreementsPresenterSpy()
    private let dependenciesSpy = LoanDependencyContainerMock(AnalyticsSpy())
    
    private lazy var sut = MyLoanAgreementsInteractor(service: serviceSpy,
                                                      presenter: presenterSpy,
                                                      from: .settingsMenu,
                                                      dependencies: dependenciesSpy)
    
    func testFetchLoanAgreements_WhenFetchSucceeds_ShouldPresentLoanAgreements() {
        sut.fetch()
        
        XCTAssertEqual(presenterSpy.presentLoanAgreementsCallCount, 1)
    }
    
    func testFetchAgreements_WhenFetchError_ShouldPresentError() {
        serviceSpy.response = .failure(.serverError)
        sut.fetch()
        
        XCTAssertEqual(presenterSpy.presentErrorCallCount, 1)
    }
    
    func testTrackEnterView_WhenComesFromSettings_ShouldLogEventDidEnterFromSettings() throws {
        let expectedEvent = LoanEvent.didEnter(to: .myLoanAgreements, from: .settingsMenu)
        
        sut.trackEnterView()
        
        try assertOrigins(with: expectedEvent, originType: .settingsMenu)
    }
    
    func testSelectedLoan_ShouldGoToNextStep() {
        sut.fetch()
        sut.didTapRow(index: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(presenterSpy.didNextStepCallCount, 1)
        XCTAssertEqual(presenterSpy.performedAction, .installments(contractId: "1a1a1a"))
    }

    private func assertOrigins(with expectedEvent: LoanEvent, originType: OriginType) throws {
        let analytics = try XCTUnwrap(dependenciesSpy.analytics as? AnalyticsSpy)
        let origin = try XCTUnwrap(analytics.value(forKey: "origin"))

        XCTAssertTrue(analytics.equals(to: expectedEvent.event()))
        XCTAssertEqual(origin, originType.value)
    }
    
    func testOpenFAQ_ShouldOpenFAQ() {
        sut.didTapFAQ()
        
        XCTAssertEqual(presenterSpy.openFAQCallCount, 1)
        XCTAssertEqual(presenterSpy.article, .installments)
    }
}
