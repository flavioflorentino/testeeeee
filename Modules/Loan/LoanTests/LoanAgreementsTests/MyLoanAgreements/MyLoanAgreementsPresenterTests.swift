import XCTest
import Core
@testable import Loan

final private class MyLoanAgreementsControllerSpy: MyLoanAgreementsDisplay {
    private(set) var displayItemsCallCount = 0
    private(set) var displayedItemsCount = 0
    private(set) var displayEmptyStateCallCount = 0
    private(set) var displayErrorCallCount = 0
    private(set) var startLoadingCallCount = 0
    private(set) var stopLoadingCallCount = 0

    func displayItems(data: [LoanAgreementViewModel]) {
        displayItemsCallCount += 1
        displayedItemsCount = data.count
    }

    func displayEmptyState() {
        displayEmptyStateCallCount += 1
    }

    func displayError() {
        displayErrorCallCount += 1
    }

    func startLoading() {
        startLoadingCallCount += 1
    }

    func stopLoading() {
        stopLoadingCallCount += 1
    }
}

final private class MyLoanAgreementsCoordinatorSpy: MyLoanAgreementsCoordinating {
    private(set) var performActionCallCount = 0
    private(set) var performedAction: MyLoanAgreementsAction?

    func perform(action: MyLoanAgreementsAction) {
        performActionCallCount += 1
        performedAction = action
    }
}

final class MyLoanAgreementsPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = MyLoanAgreementsCoordinatorSpy()
    private let controllerSpy = MyLoanAgreementsControllerSpy()

    private lazy var sut: MyLoanAgreementsPresenter = {
        let sut = MyLoanAgreementsPresenter(coordinator: coordinatorSpy)
        sut.viewController = controllerSpy
        return sut
    }()

    func testPresent_whenLoansIsEmpty_ShouldDisplayEmptyState() {
        sut.present(loans: [])

        XCTAssertEqual(controllerSpy.displayEmptyStateCallCount, 1)
        XCTAssertEqual(controllerSpy.displayItemsCallCount, 0)
    }

    func testPresent_ShouldDisplayItems() {
        let dumbLoan = LoanAgreement(id: "12345",
                                     bankId: "212",
                                     loanGoal: .init(category: .bills, description: nil),
                                     totalInstallments: 4,
                                     totalInstallmentsAlreadyPaid: 2,
                                     status: "status")
        sut.present(loans: [dumbLoan])

        XCTAssertEqual(controllerSpy.displayEmptyStateCallCount, 0)
        XCTAssertEqual(controllerSpy.displayItemsCallCount, 1)
        XCTAssertEqual(controllerSpy.displayedItemsCount, 1)
    }

    func testDidNextStep_ShouldPerformAction() {
        sut.didNextStep(action: .installments(contractId: "Id"))

        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .installments(contractId: "Id"))
    }

    func testBack_ShouldPerformBackAction() {
        sut.back()

        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .back)
    }

    func testStartLoading_ShouldStartLoading() {
        sut.startLoading()

        XCTAssertEqual(controllerSpy.startLoadingCallCount, 1)
        XCTAssertEqual(controllerSpy.stopLoadingCallCount, 0)
    }

    func testStopLoading_ShouldStopLoading() {
        sut.stopLoading()

        XCTAssertEqual(controllerSpy.startLoadingCallCount, 0)
        XCTAssertEqual(controllerSpy.stopLoadingCallCount, 1)
    }

    func testPresentError_ShouldDisplayError() {
        sut.presentError()
        XCTAssertEqual(controllerSpy.displayErrorCallCount, 1)
    }
}
