import XCTest
@testable import Loan

private final class TimeExceededServiceSpy: TimeExceededServicing {
}

private final class TimeExceededPresenterSpy: TimeExceededPresenting {
    var viewController: TimeExceededDisplaying?
    
    private(set) var presentDescriptionCallCount = 0
    private(set) var presentBulletItemsCallCount = 0
    private(set) var openFAQCallCount = 0
    private(set) var didNextStepCallCount = 0
    private(set) var performedAction: TimeExceededAction?
    
    func presentDescription() {
        presentDescriptionCallCount += 1
    }
    
    func presentBulletItems() {
        presentBulletItemsCallCount += 1
    }
    
    func openFAQ() {
        openFAQCallCount += 1
    }
    
    func didNextStep(action: TimeExceededAction) {
        didNextStepCallCount += 1
        performedAction = action
    }
}

final class TimeExceededInteractorTests: XCTestCase {
    private let serviceSpy = TimeExceededServiceSpy()
    private let presenterSpy = TimeExceededPresenterSpy()
    private lazy var sut = TimeExceededInteractor(service: serviceSpy, presenter: presenterSpy)
    
    func testFetchInstallments_WhenViewDidLoad_ShouldPresentDescription() {
        sut.viewDidLoad()
        
        XCTAssertEqual(presenterSpy.presentDescriptionCallCount, 1)
    }
    
    func testFetchInstallments_WhenViewDidLoad_ShouldPresentBulletItems() {
        sut.viewDidLoad()
        
        XCTAssertEqual(presenterSpy.presentBulletItemsCallCount, 1)
    }
    
    func testOpenFAQ_WhenTapFAQLink_ShouldPresentFAQ() {
        sut.didTapFAQ()
        
        XCTAssertEqual(presenterSpy.openFAQCallCount, 1)
    }
}
