import XCTest
@testable import Loan

private final class TimeExceededCoordinatorDumb: TimeExceededCoordinating {
    var viewController: UIViewController?
    
    private(set) var performActionCallCount = 0
    var performedAction: TimeExceededAction?
    
    func perform(action: TimeExceededAction) {
        performActionCallCount += 1
        performedAction = action
    }
}

private final class TimeExceededControllerSpy: TimeExceededDisplaying {
    private(set) var displayDescriptionCallCount = 0
    private(set) var displayBulletItemCallCount = 0
    
    private(set) var descriptionDisplayed: NSAttributedString?
    private(set) var bulletItemsDisplayed: [String] = []
    
    func displayDescription(_ description: NSMutableAttributedString) {
        displayDescriptionCallCount += 1
        descriptionDisplayed = description
    }
    
    func displayBulletItem(description: String) {
        displayBulletItemCallCount += 1
        bulletItemsDisplayed.append(description)
    }
}

final class TimeExceededPresenterTests: XCTestCase {
    private let coordinatorSpy = TimeExceededCoordinatorDumb()
    private let controllerSpy = TimeExceededControllerSpy()
    
    private lazy var sut: TimeExceededPresenting = {
        let sut = TimeExceededPresenter(coordinator: coordinatorSpy)
        sut.viewController = controllerSpy
        return sut
    }()
    
    func testPresentDescription_ShouldDisplayDescription() {
        sut.presentDescription()
        
        XCTAssertEqual(controllerSpy.displayDescriptionCallCount, 1)
        XCTAssertEqual(
            controllerSpy.descriptionDisplayed?.string,
            "O boleto do seu empréstimo venceu, e você pode emitir a 2ª via até às 21h, em dias úteis."
        )
    }
    
    func testPresentBulletItems_ShouldDisplayBulletItems() {
        sut.presentBulletItems()
        
        XCTAssertEqual(controllerSpy.displayBulletItemCallCount, 2)
        XCTAssertEqual(controllerSpy.bulletItemsDisplayed.count, 2)
        XCTAssertEqual(
            controllerSpy.bulletItemsDisplayed[0],
            "**Pagar no PicPay**\nDas 7 às 19h, em dias úteis. Após às 19h, consulte sua instituição financeira."
        )
        XCTAssertEqual(
            controllerSpy.bulletItemsDisplayed[1],
            "**Pagar em finais de semana e feriados**\nNão precisa gerar 2ª via, é só pagar no próximo dia útil, sem juros."
        )
    }
}
