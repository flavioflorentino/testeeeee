import UIKit
@testable import Loan

final class LoanAgreementsCoordinatingSpy: LoanAgreementsCoordinating {
    var childViewController: [UIViewController] = []
    var viewController: UIViewController?
    
    private(set) var performActionCallCount = 0
    private(set) var performedAction: LoanAgreementsAction?
    
    func perform(action: LoanAgreementsAction) {
        performActionCallCount += 1
        performedAction = action
    }
}
