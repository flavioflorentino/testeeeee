import AnalyticsModule
import XCTest
@testable import Loan
@testable import Core

final private class SimulateAdvancePaymentServiceSpy: SimulateAdvancePaymentServicing {
    
    private(set) var verifyWordayCallsCount = 0
    var response: Result<NoContent, ApiError> = .failure(.serverError)
    
    func verifyWorkday(completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        verifyWordayCallsCount += 1
        completion(response)
    }
}

private final class SimulateAdvancePaymentPresenterSpy: SimulateAdvancePaymentPresenting {
    var viewController: SimulateAdvancePaymentDisplaying?
    
    private(set) var presentDataCallCount = 0
    private(set) var setupFooterDescriptionCallCount = 0
    private(set) var presentNextButtonStateCallCount = 0
    private(set) var openHireAdvancePaymentCallCount = 0
    private(set) var presentErrorCallCount = 0
    private(set) var presentTimeExceededWarningCallCount = 0
    private(set) var presentConnectionErrorCallCount = 0
    private(set) var closeCallCount = 0
    private(set) var popCallCount = 0
    private(set) var openFAQCallCount = 0
    
    private(set) var totalToPay: Double?
    private(set) var totalDiscount: Double?
    private(set) var totalInstallments: Int?
    private(set) var previousValue: Double?
    private(set) var previousDiscount: Double?
    private(set) var isNextButtonEnabled: Bool?
    
    func presentData(items: [SimulateAdvancePaymentViewModel]) {
        presentDataCallCount += 1
    }
    
    func setupFooterDescription(valueToPay value: Double,
                                previousValue: Double,
                                totalInstallments: Int,
                                previousDiscount: Double,
                                totalDiscount: Double) {
        setupFooterDescriptionCallCount += 1
        totalToPay = value
        self.totalDiscount = totalDiscount
        self.totalInstallments = totalInstallments
        self.previousValue = previousValue
        self.previousDiscount = previousDiscount
    }
    
    func presentNextButtonState(isNextButtonEnabled: Bool) {
        presentNextButtonStateCallCount += 1
        self.isNextButtonEnabled = isNextButtonEnabled
    }
    
    func openHireAdvancePayment(contractId: String, installments: [Installment]) {
        openHireAdvancePaymentCallCount += 1
    }
    
    func presentError() {
        presentErrorCallCount += 1
    }
    
    func presentTimeExceededWarning() {
        presentTimeExceededWarningCallCount += 1
    }
    
    func presentConnectionError() {
        presentConnectionErrorCallCount += 1
    }
    
    func close() {
        closeCallCount += 1
    }
    
    func pop() {
        popCallCount += 1
    }
    
    func openFAQ(article: FAQArticle) {
        openFAQCallCount += 1
    }
}

final class SimulateAdvancePaymentInteractorTests: XCTestCase {
    private let presenterSpy = SimulateAdvancePaymentPresenterSpy()
    private let dependenciesSpy = LoanDependencyContainerMock(AnalyticsSpy())
    private let serviceSpy = SimulateAdvancePaymentServiceSpy()
    
    private let mockValue = 1036.25
    private let mockDiscount = 20.63
    
    private func installment(status: InstallmentStatus) -> Installment {
        Installment(
            id: "1",
            originalValue: 1056.61,
            value: mockValue,
            penaltyValue: 0,
            order: 1,
            status: status,
            dueDate: Date(),
            paymentDate: nil,
            daysOverDue: 0,
            isPayable: true,
            discountValue: mockDiscount
        )
    }
    
    private var bankSelectionOrderMoked: InstallmentSelectionOrder = .both
    private lazy var sut =
        SimulateAdvancePaymentInteractor(contractId: "ABC12345",
                                         installments: [installment(status: .renegotiated),
                                                        installment(status: .overdue),
                                                        installment(status: .open),
                                                        installment(status: .open),
                                                        installment(status: .open)],
                                         bankSelectionOrder: bankSelectionOrderMoked,
                                         presenter: presenterSpy,
                                         dependencies: dependenciesSpy,
                                         service: serviceSpy)
    
    private lazy var newInstallmentsArrayWithMandatory = [
        SimulateAdvancePaymentViewModel(installment: installment(status: .renegotiated),
                                        isSelectionAvailable: false),
        SimulateAdvancePaymentViewModel(installment: installment(status: .overdue),
                                        isSelectionAvailable: false),
        SimulateAdvancePaymentViewModel(installment: installment(status: .open), isSelectionAvailable: true),
        SimulateAdvancePaymentViewModel(installment: installment(status: .open), isSelectionAvailable: false),
        SimulateAdvancePaymentViewModel(installment: installment(status: .open), isSelectionAvailable: false),
        SimulateAdvancePaymentViewModel(installment: installment(status: .open), isSelectionAvailable: false),
        SimulateAdvancePaymentViewModel(installment: installment(status: .open), isSelectionAvailable: true)
    ]
    
    private lazy var newInstallmentsArrayWithOutMandatory = [
        SimulateAdvancePaymentViewModel(installment: installment(status: .renegotiated),
                                        isSelectionAvailable: false),
        SimulateAdvancePaymentViewModel(installment: installment(status: .open), isSelectionAvailable: true),
        SimulateAdvancePaymentViewModel(installment: installment(status: .open), isSelectionAvailable: false),
        SimulateAdvancePaymentViewModel(installment: installment(status: .open), isSelectionAvailable: false),
        SimulateAdvancePaymentViewModel(installment: installment(status: .open), isSelectionAvailable: true)
    ]
    
    func testFetch_WhenSuccess_ShouldOpenHireAdvancePayment() {
        serviceSpy.response = .success(NoContent())
        sut.createData()
        sut.fetch()
        
        XCTAssertEqual(serviceSpy.verifyWordayCallsCount, 1)
        XCTAssertEqual(presenterSpy.openHireAdvancePaymentCallCount, 1)
    }
    
    func testFetch_WhenError_ShouldPresentError() {
        sut.fetch()
        
        XCTAssertEqual(presenterSpy.presentErrorCallCount, 1)
    }
    
    func testFetch_WhenConnectionFailureError_ShouldPresentConnectionError() {
        serviceSpy.response = .failure(.connectionFailure)
        sut.fetch()
        
        XCTAssertEqual(serviceSpy.verifyWordayCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentConnectionErrorCallCount, 1)
    }
    
    func testFetch_WhenTimeExceededError_ShouldPresentTimeExceededWarning() {
        serviceSpy.response = .failure(
            .otherErrors(
                body: RequestError(
                    title: "Error",
                    message: "Error",
                    code: LoanApiError.timeExceeded.rawValue
                )
            )
        )
        
        sut.fetch()
        
        XCTAssertEqual(serviceSpy.verifyWordayCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentTimeExceededWarningCallCount, 1)
    }
    
    func testPop_WhenPopInAlertDialog_ShouldPresentPop() {
        sut.pop()
        
        XCTAssertEqual(presenterSpy.popCallCount, 1)
    }
    
    func testOpenFaq_WhenTapInAlertDialog_ShouldOpenFaq() {
        sut.didTapFAQ(article: .bankSlip)
        
        XCTAssertEqual(presenterSpy.openFAQCallCount, 1)
    }
    
    //MARK: - Test Generic Selection and Deselection
    func testCreateData_WhenViewDidLoad_ShouldPresentDataAndFooterDescription() {
        sut.createData()
        
        XCTAssertFalse(sut.newInstallments[0].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[1].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[2].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[3].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[4].isSelectionAvailable)
        
        XCTAssertFalse(sut.newInstallments[0].isSelected)
        XCTAssertTrue(sut.newInstallments[1].isSelected)
        XCTAssertFalse(sut.newInstallments[2].isSelected)
        XCTAssertFalse(sut.newInstallments[3].isSelected)
        XCTAssertFalse(sut.newInstallments[4].isSelected)
        
        XCTAssertEqual(presenterSpy.presentDataCallCount, 1)
        XCTAssertEqual(presenterSpy.setupFooterDescriptionCallCount, 1)
        XCTAssertEqual(presenterSpy.presentNextButtonStateCallCount, 1)
        XCTAssertEqual(presenterSpy.totalToPay, mockValue)
        XCTAssertEqual(presenterSpy.totalDiscount, mockDiscount)
        XCTAssertEqual(presenterSpy.totalInstallments, 1)
        XCTAssertEqual(presenterSpy.isNextButtonEnabled, true)
    }
    
    func testDidTapRow_WhenIndexDoesNotExist_ShouldNotPresentDataAndFooterDescription() {
        sut.newInstallments = newInstallmentsArrayWithMandatory
        sut.didTapRow(index: IndexPath(row: -1, section: 0))
        
        XCTAssertEqual(presenterSpy.presentDataCallCount, 0)
        XCTAssertEqual(presenterSpy.setupFooterDescriptionCallCount, 0)
    }
    
    func testSelectAction_WhenTapMandatoryRow_ShouldNotPresentDataAndFooterDescription() {
        sut.newInstallments = newInstallmentsArrayWithMandatory
        sut.didTapRow(index: IndexPath(row: 0, section: 0))
        
        XCTAssertFalse(sut.newInstallments[0].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[1].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[2].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[3].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[4].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[5].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[6].isSelectionAvailable)
        
        XCTAssertEqual(presenterSpy.presentDataCallCount, 0)
        XCTAssertEqual(presenterSpy.setupFooterDescriptionCallCount, 0)
        XCTAssertEqual(presenterSpy.presentNextButtonStateCallCount, 0)
    }
    
    func testSelectAction_WhenHasOnlyOneDataAndDidTapThatRow_ShouldPresentDataAndFooterDescription() {
        sut.newInstallments = [
            SimulateAdvancePaymentViewModel(installment: installment(status: .open),
                                            isSelectionAvailable: true)
        ]
        sut.didTapRow(index: IndexPath(row: 0, section: 0))
        
        XCTAssertTrue(sut.newInstallments[0].isSelectionAvailable)
        
        XCTAssertEqual(presenterSpy.presentDataCallCount, 1)
        XCTAssertEqual(presenterSpy.setupFooterDescriptionCallCount, 1)
        XCTAssertEqual(presenterSpy.presentNextButtonStateCallCount, 1)
        XCTAssertEqual(presenterSpy.totalToPay, mockValue)
        XCTAssertEqual(presenterSpy.totalDiscount, mockDiscount)
        XCTAssertEqual(presenterSpy.totalInstallments, 1)
        XCTAssertEqual(presenterSpy.isNextButtonEnabled, true)
    }
    
    func testSelectAction_WhenTapRowWithIndexZero_ShouldPresentDataAndFooterDescription() {
        sut.newInstallments = [
            SimulateAdvancePaymentViewModel(installment: installment(status: .open),
                                            isSelectionAvailable: true),
            SimulateAdvancePaymentViewModel(installment: installment(status: .open),
                                            isSelectionAvailable: false),
            SimulateAdvancePaymentViewModel(installment: installment(status: .open),
                                            isSelectionAvailable: true)
        ]
        sut.didTapRow(index: IndexPath(row: 0, section: 0))
        
        XCTAssertTrue(sut.newInstallments[0].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[1].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[2].isSelectionAvailable)
        
        XCTAssertEqual(presenterSpy.presentDataCallCount, 1)
        XCTAssertEqual(presenterSpy.setupFooterDescriptionCallCount, 1)
        XCTAssertEqual(presenterSpy.presentNextButtonStateCallCount, 1)
        XCTAssertEqual(presenterSpy.totalToPay, mockValue)
        XCTAssertEqual(presenterSpy.totalDiscount, mockDiscount)
        XCTAssertEqual(presenterSpy.totalInstallments, 1)
        XCTAssertEqual(presenterSpy.isNextButtonEnabled, true)
    }
    
    //MARK: - Test Both Orientation
    func testBothOrientationSelection_WhenTapFirstNonMandatoryRow_ShouldPresentDataAndFooterDescription() {
        sut.newInstallments = newInstallmentsArrayWithMandatory
        sut.didTapRow(index: IndexPath(row: 2, section: 0))
        
        XCTAssertFalse(sut.newInstallments[0].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[1].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[2].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[3].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[4].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[5].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[6].isSelectionAvailable)
        
        XCTAssertFalse(sut.newInstallments[0].isSelected)
        XCTAssertTrue(sut.newInstallments[1].isSelected)
        XCTAssertTrue(sut.newInstallments[2].isSelected)
        XCTAssertFalse(sut.newInstallments[3].isSelected)
        XCTAssertFalse(sut.newInstallments[4].isSelected)
        XCTAssertFalse(sut.newInstallments[5].isSelected)
        XCTAssertFalse(sut.newInstallments[6].isSelected)
        
        XCTAssertEqual(presenterSpy.presentDataCallCount, 1)
        XCTAssertEqual(presenterSpy.setupFooterDescriptionCallCount, 1)
        XCTAssertEqual(presenterSpy.presentNextButtonStateCallCount, 1)
        XCTAssertEqual(presenterSpy.totalToPay, mockValue * 2)
        XCTAssertEqual(presenterSpy.totalDiscount, mockDiscount * 2)
        XCTAssertEqual(presenterSpy.totalInstallments, 2)
        XCTAssertEqual(presenterSpy.isNextButtonEnabled, true)
    }
    
    func testBothOrientationSelection_WhenTapNonMandatoryLastRow_ShouldPresentDataAndFooterDescription() {
        sut.newInstallments = newInstallmentsArrayWithMandatory
        sut.didTapRow(index: IndexPath(row: 6, section: 0))
        
        XCTAssertFalse(sut.newInstallments[0].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[1].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[2].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[3].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[4].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[5].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[6].isSelectionAvailable)
        
        XCTAssertFalse(sut.newInstallments[0].isSelected)
        XCTAssertTrue(sut.newInstallments[1].isSelected)
        XCTAssertFalse(sut.newInstallments[2].isSelected)
        XCTAssertFalse(sut.newInstallments[3].isSelected)
        XCTAssertFalse(sut.newInstallments[4].isSelected)
        XCTAssertFalse(sut.newInstallments[5].isSelected)
        XCTAssertTrue(sut.newInstallments[6].isSelected)
        
        XCTAssertEqual(presenterSpy.presentDataCallCount, 1)
        XCTAssertEqual(presenterSpy.setupFooterDescriptionCallCount, 1)
        XCTAssertEqual(presenterSpy.presentNextButtonStateCallCount, 1)
        XCTAssertEqual(presenterSpy.totalToPay, mockValue * 2)
        XCTAssertEqual(presenterSpy.totalDiscount, mockDiscount * 2)
        XCTAssertEqual(presenterSpy.totalInstallments, 2)
        XCTAssertEqual(presenterSpy.isNextButtonEnabled, true)
    }
    
    func testBothOrientationSelection_WhenFirstNonMandatoryRowIsSelectedAndDidTapNextRow_ShouldPresentDataAndFooterDescription() {
        sut.newInstallments = newInstallmentsArrayWithMandatory
        
        sut.newInstallments[2].isSelected = true
        sut.newInstallments[3].isSelectionAvailable = true
        
        sut.didTapRow(index: IndexPath(row: 3, section: 0))
        
        XCTAssertFalse(sut.newInstallments[0].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[1].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[2].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[3].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[4].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[5].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[6].isSelectionAvailable)
        
        XCTAssertFalse(sut.newInstallments[0].isSelected)
        XCTAssertTrue(sut.newInstallments[1].isSelected)
        XCTAssertTrue(sut.newInstallments[2].isSelected)
        XCTAssertTrue(sut.newInstallments[3].isSelected)
        XCTAssertFalse(sut.newInstallments[4].isSelected)
        XCTAssertFalse(sut.newInstallments[5].isSelected)
        XCTAssertFalse(sut.newInstallments[6].isSelected)
        
        XCTAssertEqual(presenterSpy.presentDataCallCount, 1)
        XCTAssertEqual(presenterSpy.setupFooterDescriptionCallCount, 1)
        XCTAssertEqual(presenterSpy.presentNextButtonStateCallCount, 1)
        XCTAssertEqual(presenterSpy.totalToPay, mockValue * 3)
        XCTAssertEqual(presenterSpy.totalDiscount, mockDiscount * 3)
        XCTAssertEqual(presenterSpy.totalInstallments, 3)
        XCTAssertEqual(presenterSpy.isNextButtonEnabled, true)
    }
    
    func testBothOrientationSelection_WhenLastNonMandatoryRowIsSelectedAndTapPreviousRow_ShouldPresentDataAndFooterDescription() {
        sut.newInstallments = newInstallmentsArrayWithMandatory
        
        sut.newInstallments[6].isSelected = true
        sut.newInstallments[5].isSelectionAvailable = true
        
        sut.didTapRow(index: IndexPath(row: 5, section: 0))
        
        XCTAssertFalse(sut.newInstallments[0].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[1].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[2].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[3].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[4].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[5].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[6].isSelectionAvailable)
        
        XCTAssertFalse(sut.newInstallments[0].isSelected)
        XCTAssertTrue(sut.newInstallments[1].isSelected)
        XCTAssertFalse(sut.newInstallments[2].isSelected)
        XCTAssertFalse(sut.newInstallments[3].isSelected)
        XCTAssertFalse(sut.newInstallments[4].isSelected)
        XCTAssertTrue(sut.newInstallments[5].isSelected)
        XCTAssertTrue(sut.newInstallments[6].isSelected)
        
        XCTAssertEqual(presenterSpy.presentDataCallCount, 1)
        XCTAssertEqual(presenterSpy.setupFooterDescriptionCallCount, 1)
        XCTAssertEqual(presenterSpy.presentNextButtonStateCallCount, 1)
        XCTAssertEqual(presenterSpy.totalToPay, mockValue * 3)
        XCTAssertEqual(presenterSpy.totalDiscount, mockDiscount * 3)
        XCTAssertEqual(presenterSpy.totalInstallments, 3)
        XCTAssertEqual(presenterSpy.isNextButtonEnabled, true)
    }
    
    func testBothOrientationDeselection_WhenFirstNonMandatoryRowIsSelectedAndLastNonMandadoryRowIsNotSelectedAndTapFirstNonMandatoryRow_ShouldPresentDataAndFooterDescription() {
        sut.newInstallments = newInstallmentsArrayWithMandatory
        
        sut.newInstallments[2].isSelected = true
        sut.newInstallments[3].isSelected = true
        sut.newInstallments[4].isSelected = true
        sut.newInstallments[3].isSelectionAvailable = true
        sut.newInstallments[4].isSelectionAvailable = true
        
        sut.didTapRow(index: IndexPath(row: 2, section: 0))
        
        XCTAssertFalse(sut.newInstallments[0].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[1].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[2].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[3].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[4].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[5].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[6].isSelectionAvailable)
        
        XCTAssertFalse(sut.newInstallments[0].isSelected)
        XCTAssertTrue(sut.newInstallments[1].isSelected)
        XCTAssertFalse(sut.newInstallments[2].isSelected)
        XCTAssertFalse(sut.newInstallments[3].isSelected)
        XCTAssertFalse(sut.newInstallments[4].isSelected)
        XCTAssertFalse(sut.newInstallments[5].isSelected)
        XCTAssertFalse(sut.newInstallments[6].isSelected)
        
        XCTAssertEqual(presenterSpy.presentDataCallCount, 1)
        XCTAssertEqual(presenterSpy.setupFooterDescriptionCallCount, 1)
        XCTAssertEqual(presenterSpy.presentNextButtonStateCallCount, 1)
        XCTAssertEqual(presenterSpy.totalToPay, mockValue)
        XCTAssertEqual(presenterSpy.totalDiscount, mockDiscount)
        XCTAssertEqual(presenterSpy.totalInstallments, 1)
        XCTAssertEqual(presenterSpy.isNextButtonEnabled, true)
    }
    
    func testBothOrientationDeselection_WhenLastNonMandatoryRowIsSelectedAndFirstNonMandadoryRowIsNotSelectedAndTapLastNonMandatoryRow_ShouldPresentDataAndFooterDescription() {
        sut.newInstallments = newInstallmentsArrayWithMandatory
        
        sut.newInstallments[6].isSelected = true
        sut.newInstallments[5].isSelected = true
        sut.newInstallments[4].isSelected = true
        sut.newInstallments[3].isSelected = true
        sut.newInstallments[5].isSelectionAvailable = true
        sut.newInstallments[4].isSelectionAvailable = true
        sut.newInstallments[3].isSelectionAvailable = true
       
        sut.didTapRow(index: IndexPath(row: 6, section: 0))
        
        XCTAssertFalse(sut.newInstallments[0].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[1].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[2].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[3].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[4].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[5].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[6].isSelectionAvailable)
        
        XCTAssertFalse(sut.newInstallments[0].isSelected)
        XCTAssertTrue(sut.newInstallments[1].isSelected)
        XCTAssertFalse(sut.newInstallments[2].isSelected)
        XCTAssertFalse(sut.newInstallments[3].isSelected)
        XCTAssertFalse(sut.newInstallments[4].isSelected)
        XCTAssertFalse(sut.newInstallments[5].isSelected)
        XCTAssertFalse(sut.newInstallments[6].isSelected)
        
        XCTAssertEqual(presenterSpy.presentDataCallCount, 1)
        XCTAssertEqual(presenterSpy.setupFooterDescriptionCallCount, 1)
        XCTAssertEqual(presenterSpy.presentNextButtonStateCallCount, 1)
        XCTAssertEqual(presenterSpy.totalToPay, mockValue)
        XCTAssertEqual(presenterSpy.totalDiscount, mockDiscount)
        XCTAssertEqual(presenterSpy.totalInstallments, 1)
        XCTAssertEqual(presenterSpy.isNextButtonEnabled, true)
    }
    
    func testBothOrientationDeselection_WhenAllNonMandatoryRowIsSelectedAndTapMiddleNonMandatoryRow_ShouldPresentDataAndFooterDescription() {
        sut.newInstallments = newInstallmentsArrayWithMandatory
        
        sut.newInstallments[2].isSelected = true
        sut.newInstallments[3].isSelected = true
        sut.newInstallments[4].isSelected = true
        sut.newInstallments[5].isSelected = true
        sut.newInstallments[6].isSelected = true
        sut.newInstallments[3].isSelectionAvailable = true
        sut.newInstallments[4].isSelectionAvailable = true
        sut.newInstallments[5].isSelectionAvailable = true
        
        sut.didTapRow(index: IndexPath(row: 4, section: 0))
        
        XCTAssertFalse(sut.newInstallments[0].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[1].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[2].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[3].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[4].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[5].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[6].isSelectionAvailable)
        
        XCTAssertFalse(sut.newInstallments[0].isSelected)
        XCTAssertTrue(sut.newInstallments[1].isSelected)
        XCTAssertTrue(sut.newInstallments[2].isSelected)
        XCTAssertTrue(sut.newInstallments[3].isSelected)
        XCTAssertFalse(sut.newInstallments[4].isSelected)
        XCTAssertTrue(sut.newInstallments[5].isSelected)
        XCTAssertTrue(sut.newInstallments[6].isSelected)
        
        XCTAssertEqual(presenterSpy.presentDataCallCount, 1)
        XCTAssertEqual(presenterSpy.setupFooterDescriptionCallCount, 1)
        XCTAssertEqual(presenterSpy.presentNextButtonStateCallCount, 1)
        XCTAssertEqual(presenterSpy.totalToPay, mockValue * 5)
        XCTAssertEqual(presenterSpy.totalDiscount, mockDiscount * 5)
        XCTAssertEqual(presenterSpy.totalInstallments, 5)
        XCTAssertEqual(presenterSpy.isNextButtonEnabled, true)
    }
    
    
    //MARK: - Test TopDown Orientation
    func testTopDownSelection_WhenTapFirstNonMandatoryRow_ShouldPresentDataAndFooterDescription() {
        bankSelectionOrderMoked = .topDown
        sut.newInstallments = newInstallmentsArrayWithMandatory
        sut.didTapRow(index: IndexPath(row: 2, section: 0))
        
        XCTAssertFalse(sut.newInstallments[0].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[1].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[2].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[3].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[4].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[5].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[6].isSelectionAvailable)
        
        XCTAssertFalse(sut.newInstallments[0].isSelected)
        XCTAssertTrue(sut.newInstallments[1].isSelected)
        XCTAssertTrue(sut.newInstallments[2].isSelected)
        XCTAssertFalse(sut.newInstallments[3].isSelected)
        XCTAssertFalse(sut.newInstallments[4].isSelected)
        XCTAssertFalse(sut.newInstallments[5].isSelected)
        XCTAssertFalse(sut.newInstallments[6].isSelected)
        
        XCTAssertEqual(presenterSpy.presentDataCallCount, 1)
        XCTAssertEqual(presenterSpy.setupFooterDescriptionCallCount, 1)
        XCTAssertEqual(presenterSpy.presentNextButtonStateCallCount, 1)
        XCTAssertEqual(presenterSpy.totalToPay, mockValue * 2)
        XCTAssertEqual(presenterSpy.totalDiscount, mockDiscount * 2)
        XCTAssertEqual(presenterSpy.totalInstallments, 2)
        XCTAssertEqual(presenterSpy.isNextButtonEnabled, true)
    }
    
    func testTopDownDeselection_WhenAllNonMandatoryRowsIsSelectedAndTapFirstNonMandatoryRow_ShouldPresentDataAndFooterDescriptio() {
        bankSelectionOrderMoked = .topDown
        sut.newInstallments = newInstallmentsArrayWithMandatory
        
        sut.newInstallments[2].isSelected = true
        sut.newInstallments[3].isSelected = true
        sut.newInstallments[4].isSelected = true
        sut.newInstallments[5].isSelected = true
        sut.newInstallments[6].isSelected = true
        
        sut.newInstallments[2].isSelectionAvailable = true
        sut.newInstallments[3].isSelectionAvailable = true
        sut.newInstallments[4].isSelectionAvailable = true
        sut.newInstallments[5].isSelectionAvailable = true
        sut.newInstallments[6].isSelectionAvailable = true

        sut.didTapRow(index: IndexPath(row: 2, section: 0))
        
        XCTAssertFalse(sut.newInstallments[0].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[1].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[2].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[3].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[4].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[5].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[6].isSelectionAvailable)
        
        XCTAssertFalse(sut.newInstallments[0].isSelected)
        XCTAssertTrue(sut.newInstallments[1].isSelected)
        XCTAssertFalse(sut.newInstallments[2].isSelected)
        XCTAssertFalse(sut.newInstallments[3].isSelected)
        XCTAssertFalse(sut.newInstallments[4].isSelected)
        XCTAssertFalse(sut.newInstallments[5].isSelected)
        XCTAssertFalse(sut.newInstallments[6].isSelected)
        
        XCTAssertEqual(presenterSpy.presentDataCallCount, 1)
        XCTAssertEqual(presenterSpy.setupFooterDescriptionCallCount, 1)
        XCTAssertEqual(presenterSpy.presentNextButtonStateCallCount, 1)
        XCTAssertEqual(presenterSpy.totalToPay, mockValue)
        XCTAssertEqual(presenterSpy.totalDiscount, mockDiscount)
        XCTAssertEqual(presenterSpy.totalInstallments, 1)
        XCTAssertEqual(presenterSpy.isNextButtonEnabled, true)
    }
    
    func testTopDownDeselection_WhenOnlyLastNonMandatoryRowIsNotSelectedAndTapFirstNonMandatoryRow_ShouldPresentDataAndFooterDescription() {
        bankSelectionOrderMoked = .topDown
        sut.newInstallments = newInstallmentsArrayWithMandatory
        
        sut.newInstallments[2].isSelected = true
        sut.newInstallments[3].isSelected = true
        sut.newInstallments[4].isSelected = true
        sut.newInstallments[5].isSelected = true
        
        sut.newInstallments[2].isSelectionAvailable = true
        sut.newInstallments[3].isSelectionAvailable = true
        sut.newInstallments[4].isSelectionAvailable = true
        sut.newInstallments[5].isSelectionAvailable = true
        sut.newInstallments[6].isSelectionAvailable = true

        sut.didTapRow(index: IndexPath(row: 2, section: 0))
        
        XCTAssertFalse(sut.newInstallments[0].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[1].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[2].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[3].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[4].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[5].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[6].isSelectionAvailable)
        
        XCTAssertFalse(sut.newInstallments[0].isSelected)
        XCTAssertTrue(sut.newInstallments[1].isSelected)
        XCTAssertFalse(sut.newInstallments[2].isSelected)
        XCTAssertFalse(sut.newInstallments[3].isSelected)
        XCTAssertFalse(sut.newInstallments[4].isSelected)
        XCTAssertFalse(sut.newInstallments[5].isSelected)
        XCTAssertFalse(sut.newInstallments[6].isSelected)
        
        XCTAssertEqual(presenterSpy.presentDataCallCount, 1)
        XCTAssertEqual(presenterSpy.setupFooterDescriptionCallCount, 1)
        XCTAssertEqual(presenterSpy.presentNextButtonStateCallCount, 1)
        XCTAssertEqual(presenterSpy.totalToPay, mockValue)
        XCTAssertEqual(presenterSpy.totalDiscount, mockDiscount)
        XCTAssertEqual(presenterSpy.totalInstallments, 1)
        XCTAssertEqual(presenterSpy.isNextButtonEnabled, true)
    }
    
    //MARK: - Test BottomUp Orientation
    func testBottomUpSelection_WhenTapLastNonMandatoryRow_ShouldPresentDataAndFooterDescription() {
        bankSelectionOrderMoked = .bottomUp
        sut.newInstallments = newInstallmentsArrayWithMandatory
        
        sut.didTapRow(index: IndexPath(row: 6, section: 0))

        XCTAssertFalse(sut.newInstallments[0].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[1].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[2].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[3].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[4].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[5].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[6].isSelectionAvailable)

        XCTAssertFalse(sut.newInstallments[0].isSelected)
        XCTAssertTrue(sut.newInstallments[1].isSelected)
        XCTAssertFalse(sut.newInstallments[2].isSelected)
        XCTAssertFalse(sut.newInstallments[3].isSelected)
        XCTAssertFalse(sut.newInstallments[4].isSelected)
        XCTAssertFalse(sut.newInstallments[5].isSelected)
        XCTAssertTrue(sut.newInstallments[6].isSelected)

        XCTAssertEqual(presenterSpy.presentDataCallCount, 1)
        XCTAssertEqual(presenterSpy.setupFooterDescriptionCallCount, 1)
        XCTAssertEqual(presenterSpy.presentNextButtonStateCallCount, 1)
        XCTAssertEqual(presenterSpy.totalToPay, mockValue * 2)
        XCTAssertEqual(presenterSpy.totalDiscount, mockDiscount * 2)
        XCTAssertEqual(presenterSpy.totalInstallments, 2)
        XCTAssertEqual(presenterSpy.isNextButtonEnabled, true)
    }
    
    func testBottomUpDeselection_WhenAllNonMandatoryRowsIsSelectedAndTapLasNonMandatoryRow_ShouldPresentDataAndFooterDescription() {
        bankSelectionOrderMoked = .bottomUp
        sut.newInstallments = newInstallmentsArrayWithMandatory
        
        sut.newInstallments[2].isSelected = true
        sut.newInstallments[3].isSelected = true
        sut.newInstallments[4].isSelected = true
        sut.newInstallments[5].isSelected = true
        sut.newInstallments[6].isSelected = true
        
        sut.newInstallments[2].isSelectionAvailable = true
        sut.newInstallments[3].isSelectionAvailable = true
        sut.newInstallments[4].isSelectionAvailable = true
        sut.newInstallments[5].isSelectionAvailable = true
        sut.newInstallments[6].isSelectionAvailable = true

        sut.didTapRow(index: IndexPath(row: 6, section: 0))
        
        XCTAssertFalse(sut.newInstallments[0].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[1].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[2].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[3].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[4].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[5].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[6].isSelectionAvailable)
        
        XCTAssertFalse(sut.newInstallments[0].isSelected)
        XCTAssertTrue(sut.newInstallments[1].isSelected)
        XCTAssertFalse(sut.newInstallments[2].isSelected)
        XCTAssertFalse(sut.newInstallments[3].isSelected)
        XCTAssertFalse(sut.newInstallments[4].isSelected)
        XCTAssertFalse(sut.newInstallments[5].isSelected)
        XCTAssertFalse(sut.newInstallments[6].isSelected)
        
        XCTAssertEqual(presenterSpy.presentDataCallCount, 1)
        XCTAssertEqual(presenterSpy.setupFooterDescriptionCallCount, 1)
        XCTAssertEqual(presenterSpy.presentNextButtonStateCallCount, 1)
        XCTAssertEqual(presenterSpy.totalToPay, mockValue)
        XCTAssertEqual(presenterSpy.totalDiscount, mockDiscount)
        XCTAssertEqual(presenterSpy.totalInstallments, 1)
        XCTAssertEqual(presenterSpy.isNextButtonEnabled, true)
    }

    func testBottomUpDeselection_WhenOnlyFirstNonMandatoryRowIsNotSelectedAndTapLastNonMandatoryRow_ShouldPresentDataAndFooterDescription() {
        bankSelectionOrderMoked = .bottomUp
        sut.newInstallments = newInstallmentsArrayWithMandatory
        
        sut.newInstallments[3].isSelected = true
        sut.newInstallments[4].isSelected = true
        sut.newInstallments[5].isSelected = true
        sut.newInstallments[6].isSelected = true
        
        sut.newInstallments[2].isSelectionAvailable = true
        sut.newInstallments[3].isSelectionAvailable = true
        sut.newInstallments[4].isSelectionAvailable = true
        sut.newInstallments[5].isSelectionAvailable = true
        sut.newInstallments[6].isSelectionAvailable = true

        sut.didTapRow(index: IndexPath(row: 6, section: 0))
        
        XCTAssertFalse(sut.newInstallments[0].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[1].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[2].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[3].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[4].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[5].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[6].isSelectionAvailable)
        
        XCTAssertFalse(sut.newInstallments[0].isSelected)
        XCTAssertTrue(sut.newInstallments[1].isSelected)
        XCTAssertFalse(sut.newInstallments[2].isSelected)
        XCTAssertFalse(sut.newInstallments[3].isSelected)
        XCTAssertFalse(sut.newInstallments[4].isSelected)
        XCTAssertFalse(sut.newInstallments[5].isSelected)
        XCTAssertFalse(sut.newInstallments[6].isSelected)
        
        XCTAssertEqual(presenterSpy.presentDataCallCount, 1)
        XCTAssertEqual(presenterSpy.setupFooterDescriptionCallCount, 1)
        XCTAssertEqual(presenterSpy.presentNextButtonStateCallCount, 1)
        XCTAssertEqual(presenterSpy.totalToPay, mockValue)
        XCTAssertEqual(presenterSpy.totalDiscount, mockDiscount)
        XCTAssertEqual(presenterSpy.totalInstallments, 1)
        XCTAssertEqual(presenterSpy.isNextButtonEnabled, true)
    }
    
    //MARK: - Test SingleDirection Orientation
    func testSingleDirectionSelection_WhenTapFirstRowSelectable_ShouldPresentDataAndFooterDescription() {
        bankSelectionOrderMoked = .singleDirection
        sut.newInstallments = newInstallmentsArrayWithOutMandatory
        sut.didTapRow(index: IndexPath(row: 1, section: 0))

        XCTAssertFalse(sut.newInstallments[0].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[1].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[2].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[3].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[4].isSelectionAvailable)
        
        XCTAssertFalse(sut.newInstallments[0].isSelected)
        XCTAssertTrue(sut.newInstallments[1].isSelected)
        XCTAssertFalse(sut.newInstallments[2].isSelected)
        XCTAssertFalse(sut.newInstallments[3].isSelected)
        XCTAssertFalse(sut.newInstallments[4].isSelected)
        
        XCTAssertEqual(presenterSpy.presentDataCallCount, 1)
        XCTAssertEqual(presenterSpy.setupFooterDescriptionCallCount, 1)
        XCTAssertEqual(presenterSpy.presentNextButtonStateCallCount, 1)
        XCTAssertEqual(presenterSpy.totalToPay, mockValue)
        XCTAssertEqual(presenterSpy.totalDiscount, mockDiscount)
        XCTAssertEqual(presenterSpy.totalInstallments, 1)
        XCTAssertEqual(presenterSpy.isNextButtonEnabled, true)
    }
    
    func testSingleDirectionSelection_WhenTapLastRow_ShouldPresentDataAndFooterDescription() {
        bankSelectionOrderMoked = .singleDirection
        sut.newInstallments = newInstallmentsArrayWithOutMandatory
        sut.didTapRow(index: IndexPath(row: 4, section: 0))

        XCTAssertFalse(sut.newInstallments[0].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[1].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[2].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[3].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[4].isSelectionAvailable)
        
        XCTAssertFalse(sut.newInstallments[0].isSelected)
        XCTAssertFalse(sut.newInstallments[1].isSelected)
        XCTAssertFalse(sut.newInstallments[2].isSelected)
        XCTAssertFalse(sut.newInstallments[3].isSelected)
        XCTAssertTrue(sut.newInstallments[4].isSelected)
        
        XCTAssertEqual(presenterSpy.presentDataCallCount, 1)
        XCTAssertEqual(presenterSpy.setupFooterDescriptionCallCount, 1)
        XCTAssertEqual(presenterSpy.presentNextButtonStateCallCount, 1)
        XCTAssertEqual(presenterSpy.totalToPay, mockValue)
        XCTAssertEqual(presenterSpy.totalDiscount, mockDiscount)
        XCTAssertEqual(presenterSpy.totalInstallments, 1)
        XCTAssertEqual(presenterSpy.isNextButtonEnabled, true)
    }
    
    func testSingleDirectionDeselection_WhenHasAtLeastOneSelectableRowsNotSelectedAndTapFirstSelectableRow_ShouldPresentDataAndFooterDescription() {
        bankSelectionOrderMoked = .singleDirection
        sut.userSelectionOrder = .downwards
        sut.newInstallments = newInstallmentsArrayWithOutMandatory
        
        sut.newInstallments[1].isSelected = true
        sut.newInstallments[2].isSelected = true
        sut.newInstallments[3].isSelected = true

        sut.newInstallments[2].isSelectionAvailable = true
        sut.newInstallments[3].isSelectionAvailable = true
        sut.newInstallments[4].isSelectionAvailable = true

        sut.didTapRow(index: IndexPath(row: 1, section: 0))

        XCTAssertFalse(sut.newInstallments[0].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[1].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[2].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[3].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[4].isSelectionAvailable)

        XCTAssertFalse(sut.newInstallments[0].isSelected)
        XCTAssertFalse(sut.newInstallments[1].isSelected)
        XCTAssertFalse(sut.newInstallments[2].isSelected)
        XCTAssertFalse(sut.newInstallments[3].isSelected)
        XCTAssertFalse(sut.newInstallments[4].isSelected)

        XCTAssertEqual(presenterSpy.presentDataCallCount, 1)
        XCTAssertEqual(presenterSpy.setupFooterDescriptionCallCount, 1)
        XCTAssertEqual(presenterSpy.presentNextButtonStateCallCount, 1)
        XCTAssertEqual(presenterSpy.totalToPay, 0)
        XCTAssertEqual(presenterSpy.totalDiscount, 0)
        XCTAssertEqual(presenterSpy.totalInstallments, 0)
        XCTAssertEqual(presenterSpy.isNextButtonEnabled, false)
        
    }
 
    func testSingleDirectionDeselection_WhenHasAtLeastOneSelectableRowsNotSelectedAndTapLastSelectableRow_ShouldPresentDataAndFooterDescription() {
        bankSelectionOrderMoked = .singleDirection
        sut.userSelectionOrder = .upwards
        sut.newInstallments = newInstallmentsArrayWithOutMandatory
        
        sut.newInstallments[2].isSelected = true
        sut.newInstallments[3].isSelected = true
        sut.newInstallments[4].isSelected = true

        sut.newInstallments[1].isSelectionAvailable = true
        sut.newInstallments[2].isSelectionAvailable = true
        sut.newInstallments[3].isSelectionAvailable = true

        sut.didTapRow(index: IndexPath(row: 4, section: 0))

        XCTAssertFalse(sut.newInstallments[0].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[1].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[2].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[3].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[4].isSelectionAvailable)

        XCTAssertFalse(sut.newInstallments[0].isSelected)
        XCTAssertFalse(sut.newInstallments[1].isSelected)
        XCTAssertFalse(sut.newInstallments[2].isSelected)
        XCTAssertFalse(sut.newInstallments[3].isSelected)
        XCTAssertFalse(sut.newInstallments[4].isSelected)

        XCTAssertEqual(presenterSpy.presentDataCallCount, 1)
        XCTAssertEqual(presenterSpy.setupFooterDescriptionCallCount, 1)
        XCTAssertEqual(presenterSpy.presentNextButtonStateCallCount, 1)
        XCTAssertEqual(presenterSpy.totalToPay, 0)
        XCTAssertEqual(presenterSpy.totalDiscount, 0)
        XCTAssertEqual(presenterSpy.totalInstallments, 0)
        XCTAssertEqual(presenterSpy.isNextButtonEnabled, false)
    }
    
    func testSingleDirectionDeselection_WhenAllSelectableRowsIsSelectedAndTapFirstSelectableRow_ShouldPresentDataAndFooterDescription() {
        bankSelectionOrderMoked = .singleDirection
        sut.userSelectionOrder = .downwards
        sut.newInstallments = newInstallmentsArrayWithOutMandatory
        
        assertOneDirectionDeselection_WhenAllSelectableRowsIsSelected(tappedRow: 1)
    }
    
    func testSingleDirectionDeselection_WhenDoesNotHaveMandatoryRowAndAllSelectableRowsIsSelectedAndTapLastSelectableRow_ShouldPresentDataAndFooterDescription()  {
        bankSelectionOrderMoked = .singleDirection
        sut.userSelectionOrder = .upwards
        sut.newInstallments = newInstallmentsArrayWithOutMandatory
        
        assertOneDirectionDeselection_WhenAllSelectableRowsIsSelected(tappedRow: 4)
    }
    
    func assertOneDirectionDeselection_WhenAllSelectableRowsIsSelected(tappedRow: Int) {
        sut.newInstallments[1].isSelected = true
        sut.newInstallments[2].isSelected = true
        sut.newInstallments[3].isSelected = true
        sut.newInstallments[4].isSelected = true

        sut.newInstallments[2].isSelectionAvailable = true
        sut.newInstallments[3].isSelectionAvailable = true
        sut.newInstallments[4].isSelectionAvailable = true

        sut.didTapRow(index: IndexPath(row: tappedRow, section: 0))

        XCTAssertFalse(sut.newInstallments[0].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[1].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[2].isSelectionAvailable)
        XCTAssertFalse(sut.newInstallments[3].isSelectionAvailable)
        XCTAssertTrue(sut.newInstallments[4].isSelectionAvailable)

        XCTAssertFalse(sut.newInstallments[0].isSelected)
        XCTAssertFalse(sut.newInstallments[1].isSelected)
        XCTAssertFalse(sut.newInstallments[2].isSelected)
        XCTAssertFalse(sut.newInstallments[3].isSelected)
        XCTAssertFalse(sut.newInstallments[4].isSelected)

        XCTAssertEqual(presenterSpy.presentDataCallCount, 1)
        XCTAssertEqual(presenterSpy.setupFooterDescriptionCallCount, 1)
        XCTAssertEqual(presenterSpy.presentNextButtonStateCallCount, 1)
        XCTAssertEqual(presenterSpy.totalToPay, 0)
        XCTAssertEqual(presenterSpy.totalDiscount, 0)
        XCTAssertEqual(presenterSpy.totalInstallments, 0)
        XCTAssertEqual(presenterSpy.isNextButtonEnabled, false)
    }
}
