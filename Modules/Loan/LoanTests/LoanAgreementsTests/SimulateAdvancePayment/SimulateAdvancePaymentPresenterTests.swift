import XCTest
import Core
import AssetsKit
@testable import Loan

final private class SimulateAdvancePaymentCoordinatorSpy: SimulateAdvancePaymentCoordinating {
    var viewController: UIViewController?
    
    private(set) var performActionCallCount = 0
    private(set) var performedAction: SimulateAdvancePaymentAction?
       
    func perform(action: SimulateAdvancePaymentAction) {
        performActionCallCount += 1
        performedAction = action
    }
}

final private class SimulateAdvancePaymentControllerSpy: SimulateAdvancePaymentDisplaying {
    private(set) var displayItemsCallCount = 0
    private(set) var displayFooterDescriptionCallCount = 0
    private(set) var showErrorDialogCallCount = 0
    private(set) var displayTimeExceededWarningCallCount = 0
    private(set) var displayNextButtonStateCallCount = 0
    
    private(set) var installments: [SimulateAdvancePaymentViewModel]?
    private(set) var totalInstallments: String?
    private(set) var previousValue: Double?
    private(set) var totalValue: Double?
    private(set) var previousDiscount: Double?
    private(set) var totalDiscount: Double?
    private(set) var isNextButtonEnabled: Bool?
    
    func displayItems(data: [SimulateAdvancePaymentViewModel]) {
        displayItemsCallCount += 1
        installments = data
    }
    
    func displayFooterDescription(installments: String,
                                  previousValue: Double,
                                  totalValue: Double,
                                  previousDiscount: Double,
                                  totalDiscount: Double) {
        displayFooterDescriptionCallCount += 1
        self.totalInstallments = installments
        self.previousValue = previousValue
        self.totalValue = totalValue
        self.previousDiscount = previousDiscount
        self.totalDiscount = totalDiscount
    }
    
    func displayNextButtonState(isNextButtonEnabled: Bool) {
        displayNextButtonStateCallCount += 1
        self.isNextButtonEnabled = isNextButtonEnabled
    }
    
    func showErrorDialog(imageAsset: UIImage, title: String, subtitle: String) {
        showErrorDialogCallCount += 1
    }
    
    func displayTimeExceededWarning() {
        displayTimeExceededWarningCallCount += 1
    }
}

final class SimulateAdvancePaymentPresenterTests: XCTestCase {
    private let coordinatorSpy = SimulateAdvancePaymentCoordinatorSpy()
    private let controllerSpy = SimulateAdvancePaymentControllerSpy()
    
    private lazy var sut: SimulateAdvancePaymentPresenting = {
        let presenter = SimulateAdvancePaymentPresenter(coordinator: coordinatorSpy)
        presenter.viewController = controllerSpy
        return presenter
    }()
    
    let installment = Installment(
        id: "1",
        originalValue: 1056.61,
        value: 1009.58,
        penaltyValue: 0,
        order: 1,
        status: .open,
        dueDate: Date(),
        paymentDate: nil,
        daysOverDue: -28,
        isPayable: true,
        discountValue: 0
    )
    
    func testPresentData_ShouldDisplayData() {
        let newInstallments = [
            SimulateAdvancePaymentViewModel(installment: installment,
                                            isSelectionAvailable: false),
            SimulateAdvancePaymentViewModel(installment: installment,
                                            isSelectionAvailable: true),
        ]
        
        sut.presentData(items: newInstallments)
        
        XCTAssertEqual(controllerSpy.displayItemsCallCount, 1)
    }
    
    func testSetupFooterDescription_ShouldDisplayFooterDescription() {
        let discount = 20.0
        let previousDiscount = 0.0
        let previousValue = 0.0
        sut.setupFooterDescription(valueToPay: 60.6,
                                   previousValue: previousValue,
                                   totalInstallments: 3,
                                   previousDiscount: previousDiscount,
                                   totalDiscount: discount)
        
        XCTAssertEqual(controllerSpy.displayFooterDescriptionCallCount, 1)
        XCTAssertEqual(controllerSpy.totalDiscount, discount)
        XCTAssertEqual(controllerSpy.previousValue, previousValue)
        XCTAssertEqual(controllerSpy.previousDiscount, previousDiscount)
    }
    
    func testPresentNextButtonState_ShouldDisplayNextButtonState() {
        sut.presentNextButtonState(isNextButtonEnabled: true)
        
        XCTAssertEqual(controllerSpy.displayNextButtonStateCallCount, 1)
        XCTAssertEqual(controllerSpy.isNextButtonEnabled, true)
    }
    
    func testPresentError_ShouldDisplayError() {
        sut.presentError()
        
        XCTAssertEqual(controllerSpy.showErrorDialogCallCount, 1)
    }
    
    func testPresentError_ShouldDisplayConnectionError() {
        sut.presentConnectionError()
        
        XCTAssertEqual(controllerSpy.showErrorDialogCallCount, 1)
    }
    
    func testPresentError_ShouldDisplayTimeExceededWarning() {
        sut.presentTimeExceededWarning()
        
        XCTAssertEqual(controllerSpy.displayTimeExceededWarningCallCount, 1)
    }
    
    func testOpenHireAdvancePayment_ShouldPerformeHireAdvancePaymentAction() {
        sut.openHireAdvancePayment(contractId: "ABC1234", installments: [installment])

        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction,
                       .hireAdvancePayment(contractId: "ABC1234",
                                           installments: [installment]))
    }
    
    func testPop_ShouldPerformePopAction() {
        sut.pop()
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .pop)
    }
    
    func testOpenFaq_ShouldPerformeOpenFaqAction() {
        let FAQMoked: FAQArticle = .bankSlip
        sut.openFAQ(article: FAQMoked)
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .openFAQ(FAQMoked))
    }
}
