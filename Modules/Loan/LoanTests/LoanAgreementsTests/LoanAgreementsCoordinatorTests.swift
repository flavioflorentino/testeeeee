import XCTest
@testable import Loan

final class LoanAgreementsCoordinatorTests: XCTestCase {
    private let navigationSpy = UINavigationControllerSpy()
    private let delegateSpy = LoanFlowCoordinatingSpy()
    private lazy var sut = LoanAgreementsCoordinator(from: navigationSpy, delegate: delegateSpy)
    
    func testPerformAction_WhenActionIsMyLoanAgreements_ShouldStartMyLoanAgreementsView() {
        sut.perform(action: .myLoanAgreements(origin: .settingsMenu))
        
        XCTAssertEqual(navigationSpy.pushCallCount, 1)
        XCTAssert(navigationSpy.pushedViewController is MyLoanAgreementsDisplay)
    }
    
    func testPerformAction_WhenActionIsInstallments_ShouldStartInstallmentsView() {
        sut.perform(action: .installments(contractId: "1", origin: .myLoanAgreements))
        
        XCTAssertEqual(navigationSpy.pushCallCount, 1)
        XCTAssert(navigationSpy.pushedViewController is InstallmentsDisplay)
    }
    
    func testPerformActionClose_ShouldClose() {
        sut.perform(action: .close)
        
        XCTAssertEqual(delegateSpy.closeCallCount, 1)
    }
}
