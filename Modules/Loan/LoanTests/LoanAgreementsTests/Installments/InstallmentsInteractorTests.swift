import AnalyticsModule
import Core
import FeatureFlag
import XCTest
@testable import Loan

private final class MyLoanAgreementsServiceSpy: MyLoanAgreementsServicing {
    let loan = LoanAgreement(id: "1a1a1a",
                              bankId: "212",
                              loanGoal:
                                LoanGoal(category: .build, description: ""),
                              totalInstallments: 10,
                              totalInstallmentsAlreadyPaid: 2,
                              status: "ACTIVE")
    lazy var response: Result<LoanAgreementsResponse, ApiError> = .success(LoanAgreementsResponse(loans: [loan]))
    
    func getLoanAgreements(completion: @escaping (Result<LoanAgreementsResponse, ApiError>) -> Void) {
        completion(response)
    }
}

private final class InstallmentsServiceSpy: InstallmentsServicing {
    let payableInstallment = Installment(
        id: "1",
        originalValue: 1056.61,
        value: 1009.58,
        penaltyValue: 0,
        order: 1,
        status: .open,
        dueDate: Date(),
        paymentDate: nil,
        daysOverDue: -28,
        isPayable: true,
        discountValue: 0
    )
    
    var statusMocked: InstallmentStatus = .open
    var discountValueMocked: Double = 20.68
    lazy var unpayableInstallment = Installment(
        id: "1",
        originalValue: 1056.61,
        value: 1009.58,
        penaltyValue: 0,
        order: 1,
        status: statusMocked,
        dueDate: Date(),
        paymentDate: nil,
        daysOverDue: -28,
        isPayable: false,
        discountValue: 0
    )
    
    lazy var response: Result<InstallmentsResponse, ApiError> = .success(
        InstallmentsResponse(outstandingBalance: 1056.61,
                             discountValueTotal: discountValueMocked,
                             pendingList: [payableInstallment, unpayableInstallment],
                             paidList: [payableInstallment, unpayableInstallment],
                             selectionOrder: .both)
    )
    
    func getInstallments(of agreementId: String, completion: @escaping (Result<InstallmentsResponse, ApiError>) -> Void) {
        completion(response)
    }
}

private final class InstallmentsPresenterSpy: InstallmentsPresenting {
    var viewController: InstallmentsDisplay?
    
    private(set) var setupDescriptionCallCount = 0
    private(set) var presentPaidInstallmentsCallCount = 0
    private(set) var presentPendingInstallmentsCallCount = 0
    private(set) var openPaymentMethodsCallCount = 0
    private(set) var startLoadingCallCount = 0
    private(set) var stopLoadingCallCount = 0
    private(set) var closeCallCount = 0
    private(set) var presentErrorCallCount = 0
    private(set) var openFAQCallCount = 0
    private(set) var setTabSegmentControlCallCount = 0
    private(set) var openSimulateAdvancePaymentCallCount = 0
    private(set) var presentTotalDiscountCallCount = 0
    private(set) var showFooterViewIfNeededCallCount = 0
    private(set) var popCallCount = 0
    private(set) var presentFooterViewIfNeedCallCount = 0
    private(set) var showPayInAdvanceIsNotAvailableWarningCallCount = 0
    private(set) var showPayInAdvanceIsAvailableWarningCallCount = 0
    private(set) var openNegotiatedInstallmentCallCount = 0
    private(set) var presentPaidEmptyViewCallCount = 0
    private(set) var presentOpenEmptyViewCallCount = 0
    private(set) var presentPaymentFeatureCallCount = 0
    
    private(set) var segmentIndex: Int?
    private(set) var shouldShowFooterView: Bool?
 
    func setupDescription(valueToPay value: Double) {
        setupDescriptionCallCount += 1
    }
    
    func presentPaidList(_ paidList: [Installment]) {
        presentPaidInstallmentsCallCount += 1
    }
    
    func presentPendingList(_ pendingList: [Installment]) {
        presentPendingInstallmentsCallCount += 1
    }
    
    func presentPaymentFeature() {
        presentPaymentFeatureCallCount += 1
    }
    
    func openPaymentMethods(contractId: String, installment: Installment) {
        openPaymentMethodsCallCount += 1
    }
    
    func startLoading() {
        startLoadingCallCount += 1
    }
    
    func stopLoading() {
        stopLoadingCallCount += 1
    }
    
    func close() {
        closeCallCount += 1
    }
    
    func pop() {
        popCallCount += 1
    }
    
    func presentError() {
        presentErrorCallCount += 1
    }

    func openFAQ(_ article: FAQArticle) {
        openFAQCallCount += 1
    }
    
    func setTabSegmentControl(index: Int) {
        setTabSegmentControlCallCount += 1
        segmentIndex = index
    }
    
    func openSimulateAdvancePayment(contractId: String,
                                    installments: [Installment],
                                    selectionOrder: InstallmentSelectionOrder) {
        openSimulateAdvancePaymentCallCount += 1
    }
    
    func presentTotalDiscount(value: Double) {
        presentTotalDiscountCallCount += 1
    }
    
    func presentFooterViewIfNeeded(shouldShow: Bool) {
        presentFooterViewIfNeedCallCount += 1
        shouldShowFooterView = shouldShow
    }
    
    func showPayInAdvanceIsNotAvailableWarning() {
        showPayInAdvanceIsNotAvailableWarningCallCount += 1
    }
    
    func showPayInAdvanceIsAvailableWarning() {
        showPayInAdvanceIsAvailableWarningCallCount += 1
    }
    
    func openNegotiatedInstallment(installment: Installment) {
        openNegotiatedInstallmentCallCount += 1
    }
    
    func displayPaidEmptyView() {
        presentPaidEmptyViewCallCount += 1
    }
    
    func displayOpenEmptyView() {
        presentOpenEmptyViewCallCount += 1
    }
}

final class InstallmentsInteractorTests: XCTestCase {
    private let serviceSpy = InstallmentsServiceSpy()
    private let presenterSpy = InstallmentsPresenterSpy()
    private lazy var dependenciesSpy = LoanDependencyContainerMock(AnalyticsSpy(), featureManagerMock)

    private let featureManagerMock: FeatureManagerMock = {
        let featureManagerMock = FeatureManagerMock()
        featureManagerMock.override(keys: .isAnticipateLoanInstallmentsAvailable, with: false)
        featureManagerMock.override(keys: .opsFeatureLoanPayment, with: true)
        return featureManagerMock
    }()
    
    private lazy var sut = InstallmentsInteractor(service: serviceSpy,
                                                  presenter: presenterSpy,
                                                  from: .myLoanAgreements,
                                                  dependencies: dependenciesSpy,
                                                  contractId: "1")

    func newsInstallments (origin: OriginType) -> InstallmentsInteractor {
        return InstallmentsInteractor(service: serviceSpy,
                                      presenter: presenterSpy,
                                      from: origin,
                                      dependencies: dependenciesSpy,
                                      contractId: "1")
    }
    
    func testFetchInstallments_WhenFetchSucceeds_ShouldPresentInstallments() {
        sut.fetchInstallments()
        
        XCTAssertEqual(presenterSpy.startLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.presentPaidInstallmentsCallCount, 1)
        XCTAssertEqual(presenterSpy.presentPendingInstallmentsCallCount, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.setupDescriptionCallCount, 1)
        XCTAssertEqual(presenterSpy.presentTotalDiscountCallCount, 1)
        XCTAssertEqual(presenterSpy.presentPaidEmptyViewCallCount, 0)
        XCTAssertEqual(presenterSpy.presentOpenEmptyViewCallCount, 0)
        XCTAssertEqual(presenterSpy.presentPaymentFeatureCallCount, 1)
    }
    
    func testFetchAgreements_WhenFetchError_ShouldPresentError() {
        serviceSpy.response = .failure(.serverError)
        sut.fetchInstallments()
        
        XCTAssertEqual(presenterSpy.startLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.presentErrorCallCount, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallCount, 1)
    }

    func testDidTapRow_WhenSelectPayableInstallment_ShouldPresentPaymentsMethod() {
        sut.fetchInstallments()
        sut.didTapRow(index: IndexPath(row: 0, section: 0))

        XCTAssertEqual(presenterSpy.openPaymentMethodsCallCount, 1)
        XCTAssertEqual(presenterSpy.showPayInAdvanceIsNotAvailableWarningCallCount, 0)
    }
    
    func testDidTapRow_WhenSelectUnpayableInstallmentAndStatusIsRenegotiated_ShouldPresentPaymentsMethod() {
        serviceSpy.statusMocked = .renegotiated
        sut.fetchInstallments()
        
        sut.didTapRow(index: IndexPath(row: 1, section: 0))

        XCTAssertEqual(presenterSpy.openNegotiatedInstallmentCallCount, 1)
    }

    func testDidTapRow_WhenSelectUnpayableInstallmentAndStatusIsNotRenegotiatedAndFeatureFlagIsOff_ShouldPresentPayInAdvanceIsNotAvailableWarning() {
        sut.fetchInstallments()
        featureManagerMock.override(key: .isAnticipateLoanInstallmentsAvailable, with: false)
        sut.didTapRow(index: IndexPath(row: 1, section: 0))

        XCTAssertEqual(presenterSpy.openPaymentMethodsCallCount, 0)
        XCTAssertEqual(presenterSpy.showPayInAdvanceIsNotAvailableWarningCallCount, 1)
    }
    
    func testDidTapRow_WhenSelectUnpayableInstallmentAndStatusIsNotRenegotiatedAndFeatureFlagIsOn_ShouldPresentPayInAdvanceIsAvailableWarning() {
        sut.fetchInstallments()
        featureManagerMock.override(key: .isAnticipateLoanInstallmentsAvailable, with: true)
        sut.didTapRow(index: IndexPath(row: 1, section: 0))

        XCTAssertEqual(presenterSpy.openPaymentMethodsCallCount, 0)
        XCTAssertEqual(presenterSpy.showPayInAdvanceIsAvailableWarningCallCount, 1)
    }
    
    func testClose_ShoulPresentClose() {
        sut.close()
        
        XCTAssertEqual(presenterSpy.closeCallCount, 1)
    }
    
    func testPop_ShoulPresentPop() {
        sut.pop()
        
        XCTAssertEqual(presenterSpy.popCallCount, 1)
    }
    
    func tesOpenFaq_ShouldPresentFAQ() {
        sut.openFAQ()

        XCTAssertEqual(presenterSpy.openFAQCallCount, 1)
    }
    
    func testUpdateView_WhenComesFromMyAgreements_ShouldLogEventDidEnterFromMyAgreements() throws {
        let expectedEvent = LoanEvent.didEnter(to: .installments, from: .myLoanAgreements)
        
        sut.setupAnalytics()
        
        try assertOrigins(with: expectedEvent, originType: .myLoanAgreements)
    }
    
    private func assertOrigins(with expectedEvent: LoanEvent, originType: OriginType) throws {
        let analytics = try XCTUnwrap(dependenciesSpy.analytics as? AnalyticsSpy)
        let origin = try XCTUnwrap(analytics.value(forKey: "origin"))

        XCTAssertTrue(analytics.equals(to: expectedEvent.event()))
        XCTAssertEqual(origin, originType.value)
    }
    
    func testDidTapNext_WhenFooterButtonIsTapped_ShouldOpenSimulateAdvancePayment() {
        sut.fetchInstallments()
        sut.didTapNext()
        
        XCTAssertEqual(presenterSpy.openSimulateAdvancePaymentCallCount, 1)
    }
    
    func testsetTabSegmentControl_WhenOriginIsNotCharging_ShouldSetTabSegmentControlWithIndexZero() {
        sut.setupIndexSegmentControl()
        sut.fetchInstallments()
        
        XCTAssertEqual(presenterSpy.setTabSegmentControlCallCount, 1)
        XCTAssertEqual(presenterSpy.segmentIndex, 0)
        XCTAssertEqual(presenterSpy.presentFooterViewIfNeedCallCount, 1)
        XCTAssertEqual(presenterSpy.shouldShowFooterView, false)
    }
    
    func testsetTabSegmentControl_WhenOriginIsChargingAndHasTypeEqualToPaid_ShouldSetTabSegmentControlWithIndexOne() {
        let paramsMock = ["type": "paid"]
        let newSut = newsInstallments(origin: .charge(params: paramsMock))
        
        newSut.setupIndexSegmentControl()
        newSut.fetchInstallments()

        XCTAssertEqual(presenterSpy.setTabSegmentControlCallCount, 1)
        XCTAssertEqual(presenterSpy.segmentIndex, 1)
        XCTAssertEqual(presenterSpy.presentFooterViewIfNeedCallCount, 1)
        XCTAssertEqual(presenterSpy.shouldShowFooterView, false)
    }
    
    func testsetTabSegmentControl_WhenOriginIsChargingAndDoesNotHaveTypeEqualToPaid_ShouldSetTabSegmentControlWithIndexZero() {
        let paramsMock = ["type": "xx"]
        let newSut = newsInstallments(origin: .charge(params: paramsMock))
        
        featureManagerMock.override(key: .isAnticipateLoanInstallmentsAvailable, with: true)

        newSut.setupIndexSegmentControl()
        newSut.fetchInstallments()
        
        XCTAssertEqual(presenterSpy.setTabSegmentControlCallCount, 1)
        XCTAssertEqual(presenterSpy.segmentIndex, 0)
        XCTAssertEqual(presenterSpy.presentFooterViewIfNeedCallCount, 1)
        XCTAssertEqual(presenterSpy.shouldShowFooterView, true)
    }
    
    func testShowFooterViewIfNeeded_WhenFeatureFlagIsOnAndHasTypeEqualToPaidAndHasNoDiscount_ShouldNotPresentShowFooterViewIfNeeded() {
        let paramsMock = ["type": "paid"]
        let newSut = newsInstallments(origin: .charge(params: paramsMock))
        
        featureManagerMock.override(key: .isAnticipateLoanInstallmentsAvailable, with: true)
        serviceSpy.discountValueMocked = 0.00

        newSut.setupIndexSegmentControl()
        newSut.fetchInstallments()
        
        XCTAssertEqual(presenterSpy.setTabSegmentControlCallCount, 1)
        XCTAssertEqual(presenterSpy.segmentIndex, 1)
        XCTAssertEqual(presenterSpy.presentFooterViewIfNeedCallCount, 1)
        XCTAssertEqual(presenterSpy.shouldShowFooterView, false)
    }
    
    func testShowFooterViewIfNeeded_WhenFeatureFlagIsOnAndHasTypeEqualToPaidHasDiscount_ShouldNotPresentShowFooterViewIfNeeded() {
        let paramsMock = ["type": "paid"]
        let newSut = newsInstallments(origin: .charge(params: paramsMock))
        
        featureManagerMock.override(key: .isAnticipateLoanInstallmentsAvailable, with: true)
        newSut.setupIndexSegmentControl()
        newSut.fetchInstallments()
        
        XCTAssertEqual(presenterSpy.setTabSegmentControlCallCount, 1)
        XCTAssertEqual(presenterSpy.segmentIndex, 1)
        XCTAssertEqual(presenterSpy.presentFooterViewIfNeedCallCount, 1)
        XCTAssertEqual(presenterSpy.shouldShowFooterView, false)
    }
    
    func testShowFooterViewIfNeeded_WhenFeatureFlagIsOnAndHasNoDiscount_ShouldNotPresentShowFooterViewIfNeeded() {
        let paramsMock = ["type": "xx"]
        let newSut = newsInstallments(origin: .charge(params: paramsMock))
        
        featureManagerMock.override(key: .isAnticipateLoanInstallmentsAvailable, with: true)
        serviceSpy.discountValueMocked = 0.00
        newSut.setupIndexSegmentControl()
        newSut.fetchInstallments()

        XCTAssertEqual(presenterSpy.setTabSegmentControlCallCount, 1)
        XCTAssertEqual(presenterSpy.segmentIndex, 0)
        XCTAssertEqual(presenterSpy.presentFooterViewIfNeedCallCount, 1)
        XCTAssertEqual(presenterSpy.shouldShowFooterView, false)
    }
    
    func testShowFooterViewIfNeeded_WhenFeatureFlagIsOffAndHasNoDiscount_ShouldNotPresentShowFooterViewIfNeededWhenSetTabSegmentControlWithIndexZero() {
        let paramsMock = ["type": "xx"]
        let newSut = newsInstallments(origin: .charge(params: paramsMock))
        
        serviceSpy.discountValueMocked = 0.00
        newSut.setupIndexSegmentControl()
        newSut.fetchInstallments()

        XCTAssertEqual(presenterSpy.setTabSegmentControlCallCount, 1)
        XCTAssertEqual(presenterSpy.segmentIndex, 0)
        XCTAssertEqual(presenterSpy.presentFooterViewIfNeedCallCount, 1)
        XCTAssertEqual(presenterSpy.shouldShowFooterView, false)
    }
    
    func testShowFooterViewIfNeeded_WhenFeatureFlagIsOffAndHasTypeEqualToPaidAndHasNoDiscount_ShouldNotPresentShowFooterViewIfNeededWhenSetTabSegmentControlWithIndexOne() {
        let paramsMock = ["type": "paid"]
        let newSut = newsInstallments(origin: .charge(params: paramsMock))
        
        serviceSpy.discountValueMocked = 0.00
        newSut.setupIndexSegmentControl()
        newSut.fetchInstallments()

        XCTAssertEqual(presenterSpy.setTabSegmentControlCallCount, 1)
        XCTAssertEqual(presenterSpy.segmentIndex, 1)
        XCTAssertEqual(presenterSpy.presentFooterViewIfNeedCallCount, 1)
        XCTAssertEqual(presenterSpy.shouldShowFooterView, false)
    }
        
    func testShowFooterViewIfNeeded_WhenFeatureFlagIsOn_ShouldPresentShowFooterViewIfNeededWithFalseParameter() {
        featureManagerMock.override(key: .isAnticipateLoanInstallmentsAvailable, with: true)
        
        sut.showFooterViewIfNeeded(indexSegmentControl: 1)
        
        XCTAssertEqual(presenterSpy.presentFooterViewIfNeedCallCount, 1)
        XCTAssertEqual(presenterSpy.shouldShowFooterView, false)
    }
    
    func testShowEmptyView_ShouldPresentOpenEmptyView() {
        serviceSpy.response = .success(InstallmentsResponse(outstandingBalance: 1056.61,
                                                            discountValueTotal: 20.68,
                                                            pendingList: [],
                                                            paidList: [serviceSpy.payableInstallment], selectionOrder: .both))
        sut.fetchInstallments()
        
        XCTAssertEqual(presenterSpy.presentPaidEmptyViewCallCount, 0)
        XCTAssertEqual(presenterSpy.presentOpenEmptyViewCallCount, 1)
    }
    
    func testShowEmptyView_ShouldPresentPaidEmptyView() {
        serviceSpy.response = .success(InstallmentsResponse(outstandingBalance: 1056.61,
                                                            discountValueTotal: 20.68,
                                                            pendingList: [serviceSpy.payableInstallment],
                                                            paidList: [], selectionOrder: .both))
        sut.fetchInstallments()
        
        XCTAssertEqual(presenterSpy.presentPaidEmptyViewCallCount, 1)
        XCTAssertEqual(presenterSpy.presentOpenEmptyViewCallCount, 0)
    }
    
    func testShowEmptyView_ShouldPresentPaidAndOpenEmptyView() {
        serviceSpy.response = .success(InstallmentsResponse(outstandingBalance: 1056.61,
                                                            discountValueTotal: 20.68,
                                                            pendingList: [],
                                                            paidList: [], selectionOrder: .both))
        sut.fetchInstallments()
        
        XCTAssertEqual(presenterSpy.presentPaidEmptyViewCallCount, 1)
        XCTAssertEqual(presenterSpy.presentOpenEmptyViewCallCount, 1)
    }
}
