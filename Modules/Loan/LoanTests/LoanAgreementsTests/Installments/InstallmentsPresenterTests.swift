import AnalyticsModule
import Core
import XCTest
import FeatureFlag
import AssetsKit
import UI

@testable import Loan

private final class InstallmentsCoordinatorDumb: InstallmentsCoordinating {
    var viewController: UIViewController?
    private(set) var performActionCallCount = 0
    var performedAction: InstallmentsAction?

    func perform(action: InstallmentsAction) {
        performActionCallCount += 1
        performedAction = action
    }
}

private final class InstallmentsControllerSpy: InstallmentsDisplay {
    private(set) var displayPendingCallCount = 0
    private(set) var displayPaidCallCount = 0
    private(set) var displayDescriptionCallCount = 0
    private(set) var enablePaymentFeatureCallCount = 0
    private(set) var startLoadingCallCount = 0
    private(set) var stopLoadingCallCount = 0
    private(set) var displayErrorCallCount = 0
    private(set) var displayTabSegmentControlCallCount = 0
    private(set) var displayTotalDiscountCallCount = 0
    private(set) var displayFooterViewAnimationCallCount = 0
    private(set) var displayPayInAdvanceIsAvailableCallCount = 0
    private(set) var displayEmptyPaidViewCallCount = 0
    private(set) var displayEmptyOpenViewCallCount = 0

    private(set) var descriptionDisplayed: NSAttributedString?
    private(set) var pendingList: [InstallmentViewModel] = []
    private(set) var paidList: [InstallmentViewModel] = []
    private(set) var segmentControlIndex: Int?
    private(set) var totalDiscount: String?
    private(set) var showFooterView: Bool?
    
    func displayPending(data: [InstallmentViewModel]) {
        displayPendingCallCount += 1
        pendingList = data
    }
    
    func displayPaid(data: [InstallmentViewModel]) {
        displayPaidCallCount += 1
        paidList = data
    }
    
    func displayDescription(text: NSAttributedString) {
        displayDescriptionCallCount += 1
        descriptionDisplayed = text
    }
    
    func enablePaymentFeature() {
        enablePaymentFeatureCallCount += 1
    }
    
    func startLoading() {
        startLoadingCallCount += 1
    }
    
    func stopLoading() {
        stopLoadingCallCount += 1
    }
    
    func displayError() {
        displayErrorCallCount += 1
    }
    
    func displayTabSegmentControl(index: Int) {
        displayTabSegmentControlCallCount += 1
        segmentControlIndex = index
    }
    
    func displayTotalDiscount(text: String) {
        displayTotalDiscountCallCount += 1
        totalDiscount = text
    }
 
    func displayPayInAdvanceIsAvailable() {
        displayPayInAdvanceIsAvailableCallCount += 1
    }
    
    func displayFooterViewAnimation(showFooterView: Bool) {
        displayFooterViewAnimationCallCount += 1
        self.showFooterView = showFooterView
    }
    
    func displayEmptyPaidView(message: String, image: UIImage) {
        displayEmptyPaidViewCallCount += 1
    }
    
    func displayEmptyOpenView(message: String, image: UIImage) {
        displayEmptyOpenViewCallCount += 1
    }
}

final class InstallmentsPresenterTests: XCTestCase {
    private let coordinatorSpy = InstallmentsCoordinatorDumb()
    private let controllerSpy = InstallmentsControllerSpy()
    private lazy var dependenciesSpy = LoanDependencyContainerMock(AnalyticsSpy())
    
    private lazy var sut: InstallmentsPresenter = {
        let sut = InstallmentsPresenter(coordinator: coordinatorSpy, dependencies: dependenciesSpy)
        sut.viewController = controllerSpy
        return sut
    }()
    
    private func newInstallment(status: InstallmentStatus) -> Installment {
        Installment(
            id: "1",
            originalValue: 1056.61,
            value: 1009.58,
            penaltyValue: 0,
            order: 1,
            status: .open,
            dueDate: Date(),
            paymentDate: nil,
            daysOverDue: -28,
            isPayable: true,
            discountValue: 0
        )
    }
    
    func testSetupDescription_ShouldDisplayDescription() {
        sut.setupDescription(valueToPay: 1_000)
        
        XCTAssertEqual(controllerSpy.descriptionDisplayed?.string, "Suas parcelas estão organizadas aqui. O restante a pagar é R$ 1.000,00.")
    }
    
    func testPresentInstallments_ShouldDisplayInstallments() {
        let paidList = [
            newInstallment(status: .paid),
            newInstallment(status: .paidInAdvance),
            newInstallment(status: .paidOverdued),
            newInstallment(status: .paid)
        ]
        
        let pendingList = [
            newInstallment(status: .overdue),
            newInstallment(status: .closeToExpire),
            newInstallment(status: .open),
            newInstallment(status: .open)
        ]
        
        sut.presentPaidList(paidList)
        sut.presentPendingList(pendingList)
        
        XCTAssertEqual(controllerSpy.displayPendingCallCount, 1)
        XCTAssertEqual(controllerSpy.displayPaidCallCount, 1)
    }
    
    func testEmptyView_ShouldDisplayPaidInstallmentsEmptyView() {
        sut.displayPaidEmptyView()
        sut.displayOpenEmptyView()
        
        XCTAssertEqual(controllerSpy.displayEmptyPaidViewCallCount, 1)
        XCTAssertEqual(controllerSpy.displayEmptyOpenViewCallCount, 1)
    }
    
    func testPresentPaymentFeature_ShouldDisplayPaymentFeature() {
        sut.presentPaymentFeature()
        
        XCTAssertEqual(controllerSpy.enablePaymentFeatureCallCount, 1)
    }
    
    func testStartLoading_ShouldCallDisplayStartLoading() {
        sut.startLoading()
        
        XCTAssertEqual(controllerSpy.startLoadingCallCount, 1)
    }
    
    func testOpenAntecipateInstallments_ShouldCallDisplayStopLoading() {
        sut.stopLoading()
        
        XCTAssertEqual(controllerSpy.stopLoadingCallCount, 1)
    }
    
    func testPresentTotalDiscount_ShouldDisplayTotalDiscount() {
        let discount = 20.20
        let discountString = discount.toCurrencyString()
        sut.presentTotalDiscount(value: discount)
        
        XCTAssertEqual(controllerSpy.displayTotalDiscountCallCount, 1)
        XCTAssertEqual(controllerSpy.totalDiscount, discountString)
    }
    
    func testOpenSimulateAdvancePayment_ShouldPerformSimulateAdvancePaymentAction() {
        let pendingList = [
            newInstallment(status: .overdue),
            newInstallment(status: .closeToExpire),
            newInstallment(status: .open),
            newInstallment(status: .open)
        ]
        
        sut.openSimulateAdvancePayment(contractId: "ABC12345",
                                       installments: pendingList,
                                       selectionOrder: .both)
       
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .simulateAdvancePayment(contractId: "ABC12345", installments: pendingList, selectionOrder: .both))
    }
    
    func testPresentError_ShouldCallDisplayError() {
        sut.presentError()
        
        XCTAssertEqual(controllerSpy.displayErrorCallCount, 1)
    }
    
    func testSetTabSegmentControl_ShouldDisplayTabSegmentControl() {
        sut.setTabSegmentControl(index: 0)
        
        XCTAssertEqual(controllerSpy.displayTabSegmentControlCallCount, 1)
    }

    func testShowPayInAdvanceIsNotAvailableWarning_ShouldPresentFormattedWarning() {
        let warning = ApolloFeedbackViewContent(image: Resources.Illustrations.iluTool.image,
                                                title: Strings.PayInAdvance.Warning.title,
                                                description: NSAttributedString(string: Strings.PayInAdvance.Warning.subtitle),
                                                primaryButtonTitle: Strings.PayInAdvance.Warning.dismissButtonTitle,
                                                secondaryButtonTitle: Strings.PayInAdvance.Warning.supportButtonTitle)

        sut.showPayInAdvanceIsNotAvailableWarning()

        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .payInAdvanceWarning(warning, article: .payInAdvance))
    }

    func testShowPayInAdvanceIsAvailableWarning_ShouldPresentFormattedWarning() {
        sut.showPayInAdvanceIsAvailableWarning()

        XCTAssertEqual(controllerSpy.displayPayInAdvanceIsAvailableCallCount, 1)
    }
    
    func testPop_ShouldPerformPopAction() {
        sut.pop()
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .pop)
    }
    
    func testClose_ShouldPerformCloseAction() {
        sut.close()
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .close)
    }
    
    func testPresentFooterViewIfNeeded_ShouldDisplayFooterViewAnimationWithTrueArgument() {
        let mockShouldShowFooterView = true
        sut.presentFooterViewIfNeeded(shouldShow: mockShouldShowFooterView)
        
        XCTAssertEqual(controllerSpy.displayFooterViewAnimationCallCount, 1)
        XCTAssertEqual(controllerSpy.showFooterView, mockShouldShowFooterView)
    }
    
    func testPresentFooterViewIfNeeded_ShouldDisplayFooterViewAnimationWithFalseArgument() {
        let mockShouldShowFooterView = false
        sut.presentFooterViewIfNeeded(shouldShow: mockShouldShowFooterView)
        
        XCTAssertEqual(controllerSpy.displayFooterViewAnimationCallCount, 1)
        XCTAssertEqual(controllerSpy.showFooterView, mockShouldShowFooterView)
    }
    
    func tesOpenNegotiatedInstallment_ShouldPerformOpenNegotiatedInstallmentAction() {
        let installment = newInstallment(status: .renegotiated)
        sut.openNegotiatedInstallment(installment: installment)
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .openNegotiatedInstallment(installment: installment))
    }
}
