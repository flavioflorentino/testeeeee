import AnalyticsModule
import XCTest
@testable import Loan
@testable import Core

private final class NegotiatedInstallmentPresenterSpy: NegotiatedInstallmentPresenting {
    var viewController: NegotiatedInstallmentDisplaying?
    
    private(set) var presentDataCallCount = 0
    private(set) var openFAQCallCount = 0
    
    private(set) var value: Double?
    private(set) var email: String?
    
    func presentData(value: Double, userEmail: String) {
        presentDataCallCount += 1
        self.value = value
        email = userEmail
    }
    
    func openFAQ() {
        openFAQCallCount  += 1
    }
}

final class NegotiatedInstallmentInteractorTests: XCTestCase {
    private let presenterSpy = NegotiatedInstallmentPresenterSpy()
    private let dependenciesSpy = LoanDependencyContainerMock(AnalyticsSpy(),
                                                              LoanSetupMock(.consumer))
    
    private let valueMocked = 1036.25
    private lazy var installment = Installment(id: "1",
                                  originalValue: 1056.61,
                                  value: valueMocked,
                                  penaltyValue: 0,
                                  order: 1,
                                  status: .renegotiated,
                                  dueDate: Date(),
                                  paymentDate: nil,
                                  daysOverDue: 0,
                                  isPayable: true,
                                  discountValue: 20.63)

    private lazy var sut = NegotiatedInstallmentInteractor(presenter: presenterSpy,
                                                           dependencies: dependenciesSpy,
                                                           installment: installment)
    
    func testFetch_WhenViewDidLoad_ShouldPresentData() {
        sut.fetch()
   
        let userEmail = dependenciesSpy.legacy.consumer?.consumerEmail
        
        XCTAssertEqual(presenterSpy.presentDataCallCount, 1)
        XCTAssertEqual(presenterSpy.value, valueMocked)
        XCTAssertEqual(presenterSpy.email, userEmail)
    }
    
    func tesDidTapFAQ_WhenTapHelButton_ShouldOpenFAQ() {
        sut.didTapFAQ()
        
        XCTAssertEqual(presenterSpy.openFAQCallCount, 1)
    }
}
