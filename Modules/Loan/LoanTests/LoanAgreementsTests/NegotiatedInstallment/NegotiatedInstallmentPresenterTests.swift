import XCTest
import Core
import AssetsKit
@testable import Loan

final private class NegotiatedInstallmentCoordinatorSpy: NegotiatedInstallmentCoordinating {
    var viewController: UIViewController?
    
    private(set) var performActionCallCount = 0
    private(set) var performedAction: NegotiatedInstallmentAction?
       
    func perform(action: NegotiatedInstallmentAction) {
        performActionCallCount += 1
        performedAction = action
    }
}

final private class NegotiatedInstallmentControllerSpy: NegotiatedInstallmentDisplaying {
    private(set) var displayDataCallCount = 0
    
    private(set) var value: String?
    private(set) var email: String?
    
    func displayData(value: String, userEmail: String) {
        displayDataCallCount += 1
        self.value = value
        email = userEmail
    }
}

final class NegotiatedInstallmentPresenterTests: XCTestCase {
    private let coordinatorSpy = NegotiatedInstallmentCoordinatorSpy()
    private let controllerSpy = NegotiatedInstallmentControllerSpy()
    
    private lazy var sut: NegotiatedInstallmentPresenting = {
        let presenter = NegotiatedInstallmentPresenter(coordinator: coordinatorSpy)
        presenter.viewController = controllerSpy
        return presenter
    }()
    
    func testOpenFaq_ShouldPerformeOpenFaqAction() {
        sut.openFAQ()
    
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .openFAQ(.negotiated))
    }
    
    func testPresenterData_ShouldDisplayData() {
        let valueMocked = 296.56
        let emailMocked = "abcd@gmail.com"
        
        sut.presentData(value: valueMocked, userEmail: emailMocked)
        
        XCTAssertEqual(controllerSpy.displayDataCallCount, 1)
        XCTAssertEqual(controllerSpy.value, "R$ 296,56")
        XCTAssertEqual(controllerSpy.email, "a**d@g***l.com")
    }
}
