import AnalyticsModule
import XCTest
@testable import Loan
@testable import Core

private final class HireAdvancePaymentPresenterSpy: HireAdvancePaymentPresenting {
    var viewController: HireAdvancePaymentDisplaying?

    private(set) var presentPaymentCallCount = 0
    private(set) var openPaymentMethodsCallCount = 0
    private(set) var presentDiscountLabelsCallCount = 0
    
    private(set) var totalToPay: Double?
    private(set) var totalDiscount: Double?

    func presentPayment(installments: [Installment], valueToPay: Double) {
        presentPaymentCallCount += 1
        totalToPay = valueToPay
    }

    func openPaymentMethods(contractId: String, installments: [Installment]) {
        openPaymentMethodsCallCount += 1
    }
    
    func presentDiscountLabel(totalDiscount: Double) {
        presentDiscountLabelsCallCount += 1
        self.totalDiscount = totalDiscount
    }
}

final class HireAdvancePaymentInteractorTests: XCTestCase {
    private let presenterSpy = HireAdvancePaymentPresenterSpy()
    private let dependenciesSpy = LoanDependencyContainerMock(AnalyticsSpy())

    private lazy var totalToPayMocked = 1036.25
    private lazy var discountValueMocked = 20.63
    private func installment(status: InstallmentStatus) -> Installment {
        Installment(
            id: "1",
            originalValue: 1056.61,
            value: totalToPayMocked,
            penaltyValue: 0,
            order: 1,
            status: status,
            dueDate: Date(),
            paymentDate: nil,
            daysOverDue: 0,
            isPayable: true,
            discountValue: discountValueMocked
        )
    }

    private lazy var sut = HireAdvancePaymentInteractor(presenter: presenterSpy,
                                                        dependencies: dependenciesSpy,
                                                        contractId: "ABC12345",
                                                        installments: [installment(status: .open),
                                                                       installment(status: .open),
                                                                       installment(status: .open)])

    func testFetchPayment_ShouldPresentPayment() {
        sut.fetchPayment()
        
        XCTAssertEqual(presenterSpy.presentPaymentCallCount, 1)
        XCTAssertEqual(presenterSpy.totalToPay, totalToPayMocked * 3)
    }

    func testFetchPayment_WhenTotalDiscountIsZero_ShouldNotPresentDiscountLabel() {
        discountValueMocked = 0.00
        sut.fetchPayment()
        
        XCTAssertEqual(presenterSpy.presentDiscountLabelsCallCount, 0)
    }
    
    func testFetchPayment_WhenTotalDiscountIsGreaterThenZero_ShouldPresentDiscountLabel() {
        sut.fetchPayment()
        
        XCTAssertEqual(presenterSpy.presentDiscountLabelsCallCount, 1)
        XCTAssertEqual(presenterSpy.totalDiscount, discountValueMocked * 3)
    }
    
    func testNexStep_ShouldOpenPaymentsMehods() {
        sut.nextStep()
        
        XCTAssertEqual(presenterSpy.openPaymentMethodsCallCount, 1)
    }
}
