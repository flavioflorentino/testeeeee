import XCTest
import Core
@testable import Loan

final class HireAdvancePaymentControllerSpy: HireAdvancePaymentDisplaying {
    private(set) var displayValueCallCount = 0
    private(set) var displayDiscountLabelCallCount = 0
    private(set) var displayHeaderTitleCallCount = 0
    private(set) var updateInstallmentsCallCount = 0
    
    private(set) var value: Double?
    private(set) var valueAttributedString: NSAttributedString?
    private(set) var installmentsCount: Int?
    
    func displayValue(_ value: Double) {
        displayValueCallCount += 1
        self.value = value
    }
    
    func displayDiscountLabel(_ value: NSAttributedString) {
        displayDiscountLabelCallCount += 1
        valueAttributedString = value
    }
    
    func displayHeaderTitle(installmentsCount: Int) {
        displayHeaderTitleCallCount += 1
        self.installmentsCount = installmentsCount
    }
    
    func updateInstallments(installments: [HireAdvancePaymentViewModel]) {
        updateInstallmentsCallCount += 1
    }
}

final class HireAdvancePaymentCoordinatorSpy: HireAdvancePaymentCoordinating {
    var viewController: UIViewController?
    
    private(set) var performActionCallCount = 0
    private(set) var performedAction: HireAdvancePaymentAction?
    
    func perform(action: HireAdvancePaymentAction) {
        performActionCallCount += 1
        performedAction = action
    }
}

final class HireAdvancePaymentPresenterTest: XCTestCase {
    private lazy var coordinatorSpy = HireAdvancePaymentCoordinatorSpy()
    private lazy var controllerSpy = HireAdvancePaymentControllerSpy()
    
    private lazy var sut: HireAdvancePaymentPresenter = {
        let sut = HireAdvancePaymentPresenter(coordinator: coordinatorSpy)
        sut.viewController = controllerSpy
        return sut
    }()
    
    private lazy var valueToPayMocked = 69.65
    private lazy var installmentMocked = [Installment(id: "1",
                                                      originalValue: 35.00,
                                                      value: 36.00,
                                                      penaltyValue: 1.00,
                                                      order: 1,
                                                      status: .overdue,
                                                      dueDate: Date(),
                                                      paymentDate: nil,
                                                      daysOverDue: 1,
                                                      isPayable: true,
                                                      discountValue: nil),
                                          Installment(id: "2",
                                                      originalValue: 35.00,
                                                      value: 24.00,
                                                      penaltyValue: nil,
                                                      order: 2,
                                                      status: .open,
                                                      dueDate: Date(),
                                                      paymentDate: nil,
                                                      daysOverDue: nil,
                                                      isPayable: false,
                                                      discountValue: 11.00)]
    
    func testOpenPaymentMethod_ShouldPerformPaymentAction() {
        let contractIdMocked = "abababa"
        
        sut.openPaymentMethods(contractId: contractIdMocked, installments: installmentMocked)
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, .payment(contractId: contractIdMocked,
                                                                installments: installmentMocked))
    }
    
    func testPresentPayment_ShouldUpdateInstallments() {
        sut.presentPayment(installments: installmentMocked, valueToPay: valueToPayMocked)
        
        XCTAssertEqual(controllerSpy.updateInstallmentsCallCount, 1)
        XCTAssertEqual(controllerSpy.installmentsCount, installmentMocked.count)
    }
    
    func testPresentPayment_ShouldDisplayValue() {
        sut.presentPayment(installments: installmentMocked, valueToPay: valueToPayMocked)
        
        XCTAssertEqual(controllerSpy.displayValueCallCount, 1)
        XCTAssertEqual(controllerSpy.value, valueToPayMocked)
    }
    
    func testPresentPayment_ShouldDisplayDiscountLabel() {
        let totalDiscountMoked = 8.65
        sut.presentDiscountLabel(totalDiscount: totalDiscountMoked)
        
        XCTAssertEqual(controllerSpy.displayDiscountLabelCallCount, 1)
        XCTAssertEqual(controllerSpy.valueAttributedString?.string, "Você vai economizar R$ 8,65")
    }
}
