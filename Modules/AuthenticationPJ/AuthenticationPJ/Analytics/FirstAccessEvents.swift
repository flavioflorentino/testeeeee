import Foundation

enum FirstAccessEvents: TrackerEventProtocol, Equatable {
    case savedData
    case recoverPassword
    case success(_ fastAccess: Bool, saveData: Bool)
    case error(_ type: ErrorType)
    case faq

    var name: String {
        switch self {
        case .savedData:
            return "Login - Save Data"
        case .recoverPassword:
            return "Recover Password"
        case .success:
            return "Login - Success"
        case .error:
            return "Login - Error"
        case .faq:
            return "Login - FAQ"
        }
    }

    var properties: [String: Any] {
        switch self {
        case .recoverPassword:
            return [
                "recovery_pass_flow": "logged_out"
            ]
        case let .success(fastAccess, saveData):
            return [
                "fast_access": fastAccess,
                "save_data": saveData
            ]
        case let .error(type):
            return ["error_type": type.rawValue]
        default:
            return [:]
        }
    }
}
