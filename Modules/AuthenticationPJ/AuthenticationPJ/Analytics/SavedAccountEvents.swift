import Foundation

enum SavedAccountEvents: TrackerEventProtocol, Equatable {
    case success(_ fastAccess: Bool, saveData: Bool)
    case error(_ type: ErrorType)
    case faq
    case anotherAccount
    case recoverPassword
    
    var name: String {
        switch self {
        case .success:
            return "Login - Success"
        case .error:
            return "Login - Error"
        case .faq:
            return "Login - FAQ"
        case .anotherAccount:
            return "Login - Access with Another Account"
        case .recoverPassword:
            return "Recover Password"
        }
    }

    var properties: [String: Any] {
        switch self {
        case let .success(fastAccess, saveData):
            return [
                "fast_access": fastAccess,
                "save_data": saveData
            ]
        case let .error(type):
            return ["error_type": type.rawValue]
        case .recoverPassword:
            return [
                "recovery_pass_flow": "logged_out"
            ]
        default:
            return [:]
        }
    }
}
