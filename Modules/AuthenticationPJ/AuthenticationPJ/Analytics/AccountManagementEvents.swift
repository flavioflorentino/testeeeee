import Foundation

enum AccountManagementEvents: TrackerEventProtocol, Equatable {
    case delete(_ isDeletingAll: Bool)
    case faq
    case anotherAccount
    case register

    var name: String {
        switch self {
        case .delete:
            return "Login - Delete Data"
        case .faq:
            return "Login - FAQ"
        case .anotherAccount:
            return "Login - Add Another Account"
        case .register:
            return "Login - Register New Account"
        }
    }

    var properties: [String: Any] {
        switch self {
        case let .delete(isDeletingAll):
            return [
                "delete_all": isDeletingAll
            ]
        default:
            return [:]
        }
    }
}
