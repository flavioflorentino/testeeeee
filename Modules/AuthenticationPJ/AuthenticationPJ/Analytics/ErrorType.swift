import Foundation

enum ErrorType: String {
    case cnpjInvalid = "cnpj_invalid"
    case cnpjNotRegistered = "cnpj_not_registered"
    case invalidLoginData = "invalid_login_data"
}
