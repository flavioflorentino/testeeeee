import Foundation

enum AccountEditingEvents: TrackerEventProtocol, Equatable {
    case edit(_ hasDescription: Bool, isUserEdited: Bool, isDescriptionEdited: Bool)
    case delete

    var name: String {
        switch self {
        case .edit:
            return "Login - Edit Data"
        case .delete:
            return "Login - Delete Data"
        }
    }

    var properties: [String: Any] {
        switch self {
        case let .edit(hasDescription, isUserEdited, isDescriptionEdited):
            return [
                "description": hasDescription,
                "edit_user": isUserEdited,
                "edit_description": isDescriptionEdited
            ]
        case .delete:
            return [
                "delete_all": false
            ]
        }
    }
}
