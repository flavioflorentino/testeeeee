import AnalyticsModule
import Foundation

protocol TrackerEventProtocol {
    var name: String { get }
    var properties: [String: Any] { get }
}

struct Tracker: AnalyticsKeyProtocol {
    var eventType: TrackerEventProtocol

    private var properties: [String: Any] {
        eventType.properties
    }

    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(
            eventType.name,
            properties: properties,
            providers: [.eventTracker]
        )
    }
}
