import Foundation
import Core

private enum KeychainKeys: String, KeychainKeyable {
    case accounts
}

protocol AuthenticationWorking {
    func authentication(document: String, username: String, password: String, mustRemind: Bool, completion: @escaping (Result<UserAuthResponse, AuthenticationError>) -> Void)
    func retrieveAccounts(completion: @escaping ([Account]) -> Void)
    func updateAccount(_ account: Account)
    func removeAccount(_ account: Account)
    func removeAccounts()
}

final class AuthenticationWorker {
    typealias Dependencies = HasKeychainManager
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension AuthenticationWorker: AuthenticationWorking {
    func authentication(document: String,
                        username: String,
                        password: String,
                        mustRemind: Bool,
                        completion: @escaping (Result<UserAuthResponse, AuthenticationError>) -> Void) {
        DispatchQueue.global().async { [weak self] in
            guard let self = self else { return }
            let userAuthResponse = UserAuthResponse()
            let params = (document, username, password, mustRemind, userAuthResponse)
            self.handleValidate(params: params, completion: completion)
        }
    }
    
    func retrieveAccounts(completion: @escaping ([Account]) -> Void) {
        completion(retrieveAccounts())
    }
    
    func updateAccount(_ account: Account) {
        save(account: account)
    }
    
    func removeAccount(_ account: Account) {
        remove(account: account)
    }
    
    func removeAccounts() {
        removeAll()
    }
}

// MARK: - Network requests
private extension AuthenticationWorker {
    typealias AuthorizationParams = (document: String, username: String, password: String, mustRemind: Bool, userAuthResponse: UserAuthResponse)

    func handleValidate(params: AuthorizationParams,
                        completion: @escaping (Result<UserAuthResponse, AuthenticationError>) -> Void) {
        validate(document: params.document) { [weak self] result in
            switch result {
            case let .success(response) where response.meta.code == HTTPStatusCode.ok.rawValue:
                self?.handleAuthorize(params: params, completion: completion)
            case let .success(response):
                let message = response.meta.errorMessage
                completion(.failure(.document(message)))
            case .failure(.connectionFailure), .failure(.timeout):
                completion(.failure(.network))
            case let .failure(error):
                let message = error.requestError?.handleBadRequest()
                completion(.failure(.document(message)))
            }
        }
    }

    func handleAuthorize(params: AuthorizationParams,
                         completion: @escaping (Result<UserAuthResponse, AuthenticationError>) -> Void) {
        let document = params.document
        let username = params.username
        let password = params.password
        
        authorize(document: document, username: username, password: password) { [weak self] result in
            switch result {
            case let .success(response):
                self?.dependencies.keychain.set(key: KeychainKey.token, value: response.accessToken)
                params.userAuthResponse.auth = response
                self?.handleRetrieveUser(params: params, completion: completion)
            case .failure(.connectionFailure), .failure(.timeout):
                completion(.failure(.network))
            case let .failure(error):
                let message = error.requestError?.handleBadRequest()
                completion(.failure(.auth(message)))
            }
        }
    }
    
    func handleRetrieveUser(params: AuthorizationParams,
                            completion: @escaping (Result<UserAuthResponse, AuthenticationError>) -> Void) {
        retrieveUser { [weak self] result in
            switch result {
            case let .success(response):
                params.userAuthResponse.user = response
                self?.handleRetrieveSeller(params: params, completion: completion)
            case .failure(.connectionFailure), .failure(.timeout):
                _ = self?.dependencies.keychain.clearValue(key: KeychainKey.token)
                completion(.failure(.network))
            case let .failure(error):
                _ = self?.dependencies.keychain.clearValue(key: KeychainKey.token)
                let message = error.requestError?.handleBadRequest()
                completion(.failure(.user(message)))
            }
        }
    }
    
    func handleRetrieveSeller(params: AuthorizationParams,
                              completion: @escaping (Result<UserAuthResponse, AuthenticationError>) -> Void) {
        retrieveSeller { [weak self] result in
            switch result {
            case let .success(response):
                params.userAuthResponse.seller = response
                if params.mustRemind {
                    self?.handleSaveAccount(userAuthResponse: params.userAuthResponse, username: params.username)
                }
                completion(.success(params.userAuthResponse))
            case .failure(.connectionFailure), .failure(.timeout):
                _ = self?.dependencies.keychain.clearValue(key: KeychainKey.token)
                completion(.failure(.network))
            case let .failure(error):
                _ = self?.dependencies.keychain.clearValue(key: KeychainKey.token)
                let message = error.requestError?.handleBadRequest()
                completion(.failure(.seller(message)))
            }
        }
    }
    
    func validate(document: String, completion: @escaping (Result<ValidateDocumentResponse, ApiError>) -> Void) {
        let decoder = JSONDecoder(.convertFromSnakeCase)
        let endpoint = AuthenticationEndpoint.validate(document)
        Api<ValidateDocumentResponse>(endpoint: endpoint).execute(jsonDecoder: decoder) { result in
            completion(result.map(\.model))
        }
    }
    
    func authorize(document: String, username: String, password: String, completion: @escaping (Result<AuthResponse, ApiError>) -> Void) {
        let decoder = JSONDecoder(.convertFromSnakeCase)
        let endpoint = AuthenticationEndpoint.authorize(document, username: username, password: password)
        Api<AuthResponse>(endpoint: endpoint).execute(jsonDecoder: decoder) { result in
            completion(result.map(\.model))
        }
    }
    
    func retrieveUser(completion: @escaping (Result<UserResponse, ApiError>) -> Void) {
        let decoder = JSONDecoder(.convertFromSnakeCase)
        let endpoint = AuthenticationEndpoint.retrieveUser
        Api<UserResponse>(endpoint: endpoint).execute(jsonDecoder: decoder) { result in
            completion(result.map(\.model))
        }
    }
    
    func retrieveSeller(completion: @escaping (Result<SellerResponse, ApiError>) -> Void) {
        let decoder = JSONDecoder(.convertFromSnakeCase)
        let endpoint = AuthenticationEndpoint.retrieveSeller
        Api<SellerResponse>(endpoint: endpoint).execute(jsonDecoder: decoder) { result in
            completion(result.map(\.model))
        }
    }
}

// MARK: - Local requests
private extension AuthenticationWorker {
    func handleSaveAccount(userAuthResponse: UserAuthResponse, username: String) {
        guard let user = userAuthResponse.user,
              let seller = userAuthResponse.seller else {
            return
        }
        let oldDescription = retrieveAccounts().first(where: { $0.id == user.id })?.description
        let account = Account(id: user.id,
                              document: seller.cnpj,
                              username: username,
                              companyName: seller.name,
                              lastLogin: Date(),
                              imageUrl: seller.imageUrl,
                              description: oldDescription)

        save(account: account)
    }

    func save(account: Account) {
        var accounts = retrieveAccounts()
        accounts.removeAll { $0.id == account.id }
        accounts.append(account)
        dependencies.keychain.set(key: KeychainKeys.accounts, value: accounts)
    }

    func remove(account: Account) {
        var accounts = retrieveAccounts()
        accounts.removeAll { $0.id == account.id }
        dependencies.keychain.set(key: KeychainKeys.accounts, value: accounts)
    }

    func removeAll() {
        let accounts: [Account] = []
        dependencies.keychain.set(key: KeychainKeys.accounts, value: accounts)
    }

    func retrieveAccounts() -> [Account] {
        guard let accounts: [Account] = dependencies.keychain.getModelData(key: KeychainKeys.accounts) else {
            return []
        }

        return orderAccounts(accounts)
    }

    func orderAccounts(_ accounts: [Account]) -> [Account] {
        var accounts = accounts
        accounts.sort { lhs, rhs in
            lhs.lastLogin.compare(rhs.lastLogin) == .orderedDescending
        }
        return accounts
    }
}
