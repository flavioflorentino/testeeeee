import Foundation

public final class UserAuthResponse: Equatable {
    public var auth: AuthResponse?
    public var user: UserResponse?
    public var seller: SellerResponse?

    public static func == (lhs: UserAuthResponse, rhs: UserAuthResponse) -> Bool {
        lhs.auth == rhs.auth && lhs.user == rhs.user && lhs.seller == rhs.seller
    }
}
