import Foundation

public struct SellerResponse: Decodable, Equatable {
    public let name: String
    public let cnpj: String
    public let imageUrl: String?
    
    private enum CodingKeys: String, CodingKey {
        case data
        case name
        case cnpj = "cpfCnpj"
        case imageUrl = "imgUrl"
    }
    
    public init(from decoder: Decoder) throws {
        let dataContainer = try decoder.container(keyedBy: CodingKeys.self)
        let container = try dataContainer.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        
        name = try container.decode(String.self, forKey: .name)
        cnpj = try container.decode(String.self, forKey: .cnpj)
        imageUrl = try container.decodeIfPresent(String.self, forKey: .imageUrl)
    }
}
