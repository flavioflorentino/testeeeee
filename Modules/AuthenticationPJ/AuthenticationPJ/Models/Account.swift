import Foundation

struct Account: Codable, Equatable, Hashable {
    let id: Int
    let document: String
    let username: String
    let companyName: String
    let lastLogin: Date
    let imageUrl: String?
    let description: String?
}
