import Foundation

enum AuthenticationError: Error {
    case network
    case document(_ message: String?)
    case auth(_ message: String?)
    case user(_ message: String?)
    case seller(_ message: String?)
}

typealias ValidateDocumentResponse = AuthenticationDefaultError

struct AuthenticationDefaultError: Decodable {
    let meta: AuthenticationMetaError
}

struct AuthenticationMetaError: Decodable {
    let code: Int?
    let errorType: String?
    let errorMessage: String?
}
