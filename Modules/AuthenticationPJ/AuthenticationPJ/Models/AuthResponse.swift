import Foundation

public struct AuthResponse: Decodable, Equatable {
    public let accessToken: String
    public let tokenType: String
    public let expires: Date
    public let refreshToken: String
    public var biometry: String
    public let completed: Bool
    
    private enum CodingKeys: String, CodingKey {
        case data
        case accessToken
        case tokenType
        case expires = "expiresIn"
        case refreshToken
        case biometry
        case completed
    }
    
    public init(from decoder: Decoder) throws {
        let dataContainer = try decoder.container(keyedBy: CodingKeys.self)
        let container = try dataContainer.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        
        accessToken = try container.decode(String.self, forKey: .accessToken)
        tokenType = try container.decode(String.self, forKey: .tokenType)
        let expireDate = try container.decode(Int.self, forKey: .expires)
        // adjusts the expiration time (the server sends only the expires time since now)
        // to stores this the expire time needs to transformed in timestamp
        let expireInt = Int(Date(timeIntervalSinceNow: TimeInterval(expireDate)).timeIntervalSince1970)
        expires = Date(timeIntervalSince1970: TimeInterval(expireInt))
        refreshToken = try container.decode(String.self, forKey: .refreshToken)
        biometry = try container.decodeIfPresent(String.self, forKey: .biometry) ?? "accepted"
        completed = try container.decodeIfPresent(Bool.self, forKey: .completed) ?? true
    }
}
