import Foundation

public struct UserResponse: Decodable, Equatable {
    public let id: Int
    public let cnpj: String?
    public let headOfficeId: Int?
    public let imageUrl: String?
    public let mobilePhone: String?
    public let birthDateFormated: String?
    public let username: String?
    public let superId: Int
    public let role: Int
    public let email: String?
    public let name: String?
    
    private enum CodingKeys: String, CodingKey {
        case data
        case id
        case cnpj = "cpf"
        case headOfficeId
        case imageUrl = "imgUrl"
        case mobilePhone
        case birthDateFormated
        case username
        case superId = "super"
        case role
        case email
        case name
    }
    
    public init(from decoder: Decoder) throws {
        let dataContainer = try decoder.container(keyedBy: CodingKeys.self)
        let container = try dataContainer.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        
        id = try container.decode(Int.self, forKey: .id)
        cnpj = try container.decodeIfPresent(String.self, forKey: .cnpj)
        headOfficeId = try container.decodeIfPresent(Int.self, forKey: .headOfficeId)
        imageUrl = try container.decodeIfPresent(String.self, forKey: .imageUrl)
        mobilePhone = try container.decodeIfPresent(String.self, forKey: .mobilePhone)
        birthDateFormated = try container.decodeIfPresent(String.self, forKey: .birthDateFormated)
        username = try container.decodeIfPresent(String.self, forKey: .username)
        superId = try container.decode(Int.self, forKey: .superId)
        role = try container.decode(Int.self, forKey: .role)
        email = try container.decodeIfPresent(String.self, forKey: .email)
        name = try container.decodeIfPresent(String.self, forKey: .name)
    }
}
