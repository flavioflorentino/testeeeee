import Foundation

// swiftlint:disable convenience_type
final class AuthenticationPJResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: AuthenticationPJResources.self).url(forResource: "AuthenticationPJResources", withExtension: "bundle") else {
            return Bundle(for: AuthenticationPJResources.self)
        }
        return Bundle(url: url) ?? Bundle(for: AuthenticationPJResources.self)
    }()
}
