import Core
import Foundation

extension RequestError {
    func handleBadRequest() -> String? {
        let decoder = JSONDecoder(.convertFromSnakeCase)
        guard let data = jsonData,
              let body = try? decoder.decode(AuthenticationDefaultError.self,
                                             from: data),
              let value = body.meta.errorMessage,
              value.isNotEmpty else {
            return nil
        }
        return value
    }
}
