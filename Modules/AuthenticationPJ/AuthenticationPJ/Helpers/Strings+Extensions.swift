import Foundation

extension String {
    var isValidCNPJ: Bool {
        let numbers = compactMap(\.wholeNumberValue)
        guard numbers.count == 14 && Set(numbers).count != 1 else { return false }
        return numbers.prefix(12).digitCNPJ == numbers[12] &&
            numbers.prefix(13).digitCNPJ == numbers[13]
    }
    
    var onlyNumbers: String {
        components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
    }
}
