import Foundation
import UI

protocol SavedAccountPresenting: AnyObject {
    var viewController: SavedAccountDisplaying? { get set }
    func didNextStep(action: SavedAccountAction)
    func displayAccount(_ account: Account)
    func displayError(_ message: String)
    func dismissError()
    func enableLoginButton()
    func disableLoginButton()
    func displayLoading()
    func dismissLoading()
    func displayNetworkBanner()
    func displayAccountRemovedSnackbar()
    func displayAccountEditedSnackbar()
}

final class SavedAccountPresenter {
    private let coordinator: SavedAccountCoordinating
    weak var viewController: SavedAccountDisplaying?

    init(coordinator: SavedAccountCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - SavedAccountPresenting
extension SavedAccountPresenter: SavedAccountPresenting {
    func didNextStep(action: SavedAccountAction) {
        coordinator.perform(action: action)
    }

    func displayAccount(_ account: Account) {
        viewController?.displayAccount(account)
    }
    
    func displayError(_ message: String) {
        viewController?.displayPasswordError(message)
    }
    
    func dismissError() {
        viewController?.dismissPasswordError()
    }
    
    func enableLoginButton() {
        viewController?.enableLoginButton()
    }
    
    func disableLoginButton() {
        viewController?.disableLoginButton()
    }
    
    func displayLoading() {
        viewController?.displayLoading()
    }
    
    func dismissLoading() {
        viewController?.dismissLoading()
    }
    
    func displayNetworkBanner() {
        coordinator.perform(action: .network)
    }

    func displayAccountRemovedSnackbar() {
        let snackbar = ApolloSnackbar(text: Strings.General.Account.removed, iconType: .success)
        coordinator.perform(action: .snackbar(snackbar))
    }

    func displayAccountEditedSnackbar() {
        let snackbar = ApolloSnackbar(text: Strings.General.Account.edited, iconType: .success)
        coordinator.perform(action: .snackbar(snackbar))
    }
}
