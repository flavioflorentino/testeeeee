import UIKit

enum SavedAccountFactory {
    static func make(account: Account, delegate: AuthenticationCoordinating) -> SavedAccountViewController {
        let container = DependencyContainer()
        let service: SavedAccountServicing = SavedAccountService(dependencies: container)
        let coordinator: SavedAccountCoordinating = SavedAccountCoordinator(delegate: delegate)
        let presenter: SavedAccountPresenting = SavedAccountPresenter(coordinator: coordinator)
        let interactor = SavedAccountInteractor(service: service, presenter: presenter, account: account, dependencies: container)
        let viewController = SavedAccountViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
