import AssetsKit
import UI
import UIKit
import SnapKit

protocol SavedAccountDisplaying: AnyObject {
    func displayAccount(_ account: Account)
    func displayPasswordError(_ message: String)
    func dismissPasswordError()
    func enableLoginButton()
    func disableLoginButton()
    func displayLoading()
    func dismissLoading()
}

private extension SavedAccountViewController.Layout {
    enum Animation {
        static let time = 0.3
    }
}

final class SavedAccountViewController: ViewController<SavedAccountInteracting, UIView> {
    fileprivate enum Layout { }
    private typealias Localizable = Strings.Saved.Account
    private lazy var keyboardHandler = KeyboardHandler()
    private lazy var scrollView = UIScrollView()
    private lazy var containerView = UIView()

    private lazy var savedAccountCardSizing = SpacerView(size: Spacing.base05)
    private lazy var savedAccountCardView = SavedAccountCardView()
    private lazy var passwordSizing = SpacerView(size: Spacing.base06)
    private lazy var passwordToggleButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(Resources.Icons.icoPasswordInvisible.image, for: .normal)
        button.setImage(Resources.Icons.icoPasswordVisible.image, for: .selected)
        button.addTarget(self, action: #selector(didTogglePassword(_:)), for: .touchDown)
        return button
    }()
    private lazy var passwordTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholderColor = Colors.grayscale400.color
        textField.titleColor = Colors.grayscale400.color
        textField.selectedTitleColor = Colors.branding600.color
        textField.textColor = Colors.grayscale750.color
        textField.placeholder = Localizable.Enter.password
        textField.title = Localizable.password
        textField.selectedTitle = Localizable.password
        textField.isSecureTextEntry = true
        textField.rightView = passwordToggleButton
        textField.rightViewMode = .always
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        return textField
    }()
    private lazy var passwordErrorLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
            .with(\.textColor, .critical900())
        label.isHidden = true
        return label
    }()
    private lazy var passwordStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            passwordSizing,
            passwordTextField,
            SpacerView(size: Spacing.base01),
            passwordErrorLabel,
            SpacerView(size: Spacing.base02)
        ])
        stackView.axis = .vertical
        return stackView
    }()
    private lazy var containerStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            savedAccountCardSizing,
            savedAccountCardView,
            passwordStackView
        ])
        stackView.axis = .vertical
        return stackView
    }()
    private lazy var loginButton: UIButton = {
        let button = UIButton(type: .custom)
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Localizable.enter, for: .normal)
        button.addTarget(self, action: #selector(didTapLogin(_:)), for: .touchUpInside)
        button.isEnabled = false
        return button
    }()
    private lazy var anotherButton: UIButton = {
        let button = UIButton(type: .custom)
        button.buttonStyle(SecondaryButtonStyle())
        button.setTitle(Localizable.Enter.another, for: .normal)
        button.addTarget(self, action: #selector(didTapAnother(_:)), for: .touchUpInside)
        return button
    }()
    private lazy var forgotButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.forgot, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(didTapForgot(_:)), for: .touchUpInside)
        return button
    }()
    private lazy var buttonsStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            loginButton,
            SpacerView(size: Spacing.base01),
            anotherButton,
            SpacerView(size: Spacing.base02),
            forgotButton
        ])
        stackView.axis = .vertical
        return stackView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.retrieveAccount()
    }

    override func buildViewHierarchy() {
        containerView.addSubviews(containerStackView, buttonsStackView)
        scrollView.addSubview(containerView)
        view.addSubview(scrollView)
    }

    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalTo(view.compatibleSafeArea.edges)
        }

        containerView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.height.equalToSuperview().priority(.low)
            $0.width.equalToSuperview().offset(-Spacing.base04)
            $0.centerX.equalToSuperview()
        }

        containerStackView.snp.makeConstraints {
            $0.leading.top.trailing.equalToSuperview()
        }

        buttonsStackView.snp.makeConstraints {
            $0.top.greaterThanOrEqualTo(containerStackView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalToSuperview().offset(-Spacing.base04)
        }
    }

    override func configureViews() {
        title = Localizable.title
        view.backgroundColor = .backgroundPrimary()
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: Resources.Icons.icoQuestion.image,
            style: .plain,
            target: self,
            action: #selector(didTapFAQ(_:))
        )
        keyboardHandler.handle { [weak self] keyboardSize in
            guard let self = self else { return }
            let savedAccountCardSize = keyboardSize.isZero ? Spacing.base05 : Spacing.base01
            self.savedAccountCardSizing.updateConstraint(size: savedAccountCardSize)
            let passwordSize = keyboardSize.isZero ? Spacing.base06 : Spacing.base01
            self.passwordSizing.updateConstraint(size: passwordSize)
            self.scrollView.snp.updateConstraints {
                $0.bottom.equalTo(self.view.compatibleSafeArea.bottom).inset(keyboardSize)
            }
            self.view.layoutIfNeeded()
        }
        let gesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(gesture)
    }
}

// MARK: - SavedAccountDisplaying
extension SavedAccountViewController: SavedAccountDisplaying {
    func displayAccount(_ account: Account) {
        savedAccountCardView.setup(account: account, target: self, action: #selector(didTapEdit(_:)))
    }

    func displayPasswordError(_ message: String) {
        setError(true, with: message)
    }

    func dismissPasswordError() {
        setError(false)
    }

    func enableLoginButton() {
        loginButton.isEnabled = true
    }

    func disableLoginButton() {
        loginButton.isEnabled = false
    }

    func displayLoading() {
        loginButton.startLoading()
    }

    func dismissLoading() {
        loginButton.stopLoading()
    }
}

@objc
private extension SavedAccountViewController {
    func didTapFAQ(_ sender: UIBarButtonItem) {
        interactor.faq()
    }
    
    func textFieldDidChange(_ sender: UITextField) {
        interactor.validateField(password: passwordTextField.text)
    }
    
    func didTogglePassword(_ sender: UIButton) {
        sender.isSelected.toggle()
        passwordTextField.isSecureTextEntry = !sender.isSelected
    }

    func didTapLogin(_ sender: UIButton) {
        interactor.authorize(password: passwordTextField.text)
    }

    func didTapAnother(_ sender: UIButton) {
        interactor.chooseAnotherAccount()
    }

    func didTapForgot(_ sender: UIButton) {
        interactor.forgotPassword()
    }
    
    func didTapEdit(_ sender: UIButton) {
        interactor.editAccount()
    }

    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func setError(_ hasError: Bool, with message: String = "") {
        if hasError {
            passwordTextField.lineColor = Colors.critical900.color
            passwordTextField.selectedLineColor = Colors.critical900.color
            passwordTextField.titleColor = Colors.critical900.color
            passwordTextField.selectedTitleColor = Colors.critical900.color
        } else {
            passwordTextField.lineColor = Colors.grayscale400.color
            passwordTextField.selectedLineColor = Colors.branding300.color
            passwordTextField.titleColor = Colors.grayscale400.color
            passwordTextField.selectedTitleColor = Colors.branding600.color
        }
        
        passwordErrorLabel.isHidden = !hasError
        passwordErrorLabel.text = message
        
        UIView.animate(withDuration: Layout.Animation.time) {
            self.view.layoutIfNeeded()
        }
    }
}
