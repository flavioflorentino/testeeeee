import Foundation
import Core

protocol SavedAccountServicing {
    func authentication(document: String, username: String, password: String, completion: @escaping (Result<UserAuthResponse, AuthenticationError>) -> Void)
}

final class SavedAccountService {
    typealias Dependencies = HasMainQueue & HasAuthenticationWorker
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - SavedAccountServicing
extension SavedAccountService: SavedAccountServicing {
    func authentication(document: String, username: String, password: String, completion: @escaping (Result<UserAuthResponse, AuthenticationError>) -> Void) {
        dependencies.authenticationWorker.authentication(document: document,
                                                         username: username,
                                                         password: password,
                                                         mustRemind: true) {  [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result)
            }
        }
    }
}
