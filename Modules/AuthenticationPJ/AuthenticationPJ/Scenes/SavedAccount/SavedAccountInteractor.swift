import AnalyticsModule
import Foundation

protocol SavedAccountInteracting: AnyObject {
    func retrieveAccount()
    func faq()
    func validateField(password: String?)
    func authorize(password: String?)
    func chooseAnotherAccount()
    func editAccount()
    func forgotPassword()
}

final class SavedAccountInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    private let service: SavedAccountServicing
    private let presenter: SavedAccountPresenting
    private var account: Account
    private typealias Localizable = Strings.Saved.Account
    private let minimumCountRule = 1

    init(service: SavedAccountServicing, presenter: SavedAccountPresenting, account: Account, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.account = account
        self.dependencies = dependencies
    }
}

// MARK: - SavedAccountInteracting
extension SavedAccountInteractor: SavedAccountInteracting {
    func retrieveAccount() {
        presenter.displayAccount(account)
    }

    func faq() {
        trackEvent(.faq)
        presenter.didNextStep(action: .faq)
    }

    func validateField(password: String?) {
        guard let password = password, password.count >= minimumCountRule else {
            presenter.disableLoginButton()
            return
        }

        presenter.enableLoginButton()
    }

    func authorize(password: String?) {
        guard let password = password else {
            return
        }
        presenter.displayLoading()
        presenter.dismissError()
        service.authentication(document: account.document, username: account.username, password: password) { [weak self] result in
            guard let self = self else { return }
            self.presenter.dismissLoading()
            switch result {
            case let .success(response):
                self.trackEvent(.success(true, saveData: true))
                let action = SavedAccountAction.complete(response)
                self.presenter.didNextStep(action: action)
            case .failure(.network):
                self.presenter.displayNetworkBanner()
            case let .failure(.document(message)):
                self.errorHandler(message: message, errorEvent: .cnpjNotRegistered)
            case let .failure(.auth(message)),
                 let .failure(.user(message)),
                 let .failure(.seller(message)):
                self.errorHandler(message: message, errorEvent: .invalidLoginData)
            }
        }
    }

    func chooseAnotherAccount() {
        trackEvent(.anotherAccount)
        presenter.didNextStep(action: .anotherAccount)
    }

    func editAccount() {
        presenter.didNextStep(action: .edit(account, editingDelegate: self))
    }

    func forgotPassword() {
        trackEvent(.recoverPassword)
        presenter.didNextStep(action: .forgotPassword)
    }
}

extension SavedAccountInteractor: AccountEditingDelegate {
    func accountWasEdited(account: Account) {
        self.account = account
        retrieveAccount()
        presenter.displayAccountEditedSnackbar()
    }

    func accountWasRemoved() {
        presenter.didNextStep(action: .restart)
        presenter.displayAccountRemovedSnackbar()
    }
}

private extension SavedAccountInteractor {
    func errorHandler(message: String?, errorEvent: ErrorType) {
        trackEvent(.error(errorEvent))
        let message = message ?? Localizable.Error.default
        presenter.displayError(message)
    }

    func trackEvent(_ event: SavedAccountEvents) {
        let event = Tracker(eventType: event)
        dependencies.analytics.log(event)
    }
}
