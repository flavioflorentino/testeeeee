import UIKit
import UI

enum SavedAccountAction: Equatable {
    case faq
    case complete(_ userAuthResponse: UserAuthResponse)
    case edit(_ account: Account, editingDelegate: AccountEditingDelegate)
    case anotherAccount
    case forgotPassword
    case restart
    case snackbar(_ snackbar: ApolloSnackbar)
    case network

    static func == (lhs: SavedAccountAction, rhs: SavedAccountAction) -> Bool {
        switch (lhs, rhs) {
        case let (.complete(lhs), .complete(rhs)):
            return lhs == rhs
        case let (.edit(lhs, _), .edit(rhs, _)):
            return lhs == rhs
        case (.faq, .faq),
             (.anotherAccount, .anotherAccount),
             (.forgotPassword, .forgotPassword),
             (.restart, .restart),
             (.network, .network):
            return true
        case let (.snackbar(lhs), .snackbar(rhs)):
            return lhs == rhs
        default:
            return false
        }
    }
}

protocol SavedAccountCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: SavedAccountAction)
}

final class SavedAccountCoordinator {
    weak var viewController: UIViewController?
    private weak var delegate: AuthenticationCoordinating?

    init(delegate: AuthenticationCoordinating) {
        self.delegate = delegate
    }
}

// MARK: - SavedAccountCoordinating
extension SavedAccountCoordinator: SavedAccountCoordinating {
    func perform(action: SavedAccountAction) {
        switch action {
        case .faq:
            delegate?.faq()
        case let .complete(userAuthResponse):
            delegate?.complete(with: userAuthResponse)
        case let .edit(account, editingDelegate):
            delegate?.accountEditing(account, hasDeleteButton: true, delegate: editingDelegate)
        case .anotherAccount:
            delegate?.login(isAddingAccount: true)
        case .forgotPassword:
            delegate?.forgotPassword()
        case .restart:
            delegate?.start()
        case let .snackbar(snackbar):
            delegate?.snackbar(snackbar)
        case .network:
            delegate?.networkSnackbar()
        }
    }
}
