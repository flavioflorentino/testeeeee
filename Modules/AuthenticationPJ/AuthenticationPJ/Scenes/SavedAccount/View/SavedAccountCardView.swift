import AssetsKit
import UI
import UIKit
import SnapKit

final class SavedAccountCardView: UIView {
    private lazy var logoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.imageStyle(AvatarImageStyle())
        return imageView
    }()
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .small))
        return label
    }()
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
        return label
    }()
    private lazy var documentLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
        return label
    }()
    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [nameLabel, descriptionLabel, documentLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    private lazy var editButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.Saved.Account.edit, for: .normal)
        button.buttonStyle(LinkButtonStyle(size: .small))
        return button
    }()
    
    init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(account: Account, target: Any, action: Selector) {
        logoImageView.showActivityIndicator = true
        logoImageView.setImage(url: URL(string: account.imageUrl ?? ""),
                               placeholder: Resources.Placeholders.greenAvatarPlaceholder.image)
        nameLabel.text = account.companyName
        descriptionLabel.isHidden = account.description == nil
        descriptionLabel.text = account.description
        documentLabel.text = StringObfuscationMasker.mask(cnpj: account.document)
        editButton.addTarget(target, action: action, for: .touchUpInside)
    }
}

extension SavedAccountCardView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubviews(logoImageView, contentStackView, editButton)
    }
    
    func setupConstraints() {
        logoImageView.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.top.equalToSuperview().offset(Spacing.base04)
        }
        contentStackView.snp.makeConstraints {
            $0.leading.equalTo(logoImageView.snp.trailing).offset(Spacing.base02)
            $0.trailing.lessThanOrEqualToSuperview().offset(-Spacing.base02)
            $0.top.equalToSuperview().offset(Spacing.base04)
        }
        editButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.top.equalTo(contentStackView.snp.bottom).offset(Spacing.base03)
            $0.bottom.equalToSuperview().offset(-Spacing.base01)
        }
    }
    
    func configureViews() {
        viewStyle(CardViewStyle())
            .with(\.shadow, .light(color: .black(), opacity: .light))
            .with(\.backgroundColor, .backgroundPrimary())
            .with(\.border, .light(color: .grayscale200()))
    }
}
