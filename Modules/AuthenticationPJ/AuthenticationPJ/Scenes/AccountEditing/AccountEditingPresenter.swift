import Foundation

protocol AccountEditingPresenting: AnyObject {
    var viewController: AccountEditingDisplaying? { get set }
    func didNextStep(action: AccountEditingAction)
    func displayAccount(_ account: Account)
    func displayUsernameError()
    func dismissUsernameError()
    func displayDescription(_ count: Int, hasError: Bool)
    func enableButton()
    func disableButton()
    func displayRemoveAccount()
    func dismissRemoveAccount()
    func displayRemoveButton()
    func dismissRemoveButton()
}

final class AccountEditingPresenter {
    private let coordinator: AccountEditingCoordinating
    weak var viewController: AccountEditingDisplaying?
    fileprivate typealias Localizable = Strings.Account.Editing

    init(coordinator: AccountEditingCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - AccountEditingPresenting
extension AccountEditingPresenter: AccountEditingPresenting {
    func didNextStep(action: AccountEditingAction) {
        coordinator.perform(action: action)
    }
    
    func displayAccount(_ account: Account) {
        viewController?.displayAccount(account)
    }

    func displayUsernameError() {
        let message = Localizable.Error.username
        viewController?.displayUsernameError(message)
    }

    func dismissUsernameError() {
        viewController?.dismissUsernameError()
    }

    func displayDescription(_ count: Int, hasError: Bool) {
        let message = hasError
            ? Localizable.Count.error(count)
            : Localizable.count(count)
        hasError
            ? viewController?.displayDescriptionError(message)
            : viewController?.displayDescription(message)
    }

    func enableButton() {
        viewController?.enableButton()
    }

    func disableButton() {
        viewController?.disableButton()
    }
    
    func displayRemoveAccount() {
        viewController?.displayRemoveAccount()
    }

    func dismissRemoveAccount() {
        viewController?.dismissRemoveAccount()
    }

    func displayRemoveButton() {
        viewController?.displayRemoveButton()
    }

    func dismissRemoveButton() {
        viewController?.dismissRemoveButton()
    }
}
