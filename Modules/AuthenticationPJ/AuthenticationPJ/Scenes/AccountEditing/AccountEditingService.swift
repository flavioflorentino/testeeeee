import Foundation
import Core

protocol AccountEditingServicing {
    func updateAccount(_ account: Account)
    func removeAccount(_ account: Account)
}

final class AccountEditingService {
    typealias Dependencies = HasAuthenticationWorker
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - AccountEditingServicing
extension AccountEditingService: AccountEditingServicing {
    func updateAccount(_ account: Account) {
        dependencies.authenticationWorker.updateAccount(account)
    }
    
    func removeAccount(_ account: Account) {
        dependencies.authenticationWorker.removeAccount(account)
    }
}
