import UIKit

enum AccountEditingFactory {
    static func make(account: Account, hasDeleteButton: Bool, delegate: AccountEditingDelegate) -> AccountEditingViewController {
        let container = DependencyContainer()
        let service: AccountEditingServicing = AccountEditingService(dependencies: container)
        let coordinator: AccountEditingCoordinating = AccountEditingCoordinator(delegate: delegate)
        let presenter: AccountEditingPresenting = AccountEditingPresenter(coordinator: coordinator)
        let interactor = AccountEditingInteractor(service: service, presenter: presenter, account: account, hasDeleteButton: hasDeleteButton, dependencies: container)
        let viewController = AccountEditingViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
