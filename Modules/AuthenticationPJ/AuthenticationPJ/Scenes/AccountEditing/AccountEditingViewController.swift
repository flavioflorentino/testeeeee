import AssetsKit
import UI
import UIKit
import SnapKit

protocol AccountEditingDisplaying: AnyObject {
    func displayAccount(_ account: Account)
    func displayUsernameError(_ message: String)
    func dismissUsernameError()
    func displayDescription(_ message: String)
    func displayDescriptionError(_ message: String)
    func enableButton()
    func disableButton()
    func displayRemoveAccount()
    func dismissRemoveAccount()
    func displayRemoveButton()
    func dismissRemoveButton()
}

private extension AccountEditingViewController.Layout {
    enum Animation {
        static let time = 0.3
    }
    enum Feedback {
        static let alphaVisible: CGFloat = 1.0
        static let alphaInvisible: CGFloat = 0.0
    }
}

final class AccountEditingViewController: ViewController<AccountEditingInteracting, UIView> {
    fileprivate enum Layout { }
    fileprivate typealias Localizable = Strings.Account.Editing
    private lazy var keyboardHandler = KeyboardHandler()
    private lazy var scrollView = UIScrollView()
    private lazy var containerView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            SpacerView(size: Spacing.base03),
            accessTitleLabel,
            SpacerView(size: Spacing.base01),
            accessDescriptionLabel,
            SpacerView(size: Spacing.base02),
            documentStackView,
            usernameStackView,
            identifierTitleLabel,
            SpacerView(size: Spacing.base01),
            identifierDescriptionLabel,
            SpacerView(size: Spacing.base02),
            identifierStackView,
            confirmButton,
            SpacerView(size: Spacing.base02),
            removeButton,
            SpacerView(size: Spacing.base02)
        ])
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var closeButton: UIButton = {
        let button = UIButton()
        button.setImage(Resources.Icons.icoClose.image, for: .normal)
        button.addTarget(self, action: #selector(didTapCloseButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var accessTitleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.Access.title
        label.labelStyle(TitleLabelStyle(type: .small))
        return label
    }()
    private lazy var accessDescriptionLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.Access.description
        label.labelStyle(BodyPrimaryLabelStyle())
        return label
    }()
    private lazy var documentTextField: UIPPFloatingTextField = {
        let textField = defaultTextField(with: Localizable.document)
        textField.textColor = Colors.grayscale400.color
        textField.isEnabled = false
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField.keyboardType = .numberPad
        return textField
    }()
    private lazy var documentLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.Document.description
        label.labelStyle(CaptionLabelStyle())
            .with(\.textColor, .grayscale400())
        return label
    }()
    private lazy var documentStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            documentTextField,
            documentLabel,
            SpacerView(size: Spacing.base02)
        ])
        stackView.axis = .vertical
        return stackView
    }()
    private lazy var usernameTextField: UIPPFloatingTextField = {
        let textField = defaultTextField(with: Localizable.username)
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        return textField
    }()
    private lazy var usernameErrorLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
            .with(\.textColor, .critical900())
        label.isHidden = true
        return label
    }()
    private lazy var usernameStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            usernameTextField,
            usernameErrorLabel,
            SpacerView(size: Spacing.base05)
        ])
        stackView.axis = .vertical
        return stackView
    }()
    private lazy var identifierTitleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.Identifier.title
        label.labelStyle(TitleLabelStyle(type: .small))
        return label
    }()
    private lazy var identifierDescriptionLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.Identifier.description
        label.labelStyle(BodyPrimaryLabelStyle())
        return label
    }()
    private lazy var identifierTextField: UIPPFloatingTextField = {
        let textField = defaultTextField(with: Localizable.description)
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        return textField
    }()
    private lazy var identifierErrorLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
            .with(\.textColor, .grayscale700())
        return label
    }()
    private lazy var identifierStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            identifierTextField,
            identifierErrorLabel,
            SpacerView(size: Spacing.base02)
        ])
        stackView.axis = .vertical
        return stackView
    }()
    private lazy var confirmButton: UIButton = {
        let button = UIButton(type: .custom)
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Localizable.confirm, for: .normal)
        button.addTarget(self, action: #selector(didTapConfirm(_:)), for: .touchUpInside)
        button.isEnabled = false
        return button
    }()
    private lazy var removeButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.remove, for: .normal)
        button.buttonStyle(DangerButtonStyle(icon: (name: .exclamationCircle, alignment: IconAlignment.left)))
        button.addTarget(self, action: #selector(didTapRemove(_:)), for: .touchUpInside)
        return button
    }()
    private lazy var feedbackView: ApolloFeedbackView = {
        let title = Localizable.Remove.title
        let description = NSAttributedString(string: Localizable.Remove.description)
        let confirmTitle = Localizable.Remove.confirm
        let cancelTitle = Localizable.Remove.cancel
        let content = ApolloFeedbackViewContent(type: .icon(style: .error),
                                                title: title,
                                                description: description,
                                                primaryButtonTitle: confirmTitle,
                                                secondaryButtonTitle: cancelTitle)
        let feedbackView = ApolloFeedbackView(content: content, style: .danger)
        feedbackView.alpha = Layout.Feedback.alphaInvisible
        setupCallbacks(on: feedbackView)
        return feedbackView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.retrieveInfos()
    }
    
    override func buildViewHierarchy() {
        scrollView.addSubview(containerView)
        view.addSubview(scrollView)
        view.addSubview(feedbackView)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.leading.equalTo(view.compatibleSafeArea.leading)
            $0.top.equalTo(view.compatibleSafeArea.top)
            $0.trailing.equalTo(view.compatibleSafeArea.trailing)
            $0.bottom.lessThanOrEqualTo(view.compatibleSafeArea.bottom)
        }
        
        containerView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.height.equalToSuperview().priority(.low)
            $0.width.equalToSuperview().offset(-Spacing.base04)
            $0.centerX.equalToSuperview()
        }

        feedbackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    override func configureViews() {
        view.backgroundColor = .backgroundPrimary()
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: closeButton)
        keyboardHandler.handle { [weak self] keyboardSize in
            guard let self = self else { return }
            self.scrollView.snp.updateConstraints {
                $0.bottom.lessThanOrEqualTo(self.view.compatibleSafeArea.bottom).inset(keyboardSize)
            }
            self.view.layoutIfNeeded()
        }
        let gesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(gesture)
    }
}

// MARK: - AccountEditingDisplaying
extension AccountEditingViewController: AccountEditingDisplaying {
    func displayAccount(_ account: Account) {
        title = account.companyName
        documentTextField.text = StringObfuscationMasker.mask(cnpj: account.document)
        usernameTextField.text = account.username
        identifierTextField.text = account.description
    }
    
    func displayUsernameError(_ message: String) {
        setError(true, in: usernameTextField, and: usernameErrorLabel, with: message)
    }
    
    func dismissUsernameError() {
        setError(false, in: usernameTextField, and: usernameErrorLabel)
    }
    
    func displayDescription(_ message: String) {
        identifierErrorLabel.text = message
        identifierErrorLabel.textColor = .grayscale700()
    }

    func displayDescriptionError(_ message: String) {
        identifierErrorLabel.text = message
        identifierErrorLabel.textColor = .critical900()
    }
    
    func enableButton() {
        confirmButton.isEnabled = true
    }
    
    func disableButton() {
        confirmButton.isEnabled = false
    }
    
    func displayRemoveAccount() {
        removeAccount(isHidden: false)
    }

    func dismissRemoveAccount() {
        removeAccount(isHidden: true)
    }

    func displayRemoveButton() {
        removeButton.isHidden = false
    }

    func dismissRemoveButton() {
        removeButton.isHidden = true
    }
}

@objc
private extension AccountEditingViewController {
    func textFieldDidChange(_ sender: UITextField) {
        interactor.validateFields(username: usernameTextField.text, description: identifierTextField.text)
    }
    
    func didTapCloseButton(_ sender: UIButton) {
        interactor.close()
    }
    
    func didTapConfirm(_ sender: UIButton) {
        interactor.updateAccount(username: usernameTextField.text, description: identifierTextField.text)
    }
    
    func didTapRemove(_ sender: UIButton) {
        interactor.remove()
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func defaultTextField(with placeholder: String) -> UIPPFloatingTextField {
        let textField = UIPPFloatingTextField()
        textField.placeholderColor = Colors.grayscale400.color
        textField.titleColor = Colors.grayscale400.color
        textField.selectedTitleColor = Colors.branding600.color
        textField.textColor = Colors.grayscale750.color
        textField.placeholder = placeholder
        textField.title = placeholder
        textField.selectedTitle = placeholder
        return textField
    }
    
    func setError(_ hasError: Bool, in textField: UIPPFloatingTextField, and label: UILabel, with message: String = "") {
        if hasError {
            textField.lineColor = Colors.critical900.color
            textField.selectedLineColor = Colors.critical900.color
            textField.titleColor = Colors.critical900.color
            textField.selectedTitleColor = Colors.critical900.color
        } else {
            textField.lineColor = Colors.grayscale400.color
            textField.selectedLineColor = Colors.branding300.color
            textField.titleColor = Colors.grayscale400.color
            textField.selectedTitleColor = Colors.branding600.color
        }
        
        label.isHidden = !hasError
        label.text = message
        
        UIView.animate(withDuration: Layout.Animation.time) {
            self.view.layoutIfNeeded()
        }
    }

    func setupCallbacks(on feedbackView: ApolloFeedbackView) {
        feedbackView.didTapPrimaryButton = { [weak self] in
            self?.interactor.confirmRemoveAccount()
        }

        feedbackView.didTapSecondaryButton = { [weak self] in
            self?.interactor.cancelRemoveAccount()
        }
    }

    func removeAccount(isHidden: Bool) {
        dismissKeyboard()
        UIView.animate(withDuration: Layout.Animation.time) {
            self.feedbackView.alpha = isHidden
                ? Layout.Feedback.alphaInvisible
                : Layout.Feedback.alphaVisible
        }
    }
}
