import UIKit

protocol AccountEditingDelegate: AnyObject {
    func accountWasEdited(account: Account)
    func accountWasRemoved()
}

extension AccountEditingDelegate {
    func accountWasRemoved() { }
}

enum AccountEditingAction: Equatable {
    case close
    case edited(_ account: Account)
    case removed
}

protocol AccountEditingCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: AccountEditingAction)
}

final class AccountEditingCoordinator {
    weak var viewController: UIViewController?
    private weak var delegate: AccountEditingDelegate?

    init(delegate: AccountEditingDelegate?) {
        self.delegate = delegate
    }
}

// MARK: - AccountEditingCoordinating
extension AccountEditingCoordinator: AccountEditingCoordinating {
    func perform(action: AccountEditingAction) {
        switch action {
        case .close:
            viewController?.dismiss(animated: true)
        case let .edited(account):
            viewController?.dismiss(animated: true) {
                self.delegate?.accountWasEdited(account: account)
            }
        case .removed:
            viewController?.dismiss(animated: true) {
                self.delegate?.accountWasRemoved()
            }
        }
    }
}
