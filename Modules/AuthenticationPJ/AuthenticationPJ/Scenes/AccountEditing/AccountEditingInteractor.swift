import AnalyticsModule
import Foundation

protocol AccountEditingInteracting: AnyObject {
    func close()
    func retrieveInfos()
    func validateFields(username: String?, description: String?)
    func updateAccount(username: String?, description: String?)
    func remove()
    func confirmRemoveAccount()
    func cancelRemoveAccount()
}

final class AccountEditingInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    private let service: AccountEditingServicing
    private let presenter: AccountEditingPresenting
    private let account: Account
    private let hasDeleteButton: Bool
    private let minimumCountRule = 1
    private let descriptionCountRule = 20

    init(service: AccountEditingServicing,
         presenter: AccountEditingPresenting,
         account: Account,
         hasDeleteButton: Bool,
         dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.account = account
        self.hasDeleteButton = hasDeleteButton
        self.dependencies = dependencies
    }
}

// MARK: - AccountEditingInteracting
extension AccountEditingInteractor: AccountEditingInteracting {
    func close() {
        presenter.didNextStep(action: .close)
    }

    func retrieveInfos() {
        presenter.displayAccount(account)
        let descriptionCount = account.description?.count ?? 0
        presenter.displayDescription(descriptionCount,
                                     hasError: descriptionCount > descriptionCountRule)
        hasDeleteButton
            ? presenter.displayRemoveButton()
            : presenter.dismissRemoveButton()
    }
    
    func validateFields(username: String?, description: String?) {
        guard let username = username,
              let description = description else {
            presenter.disableButton()
            return
        }

        if username.isEmpty {
            presenter.displayUsernameError()
        } else {
            presenter.dismissUsernameError()
        }
        presenter.displayDescription(description.count,
                                     hasError: description.count > descriptionCountRule)

        if username.count >= minimumCountRule,
           username != account.username || description != account.description,
           description.count <= descriptionCountRule {
            presenter.enableButton()
        } else {
            presenter.disableButton()
        }
    }
    
    func updateAccount(username: String?, description: String?) {
        guard let username = username,
              let description = description else {
            return
        }
        let event = AccountEditingEvents.edit(!description.isEmpty,
                                              isUserEdited: username != account.username,
                                              isDescriptionEdited: description != account.description)
        trackEvent(event)
        let oldAccount = account
        let updatedAccount = Account(id: oldAccount.id,
                                     document: oldAccount.document,
                                     username: username,
                                     companyName: oldAccount.companyName,
                                     lastLogin: oldAccount.lastLogin,
                                     imageUrl: oldAccount.imageUrl,
                                     description: description)
        service.updateAccount(updatedAccount)
        presenter.didNextStep(action: .edited(updatedAccount))
    }
    
    func remove() {
        presenter.displayRemoveAccount()
    }

    func confirmRemoveAccount() {
        trackEvent(.delete)
        service.removeAccount(account)
        presenter.dismissRemoveAccount()
        presenter.didNextStep(action: .removed)
    }

    func cancelRemoveAccount() {
        presenter.dismissRemoveAccount()
    }
}

private extension AccountEditingInteractor {
    func trackEvent(_ event: AccountEditingEvents) {
        let event = Tracker(eventType: event)
        dependencies.analytics.log(event)
    }
}
