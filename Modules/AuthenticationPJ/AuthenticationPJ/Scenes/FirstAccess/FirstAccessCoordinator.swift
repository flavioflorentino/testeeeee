import UIKit

enum FirstAccessAction: Equatable {
    case faq
    case complete(_ userAuthResponse: UserAuthResponse)
    case forgotPassword
    case network
}

protocol FirstAccessCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: FirstAccessAction)
}

final class FirstAccessCoordinator {
    weak var viewController: UIViewController?
    private weak var delegate: AuthenticationCoordinating?

    init(delegate: AuthenticationCoordinating) {
        self.delegate = delegate
    }
}

// MARK: - FirstAccessCoordinating
extension FirstAccessCoordinator: FirstAccessCoordinating {
    func perform(action: FirstAccessAction) {
        switch action {
        case .faq:
            delegate?.faq()
        case let .complete(userAuthResponse):
            delegate?.complete(with: userAuthResponse)
        case .forgotPassword:
            delegate?.forgotPassword()
        case .network:
            delegate?.networkSnackbar()
        }
    }
}
