import Foundation

protocol FirstAccessPresenting: AnyObject {
    var viewController: FirstAccessDisplaying? { get set }
    func didNextStep(action: FirstAccessAction)
    func displayDocumentError(_ message: String)
    func displayUsernameError(_ message: String)
    func displayPasswordError(_ message: String)
    func dismissErrors()
    func displayNetworkBanner()
    func enableLoginButton()
    func disableLoginButton()
    func displayLoading()
    func dismissLoading()
}

final class FirstAccessPresenter {
    private let coordinator: FirstAccessCoordinating
    weak var viewController: FirstAccessDisplaying?

    init(coordinator: FirstAccessCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - FirstAccessPresenting
extension FirstAccessPresenter: FirstAccessPresenting {
    func didNextStep(action: FirstAccessAction) {
        coordinator.perform(action: action)
    }
    
    func displayDocumentError(_ message: String) {
        viewController?.displayDocumentError(message)
    }

    func displayUsernameError(_ message: String) {
        viewController?.displayUsernameError(message)
    }

    func displayPasswordError(_ message: String) {
        viewController?.displayPasswordError(message)
    }
    
    func dismissErrors() {
        viewController?.dismissDocumentError()
        viewController?.dismissUsernameError()
        viewController?.dismissPasswordError()
    }

    func displayNetworkBanner() {
        coordinator.perform(action: .network)
    }

    func enableLoginButton() {
        viewController?.enableLoginButton()
    }

    func disableLoginButton() {
        viewController?.disableLoginButton()
    }

    func displayLoading() {
        viewController?.displayLoading()
    }

    func dismissLoading() {
        viewController?.dismissLoading()
    }
}
