import UI
import UIKit
import SnapKit
import AssetsKit

protocol FirstAccessDisplaying: AnyObject {
    func displayDocumentError(_ message: String)
    func dismissDocumentError()
    func displayUsernameError(_ message: String)
    func dismissUsernameError()
    func displayPasswordError(_ message: String)
    func dismissPasswordError()
    func enableLoginButton()
    func disableLoginButton()
    func displayLoading()
    func dismissLoading()
}

private extension FirstAccessViewController.Layout {
    enum Animation {
        static let time = 0.3
    }
}

final class FirstAccessViewController: ViewController<FirstAccessInteracting, UIView> {
    fileprivate enum Layout { }
    fileprivate typealias Localizable = Strings.First.Access
    private lazy var keyboardHandler = KeyboardHandler()
    private lazy var scrollView = UIScrollView()
    private lazy var containerView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            SpacerView(size: Spacing.base04),
            descriptionLabel,
            SpacerView(size: Spacing.base04),
            documentStackView,
            usernameStackView,
            passwordStackView,
            rememberContainer,
            SpacerView(size: Spacing.base03),
            loginButton,
            SpacerView(size: Spacing.base04),
            forgotButton
        ])
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.description
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .black())
        return label
    }()
    private lazy var documentTextField: UIPPFloatingTextField = {
        let textField = defaultTextField(with: Localizable.document)
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField.keyboardType = .numberPad
        return textField
    }()
    private lazy var documentErrorLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
            .with(\.textColor, .critical900())
        label.isHidden = true
        return label
    }()
    private lazy var documentStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            documentTextField,
            SpacerView(size: Spacing.base01),
            documentErrorLabel,
            SpacerView(size: Spacing.base02)
        ])
        stackView.axis = .vertical
        return stackView
    }()
    private lazy var usernameTextField: UIPPFloatingTextField = {
        let textField = defaultTextField(with: Localizable.username)
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        return textField
    }()
    private lazy var usernameErrorLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
            .with(\.textColor, .critical900())
        label.isHidden = true
        return label
    }()
    private lazy var usernameStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            usernameTextField,
            SpacerView(size: Spacing.base01),
            usernameErrorLabel,
            SpacerView(size: Spacing.base02)
        ])
        stackView.axis = .vertical
        return stackView
    }()
    private lazy var passwordToggleButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(Resources.Icons.icoPasswordInvisible.image, for: .normal)
        button.setImage(Resources.Icons.icoPasswordVisible.image, for: .selected)
        button.addTarget(self, action: #selector(didTogglePassword(_:)), for: .touchDown)
        return button
    }()
    private lazy var passwordTextField: UIPPFloatingTextField = {
        let textField = defaultTextField(with: Localizable.password)
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField.isSecureTextEntry = true
        textField.rightView = passwordToggleButton
        textField.rightViewMode = .always
        return textField
    }()
    private lazy var passwordErrorLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
            .with(\.textColor, .critical900())
        label.isHidden = true
        return label
    }()
    private lazy var passwordStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            passwordTextField,
            SpacerView(size: Spacing.base01),
            passwordErrorLabel,
            SpacerView(size: Spacing.base02)
        ])
        stackView.axis = .vertical
        return stackView
    }()
    private lazy var rememberLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.remind
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
        return label
    }()
    private lazy var rememberSwitch: UISwitch = {
        let switcher = UISwitch()
        switcher.onTintColor = Colors.branding600.color
        switcher.isOn = true
        return switcher
    }()
    private lazy var rememberContainer: UIView = {
        let view = UIView()
        view.viewStyle(CardViewStyle())
            .with(\.shadow, .light(color: .black(.light), opacity: .ultraLight))
        return view
    }()
    private lazy var loginButton: UIButton = {
        let button = UIButton(type: .custom)
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Localizable.login, for: .normal)
        button.addTarget(self, action: #selector(didTapLogin(_:)), for: .touchUpInside)
        button.isEnabled = false
        return button
    }()
    private lazy var forgotButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.Forgot.password, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(didTapForgot(_:)), for: .touchUpInside)
        return button
    }()
    
    private var documentMask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cnpj)
    
    override func buildViewHierarchy() {
        rememberContainer.addSubviews(rememberLabel, rememberSwitch)
        scrollView.addSubview(containerView)
        view.addSubview(scrollView)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalTo(view.compatibleSafeArea.edges)
        }
        
        containerView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.height.equalToSuperview().priority(.low)
            $0.width.equalToSuperview().offset(-Spacing.base04)
            $0.centerX.equalToSuperview()
        }
        
        rememberLabel.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.centerY.equalToSuperview()
        }
        
        rememberSwitch.snp.makeConstraints {
            $0.leading.lessThanOrEqualTo(rememberLabel.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base01)
            $0.top.bottom.equalToSuperview().inset(Spacing.base01)
        }
    }

    override func configureViews() {
        title = Localizable.title
        view.backgroundColor = .backgroundPrimary()
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: Resources.Icons.icoQuestion.image,
            style: .plain,
            target: self,
            action: #selector(didTapFAQ(_:))
        )
        keyboardHandler.handle { [weak self] keyboardSize in
            guard let self = self else { return }
            self.scrollView.snp.updateConstraints {
                $0.bottom.equalTo(self.view.compatibleSafeArea.bottom).inset(keyboardSize)
            }
            self.view.layoutIfNeeded()
        }
        let gesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(gesture)
    }
}

// MARK: - FirstAccessDisplaying
extension FirstAccessViewController: FirstAccessDisplaying {
    func displayDocumentError(_ message: String) {
        setError(true, in: documentTextField, and: documentErrorLabel, with: message)
    }
    
    func dismissDocumentError() {
        setError(false, in: documentTextField, and: documentErrorLabel)
    }
    
    func displayUsernameError(_ message: String) {
        setError(true, in: usernameTextField, and: usernameErrorLabel, with: message)
    }
    
    func dismissUsernameError() {
        setError(false, in: usernameTextField, and: usernameErrorLabel)
    }
    
    func displayPasswordError(_ message: String) {
        setError(true, in: passwordTextField, and: passwordErrorLabel, with: message)
    }
    
    func dismissPasswordError() {
        setError(false, in: passwordTextField, and: passwordErrorLabel)
    }
    
    func enableLoginButton() {
        loginButton.isEnabled = true
    }
    
    func disableLoginButton() {
        loginButton.isEnabled = false
    }
    
    func displayLoading() {
        loginButton.startLoading()
    }
    
    func dismissLoading() {
        loginButton.stopLoading()
    }
}

@objc
private extension FirstAccessViewController {
    func didTapFAQ(_ sender: UIBarButtonItem) {
        interactor.faq()
    }
    
    func textFieldDidChange(_ sender: UITextField) {
        if sender === documentTextField {
            documentTextField.text = documentMask.maskedText(from: sender.text)
        }
        interactor.validateFields(document: documentTextField.text,
                                  username: usernameTextField.text,
                                  password: passwordTextField.text)
    }
    
    func didTogglePassword(_ sender: UIButton) {
        sender.isSelected.toggle()
        passwordTextField.isSecureTextEntry = sender.isSelected == false
    }
    
    func didTapLogin(_ sender: UIButton) {
        interactor.authorize(document: documentTextField.text,
                             username: usernameTextField.text,
                             password: passwordTextField.text,
                             mustRemind: rememberSwitch.isOn)
    }
    
    func didTapForgot(_ sender: UIButton) {
        interactor.forgotPassword()
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func defaultTextField(with placeholder: String) -> UIPPFloatingTextField {
        let textField = UIPPFloatingTextField()
        textField.placeholderColor = Colors.grayscale400.color
        textField.titleColor = Colors.grayscale400.color
        textField.selectedTitleColor = Colors.branding600.color
        textField.textColor = Colors.grayscale750.color
        textField.placeholder = placeholder
        textField.title = placeholder
        textField.selectedTitle = placeholder
        return textField
    }
    
    func setError(_ hasError: Bool, in textField: UIPPFloatingTextField, and label: UILabel, with message: String = "") {
        if hasError {
            textField.lineColor = Colors.critical900.color
            textField.selectedLineColor = Colors.critical900.color
            textField.titleColor = Colors.critical900.color
            textField.selectedTitleColor = Colors.critical900.color
        } else {
            textField.lineColor = Colors.grayscale400.color
            textField.selectedLineColor = Colors.branding300.color
            textField.titleColor = Colors.grayscale400.color
            textField.selectedTitleColor = Colors.branding600.color
        }
        
        label.isHidden = !hasError
        label.text = message
        
        UIView.animate(withDuration: Layout.Animation.time) {
            self.view.layoutIfNeeded()
        }
    }
}
