import Foundation
import Core

protocol FirstAccessServicing {
    func authentication(document: String, username: String, password: String, mustRemind: Bool, completion: @escaping (Result<UserAuthResponse, AuthenticationError>) -> Void)
}

final class FirstAccessService {
    typealias Dependencies = HasMainQueue & HasAuthenticationWorker
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - FirstAccessServicing
extension FirstAccessService: FirstAccessServicing {
    func authentication(document: String, username: String, password: String, mustRemind: Bool, completion: @escaping (Result<UserAuthResponse, AuthenticationError>) -> Void) {
        dependencies.authenticationWorker.authentication(document: document,
                                                         username: username,
                                                         password: password,
                                                         mustRemind: mustRemind) {  [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result)
            }
        }
    }
}
