import AnalyticsModule
import Foundation
import Core

protocol FirstAccessInteracting: AnyObject {
    func faq()
    func validateFields(document: String?, username: String?, password: String?)
    func authorize(document: String?, username: String?, password: String?, mustRemind: Bool)
    func forgotPassword()
}

final class FirstAccessInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    private let service: FirstAccessServicing
    private let presenter: FirstAccessPresenting
    private typealias Localizable = Strings.First.Access.Error
    private let documentSize = 14
    private let minimumCountRule = 1

    init(service: FirstAccessServicing, presenter: FirstAccessPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - FirstAccessInteracting
extension FirstAccessInteractor: FirstAccessInteracting {
    func faq() {
        trackEvent(.faq)
        presenter.didNextStep(action: .faq)
    }

    func validateFields(document: String?, username: String?, password: String?) {
        guard let document = document,
              let username = username,
              let password = password else {
            return
        }

        if document.onlyNumbers.count == documentSize, document.isValidCNPJ == false {
            trackEvent(.error(.cnpjInvalid))
            presenter.displayDocumentError(Localizable.Document.default)
        } else {
            presenter.dismissErrors()
        }
        
        guard document.isValidCNPJ,
              username.count >= minimumCountRule,
              password.count >= minimumCountRule else {
            presenter.disableLoginButton()
            return
        }
        
        presenter.enableLoginButton()
    }

    func authorize(document: String?, username: String?, password: String?, mustRemind: Bool) {
        guard let document = document,
              let username = username,
              let password = password else {
            return
        }
        presenter.displayLoading()
        presenter.dismissErrors()
        service.authentication(document: document, username: username, password: password, mustRemind: mustRemind) { [weak self] result in
            guard let self = self else { return }
            self.presenter.dismissLoading()
            switch result {
            case let .success(response):
                self.trackEvent(.success(false, saveData: mustRemind))
                if mustRemind {
                    self.trackEvent(.savedData)
                }
                let action = FirstAccessAction.complete(response)
                self.presenter.didNextStep(action: action)
            case .failure(.network):
                self.presenter.displayNetworkBanner()
            case let .failure(.document(message)):
                self.trackEvent(.error(.cnpjNotRegistered))
                let message = message ?? Localizable.Document.default
                self.presenter.displayDocumentError(message)
            case let .failure(.auth(message)), let .failure(.user(message)), let .failure(.seller(message)):
                self.trackEvent(.error(.invalidLoginData))
                let message = message ?? Localizable.default
                self.presenter.displayUsernameError("")
                self.presenter.displayPasswordError(message)
            }
        }
    }

    func forgotPassword() {
        trackEvent(.recoverPassword)
        presenter.didNextStep(action: .forgotPassword)
    }
}

private extension FirstAccessInteractor {
    func trackEvent(_ event: FirstAccessEvents) {
        let event = Tracker(eventType: event)
        dependencies.analytics.log(event)
    }
}
