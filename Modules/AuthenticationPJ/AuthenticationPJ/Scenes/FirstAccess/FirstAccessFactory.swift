import UIKit

enum FirstAccessFactory {
    static func make(delegate: AuthenticationCoordinating) -> FirstAccessViewController {
        let container = DependencyContainer()
        let service: FirstAccessServicing = FirstAccessService(dependencies: container)
        let coordinator: FirstAccessCoordinating = FirstAccessCoordinator(delegate: delegate)
        let presenter: FirstAccessPresenting = FirstAccessPresenter(coordinator: coordinator)
        let interactor = FirstAccessInteractor(service: service, presenter: presenter, dependencies: container)
        let viewController = FirstAccessViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
