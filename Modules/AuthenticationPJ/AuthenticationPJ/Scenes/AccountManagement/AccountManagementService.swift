import Foundation
import Core

protocol AccountManagementServicing {
    func retrieveAccounts(_ completion: @escaping ([Account]) -> Void)
    func updateAccount(_ account: Account)
    func removeAccount(_ account: Account)
    func removeAccounts()
}

final class AccountManagementService {
    typealias Dependencies = HasAuthenticationWorker
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - AccountManagementServicing
extension AccountManagementService: AccountManagementServicing {
    func retrieveAccounts(_ completion: @escaping ([Account]) -> Void) {
        dependencies.authenticationWorker.retrieveAccounts(completion: completion)
    }
    
    func updateAccount(_ account: Account) {
        dependencies.authenticationWorker.updateAccount(account)
    }
    
    func removeAccount(_ account: Account) {
        dependencies.authenticationWorker.removeAccount(account)
    }

    func removeAccounts() {
        dependencies.authenticationWorker.removeAccounts()
    }
}
