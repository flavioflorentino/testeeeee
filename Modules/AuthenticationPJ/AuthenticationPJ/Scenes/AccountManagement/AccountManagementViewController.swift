import AssetsKit
import UI
import UIKit

protocol AccountManagementDisplaying: AnyObject {
    func displayAccounts(_ accounts: [Account])
    func updateAccounts(_ accounts: [Account])
    func endEditingMode()
    func displayRemove(title: String, description: String, account: Account)
    func displayRemove(title: String, description: String)
}

private extension AccountManagementViewController.Layout {
    enum Animation {
        static let time = 0.3
    }
    enum Button {
        static let alphaVisible: CGFloat = 1.0
        static let alphaInvisible: CGFloat = 0.0
    }
}

final class AccountManagementViewController: ViewController<AccountManagementInteracting, UIView> {
    fileprivate enum Layout { }
    fileprivate typealias Localizable = Strings.Account.Management
    
    private lazy var accessTitleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.title
        label.labelStyle(TitleLabelStyle(type: .small))
        return label
    }()
    private lazy var accessDescriptionLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.description
        label.labelStyle(BodyPrimaryLabelStyle())
        return label
    }()
    private lazy var editButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.edit, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(didTapEdit(_:)), for: .touchUpInside)
        return button
    }()
    private lazy var removeButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.remove, for: .normal)
        button.buttonStyle(LinkButtonStyle())
            .with(\.textAttributedColor, (color: .critical600(), state: .normal))
            .with(\.textAttributedColor, (color: .critical900(), state: .highlighted))
        button.addTarget(self, action: #selector(didTapRemove(_:)), for: .touchUpInside)
        button.alpha = Layout.Button.alphaInvisible
        return button
    }()
    private lazy var footerView: AccountView = {
        let view = AccountView()
        let gesture = UITapGestureRecognizer(target: self, action: #selector(didTapFooter(_:)))
        view.addGestureRecognizer(gesture)
        return view
    }()
    private lazy var accountsTableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.backgroundColor = .backgroundPrimary()
        tableView.rowHeight = Sizing.base11
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.delegate = self
        tableView.register(AccountCell.self, forCellReuseIdentifier: AccountCell.identifier)
        return tableView
    }()
    private lazy var registerButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.register, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(didTapRegister(_:)), for: .touchUpInside)
        return button
    }()
    
    private lazy var dataSource: TableViewDataSource<Int, Account> = {
        let dataSource = TableViewDataSource<Int, Account>(view: accountsTableView)
        dataSource.itemProvider = { tableView, indexPath, account -> AccountCell? in
            let identifier = AccountCell.identifier
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? AccountCell
            cell?.setup(account, delegate: self)
            return cell
        }
        return dataSource
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.retrieveAccounts()
    }
    
    override func buildViewHierarchy() {
        view.addSubviews(accessTitleLabel,
                         accessDescriptionLabel,
                         editButton,
                         removeButton,
                         accountsTableView,
                         registerButton)
    }
    
    override func setupConstraints() {
        accessTitleLabel.snp.makeConstraints {
            $0.leading.equalTo(view.compatibleSafeArea.leading).offset(Spacing.base02)
            $0.top.equalTo(view.compatibleSafeArea.top).offset(Spacing.base01)
            $0.trailing.equalTo(view.compatibleSafeArea.trailing).offset(-Spacing.base02)
        }
        accessDescriptionLabel.snp.makeConstraints {
            $0.leading.equalTo(view.compatibleSafeArea.leading).offset(Spacing.base02)
            $0.top.equalTo(accessTitleLabel.snp.bottom).offset(Spacing.base01)
            $0.trailing.equalTo(view.compatibleSafeArea.trailing).offset(-Spacing.base02)
        }
        editButton.snp.makeConstraints {
            $0.leading.equalTo(view.compatibleSafeArea.leading).offset(Spacing.base02)
            $0.top.equalTo(accessDescriptionLabel.snp.bottom).offset(Spacing.base02)
        }
        removeButton.snp.makeConstraints {
            $0.trailing.equalTo(view.compatibleSafeArea.trailing).offset(-Spacing.base02)
            $0.top.equalTo(accessDescriptionLabel.snp.bottom).offset(Spacing.base02)
        }
        accountsTableView.snp.makeConstraints {
            $0.leading.equalTo(view.compatibleSafeArea.leading).offset(Spacing.base02)
            $0.top.equalTo(editButton.snp.bottom).offset(Spacing.base01)
            $0.trailing.equalTo(view.compatibleSafeArea.trailing).offset(-Spacing.base02)
        }
        registerButton.snp.makeConstraints {
            $0.leading.equalTo(view.compatibleSafeArea.leading).offset(Spacing.base02)
            $0.top.equalTo(accountsTableView.snp.bottom).offset(Spacing.base01)
            $0.trailing.equalTo(view.compatibleSafeArea.trailing).offset(-Spacing.base02)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).offset(-Spacing.base04)
        }
    }

    override func configureViews() {
        title = Localizable.Screen.title
        view.backgroundColor = .backgroundPrimary()
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: Resources.Icons.icoQuestion.image,
            style: .plain,
            target: self,
            action: #selector(didTapFAQ(_:))
        )
        accountsTableView.dataSource = dataSource
        footerView.setup(type: .add)
    }
}

// MARK: - AccountManagementDisplaying
extension AccountManagementViewController: AccountManagementDisplaying {
    func displayAccounts(_ accounts: [Account]) {
        dataSource.remove(section: 0)
        dataSource.add(items: accounts, to: 0)
    }

    func updateAccounts(_ accounts: [Account]) {
        dataSource.update(items: accounts, from: 0)
    }

    func endEditingMode() {
        accountsTableView.setEditing(false, animated: true)
        updateEditMode()
    }

    func displayRemove(title: String, description: String, account: Account) {
        presentDialog(title: title, description: description, account: account)
    }

    func displayRemove(title: String, description: String) {
        presentDialog(title: title, description: description)
    }
}

private extension AccountManagementViewController {
    func presentDialog(title: String, description: String, account: Account? = nil) {
        let removeAction = PopupAction(title: Localizable.Dialog.remove, style: .destructive) {
            guard let account = account else {
                self.interactor.removeAccounts()
                return
            }
            self.interactor.removeAccount(account)
        }
        let cancelAction = PopupAction(title: Localizable.Dialog.cancel, style: .link)
        let dialog = PopupViewController(title: title, description: description)
        dialog.hideCloseButton = true
        dialog.addActions([removeAction, cancelAction])
        showPopup(dialog)
    }
    
    func updateEditMode() {
        let isEditing = accountsTableView.isEditing
        let isNotEditing = isEditing == false
        
        UIView.animate(withDuration: Layout.Animation.time) {
            self.removeButton.alpha = isEditing
                ? Layout.Button.alphaVisible
                : Layout.Button.alphaInvisible
        }
        
        footerView.setEnabled(isNotEditing)
        
        let title = isNotEditing ? Localizable.edit : Localizable.done
        editButton.setTitle(title, for: .normal)
        editButton.textAttributedColor = (color: .brandingBase(), state: .normal)
        editButton.textAttributedColor = (color: .branding700(), state: .highlighted)
    }
}

@objc
private extension AccountManagementViewController {
    func didTapFAQ(_ sender: UIBarButtonItem) {
        interactor.faq()
    }

    func didTapFooter(_ sender: UITapGestureRecognizer) {
        interactor.addAccount()
    }
    
    func didTapEdit(_ sender: UIButton) {
        accountsTableView.isEditing.toggle()
        updateEditMode()
    }
    
    func didTapRemove(_ sender: UIButton) {
        interactor.didTapRemoveAccounts()
    }
    
    func didTapRegister(_ sender: UIButton) {
        interactor.registerAccount()
    }
}

extension AccountManagementViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        interactor.accessAccount(indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        .zero
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        footerView
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        .none
    }
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        false
    }
}

extension AccountManagementViewController: AccountViewDelegate {
    func didTapDelete(identifier: Int) {
        interactor.didTapRemoveAccount(identifier)
    }
    
    func didTapEdit(identifier: Int) {
        interactor.didTapEditAccount(identifier)
    }
}
