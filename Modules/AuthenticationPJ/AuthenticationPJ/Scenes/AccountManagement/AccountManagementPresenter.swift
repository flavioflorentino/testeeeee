import Foundation
import UI

protocol AccountManagementPresenting: AnyObject {
    var viewController: AccountManagementDisplaying? { get set }
    func didNextStep(action: AccountManagementAction)
    func displayAccounts(_ accounts: [Account])
    func updateAccounts(_ accounts: [Account])
    func displayRemove(_ account: Account)
    func displayRemoveAll()
    func displayAccountRemovedSnackbar()
    func displayAccountEditedSnackbar()
    func displayAccountsRemovedSnackbar()
}

final class AccountManagementPresenter {
    fileprivate typealias Localizable = Strings.Account.Management.Dialog
    private let coordinator: AccountManagementCoordinating
    weak var viewController: AccountManagementDisplaying?

    init(coordinator: AccountManagementCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - AccountManagementPresenting
extension AccountManagementPresenter: AccountManagementPresenting {
    func didNextStep(action: AccountManagementAction) {
        coordinator.perform(action: action)
    }

    func displayAccounts(_ accounts: [Account]) {
        viewController?.displayAccounts(accounts)
    }

    func updateAccounts(_ accounts: [Account]) {
        viewController?.updateAccounts(accounts)
    }

    func displayRemove(_ account: Account) {
        viewController?.displayRemove(title: Localizable.Title.single,
                                      description: Localizable.Description.single,
                                      account: account)
    }

    func displayRemoveAll() {
        viewController?.displayRemove(title: Localizable.Title.all,
                                      description: Localizable.Description.all)
    }

    func displayAccountRemovedSnackbar() {
        let snackbar = ApolloSnackbar(text: Strings.General.Account.removed, iconType: .success)
        coordinator.perform(action: .snackbar(snackbar))
    }

    func displayAccountEditedSnackbar() {
        let snackbar = ApolloSnackbar(text: Strings.General.Account.edited, iconType: .success)
        coordinator.perform(action: .snackbar(snackbar))
    }

    func displayAccountsRemovedSnackbar() {
        let snackbar = ApolloSnackbar(text: Strings.General.Accounts.removed, iconType: .success)
        coordinator.perform(action: .snackbar(snackbar))
    }
}
