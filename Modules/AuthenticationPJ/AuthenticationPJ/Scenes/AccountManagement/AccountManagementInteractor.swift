import AnalyticsModule
import Foundation

protocol AccountManagementInteracting: AnyObject {
    func retrieveAccounts()
    func faq()
    func accessAccount(_ index: Int)
    func didTapEditAccount(_ identifier: Int)
    func didTapRemoveAccount(_ identifier: Int)
    func removeAccount(_ account: Account)
    func didTapRemoveAccounts()
    func removeAccounts()
    func addAccount()
    func registerAccount()
}

final class AccountManagementInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    private let service: AccountManagementServicing
    private let presenter: AccountManagementPresenting

    private var accounts: [Account] = []

    init(service: AccountManagementServicing, presenter: AccountManagementPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - AccountManagementInteracting
extension AccountManagementInteractor: AccountManagementInteracting {
    func retrieveAccounts() {
        fetchAccounts {
            self.presenter.displayAccounts(self.accounts)
        }
    }

    func faq() {
        trackEvent(.faq)
        presenter.didNextStep(action: .faq)
    }

    func accessAccount(_ index: Int) {
        let account = accounts[index]
        presenter.didNextStep(action: .access(account))
    }

    func didTapEditAccount(_ identifier: Int) {
        guard let account = findAccountByIdentifier(identifier) else {
            return
        }
        presenter.didNextStep(action: .edit(account, editingDelegate: self))
    }

    func didTapRemoveAccount(_ identifier: Int) {
        guard let account = findAccountByIdentifier(identifier) else {
            return
        }
        presenter.displayRemove(account)
    }
    
    func removeAccount(_ account: Account) {
        trackEvent(.delete(false))
        service.removeAccount(account)
        updateAccounts()
        presenter.displayAccountRemovedSnackbar()
    }

    func didTapRemoveAccounts() {
        presenter.displayRemoveAll()
    }

    func removeAccounts() {
        trackEvent(.delete(true))
        service.removeAccounts()
        presenter.didNextStep(action: .restart)
        presenter.displayAccountsRemovedSnackbar()
    }

    func addAccount() {
        trackEvent(.anotherAccount)
        presenter.didNextStep(action: .add)
    }

    func registerAccount() {
        trackEvent(.register)
        presenter.didNextStep(action: .register)
    }
}

private extension AccountManagementInteractor {
    func findAccountByIdentifier(_ identifier: Int) -> Account? {
        accounts.first { $0.id == identifier }
    }
    
    func fetchAccounts(_ completion: @escaping () -> Void) {
        service.retrieveAccounts { [weak self] accounts in
            guard let self = self else { return }
            self.accounts = accounts
            completion()
        }
    }
    
    func updateAccounts() {
        fetchAccounts {
            self.presenter.updateAccounts(self.accounts)
        }
    }

    func trackEvent(_ event: AccountManagementEvents) {
        let event = Tracker(eventType: event)
        dependencies.analytics.log(event)
    }
}

extension AccountManagementInteractor: AccountEditingDelegate {
    func accountWasEdited(account: Account) {
        retrieveAccounts()
        presenter.displayAccountEditedSnackbar()
    }
}
