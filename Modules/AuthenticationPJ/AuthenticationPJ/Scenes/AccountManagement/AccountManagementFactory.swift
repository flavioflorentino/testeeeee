import UIKit

enum AccountManagementFactory {
    static func make(delegate: AuthenticationCoordinating) -> AccountManagementViewController {
        let container = DependencyContainer()
        
        let service: AccountManagementServicing = AccountManagementService(dependencies: container)
        let coordinator: AccountManagementCoordinating = AccountManagementCoordinator(delegate: delegate)
        let presenter: AccountManagementPresenting = AccountManagementPresenter(coordinator: coordinator)
        let interactor = AccountManagementInteractor(service: service, presenter: presenter, dependencies: container)
        let viewController = AccountManagementViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
