import UIKit
import UI

enum AccountManagementAction: Equatable {
    case faq
    case add
    case edit(_ account: Account, editingDelegate: AccountEditingDelegate)
    case access(_ account: Account)
    case register
    case restart
    case snackbar(_ snackbar: ApolloSnackbar)

    static func == (lhs: AccountManagementAction, rhs: AccountManagementAction) -> Bool {
        switch (lhs, rhs) {
        case let (.edit(lhs, _), .edit(rhs, _)):
            return lhs == rhs
        case let (.access(lhs), .access(rhs)):
            return lhs == rhs
        case (.faq, .faq),
             (.add, .add),
             (.register, .register),
             (.restart, .restart):
            return true
        case let (.snackbar(lhs), .snackbar(rhs)):
            return lhs == rhs
        default:
            return false
        }
    }
}

protocol AccountManagementCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: AccountManagementAction)
}

final class AccountManagementCoordinator {
    weak var viewController: UIViewController?
    private weak var delegate: AuthenticationCoordinating?

    init(delegate: AuthenticationCoordinating) {
        self.delegate = delegate
    }
}

// MARK: - AccountManagementCoordinating
extension AccountManagementCoordinator: AccountManagementCoordinating {
    func perform(action: AccountManagementAction) {
        switch action {
        case .faq:
            delegate?.faq()
        case .add:
            delegate?.firstAccess()
        case let .edit(account, editingDelegate):
            delegate?.accountEditing(account, hasDeleteButton: false, delegate: editingDelegate)
        case let .access(account):
            delegate?.savedAccount(account)
        case .register:
            delegate?.register()
        case .restart:
            delegate?.start()
        case let .snackbar(snackbar):
            delegate?.snackbar(snackbar)
        }
    }
}
