import AssetsKit
import UIKit
import UI
import SnapKit

protocol AccountViewDelegate: AnyObject {
    func didTapDelete(identifier: Int)
    func didTapEdit(identifier: Int)
}

extension AccountView.Layout {
    enum Animation {
        static let time = 0.3
    }
    enum Logo {
        static let topEdge: CGFloat = 12
    }
    enum Icon {
        static let topEdge: CGFloat = 10
    }
}

final class AccountView: UIView {
    enum ViewType {
        case account(_ account: Account, delegate: AccountViewDelegate)
        case add
    }
    fileprivate enum Layout {}
    fileprivate typealias Localizable = Strings.Account.Management
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    private lazy var deleteIconButton: UIButton = {
        let image = Resources.Icons.icoRemoveFill.image
        let button = UIButton(type: .custom)
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(didTapDelete(_:)), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    private lazy var containerStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            deleteIconButton,
            SpacerView(axis: .horizontal, size: Spacing.base01),
            logoImageView,
            SpacerView(axis: .horizontal, size: Spacing.base02),
            contentStackView
        ])
        stackView.alignment = .center
        return stackView
    }()
    private lazy var logoImageView = UIImageView()
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
            .with(\.numberOfLines, 1)
        return label
    }()
    private lazy var descriptionLabel = UILabel()
    private lazy var documentLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .default))
            .with(\.textColor, .grayscale700())
            .with(\.numberOfLines, 1)
        return label
    }()
    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [nameLabel, descriptionLabel, documentLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    private lazy var accessoryIconButton: UIButton = {
        let button = UIButton(type: .custom)
        button.addTarget(self, action: #selector(didTapEdit(_:)), for: .touchUpInside)
        return button
    }()
    
    private weak var delegate: AccountViewDelegate?
    private var account: Account?

    init() {
        super.init(frame: .zero)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setup(type: ViewType) {
        switch type {
        case let .account(account, delegate):
            self.delegate = delegate
            self.account = account
            setupAccount(account)
        case .add:
            setupAdd()
        }
    }

    func prepareForReuse() {
        logoImageView.image = nil
        nameLabel.text = nil
        descriptionLabel.text = nil
        documentLabel.text = nil
    }

    func setEditing(_ editing: Bool) {
        let backgroundColor: Colors.Style = editing
            ? .groupedBackgroundPrimary()
            : .backgroundPrimary()
        let border: Border.Style = editing
            ? .none
            : .light(color: .grayscale200())
        containerView.viewStyle(CardViewStyle())
            .with(\.backgroundColor, backgroundColor)
            .with(\.shadow, .light(color: .black(), opacity: .light))
            .with(\.border, border)

        UIView.animate(withDuration: Layout.Animation.time) {
            self.deleteIconButton.isHidden = editing == false
        }

        accessoryIconButton.isUserInteractionEnabled = editing
        let image = accessoryIcon(isEditing: editing, color: .branding600())
        accessoryIconButton.setImage(image, for: .normal)
    }

    func setEnabled(_ enabled: Bool) {
        isUserInteractionEnabled = enabled
        descriptionLabel.textColor = enabled ? .grayscale700() : .grayscale200()
        logoImageView.image = plusIcon(isEnabled: enabled)
        let color: Colors.Style = enabled ? .branding600() : .grayscale200()
        let image = accessoryIcon(isEditing: false, color: color)
        accessoryIconButton.setImage(image, for: .normal)
    }
}

extension AccountView: ViewConfiguration {
    func buildViewHierarchy() {
        containerView.addSubviews(containerStackView, accessoryIconButton)
        addSubview(containerView)
    }

    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.height.equalTo(Sizing.base10)
        }
        deleteIconButton.snp.makeConstraints {
            $0.size.equalTo(Sizing.Button.small.rawValue)
        }
        containerStackView.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.top.bottom.lessThanOrEqualToSuperview().inset(Spacing.base01)
            $0.centerY.equalToSuperview()
        }
        accessoryIconButton.snp.makeConstraints {
            $0.leading.lessThanOrEqualTo(containerStackView.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.centerY.equalToSuperview()
            $0.size.equalTo(Sizing.Button.small.rawValue)
        }
    }
    
    func configureViews() {
        setEditing(false)
    }
}

private extension AccountView {
    func setupAdd() {
        logoImageView.imageStyle(AvatarImageStyle(hasBorder: true))
            .with(\.image, plusIcon(isEnabled: true))
        descriptionLabel.labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
            .with(\.numberOfLines, 1)
        descriptionLabel.text = Localizable.add
    }

    func setupAccount(_ account: Account) {
        logoImageView.imageStyle(AvatarImageStyle())
        logoImageView.showActivityIndicator = true
        logoImageView.setImage(url: URL(string: account.imageUrl ?? ""),
                               placeholder: Resources.Placeholders.greenAvatarPlaceholder.image)
        nameLabel.text = account.companyName
        descriptionLabel.isHidden = account.description == nil
        descriptionLabel.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.numberOfLines, 1)
        descriptionLabel.text = account.description
        documentLabel.text = StringObfuscationMasker.mask(cnpj: account.document)
    }

    func plusIcon(isEnabled: Bool) -> UIImage? {
        UIImage(
            iconName: Iconography.plus.rawValue,
            font: Typography.icons(.medium).font(),
            color: isEnabled ? Colors.branding600.color : Colors.grayscale200.color,
            size: Sizing.ImageView.medium.rawValue,
            edgeInsets: UIEdgeInsets(top: Layout.Logo.topEdge,
                                     left: .zero,
                                     bottom: .zero,
                                     right: .zero)
        )
    }
    
    func accessoryIcon(isEditing: Bool, color: Colors.Style) -> UIImage? {
        UIImage(
            iconName: isEditing ? Iconography.pen.rawValue : Iconography.angleRight.rawValue,
            font: Typography.icons(isEditing ? .medium : .large).font(),
            color: color.rawValue,
            size: Sizing.ImageView.medium.rawValue,
            edgeInsets: UIEdgeInsets(top: Layout.Icon.topEdge,
                                     left: .zero,
                                     bottom: .zero,
                                     right: .zero)
        )
    }
}

@objc
private extension AccountView {
    func didTapDelete(_ sender: UIButton) {
        guard let account = account else {
            return
        }
        delegate?.didTapDelete(identifier: account.id)
    }
    
    func didTapEdit(_ sender: UIButton) {
        guard let account = account else {
            return
        }
        delegate?.didTapEdit(identifier: account.id)
    }
}
