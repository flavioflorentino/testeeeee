import AssetsKit
import UI
import UIKit
import SnapKit

final class AccountCell: UITableViewCell {
    private lazy var accountView = AccountView()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        accountView.prepareForReuse()
    }

    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: true)
        accountView.setEditing(editing)
    }
    
    func setup(_ item: Account, delegate: AccountViewDelegate) {
        accountView.setup(type: .account(item, delegate: delegate))
    }
}

extension AccountCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(accountView)
    }

    func setupConstraints() {
        accountView.snp.makeConstraints {
            $0.leading.top.trailing.equalToSuperview()
            $0.bottom.equalToSuperview().offset(-Spacing.base01)
        }
    }
    
    func configureViews() {
        backgroundColor = .backgroundPrimary()
    }
}
