import Core
import Foundation

enum AuthenticationEndpoint {
    case validate(_ cnpj: String)
    case authorize(_ cnpj: String, username: String, password: String)
    case retrieveUser
    case retrieveSeller
}

extension AuthenticationEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .validate:
            return "/login/verify-cnpj"
        case .authorize:
            return "/oauth/access-token"
        case .retrieveUser:
            return "/authenticated"
        case .retrieveSeller:
            return "/seller"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .retrieveUser, .retrieveSeller:
            return .get
        default:
            return .post
        }
    }
    
    var isTokenNeeded: Bool {
        switch self {
        case .retrieveUser, .retrieveSeller:
            return true
        default:
            return false
        }
    }
    
    var body: Data? {
        switch self {
        case let .validate(cnpj):
            return ["cnpj": cnpj.onlyNumbers].toData()
        case let .authorize(cnpj, username, password):
            let clientId = Credentials.ApiClient.id
            let clientSecret = Credentials.ApiClient.secret
            return [
                "client_id": clientId,
                "client_secret": clientSecret,
                "grant_type": "password",
                "password": password,
                "username": [
                    "cnpj": cnpj.onlyNumbers,
                    "email": username
                ]
            ].toData()
        default:
            return nil
        }
    }
}
