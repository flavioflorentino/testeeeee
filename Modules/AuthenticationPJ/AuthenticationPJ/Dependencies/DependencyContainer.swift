import AnalyticsModule
import Core
import Foundation

protocol HasAuthenticationWorker {
    var authenticationWorker: AuthenticationWorking { get }
}

typealias Dependencies =
    HasMainQueue &
    HasAnalytics &
    HasKeychainManager &
    HasAuthenticationWorker

final class DependencyContainer: Dependencies {
    lazy var mainQueue = DispatchQueue.main
    lazy var analytics: AnalyticsProtocol = Analytics.shared
    lazy var keychain: KeychainManagerContract = KeychainManager()
    lazy var authenticationWorker: AuthenticationWorking = AuthenticationWorker(dependencies: self)
}
