import UI
import UIKit

public protocol AuthenticationDelegate: AnyObject {
    func authenticationWasCompleted(_ navigation: UINavigationController, userAuthResponse: UserAuthResponse)
    func authenticationDidTapFAQ(_ navigation: UINavigationController)
    func authenticationDidTapForgotPassword(_ navigation: UINavigationController)
    func authenticationDidTapRegister(_ navigation: UINavigationController)
}

protocol AuthenticationCoordinating: AnyObject {
    func start()
    func login(isAddingAccount: Bool)
    func firstAccess()
    func savedAccount(_ account: Account)
    func accountManagement()
    func accountEditing(_ account: Account, hasDeleteButton: Bool, delegate: AccountEditingDelegate)
    func complete(with userAuthResponse: UserAuthResponse)
    func close()
    func faq()
    func forgotPassword()
    func register()
    func snackbar(_ snackbar: ApolloSnackbar)
    func networkSnackbar()
}

public final class AuthenticationCoordinator {
    typealias Dependencies = HasAuthenticationWorker
    private let dependencies: Dependencies

    private let navigationController: UINavigationController
    private var childViewController: [UIViewController] = []
    private weak var delegate: AuthenticationDelegate?

    public convenience init(from originNavigationController: UINavigationController,
                            delegate: AuthenticationDelegate) {
        self.init(from: originNavigationController,
                  delegate: delegate,
                  dependencies: DependencyContainer())
    }

    init(from originNavigationController: UINavigationController,
         delegate: AuthenticationDelegate,
         dependencies: Dependencies) {
        self.navigationController = originNavigationController
        self.delegate = delegate
        self.dependencies = dependencies
    }
}

extension AuthenticationCoordinator: AuthenticationCoordinating {
    public func start() {
        dependencies.authenticationWorker.retrieveAccounts { [weak self] accounts in
            guard let self = self else { return }
            guard accounts.isNotEmpty, let account = accounts.first else {
                self.close()
                return
            }
            self.savedAccount(account)
        }
    }

    public func login(isAddingAccount: Bool) {
        dependencies.authenticationWorker.retrieveAccounts { [weak self] accounts in
            guard let self = self else { return }

            if isAddingAccount, accounts.count > 1 {
                self.accountManagement()
            } else if !isAddingAccount, let account = accounts.first {
                self.savedAccount(account)
            } else {
                self.firstAccess()
            }
        }
    }

    func firstAccess() {
        let controller = FirstAccessFactory.make(delegate: self)
        self.navigationController.pushViewController(controller, animated: true)
    }

    func savedAccount(_ account: Account) {
        let controller = SavedAccountFactory.make(account: account, delegate: self)
        navigationController.pushViewController(controller, animated: true)
    }

    func accountManagement() {
        let controller = AccountManagementFactory.make(delegate: self)
        navigationController.pushViewController(controller, animated: true)
    }

    func accountEditing(_ account: Account, hasDeleteButton: Bool, delegate: AccountEditingDelegate) {
        let controller = AccountEditingFactory.make(account: account, hasDeleteButton: hasDeleteButton, delegate: delegate)
        let navigation = UINavigationController(rootViewController: controller)
        navigationController.present(navigation, animated: true)
    }

    func complete(with userAuthResponse: UserAuthResponse) {
        delegate?.authenticationWasCompleted(navigationController, userAuthResponse: userAuthResponse)
    }

    func close() {
        navigationController.popToRootViewController(animated: false)
    }

    func faq() {
        delegate?.authenticationDidTapFAQ(navigationController)
    }

    func forgotPassword() {
        delegate?.authenticationDidTapForgotPassword(navigationController)
    }

    func register() {
        delegate?.authenticationDidTapRegister(navigationController)
    }

    func snackbar(_ snackbar: ApolloSnackbar) {
        navigationController.showSnackbar(snackbar)
    }

    func networkSnackbar() {
        let button = ApolloSnackbarButton(title: Strings.Network.Unavailable.close, position: .right) {
            //  Does not contain a way to remove the snackbar yet
        }
        let snackbar = ApolloSnackbar(text: Strings.Network.Unavailable.description, iconType: .error, button: button)
        navigationController.showSnackbar(snackbar)
    }
}
