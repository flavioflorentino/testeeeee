import UIKit
import AuthenticationPJ

final class WelcomeViewController: UIViewController {
    private lazy var coordinator: AuthenticationCoordinator? = {
        guard let navigationController = navigationController else {
            return nil
        }
        return AuthenticationCoordinator(from: navigationController, delegate: self)
    }()
    private lazy var loginButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Logar", for: .normal)
        button.addTarget(self, action: #selector(didTapLogin(_:)), for: .touchUpInside)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        coordinator?.start()
    }

    private func configureViews() {
        view.backgroundColor = .white
        view.addSubview(loginButton)
        loginButton.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        loginButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }

    @objc
    private func didTapLogin(_ sender: UIButton) {
        coordinator?.login(isAddingAccount: false)
    }
}

extension WelcomeViewController: AuthenticationDelegate {
    func authenticationWasCompleted(_ navigation: UINavigationController, userAuthResponse: UserAuthResponse) {
        print("Must display logged area")
    }

    func authenticationDidTapFAQ(_ navigation: UINavigationController) {
        print("Must display faq")
    }

    func authenticationDidTapForgotPassword(_ navigation: UINavigationController) {
        print("Must display forgot password")
    }

    func authenticationDidTapRegister(_ navigation: UINavigationController) {
        print("Must display register")
    }
}
