import AnalyticsModule
import Core
import Foundation
import XCTest
@testable import AuthenticationPJ

// swiftlint:disable discouraged_optional_collection
private final class AccountManagementServiceMock: AccountManagementServicing {
    var accounts: [Account]?
    
    func retrieveAccounts(_ completion: @escaping ([Account]) -> Void) {
        guard let accounts = accounts else {
            XCTFail("No accounts provided")
            return
        }
        completion(accounts)
    }
    
    private(set) var updateAccountCallsCount = 0
    private(set) var updateAccount: Account?
    
    func updateAccount(_ account: Account) {
        updateAccountCallsCount += 1
        updateAccount = account
    }
    
    private(set) var removeAccountCallsCount = 0
    private(set) var removeAccount: Account?
    
    func removeAccount(_ account: Account) {
        removeAccountCallsCount += 1
        removeAccount = account
    }
    
    private(set) var removeAccountsCallsCount = 0
    
    func removeAccounts() {
        removeAccountsCallsCount += 1
    }
}

private final class AccountManagementPresenterSpy: AccountManagementPresenting {
    var viewController: AccountManagementDisplaying?
    
    private(set) var actionCallsCount = 0
    private(set) var actionSelected: AccountManagementAction?
    
    func didNextStep(action: AccountManagementAction) {
        actionSelected = action
        actionCallsCount += 1
    }
    
    private(set) var displayAccountsCallsCount = 0
    private(set) var displayAccounts: [Account]?
    
    func displayAccounts(_ accounts: [Account]) {
        displayAccountsCallsCount += 1
        displayAccounts = accounts
    }
    
    private(set) var updateAccountsCallsCount = 0
    private(set) var updateAccounts: [Account]?
    
    func updateAccounts(_ accounts: [Account]) {
        updateAccountsCallsCount += 1
        updateAccounts = accounts
    }
    
    private(set) var displayRemoveCallsCount = 0
    private(set) var removeAccount: Account?
    
    func displayRemove(_ account: Account) {
        displayRemoveCallsCount += 1
        removeAccount = account
    }

    private(set) var displayAccountRemovedSnackbarCallsCount = 0

    func displayAccountRemovedSnackbar() {
        displayAccountRemovedSnackbarCallsCount += 1
    }

    private(set) var displayAccountEditedSnackbarCallsCount = 0

    func displayAccountEditedSnackbar() {
        displayAccountEditedSnackbarCallsCount += 1
    }

    private(set) var displayAccountsRemovedSnackbarCallsCount = 0

    func displayAccountsRemovedSnackbar() {
        displayAccountsRemovedSnackbarCallsCount += 1
    }

    private(set) var displayRemoveAllCallsCount = 0

    func displayRemoveAll() {
        displayRemoveAllCallsCount += 1
    }
}
// swiftlint:enable discouraged_optional_collection

private final class AccountEditingDelegateSpy: AccountEditingDelegate {
    private(set) var accountWasEditedCallsCount = 0
    private(set) var account: Account?

    func accountWasEdited(account: Account) {
        accountWasEditedCallsCount += 1
        self.account = account
    }

    private(set) var accountWasRemovedCallsCount = 0

    func accountWasRemoved() {
        accountWasRemovedCallsCount += 1
    }
}

final class AccountManagementInteractorTests: XCTestCase {
    private let analyticsMock = AnalyticsSpy()
    private lazy var dependenciesMock = DependencyContainerMock(analyticsMock)
    private let serviceMock = AccountManagementServiceMock()
    private let presenterSpy = AccountManagementPresenterSpy()
    private let delegateSpy = AccountEditingDelegateSpy()
    private lazy var sut = AccountManagementInteractor(
        service: serviceMock,
        presenter: presenterSpy,
        dependencies: dependenciesMock
    )

    func testRetrieveAccounts_WhenReceiveRetrieveAccountsFromViewController_ShouldDisplayAccounts() {
        let account = AuthenticationMock.Account.full
        let accounts = [account]
        serviceMock.accounts = accounts
        sut.retrieveAccounts()

        XCTAssertEqual(presenterSpy.displayAccountsCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayAccounts, accounts)
    }

    func testAccessAccount_WhenReceiveAccessAccountFromViewController_ShouldPushViewController() {
        let account = AuthenticationMock.Account.full
        let accounts = [account]
        serviceMock.accounts = accounts
        sut.retrieveAccounts()
        sut.accessAccount(0)

        XCTAssertEqual(presenterSpy.displayAccountsCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayAccounts, accounts)
        XCTAssertEqual(presenterSpy.actionCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionSelected, .access(account))
    }

    func testEditAccount_WhenReceiveEditAccountFromViewController_ShouldPresentAccountEditingViewController() {
        let account = AuthenticationMock.Account.full
        let accounts = [account]
        serviceMock.accounts = accounts
        sut.retrieveAccounts()
        sut.didTapEditAccount(123)

        XCTAssertEqual(presenterSpy.displayAccountsCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayAccounts, accounts)
        XCTAssertEqual(presenterSpy.actionCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionSelected, .edit(account, editingDelegate: delegateSpy))
    }

    func testEditAccount_WhenReceiveEditAccountFromViewController_ShouldDoNothing() {
        sut.didTapEditAccount(123)

        XCTAssertEqual(presenterSpy.actionCallsCount, 0)
        XCTAssertNil(presenterSpy.actionSelected)
    }

    func testRemoveAccount_WhenReceiveRemoveAccountFromViewController_ShouldDisplayADialog() {
        let account = AuthenticationMock.Account.full
        let accounts = [account]
        serviceMock.accounts = accounts
        sut.retrieveAccounts()
        sut.didTapRemoveAccount(123)

        XCTAssertEqual(presenterSpy.displayAccountsCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayAccounts, accounts)
        XCTAssertEqual(presenterSpy.displayRemoveCallsCount, 1)
    }

    func testDidTapRemoveAccount_WhenReceiveDidTapRemoveAccountFromViewController_ShouldDoNothing() {
        sut.didTapRemoveAccount(123)

        XCTAssertEqual(presenterSpy.displayRemoveCallsCount, 0)
    }

    func testRemoveAccount_WhenReceiveRemoveAccountFromViewController_ShouldRemoveAccountAndUpdateAccountsAndDisplayANotification() {
        serviceMock.accounts = []
        let account = AuthenticationMock.Account.full
        sut.removeAccount(account)

        let event = Tracker(eventType: AccountManagementEvents.delete(false))
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(serviceMock.removeAccountCallsCount, 1)
        XCTAssertEqual(serviceMock.removeAccount, account)
        XCTAssertEqual(presenterSpy.updateAccountsCallsCount, 1)
        XCTAssertEqual(presenterSpy.updateAccounts, [])
        XCTAssertEqual(presenterSpy.displayAccountRemovedSnackbarCallsCount, 1)
    }

    func testDidTapRemoveAccounts_WhenReceiveDidTapRemoveAccountsFromViewController_ShouldDisplayADialog() {
        sut.didTapRemoveAccounts()

        XCTAssertEqual(presenterSpy.displayRemoveAllCallsCount, 1)
    }

    func testRemoveAccounts_WhenReceiveRemoveAccountsFromViewController_ShouldRemoveAccounts() {
        serviceMock.accounts = []
        sut.removeAccounts()

        let event = Tracker(eventType: AccountManagementEvents.delete(true))
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(serviceMock.removeAccountsCallsCount, 1)
    }

    func testAddAccount_WhenReceiveAddAccountFromViewController_ShouldPushFirstAccessViewController() {
        sut.addAccount()

        let event = Tracker(eventType: AccountManagementEvents.anotherAccount)
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.actionCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionSelected, .add)
    }

    func testRegisterAccount_WhenReceiveRegisterAccountFromViewController_ShouldCallDelegateFunction() {
        sut.registerAccount()

        let event = Tracker(eventType: AccountManagementEvents.register)
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.actionCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionSelected, .register)
    }

    func testAccountWasEdited_WhenReceiveAccountWasEditedFromDelegate_ShouldDisplayAccountEditedSnackbar() {
        let account = AuthenticationMock.Account.full
        let accounts = [account]
        serviceMock.accounts = accounts
        sut.accountWasEdited(account: account)

        XCTAssertEqual(presenterSpy.displayAccountsCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayAccounts, accounts)
        XCTAssertEqual(presenterSpy.displayAccountEditedSnackbarCallsCount, 1)
    }

    func testFaq_WhenReceiveFaqFromViewController_ShouldDisplayFaq() {
        sut.faq()

        let event = Tracker(eventType: AccountManagementEvents.faq)
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.actionCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionSelected, .faq)
    }
}
