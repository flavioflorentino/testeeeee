import XCTest
import UI
@testable import AuthenticationPJ

final class AccountManagementCoordinatorSpy: AccountManagementCoordinating {
    var viewController: UIViewController?
    
    private(set) var actionCallsCount = 0
    private(set) var actionSelected: AccountManagementAction?
    
    func perform(action: AccountManagementAction) {
        actionSelected = action
        actionCallsCount += 1
    }
}

final class AccountManagementDisplaySpy: AccountManagementDisplaying {
    private(set) var displayAccountsCallsCount = 0
    // swiftlint:disable:next discouraged_optional_collection
    private(set) var accounts: [Account]?
    
    func displayAccounts(_ accounts: [Account]) {
        displayAccountsCallsCount += 1
        self.accounts = accounts
    }
    
    private(set) var updateAccountsCallsCount = 0
    
    func updateAccounts(_ accounts: [Account]) {
        updateAccountsCallsCount += 1
        self.accounts = accounts
    }
    
    private(set) var endEditingModeCallsCount = 0
    
    func endEditingMode() {
        endEditingModeCallsCount += 1
    }
    
    private(set) var displayRemoveCallsCount = 0
    private(set) var title: String?
    private(set) var description: String?
    private(set) var account: Account?

    func displayRemove(title: String, description: String, account: Account) {
        displayRemoveCallsCount += 1
        self.title = title
        self.description = description
        self.account = account
    }

    func displayRemove(title: String, description: String) {
        displayRemoveCallsCount += 1
        self.title = title
        self.description = description
    }
    
    private(set) var displayRemovedCallsCount = 0
    
    func displayAccountRemoved() {
        displayRemovedCallsCount += 1
    }
}

private final class AccountEditingDelegateSpy: AccountEditingDelegate {
    private(set) var accountWasEditedCallsCount = 0
    private(set) var account: Account?

    func accountWasEdited(account: Account) {
        accountWasEditedCallsCount += 1
        self.account = account
    }

    private(set) var accountWasRemovedCallsCount = 0

    func accountWasRemoved() {
        accountWasRemovedCallsCount += 1
    }
}

final class AccountManagementPresenterTests: XCTestCase {
    private let coordinatorSpy = AccountManagementCoordinatorSpy()
    private let displaySpy = AccountManagementDisplaySpy()
    private lazy var sut: AccountManagementPresenting = {
        let presenter = AccountManagementPresenter(coordinator: coordinatorSpy)
        presenter.viewController = displaySpy
        return presenter
    }()
    private let delegateSpy = AccountEditingDelegateSpy()
    
    func testDidNextStep_WhenReceiveDidNextStepFromInteractor_ShouldPushFirstAccessViewController() {
        sut.didNextStep(action: .add)
        
        XCTAssertEqual(coordinatorSpy.actionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.actionSelected, .add)
    }
    
    func testDidNextStep_WhenReceiveDidNextStepFromInteractor_ShouldPresentAccountEditingViewController() {
        let account = AuthenticationMock.Account.full
        sut.didNextStep(action: .edit(account, editingDelegate: delegateSpy))
        
        XCTAssertEqual(coordinatorSpy.actionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.actionSelected, .edit(account, editingDelegate: delegateSpy))
    }
    
    func testDidNextStep_WhenReceiveDidNextStepFromInteractor_ShouldPushSavedAccountViewController() {
        let account = AuthenticationMock.Account.full
        sut.didNextStep(action: .access(account))
        
        XCTAssertEqual(coordinatorSpy.actionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.actionSelected, .access(account))
    }
    
    func testDidNextStep_WhenReceiveDidNextStepFromInteractor_ShouldCallDelegateFunction() {
        sut.didNextStep(action: .register)
        
        XCTAssertEqual(coordinatorSpy.actionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.actionSelected, .register)
    }
    
    func testDisplayAccounts_WhenReceiveDisplayAccountsFromInteractor_ShouldDisplayAccounts() {
        let account = AuthenticationMock.Account.full
        let accounts = [account]
        sut.displayAccounts(accounts)
        
        XCTAssertEqual(displaySpy.displayAccountsCallsCount, 1)
        XCTAssertEqual(displaySpy.accounts, accounts)
    }
    
    func testUpdateAccounts_WhenReceiveUpdateAccountsFromInteractor_ShouldUpdateAccounts() {
        let account = AuthenticationMock.Account.full
        let accounts = [account]
        sut.updateAccounts(accounts)
        
        XCTAssertEqual(displaySpy.updateAccountsCallsCount, 1)
        XCTAssertEqual(displaySpy.accounts, accounts)
    }
    
    func testDisplayRemove_WhenReceiveDisplayRemoveFromInteractor_ShouldDisplayRemove() {
        let account = AuthenticationMock.Account.full
        sut.displayRemove(account)
        
        XCTAssertEqual(displaySpy.displayRemoveCallsCount, 1)
        XCTAssertEqual(displaySpy.account, account)
    }

    func testDisplayRemoveAll_WhenReceiveDisplayRemoveAllFromInteractor_ShouldDisplayRemoveAll() {
        sut.displayRemoveAll()

        XCTAssertEqual(displaySpy.displayRemoveCallsCount, 1)
        XCTAssertEqual(displaySpy.title, "Remover todas as contas")
        XCTAssertEqual(displaySpy.description, "Ao remover os dados das contas, elas não estarão mais disponíveis para acesso rápido neste dispositivo. Para entrar novamente, é só inserir CNPJ e os dados de acesso.")
        XCTAssertNil(displaySpy.account)
    }
    
    func testDisplayAccountRemovedSnackbar_WhenReceiveDisplayAccountRemovedSnackbarFromInteractor_ShouldDisplaySnackbar() {
        sut.displayAccountRemovedSnackbar()
        
        XCTAssertEqual(coordinatorSpy.actionCallsCount, 1)
        let snackbar = ApolloSnackbar(text: "Dados da conta removidos.", iconType: .success)
        XCTAssertEqual(coordinatorSpy.actionSelected, .snackbar(snackbar))
    }

    func testDisplayAccountEditedSnackbar_WhenReceiveDisplayAccountEditedSnackbarFromInteractor_ShouldDisplaySnackbar() {
        sut.displayAccountEditedSnackbar()

        XCTAssertEqual(coordinatorSpy.actionCallsCount, 1)
        let snackbar = ApolloSnackbar(text: "Dados da conta editados.", iconType: .success)
        XCTAssertEqual(coordinatorSpy.actionSelected, .snackbar(snackbar))
    }

    func testDisplayAccountsRemovedSnackbar_WhenReceiveDisplayAccountsRemovedSnackbarFromInteractor_ShouldDisplaySnackbar() {
        sut.displayAccountsRemovedSnackbar()

        XCTAssertEqual(coordinatorSpy.actionCallsCount, 1)
        let snackbar = ApolloSnackbar(text: "Dados removidos.", iconType: .success)
        XCTAssertEqual(coordinatorSpy.actionSelected, .snackbar(snackbar))
    }
}
