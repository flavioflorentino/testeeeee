import XCTest
@testable import AuthenticationPJ

private final class AccountEditingCoordinatorSpy: AccountEditingCoordinating {
    var viewController: UIViewController?
    
    private(set) var actionCallsCount = 0
    private(set) var actionSelected: AccountEditingAction?
    
    func perform(action: AccountEditingAction) {
        actionSelected = action
        actionCallsCount += 1
    }
}

private final class AccountEditingDisplaySpy: AccountEditingDisplaying {
    private(set) var displayAccountCallsCount = 0
    private(set) var displayAccount: Account?
    
    func displayAccount(_ account: Account) {
        displayAccountCallsCount += 1
        displayAccount = account
    }
    
    private(set) var displayUsernameErrorCallsCount = 0
    private(set) var displayUsernameErrorMessage: String?
    
    func displayUsernameError(_ message: String) {
        displayUsernameErrorCallsCount += 1
        displayUsernameErrorMessage = message
    }
    
    private(set) var dismissUsernameErrorCallsCount = 0
    
    func dismissUsernameError() {
        dismissUsernameErrorCallsCount += 1
    }
    
    private(set) var displayDescriptionCallsCount = 0
    private(set) var displayDescriptionMessage: String?
    
    func displayDescription(_ message: String) {
        displayDescriptionCallsCount += 1
        displayDescriptionMessage = message
    }

    private(set) var displayDescriptionErrorCallsCount = 0

    func displayDescriptionError(_ message: String) {
        displayDescriptionErrorCallsCount += 1
        displayDescriptionMessage = message
    }
    
    private(set) var enableButtonCallsCount = 0
    
    func enableButton() {
        enableButtonCallsCount += 1
    }
    
    private(set) var disableButtonCallsCount = 0
    
    func disableButton() {
        disableButtonCallsCount += 1
    }
    
    private(set) var displayRemoveAccountCallsCount = 0
    
    func displayRemoveAccount() {
        displayRemoveAccountCallsCount += 1
    }

    private(set) var dismissRemoveAccountCallsCount = 0

    func dismissRemoveAccount() {
        dismissRemoveAccountCallsCount += 1
    }

    private(set) var displayRemoveButtonCallsCount = 0

    func displayRemoveButton() {
        displayRemoveButtonCallsCount += 1
    }

    private(set) var dismissRemoveButtonCallsCount = 0

    func dismissRemoveButton() {
        dismissRemoveButtonCallsCount += 1
    }
}

final class AccountEditingPresenterTests: XCTestCase {
    private let coordinatorSpy = AccountEditingCoordinatorSpy()
    private let displaySpy = AccountEditingDisplaySpy()
    private lazy var sut: AccountEditingPresenting = {
        let presenter = AccountEditingPresenter(coordinator: coordinatorSpy)
        presenter.viewController = displaySpy
        return presenter
    }()
    
    func testDidNextStep_WhenReceiveDidNextStepFromInteractor_ShouldCloseFlow() {
        sut.didNextStep(action: .close)
        
        XCTAssertEqual(coordinatorSpy.actionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.actionSelected, .close)
    }
    
    func testDisplayAccount_WhenReceiveDisplayAccountFromInteractor_ShouldDisplayAccount() {
        let account = AuthenticationMock.Account.full
        sut.displayAccount(account)
        
        XCTAssertEqual(displaySpy.displayAccountCallsCount, 1)
        XCTAssertEqual(displaySpy.displayAccount, account)
    }
    
    func testDisplayUsernameError_WhenReceiveDisplayUsernameErrorFromInteractor_Should() {
        sut.displayUsernameError()
        
        XCTAssertEqual(displaySpy.displayUsernameErrorCallsCount, 1)
        XCTAssertEqual(displaySpy.displayUsernameErrorMessage, "Esta informação é obrigatória.")
    }
    
    func testDismissUsernameError_WhenReceiveDismissUsernameErrorFromInteractor_Should() {
        sut.dismissUsernameError()
        
        XCTAssertEqual(displaySpy.dismissUsernameErrorCallsCount, 1)
    }

    func testDisplayDescription_WhenReceiveDisplayDescriptionFromInteractor_Should() {
        sut.displayDescription(10, hasError: false)
        
        XCTAssertEqual(displaySpy.displayDescriptionCallsCount, 1)
        XCTAssertEqual(displaySpy.displayDescriptionMessage, "10 de 20 caracteres.")
    }

    func testDisplayDescription_WhenReceiveDisplayDescriptionFromInteractor_ShouldDisplayError() {
        sut.displayDescription(30, hasError: true)

        XCTAssertEqual(displaySpy.displayDescriptionErrorCallsCount, 1)
        XCTAssertEqual(displaySpy.displayDescriptionMessage, "30 de 20 caracteres. Revise sua descrição.")
    }
    
    func testEnableButton_WhenReceiveEnableButtonFromInteractor_Should() {
        sut.enableButton()
        
        XCTAssertEqual(displaySpy.enableButtonCallsCount, 1)
    }
    
    func testDisableButton_WhenReceiveDisableButtonFromInteractor_Should() {
        sut.disableButton()
        
        XCTAssertEqual(displaySpy.disableButtonCallsCount, 1)
    }
    
    func testDisplayRemoveAccount_WhenReceiveDisplayRemoveAccountFromInteractor_Should() {
        sut.displayRemoveAccount()
        
        XCTAssertEqual(displaySpy.displayRemoveAccountCallsCount, 1)
    }

    func testDismissRemoveAccount_WhenReceiveDismissRemoveAccountFromInteractor_Should() {
        sut.dismissRemoveAccount()

        XCTAssertEqual(displaySpy.dismissRemoveAccountCallsCount, 1)
    }

    func testDisplayRemoveButton_WhenReceiveDisplayRemoveButtonFromInteractor_Should() {
        sut.displayRemoveButton()

        XCTAssertEqual(displaySpy.displayRemoveButtonCallsCount, 1)
    }

    func testDismissRemoveButton_WhenReceiveDismissRemoveButtonFromInteractor_Should() {
        sut.dismissRemoveButton()

        XCTAssertEqual(displaySpy.dismissRemoveButtonCallsCount, 1)
    }
}
