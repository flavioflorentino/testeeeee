import AnalyticsModule
import Core
import Foundation
import XCTest
@testable import AuthenticationPJ

private final class AccountEditingServiceMock: AccountEditingServicing {
    private(set) var updateAccountCallsCount = 0
    private(set) var updateAccount: Account?
    
    func updateAccount(_ account: Account) {
        updateAccountCallsCount += 1
        updateAccount = account
    }
    
    private(set) var removeAccountCallsCount = 0
    private(set) var removeAccount: Account?
    
    func removeAccount(_ account: Account) {
        removeAccountCallsCount += 1
        removeAccount = account
    }
}

private final class AccountEditingPresenterSpy: AccountEditingPresenting {
    var viewController: AccountEditingDisplaying?
    
    private(set) var actionCallsCount = 0
    private(set) var actionSelected: AccountEditingAction?
    
    func didNextStep(action: AccountEditingAction) {
        actionSelected = action
        actionCallsCount += 1
    }
    
    private(set) var displayAccountCallsCount = 0
    private(set) var displayAccount: Account?
    
    func displayAccount(_ account: Account) {
        displayAccountCallsCount += 1
        displayAccount = account
    }
    
    private(set) var displayUsernameErrorCallsCount = 0
    
    func displayUsernameError() {
        displayUsernameErrorCallsCount += 1
    }
    
    private(set) var dismissUsernameErrorCallsCount = 0
    
    func dismissUsernameError() {
        dismissUsernameErrorCallsCount += 1
    }
    
    private(set) var displayDescriptionCallsCount = 0
    private(set) var displayDescriptionCount: Int?
    private(set) var displayDescriptionError: Bool?
    
    func displayDescription(_ count: Int, hasError: Bool)  {
        displayDescriptionCallsCount += 1
        displayDescriptionCount = count
        displayDescriptionError = hasError
    }
    
    private(set) var enableButtonCallsCount = 0
    
    func enableButton() {
        enableButtonCallsCount += 1
    }
    
    private(set) var disableButtonCallsCount = 0
    
    func disableButton() {
        disableButtonCallsCount += 1
    }
    
    private(set) var displayRemoveAccountCallsCount = 0
    
    func displayRemoveAccount() {
        displayRemoveAccountCallsCount += 1
    }

    private(set) var dismissRemoveAccountCallsCount = 0

    func dismissRemoveAccount() {
        dismissRemoveAccountCallsCount += 1
    }

    private(set) var displayRemoveButtonCallsCount = 0

    func displayRemoveButton() {
        displayRemoveButtonCallsCount += 1
    }

    private(set) var dismissRemoveButtonCallsCount = 0

    func dismissRemoveButton() {
        dismissRemoveButtonCallsCount += 1
    }
}

final class AccountEditingInteractorTests: XCTestCase {
    private let analyticsMock = AnalyticsSpy()
    private lazy var dependenciesMock = DependencyContainerMock(analyticsMock)
    private let serviceMock = AccountEditingServiceMock()
    private let presenterSpy = AccountEditingPresenterSpy()
    private lazy var sut = AccountEditingInteractor(
        service: serviceMock,
        presenter: presenterSpy,
        account: AuthenticationMock.Account.full,
        hasDeleteButton: true,
        dependencies: dependenciesMock
    )
    private lazy var sutDescription = AccountEditingInteractor(
        service: serviceMock,
        presenter: presenterSpy,
        account: AuthenticationMock.Account.withoutDescription,
        hasDeleteButton: true,
        dependencies: dependenciesMock
    )
    private lazy var sutWithoutDeleteButton = AccountEditingInteractor(
        service: serviceMock,
        presenter: presenterSpy,
        account: AuthenticationMock.Account.full,
        hasDeleteButton: false,
        dependencies: dependenciesMock
    )
    private lazy var sutDescriptionWithoutDeleteButton = AccountEditingInteractor(
        service: serviceMock,
        presenter: presenterSpy,
        account: AuthenticationMock.Account.withoutDescription,
        hasDeleteButton: false,
        dependencies: dependenciesMock
    )
    
    func testClose_WhenReceiveCloseFromViewController_ShouldCloseFlow() {
        sut.close()
        
        XCTAssertEqual(presenterSpy.actionCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionSelected, .close)
    }
    
    func testRetrieveAccount_WhenReceiveCloseFromViewController_ShouldDisplayAccountAndDescriptionNilAndDisplayDeleteButton() {
        sutDescription.retrieveInfos()
        
        XCTAssertEqual(presenterSpy.displayAccountCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayAccount, AuthenticationMock.Account.withoutDescription)
        XCTAssertEqual(presenterSpy.displayDescriptionCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayDescriptionCount, 0)
        XCTAssertEqual(presenterSpy.displayRemoveButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.dismissRemoveButtonCallsCount, 0)
    }
    
    func testRetrieveAccount_WhenReceiveCloseFromViewController_ShouldDisplayAccountAndDescriptionAndDisplayDeleteButton() {
        sut.retrieveInfos()
        
        XCTAssertEqual(presenterSpy.displayAccountCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayAccount, AuthenticationMock.Account.full)
        XCTAssertEqual(presenterSpy.displayDescriptionCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayDescriptionCount, 31)
        XCTAssertEqual(presenterSpy.displayRemoveButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.dismissRemoveButtonCallsCount, 0)
    }

    func testRetrieveAccount_WhenReceiveCloseFromViewController_ShouldDisplayAccountAndDescriptionNilAndDismissDeleteButton() {
        sutWithoutDeleteButton.retrieveInfos()

        XCTAssertEqual(presenterSpy.displayAccountCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayAccount, AuthenticationMock.Account.full)
        XCTAssertEqual(presenterSpy.displayDescriptionCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayDescriptionCount, 31)
        XCTAssertEqual(presenterSpy.displayRemoveButtonCallsCount, 0)
        XCTAssertEqual(presenterSpy.dismissRemoveButtonCallsCount, 1)
    }

    func testRetrieveAccount_WhenReceiveCloseFromViewController_ShouldDisplayAccountAndDescriptionAndDismissDeleteButton() {
        sutDescriptionWithoutDeleteButton.retrieveInfos()

        XCTAssertEqual(presenterSpy.displayAccountCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayAccount, AuthenticationMock.Account.withoutDescription)
        XCTAssertEqual(presenterSpy.displayDescriptionCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayDescriptionCount, 0)
        XCTAssertEqual(presenterSpy.displayRemoveButtonCallsCount, 0)
        XCTAssertEqual(presenterSpy.dismissRemoveButtonCallsCount, 1)
    }
    
    func testValidateFields_WhenReceiveValidateFieldFromViewController_ShouldDisableButton() {
        sut.validateFields(username: nil, description: nil)

        XCTAssertEqual(presenterSpy.displayUsernameErrorCallsCount, 0)
        XCTAssertEqual(presenterSpy.dismissUsernameErrorCallsCount, 0)
        XCTAssertEqual(presenterSpy.displayDescriptionCallsCount, 0)
        XCTAssertNil(presenterSpy.displayDescriptionCount)
        XCTAssertEqual(presenterSpy.disableButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.enableButtonCallsCount, 0)
    }

    func testValidateFields_WhenReceiveValidateFieldFromViewController_ShouldDisplayUsernameErrorAndDisableButton() {
        sut.validateFields(username: "", description: "Descrição marota")

        XCTAssertEqual(presenterSpy.displayUsernameErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.dismissUsernameErrorCallsCount, 0)
        XCTAssertEqual(presenterSpy.displayDescriptionCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayDescriptionCount, 16)
        XCTAssertEqual(presenterSpy.disableButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.enableButtonCallsCount, 0)
    }

    func testValidateFields_WhenReceiveValidateFieldFromViewController_ShouldDismissUsernameErrorAndDisableButton() {
        sut.validateFields(username: "teste123@picpay.com", description: "Descrição marota bem maneira né")
        
        XCTAssertEqual(presenterSpy.displayUsernameErrorCallsCount, 0)
        XCTAssertEqual(presenterSpy.dismissUsernameErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayDescriptionCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayDescriptionCount, 31)
        XCTAssertEqual(presenterSpy.disableButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.enableButtonCallsCount, 0)
    }
    
    func testValidateFields_WhenReceiveValidateFieldFromViewController_ShouldDismissUsernameErrorAndEnableButton() {
        sut.validateFields(username: "teste123@picpay.com", description: "Descrição")
        
        XCTAssertEqual(presenterSpy.displayUsernameErrorCallsCount, 0)
        XCTAssertEqual(presenterSpy.dismissUsernameErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayDescriptionCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayDescriptionCount, 9)
        XCTAssertEqual(presenterSpy.disableButtonCallsCount, 0)
        XCTAssertEqual(presenterSpy.enableButtonCallsCount, 1)
    }
    
    func testUpdateAccount_WhenReceiveUpdateAccountFromViewController_ShouldDoNothing() {
        sut.updateAccount(username: nil, description: nil)
        
        XCTAssertEqual(serviceMock.updateAccountCallsCount, 0)
    }
    
    func testUpdateAccount_WhenReceiveUpdateAccountFromViewController_ShouldUpdateAccount() {
        sut.updateAccount(username: "teste123@picpay.com", description: "Descrição")
        
        XCTAssertEqual(serviceMock.updateAccountCallsCount, 1)
        let event = Tracker(eventType: AccountEditingEvents.edit(true, isUserEdited: false, isDescriptionEdited: true))
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
    }
    
    func testRemove_WhenReceiveRemoveFromViewController_ShouldDisplayRemoveAccount() {
        sut.remove()
        
        XCTAssertEqual(presenterSpy.displayRemoveAccountCallsCount, 1)
    }
    
    func testConfirmRemoveAccount_WhenReceiveConfirmRemoveAccountFromViewController_ShouldConfirmRemoveAccount() {
        sut.confirmRemoveAccount()

        let event = Tracker(eventType: AccountEditingEvents.delete)
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(serviceMock.removeAccountCallsCount, 1)
        XCTAssertEqual(serviceMock.removeAccount, AuthenticationMock.Account.full)
        XCTAssertEqual(presenterSpy.dismissRemoveAccountCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionSelected, .removed)
    }

    func testCancelRemoveAccount_WhenReceiveCancelRemoveAccountFromViewController_ShouldDisplayCancelRemoveAccount() {
        sut.cancelRemoveAccount()

        XCTAssertEqual(presenterSpy.dismissRemoveAccountCallsCount, 1)
    }
}
