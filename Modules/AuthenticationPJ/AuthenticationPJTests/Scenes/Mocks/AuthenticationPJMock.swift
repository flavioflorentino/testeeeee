import Foundation
@testable import AuthenticationPJ

enum AuthenticationMock {
    enum Account {}
}

extension AuthenticationMock.Account {
    private static let dateAccount = Date(timeIntervalSinceReferenceDate: 629_125_050.397_511)
    private static let imageUrl = "https://images1.fanpop.com/images/image_uploads/Naruto-Sad-uzumaki-naruto-987855_620_465.jpg"
    static let full = Account(id: 123,
                              document: "95.828.503/0001-66",
                              username: "teste123@picpay.com",
                              companyName: "PicPay",
                              lastLogin: dateAccount,
                              imageUrl: imageUrl,
                              description: "Descrição marota bem maneira né")
    static let withoutDescription = Account(id: 123,
                                            document: "95.828.503/0001-66",
                                            username: "teste123@picpay.com",
                                            companyName: "PicPay",
                                            lastLogin: dateAccount,
                                            imageUrl: imageUrl,
                                            description: nil)
}
