import AnalyticsModule
import Core
import Foundation
import XCTest
@testable import AuthenticationPJ

private final class SavedAccountServiceMock: SavedAccountServicing {
    var authenticationResult: Result<UserAuthResponse, AuthenticationError>?
    
    func authentication(document: String, username: String, password: String, completion: @escaping (Result<UserAuthResponse, AuthenticationError>) -> Void) {
        guard let authenticationResult = authenticationResult else {
            XCTFail("No authenticationResult provided")
            return
        }
        completion(authenticationResult)
    }
}

private final class SavedAccountPresenterSpy: SavedAccountPresenting {
    var viewController: SavedAccountDisplaying?
    
    private(set) var actionCallsCount = 0
    private(set) var actionSelected: SavedAccountAction?
    
    func didNextStep(action: SavedAccountAction) {
        actionSelected = action
        actionCallsCount += 1
    }
    
    private(set) var displayAccountCallsCount = 0
    private(set) var account: Account?
    
    func displayAccount(_ account: Account) {
        displayAccountCallsCount += 1
        self.account = account
    }
    
    private(set) var displayErrorCallsCount = 0
    private(set) var errorMessage: String?
    
    func displayError(_ message: String) {
        displayErrorCallsCount += 1
        errorMessage = message
    }
    
    private(set) var dismissErrorCallsCount = 0
    
    func dismissError() {
        dismissErrorCallsCount += 1
    }
    
    private(set) var enableLoginButtonCallsCount = 0
    
    func enableLoginButton() {
        enableLoginButtonCallsCount += 1
    }
    
    private(set) var disableLoginButtonCallsCount = 0
    
    func disableLoginButton() {
        disableLoginButtonCallsCount += 1
    }
    
    private(set) var displayLoadingCallsCount = 0
    
    func displayLoading() {
        displayLoadingCallsCount += 1
    }
    
    private(set) var dismissLoadingCallsCount = 0
    
    func dismissLoading() {
        dismissLoadingCallsCount += 1
    }
    
    private(set) var displayNetworkBannerCallsCount = 0
    
    func displayNetworkBanner() {
        displayNetworkBannerCallsCount += 1
    }
    
    private(set) var displayUpdateBannerCallsCount = 0
    
    func displayUpdateBanner() {
        displayUpdateBannerCallsCount += 1
    }

    private(set) var displayAccountRemovedSnackbarCallsCount = 0

    func displayAccountRemovedSnackbar() {
        displayAccountRemovedSnackbarCallsCount += 1
    }

    private(set) var displayAccountEditedSnackbarCallsCount = 0

    func displayAccountEditedSnackbar() {
        displayAccountEditedSnackbarCallsCount += 1
    }
}

private final class AccountEditingDelegateSpy: AccountEditingDelegate {
    private(set) var accountWasEditedCallsCount = 0
    private(set) var account: Account?

    func accountWasEdited(account: Account) {
        accountWasEditedCallsCount += 1
        self.account = account
    }

    private(set) var accountWasRemovedCallsCount = 0

    func accountWasRemoved() {
        accountWasRemovedCallsCount += 1
    }
}

final class SavedAccountInteractorTests: XCTestCase {
    private let analyticsMock = AnalyticsSpy()
    private lazy var dependenciesMock = DependencyContainerMock(analyticsMock)
    private let serviceMock = SavedAccountServiceMock()
    private let presenterSpy = SavedAccountPresenterSpy()
    private let delegateSpy = AccountEditingDelegateSpy()
    private lazy var sut = SavedAccountInteractor(
        service: serviceMock,
        presenter: presenterSpy,
        account: AuthenticationMock.Account.full,
        dependencies: dependenciesMock
    )
    
    func testRetrieveAccount_WhenReceiveRetrieveAccountFromViewController_ShouldDisplayAccount() {
        sut.retrieveAccount()
        
        XCTAssertEqual(presenterSpy.displayAccountCallsCount, 1)
        XCTAssertEqual(presenterSpy.account, AuthenticationMock.Account.full)
    }
    
    func testValidateField_WhenReceiveValidateWithNilPasswordFieldFromViewController_ShouldDisableLoginButton() {
        sut.validateField(password: nil)
        
        XCTAssertEqual(presenterSpy.disableLoginButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.enableLoginButtonCallsCount, 0)
    }
    
    func testValidateField_WhenReceiveValidateWithEmptyPasswordFieldFromViewController_ShouldDisableLoginButton() {
        sut.validateField(password: "")
        
        XCTAssertEqual(presenterSpy.disableLoginButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.enableLoginButtonCallsCount, 0)
    }
    
    func testValidateField_WhenReceiveValidateFieldFromViewController_ShouldEnableLoginButton() {
        sut.validateField(password: "123456")
        
        XCTAssertEqual(presenterSpy.disableLoginButtonCallsCount, 0)
        XCTAssertEqual(presenterSpy.enableLoginButtonCallsCount, 1)
    }
    
    func testAuthorize_WhenReceiveAuthorizeFromViewController_ShouldDoNothing() {
        sut.authorize(password: nil)
        
        XCTAssertEqual(presenterSpy.displayLoadingCallsCount, 0)
        XCTAssertEqual(presenterSpy.dismissErrorCallsCount, 0)
        XCTAssertEqual(presenterSpy.actionCallsCount, 0)
        XCTAssertNil(presenterSpy.actionSelected)
        XCTAssertEqual(presenterSpy.displayNetworkBannerCallsCount, 0)
        XCTAssertEqual(presenterSpy.displayErrorCallsCount, 0)
        XCTAssertNil(presenterSpy.errorMessage)
    }
    
    func testAuthorize_WhenReceiveAuthorizeFromViewController_ShouldCompleteFlow() {
        let response = UserAuthResponse()
        serviceMock.authenticationResult = .success(response)
        sut.authorize(password: "123456")

        let event = Tracker(eventType: SavedAccountEvents.success(true, saveData: true))
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.displayLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.dismissErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionSelected, .complete(response))
        XCTAssertEqual(presenterSpy.displayNetworkBannerCallsCount, 0)
        XCTAssertEqual(presenterSpy.displayErrorCallsCount, 0)
        XCTAssertNil(presenterSpy.errorMessage)
    }
    
    func testAuthorize_WhenReceiveAuthorizeFromViewController_ShouldDisplayNetworkError() {
        serviceMock.authenticationResult = .failure(.network)
        sut.authorize(password: "123456")
        
        XCTAssertEqual(presenterSpy.displayLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.dismissErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionCallsCount, 0)
        XCTAssertNil(presenterSpy.actionSelected)
        XCTAssertEqual(presenterSpy.displayNetworkBannerCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayErrorCallsCount, 0)
        XCTAssertNil(presenterSpy.errorMessage)
    }
    
    func testAuthorize_WhenReceiveAuthorizeFromViewController_ShouldDisplayDocumentErrorWithMessage() {
        let message = "Mensagem de erro"
        serviceMock.authenticationResult = .failure(.document(message))
        sut.authorize(password: "123456")

        let event = Tracker(eventType: SavedAccountEvents.error(.cnpjNotRegistered))
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.displayLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.dismissErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionCallsCount, 0)
        XCTAssertNil(presenterSpy.actionSelected)
        XCTAssertEqual(presenterSpy.displayNetworkBannerCallsCount, 0)
        XCTAssertEqual(presenterSpy.displayErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.errorMessage, message)
    }
    
    func testAuthorize_WhenReceiveAuthorizeFromViewController_ShouldDisplayDocumentErrorWithoutMessage() {
        serviceMock.authenticationResult = .failure(.document(nil))
        sut.authorize(password: "123456")

        let event = Tracker(eventType: SavedAccountEvents.error(.cnpjNotRegistered))
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.displayLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.dismissErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionCallsCount, 0)
        XCTAssertNil(presenterSpy.actionSelected)
        XCTAssertEqual(presenterSpy.displayNetworkBannerCallsCount, 0)
        XCTAssertEqual(presenterSpy.displayErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.errorMessage, "Dados para acesso incorretos. Verifique para continuar.")
    }
    
    func testAuthorize_WhenReceiveAuthorizeFromViewController_ShouldDisplayAuthErrorWithMessage() {
        let message = "Mensagem de erro"
        serviceMock.authenticationResult = .failure(.auth(message))
        sut.authorize(password: "123456")

        let event = Tracker(eventType: SavedAccountEvents.error(.invalidLoginData))
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.displayLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.dismissErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionCallsCount, 0)
        XCTAssertNil(presenterSpy.actionSelected)
        XCTAssertEqual(presenterSpy.displayNetworkBannerCallsCount, 0)
        XCTAssertEqual(presenterSpy.displayErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.errorMessage, message)
    }
    
    func testAuthorize_WhenReceiveAuthorizeFromViewController_ShouldDisplayAuthErrorWithoutMessage() {
        serviceMock.authenticationResult = .failure(.auth(nil))
        sut.authorize(password: "123456")

        let event = Tracker(eventType: SavedAccountEvents.error(.invalidLoginData))
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.displayLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.dismissErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionCallsCount, 0)
        XCTAssertNil(presenterSpy.actionSelected)
        XCTAssertEqual(presenterSpy.displayNetworkBannerCallsCount, 0)
        XCTAssertEqual(presenterSpy.displayErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.errorMessage, "Dados para acesso incorretos. Verifique para continuar.")
    }
    
    func testAuthorize_WhenReceiveAuthorizeFromViewController_ShouldDisplayUserErrorWithMessage() {
        let message = "Mensagem de erro"
        serviceMock.authenticationResult = .failure(.user(message))
        sut.authorize(password: "123456")

        let event = Tracker(eventType: SavedAccountEvents.error(.invalidLoginData))
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.displayLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.dismissErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionCallsCount, 0)
        XCTAssertNil(presenterSpy.actionSelected)
        XCTAssertEqual(presenterSpy.displayNetworkBannerCallsCount, 0)
        XCTAssertEqual(presenterSpy.displayErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.errorMessage, message)
    }
    
    func testAuthorize_WhenReceiveAuthorizeFromViewController_ShouldDisplayUserErrorWithoutMessage() {
        serviceMock.authenticationResult = .failure(.user(nil))
        sut.authorize(password: "123456")

        let event = Tracker(eventType: SavedAccountEvents.error(.invalidLoginData))
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.displayLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.dismissErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionCallsCount, 0)
        XCTAssertNil(presenterSpy.actionSelected)
        XCTAssertEqual(presenterSpy.displayNetworkBannerCallsCount, 0)
        XCTAssertEqual(presenterSpy.displayErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.errorMessage, "Dados para acesso incorretos. Verifique para continuar.")
    }
    
    func testAuthorize_WhenReceiveAuthorizeFromViewController_ShouldDisplaySellerErrorWithMessage() {
        let message = "Mensagem de erro"
        serviceMock.authenticationResult = .failure(.seller(message))
        sut.authorize(password: "123456")

        let event = Tracker(eventType: SavedAccountEvents.error(.invalidLoginData))
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.displayLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.dismissErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionCallsCount, 0)
        XCTAssertNil(presenterSpy.actionSelected)
        XCTAssertEqual(presenterSpy.displayNetworkBannerCallsCount, 0)
        XCTAssertEqual(presenterSpy.displayErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.errorMessage, message)
    }
    
    func testAuthorize_WhenReceiveAuthorizeFromViewController_ShouldDisplaySellerErrorWithoutMessage() {
        serviceMock.authenticationResult = .failure(.seller(nil))
        sut.authorize(password: "123456")

        let event = Tracker(eventType: SavedAccountEvents.error(.invalidLoginData))
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.displayLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.dismissErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionCallsCount, 0)
        XCTAssertNil(presenterSpy.actionSelected)
        XCTAssertEqual(presenterSpy.displayNetworkBannerCallsCount, 0)
        XCTAssertEqual(presenterSpy.displayErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.errorMessage, "Dados para acesso incorretos. Verifique para continuar.")
    }

    func testChooseAnotherAccount_WhenReceiveChooseAnotherAccountFromViewController_ShouldDisplayAnotherAccountFlow() {
        sut.chooseAnotherAccount()

        let event = Tracker(eventType: SavedAccountEvents.anotherAccount)
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.actionCallsCount, 1)
    }

    func testEditAccount_WhenReceiveEditAccountFromViewController_ShouldDisplayEditAccountFlow() {
        sut.editAccount()

        XCTAssertEqual(presenterSpy.actionCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionSelected, .edit(AuthenticationMock.Account.full, editingDelegate: delegateSpy))
    }

    func testForgotPassword_WhenReceiveForgotPasswordFromViewController_ShouldDisplayForgotPasswordFlow() {
        sut.forgotPassword()

        let event = Tracker(eventType: SavedAccountEvents.recoverPassword)
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.actionCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionSelected, .forgotPassword)
    }

    func testAccountWasEdited_WhenReceiveAccountWasEditedFromDelegate_ShouldDisplayAccountEditedSnackbar() {
        let account = AuthenticationMock.Account.full
        sut.accountWasEdited(account: account)

        XCTAssertEqual(presenterSpy.displayAccountCallsCount, 1)
        XCTAssertEqual(presenterSpy.account, AuthenticationMock.Account.full)
        XCTAssertEqual(presenterSpy.displayAccountEditedSnackbarCallsCount, 1)
    }

    func testAccountWasRemoved_WhenReceiveAccountWasRemovedFromDelegate_ShouldDisplayAccountRemovedSnackbar() {
        sut.accountWasRemoved()

        XCTAssertEqual(presenterSpy.actionCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionSelected, .restart)
        XCTAssertEqual(presenterSpy.displayAccountRemovedSnackbarCallsCount, 1)
    }

    func testFaq_WhenReceiveFaqFromViewController_ShouldDisplayFaq() {
        sut.faq()

        let event = Tracker(eventType: SavedAccountEvents.faq)
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.actionCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionSelected, .faq)
    }
}
