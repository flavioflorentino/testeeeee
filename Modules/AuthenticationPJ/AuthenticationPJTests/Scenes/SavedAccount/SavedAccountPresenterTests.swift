import XCTest
import UI
@testable import AuthenticationPJ

private final class SavedAccountCoordinatorSpy: SavedAccountCoordinating {
    var viewController: UIViewController?
    
    private(set) var actionCallsCount = 0
    private(set) var actionSelected: SavedAccountAction?
    
    func perform(action: SavedAccountAction) {
        actionSelected = action
        actionCallsCount += 1
    }
}

final class SavedAccountDisplaySpy: SavedAccountDisplaying {
    private(set) var displayAccountCallsCount = 0
    private(set) var account: Account?
    
    func displayAccount(_ account: Account) {
        displayAccountCallsCount += 1
        self.account = account
    }
    
    private(set) var displayPasswordErrorCallsCount = 0
    private(set) var passwordErrorMessage: String?
    
    func displayPasswordError(_ message: String) {
        displayPasswordErrorCallsCount += 1
        self.passwordErrorMessage = message
    }
    
    private(set) var dismissPasswordErrorCallsCount = 0
    
    func dismissPasswordError() {
        dismissPasswordErrorCallsCount += 1
    }
    
    private(set) var enableLoginButtonCallsCount = 0
    
    func enableLoginButton() {
        enableLoginButtonCallsCount += 1
    }
    
    private(set) var disableLoginButtonCallsCount = 0
    
    func disableLoginButton() {
        disableLoginButtonCallsCount += 1
    }
    
    private(set) var displayLoadingCallsCount = 0
    
    func displayLoading() {
        displayLoadingCallsCount += 1
    }
    
    private(set) var dismissLoadingCallsCount = 0
    
    func dismissLoading() {
        dismissLoadingCallsCount += 1
    }
    
    private(set) var displayUpdateBannerCallsCount = 0
    
    func displayUpdateBanner() {
        displayUpdateBannerCallsCount += 1
    }
}

final class SavedAccountPresenterTests: XCTestCase {
    private let coordinatorSpy = SavedAccountCoordinatorSpy()
    private let displaySpy = SavedAccountDisplaySpy()
    private lazy var sut: SavedAccountPresenting = {
        let presenter = SavedAccountPresenter(coordinator: coordinatorSpy)
        presenter.viewController = displaySpy
        return presenter
    }()
    
    func testDidNextStep_WhenReceiveDidNextStepFromInteractor_ShouldCompleteFlow() {
        let response = UserAuthResponse()
        sut.didNextStep(action: .complete(response))
        XCTAssertEqual(coordinatorSpy.actionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.actionSelected, .complete(response))
    }
    
    func testDisplayAccount_WhenReceiveDisplayAccountFromInteractor_ShouldDisplayAccount() {
        let account = AuthenticationMock.Account.full
        sut.displayAccount(account)
        XCTAssertEqual(displaySpy.displayAccountCallsCount, 1)
        XCTAssertEqual(displaySpy.account, account)
    }
    
    func testDisplayError_WhenReceiveDisplayErrorFromInteractor_ShouldDisplayPasswordError() {
        let message = "Mensagem de erro"
        sut.displayError(message)
        XCTAssertEqual(displaySpy.displayPasswordErrorCallsCount, 1)
        XCTAssertEqual(displaySpy.passwordErrorMessage, message)
    }
        
    func testDismissErrors_WhenReceiveDismissErrorsFromInteractor_ShouldDismissErrors() {
        sut.dismissError()
        XCTAssertEqual(displaySpy.dismissPasswordErrorCallsCount, 1)
    }
    
    func testEnableLoginButton_WhenReceiveEnableLoginButtonFromInteractor_ShouldEnableLoginButton() {
        sut.enableLoginButton()
        XCTAssertEqual(displaySpy.enableLoginButtonCallsCount, 1)
    }
    
    func testDisableLoginButton_WhenReceiveDisableLoginButtonFromInteractor_ShouldDisableLoginButton() {
        sut.disableLoginButton()
        XCTAssertEqual(displaySpy.disableLoginButtonCallsCount, 1)
    }
    
    func testDisplayLoading_WhenReceiveDisplayLoadingFromInteractor_ShouldDisplayLoading() {
        sut.displayLoading()
        XCTAssertEqual(displaySpy.displayLoadingCallsCount, 1)
    }
    
    func testDismissLoading_WhenReceiveDismissLoadingFromInteractor_ShouldDismissLoading() {
        sut.dismissLoading()
        XCTAssertEqual(displaySpy.dismissLoadingCallsCount, 1)
    }
        
    func testDisplayNetworkBanner_WhenReceiveDisplayNetworkBannerFromInteractor_ShouldDisplayNetworkBanner() {
        sut.displayNetworkBanner()
        XCTAssertEqual(coordinatorSpy.actionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.actionSelected, .network)
    }

    func testDisplayAccountRemovedSnackbar_WhenReceiveDisplayAccountRemovedSnackbarFromInteractor_ShouldDisplayAccountRemovedSnackbar() {
        sut.displayAccountRemovedSnackbar()
        XCTAssertEqual(coordinatorSpy.actionCallsCount, 1)
        let snackbar = ApolloSnackbar(text: "Dados da conta removidos.", iconType: .success)
        XCTAssertEqual(coordinatorSpy.actionSelected, .snackbar(snackbar))
    }

    func testDisplayAccountEditedSnackbar_WhenReceiveDisplayAccountEditedSnackbarFromInteractor_ShouldDisplayAccountEditedSnackbar() {
        sut.displayAccountEditedSnackbar()
        XCTAssertEqual(coordinatorSpy.actionCallsCount, 1)
        let snackbar = ApolloSnackbar(text: "Dados da conta editados.", iconType: .success)
        XCTAssertEqual(coordinatorSpy.actionSelected, .snackbar(snackbar))
    }
}
