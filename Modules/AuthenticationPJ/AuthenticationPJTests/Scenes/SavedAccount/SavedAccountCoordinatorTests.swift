import XCTest
import UI
@testable import AuthenticationPJ

private final class AuthenticationCoordinatingSpy: AuthenticationCoordinating {
    private(set) var startCallsCount = 0

    func start() {
        startCallsCount += 1
    }

    private(set) var loginCallsCount = 0
    private(set) var isAddingAccount: Bool?

    func login(isAddingAccount: Bool) {
        loginCallsCount += 1
        self.isAddingAccount = isAddingAccount
    }

    private(set) var firstAccessCallsCount = 0

    func firstAccess() {
        firstAccessCallsCount += 1
    }

    private(set) var savedAccountCallsCount = 0
    private(set) var account: Account?

    func savedAccount(_ account: Account) {
        savedAccountCallsCount += 1
        self.account = account
    }

    private(set) var accountManagementCallsCount = 0

    func accountManagement() {
        accountManagementCallsCount += 1
    }

    private(set) var accountEditingCallsCount = 0
    private(set) var hasDeleteButton: Bool?
    // swiftlint:disable:next weak_delegate
    private(set) var accountEditingDelegate: AccountEditingDelegate?

    func accountEditing(_ account: Account, hasDeleteButton: Bool, delegate: AccountEditingDelegate) {
        accountEditingCallsCount += 1
        self.account = account
        self.hasDeleteButton = hasDeleteButton
        self.accountEditingDelegate = delegate
    }

    private(set) var completeCallsCount = 0
    private(set) var userAuthResponse: UserAuthResponse?

    func complete(with userAuthResponse: UserAuthResponse) {
        completeCallsCount += 1
        self.userAuthResponse = userAuthResponse
    }

    private(set) var closeCallsCount = 0

    func close() {
        closeCallsCount += 1
    }

    private(set) var faqCallsCount = 0

    func faq() {
        faqCallsCount += 1
    }

    private(set) var forgotPasswordCallsCount = 0

    func forgotPassword() {
        forgotPasswordCallsCount += 1
    }

    private(set) var registerCallsCount = 0

    func register() {
        registerCallsCount += 1
    }

    private(set) var snackbarCallsCount = 0
    private(set) var snackbar: ApolloSnackbar?

    func snackbar(_ snackbar: ApolloSnackbar) {
        snackbarCallsCount += 1
        self.snackbar = snackbar
    }

    private(set) var networkSnackbarCallsCount = 0

    func networkSnackbar() {
        networkSnackbarCallsCount += 1
    }
}

private final class AccountEditingDelegateSpy: AccountEditingDelegate {
    private(set) var accountWasEditedCallsCount = 0
    private(set) var account: Account?

    func accountWasEdited(account: Account) {
        accountWasEditedCallsCount += 1
        self.account = account
    }

    private(set) var accountWasRemovedCallsCount = 0

    func accountWasRemoved() {
        accountWasRemovedCallsCount += 1
    }
}

final class SavedAccountCoordinatorTests: XCTestCase {
    private let navigationSpy = NavigationControllerSpy(rootViewController: UIViewController())
    private let accountEditingDelegateSpy = AccountEditingDelegateSpy()
    private let delegateSpy = AuthenticationCoordinatingSpy()
    private lazy var sut: SavedAccountCoordinating = {
        let coordinator = SavedAccountCoordinator(delegate: delegateSpy)
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()

    func testPerform_WhenReceiveDidNextStepFromPresenter_ShouldCallDelegateToFAQ() {
        sut.perform(action: .faq)

        XCTAssertEqual(delegateSpy.faqCallsCount, 1)
    }

    func testPerform_WhenReceiveDidNextStepFromPresenter_ShouldCallDelegateToComplete() {
        let userAuthResponse = UserAuthResponse()
        sut.perform(action: .complete(userAuthResponse))

        XCTAssertEqual(delegateSpy.completeCallsCount, 1)
        XCTAssertEqual(delegateSpy.userAuthResponse, userAuthResponse)
    }

    func testPerform_WhenReceiveDidNextStepFromPresenter_ShouldCallDelegateToEdit() {
        let account = AuthenticationMock.Account.full
        sut.perform(action: .edit(account, editingDelegate: accountEditingDelegateSpy))

        XCTAssertEqual(delegateSpy.accountEditingCallsCount, 1)
        XCTAssertEqual(delegateSpy.account, account)
    }

    func testPerform_WhenReceiveDidNextStepFromPresenter_ShouldCallDelegateToAccess() {
        sut.perform(action: .anotherAccount)

        XCTAssertEqual(delegateSpy.loginCallsCount, 1)
    }

    func testPerform_WhenReceiveDidNextStepFromPresenter_ShouldCallDelegateToForgotPassword() {
        sut.perform(action: .forgotPassword)

        XCTAssertEqual(delegateSpy.forgotPasswordCallsCount, 1)
    }

    func testPerform_WhenReceiveDidNextStepFromPresenter_ShouldCallDelegateToRestart() {
        sut.perform(action: .restart)

        XCTAssertEqual(delegateSpy.startCallsCount, 1)
    }

    func testPerform_WhenReceiveDidNextStepFromPresenter_ShouldCallDelegateToSnackbar() {
        let snackbar = ApolloSnackbar(text: "Dados da conta editados.", iconType: .success)
        sut.perform(action: .snackbar(snackbar))

        XCTAssertEqual(delegateSpy.snackbarCallsCount, 1)
        XCTAssertEqual(delegateSpy.snackbar, snackbar)
    }

    func testPerform_WhenReceiveDidNextStepFromPresenter_ShouldCallDelegateToNetwork() {
        sut.perform(action: .network)

        XCTAssertEqual(delegateSpy.networkSnackbarCallsCount, 1)
    }
}
