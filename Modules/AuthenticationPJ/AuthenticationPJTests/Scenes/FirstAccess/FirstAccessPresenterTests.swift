import XCTest
@testable import AuthenticationPJ

private final class FirstAccessCoordinatorSpy: FirstAccessCoordinating {
    var viewController: UIViewController?
    
    private(set) var actionCallsCount = 0
    private(set) var actionSelected: FirstAccessAction?
    
    func perform(action: FirstAccessAction) {
        actionSelected = action
        actionCallsCount += 1
    }
}

private final class FirstAccessDisplaySpy: FirstAccessDisplaying {
    private(set) var displayDocumentErrorCallsCount = 0
    private(set) var documentErrorMessage: String?
    
    func displayDocumentError(_ message: String) {
        displayDocumentErrorCallsCount += 1
        documentErrorMessage = message
    }
    
    private(set) var dismissDocumentErrorCallsCount = 0
    
    func dismissDocumentError() {
        dismissDocumentErrorCallsCount += 1
    }
    
    private(set) var displayUsernameErrorCallsCount = 0
    private(set) var usernameErrorMessage: String?
    
    func displayUsernameError(_ message: String) {
        displayUsernameErrorCallsCount += 1
        usernameErrorMessage = message
    }
    
    private(set) var dismissUsernameErrorCallsCount = 0
    
    func dismissUsernameError() {
        dismissUsernameErrorCallsCount += 1
    }
    
    private(set) var displayPasswordErrorCallsCount = 0
    private(set) var passwordErrorMessage: String?
    
    func displayPasswordError(_ message: String) {
        displayPasswordErrorCallsCount += 1
        passwordErrorMessage = message
    }
    
    private(set) var dismissPasswordErrorCallsCount = 0
    
    func dismissPasswordError() {
        dismissPasswordErrorCallsCount += 1
    }
    
    private(set) var enableLoginButtonCallsCount = 0
    
    func enableLoginButton() {
        enableLoginButtonCallsCount += 1
    }
    
    private(set) var disableLoginButtonCallsCount = 0
    
    func disableLoginButton() {
        disableLoginButtonCallsCount += 1
    }
    
    private(set) var displayLoadingCallsCount = 0
    
    func displayLoading() {
        displayLoadingCallsCount += 1
    }
    
    private(set) var dismissLoadingCallsCount = 0
    
    func dismissLoading() {
        dismissLoadingCallsCount += 1
    }
}

final class FirstAccessPresenterTests: XCTestCase {
    private let coordinatorSpy = FirstAccessCoordinatorSpy()
    private let displaySpy = FirstAccessDisplaySpy()
    private lazy var sut: FirstAccessPresenting = {
        let presenter = FirstAccessPresenter(coordinator: coordinatorSpy)
        presenter.viewController = displaySpy
        return presenter
    }()
    
    func testDidNextStep_WhenReceiveDidNextStepFromInteractor_ShouldCompleteFlow() {
        let response = UserAuthResponse()
        sut.didNextStep(action: .complete(response))
        XCTAssertEqual(coordinatorSpy.actionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.actionSelected, .complete(response))
    }
    
    func testDisplayDocumentError_WhenReceiveDisplayDocumentErrorFromInteractor_ShouldDisplayDocumentError() {
        let message = "Mensagem de erro"
        sut.displayDocumentError(message)
        XCTAssertEqual(displaySpy.displayDocumentErrorCallsCount, 1)
        XCTAssertEqual(displaySpy.documentErrorMessage, message)
    }
    
    func testDisplayUsernameError_WhenReceiveDisplayUsernameErrorFromInteractor_ShouldDisplayUsernameError() {
        let message = "Mensagem de erro"
        sut.displayUsernameError(message)
        XCTAssertEqual(displaySpy.displayUsernameErrorCallsCount, 1)
        XCTAssertEqual(displaySpy.usernameErrorMessage, message)
    }
    
    func testDisplayPasswordError_WhenReceiveDisplayPasswordErrorFromInteractor_ShouldDisplayPasswordError() {
        let message = "Mensagem de erro"
        sut.displayPasswordError(message)
        XCTAssertEqual(displaySpy.displayPasswordErrorCallsCount, 1)
        XCTAssertEqual(displaySpy.passwordErrorMessage, message)
    }
    
    func testDismissErrors_WhenReceiveDismissErrorsFromInteractor_ShouldDismissErrors() {
        sut.dismissErrors()
        XCTAssertEqual(displaySpy.dismissDocumentErrorCallsCount, 1)
        XCTAssertEqual(displaySpy.dismissUsernameErrorCallsCount, 1)
        XCTAssertEqual(displaySpy.dismissPasswordErrorCallsCount, 1)
    }
    
    func testDisplayNetworkBanner_WhenReceiveDisplayNetworkBannerFromInteractor_ShouldDisplayNetworkBanner() {
        sut.displayNetworkBanner()

        XCTAssertEqual(coordinatorSpy.actionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.actionSelected, .network)
    }
    
    func testEnableLoginButton_WhenReceiveEnableLoginButtonFromInteractor_ShouldEnableLoginButton() {
        sut.enableLoginButton()
        XCTAssertEqual(displaySpy.enableLoginButtonCallsCount, 1)
    }
    
    func testDisableLoginButton_WhenReceiveDisableLoginButtonFromInteractor_ShouldDisableLoginButton() {
        sut.disableLoginButton()
        XCTAssertEqual(displaySpy.disableLoginButtonCallsCount, 1)
    }
    
    func testDisplayLoading_WhenReceiveDisplayLoadingFromInteractor_ShouldDisplayLoading() {
        sut.displayLoading()
        XCTAssertEqual(displaySpy.displayLoadingCallsCount, 1)
    }
    
    func testDismissLoading_WhenReceiveDismissLoadingFromInteractor_ShouldDismissLoading() {
        sut.dismissLoading()
        XCTAssertEqual(displaySpy.dismissLoadingCallsCount, 1)
    }
}
