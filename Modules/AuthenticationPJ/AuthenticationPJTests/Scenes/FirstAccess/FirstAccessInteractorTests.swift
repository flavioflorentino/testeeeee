import AnalyticsModule
import Core
import Foundation
import XCTest
@testable import AuthenticationPJ

private final class FirstAccessServiceMock: FirstAccessServicing {
    var authenticationResult: Result<UserAuthResponse, AuthenticationError>?
    
    func authentication(document: String, username: String, password: String, mustRemind: Bool, completion: @escaping (Result<UserAuthResponse, AuthenticationError>) -> Void) {
        guard let authenticationResult = authenticationResult else {
            XCTFail("No authenticationResult provided")
            return
        }
        completion(authenticationResult)
    }
}

private final class FirstAccessPresenterSpy: FirstAccessPresenting {
    var viewController: FirstAccessDisplaying?
    
    private(set) var actionCallsCount = 0
    private(set) var actionSelected: FirstAccessAction?
    
    func didNextStep(action: FirstAccessAction) {
        actionSelected = action
        actionCallsCount += 1
    }
    
    private(set) var displayDocumentErrorCallsCount = 0
    private(set) var documentErrorMessage: String?
    
    func displayDocumentError(_ message: String) {
        displayDocumentErrorCallsCount += 1
        documentErrorMessage = message
    }
    private(set) var displayUsernameErrorCallsCount = 0
    private(set) var usernameErrorMessage: String?
    
    func displayUsernameError(_ message: String) {
        displayUsernameErrorCallsCount += 1
        usernameErrorMessage = message
    }
    private(set) var displayPasswordErrorCallsCount = 0
    private(set) var passwordErrorMessage: String?
    
    func displayPasswordError(_ message: String) {
        displayPasswordErrorCallsCount += 1
        passwordErrorMessage = message
    }

    private(set) var dismissErrorsCallsCount = 0
    
    func dismissErrors() {
        dismissErrorsCallsCount += 1
    }
    
    private(set) var displayNetworkBannerCallsCount = 0
    
    func displayNetworkBanner() {
        displayNetworkBannerCallsCount += 1
    }
    
    private(set) var enableLoginButtonCallsCount = 0
    
    func enableLoginButton() {
        enableLoginButtonCallsCount += 1
    }
    
    private(set) var disableLoginButtonCallsCount = 0
    
    func disableLoginButton() {
        disableLoginButtonCallsCount += 1
    }
    
    private(set) var displayLoadingCallsCount = 0
    
    func displayLoading() {
        displayLoadingCallsCount += 1
    }
    
    private(set) var dismissLoadingCallsCount = 0
    
    func dismissLoading() {
        dismissLoadingCallsCount += 1
    }
}

final class FirstAccessInteractorTests: XCTestCase {
    private let analyticsMock = AnalyticsSpy()
    private lazy var dependenciesMock = DependencyContainerMock(analyticsMock)
    private let serviceMock = FirstAccessServiceMock()
    private let presenterSpy = FirstAccessPresenterSpy()
    private lazy var sut = FirstAccessInteractor(
        service: serviceMock,
        presenter: presenterSpy,
        dependencies: dependenciesMock
    )
    
    func testValidateFields_WhenReceiveValidateFieldFromViewController_ShouldDoNothing() {
        sut.validateFields(document: nil, username: nil, password: nil)
        
        XCTAssertEqual(presenterSpy.displayDocumentErrorCallsCount, 0)
        XCTAssertNil(presenterSpy.documentErrorMessage)
        XCTAssertEqual(presenterSpy.dismissErrorsCallsCount, 0)
        XCTAssertEqual(presenterSpy.disableLoginButtonCallsCount, 0)
        XCTAssertEqual(presenterSpy.enableLoginButtonCallsCount, 0)
    }
    
    func testValidateFields_WhenReceiveValidateFieldFromViewController_ShouldDisplayDocumentErrorsAndDisableLoginButton() {
        sut.validateFields(document: "11.111.111/1111-11", username: "teste@picpay.com", password: "123456")

        let event = Tracker(eventType: FirstAccessEvents.error(.cnpjInvalid))
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.displayDocumentErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.documentErrorMessage, "CNPJ inválido, confira os caracteres.")
        XCTAssertEqual(presenterSpy.dismissErrorsCallsCount, 0)
        XCTAssertEqual(presenterSpy.disableLoginButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.enableLoginButtonCallsCount, 0)
    }
    
    func testValidateFields_WhenReceiveValidateFieldFromViewController_ShouldDismissErrorsAndDisableLoginButton() {
        sut.validateFields(document: "37.442.929/0001-80", username: "", password: "")
        
        XCTAssertEqual(presenterSpy.displayDocumentErrorCallsCount, 0)
        XCTAssertNil(presenterSpy.documentErrorMessage)
        XCTAssertEqual(presenterSpy.dismissErrorsCallsCount, 1)
        XCTAssertEqual(presenterSpy.disableLoginButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.enableLoginButtonCallsCount, 0)
    }
    
    func testValidateFields_WhenReceiveValidateFieldFromViewController_ShouldDismissErrorsAndEnableLoginButton() {
        sut.validateFields(document: "37.442.929/0001-80", username: "teste@picpay.com", password: "123456")
        
        XCTAssertEqual(presenterSpy.displayDocumentErrorCallsCount, 0)
        XCTAssertNil(presenterSpy.documentErrorMessage)
        XCTAssertEqual(presenterSpy.dismissErrorsCallsCount, 1)
        XCTAssertEqual(presenterSpy.disableLoginButtonCallsCount, 0)
        XCTAssertEqual(presenterSpy.enableLoginButtonCallsCount, 1)
    }
    
    func testAuthorize_WhenReceiveAuthorizeFromViewController_ShouldDoNothing() {
        sut.authorize(document: nil, username: nil, password: nil, mustRemind: false)
        
        XCTAssertEqual(presenterSpy.displayLoadingCallsCount, 0)
        XCTAssertEqual(presenterSpy.dismissErrorsCallsCount, 0)
        XCTAssertEqual(presenterSpy.actionCallsCount, 0)
        XCTAssertNil(presenterSpy.actionSelected)
        XCTAssertEqual(presenterSpy.displayNetworkBannerCallsCount, 0)
        XCTAssertEqual(presenterSpy.displayDocumentErrorCallsCount, 0)
        XCTAssertNil(presenterSpy.documentErrorMessage)
        XCTAssertEqual(presenterSpy.displayUsernameErrorCallsCount, 0)
        XCTAssertNil(presenterSpy.usernameErrorMessage)
        XCTAssertEqual(presenterSpy.displayPasswordErrorCallsCount, 0)
        XCTAssertNil(presenterSpy.passwordErrorMessage)
    }
    
    func testAuthorize_WhenReceiveAuthorizeFromViewController_ShouldCompleteFlow() {
        let response = UserAuthResponse()
        serviceMock.authenticationResult = .success(response)
        sut.authorize(document: "37.442.929/0001-80", username: "teste@picpay.com", password: "123456", mustRemind: false)

        let event = Tracker(eventType: FirstAccessEvents.success(false, saveData: false))
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.displayLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.dismissErrorsCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionSelected, .complete(response))
        XCTAssertEqual(presenterSpy.displayNetworkBannerCallsCount, 0)
        XCTAssertEqual(presenterSpy.displayDocumentErrorCallsCount, 0)
        XCTAssertNil(presenterSpy.documentErrorMessage)
        XCTAssertEqual(presenterSpy.displayUsernameErrorCallsCount, 0)
        XCTAssertNil(presenterSpy.usernameErrorMessage)
        XCTAssertEqual(presenterSpy.displayPasswordErrorCallsCount, 0)
        XCTAssertNil(presenterSpy.passwordErrorMessage)
    }

    func testAuthorize_WhenReceiveAuthorizeFromViewController_ShouldCompleteFlowAndRemind() {
        let response = UserAuthResponse()
        serviceMock.authenticationResult = .success(response)
        sut.authorize(document: "37.442.929/0001-80", username: "teste@picpay.com", password: "123456", mustRemind: true)

        let event1 = Tracker(eventType: FirstAccessEvents.success(false, saveData: true))
        let event2 = Tracker(eventType: FirstAccessEvents.savedData)
        XCTAssertTrue(analyticsMock.equals(to: event1.event(), event2.event()))
        XCTAssertEqual(presenterSpy.displayLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.dismissErrorsCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionSelected, .complete(response))
        XCTAssertEqual(presenterSpy.displayNetworkBannerCallsCount, 0)
        XCTAssertEqual(presenterSpy.displayDocumentErrorCallsCount, 0)
        XCTAssertNil(presenterSpy.documentErrorMessage)
        XCTAssertEqual(presenterSpy.displayUsernameErrorCallsCount, 0)
        XCTAssertNil(presenterSpy.usernameErrorMessage)
        XCTAssertEqual(presenterSpy.displayPasswordErrorCallsCount, 0)
        XCTAssertNil(presenterSpy.passwordErrorMessage)
    }
    
    func testAuthorize_WhenReceiveAuthorizeFromViewController_ShouldDisplayNetworkError() {
        serviceMock.authenticationResult = .failure(.network)
        sut.authorize(document: "37.442.929/0001-80", username: "teste@picpay.com", password: "123456", mustRemind: false)
        
        XCTAssertEqual(presenterSpy.displayLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.dismissErrorsCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionCallsCount, 0)
        XCTAssertNil(presenterSpy.actionSelected)
        XCTAssertEqual(presenterSpy.displayNetworkBannerCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayDocumentErrorCallsCount, 0)
        XCTAssertNil(presenterSpy.documentErrorMessage)
        XCTAssertEqual(presenterSpy.displayUsernameErrorCallsCount, 0)
        XCTAssertNil(presenterSpy.usernameErrorMessage)
        XCTAssertEqual(presenterSpy.displayPasswordErrorCallsCount, 0)
        XCTAssertNil(presenterSpy.passwordErrorMessage)
    }
    
    func testAuthorize_WhenReceiveAuthorizeFromViewController_ShouldDisplayDocumentErrorWithMessage() {
        let message = "Mensagem de erro"
        serviceMock.authenticationResult = .failure(.document(message))
        sut.authorize(document: "37.442.929/0001-80", username: "teste@picpay.com", password: "123456", mustRemind: false)

        let event = Tracker(eventType: FirstAccessEvents.error(.cnpjNotRegistered))
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.displayLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.dismissErrorsCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionCallsCount, 0)
        XCTAssertNil(presenterSpy.actionSelected)
        XCTAssertEqual(presenterSpy.displayNetworkBannerCallsCount, 0)
        XCTAssertEqual(presenterSpy.displayDocumentErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.documentErrorMessage, message)
        XCTAssertEqual(presenterSpy.displayUsernameErrorCallsCount, 0)
        XCTAssertNil(presenterSpy.usernameErrorMessage)
        XCTAssertEqual(presenterSpy.displayPasswordErrorCallsCount, 0)
        XCTAssertNil(presenterSpy.passwordErrorMessage)
    }
    
    func testAuthorize_WhenReceiveAuthorizeFromViewController_ShouldDisplayDocumentErrorWithoutMessage() {
        serviceMock.authenticationResult = .failure(.document(nil))
        sut.authorize(document: "37.442.929/0001-80", username: "teste@picpay.com", password: "123456", mustRemind: false)

        let event = Tracker(eventType: FirstAccessEvents.error(.cnpjNotRegistered))
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.displayLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.dismissErrorsCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionCallsCount, 0)
        XCTAssertNil(presenterSpy.actionSelected)
        XCTAssertEqual(presenterSpy.displayNetworkBannerCallsCount, 0)
        XCTAssertEqual(presenterSpy.displayDocumentErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.documentErrorMessage, "CNPJ inválido, confira os caracteres.")
        XCTAssertEqual(presenterSpy.displayUsernameErrorCallsCount, 0)
        XCTAssertNil(presenterSpy.usernameErrorMessage)
        XCTAssertEqual(presenterSpy.displayPasswordErrorCallsCount, 0)
        XCTAssertNil(presenterSpy.passwordErrorMessage)
    }
    
    func testAuthorize_WhenReceiveAuthorizeFromViewController_ShouldDisplayAuthErrorWithMessage() {
        let message = "Mensagem de erro"
        serviceMock.authenticationResult = .failure(.auth(message))
        sut.authorize(document: "37.442.929/0001-80", username: "teste@picpay.com", password: "123456", mustRemind: false)

        let event = Tracker(eventType: FirstAccessEvents.error(.invalidLoginData))
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.displayLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.dismissErrorsCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionCallsCount, 0)
        XCTAssertNil(presenterSpy.actionSelected)
        XCTAssertEqual(presenterSpy.displayNetworkBannerCallsCount, 0)
        XCTAssertEqual(presenterSpy.displayDocumentErrorCallsCount, 0)
        XCTAssertNil(presenterSpy.documentErrorMessage)
        XCTAssertEqual(presenterSpy.displayUsernameErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.usernameErrorMessage, "")
        XCTAssertEqual(presenterSpy.displayPasswordErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.passwordErrorMessage, message)
    }
    
    func testAuthorize_WhenReceiveAuthorizeFromViewController_ShouldDisplayAuthErrorWithoutMessage() {
        serviceMock.authenticationResult = .failure(.auth(nil))
        sut.authorize(document: "37.442.929/0001-80", username: "teste@picpay.com", password: "123456", mustRemind: false)

        let event = Tracker(eventType: FirstAccessEvents.error(.invalidLoginData))
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.displayLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.dismissErrorsCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionCallsCount, 0)
        XCTAssertNil(presenterSpy.actionSelected)
        XCTAssertEqual(presenterSpy.displayNetworkBannerCallsCount, 0)
        XCTAssertEqual(presenterSpy.displayDocumentErrorCallsCount, 0)
        XCTAssertNil(presenterSpy.documentErrorMessage)
        XCTAssertEqual(presenterSpy.displayUsernameErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.usernameErrorMessage, "")
        XCTAssertEqual(presenterSpy.displayPasswordErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.passwordErrorMessage, "E-mail e/ou senha incorretos. Confira os caracteres.")
    }
    
    func testAuthorize_WhenReceiveAuthorizeFromViewController_ShouldDisplayUserErrorWithMessage() {
        let message = "Mensagem de erro"
        serviceMock.authenticationResult = .failure(.user(message))
        sut.authorize(document: "37.442.929/0001-80", username: "teste@picpay.com", password: "123456", mustRemind: false)

        let event = Tracker(eventType: FirstAccessEvents.error(.invalidLoginData))
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.displayLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.dismissErrorsCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionCallsCount, 0)
        XCTAssertNil(presenterSpy.actionSelected)
        XCTAssertEqual(presenterSpy.displayNetworkBannerCallsCount, 0)
        XCTAssertEqual(presenterSpy.displayDocumentErrorCallsCount, 0)
        XCTAssertNil(presenterSpy.documentErrorMessage)
        XCTAssertEqual(presenterSpy.displayUsernameErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.usernameErrorMessage, "")
        XCTAssertEqual(presenterSpy.displayPasswordErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.passwordErrorMessage, message)
    }
    
    func testAuthorize_WhenReceiveAuthorizeFromViewController_ShouldDisplayUserErrorWithoutMessage() {
        serviceMock.authenticationResult = .failure(.user(nil))
        sut.authorize(document: "37.442.929/0001-80", username: "teste@picpay.com", password: "123456", mustRemind: false)

        let event = Tracker(eventType: FirstAccessEvents.error(.invalidLoginData))
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.displayLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.dismissErrorsCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionCallsCount, 0)
        XCTAssertNil(presenterSpy.actionSelected)
        XCTAssertEqual(presenterSpy.displayNetworkBannerCallsCount, 0)
        XCTAssertEqual(presenterSpy.displayDocumentErrorCallsCount, 0)
        XCTAssertNil(presenterSpy.documentErrorMessage)
        XCTAssertEqual(presenterSpy.displayUsernameErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.usernameErrorMessage, "")
        XCTAssertEqual(presenterSpy.displayPasswordErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.passwordErrorMessage, "E-mail e/ou senha incorretos. Confira os caracteres.")
    }
    
    func testAuthorize_WhenReceiveAuthorizeFromViewController_ShouldDisplaySellerErrorWithMessage() {
        let message = "Mensagem de erro"
        serviceMock.authenticationResult = .failure(.seller(message))
        sut.authorize(document: "37.442.929/0001-80", username: "teste@picpay.com", password: "123456", mustRemind: false)

        let event = Tracker(eventType: FirstAccessEvents.error(.invalidLoginData))
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.displayLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.dismissErrorsCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionCallsCount, 0)
        XCTAssertNil(presenterSpy.actionSelected)
        XCTAssertEqual(presenterSpy.displayNetworkBannerCallsCount, 0)
        XCTAssertEqual(presenterSpy.displayDocumentErrorCallsCount, 0)
        XCTAssertNil(presenterSpy.documentErrorMessage)
        XCTAssertEqual(presenterSpy.displayUsernameErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.usernameErrorMessage, "")
        XCTAssertEqual(presenterSpy.displayPasswordErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.passwordErrorMessage, message)
    }
    
    func testAuthorize_WhenReceiveAuthorizeFromViewController_ShouldDisplaySellerErrorWithoutMessage() {
        serviceMock.authenticationResult = .failure(.seller(nil))
        sut.authorize(document: "37.442.929/0001-80", username: "teste@picpay.com", password: "123456", mustRemind: false)

        let event = Tracker(eventType: FirstAccessEvents.error(.invalidLoginData))
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.displayLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.dismissErrorsCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionCallsCount, 0)
        XCTAssertNil(presenterSpy.actionSelected)
        XCTAssertEqual(presenterSpy.displayNetworkBannerCallsCount, 0)
        XCTAssertEqual(presenterSpy.displayDocumentErrorCallsCount, 0)
        XCTAssertNil(presenterSpy.documentErrorMessage)
        XCTAssertEqual(presenterSpy.displayUsernameErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.usernameErrorMessage, "")
        XCTAssertEqual(presenterSpy.displayPasswordErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.passwordErrorMessage, "E-mail e/ou senha incorretos. Confira os caracteres.")
    }

    func testForgotPassword_WhenReceiveForgotPasswordFromViewController_ShouldDisplayForgotPasswordFlow() {
        sut.forgotPassword()

        let event = Tracker(eventType: FirstAccessEvents.recoverPassword)
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.actionCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionSelected, .forgotPassword)
    }

    func testFaq_WhenReceiveFaqFromViewController_ShouldDisplayFaq() {
        sut.faq()

        let event = Tracker(eventType: FirstAccessEvents.faq)
        XCTAssertTrue(analyticsMock.equals(to: event.event()))
        XCTAssertEqual(presenterSpy.actionCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionSelected, .faq)
    }
}
