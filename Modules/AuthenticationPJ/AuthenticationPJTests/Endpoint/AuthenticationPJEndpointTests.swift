import XCTest
@testable import AuthenticationPJ

final class AuthenticationEndpointTests: XCTestCase {
    func testEndpointValidate() {
        let cnpj = "37.442.929/0001-80"
        let endpoint = AuthenticationEndpoint.validate(cnpj)
        XCTAssertEqual(endpoint.path, "/login/verify-cnpj")
        XCTAssertEqual(endpoint.method, .post)
        XCTAssertEqual(endpoint.customHeaders, [:])
        XCTAssertFalse(endpoint.isTokenNeeded)
        XCTAssertEqual(endpoint.body, ["cnpj": cnpj.onlyNumbers].toData())
    }
    
    func testEndpointAuthorize() throws {
        let cnpj = "37.442.929/0001-80"
        let username = "teste@picpay.com"
        let password = "123456"
        let endpoint = AuthenticationEndpoint.authorize(cnpj, username: username, password: password)
        XCTAssertEqual(endpoint.path, "/oauth/access-token")
        XCTAssertEqual(endpoint.method, .post)
        XCTAssertEqual(endpoint.customHeaders, [:])
        XCTAssertFalse(endpoint.isTokenNeeded)
        let clientId = Credentials.ApiClient.id
        let clientSecret = Credentials.ApiClient.secret
        let body = [
            "client_id": clientId,
            "client_secret": clientSecret,
            "grant_type": "password",
            "password": password,
            "username": [
                "cnpj": cnpj.onlyNumbers,
                "email": username
            ]
        ].toData()
        // Converts to [String: String] dictionary because it was giving inconsistency in the comparison of Data
        let endpointBody = try convertToDictionary(data: endpoint.body)
        let mockBody = try convertToDictionary(data: body)
        XCTAssertEqual(endpointBody, mockBody)
    }

    private func convertToDictionary(data: Data?) throws -> [String: String] {
        guard let data = data else {
            return [:]
        }
        
        let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: String]
        return json ?? [:]
    }
    
    func testEndpointRetrieveUser() {
        let endpoint = AuthenticationEndpoint.retrieveUser
        XCTAssertEqual(endpoint.path, "/authenticated")
        XCTAssertEqual(endpoint.method, .get)
        XCTAssertEqual(endpoint.customHeaders, [:])
        XCTAssertTrue(endpoint.isTokenNeeded)
        XCTAssertNil(endpoint.body)
    }
    
    func testEndpointRetrieveSeller() {
        let endpoint = AuthenticationEndpoint.retrieveSeller
        XCTAssertEqual(endpoint.path, "/seller")
        XCTAssertEqual(endpoint.method, .get)
        XCTAssertEqual(endpoint.customHeaders, [:])
        XCTAssertTrue(endpoint.isTokenNeeded)
        XCTAssertNil(endpoint.body)
    }
}
