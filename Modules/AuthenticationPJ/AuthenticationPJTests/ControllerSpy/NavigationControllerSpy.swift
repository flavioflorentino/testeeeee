import UIKit
import UI
@testable import AuthenticationPJ

final class NavigationControllerSpy: UINavigationController {
    private(set) var pushViewControllerCallsCount = 0
    private(set) var pushViewController: UIViewController?
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        super.pushViewController(viewController, animated: animated)
        pushViewController = viewController
        pushViewControllerCallsCount += 1
    }
    
    private(set) var presentCallsCount = 0
    private(set) var presentViewController: UIViewController?
    
    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        super.present(viewControllerToPresent, animated: flag, completion: completion)
        if let topViewController = (viewControllerToPresent as? UINavigationController)?.topViewController {
            presentViewController = topViewController
        } else {
            presentViewController = viewControllerToPresent
        }
        presentCallsCount += 1
    }
    
    private(set) var popViewControllerCallsCount = 0
    
    override func popViewController(animated: Bool) -> UIViewController? {
        popViewControllerCallsCount += 1
        return super.popViewController(animated: true)
    }
    
    private(set) var dismissViewControllerCallsCount = 0
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        super.dismiss(animated: flag, completion: completion)
        dismissViewControllerCallsCount += 1
    }
    
    private(set) var popToRootViewControllerCallsCount = 0
    
    // swiftlint:disable:next discouraged_optional_collection
    override func popToRootViewController(animated: Bool) -> [UIViewController]? {
        popToRootViewControllerCallsCount += 1
        return super.popToRootViewController(animated: animated)
    }
}
