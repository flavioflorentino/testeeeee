import XCTest
@testable import AuthenticationPJ

final class UserAuthResponseTests: XCTestCase {
    private let authJson = """
                {
                 "data": {
                  "accessToken": "1234567890",
                  "tokenType": "tokenType",
                  "expires_in": 123,
                  "refreshToken": "refreshToken",
                  "biometry": "denied",
                  "completed": false
                 }
                }
            """
    private let userJson = """
                {
                 "data": {
                  "id": 1,
                  "cpf": "37.442.929/0001-80",
                  "headOfficeId": 1,
                  "imgUrl": "https://images1.fanpop.com/images/image_uploads/Naruto-Sad-uzumaki-naruto-987855_620_465.jpg",
                  "mobilePhone": "11999999999",
                  "birthDateFormated": "31/03/1998",
                  "username": "username",
                  "super": 1,
                  "role": 1,
                  "email": "teste@picpay.com",
                  "name": "Uzumaki Naruto"
                 }
                }
            """
    private let sellerJson = """
                {
                 "data": {
                  "name": "Uzumaki Naruto",
                  "cpfCnpj": "37.442.929/0001-80",
                  "imgUrl": "https://images1.fanpop.com/images/image_uploads/Naruto-Sad-uzumaki-naruto-987855_620_465.jpg"
                 }
                }
            """
    
    func testUserAuth_ShouldAuthAndUserAndSellerBeNil() {
        let userAuthResponse = UserAuthResponse()
        
        XCTAssertNil(userAuthResponse.auth)
        XCTAssertNil(userAuthResponse.user)
        XCTAssertNil(userAuthResponse.seller)
    }
    
    func testUserAuth_ShouldUserAndSellerBeNil() throws {
        let userAuthResponse = UserAuthResponse()
        
        let authData = try XCTUnwrap(authJson.data(using: .utf8))
        let authResponse = try JSONDecoder(.convertFromSnakeCase).decode(AuthResponse.self, from: authData)
        userAuthResponse.auth = authResponse
        
        XCTAssertEqual(userAuthResponse.auth, authResponse)
        XCTAssertNil(userAuthResponse.user)
        XCTAssertNil(userAuthResponse.seller)
    }
    
    func testUserAuth_ShouldAuthAndSellerBeNil() throws {
        let userAuthResponse = UserAuthResponse()
        
        let userData = try XCTUnwrap(userJson.data(using: .utf8))
        let userResponse = try JSONDecoder(.convertFromSnakeCase).decode(UserResponse.self, from: userData)
        userAuthResponse.user = userResponse
        
        XCTAssertNil(userAuthResponse.auth)
        XCTAssertEqual(userAuthResponse.user, userResponse)
    }
    
    func testUserAuth_ShouldAuthAndUserBeNil() throws {
        let userAuthResponse = UserAuthResponse()
        
        let sellerData = try XCTUnwrap(sellerJson.data(using: .utf8))
        let sellerResponse = try JSONDecoder(.convertFromSnakeCase).decode(SellerResponse.self, from: sellerData)
        userAuthResponse.seller = sellerResponse
        
        XCTAssertNil(userAuthResponse.auth)
        XCTAssertNil(userAuthResponse.user)
        XCTAssertEqual(userAuthResponse.seller, sellerResponse)
    }

    func testUserAuth() throws {
        let userAuthResponse = UserAuthResponse()
        
        let authData = try XCTUnwrap(authJson.data(using: .utf8))
        let authResponse = try JSONDecoder(.convertFromSnakeCase).decode(AuthResponse.self, from: authData)
        userAuthResponse.auth = authResponse
        
        let userData = try XCTUnwrap(userJson.data(using: .utf8))
        let userResponse = try JSONDecoder(.convertFromSnakeCase).decode(UserResponse.self, from: userData)
        userAuthResponse.user = userResponse
        
        let sellerData = try XCTUnwrap(sellerJson.data(using: .utf8))
        let sellerResponse = try JSONDecoder(.convertFromSnakeCase).decode(SellerResponse.self, from: sellerData)
        userAuthResponse.seller = sellerResponse
        
        XCTAssertEqual(userAuthResponse.auth, authResponse)
        XCTAssertEqual(userAuthResponse.user, userResponse)
        XCTAssertEqual(userAuthResponse.seller, sellerResponse)
    }
    
    func testUserAuthEquatable_ShouldAuthAndUserAndSellerBeNil() {
        let userAuthResponse = UserAuthResponse()
        let userAuth2 = UserAuthResponse()
        
        XCTAssertEqual(userAuthResponse, userAuth2)
    }
    
    func testUserAuthEquatable_ShouldUserAndSellerBeNil() throws {
        let userAuthResponse = UserAuthResponse()
        let userAuth2 = UserAuthResponse()
        
        let authData = try XCTUnwrap(authJson.data(using: .utf8))
        let authResponse = try JSONDecoder(.convertFromSnakeCase).decode(AuthResponse.self, from: authData)
        userAuthResponse.auth = authResponse
        userAuth2.auth = authResponse
        
        XCTAssertEqual(userAuthResponse, userAuth2)
    }
    
    func testUserAuthEquatable_ShouldAuthAndSellerBeNil() throws {
        let userAuthResponse = UserAuthResponse()
        let userAuth2 = UserAuthResponse()
        
        let userData = try XCTUnwrap(userJson.data(using: .utf8))
        let userResponse = try JSONDecoder(.convertFromSnakeCase).decode(UserResponse.self, from: userData)
        userAuthResponse.user = userResponse
        userAuth2.user = userResponse
        
        XCTAssertEqual(userAuthResponse, userAuth2)
    }
    
    func testUserAuthEquatable_ShouldAuthAndUserBeNil() throws {
        let userAuthResponse = UserAuthResponse()
        let userAuth2 = UserAuthResponse()
        
        let userData = try XCTUnwrap(userJson.data(using: .utf8))
        let userResponse = try JSONDecoder(.convertFromSnakeCase).decode(UserResponse.self, from: userData)
        userAuthResponse.user = userResponse
        userAuth2.user = userResponse
        
        let sellerData = try XCTUnwrap(sellerJson.data(using: .utf8))
        let sellerResponse = try JSONDecoder(.convertFromSnakeCase).decode(SellerResponse.self, from: sellerData)
        userAuthResponse.seller = sellerResponse
        userAuth2.seller = sellerResponse
        
        XCTAssertEqual(userAuthResponse, userAuth2)
    }
    
    func testUserAuthEquatable() throws {
        let userAuthResponse = UserAuthResponse()
        let userAuth2 = UserAuthResponse()
        
        let authData = try XCTUnwrap(authJson.data(using: .utf8))
        let authResponse = try JSONDecoder(.convertFromSnakeCase).decode(AuthResponse.self, from: authData)
        userAuthResponse.auth = authResponse
        userAuth2.auth = authResponse
        
        let userData = try XCTUnwrap(userJson.data(using: .utf8))
        let userResponse = try JSONDecoder(.convertFromSnakeCase).decode(UserResponse.self, from: userData)
        userAuthResponse.user = userResponse
        userAuth2.user = userResponse
        
        let sellerData = try XCTUnwrap(sellerJson.data(using: .utf8))
        let sellerResponse = try JSONDecoder(.convertFromSnakeCase).decode(SellerResponse.self, from: sellerData)
        userAuthResponse.seller = sellerResponse
        userAuth2.seller = sellerResponse
        
        XCTAssertEqual(userAuthResponse, userAuth2)
    }
    
    func testUserAuthEquatable_ShouldNotBeEqual() throws {
        let userAuthResponse = UserAuthResponse()
        let userAuth2 = UserAuthResponse()
        
        let authData = try XCTUnwrap(authJson.data(using: .utf8))
        let authResponse = try JSONDecoder(.convertFromSnakeCase).decode(AuthResponse.self, from: authData)
        userAuthResponse.auth = authResponse
        userAuth2.auth = authResponse
        
        let userData = try XCTUnwrap(userJson.data(using: .utf8))
        let userResponse = try JSONDecoder(.convertFromSnakeCase).decode(UserResponse.self, from: userData)
        userAuthResponse.user = userResponse
        
        XCTAssertNotEqual(userAuthResponse, userAuth2)
    }
}
