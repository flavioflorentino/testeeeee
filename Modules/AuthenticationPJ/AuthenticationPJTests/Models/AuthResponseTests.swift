import XCTest
@testable import AuthenticationPJ

final class AuthResponseTests: XCTestCase {
    func testAuthResponseDecode_ShouldBiometryBeDeniedAndCompletedFalse() throws {
        let json = """
                {
                 "data": {
                  "accessToken": "1234567890",
                  "tokenType": "tokenType",
                  "expires_in": 123,
                  "refreshToken": "refreshToken",
                  "biometry": "denied",
                  "completed": false
                 }
                }
            """
        
        let data = try XCTUnwrap(json.data(using: .utf8))
        let decode = try JSONDecoder(.convertFromSnakeCase).decode(AuthResponse.self, from: data)
        let timeIntervalSinceNow = 123
        let timeIntervalSince1970 = Date(timeIntervalSinceNow: TimeInterval(timeIntervalSinceNow)).timeIntervalSince1970
        let expiresInt = Int(timeIntervalSince1970)
        let date = Date(timeIntervalSince1970: TimeInterval(expiresInt))
        
        XCTAssertEqual(decode.accessToken, "1234567890")
        XCTAssertEqual(decode.tokenType, "tokenType")
        XCTAssertEqual(decode.expires, date)
        XCTAssertEqual(decode.refreshToken, "refreshToken")
        XCTAssertEqual(decode.biometry, "denied")
        XCTAssertFalse(decode.completed)
    }
    
    func testAuthResponseDecode_ShouldBiometryBeAcceptedAndCompletedTrue() throws {
        let json = """
                {
                 "data": {
                  "accessToken": "1234567890",
                  "tokenType": "tokenType",
                  "expires_in": 123,
                  "refreshToken": "refreshToken"
                 }
                }
            """
        
        let data = try XCTUnwrap(json.data(using: .utf8))
        let decode = try JSONDecoder(.convertFromSnakeCase).decode(AuthResponse.self, from: data)
        let timeIntervalSinceNow = 123
        let timeIntervalSince1970 = Date(timeIntervalSinceNow: TimeInterval(timeIntervalSinceNow)).timeIntervalSince1970
        let expiresInt = Int(timeIntervalSince1970)
        let date = Date(timeIntervalSince1970: TimeInterval(expiresInt))
        
        XCTAssertEqual(decode.accessToken, "1234567890")
        XCTAssertEqual(decode.tokenType, "tokenType")
        XCTAssertEqual(decode.expires, date)
        XCTAssertEqual(decode.refreshToken, "refreshToken")
        XCTAssertEqual(decode.biometry, "accepted")
        XCTAssertTrue(decode.completed)
    }
}
