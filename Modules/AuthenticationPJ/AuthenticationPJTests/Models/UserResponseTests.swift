import XCTest
@testable import AuthenticationPJ

final class UserResponseTests: XCTestCase {
    func testUserResponseDecode() throws {
        let json = """
                {
                 "data": {
                  "id": 1,
                  "cpf": "37.442.929/0001-80",
                  "headOfficeId": 1,
                  "imgUrl": "https://images1.fanpop.com/images/image_uploads/Naruto-Sad-uzumaki-naruto-987855_620_465.jpg",
                  "mobilePhone": "11999999999",
                  "birthDateFormated": "31/03/1998",
                  "username": "username",
                  "super": 1,
                  "role": 1,
                  "email": "teste@picpay.com",
                  "name": "Uzumaki Naruto"
                 }
                }
            """
        
        let data = try XCTUnwrap(json.data(using: .utf8))
        let decode = try JSONDecoder(.convertFromSnakeCase).decode(UserResponse.self, from: data)
        
        XCTAssertEqual(decode.id, 1)
        XCTAssertEqual(decode.cnpj, "37.442.929/0001-80")
        XCTAssertEqual(decode.headOfficeId, 1)
        XCTAssertEqual(decode.imageUrl, "https://images1.fanpop.com/images/image_uploads/Naruto-Sad-uzumaki-naruto-987855_620_465.jpg")
        XCTAssertEqual(decode.mobilePhone, "11999999999")
        XCTAssertEqual(decode.birthDateFormated, "31/03/1998")
        XCTAssertEqual(decode.username, "username")
        XCTAssertEqual(decode.superId, 1)
        XCTAssertEqual(decode.role, 1)
        XCTAssertEqual(decode.email, "teste@picpay.com")
        XCTAssertEqual(decode.name, "Uzumaki Naruto")
    }
}
