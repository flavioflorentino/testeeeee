import XCTest
@testable import AuthenticationPJ

final class SellerResponseTests: XCTestCase {
    func testSellerResponseDecode_ShouldImageUrlBeNil() throws {
        let json = """
                {
                 "data": {
                  "name": "Uzumaki Naruto",
                  "cpfCnpj": "37.442.929/0001-80"
                 }
                }
            """
        
        let data = try XCTUnwrap(json.data(using: .utf8))
        let decode = try JSONDecoder(.convertFromSnakeCase).decode(SellerResponse.self, from: data)
        
        XCTAssertEqual(decode.name, "Uzumaki Naruto")
        XCTAssertEqual(decode.cnpj, "37.442.929/0001-80")
        XCTAssertNil(decode.imageUrl)
    }
    
    func testSellerResponseDecode_ShouldNotImageUrlBeNil() throws {
        let json = """
                {
                 "data": {
                  "name": "Uzumaki Naruto",
                  "cpfCnpj": "37.442.929/0001-80",
                  "imgUrl": "https://images1.fanpop.com/images/image_uploads/Naruto-Sad-uzumaki-naruto-987855_620_465.jpg"
                 }
                }
            """
        
        let data = try XCTUnwrap(json.data(using: .utf8))
        let decode = try JSONDecoder(.convertFromSnakeCase).decode(SellerResponse.self, from: data)
        
        XCTAssertEqual(decode.name, "Uzumaki Naruto")
        XCTAssertEqual(decode.cnpj, "37.442.929/0001-80")
        XCTAssertEqual(decode.imageUrl, "https://images1.fanpop.com/images/image_uploads/Naruto-Sad-uzumaki-naruto-987855_620_465.jpg")
    }
}
