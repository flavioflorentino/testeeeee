import XCTest
@testable import AuthenticationPJ

final class AuthenticationErrorTests: XCTestCase {
    func testAuthenticationDefaultError() throws {
        let json = """
                {
                 "meta": {
                  "code": 1,
                  "errorType": "errorType",
                  "errorMessage": "errorMessage"
                 }
                }
            """
        let data = try XCTUnwrap(json.data(using: .utf8))
        
        let decoder = JSONDecoder()
        let meta = try decoder.decode(AuthenticationDefaultError.self, from: data).meta
        XCTAssertEqual(meta.code, 1)
        XCTAssertEqual(meta.errorType, "errorType")
        XCTAssertEqual(meta.errorMessage, "errorMessage")
    }
}
