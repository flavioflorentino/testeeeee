import XCTest
@testable import AuthenticationPJ

final class AccountTests: XCTestCase {
    private lazy var idAccount = 14_103
    private lazy var documentAcount = "37.442.929/0001-80"
    private lazy var usernameAccount = "Uzumaki Naruto"
    private lazy var companyNameAccount = "Uzumaki Naruto"
    private lazy var descriptionAccount = "Description"
    private lazy var imageUrlAccount = "https://images1.fanpop.com/images/image_uploads/Naruto-Sad-uzumaki-naruto-987855_620_465.jpg"
    private lazy var lastLoginAccount = 629_125_050.397_511
    private lazy var dateAccount = Date(timeIntervalSinceReferenceDate: lastLoginAccount)
    
    func testAccountDecode_WhenTryToDecode_ShouldDecode() throws {
        let json = """
             {
              "id": \(idAccount),
              "document": "\(documentAcount)",
              "username": "\(usernameAccount)",
              "companyName": "\(companyNameAccount)",
              "lastLogin": \(lastLoginAccount),
              "imageUrl": "\(imageUrlAccount)",
              "description": "\(descriptionAccount)"
             }
         """
        
        let data = try XCTUnwrap(json.data(using: .utf8))
        try decode(data: data)
    }

    func testAccountEncode_WhenTryToEncode_ShouldEncode() throws {
        let account = Account(
            id: idAccount,
            document: documentAcount,
            username: usernameAccount,
            companyName: companyNameAccount,
            lastLogin: dateAccount,
            imageUrl: imageUrlAccount,
            description: descriptionAccount
        )
        
        let encoder = JSONEncoder()
        let data = try encoder.encode(account)
        try decode(data: data)
    }
    
    func decode(data: Data) throws {
        let decoder = JSONDecoder()
        let decode = try decoder.decode(Account.self, from: data)
        XCTAssertEqual(decode.document, documentAcount)
        XCTAssertEqual(decode.username, usernameAccount)
        XCTAssertEqual(decode.description, descriptionAccount)
        XCTAssertEqual(decode.imageUrl, imageUrlAccount)
        XCTAssertEqual(decode.lastLogin, dateAccount)
    }
}
