import XCTest
@testable import AuthenticationPJ

private final class AuthenticationDelegateSpy: AuthenticationDelegate {
    private(set) var authenticationNavigation: UINavigationController?
    private(set) var authenticationWantsToDisplayWelcomeCallsCount = 0

    func authenticationWantsToDisplayWelcome(_ navigation: UINavigationController) {
        authenticationWantsToDisplayWelcomeCallsCount += 1
        authenticationNavigation = navigation
    }

    private(set) var authenticationWasCompletedCallsCount = 0
    private(set) var authenticationUserAuthResponse: UserAuthResponse?

    func authenticationWasCompleted(_ navigation: UINavigationController, userAuthResponse: UserAuthResponse) {
        authenticationWasCompletedCallsCount += 1
        authenticationNavigation = navigation
        authenticationUserAuthResponse = userAuthResponse
    }

    private(set) var authenticationDidTapFAQCallsCount = 0

    func authenticationDidTapFAQ(_ navigation: UINavigationController) {
        authenticationDidTapFAQCallsCount += 1
        authenticationNavigation = navigation
    }

    private(set) var authenticationDidTapForgotPasswordCallsCount = 0

    func authenticationDidTapForgotPassword(_ navigation: UINavigationController) {
        authenticationDidTapForgotPasswordCallsCount += 1
        authenticationNavigation = navigation
    }

    private(set) var authenticationDidTapRegisterCallsCount = 0

    func authenticationDidTapRegister(_ navigation: UINavigationController) {
        authenticationDidTapRegisterCallsCount += 1
        authenticationNavigation = navigation
    }
}

private final class AccountEditingDelegateSpy: AccountEditingDelegate {
    private(set) var accountWasEditedCallsCount = 0
    private(set) var account: Account?

    func accountWasEdited(account: Account) {
        accountWasEditedCallsCount += 1
        self.account = account
    }

    private(set) var accountWasRemovedCallsCount = 0

    func accountWasRemoved() {
        accountWasRemovedCallsCount += 1
    }
}

final class AuthenticationCoordinatorTests: XCTestCase {
    private let navigationSpy = NavigationControllerSpy()
    private let accountEditingDelegateSpy = AccountEditingDelegateSpy()
    private let delegateSpy = AuthenticationDelegateSpy()
    private let workerMock = AuthenticationWorkerMock()
    private lazy var sut: AuthenticationCoordinating = AuthenticationCoordinator(
        from: navigationSpy,
        delegate: delegateSpy,
        dependencies: DependencyContainerMock(workerMock)
    )

    func testStart_WhenReceiveStartFromCoordinator_ShouldDisplaySavedAccountViewController() {
        let account = AuthenticationMock.Account.full
        let accounts = [account]
        workerMock.retrieveAccountsResult = accounts
        sut.start()

        XCTAssert(navigationSpy.viewControllers.first is SavedAccountViewController)
    }

    func testLogin_WhenReceiveLoginAddingAccountFromCoordinator_ShouldPushFirstAccessViewController() {
        let account = AuthenticationMock.Account.full
        let accounts = [account]
        workerMock.retrieveAccountsResult = accounts
        sut.login(isAddingAccount: true)

        XCTAssertEqual(navigationSpy.pushViewControllerCallsCount, 1)
        XCTAssert(navigationSpy.pushViewController is FirstAccessViewController)
    }

    func testLogin_WhenReceiveLoginAddingAccountFromCoordinator_ShouldPushAccountManagementViewController() {
        let account = AuthenticationMock.Account.full
        let accounts = [account, account]
        workerMock.retrieveAccountsResult = accounts
        sut.login(isAddingAccount: true)

        XCTAssertEqual(navigationSpy.pushViewControllerCallsCount, 1)
        XCTAssert(navigationSpy.pushViewController is AccountManagementViewController)
    }

    func testLogin_WhenReceiveLoginFromCoordinator_ShouldPushFirstAccessViewController() {
        workerMock.retrieveAccountsResult = []
        sut.login(isAddingAccount: false)

        XCTAssertEqual(navigationSpy.pushViewControllerCallsCount, 1)
        XCTAssert(navigationSpy.pushViewController is FirstAccessViewController)
    }

    func testLogin_WhenReceiveLoginFromCoordinator_ShouldPushAccountManagementViewController() {
        let account = AuthenticationMock.Account.full
        let accounts = [account]
        workerMock.retrieveAccountsResult = accounts
        sut.login(isAddingAccount: false)

        XCTAssertEqual(navigationSpy.pushViewControllerCallsCount, 1)
        XCTAssert(navigationSpy.pushViewController is SavedAccountViewController)
    }

    func testSavedAccount_WhenReceiveSavedAccountFromCoordinator_ShouldPushSavedAccountViewController() {
        let account = AuthenticationMock.Account.full
        sut.savedAccount(account)

        XCTAssertEqual(navigationSpy.pushViewControllerCallsCount, 1)
        XCTAssert(navigationSpy.pushViewController is SavedAccountViewController)
    }

    func testAccountManagement_WhenReceiveAccountManagementFromCoordinator_ShouldPushAccountManagementViewController() {
        sut.accountManagement()

        XCTAssertEqual(navigationSpy.pushViewControllerCallsCount, 1)
        XCTAssert(navigationSpy.pushViewController is AccountManagementViewController)
    }

    func testAccountEditing_WhenReceiveAccountEditingFromCoordinator_ShouldPresentAccountEditingViewController() {
        let account = AuthenticationMock.Account.full
        sut.accountEditing(account, hasDeleteButton: false, delegate: accountEditingDelegateSpy)

        XCTAssertEqual(navigationSpy.presentCallsCount, 1)
        XCTAssert(navigationSpy.presentViewController is AccountEditingViewController)
    }

    func testFirstAccess_WhenReceiveFirstAccessFromCoordinator_ShouldPushAccountManagementViewController() {
        sut.firstAccess()

        XCTAssertEqual(navigationSpy.pushViewControllerCallsCount, 1)
        XCTAssert(navigationSpy.pushViewController is FirstAccessViewController)
    }

    func testComplete_WhenReceiveCompleteFromCoordinator_ShouldCallDelegateAuthenticationWasCompleted() {
        let userAuthResponse = UserAuthResponse()
        sut.complete(with: userAuthResponse)

        XCTAssertEqual(delegateSpy.authenticationWasCompletedCallsCount, 1)
        XCTAssertEqual(delegateSpy.authenticationNavigation, navigationSpy)
        XCTAssertEqual(delegateSpy.authenticationUserAuthResponse, userAuthResponse)
    }

    func testClose_WhenReceiveCloseFromCoordinator_ShouldCallPopToRoot() {
        sut.close()

        XCTAssertEqual(navigationSpy.popToRootViewControllerCallsCount, 1)
    }

    func testFaq_WhenReceiveFaqFromCoordinator_ShouldCallDelegateAuthenticationDidTapFAQ() {
        sut.faq()

        XCTAssertEqual(delegateSpy.authenticationDidTapFAQCallsCount, 1)
        XCTAssertEqual(delegateSpy.authenticationNavigation, navigationSpy)
    }

    func testForgotPassword_WhenReceiveForgotPasswordFromCoordinator_ShouldCallDelegateAuthenticationDidTapForgotPassword() {
        sut.forgotPassword()

        XCTAssertEqual(delegateSpy.authenticationDidTapForgotPasswordCallsCount, 1)
        XCTAssertEqual(delegateSpy.authenticationNavigation, navigationSpy)
    }

    func testRegister_WhenReceiveRegisterFromCoordinator_ShouldCallDelegateAuthenticationDidTapRegister() {
        sut.register()

        XCTAssertEqual(delegateSpy.authenticationDidTapRegisterCallsCount, 1)
        XCTAssertEqual(delegateSpy.authenticationNavigation, navigationSpy)
    }
}
