import Foundation
import XCTest
@testable import AuthenticationPJ

final class AuthenticationWorkerMock: AuthenticationWorking {
    var authenticationResult: Result<UserAuthResponse, AuthenticationError>?
    private(set) var authenticationDocument: String?
    private(set) var authenticationUsername: String?
    private(set) var authenticationPassword: String?
    // swiftlint:disable:next discouraged_optional_boolean
    private(set) var authenticationMustRemind: Bool?
    
    func authentication(document: String, username: String, password: String, mustRemind: Bool, completion: @escaping (Result<UserAuthResponse, AuthenticationError>) -> Void) {
        guard let authenticationResult = authenticationResult else {
            XCTFail("No authenticationResult provided")
            return
        }
        authenticationDocument = document
        authenticationUsername = username
        authenticationPassword = password
        authenticationMustRemind = mustRemind
        completion(authenticationResult)
    }
    
    // swiftlint:disable:next discouraged_optional_collection
    var retrieveAccountsResult: [Account]?
    
    func retrieveAccounts(completion: @escaping ([Account]) -> Void) {
        guard let accountsResult = retrieveAccountsResult else {
            XCTFail("No accountsResult provided")
            return
        }
        completion(accountsResult)
    }
    
    private(set) var updateAccount: Account?
    
    func updateAccount(_ account: Account) {
        updateAccount = account
    }
    
    private(set) var removeAccount: Account?
    
    func removeAccount(_ account: Account) {
        removeAccount = account
    }

    func removeAccounts() { }
}
