import Foundation
import XCTest
@testable import AuthenticationPJ

final class CollectionExtensionsTests: XCTestCase {
    func testDigitCNPJ() {
        // CNPJ - 37.442.929/0001-80
        let firstCNPJ = [3, 7, 4, 4, 2, 9, 2, 9, 0, 0, 0, 1, 8, 0]
        XCTAssertEqual(firstCNPJ.prefix(12).digitCNPJ, 8)
        XCTAssertEqual(firstCNPJ.prefix(13).digitCNPJ, 0)
        
        // CNPJ - 39.808.425/0001-67
        let secondCNPJ = [3, 9, 8, 0, 8, 4, 2, 5, 0, 0, 0, 1, 6, 7]
        XCTAssertEqual(secondCNPJ.prefix(12).digitCNPJ, 6)
        XCTAssertEqual(secondCNPJ.prefix(13).digitCNPJ, 7)
        
        // CNPJ - 67.185.683/0001-70
        let thirdCNPJ = [6, 7, 1, 8, 5, 6, 8, 3, 0, 0, 0, 1, 7, 0]
        XCTAssertEqual(thirdCNPJ.prefix(12).digitCNPJ, 7)
        XCTAssertEqual(thirdCNPJ.prefix(13).digitCNPJ, 0)
        
        // CNPJ - 61.745.514/0001-60
        let fourthCNPJ = [6, 1, 7, 4, 5, 5, 1, 4, 0, 0, 0, 1, 6, 0]
        XCTAssertEqual(fourthCNPJ.prefix(12).digitCNPJ, 6)
        XCTAssertEqual(fourthCNPJ.prefix(13).digitCNPJ, 0)
        
        // CNPJ - 23.326.095/0001-33
        let fifthCNPJ = [2, 3, 3, 2, 6, 0, 9, 5, 0, 0, 0, 1, 3, 3]
        XCTAssertEqual(fifthCNPJ.prefix(12).digitCNPJ, 3)
        XCTAssertEqual(fifthCNPJ.prefix(13).digitCNPJ, 3)
        
        // CNPJ - 89.834.596/0001-39
        let sixthCNPJ = [8, 9, 8, 3, 4, 5, 9, 6, 0, 0, 0, 1, 3, 9]
        XCTAssertEqual(sixthCNPJ.prefix(12).digitCNPJ, 3)
        XCTAssertEqual(sixthCNPJ.prefix(13).digitCNPJ, 9)
    }
}
