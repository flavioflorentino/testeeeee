import Foundation
import XCTest
@testable import AuthenticationPJ

final class StringsExtensionsTests: XCTestCase {
    func testIsValidCNPJ_ShouldBeAllValidDocuments() {
        // CNPJ - 37.442.929/0001-80
        let firstCNPJ = "37.442.929/0001-80"
        XCTAssertTrue(firstCNPJ.isValidCNPJ)
        
        // CNPJ - 39.808.425/0001-67
        let secondCNPJ = "39.808.425/0001-67"
        XCTAssertTrue(secondCNPJ.isValidCNPJ)
        
        // CNPJ - 67.185.683/0001-70
        let thirdCNPJ = "67.185.683/0001-70"
        XCTAssertTrue(thirdCNPJ.isValidCNPJ)
        
        // CNPJ - 61.745.514/0001-60
        let fourthCNPJ = "61.745.514/0001-60"
        XCTAssertTrue(fourthCNPJ.isValidCNPJ)
        
        // CNPJ - 23.326.095/0001-33
        let fifthCNPJ = "23.326.095/0001-33"
        XCTAssertTrue(fifthCNPJ.isValidCNPJ)
        
        // CNPJ - 89.834.596/0001-39
        let sixthCNPJ = "89.834.596/0001-39"
        XCTAssertTrue(sixthCNPJ.isValidCNPJ)
    }
    
    func testIsValidCNPJ_ShouldBeAllInvalidDocuments() {
        // CNPJ - 11.111.111/1111-11
        let firstCNPJ = "11.111.111/1111-11"
        XCTAssertFalse(firstCNPJ.isValidCNPJ)
        
        // CNPJ - 00.000.000/0000-0
        let secondCNPJ = "00.000.000/0000-0"
        XCTAssertFalse(secondCNPJ.isValidCNPJ)
        
        // CNPJ - 00.000.000/0000
        let thirdCNPJ = "00.000.000/0000"
        XCTAssertFalse(thirdCNPJ.isValidCNPJ)
        
        // CNPJ - 0.000.000/0000-00
        let fourthCNPJ = "0.000.000/0000-00"
        XCTAssertFalse(fourthCNPJ.isValidCNPJ)
        
        // CNPJ - 00.00.000/0000-00
        let fifthCNPJ = "00.00.000/0000-00"
        XCTAssertFalse(fifthCNPJ.isValidCNPJ)
        
        // CNPJ - 00.000.00/0000-00
        let sixthCNPJ = "00.000.00/0000-00"
        XCTAssertFalse(sixthCNPJ.isValidCNPJ)
        
        // CNPJ - 00.000.000/000-00
        let seventhCNPJ = "00.000.000/000-00"
        XCTAssertFalse(seventhCNPJ.isValidCNPJ)
        
        // CNPJ - 00.000
        let eighthCNPJ = "00.000"
        XCTAssertFalse(eighthCNPJ.isValidCNPJ)
        
        // CNPJ - 00
        let ninthCNPJ = "00"
        XCTAssertFalse(ninthCNPJ.isValidCNPJ)
        
        // CNPJ -
        let tenthCNPJ = ""
        XCTAssertFalse(tenthCNPJ.isValidCNPJ)
    }
    
    func testOnlyNumbers() {
        // CNPJ - 37.442.929/0001-80
        let firstCNPJ = "37.442.929/0001-80"
        XCTAssertEqual(firstCNPJ.onlyNumbers, "37442929000180")
        
        // CNPJ - 39.808.425/0001-67
        let secondCNPJ = "39.808.425/0001-67"
        XCTAssertEqual(secondCNPJ.onlyNumbers, "39808425000167")
        
        // CNPJ - 67.185.683/0001-70
        let thirdCNPJ = "67.185.683/0001-70"
        XCTAssertEqual(thirdCNPJ.onlyNumbers, "67185683000170")
        
        // CNPJ - 61.745.514/0001-60
        let fourthCNPJ = "61.745.514/0001-60"
        XCTAssertEqual(fourthCNPJ.onlyNumbers, "61745514000160")
        
        // CNPJ - 23.326.095/0001-33
        let fifthCNPJ = "23.326.095/0001-33"
        XCTAssertEqual(fifthCNPJ.onlyNumbers, "23326095000133")
        
        // CNPJ - 89.834.596/0001-39
        let sixthCNPJ = "89.834.596/0001-39"
        XCTAssertEqual(sixthCNPJ.onlyNumbers, "89834596000139")
    }
}
