import Core
import Foundation
import XCTest
@testable import AuthenticationPJ

final class RequestErrorExtensionsTests: XCTestCase {
    func testHandleBadRequest_WhenReceiveABadRequestError_ShouldReturnCode() throws {
        let json = """
            {
             "meta": {
                 "code": 403,
                 "errorType": "",
                 "errorMessage": "mensagem de erro"
             }
            }
        """
        let data = try XCTUnwrap(json.data(using: .utf8))
        let requestError = RequestError.initialize(with: data)
        let response = requestError.handleBadRequest()
        XCTAssertEqual(response, "mensagem de erro")
    }
    
    func testHandleBadRequest_WhenReceiveABadRequestError_ShouldReturnNil() throws {
        let json = """
            {
             "meta": {
                 "code": 403,
                 "errorType": ""
             }
            }
        """
        let data = try XCTUnwrap(json.data(using: .utf8))
        let requestError = RequestError.initialize(with: data)
        let response = requestError.handleBadRequest()
        XCTAssertNil(response)
    }
}
