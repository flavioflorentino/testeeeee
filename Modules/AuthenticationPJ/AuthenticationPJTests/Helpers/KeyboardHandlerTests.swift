import Foundation
import XCTest
@testable import AuthenticationPJ

final class KeyboardHandlerTests: XCTestCase {
    private lazy var notificationCenter = NotificationCenter.default
    private lazy var keyboardHandler = KeyboardHandler()
    
    private let userInfoKeyboardSize: [AnyHashable: Any] = [
        UIResponder.keyboardFrameEndUserInfoKey: NSValue(cgRect: .zero)
    ]
    private let userInfo: [AnyHashable: Any] = [
        UIResponder.keyboardFrameEndUserInfoKey: NSValue(cgRect: .zero),
        UIResponder.keyboardAnimationDurationUserInfoKey: NSNumber(0.0),
        UIResponder.keyboardAnimationCurveUserInfoKey: NSNumber(0)
    ]
    
    private var keyboardCallsCount = 0
    
    override func setUp() {
        super.setUp()
        keyboardHandler.handle { _ in
            self.keyboardCallsCount += 1
        }
    }
    
    func testKeyboardWillShowNotification() {
        let notificationExpectation = XCTNSNotificationExpectation(name: UIWindow.keyboardWillShowNotification,
                                                                   object: nil,
                                                                   notificationCenter: notificationCenter)
        notificationCenter.post(name: UIWindow.keyboardWillShowNotification, object: self, userInfo: userInfo)
        wait(for: [notificationExpectation], timeout: 0.1)
        XCTAssertEqual(keyboardCallsCount, 1)
    }
    
    func testKeyboardWillHideNotification() {
        let notificationExpectation = XCTNSNotificationExpectation(name: UIWindow.keyboardWillHideNotification,
                                                                   object: nil,
                                                                   notificationCenter: notificationCenter)
        notificationCenter.post(name: UIWindow.keyboardWillHideNotification, object: self, userInfo: userInfo)
        wait(for: [notificationExpectation], timeout: 0.1)
        XCTAssertEqual(keyboardCallsCount, 1)
    }
    
    func testKeyboardWillShowNotification_ShouldReturn() {
        let notificationExpectation = XCTNSNotificationExpectation(name: UIWindow.keyboardWillShowNotification,
                                                                   object: nil,
                                                                   notificationCenter: notificationCenter)
        notificationCenter.post(name: UIWindow.keyboardWillShowNotification, object: self, userInfo: nil)
        wait(for: [notificationExpectation], timeout: 0.1)
        XCTAssertEqual(keyboardCallsCount, 0)
    }
    
    func testKeyboardWillHideNotification_ShouldReturn() {
        let notificationExpectation = XCTNSNotificationExpectation(name: UIWindow.keyboardWillHideNotification,
                                                                   object: nil,
                                                                   notificationCenter: notificationCenter)
        notificationCenter.post(name: UIWindow.keyboardWillHideNotification, object: self, userInfo: nil)
        wait(for: [notificationExpectation], timeout: 0.1)
        XCTAssertEqual(keyboardCallsCount, 0)
    }
    
    func testKeyboardWillShowNotification_ShouldReturnWhenTryRetrieveKeyboardSizes() {
        let notificationExpectation = XCTNSNotificationExpectation(name: UIWindow.keyboardWillShowNotification,
                                                                   object: nil,
                                                                   notificationCenter: notificationCenter)
        notificationCenter.post(name: UIWindow.keyboardWillShowNotification, object: self, userInfo: userInfoKeyboardSize)
        wait(for: [notificationExpectation], timeout: 0.1)
        XCTAssertEqual(keyboardCallsCount, 0)
    }
    
    func testKeyboardWillHideNotification_ShouldReturnWhenTryRetrieveKeyboardSizes() {
        let notificationExpectation = XCTNSNotificationExpectation(name: UIWindow.keyboardWillHideNotification,
                                                                   object: nil,
                                                                   notificationCenter: notificationCenter)
        notificationCenter.post(name: UIWindow.keyboardWillHideNotification, object: self, userInfo: userInfoKeyboardSize)
        wait(for: [notificationExpectation], timeout: 0.1)
        XCTAssertEqual(keyboardCallsCount, 0)
    }
}
