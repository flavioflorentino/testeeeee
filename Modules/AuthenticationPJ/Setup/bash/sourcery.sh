#!/bin/sh

#Verify if the AUTHENTICATION_PJ_API_CLIENT_ID and AUTHENTICATION_PJ_API_CLIENT_SECRET exist and are not empty.
if [ -n "${AUTHENTICATION_PJ_API_CLIENT_ID}" ] && [ -n "${AUTHENTICATION_PJ_API_CLIENT_SECRET}" ]; then
     #Create the credentials with the keys of the authentication request release environment
     echo "Gerando credenciais do request de autenticação para ambiente de Produção"
     sourcery --config Setup/Sourcery/sourcery-prod.yml
 else
     #Create the credentials with the keys of the authentication request sandbox environment
     echo "Gerando credenciais do request de autenticação para ambiente de Desenvolvimento"
     sourcery --config Setup/Sourcery/sourcery-develop.yml
fi