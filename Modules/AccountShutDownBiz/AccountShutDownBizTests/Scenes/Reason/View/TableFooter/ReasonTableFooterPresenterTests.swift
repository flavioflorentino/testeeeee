import Foundation
import UI
import XCTest
@testable import AccountShutDownBiz

private class ReasonTableFooterDisplayingSpy: ReasonTableFooterDisplaying {
    // MARK: - displayTextCount
    private(set) var displayTextCountCallsCount = 0
    private(set) var displayTextCountReceivedInvocations: [String] = []

    func displayTextCount(_ text: String) {
        displayTextCountCallsCount += 1
        displayTextCountReceivedInvocations.append(text)
    }

    // MARK: - setLineColor
    private(set) var setTextViewStatusIsValidCount = 0
    private(set) var setTextViewStatusIsValidReceivedInvocations: [Bool] = []

    func setTextViewStatus(isValid: Bool) {
        setTextViewStatusIsValidCount += 1
        setTextViewStatusIsValidReceivedInvocations.append(isValid)
    }

    // MARK: - setButtonStatus
    private(set) var setButtonStatusIsEnabledCallsCount = 0
    private(set) var setButtonStatusIsEnabledReceivedInvocations: [Bool] = []

    func setButtonStatus(isEnabled: Bool) {
        setButtonStatusIsEnabledCallsCount += 1
        setButtonStatusIsEnabledReceivedInvocations.append(isEnabled)
    }
}

final class ReasonTableFooterPresenterTests: XCTestCase {
    private lazy var viewController = ReasonTableFooterDisplayingSpy()
    private lazy var sut: ReasonTableFooterPresenter = {
        let presenter = ReasonTableFooterPresenter()
        presenter.viewController = viewController
        return presenter
    }()
    
    func testPresentTextCount_ShouldDisplayTextCount() throws {
        let count = 10
        let countText = "\(count)/1000"
        sut.presentTextCount(count)
        
        XCTAssertEqual(viewController.displayTextCountCallsCount, 1)
        let presentedCountText = try XCTUnwrap(viewController.displayTextCountReceivedInvocations.first)
        XCTAssertEqual(presentedCountText, countText)
    }
    
    func testSetTextIsValid_WhenValid_ShouldSetValidTextViewStatusAndButton() throws {
        sut.setTextIsValid(true)
        
        XCTAssertEqual(viewController.setTextViewStatusIsValidCount, 1)
        let invocation = try XCTUnwrap(viewController.setTextViewStatusIsValidReceivedInvocations.first)
        XCTAssertTrue(invocation)
        
        XCTAssertEqual(viewController.setButtonStatusIsEnabledCallsCount, 1)
        let buttonInvocation = try XCTUnwrap(viewController.setButtonStatusIsEnabledReceivedInvocations.first)
        XCTAssertTrue(buttonInvocation)
    }
    
    func testSetTextIsValid_WhenNotValid_ShouldSetValidTextViewStatusAndButton() throws {
        sut.setTextIsValid(false)
        
        XCTAssertEqual(viewController.setTextViewStatusIsValidCount, 1)
        let invocation = try XCTUnwrap(viewController.setTextViewStatusIsValidReceivedInvocations.first)
        XCTAssertFalse(invocation)
        
        XCTAssertEqual(viewController.setButtonStatusIsEnabledCallsCount, 1)
        let buttonInvocation = try XCTUnwrap(viewController.setButtonStatusIsEnabledReceivedInvocations.first)
        XCTAssertFalse(buttonInvocation)
    }
    
    func testSetButtonStatus_WhenValid_ShouldSetValidButtonStatus() throws {
        sut.setButtonStatus(isEnabled: true)
        
        XCTAssertEqual(viewController.setButtonStatusIsEnabledCallsCount, 1)
        let buttonInvocation = try XCTUnwrap(viewController.setButtonStatusIsEnabledReceivedInvocations.first)
        XCTAssertTrue(buttonInvocation)
    }
    
    func testSetButtonStatus_WhenInvalid_ShouldSetValidButtonStatus() throws {
        sut.setButtonStatus(isEnabled: false)
        
        XCTAssertEqual(viewController.setButtonStatusIsEnabledCallsCount, 1)
        let buttonInvocation = try XCTUnwrap(viewController.setButtonStatusIsEnabledReceivedInvocations.first)
        XCTAssertFalse(buttonInvocation)
    }
}
