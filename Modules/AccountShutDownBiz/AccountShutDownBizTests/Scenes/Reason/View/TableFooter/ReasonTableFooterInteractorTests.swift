import Foundation
import XCTest
@testable import AccountShutDownBiz

private class ReasonTableFooterInteractorDelegateSpy: ReasonTableFooterInteractorDelegate {
    // MARK: - ContinueAction
    private(set) var continueActionTextCallsCount = 0
    private(set) var continueActionTextReceivedInvocations: [String?] = []

    func continueAction(text: String?) {
        continueActionTextCallsCount += 1
        continueActionTextReceivedInvocations.append(text)
    }
}

private class ReasonTableFooterPresentingSpy: ReasonTableFooterPresenting {
    var viewController: ReasonTableFooterDisplaying?

    // MARK: - presentTextCount
    private(set) var presentTextCountCallsCount = 0
    private(set) var presentTextCountReceivedInvocations: [Int] = []

    func presentTextCount(_ count: Int) {
        presentTextCountCallsCount += 1
        presentTextCountReceivedInvocations.append(count)
    }

    // MARK: - setTextIsValid
    private(set) var setTextIsValidCallsCount = 0
    private(set) var setTextIsValidReceivedInvocations: [Bool] = []

    func setTextIsValid(_ isValid: Bool) {
        setTextIsValidCallsCount += 1
        setTextIsValidReceivedInvocations.append(isValid)
    }

    // MARK: - setButtonStatus
    private(set) var setButtonStatusIsEnabledCallsCount = 0
    private(set) var setButtonStatusIsEnabledReceivedInvocations: [Bool] = []

    func setButtonStatus(isEnabled: Bool) {
        setButtonStatusIsEnabledCallsCount += 1
        setButtonStatusIsEnabledReceivedInvocations.append(isEnabled)
    }
}

final class ReasonTableFooterInteractorTests: XCTestCase {
    // swiftlint:disable:next weak_delegate
    private lazy var delegate = ReasonTableFooterInteractorDelegateSpy()
    private lazy var presenter = ReasonTableFooterPresentingSpy()
    private lazy var sut: ReasonTableFooterInteractor = {
        let interactor = ReasonTableFooterInteractor(presenter: presenter)
        interactor.delegate = delegate
        return interactor
    }()
    
    func testUpdateText_ShouldPresentTextCount() throws {
        let text = "Carlsen"
        sut.updateText(text)
        
        XCTAssertEqual(presenter.presentTextCountCallsCount, 1)
        let presentedText = try XCTUnwrap(presenter.presentTextCountReceivedInvocations.first)
        XCTAssertEqual(presentedText, text.count)
    }
    
    func testUpdateText_WhenMoreThan24Characteres_ShouldEnableButtonAndText() throws {
        let text = String(repeating: "1", count: 25)
        sut.updateText(text)
        
        XCTAssertEqual(presenter.setButtonStatusIsEnabledCallsCount, 1)
        let isButtonEnabled = try XCTUnwrap(presenter.setButtonStatusIsEnabledReceivedInvocations.first)
        XCTAssertTrue(isButtonEnabled)
        
        XCTAssertEqual(presenter.setTextIsValidCallsCount, 1)
        let isTextEnabled = try XCTUnwrap(presenter.setTextIsValidReceivedInvocations.first)
        XCTAssertTrue(isTextEnabled)
    }
    
    func testUpdateText_WhenLessThan25Characteres_ShouldDisableButtonAndText() throws {
        let text = String(repeating: "1", count: 24)
        sut.updateText(text)
        
        XCTAssertEqual(presenter.setButtonStatusIsEnabledCallsCount, 1)
        let isButtonEnabled = try XCTUnwrap(presenter.setButtonStatusIsEnabledReceivedInvocations.first)
        XCTAssertFalse(isButtonEnabled)
        
        XCTAssertEqual(presenter.setTextIsValidCallsCount, 1)
        let isTextEnabled = try XCTUnwrap(presenter.setTextIsValidReceivedInvocations.first)
        XCTAssertFalse(isTextEnabled)
    }
    
    func testUpdateText_WhenNoText_ShouldSetTextEnabled() throws {
        sut.updateText("")
        
        XCTAssertEqual(presenter.setTextIsValidCallsCount, 1)
        let isEnabled = try XCTUnwrap(presenter.setTextIsValidReceivedInvocations.first)
        XCTAssertTrue(isEnabled)
    }
    
    func testUpdateText_WhenNoTextAndNoReason_ShouldDisableButton() throws {
        sut.updateText("")
        
        XCTAssertEqual(presenter.setButtonStatusIsEnabledCallsCount, 1)
        let isEnabled = try XCTUnwrap(presenter.setButtonStatusIsEnabledReceivedInvocations.first)
        XCTAssertFalse(isEnabled)
    }
    
    func testUpdateText_WhenNoTextAndHasReason_ShouldDisableButton() throws {
        sut.updateButton(hasReason: true)
        
        sut.updateText("")
        
        XCTAssertEqual(presenter.setButtonStatusIsEnabledCallsCount, 2)
        let isEnabled = try XCTUnwrap(presenter.setButtonStatusIsEnabledReceivedInvocations.last)
        XCTAssertTrue(isEnabled)
    }
    
    func testUpdateButton_WhenHasReasonAndTextHasMoreThan24Characteres_ShouldEnableButtonAndText() throws {
        let text = String(repeating: "1", count: 25)
        sut.updateText(text)
        
        sut.updateButton(hasReason: true)
        
        XCTAssertEqual(presenter.setTextIsValidCallsCount, 2)
        let isTextEnabled = try XCTUnwrap(presenter.setTextIsValidReceivedInvocations.last)
        XCTAssertTrue(isTextEnabled)
        
        XCTAssertEqual(presenter.setButtonStatusIsEnabledCallsCount, 2)
        let isButtonEnabled = try XCTUnwrap(presenter.setButtonStatusIsEnabledReceivedInvocations.last)
        XCTAssertTrue(isButtonEnabled)
    }
    
    func testUpdateButton_WhenHasReasonAndTextHasLessThan25Characteres_ShouldDisableButtonAndDisableText() throws {
        let text = String(repeating: "1", count: 24)
        sut.updateText(text)
        
        sut.updateButton(hasReason: true)
        
        XCTAssertEqual(presenter.setTextIsValidCallsCount, 2)
        let isTextEnabled = try XCTUnwrap(presenter.setTextIsValidReceivedInvocations.last)
        XCTAssertFalse(isTextEnabled)
        
        XCTAssertEqual(presenter.setButtonStatusIsEnabledCallsCount, 2)
        let isButtonEnabled = try XCTUnwrap(presenter.setButtonStatusIsEnabledReceivedInvocations.last)
        XCTAssertFalse(isButtonEnabled)
    }
    
    func testUpdateButton_WhenHasReasonAndNoText_ShouldEnableButtonAndText() throws {
        sut.updateButton(hasReason: true)
        
        XCTAssertEqual(presenter.setTextIsValidCallsCount, 1)
        let isTextEnabled = try XCTUnwrap(presenter.setTextIsValidReceivedInvocations.first)
        XCTAssertTrue(isTextEnabled)
        
        XCTAssertEqual(presenter.setButtonStatusIsEnabledCallsCount, 1)
        let isButtonEnabled = try XCTUnwrap(presenter.setButtonStatusIsEnabledReceivedInvocations.first)
        XCTAssertTrue(isButtonEnabled)
    }
    
    func testContinueAction_ShouldCallContinueAction() throws {
        let text = "Caruana"
        sut.updateText(text)
        
        sut.continueAction()
        
        XCTAssertEqual(delegate.continueActionTextCallsCount, 1)
        let continueText = try XCTUnwrap(delegate.continueActionTextReceivedInvocations.first)
        XCTAssertEqual(continueText, text)
    }
}
