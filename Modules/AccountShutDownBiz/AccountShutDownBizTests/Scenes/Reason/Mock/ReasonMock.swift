import Foundation
@testable import AccountShutDownBiz

final class ReasonMock {
    func fetchReasons() -> [ShutDownReason] {
        LocalMock<[ShutDownReason]>().pureLocalMock("shutdown_reason_list_tests") ?? []
    }
}
