import Foundation
import XCTest
@testable import AccountShutDownBiz

private class ReasonButtonHandlerSpy: ReasonButtonHandler {
    // MARK: - updateButton
    private(set) var updateButtonHasReasonCallsCount = 0
    private(set) var updateButtonHasReasonReceivedInvocations: [Bool] = []

    func updateButton(hasReason: Bool) {
        updateButtonHasReasonCallsCount += 1
        updateButtonHasReasonReceivedInvocations.append(hasReason)
    }
}

private class ReasonServicingFake: ReasonServicing {
    var fetchReasonResult: Result<[ShutDownReason], Error> = .success([])
    func fetchReasons(completion: @escaping(Result<[ShutDownReason], Error>) -> Void) {
        completion(fetchReasonResult)
    }
}

private class ReasonPresentingSpy: ReasonPresenting {
    var viewController: ReasonDisplaying?

    // MARK: - presentReasons
    var didCallPresentReasons: () -> Void = { }
    private(set) var presentReasonsCallsCount = 0
    private(set) var presentReasonsReceivedInvocations: [[ShutDownReason]] = []

    func presentReasons(_ reasons: [ShutDownReason]) {
        presentReasonsCallsCount += 1
        presentReasonsReceivedInvocations.append(reasons)
        didCallPresentReasons()
    }

    // MARK: - didNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didNextStepActionReceivedInvocations: [ReasonAction] = []

    func didNextStep(action: ReasonAction) {
        didNextStepActionCallsCount += 1
        didNextStepActionReceivedInvocations.append(action)
    }
}

final class ReasonInteractorTests: XCTestCase {
    private lazy var buttonHandlerSpy = ReasonButtonHandlerSpy()
    private lazy var service = ReasonServicingFake()
    private lazy var presenter = ReasonPresentingSpy()
    private lazy var dependencies = DependencyContainer()
    private let timeout: TimeInterval = 1
    
    private lazy var sut: ReasonInteractor = {
        let interactor = ReasonInteractor(
            service: service,
            presenter: presenter,
            dependencies: dependencies
        )
        interactor.setButtonHandler(buttonHandlerSpy)
        return interactor
    }()
    
    func testLoadReasons_ShouldLoadReasons() throws {
        let reasons = try XCTUnwrap(ReasonMock().fetchReasons())
        service.fetchReasonResult = .success(reasons)
        
        loadReasonsAndWait()
        
        XCTAssertEqual(presenter.presentReasonsCallsCount, 1)
        let presentedReasons = try XCTUnwrap(presenter.presentReasonsReceivedInvocations.first)
        XCTAssertEqual(presentedReasons.count, reasons.count)
    }
    
    func testSetSelectedReason_WhenNotNil_ShouldUpdateFooterWithReason() throws {
        sut.setSelectedReason(index: 1)
        
        XCTAssertEqual(buttonHandlerSpy.updateButtonHasReasonCallsCount, 1)
        let footerHasReasons = try XCTUnwrap(buttonHandlerSpy.updateButtonHasReasonReceivedInvocations.first)
        XCTAssertTrue(footerHasReasons)
    }
    
    func testSetSelectedReason_WhenNil_ShouldUpdateFooterWithNoReason() throws {
        sut.setSelectedReason(index: nil)
        
        XCTAssertEqual(buttonHandlerSpy.updateButtonHasReasonCallsCount, 1)
        let footerHasReasons = try XCTUnwrap(buttonHandlerSpy.updateButtonHasReasonReceivedInvocations.first)
        XCTAssertFalse(footerHasReasons)
    }
    
    func testContinueAction_WhenHasReason_ShouldConfirmWithReason() throws {
        let reasons = try XCTUnwrap(ReasonMock().fetchReasons())
        let firstReason = try XCTUnwrap(reasons.first)
        let text = "Karpov"
        service.fetchReasonResult = .success([firstReason])
        
        loadReasonsAndWait()
        sut.setSelectedReason(index: 0)
        sut.continueAction(text: text)
        
        let action = ReasonAction.confirm(reason: firstReason, text: text)
        let presentedAction = try XCTUnwrap(presenter.didNextStepActionReceivedInvocations.first)
        XCTAssertEqual(action, presentedAction)
        XCTAssertEqual(presenter.didNextStepActionCallsCount, 1)
    }
    
    func testContinueAction_WhenHasNoReason_ShouldConfirmWithOtherReason() throws {
        let text = "Karpov"
        sut.setSelectedReason(index: nil)
        sut.continueAction(text: text)
        
        let reason = ShutDownReason.other()
        let action = ReasonAction.confirm(reason: reason, text: text)
        let presentedAction = try XCTUnwrap(presenter.didNextStepActionReceivedInvocations.first)
        XCTAssertEqual(action, presentedAction)
        XCTAssertEqual(presenter.didNextStepActionCallsCount, 1)
    }
    
    func testContinueAction_WhenHasNoReasonAndNoText_ShouldNotContinue() throws {
        sut.setSelectedReason(index: nil)
        sut.continueAction(text: nil)
        
        XCTAssertEqual(presenter.didNextStepActionCallsCount, 0)
    }
}

private extension ReasonInteractorTests {
    func loadReasonsAndWait() {
        let expectation = XCTestExpectation()
        presenter.didCallPresentReasons = {
            expectation.fulfill()
        }
        
        sut.loadReasons()
        
        wait(for: [expectation], timeout: timeout)
    }
}
