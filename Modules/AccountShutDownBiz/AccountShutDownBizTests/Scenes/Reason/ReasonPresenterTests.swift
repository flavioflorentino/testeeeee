import Foundation
import XCTest
@testable import AccountShutDownBiz

private class ReasonDisplayingSpy: ReasonDisplaying {
    // MARK: - DisplayReasons
    private(set) var displayReasonsCallsCount = 0
    private(set) var displayReasonsReceivedInvocations: [[ShutDownReason]] = []

    func displayReasons(_ reasons: [ShutDownReason]) {
        displayReasonsCallsCount += 1
        displayReasonsReceivedInvocations.append(reasons)
    }
}

private class ReasonCoordinatingSpy: ReasonCoordinating {
    var viewController: UIViewController?

    // MARK: - Perform
    private(set) var performActionCallsCount = 0
    private(set) var performActionReceivedInvocations: [ReasonAction] = []

    func perform(action: ReasonAction) {
        performActionCallsCount += 1
        performActionReceivedInvocations.append(action)
    }
}

final class ReasonPresenterTests: XCTestCase {
    private lazy var viewController = ReasonDisplayingSpy()
    private lazy var coordinator = ReasonCoordinatingSpy()
    
    private lazy var sut: ReasonPresenter  = {
        let presenter = ReasonPresenter(coordinator: coordinator)
        presenter.viewController = viewController
        return presenter
    }()
    
    func testPresentReasons_ShouldDisplayReasons() throws {
        let reasons = try XCTUnwrap(ReasonMock().fetchReasons())
        sut.presentReasons(reasons)
        
        XCTAssertEqual(viewController.displayReasonsCallsCount, 1)
        
        let displayedReasons = try XCTUnwrap(viewController.displayReasonsReceivedInvocations.first)
        XCTAssertEqual(reasons.count, displayedReasons.count)
    }
    
    func testDidNextStep_ShouldPerformNextStep() throws {
        let text = "Kasparov"
        let reason = ShutDownReason.other()
        let action = ReasonAction.confirm(reason: reason, text: text)
        sut.didNextStep(action: action)
        
        XCTAssertEqual(coordinator.performActionCallsCount, 1)
        
        let invocation = try XCTUnwrap(coordinator.performActionReceivedInvocations.first)
        XCTAssertEqual(invocation, action)
    }
}
