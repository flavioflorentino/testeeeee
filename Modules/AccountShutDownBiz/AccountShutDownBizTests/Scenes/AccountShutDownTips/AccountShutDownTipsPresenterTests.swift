import XCTest
@testable import AccountShutDownBiz

private class AccountShutDownTipsDisplayingSpy: AccountShutDownTipsDisplaying {
    // MARK: - displayTips
    private(set) var displayTipsCallsCount = 0
    private(set) var displayTipsReceivedInvocations: [[NSAttributedString]] = []

    func displayTips(_ tips: [NSAttributedString]) {
        displayTipsCallsCount += 1
        displayTipsReceivedInvocations.append(tips)
    }
}

private class AccountShutDownTipsCoordinatingSpy: AccountShutDownTipsCoordinating {
    var viewController: UIViewController?

    // MARK: - perform
    private(set) var performActionCallsCount = 0
    private(set) var performActionReceivedInvocations: [AccountShutDownTipsAction] = []

    func perform(action: AccountShutDownTipsAction) {
        performActionCallsCount += 1
        performActionReceivedInvocations.append(action)
    }
}

final class AccountShutDownTipsPresenterTests: XCTestCase {
    private lazy var viewController = AccountShutDownTipsDisplayingSpy()
    private lazy var coordinator = AccountShutDownTipsCoordinatingSpy()
    private lazy var sut: AccountShutDownTipsPresenting = {
        let presenter = AccountShutDownTipsPresenter(
            coordinator: coordinator,
            dependencies: DependencyContainerMock()
        )
        presenter.viewController = viewController
        return presenter
    }()

    func testPaymentAccountTips_WhenSuccess_ShouldReturnFourTips() {
        sut.displayReceivementTips()

        XCTAssertEqual(viewController.displayTipsCallsCount, 1)
        XCTAssertEqual(viewController.displayTipsReceivedInvocations.last?.count, 4)
    }

    func testReceivementAccountTips_WhenSuccess_ShouldReturnFourTips() {
        sut.displayPaymentTips()

        XCTAssertEqual(viewController.displayTipsCallsCount, 1)
        XCTAssertEqual(viewController.displayTipsReceivedInvocations.last?.count, 4)
    }
    
    func testProceedToReasonAction_ShouldInvokeCoordinatorCorrectly() {
        sut.didNextStep(action: .proceedToReason)
        
        XCTAssertEqual(coordinator.performActionReceivedInvocations.last, .proceedToReason)
    }
    
    func testHelpAction_ShouldInvokeCoordinatorCorrectly() {
        sut.didNextStep(action: .help)
        
        XCTAssertEqual(coordinator.performActionReceivedInvocations.last, .help)
    }
}
