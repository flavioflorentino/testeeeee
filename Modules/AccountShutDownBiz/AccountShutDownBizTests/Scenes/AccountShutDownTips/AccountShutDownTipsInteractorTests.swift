import XCTest
@testable import AccountShutDownBiz

private class AccountShutDownTipsPresentingSpy: AccountShutDownTipsPresenting {
    var viewController: AccountShutDownTipsDisplaying?

    // MARK: - didNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didNextStepActionReceivedInvocations: [AccountShutDownTipsAction] = []

    func didNextStep(action: AccountShutDownTipsAction) {
        didNextStepActionCallsCount += 1
        didNextStepActionReceivedInvocations.append(action)
    }

    // MARK: - displayPaymentTips
    private(set) var displayPaymentTipsCallsCount = 0

    func displayPaymentTips() {
        displayPaymentTipsCallsCount += 1
    }

    // MARK: - displayReceivementTips
    private(set) var displayReceivmentTipsCallsCount = 0

    func displayReceivementTips() {
        displayReceivmentTipsCallsCount += 1
    }
}

final class AccountShutDownTipsInteractorTests: XCTestCase {
    private let presenter = AccountShutDownTipsPresentingSpy()
    private var accountType: BizAccountType = .paymentAccount
    private lazy var sut: AccountShutDownTipsInteractor = {
        AccountShutDownTipsInteractor(
            presenter: presenter,
            dependencies: DependencyContainerMock(),
            accountType: accountType
        )
    }()

    func testLoadTips_WhenAccountTypeIsPaymentAccount_ShouldInvokeDisplayPaymentTips() {
        accountType = .paymentAccount
        sut.loadTips()

        XCTAssertEqual(presenter.displayPaymentTipsCallsCount, 1)
    }

    func testLoadTips_WhenAccountTypeIsReceivementAccount_ShouldInvokeDisplayReceivementTips() {
        accountType = .receivementAccount
        sut.loadTips()

        XCTAssertEqual(presenter.displayReceivmentTipsCallsCount, 1)
    }
    
    func testDidTapContinue_ShouldCallCorrectActionOnPresenter() {
        sut.didTapContinue()
        
        XCTAssertEqual(presenter.didNextStepActionReceivedInvocations.last, .proceedToReason)
    }
    
    func testDidTapHelp_ShouldCallCorrectActionOnPresenter() {
        sut.didTapHelp()
        
        XCTAssertEqual(presenter.didNextStepActionReceivedInvocations.last, .help)
    }
}
