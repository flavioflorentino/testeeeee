import AnalyticsModule
import Foundation
import XCTest
@testable import AccountShutDownBiz

private final class ConfirmationServicingFake: ConfirmationServicing {
    // MARK: - FetchTips
    var fetchTipsResult: Result<[String], Error> = .success([""])
    func fetchTips(completion: @escaping(Result<[String], Error>) -> Void) {
        completion(fetchTipsResult)
    }
}

private final class ConfirmationPresentingSpy: ConfirmationPresenting {
    var viewController: ConfirmationDisplaying?

    // MARK: - PresentTips
    private(set) var presentTipsCallsCount = 0
    private(set) var presentTipsReceivedInvocations: [[String]] = []
    var didCallPresentTips: (() -> Void)?

    func presentTips(_ tips: [String]) {
        presentTipsCallsCount += 1
        presentTipsReceivedInvocations.append(tips)
        didCallPresentTips?()
    }

    // MARK: - PresentLoading
    private(set) var presentLoadingIsLoadingCallsCount = 0
    private(set) var presentLoadingIsLoadingReceivedInvocations: [Bool] = []

    func present(isLoading: Bool) {
        presentLoadingIsLoadingCallsCount += 1
        presentLoadingIsLoadingReceivedInvocations.append(isLoading)
    }
    
    // MARK: - SetConfirmButtonEnabled
   private(set) var setConfirmButtonEnabledCallsCount = 0
   private(set) var setConfirmButtonEnabledReceivedInvocations: [Bool] = []

   func setConfirmButton(isEnabled: Bool) {
       setConfirmButtonEnabledCallsCount += 1
       setConfirmButtonEnabledReceivedInvocations.append(isEnabled)
   }

    // MARK: - DidNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didNextStepActionReceivedInvocations: [ConfirmationAction] = []

    func didNextStep(action: ConfirmationAction) {
        didNextStepActionCallsCount += 1
        didNextStepActionReceivedInvocations.append(action)
    }
}

final class ConfirmationInteractorTests: XCTestCase {
    private let timeout: TimeInterval = 1
    private let service = ConfirmationServicingFake()
    private let presenter = ConfirmationPresentingSpy()
    private let analytics = AnalyticsSpy()
    
    private lazy var dependencies = DependencyContainerMock(analytics)
    private lazy var sut = ConfirmationInteractor(service: service, presenter: presenter, dependencies: dependencies)
    
    func testFetchTips_ShouldPresentTips() throws {
        let tips = try XCTUnwrap(ConfirmationMock().fetchTips())
        service.fetchTipsResult = .success(tips)
        fetchTipsAndWait()
        let invocation = presenter.presentTipsReceivedInvocations.first
        XCTAssertEqual(presenter.presentTipsCallsCount, 1)
        XCTAssertEqual(invocation, tips)
    }
    
    func testCancelAction_ShouldDoNextStepWithCancelAction() throws {
        sut.cancelAction()
        
        let invocation = try XCTUnwrap(presenter.didNextStepActionReceivedInvocations.first)
        XCTAssertEqual(presenter.didNextStepActionCallsCount, 1)
        XCTAssertEqual(invocation, .cancel)
    }
    
    func testConditionsAction_ShouldDoNextStepWithConditionsAction() throws {
        sut.conditionsAction()
        
        let invocation = try XCTUnwrap(presenter.didNextStepActionReceivedInvocations.first)
        XCTAssertEqual(presenter.didNextStepActionCallsCount, 1)
        XCTAssertEqual(invocation, .conditions)
    }
    
    func testCheckboxAction_WhenEnabled_ShouldSetConfirmButtonEnabled() throws {
        sut.checkboxAction(isEnabled: true)
        
        let invocation = try XCTUnwrap(presenter.setConfirmButtonEnabledReceivedInvocations.first)
        XCTAssertEqual(presenter.setConfirmButtonEnabledCallsCount, 1)
        XCTAssertTrue(invocation)
    }
    
    func testCheckboxAction_WhenDisabled_ShouldSetConfirmButtonDisabled() throws {
        sut.checkboxAction(isEnabled: false)
        
        let invocation = try XCTUnwrap(presenter.setConfirmButtonEnabledReceivedInvocations.first)
        XCTAssertEqual(presenter.setConfirmButtonEnabledCallsCount, 1)
        XCTAssertFalse(invocation)
    }
    
    func testContinueAction_ShouldPresentLoading() throws {
        sut.continueAction()
        
        let invocation = try XCTUnwrap(presenter.presentLoadingIsLoadingReceivedInvocations.first)
        XCTAssertEqual(presenter.presentLoadingIsLoadingCallsCount, 1)
        XCTAssertTrue(invocation)
    }
}

// MARK: - Test Events

extension ConfirmationInteractorTests {
    func testCancelAction_ShouldLogQuit() {
        sut.cancelAction()
        
        XCTAssertEqual(analytics.logCalledCount, 1)
        XCTAssertTrue(analytics.equals(to: ConfirmationAnalytics.quit.event()))
    }
    
    func testCheckboxAction_WhenTrue_ShouldLogAcceptedConditions() {
        sut.checkboxAction(isEnabled: true)
        XCTAssertEqual(analytics.logCalledCount, 1)
        XCTAssertTrue(analytics.equals(to: ConfirmationAnalytics.acceptedConditions.event()))
    }
    
    func testCheckboxAction_WhenFalse_ShouldNotLogAcceptedConditions() {
        sut.checkboxAction(isEnabled: false)
        XCTAssertEqual(analytics.logCalledCount, 0)
    }
    
    func testConditionsAction_ShouldLogReadConditions() {
        sut.conditionsAction()
        XCTAssertEqual(analytics.logCalledCount, 1)
        XCTAssertTrue(analytics.equals(to: ConfirmationAnalytics.readConditions.event()))
    }
    
    func testContinueAction_ShouldLogConfirm() {
        sut.continueAction()
        XCTAssertEqual(analytics.logCalledCount, 1)
        XCTAssertTrue(analytics.equals(to: ConfirmationAnalytics.confirm.event()))
    }
}

// MARK: - Private Methods

private extension ConfirmationInteractorTests {
    func fetchTipsAndWait() {
        let expectation = XCTestExpectation()
        presenter.didCallPresentTips = {
            expectation.fulfill()
        }
        
        sut.fetchTips()
        
        wait(for: [expectation], timeout: timeout)
    }
}
