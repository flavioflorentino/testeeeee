import Foundation
import XCTest
@testable import AccountShutDownBiz

private class ConfirmationTermsPresentingSpy: ConfirmationTermsPresenting {
    var viewController: ConfirmationTermsDisplaying?
    
    // MARK: - DisplayLoading
    private(set) var displayLoadingCallsCount = 0

    func displayLoading() {
        displayLoadingCallsCount += 1
    }

    // MARK: - StopLoading
    private(set) var stopLoadingCallsCount = 0

    func stopLoading() {
        stopLoadingCallsCount += 1
    }

    // MARK: - DisplayPDF
    private(set) var displayPDFUrlCallsCount = 0
    private(set) var displayPDFUrlReceivedInvocations: [URL] = []

    func displayPDF(url: URL) {
        displayPDFUrlCallsCount += 1
        displayPDFUrlReceivedInvocations.append(url)
    }

    // MARK: - DidNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didNextStepActionReceivedInvocations: [ConfirmationTermsAction] = []

    func didNextStep(action: ConfirmationTermsAction) {
        didNextStepActionCallsCount += 1
        didNextStepActionReceivedInvocations.append(action)
    }
}

private class ConfirmationTermsServicingFake: ConfirmationTermsServicing {
    var url: URL?
}

final class ConfirmationTermsInteractorTests: XCTestCase {
    private let timeInterval: TimeInterval = 1
    private let validPDFString = "https://www.qualitychess.co.uk/ebooks/AGameofQueens-excerpt.pdf"
    private let service = ConfirmationTermsServicingFake()
    private let presenter = ConfirmationTermsPresentingSpy()
    private lazy var sut = ConfirmationTermsInteractor(service: service, presenter: presenter)
    
    func testLoadPDF_ShouldDisplayLoading() throws {
        let url = try XCTUnwrap(URL(string: validPDFString))
        service.url = url
        
        sut.loadPDF()
        
        XCTAssertEqual(1, presenter.displayLoadingCallsCount)
    }
    
    func testLoadPDF_ShouldDisplayPDFWithServiceURL() throws {
        let url = try XCTUnwrap(URL(string: validPDFString))
        service.url = url
        
        sut.loadPDF()
        
        XCTAssertEqual(1, presenter.displayPDFUrlCallsCount)
        let invocation = try XCTUnwrap(presenter.displayPDFUrlReceivedInvocations.first)
        XCTAssertEqual(url, invocation)
    }
    
    func testCloseAction_ShouldPerformNextStepWithDismissAction() throws {
        sut.closeAction()
        
        XCTAssertEqual(1, presenter.didNextStepActionCallsCount)
        let invocation = try XCTUnwrap(presenter.didNextStepActionReceivedInvocations.first)
        XCTAssertEqual(.dismiss, invocation)
    }
    
    func testHandleWKNavigation_WhenSameURLFromService_AllowNavigation() throws {
        let url = try XCTUnwrap(URL(string: validPDFString))
        service.url = url
        
        let expectation = XCTestExpectation(description: "Should wait for handle wk navigation completion")
        sut.handleWKNavigation(request: URLRequest(url: url)) { policy in
            XCTAssertEqual(policy, .allow)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: timeInterval)
    }
    
    func testHandleWKNavigation_WhenDifferentURLFromService_AllowNavigation() throws {
        let url = try XCTUnwrap(URL(string: validPDFString))
        let otherURL = try XCTUnwrap(URL(string: "https://google.com"))
        service.url = url
        
        let expectation = XCTestExpectation(description: "Should wait for handle wk navigation completion")
        sut.handleWKNavigation(request: URLRequest(url: otherURL)) { policy in
            XCTAssertEqual(policy, .cancel)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: timeInterval)
    }
    
    func testFinishNavigation_ShouldStopLoading() {
        sut.finishNavigationAction()
        
        XCTAssertEqual(1, presenter.stopLoadingCallsCount)
    }
}
