import Foundation
import XCTest
@testable import AccountShutDownBiz

private class ConfirmationTermsCoordinatingSpy: ConfirmationTermsCoordinating {
    var viewController: UIViewController?

    // MARK: - Perform
    private(set) var performActionCallsCount = 0
    private(set) var performActionReceivedInvocations: [ConfirmationTermsAction] = []

    func perform(action: ConfirmationTermsAction) {
        performActionCallsCount += 1
        performActionReceivedInvocations.append(action)
    }
}

private class ConfirmationTermsDisplayingSpy: ConfirmationTermsDisplaying {
    // MARK: - StartLoading
    private(set) var startLoadingCallsCount = 0

    func startLoading() {
        startLoadingCallsCount += 1
    }

    // MARK: - StopLoading
    private(set) var stopLoadingCallsCount = 0

    func stopLoading() {
        stopLoadingCallsCount += 1
    }
    
    // MARK: - ShowPDF
    private(set) var showPDFUrlCallsCount = 0
    private(set) var showPDFUrlReceivedInvocations: [URL] = []

    func showPDF(url: URL) {
        showPDFUrlCallsCount += 1
        showPDFUrlReceivedInvocations.append(url)
    }
}

final class ConfirmationTermsPresenterTests {
    private let viewController = ConfirmationTermsDisplayingSpy()
    private let coordinator = ConfirmationTermsCoordinatingSpy()
    private lazy var sut: ConfirmationTermsPresenter = {
        let sut = ConfirmationTermsPresenter(coordinator: coordinator)
        sut.viewController = viewController
        return sut
    }()
    
    func testDisplayLoading_ShouldStartLoading() {
        sut.displayLoading()
        
        XCTAssertEqual(1, viewController.startLoadingCallsCount)
    }
    
    func testStopLoading_ShouldStopLoading() {
        sut.stopLoading()
        
        XCTAssertEqual(1, viewController.stopLoadingCallsCount)
    }
    
    func testDisplayPDF_ShouldDisplayPDFWithSameURL() throws {
        let url = try XCTUnwrap(URL(string: "https://picpay.com"))
        sut.displayPDF(url: url)
        
        XCTAssertEqual(1, viewController.showPDFUrlCallsCount)
        let invocation = try XCTUnwrap(viewController.showPDFUrlReceivedInvocations.first)
        XCTAssertEqual(url, invocation)
    }
    
    func testDidNextStep_ShouldPerformNextStepWithSameAction() throws {
        let action = ConfirmationTermsAction.dismiss
        sut.didNextStep(action: action)
        
        XCTAssertEqual(1, coordinator.performActionCallsCount)
        let invocation = try XCTUnwrap(coordinator.performActionReceivedInvocations.first)
        XCTAssertEqual(action, invocation)
    }
}
