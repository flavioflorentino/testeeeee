import Foundation
import XCTest
@testable import AccountShutDownBiz

private class ConfirmationCoordinatingSpy: ConfirmationCoordinating {
    var viewController: UIViewController?

    // MARK: - Perform
    private(set) var performActionCallsCount = 0
    private(set) var performActionReceivedInvocations: [ConfirmationAction] = []

    func perform(action: ConfirmationAction) {
        performActionCallsCount += 1
        performActionReceivedInvocations.append(action)
    }
}

private class ConfirmationDisplayingSpy: ConfirmationDisplaying {    
    // MARK: - DisplayTips
    private(set) var displayTipsCallsCount = 0
    private(set) var displayTipsReceivedInvocations: [[String]] = []

    func displayTips(_ tips: [String]) {
        displayTipsCallsCount += 1
        displayTipsReceivedInvocations.append(tips)
    }

    // MARK: - SetLoading
    private(set) var setLoadingCallsCount = 0
    private(set) var setLoadingReceivedInvocations: [Bool] = []

    func set(isLoading: Bool) {
        setLoadingCallsCount += 1
        setLoadingReceivedInvocations.append(isLoading)
    }
    
    // MARK: - SetConfirmButtonEnabled
    private(set) var setConfirmButtonEnabledCallsCount = 0
    private(set) var setConfirmButtonEnabledReceivedInvocations: [Bool] = []

    func setConfirmButton(isEnabled: Bool) {
        setConfirmButtonEnabledCallsCount += 1
        setConfirmButtonEnabledReceivedInvocations.append(isEnabled)
    }
}

final class ConfirmationPresenterTests: XCTestCase {
    private lazy var coordinator = ConfirmationCoordinatingSpy()
    private lazy var viewController = ConfirmationDisplayingSpy()
    private lazy var sut: ConfirmationPresenter = {
        let sut = ConfirmationPresenter(coordinator: coordinator)
        sut.viewController = viewController
        return sut
    }()
    
    func testDidPresentTips_ShouldDisplayTips() throws {
        let tips = try XCTUnwrap(ConfirmationMock().fetchTips())
        sut.presentTips(tips)
        
        let invocation = try XCTUnwrap(viewController.displayTipsReceivedInvocations.first)
        XCTAssertEqual(invocation, tips)
        XCTAssertEqual(viewController.displayTipsCallsCount, 1)
    }
    
    func testPresentLoading_WhenTrue_ShouldSetLoadingWithTrue() throws {
        sut.present(isLoading: true)
        
        let invocation = try XCTUnwrap(viewController.setLoadingReceivedInvocations.first)
        XCTAssertTrue(invocation)
        XCTAssertEqual(viewController.setLoadingCallsCount, 1)
    }
    
    func testPresentLoading_WhenFalse_ShouldSetLoadingWithFalse() throws {
        sut.present(isLoading: false)
        
        let invocation = try XCTUnwrap(viewController.setLoadingReceivedInvocations.first)
        XCTAssertFalse(invocation)
        XCTAssertEqual(viewController.setLoadingCallsCount, 1)
    }
    
    func testSetConfirmButton_WhenEnabled_ShouldSetConfirmButtonToEnabled() throws {
        sut.setConfirmButton(isEnabled: true)
        
        let invocation = try XCTUnwrap(viewController.setConfirmButtonEnabledReceivedInvocations.first)
        XCTAssertTrue(invocation)
        XCTAssertEqual(viewController.setConfirmButtonEnabledCallsCount, 1)
    }
    
    func testSetConfirmButton_WhenDisabled_ShouldSetConfirmButtonToDisabled() throws {
        sut.setConfirmButton(isEnabled: false)
        
        let invocation = try XCTUnwrap(viewController.setConfirmButtonEnabledReceivedInvocations.first)
        XCTAssertFalse(invocation)
        XCTAssertEqual(viewController.setConfirmButtonEnabledCallsCount, 1)
    }
    
    func testDidNextStep_ShouldPerformNextAction() throws {
        sut.didNextStep(action: .cancel)
        
        let invocation = try XCTUnwrap(coordinator.performActionReceivedInvocations.first)
        XCTAssertEqual(invocation, .cancel)
        XCTAssertEqual(coordinator.performActionCallsCount, 1)
    }
}
