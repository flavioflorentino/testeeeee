import Foundation
@testable import AccountShutDownBiz

final class ConfirmationMock {
    // swiftlint:disable:next discouraged_optional_collection
    func fetchTips() -> [String]? {
        LocalMock<[String]>().pureLocalMock("confirmation_tips_test")
    }
}
