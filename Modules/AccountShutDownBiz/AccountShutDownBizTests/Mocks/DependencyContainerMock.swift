import AnalyticsModule
import Core
@testable import AccountShutDownBiz

final class DependencyContainerMock: Dependencies {
    lazy var mainQueue: DispatchQueue = resolve()
    lazy var analytics: AnalyticsProtocol = resolve()
    
    private let dependencies: [Any]
    
    init(_ dependencies: Any...) {
        self.dependencies = dependencies
    }
    
    func resolve<T>() -> T {
        let resolved = dependencies.compactMap { $0 as? T }
                
        switch resolved.first {
        case .none:
            fatalError("DependencyContainerMock could not resolve dependency: \(T.self)\n")
        case .some where resolved.count > 1:
            fatalError("DependencyContainerMock resolved mutiple dependencies for: \(T.self)\n")
        case .some(let mock):
            return mock
        }
    }
    
    func resolve() -> DispatchQueue {
        dependencies.compactMap { $0 as? DispatchQueue }.first ?? DispatchQueue(label: "DependencyContainerMock")
    }
    
    func resolve() -> AnalyticsProtocol {
        dependencies.compactMap { $0 as? AnalyticsProtocol }.first ?? AnalyticsSpy()
    }
}
