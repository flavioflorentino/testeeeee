import Foundation

// swiftlint:disable convenience_type
final class AccountShutDownBizTestsResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: AccountShutDownBizTestsResources.self).url(forResource: "AccountShutDownBizTestsResources", withExtension: "bundle") else {
            return Bundle(for: AccountShutDownBizTestsResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: AccountShutDownBizTestsResources.self)
    }()
}

final class LocalMock<T: Decodable> {
    func pureLocalMock(_ fileName: String, decoder: JSONDecoder = JSONDecoder()) -> T? {
        guard let data = Mocker().data(fileName) else { return nil }
        
        do {
            return try decoder.decode(T.self, from: data)
        } catch {
            return nil
        }
    }
}

final class Mocker {
    func data(_ fileName: String) -> Data? {
        guard
            let file = AccountShutDownBizTestsResources.resourcesBundle.path(forResource: fileName, ofType: "json"),
            let data = try? Data(contentsOf: URL(fileURLWithPath: file))
            else {
                return nil
        }
        return data
    }
}
