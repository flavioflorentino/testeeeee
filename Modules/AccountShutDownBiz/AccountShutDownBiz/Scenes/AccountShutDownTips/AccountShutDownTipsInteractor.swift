import Foundation

protocol AccountShutDownTipsInteracting: AnyObject {
    func loadTips()
    func didTapContinue()
    func didTapHelp()
}

final class AccountShutDownTipsInteractor {
    typealias Dependencies = HasNoDependency

    private let presenter: AccountShutDownTipsPresenting
    private let dependencies: Dependencies
    private let accountType: BizAccountType

    init(
        presenter: AccountShutDownTipsPresenting, 
        dependencies: Dependencies,
        accountType: BizAccountType
    ) {
        self.presenter = presenter
        self.dependencies = dependencies
        self.accountType = accountType
    }
}

// MARK: - AccountShutDownTipsInteracting
extension AccountShutDownTipsInteractor: AccountShutDownTipsInteracting {
    func loadTips() {
        switch accountType {
        case .paymentAccount:
            presenter.displayPaymentTips()
        case .receivementAccount, .unknown:
            presenter.displayReceivementTips()
        }
    }

    func didTapContinue() {
        presenter.didNextStep(action: .proceedToReason)
    }

    func didTapHelp() {
        presenter.didNextStep(action: .help)
    }
}
