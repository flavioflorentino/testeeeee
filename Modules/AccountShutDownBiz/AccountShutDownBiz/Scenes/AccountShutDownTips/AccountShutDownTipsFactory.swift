import UIKit

enum AccountShutDownTipsFactory {
    static func make(_ accountType: BizAccountType) -> AccountShutDownTipsViewController {
        let container = DependencyContainer()
        let coordinator: AccountShutDownTipsCoordinating = AccountShutDownTipsCoordinator(dependencies: container)
        let presenter: AccountShutDownTipsPresenting = AccountShutDownTipsPresenter(coordinator: coordinator, dependencies: container)
        let interactor = AccountShutDownTipsInteractor(
            presenter: presenter, 
            dependencies: container,
            accountType: accountType
        )
        let viewController = AccountShutDownTipsViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
