import UIKit

enum AccountShutDownTipsAction: Equatable {
    case proceedToReason
    case help
}

protocol AccountShutDownTipsCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: AccountShutDownTipsAction)
}

final class AccountShutDownTipsCoordinator {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - AccountShutDownTipsCoordinating
extension AccountShutDownTipsCoordinator: AccountShutDownTipsCoordinating {
    func perform(action: AccountShutDownTipsAction) {
        switch action {
        case .proceedToReason:
            let controller = ReasonTableFooterFactory.make()
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case .help:
            // todo implementar chamada da FAQ quando o ticket id estiver diponível
            break
        }
    }
}
