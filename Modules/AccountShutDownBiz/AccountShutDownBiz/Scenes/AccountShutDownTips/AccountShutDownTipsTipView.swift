import UI
import UIKit

private extension AccountShutDownTipsTipView.Layout {
    enum DotView {
        static let size = CGSize(width: 8, height: 8)
    }
}

final class AccountShutDownTipsTipView: UIView {
    fileprivate enum Layout { }

    private lazy var dotView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.branding600.color
        view.layer.cornerRadius = CornerRadius.light
        return view
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    init(attrString: NSAttributedString) {
        super.init(frame: .zero)
        buildLayout()
        titleLabel.attributedText = attrString
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: ViewConfiguration

extension AccountShutDownTipsTipView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(dotView)
        addSubview(titleLabel)
    }
    
    func setupConstraints() {
        dotView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.size.equalTo(Layout.DotView.size)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(dotView).offset(-Spacing.base00)
            $0.leading.equalTo(dotView.snp.trailing).offset(Spacing.base01)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.bottom.equalToSuperview()
        }
    }
}
