import AssetsKit
import UI
import UIKit

protocol AccountShutDownTipsDisplaying: AnyObject {
    func displayTips(_ tips: [NSAttributedString])
}

private extension AccountShutDownTipsViewController.Layout {
    enum ElipseView {
        static let cornerRadius: CGFloat = 210
        static let height: CGFloat = 290
    }
    enum ImageView {
        static let size = CGSize(width: 171, height: 166)
    }
}

final class AccountShutDownTipsViewController: ViewController<AccountShutDownTipsInteracting, UIView> {
    typealias Localizable = Strings.TipsLocalizable

    fileprivate enum Layout { }

    private lazy var elipseView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.success050.color
        view.layer.cornerRadius = Layout.ElipseView.cornerRadius
        return view
    }()

    private lazy var imageView: UIImageView = {
        let imageview = UIImageView()
        imageview.clipsToBounds = true
        imageview.contentMode = .scaleAspectFit
        imageview.image = Resources.Illustrations.iluFallingNoBackground.image
        return imageview
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
            .with(\.textColor, Colors.grayscale850.color)
            .with(\.textAlignment, .center)
            .with(\.text, Localizable.title)
        return label
    }()

    private lazy var shutDownLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.grayscale850.color)
            .with(\.textAlignment, .center)
            .with(\.numberOfLines, 1)
            .with(\.text, Localizable.explanation)
        return label
    }()

    private lazy var tipsView = UIView()
    
    private lazy var tipsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base03
        return stackView
    }()

    private lazy var continueButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.Button.continue, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTapContinueButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var needHelpButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.Button.help, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(didTapNeedHelpButton), for: .touchUpInside)
        return button
    }()

    private lazy var buttonsStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [continueButton, needHelpButton])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        return stackView
    }()

    private lazy var contentView: UIView = {
        let view = UIView()
        view.isUserInteractionEnabled = true
        return view
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.alwaysBounceVertical = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.isUserInteractionEnabled = true
        return scrollView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.loadTips()
    }

    override func buildViewHierarchy() {
        tipsView.addSubview(tipsStackView)
        contentView.addSubview(elipseView)
        contentView.addSubview(imageView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(shutDownLabel)
        contentView.addSubview(tipsView)
        contentView.addSubview(continueButton)
        contentView.addSubview(needHelpButton)

        scrollView.addSubview(contentView)
        view.addSubview(scrollView)
    }
    
    override func setupConstraints() {
        scrollViewConstraints()
        bodyConstraints()
    }

    override func configureViews() { 
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}

// MARK: - Contraints extension
private extension AccountShutDownTipsViewController {
    func scrollViewConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        contentView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.size.equalTo(view)
        }
    }

    func bodyConstraints() {
        elipseView.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(-Spacing.base04)
            $0.height.equalTo(Layout.ElipseView.height)
            $0.centerY.equalTo(contentView.snp.top).offset(-Spacing.base02)
        }

        imageView.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalToSuperview().offset(-Spacing.base02)
            $0.size.equalTo(Layout.ImageView.size)
        }

        titleLabel.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base01)
        }

        shutDownLabel.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base03)
        }

        tipsView.snp.makeConstraints {
            $0.top.equalTo(shutDownLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview()
        }

        tipsStackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        continueButton.snp.makeConstraints {
            $0.top.equalTo(tipsView.snp.bottom).offset(Spacing.base05)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        needHelpButton.snp.makeConstraints {
            $0.top.equalTo(continueButton.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
}

// MARK: - AccountShutDownTipsDisplaying
extension AccountShutDownTipsViewController: AccountShutDownTipsDisplaying {
    func displayTips(_ tips: [NSAttributedString]) { 
        tips.forEach { 
            tipsStackView.addArrangedSubview(AccountShutDownTipsTipView(attrString: $0))
        }
    }
}

@objc
extension AccountShutDownTipsViewController {
    func didTapContinueButton() {
        interactor.didTapContinue()
    }

    func didTapNeedHelpButton() {
        interactor.didTapHelp()
    }
}
