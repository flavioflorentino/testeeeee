import Foundation
import UI

protocol AccountShutDownTipsPresenting: AnyObject {
    var viewController: AccountShutDownTipsDisplaying? { get set }
    func didNextStep(action: AccountShutDownTipsAction)
    func displayPaymentTips()
    func displayReceivementTips()
}

final class AccountShutDownTipsPresenter {
    typealias Dependencies = HasNoDependency
    typealias Localizable = Strings.TipsLocalizable

    private let dependencies: Dependencies
    private let coordinator: AccountShutDownTipsCoordinating

    weak var viewController: AccountShutDownTipsDisplaying?

    init(coordinator: AccountShutDownTipsCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
}

// MARK: - AccountShutDownTipsPresenting
extension AccountShutDownTipsPresenter: AccountShutDownTipsPresenting {
    func didNextStep(action: AccountShutDownTipsAction) {
        coordinator.perform(action: action)
    }

    func displayReceivementTips() {
        let firstTip = NSMutableAttributedString()
        firstTip.append(createAttributed(Localizable.ReceivementAccount.First.bold, bold: true))
        firstTip.append(createAttributed(Localizable.ReceivementAccount.First.normal))
        let secondTip = NSMutableAttributedString()
        secondTip.append(createAttributed(Localizable.ReceivementAccount.Second.bold, bold: true))
        secondTip.append(createAttributed(Localizable.ReceivementAccount.Second.normal))
        let thirdTip = NSMutableAttributedString()
        thirdTip.append(createAttributed(Localizable.ReceivementAccount.Third.bold, bold: true))
        thirdTip.append(createAttributed(Localizable.ReceivementAccount.Third.normal))
        let fourthTip = NSMutableAttributedString()
        fourthTip.append(createAttributed(Localizable.ReceivementAccount.Fourth.bold, bold: true))
        fourthTip.append(createAttributed(Localizable.ReceivementAccount.Fourth.normal))

        viewController?.displayTips([firstTip, secondTip, thirdTip, fourthTip])
    }

    func displayPaymentTips() {
        let firstTip = NSMutableAttributedString()
        firstTip.append(createAttributed(Localizable.PaymentAccount.First.bold, bold: true))
        firstTip.append(createAttributed(Localizable.PaymentAccount.First.normal))
        let secondTip = NSMutableAttributedString()
        secondTip.append(createAttributed(Localizable.PaymentAccount.Second.bold, bold: true))
        secondTip.append(createAttributed(Localizable.PaymentAccount.Second.normal))
        let thirdTip = NSMutableAttributedString()
        thirdTip.append(createAttributed(Localizable.PaymentAccount.Third.bold, bold: true))
        thirdTip.append(createAttributed(Localizable.PaymentAccount.Third.normal))
        let fourthTip = NSMutableAttributedString()
        fourthTip.append(createAttributed(Localizable.PaymentAccount.Fourth.bold, bold: true))
        fourthTip.append(createAttributed(Localizable.PaymentAccount.Fourth.normal))

        viewController?.displayTips([firstTip, secondTip, thirdTip, fourthTip])
    }
}

private extension AccountShutDownTipsPresenter {
    func createAttributed(_ text: String, bold: Bool = false) -> NSAttributedString {
        let typography = bold ? Typography.bodySecondary(.highlight) : Typography.bodySecondary()
        return NSAttributedString(string: text, attributes: attributedKey(typography, color: Colors.black.color))
    }

    func attributedKey(_ typo: Typography, color: UIColor) -> [NSAttributedString.Key: Any] {
        [.font: typo.font(), .foregroundColor: color]
    }
}
