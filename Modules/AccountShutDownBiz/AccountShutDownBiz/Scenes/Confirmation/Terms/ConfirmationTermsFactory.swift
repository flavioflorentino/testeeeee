import UIKit

enum ConfirmationTermsFactory {
    static func make() -> ConfirmationTermsViewController {
        let service: ConfirmationTermsServicing = ConfirmationTermsService()
        let coordinator: ConfirmationTermsCoordinating = ConfirmationTermsCoordinator()
        let presenter: ConfirmationTermsPresenting = ConfirmationTermsPresenter(coordinator: coordinator)
        let interactor = ConfirmationTermsInteractor(service: service, presenter: presenter)
        let viewController = ConfirmationTermsViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
