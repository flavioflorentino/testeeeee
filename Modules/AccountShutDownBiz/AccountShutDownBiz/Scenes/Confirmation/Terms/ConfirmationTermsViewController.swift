import AssetsKit
import SnapKit
import UI
import UIKit
import WebKit

protocol ConfirmationTermsDisplaying: AnyObject {
    func showPDF(url: URL)
    func startLoading()
    func stopLoading()
}

final class ConfirmationTermsViewController: ViewController<ConfirmationTermsInteracting, UIView> {
    private lazy var pdfView: WKWebView = {
        let webView = WKWebView()
        webView.navigationDelegate = self
        return webView
    }()
    
    private lazy var closeButton: UIBarButtonItem = {
        let image = Resources.Icons.icoClose.image
        let closeButton = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(didTapCloseButton))
        closeButton.tintColor = Colors.branding600.color
        return closeButton
    }()
    
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator: UIActivityIndicatorView
        if #available(iOS 13.0, *) {
            activityIndicator = UIActivityIndicatorView(style: .large)
        } else {
            activityIndicator = UIActivityIndicatorView(style: .gray)
        }
        activityIndicator.hidesWhenStopped = true
        return activityIndicator
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.loadPDF()
    }

    override func buildViewHierarchy() {
        view.addSubview(pdfView)
        view.addSubview(activityIndicator)
    }
    
    override func setupConstraints() {
        pdfView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        activityIndicator.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }

    override func configureViews() {
        title = Strings.ConfirmationLocalizable.Conditions.title
        navigationItem.rightBarButtonItem = closeButton
    }
}

// MARK: - @Objc Methods
@objc
private extension ConfirmationTermsViewController {
    func didTapCloseButton() {
        interactor.closeAction()
    }
}

// MARK: - ConfirmationTermsDisplaying

extension ConfirmationTermsViewController: ConfirmationTermsDisplaying {
    func showPDF(url: URL) {
        pdfView.load(URLRequest(url: url))
    }
    
    func startLoading() {
        view.bringSubviewToFront(activityIndicator)
        activityIndicator.startAnimating()
    }
    
    func stopLoading() {
        activityIndicator.stopAnimating()
    }
}

// MARK: - WKNavigationDelegate

extension ConfirmationTermsViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        interactor.handleWKNavigation(request: navigationAction.request, decisionHandler: decisionHandler)
    }
    
    // swiftlint:disable:next implicitly_unwrapped_optional
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        interactor.finishNavigationAction()
    }
}
