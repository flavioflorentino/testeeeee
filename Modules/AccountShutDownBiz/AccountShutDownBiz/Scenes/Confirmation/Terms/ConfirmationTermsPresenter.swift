import Foundation

protocol ConfirmationTermsPresenting: AnyObject {
    var viewController: ConfirmationTermsDisplaying? { get set }
    func displayLoading()
    func stopLoading()
    func displayPDF(url: URL)
    func didNextStep(action: ConfirmationTermsAction)
}

final class ConfirmationTermsPresenter {
    private let coordinator: ConfirmationTermsCoordinating
    weak var viewController: ConfirmationTermsDisplaying?

    init(coordinator: ConfirmationTermsCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - ConfirmationTermsPresenting
extension ConfirmationTermsPresenter: ConfirmationTermsPresenting {
    func displayLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
    
    func displayPDF(url: URL) {
        viewController?.showPDF(url: url)
    }
    
    func didNextStep(action: ConfirmationTermsAction) {
        coordinator.perform(action: action)
    }
}
