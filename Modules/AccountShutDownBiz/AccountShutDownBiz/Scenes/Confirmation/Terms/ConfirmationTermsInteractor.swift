import Foundation
import WebKit

protocol ConfirmationTermsInteracting: AnyObject {
    func loadPDF()
    func closeAction()
    func handleWKNavigation(request: URLRequest, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void)
    func finishNavigationAction()
}

final class ConfirmationTermsInteractor {
    private let service: ConfirmationTermsServicing
    private let presenter: ConfirmationTermsPresenting

    init(service: ConfirmationTermsServicing, presenter: ConfirmationTermsPresenting) {
        self.service = service
        self.presenter = presenter
    }
}

// MARK: - ConfirmationTermsInteracting
extension ConfirmationTermsInteractor: ConfirmationTermsInteracting {
    func loadPDF() {
        guard let url = service.url else { return }
        presenter.displayLoading()
        presenter.displayPDF(url: url)
    }
    
    func closeAction() {
        presenter.didNextStep(action: .dismiss)
    }
    
    func handleWKNavigation(request: URLRequest, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        guard request.url == service.url else {
            decisionHandler(.cancel)
            return
        }
        decisionHandler(.allow)
    }
    
    func finishNavigationAction() {
        presenter.stopLoading()
    }
}
