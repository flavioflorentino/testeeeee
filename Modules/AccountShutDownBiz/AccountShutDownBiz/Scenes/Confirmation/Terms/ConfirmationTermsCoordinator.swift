import UIKit

enum ConfirmationTermsAction {
    case dismiss
}

protocol ConfirmationTermsCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ConfirmationTermsAction)
}

final class ConfirmationTermsCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - ConfirmationTermsCoordinating
extension ConfirmationTermsCoordinator: ConfirmationTermsCoordinating {
    func perform(action: ConfirmationTermsAction) {
        switch action {
        case .dismiss:
            viewController?.dismiss(animated: true)
        }
    }
}
