import Core
import Foundation

protocol ConfirmationTermsServicing {
    var url: URL? { get }
}

final class ConfirmationTermsService: ConfirmationTermsServicing {
    // Mocked URL
    let url = URL(string: "https://www.qualitychess.co.uk/ebooks/AGameofQueens-excerpt.pdf")
}
