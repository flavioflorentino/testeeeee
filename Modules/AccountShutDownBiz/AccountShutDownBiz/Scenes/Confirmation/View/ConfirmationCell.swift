import Foundation
import SnapKit
import UI
import UIKit

private extension ConfirmationCell.Layout {
    enum DotView {
        static let size = CGSize(width: 8, height: 8)
    }
}

final class ConfirmationCell: UITableViewCell {
    fileprivate enum Layout { }
    
    private lazy var dotView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.branding600.color
        view.cornerRadius = CornerRadius.Style(rawValue: Layout.DotView.size.height / 2)
        return view
    }()
    
    private lazy var detailsLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Internal Methods

extension ConfirmationCell {
    func setupText(_ text: String) {
        detailsLabel.attributedText = text.attributedStringWith(
            normalFont: Typography.bodySecondary().font(),
            highlightFont: Typography.bodySecondary(.highlight).font(),
            normalColor: Colors.grayscale850.color,
            highlightColor: Colors.grayscale850.color,
            underline: false
        )
    }
}

// MARK: - ViewConfiguration

extension ConfirmationCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(dotView)
        contentView.addSubview(detailsLabel)
    }
    
    func setupConstraints() {
        dotView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.size.equalTo(Layout.DotView.size)
        }
        
        detailsLabel.snp.makeConstraints {
            $0.top.equalTo(dotView).offset(-Spacing.base00)
            $0.leading.equalTo(dotView.snp.trailing).offset(Spacing.base02)
            $0.trailing.bottom.equalToSuperview().offset(-Spacing.base02)
        }
    }
}
