import Foundation
import SnapKit
import UI
import UIKit

protocol ConfirmationCheckViewDelegate: AnyObject {
    func checkboxTapped(isEnabled: Bool)
    func conditionsTapped()
}

final class ConfirmationCheckView: UIView {
    private typealias Localizable = Strings.ConfirmationLocalizable.Checkbox
    weak var delegate: ConfirmationCheckViewDelegate?
    
    private lazy var checkbox: CheckboxButton = {
        let checkbox = CheckboxButton()
        checkbox.isSelected = false
        checkbox.tintColor = Colors.branding600.color
        checkbox.addTarget(self, action: #selector(checkboxTapped), for: .valueChanged)
        return checkbox
    }()
    
    private lazy var conditionText: NSAttributedString = {
        let text = NSMutableAttributedString()
        
        let attributes: [NSAttributedString.Key: Any] = [
            .font: Typography.bodySecondary().font(),
            .foregroundColor: Colors.grayscale800.color
        ]
        
        let linkAttributes: [NSAttributedString.Key: Any] = [
            .font: Typography.bodySecondary().font(),
            .link: ""
        ]
        
        text.append(NSAttributedString(string: Localizable.normal, attributes: attributes))
        text.append(NSAttributedString(string: Localizable.link, attributes: linkAttributes))
        return text
    }()
    
    private lazy var conditionTextView: UITextView = {
        let textView = UITextView()
        textView.linkTextAttributes = [
            .foregroundColor: Colors.branding600.color,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        textView.attributedText = conditionText
        textView.delegate = self
        textView.isScrollEnabled = false
        textView.isEditable = false
        textView.isUserInteractionEnabled = true
        return textView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - ViewConfiguration

extension ConfirmationCheckView: ViewConfiguration {
    func configureViews() {
        clipsToBounds = true
        layer.borderWidth = 1
        layer.borderColor = Colors.grayscale100.color.cgColor
        layer.cornerRadius = CornerRadius.light
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapCheckbox))
        tapGesture.cancelsTouchesInView = true
        addGestureRecognizer(tapGesture)
    }
    
    func buildViewHierarchy() {
        addSubview(checkbox)
        addSubview(conditionTextView)
    }
    
    func setupConstraints() {
        checkbox.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.centerY.equalToSuperview()
        }
        
        conditionTextView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base02)
            $0.leading.equalTo(checkbox.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
    }
}

// MARK: - @Objc Methods

@objc
private extension ConfirmationCheckView {
    func tapCheckbox() {
        checkbox.isSelected.toggle()
        checkboxTapped()
    }
    
    func checkboxTapped() {
        delegate?.checkboxTapped(isEnabled: checkbox.isSelected)
    }
}

// MARK: - UITextViewDelegate

extension ConfirmationCheckView: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        delegate?.conditionsTapped()
        return false
    }
}
