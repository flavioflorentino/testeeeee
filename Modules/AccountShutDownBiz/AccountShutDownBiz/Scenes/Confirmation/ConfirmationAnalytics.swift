import AnalyticsModule

enum ConfirmationAnalytics: AnalyticsKeyProtocol, Equatable {
    case acceptedConditions
    case readConditions
    case confirm
    case quit
    
    func event() -> AnalyticsEventProtocol {
        var name = "Shut Down Account - "
        switch self {
        case .acceptedConditions:
            name += "Accepted Conditions"
        case .readConditions:
            name += "General Conditions"
        case .confirm:
            name += "Confirmed"
        case .quit:
            name += "Quited"
        }
        return AnalyticsEvent(name, providers: [.mixPanel])
    }
}
