import UI
import UIKit

protocol ConfirmationDisplaying: AnyObject {
    func displayTips(_ tips: [String])
    func set(isLoading: Bool)
    func setConfirmButton(isEnabled: Bool)
}

private extension ConfirmationViewController.Layout {
    enum TableView {
        static let estimatedRowHeight: CGFloat = 200
    }
}

final class ConfirmationViewController: ViewController<ConfirmationInteracting, UIView> {
    private typealias Localizable = Strings.ConfirmationLocalizable
    fileprivate enum Layout { }
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.alwaysBounceVertical = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.isUserInteractionEnabled = true
        return scrollView
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.attributedText = Localizable.subtitle.attributedStringWith(
            normalFont: Typography.bodyPrimary().font(),
            highlightFont: Typography.bodyPrimary(.highlight).font(),
            normalColor: Colors.grayscale850.color,
            highlightColor: Colors.grayscale850.color,
            underline: false)
        return label
    }()
    
    private lazy var dataSource: TableViewDataSource<Int, String> = {
        let dataSource = TableViewDataSource<Int, String>(view: tableView)
        dataSource.add(section: 0)
        dataSource.itemProvider = { tableView, indexPath, text -> ConfirmationCell? in
            let cellIdentifier = ConfirmationCell.identifier
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ConfirmationCell
            cell?.setupText(text)
            return cell
        }
        return dataSource
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.isScrollEnabled = false
        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = Layout.TableView.estimatedRowHeight
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.register(ConfirmationCell.self, forCellReuseIdentifier: ConfirmationCell.identifier)
        return tableView
    }()
    
    private lazy var checkView: ConfirmationCheckView = {
        let checkView = ConfirmationCheckView()
        checkView.delegate = self
        return checkView
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.Button.confirm, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.isEnabled = false
        button.addTarget(self, action: #selector(confirmButtonTouched), for: .touchUpInside)
        return button
    }()
    
    private lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.Button.cancel, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(cancelButtonTouched), for: .touchUpInside)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.fetchTips()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let height = tableView.contentSize.height
        tableView.snp.updateConstraints {
            $0.height.equalTo(height)
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        title = Localizable.title
        
        tableView.dataSource = dataSource
    }

    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(subtitleLabel)
        contentView.addSubview(tableView)
        contentView.addSubview(checkView)
        contentView.addSubview(confirmButton)
        contentView.addSubview(cancelButton)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        contentView.snp.makeConstraints {
            $0.leading.trailing.equalTo(view)
            $0.height.greaterThanOrEqualTo(view.compatibleSafeArea.height)
            $0.edges.equalToSuperview()
        }
        
        subtitleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        tableView.snp.makeConstraints {
            $0.top.equalTo(subtitleLabel.snp.bottom).offset(Spacing.base04)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(0)
        }
        
        checkView.snp.makeConstraints {
            $0.top.greaterThanOrEqualTo(tableView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        confirmButton.snp.makeConstraints {
            $0.top.equalTo(checkView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        cancelButton.snp.makeConstraints {
            $0.top.equalTo(confirmButton.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base02)
        }
    }
}

// MARK: - ConfirmationDisplaying

extension ConfirmationViewController: ConfirmationDisplaying {
    func displayTips(_ tips: [String]) {
        dataSource.add(items: tips, to: 0)
    }
    
    func set(isLoading: Bool) {
        isLoading ? confirmButton.startLoading() : confirmButton.stopLoading()
    }
    
    func setConfirmButton(isEnabled: Bool) {
        confirmButton.isEnabled = isEnabled
    }
}

// MARK: - @Objc Methods
@objc
private extension ConfirmationViewController {
    func confirmButtonTouched() {
        interactor.continueAction()
    }
    
    func cancelButtonTouched() {
        interactor.cancelAction()
    }
}

// MARK: - ConfirmationCheckViewDelegate

extension ConfirmationViewController: ConfirmationCheckViewDelegate {
    func checkboxTapped(isEnabled: Bool) {
        interactor.checkboxAction(isEnabled: isEnabled)
    }
    
    func conditionsTapped() {
        interactor.conditionsAction()
    }
}

// MARK: - StyledNavigationDisplayable

extension ConfirmationViewController: StyledNavigationDisplayable {
    var navigationBarStyle: DefaultNavigationStyle {
        ClearNavigationStyle(prefersLargeTitle: true)
    }
}
