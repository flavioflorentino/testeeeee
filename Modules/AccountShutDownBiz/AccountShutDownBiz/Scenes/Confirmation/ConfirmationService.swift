import Core
import Foundation

protocol ConfirmationServicing {
    func fetchTips(completion: @escaping(Result<[String], Error>) -> Void)
}

final class ConfirmationService { }

// MARK: - ConfirmationServicing
extension ConfirmationService: ConfirmationServicing {
    func fetchTips(completion: @escaping (Result<[String], Error>) -> Void) {
        let bundle = AccountShutDownBizResources.resourcesBundle
        guard let filepath = bundle.url(forResource: "confirmation_tips", withExtension: ".json") else {
            completion(.failure(ApiError.bodyNotFound))
            return
        }
        
        do {
            let data = try Data(contentsOf: filepath)
            let tips = try JSONDecoder().decode([String].self, from: data)
            completion(.success(tips))
        } catch {
            completion(.failure(error))
        }
    }
}
