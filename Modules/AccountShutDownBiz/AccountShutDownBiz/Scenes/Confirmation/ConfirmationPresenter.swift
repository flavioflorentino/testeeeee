import Foundation

protocol ConfirmationPresenting: AnyObject {
    var viewController: ConfirmationDisplaying? { get set }
    func presentTips(_ tips: [String])
    func present(isLoading: Bool)
    func setConfirmButton(isEnabled: Bool)
    func didNextStep(action: ConfirmationAction)
}

final class ConfirmationPresenter {
    private let coordinator: ConfirmationCoordinating
    weak var viewController: ConfirmationDisplaying?

    init(coordinator: ConfirmationCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - ConfirmationPresenting
extension ConfirmationPresenter: ConfirmationPresenting {
    func presentTips(_ tips: [String]) {
        viewController?.displayTips(tips)
    }
    
    func present(isLoading: Bool) {
        viewController?.set(isLoading: isLoading)
    }
    
    func setConfirmButton(isEnabled: Bool) {
        viewController?.setConfirmButton(isEnabled: isEnabled)
    }
    
    func didNextStep(action: ConfirmationAction) {
        coordinator.perform(action: action)
    }
}
