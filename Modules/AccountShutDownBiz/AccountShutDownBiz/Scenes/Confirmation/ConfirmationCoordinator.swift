import UIKit

enum ConfirmationAction {
    case cancel
    case conditions
}

protocol ConfirmationCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ConfirmationAction)
}

final class ConfirmationCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - ConfirmationCoordinating
extension ConfirmationCoordinator: ConfirmationCoordinating {
    func perform(action: ConfirmationAction) {
        switch action {
        case .cancel:
            viewController?.navigationController?.popToRootViewController(animated: true)
        case .conditions:
            let termsController = ConfirmationTermsFactory.make()
            let navController = UINavigationController(rootViewController: termsController)
            viewController?.navigationController?.present(navController, animated: true)
        }
    }
}
