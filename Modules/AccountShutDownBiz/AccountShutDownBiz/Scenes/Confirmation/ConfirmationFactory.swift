import UIKit

enum ConfirmationFactory {
    static func make() -> ConfirmationViewController {
        let container = DependencyContainer()
        let service: ConfirmationServicing = ConfirmationService()
        let coordinator: ConfirmationCoordinating = ConfirmationCoordinator()
        let presenter: ConfirmationPresenting = ConfirmationPresenter(coordinator: coordinator)
        let interactor = ConfirmationInteractor(service: service, presenter: presenter, dependencies: container)
        let viewController = ConfirmationViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
