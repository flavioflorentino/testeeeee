import AnalyticsModule
import Foundation

protocol ConfirmationInteracting: AnyObject {
    func fetchTips()
    func cancelAction()
    func checkboxAction(isEnabled: Bool)
    func conditionsAction()
    func continueAction()
}

final class ConfirmationInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let service: ConfirmationServicing
    private let presenter: ConfirmationPresenting

    init(service: ConfirmationServicing, presenter: ConfirmationPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - ConfirmationInteracting
extension ConfirmationInteractor: ConfirmationInteracting {
    func fetchTips() {
        service.fetchTips { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .success(let tips):
                self.presenter.presentTips(tips)
            case .failure:
                break
            }
        }
    }
    
    func cancelAction() {
        dependencies.analytics.log(ConfirmationAnalytics.quit)
        presenter.didNextStep(action: .cancel)
    }
    
    func checkboxAction(isEnabled: Bool) {
        if isEnabled {
            dependencies.analytics.log(ConfirmationAnalytics.acceptedConditions)
        }
        
        presenter.setConfirmButton(isEnabled: isEnabled)
    }
    
    func conditionsAction() {
        dependencies.analytics.log(ConfirmationAnalytics.readConditions)
        presenter.didNextStep(action: .conditions)
    }
    
    func continueAction() {
        dependencies.analytics.log(ConfirmationAnalytics.confirm)
        presenter.present(isLoading: true)
        // Ask for password
    }
}
