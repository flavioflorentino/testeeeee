import UIKit

enum ReasonAction: Equatable {
    case confirm(reason: ShutDownReason, text: String?)
}

protocol ReasonCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ReasonAction)
}

final class ReasonCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - ReasonCoordinating
extension ReasonCoordinator: ReasonCoordinating {
    func perform(action: ReasonAction) {
    }
}
