import AnalyticsModule
import Foundation

protocol ReasonInteracting: AnyObject {
    func setButtonHandler(_ buttonHandler: ReasonButtonHandler?)
    func loadReasons()
    func setSelectedReason(index: Int?)
}

final class ReasonInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let service: ReasonServicing
    private let presenter: ReasonPresenting
    private var buttonHandler: ReasonButtonHandler?
    
    private var reasons: [ShutDownReason] = []
    private var selectedIndex: Int?

    init(service: ReasonServicing, presenter: ReasonPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - ReasonInteracting

extension ReasonInteractor: ReasonInteracting {
    func setButtonHandler(_ buttonHandler: ReasonButtonHandler?) {
        self.buttonHandler = buttonHandler
    }
    
    func loadReasons() {
        service.fetchReasons { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let reasons):
                self.reasons = reasons.shuffled()
                self.presenter.presentReasons(self.reasons)
            case .failure:
                break
            }
        }
    }
    
    func setSelectedReason(index: Int?) {
        selectedIndex = index
        buttonHandler?.updateButton(hasReason: index != nil)
    }
}

// MARK: - Private Methods

private extension ReasonInteractor {
    func logContinueEvent(reasonType: String, reasonDescription: String?) {
        let event = ReasonAnalytics.shutDownNext(reasonType: reasonType, reasonDescription: reasonDescription)
        dependencies.analytics.log(event)
    }
}

// MARK: - ReasonTableFooterInteractorDelegate

extension ReasonInteractor: ReasonTableFooterInteractorDelegate {
    func continueAction(text: String?) {
        if let index = selectedIndex, let reason = reasons[safe: index] {
            let action = ReasonAction.confirm(reason: reason, text: text)
            logContinueEvent(reasonType: reason.text, reasonDescription: text)
            presenter.didNextStep(action: action)
            return
        }
        
        guard let text = text else { return }
        let reason = ShutDownReason.other()
        let action = ReasonAction.confirm(reason: reason, text: text)
        logContinueEvent(reasonType: reason.text, reasonDescription: text)
        presenter.didNextStep(action: action)
    }
}
