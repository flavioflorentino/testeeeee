import SnapKit
import UI
import UIKit

protocol ReasonDisplaying: AnyObject {
    func displayReasons(_ reasons: [ShutDownReason])
}

private extension ReasonViewController.Layout {
    enum TableView {
        static let estimatedRowHeight: CGFloat = 80
    }
}

final class ReasonViewController: ViewController<ReasonInteracting, UIView> {
    private typealias Localizable = Strings.ReasonLocalizable
    fileprivate enum Layout { }
    
    private lazy var isDisplayingKeyboard = false
    
    private lazy var tableHeaderContainer = UIView()
    private lazy var tableHeaderView = ReasonTableHeaderView()
    
    private lazy var tableFooterContainer = UIView()
    private lazy var tableFooterViewController = ReasonTableFooterFactory.make()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(ReasonCell.self, forCellReuseIdentifier: ReasonCell.identifier)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = Layout.TableView.estimatedRowHeight
        tableView.separatorStyle = .singleLine
        tableView.separatorInset = .zero
        tableView.delegate = self
        
        tableView.tableHeaderView = tableHeaderContainer
        tableView.tableFooterView = tableFooterContainer
        tableView.keyboardDismissMode = .onDrag
        
        let closeKeyboardGesture = UITapGestureRecognizer(target: self, action: #selector(closeKeyboard))
        closeKeyboardGesture.cancelsTouchesInView = false
        tableView.addGestureRecognizer(closeKeyboardGesture)
        
        return tableView
    }()
    
    private lazy var dataSource: TableViewDataSource<Int, ShutDownReason> = {
        let dataSource = TableViewDataSource<Int, ShutDownReason>(view: tableView)
        dataSource.add(section: 0)
        dataSource.itemProvider = { tableView, indexPath, reason -> ReasonCell? in
            let cellIdentifier = ReasonCell.identifier
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ReasonCell
            cell?.setText(reason.text)
            return cell
        }
        return dataSource
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        title = Localizable.title
        
        interactor.setButtonHandler(tableFooterViewController.interactor)
        interactor.loadReasons()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        tableView.updateTableHeaderView(with: tableHeaderView)
        tableView.updateTableFooterView(with: tableFooterViewController.view)
    }

    override func buildViewHierarchy() {
        view.addSubview(tableView)
        
        tableHeaderContainer.addSubview(tableHeaderView)
        
        addChild(tableFooterViewController)
        tableFooterContainer.addSubview(tableFooterViewController.view)
        tableFooterViewController.delegate = self
        tableFooterViewController.didMove(toParent: self)
    }
    
    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        tableHeaderView.snp.makeConstraints {
            $0.width.equalTo(tableView)
        }
        
        tableFooterViewController.view.snp.makeConstraints {
            $0.width.equalTo(tableView)
        }
    }

    override func configureViews() {
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        }
        
        setupKeyboardObservers()
        
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        tableView.dataSource = dataSource
    }
}

// MARK: - Private Methods

private extension ReasonViewController {
    func setupKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}

// MARK: - Objc Methodss

@objc
private extension ReasonViewController {
    func closeKeyboard() {
        view.endEditing(true)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        let frameKey = UIResponder.keyboardFrameEndUserInfoKey
        guard
            let keyboardRect = (notification.userInfo?[frameKey] as? NSValue)?.cgRectValue,
            !isDisplayingKeyboard else { return }
        
        let height = keyboardRect.height
        
        tableView.snp.remakeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.bottom.equalToSuperview().inset(height)
        }
        
        tableView.setContentOffset(CGPoint(x: 0, y: tableView.contentSize.height), animated: true)
        
        isDisplayingKeyboard = true
    }
    
    func keyboardWillHide(_ notification: Notification) {
        tableView.snp.remakeConstraints {
            $0.edges.equalToSuperview()
        }
        
        isDisplayingKeyboard = false
    }
}

// MARK: - ReasonDisplaying

extension ReasonViewController: ReasonDisplaying {
    func displayReasons(_ reasons: [ShutDownReason]) {
        dataSource.add(items: reasons, to: 0)
    }
}

// MARK: - UITableViewDelegate

extension ReasonViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        guard let cell = tableView.cellForRow(at: indexPath) else { return nil }
        
        if cell.isSelected {
            tableView.deselectRow(at: indexPath, animated: false)
            interactor.setSelectedReason(index: nil)
            return nil
        }
        
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        interactor.setSelectedReason(index: indexPath.row)
    }
}

// MARK: - ReasonTableFooterViewControllerDelegate

extension ReasonViewController: ReasonTableFooterViewControllerDelegate {
    func shouldUpdateFooterSize() {
        tableView.updateTableFooterView(with: tableFooterViewController.view)
    }
}

// MARK: - StyledNavigationDisplayable

extension ReasonViewController: StyledNavigationDisplayable {
    public var navigationBarStyle: DefaultNavigationStyle {
        ClearNavigationStyle(prefersLargeTitle: true)
    }
}
