import Foundation

protocol ReasonPresenting: AnyObject {
    var viewController: ReasonDisplaying? { get set }
    func presentReasons(_ reasons: [ShutDownReason])
    func didNextStep(action: ReasonAction)
}

final class ReasonPresenter {
    private let coordinator: ReasonCoordinating
    weak var viewController: ReasonDisplaying?

    init(coordinator: ReasonCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - ReasonPresenting
extension ReasonPresenter: ReasonPresenting {
    func presentReasons(_ reasons: [ShutDownReason]) {
        viewController?.displayReasons(reasons)
    }
    
    func didNextStep(action: ReasonAction) {
        coordinator.perform(action: action)
    }
}
