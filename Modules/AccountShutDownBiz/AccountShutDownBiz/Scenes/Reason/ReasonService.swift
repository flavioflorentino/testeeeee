import Core
import Foundation

struct ShutDownReason: Decodable, Equatable {
    let id: String
    let text: String
    
    private enum CodingKeys: String, CodingKey {
        case id = "reason_type"
        case text = "reason_description"
    }
    
    static func other() -> ShutDownReason {
        ShutDownReason(id: "other", text: "Outro motivo")
    }
}

protocol ReasonServicing {
    func fetchReasons(completion: @escaping(Result<[ShutDownReason], Error>) -> Void)
}

final class ReasonService { }

// MARK: - ReasonServicing
extension ReasonService: ReasonServicing {
    func fetchReasons(completion: @escaping (Result<[ShutDownReason], Error>) -> Void) {
        let bundle = AccountShutDownBizResources.resourcesBundle
        guard let path = bundle.path(forResource: "shutdown_reason_list", ofType: "json") else {
            completion(.failure(ApiError.serverError))
            return
        }
        
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            let reasons = try JSONDecoder().decode([ShutDownReason].self, from: data)
            completion(.success(reasons))
        } catch {
            completion(.failure(error))
        }
    }
}
