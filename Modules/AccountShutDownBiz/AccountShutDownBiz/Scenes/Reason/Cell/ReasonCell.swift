import SnapKit
import UI
import UIKit

private extension ReasonCell.Layout {
    enum Checkbox {
        static let size = CGSize(width: 24, height: 24)
    }
}

final class ReasonCell: UITableViewCell {
    fileprivate enum Layout { }
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [checkbox, reasonContainerView])
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var checkbox: CheckboxButton = {
        let checkbox = CheckboxButton()
        checkbox.isSelected = false
        checkbox.isUserInteractionEnabled = false
        checkbox.tintColor = Colors.branding600.color
        return checkbox
    }()
    
    private lazy var reasonContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var reasonLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale850.color)
        return label
    }()
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        buildLayout()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        checkbox.isSelected = selected
    }
}

// MARK: - ViewConfiguration

extension ReasonCell: ViewConfiguration {
    func configureViews() {
        selectionStyle = .none
    }
    
    func buildViewHierarchy() {
        reasonContainerView.addSubview(reasonLabel)
        
        contentView.addSubview(stackView)
    }
    
    func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        checkbox.snp.makeConstraints {
            $0.size.equalTo(Layout.Checkbox.size)
        }
        
        reasonLabel.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}

// MARK: - Internal Methods

extension ReasonCell {
    func setText(_ text: String) {
        reasonLabel.text = text
    }
}
