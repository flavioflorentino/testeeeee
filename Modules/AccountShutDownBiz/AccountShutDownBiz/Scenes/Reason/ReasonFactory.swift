import UIKit

enum ReasonFactory {
    static func make() -> ReasonViewController {
        let container = DependencyContainer()
        let service: ReasonServicing = ReasonService()
        let coordinator: ReasonCoordinating = ReasonCoordinator()
        let presenter: ReasonPresenting = ReasonPresenter(coordinator: coordinator)
        let interactor = ReasonInteractor(service: service, presenter: presenter, dependencies: container)
        let viewController = ReasonViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
