import SnapKit
import UI
import UIKit

final class ReasonTableHeaderView: UIView {
    private typealias Localizable = Strings.ReasonLocalizable
    
    private lazy var reasonTitleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.Reason.title
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale850.color)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale300.color
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - ViewConfiguration

extension ReasonTableHeaderView: ViewConfiguration {
    func configureViews() {
        backgroundColor = .clear
    }
    
    func buildViewHierarchy() {
        addSubview(reasonTitleLabel)
        addSubview(separatorView)
    }
    
    func setupConstraints() {
        reasonTitleLabel.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        separatorView.snp.makeConstraints {
            $0.top.equalTo(reasonTitleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.bottom.equalToSuperview()
            $0.height.equalTo(0.5)
        }
    }
}
