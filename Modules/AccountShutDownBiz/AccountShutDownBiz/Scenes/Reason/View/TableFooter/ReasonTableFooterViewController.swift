import SnapKit
import UI
import UIKit

protocol ReasonTableFooterViewControllerDelegate: AnyObject {
    func shouldUpdateFooterSize()
}

protocol ReasonTableFooterDisplaying: AnyObject {
    func displayTextCount(_ text: String)
    func setTextViewStatus(isValid: Bool)
    func setButtonStatus(isEnabled: Bool)
}

final class ReasonTableFooterViewController: ViewController<ReasonTableFooterInteracting & ReasonButtonHandler, UIView> {
    private typealias Localizable = Strings.ReasonLocalizable.Footer
    
    weak var delegate: ReasonTableFooterViewControllerDelegate?
    
    private lazy var separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale300.color
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        
        let color = Colors.grayscale850.color
        
        let boldAttributes: [NSAttributedString.Key: Any] = [
            .font: Typography.bodyPrimary(.highlight).font(),
            .foregroundColor: color
        ]
        
        let normalAttributes: [NSAttributedString.Key: Any] = [
            .font: Typography.bodyPrimary().font(),
            .foregroundColor: color
        ]
        
        let boldText = NSAttributedString(string: Localizable.Title.bold, attributes: boldAttributes)
        let normalText = NSAttributedString(string: Localizable.Title.normal, attributes: normalAttributes)
        
        let text = NSMutableAttributedString()
        text.append(boldText)
        text.append(normalText)
        
        label.attributedText = text
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var textView: TextView = {
        let textView = TextView()
        textView.backgroundColor = .clear
        textView.placeholderText = Localizable.TextView.placeholder
        textView.isSeparatorVisible = true
        textView.returnKeyType = .done
        textView.tintColor = Colors.branding600.color
        textView.font = Typography.bodyPrimary().font()
        textView.textColor = Colors.grayscale850.color
        textView.isScrollEnabled = false
        textView.isUserInteractionEnabled = true
        textView.delegate = self
        return textView
    }()
    
    private lazy var tipCountStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [tipLabel, countLabel])
        stackView.axis = .horizontal
        stackView.spacing = Spacing.base00
        stackView.alpha = 0
        return stackView
    }()
    
    private lazy var tipLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(CaptionLabelStyle())
            .with(\.textColor, Colors.grayscale700.color)
            .with(\.numberOfLines, 1)
        label.text = Localizable.TextView.placeholder
        label.setContentCompressionResistancePriority(.required, for: .horizontal)
        return label
    }()
    
    private lazy var countLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(CaptionLabelStyle())
            .with(\.textColor, Colors.grayscale700.color)
            .with(\.textAlignment, .right)
        label.text = Localizable.TextView.count
        return label
    }()
    
    private lazy var continueButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.continueButton, for: .normal)
        button.buttonStyle(PrimaryButtonStyle(size: .default))
        button.isEnabled = false
        button.addTarget(self, action: #selector(continueButtonTouched), for: .touchUpInside)
        return button
    }()
    
    private lazy var tellMoreLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(CaptionLabelStyle())
            .with(\.textColor, Colors.critical900.color)
        label.text = Localizable.TextView.tellMore
        label.isHidden = true
        return label
    }()
    
    override func buildViewHierarchy() {
        view.addSubview(separatorView)
        view.addSubview(titleLabel)
        view.addSubview(tipCountStackView)
        view.addSubview(textView)
        view.addSubview(tellMoreLabel)
        view.addSubview(continueButton)
    }
    
    override func setupConstraints() {
        separatorView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.height.equalTo(0.5)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(separatorView).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        tipCountStackView.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalTo(titleLabel)
        }
        
        textView.snp.makeConstraints {
            $0.top.equalTo(tipCountStackView.snp.bottom).offset(Spacing.base00)
            $0.leading.trailing.equalTo(titleLabel)
        }
        
        tellMoreLabel.snp.makeConstraints {
            $0.top.equalTo(textView.snp.bottom).offset(Spacing.base00)
            $0.leading.trailing.equalTo(titleLabel)
        }
        
        continueButton.snp.makeConstraints {
            $0.top.equalTo(tellMoreLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalTo(titleLabel)
            $0.bottom.equalToSuperview().offset(-Spacing.base02)
        }
    }

    override func configureViews() { 
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}

// MARK: - Private Methods

private extension ReasonTableFooterViewController {
    func animateToggleTipsVisibility() {
        UIView.animate(withDuration: 0.3, animations: toggleTipsVisibility)
    }
    
    func toggleTipsVisibility() {
        let isHidden = tipCountStackView.alpha == 0
        tipCountStackView.alpha = isHidden ? 1 : 0
    }
}

// MARK: - Objc Methods

@objc
private extension ReasonTableFooterViewController {
    func continueButtonTouched() {
        interactor.continueAction()
    }
}

// MARK: - UITextViewDelegate

extension ReasonTableFooterViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard let oldText = textView.text else { return true }
        
        // Check if return button was tapped
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        
        let nsText = oldText as NSString
        let newText = nsText.replacingCharacters(in: range, with: text)
        
        guard text.count <= 1_000 else { return false }
        
        interactor.updateText(newText)
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        guard tipCountStackView.alpha == 0 else { return }
        animateToggleTipsVisibility()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        guard textView.text.isEmpty else { return }
        animateToggleTipsVisibility()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        delegate?.shouldUpdateFooterSize()
    }
}

// MARK: - ReasonTableFooterDisplaying

extension ReasonTableFooterViewController: ReasonTableFooterDisplaying {
    func displayTextCount(_ text: String) {
        countLabel.text = text
    }
    
    func setTextViewStatus(isValid: Bool) {
        textView.tintColor = isValid ? Colors.branding600.color : Colors.critical900.color
        tellMoreLabel.isHidden = isValid
    }
    
    func setButtonStatus(isEnabled: Bool) {
        continueButton.isEnabled = isEnabled
    }
}
