import Foundation
import UI
import UIKit

protocol ReasonTableFooterPresenting: AnyObject {
    var viewController: ReasonTableFooterDisplaying? { get set }
    func presentTextCount(_ count: Int)
    func setTextIsValid(_ isValid: Bool)
    func setButtonStatus(isEnabled: Bool)
}

final class ReasonTableFooterPresenter {
    weak var viewController: ReasonTableFooterDisplaying?
}

// MARK: - ReasonTableFooterPresenting
extension ReasonTableFooterPresenter: ReasonTableFooterPresenting {
    func presentTextCount(_ count: Int) {
        let text = "\(count)/1000"
        viewController?.displayTextCount(text)
    }
    
    func setTextIsValid(_ isValid: Bool) {
        viewController?.setTextViewStatus(isValid: isValid)
        viewController?.setButtonStatus(isEnabled: isValid)
    }
    
    func setButtonStatus(isEnabled: Bool) {
        viewController?.setButtonStatus(isEnabled: isEnabled)
    }
}
