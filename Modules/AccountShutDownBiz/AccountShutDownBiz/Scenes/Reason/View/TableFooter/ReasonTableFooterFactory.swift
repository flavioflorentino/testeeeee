import UIKit

enum ReasonTableFooterFactory {
    static func make() -> ReasonTableFooterViewController {
        let presenter: ReasonTableFooterPresenting = ReasonTableFooterPresenter()
        let interactor = ReasonTableFooterInteractor(presenter: presenter)
        let viewController = ReasonTableFooterViewController(interactor: interactor)

        presenter.viewController = viewController

        return viewController
    }
}
