import Foundation

protocol ReasonTableFooterInteractorDelegate: AnyObject {
    func continueAction(text: String?)
}

protocol ReasonButtonHandler: AnyObject {
    func updateButton(hasReason: Bool)
}

protocol ReasonTableFooterInteracting: AnyObject {
    func updateText(_ text: String)
    func continueAction()
}

final class ReasonTableFooterInteractor {
    private let presenter: ReasonTableFooterPresenting
    private var hasReason = false
    private var text: String?
    
    weak var delegate: ReasonTableFooterInteractorDelegate?

    init(presenter: ReasonTableFooterPresenting) {
        self.presenter = presenter
    }
}

// MARK: - ReasonTableFooterInteracting

extension ReasonTableFooterInteractor: ReasonTableFooterInteracting {
    func updateText(_ text: String) {
        self.text = text
        presenter.presentTextCount(text.count)
        updateButton()
    }
    
    func continueAction() {
        delegate?.continueAction(text: text)
    }
}

// MARK: - ReasonButtonHandler

extension ReasonTableFooterInteractor: ReasonButtonHandler {
    func updateButton(hasReason: Bool) {
        self.hasReason = hasReason
        updateButton()
    }
}

// MARK: - Private Methods

private extension ReasonTableFooterInteractor {
    func updateButton() {
        guard let text = text, !text.isEmpty else {
            presenter.setTextIsValid(true)
            presenter.setButtonStatus(isEnabled: hasReason)
            return
        }
        
        let hasEnoughCharacteres = text.count >= 25
        presenter.setButtonStatus(isEnabled: hasEnoughCharacteres)
        presenter.setTextIsValid(hasEnoughCharacteres)
    }
}
