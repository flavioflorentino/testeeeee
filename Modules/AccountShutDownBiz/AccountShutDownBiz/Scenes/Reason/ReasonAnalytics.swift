import AnalyticsModule

enum ReasonAnalytics: AnalyticsKeyProtocol {
    case shutDownNext(reasonType: String, reasonDescription: String?)
    
    func event() -> AnalyticsEventProtocol {
        switch self {
        case let .shutDownNext(reasonType, reasonDescription):
            let name = "Shut Down Account - Next"
            let properties: [String: Any] = [
                "reasonType": reasonType,
                "reasonDescription": reasonDescription ?? ""
            ]
            return AnalyticsEvent(name, properties: properties, providers: [.mixPanel])
        }
    }
}
