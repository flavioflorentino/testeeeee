import Foundation

protocol ConclusionInteracting: AnyObject {
    func doSomething()
}

final class ConclusionInteractor {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let service: ConclusionServicing
    private let presenter: ConclusionPresenting

    init(service: ConclusionServicing, presenter: ConclusionPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - ConclusionInteracting
extension ConclusionInteractor: ConclusionInteracting {
    func doSomething() {
        presenter.displaySomething()
    }
}
