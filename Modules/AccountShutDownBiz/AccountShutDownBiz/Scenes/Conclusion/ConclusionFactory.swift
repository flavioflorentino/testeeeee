import UIKit

enum ConclusionFactory {
    static func make() -> ConclusionViewController {
        let container = DependencyContainer()
        let service: ConclusionServicing = ConclusionService(dependencies: container)
        let coordinator: ConclusionCoordinating = ConclusionCoordinator(dependencies: container)
        let presenter: ConclusionPresenting = ConclusionPresenter(coordinator: coordinator, dependencies: container)
        let interactor = ConclusionInteractor(service: service, presenter: presenter, dependencies: container)
        let viewController = ConclusionViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
