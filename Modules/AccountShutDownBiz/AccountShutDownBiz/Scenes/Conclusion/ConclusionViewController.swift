import UI
import UIKit

protocol ConclusionDisplaying: AnyObject {
    func displaySomething()
}

private extension ConclusionViewController.Layout {
    enum Size {
        static let imageHeight: CGFloat = 90.0
    }
}

final class ConclusionViewController: ViewController<ConclusionInteracting, UIView> {
    fileprivate enum Layout { }

    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.doSomething()
    }

    override func buildViewHierarchy() { }
    
    override func setupConstraints() { }

    override func configureViews() { }
}

// MARK: - ConclusionDisplaying
extension ConclusionViewController: ConclusionDisplaying {
    func displaySomething() { }
}
