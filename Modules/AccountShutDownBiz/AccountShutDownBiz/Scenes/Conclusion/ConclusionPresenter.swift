import Foundation

protocol ConclusionPresenting: AnyObject {
    var viewController: ConclusionDisplaying? { get set }
    func displaySomething()
    func didNextStep(action: ConclusionAction)
}

final class ConclusionPresenter {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let coordinator: ConclusionCoordinating
    weak var viewController: ConclusionDisplaying?

    init(coordinator: ConclusionCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
}

// MARK: - ConclusionPresenting
extension ConclusionPresenter: ConclusionPresenting {
    func displaySomething() {
        viewController?.displaySomething()
    }
    
    func didNextStep(action: ConclusionAction) {
        coordinator.perform(action: action)
    }
}
