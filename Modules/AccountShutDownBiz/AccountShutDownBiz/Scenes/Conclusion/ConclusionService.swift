import Core
import Foundation

protocol ConclusionServicing {
}

final class ConclusionService {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - ConclusionServicing
extension ConclusionService: ConclusionServicing {
}
