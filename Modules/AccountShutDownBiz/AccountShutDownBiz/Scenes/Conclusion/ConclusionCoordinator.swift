import UIKit

enum ConclusionAction {
}

protocol ConclusionCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ConclusionAction)
}

final class ConclusionCoordinator {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - ConclusionCoordinating
extension ConclusionCoordinator: ConclusionCoordinating {
    func perform(action: ConclusionAction) {
    }
}
