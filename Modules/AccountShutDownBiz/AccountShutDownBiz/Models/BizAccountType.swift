import Foundation

// todo - Puxar o biz account type do modulo de seller info manager quando for criado
public enum BizAccountType: String {
    case receivementAccount = "receivement_account"
    case paymentAccount = "payment_account"
    case unknown
}
