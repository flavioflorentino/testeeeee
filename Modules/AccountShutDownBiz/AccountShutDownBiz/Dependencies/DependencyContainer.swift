import AnalyticsModule
import Core
import Foundation

typealias Dependencies = HasNoDependency
    & HasMainQueue
    & HasAnalytics 

final class DependencyContainer: Dependencies {
    lazy var mainQueue = DispatchQueue.main
    lazy var analytics: AnalyticsProtocol = Analytics.shared
}
