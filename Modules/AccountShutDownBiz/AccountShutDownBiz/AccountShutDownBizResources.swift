import Foundation

// swiftlint:disable convenience_type
final class AccountShutDownBizResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: AccountShutDownBizResources.self).url(forResource: "AccountShutDownBizResources", withExtension: "bundle") else {
            return Bundle(for: AccountShutDownBizResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: AccountShutDownBizResources.self)
    }()
}
