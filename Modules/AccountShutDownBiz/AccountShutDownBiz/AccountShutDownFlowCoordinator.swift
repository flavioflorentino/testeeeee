import UIKit
import UI

public protocol AccountShutDownFlowCoordinating: AnyObject {
    func perform()
    func close()
}

public final class AccountShutDownFlowCoordinator: Coordinating {
    public var childViewController: [UIViewController] = []
    public var viewController: UIViewController?
    
    private let navigationController: UINavigationController
    
    public init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    public func start(_ accountType: BizAccountType) {
        let controller = AccountShutDownTipsFactory.make(accountType)
        
        navigationController.viewControllers = [controller]
    }
}
