import AccountShutDownBiz
import UI
import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        if let windowScene = scene as? UIWindowScene {
            let window = UIWindow(windowScene: windowScene)
            let navController = UINavigationController()
            navController.navigationBar.setup(appearance: NavigationBarAppearanceTransparent())
            window.rootViewController = navController
            AccountShutDownFlowCoordinator(navigationController: navController).start(.paymentAccount)
            self.window = window
            window.makeKeyAndVisible()
        }
    }
}
