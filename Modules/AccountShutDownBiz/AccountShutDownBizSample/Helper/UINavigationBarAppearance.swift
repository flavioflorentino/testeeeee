import UI
import UIKit

extension UINavigationBar {
    func setup(appearance: NavigationBarAppearance) {
        shadowImage = appearance.shadowImage
        tintColor = appearance.tintColor
        barTintColor = appearance.barTintColor
        barStyle = appearance.barStyle
        isTranslucent = appearance.isTranslucent
        titleTextAttributes = appearance.titleTextAttributes
        setBackgroundImage(appearance.backgroundImage, for: .default)
    }
}

protocol NavigationBarAppearance {
    var shadowImage: UIImage? { get }
    var tintColor: UIColor { get }
    var barTintColor: UIColor { get }
    var barStyle: UIBarStyle { get }
    var isTranslucent: Bool { get }
    var titleTextAttributes: [NSAttributedString.Key: Any] { get }
    var backgroundImage: UIImage? { get }
}

extension NavigationBarAppearance {
    var shadowImage: UIImage? {
        nil
    }
    
    var tintColor: UIColor {
        Colors.branding600.color
    }
    
    var barTintColor: UIColor {
        Colors.black.color
    }
    
    var barStyle: UIBarStyle {
        .default
    }
    
    var isTranslucent: Bool {
        true
    }
    
    var titleTextAttributes: [NSAttributedString.Key: Any] {
        [:]
    }
    
    var backgroundImage: UIImage? {
        nil
    }
}

struct NavigationBarAppearanceTransparent: NavigationBarAppearance {
    let shadowImage: UIImage? = UIImage()
    let isTranslucent: Bool = true
    let backgroundImage: UIImage? = UIImage()
}
