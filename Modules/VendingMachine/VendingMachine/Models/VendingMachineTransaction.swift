import Foundation

struct VendingMachineTransaction: Codable {
    var session: VendingMachineSession
}
