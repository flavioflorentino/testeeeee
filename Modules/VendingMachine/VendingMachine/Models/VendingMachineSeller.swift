import Foundation

public struct VendingMachineSeller: Decodable {
    public let storeId: String
    public let sellerId: String
}
