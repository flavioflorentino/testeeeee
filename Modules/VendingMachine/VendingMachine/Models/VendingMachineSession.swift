import Foundation

struct VendingMachineSession: Codable {
    let id: String
    var status: VendingMachineStatus
    var order: VendingMachineOrder?
    let machine: VendingMachine?
    let target: String
}
