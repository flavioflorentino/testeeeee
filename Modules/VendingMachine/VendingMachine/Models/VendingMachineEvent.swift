import Foundation

struct VendingMachineEvent: Decodable {
    let sessionId: String
    let seller: VendingMachineSeller
}
