import Foundation

struct VendingMachine: Codable {
    let type: String
    let name: String
    let location: String
}
