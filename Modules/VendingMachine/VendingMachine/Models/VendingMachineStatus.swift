import Foundation

enum VendingMachineStatus: String, Codable {
    case waitingMachineLink = "waiting_machine_link"
    case waitingSelection = "waiting_selection"
    case waitingAuthorization = "waiting_authorization"
    case cancelled
    case authorized
    case declined
    case success
    case rolledBack = "rolled_back"
}
