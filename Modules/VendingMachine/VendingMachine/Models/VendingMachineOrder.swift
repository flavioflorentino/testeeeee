import Foundation

struct VendingMachineOrder: Codable {
    let items: [VendingMachineItem]
    let totalValue: Decimal
    var paymentId: String?
}

public struct VendingMachineItem: Codable {
    public let name: String
    public let unitValue: Decimal
    public let quantity: Int
    public let upcCode: String
}
