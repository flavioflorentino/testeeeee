import Core
import Foundation
import Starscream

public final class ApiWebSocket<Model: Decodable> {
    private let endpoint: ApiEndpointExposable
    private var currentSession: WebSocketTaskable?
    
    public typealias Completion<Model> = (Result<WebSocketEvent<Model>, ApiError>) -> Void

    public init(endpoint: ApiEndpointExposable) {
        self.endpoint = endpoint
    }
    
    public func open(completion: @escaping Completion<Model>) {
        let request: URLRequest
        do {
            try request = makeRequest()
        } catch {
            completion(.failure(ApiError.unknown(error)))
            return
        }
        
        let session: WebSocketTaskable = WebSocket(request: request)
        session.open()
        session.received { result in
            completion(result)
        }
        currentSession = session
    }
    
    public func close() {
        currentSession?.close()
    }
    
    public func send<T: Encodable>(message: T) throws {
        try currentSession?.send(data: message)
    }
    
    private func makeRequest() throws -> URLRequest {
        let urlComponent = URLComponents(string: endpoint.absoluteStringUrl)

        guard let url = urlComponent?.url else {
            throw ApiError.malformedRequest("Unable to parse url")
        }

        var request = URLRequest(url: url)
        request.timeoutInterval = 5

        #if DEBUG
        print("[WebSocket request] \(endpoint.method.rawValue) at \(url.absoluteString)")
        #endif

        return request
    }
}
