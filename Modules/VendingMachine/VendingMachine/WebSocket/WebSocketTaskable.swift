import Core
import Foundation
import Starscream

public enum WebSocketEvent<Model> {
    case connect([String: String])
    case disconnect(String, WebSocketCloseCode)
    case object(Model)
}

public enum WebSocketCloseCode: UInt16 {
    case normalClosure = 1000
    case goingAway
    case protocolError
    case unsupportedData
    case noStatusReceived = 1005
    case abnormalClosure = 1007
    case policyViolation
    case messageTooBig
}

public protocol WebSocketTaskable {
    func open()
    func close()
    func send(message: String)
    func send<Model: Encodable> (data: Model) throws
    func received<Model: Decodable>(completionHandler: @escaping (Result<WebSocketEvent<Model>, ApiError>) -> Void)
}

extension WebSocket: WebSocketTaskable {    
    public func open() {
        connect()
    }
    
    public func close() {
        disconnect()
    }
    
    public func send(message: String) {
        #if DEBUG
        print("[websocket message] \(message)")
        #endif
        write(string: message, completion: nil)
    }
    
    public func send<Model: Encodable> (
        data: Model
    ) throws {
        let encoder = JSONEncoder()
        encoder.keyEncodingStrategy = .convertToSnakeCase
        let encodedData = try encoder.encode(data)
        
        guard let message = String(data: encodedData, encoding: .utf8) else {
            return
        }
        
        send(message: message)
    }
    
    public func received<Model: Decodable>(
        completionHandler: @escaping (Result<WebSocketEvent<Model>, ApiError>) -> Void
    ) {
        onEvent = { [weak self] event in
            #if DEBUG
            print("[websocket event] \(event)")
            #endif
            guard
                let self = self,
                let result: Result<WebSocketEvent<Model>, ApiError> = self.handle(event: event)
                else {
                return
            }
            completionHandler(result)
        }
    }
    
    private func handle<Model: Decodable>(event: Starscream.WebSocketEvent) -> Result<WebSocketEvent<Model>, ApiError>? {
        switch event {
        case .connected(let protocols):
            return .success(.connect(protocols))
        case let .disconnected(status, statusCode):
            return .success(.disconnect(status, WebSocketCloseCode(rawValue: statusCode) ?? .goingAway))
        case .text(let message):
            let data = message.data(using: .utf8)
            return handle(data: data ?? Data())
        case .binary(let data):
            return handle(data: data)
        case .error(let error):
            return .failure(ApiError.decodeError(error ?? ApiError.unknown(nil)))
        default:
            return nil
        }
    }
    
    private func handle<Model: Decodable>(data: Data) -> Result<WebSocketEvent<Model>, ApiError>? {
        Result {
            WebSocketEvent<Model>.object(try self.decode(data: data))
        }
        .mapError {
            ApiError.decodeError($0)
        }
    }
    
    private func decode<Model: Decodable>(data: Data) throws -> Model {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return try decoder.decode(Model.self, from: data)
    }
}
