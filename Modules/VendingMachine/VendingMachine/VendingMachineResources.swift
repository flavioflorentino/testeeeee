import Foundation

// swiftlint:disable convenience_type
final class VendingMachineResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: VendingMachineResources.self).url(forResource: "VendingMachineResources", withExtension: "bundle") else {
            return Bundle(for: VendingMachineResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: VendingMachineResources.self)
    }()
}
