import Foundation
import Core

enum VendingMachineEndpoint {
    case wakeMachine(String)
    case status(String)
}

struct VendingMachineBody: Encodable {
    let target: String
}

extension VendingMachineEndpoint: ApiEndpointExposable {
    typealias Dependencies = HasKeychainManager
    var baseURL: URL {
        switch self {
        case .wakeMachine:
            #if DEBUG
            let dependencies: Dependencies = DependencyContainer()
            if let userDefinedHost = dependencies.keychain.getData(key: KeychainKey.hostBaseUrl), let userUrl = URL(string: userDefinedHost) {
                return userUrl
            }
            #endif
            
            guard let apiUrl = Environment.apiUrl, let url = URL(string: apiUrl) else {
                fatalError("You need to define the api url")
            }
            return url
        case .status:
            let ppvmWebSocketApiUrl: String? = Environment.get(
                "PPVM_WEBSOCKET_API_URL",
                bundle: Bundle(identifier: "com.picpay.VendingMachine") ?? Bundle.main
            )
            guard let api = ppvmWebSocketApiUrl, let apiUrl = URL(string: api) else {
                fatalError("Could not parse PPVM WebSocket url")
            }
            return apiUrl
        }
    }
    
    var path: String {
        switch self {
        case .wakeMachine:
            return "vending-machine/session/start"
        case .status(let sessionId):
            return "status/\(sessionId)"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .wakeMachine:
            return .post
        case .status:
            return .get
        }
    }
    
    var body: Data? {
        guard case let VendingMachineEndpoint.wakeMachine(target) = self else {
            return nil
        }
        
        let rawBody = VendingMachineBody(target: target)
        let encoder = JSONEncoder()
        let encodedBody = try? encoder.encode(rawBody)
        return encodedBody
    }
    
    var isTokenNeeded: Bool {
        guard case .wakeMachine = self else {
            return false
        }
        return true
    }
}
