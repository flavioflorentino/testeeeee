import UIKit

enum MachineStateAction {
    case close
    case payment(
        seller: VendingMachineSeller,
        totalValue: Double,
        items: [VendingMachineItem],
        inputs: VendingMachinePaymentInputs?
    )
    case cancelPayment
}

protocol MachineStateCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: MachineStateCoordinatingDelegate? { get set }
    func perform(action: MachineStateAction)
}

public protocol MachineStateCoordinatingDelegate: AnyObject {
    func paymentViewController(
        seller: VendingMachineSeller,
        totalValue: Double,
        items: [VendingMachineItem],
        paymentInputs: VendingMachinePaymentInputs?
    ) -> UIViewController?
}

final class MachineStateCoordinator: MachineStateCoordinating {
    weak var delegate: MachineStateCoordinatingDelegate?
    weak var viewController: UIViewController?
    
    func perform(action: MachineStateAction) {
        switch action {
        case .close:
            viewController?.dismiss(animated: true)
        case let .payment(seller, totalValue, items, inputs):
            guard viewController?.presentedViewController == nil,
                let paymentVC = delegate?.paymentViewController(
                    seller: seller,
                    totalValue: totalValue,
                    items: items,
                    paymentInputs: inputs
                )
            else {
                return
            }
            viewController?.present(paymentVC, animated: true)
        case .cancelPayment:
            viewController?.presentedViewController?.dismiss(animated: true)
        }
    }
}
