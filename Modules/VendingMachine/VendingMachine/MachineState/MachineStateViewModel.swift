import Core
import Foundation

public protocol MachineStateViewModelInputs: AnyObject {
    func wakeMachine()
    func getStatus(fromSession sessionId: String)
    func cancel()
    func close()
}

public protocol VendingMachinePaymentInputs: AnyObject {
    func paymentSuccess(transactionId: String)
    func paymentCancel()
}

final class MachineStateViewModel {
    typealias Dependencies = HasMainQueue
    
    private let dependencies: Dependencies
    
    private let service: MachineStateServicing
    private let presenter: MachineStatePresenting
    
    private let targetCode: String
    private var seller: VendingMachineSeller?
    private var currentTransaction: VendingMachineTransaction?
    
    private var workItem: DispatchWorkItem?
    
    init(
        dependencies: Dependencies,
        targetCode: String,
        service: MachineStateServicing,
        presenter: MachineStatePresenting
    ) {
        self.dependencies = dependencies
        self.service = service
        self.presenter = presenter
        self.targetCode = targetCode
    }
    
    func start(machineSession sessionId: String, seller: VendingMachineSeller) {
        self.seller = seller
        self.getStatus(fromSession: sessionId)
    }
}

extension MachineStateViewModel: MachineStateViewModelInputs {
    func wakeMachine() {
        service.postWakeMachine(target: targetCode) { result in
            switch result {
            case .success(let vendingMachine):
                self.start(machineSession: vendingMachine.sessionId, seller: vendingMachine.seller)
            case .failure:
                self.presenter.displayFailure(
                    title: Strings.MachineState.MachineFailure.title,
                    message: Strings.MachineState.MachineFailure.subtitle
                )
            }
        }
    }
    
    func getStatus(fromSession sessionId: String) {
        service.openStatusStream(sessionId: sessionId) { [weak self] result in
            self?.setRetry(for: sessionId)
            
            switch result {
            case .success(let event):
                self?.handle(event: event)
            case .failure:
                self?.presenter.displayFailure(
                    title: Strings.MachineState.MachineFailure.title,
                    message: Strings.MachineState.MachineFailure.subtitle
                )
            }
        }
    }
    
    func cancel() {
        changeEvent(newStatus: .cancelled, paymentId: nil)
        close()
    }
    
    func close() {
        service.closeStatusStream()
        presenter.didNextStep(action: .close)
    }
    
    func setRetry(for sessionId: String, after time: DispatchTime = .now() + .seconds(10)) {
        workItem?.cancel()
        
        let requestWorkItem = DispatchWorkItem { [weak self] in
            #if DEBUG
            print("[websocket] Retrying...")
            #endif
            guard
                let seller = self?.seller,
                let status = self?.currentTransaction?.session.status
                else {
                    return
            }
            self?.retry(for: sessionId, withSeller: seller, andStatus: status)
        }
        
        workItem = requestWorkItem
        dependencies.mainQueue.asyncAfter(
            deadline: time,
            execute: requestWorkItem
        )
    }
    
    func retry(for sessionId: String, withSeller seller: VendingMachineSeller, andStatus status: VendingMachineStatus) {
        guard status != .waitingAuthorization else {
            return
        }
        
        service.closeStatusStream()
        start(machineSession: sessionId, seller: seller)
    }
}

private extension MachineStateViewModel {
    func handle(event: WebSocketEvent<VendingMachineTransaction>) {
        guard
            case let .object(newTransaction) = event,
            let seller = seller
            else {
                return
        }
        currentTransaction = newTransaction
        handle(transaction: newTransaction, seller: seller)
    }
    
    func handle(transaction: VendingMachineTransaction, seller: VendingMachineSeller) {
        switch transaction.session.status {
        case .waitingSelection, .authorized, .success:
            presenter.display(newStatus: transaction.session.status)
        case .waitingAuthorization:
            guard let order = transaction.session.order else {
                return
            }
            presenter.didNextStep(action:
                .payment(
                    seller: seller,
                    totalValue: NSDecimalNumber(decimal: order.totalValue).doubleValue,
                    items: order.items,
                    inputs: self
                )
            )
        case .rolledBack, .declined:
            presenter.displayFailure(
                title: Strings.MachineState.TransactionFailure.title,
                message: Strings.MachineState.TransactionFailure.subtitle
            )
        case .cancelled:
            presenter.displayFailure(
                title: Strings.MachineState.MachineFailure.title,
                message: Strings.MachineState.MachineFailure.subtitle
            )
        default:
            return
        }
    }
    
    func update(transaction: VendingMachineTransaction) {
        try? service.updateStatus(transaction: transaction)
    }
    
    func changeEvent(newStatus: VendingMachineStatus, paymentId: String?) {
        guard var newTransaction = currentTransaction else {
            return
        }
        
        newTransaction.session.status = newStatus
        
        if let paymentId = paymentId {
            newTransaction.session.order?.paymentId = paymentId
        }
        
        update(transaction: newTransaction)
    }
}

extension MachineStateViewModel: VendingMachinePaymentInputs {
    func paymentSuccess(transactionId: String) {
        changeEvent(newStatus: .authorized, paymentId: transactionId)
    }
    
    func paymentCancel() {
        dependencies.mainQueue.asyncAfter(deadline: .now() + .milliseconds(3)) {
            self.cancel()
        }
    }
}
