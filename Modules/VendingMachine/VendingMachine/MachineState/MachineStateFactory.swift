import Foundation

public enum MachineStateFactory {
    public static func make(
        withTarget code: String,
        _ delegate: MachineStateCoordinatingDelegate? = nil
    ) -> MachineStateViewController {
        let service: MachineStateServicing = MachineStateService(dependencies: DependencyContainer())
        let coordinator: MachineStateCoordinating = MachineStateCoordinator()
        let presenter: MachineStatePresenting = MachineStatePresenter(coordinator: coordinator)
        let viewModel = MachineStateViewModel(
            dependencies: DependencyContainer(),
            targetCode: code,
            service: service,
            presenter: presenter
        )
        let viewController = MachineStateViewController(viewModel: viewModel)
        
        coordinator.delegate = delegate
        coordinator.viewController = viewController
        presenter.viewController = viewController
        
        return viewController
    }
}
