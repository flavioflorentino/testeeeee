import UI
import UIKit

private extension MachineStateViewController.Layout {
    enum Margin {
        static let leading = Spacing.base05
        static let trailing = Spacing.base05
        static let top = Spacing.base08
    }
}

public final class MachineStateViewController: ViewController<MachineStateViewModelInputs, UIView> {
    fileprivate enum Layout { }
    
    // MARK: Views
    private lazy var connectingView = ConnectingView()
    
    private lazy var statusView: StatusView = {
        let statusView = StatusView()
        statusView.set(
            title: Strings.MachineState.AwaitingProductSelection.title,
            subtitle: Strings.MachineState.AwaitingProductSelection.subtitle,
            animation: "select-product"
        )
        return statusView
    }()
    
    // MARK: ViewController Lifecycle
    override public func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Palette.ppColorGrayscale000.color
        viewModel.wakeMachine()
    }
    
    override public func buildViewHierarchy() {
        view.addSubview(connectingView)
    }
    
    override public func configureViews() {
        title = Strings.MachineState.title
        navigationController?.navigationBar.tintColor = Palette.ppColorBranding300.color
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .cancel,
            target: self,
            action: #selector(cancel)
        )
    }
    
    override public func setupConstraints() {
        connectingView.layout {
            $0.top == view.topAnchor + Layout.Margin.top
            $0.bottom <= view.bottomAnchor
            $0.centerX == view.centerXAnchor
            $0.leading >= view.leadingAnchor + Layout.Margin.leading
            $0.trailing <= view.trailingAnchor - Layout.Margin.trailing
        }
    }
}

@objc
private extension MachineStateViewController {
    func cancel() {
        viewModel.cancel()
    }
    
    func close() {
        viewModel.close()
    }
    
    func displayStatusView() {
        guard !statusView.isDescendant(of: view) else {
            return
        }
        
        connectingView.removeFromSuperview()
        view.addSubview(statusView)
        statusView.layout {
            $0.top == view.topAnchor + Layout.Margin.top
            $0.bottom <= view.bottomAnchor
            $0.centerX == view.centerXAnchor
            $0.leading >= view.leadingAnchor + Layout.Margin.leading
            $0.trailing <= view.trailingAnchor - Layout.Margin.trailing
        }
    }
}

// MARK: View Model Outputs
extension MachineStateViewController: MachineStateDisplay {
    func displayStatus(title: String, subtitle: String, animation: String) {
        UIImpactFeedbackGenerator(style: .heavy).impactOccurred()
        displayStatusView()
        statusView.set(title: title, subtitle: subtitle, animation: animation)
    }
    
    func displayFailure(title: String, message: String) {
        guard presentedViewController == nil else {
            presentedViewController?.dismiss(animated: true) {
                self.displayFailure(title: title, message: message)
            }
            return
        }
        
        let popup = PopupViewController(title: title, description: message, preferredType: .text)
        let action = PopupAction(title: Strings.MachineState.Alert.Button.title, style: .fill) { [weak self] in
            popup.dismiss(animated: true) {
                self?.viewModel.close()
            }
        }
        popup.addAction(action)
        
        showPopup(popup)
    }
    
    func displayDoneButton() {
        removeCancelButton()
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .done,
            target: self,
            action: #selector(close)
        )
    }
    
    func removeCancelButton() {
        navigationItem.leftBarButtonItem = nil
    }
}
