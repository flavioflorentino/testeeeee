import Core
import Foundation

protocol MachineStatePresenting: AnyObject {
    var viewController: MachineStateDisplay? { get set }
    func display(newStatus: VendingMachineStatus)
    func displayFailure(title: String, message: String)
    func didNextStep(action: MachineStateAction)
}

final class MachineStatePresenter: MachineStatePresenting {
    private let coordinator: MachineStateCoordinating
    weak var viewController: MachineStateDisplay?

    init(coordinator: MachineStateCoordinating) {
        self.coordinator = coordinator
    }
    
    func display(newStatus: VendingMachineStatus) {
        switch newStatus {
        case .waitingSelection:
            viewController?.displayStatus(
                title: Strings.MachineState.AwaitingProductSelection.title,
                subtitle: Strings.MachineState.AwaitingProductSelection.subtitle,
                animation: "select-product"
            )
        case .authorized:
            viewController?.displayStatus(
                title: Strings.MachineState.OrderProcessing.title,
                subtitle: Strings.MachineState.OrderProcessing.subtitle,
                animation: "process-order"
            )
            viewController?.removeCancelButton()
        case .success:
            viewController?.displayStatus(
                title: Strings.MachineState.Success.title,
                subtitle: Strings.MachineState.Success.subtitle,
                animation: "take-product"
            )
            viewController?.displayDoneButton()
        default:
            return
        }
    }
    
    func displayFailure(title: String, message: String) {
        coordinator.perform(action: .cancelPayment)
        viewController?.displayFailure(title: title, message: message)
        viewController?.displayDoneButton()
    }
    
    func didNextStep(action: MachineStateAction) {
        coordinator.perform(action: action)
    }
}
