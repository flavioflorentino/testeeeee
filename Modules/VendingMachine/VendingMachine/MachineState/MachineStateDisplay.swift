import UIKit

protocol MachineStateDisplay: AnyObject {
    func displayStatus(title: String, subtitle: String, animation: String)
    func displayFailure(title: String, message: String)
    func removeCancelButton()
    func displayDoneButton()
}
