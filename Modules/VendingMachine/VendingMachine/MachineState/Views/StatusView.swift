import Lottie
import UI
import UIKit

private extension StatusView.Layout {
    enum Size {
        static let animation = CGSize(width: 126, height: 126)
    }
    
    enum Font {
        static let titleLabelFont = UIFont.systemFont(ofSize: 18, weight: .semibold)
        static let subtitleLabelFont = UIFont.systemFont(ofSize: 14, weight: .regular)
    }
}

public final class StatusView: UIView {
    fileprivate enum Layout {}
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var animationView: AnimationView = {
        let view = AnimationView()
        view.loopMode = .loop
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.titleLabelFont
        label.textAlignment = .center
        label.numberOfLines = 0
        label.textColor = Palette.ppColorGrayscale600.color
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.subtitleLabelFont
        label.textAlignment = .center
        label.numberOfLines = 0
        label.textColor = Palette.ppColorGrayscale500.color
        return label
    }()
    
    override public init(frame: CGRect = .zero) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension StatusView: ViewConfiguration {
    public func configureViews() {
        backgroundColor = Palette.ppColorGrayscale000.color
        titleLabel.text = Strings.MachineState.AwaitingProductSelection.title
        subtitleLabel.text = Strings.MachineState.AwaitingProductSelection.subtitle
    }
    
    public func buildViewHierarchy() {
        stackView.addArrangedSubview(animationView)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(subtitleLabel)
        addSubview(stackView)
    }
    
    public func setupConstraints() {
        stackView.layout {
            $0.leading == leadingAnchor
            $0.trailing == trailingAnchor
            $0.top == topAnchor
            $0.bottom == bottomAnchor
        }
        
        animationView.layout {
            $0.height == Layout.Size.animation.height
            $0.width == Layout.Size.animation.width
        }
    }
    
    public func set(title: String, subtitle: String?, animation: String?) {
        titleLabel.text = title
        subtitleLabel.text = subtitle
        
        animationView.stop()
        guard let animation = animation else {
            return
        }
        animationView.animation = Animation.named(animation, bundle: Bundle(for: StatusView.self))
        animationView.play()
    }
}
