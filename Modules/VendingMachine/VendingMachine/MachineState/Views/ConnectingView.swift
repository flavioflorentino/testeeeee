import Lottie
import UI
import UIKit

private extension ConnectingView.Layout {
    enum ItemSpacing {
        static let stackViewSpacing = Spacing.base02
    }
    
    enum Font {
        static let connectingLabelFont = UIFont.systemFont(ofSize: 18, weight: .semibold)
    }
    
    enum Size {
        static let animation = CGSize(width: 87, height: 67)
    }
}

public final class ConnectingView: UIView {
    fileprivate enum Layout {}
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = Layout.ItemSpacing.stackViewSpacing
        return stackView
    }()
    
    private lazy var vendingMachineImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.icoVendingMachine.image
        return imageView
    }()
    
    private lazy var wifiAnimationView: AnimationView = {
        let view = AnimationView(name: "wifi", bundle: Bundle(for: ConnectingView.self))
        view.loopMode = .loop
        return view
    }()
    
    private lazy var connectingLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.connectingLabelFont
        label.textAlignment = .center
        label.numberOfLines = 0
        label.textColor = Palette.ppColorGrayscale600.color
        return label
    }()
    
    override public init(frame: CGRect = .zero) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ConnectingView: ViewConfiguration {
    public func configureViews() {
        backgroundColor = Palette.ppColorGrayscale000.color
        connectingLabel.text = Strings.MachineState.connecting
        wifiAnimationView.play()
    }
    
    public func buildViewHierarchy() {
        stackView.addArrangedSubview(vendingMachineImageView)
        stackView.addArrangedSubview(wifiAnimationView)
        stackView.addArrangedSubview(connectingLabel)
        
        addSubview(stackView)
    }
    
    public func setupConstraints() {
        stackView.layout {
            $0.leading == leadingAnchor
            $0.trailing == trailingAnchor
            $0.top == topAnchor
            $0.bottom == bottomAnchor
        }
        
        wifiAnimationView.layout {
            $0.height == Layout.Size.animation.height
            $0.width == Layout.Size.animation.width
        }
    }
}
