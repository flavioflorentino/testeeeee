import Core
import Foundation

typealias CompletionWakeMachine = (Result<VendingMachineEvent, ApiError>) -> Void
typealias CompletionMachineStatus = (Result<WebSocketEvent<VendingMachineTransaction>, ApiError>) -> Void

protocol MachineStateServicing {
    func postWakeMachine(target: String, completion: @escaping CompletionWakeMachine)
    func openStatusStream(sessionId: String, completion: @escaping CompletionMachineStatus)
    func updateStatus(transaction: VendingMachineTransaction) throws
    func closeStatusStream()
}

final class MachineStateService: MachineStateServicing {
    typealias Dependencies = HasMainQueue
    
    private let dependencies: Dependencies
    private var webSocket: ApiWebSocket<VendingMachineTransaction>?
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func postWakeMachine(target: String, completion: @escaping CompletionWakeMachine) {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        Api<VendingMachineEvent>(
            endpoint: VendingMachineEndpoint.wakeMachine(target)).execute(jsonDecoder: decoder
        ) { result in
            self.dependencies.mainQueue.async {
                let mappedResult = result
                    .mapError { $0 as ApiError }
                    .map { $0.model }
                completion(mappedResult)
            }
        }
    }
    
    func openStatusStream(sessionId: String, completion: @escaping CompletionMachineStatus) {
        webSocket = ApiWebSocket<VendingMachineTransaction>(endpoint: VendingMachineEndpoint.status(sessionId))
        webSocket?.open { result in
            completion(result)
        }
    }
    
    func updateStatus(transaction: VendingMachineTransaction) throws {
        try webSocket?.send(message: transaction)
    }
    
    func closeStatusStream() {
        webSocket?.close()
    }
}
