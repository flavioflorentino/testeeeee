import Core
import Foundation

typealias AppDependencies =
    HasNoDependency &
    HasMainQueue &
    HasKeychainManager

final class DependencyContainer: AppDependencies {
    lazy var mainQueue = DispatchQueue.main
    lazy var keychain: KeychainManagerContract = KeychainManager()
}
