import Core
import XCTest
@testable import VendingMachine

/// Don't use this class for testing, use LoanDependencyContainerMock
final class VMDependencyContainer {
    init() {
        fatalError("Use a LoanDependencyContainerMock on test target")
    }
}

final class VMDependencyContainerMock: AppDependencies {
    lazy var mainQueue: DispatchQueue = resolve()
    lazy var keychain: KeychainManagerContract = resolve()

    
    private let dependencies: [Any]
    
    init(_ dependencies: Any...) {
        self.dependencies = dependencies
    }
}

extension VMDependencyContainerMock {
    func resolve<T>() -> T {
        let resolved = dependencies.compactMap { $0 as? T }
                
        switch resolved.first {
        case .none:
            fatalError("VMDependencyContainerMock could not resolve dependency: \(T.self)\n")
        case .some where resolved.count > 1:
            fatalError("VMDependencyContainerMock resolved mutiple dependencies for: \(T.self)\n")
        case .some(let mock):
            return mock
        }
    }
    
    func resolve() -> DispatchQueue {
        dependencies.compactMap { $0 as? DispatchQueue }.first ?? DispatchQueue(label: "VMDependencyContainerMock")
    }
    
    func resolve() -> KeychainManagerContract {
        dependencies.compactMap { $0 as? KeychainManagerContract }.first ?? KeychainManagerMock()
    }
}
