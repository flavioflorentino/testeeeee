import UIKit

class FakeViewController: UIViewController {
    private(set) var isDismissCalled: Bool = false
    private(set) var isPresentCalled: Bool = false
    
    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        isPresentCalled = true
        super.present(viewControllerToPresent, animated: flag, completion: completion)
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        isDismissCalled = true
        super.dismiss(animated: flag, completion: completion)
    }
}
