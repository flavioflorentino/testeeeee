import XCTest
@testable import VendingMachine

final class MachineStateCoordinatorTests: XCTestCase {
    private let fakeViewController = FakeViewController()
    private let delegate = CoordinatorDelegate()
    private lazy var sut: MachineStateCoordinator = {
        let sut =  MachineStateCoordinator()
        sut.delegate = delegate
        sut.viewController = fakeViewController
        return sut
    }()
    
    func testPerform_WhenCloseAction_ShouldDismiss() {
        sut.perform(action: .close)
        XCTAssertTrue(fakeViewController.isDismissCalled)
    }
    
    func testPerform_WhenPaymentAction_ShouldPresentPaymentScreen() {
        let seller = VendingMachineSeller(storeId: "123123", sellerId: "123123")
        let action: MachineStateAction = .payment(seller: seller, totalValue: 12.22, items: [], inputs: nil)
        
        sut.perform(action: action)
        XCTAssertTrue(fakeViewController.isPresentCalled)
    }
}

private final class CoordinatorDelegate: MachineStateCoordinatingDelegate {
    func paymentViewController(
        seller: VendingMachineSeller,
        totalValue: Double,
        items: [VendingMachineItem],
        paymentInputs: VendingMachinePaymentInputs?
    ) -> UIViewController? {
        return UIViewController()
    }
}
