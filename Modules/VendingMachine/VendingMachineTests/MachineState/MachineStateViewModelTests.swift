import XCTest
@testable import VendingMachine

final class MachineStateViewModelTests: XCTestCase {
    private let dependencies = VMDependencyContainerMock()
    
    private lazy var sut: MachineStateViewModel = {
        let sut = MachineStateViewModel(
            dependencies: dependencies,
            targetCode: "test_code",
            service: serviceMock,
            presenter: presenterSpy
        )
        return sut
    }()
    
    private var serviceMock = MachineStateServiceMock()
    private var presenterSpy = MachineStatePresenterSpy()

    func testWakeMachine_WhenReceiveSuccess_ShouldDisplayNewStatus() {
        serviceMock.isTestingSuccess = true
        sut.wakeMachine()
        XCTAssertEqual(serviceMock.didWokeMachine, 1)
    }
    
    func testWakeMachine_WhenReceiveFailure_ShouldHandleError() {
        serviceMock.isTestingSuccess = false
        sut.wakeMachine()
        XCTAssertEqual(presenterSpy.didDisplayError, 1)
    }
        
    func testStart_WhenReceiveWaitingSelection_ShouldDisplayNewStatus() {
        serviceMock.isTestingSuccess = true
        serviceMock.testingState = .waitingSelection
        sut.start(machineSession: "test_session", seller: VendingMachineSeller(storeId: "store_id", sellerId: "seller_id"))
        XCTAssertEqual(presenterSpy.didDisplayNewStatus, 1)
    }
    
    func testStart_WhenReceiveWaitingAuthorization_ShouldDisplayPayment() {
        serviceMock.isTestingSuccess = true
        serviceMock.testingState = .waitingAuthorization
        sut.start(machineSession: "test_session", seller: VendingMachineSeller(storeId: "store_id", sellerId: "seller_id"))
        XCTAssertEqual(presenterSpy.didPerformPaymentAction, 1)
    }
    
    func testStart_WhenReceiveDeclined_ShouldHandleError() {
        serviceMock.isTestingSuccess = true
        serviceMock.testingState = .declined
        sut.start(machineSession: "test_session", seller: VendingMachineSeller(storeId: "store_id", sellerId: "seller_id"))
        XCTAssertEqual(presenterSpy.didDisplayError, 1)
    }
    
    func testStart_WhenReceiveRolledBack_ShouldHandleError() {
        serviceMock.isTestingSuccess = false
        serviceMock.testingState = .rolledBack
        sut.start(machineSession: "test_session", seller: VendingMachineSeller(storeId: "store_id", sellerId: "seller_id"))
        XCTAssertEqual(presenterSpy.didDisplayError, 1)
    }
    
    func testStart_WhenReceiveCancelled_ShouldHandleError() {
        serviceMock.isTestingSuccess = true
        serviceMock.testingState = .cancelled
        sut.start(machineSession: "test_session", seller: VendingMachineSeller(storeId: "store_id", sellerId: "seller_id"))
        XCTAssertEqual(presenterSpy.didDisplayError, 1)
    }
    
    func testGetStatus_WhenReceiveFailure_ShouldHandleError() {
        serviceMock.isTestingSuccess = false
        sut.getStatus(fromSession: "test_case")
        XCTAssertEqual(presenterSpy.didDisplayError, 1)
    }
    
    func testClose_WhenCalled_ShouldCloseStreamAndDismiss() {
        sut.start(machineSession: "test", seller: VendingMachineSeller(storeId: "store_id", sellerId: "seller_id"))
        sut.close()
        XCTAssert(serviceMock.sentMessageStatus != .cancelled)
        XCTAssertEqual(serviceMock.didCloseStream, 1)
        XCTAssertEqual(presenterSpy.didPerformCloseAction, 1)
    }
    
    func testCancel_WhenCalled_ShouldSendCancel() {
        sut.start(machineSession: "test", seller: VendingMachineSeller(storeId: "store_id", sellerId: "seller_id"))
        sut.cancel()
        XCTAssert(serviceMock.sentMessageStatus == .cancelled)
    }
    
    func testPaymentSuccess_WhenCalled_ShouldSendTransactionId() {
        sut.start(machineSession: "test", seller: VendingMachineSeller(storeId: "store_id", sellerId: "seller_id"))
        sut.paymentSuccess(transactionId: "transaction_id")
        XCTAssert(serviceMock.sentMessageStatus == .authorized)
    }
    
    func testPaymentCancel_WhenCalled_ShouldSendCancel() {
        sut.start(machineSession: "test", seller: VendingMachineSeller(storeId: "store_id", sellerId: "seller_id"))
        sut.paymentCancel()
        let expectation = XCTestExpectation(description: "Cancel")
        dependencies.mainQueue.asyncAfter(deadline: .now() + .milliseconds(3)) {
            XCTAssert(self.serviceMock.sentMessageStatus == .cancelled)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 4.0)
    }
    
    func testSetRetry_WhenCalled_ShouldReconnect() {
        sut.start(machineSession: "test", seller: VendingMachineSeller(storeId: "store_id", sellerId: "seller_id"))
        sut.setRetry(for: "session_id", after: .now() + .milliseconds(1))
        let expectation = XCTestExpectation(description: "Reconnect")
        dependencies.mainQueue.asyncAfter(deadline: .now() + .milliseconds(1)) {
            XCTAssertEqual(self.serviceMock.didCloseStream, 1)
            XCTAssertEqual(self.serviceMock.didOpenStream, 2)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 2.0)
    }
    
    func testRetry_WhenCalled_ShouldReconnect() {
        sut.retry(for: "session_id", withSeller: VendingMachineSeller(storeId: "store_id", sellerId: "seller_id"), andStatus: .waitingSelection)
        XCTAssertEqual(self.serviceMock.didCloseStream, 1)
        XCTAssertEqual(self.serviceMock.didOpenStream, 1)
    }
    
    func testRetry_WhenReceiveWaitingAuthorization_ShouldNotReconnect() {
        sut.retry(for: "session_id", withSeller: VendingMachineSeller(storeId: "store_id", sellerId: "seller_id"), andStatus: .waitingAuthorization)
        XCTAssertEqual(self.serviceMock.didCloseStream, 0)
        XCTAssertEqual(self.serviceMock.didOpenStream, 0)
    }
}

private final class MachineStateServiceMock: MachineStateServicing {
    var isTestingSuccess: Bool = true
    var testingState: VendingMachineStatus = .waitingMachineLink
    
    private(set) var didWokeMachine: Int = 0
    private(set) var didCloseStream: Int = 0
    private(set) var didSendMessage: Int = 0
    private(set) var didOpenStream: Int = 0
    private(set) var sentMessageStatus: VendingMachineStatus = .waitingMachineLink
    
    func postWakeMachine(target: String, completion: @escaping CompletionWakeMachine) {
        if isTestingSuccess {
            didWokeMachine += 1
            let seller = VendingMachineSeller(storeId: "store_id", sellerId: "seller_id")
            completion(.success(
                VendingMachineEvent(sessionId: "test_id", seller: seller)
            ))
        } else {
            completion(.failure(.connectionFailure))
        }
    }
    
    func openStatusStream(sessionId: String, completion: @escaping CompletionMachineStatus) {
        didOpenStream += 1
        if isTestingSuccess {
            var order: VendingMachineOrder? = VendingMachineOrder(items: [], totalValue: 1.20, paymentId: "id")
            if testingState == .waitingMachineLink || testingState == .waitingSelection {
                order = nil
            }
            completion(.success(generate(status: testingState, order: order, paymentId: nil)))
        } else {
            completion(.failure(.connectionFailure))
        }
    }
    
    func updateStatus(transaction: VendingMachineTransaction) throws {
        sentMessageStatus = transaction.session.status
        didSendMessage += 1
    }
    
    func closeStatusStream() {
        didCloseStream += 1
    }
    
    private func generate(
        status: VendingMachineStatus,
        order: VendingMachineOrder?,
        paymentId: String?
    ) -> WebSocketEvent<VendingMachineTransaction> {
        let machineInfo = VendingMachine(type: "MM", name: "70B3D5CB8053", location: "Sede Verti")
        let session = VendingMachineSession(
            id: "test_id",
            status: status,
            order: order,
            machine: machineInfo,
            target: "vmpay://vmbox/1/SUCCESS"
        )
        let transaction = VendingMachineTransaction(
            session: session
        )
        return .object(transaction)
    }
}

private final class MachineStatePresenterSpy: MachineStatePresenting {
    weak var viewController: MachineStateDisplay?
    
    private(set) var didDisplayNewStatus: Int = 0
    func display(newStatus: VendingMachineStatus) {
        didDisplayNewStatus += 1
    }
    
    private(set) var didDisplayError: Int = 0
    func displayFailure(title: String, message: String) {
        didDisplayError += 1
    }
    
    private(set) var didPerformPaymentAction: Int = 0
    private(set) var didPerformCloseAction: Int = 0
    func didNextStep(action: MachineStateAction) {
        switch action {
        case .payment:
            didPerformPaymentAction += 1
        case .close:
            didPerformCloseAction += 1
        default:
            break
        }
    }
}
