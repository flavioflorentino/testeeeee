import XCTest
@testable import VendingMachine

final class MachineStatePresenterTests: XCTestCase {
    private lazy var sut: MachineStatePresenting = {
        let sut = MachineStatePresenter(coordinator: coordinatorSpy)
        sut.viewController = viewControllerSpy
        return sut
    }()
    
    private var viewControllerSpy = MachineStateViewControllerSpy()
    private var coordinatorSpy = MachineStateCoordinatorSpy()
    
    func testDidNextStep_WhenCloseAction_ShouldPerformDismiss() {
        sut.didNextStep(action: .close)
        XCTAssertEqual(coordinatorSpy.didPerformClose, 1)
    }
    
    func testDidNextStep_WhenPaymentAction_ShouldPresentPayment() {
        let seller = VendingMachineSeller(storeId: "store_id", sellerId: "seller_id")
        sut.didNextStep(action: .payment(seller: seller, totalValue: 1.20, items: [], inputs: nil))
        XCTAssertEqual(coordinatorSpy.didPerformPayment, 1)
    }
    
    func testDisplay_WhenWaitingSelectionStatus_ShouldUpdateView() {
        sut.display(newStatus: .waitingSelection)
        XCTAssert(viewControllerSpy.displayedStatus == Strings.MachineState.AwaitingProductSelection.title)
    }
    
    func testDisplay_WhenAuthorizedStatus_ShouldUpdateView() {
        sut.display(newStatus: .authorized)
        XCTAssert(viewControllerSpy.displayedStatus == Strings.MachineState.OrderProcessing.title)
        XCTAssertEqual(viewControllerSpy.didRemoveCancel, 1)
    }
    
    func testDisplay_WhenSuccessStatus_ShouldUpdateView() {
        sut.display(newStatus: .success)
        XCTAssert(viewControllerSpy.displayedStatus == Strings.MachineState.Success.title)
    }
    
    func testDisplayFailure_WhenCalled_ShouldDisplayAlert() {
        sut.displayFailure(title: "Error", message: "error message")
        XCTAssertEqual(viewControllerSpy.didDisplayError, 1)
        XCTAssertEqual(coordinatorSpy.didPerformCancelPayment, 1)
    }
}

private final class MachineStateViewControllerSpy: MachineStateDisplay {
    private(set) var displayedStatus: String = ""
    private(set) var didDisplayError: Int = 0
    private(set) var didDisplayDone: Int = 0
    private(set) var didRemoveCancel: Int = 0
    
    func displayStatus(title: String, subtitle: String, animation: String) {
        displayedStatus = title
    }
    
    func displayFailure(title: String, message: String) {
        didDisplayError += 1
    }
    
    func displayDoneButton() {
        didDisplayDone += 1
    }
    
    func removeCancelButton() {
        didRemoveCancel += 1
    }
}

private final class MachineStateCoordinatorSpy: MachineStateCoordinating {
    var delegate: MachineStateCoordinatingDelegate?
    
    weak var viewController: UIViewController?
    
    private(set) var didPerformClose: Int = 0
    private(set) var didPerformPayment: Int = 0
    private(set) var didPerformCancelPayment: Int = 0
    
    func perform(action: MachineStateAction) {
        switch action {
        case .close:
            didPerformClose += 1
        case .payment:
            didPerformPayment += 1
        case .cancelPayment:
            didPerformCancelPayment += 1
        }
    }
}
