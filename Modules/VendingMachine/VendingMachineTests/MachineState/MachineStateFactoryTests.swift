import XCTest
@testable import VendingMachine

final class MachineStateFactoryTests: XCTestCase {
    func testMake_WhenCalled_ShouldReturnViewController() {
        let viewController = MachineStateFactory.make(withTarget: "target_test")
        XCTAssertNotNil(viewController)
        XCTAssertNotNil(viewController.viewModel)
    }
}
