import Foundation
import UI
import AssetsKit

private extension SearchErrorView.Layout {
    static let topSpacerMinHeight = 40
    static let topSpacerMaxHeight = 72
    static let imageSize = 144
    static let bottomSpacerMinHeight = 26
}

public final class SearchErrorView: UIView {
    fileprivate enum Layout { }

    public struct EmptyStateViewModel {
        var title: String
        var description: String

        public init(title: String, description: String) {
            self.title = title
            self.description = description
        }
    }

    public enum Style {
        case noConnection
        case generalError
        case emptyState(viewModel: EmptyStateViewModel? = nil)
        case waitingInput
    }

    public var style: Style = .generalError {
        didSet {
            setNeedsLayoutUpdate()
        }
    }

    public var tryAgainAction: (() -> Void)?

    private lazy var imageView = UIImageView()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
            .with(\.textColor, .black())
        label.textAlignment = .center
        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.textColor, .grayscale600())
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()

    private lazy var tryAgainButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.tryAgain, for: .normal)
        button.addTarget(self, action: #selector(tryAgain), for: .touchUpInside)
        button.buttonStyle(PrimaryButtonStyle())
        return button
    }()

    private let topSpacerView = UIView()
    private let bottomSpacerView = UIView()

    public init() {
        super.init(frame: .zero)
        buildLayout()
        setNeedsLayoutUpdate()
    }

    @available(*, unavailable)
    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setNeedsLayoutUpdate() {
        switch style {
        case .generalError:
            imageView.image = Resources.Illustrations.iluNotFoundCircledBackground.image
            titleLabel.text = Strings.generalErrorTitle
            descriptionLabel.text = Strings.generalErrorDescription
            tryAgainButton.isHidden = false
        case .noConnection:
            imageView.image = Resources.Illustrations.iluNoConnectionCircledBackground.image
            titleLabel.text = Strings.noConnectionErrorTitle
            descriptionLabel.text = Strings.noConnectionErrorDescription
            tryAgainButton.isHidden = false
        case .emptyState(let viewModel):
            imageView.image = Resources.Illustrations.iluEmptyCircledBackground.image
            titleLabel.text = viewModel?.title ?? Strings.emptyStateTitle
            descriptionLabel.text = viewModel?.description ?? Strings.emptyStateDescription
            tryAgainButton.isHidden = true
        case .waitingInput:
            imageView.image = Resources.Illustrations.iluPersonErrorCircledBackground.image
            titleLabel.text = Strings.waitingInputTitle
            descriptionLabel.text = Strings.waitingInputDescription
            tryAgainButton.isHidden = true
        }
    }

    @objc
    private func tryAgain() {
        self.tryAgainAction?()
    }
}

extension SearchErrorView: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubviews(imageView, titleLabel, descriptionLabel, tryAgainButton, topSpacerView, bottomSpacerView)
    }

    public func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }

    public func setupConstraints() {
        topSpacerView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.height.equalTo(Layout.topSpacerMaxHeight).priority(.high)
            $0.height.equalTo(Layout.topSpacerMinHeight).priority(.medium)
            $0.centerX.equalToSuperview()
            $0.width.equalTo(10)
        }

        topSpacerView.snp.contentHuggingVerticalPriority = UILayoutPriority.defaultHigh.rawValue

        imageView.snp.makeConstraints {
            $0.top.equalTo(topSpacerView.snp.bottom)
            $0.centerX.equalToSuperview()
            $0.size.equalTo(Layout.imageSize)
        }

        imageView.snp.contentCompressionResistanceVerticalPriority = UILayoutPriority.required.rawValue

        titleLabel.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        titleLabel.snp.contentCompressionResistanceVerticalPriority = UILayoutPriority.required.rawValue

        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        descriptionLabel.snp.contentCompressionResistanceVerticalPriority = UILayoutPriority.required.rawValue

        bottomSpacerView.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom)
            $0.bottom.equalTo(tryAgainButton.snp.top)
            $0.height.greaterThanOrEqualTo(Layout.bottomSpacerMinHeight)
        }

        bottomSpacerView.snp.contentHuggingVerticalPriority = UILayoutPriority.defaultLow.rawValue
        bottomSpacerView.snp.contentCompressionResistanceVerticalPriority = UILayoutPriority.required.rawValue

        tryAgainButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base03)
        }
    }
}
