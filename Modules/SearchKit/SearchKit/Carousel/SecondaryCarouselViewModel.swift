import Foundation
import UIKit

public struct SecondaryCarouselCellViewModel: Equatable {
    public var title: String?
    public var highlight: String
    public var icon: UIImage

    public init(title: String?, highlight: String, icon: UIImage) {
        self.title = title
        self.highlight = highlight
        self.icon = icon
    }
}
