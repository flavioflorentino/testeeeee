import Foundation
import UIKit
import UI

extension SearchResultCarousel.Layout {
    static let itemWidth: CGFloat = 128
    static var cellHeight: CGFloat = 195
    static var collectionViewHeight: CGFloat {
        cellHeight + verticalPadding
    }
    static let verticalPadding: CGFloat = 16
    static let fullSizeCarouselHeight: CGFloat = 195
    static let compactSizeCarouselHeight: CGFloat = 142
}

public class SearchResultCarousel: UIView {
    fileprivate enum Layout { }
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout)
        collectionView.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        collectionView.register(SearchResultCarouselCell.self, forCellWithReuseIdentifier: SearchResultCarouselCell.identifier)
        collectionView.showsHorizontalScrollIndicator = false
        return collectionView
    }()

    private lazy var collectionViewLayout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: Layout.itemWidth, height: Layout.cellHeight)
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = Spacing.base02
        layout.sectionInset = UIEdgeInsets(top: 0, left: Spacing.base02, bottom: 0, right: Spacing.base02)
        return layout
    }()

    private lazy var dataSource: CollectionViewDataSource<Int, SearchResultCarouselCellViewModel> = {
        let dataSource = CollectionViewDataSource<Int, SearchResultCarouselCellViewModel>(view: collectionView)
        dataSource.add(section: 0)
        dataSource.itemProvider = { [weak self] _, indexPath, item in
            guard let self = self else { return nil }
            return self.cell(forIndexPath: indexPath, item: item, shouldRenderActionButton: self.shouldRenderActionButton)
        }
        return dataSource
    }()

    private var shouldRenderActionButton = true {
        didSet {
            updateCollectionViewHeight()
        }
    }

    public init() {
        super.init(frame: .zero)
        buildLayout()
    }

    private func cell(forIndexPath indexPath: IndexPath, item: SearchResultCarouselCellViewModel, shouldRenderActionButton: Bool) -> UICollectionViewCell? {
        let cell = collectionView.dequeueReusableCell(for: indexPath, cellType: SearchResultCarouselCell.self)
        cell.configure(viewModel: item, shouldRenderActionButton: shouldRenderActionButton)
        return cell
    }

    public func display(items: [SearchResultCarouselCellViewModel], shouldRenderActionButton: Bool) {
        self.shouldRenderActionButton = shouldRenderActionButton
        dataSource.update(items: items, from: 0)
        collectionView.dataSource = dataSource
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func updateCollectionViewHeight() {
        Layout.cellHeight = shouldRenderActionButton ? Layout.fullSizeCarouselHeight : Layout.compactSizeCarouselHeight
        collectionViewLayout.itemSize = CGSize(width: Layout.itemWidth, height: Layout.cellHeight)
        collectionView.snp.updateConstraints { $0.height.equalTo(Layout.collectionViewHeight) }
    }
}

extension SearchResultCarousel: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(collectionView)
    }

    public func setupConstraints() {
        collectionView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.height.equalTo(Layout.collectionViewHeight)
        }
    }
}
