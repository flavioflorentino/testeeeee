import Foundation
import UI
import UIKit

extension SecondaryCarousel.Layout {
    static let collectionViewHeight = 162
}

public final class SecondaryCarousel: UIView {
    fileprivate enum Layout { }

    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout)
        collectionView.register(cellType: SecondaryCarouselCell.self)
        collectionView.showsHorizontalScrollIndicator = false
        return collectionView
    }()

    private lazy var collectionViewLayout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = SecondaryCarouselCell.size
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = Spacing.base01
        layout.sectionInset = UIEdgeInsets(top: 0, left: Spacing.base02, bottom: 0, right: Spacing.base02)
        return layout
    }()

    private lazy var dataSource: CollectionViewDataSource<Int, SecondaryCarouselCellViewModel> = {
        let dataSource = CollectionViewDataSource<Int, SecondaryCarouselCellViewModel>(view: collectionView)
        dataSource.add(section: 0)
        dataSource.itemProvider = { _, indexPath, item in
            self.cell(forIndexPath: indexPath, item: item)
        }
        return dataSource
    }()

    public init() {
        super.init(frame: .zero)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func display(items: [SecondaryCarouselCellViewModel]) {
        dataSource.update(items: items, from: 0)
        collectionView.dataSource = dataSource
    }

    private func cell(forIndexPath indexPath: IndexPath, item: SecondaryCarouselCellViewModel) -> UICollectionViewCell? {
        let cell = collectionView.dequeueReusableCell(for: indexPath, cellType: SecondaryCarouselCell.self)
        cell.configure(viewModel: item)
        return cell
    }
}

extension SecondaryCarousel: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(collectionView)
    }

    public func setupConstraints() {
        collectionView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.height.equalTo(Layout.collectionViewHeight)
        }
    }

    public func configureViews() {
        collectionView.backgroundColor = Colors.backgroundPrimary.color
    }
}
