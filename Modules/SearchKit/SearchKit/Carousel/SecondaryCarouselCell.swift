import Foundation
import UI
import UIKit

extension SecondaryCarouselCell.Layout {
    static let size = CGSize(width: 128, height: 130)
    static let titleVerticalOffset = 28
}

public final class SecondaryCarouselCell: UICollectionViewCell {
    static var size: CGSize {
        Layout.size
    }

    fileprivate enum Layout { }

    private lazy var iconImageView = UIImageView()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.numberOfLines, 1)
        return label
    }()

    private lazy var highlightLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.numberOfLines, 1)
        return label
    }()

    override public init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func configure(viewModel: SecondaryCarouselCellViewModel) {
        self.titleLabel.text = viewModel.title
        self.highlightLabel.text = viewModel.highlight
        self.iconImageView.image = viewModel.icon
    }
}

extension SecondaryCarouselCell: ViewConfiguration {
    public func buildViewHierarchy() {
        contentView.addSubviews(titleLabel, highlightLabel, iconImageView)
    }

    public func setupConstraints() {
        iconImageView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.size.equalTo(Spacing.base03)
        }

        titleLabel.snp.makeConstraints {
            $0.top.equalTo(iconImageView.snp.bottom).offset(Layout.titleVerticalOffset)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        highlightLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base00)
            $0.leading.trailing.bottom.equalToSuperview().inset(Spacing.base02)
        }
    }

    public func configureViews() {
        contentView.cornerRadius = .light
        contentView.border = .light(color: .grayscale100())
    }
}
