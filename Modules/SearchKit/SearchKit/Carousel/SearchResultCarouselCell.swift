import Foundation
import UI
import AssetsKit

extension SearchResultCarouselCell.Layout {
    enum Size {
        static let backgroundHeaderHeight = 32
        static let profileImageViewHeight = 52
        static let titleLabelHeight = 39
        static let iconHeight = 16
        static let descriptionLabelHeight = 17
        static let profileImageCornerRadius: CGFloat = 12
    }
}

public final class SearchResultCarouselCell: UICollectionViewCell {
    fileprivate enum Layout { }

    private lazy var backgroundHeaderView = UIView()

    private lazy var profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.masksToBounds = true
        imageView.border = .light(color: .grayscale100())
        imageView.layer.cornerRadius = Layout.Size.profileImageCornerRadius
        return imageView
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale950())
        label.numberOfLines = 2
        label.textAlignment = .center
        return label
    }()

    private let distanceContainer = UIView()

    private lazy var descriptionIcon: UIImageView = {
        let icon = UIImageView()
        icon.image = Resources.Icons.icoLocationPoint.image
        return icon
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle()).with(\.textColor, .grayscale500())
        return label
    }()

    private lazy var payButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(SecondaryButtonStyle(size: .small))
        button.setTitle(Strings.ResultCell.pay, for: .normal)
        return button
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override public func prepareForReuse() {
        super.prepareForReuse()
        backgroundHeaderView.backgroundColor = Colors.branding900.color
        profileImageView.image = nil
        profileImageView.cancelRequest()
        titleLabel.text = nil
        descriptionLabel.text = nil
        addSubview(payButton)
    }

    public func configure(viewModel: SearchResultCarouselCellViewModel, shouldRenderActionButton: Bool) {
        descriptionLabel.text = viewModel.descriptionText
        let profileImageUrl = URL(string: viewModel.profileImageUrl ?? "")
        profileImageView.setImage(url: profileImageUrl) { [weak self] image in
            self?.profileImageView.image = image
            self?.backgroundHeaderView.backgroundColor = image?.averageColor
        }
        titleLabel.text = viewModel.titleText
        addBorder()

        shouldRenderActionButton ? showActionButton() : hideActionButton()
    }
}

extension SearchResultCarouselCell: ViewConfiguration {
    public func buildViewHierarchy() {
        contentView.addSubviews(backgroundHeaderView, profileImageView, titleLabel, distanceContainer, payButton)
        distanceContainer.addSubviews(descriptionIcon, descriptionLabel)
    }

    public func setupConstraints() {
        backgroundHeaderView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.height.equalTo(Layout.Size.backgroundHeaderHeight)
        }

        sendSubviewToBack(backgroundHeaderView)

        profileImageView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base01)
            $0.centerX.equalToSuperview()
            $0.size.equalTo(Layout.Size.profileImageViewHeight)
        }

        titleLabel.snp.makeConstraints {
            $0.top.equalTo(profileImageView.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base01)
            $0.height.equalTo(Layout.Size.titleLabelHeight)
        }

        distanceContainer.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.greaterThanOrEqualToSuperview()
            $0.trailing.lessThanOrEqualToSuperview()
            $0.centerX.equalToSuperview()
        }

        descriptionIcon.snp.makeConstraints {
            $0.leading.centerY.equalToSuperview()
            $0.size.equalTo(Layout.Size.iconHeight)
        }

        descriptionLabel.snp.makeConstraints {
            $0.leading.equalTo(descriptionIcon.snp.trailing).offset(Spacing.base00)
            $0.trailing.bottom.top.equalToSuperview()
            $0.height.equalTo(Spacing.base02)
        }
    }

    public func configureViews() {
        cornerRadius = .medium
        clipsToBounds = true
    }

    private func addBorder() {
        backgroundColor = Colors.backgroundPrimary.color
        contentView.cornerRadius = .medium
        contentView.border = .light(color: .grayscale050())
        contentView.layer.masksToBounds = true
    }

    private func hideActionButton() {
        payButton.removeFromSuperview()
    }

    private func showActionButton() {
        payButton.snp.makeConstraints {
            $0.leading.trailing.bottom.equalToSuperview().inset(Spacing.base02)
            $0.top.equalTo(distanceContainer.snp.bottom).offset(Spacing.base01)
        }
    }
}

public struct SearchResultCarouselCellViewModel: Equatable {
    public var profileImageUrl: String?
    public var titleText: String
    public var descriptionText: String?

    public init(profileImageUrl: String?,
                titleText: String,
                descriptionText: String?) {
        self.profileImageUrl = profileImageUrl
        self.titleText = titleText
        self.descriptionText = descriptionText
    }
}
