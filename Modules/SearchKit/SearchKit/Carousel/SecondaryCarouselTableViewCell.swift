import Foundation
import UIKit
import UI

public class SecondaryCarouselTableViewCell: UITableViewCell {
    private let carousel = SecondaryCarousel()

    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func display(items: [SecondaryCarouselCellViewModel]) {
        carousel.display(items: items)
    }
}

extension SecondaryCarouselTableViewCell: ViewConfiguration {
    public func buildViewHierarchy() {
        contentView.addSubview(carousel)
    }

    public func setupConstraints() {
        carousel.snp.makeConstraints { $0.edges.equalToSuperview() }
    }
}
