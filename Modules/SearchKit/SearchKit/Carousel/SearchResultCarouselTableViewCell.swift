import Foundation
import UIKit
import UI

public class SearchResultCarouselTableViewCell: UITableViewCell {    
    private let carousel = SearchResultCarousel()

    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func display(items: [SearchResultCarouselCellViewModel], shouldRenderActionButton: Bool) {
        carousel.display(items: items, shouldRenderActionButton: shouldRenderActionButton)
    }
}

extension SearchResultCarouselTableViewCell: ViewConfiguration {
    public func buildViewHierarchy() {
        contentView.addSubview(carousel)
    }

    public func setupConstraints() {
        carousel.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.trailing.equalToSuperview()
        }
    }
}
