import Foundation
import UI
import UIKit

public protocol SearchResultTableViewDelegate: AnyObject {
    func didSelectRowAt(indexPath: IndexPath)
    func scrollViewDidScroll(_ scrollView: UIScrollView)
}

public class SearchResultTableView: UIView {
    typealias SectionItem = SearchResultSectionViewModel.SearchResultItemViewModel

    public weak var delegate: SearchResultTableViewDelegate?

    private var sections = [SearchResultSectionViewModel]()

    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.allowsSelection = false 
        tableView.register(cellType: SearchRecentsCell.self)
        tableView.register(cellType: SearchResultCell.self)
        tableView.register(cellType: SearchResultCarouselTableViewCell.self)
        tableView.register(cellType: SearchInformationalCardCell.self)
        tableView.register(cellType: SecondaryCarouselTableViewCell.self)
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.sectionFooterHeight = CGFloat.leastNormalMagnitude
        tableView.separatorStyle = .none
        tableView.backgroundColor = Colors.backgroundPrimary.color
        tableView.keyboardDismissMode = .onDrag
        return tableView
    }()

    private lazy var dataSource: TableViewDataSource<Int, SectionItem> = {
        let dataSource = TableViewDataSource<Int, SectionItem>(view: tableView)
        dataSource.itemProvider = { [weak self] _, indexPath, item -> UITableViewCell? in
            let cell = self?.cell(forIndexPath: indexPath, item: item)
            return cell
        }
        return dataSource
    }()

    public init() {
        super.init(frame: .zero)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func display(sections: [SearchResultSectionViewModel]) {
        sections.enumerated().forEach { index, section in
            dataSource.remove(section: index)
        }

        self.sections = sections
    
        sections.enumerated().forEach { index, section in
            dataSource.add(items: section.items, to: index)
        }
    }

    private func cell(forIndexPath indexPath: IndexPath, item: SectionItem) -> UITableViewCell? {
        switch item {
        case let .recents(item):
            let cell = tableView.dequeueReusableCell(for: indexPath, cellType: SearchRecentsCell.self)
            cell.configureCell(with: item)
            return cell
        case let .list(item):
            let cell = tableView.dequeueReusableCell(for: indexPath, cellType: SearchResultCell.self)
            cell.configureCell(with: item)
            return cell
        case let .carousel(items, shouldRenderActionButton):
            let cell = tableView.dequeueReusableCell(for: indexPath, cellType: SearchResultCarouselTableViewCell.self)
            cell.display(items: items, shouldRenderActionButton: shouldRenderActionButton)
            return cell
        case let .secondaryCarousel(items: items):
            let cell = tableView.dequeueReusableCell(for: indexPath, cellType: SecondaryCarouselTableViewCell.self)
            cell.display(items: items)
            return cell
        case let .help(item: item):
            guard let item = item.first else { return nil }
            let cell = tableView.dequeueReusableCell(for: indexPath, cellType: SearchInformationalCardCell.self)
            cell.configureCell(with: item)
            return cell
        }
    }
}

extension SearchResultTableView: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(tableView)
    }

    public func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }

    public func configureViews() {
        tableView.dataSource = dataSource
        tableView.delegate = self
    }
}

extension SearchResultTableView: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard sections.isNotEmpty, case let section = sections[section], let title = section.title else { return nil }
        let view = SearchResultSectionView(width: tableView.frame.size.width)
        view.setup(title: title, actionTitle: section.actionTitle)
        return view
    }

    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard sections.isNotEmpty, case let section = sections[section] else { return 0 }

        let title = section.title
        let actionTitle = section.actionTitle

        switch (title, actionTitle) {
        case (.none, _):
            return SearchResultSectionView.Layout.noElementsSectionHeaderHeight
        case (.some, .none):
            return SearchResultSectionView.Layout.noActionSectionHeaderHeight
        default:
            return SearchResultSectionView.Layout.fullSizeSectionHeaderHeight
        }
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didSelectRowAt(indexPath: indexPath)
    }

    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        delegate?.scrollViewDidScroll(scrollView)
    }
}
