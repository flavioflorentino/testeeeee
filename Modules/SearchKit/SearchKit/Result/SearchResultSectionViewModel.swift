import Foundation

public struct SearchResultSectionViewModel: Equatable {
    public let title: String?
    public let actionTitle: String?
    public let items: [SearchResultItemViewModel]

    public init(title: String?, actionTitle: String?, items: [SearchResultItemViewModel]) {
        self.title = title
        self.actionTitle = actionTitle
        self.items = items
    }
}

public extension SearchResultSectionViewModel {
    enum SearchResultItemViewModel {
        case recents(item: SearchResultCellViewModel)
        case carousel(items: [SearchResultCarouselCellViewModel], shouldRenderActionButton: Bool)
        case list(item: SearchResultCellViewModel)
        case secondaryCarousel(items: [SecondaryCarouselCellViewModel])
        case help(item: [SearchInformationalCardCellViewModel])
    }
}

extension SearchResultSectionViewModel.SearchResultItemViewModel: Equatable {
    public static func == (lhs: SearchResultSectionViewModel.SearchResultItemViewModel, rhs: SearchResultSectionViewModel.SearchResultItemViewModel) -> Bool {
        switch (lhs, rhs) {
        case let (.recents(lhsItem), .recents(rhsItem)):
            return lhsItem == rhsItem
        case let (.list(lhsItem), .list(rhsItem)):
            return lhsItem == rhsItem
        case let (.carousel(lhsItem, lhsAction), .carousel(rhsItem, rhsAction)):
            return lhsItem == rhsItem && lhsAction == rhsAction
        case let (.secondaryCarousel(lhsItem), .secondaryCarousel(rhsItem)):
            return lhsItem == rhsItem
        case let (.help(lhsItem), .help(item: rhsItem)):
            return lhsItem == rhsItem
        default:
            return false
        }
    }
}
