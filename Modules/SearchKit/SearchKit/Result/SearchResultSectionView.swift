import UI
import UIKit
import Foundation

extension SearchResultSectionView.Layout {
    public static let topVerticalPadding: CGFloat = 40
    static let bottomVerticalPadding: CGFloat = 4
    static var fullSizeSectionHeaderHeight: CGFloat {
        topVerticalPadding + buttonHeight + bottomVerticalPadding
    }
    static var noActionSectionHeaderHeight: CGFloat {
        topVerticalPadding + sectionHeaderLabelHeight + bottomVerticalPadding
    }
    static var noElementsSectionHeaderHeight: CGFloat {
        bottomVerticalPadding
    }
    static let buttonHeight: CGFloat = 48
    static let sectionHeaderLabelHeight: CGFloat = 24
    public static let labelTopSpacing: CGFloat = 10
    static let labelBottomSpacing = 14
}

public class SearchResultSectionView: UIView {
    public enum Layout {}

    var action: (() -> Void)?

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .medium)).with(\.textColor, .grayscale800())
        label.adjustsFontSizeToFitWidth = true
        label.numberOfLines = 1
        return label
    }()

    private lazy var actionButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(LinkButtonStyle(size: .default, icon: (name: Iconography.angleRight, alignment: .right)))
        button.addTarget(self, action: #selector(performAction), for: .touchUpInside)
        return button
    }()

    private lazy var topLayoutGuide = UILayoutGuide()
    private lazy var bottomLayoutGuide = UILayoutGuide()

    init(width: CGFloat) {
        let frame = CGRect(x: 0, y: 0, width: width, height: 0)
        super.init(frame: frame)
        buildLayout()
    }

    func setup(title: String?, actionTitle: String?) {
        titleLabel.text = title
        if let actionTitle = actionTitle {
            actionButton.setTitle(actionTitle, for: .normal)
        } else {
            titleLabel.snp.updateConstraints {
                $0.top.equalTo(topLayoutGuide.snp.bottom).offset(0).priority(.medium)
                $0.bottom.equalTo(bottomLayoutGuide.snp.top).offset(0).priority(.medium)
            }
            actionButton.removeFromSuperview()
        }
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc
    private func performAction() {
        self.action?()
    }
}

extension SearchResultSectionView: ViewConfiguration {
    public func buildViewHierarchy() {
        addLayoutGuide(topLayoutGuide)
        addLayoutGuide(bottomLayoutGuide)
        addSubview(titleLabel)
        addSubview(actionButton)
    }

    public func setupConstraints() {
        topLayoutGuide.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.height.equalTo(Layout.topVerticalPadding)
        }

        titleLabel.snp.makeConstraints {
            $0.top.equalTo(topLayoutGuide.snp.bottom).offset(Layout.labelTopSpacing).priority(.medium)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.height.equalTo(Layout.sectionHeaderLabelHeight)
            $0.trailing.lessThanOrEqualTo(actionButton.snp.leading).offset(-Spacing.base02)
            $0.bottom.equalTo(bottomLayoutGuide.snp.top).offset(-Layout.labelTopSpacing).priority(.medium)
        }
        
        actionButton.snp.makeConstraints {
            $0.top.equalTo(topLayoutGuide.snp.bottom)
            $0.bottom.equalTo(bottomLayoutGuide.snp.top)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }

        bottomLayoutGuide.snp.makeConstraints {
            $0.bottom.equalToSuperview()
            $0.height.equalTo(Layout.bottomVerticalPadding)
        }
    }
}
