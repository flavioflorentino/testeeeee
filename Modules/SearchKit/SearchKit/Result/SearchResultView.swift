import Foundation
import UI
import UIKit
import AssetsKit

extension SearchResultView.Layout {
    static var profileImageSize: CGFloat {
        isSmallScreen ? 48 : 56
    }
    static let profileBadgeSize: CGFloat = 16
    static let actionContainerSize: CGFloat = 50
    static let verticalOffset: CGFloat = 8
    static let footerIconSize: CGFloat = 16
    static let isSmallScreen = UIScreen.main.bounds.width <= 360
}

final class SearchResultView: UIView {
    fileprivate enum Layout { }
    
    lazy var profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.masksToBounds = true
        return imageView
    }()

    lazy var profileBadgeView = UIImageView()

    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle()).with(\.textColor, .black())
        label.numberOfLines = 1
        return label
    }()

    lazy var titleBadgeView = UIImageView()

    lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight)).with(\.textColor, .grayscale800())
        label.numberOfLines = 1
        return label
    }()

    lazy var footerIcon = UIImageView()

    lazy var footerLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle()).with(\.textColor, .grayscale500())
        label.numberOfLines = 1
        return label
    }()

    lazy var actionContainer = UIView()

    lazy var actionImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.tintColor = Colors.branding600.color
        return imageView
    }()

    lazy var actionTextLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle()).with(\.textColor, .branding600())
            .with(\.numberOfLines, 1)
        return label
    }()

    init() {
        super.init(frame: .zero)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setup(viewModel: SearchResultCellViewModel) {
        profileImageView.setImage(url: URL(string: viewModel.profileImageUrl ?? ""))
        titleLabel.text = viewModel.title
        profileImageView.border = viewModel.style.border
        titleBadgeView.image = viewModel.style.titleBadgeImage
        profileBadgeView.image = viewModel.style.profileBadgeImage

        descriptionLabel.text = viewModel.description
        footerLabel.text = viewModel.footer
        profileImageView.layer.cornerRadius = getProfileCornerRadius(viewModel: viewModel)
        actionTextLabel.text = viewModel.style.actionText
        actionImageView.image = viewModel.style.actionIcon

        if let footer = viewModel.footer, footer.isNotEmpty {
            footerIcon.image = viewModel.style.footerIcon
            footerLabel.text = footer
        } else {
            removeFooter()
        }
    }

    private func getProfileCornerRadius(viewModel: SearchResultCellViewModel) -> CGFloat {
        switch viewModel.style.profilePictureRadius {
        case .halve:
            return Layout.profileImageSize / 2
        case .fixed(let value):
            return value
        }
    }
}

extension SearchResultView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubviews(profileImageView, profileBadgeView, titleLabel, titleBadgeView)
        addSubviews(descriptionLabel, footerIcon, footerLabel, actionContainer)
        actionContainer.addSubviews(actionTextLabel, actionImageView)
    }

    // swiftlint:disable function_body_length
    func setupConstraints() {
        profileImageView.snp.makeConstraints {
            $0.leading.centerY.equalToSuperview()
            $0.top.greaterThanOrEqualToSuperview().offset(Layout.verticalOffset)
            $0.bottom.lessThanOrEqualToSuperview().offset(-Layout.verticalOffset)
            $0.size.equalTo(Layout.profileImageSize)
        }

        profileBadgeView.snp.makeConstraints {
            $0.bottom.trailing.equalTo(profileImageView)
            $0.size.equalTo(Layout.profileBadgeSize)
        }

        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Layout.verticalOffset)
            $0.leading.equalTo(profileImageView.snp.trailing).offset(Spacing.base02)
        }

        titleBadgeView.snp.makeConstraints {
            $0.centerY.equalTo(titleLabel)
            $0.leading.equalTo(titleLabel.snp.trailing).offset(Spacing.base01)
            $0.trailing.lessThanOrEqualTo(actionContainer.snp.leading).offset(-Spacing.base02)
        }

        titleBadgeView.snp.contentCompressionResistanceHorizontalPriority = UILayoutPriority.required.rawValue

        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom)
            $0.leading.equalTo(profileImageView.snp.trailing).offset(Spacing.base02)
            $0.trailing.lessThanOrEqualTo(actionContainer.snp.leading).offset(-Spacing.base02)
        }

        footerIcon.snp.makeConstraints {
            $0.leading.equalTo(profileImageView.snp.trailing).offset(Spacing.base02)
            $0.centerY.equalTo(footerLabel)
            $0.size.equalTo(Layout.footerIconSize)
        }

        footerLabel.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom)
            $0.leading.equalTo(footerIcon.snp.trailing).offset(Spacing.base00)
            $0.trailing.lessThanOrEqualTo(actionContainer.snp.leading).offset(-Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Layout.verticalOffset)
        }

        actionContainer.snp.makeConstraints {
            $0.top.greaterThanOrEqualToSuperview()
            $0.bottom.lessThanOrEqualToSuperview()
            $0.centerY.trailing.equalToSuperview()
            $0.width.equalTo(Layout.actionContainerSize)
        }

        actionContainer.snp.contentHuggingHorizontalPriority = UILayoutPriority.required.rawValue

        actionImageView.snp.makeConstraints {
            $0.top.centerX.equalToSuperview()
            $0.leading.greaterThanOrEqualToSuperview()
            $0.trailing.lessThanOrEqualToSuperview()
        }

        actionTextLabel.snp.makeConstraints {
            $0.top.equalTo(actionImageView.snp.bottom).offset(Spacing.base01)
            $0.centerX.bottom.equalToSuperview()
            $0.leading.greaterThanOrEqualToSuperview()
            $0.trailing.lessThanOrEqualToSuperview()
        }
    }

    func removeFooter() {
        [footerIcon, footerLabel].forEach { $0.removeFromSuperview() }
        descriptionLabel.snp.makeConstraints {
            $0.bottom.equalToSuperview().offset(-Spacing.base02)
        }

        titleLabel.snp.updateConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
        }
    }
}
