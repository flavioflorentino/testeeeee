import Foundation
import UI
import UIKit
import AssetsKit

public final class SearchResultCell: UITableViewCell {
    private lazy var resultView = SearchResultView()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }

    override public func prepareForReuse() {
        super.prepareForReuse()
        resultView.profileImageView.image = nil
        resultView.profileImageView.border = .none
        resultView.profileBadgeView.image = nil
        resultView.titleLabel.text = nil
        resultView.titleBadgeView.image = nil
        resultView.descriptionLabel.text = nil
        resultView.footerIcon.image = nil
        resultView.footerLabel.text = nil
        resultView.actionImageView.image = nil
        resultView.actionTextLabel.text = nil
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func configureCell(with viewModel: SearchResultCellViewModel) {
        resultView.setup(viewModel: viewModel)
    }
}

extension SearchResultCell: ViewConfiguration {
    public func buildViewHierarchy() {
        contentView.addSubviews(resultView)
        backgroundColor = Colors.backgroundPrimary.color
    }

    public func setupConstraints() {
        resultView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.top.bottom.equalToSuperview().inset(Spacing.base00)
        }
    }
}
