import UI
import Core
import UIKit
import SkeletonView

extension SkeletonSectionView.Layout {
    static let descriptionProportionToAvailableWidth: CGFloat = 0.42
    static let viewHeight: CGFloat = Spacing.base08

    enum Gradient {
        private static let alpha: CGFloat = 0.16
        private static let base = Colors.grayscale400.color.withAlphaComponent(alpha)
        private static let secondary = Colors.grayscale200.color.withAlphaComponent(alpha)
        static let value = SkeletonGradient(baseColor: base, secondaryColor: secondary)
    }
}

final class SkeletonSectionView: UIView {
    enum Layout { }
    private lazy var titleSkeletonView: UILabel = {
        let label = UILabel()
        label.linesCornerRadius = Int(CornerRadius.medium)
        label.isSkeletonable = true
        return label
    }()

    private let width: CGFloat

    init(width: CGFloat) {
        self.width = width
        super.init(frame: .init(x: 0, y: 0, width: width, height: Layout.viewHeight))
        buildLayout()
        showAnimatedGradientSkeleton(usingGradient: Layout.Gradient.value)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        nil
    }
}

extension SkeletonSectionView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(titleSkeletonView)
    }

    func setupConstraints() {
        titleSkeletonView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.width.equalTo(width * Layout.descriptionProportionToAvailableWidth)
            $0.height.equalTo(Sizing.base02)
            $0.bottom.lessThanOrEqualToSuperview()
        }
    }
}
