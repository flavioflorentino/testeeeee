import UI
import Core
import UIKit
import SkeletonView

extension SearchResultLoadingView.Layout {
    static let numberOfRows = 3
    static let numberOfSections = 2
}

public final class SearchResultLoadingView: UIView {
    fileprivate enum Layout { }

    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.register(cellType: SearchResultLoadingCell.self)
        tableView.isUserInteractionEnabled = false
        tableView.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        return tableView
    }()

    private lazy var dataSource: TableViewDataSource<Int, Void> = {
        let dataSource = TableViewDataSource<Int, Void>(view: tableView)
        dataSource.itemProvider = { [weak self] _, indexPath, _ -> UITableViewCell? in
            self?.tableView.dequeueReusableCell(for: indexPath, cellType: SearchResultLoadingCell.self)
        }
        return dataSource
    }()

    public init() {
        super.init(frame: .zero)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        nil
    }
}

extension SearchResultLoadingView: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(tableView)
    }

    public func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }

    public func configureViews() {
        tableView.dataSource = dataSource
        for sectionIndex in 0..<Layout.numberOfSections {
            for _ in 0..<Layout.numberOfRows {
                dataSource.add(items: [()], to: sectionIndex)
            }
        }
    }
}

extension SearchResultLoadingView: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        SkeletonSectionView(width: tableView.frame.width)
    }

    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        SkeletonSectionView.Layout.viewHeight
    }
}
