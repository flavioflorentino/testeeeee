import Foundation
import UI
import SkeletonView

extension SearchResultLoadingCell.Layout {
    static let profileViewSize: CGFloat = Spacing.base06
    static let titleProportionToScreenWidth: CGFloat = 0.63
    static let descriptionProportionToTitleWidth: CGFloat = 0.65

    enum Gradient {
        private static let alpha: CGFloat = 0.16
        private static let base = Colors.grayscale400.color.withAlphaComponent(alpha)
        private static let secondary = Colors.grayscale200.color.withAlphaComponent(alpha)
        static let value = SkeletonGradient(baseColor: base, secondaryColor: secondary)
    }
}

final class SearchResultLoadingCell: UITableViewCell {
    fileprivate enum Layout { }

    private lazy var profileSkeletonView: UIImageView = {
        let view = UIImageView()
        view.layer.masksToBounds = true
        view.layer.cornerRadius = Layout.profileViewSize / 2
        view.isSkeletonable = true
        return view
    }()

    private lazy var titleSkeletonView: UILabel = {
        let label = UILabel()
        label.linesCornerRadius = Int(CornerRadius.medium)
        label.isSkeletonable = true
        return label
    }()

    private lazy var descriptionSkeletonView: UILabel = {
        let label = UILabel()
        label.isSkeletonable = true
        label.linesCornerRadius = Int(CornerRadius.medium)
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
        showAnimationsSkeleton()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        nil
    }

    func showAnimationsSkeleton() {
        let subviews = [profileSkeletonView, titleSkeletonView, descriptionSkeletonView]
        subviews.forEach { $0.showAnimatedGradientSkeleton(usingGradient: Layout.Gradient.value) }
    }
}

extension SearchResultLoadingCell: ViewConfiguration {
    func buildViewHierarchy() {
        addSubviews(profileSkeletonView, titleSkeletonView, descriptionSkeletonView)
    }

    func setupConstraints() {
        profileSkeletonView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base01).priority(.medium)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.size.equalTo(Layout.profileViewSize).priority(.medium)
            $0.bottom.equalToSuperview().offset(-Spacing.base01).priority(.medium)
        }

        titleSkeletonView.snp.makeConstraints {
            $0.top.equalTo(profileSkeletonView).offset(Spacing.base00)
            $0.leading.equalTo(profileSkeletonView.snp.trailing).offset(Spacing.base02)
            $0.width.equalTo(UIScreen.main.bounds.width * Layout.titleProportionToScreenWidth)
            $0.height.equalTo(Sizing.base02)
        }

        descriptionSkeletonView.snp.makeConstraints {
            $0.bottom.equalTo(profileSkeletonView).inset(Spacing.base00)
            $0.leading.equalTo(titleSkeletonView)
            $0.height.equalTo(Sizing.base02)
            $0.width.equalTo(titleSkeletonView).multipliedBy(Layout.descriptionProportionToTitleWidth)
        }
    }
}
