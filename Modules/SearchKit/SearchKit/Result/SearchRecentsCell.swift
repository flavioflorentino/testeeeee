import Foundation
import UI
import UIKit
import AssetsKit

extension SearchRecentsCell.Layout {
    static let closeButtonSize = 16
    static let resultViewLeadingOffset = 12
}

public class SearchRecentsCell: UITableViewCell {
    fileprivate enum Layout { }
    var closeAction: (() -> Void)?

    private lazy var closeButton: UIButton = {
        let button = UIButton()
        button.setImage(Resources.Icons.icoClose.image, for: .normal)
        button.addTarget(self, action: #selector(didTapCloseButton), for: .touchUpInside)
        return button
    }()

    private lazy var resultView = SearchResultView()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc
    private func didTapCloseButton() {
        closeAction?()
    }

    public func configureCell(with viewModel: SearchResultCellViewModel) {
        resultView.setup(viewModel: viewModel)
    }
}

extension SearchRecentsCell: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubviews(closeButton, resultView)
        backgroundColor = Colors.backgroundPrimary.color
    }

    public func setupConstraints() {
        closeButton.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.centerY.equalToSuperview()
            $0.size.equalTo(Layout.closeButtonSize)
        }

        closeButton.snp.contentHuggingHorizontalPriority = UILayoutPriority.defaultHigh.rawValue

        resultView.snp.makeConstraints {
            $0.leading.equalTo(closeButton.snp.trailing).offset(Spacing.base01)
            $0.trailing.equalToSuperview().offset(-Spacing.base01)
            $0.top.bottom.equalToSuperview().inset(Spacing.base01)
        }
    }
}
