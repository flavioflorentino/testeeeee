import Foundation
import UIKit
import AssetsKit
import UI

public struct SearchResultCellViewModel: Equatable, Hashable {
    public var style: Style
    public var profileImageUrl: String?
    public var title: String
    public var description: String
    public var footer: String?

    public init(style: Style,
                profileImageUrl: String?,
                title: String,
                description: String,
                footer: String?) {
        self.style = style
        self.profileImageUrl = profileImageUrl
        self.title = title
        self.description = description
        self.footer = footer
    }
}

public extension SearchResultCellViewModel {
    enum Style: Equatable, Hashable {
        case people(isVerified: Bool, isPro: Bool)
        case store
        case p2m
        case subscription
        case apps

        // swiftlint:disable nesting
        enum RadiusType {
            case halve
            case fixed(value: CGFloat)
        }

        var profilePictureRadius: RadiusType {
            switch self {
            case .people:
                return .halve
            default:
                return .fixed(value: 12)
            }
        }

        var actionIcon: UIImage {
            switch self {
            case .people, .store, .subscription:
                return Resources.Icons.icoPay.image.withRenderingMode(.alwaysTemplate)
            case .apps:
                return Resources.Icons.icoShoppingBag.image
            case .p2m:
                return Resources.Icons.icoQrScan.image
            }
        }

        var actionText: String {
            switch self {
            case .people, .store:
                return Strings.ResultCell.pay
            case .subscription:
                return Strings.ResultCell.plans
            case .p2m:
                return Strings.ResultCell.readQR
            case .apps:
                return Strings.ResultCell.buy
            }
        }

        var footerIcon: UIImage? {
            switch self {
            case .people:
                return Resources.Icons.icoPeople.image
            case .p2m, .store:
                return Resources.Icons.icoLocationPoint.image
            default:
                return nil
            }
        }

        var border: Border.Style {
            switch self {
            case .people:
                return .none
            default:
                return .light(color: .grayscale100())
            }
        }

        var titleBadgeImage: UIImage? {
            switch self {
            case let .people(_, isPro):
                return isPro ? Resources.Icons.icoProBadgeGray.image : nil
            default:
                return nil
            }
        }

        var profileBadgeImage: UIImage? {
            switch self {
            case let .people(isVerified, _):
                return isVerified ? Resources.Icons.icoVerifiedBadge.image : nil
            default:
                return nil
            }
        }

        public static func == (lhs: SearchResultCellViewModel.Style, rhs: SearchResultCellViewModel.Style) -> Bool {
            switch (lhs, rhs) {
            case (.people, .people):
                return true
            case (.store, .store):
                return true
            case (.p2m, .p2m):
                return true
            case (.subscription, .subscription):
                return true
            default:
                return false
            }
        }
    }
}
