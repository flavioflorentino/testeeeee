import Foundation
import UI
import UIKit

extension SearchInformationalCardCell.Layout {
    static let containerHeight = 54
}

public class SearchInformationalCardCell: UITableViewCell {
    fileprivate enum Layout { }

    private lazy var container = UIView()

    private lazy var titleButton: UIButton = {
        let button = UIButton()
        button.isUserInteractionEnabled = false
        return button
    }()

    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configureCell(with viewModel: SearchInformationalCardCellViewModel) {
        titleButton.setTitle(viewModel.title, for: .normal)
        titleButton.buttonStyle(LinkButtonStyle(size: .small))
    }
}

extension SearchInformationalCardCell: ViewConfiguration {
    public func buildViewHierarchy() {
        contentView.addSubview(container)
        container.addSubview(titleButton)
    }

    public func setupConstraints() {
        container.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.top.bottom.equalToSuperview()
            $0.height.equalTo(Layout.containerHeight)
        }

        titleButton.snp.makeConstraints {
            $0.leading.trailing.centerY.equalToSuperview()
        }
    }

    public func configureViews() {
        container.backgroundColor = Colors.grayscale050.color
        container.cornerRadius = .medium
        backgroundColor = Colors.backgroundPrimary.color
    }
}

public struct SearchInformationalCardCellViewModel: Equatable {
    public let title: String

    public init(title: String) {
        self.title = title
    }
}
