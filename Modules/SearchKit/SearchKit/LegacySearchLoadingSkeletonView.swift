import UI
import Core
import UIKit
import SkeletonView
import SnapKit

extension LegacySearchLoadingSkeletonView.Layout {
    enum Cell {
        static let numberOfRows = 7
    }
}

public final class LegacySearchLoadingSkeletonView: UIView {
    fileprivate enum Layout { }

    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(cellType: LegacySearchLoadingSkeletonCell.self)
        tableView.isUserInteractionEnabled = false
        tableView.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        return tableView
    }()

    private lazy var dataSource: TableViewDataSource<Int, Void> = {
        let dataSource = TableViewDataSource<Int, Void>(view: tableView)
        dataSource.itemProvider = { [weak self] _, indexPath, _ -> UITableViewCell? in
            let cell = self?.tableView.dequeueReusableCell(for: indexPath,
                                                           cellType: LegacySearchLoadingSkeletonCell.self)
            cell?.showAnimationsSkeleton()
            return cell
        }
        return dataSource
    }()

    public init() {
        super.init(frame: .zero)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension LegacySearchLoadingSkeletonView: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(tableView)
    }

    public func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }

    public func configureViews() {
        tableView.dataSource = dataSource
        for _ in 0..<Layout.Cell.numberOfRows {
            dataSource.add(items: [()], to: 1)
        }
    }
}
