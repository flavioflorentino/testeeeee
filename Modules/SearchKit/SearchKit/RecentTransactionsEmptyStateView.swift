import Foundation
import UIKit
import UI

extension RecentTransactionsEmptyStateView.Layout {
    static let topOffset = 26
    static let imageWidth = 144
}

public final class RecentTransactionsEmptyStateView: UIView {
    fileprivate enum Layout { }

    public var image: UIImage? {
        didSet {
            imageView.image = image
        }
    }

    public var descriptionText: String? {
        didSet {
            descriptionLabel.text = descriptionText
        }
    }

    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textAlignment, .center)
            .with(\.textColor, Colors.grayscale600.color)
        return label
    }()

    public init() {
        super.init(frame: .zero)
        buildLayout()
    }

    @available(*, unavailable)
    public required init?(coder: NSCoder) {
        nil
    }
}

extension RecentTransactionsEmptyStateView: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubviews(imageView, descriptionLabel)
    }

    public func setupConstraints() {
        imageView.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalToSuperview().offset(Layout.topOffset)
            $0.width.equalTo(Layout.imageWidth)
        }

        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
}
