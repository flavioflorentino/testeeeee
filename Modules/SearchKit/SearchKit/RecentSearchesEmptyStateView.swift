import Foundation
import UIKit
import UI
import AssetsKit

extension RecentSearchesEmptyStateView.Layout {
    static let imageSize: CGFloat = 74
}

public final class RecentSearchesEmptyStateView: UIView {
    fileprivate enum Layout { }

    public var title: String? {
        didSet {
            titleLabel.text = title
        }
    }

    public var descriptionText: String? {
        didSet {
            descriptionLabel.text = descriptionText
        }
    }

    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Resources.Illustrations.iluFavorite.image
        return imageView
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textColor, Colors.grayscale800.color)
        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale600.color)
        return label
    }()

    public init() {
        super.init(frame: .zero)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        nil
    }
}

extension RecentSearchesEmptyStateView: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubviews(imageView, titleLabel, descriptionLabel)
    }

    public func setupConstraints() {
        imageView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.size.equalTo(Layout.imageSize)
        }

        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.leading.equalTo(imageView.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
        }

        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalTo(imageView.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview()
        }
    }
}
