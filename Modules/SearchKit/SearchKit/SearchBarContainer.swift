import UI
import UIKit
import Foundation

public final class SearchBarContainer: UIView {
    public weak var delegate: ApolloSearchBarDelegate? {
        didSet {
            searchBar.delegate = delegate
        }
    }

    public var placeholderText: String? {
        didSet {
            searchBar.placeholderText = placeholderText ?? ""
        }
    }

    private lazy var separator: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale100.color
        return view
    }()

    private lazy var searchBar = ApolloSearchBar()

    public init() {
        super.init(frame: .zero)
        buildLayout()
        hideSeparator()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        nil
    }

    public func showSeparator() {
        separator.isHidden = false
    }

    public func hideSeparator() {
        separator.isHidden = true
    }
}

extension SearchBarContainer: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubviews(searchBar, separator)
    }

    public func setupConstraints() {
        separator.snp.makeConstraints {
            $0.bottom.leading.trailing.equalToSuperview()
            $0.height.equalTo(1.2)
        }

        searchBar.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.trailing.bottom.equalToSuperview().inset(Spacing.base02)
        }
    }

    public func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
}
