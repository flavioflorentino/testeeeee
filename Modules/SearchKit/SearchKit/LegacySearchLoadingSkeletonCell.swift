import Foundation
import UI
import SkeletonView
import SnapKit

extension LegacySearchLoadingSkeletonCell.Layout {
    static let profileViewSize: CGFloat = 48
    static let titleSkeletonHeight: CGFloat = 16
    static let titleProportionToScreenWidth: CGFloat = 0.70
    static let descriptionProportionToTitleWidth: CGFloat = 0.72
    static let labelHeight: CGFloat = 16
    static let grandientAlpha: CGFloat = 0.16
}

final class LegacySearchLoadingSkeletonCell: UITableViewCell {
    fileprivate enum Layout { }

    private lazy var profileSkeletonView: UIImageView = {
        let view = UIImageView()
        view.layer.masksToBounds = true
        view.layer.cornerRadius = Layout.profileViewSize / 2
        view.isSkeletonable = true
        return view
    }()

    private lazy var titleSkeletonView: UILabel = {
        let label = UILabel()
        label.linesCornerRadius = Int(CornerRadius.medium)
        label.isSkeletonable = true
        return label
    }()

    private lazy var descriptionSkeletonView: UILabel = {
        let label = UILabel()
        label.isSkeletonable = true
        label.linesCornerRadius = Int(CornerRadius.medium)
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func showAnimationsSkeleton() {
        let gradientBaseColor = Colors.grayscale400.color.withAlphaComponent(Layout.grandientAlpha)
        let gradientSecondaryColor = Colors.grayscale200.color.withAlphaComponent(Layout.grandientAlpha)
        let skeletonGradient = SkeletonGradient(baseColor: gradientBaseColor, secondaryColor: gradientSecondaryColor)
        profileSkeletonView.showAnimatedGradientSkeleton(usingGradient: skeletonGradient)
        titleSkeletonView.showAnimatedGradientSkeleton(usingGradient: skeletonGradient)
        descriptionSkeletonView.showAnimatedGradientSkeleton(usingGradient: skeletonGradient)
    }
}

extension LegacySearchLoadingSkeletonCell: ViewConfiguration {
    func buildViewHierarchy() {
        addSubviews(profileSkeletonView, titleSkeletonView, descriptionSkeletonView)
    }

    func setupConstraints() {
        profileSkeletonView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.size.equalTo(Layout.profileViewSize)
            $0.bottom.equalToSuperview().offset(-Spacing.base01)
        }

        titleSkeletonView.snp.makeConstraints {
            $0.top.equalTo(profileSkeletonView)
            $0.leading.equalTo(profileSkeletonView.snp.trailing).offset(Spacing.base02)
            $0.width.equalTo(UIScreen.main.bounds.width * Layout.titleProportionToScreenWidth)
            $0.height.equalTo(Layout.labelHeight)
            $0.bottom.lessThanOrEqualToSuperview()
        }

        descriptionSkeletonView.snp.makeConstraints {
            $0.top.equalTo(titleSkeletonView.snp.bottom).offset(Spacing.base02)
            $0.leading.equalTo(titleSkeletonView)
            $0.height.equalTo(Layout.labelHeight)
            $0.width.equalTo(titleSkeletonView).multipliedBy(Layout.descriptionProportionToTitleWidth)
        }
    }
}
