import Foundation
import AssetsKit
import SearchKit

enum SearchResultSectionFactory {
    typealias Item = SearchResultSectionViewModel.SearchResultItemViewModel

    static func build() -> [SearchResultSectionViewModel] {
        let recentSection = buildRecentSection()
        let peopleSection = buildPeopleSection()
        let carouselSection = buildCarouselSection()
        let carouselNoActionSection = buildCarouselNoActionSection()
        let secondaryCarouselSection = buildSecondaryCarouselSection()
        let informationalSection = buildInformationalCardSection()
        return [recentSection, peopleSection, carouselSection, carouselNoActionSection, secondaryCarouselSection, informationalSection]
    }

    private static func buildRecentSection() -> SearchResultSectionViewModel {
        .init(title: "Buscas Recentes", actionTitle: "Exibir todas", items: buildRecentItems())
    }

    private static func buildRecentItems() -> [Item] {
        let cellViewModel1 = SearchResultCellViewModel(style: .people(isVerified: true, isPro: true),
                                                       profileImageUrl: "https://i.ibb.co/3s5qy6K/profile-image.png",
                                                       title: "marcos.jose",
                                                       description: "Marcos Jose Barbosa",
                                                       footer: nil)
        let viewModel1 = Item.recents(item: cellViewModel1)

        let cellViewModel2 = SearchResultCellViewModel(style: .p2m,
                                                       profileImageUrl: "https://i.ibb.co/2N5s3ZD/avatar.png",
                                                       title: "Marcelo Eventos",
                                                       description: "Avenida Getúlio Vargas de Almeida Teste",
                                                       footer: nil)
        let viewModel2 = Item.recents(item: cellViewModel2)
        return [viewModel1, viewModel2]
    }

    private static func buildPeopleSection() -> SearchResultSectionViewModel {
        .init(title: "Pessoas", actionTitle: "Exibir todas", items: buildPeopleItems())
    }

    private static func buildPeopleItems() -> [Item] {
        let viewModel1 = SearchResultCellViewModel(style: .people(isVerified: true, isPro: true),
                                                   profileImageUrl: "https://i.ibb.co/3s5qy6K/profile-image.png",
                                                   title: "marcos.josemarcos.josemarcos.josemarcos.jose",
                                                   description: "Marcos Jose Barbosa",
                                                   footer: "Contato na sua agenda")

        let viewModel2 = SearchResultCellViewModel(style: .store,
                                                   profileImageUrl: "https://i.ibb.co/KWHtNJZ/image-14.png",
                                                   title: "Marcelo Elias Serviços",
                                                   description: "Rua dos Jasmin, Jardim Novo",
                                                   footer: "4,5km de distância")

        let viewModel3 = SearchResultCellViewModel(style: .apps,
                                                   profileImageUrl: "https://i.ibb.co/KWHtNJZ/image-14.png",
                                                   title: "Marcelo Elias Serviços",
                                                   description: "Rua dos Jasmin, Jardim Novo",
                                                   footer: nil)
        let viewModel4 = SearchResultCellViewModel(style: .p2m,
                                                   profileImageUrl: "https://i.ibb.co/2N5s3ZD/avatar.png",
                                                   title: "Marcelo Eventos",
                                                   description: "Avenida Getúlio Vargas de Almeida Teste",
                                                   footer: nil)
        let viewModel5 = SearchResultCellViewModel(style: .subscription,
                                                   profileImageUrl: "https://i.ibb.co/mq2c4xS/avatar.png",
                                                   title: "Super Mario Super Mario Super Mario Super Mario",
                                                   description: "Créditos para Super Mario",
                                                   footer: nil)

        let items = [viewModel1, viewModel2, viewModel3, viewModel4, viewModel5].map { Item.list(item: $0) }
        return items
    }

    private static func buildCarouselSection() -> SearchResultSectionViewModel {
        .init(title: "Estabelecimentos", actionTitle: "Exibir todos", items: [buildCarouselItems()])
    }

    private static func buildCarouselItems() -> Item {
        let item1 = SearchResultCarouselCellViewModel(profileImageUrl: "https://i.ibb.co/3s5qy6K/profile-image.png",
                                                      titleText: "Marcelo Eventos",
                                                      descriptionText: "a 500m")
        let item2 = SearchResultCarouselCellViewModel(profileImageUrl: "https://sguru.org/wp-content/uploads/2017/06/steam-avatar-profile-picture-0210.jpg",
                                                      titleText: "Marcelo Fabiano Foto",
                                                      descriptionText: "a 3km")
        let item3 = SearchResultCarouselCellViewModel(profileImageUrl: "https://sguru.org/wp-content/uploads/2017/06/steam-avatar-profile-picture-0226.jpg",
                                                      titleText: "Teste",
                                                      descriptionText: "a 1km")
        let item4 = SearchResultCarouselCellViewModel(profileImageUrl: "https://sguru.org/wp-content/uploads/2017/06/steam-avatar-profile-picture-0320.jpg",
                                                      titleText: "Batata Restaurante",
                                                      descriptionText: "a 10km")
        let items = Item.carousel(items: [item1, item2, item3, item4], shouldRenderActionButton: true)
        return items
    }

    private static func buildCarouselNoActionSection() -> SearchResultSectionViewModel {
        .init(title: "Estabelecimentos", actionTitle: "Exibir todos", items: [buildCarouselNoActionItems()])
    }

    private static func buildCarouselNoActionItems() -> Item {
        let item1 = SearchResultCarouselCellViewModel(profileImageUrl: "https://i.ibb.co/3s5qy6K/profile-image.png",
                                                      titleText: "Marcelo Eventos",
                                                      descriptionText: "a 500m")
        let item2 = SearchResultCarouselCellViewModel(profileImageUrl: "https://sguru.org/wp-content/uploads/2017/06/steam-avatar-profile-picture-0210.jpg",
                                                      titleText: "Marcelo Fabiano Foto",
                                                      descriptionText: "a 3km")
        let item3 = SearchResultCarouselCellViewModel(profileImageUrl: "https://sguru.org/wp-content/uploads/2017/06/steam-avatar-profile-picture-0226.jpg",
                                                      titleText: "Teste",
                                                      descriptionText: "a 1km")
        let item4 = SearchResultCarouselCellViewModel(profileImageUrl: "https://sguru.org/wp-content/uploads/2017/06/steam-avatar-profile-picture-0320.jpg",
                                                      titleText: "Batata Restaurante",
                                                      descriptionText: "a 10km")
        let items = Item.carousel(items: [item1, item2, item3, item4], shouldRenderActionButton: false)
        return items
    }

    private static func buildSecondaryCarouselSection() -> SearchResultSectionViewModel {
        .init(title: "Encontre facilidades para você", actionTitle: nil, items: [buildSecondaryCarouselItems()])
    }

    private static func buildSecondaryCarouselItems() -> Item {
        let item1 = SecondaryCarouselCellViewModel(title: "Pague nas",
                                                   highlight: "Maquininhas",
                                                   icon: Resources.Icons.icoP2m.image)
        let item2 = SecondaryCarouselCellViewModel(title: "Pagar",
                                                   highlight: "Boleto",
                                                   icon: Resources.Icons.icoBarcode.image)
        let item3 = SecondaryCarouselCellViewModel(title: "Pague com",
                                                   highlight: "PIX",
                                                   icon: Resources.Icons.icoPixOutline.image)
        let item4 = SecondaryCarouselCellViewModel(title: "Pague com",
                                                   highlight: "PIX",
                                                   icon: Resources.Icons.icoPixOutline.image)
        let items = Item.secondaryCarousel(items: [item1, item2, item3, item4])
        return items
    }

    private static func buildInformationalCardSection() -> SearchResultSectionViewModel {
        .init(title: nil, actionTitle: nil, items: [buildInformationalItem()])
    }

    private static func buildInformationalItem() -> Item {
        let item = SearchInformationalCardCellViewModel(title: "Não encontrou alguém?")
        return .help(item: [item])
    }
}
