import UI
import UIKit
import DirectMessage

class HomeViewController: UIViewController {
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 16
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private lazy var actionButton: UIButton = {
        let button = UIButton()
        button.setTitle("POCMqtt", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = Colors.branding600.color
        button.addTarget(self, action: #selector(didTouchButton1), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    init() {
        super.init(nibName: nil, bundle: nil)
        view.addSubview(stackView)
        stackView.addArrangedSubview(actionButton)
        
        stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        stackView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        actionButton.widthAnchor.constraint(equalToConstant: 200).isActive = true
        
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    func didTouchButton1() {
        let viewController = DMChatListFactory.make()
        navigationController?.pushViewController(viewController, animated: true)
    }
}
