import XCTest
@testable import DirectMessage

final class DMStoreNotificationTests: XCTestCase {
    private lazy var notificationCenterSpy = NotificationCenterSpy()
    private lazy var sut = DMStoreNotification(notificationCenter: notificationCenterSpy)
    
    func testDidSaveNewMessage_WhenSavingMessageOnStore_ShouldPostMessageNotification() throws {
        sut.didSaveNew(message: Stubs.message)
        
        let notification = try XCTUnwrap(notificationCenterSpy.postedNotifications.first)
        XCTAssertTrue(notification.object is DMStoreNotification)
        XCTAssertEqual(notification.name.rawValue, DMStoreNotification.Name.didSaveNewMessage)

        let message = try XCTUnwrap(notification.userInfo?[DMStoreNotification.UserInfoKey.message] as? Message)
        XCTAssertEqual(message, Stubs.message)
    }

    func testDidUpdateChat_WhenUpdatingChatOnStore_ShouldPostChatNotification() throws {
        sut.didUpdate(chat: Stubs.chat)

        let notification = try XCTUnwrap(notificationCenterSpy.postedNotifications.first)
        XCTAssertTrue(notification.object is DMStoreNotification)
        XCTAssertEqual(notification.name.rawValue, DMStoreNotification.Name.didUpdateChat)

        let chat = try XCTUnwrap(notification.userInfo?[DMStoreNotification.UserInfoKey.chat] as? Chat)
        XCTAssertEqual(chat, Stubs.chat)
    }
}
