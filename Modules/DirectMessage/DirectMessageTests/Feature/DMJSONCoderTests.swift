import XCTest
@testable import DirectMessage

final class DMJSONCoderTests: XCTestCase {
    private lazy var sut = DMJSONCoder()
    
    func testEncode_WhenEncodingDate_ShouldParseDateToUInt() throws {
        let model = ModelWithDate(date: Date(timeIntervalSince1970: 0))
        
        let encodedData = try XCTUnwrap(sut.encoder.encode(model))
        
        XCTAssertEqual(String(data: encodedData, encoding: .utf8), "{\"date\":0}")
    }
    
    func testEncode_WhenEncodingDate_ShouldParseDateToUIntRoundingFloat() throws {
        let model = ModelWithDate(date: Date(timeIntervalSince1970: 1606425127.223))
        
        let encodedData = try XCTUnwrap(sut.encoder.encode(model))
        
        XCTAssertEqual(String(data: encodedData, encoding: .utf8), "{\"date\":1606425127}")
    }
    
    func testDecode_WhenDecodingJsonWithEmptyDate_ShouldUIntToDateCorrectly() throws {
        let json = try XCTUnwrap(jsonWithEmptyDate)
        
        let decodedData = try sut.decoder.decode(ModelWithDate.self, from: json)
        
        XCTAssertNotNil(decodedData.date)
        XCTAssertEqual(decodedData.date?.formatted, Date(timeIntervalSince1970: 0).formatted)
    }
    
    func testDecode_WhenDecodingJsonWithoutDate_ShouldUIntToDateCorrectly() throws {
        let json = try XCTUnwrap(jsonWithoutDate)
        
        let decodedData = try sut.decoder.decode(ModelWithDate.self, from: json)
        
        XCTAssertNil(decodedData.date)
    }
    
    func testDecode_WhenDecodingJsonWithDate_ShouldUIntToDateCorrectly() throws {
        let json = try XCTUnwrap(jsonWithDate)
        
        let decodedData = try sut.decoder.decode(ModelWithDate.self, from: json)
        
        XCTAssertNotNil(decodedData.date)
        XCTAssertEqual(decodedData.date?.formatted, Date(timeIntervalSince1970: 1606425127).formatted)
    }
}

private struct ModelWithDate: Codable {
    let date: Date?
}

private extension DMJSONCoderTests {
    var jsonWithoutDate: Data? { "{ }".data(using: .utf8) }
    
    var jsonWithEmptyDate: Data? { "{ \"date\":0 }".data(using: .utf8) }
    
    var jsonWithDate: Data? { "{ \"date\":1606425127 }".data(using: .utf8) }
}

private extension Date {
    var formatted: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter.string(from: self)
    }
}
