import XCTest
@testable import DirectMessage

final class DMChatPresenterTests: XCTestCase {
    private lazy var displaySpy = DMChatDisplaySpy()
    private lazy var coordinatorSpy = DMChatCoordinatingSpy()
    private lazy var sut = createSut()
    
    func testPresentMessage_WhenPresentingNewMessage_ShouldDisplayMessageOnController() {
        sut.present(message: messageCellMock)
        
        XCTAssertEqual(displaySpy.didCallDisplayMessageCell, 1)
        XCTAssertEqual(displaySpy.displayMessageCell?.sender.senderId, senderMock.senderId)
        XCTAssertEqual(displaySpy.displayMessageCell?.sender.displayName, senderMock.displayName)
        XCTAssertEqual(displaySpy.displayMessageCell?.messageId, messageCellMock.messageId)
        XCTAssertEqual(displaySpy.displayMessageCell?.sentDate.description, messageCellMock.sentDate.description)
        XCTAssertEqual(displaySpy.displayMessageCell?.textMessage, messageCellMock.textMessage)
    }

    func testPresentContact_WhenPresentingContact_ShouldDisplayNameAndAvatar() {
        sut.present(contact: senderMock)

        XCTAssertEqual(displaySpy.didCallDisplayNameAndAvatarCount, 1)
    }

    func testRetreat_ShouldPerformBackAction() {
        sut.retreat()

        XCTAssertEqual(coordinatorSpy.didPerformActionCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .back)
    }
}

private extension DMChatPresenterTests {
    func createSut() -> DMChatPresenter {
        let sut = DMChatPresenter(coordinator: coordinatorSpy)
        sut.viewController = displaySpy
        return sut
    }
    
    var senderMock: Contact {
        .init(id: 1, imageURL: "", nickname: "mock", name: "Mock Bond")
    }
    
    var messageCellMock: MessageCellModel {
        .init(sender: senderMock,
              messageId: "1",
              sentDate: Date(),
              kind: .text("message content"))
    }
}

private extension MessageCellModel {
    var textMessage: String? {
        guard case let .text(message) = self.kind else { return nil }
        return message
    }
}
