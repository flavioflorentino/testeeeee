import XCTest
@testable import DirectMessage

final class DMChatInteractorTests: XCTestCase {
    private lazy var serviceSpy = DMChatServiceSpy()
    private lazy var presenterSpy = DMChatPresenterSpy()
    private lazy var sut = createSut()
    
    func testSendMessage_WhenSendingMessage_ShouldCallServiceSendMessage() throws {
        let message = try XCTUnwrap(Stubs.message.content.textMessage)
        
        sut.send(messageText: message)
        XCTAssertEqual(serviceSpy.didCallSendMessageCount, 1)
        
        XCTAssertEqual(serviceSpy.sentMessage?.type, DMBrokerMessageType.text.rawValue)
        XCTAssertEqual(serviceSpy.sentMessage?.senderId, meMock.senderId)
        XCTAssertEqual(serviceSpy.sentMessage?.recipientId, replierMock.senderId)
        XCTAssertNotNil(serviceSpy.sentMessage?.sendDate)
        XCTAssertNotNil(serviceSpy.sentMessage?.readDate)
           
        let sentMessage = try XCTUnwrap(serviceSpy.sentMessage?.content.textMessage)
        XCTAssertEqual(sentMessage, message)
    }
        
    func testIsMessageFromCurrentChat_WhenRecevingMessageFromCorrectChat_ShouldReturnTrue() {
        let isFromCurrentChat = sut.isMessageFromCurrentChat(message: receivedMessageFromCurrentReplier, replier: replierMock)
        XCTAssertTrue(isFromCurrentChat)
    }
    
    func testIsMessageFromCurrentChat_WhenRecevingMessageFromOtherChat_ShouldReturnFalse() {
        let isFromCurrentChat = sut.isMessageFromCurrentChat(message: receivedMessageFromOtherReplier, replier: replierMock)
        XCTAssertFalse(isFromCurrentChat)
    }
    
    func testDidSendMessage_WhenSendingNewMessage_ShouldPresentMessageCell() throws {
        sut.didSend(message: sentMessage)
        XCTAssertEqual(presenterSpy.callPresentMessageCount, 1)
        
        let messageCellModel = try XCTUnwrap(presenterSpy.messageCellModel)
        XCTAssertFalse(messageCellModel.messageId.isEmpty)
        XCTAssertEqual(messageCellModel.sender.displayName, meMock.displayName)
        XCTAssertEqual(messageCellModel.sender.senderId, meMock.senderId)
        XCTAssertEqual(messageCellModel.sentDate.description, sentMessage.sendDate.description)
        XCTAssertTrue(messageCellModel.isSameTextMessage(content: sentMessage.content))
    }
    
    func testDidReceiveMessageFromCurrentChat_WhenRecevingMessage_ShouldPresentMessageCell() throws {
        sut.didReceive(message: receivedMessageFromCurrentReplier)
        XCTAssertEqual(presenterSpy.callPresentMessageCount, 1)
        
        let messageCellModel = try XCTUnwrap(presenterSpy.messageCellModel)
        XCTAssertFalse(messageCellModel.messageId.isEmpty)
        XCTAssertEqual(messageCellModel.sender.displayName, replierMock.displayName)
        XCTAssertEqual(messageCellModel.sender.senderId, replierMock.senderId)
        XCTAssertEqual(messageCellModel.sentDate.description, receivedMessageFromCurrentReplier.sendDate.description)
        XCTAssertTrue(messageCellModel.isSameTextMessage(content: receivedMessageFromCurrentReplier.content))
    }
    
    func testDidReceiveMessageFromOtherChat_WhenRecevingMessage_ShouldNotPresentMessageCell() throws {
        sut.didReceive(message: receivedMessageFromOtherReplier)
        
        XCTAssertEqual(presenterSpy.callPresentMessageCount, 0)
        XCTAssertNil(presenterSpy.messageCellModel)
    }

    func testGetContactProfile_ShouldCallPresentContact() {
        sut.getContactProfile()

        XCTAssertEqual(presenterSpy.didCallPresentContactCount, 1)
        XCTAssertEqual(sut.replier.id, presenterSpy.contact?.id)
    }

    func testRetreat_ShouldCallPresenterRetreat() {
        sut.retreat()

        XCTAssertEqual(presenterSpy.didCallRetreatCount, 1)
    }
}

private extension MessageCellModel {
    func isSameTextMessage(content: MessageContent) -> Bool {
        guard case let .text(message) = self.kind else { return false }
        guard case let .text(contentMessage) = content else { return false }
        return message == contentMessage
    }
}

private extension MessageContent {
    var textMessage: String? {
        guard case let .text(message) = self else { return nil }
        return message
    }
}

private extension DMChatInteractorTests {
    var meMock: Contact {
        .init(id: 1,
              imageURL: "",
              nickname: "Tiger",
              name: "Peter Parker")
    }
    
    var replierMock: Contact {
        .init(id: 2,
              imageURL: "",
              nickname: "MJ",
              name: "Mary Jane")
    }
    
    var sentMessage: Message {
        .init(type: .text, senderId: meMock.senderId, recipientId: replierMock.senderId, sendDate: Date(), readDate: nil, content: .text("Fala Gwen"))
    }
    
    var receivedMessageFromCurrentReplier: Message {
        .init(type: .text, senderId: replierMock.senderId, recipientId: meMock.senderId, sendDate: Date(), readDate: nil, content: .text("Fala tigrao"))
    }
    
    var receivedMessageFromOtherReplier: Message {
        .init(type: .text, senderId: "3", recipientId: meMock.senderId, sendDate: Date(), readDate: nil, content: .text("Fala Peter"))
    }
    
    func createSut() -> DMChatInteractor {
        .init(service: serviceSpy,
              presenter: presenterSpy,
              me: meMock,
              replier: replierMock)
    }
}
