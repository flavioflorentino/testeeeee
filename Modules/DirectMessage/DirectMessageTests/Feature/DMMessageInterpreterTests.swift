import XCTest
@testable import DirectMessage

final class DMMessageInterpreterTests: XCTestCase {
    private lazy var notificationSpy = DMStoreNotificationSpy()
    private lazy var storeSpy = DMStoreSpy(notification: notificationSpy)
    private lazy var sut = DMMessageInterpreter(dependencies: DependenciesContainerMock(storeSpy))
    
    // MARK: SocketInterpreter

    func testInterpret_WhenDataIsNotParsable_ShouldNotUpdateStore() {
        sut.interpret(data: Data())

        XCTAssertEqual(storeSpy.didCallAddMessageCount, 0)
    }
    
    func testInterpret_WhenDataIsParsable_ShouldUpdateStore() throws {
        sut.interpret(data: expectedTextMessage)
        
        XCTAssertEqual(storeSpy.didCallAddMessageCount, 1)
        
        let spiedMessage = try XCTUnwrap(storeSpy.addedMessage)
        XCTAssertEqual(spiedMessage.type, expectedMessage.type)
    }
    
    // MARK: DMMessageInterpreting - Message
    
    func testInterpretAsMessage_WhenInterpretingTextData_ShouldAddMessageOnStore() throws {
        sut.interpretAsMessage(expectedTextMessage)
        
        XCTAssertEqual(storeSpy.didCallAddMessageCount, 1)
        
        let spiedMessage = try XCTUnwrap(storeSpy.addedMessage)
        XCTAssertEqual(spiedMessage, expectedMessage)
    }
    
    func testInterpretAsMessage_WhenInterpretingDataWithWrongFrom_ShouldNotAddMessageOnStore() {
        sut.interpretAsMessage(dataMessageWithoutFrom)
        
        XCTAssertEqual(storeSpy.didCallAddMessageCount, 0)
        XCTAssertNil(storeSpy.addedMessage)
    }
    
    func testInterpretAsMessage_WhenInterpretingDataWithoutTo_ShouldNotAddMessageOnStore() {
        sut.interpretAsMessage(dataMessageWithoutTo)
        
        XCTAssertEqual(storeSpy.didCallAddMessageCount, 0)
        XCTAssertNil(storeSpy.addedMessage)
    }
    
    func testInterpretAsMessage_WhenInterpretingDataWithoutSendDate_ShouldNotAddMessageOnStore() {
        sut.interpretAsMessage(dataMessageWithoutSendDate)
        
        XCTAssertEqual(storeSpy.didCallAddMessageCount, 0)
        XCTAssertNil(storeSpy.addedMessage)
    }
    
    func testInterpretAsMessage_WhenInterpretingDataWithoutMessageType_ShouldNotAddMessageOnStore() {
        sut.interpretAsMessage(dataMessageWithoutMessageType)
        
        XCTAssertEqual(storeSpy.didCallAddMessageCount, 0)
        XCTAssertNil(storeSpy.addedMessage)
    }
    
    func testInterpretAsMessage_WhenInterpretingDataWithoutContent_ShouldNotAddMessageOnStore() {
        sut.interpretAsMessage(dataMessageWithoutContent)
        
        XCTAssertEqual(storeSpy.didCallAddMessageCount, 0)
        XCTAssertNil(storeSpy.addedMessage)
    }

    // MARK: DMMessageInterpreting - Chat

    func testInterpretAsChat_WhenDataIsNotParsableAsChats_ShouldAddChatOnStore() {
        sut.interpret(data: expectedTextMessage)

        XCTAssertEqual(storeSpy.didCallAddListOfChatsCount, 0)
        XCTAssertEqual(storeSpy.didCallAddChatCount, 0)
    }

    func testInterpretAsChat_WhenDataIsParsableAsEmptyChats_ShouldAddChatOnStore() {
        sut.interpret(data: dataBrokerMessageWithEmptyChatContent)

        XCTAssertEqual(storeSpy.didCallAddListOfChatsCount, 0)
        XCTAssertEqual(storeSpy.didCallAddChatCount, 0)
    }

    func testInterpretAsChat_WhenDataIsParsableAsChats_ShouldAddChatOnStore() throws {
        sut.interpret(data: dataBrokerMessageWithChatContent)

        XCTAssertEqual(storeSpy.didCallAddListOfChatsCount, 1)
        XCTAssertEqual(storeSpy.didCallAddChatCount, 1)

        XCTAssertEqual(storeSpy.chats.count, expectedChats.count)
        let spiedChat = try XCTUnwrap(storeSpy.chats.first)
        XCTAssertEqual(spiedChat, expectedChat)
    }
}

private extension DMMessageInterpreterTests {
    var expectedMessage: Message {
        Stubs.message
    }

    var expectedChat: Chat {
        Stubs.chat
    }

    var expectedChats: [Chat] {
        [expectedChat]
    }
    
    var expectedTextMessage: Data {
        """
            {
              "from": "17",
              "to": "25",
              "contact_type": "P2P",
              "status": "SENT",
              "send_date": 1606241862,
              "read_date": 0,
              "received_date": 0,
              "token": "c9605a80-dd15-4a92-a897-0ff9af74b75a",
              "message_type": "TEXT",
              "content": {
                "text": "Oi, tudo bem?"
              }
            }
        """.data(using: .utf8) ?? Data()
    }
    
    var dataMessageWithoutFrom: Data {
        """
            {
              "to": "25",
              "contact_type": "P2P",
              "status": "SENT",
              "send_date": 1606241862,
              "received_date": 0,
              "read_date": 0,
              "token": "c9605a80-dd15-4a92-a897-0ff9af74b75a",
              "message_type": "TEXT",
              "content": {
                "text": "Oi, tudo bem?"
              }
            }
        """.data(using: .utf8) ?? Data()
    }
    
    var dataMessageWithoutTo: Data {
        """
            {
              "from": "17",
              "contact_type": "P2P",
              "status": "SENT",
              "send_date": 1606241862,
              "received_date": 0,
              "read_date": 0,
              "token": "c9605a80-dd15-4a92-a897-0ff9af74b75a",
              "message_type": "TEXT",
              "content": {
                "text": "Oi, tudo bem?"
              }
            }
        """.data(using: .utf8) ?? Data()
    }
    
    var dataMessageWithoutSendDate: Data {
        """
            {
              "from": "17",
              "to": "25",
              "contact_type": "P2P",
              "status": "SENT",
              "read_date": 1606241862,
              "received_date": 0,
              "token": "c9605a80-dd15-4a92-a897-0ff9af74b75a",
              "message_type": "TEXT",
              "content": {
                "text": "Oi, tudo bem?"
              }
            }
        """.data(using: .utf8) ?? Data()
    }
    
    var dataMessageWithoutMessageType: Data {
        """
            {
              "from": "17",
              "to": "25",
              "contact_type": "P2P",
              "status": "SENT",
              "send_date": 1606241862,
              "received_date": 0,
              "read_date": 0,
              "token": "c9605a80-dd15-4a92-a897-0ff9af74b75a",
              "content": {
                "text": "Oi, tudo bem?"
              }
            }
        """.data(using: .utf8) ?? Data()
    }
    
    var dataMessageWithoutContent: Data {
        """
            {
              "from": "17",
              "to": "25",
              "contact_type": "P2P",
              "status": "SENT",
              "send_date": 1606241862,
              "received_date": 0,
              "read_date": 0,
              "token": "c9605a80-dd15-4a92-a897-0ff9af74b75a",
              "message_type": "TEXT",
            }
        """.data(using: .utf8) ?? Data()
    }

    var dataBrokerMessageWithChatContent: Data {
        (try? DMJSONCoder().encoder.encode(Stubs.brokerMessageWithChatContent)) ?? Data()
    }

    var dataBrokerMessageWithEmptyChatContent: Data {
        let model = DMBrokerMessage(type: .chats,
                                    from: Stubs.senderId,
                                    to: Stubs.recipientId,
                                    content: [Chat]())
        return (try? DMJSONCoder().encoder.encode(model)) ?? Data()
    }

}
