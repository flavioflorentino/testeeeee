import XCTest
@testable import DirectMessage

private extension DateExtensionsTests {

    var dateStub: Date {
        var dateComponents = DateComponents()
        dateComponents.year = 2020
        dateComponents.month = 11
        dateComponents.day = 3
        dateComponents.hour = 9
        dateComponents.minute = 0
        return Calendar.current.date(from: dateComponents) ?? Date()
    }
}

final class DateExtensionsTests: XCTestCase {

    func testAsMessageTime() {
        XCTAssertEqual(dateStub.asMessageTime, "09:00")
    }

    func testAsMessageDate() {
        XCTAssertEqual(dateStub.asMessageDate, "Nov, 03 2020")
    }
}
