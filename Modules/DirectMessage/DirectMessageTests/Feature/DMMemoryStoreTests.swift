import XCTest
@testable import DirectMessage

final class DMMemoryStoreTests: XCTestCase {
    private lazy var notificationSpy = DMStoreNotificationSpy()
    private lazy var sut = DMMemoryStore(notification: notificationSpy)
    
    func testAddMessage_WhenAddingNewMessage_ShouldTriggerNotification() {
        sut.add(message: firstMessage)
        
        XCTAssertEqual(notificationSpy.didCallSaveNewMessageCount, 1)
        XCTAssertEqual(notificationSpy.savedNewMessage, firstMessage)
    }
    
    func testAddMessage_WhenAddingNewMessage_ShouldBeAbleToRetrieveMessages() {
        sut.add(message: firstMessage)

        let addedMessageExpectation = expectation(description: "The messaged should be added")

        sut.getMessages(by: Stubs.senderId) { messages in
            XCTAssertEqual(messages.count, 1)
            XCTAssertEqual(messages.first, firstMessage)
            addedMessageExpectation.fulfill()
        }

        waitForExpectations(timeout: 0.5, handler: nil)
    }

    func testAddMessage_WhenAddingNewMessageWithNoPreviousChats_ShouldBeAbleToRetrieveChats() {
        sut.add(message: firstMessage)

        let newChatExpectation = expectation(description: "A new chat should be created and added")

        sut.getChats { chats in
            let message = chats.first?.lastMessage
            let messages = chats.first?.messages

            XCTAssertEqual(message, firstMessage)
            XCTAssertEqual(messages, [firstMessage])

            newChatExpectation.fulfill()
        }

        waitForExpectations(timeout: 0.5, handler: nil)
    }

    func testAddMessage_WhenThereIsAPreviousChat_ShouldUpdateLastMessage() throws {
        sut.add(message: firstMessage)

        let newChatExpectation = expectation(description: "A new chat should be created and added")

        sut.getChats { chats in
            newChatExpectation.fulfill()
        }

        sut.add(message: secondMessage)

        let chatUpdatingExpectation = expectation(description: "Current chat should be updated")

        sut.getChats { chats in
            let updatedMessage = chats.first?.lastMessage
            let updatedMessages = chats.first?.messages

            XCTAssertEqual(updatedMessage, secondMessage)
            XCTAssertEqual(updatedMessages, [firstMessage, secondMessage])

            chatUpdatingExpectation.fulfill()
        }

        wait(for: [newChatExpectation, chatUpdatingExpectation], timeout: 0.5, enforceOrder: true)
    }

    func testGetMessages_WhenSenderIdIsNotFound_ShouldReturnEmptyMessages() {
        let exp = expectation(description: "No messages should retrievable")

        sut.getMessages(by: String()) {
            XCTAssert($0.isEmpty)
            exp.fulfill()
        }

        waitForExpectations(timeout: 0.5, handler: nil)
    }

    func testAddChat_WhenThereAreNoExistingChats_ShouldAddChatOnStore() {
        let chat = Stubs.chat

        sut.add(chat: chat, replacing: false)

        let newChatExpectation = expectation(description: "The chat should be added")

        sut.getChats { chats in
            XCTAssertEqual(chat, chats.first)
            XCTAssertEqual(notificationSpy.didCallUpdateChatCount, 1)

            newChatExpectation.fulfill()
        }

        waitForExpectations(timeout: 0.5, handler: nil)
    }

    func testAddChat_WhenReplacingExistingChat_ShouldUpdateChatOnStoreReplacingMessages() throws {
        let startingChats = getStartingChats()

        sut.add(chats: startingChats, replacing: false)

        let chat = try XCTUnwrap(startingChats.first)

        let startingMessagesExpectation = expectation(description: "The starting messages should be retrievable")

        sut.getMessages(by: chat.from) { messages in
            XCTAssertEqual(messages.count, 1)
            XCTAssertEqual(messages.first, firstMessage)

            startingMessagesExpectation.fulfill()
        }

        let replacedMessagesExpectation = expectation(description: "The existing messages should be replaced")

        let newChat = Chat(from: chat.from,
                           unreadMessagesCount: chat.unreadMessagesCount,
                           lastMessage: secondMessage)
        newChat.messages = []
        sut.add(chat: newChat, replacing: true)

        sut.getMessages(by: newChat.from) { messages in
            XCTAssert(messages.isEmpty)
            replacedMessagesExpectation.fulfill()
        }

        wait(for: [startingMessagesExpectation, replacedMessagesExpectation], timeout: 0.5, enforceOrder: true)
    }

    func testAddChat_WhenNotReplacingExistingChat_ShouldUpdateChatOnStoreAppendingMessages() throws {
        let startingChats = getStartingChats()

        sut.add(chats: startingChats, replacing: false)

        let chat = try XCTUnwrap(startingChats.first)

        let startingMessagesExpectation = expectation(description: "The starting messages should be retrievable")

        sut.getMessages(by: chat.from) { messages in
            XCTAssertEqual(messages.count, 1)
            XCTAssertEqual(messages.first, firstMessage)

            startingMessagesExpectation.fulfill()
        }

        let replacedMessagesExpectation = expectation(description: "The existing messages should be replaced")

        let newChat = Chat(from: chat.from,
                           unreadMessagesCount: chat.unreadMessagesCount,
                           lastMessage: secondMessage)
        sut.add(chat: newChat, replacing: false)

        sut.getMessages(by: newChat.from) { messages in
            XCTAssertEqual(messages.count, 2)
            XCTAssertEqual(messages.first, firstMessage)
            XCTAssertEqual(messages.last, secondMessage)
            replacedMessagesExpectation.fulfill()
        }

        wait(for: [startingMessagesExpectation, replacedMessagesExpectation], timeout: 0.5, enforceOrder: true)
    }
}

private extension DMMemoryStoreTests {
    var firstMessage: Message {
        Stubs.message
    }

    var secondMessage: Message {
        .init(type: .text,
              senderId: Stubs.recipientId,
              recipientId: Stubs.senderId,
              sendDate: Date(timeIntervalSince1970: TimeInterval(1606241862)),
              readDate: nil,
              content: .text("Qual é boa?"))
    }

    func getStartingChats() -> [Chat] {
        [Chat(from: Stubs.senderId, unreadMessagesCount: 1, lastMessage: firstMessage),
         Chat(from: "1", unreadMessagesCount: 0, lastMessage: firstMessage),
         Chat(from: "2", unreadMessagesCount: 0, lastMessage: firstMessage)]
    }
}
