import XCTest
@testable import DirectMessage

final class DMChatServiceTests: XCTestCase {
    private var socketContractSpy = DMSocketContractSpy()
    private var notificationSpy = NotificationCenterSpy()
    private var serviceDelegateSpy = DMChatServiceDelegateSpy()
    private let jsonDecoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .secondsSince1970
        return decoder
    }()
    private lazy var dependenciesMock = DependenciesContainerMock(socketContractSpy, notificationSpy)
    private lazy var sut = createSut()
    
    func testSetupObservers_WhenChatScreenIsOpen_ShouldObserveNewMessages() throws {
        sut.observeNewMessages()
        
        let observer = try XCTUnwrap(
            notificationSpy.observers
            .first(where: { $0.notificationName == DMStoreNotification.Name.didSaveNewMessage })
        )
        
        XCTAssertTrue(observer.instance is DMChatService)
    }
    
    func testSetupObservers_WhenChatScreenIsOpen_ShouldListenCorrectNumberOfObservers() {
        sut.observeNewMessages()
        
        XCTAssertEqual(notificationSpy.didCallAddObserverCount, 1)
        XCTAssertEqual(notificationSpy.observers.count, 1)
    }
    
    func testSendMessage_WhenSendingMessage_ShouldPublishCorrectlyOnSocket() throws {
        let sentMessage = Stubs.message
        
        sut.send(message: sentMessage)
        
        XCTAssertEqual(socketContractSpy.callPublishCount, 1)
        XCTAssertEqual(socketContractSpy.publish?.topic, DMSocket.Topic.sendMessage)
        XCTAssertEqual(serviceDelegateSpy.didCallDidSendMessageCount, 1)
        XCTAssertEqual(serviceDelegateSpy.sentMessage, sentMessage)
        
        let publishedMessage = try XCTUnwrap(
            jsonDecoder.decode(Message.self, from: socketContractSpy.publish?.content ?? Data())
        )
        XCTAssertEqual(publishedMessage, sentMessage)
    }
    
    func testDidReceiveMessage_WhenReceivingNewMessageNotification_ShouldReceiveMessageObjectInNotification() {
        let storeNotification = DMStoreNotification(notificationCenter: notificationSpy)
        let notification = storeNotification.createNewNotification(fromSavedMessage: Stubs.message)
        
        sut.didReceiveMessage(notification: notification)
        
        XCTAssertEqual(serviceDelegateSpy.didCallReceiveMessageCount, 1)
        XCTAssertEqual(serviceDelegateSpy.receivedMessage, Stubs.message)
    }
}

private extension DMChatServiceTests {
    func createSut() -> DMChatService {
        sut = DMChatService(dependencies: dependenciesMock, notificationCenter: notificationSpy)
        sut.delegate = serviceDelegateSpy
        return sut
    }
}
