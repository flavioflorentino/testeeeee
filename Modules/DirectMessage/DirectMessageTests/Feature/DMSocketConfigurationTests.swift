import XCTest
@testable import DirectMessage

final class DMSocketConfigurationTests: XCTestCase {
    private lazy var dependenciesMock = SocketDependencyContainerMock()
    private lazy var sut = DMSocketConfiguration(dependencies: dependenciesMock)
    
    func testClientId_WhenRetrieving_ShouldReturnCorrectValue() {
        let clientId = sut.clientId
        
        XCTAssertEqual(clientId, dependenciesMock.consumer?.consumerId)
    }
    
    func testConnectionId_WhenRetrieving_ShouldReturnCorrectValue() {
        let clientId = dependenciesMock.consumer?.consumerId ?? -1
        
        let connectionId = sut.connectionId
        
        XCTAssertEqual(connectionId.count, 41)
        XCTAssertEqual(connectionId.prefix(5), "\(clientId)UUID")
    }
    
    func testPassword_WhenRetrieving_ShouldReturnCorrectValue() {
        let password = sut.password
        
        XCTAssertEqual(password, dependenciesMock.user?.token)
    }
    
    // TODO: How to test values from Environment?
}

private final class SocketDependencyContainerMock: DMSocketConfiguration.Dependencies {
    var consumer: DMConsumerContract? = ConsumerContractMock()
    var user: DMUserManagerContract? = UserManagerContractMock()
}
