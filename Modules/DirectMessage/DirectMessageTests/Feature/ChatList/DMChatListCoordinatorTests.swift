import XCTest
@testable import DirectMessage

final class DMChatListCoordinatorTests: XCTestCase {
    private lazy var viewController = UIViewController()
    private lazy var navControllerSpy = NavigationControllerSpy(rootViewController: viewController)
    private lazy var sut: DMChatListCoordinator = {
        let coordinator = DMChatListCoordinator(dependencies: DependenciesContainerMock())
        coordinator.viewController = navControllerSpy.topViewController
        return coordinator
    }()
    
    func testPerform_WhenActionIsChat_ShouldPushTheChatScene() {
        sut.perform(action: .chat(contact: ContactStubs.contact1))
        
        XCTAssertTrue(navControllerSpy.isPushViewControllerCalled)
        XCTAssertTrue(navControllerSpy.pushedViewController is DMChatViewController)
    }
}
