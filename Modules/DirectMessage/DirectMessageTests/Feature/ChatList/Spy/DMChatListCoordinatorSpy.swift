import UIKit
@testable import DirectMessage

final class DMChatListCoordinatorSpy: DMChatListCoordinating {
    var viewController: UIViewController?
    
    lazy var didCallPerformActionCount = 0
    lazy var actionPerformed: DMChatListAction? = nil
    
    func perform(action: DMChatListAction) {
        didCallPerformActionCount += 1
        actionPerformed = action
    }
}
