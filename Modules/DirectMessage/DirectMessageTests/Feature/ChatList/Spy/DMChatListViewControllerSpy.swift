import UI
@testable import DirectMessage

final class DMChatListViewControllerSpy: DMChatListDisplaying {
    var didCallDisplayTitleCount = 0
    var didCallDisplayChatsCount = 0
    var didCallDisplayLoadingStateCount = 0
    var didCallDisplayStateCount = 0
    
    var displayedTitle: String?
    var displayedChats: [DMChatListCellViewModeling]?
    var displayedStateModel: StatefulFeedbackViewModel?
    
    func display(title: String) {
        didCallDisplayTitleCount += 1
        displayedTitle = title
    }
    
    func display(chats: [DMChatListCellViewModeling]) {
        didCallDisplayChatsCount += 1
        displayedChats = chats
    }
    
    func displayLoadingState() {
        didCallDisplayLoadingStateCount += 1
    }
    
    func displayState(forModel model: StatefulFeedbackViewModel) {
        didCallDisplayStateCount += 1
        displayedStateModel = model
    }
}
