import UI
import XCTest
@testable import DirectMessage

final class DMChatListPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = DMChatListCoordinatorSpy()
    private lazy var viewControllerSpy = DMChatListViewControllerSpy()
    private lazy var sut: DMChatListPresenter = {
        let presenter = DMChatListPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testStartLoading_WhenLoadingPresenter_ShouldDisplayTitleAndLoadingIndicator() {
        sut.startLoading()
        
        XCTAssertEqual(viewControllerSpy.didCallDisplayTitleCount, 1)
        XCTAssertEqual(viewControllerSpy.displayedTitle, Strings.ChatList.title)
        XCTAssertEqual(viewControllerSpy.didCallDisplayLoadingStateCount, 1)
    }
    
    func testPresentContacts_WhenItHaveNoChats_ShouldDisplayEmptyState() throws {
        sut.present(contacts: [])
        
        XCTAssertEqual(viewControllerSpy.didCallDisplayStateCount, 1)
        XCTAssertEqual(viewControllerSpy.didCallDisplayChatsCount, 0)

        let displayedStateContent = try XCTUnwrap(viewControllerSpy.displayedStateModel?.content)
        XCTAssertEqual(displayedStateContent.title, Strings.ChatList.EmptyState.title)
        XCTAssertEqual(displayedStateContent.description, Strings.ChatList.EmptyState.description)
    }
    
    func testPresentContacts_WhenRetrievedChats_ShouldDisplayList() {
        sut.present(contacts: [
            .init(id: 1, imageURL: "", nickname: "@1", name: ""),
            .init(id: 2, imageURL: "", nickname: "@2", name: "")
        ])
        
        XCTAssertEqual(viewControllerSpy.didCallDisplayStateCount, 0)
        XCTAssertEqual(viewControllerSpy.didCallDisplayChatsCount, 1)
        XCTAssertEqual(viewControllerSpy.displayedChats?.count, 2)
    }
    
    func testDidNextStep_WhenPresentingChat_ShouldDisplayList() {
        let expectedAction: DMChatListAction = .chat(contact: .init(id: 1, imageURL: "", nickname: "@1", name: ""))
        sut.didNextStep(action: expectedAction)
        
        XCTAssertEqual(coordinatorSpy.didCallPerformActionCount, 1)
        XCTAssertEqual(coordinatorSpy.actionPerformed, expectedAction)
    }
}
