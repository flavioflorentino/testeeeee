import XCTest
@testable import DirectMessage

final class DMSocketTopicTests: XCTestCase {
    private typealias sut = DMSocket.Topic
    
    func testSubscription_WhenSubscribingAnUser_ShouldRetrieveTopicCorrectly() {
        let subscription = sut.subscription(for: 123)
        XCTAssertEqual(subscription, "topic/user/123")
    }
    
    func testSendMessage_WhenPostingAMessageIntoBroker_ShouldPostOnCorrectTopic() {
        let postMessageTopic = sut.sendMessage
        XCTAssertEqual(postMessageTopic, "$aws/rules/dm_send_message")
    }
}
