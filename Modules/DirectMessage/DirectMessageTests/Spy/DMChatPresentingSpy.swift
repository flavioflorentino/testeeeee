@testable import DirectMessage

final class DMChatPresenterSpy: DMChatPresenting {

    lazy var viewController: DMChatDisplaying? = nil
    lazy var callPresentMessageCount = 0
    lazy var didCallPresentContactCount = 0
    lazy var didCallRetreatCount = 0
    lazy var messageCellModel: MessageCellModel? = nil
    lazy var contact: Contact? = nil
    
    func present(message: MessageCellModel) {
        callPresentMessageCount += 1
        messageCellModel = message
    }

    func present(contact: Contact) {
        didCallPresentContactCount += 1
        self.contact = contact
    }

    func retreat() {
        didCallRetreatCount += 1
    }

}
