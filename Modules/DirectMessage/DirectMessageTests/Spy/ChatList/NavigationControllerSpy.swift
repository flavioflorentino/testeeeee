import UIKit

public final class NavigationControllerSpy: UINavigationController {
    private(set) var isPushViewControllerCalled: Bool = false
    private(set) var pushedViewController: UIViewController?
    
    public override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        pushedViewController = viewController
        isPushViewControllerCalled = true
        super.pushViewController(viewController, animated: false)
    }
}
