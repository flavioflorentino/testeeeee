@testable import DirectMessage

final class DMChatCoordinatingSpy: DMChatCoordinating {

    private(set) var didPerformActionCount = 0
    private(set) var action: DMChatAction
    var viewController: UIViewController?

    init(action: DMChatAction = .back) {
        self.action = action
    }

    func perform(action: DMChatAction) {
        didPerformActionCount += 1
        self.action = action
    }

}
