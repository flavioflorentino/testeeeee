@testable import DirectMessage

final class DMChatServiceDelegateSpy: DMChatServiceDelegate {
    lazy var didCallDidSendMessageCount = 0
    lazy var sentMessage: Message? = nil
    lazy var didCallReceiveMessageCount = 0
    lazy var receivedMessage: Message? = nil
    
    func didSend(message: Message) {
        didCallDidSendMessageCount += 1
        sentMessage = message
    }
    
    func didReceive(message: Message) {
        didCallReceiveMessageCount += 1
        receivedMessage = message
    }
}
