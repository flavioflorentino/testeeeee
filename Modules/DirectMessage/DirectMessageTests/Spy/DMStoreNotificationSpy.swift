@testable import DirectMessage

final class DMStoreNotificationSpy: StoreNotificating {

    lazy var savedNewMessage: Message? = nil
    lazy var savedChat: Chat? = nil
    lazy var didCallCreateNewNotificationMessage = 0
    lazy var didCallSaveNewMessageCount = 0
    lazy var didCallCreateNewNotificationChatCount = 0
    lazy var didCallUpdateChatCount = 0
    
    func didSaveNew(message: Message) {
        didCallSaveNewMessageCount += 1
        savedNewMessage = message
    }
    
    func createNewNotification(fromSavedMessage message: Message) -> Notification {
        didCallCreateNewNotificationMessage += 1
        let userInfo = [DMStoreNotification.UserInfoKey.message: message]
        return Notification(name: .init(DMStoreNotification.Name.didSaveNewMessage),
                            object: self,
                            userInfo: userInfo)
    }

    func createNewNotification(from chat: Chat) -> Notification {
        didCallCreateNewNotificationChatCount += 1
        let userInfo = [DMStoreNotification.UserInfoKey.chat: chat]
        return Notification(name: .init(DMStoreNotification.Name.didUpdateChat),
                            object: self,
                            userInfo: userInfo)
    }

    func didUpdate(chat: Chat) {
        didCallUpdateChatCount += 1
        savedChat = chat
    }

}
