import Foundation

final class NotificationCenterSpy: NotificationCenter {
    typealias NotificationObserver = (instance: Any, notificationName: String)
    
    lazy var didCallAddObserverCount = 0
    lazy var didCallPostNotificationCount = 0
    lazy var observers: [NotificationObserver] = []
    lazy var postedNotifications: [Notification] = []
    
    override func addObserver(_ observer: Any, selector aSelector: Selector, name aName: NSNotification.Name?, object anObject: Any?) {
        didCallAddObserverCount += 1
        observers.append(
            NotificationObserver(instance: observer,
                                 notificationName: aName?.rawValue ?? String())
        )
    }
    
    override func post(_ notification: Notification) {
        didCallPostNotificationCount += 1
        postedNotifications.append(notification)
    }
}
