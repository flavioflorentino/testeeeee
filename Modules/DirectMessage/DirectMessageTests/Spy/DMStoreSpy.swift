@testable import DirectMessage

final class DMStoreSpy: DMStoring {

    static var shared: DMStoring = DMStoreSpy.shared
    var notification: StoreNotificating

    lazy var addedMessage: Message? = nil
    lazy var chats = [Chat]()
    lazy var senderIdGot = String()
    lazy var didCallAddMessageCount = 0
    lazy var didCallAddChatCount = 0
    lazy var didCallAddListOfChatsCount = 0

    init(notification: StoreNotificating = DMStoreNotificationSpy()) {
        self.notification = notification
    }

    func add(chat: Chat, replacing: Bool) {
        didCallAddChatCount += 1
        chats.append(chat)
        notification.didUpdate(chat: chat)
    }

    func add(chats: [Chat], replacing: Bool) {
        didCallAddListOfChatsCount += 1
        chats.forEach({ add(chat: $0, replacing: replacing) })
    }
    
    func add(message: Message) {
        didCallAddMessageCount += 1
        addedMessage = message
        notification.didSaveNew(message: message)
    }

    func getChats(completion: (([Chat]) -> Void)) {
        completion(chats)
    }

    func getMessages(by senderId: String, completion: (([Message]) -> Void)) {
        senderIdGot = senderId
        let messages = chats.first(where: { $0.from == senderId })?.messages ?? []
        completion(messages)
    }
}
