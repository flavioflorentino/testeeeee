import DirectMessage

final class SocketContractSpy: DMSocketContract {
    var callPublishCount = 0
    var publish: (content: Data, topic: String)? = nil
    
    public func publish(content: Data, onTopic topic: String, completion: @escaping ((Error?) -> Void)) {
        callPublishCount += 1
        publish = (content: content, topic: topic)
        
        completion(nil)
    }
}
