@testable import DirectMessage

final class DMChatServiceSpy: DMChatServicing {
        
    lazy var delegateSpy = DMChatServiceDelegateSpy()
    lazy var delegate: DMChatServiceDelegate? = delegateSpy
    
    lazy var didCallObserveNewMessages = 0
    lazy var didCallReceiveMessageNotification = 0
    lazy var didCallSendMessageCount = 0
    lazy var sentMessage: Message? = nil
    lazy var receivedMessageNotification: Notification? = nil
    
    func observeNewMessages() {
        didCallObserveNewMessages += 1
    }
    
    func send(message: Message) {
        didCallSendMessageCount += 1
        sentMessage = message
    }
    
    func didReceiveMessage(notification: Notification) {
        didCallReceiveMessageNotification += 1
        receivedMessageNotification = notification
    }
}
