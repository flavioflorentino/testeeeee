@testable import DirectMessage

final class DMChatDisplaySpy: DMChatDisplaying {

    lazy var didCallStopLoadingCount = 0
    lazy var didCallDisplayMessageCell = 0
    lazy var didCallDisplayNameAndAvatarCount = 0
    lazy var displayMessageCell: MessageCellModel? = nil
    
    func stopLoading() {
        didCallStopLoadingCount += 1
    }
    
    func display(message: MessageCellModel) {
        didCallDisplayMessageCell += 1
        displayMessageCell = message
    }

    func display(name: String, avatar: URL?) {
        didCallDisplayNameAndAvatarCount += 1
    }
}
