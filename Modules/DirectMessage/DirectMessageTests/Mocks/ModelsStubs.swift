import Core
@testable import DirectMessage

enum Stubs {
    private static let dateFormatterHelper = DecodableDateFormatter()

    static let senderId = "17"

    static let recipientId = "25"
    
    static let message: Message =
        .init(type: .text,
              senderId: senderId,
              recipientId: recipientId,
              sendDate: Date(timeIntervalSince1970: TimeInterval(1606241862)),
              readDate: nil,
              content: .text("Oi, tudo bem?"))

    static let chat: Chat =
        .init(from: senderId, unreadMessagesCount: 1, lastMessage: message)

    static let brokerMessageWithChatContent: DMBrokerMessage<[Chat]> =
        .init(type: .chats,
              from: senderId,
              to: recipientId,
              content: [chat])
}
