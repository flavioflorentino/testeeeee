import DirectMessage

struct ConsumerContractMock: DMConsumerContract {
    var consumerId: Int = 0
    
    var name: String = "Flavio Teixeira"
    
    var username: String? = "flavinho"
    
    var avatarUrl: String? = "https://png.pngtree.com/png-vector/20190710/ourmid/pngtree-user-vector-avatar-png-image_1541962.jpg"
}
