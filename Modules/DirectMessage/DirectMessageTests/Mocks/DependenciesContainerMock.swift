import Foundation
@testable import DirectMessage

final class DependenciesContainerMock: Dependencies {
    lazy var consumer: DMConsumerContract? = resolve(default: ConsumerContractMock())
    lazy var socket: DMSocketContract? = resolve(default: DMSocketContractSpy())
    lazy var notificationCenter = resolve(default: NotificationCenterSpy())
    lazy var user: DMUserManagerContract? = nil
    lazy var database: DMStoring = resolve(default: DMStoreSpy())
    lazy var json: DMJSONCoding = resolve(default: DMJSONCoder())
    
    private let dependencies: [Any]
    init(_ dependencies: Any...) {
        self.dependencies = dependencies
    }
}

extension DependenciesContainerMock {
    func resolve<T>(default: T) -> T {
        guard let mock = dependencies.first(where: { $0 is T }) as? T else {
            return `default`
        }
        return mock
    }
}
