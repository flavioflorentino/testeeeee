@testable import DirectMessage

enum ContactStubs {
    static let contact1: Contact =
        .init(id: 0,
              imageURL: "",
              nickname: "Tiger",
              name: "Peter Parker")
}
