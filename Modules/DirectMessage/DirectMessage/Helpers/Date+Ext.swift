extension Date {
    enum DateFormat: String, CaseIterable {
        case now
        case minutes = "mm"
        case hours = "HH"
        case time = "HH:mm"
        case day = "EEEE"
        case ddmmyy = "dd/MM/yy"
        case mmmddyyyy = "MMM, dd yyyy"

        static func formatToNow(from date: Date) -> DateFormat {
            let diff = Calendar.current.dateComponents([.day, .hour, .minute], from: date, to: Date())
            let (days, hours, minutes) = (diff.day ?? 0, diff.hour ?? 0, diff.minute ?? 0)
            if days >= 7 {
                return .ddmmyy
            } else if hours >= 24 {
                return .day
            } else if minutes > 1 {
                return .minutes
            }
            return .now
        }

        var asFormatter: DateFormatter {
            let formatter = DateFormatter()
            formatter.dateFormat = self.rawValue
            return formatter
        }

        func string(from date: Date) -> String {
            let formatter = self.asFormatter
            let formatted = formatter.string(from: date)
            switch self {
            case .now:
                return Strings.Date.now
            case .minutes:
                return Strings.Date.minutes(formatted)
            case .hours:
                return Strings.Date.hours(formatted)
            case .day, .ddmmyy, .time, .mmmddyyyy:
                return formatted.capitalized
            }
        }
    }

    var asChatDate: String {
        DateFormat
            .formatToNow(from: self)
            .string(from: self)
    }

    var asMessageTime: String {
        DateFormat.time.string(from: self)
    }

    var asMessageDate: String {
        DateFormat.mmmddyyyy.string(from: self)
    }
}
