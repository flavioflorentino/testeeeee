import UI
import UIKit

extension UINavigationBar {
    func asDirectMessageStyle() {
        let shadowColor = Colors.grayscale200.color
        setUpNavigationBarDefaultAppearance(backgroundColor: Colors.groupedBackgroundSecondary.color, shadowColor: shadowColor)
        setBackgroundImage(nil, for: .default)
        shadowImage = shadowColor.asShadowImage()
    }
}

extension UINavigationController {
    func asDirectMessageStyle() {
        setNavigationBarHidden(false, animated: true)
        navigationBar.asDirectMessageStyle()
    }
}
