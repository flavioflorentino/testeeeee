import Foundation

public enum DMChatFactory {
    public static func make(sender: Contact, recipient: Contact) -> DMChatViewController {
        let container = DependencyContainer()
        var service: DMChatServicing = DMChatService(dependencies: container)
        let coordinator: DMChatCoordinating = DMChatCoordinator()
        let presenter: DMChatPresenting = DMChatPresenter(coordinator: coordinator)
        let interactor = DMChatInteractor(service: service, presenter: presenter, me: sender, replier: recipient)
        let viewController = DMChatViewController(interactor: interactor)

        service.delegate = interactor
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
