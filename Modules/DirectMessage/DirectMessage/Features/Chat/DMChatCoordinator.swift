import UIKit

enum DMChatAction {
    case back
}

protocol DMChatCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: DMChatAction)
}

final class DMChatCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - DMChatCoordinating

extension DMChatCoordinator: DMChatCoordinating {
    func perform(action: DMChatAction) {
        switch action {
        case .back:
            viewController?.navigationController?.popViewController(animated: true)
        }
    }
}
