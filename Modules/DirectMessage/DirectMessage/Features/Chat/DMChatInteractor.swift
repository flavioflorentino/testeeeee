import Foundation

protocol DMChatInteracting: AnyObject {
    var me: Contact { get }
    var replier: Contact { get }
    
    func send(messageText: String)
    func isMessageFromCurrentChat(message: Message, replier: Contact) -> Bool
    func getContactProfile()
    func retreat()
}

final class DMChatInteractor: NSObject {
    private let service: DMChatServicing
    private let presenter: DMChatPresenting
    private(set) var me: Contact
    private(set) var replier: Contact
    
    init(service: DMChatServicing,
         presenter: DMChatPresenting,
         me: Contact,
         replier: Contact) {
        self.service = service
        self.presenter = presenter
        self.me = me
        self.replier = replier
        self.service.observeNewMessages()
    }
}

// MARK: - DMChatInteracting

extension DMChatInteractor: DMChatInteracting {
    func send(messageText: String) {
        service.send(message: .init(type: .text,
                                    senderId: me.senderId,
                                    recipientId: replier.senderId,
                                    sendDate: Date(),
                                    readDate: nil,
                                    content: .text(messageText)))
    }
    
    func isMessageFromCurrentChat(message: Message, replier: Contact) -> Bool {
        message.senderId == replier.senderId
    }

    func getContactProfile() {
        presenter.present(contact: replier)
    }

    func retreat() {
        presenter.retreat()
    }
}

// MARK: - DMChatServiceDelegate

extension DMChatInteractor: DMChatServiceDelegate {
    func didSend(message: Message) {
        guard case let .text(messageText) = message.content else { return }
        
        let messageCell = MessageCellModel(sender: me,
                                           messageId: UUID().uuidString,
                                           sentDate: message.sendDate,
                                           kind: .text(messageText))
        presenter.present(message: messageCell)
    }

    func didReceive(message: Message) {
        guard isMessageFromCurrentChat(message: message, replier: replier),
              case let .text(messageText) = message.content
        else { return }

        let messageModel = MessageCellModel(sender: replier,
                                            messageId: UUID().uuidString,
                                            sentDate: message.sendDate,
                                            kind: .text(messageText))
        presenter.present(message: messageModel)
    }
}
