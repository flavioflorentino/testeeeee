import Foundation

protocol DMChatPresenting: AnyObject {
    var viewController: DMChatDisplaying? { get set }
    func present(message: MessageCellModel)
    func present(contact: Contact)
    func retreat()
}

final class DMChatPresenter {
    private let coordinator: DMChatCoordinating
    weak var viewController: DMChatDisplaying?

    init(coordinator: DMChatCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - DMChatPresenting

extension DMChatPresenter: DMChatPresenting {
    func present(message: MessageCellModel) {
        viewController?.display(message: message)
    }

    func present(contact: Contact) {
        viewController?.display(name: "@\(contact.nickname)", avatar: URL(string: contact.imageURL))
    }

    func retreat() {
        coordinator.perform(action: .back)
    }
}
