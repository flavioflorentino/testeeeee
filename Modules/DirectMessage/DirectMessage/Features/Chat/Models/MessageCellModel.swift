import Foundation
import MessageKit

struct MessageCellModel: MessageType {
    var sender: SenderType
    var messageId: String
    var sentDate: Date
    var kind: MessageKind

    var time: String {
        sentDate.asMessageTime
    }

    var chatDate: String {
        sentDate.asMessageDate
    }
}
