import Foundation

struct Message: Codable, Equatable {
    let type: String
    let senderId: String
    let recipientId: String
    let sendDate: Date
    let receivedDate: Date?
    let readDate: Date?
    let status: String
    let content: MessageContent
    let contactType: ContactType
    
    enum CodingKeys: String, CodingKey {
        case type = "message_type"
        case senderId = "from"
        case recipientId = "to"
        case sendDate = "send_date"
        case receivedDate = "received_date"
        case readDate = "read_date"
        case contactType = "contact_type"
        case content
        case status
    }
    
    init(type: DMBrokerMessageType,
         senderId: String,
         recipientId: String,
         sendDate: Date,
         readDate: Date?,
         content: MessageContent) {
        self.type = type.rawValue
        self.senderId = senderId
        self.recipientId = recipientId
        self.sendDate = sendDate
        self.content = content
        
        // TODO: Valores default paliativos por motivos de: Android não sabe tratar optional
        self.contactType = .p2p
        self.status = "SENT"
        self.readDate = Date(timeIntervalSince1970: 0)
        self.receivedDate = Date(timeIntervalSince1970: 0)
    }
}

extension Message: CustomStringConvertible {
    var description: String {
        """
        text: \(content.value)
        sendDate: \(sendDate.asMessageDate)
        """
    }
}

enum MessageContent: Codable, Equatable {
    // Cases
    case text(String)

    var value: String {
        switch self {
        case .text(let value):
            return value
        }
    }
    
    // Keys
    enum CodingKeys: CodingKey {
        case text
    }
    
    // Codable
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let textValue = try container.decode(String.self, forKey: .text)
        self = .text(textValue)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        if case let .text(value) = self {
            try container.encode(value, forKey: .text)
        }
    }
}

enum ContactType: String, Equatable, Codable {
    case p2p = "P2P"
    case group = "GROUP"
}
