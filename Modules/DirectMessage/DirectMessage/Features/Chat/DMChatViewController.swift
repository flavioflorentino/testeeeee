import AssetsKit
import Foundation
import InputBarAccessoryView
import MessageKit
import UI
import UIKit

protocol DMChatDisplaying: AnyObject {
    func stopLoading()
    func display(message: MessageCellModel)
    func display(name: String, avatar: URL?)
}

extension DMChatViewController.Layout {
    enum Content {
        static let dateFormat = "MMM dd, HH:mm"
    }
    
    enum Measures {
        static let cellTopLabelHeight: CGFloat = Spacing.base02
        static let messageBottomLabelHeight: CGFloat = Spacing.base03
        static let inputBarBorderWidth: CGFloat = Border.light
        static let inputBarCornerRadius: CGFloat = UIScreen.main.bounds.width * (20 / 375)
    }

    enum Fonts {
        static let username = Typography.title(.small).font()
        static let input = Typography.bodyPrimary().font()
        static let date = Typography.caption().font()
    }

    enum Colors {
        static let backButton = UI.Colors.grayscale700.color
        static let usernameText = UI.Colors.grayscale700.color
        static let standardNavBar = UI.Colors.grayscale050.color
        static let inputText = UI.Colors.black.color
        static let inputTint = UI.Colors.branding600.color
        static let inputPlaceholder = UI.Colors.grayscale300.color
        static let inputBorder = UI.Colors.grayscale200.color.cgColor
        static let inputSeparator = UI.Colors.grayscale100.color
        static let date = UI.Colors.grayscale300.color
        static let sentMessage = UI.Colors.white.color
        static let receivedMessage = UI.Colors.grayscale050.color
        static let inputBar = UI.Colors.groupedBackgroundSecondary.color
        static let bodyBackground = UI.Colors.backgroundPrimary.color
    }

    enum NavigationBarAppearance {
        case standard
        case directMessage
    }

    enum Images {
        static let backButton = Resources.Icons.icoGreenLeftArrow.image.withRenderingMode(.alwaysTemplate)
        static let sendButton = Resources.Icons.icoDmSend.image.withRenderingMode(.alwaysOriginal)
    }

    enum Alignment {
        static let sentMessageTime = LabelAlignment(textAlignment: .right, textInsets: Insets.sentTime)
        static let receivedMessageTime = LabelAlignment(textAlignment: .left, textInsets: Insets.receivedTime)
    }

    enum Insets {
        static let textContainer = UIEdgeInsets(top: 12, left: Spacing.base02, bottom: 12, right: Spacing.base02)
        static let placeholderLabel = UIEdgeInsets(top: 12, left: 20, bottom: 12, right: 20)
        static let sendButton = UIEdgeInsets(top: Spacing.base00, left: 2, bottom: Spacing.base00, right: 2)
        static let chat = UIEdgeInsets(top: Spacing.base01, left: Spacing.none, bottom: Spacing.base02, right: Spacing.none)
        static let sentTime = UIEdgeInsets(top: Spacing.none, left: Spacing.none, bottom: Spacing.none, right: Spacing.base01)
        static let receivedTime = UIEdgeInsets(top: Spacing.none, left: Spacing.base01, bottom: Spacing.none, right: Spacing.none)
    }
}

public final class DMChatViewController: MessagesViewController {
    fileprivate enum Layout { }
    
    // MARK: - Properties
    
    private lazy var messages: [MessageCellModel] = []
    
    private let interactor: DMChatInteracting
    
    private let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = Layout.Content.dateFormat
        return formatter
    }()

    private lazy var backButton: UIBarButtonItem = {
        let button = UIButton()
        button.setImage(Layout.Images.backButton, for: .normal)
        button.tintColor = Layout.Colors.backButton
        button.addTarget(self, action: #selector(retreat), for: .touchUpInside)
        return UIBarButtonItem(customView: button)
    }()

    private lazy var avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView
            .imageStyle(AvatarImageStyle(size: .xSmall))
            .with(\.backgroundColor, .backgroundPrimary())
        return imageView
    }()

    private lazy var avatarButtonItem = UIBarButtonItem(customView: avatarImageView)

    private lazy var usernameLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, Layout.Colors.usernameText)
            .with(\.font, Layout.Fonts.username)
        label.numberOfLines = 1
        label.clipsToBounds = true
        return label
    }()

    private lazy var usernameButtonItem = UIBarButtonItem(customView: usernameLabel)

    private lazy var inputBarBorderView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.layer.borderWidth = Layout.Measures.inputBarBorderWidth
        view.layer.cornerRadius = Layout.Measures.inputBarCornerRadius
        view.layer.masksToBounds = true
        return view
    }()
    
    // MARK: - Initialization
    
    init(interactor: DMChatInteracting) {
        self.interactor = interactor
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle

    override public func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        interactor.getContactProfile()
    }

    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        updateNavigationBar(appearance: .directMessage)
    }

    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        updateMessageSendButton()
    }

    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        updateNavigationBar(appearance: .standard)
    }

    override public func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)

        guard #available(iOS 13, *), traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) else {
            return
        }
        updateMessageInputBar()
    }
}

// MARK: - Private methods

private extension DMChatViewController {
    func isLastSectionVisible() -> Bool {
        guard messages.isNotEmpty else { return false }
        
        let lastIndexPath = IndexPath(item: 0, section: messages.count - 1)
        
        return messagesCollectionView.indexPathsForVisibleItems.contains(lastIndexPath)
    }
}

// MARK: - UI

private extension DMChatViewController {
    func setupUI() {
        view.backgroundColor = Layout.Colors.bodyBackground
        setupMessageCollectionView()
        setupMessageInputBar()
    }

    func updateNavigationBar(appearance: Layout.NavigationBarAppearance) {
        switch appearance {
        case .directMessage:
            navigationController?.asDirectMessageStyle()
            navigationItem.leftBarButtonItems = [backButton, avatarButtonItem, usernameButtonItem]
            usernameLabel.isHidden = false
        case .standard:
            setUpNavigationBarDefaultAppearance(backgroundColor: Layout.Colors.standardNavBar)
            navigationItem.leftBarButtonItems = [backButton, avatarButtonItem]
        }
    }

    func setupMessageCollectionView() {
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        scrollsToBottomOnKeyboardBeginsEditing = true
        maintainPositionOnKeyboardFrameChanged = true
        messagesCollectionView.backgroundColor = Layout.Colors.bodyBackground
        messagesCollectionView.contentInset = Layout.Insets.chat
        if let layout = messagesCollectionView.collectionViewLayout as? MessagesCollectionViewFlowLayout {
            layout.setMessageIncomingAvatarSize(.zero)
            layout.setMessageOutgoingAvatarSize(.zero)
            layout.setMessageOutgoingMessageBottomLabelAlignment(Layout.Alignment.sentMessageTime)
            layout.setMessageIncomingMessageBottomLabelAlignment(Layout.Alignment.receivedMessageTime)
        }
    }

    func setupMessageInputTextView() {
        messageInputBar.inputTextView.textColor = Layout.Colors.inputText
        messageInputBar.inputTextView.tintColor = Layout.Colors.inputTint
        messageInputBar.inputTextView.font = Layout.Fonts.input
        messageInputBar.inputTextView.placeholderLabel.font = Layout.Fonts.input
        messageInputBar.inputTextView.placeholder = Strings.Chat.Input.placeholder
        messageInputBar.inputTextView.placeholderTextColor = Layout.Colors.inputPlaceholder
        messageInputBar.inputTextView.textContainerInset = Layout.Insets.textContainer
        messageInputBar.inputTextView.placeholderLabelInsets = Layout.Insets.placeholderLabel
    }

    func setupMessageSendButton() {
        messageInputBar.sendButton.title = nil
        messageInputBar.sendButton.image = Layout.Images.sendButton
        messageInputBar.sendButton.isHidden = true
        messageInputBar.sendButton
            .onEnabled { $0.isHidden = false }
            .onDisabled { $0.isHidden = true }
    }

    func setupMessageInputBarBorder() {
        messageInputBar.insertSubview(inputBarBorderView, belowSubview: messageInputBar.contentView)
        inputBarBorderView.snp.makeConstraints {
            $0.top.left.bottom.equalTo(messageInputBar.inputTextView)
            $0.right.equalTo(messageInputBar.sendButton).offset(-4)
        }
    }

    func updateMessageInputBar() {
        messageInputBar.separatorLine.backgroundColor = Layout.Colors.inputSeparator
        messageInputBar.backgroundView.backgroundColor = Layout.Colors.inputBar
        inputBarBorderView.layer.borderColor = Layout.Colors.inputBorder
    }

    func setupMessageInputBar() {
        messageInputBar.delegate = self
        setupMessageInputTextView()
        setupMessageSendButton()
        setupMessageInputBarBorder()
        updateMessageInputBar()
    }

    func updateMessageSendButton() {
        let measureForButton = messageInputBar.inputTextView.bounds.height
        messageInputBar.sendButton.setSize(CGSize(width: measureForButton, height: measureForButton), animated: true)
        messageInputBar.sendButton.contentEdgeInsets = Layout.Insets.sendButton
    }
}

// MARK: - Actions

private extension DMChatViewController {
    @objc
    func retreat() {
        interactor.retreat()
    }

    func cleanTextField() {
        messageInputBar.inputTextView.text.removeAll()
    }

    func insert(message: MessageCellModel) {
        messages.append(message)

        messagesCollectionView.performBatchUpdates({
            messagesCollectionView.insertSections([messages.count - 1])
            if messages.count >= 2 {
                messagesCollectionView.reloadSections([messages.count - 2])
            }
        }, completion: { [weak self] _ in
            if self?.isLastSectionVisible() == true {
                self?.messagesCollectionView.scrollToBottom(animated: true)
            }
        })
    }
}

// MARK: - MessagesDataSource

extension DMChatViewController: MessagesDataSource {
    public func currentSender() -> SenderType {
        interactor.me
    }
    
    public func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        messages[indexPath.section]
    }
    
    public func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        messages.count
    }

    public func cellTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        guard indexPath.section == 0 else { return nil }
        return NSAttributedString(string: messages[indexPath.section].chatDate,
                                  attributes: [
                                    .font: Layout.Fonts.date,
                                    .foregroundColor: Layout.Colors.date
                                  ])
    }

    public func messageBottomLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        NSAttributedString(string: messages[indexPath.section].time,
                           attributes: [.font: Layout.Fonts.date, .foregroundColor: Layout.Colors.date])
    }
}

// MARK: - MessagesLayoutDelegate

extension DMChatViewController: MessagesLayoutDelegate {
    public func cellTopLabelHeight(for message: MessageType,
                                   at indexPath: IndexPath,
                                   in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        indexPath.section == 0 ? Layout.Measures.messageBottomLabelHeight : .leastNormalMagnitude
    }

    public func messageBottomLabelHeight(for message: MessageType,
                                         at indexPath: IndexPath,
                                         in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        Layout.Measures.messageBottomLabelHeight
    }
 }

// MARK: - MessagesDisplayDelegate

extension DMChatViewController: MessagesDisplayDelegate {
    public func messageStyle(for message: MessageType,
                             at indexPath: IndexPath,
                             in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
        .bubbleOutline(Colors.grayscale100.color)
    }

    public func backgroundColor(for message: MessageType,
                                at indexPath: IndexPath,
                                in messagesCollectionView: MessagesCollectionView) -> UIColor {
        isFromCurrentSender(message: message) ? Layout.Colors.receivedMessage : Layout.Colors.sentMessage
    }

    public func textColor(for message: MessageType,
                          at indexPath: IndexPath,
                          in messagesCollectionView: MessagesCollectionView) -> UIColor {
        Colors.black.color
    }

    public func configureAvatarView(_ avatarView: AvatarView,
                                    for message: MessageType,
                                    at indexPath: IndexPath,
                                    in messagesCollectionView: MessagesCollectionView) {
        avatarView.isHidden = true
    }
}

// MARK: - InputBarAccessoryViewDelegate

extension DMChatViewController: InputBarAccessoryViewDelegate {
    public func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {
        interactor.send(messageText: text)
        cleanTextField()
    }
}

// MARK: - DMChatDisplaying

extension DMChatViewController: DMChatDisplaying {
    func stopLoading() { }
    
    func display(message: MessageCellModel) {
        insert(message: message)
    }

    func display(name: String, avatar: URL?) {
        usernameLabel.text = name
        avatarImageView.setImage(
            url: avatar,
            placeholder: Resources.Placeholders.greenAvatarPlaceholder.image
        )
    }
}
