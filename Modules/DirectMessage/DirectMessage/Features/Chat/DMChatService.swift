import Foundation
import Socket

protocol DMChatServicing {
    var delegate: DMChatServiceDelegate? { get set }
    
    func observeNewMessages()
    func didReceiveMessage(notification: Notification)
    func send(message: Message)
}

protocol DMChatServiceDelegate: AnyObject {
    func didSend(message: Message)
    func didReceive(message: Message)
}

final class DMChatService: DMChatServicing {
    typealias Dependencies = HasConsumer & HasSocket & HasJsonCoding
    
    // MARK: Attributes
    private let dependencies: Dependencies
    private let notificationCenter: NotificationCenter
    weak var delegate: DMChatServiceDelegate?
    
    // MARK: Initialization
    init(dependencies: Dependencies,
         notificationCenter: NotificationCenter = .default) {
        self.dependencies = dependencies
        self.notificationCenter = notificationCenter
    }
    
    // MARK: Notifications
    func observeNewMessages() {
        notificationCenter.addObserver(self,
                                       selector: #selector(didReceiveMessage(notification:)),
                                       name: .init(DMStoreNotification.Name.didSaveNewMessage),
                                       object: nil)
    }
    
    @objc
    func didReceiveMessage(notification: Notification) {
        guard let message = notification.userInfo?[DMStoreNotification.UserInfoKey.message] as? Message
        else { return }
        
        delegate?.didReceive(message: message)
    }
    
    // MARK: DMChatServicing
    func send(message: Message) {
        guard let messageData = try? dependencies.json.encoder.encode(message) else { return }
        
        dependencies.socket?.publish(content: messageData, onTopic: DMSocket.Topic.sendMessage) { [weak self] _ in
            self?.delegate?.didSend(message: message)
        }
    }
}
