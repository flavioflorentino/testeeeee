import UIKit

public enum DMChatListFactory {
    public static func make() -> UIViewController {
        let container = DependencyContainer()
        let service: DMChatListServicing = DMChatListService(dependencies: container)
        let coordinator: DMChatListCoordinating = DMChatListCoordinator(dependencies: container)
        let presenter: DMChatListPresenting = DMChatListPresenter(coordinator: coordinator)
        let interactor = DMChatListInteractor(service: service, presenter: presenter)
        let viewController = DMChatListViewController(interactor: interactor)
        viewController.hidesBottomBarWhenPushed = true

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
