import Foundation
import MessageKit

final class Chat: Codable {
    let from: String
    var unreadMessagesCount: Int
    var lastMessage: Message
    var messages = [Message]()

    init(from: String, unreadMessagesCount: Int, lastMessage: Message) {
        self.from = from
        self.unreadMessagesCount = unreadMessagesCount
        self.lastMessage = lastMessage
        self.messages = [lastMessage]
    }
    
    enum CodingKeys: String, CodingKey {
        case from
        case unreadMessagesCount = "not_read"
        case lastMessage = "last_message"
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.from = try container.decode(String.self, forKey: .from)
        self.unreadMessagesCount = try container.decode(Int.self, forKey: .unreadMessagesCount)
        self.lastMessage = try container.decode(Message.self, forKey: .lastMessage)
        self.messages.append(self.lastMessage)
    }
}

extension Chat: Equatable {
    static func == (lhs: Chat, rhs: Chat) -> Bool {
        lhs.from == rhs.from
            && lhs.unreadMessagesCount == rhs.unreadMessagesCount
            && lhs.lastMessage == rhs.lastMessage
            && lhs.messages == rhs.messages
    }
}

extension Chat: CustomStringConvertible {
    var description: String {
        """
        from: \(from)
        unreadMessagesCount: \(unreadMessagesCount)
        lastMessage: \(lastMessage)
        """
    }
}
