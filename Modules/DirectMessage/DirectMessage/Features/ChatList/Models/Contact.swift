import Foundation
import MessageKit

public struct Contact: SenderType, Equatable {
    let id: Int
    let imageURL: String
    let nickname: String
    let name: String

    public init(id: Int, imageURL: String, nickname: String, name: String) {
        self.id = id
        self.imageURL = imageURL
        self.nickname = nickname
        self.name = name
    }
    
    public var senderId: String {
        "\(id)"
    }

    public var displayName: String {
        nickname
    }
}
