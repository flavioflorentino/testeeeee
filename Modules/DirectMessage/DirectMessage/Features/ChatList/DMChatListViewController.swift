import UI
import UIKit
import SnapKit

protocol DMChatListDisplaying: AnyObject {
    func display(title: String)
    func display(chats: [DMChatListCellViewModeling])
    func displayLoadingState()
    func displayState(forModel model: StatefulFeedbackViewModel)
}

private extension DMChatListViewController.Layout {
    enum Size {
        static let tableViewRowHeight: CGFloat = 90.0
    }
}

final class DMChatListViewController: ViewController<DMChatListInteracting, UIView> {
    fileprivate enum Layout { }
    
    // MARK: - Outlets
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(DMChatListCell.self, forCellReuseIdentifier: DMChatListCell.identifier)
        tableView.isHidden = true
        tableView.backgroundColor = Colors.backgroundPrimary.color
        tableView.estimatedRowHeight = Sizing.base10
        tableView.separatorColor = Colors.grayscale050.color
        tableView.separatorInset = UIEdgeInsets(top: Spacing.none,
                                                left: Spacing.base02,
                                                bottom: Spacing.none,
                                                right: Spacing.base02)
        
        tableView.tableFooterView = UIView()
        
        return tableView
    }()
    
    private lazy var dataSource: TableViewDataSource<Int, DMChatListCellViewModeling> = {
        let dataSource = TableViewDataSource<Int, DMChatListCellViewModeling>(view: tableView)
        dataSource.itemProvider = { tableView, indexPath, viewModel in
            let cellIdentifier = DMChatListCell.identifier
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? DMChatListCell
            cell?.configure(with: viewModel)
            return cell
        }
        return dataSource
    }()
    
    // MARK: - Lifecycle & ViewConfiguration

    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.fetchContacts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationController()
    }

    override func buildViewHierarchy() {
        view.addSubview(tableView)
    }
    
    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }

    override func configureViews() {
        tableView.dataSource = dataSource
        tableView.delegate = self
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }
}

// MARK: - Private methods

private extension DMChatListViewController {
    func setupNavigationController() {
        guard let navigationController = navigationController else { return }
        
        title = Strings.ChatList.title
        
        navigationController.setNavigationBarHidden(false, animated: true)
        navigationController.navigationBar.navigationStyle(StandardNavigationStyle())
        
        let backButton = UIBarButtonItem()
        backButton.title = String()
        navigationController.navigationBar.topItem?.backBarButtonItem = backButton
        navigationController.navigationBar.backItem?.title = String()
    }
}

// MARK: - UITableViewDelegate

extension DMChatListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        interactor.chatSelected(with: indexPath.row)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - DMChatListDisplaying

extension DMChatListViewController: DMChatListDisplaying {
    func display(title: String) {
        self.title = title
    }
    
    func displayLoadingState() {
        beginState()
    }
    
    func displayState(forModel model: StatefulFeedbackViewModel) {
        safeEndState { [weak self] in
            self?.endState(model: model)
        }
    }
    
    func display(chats: [DMChatListCellViewModeling]) {
        tableView.isHidden = false
        dataSource.add(items: chats, to: 0)
        endState()
    }
    
    private func safeEndState(completion: @escaping (() -> Void)) {
        endState {
            DispatchQueue.main.async {
                completion()
            }
        }
    }
}

// MARK: - StatefulProviding

extension DMChatListViewController: StatefulProviding {
    func statefulViewForError() -> StatefulViewing {
        StatefulEmptyStateView()
    }
}
