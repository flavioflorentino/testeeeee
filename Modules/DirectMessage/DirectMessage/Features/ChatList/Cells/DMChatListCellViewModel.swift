import Foundation

protocol DMChatListCellViewModeling {
    var imageURL: String { get }
    var nickname: String { get }
    var name: String { get }
}

struct DMChatListCellViewModel: DMChatListCellViewModeling {
    private(set) var imageURL: String
    private(set) var nickname: String
    private(set) var name: String
}
