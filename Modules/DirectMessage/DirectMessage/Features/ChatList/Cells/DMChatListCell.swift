import AssetsKit
import Foundation
import SnapKit
import UI
import UIKit

extension DMChatListCell.Layout {
    enum Size {
        static let image = CGSize(width: 40, height: 40)
    }
}

final class DMChatListCell: UITableViewCell {
    fileprivate enum Layout { }
    
    // MARK: - Outlets
    
    private lazy var iconImageView: UIImageView = {
        let image = UIImageView(image: Resources.Placeholders.greenAvatarPlaceholder.image)
        image.cornerRadius = .full
        return image
    }()
    
    private lazy var nicknameLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .small))
        label.textColor = Colors.black.color
        return label
    }()
    
    private lazy var messagePreviewLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .default))
        label.textColor = Colors.grayscale600.color
        return label
    }()
    // MARK: - Setup
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(with viewModel: DMChatListCellViewModeling) {
        nicknameLabel.text = viewModel.nickname
        messagePreviewLabel.text = viewModel.name
    }
}

// MARK: - ViewConfiguration

extension DMChatListCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(iconImageView)
        contentView.addSubview(nicknameLabel)
        contentView.addSubview(messagePreviewLabel)
    }
    
    func setupConstraints() {
        iconImageView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.size.equalTo(Layout.Size.image)
        }
        
        nicknameLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.leading.equalTo(iconImageView.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        messagePreviewLabel.snp.makeConstraints {
            $0.top.equalTo(nicknameLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalTo(iconImageView.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base02)
        }
    }
    
    func configureStyles() {
        backgroundColor = Colors.backgroundPrimary.color
    }
}
