import UI
import Foundation

protocol DMChatListPresenting: AnyObject {
    var viewController: DMChatListDisplaying? { get set }
    
    func startLoading()
    func present(contacts: [Contact])
    func didNextStep(action: DMChatListAction)
}

final class DMChatListPresenter {
    // MARK: Properties
    private let coordinator: DMChatListCoordinating
    weak var viewController: DMChatListDisplaying?
    
    private lazy var emptyStateFeedbackViewModel: StatefulFeedbackViewModel = {
        let content = (title: Strings.ChatList.EmptyState.title,
                       description: Strings.ChatList.EmptyState.description)
        
        return .init(image: Assets.Chats.emptystate.image,
                     content: content,
                     button: nil,
                     secondaryButton: nil)
    }()

    // MARK: Initialization
    init(coordinator: DMChatListCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - DMChatListPresenting

extension DMChatListPresenter: DMChatListPresenting {
    func startLoading() {
        viewController?.display(title: Strings.ChatList.title)
        viewController?.displayLoadingState()
    }
    
    func present(contacts: [Contact]) {
        let contactViewModels = contacts.map {
            DMChatListCellViewModel(imageURL: $0.imageURL, nickname: $0.nickname, name: $0.name)
        }
        
        contactViewModels.isEmpty
            ? viewController?.displayState(forModel: emptyStateFeedbackViewModel)
            : viewController?.display(chats: contactViewModels)
    }
    
    func didNextStep(action: DMChatListAction) {
        coordinator.perform(action: action)
    }
}
