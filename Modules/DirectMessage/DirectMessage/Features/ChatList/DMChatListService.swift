import Core
import Foundation

protocol DMChatListServicing {
    func fetchContacts(completion: @escaping ((Result<[Contact], ApiError>) -> Void))
}

final class DMChatListService {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - DMChatListServicing

extension DMChatListService: DMChatListServicing {
    func fetchContacts(completion: @escaping ((Result<[Contact], ApiError>) -> Void)) {
        // Mock temporário para testes para uso somente da POC
        let contacts = [
            Contact(id: 8_586_120,
                    imageURL: "https://png.pngtree.com/png-vector/20190710/ourmid/pngtree-user-vector-avatar-png-image_1541962.jpg",
                    nickname: "arturkida",
                    name: "Artur Kida"),
            Contact(id: 299_879,
                    imageURL: "https://picpay-dev.s3.amazonaws.com/profiles-processed-images/b291a02e808984ad3930dfad14ab5cc7.200.jpg",
                    nickname: "borsoi",
                    name: "Victor Antonio Carlete Borsoi"),
            Contact(id: 8_655_270,
                    imageURL: "https://picpay-dev.s3.sa-east-1.amazonaws.com/profiles/8306013166c6a044c89b8da05c43ddf8.jpg",
                    nickname: "cleiton.uchoa",
                    name: "Cleiton Uchoa")
        ]
        // completion(.success(contacts))
        completion(.success([]))
    }
}
