import UIKit

enum DMChatListAction: Equatable {
    case chat(contact: Contact)
    
    static func == (lhs: DMChatListAction, rhs: DMChatListAction) -> Bool {
        switch (lhs, rhs) {
        case let (.chat(lhsContact), .chat(rhsContact)):
            return lhsContact == rhsContact
        }
    }
}

protocol DMChatListCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: DMChatListAction)
}

final class DMChatListCoordinator {
    weak var viewController: UIViewController?
    
    typealias Dependencies = HasConsumer
    private let dependencies: Dependencies
    
    private let sender: Contact
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
        
        sender = Contact(id: dependencies.consumer?.consumerId ?? 0,
                         imageURL: dependencies.consumer?.avatarUrl ?? "",
                         nickname: dependencies.consumer?.username ?? "",
                         name: dependencies.consumer?.name ?? "")
    }
}

// MARK: - DMChatListCoordinating

extension DMChatListCoordinator: DMChatListCoordinating {
    func perform(action: DMChatListAction) {
        guard case .chat(let contact) = action else { return }
    
        let vc = DMChatFactory.make(sender: sender, recipient: contact)

        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
}
