import Foundation
import Core

protocol DMChatListInteracting: AnyObject {
    func fetchContacts()
    func chatSelected(with index: Int)
}

final class DMChatListInteractor {
    private let service: DMChatListServicing
    private let presenter: DMChatListPresenting
    
    private var contacts = [Contact]()

    init(service: DMChatListServicing, presenter: DMChatListPresenting) {
        self.service = service
        self.presenter = presenter
    }
}

// MARK: - DMChatListInteracting

extension DMChatListInteractor: DMChatListInteracting {
    func fetchContacts() {
        presenter.startLoading()
        service.fetchContacts { [weak self] result in
            switch result {
            case .success(let contacts):
                self?.contacts = contacts
                self?.presenter.present(contacts: contacts)
            case .failure:
                break
            }
        }
    }
    
    func chatSelected(with index: Int) {
        guard index < contacts.count, let contact = contacts[safe: index] else { return }
        presenter.didNextStep(action: .chat(contact: contact))
    }
}
