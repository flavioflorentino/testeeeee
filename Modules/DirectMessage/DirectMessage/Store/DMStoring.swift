import Foundation

protocol DMStoring {
    static var shared: DMStoring { get }
    
    var notification: StoreNotificating { get }
    
    func add(message: Message)

    func add(chat: Chat, replacing: Bool)

    func add(chats: [Chat], replacing: Bool)
    
    func getMessages(by senderId: String, completion: (([Message]) -> Void))

    func getChats(completion: (([Chat]) -> Void))
}
