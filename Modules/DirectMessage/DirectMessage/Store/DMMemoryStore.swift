import Foundation

final class DMMemoryStore {
    static let shared: DMStoring = DMMemoryStore()
    
    let notification: StoreNotificating

    private lazy var chats = [Chat]()
    
    init(notification: StoreNotificating = DMStoreNotification()) {
        self.notification = notification
    }
}

extension DMMemoryStore: DMStoring {
    func add(message: Message) {
        if let chat = chats.first(where: { $0.from == message.senderId || $0.from == message.recipientId }) {
            chat.lastMessage = message
            chat.messages.append(message)
        } else {
            let chat = Chat(from: message.senderId, unreadMessagesCount: 1, lastMessage: message)
            chats.append(chat)
        }
        notification.didSaveNew(message: message)
    }

    func add(chat: Chat, replacing: Bool) {
        guard let existingChat = chats.first(where: { chat.from == $0.from }) else {
            chats.append(chat)
            notification.didUpdate(chat: chat)
            return
        }
        existingChat.lastMessage = chat.lastMessage
        existingChat.unreadMessagesCount = chat.unreadMessagesCount
        if replacing {
            existingChat.messages = chat.messages
        } else {
            existingChat.messages.append(chat.lastMessage)
        }
        notification.didUpdate(chat: existingChat)
    }

    func add(chats: [Chat], replacing: Bool) {
        chats.forEach({ add(chat: $0, replacing: replacing) })
    }

    func getMessages(by senderId: String, completion: (([Message]) -> Void)) {
        guard let messages = chats.first(where: { $0.from == senderId })?.messages else {
            completion([])
            return
        }
        completion(messages)
    }

    func getChats(completion: (([Chat]) -> Void)) {
        completion(chats)
    }
}
