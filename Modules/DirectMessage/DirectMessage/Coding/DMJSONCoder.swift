import Foundation

protocol DMJSONCoding {
    var encoder: JSONEncoder { get }
    var decoder: JSONDecoder { get }
}

final class DMJSONCoder: DMJSONCoding {
    lazy var encoder: JSONEncoder = {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .custom { date, encoder in
            var container = encoder.singleValueContainer()
            let seconds = UInt(date.timeIntervalSince1970)
            try container.encode(seconds)
        }
        return encoder
    }()
    
    lazy var decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .custom { decoder in
            var container = try decoder.singleValueContainer()
            let timeInterval = TimeInterval(try container.decode(UInt.self))
            let date = Date(timeIntervalSince1970: timeInterval)
            return date
        }
        return decoder
    }()
}
