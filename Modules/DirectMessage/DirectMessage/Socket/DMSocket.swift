import Foundation
import Socket
import MQTTClient

public enum DMSocketFactory {
    public static func make() -> DMSocket {
        DMSocket()
    }
}

public final class DMSocket: NSObject {
    // MARK: Attributes
    public static let identifier: String = "DirectMessageSocket"
    private let configuration: SocketConfiguration
    private let session: MQTTSession
    private var interpreters = [String: SocketInterpreter]()
    
    private var connectionClosedByBroker = false
    
    // MARK: Initialization
    init(configuration: DMSocketConfiguration = DMSocketConfiguration(),
         session: MQTTSession = MQTTSession()) {
        self.session = session
        self.configuration = configuration
        super.init()
        
        let transport = MQTTWebsocketTransport()
        transport.url = configuration.url
        transport.port = configuration.port
        transport.tls = configuration.tls
        transport.additionalHeaders = configuration.additionalHeaders
        
        session.transport = transport
        session.delegate = self
        session.clientId = configuration.connectionId
        session.userName = configuration.userName
        session.password = configuration.password
    }
    
    deinit {
        disconnect()
    }
    
    // MARK: Notification
    private func subscribeOnUserTopic() {
        let clientId = configuration.clientId
        let topic = DMSocket.Topic.subscription(for: clientId)
        
        session.subscribe(toTopic: topic, at: .atMostOnce)
        interpreters = [topic: DMMessageInterpreter()]
    }
}

extension DMSocket: Socket {
    public var identifier: String {
        DMSocket.identifier
    }
    
    public func connect() {
        session.connect()
    }
    
    public func disconnect() {
        session.disconnect()
    }
    
    public func publish(content: Data,
                        additionalProperties: [String: Any],
                        completion: @escaping Socket.PublishCompletion) {
        session.publishData(content,
                            onTopic: DMSocket.Topic.sendMessage,
                            retain: false,
                            qos: .atMostOnce,
                            publishHandler: completion)
    }
}

// swiftlint:disable implicitly_unwrapped_optional function_parameter_count
extension DMSocket: MQTTSessionDelegate {
    public func handleEvent(_ session: MQTTSession!, event eventCode: MQTTSessionEvent, error: Error!) {
        switch eventCode {
        case .connected:
            subscribeOnUserTopic()
            
        case .connectionClosedByBroker:
            connectionClosedByBroker = true

        default:
            // TODO: Handle errors and disconnections
            break
        }
    }
    
    public func subAckReceived(_ session: MQTTSession!, msgID: UInt16, grantedQoss qoss: [NSNumber]!) { }
    
    public func newMessage(_ session: MQTTSession!, data: Data!, onTopic topic: String!, qos: MQTTQosLevel, retained: Bool, mid: UInt32) {
        interpreters[topic]?.interpret(data: data)
    }
    
    public func sending(_ session: MQTTSession!, type: MQTTCommandType, qos: MQTTQosLevel, retained: Bool, duped: Bool, mid: UInt16, data: Data!) { }

    public func connectionClosed(_ session: MQTTSession!) {
        guard connectionClosedByBroker else { return }
        session.connect()
        connectionClosedByBroker = false
    }
}
// swiftlint:enable implicitly_unwrapped_optional function_parameter_count
