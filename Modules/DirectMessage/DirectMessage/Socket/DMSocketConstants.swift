import Foundation

extension DMSocket {
    enum Topic {
        /// Used to listen a specific topic for a given user
        static func subscription(for clientId: Int) -> String {
            "topic/user/\(clientId)"
        }
        
        /// Used to send messages to another user
        static let sendMessage: String = "$aws/rules/dm_send_message"
    }
}
