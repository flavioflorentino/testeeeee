import Foundation
import Core
import Socket

struct DMSocketConfiguration: SocketConfiguration {
    typealias Dependencies = HasUser & HasConsumer
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    let url: URL? = URL(string: Environment.url ?? String())
    
    let port: UInt32 = Environment.port ?? UInt32()

    let tls: Bool = Environment.enableTLS

    let additionalHeaders: [String: String] = {
        [
            "dm-auth": "testToken", // FIXME: Temporary value for development purpose
            "host": Environment.host ?? String()
        ]
    }()
    
    let shouldReconnect: Bool = Environment.autoReconnect
    
    var clientId: Int {
        dependencies.consumer?.consumerId ?? Int()
    }
    
    var connectionId: String {
        guard let clientId = dependencies.consumer?.consumerId else {
            return String()
        }
        return "\(clientId)UUID\(UUID().uuidString)"
    }
    
    var userName: String = {
        "testeUserName" // FIXME: Temporary value for development purpose
    }()
    
    var password: String {
        dependencies.user?.token ?? String()
    }
}
