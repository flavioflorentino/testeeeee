import Foundation

enum DMBrokerMessageType: String, CaseIterable, Codable {
    case text = "TEXT"
    case chats = "CHATS"
}

enum DMBrokerMessageContent: Codable {
    case empty

    init(from decoder: Decoder) throws {
        self = .empty
    }

    func encode(to encoder: Encoder) throws {}
}

struct DMBrokerMessage<T: Codable>: Codable {
    let type: DMBrokerMessageType
    let from: String
    let to: String
    let content: T?

    enum CodingKeys: String, CodingKey {
        case type = "message_type"
        case from, to, content
    }
}
