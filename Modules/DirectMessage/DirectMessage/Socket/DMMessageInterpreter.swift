import Foundation
import Socket

protocol DMMessageInterpreting {
    func interpretAsMessage(_ data: Data)
    func interpretAsChat(_ data: Data)
}

final class DMMessageInterpreter {
    // Attributes
    typealias Dependencies = HasDatabaseStore & HasJsonCoding
    private let dependencies: Dependencies
    
    // Initialization
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
}

extension DMMessageInterpreter: SocketInterpreter {
    func interpret(data: Data) {
        guard let brokerMessage: DMBrokerMessage<DMBrokerMessageContent> = decode(data) else {
            debugPrint("Unmapped message") // TODO: Should log an error
            return
        }
        switch brokerMessage.type {
        case .text:
            interpretAsMessage(data)
        case .chats:
            interpretAsChat(data)
        }
    }
}

extension DMMessageInterpreter: DMMessageInterpreting {
    func interpretAsMessage(_ data: Data) {
        guard let message: Message = decode(data) else {
            debugPrint("Unmapped message") // TODO: Should log an error
            return
        }
        dependencies.database.add(message: message)
    }

    func interpretAsChat(_ data: Data) {
        guard let brokerMessage: DMBrokerMessage<[Chat]> = decode(data),
              let chats = brokerMessage.content,
              !chats.isEmpty else {
            debugPrint("Unmapped message") // TODO: Should log an error
            return
        }
        dependencies.database.add(chats: chats, replacing: false)
    }
}

// MARK: - Private
private extension DMMessageInterpreter {
    func decode<T: Decodable>(_ data: Data) -> T? {
        do {
            return try dependencies.json.decoder.decode(T.self, from: data)
        } catch {
            debugPrint(error.localizedDescription)
            return nil
        }
    }
}
