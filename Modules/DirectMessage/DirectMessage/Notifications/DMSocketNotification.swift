import Foundation

protocol StoreNotificating {
    func createNewNotification(fromSavedMessage message: Message) -> Notification
    func createNewNotification(from chat: Chat) -> Notification
    func didSaveNew(message: Message)
    func didUpdate(chat: Chat)
}

public final class DMStoreNotification: StoreNotificating {
    public enum Name {
        static let didSaveNewMessage = "DMStoreNotificationDidSaveNewMessage"
        static let didUpdateChat = "DMStoreNotificationDidUpdateChat"
    }
    
    public enum UserInfoKey {
        static let message = "message"
        static let chat = "chat"
    }
    
    let notificationCenter: NotificationCenter
    
    init(notificationCenter: NotificationCenter = .default) {
        self.notificationCenter = notificationCenter
    }
}

// MARK: - StoreNotificating
extension DMStoreNotification {
    func createNewNotification(fromSavedMessage message: Message) -> Notification {
        let userInfo = [DMStoreNotification.UserInfoKey.message: message]
        return Notification(name: .init(Name.didSaveNewMessage),
                            object: self,
                            userInfo: userInfo)
    }

    func createNewNotification(from chat: Chat) -> Notification {
        let userInfo = [UserInfoKey.chat: chat]
        return Notification(name: .init(Name.didUpdateChat),
                            object: self,
                            userInfo: userInfo)
    }
    
    func didSaveNew(message: Message) {
        let notification = createNewNotification(fromSavedMessage: message)
        notificationCenter.post(notification)
    }

    func didUpdate(chat: Chat) {
        let notification = createNewNotification(from: chat)
        notificationCenter.post(notification)
    }
}
