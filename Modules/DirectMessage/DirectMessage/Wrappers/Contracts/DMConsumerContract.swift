public protocol DMConsumerContract {
    var consumerId: Int { get }
    var name: String { get }
    var username: String? { get }
    var avatarUrl: String? { get }
}
