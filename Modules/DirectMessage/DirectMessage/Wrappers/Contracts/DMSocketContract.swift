import Foundation
import Socket

public protocol DMSocketContract {
    func publish(content: Data, onTopic topic: String, completion: @escaping Socket.PublishCompletion)
}

public extension DMSocketContract {
    func publish(content: Data, onTopic topic: String) {
        publish(content: content, onTopic: topic, completion: { _ in })
    }
}
