public protocol DMUserManagerContract {
    var token: String { get }
}
