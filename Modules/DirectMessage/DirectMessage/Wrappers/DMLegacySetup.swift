import Core

public enum DMLegacySetup {
    static var consumer: DMConsumerContract?
    static var user: DMUserManagerContract?
    static var socket: DMSocketContract?
    
    public static func inject(instance: Any) {
        switch instance {
        case let instance as DMConsumerContract:
            consumer = instance
        case let instance as DMUserManagerContract:
            user = instance
        case let instance as DMSocketContract:
            socket = instance
        default:
            assertionFailure("🚨\nAttempted to inject unexpected depedency: \(instance)\n")
        }
    }
}
