import Core
import Foundation

typealias Dependencies = HasNoDependency
    & HasConsumer
    & HasUser
    & HasSocket
    & HasDatabaseStore
    & HasJsonCoding

final class DependencyContainer: Dependencies {
    lazy var consumer: DMConsumerContract? = DMLegacySetup.consumer
    lazy var user: DMUserManagerContract? = DMLegacySetup.user
    lazy var socket: DMSocketContract? = DMLegacySetup.socket
    lazy var database: DMStoring = DMMemoryStore.shared
    lazy var json: DMJSONCoding = DMJSONCoder()
}
