import Core
import Socket

protocol HasNoDependency { }

protocol HasConsumer {
    var consumer: DMConsumerContract? { get }
}

protocol HasUser {
    var user: DMUserManagerContract? { get }
}

protocol HasSocket {
    var socket: DMSocketContract? { get }
}

protocol HasDatabaseStore {
    var database: DMStoring { get }
}

protocol HasJsonCoding {
    var json: DMJSONCoding { get }
}
