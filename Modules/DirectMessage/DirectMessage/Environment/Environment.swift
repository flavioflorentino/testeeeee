import Core
import Foundation

extension Environment {
    static var bundleID: String {
        "com.picpay.DirectMessage"
    }
    
    static var bundle: Bundle {
        Bundle(identifier: bundleID) ?? Bundle.main
    }
    
    static var url: String? {
        Environment.getValue("MQTT_URL", bundle: bundle)
    }
    
    static var host: String? {
        Environment.getValue("MQTT_HOST", bundle: bundle)
    }
    
    static var port: UInt32? {
        Environment.getValue("MQTT_PORT", bundle: bundle)
    }
    
    static var autoReconnect: Bool {
        Environment.getValue("MQTT_AUTO_RECONNECT", bundle: bundle) ?? false
    }
    
    static var enableTLS: Bool {
        Environment.getValue("MQTT_ENABLE_TLS", bundle: bundle) ?? false
    }
    
    public class func getValue<T>(_ name: String, bundle: Bundle = Bundle.main) -> T? {
        guard let key: String = get(name, bundle: bundle) else {
            return nil
        }
        
        switch T.self {
        case is String.Type:
            return key as? T
        case is UInt32.Type:
            return UInt32(key) as? T
        case is Bool.Type:
            let value = key == "YES" ? true : false
            return value as? T
        default:
            return nil
        }
    }
}
