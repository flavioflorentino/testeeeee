import PIX

class UserAuthManagerMock: UserAuthManagerContract {
    func getUserInfo(completion: @escaping (KeyManagerBizUserInfo?) -> Void) {
        completion(.init(name: "", cnpj: "", mail: "", phone: "", userId: ""))
    }
}
