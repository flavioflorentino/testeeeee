import Core

final class AuthenticationContractMock: AuthenticationContract {
    var isAuthenticated: Bool = true
    
    private(set) var authenticateCallsCount = 0
    var authenticationResult: Result<String, AuthenticationStatus> = .failure(.cancelled)
    
    func authenticate(_ completion: @escaping AuthenticationResult) {
        authenticateCallsCount += 1
        completion(authenticationResult)
    }

    private(set) var disableBiometricAuthenticationCallCount = 0
    func disableBiometricAuthentication() {
        disableBiometricAuthenticationCallCount += 1
    }
}
