import CustomerSupport
import Foundation
import XCTest

final class ZendeskMock: ThirdPartySupportSDKContract {
    private(set) var didCallActivate = 0
    private(set) var didCallRegisterJWT = 0
    private(set) var didCallRegisterAnonymous = 0
    private(set) var didCallRegister = 0
    private(set) var didCallSetDebugMode = 0
    private(set) var didCallSetLocalizableFile = 0
    private(set) var didCallRequestTicket = 0
    private(set) var didCallIsAnyTicketOpen = 0
    private(set) var didCallSearchArticles = 0
    private(set) var didCallArticlesLabel = 0
    private(set) var didCallArticleByQuery = 0
    private(set) var didCallBuildFAQController = 0
    private(set) var didCallBuildTicketListController = 0

    public var requestTicketCompletionResult: Result<Void, Error>?
    private(set) var isAnyTicketSubjects: [String]?
    private(set) var articlesQuery: String?
    private(set) var buildTicketId: String?
    private(set) var buildTicketOption: FAQOptions?
    var isAnyTicketOpenCompletionResult: String?
    var searchCompletionResult: Result<ResultArticlesResearch, Error>?
    var articlesCompletionResult: Result<ResultArticlesResearch, Error>?
    var articlesByQueryResult: FAQOptions = .home

    public func activate(appId: String, clientId: String) {
        didCallActivate += 1
    }

    public func registerJWT(by userId: String) {
        didCallRegisterJWT += 1
    }

    public func registerAnonymous() {
        didCallRegisterAnonymous += 1
    }

    public func register(notificationToken: String) {
        didCallRegister += 1
    }

    public func setDebugMode(enabled: Bool) {
        didCallSetDebugMode += 1
    }

    public func setLocalizableFile(fileName: String) {
        didCallSetLocalizableFile += 1
    }

    public func requestTicket(
        title: String,
        message: String,
        tags: [String],
        extraCustomFields: [TicketCustomField],
        completion: @escaping (Result<Void, Error>) -> Void
    ) {
        didCallRequestTicket += 1
        guard let requestTicketCompletionResult = requestTicketCompletionResult else {
            XCTFail("requestTicketCompletionResult must be set")
            return
        }

        completion(requestTicketCompletionResult)
    }

    public func isAnyTicketOpen(_ subjects: [String], completion: @escaping (String?) -> Void) {
        didCallIsAnyTicketOpen += 1
        isAnyTicketSubjects = subjects

        completion(isAnyTicketOpenCompletionResult)
    }

    public func search(_ searchArticle: ArticlesResearch, completion: @escaping (Result<ResultArticlesResearch, Error>) -> Void) {
        didCallSearchArticles += 1
        guard let searchCompletionResult = searchCompletionResult else {
            XCTFail("requestTicketCompletionResult must be set")
            return
        }

        completion(searchCompletionResult)
    }

    public func articles(labels: [String], completion: @escaping (Result<ResultArticlesResearch, Error>) -> Void) {
        didCallArticlesLabel += 1
        guard let articlesCompletionResult = articlesCompletionResult else {
            XCTFail("articles must be set")
            return
        }

        completion(articlesCompletionResult)
    }

    public func articleBy(query: String) -> FAQOptions {
        articlesQuery = query
        didCallArticleByQuery += 1
        return articlesByQueryResult
    }


    public func buildFAQController(option: FAQOptions = .home) -> UIViewController {
        didCallBuildFAQController += 1
        buildTicketOption = option
        return UIViewController()
    }

    public func buildTicketListController(ticketId: String? = nil) -> UIViewController {
        didCallBuildTicketListController += 1
        buildTicketId = ticketId
        return UIViewController()
    }
}

