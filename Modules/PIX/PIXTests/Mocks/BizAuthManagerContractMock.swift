import PIX

final class BizAuthManagerContractMock: BizAuthManagerContract {
    private(set) var performActionWithAuthorizationCallsCount = 0
    var authorizationResult: PerformActionResult = .success("")
    
    func performActionWithAuthorization(_ message: String?, _ action: @escaping ((PerformActionResult) -> Void)) {
        performActionWithAuthorizationCallsCount += 1
        action(authorizationResult)
    }
    
    private(set) var getAuthorizationTokenCallsCount = 0
    var token = ""
    
    func getAuthorizationToken() -> String {
        getAuthorizationTokenCallsCount += 1
    return token
    }
}
