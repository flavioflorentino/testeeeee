import XCTest
import Foundation
import Core
@testable import PIX

final class PixBizKeyServiceMock: PixBizKeyServicing {
    private(set) var createNewPixKeyCallsCount = 0
    private(set) var keyReceived: CreateKey?
    var createNewPixKeyResult: Result<PixKeyData, ApiError>?
    var deleteResult: Result<NoContent, ApiError>?
    var claimResult: Result<NoContent, ApiError>?
    var confirmClaimResult: Result<NoContent, ApiError>?
    var completeClaimResult: Result<NoContent, ApiError>?
    var validateTFAResult: Result<TFAValidation, ApiError>?
    var cancelClaimResult: Result<NoContent, ApiError>?
    
    private(set) var listPixKeysCallsCount = 0
    private(set) var didCallConfirmClaim = 0
    private(set) var deletePixKeyCallsCount = 0
    private(set) var claimPixKeyCallCount = 0
    private(set) var didCallCompleteClaim = 0
    private(set) var didCallValidateTFA = 0
    private(set) var getKeysInfosCount = 0
    
    private(set) var userIdReceived: String?
    private(set) var pixUserTypeReceived: PixKeysUserType?
    var listPixKeysResult: Result<PixKeyDataList, ApiError>?

    func createNewPixKey(key: CreateKey, pin: String, completion: @escaping (Result<PixKeyData, ApiError>) -> Void) {
        createNewPixKeyCallsCount += 1
        keyReceived = key
        guard let createResult = createNewPixKeyResult else {
            XCTFail("Result not defined")
            return
        }
        completion(createResult)
    }
    
    func getKeysInfos(key: String, completion: @escaping (Result<PixKeyData, ApiError>) -> Void) {
        getKeysInfosCount += 1
        guard let createResult = createNewPixKeyResult else {
            XCTFail("Result not defined")
            return
        }
        completion(createResult)
    }
    
    func confirmClaim(userId: String, keyId: String, pin: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        didCallConfirmClaim += 1
        completion(confirmClaimResult ?? Result<NoContent, ApiError>.success(NoContent()))
    }

    func listPixKeys(with userId: String,
                     and type: PixKeysUserType,
                     completion: @escaping (Result<PixKeyDataList, ApiError>) -> Void) {
        listPixKeysCallsCount += 1
        userIdReceived = userId
        pixUserTypeReceived = type
        guard let createResult = listPixKeysResult else {
            XCTFail("Result not defined")
            return
        }
        completion(createResult)
    }
    
    func deleteKey(userId: String,
                   keyId: String,
                   userType: PixKeysUserType,
                   pin: String,
                   completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        deletePixKeyCallsCount += 1
        completion(deleteResult ?? Result<NoContent, ApiError>.success(NoContent()))
    }
    
    func claim(claimKey: ClaimKey, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        claimPixKeyCallCount += 1
        completion(claimResult ?? Result<NoContent, ApiError>.success(NoContent()))
    }
    
    func validateTFA(hash: String, completion: @escaping (Result<TFAValidation, ApiError>) -> Void) {
        didCallValidateTFA += 1
        guard let tfaResult = validateTFAResult else {
            XCTFail("Result not defined")
            return
        }
        completion(tfaResult)
    }
    
    func completeClaim(uuid: String, hash: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        didCallCompleteClaim += 1
        completion(completeClaimResult ?? Result<NoContent, ApiError>.success(NoContent()))
    }

    func cancelClaim(withKeyId keyId: String, hash: String?, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        completion(cancelClaimResult ?? Result<NoContent, ApiError>.success(NoContent()))
    }
}
