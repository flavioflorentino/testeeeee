import AnalyticsModule
import Core
import CustomerSupport
import FeatureFlag
@testable import PIX

final class DependencyContainerMock: PIXDependencies {
    lazy var mainQueue: DispatchQueue = resolve()
    lazy var analytics: AnalyticsProtocol = resolve()
    lazy var kvStore: KVStoreContract = resolve()
    lazy var featureManager: FeatureManagerContract = resolve()
    lazy var authContract: AuthenticationContract? = AuthenticationContractMock()
    lazy var businessAuthContract: BizAuthManagerContract? = BizAuthManagerContractMock()
    lazy var userAuthManagerContract: UserAuthManagerContract? = UserAuthManagerMock()
    lazy var costumeSupportContract: CostumeSupportContract? = CostumerSupportMock()
    lazy var zendeskContainer: ThirdPartySupportSDKContract? = resolve()
    
    private let dependencies: [Any]
    
    init(_ dependencies: Any...) {
        self.dependencies = dependencies
    }
    
    func resolve<T>() -> T {
        let resolved = dependencies.compactMap { $0 as? T }
                
        switch resolved.first {
        case .none:
            fatalError("DependencyContainerMock could not resolve dependency: \(T.self)\n")
        case .some where resolved.count > 1:
            fatalError("DependencyContainerMock resolved mutiple dependencies for: \(T.self)\n")
        case .some(let mock):
            return mock
        }
    }
    
    func resolve() -> DispatchQueue {
        dependencies.compactMap { $0 as? DispatchQueue }.first ?? DispatchQueue(label: "DependencyContainerMock")
    }
}
