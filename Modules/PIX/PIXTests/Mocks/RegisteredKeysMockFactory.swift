import Foundation
@testable import PIX

enum RegisteredKeysMockFactory {
    public static func make() -> [RegisteredKey] {
        [
            RegisteredKey(id: "1",
                          statusSlug: .processed,
                          status: "",
                          name: "Celular",
                          keyType: .phone,
                          keyValue: "(61) 98888-8888"),
            RegisteredKey(id: "2",
                          statusSlug: .processed,
                          status: "",
                          name: "CPF/CNPF",
                          keyType: .cpf,
                          keyValue: "855.555.555-33"),
            RegisteredKey(id: "3",
                          statusSlug: .processed,
                          status: "",
                          name: "Email",
                          keyType: .email,
                          keyValue: "nadir@email.com.br"),
            RegisteredKey(id: "4",
                          statusSlug: .processed,
                          status: "",
                          name: "Chave Aleatória",
                          keyType: .random,
                          keyValue: "189hsahsnd8902"),
            RegisteredKey(id: "5",
                          statusSlug: .processed,
                          status: "",
                          name: "Chave Aleatória",
                          keyType: .random,
                          keyValue: "189hsahsnd8905")
        ]
    }
}
