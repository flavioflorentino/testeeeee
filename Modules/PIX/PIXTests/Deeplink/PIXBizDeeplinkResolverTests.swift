import XCTest
@testable import PIX

final class PIXBizDeeplinkResolverTests: XCTestCase {
    private let dependencyContainer = DependencyContainerMock()
    private lazy var sut = PIXBizDeeplinkResolver(dependencies: dependencyContainer, navigation: UINavigationController())
    
    func testCanHandleUrl_WhenIsportabilityCompletedAndUserIsAuthenticated_ShouldReturnHandleable() throws {
        let deeplink = "picpaybiz://picpaybiz/pix/feedback/portabilityCompleted?key=minhaChaveDeTeste"
        let url = try XCTUnwrap(URL(string: deeplink))
        
        let canHandleDeeplink = sut.canHandle(url: url, isAuthenticated: true)
        
        XCTAssertEqual(canHandleDeeplink, .handleable)
    }

    func testCanHandleUrl_WhenIsportabilityRefuseddAndUserIsAuthenticated_ShouldReturnHandleable() throws {
        let deeplink = "picpaybiz://picpaybiz/pix/feedback/portabilityRefused?key=minhaChaveDeTeste"
        let url = try XCTUnwrap(URL(string: deeplink))
        
        let canHandleDeeplink = sut.canHandle(url: url, isAuthenticated: true)
        
        XCTAssertEqual(canHandleDeeplink, .handleable)
    }
    
    func testCanHandleUrl_WhenIsclaimCompletedAndUserIsAuthenticated_ShouldReturnHandleable() throws {
        let deeplink = "picpaybiz://picpaybiz/pix/feedback/portabilityCompleted?key=minhaChaveDeTeste"
        let url = try XCTUnwrap(URL(string: deeplink))
        
        let canHandleDeeplink = sut.canHandle(url: url, isAuthenticated: true)
        
        XCTAssertEqual(canHandleDeeplink, .handleable)
    }
    
    func testCanHandleUrl_WhenIsclaimRefusedAndUserIsAuthenticated_ShouldReturnHandleable() throws {
        let deeplink = "picpaybiz://picpaybiz/pix/feedback/portabilityCompleted?key=minhaChaveDeTeste"
        let url = try XCTUnwrap(URL(string: deeplink))
        
        let canHandleDeeplink = sut.canHandle(url: url, isAuthenticated: true)
        
        XCTAssertEqual(canHandleDeeplink, .handleable)
    }
    
    func testCanHandleUrl_WhenIsportabilityCompletedAndUserIsNotAuthenticated_ShouldReturnonlyWithAuth() throws {
        let deeplink = "picpaybiz://picpaybiz/pix/feedback/portabilityCompleted?key=minhaChaveDeTeste"
        let url = try XCTUnwrap(URL(string: deeplink))
        
        let canHandleDeeplink = sut.canHandle(url: url, isAuthenticated: false)
        
        XCTAssertEqual(canHandleDeeplink, .onlyWithAuth)
    }
    
    func testCanHandleUrl_WhenIsInvalidUrlAndUserIsAuthenticated_ShouldReturnNotHandleable() throws {
        let deeplink = "picpay://picpay/portabilityCompleted?key=minhaChaveDeTeste"
        let url = try XCTUnwrap(URL(string: deeplink))
        
        let result = sut.canHandle(url: url, isAuthenticated: true)
        
        XCTAssertEqual(result, .notHandleable)
    }
    
    // MARK: - pix/dailyLimit
    func testCanHandleUrl_WhenIsValidDailyLimitUrlAndUserIsAuthenticated_ShouldReturnHandleable() throws {
        let deeplink = "picpaybiz://picpaybiz/pix/dailyLimit"
        let url = try XCTUnwrap(URL(string: deeplink))
        
        let result = sut.canHandle(url: url, isAuthenticated: true)
        
        XCTAssertEqual(result, .handleable)
    }
    
    func testCanHandleUrl_WhenIsValidDailyLimitUrlAndUserIsNotAuthenticated_ShouldReturnOnlyWithAuth() throws {
        let deeplink = "picpaybiz://picpaybiz/pix/dailyLimit"
        let url = try XCTUnwrap(URL(string: deeplink))
        
        let result = sut.canHandle(url: url, isAuthenticated: false)
        
        XCTAssertEqual(result, .onlyWithAuth)
    }
}
