import Core
import FeatureFlag
@testable import PIX
import XCTest

final class PIXPaymentOpenerMock: PIXPaymentOpening {
    func openPaymentWithDetails(_ details: PIXPaymentDetails,
                                onViewController controller: UIViewController,
                                origin: PixPaymentOrigin,
                                flowType: PixPaymentFlowType) {}
}

enum PIXBankSelector: BankSelector {
    static func make(title: String, searchPlaceholder: String, onBankSelectionCompletion: @escaping (String, String) -> Void) -> UIViewController {
        let controller = UIViewController()
        controller.title = title
        return controller
    }
}

final class PIXQrCodeScanningMock: UIViewController, QrCodeScanning {
    let backButtonSide: BackButtonSide
    let hasMyCodeBottomSheet: Bool
    let scannerType: ScannerType
    
    init(backButtonSide: BackButtonSide, hasMyCodeBottomSheet: Bool, scannerType: ScannerType) {
        self.backButtonSide = backButtonSide
        self.hasMyCodeBottomSheet = hasMyCodeBottomSheet
        self.scannerType = scannerType
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

final class TransactionReceiptSpy: TransactionReceipting {
    private(set) static var id: String?
    private(set) static var type: String?
    
    static func createReceipt(id: String, type: String) -> UIViewController {
        self.id = id
        self.type = type
        return UIViewController()
    }
}

final class PIXDeeplinkResolverTests: XCTestCase {
    private let featureManager = FeatureManagerMock()
    private lazy var sut = PIXDeeplinkResolver(
        dependencies: DependencyContainerMock(featureManager),
        paymentOpener: PIXPaymentOpenerMock(),
        bankSelectorType: PIXBankSelector.self,
        qrCodeScannerType: PIXQrCodeScanningMock.self,
        receiptType: TransactionReceiptSpy.self
    )
    
    // MARK: - invalidUrl
    func testCanHandleUrl_WhenIsInvalidPixUrl_ShouldReturnNotHandleable() throws {
        let deeplink = "picpay://picpay"
        let url = try XCTUnwrap(URL(string: deeplink))
        
        let result = sut.canHandle(url: url, isAuthenticated: true)
        
        XCTAssertEqual(result, .notHandleable)
    }
    
    // MARK: - pix/hub
    func testCanHandleUrl_WhenIsValidHubUrlAndUserIsAuthenticated_ShouldReturnHandleable() throws {
        let deeplink = "picpay://picpay/pix/hub"
        let url = try XCTUnwrap(URL(string: deeplink))
        
        let result = sut.canHandle(url: url, isAuthenticated: true)
        
        XCTAssertEqual(result, .handleable)
    }
    
    func testCanHandleUrl_WhenIsValidHubUrlAndUserIsNotAuthenticated_ShouldReturnOnlyWithAuth() throws {
        let deeplink = "picpay://picpay/pix/hub"
        let url = try XCTUnwrap(URL(string: deeplink))
        
        let result = sut.canHandle(url: url, isAuthenticated: false)
        
        XCTAssertEqual(result, .onlyWithAuth)
    }
    
    // MARK: - pix/keySelector
    func testCanHandleUrl_WhenIsValidKeySelectorUrlAndUserIsAuthenticated_ShouldReturnHandleable() throws {
        let deeplink = "picpay://picpay/pix/keySelector"
        let url = try XCTUnwrap(URL(string: deeplink))
        
        let result = sut.canHandle(url: url, isAuthenticated: true)
        
        XCTAssertEqual(result, .handleable)
    }
    
    func testCanHandleUrl_WhenIsValidKeySelectorUrlAndUserIsNotAuthenticated_ShouldReturnOnlyWithAuth() throws {
        let deeplink = "picpay://picpay/pix/keySelector"
        let url = try XCTUnwrap(URL(string: deeplink))
        
        let result = sut.canHandle(url: url, isAuthenticated: false)
        
        XCTAssertEqual(result, .onlyWithAuth)
    }
    
    // MARK: - pix/keySelector
    func testCanHandleUrl_WhenIsValidCopyPasteUrlAndUserIsAuthenticated_ShouldReturnHandleable() throws {
        let deeplink = "picpay://picpay/pix/copyPaste"
        let url = try XCTUnwrap(URL(string: deeplink))
        
        let result = sut.canHandle(url: url, isAuthenticated: true)
        
        XCTAssertEqual(result, .handleable)
    }
    
    func testCanHandleUrl_WhenIsValidCopyPasteUrlAndUserIsNotAuthenticated_ShouldReturnOnlyWithAuth() throws {
        let deeplink = "picpay://picpay/pix/copyPaste"
        let url = try XCTUnwrap(URL(string: deeplink))
        
        let result = sut.canHandle(url: url, isAuthenticated: false)
        
        XCTAssertEqual(result, .onlyWithAuth)
    }
    
    // MARK: - pix/qrCode
    func testCanHandleUrl_WhenIsValidQrCodeUrlAndUserIsAuthenticated_ShouldReturnHandleable() throws {
        let deeplink = "picpay://picpay/pix/qrCode"
        let url = try XCTUnwrap(URL(string: deeplink))
        
        let result = sut.canHandle(url: url, isAuthenticated: true)
        
        XCTAssertEqual(result, .handleable)
    }
    
    func testCanHandleUrl_WhenIsValidQrCodeUrlAndUserIsNotAuthenticated_ShouldReturnOnlyWithAuth() throws {
        let deeplink = "picpay://picpay/pix/qrCode"
        let url = try XCTUnwrap(URL(string: deeplink))
        
        let result = sut.canHandle(url: url, isAuthenticated: false)
        
        XCTAssertEqual(result, .onlyWithAuth)
    }
    
    // MARK: - pix/qrCode
    func testCanHandleUrl_WhenIsValidManualInsertionUrlAndUserIsAuthenticated_ShouldReturnHandleable() throws {
        let deeplink = "picpay://picpay/pix/manualInsertion"
        let url = try XCTUnwrap(URL(string: deeplink))
        
        let result = sut.canHandle(url: url, isAuthenticated: true)
        
        XCTAssertEqual(result, .handleable)
    }
    
    func testCanHandleUrl_WhenIsValidManualInsertionUrlAndUserIsNotAuthenticated_ShouldReturnOnlyWithAuth() throws {
        let deeplink = "picpay://picpay/pix/manualInsertion"
        let url = try XCTUnwrap(URL(string: deeplink))
        
        let result = sut.canHandle(url: url, isAuthenticated: false)
        
        XCTAssertEqual(result, .onlyWithAuth)
    }
    
    // MARK: - pix/qrCode
    func testCanHandleUrl_WhenIsValidReceivementUrlAndUserIsAuthenticated_ShouldReturnHandleable() throws {
        let deeplink = "picpay://picpay/pix/receivement"
        let url = try XCTUnwrap(URL(string: deeplink))
        
        let result = sut.canHandle(url: url, isAuthenticated: true)
        
        XCTAssertEqual(result, .handleable)
    }
    
    func testCanHandleUrl_WhenIsValidReceivementUrlAndUserIsNotAuthenticated_ShouldReturnOnlyWithAuth() throws {
        let deeplink = "picpay://picpay/pix/receivement"
        let url = try XCTUnwrap(URL(string: deeplink))
        
        let result = sut.canHandle(url: url, isAuthenticated: false)
        
        XCTAssertEqual(result, .onlyWithAuth)
    }
    
    // MARK: - pix/keyManagement
    func testCanHandleUrl_WhenIsValidKeyManagerUrlAndUserIsAuthenticated_ShouldReturnHandleable() throws {
        let deeplink = "picpay://picpay/pix/receipt?id=12345&type=pix"
        let url = try XCTUnwrap(URL(string: deeplink))
        
        let result = sut.canHandle(url: url, isAuthenticated: true)
        
        XCTAssertEqual(result, .handleable)
    }
    
    func testCanHandleUrl_WhenIsValidReceiptUrlAndUserIsNotAuthenticated_ShouldReturnOnlyWithAuth() throws {
        let deeplink = "picpay://picpay/pix/receipt?id=12345&type=pix"
        let url = try XCTUnwrap(URL(string: deeplink))
        
        let result = sut.canHandle(url: url, isAuthenticated: false)
        
        XCTAssertEqual(result, .onlyWithAuth)
    }
    
    // MARK: - pix/keyManagement
    func testCanHandleUrl_WhenIsValidReceiptUrlAndUserIsAuthenticated_ShouldReturnHandleable() throws {
        let deeplink = "picpay://picpay/pix/keyManagement"
        let url = try XCTUnwrap(URL(string: deeplink))
        
        let result = sut.canHandle(url: url, isAuthenticated: true)
        
        XCTAssertEqual(result, .handleable)
    }
    
    func testCanHandleUrl_WhenIsValidKeyManagerUrlAndUserIsNotAuthenticated_ShouldReturnOnlyWithAuth() throws {
        let deeplink = "picpay://picpay/pix/keyManagement"
        let url = try XCTUnwrap(URL(string: deeplink))
        
        let result = sut.canHandle(url: url, isAuthenticated: false)
        
        XCTAssertEqual(result, .onlyWithAuth)
    }
    
    // MARK: - pix/dailyLimit
    func testCanHandleUrl_WhenIsValidDailyLimitUrlAndUserIsAuthenticated_ShouldReturnHandleable() throws {
        let deeplink = "picpay://picpay/pix/dailyLimit"
        let url = try XCTUnwrap(URL(string: deeplink))
        
        let result = sut.canHandle(url: url, isAuthenticated: true)
        
        XCTAssertEqual(result, .handleable)
    }
    
    func testCanHandleUrl_WhenIsValidDailyLimitUrlAndUserIsNotAuthenticated_ShouldReturnOnlyWithAuth() throws {
        let deeplink = "picpay://picpay/pix/dailyLimit"
        let url = try XCTUnwrap(URL(string: deeplink))
        
        let result = sut.canHandle(url: url, isAuthenticated: false)
        
        XCTAssertEqual(result, .onlyWithAuth)
    }
    
    // MARK: - identityanalysis
    func testCanHandleUrl_WhenIsNotPixUrl_ShouldReturnNotHandleable() throws {
        let deeplink = "picpay://picpay/identityanalysis"
        let url = try XCTUnwrap(URL(string: deeplink))
        
        let result = sut.canHandle(url: url, isAuthenticated: true)
        
        XCTAssertEqual(result, .notHandleable)
    }
    
    // MARK: - feedback/keyRecordInProgress
    func testCanHandleUrl_WhenIsValidFeedbackUrlAndUserIsAuthenticated_ShouldReturnHandleable() throws {
        let deeplink = "picpay://picpay/pix/feedback/keyRecordInProgress"
        let url = try XCTUnwrap(URL(string: deeplink))
        
        let result = sut.canHandle(url: url, isAuthenticated: true)
        
        XCTAssertEqual(result, .handleable)
    }
    
    func testCanHandleUrl_WhenIsValidFeedbackUrlAndIsNotAuthenticated_ShouldReturnOnlyWithAuth() throws {
        let deeplink = "picpay://picpay/pix/feedback/keyRecordInProgress"
        let url = try XCTUnwrap(URL(string: deeplink))
        
        let result = sut.canHandle(url: url, isAuthenticated: false)
        
        XCTAssertEqual(result, .onlyWithAuth)
    }
    
    // MARK: - feedback/keyRecordRegisteredAndIdValidation
    func testCanHandleUrl_WhenIsKeyRecordRegisteredAndIdValidationAndIsAuthenticated_ShouldReturnHandleable() throws {
        let deeplink = "picpay://picpay/pix/feedback/keyRecordRegisteredAndIdValidation"
        let url = try XCTUnwrap(URL(string: deeplink))
        
        let result = sut.canHandle(url: url, isAuthenticated: true)
        
        XCTAssertEqual(result, .handleable)
    }
}
