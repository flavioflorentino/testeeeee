import UIKit

final class SpyNavigationController: UINavigationController {
    private(set) var dismissCallCount = 0
    private(set) var poppedViewControllerCount = 0
    private(set) var pushViewControllerCallCount = 0
    private(set) var presentViewControllerCallCount = 0
    private(set) var setViewControllersCallsCount = 0
    
    private(set) var pushedViewController: UIViewController?
    private(set) var lastPopedViewController: UIViewController?
    private(set) var lastPresentedViewController: UIViewController?
    private(set) var lastSetViewControllers: [UIViewController] = []
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        dismissCallCount += 1
        super.dismiss(animated: false, completion: completion)
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        pushedViewController = viewController
        pushViewControllerCallCount += 1
        super.pushViewController(viewController, animated: false)
    }
    
    override func popViewController(animated: Bool) -> UIViewController? {
        let viewController = super.popViewController(animated: false)
        lastPopedViewController = viewController
        poppedViewControllerCount += 1
        return viewController
    }
    
    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        lastPresentedViewController = viewControllerToPresent
        presentViewControllerCallCount += 1
        super.present(viewControllerToPresent, animated: false, completion: completion)
    }
    
    override func setViewControllers(_ viewControllers: [UIViewController], animated: Bool) {
        setViewControllersCallsCount += 1
        lastSetViewControllers = viewControllers
        super.setViewControllers(viewControllers, animated: true)
    }
}
