import UIKit

class SpyViewController: UIViewController {
    var dismissCallCount = 0
    var presentCallCount = 0
    var viewControllerPresented: UIViewController?

    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        dismissCallCount += 1
        super.dismiss(animated: flag, completion: completion)
    }

    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        presentCallCount += 1
        viewControllerPresented = viewControllerToPresent
        super.present(viewControllerToPresent, animated: false, completion: completion)
    }
}
