import XCTest

final class Mocker {
    func data(_ fileName: String) -> Data? {
        guard
            let file = Bundle(for: type(of: self)).path(forResource: fileName, ofType: "json"),
            let data = try? Data(contentsOf: URL(fileURLWithPath: file))
            else {
                return nil
        }
        return data
    }
}
