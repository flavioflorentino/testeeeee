import XCTest
@testable import PIX

final class PixBizOptinFlowCoordinatorTests: XCTestCase {
    private let userInfo = KeyManagerBizUserInfo(name: "", cnpj: "24894589000187", mail: "", phone: "27971830410", userId: "1")
    private let navigationController = SpyNavigationController()
    private lazy var viewController: SpyViewController = {
        let controller = SpyViewController()
        self.navigationController.pushViewController(controller, animated: false)
        return controller
    }()
    private lazy var coordinator: PIXBizOptinFlowCoordinator = {
        let coordinator = PIXBizOptinFlowCoordinator(from: navigationController, userInfo: userInfo)
        coordinator.viewController = viewController
        return coordinator
    }()
    
    func testPerform_WhenActionOnboarding_ShouldShowOnboarding() throws {
        coordinator.perform(action: .onboarding)
        
        let welcomeViewController = try XCTUnwrap(navigationController.pushedViewController)
        XCTAssertEqual(navigationController.pushViewControllerCallCount, 2)
        XCTAssertTrue(welcomeViewController is KeyManagementWelcomeViewController)
    }
    
    func testPerform_WhenActionCreateRandomKey_ShouldShowCreateRandomKey() throws {
        coordinator.perform(action: .createRandomKey)
        XCTAssertEqual(navigationController.pushViewControllerCallCount, 2)
    }
    
    func testPerform_WhenActionKeyManager_ShouldShowKeyManager() throws {
        coordinator.perform(action: .keyManager(input: .none))
        
        let keyManagerViewController = try XCTUnwrap(navigationController.pushedViewController)
        XCTAssertEqual(navigationController.pushViewControllerCallCount, 2)
        XCTAssertTrue(keyManagerViewController is KeyManagerBizViewController)
    }
    
    func testPerform_WhenActionCreateKey_ShouldShowCreateKey() throws {
        coordinator.perform(action: .createKey(userInfo: userInfo, key: CreateKey(), hash: nil))
        
        let createKeyViewController = try XCTUnwrap(navigationController.pushedViewController)
        XCTAssertEqual(navigationController.pushViewControllerCallCount, 2)
        XCTAssertTrue(createKeyViewController is CreateBizKeyViewController)
    }
    
    func testPerform_WhenActionFeedback_ShouldShowFeedback() throws {
        coordinator.perform(action: .feedback(type: .unauthorized))
        
        let feedbackViewController = try XCTUnwrap(navigationController.pushedViewController)
        XCTAssertEqual(navigationController.pushViewControllerCallCount, 2)
        XCTAssertTrue(feedbackViewController is FeedbackViewController)
    }
    
    func testPerform_WhenActionKeyDescription_ShouldShowKeyDescription() throws {
        coordinator.perform(action: .keyDescription(userInfo: userInfo, createKey: CreateKey()))
        
        let keyDescriptionViewController = try XCTUnwrap(navigationController.pushedViewController)
        XCTAssertEqual(navigationController.pushViewControllerCallCount, 2)
        XCTAssertTrue(keyDescriptionViewController is KeyDescriptionViewController)
    }
}
