import Core
import XCTest
@testable import PIX

private final class PixPaymentOpeningMock: PIXPaymentOpening {
    func openPaymentWithDetails(
        _ details: PIXPaymentDetails,
        onViewController controller: UIViewController,
        origin: PixPaymentOrigin,
        flowType: PixPaymentFlowType) {}
}

private enum BankSelectorMock: BankSelector {
    static func make(title: String, searchPlaceholder: String, onBankSelectionCompletion: @escaping (String, String) -> Void) -> UIViewController {
        return UIViewController()
    }
    
    static func make(
        title: String,
        onBankSelectionCompletion: @escaping (String, String) -> Void
    ) -> UIViewController {
        return UIViewController()
    }
}

private final class QrCodeScanningMock: UIViewController, QrCodeScanning {
    init(backButtonSide: BackButtonSide, hasMyCodeBottomSheet: Bool, scannerType: ScannerType) {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

final class PIXConsumerFlowCoordinatorTests: XCTestCase {
    private let kvStoreMock = KVStoreMock()
    private let originNavController = SpyNavigationController()
    
    func testStart_WhenDestinationIsHub_ShouldPresentHubModally() throws {
        let sut = PIXConsumerFlowCoordinator(
            originViewController: originNavController,
            originFlow: .deeplink,
            destination: .hub(
                origin: "abcd",
                paymentOpening: PixPaymentOpeningMock(),
                bankSelectorType: BankSelectorMock.self,
                qrCodeScannerType: QrCodeScanningMock.self
            )
        )
        
        sut.start()
        
        XCTAssertEqual(originNavController.presentViewControllerCallCount, 1)
        let presentedNavController = try XCTUnwrap(originNavController.lastPresentedViewController as? UINavigationController)
        XCTAssert(presentedNavController.topViewController is HubViewController)
    }
    
    private func topPresentedViewController() throws -> UIViewController {
        try XCTUnwrap((originNavController.lastPresentedViewController as? UINavigationController)?.topViewController)
    }
    
    func testStart_WhenWelcomeIsVisualizedAndDestinationIsKeyManagement_ShouldPresentKeyManagementModally() throws {
        kvStoreMock.setBool(true, with: KVKey.isPixKeyManagementWelcomeVisualized)
        let sut = PIXConsumerFlowCoordinator(
            originViewController: originNavController,
            originFlow: .deeplink,
            destination: .keyManagement,
            dependencies: DependencyContainerMock(kvStoreMock)
        )
        
        sut.start()
        
        XCTAssertEqual(originNavController.presentViewControllerCallCount, 1)
        XCTAssert(try topPresentedViewController() is KeyManagerConsumerViewController)
    }

    func testStart_WhenWelcomeIsNotVisualizedAndDestinationIsKeyManagement_ShouldPresentWelcomeModally() throws {
        kvStoreMock.setBool(false, with: KVKey.isPixKeyManagementWelcomeVisualized)
        let sut = PIXConsumerFlowCoordinator(
            originViewController: originNavController,
            originFlow: .deeplink,
            destination: .keyManagement,
            dependencies: DependencyContainerMock(kvStoreMock)
        )
        
        sut.start()
        
        XCTAssertEqual(originNavController.presentViewControllerCallCount, 1)
        XCTAssert(try topPresentedViewController() is KeyManagementWelcomeViewController)
    }
    
    func testStart_WhenWelcomeIsVisualizedAndDestinationIsPreRegistration_ShouldPresentKeyManagementModally() throws {
        kvStoreMock.setBool(true, with: KVKey.isPixKeyManagementWelcomeVisualized)
        let sut = PIXConsumerFlowCoordinator(
            originViewController: originNavController,
            originFlow: .deeplink,
            destination: .preRegistration,
            dependencies: DependencyContainerMock(kvStoreMock)
        )
        
        sut.start()
        
        XCTAssertEqual(originNavController.presentViewControllerCallCount, 1)
        XCTAssert(try topPresentedViewController() is KeyManagerConsumerViewController)
    }

    func testStart_WhenWelcomeIsNotVisualizedAndDestinationIsPreRegistration_ShouldPresentWelcomeModally() throws {
        kvStoreMock.setBool(false, with: KVKey.isPixKeyManagementWelcomeVisualized)
        let sut = PIXConsumerFlowCoordinator(
            originViewController: originNavController,
            originFlow: .deeplink,
            destination: .preRegistration,
            dependencies: DependencyContainerMock(kvStoreMock)
        )
        
        sut.start()
        
        XCTAssertEqual(originNavController.presentViewControllerCallCount, 1)
        XCTAssert(try topPresentedViewController() is KeyManagementWelcomeViewController)
    }
    
    func testStart_WhenWelcomeIsVisualizedAndDestinationIsRemovalCompleted_ShouldPresentKeyManagementModally() throws {
        kvStoreMock.setBool(true, with: KVKey.isPixKeyManagementWelcomeVisualized)
        let sut = PIXConsumerFlowCoordinator(
            originViewController: originNavController,
            originFlow: .deeplink,
            destination: .removalCompleted,
            dependencies: DependencyContainerMock(kvStoreMock)
        )
        
        sut.start()
        
        XCTAssertEqual(originNavController.presentViewControllerCallCount, 1)
        XCTAssert(try topPresentedViewController() is KeyManagerConsumerViewController)
    }

    func testStart_WhenWelcomeIsNotVisualizedAndDestinationIsRemovalCompleted_ShouldPresentWelcomeModally() throws {
        kvStoreMock.setBool(false, with: KVKey.isPixKeyManagementWelcomeVisualized)
        let sut = PIXConsumerFlowCoordinator(
            originViewController: originNavController,
            originFlow: .deeplink,
            destination: .removalCompleted,
            dependencies: DependencyContainerMock(kvStoreMock)
        )
        
        sut.start()
        
        XCTAssertEqual(originNavController.presentViewControllerCallCount, 1)
        XCTAssert(try topPresentedViewController() is KeyManagementWelcomeViewController)
    }
    
    func testStart_WhenWelcomeIsVisualizedAndDestinationIsFeedback_ShouldPresentKeyManagementModally() throws {
        kvStoreMock.setBool(true, with: KVKey.isPixKeyManagementWelcomeVisualized)
        let sut = PIXConsumerFlowCoordinator(
            originViewController: originNavController,
            originFlow: .deeplink,
            destination: .feedback(type: .claimKeyAccepted(uuid: "")),
            dependencies: DependencyContainerMock(kvStoreMock)
        )
        
        sut.start()
        
        XCTAssertEqual(originNavController.presentViewControllerCallCount, 1)
        XCTAssert(try topPresentedViewController() is KeyManagerConsumerViewController)
    }

    func testStart_WhenWelcomeIsNotVisualizedAndDestinationIsFeedback_ShouldPresentWelcomeModally() throws {
        kvStoreMock.setBool(false, with: KVKey.isPixKeyManagementWelcomeVisualized)
        let sut = PIXConsumerFlowCoordinator(
            originViewController: originNavController,
            originFlow: .deeplink,
            destination: .feedback(type: .portabilityCompleted(uuid: "")),
            dependencies: DependencyContainerMock(kvStoreMock)
        )
        
        sut.start()
        
        XCTAssertEqual(originNavController.presentViewControllerCallCount, 1)
        XCTAssert(try topPresentedViewController() is KeyManagementWelcomeViewController)
    }
    
    func testStart_WhenUsingModalPresentation_ShouldPushWelcomeScene() throws {
        kvStoreMock.setBool(false, with: KVKey.isPixKeyManagementWelcomeVisualized)
        let sut = PIXConsumerFlowCoordinator(
            originViewController: originNavController,
            originFlow: .deeplink,
            destination: .keyManagement,
            dependencies: DependencyContainerMock(kvStoreMock)
        )
        
        sut.start(isModalPresentation: false)
        
        XCTAssertEqual(originNavController.pushViewControllerCallCount, 1)
        XCTAssert(originNavController.pushedViewController is KeyManagementWelcomeViewController)
    }
    
    func testStart_WhenUsingModalPresentationAndOriginViewControllerIsNotNavController_ShouldNotPushAnything() throws {
        kvStoreMock.setBool(false, with: KVKey.isPixKeyManagementWelcomeVisualized)
        let sut = PIXConsumerFlowCoordinator(
            originViewController: UIViewController(),
            originFlow: .deeplink,
            destination: .keyManagement,
            dependencies: DependencyContainerMock(kvStoreMock)
        )
        
        sut.start(isModalPresentation: false)
        
        XCTAssertEqual(originNavController.pushViewControllerCallCount, 0)
    }
}
