@testable import PIX
import XCTest

final class ReceiveLoadingCoordinatorTests: XCTestCase {
    private let viewController = SpyViewController()
    private lazy var navigationController = SpyNavigationController(rootViewController: viewController)
    
    private lazy var coordinator: ReceiveLoadingCoordinating = {
        let coordinator = ReceiveLoadingCoordinator()
        coordinator.viewController = viewController
        return coordinator
    }()

    func testPerform_WhenActionIsReceivePayment_ShouldCallPushViewController() throws {
        XCTAssertEqual(navigationController.pushViewControllerCallCount, 1)
        let userData = PixUserDataMockFactory.make()
        let keys = RegisteredKeysMockFactory.make()
        let selectedKey = try XCTUnwrap(keys.first)
        let qrCodeImage = try XCTUnwrap(QRCodeImageMockFactory.make())
        let qrCodeText = "Test"
        let output = ReceivePaymentsData(keys: keys, userData: userData, selectedKey: selectedKey, qrCodeImage: qrCodeImage, qrCodeText: qrCodeText)
        coordinator.perform(action: .receivePayment(output: output, origin: .pixHub))
        XCTAssertEqual(navigationController.pushViewControllerCallCount, 2)
        XCTAssert(navigationController.viewControllers.last is ReceivePaymentsViewController)
    }

    func testPerform_WhenActionIsRegisterKeys_ShouldCallPushViewController() {
        XCTAssertEqual(navigationController.pushViewControllerCallCount, 1)
        coordinator.perform(action: .registerKeys(origin: .pixHub))
        XCTAssertEqual(navigationController.pushViewControllerCallCount, 2)
        XCTAssert(navigationController.viewControllers.last is ReceiveWallViewController)
    }
}
