import Foundation
import Core
@testable import PIX

final class ReceiveServiceMock: ReceiveServicing {
    private(set) var listPixKeysCallCount = 0
    private(set) var generateQRCodeCallCount = 0

    private(set) var userId: String?
    var listPixKeysResult: Result<PixKeyDataList, ApiError> = .failure(.unknown(nil))
    var generateQRCodeResult: Result<ReceiveQRCodeResponse, ApiError> = .failure(.unknown(nil))


    func listPixKeys(with userId: String, completion: @escaping (Result<PixKeyDataList, ApiError>) -> Void) {
        listPixKeysCallCount += 1
        self.userId = userId
        completion(listPixKeysResult)
    }

    func generateQRCode(request: ReceiveQRCodeRequest, completion: @escaping (Result<ReceiveQRCodeResponse, ApiError>) -> Void) {
        generateQRCodeCallCount += 1
        completion(generateQRCodeResult)
    }
}
