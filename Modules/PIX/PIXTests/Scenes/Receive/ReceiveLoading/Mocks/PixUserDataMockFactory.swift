import Foundation
@testable import PIX

enum PixUserDataMockFactory {
    static func make() -> PixUserData {
        .init(email: "test@email.com", cpf: "11144477735", phoneNumber: .init(country: 55, area: 61, number: 988888888))
    }
}
