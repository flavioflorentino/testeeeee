import Foundation
@testable import PIX

enum ProfileURLImageMockFactory {
    static func make() -> URL? {
        URL(string: "https://s3-sa-east-1.amazonaws.com/picpay/banks/ico_cadastro_original.png")
    }
}
