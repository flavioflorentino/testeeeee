@testable import PIX
import XCTest
import Core

final class ReceiveLoadingInteractorTests: XCTestCase {
    private let presenter = ReceiveLoadingPresenterSpy()
    private lazy var service = ReceiveServiceMock()
    private lazy var consumerService = KeyManagerServiceMock()
    private let origin = PIXReceiveOrigin.pixHub
    private lazy var interactor = ReceiveLoadingInteractor(
        presenter: presenter,
        service: service,
        consumerService: consumerService,
        origin: origin
    )
    
    func testLoadReceiveRequirements_WhenFailureInGetKeysCall_ShouldPresentError() {
        service.listPixKeysResult = .failure(.connectionFailure)
        let userData = PixUserDataMockFactory.make()
        consumerService.getConsumerDataResult = .success(userData)

        interactor.loadReceiveRequirements()
        XCTAssertEqual(presenter.presentLoadingCallCount, 1)
        XCTAssertEqual(service.userId, "123")
        XCTAssertEqual(presenter.presentErrorCallCount, 1)
    }

    func testLoadReceiveRequirements_WhenNoRegisteredKeys_ShouldCallDidNextStepWithRegisterKeyAction() {
        service.listPixKeysResult = .success(.init(data: []))
        let userData = PixUserDataMockFactory.make()
        consumerService.getConsumerDataResult = .success(userData)

        interactor.loadReceiveRequirements()
        XCTAssertEqual(presenter.presentLoadingCallCount, 1)
        XCTAssertEqual(service.userId, "123")
        XCTAssertEqual(presenter.didNextStepActionReceived, .registerKeys(origin: origin))
    }

    func testViewDidLoad_WhenAtLeastOneProcessedKeyAndSuccessInQRCodeCall_ShouldCallDidNextStepWithReceivePaymentsAction() throws {
        var key = PixKey(type: .registeredKey)
        let registeredKey = RegisteredKey(id: "", statusSlug: .processed, status: "", name: "", keyType: .cpf, keyValue: "")
        key.registeredKeys = [registeredKey]
        let userData = PixUserDataMockFactory.make()
        let qrCodeImage = try XCTUnwrap(QRCodeImageMockFactory.make())
        let qrCodeText = "Test"
        consumerService.getConsumerDataResult = .success(userData)
        service.listPixKeysResult = .success(.init(data: [key]))
        service.generateQRCodeResult = .success(.init(qrCodeInBase64: QRCodeImageMockFactory.base64, qrCodeText: qrCodeText))
        interactor.loadReceiveRequirements()
        XCTAssertEqual(presenter.presentLoadingCallCount, 1)
        XCTAssertEqual(service.userId, "123")

        XCTAssertEqual(presenter.didNextStepCallCount, 1)
        let output = ReceivePaymentsData(keys: [registeredKey], userData: userData, selectedKey: registeredKey, qrCodeImage: qrCodeImage, qrCodeText: qrCodeText)
        XCTAssertEqual(presenter.didNextStepActionReceived, .receivePayment(output: output, origin: origin))
    }

    func testViewDidLoad_WhenARegisteredKeyThatIsNotProcessedAndSuccessInQRCodeCall_ShouldCallDidNextStepWithRegisterKeyAction() throws {
        var key = PixKey(type: .registeredKey)
        let registeredKey = RegisteredKey(id: "", statusSlug: .automaticConfirmed, status: "", name: "", keyType: .cpf, keyValue: "")
        key.registeredKeys = [registeredKey]
        let userData = PixUserDataMockFactory.make()
        let qrCodeText = "Test"
        consumerService.getConsumerDataResult = .success(userData)
        service.listPixKeysResult = .success(.init(data: [key]))
        service.generateQRCodeResult = .success(.init(qrCodeInBase64: QRCodeImageMockFactory.base64, qrCodeText: qrCodeText))
        interactor.loadReceiveRequirements()
        XCTAssertEqual(presenter.presentLoadingCallCount, 1)
        XCTAssertEqual(service.userId, "123")

        XCTAssertEqual(presenter.didNextStepCallCount, 1)
        XCTAssertEqual(presenter.didNextStepActionReceived, .registerKeys(origin: origin))
    }
}


private final class KeyManagerServiceMock: KeyManagerConsumerServicing {
    var getConsumerDataResult: Result<PixUserData, ApiError> = .failure(.cancelled)
    var getIdValidationStatusResult: Result<IdentityValidationStatus, ApiError> = .failure(.cancelled)
    
    var consumerId: Int? { 123 }

    var authenticationIsEnabled: Bool = false
    var identityValidationIsEnabled: Bool = false

    func getConsumerData(completion: @escaping (Result<PixUserData, ApiError>) -> Void) {
        completion(getConsumerDataResult)
    }
    
    func getIdValidationStatus(completion: @escaping (Result<IdentityValidationStatus, ApiError>) -> Void) {
        completion(getIdValidationStatusResult)
    }

    func requestNewValidationCode(inputType: SecurityCodeValidationType, completion: @escaping (Result<SecurityCodeRequestResponse, ApiError>) -> Void) { }

    func requestPasswordFromUser(completion: @escaping (Result<String, AuthenticationStatus>) -> Void) { }
    
    func setOnClipboard(keyValue: String) { }
}

private final class ReceiveLoadingPresenterSpy: ReceiveLoadingPresenting {
    var viewController: ReceiveLoadingDisplay?
    private(set) var didNextStepActionReceived: ReceiveLoadingAction?
    private(set) var presentLoadingCallCount = 0
    private(set) var presentErrorCallCount = 0
    private(set) var didNextStepCallCount = 0

    func presentLoading() {
        presentLoadingCallCount += 1
    }

    func presentError() {
        presentErrorCallCount += 1
    }

    func didNextStep(action: ReceiveLoadingAction) {
        didNextStepCallCount += 1
        didNextStepActionReceived = action
    }
}
