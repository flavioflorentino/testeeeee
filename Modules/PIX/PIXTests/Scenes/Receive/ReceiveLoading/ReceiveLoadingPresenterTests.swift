@testable import PIX
import UI
import XCTest
import AssetsKit

private final class ReceiveLoadingCoordinatorSpy: ReceiveLoadingCoordinating {
    var viewController: UIViewController?

    private(set) var performCallsCount = 0
    private(set) var action: ReceiveLoadingAction?

    func perform(action: ReceiveLoadingAction) {
        performCallsCount += 1
        self.action = action
    }
}

private final class ReceiveLoadingViewControllerSpy: ReceiveLoadingDisplay {
    private(set) var displayLoadingCallCount = 0
    private(set) var displayErrorCallCount = 0
    private(set) var errorViewModel: ReceiveLoadingErrorViewModel?

    func displayLoading() {
        self.displayLoadingCallCount += 1
    }

    func displayError(viewModel: ReceiveLoadingErrorViewModel) {
        self.displayErrorCallCount += 1
        self.errorViewModel = viewModel
    }
}

final class ReceiveLoadingPresenterTests: XCTestCase {
    private let coordinatorSpy = ReceiveLoadingCoordinatorSpy()
    private let viewControllerSpy = ReceiveLoadingViewControllerSpy()
    private lazy var presenter: ReceiveLoadingPresenting = {
        let presenter = ReceiveLoadingPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()

    func testPresentLoading_ShouldDisplayLoading() {
        presenter.presentLoading()
        XCTAssertEqual(viewControllerSpy.displayLoadingCallCount, 1)
    }

    func testPresentError_ShouldDisplayError() {
        presenter.presentError()
        XCTAssertEqual(viewControllerSpy.displayErrorCallCount, 1)
        let expectedViewModel = ReceiveLoadingErrorViewModel(
            image: Resources.Illustrations.iluFalling.image,
            title: "Opa! Algo deu errado",
            description: "Por enquanto, as informações de recebimento estão indisponíveis. Tente mais uma vez.",
            buttonTitle: "Tentar novamente"
        )
        XCTAssertEqual(viewControllerSpy.errorViewModel, expectedViewModel)
    }

    func testDidNextStep_WhenActionIsRegisterKeys_ShoudlRedirectCallToCoordinator() {
        presenter.didNextStep(action: .registerKeys(origin: .pixHub))
        XCTAssertEqual(coordinatorSpy.action, .registerKeys(origin: .pixHub))
    }

    func testDidNextStep_WhenActionIsReceivePayment_ShoudlRedirectCallToCoordinator() throws {
        let userData = PixUserDataMockFactory.make()
        let keys = RegisteredKeysMockFactory.make()
        let selectedKey = try XCTUnwrap(keys.first)
        let qrCodeImage = try XCTUnwrap(QRCodeImageMockFactory.make())
        let qrCodeText = "test"
        let output = ReceivePaymentsData(keys: keys, userData: userData, selectedKey: selectedKey, qrCodeImage: qrCodeImage, qrCodeText: qrCodeText)
        let action: ReceiveLoadingAction = .receivePayment(output: output, origin: .pixHub)
        presenter.didNextStep(action: action)
        XCTAssertEqual(coordinatorSpy.action, action)
    }
}
