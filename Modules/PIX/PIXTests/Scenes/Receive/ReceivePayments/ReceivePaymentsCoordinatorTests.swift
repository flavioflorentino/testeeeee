@testable import PIX
import XCTest

final class ReceivePaymentsCoordinatorTests: XCTestCase {
    private lazy var viewController = SpyViewController()
    private lazy var navigationController = SpyNavigationController(rootViewController: viewController)

    private lazy var coordinator: ReceivePaymentsCoordinating = {
        let coordinator = ReceivePaymentsCoordinator()
        coordinator.viewController = viewController
        return coordinator
    }()

    func testPerform_WhenShareQRCodeAction_ShouldPresentViewController() {
        XCTAssertEqual(viewController.presentCallCount, 0)
        let controller = UIActivityViewController(activityItems: [], applicationActivities: nil)
        coordinator.perform(action: .shareQRCode(activityController: controller))
        XCTAssertEqual(viewController.presentCallCount, 1)
    }

    func testPerform_WithCustomizeQRCodeAction_ShouldPresentViewController() throws {
        let keys = RegisteredKeysMockFactory.make()
        coordinator.perform(action: .customizeQRCode(userKeys: keys, selectedKey: keys[0], origin: .pixHub))
        XCTAssertEqual(viewController.presentCallCount, 1)
        let controller = try XCTUnwrap((viewController.viewControllerPresented as? UINavigationController)?.topViewController)
        XCTAssert(controller is CustomizeQRCodeConsumerViewController)
    }

    func testPerform_WithRegisterNewKeyAction_ShouldPushViewController() {
        XCTAssertEqual(navigationController.pushViewControllerCallCount, 1)
        coordinator.perform(action: .registerNewKey)
        XCTAssertEqual(navigationController.pushViewControllerCallCount, 2)
        XCTAssert(navigationController.viewControllers.last is KeyManagerConsumerViewController)
    }
}
