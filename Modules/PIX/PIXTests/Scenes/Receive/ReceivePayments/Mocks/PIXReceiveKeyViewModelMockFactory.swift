@testable import PIX

enum PIXReceiveKeyViewModelMockFactory {
    static func make() -> [PIXReceiveKeyViewModel] {
        [
            PIXReceiveKeyViewModel(id: "1", type: .phone, value: "(61) 98888-8888"),
            PIXReceiveKeyViewModel(id: "2", type: .cpf, value: "111.444.777-35"),
            PIXReceiveKeyViewModel(id: "3", type: .email, value: "test@email.com"),
            PIXReceiveKeyViewModel(id: "4", type: .random, value: "189hsahsnd8902"),
            PIXReceiveKeyViewModel(id: "5", type: .random, value: "189hsahsnd8905")
        ]
    }
}
