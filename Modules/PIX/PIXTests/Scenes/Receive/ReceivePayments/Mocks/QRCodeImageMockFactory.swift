import Foundation
import UIKit
import UI
@testable import PIX

enum QRCodeImageMockFactory {
    static func make() -> UIImage? {
        UIImage(base64: QRCodeImageMockFactory.base64)
    }

     static var base64: String {
        model.qrCodeInBase64
     }

    private static var model: ReceiveQRCodeResponse {
        do {
            let model = try JSONFileDecoder<ReceiveQRCodeResponse>().load(resource: "GenerateQRCodeResponse")
            return model
        } catch {
            return .init(qrCodeInBase64: "", qrCodeText: "")
        }
    }
 }
