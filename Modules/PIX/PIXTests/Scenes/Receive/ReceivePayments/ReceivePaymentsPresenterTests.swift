@testable import PIX
import UI
import XCTest

private final class ReceivePaymentsCoordinatorSpy: ReceivePaymentsCoordinating {
    var viewController: UIViewController?

    private(set) var performCallsCount = 0
    private(set) var action: ReceivePaymentsAction?

    func perform(action: ReceivePaymentsAction) {
        performCallsCount += 1
        self.action = action
    }
}

private final class ReceivePaymentsViewControllerSpy: ReceivePaymentsDisplay {
    private(set) var displayUserKeysCallCount = 0
    private(set) var displayLoadingQRCodeCallCount = 0
    private(set) var displayErrorQRCodeCallCount = 0
    private(set) var displayQRCodeCallCount = 0
    private(set) var displaySelectedKeyCallCount = 0
    private(set) var displayUserNameCallCount = 0
    private(set) var displayRegisterKeyButtonCallCount = 0
    private(set) var displayChangeKeyButtonCallCount = 0
    private(set) var displayTextsCallCount = 0
    private(set) var displaySecondaryButtonCallCount = 0
    private(set) var hideChangeKeyButtonCallCount = 0
    private(set) var displayImageSavedAlertCallCount = 0
    private(set) var displayButtonStyleCallsCount = 0
    private(set) var displayQRCodeContainerCallsCount = 0
    private(set) var displayCustomQRCodeContainerCallsCount = 0
    private(set) var displayCopyPasteContainerCallsCount = 0
    private(set) var displayCopyPasteNotificationCallsCount = 0

    private(set) var formattedKey: String = ""
    private(set) var userNameReceived: String = ""
    private(set) var keysReceived = [PIXReceiveKeyViewModel]()
    private(set) var imageReceived: UIImage?
    private(set) var profileImageURLReceived: URL?
    private(set) var qrCodeText: String?
    private(set) var titleReceived: String?
    private(set) var descriptionReceived: String?
    private(set) var secondaryButtonTypeReceived: ReceivePaymentsViewController.SecondaryButtonAction?
    private(set) var alertTitleReceived: String?
    private(set) var alertDescriptionReceived: String?
    private(set) var backgroundColor: UIColor?
    private(set) var titleColor: UIColor?
    private(set) var index: Int?
    private(set) var message: String?

    func displayUserKeys(keys: [PIXReceiveKeyViewModel]) {
        keysReceived = keys
        displayUserKeysCallCount += 1
    }

    func displayLoadingQRCode() {
        displayLoadingQRCodeCallCount += 1
    }

    func displayErrorQRCode() {
        displayErrorQRCodeCallCount += 1
    }

    func displayQRCode(image: UIImage, profileImageURL: URL?, qrCodeText: String) {
        imageReceived = image
        profileImageURLReceived = profileImageURL
        self.qrCodeText = qrCodeText
        displayQRCodeCallCount += 1
    }

    func displaySelectedKey(key: String) {
        displaySelectedKeyCallCount += 1
        formattedKey = key
    }

    func displayUserName(name: String) {
        displayUserNameCallCount += 1
        userNameReceived = name
    }

    func displayRegisterKeyButton() {
        displayRegisterKeyButtonCallCount += 1
    }

    func displayChangeKeyButton() {
        displayChangeKeyButtonCallCount += 1
    }

    func displayFAQButton() { }

    func displayTexts(title: String, description: String) {
        displayTextsCallCount += 1
        titleReceived = title
        descriptionReceived = description
    }

    func displaySecondaryButton(type: ReceivePaymentsViewController.SecondaryButtonAction) {
        displaySecondaryButtonCallCount += 1
        secondaryButtonTypeReceived = type
    }

    func hideChangeKeyButton() {
        hideChangeKeyButtonCallCount += 1
    }

    func displayImageSavedAlert(title: String, description: String) {
        displayImageSavedAlertCallCount += 1
        alertTitleReceived = title
        alertDescriptionReceived = description
    }
    
    func displayButtonStyle(backgroundColor: UIColor, titleColor: UIColor, index: Int) {
        self.backgroundColor = backgroundColor
        self.titleColor = titleColor
        self.index = index
        displayButtonStyleCallsCount += 1
    }
    
    func displayQRCodeContainer() {
        displayQRCodeContainerCallsCount += 1
    }
    
    func displayCustomQRCodeContainer() {
        displayCustomQRCodeContainerCallsCount += 1
    }
    
    func displayCopyPasteContainer() {
        displayCopyPasteContainerCallsCount += 1
    }
    
    func displayCopyPasteNotification(message: String) {
        displayCopyPasteNotificationCallsCount += 1
        self.message = message
    }
}

final class ReceivePaymentsPresenterTests: XCTestCase {
    private let coordinatorSpy = ReceivePaymentsCoordinatorSpy()
    private let viewControllerSpy = ReceivePaymentsViewControllerSpy()
    private lazy var presenter: ReceivePaymentsPresenting = {
        let presenter = ReceivePaymentsPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()

    func testPresentErrorQRCode_DisplayErrorQRCode() {
        presenter.presentErrorQRCode()
        XCTAssertEqual(viewControllerSpy.displayErrorQRCodeCallCount, 1)
    }

    func testPresentLoadingQRCode_DisplayLoadingQRCode() {
        presenter.presentLoadingQRCode()
        XCTAssertEqual(viewControllerSpy.displayLoadingQRCodeCallCount, 1)
    }

    func testPresentQRCode_DisplayQRCode() throws {
        let image = try XCTUnwrap(UIImage(base64: QRCodeImageMockFactory.base64))
        let url = ProfileURLImageMockFactory.make()
        let qrCodeText = "Test"
        presenter.presentQRCode(image: image, profileImageURL: url, qrCodeText: qrCodeText)
        XCTAssertEqual(viewControllerSpy.displayQRCodeCallCount, 1)
        XCTAssertNotNil(viewControllerSpy.imageReceived)
        XCTAssertEqual(viewControllerSpy.profileImageURLReceived, url)
        XCTAssertEqual(viewControllerSpy.qrCodeText, qrCodeText)
    }

    func testPresentSelectedKey_ShouldDisplaySelectedKey() throws {
        let key = try XCTUnwrap(RegisteredKeysMockFactory.make().first)
        let userData = PixUserDataMockFactory.make()
        presenter.presentSelectedKey(key: key, userData: userData)
        XCTAssertEqual(viewControllerSpy.displaySelectedKeyCallCount, 1)
        XCTAssertEqual(viewControllerSpy.formattedKey, "Celular: (61) 98888-8888")
    }

    func testPresentUserName_ShouldDisplayUserName() {
        presenter.presentUserName(name: "username")
        XCTAssertEqual(viewControllerSpy.displayUserNameCallCount, 1)
        XCTAssertEqual(viewControllerSpy.userNameReceived, "username")
    }

    func testPresentUserKeys_ShouldDisplayUserKeys() {
        let keys = RegisteredKeysMockFactory.make()
        let viewModels = PIXReceiveKeyViewModelMockFactory.make()
        let userData = PixUserDataMockFactory.make()
        presenter.presentUserKeys(keys: keys, userData: userData)
        XCTAssertEqual(viewControllerSpy.displayUserKeysCallCount, 1)
        XCTAssertEqual(viewControllerSpy.keysReceived.count, 5)
        XCTAssertEqual(viewControllerSpy.keysReceived.first?.type, viewModels.first?.type)
        XCTAssertEqual(viewControllerSpy.keysReceived.first?.value, viewModels.first?.value)
        XCTAssertEqual(viewControllerSpy.keysReceived[1].type, viewModels[1].type)
        XCTAssertEqual(viewControllerSpy.keysReceived[1].value, viewModels[1].value)
        XCTAssertEqual(viewControllerSpy.keysReceived[2].type, viewModels[2].type)
        XCTAssertEqual(viewControllerSpy.keysReceived[2].value, viewModels[2].value)
        XCTAssertEqual(viewControllerSpy.keysReceived.last?.type, viewModels.last?.type)
        XCTAssertEqual(viewControllerSpy.keysReceived.last?.value, viewModels.last?.value)
    }

    func testDidNextStep_ShouldCallDidNextStep() {
        let keys = RegisteredKeysMockFactory.make()
        let action: ReceivePaymentsAction = .customizeQRCode(userKeys: RegisteredKeysMockFactory.make(), selectedKey: keys[0], origin: .pixHub)
        presenter.didNextStep(action: action)
        XCTAssertEqual(coordinatorSpy.action, action)
    }

    func testRegisterKeyButton_ShouldDisplayRegisterKeyButton() {
        presenter.presentRegisterKeyButton()
        XCTAssertEqual(viewControllerSpy.displayRegisterKeyButtonCallCount, 1)
    }

    func testChangeKeyButton_ShoudlDisplayChangeKeyButton() {
        presenter.presentChangeKeyButton()
        XCTAssertEqual(viewControllerSpy.displayChangeKeyButtonCallCount, 1)
    }

    func testPresentCustomizeQRCodeButton_ShouldCallDisplaySecondaryButton() {
        presenter.presentCustomizeQRCodeButton()
        XCTAssertEqual(viewControllerSpy.displaySecondaryButtonCallCount, 1)
        XCTAssertEqual(viewControllerSpy.secondaryButtonTypeReceived, .customizeQRCode(title: "Criar QR Code personalizado"))
    }

    func testPresentTexts_ShouldCallDisplaySecondaryButton() {
        presenter.presentTexts()
        XCTAssertEqual(viewControllerSpy.displayTextsCallCount, 1)
        XCTAssertEqual(viewControllerSpy.titleReceived, Strings.Receive.ReceivePayments.title)
        XCTAssertEqual(viewControllerSpy.descriptionReceived, Strings.Receive.ReceivePayments.description)
    }
    
    func testPresentReceivementSelectionStyle_WhenHasNoPreviousType_ShouldCallDisplayButtonStyleOnce() {
        let newType = ReceivementPixType.qrcode
        presenter.presentReceivementSelectionStyle(newType: newType, previousType: nil)
        XCTAssertEqual(viewControllerSpy.displayButtonStyleCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.index, newType.rawValue)
    }
    
    func testPresentReceivementSelectionStyle_WhenHasPreviousType_ShouldCallDisplayButtonStyleTwice() {
        let newType = ReceivementPixType.qrcode
        let previousType = ReceivementPixType.copyPaste
        presenter.presentReceivementSelectionStyle(newType: newType, previousType: previousType)
        XCTAssertEqual(viewControllerSpy.displayButtonStyleCallsCount, 2)
        XCTAssertEqual(viewControllerSpy.index, previousType.rawValue)
    }
    
    func testPresentQRCodeContainer_ShouldCallDisplayQRCodeContainer() {
        presenter.presentQRCodeContainer()
        XCTAssertEqual(viewControllerSpy.displayQRCodeContainerCallsCount, 1)
    }
    
    func testPresentCustomQRCodeContainer_ShouldCallDisplayCustomQRCodeContainer() {
        presenter.presentCopyPasteContainer()
        XCTAssertEqual(viewControllerSpy.displayCopyPasteContainerCallsCount, 1)
    }
    
    func testPresentCopyPasteNotification_ShouldCallDisplayCopyPasteNotificationWithCopiedCodeMessage() {
        presenter.presentCopyPasteNotification()
        XCTAssertEqual(viewControllerSpy.displayCopyPasteNotificationCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.message, Strings.Receive.ReceivePayments.copiedCode)
    }
}
