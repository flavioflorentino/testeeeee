@testable import PIX
import Foundation
import XCTest
import Core
import AnalyticsModule

private final class ReceivePaymentsPresenterSpy: ReceivePaymentsPresenting {
    var viewController: ReceivePaymentsDisplay?
    
    private(set) var presentLoadingQRCodeCallCount = 0
    private(set) var presentErrorQRCodeCallCount = 0
    private(set) var presentQRCodeCallCount = 0
    private(set) var presentSelectedKeyCallCount = 0
    private(set) var presentUserNameCallCount = 0
    private(set) var presentUserKeysCallCount = 0
    private(set) var presentRegisterKeyButtonCallCount = 0
    private(set) var presentChangeKeyButtonCallCount = 0
    private(set) var presentCustomizeQRCodeButtonCallCount = 0
    private(set) var presentTextsCallCount = 0
    private(set) var presentReceivementSelectionStyleCallsCount = 0
    private(set) var presentQRCodeContainerCallsCount = 0
    private(set) var presentCustomQRCodeContainerCallsCount = 0
    private(set) var presentCopyPasteContainerCallsCount = 0
    private(set) var didNextStepCallsCount = 0
    private(set) var presentCopyPasteNotificationCallsCount = 0

    private(set) var keys = [RegisteredKey]()
    private(set) var didNextStepActionReceived: ReceivePaymentsAction?
    private(set) var nameReceived: String?
    private(set) var imageReceived: UIImage?
    private(set) var keyReceived: RegisteredKey?
    private(set) var userDataReceived: PixUserData?
    private(set) var profileImageURLReceived: URL?
    private(set) var qrCodeText: String?
    private(set) var newType: ReceivementPixType?
    private(set) var previousType: ReceivementPixType?

    func presentLoadingQRCode() {
        presentLoadingQRCodeCallCount += 1
    }

    func presentErrorQRCode() {
        presentErrorQRCodeCallCount += 1
    }

    func presentQRCode(image: UIImage, profileImageURL: URL?, qrCodeText: String) {
        imageReceived = image
        profileImageURLReceived = profileImageURL
        self.qrCodeText = qrCodeText
        presentQRCodeCallCount += 1
    }

    func presentSelectedKey(key: RegisteredKey, userData: PixUserData) {
        keyReceived = key
        userDataReceived = userData
        presentSelectedKeyCallCount += 1
    }

    func presentUserName(name: String) {
        self.nameReceived = name
        presentUserNameCallCount += 1
    }

    func presentUserKeys(keys: [RegisteredKey], userData: PixUserData) {
        self.keys = keys
        presentUserKeysCallCount += 1
    }

    func didNextStep(action: ReceivePaymentsAction) {
        didNextStepActionReceived = action
        didNextStepCallsCount += 1
    }

    func presentRegisterKeyButton() {
        presentRegisterKeyButtonCallCount += 1
    }

    func presentChangeKeyButton() {
        presentChangeKeyButtonCallCount += 1
    }

    func presentCustomizeQRCodeButton() {
        presentCustomizeQRCodeButtonCallCount += 1
    }

    func presentTexts() {
        presentTextsCallCount += 1
    }
    
    func presentReceivementSelectionStyle(newType: ReceivementPixType, previousType: ReceivementPixType?) {
        self.newType = newType
        self.previousType = previousType
        presentReceivementSelectionStyleCallsCount += 1
    }
    
    func presentQRCodeContainer() {
        presentQRCodeContainerCallsCount += 1
    }
    
    func presentCustomQRCodeContainer() {
        presentCustomQRCodeContainerCallsCount += 1
    }
    
    func presentCopyPasteContainer() {
        presentCopyPasteContainerCallsCount += 1
    }
    
    func presentCopyPasteNotification() {
        presentCopyPasteNotificationCallsCount += 1
    }
}

private final class ReceivePixConsumerMock: PixConsumerContract {
    var consumerId: Int { 123 }

    var name: String { "Test" }

    var imageURL: URL? {
        ProfileURLImageMockFactory.make()
    }
}

final class ReceivePaymentsInteractorTests: XCTestCase {
    private let presenter = ReceivePaymentsPresenterSpy()
    private let service = ReceiveServiceMock()
    private let keys = RegisteredKeysMockFactory.make()
    private let userData = PixUserDataMockFactory.make()
    private let consumerInstance = ReceivePixConsumerMock()
    private let qrCodeImage = QRCodeImageMockFactory.make() ?? .init()
    private let qrCodeText = "test"
    private var isCustomQRCodeEnabled = false
    private lazy var selectedKey = keys.first ?? .init(id: "", statusSlug: .processed, status: "", name: "", keyType: .cpf, keyValue: "")
    private lazy var data = ReceivePaymentsData(keys: keys, userData: userData, selectedKey: selectedKey, qrCodeImage: qrCodeImage, qrCodeText: qrCodeText)

    private let analyticsSpy = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(analyticsSpy)
    private let origin = PIXReceiveOrigin.pixHub
    private lazy var interactor = ReceivePaymentsInteractor(
        presenter: presenter,
        service: service,
        dependencies: dependencies,
        consumerInstance: consumerInstance,
        data: data, isCustomQRCodeEnabled: isCustomQRCodeEnabled,
        origin: origin
    )

    func testViewDidLoad_With_ShouldPresentQRCodeSelectedQRCode() {
        let response = ReceiveQRCodeResponse(qrCodeInBase64: QRCodeImageMockFactory.base64, qrCodeText: qrCodeText)
        service.generateQRCodeResult = .success(response)
        interactor.viewDidLoad()

        XCTAssertEqual(presenter.presentUserKeysCallCount, 1)
        XCTAssertEqual(presenter.keys, keys)
        XCTAssertEqual(presenter.presentQRCodeCallCount, 1)
        XCTAssertNotNil(presenter.imageReceived)
        XCTAssertEqual(presenter.qrCodeText, qrCodeText)
        XCTAssertEqual(presenter.profileImageURLReceived, consumerInstance.imageURL)
        XCTAssertEqual(presenter.presentChangeKeyButtonCallCount, 1)
        XCTAssertEqual(presenter.presentSelectedKeyCallCount, 1)
        XCTAssertEqual(presenter.keyReceived, selectedKey)
        XCTAssertEqual(presenter.userDataReceived, userData)
        XCTAssertEqual(presenter.presentUserNameCallCount, 1)
        XCTAssertEqual(presenter.nameReceived, consumerInstance.name)
        XCTAssertEqual(presenter.presentTextsCallCount, 1)
        XCTAssertEqual(presenter.presentCustomizeQRCodeButtonCallCount, 1)
    }

    func testViewDidLoad_WithOnlyOneKey_ShouldPresentRegisterNewKeyButton() throws {
        let response = ReceiveQRCodeResponse(qrCodeInBase64: QRCodeImageMockFactory.base64, qrCodeText: qrCodeText)
        service.generateQRCodeResult = .success(response)

        data.keys = [selectedKey]
        interactor = ReceivePaymentsInteractor(
            presenter: presenter,
            service: service,
            dependencies: dependencies,
            consumerInstance: consumerInstance,
            data: data,
            isCustomQRCodeEnabled: isCustomQRCodeEnabled,
            origin: origin
        )
        interactor.viewDidLoad()

        XCTAssertEqual(presenter.presentUserKeysCallCount, 1)
        XCTAssertEqual(presenter.keys, [selectedKey])
        XCTAssertEqual(presenter.presentQRCodeCallCount, 1)
        XCTAssertNotNil(presenter.imageReceived)
        XCTAssertEqual(presenter.qrCodeText, qrCodeText)
        XCTAssertEqual(presenter.profileImageURLReceived, consumerInstance.imageURL)
        XCTAssertEqual(presenter.presentRegisterKeyButtonCallCount, 1)
        XCTAssertEqual(presenter.presentSelectedKeyCallCount, 1)
        XCTAssertEqual(presenter.keyReceived, selectedKey)
        XCTAssertEqual(presenter.userDataReceived, userData)
        XCTAssertEqual(presenter.presentUserNameCallCount, 1)
        XCTAssertEqual(presenter.nameReceived, consumerInstance.name)
    }
    
    func testShareQRCode_ShouldCallDidNextStep() throws {
        interactor.shareQRCode(image: UIImage())
        let finalAction = try XCTUnwrap(presenter.didNextStepActionReceived)
        XCTAssertEqual(presenter.didNextStepCallsCount, 1)
        guard case ReceivePaymentsAction.shareQRCode(_) = finalAction else {
            XCTFail("Wrong action type")
            return
        }
    }

    func testDidSelectKey_WithLoadingAnInvalidBase64_ShoudlPresentError() throws {
        let response = ReceiveQRCodeResponse(qrCodeInBase64: "", qrCodeText: qrCodeText)
        service.generateQRCodeResult = .success(response)
        let key = try XCTUnwrap(PIXReceiveKeyViewModelMockFactory.make().last)
        interactor.didSelectKey(viewModel: key)

        XCTAssertEqual(presenter.presentLoadingQRCodeCallCount, 1)
        XCTAssertEqual(presenter.presentErrorQRCodeCallCount, 1)
    }

    func testDidSelectKey_WithLoadingAValidBase64_ShoudlPresentNewKeyAndNewQRCode() throws {
        let response = ReceiveQRCodeResponse(qrCodeInBase64: QRCodeImageMockFactory.base64, qrCodeText: qrCodeText)
        service.generateQRCodeResult = .success(response)

        let key = try XCTUnwrap(PIXReceiveKeyViewModelMockFactory.make().last)
        interactor.didSelectKey(viewModel: key)

        XCTAssertEqual(presenter.presentLoadingQRCodeCallCount, 1)
        XCTAssertEqual(presenter.presentQRCodeCallCount, 1)
        XCTAssertNotNil(presenter.imageReceived)
        XCTAssertEqual(presenter.qrCodeText, qrCodeText)
        XCTAssertEqual(presenter.presentSelectedKeyCallCount, 1)
        XCTAssertEqual(presenter.keyReceived, keys.last)
        XCTAssertEqual(presenter.userDataReceived, userData)
    }

    func testDidSelectKey_WithFailedNetworkCall_ShouldPresentError() throws {
        service.generateQRCodeResult = .failure(ApiError.timeout)

        let key = try XCTUnwrap(PIXReceiveKeyViewModelMockFactory.make().last)
        interactor.didSelectKey(viewModel: key)

        XCTAssertEqual(presenter.presentLoadingQRCodeCallCount, 1)
        XCTAssertEqual(presenter.presentQRCodeCallCount, 1)
        XCTAssertEqual(presenter.presentErrorQRCodeCallCount, 1)
    }

    func testRegisterNewKeyButton_ShouldPresentChangeKey() {
        interactor.registerNewKey()
        XCTAssertEqual(presenter.didNextStepActionReceived, .registerNewKey)
    }
    
    func testSelectReceivementType_WhenNewTypeIsDifferentThanSelectedReceivementType_ShouldCallPresentReceivementSelectionStyleAndPresentCopyPasteContainer() {
        interactor.selectReceivementType(.copyPaste)
        XCTAssertEqual(presenter.presentReceivementSelectionStyleCallsCount, 1)
        XCTAssertEqual(presenter.presentCopyPasteContainerCallsCount, 1)
    }
    
    func testSelectReceivementType_WhenNewTypeIsDifferentThanSelectedReceivementTypeAndReceivementTypeIsQrCodeAndIsCustomQRCodeEnabledIsFalse_ShouldCallPresentReceivementSelectionStyleAndPresentQRCodeContainer() {
        isCustomQRCodeEnabled = false
        interactor.selectReceivementType(.copyPaste)
        interactor.selectReceivementType(.qrcode)
        XCTAssertEqual(presenter.presentReceivementSelectionStyleCallsCount, 2)
        XCTAssertEqual(presenter.presentQRCodeContainerCallsCount, 1)
    }
    
    func testSelectReceivementType_WhenNewTypeIsDifferentThanSelectedReceivementTypeAndReceivementTypeIsQrCodeAndIsCustomQRCodeEnabledIsTrue_ShouldCallPresentReceivementSelectionStyleAndPresentCustomQRCodeContainer() {
        isCustomQRCodeEnabled = true
        interactor.selectReceivementType(.copyPaste)
        interactor.selectReceivementType(.qrcode)
        XCTAssertEqual(presenter.presentReceivementSelectionStyleCallsCount, 2)
        XCTAssertEqual(presenter.presentCustomQRCodeContainerCallsCount, 1)
    }
  
    /// Remover qualquer iteração com o simulador
//    func testCopyCode_ShouldHasQrCodeTextInUIPasteboardAndCallPresentCopyPasteNotification() {
//        interactor.copyCode()
//        XCTAssertEqual(UIPasteboard.general.string, qrCodeText)
//        XCTAssertEqual(presenter.presentCopyPasteNotificationCallsCount, 1)
//    }
}
