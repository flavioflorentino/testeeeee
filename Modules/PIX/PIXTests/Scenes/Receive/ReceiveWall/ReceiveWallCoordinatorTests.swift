@testable import PIX
import XCTest

final class IdentityValidationMock: IdentityValidationContract {
    var openIdentityValidationCallCount = 0

    func openIdentityValidation() {
        openIdentityValidationCallCount += 1
    }
}

final class ReceiveWallCoordinatorTests: XCTestCase {
    private let viewController = SpyViewController()
    private lazy var navigationController = SpyNavigationController(rootViewController: viewController)

    private lazy var coordinator: ReceiveWallCoordinating = {
        let coordinator = ReceiveWallCoordinator()
        coordinator.viewController = viewController
        return coordinator
    }()

    private let identityValidationMock = IdentityValidationMock()

    func testPerform_WhenActionIsIdentityNotValidated_ShouldOpenIdentityValidationFlow() {
        PIXSetup.inject(instance: identityValidationMock)
        XCTAssertEqual(navigationController.pushViewControllerCallCount, 1)
        coordinator.perform(action: .identityNotValidated)
        XCTAssertEqual(navigationController.dismissCallCount, 1)
        XCTAssertEqual(identityValidationMock.openIdentityValidationCallCount, 1)
    }

    func testPerform_WhenActionIsIdentityNotValidated_ShouldPushCorrectViewController() {
        XCTAssertEqual(navigationController.pushViewControllerCallCount, 1)
        coordinator.perform(action: .noKeysRegistered)
        XCTAssertEqual(navigationController.pushViewControllerCallCount, 2)
        XCTAssertTrue(navigationController.viewControllers.last is KeyManagerConsumerViewController)
    }
}
