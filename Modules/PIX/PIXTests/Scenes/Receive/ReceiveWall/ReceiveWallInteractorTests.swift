@testable import PIX
import XCTest
import AnalyticsModule

private final class ReceiveWallPresenterSpy: ReceiveWallPresenting {
    var viewController: ReceiveWallDisplay?
    private(set) var didNextStepCallsCount = 0
    private(set) var didNextStepActionReceived: ReceiveWallAction?
    private(set) var presentCallsCount = 0
    private(set) var presentActionReceived: ReceiveWallDisplayAction?

    func didNextStep(action: ReceiveWallAction) {
        didNextStepCallsCount += 1
        didNextStepActionReceived = action
    }

    func present(type: ReceiveWallDisplayAction) {
        presentCallsCount += 1
        presentActionReceived = type
    }
}

final class ReceiveWallInteractorTests: XCTestCase {
    private let presenter = ReceiveWallPresenterSpy()

    private lazy var type: ReceiveWallDisplayAction = .identityNotValidated
    private lazy var keysMock = RegisteredKeysMockFactory.make()
    private let analyticsSpy = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(analyticsSpy)
    private let origin = PIXReceiveOrigin.pixHub
    private lazy var interactor = ReceiveWallInteractor(
        presenter: presenter,
        dependencies: dependencies,
        type: type,
        origin: origin
    )

    // MARK: - viewDidLoad
    func testViewDidLoad_ShoudCallPresent_WithCorrectType() {
        interactor.viewDidLoad()
        XCTAssertEqual(presenter.presentCallsCount, 1)
        XCTAssertEqual(presenter.presentActionReceived, type)
    }
    
    // MARK: - didNextStep
    func testDidNextStep_ShouldCallDidNextStep_WithCorrectAction() {
        interactor.didNextStep()
        XCTAssertEqual(presenter.didNextStepCallsCount, 1)
        XCTAssertEqual(presenter.didNextStepActionReceived, .identityNotValidated)
    }
    
    // MARK: - close
    func testClose_ShouldCallDidNextStep() {
        interactor.close()
        XCTAssertEqual(presenter.didNextStepCallsCount, 1)
        XCTAssertEqual(presenter.didNextStepActionReceived, .close)
    }
}
