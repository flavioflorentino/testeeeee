@testable import PIX
import UI
import XCTest

private final class ReceiveWallCoordinatorSpy: ReceiveWallCoordinating {
    var viewController: UIViewController?

    private(set) var performCallsCount = 0
    private(set) var action: ReceiveWallAction?

    func perform(action: ReceiveWallAction) {
        performCallsCount += 1
        self.action = action
    }
}

private final class ReceiveWallViewControllerSpy: ReceiveWallDisplay {
    private(set) var displayViewModelsCallsCount = 0
    private(set) var viewModel: ReceiveWallViewModel?

    func display(viewModel: ReceiveWallViewModel) {
        displayViewModelsCallsCount += 1
        self.viewModel = viewModel
    }
}

final class ReceiveWallPresenterTests: XCTestCase {
    private let coordinatorSpy = ReceiveWallCoordinatorSpy()
    private let viewControllerSpy = ReceiveWallViewControllerSpy()
    private lazy var sut: ReceiveWallPresenting = {
        let presenter = ReceiveWallPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()

    func testPresent_WhenTypeIsNoKeysRegistered_ShouldDisplayWithCorrectViewModel() {
        sut.present(type: .noKeysRegistered)

        XCTAssertEqual(viewControllerSpy.displayViewModelsCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttonTitle, "Cadastrar chave")
        XCTAssertEqual(viewControllerSpy.viewModel?.description, "Para receber pagamentos, você precisa ter uma chave cadastratada com o PicPay.")
        XCTAssertEqual(viewControllerSpy.viewModel?.title, "Cadastre uma chave Pix")
        XCTAssertEqual(viewControllerSpy.viewModel?.image, Assets.registerKey.image)
    }

    func testPresent_WhenTypeIsIdentityNotValidated_ShouldDisplayWithCorrectViewModel() {
        sut.present(type: .identityNotValidated)

        XCTAssertEqual(viewControllerSpy.displayViewModelsCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttonTitle, "Validar Identidade")
        XCTAssertEqual(viewControllerSpy.viewModel?.description, "Para receber pagamentos, você precisa validar sua identidade com o PicPay.")
        XCTAssertEqual(viewControllerSpy.viewModel?.title, "Valide sua identidade")
        XCTAssertEqual(viewControllerSpy.viewModel?.image, Assets.identity.image)
    }

    func testDidNextStep_WhenActionIsIdentityNotValidated_ShouldPerformIdentityNotValidatedAction() {
        sut.didNextStep(action: .identityNotValidated)
        XCTAssertEqual(coordinatorSpy.action, .identityNotValidated)
        XCTAssertEqual(coordinatorSpy.performCallsCount, 1)
    }

    func testDidNextStep_WhenActionIsNoKeysRegistered_ShouldPerformNoKeysRegisteredAction() {
        sut.didNextStep(action: .noKeysRegistered)
        XCTAssertEqual(coordinatorSpy.action, .noKeysRegistered)
        XCTAssertEqual(coordinatorSpy.performCallsCount, 1)
    }
}
