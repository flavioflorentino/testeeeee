import Foundation
import XCTest
@testable import PIX
import AnalyticsModule

private final class CustomizeQRCodeConsumerPresenterSpy: CustomizeQRCodeConsumerPresenting {
    private(set) var presentUserKeysCallCount = 0
    private(set) var presentSelectedKeyCallCount = 0
    private(set) var presentDescriptionTextFieldCallCount = 0
    private(set) var presentIdentifierTextFieldCallCount = 0
    private(set) var deleteDescriptionTextFieldBackwardCallCount = 0
    private(set) var deleteIdentifierTextFieldBackwardCallCount = 0
    private(set) var presentValueTextFieldCallCount = 0
    private(set) var startLoadingCallCount = 0
    private(set) var stopLoadingCallCount = 0
    private(set) var presentErrorCallCount = 0
    private(set) var didNextStepCallCount = 0

    private(set) var keysReceived = [RegisteredKey]()
    private(set) var selectedKeyReceived: RegisteredKey?
    private(set) var currentTextLengthReceived: Int?
    private(set) var maxDescriptionLengthReceived: Int?
    private(set) var valueTextReceived: String?
    private(set) var actionReceived: CustomizeQRCodeConsumerAction?

    var viewController: CustomizeQRCodeConsumerDisplaying?

    func presentUserKeys(keys: [RegisteredKey]) {
        presentUserKeysCallCount += 1
        keysReceived = keys
    }

    func presentSelectedKey(key: RegisteredKey) {
        presentSelectedKeyCallCount += 1
        selectedKeyReceived = key
    }

    func presentDescriptionTextField(currentTextLength: Int, maxDescriptionLength: Int) {
        presentDescriptionTextFieldCallCount += 1
        currentTextLengthReceived = currentTextLength
        maxDescriptionLengthReceived = maxDescriptionLength
    }

    func presentIdentifierTextField(currentTextLength: Int, maxDescriptionLength: Int) {
        presentIdentifierTextFieldCallCount += 1
        currentTextLengthReceived = currentTextLength
        maxDescriptionLengthReceived = maxDescriptionLength
    }

    func deleteDescriptionTextFieldBackward() {
        deleteDescriptionTextFieldBackwardCallCount += 1
    }

    func deleteIdentifierTextFieldBackward() {
        deleteIdentifierTextFieldBackwardCallCount += 1
    }

    func presentValueTextField(with text: String) {
        presentValueTextFieldCallCount += 1
        valueTextReceived = text
    }

    func startLoading() {
        startLoadingCallCount += 1
    }

    func stopLoading() {
        stopLoadingCallCount += 1
    }

    func presentError() {
        presentErrorCallCount += 1
    }

    func didNextStep(action: CustomizeQRCodeConsumerAction) {
        didNextStepCallCount += 1
        actionReceived = action
    }
}

final class CustomizeQRCodeConsumerInteractorTests: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(analyticsSpy)

    private let presenter = CustomizeQRCodeConsumerPresenterSpy()
    private lazy var service = ReceiveServiceMock()
    private let keys = RegisteredKeysMockFactory.make()
    private lazy var selectedKey = keys.first ?? .init(id: "", statusSlug: .processed, status: "", name: "", keyType: .cpf, keyValue: "")
    private let origin = PIXReceiveOrigin.pixHub
    private lazy var interactor = CustomizeQRCodeConsumerInteractor(
        userKeys: keys,
        selectedKey: selectedKey,
        service: service,
        presenter: presenter,
        dependencies: dependencies,
        origin: origin
    )

    func testLoadPixKeys_ShouldPresentInitialData() {
        interactor.loadPixKeys()

        XCTAssertEqual(presenter.presentUserKeysCallCount, 1)
        XCTAssertEqual(presenter.keysReceived, keys)
        XCTAssertEqual(presenter.presentSelectedKeyCallCount, 1)
        XCTAssertEqual(presenter.selectedKeyReceived, selectedKey)
    }

    func testDidSelectKey_ShouldPresentNewSelectedKey() {
        interactor.didSelectKey(viewModel: .init(id: "5", type: .random, value: "189hsahsnd8905"))

        XCTAssertEqual(presenter.presentSelectedKeyCallCount, 1)
        XCTAssertEqual(presenter.selectedKeyReceived, keys.last)
    }

    func testRegisterNewKey_ShouldCallDidNextStep() {
        interactor.registerNewKey()

        XCTAssertEqual(presenter.didNextStepCallCount, 1)
        XCTAssertEqual(presenter.actionReceived, .keyManager)
    }

    func testCheckDescriptionLength_WithValidLength_ShouldPresentDescriptionTextField() {
        let text = "description test"
        interactor.checkDescriptionLength(with: text)

        XCTAssertEqual(presenter.presentDescriptionTextFieldCallCount, 1)
        XCTAssertEqual(presenter.currentTextLengthReceived, text.count)
        XCTAssertEqual(presenter.maxDescriptionLengthReceived, 100)
    }

    func testCheckDescriptionLength_WithInvalidLength_ShouldDeleteBackward() {
        let text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis hendrerit massa erat, et tincidunt nisi sollicitudin non. Integer sed rhoncus ligula. Orci varius natoque penatibus"
        interactor.checkDescriptionLength(with: text)

        XCTAssertEqual(presenter.presentDescriptionTextFieldCallCount, 0)
        XCTAssertEqual(presenter.deleteDescriptionTextFieldBackwardCallCount, 1)
    }

    func testEditIdentifierText_WithValidLength_ShouldPresentDescriptionTextField() {
        let text = "identifier test"
        interactor.editIdentifierText(with: text)

        XCTAssertEqual(presenter.presentIdentifierTextFieldCallCount, 1)
        XCTAssertEqual(presenter.currentTextLengthReceived, text.count)
        XCTAssertEqual(presenter.maxDescriptionLengthReceived, 50)
    }

    func testEditIdentifierText_WithInvalidLength_ShouldDeleteBackward() {
        let text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis hendrerit massa erat, et tincidunt nisi sollicitudin non. Integer sed rhoncus ligula. Orci varius natoque penatibus"
        interactor.editIdentifierText(with: text)

        XCTAssertEqual(presenter.presentIdentifierTextFieldCallCount, 0)
        XCTAssertEqual(presenter.deleteIdentifierTextFieldBackwardCallCount, 1)
    }

    func testEditValueText_ShouldPresentValueTextField() {
        let text = "231,42"
        interactor.editValueText(with: text)

        XCTAssertEqual(presenter.presentValueTextFieldCallCount, 1)
        XCTAssertEqual(presenter.valueTextReceived, "23142")
    }

    func testCreateQRCode_WithFailureInNetworkCall_ShouldPresenterror() {
        service.generateQRCodeResult = .failure(.cancelled)
        interactor.createQRCode(identifier: nil, description: nil, value: nil)

        XCTAssertEqual(presenter.startLoadingCallCount, 1)
        XCTAssertEqual(presenter.stopLoadingCallCount, 1)
        XCTAssertEqual(presenter.presentErrorCallCount, 1)
    }

    func testCreateQRCode_WithSuccessInNetworkCall_WithInvalidBase64_ShouldPresenterror() {
        service.generateQRCodeResult = .success(.init(qrCodeInBase64: "", qrCodeText: ""))
        interactor.createQRCode(identifier: nil, description: nil, value: nil)

        XCTAssertEqual(presenter.startLoadingCallCount, 1)
        XCTAssertEqual(presenter.stopLoadingCallCount, 1)
        XCTAssertEqual(presenter.presentErrorCallCount, 1)
    }

    func testCreateQRCode_WithSuccessInNetworkCall_WithValidBase64_ShouldCallDidNextStep() {
        let image = QRCodeImageMockFactory.base64
        service.generateQRCodeResult = .success(.init(qrCodeInBase64: image, qrCodeText: ""))
        interactor.createQRCode(identifier: nil, description: nil, value: nil)

        XCTAssertEqual(presenter.startLoadingCallCount, 1)
        XCTAssertEqual(presenter.stopLoadingCallCount, 1)
    }

    func testClose_ShouldCallDidNextStepWithClose() {
        interactor.close()
        XCTAssertEqual(presenter.didNextStepCallCount, 1)
        XCTAssertEqual(presenter.actionReceived, .close)
    }
}

