@testable import PIX
import UI
import XCTest

private final class CustomizeQRCodeConsumerCoordinatorSpy: CustomizeQRCodeConsumerCoordinating {
    var viewController: UIViewController?
    var callQrCodeFlowClosure: ((UIImage, RegisteredKey, String) -> Void)?

    private(set) var performCallsCount = 0
    private(set) var action: CustomizeQRCodeConsumerAction?

    func perform(action: CustomizeQRCodeConsumerAction) {
        performCallsCount += 1
        self.action = action
    }
}

private final class CustomizeQRCodeConsumerViewControllerSpy: CustomizeQRCodeConsumerDisplaying {
    private(set) var displaySelectedKeyCallCount = 0
    private(set) var displayUserKeysCallCount = 0
    private(set) var displayDescriptionCountCallCount = 0
    private(set) var displayIdentifierCountCallCount = 0
    private(set) var deleteDescriptionTextFieldBackwardCallCount = 0
    private(set) var displayValueTextFieldCallCount = 0
    private(set) var deleteValueTextFieldBackwardCallCount = 0
    private(set) var deleteIdentifierTextFieldBackwardCallCount = 0
    private(set) var displayLoadingCallCount = 0
    private(set) var stopLoadingCallCount = 0
    private(set) var displayErrorCallCount = 0

    private(set) var selectedKeyReceived: String?
    private(set) var keysReceived = [PIXReceiveKeyViewModel]()
    private(set) var descriptionCountReceived: String?
    private(set) var identifierCountReceived: String?
    private(set) var valueTextFieldReceived: String?

    func displayKeySelected(text: String) {
        displaySelectedKeyCallCount += 1
        selectedKeyReceived = text
    }

    func displayUserKeys(keys: [PIXReceiveKeyViewModel]) {
        displayUserKeysCallCount += 1
        keysReceived = keys
    }

    func displayDescriptionCount(text: String) {
        displayDescriptionCountCallCount += 1
        descriptionCountReceived = text
    }

    func displayIdentifierCount(text: String) {
        displayIdentifierCountCallCount += 1
        identifierCountReceived = text
    }

    func deleteDescriptionTextFieldBackward() {
        deleteDescriptionTextFieldBackwardCallCount += 1
    }

    func displayValueTextField(text: String) {
        displayValueTextFieldCallCount += 1
        valueTextFieldReceived = text
    }

    func deleteValueTextFieldBackward() {
        deleteValueTextFieldBackwardCallCount += 1
    }

    func deleteIdentifierTextFieldBackward() {
        deleteIdentifierTextFieldBackwardCallCount += 1
    }

    func displayLoading() {
        displayLoadingCallCount += 1
    }

    func stopLoading() {
        stopLoadingCallCount += 1
    }

    func displayError() {
        displayErrorCallCount += 1
    }
}

final class CustomizeQRCodeConsumerPresenterTests: XCTestCase {
    private let coordinatorSpy = CustomizeQRCodeConsumerCoordinatorSpy()
    private let viewControllerSpy = CustomizeQRCodeConsumerViewControllerSpy()
    private let keys = RegisteredKeysMockFactory.make()
    private lazy var selectedKey = keys.first ?? .init(id: "", statusSlug: .processed, status: "", name: "", keyType: .cpf, keyValue: "")

    private lazy var presenter: CustomizeQRCodeConsumerPresenting = {
        let presenter = CustomizeQRCodeConsumerPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()

    func testPresentUserKeys_ShouldDisplayUserKeys() {
        presenter.presentUserKeys(keys: keys)

        XCTAssertEqual(viewControllerSpy.displayUserKeysCallCount, 1)
        XCTAssertEqual(viewControllerSpy.keysReceived.count, keys.count)
    }

    func testPresentSelectedKey_ShouldDisplaySelectedKey() {
        presenter.presentSelectedKey(key: selectedKey)

        XCTAssertEqual(viewControllerSpy.displaySelectedKeyCallCount, 1)
        XCTAssertEqual(viewControllerSpy.selectedKeyReceived, "Celular: (61) 98888-8888")
    }

    func testPresentDescriptionTextField_ShouldDisplayDescriptionCount() {
        presenter.presentDescriptionTextField(currentTextLength: 50, maxDescriptionLength: 100)
        XCTAssertEqual(viewControllerSpy.displayDescriptionCountCallCount, 1)
        XCTAssertEqual(viewControllerSpy.descriptionCountReceived, "50 de 100 caracteres")
    }

    func testPresentIdentifierTextField_ShouldDisplayDescriptionCount() {
        presenter.presentIdentifierTextField(currentTextLength: 50, maxDescriptionLength: 100)
        XCTAssertEqual(viewControllerSpy.displayIdentifierCountCallCount, 1)
        XCTAssertEqual(viewControllerSpy.identifierCountReceived, "50 de 100 caracteres")
    }

    func testDeleteDescriptionTextFieldBackward_ShouldDeleteDescriptionTextFieldBackward() {
        presenter.deleteDescriptionTextFieldBackward()
        XCTAssertEqual(viewControllerSpy.deleteDescriptionTextFieldBackwardCallCount, 1)
    }

    func testDeleteIdentifierTextFieldBackward_ShouldDeleteIdentifierTextFieldBackward() {
        presenter.deleteIdentifierTextFieldBackward()
        XCTAssertEqual(viewControllerSpy.deleteIdentifierTextFieldBackwardCallCount, 1)
    }

    func testPresentValueTextField_WithValidText_ShouldDisplayValueTextField() {
        presenter.presentValueTextField(with: "435213")
        XCTAssertEqual(viewControllerSpy.displayValueTextFieldCallCount, 1)
    }

    func testPresentValueTextField_WithInValidText_ShouldDeleteValueTextFieldBackward() {
        presenter.presentValueTextField(with: "124A")
        XCTAssertEqual(viewControllerSpy.displayValueTextFieldCallCount, 0)
        XCTAssertEqual(viewControllerSpy.deleteValueTextFieldBackwardCallCount, 1)
    }

    func testStartLoading_ShouldDisplayLoading() {
        presenter.startLoading()
        XCTAssertEqual(viewControllerSpy.displayLoadingCallCount, 1)
    }

    func testStopLoading_ShouldStopLoading() {
        presenter.stopLoading()
        XCTAssertEqual(viewControllerSpy.stopLoadingCallCount, 1)
    }

    func testPresentError_ShouldDisplayError() {
        presenter.presentError()
        XCTAssertEqual(viewControllerSpy.displayErrorCallCount, 1)
    }

    func testDidNextStep_ShouldPerformAction() throws {
        let image = try XCTUnwrap(QRCodeImageMockFactory.make())
        let qrCodeText = "Test"
        let action = CustomizeQRCodeConsumerAction.qrCode(image: image, selectedKey: selectedKey, qrCodeText: qrCodeText)
        presenter.didNextStep(action: action)

        XCTAssertEqual(coordinatorSpy.performCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.action, action)
    }
}
