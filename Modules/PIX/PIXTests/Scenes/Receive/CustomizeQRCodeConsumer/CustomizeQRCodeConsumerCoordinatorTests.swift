@testable import PIX
import XCTest

final class CustomizeQRCodeConsumerClosureSpy {
    var spyClosureCallCount = 0
    var image: UIImage?
    var key: RegisteredKey?

    func spyClosure(image: UIImage, key: RegisteredKey, qrCodeText: String) -> Void {
        spyClosureCallCount += 1
        self.image = image
        self.key = key
    }
}

final class CustomizeQRCodeConsumerCoordinatorTests: XCTestCase {

    private lazy var viewController = SpyViewController()
    private lazy var navigationController = SpyNavigationController(rootViewController: viewController)
    private let closureSpy = CustomizeQRCodeConsumerClosureSpy()

    private lazy var coordinator: CustomizeQRCodeConsumerCoordinating = {
        let coordinator = CustomizeQRCodeConsumerCoordinator()
        coordinator.viewController = viewController
        coordinator.callQrCodeFlowClosure = closureSpy.spyClosure
        return coordinator
    }()

    func testPerform_WithKeyManagerAction_ShouldPushViewController() {
        XCTAssertEqual(navigationController.pushViewControllerCallCount, 1)
        let action = CustomizeQRCodeConsumerAction.keyManager
        coordinator.perform(action: action)
        XCTAssertEqual(navigationController.pushViewControllerCallCount, 2)
        XCTAssert(navigationController.viewControllers.last is KeyManagerConsumerViewController)
    }

    func testPerform_WithQRCodeAction_ShouldCallQrCodeClosure() throws {
        let image = try XCTUnwrap(QRCodeImageMockFactory.make())
        let qrCodeText = "Test"
        let selectedKey = try XCTUnwrap(RegisteredKeysMockFactory.make().first)
        let action = CustomizeQRCodeConsumerAction.qrCode(image: image, selectedKey: selectedKey, qrCodeText: qrCodeText)
        coordinator.perform(action: action)
        XCTAssertEqual(closureSpy.spyClosureCallCount, 1)
        XCTAssertEqual(closureSpy.image, image)
        XCTAssertEqual(closureSpy.key, selectedKey)
    }

    func testPerform_WithCloseAction_ShouldDismissViewController() throws {
        coordinator.perform(action: .close)
        XCTAssertEqual(viewController.dismissCallCount, 1)
    }
}
