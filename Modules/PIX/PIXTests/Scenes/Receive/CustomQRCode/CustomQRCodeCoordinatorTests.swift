@testable import PIX
import XCTest

final class CustomQRCodeCoordinatorTests: XCTestCase {
    private lazy var viewController = SpyViewController()
    private lazy var navigationController = SpyNavigationController(rootViewController: viewController)

    private lazy var coordinator: CustomQRCodeCoordinating = {
        let coordinator = CustomQRCodeCoordinator()
        coordinator.viewController = viewController
        return coordinator
    }()

    func testPerform_WhenShareQRCodeAction_ShouldPresentViewController() {
        XCTAssertEqual(viewController.presentCallCount, 0)
        let controller = UIActivityViewController(activityItems: [], applicationActivities: nil)
        coordinator.shareQRCode(activityController: controller)
        XCTAssertEqual(viewController.presentCallCount, 1)
    }
}
