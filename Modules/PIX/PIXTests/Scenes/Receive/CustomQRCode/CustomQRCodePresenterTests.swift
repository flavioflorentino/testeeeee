@testable import PIX
import UI
import XCTest

private final class CustomQRCodeCoordinatorSpy: CustomQRCodeCoordinating {
    var viewController: UIViewController?

    private(set) var shareQRCodeCallCount = 0
    private(set) var activityControllerReceived: UIActivityViewController?

    func shareQRCode(activityController: UIActivityViewController) {
        shareQRCodeCallCount += 1
        activityControllerReceived = activityController
    }
}

private final class ReceivePaymentsViewControllerSpy: ReceivePaymentsDisplay {
    private(set) var displayUserKeysCallCount = 0
    private(set) var displayLoadingQRCodeCallCount = 0
    private(set) var displayErrorQRCodeCallCount = 0
    private(set) var displayQRCodeCallCount = 0
    private(set) var displaySelectedKeyCallCount = 0
    private(set) var displayUserNameCallCount = 0
    private(set) var displayRegisterKeyButtonCallCount = 0
    private(set) var displayChangeKeyButtonCallCount = 0
    private(set) var displayTextsCallCount = 0
    private(set) var displaySecondaryButtonCallCount = 0
    private(set) var hideChangeKeyButtonCallCount = 0
    private(set) var displayImageSavedAlertCallCount = 0
    private(set) var displayButtonStyleCallsCount = 0
    private(set) var displayQRCodeContainerCallsCount = 0
    private(set) var displayCustomQRCodeContainerCallsCount = 0
    private(set) var displayCopyPasteContainerCallsCount = 0
    private(set) var displayCopyPasteNotificationCallsCount = 0

    private(set) var formattedKey: String = ""
    private(set) var userNameReceived: String = ""
    private(set) var keysReceived = [PIXReceiveKeyViewModel]()
    private(set) var imageReceived: UIImage?
    private(set) var profileImageURLReceived: URL?
    private(set) var qrCodeText: String?
    private(set) var titleReceived: String?
    private(set) var descriptionReceived: String?
    private(set) var secondaryButtonTypeReceived: ReceivePaymentsViewController.SecondaryButtonAction?
    private(set) var alertTitleReceived: String?
    private(set) var alertDescriptionReceived: String?
    private(set) var backgroundColor: UIColor?
    private(set) var titleColor: UIColor?
    private(set) var index: Int?
    private(set) var message: String?

    func displayUserKeys(keys: [PIXReceiveKeyViewModel]) {
        keysReceived = keys
        displayUserKeysCallCount += 1
    }

    func displayLoadingQRCode() {
        displayLoadingQRCodeCallCount += 1
    }

    func displayErrorQRCode() {
        displayErrorQRCodeCallCount += 1
    }

    func displayQRCode(image: UIImage, profileImageURL: URL?, qrCodeText: String) {
        imageReceived = image
        profileImageURLReceived = profileImageURL
        self.qrCodeText = qrCodeText
        displayQRCodeCallCount += 1
    }

    func displaySelectedKey(key: String) {
        displaySelectedKeyCallCount += 1
        formattedKey = key
    }

    func displayUserName(name: String) {
        displayUserNameCallCount += 1
        userNameReceived = name
    }

    func displayRegisterKeyButton() {
        displayRegisterKeyButtonCallCount += 1
    }

    func displayChangeKeyButton() {
        displayChangeKeyButtonCallCount += 1
    }

    func displayFAQButton() { }

    func displayTexts(title: String, description: String) {
        displayTextsCallCount += 1
        titleReceived = title
        descriptionReceived = description
    }

    func displaySecondaryButton(type: ReceivePaymentsViewController.SecondaryButtonAction) {
        displaySecondaryButtonCallCount += 1
        secondaryButtonTypeReceived = type
    }

    func hideChangeKeyButton() {
        hideChangeKeyButtonCallCount += 1
    }

    func displayImageSavedAlert(title: String, description: String) {
        displayImageSavedAlertCallCount += 1
        alertTitleReceived = title
        alertDescriptionReceived = description
    }
    
    func displayButtonStyle(backgroundColor: UIColor, titleColor: UIColor, index: Int) {
        displayButtonStyleCallsCount += 1
        self.backgroundColor = backgroundColor
        self.titleColor = titleColor
        self.index = index
    }
    
    func displayQRCodeContainer() {
        displayQRCodeContainerCallsCount += 1
    }
    
    func displayCustomQRCodeContainer() {
        displayCustomQRCodeContainerCallsCount += 1
    }
    
    func displayCopyPasteContainer() {
        displayCopyPasteContainerCallsCount += 1
    }
    
    func displayCopyPasteNotification(message: String) {
        displayCopyPasteNotificationCallsCount += 1
        self.message = message
    }
}

final class CustomQRCodePresenterTests: XCTestCase {
    private let coordinatorSpy = CustomQRCodeCoordinatorSpy()
    private let viewControllerSpy = ReceivePaymentsViewControllerSpy()
    private lazy var presenter: CustomQRCodePresenting = {
        let presenter = CustomQRCodePresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()

    func testPresentQRCode_ShouldDisplayQRCode() throws {
        let image = try XCTUnwrap(UIImage(base64: QRCodeImageMockFactory.base64))
        let url = ProfileURLImageMockFactory.make()
        let qrCodeText = "test"
        presenter.presentQRCode(image: image, profileImageURL: url, qrCodeText: qrCodeText)
        XCTAssertEqual(viewControllerSpy.displayQRCodeCallCount, 1)
        XCTAssertNotNil(viewControllerSpy.imageReceived)
        XCTAssertEqual(viewControllerSpy.profileImageURLReceived, url)
    }

    func testPresentSelectedKey_ShouldDisplaySelectedKey() throws {
        let key = try XCTUnwrap(RegisteredKeysMockFactory.make().first)
        presenter.presentSelectedKey(key: key)
        XCTAssertEqual(viewControllerSpy.displaySelectedKeyCallCount, 1)
        XCTAssertEqual(viewControllerSpy.formattedKey, "Celular: (61) 98888-8888")
    }

    func testPresentUserName_ShouldDisplayUserName() {
        presenter.presentUserName(name: "username")
        XCTAssertEqual(viewControllerSpy.displayUserNameCallCount, 1)
        XCTAssertEqual(viewControllerSpy.userNameReceived, "username")
    }

    func testShareQRCode_ShouldCallCoordinator() {
        let controller = UIActivityViewController(activityItems: [], applicationActivities: nil)
        presenter.shareQRCode(controller: controller)

        XCTAssertEqual(coordinatorSpy.shareQRCodeCallCount, 1)
        XCTAssertEqual(coordinatorSpy.activityControllerReceived, controller)
    }

    func testPresentSaveImageButton_ShouldDisplaySaveImageButton() {
        presenter.presentSaveImageButton()
        XCTAssertEqual(viewControllerSpy.displaySecondaryButtonCallCount, 1)
        XCTAssertEqual(viewControllerSpy.secondaryButtonTypeReceived, .saveImage(title: "Salvar imagem"))
    }

    func testPresentTexts_ShouldCallDisplaySecondaryButton() {
        presenter.presentTexts()
        XCTAssertEqual(viewControllerSpy.displayTextsCallCount, 1)
        XCTAssertEqual(viewControllerSpy.titleReceived, "QR Code criado")
        XCTAssertEqual(viewControllerSpy.descriptionReceived, "As informações ficarão visíveis para a pessoa que ler esse QR Code.")
    }

    func testHideChangeKeyButton_ShouldHideChangeKeyButton() {
        presenter.hideChangeKeysButton()
        XCTAssertEqual(viewControllerSpy.hideChangeKeyButtonCallCount, 1)
    }

    func testPresentImageAlert_WithImageSaved_ShouldDisplayImageAlert() {
        presenter.presentImageAlert(didSave: true)
        XCTAssertEqual(viewControllerSpy.displayImageSavedAlertCallCount, 1)
        XCTAssertEqual(viewControllerSpy.alertTitleReceived, "Imagem salva")
        XCTAssertEqual(viewControllerSpy.alertDescriptionReceived, "A imagem foi salva no rolo da câmera")
    }

    func testPresentImageAlert_WithImageNotSaved_ShouldDisplayImageAlert() {
        presenter.presentImageAlert(didSave: false)
        XCTAssertEqual(viewControllerSpy.displayImageSavedAlertCallCount, 1)
        XCTAssertEqual(viewControllerSpy.alertTitleReceived, "Falha ao salvar imagem")
        XCTAssertEqual(viewControllerSpy.alertDescriptionReceived, "Houve um erro ao tentar executar a operação")
    }
    
    func testPresentReceivementSelectionStyle_WhenPerviousTypeIsNil_ShouldCallDisplayButtonStyleOnce() {
        let newType = ReceivementPixType.copyPaste
        presenter.presentReceivementSelectionStyle(newType: newType, previousType: nil)
        XCTAssertEqual(viewControllerSpy.displayButtonStyleCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.index, newType.rawValue)
    }
    
    func testPresentReceivementSelectionStyle_WhenPerviousTypeIsNil_ShouldCallDisplayButtonStyleTwice() {
        let newType = ReceivementPixType.copyPaste
        let previousType = ReceivementPixType.qrcode
        presenter.presentReceivementSelectionStyle(newType: newType, previousType: previousType)
        XCTAssertEqual(viewControllerSpy.displayButtonStyleCallsCount, 2)
        XCTAssertEqual(viewControllerSpy.index, previousType.rawValue)
    }
    
    func testPresentQRCodeContainer_ShouldCallDisplayQRCodeContainer() {
        presenter.presentQRCodeContainer()
        XCTAssertEqual(viewControllerSpy.displayQRCodeContainerCallsCount, 1)
    }
    
    func testPresentCopyPasteContainer_ShouldCallDisplayCopyPasteContainer() {
        presenter.presentCopyPasteContainer()
        XCTAssertEqual(viewControllerSpy.displayCopyPasteContainerCallsCount, 1)
    }
    
    func testPresentCopyPasteNotification_ShouldCallDisplayCopyPasteNotificationWithCopiedCodeMessage() {
        presenter.presentCopyPasteNotification()
        XCTAssertEqual(viewControllerSpy.displayCopyPasteNotificationCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.message, Strings.Receive.ReceivePayments.copiedCode)
    }
}
