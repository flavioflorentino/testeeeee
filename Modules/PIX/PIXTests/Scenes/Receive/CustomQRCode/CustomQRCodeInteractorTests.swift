@testable import PIX
import Foundation
import XCTest
import Core
import AnalyticsModule

private final class CustomQRCodePresenterSpy: CustomQRCodePresenting {
    var viewController: ReceivePaymentsDisplay?

    private(set) var presentQRCodeCallCount = 0
    private(set) var presentSelectedKeyCallCount = 0
    private(set) var presentUserNameCallCount = 0
    private(set) var presentTextsCallCount = 0
    private(set) var presentSaveImageButtonCallCount = 0
    private(set) var shareQRCodeCallCount = 0
    private(set) var hideChangeKeysButtonCallCount = 0
    private(set) var presentImageAlertCallCount = 0
    private(set) var presentReceivementSelectionStyleCallsCount = 0
    private(set) var presentQRCodeContainerCallsCount = 0
    private(set) var presentCopyPasteContainerCallsCount = 0
    private(set) var presentCopyPasteNotificationCallsCount = 0

    private(set) var keys = [RegisteredKey]()
    private(set) var didNextStepActionReceived: ReceivePaymentsAction?
    private(set) var nameReceived: String?
    private(set) var imageReceived: UIImage?
    private(set) var keyReceived: RegisteredKey?
    private(set) var profileImageURLReceived: URL?
    private(set) var qrCodeText: String?
    private(set) var didSaveImageReceived: Bool?
    private(set) var controllerReceived: UIActivityViewController?
    private(set) var newType: ReceivementPixType?
    private(set) var previousType: ReceivementPixType?

    func presentQRCode(image: UIImage, profileImageURL: URL?, qrCodeText: String) {
        imageReceived = image
        profileImageURLReceived = profileImageURL
        self.qrCodeText = qrCodeText
        presentQRCodeCallCount += 1
    }

    func presentSelectedKey(key: RegisteredKey) {
        keyReceived = key
        presentSelectedKeyCallCount += 1
    }

    func presentUserName(name: String) {
        self.nameReceived = name
        presentUserNameCallCount += 1
    }

    func presentSaveImageButton() {
        presentSaveImageButtonCallCount += 1
    }

    func presentTexts() {
        presentTextsCallCount += 1
    }

    func shareQRCode(controller: UIActivityViewController) {
        shareQRCodeCallCount += 1
    }

    func hideChangeKeysButton() {
        hideChangeKeysButtonCallCount += 1
    }

    func presentImageAlert(didSave: Bool) {
        presentImageAlertCallCount += 1
        didSaveImageReceived = didSave
    }
    
    func presentReceivementSelectionStyle(newType: ReceivementPixType, previousType: ReceivementPixType?) {
        self.newType = newType
        self.previousType = previousType
        presentReceivementSelectionStyleCallsCount += 1
    }
    
    func presentQRCodeContainer() {
        presentQRCodeContainerCallsCount += 1
    }
    
    func presentCopyPasteContainer() {
        presentCopyPasteContainerCallsCount += 1
    }
    
    func presentCopyPasteNotification() {
        presentCopyPasteNotificationCallsCount += 1
    }
}

private final class ReceivePixConsumerMock: PixConsumerContract {
    var consumerId: Int { 123 }

    var name: String { "Test" }

    var imageURL: URL? {
        ProfileURLImageMockFactory.make()
    }
}

final class CustomQRCodeInteractorTests: XCTestCase {
    private let presenter = CustomQRCodePresenterSpy()
    private let keys = RegisteredKeysMockFactory.make()
    private let consumerInstance = ReceivePixConsumerMock()
    private let qrCodeImage = QRCodeImageMockFactory.make() ?? .init()
    private let qrCodeText = "Test"
    private lazy var selectedKey = keys.first ?? .init(id: "", statusSlug: .processed, status: "", name: "", keyType: .cpf, keyValue: "")

    private let analyticsSpy = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(analyticsSpy)
    private lazy var interactor = CustomQRCodeInteractor(
        presenter: presenter,
        dependencies: dependencies,
        consumerInstance: consumerInstance,
        selectedKey: selectedKey,
        qrCodeImage: qrCodeImage, qrCodeText: qrCodeText
    )

    func testViewDidLoad_ShouldPresentInfo() {
        interactor.viewDidLoad()

        XCTAssertEqual(presenter.presentUserNameCallCount, 1)
        XCTAssertEqual(presenter.presentTextsCallCount, 1)
        XCTAssertEqual(presenter.presentSaveImageButtonCallCount, 1)
        XCTAssertEqual(presenter.presentSelectedKeyCallCount, 1)
        XCTAssertEqual(presenter.presentQRCodeCallCount, 1)
        XCTAssertEqual(presenter.profileImageURLReceived, consumerInstance.imageURL)
        XCTAssertEqual(presenter.hideChangeKeysButtonCallCount, 1)
        XCTAssertEqual(presenter.presentReceivementSelectionStyleCallsCount, 1)
        XCTAssertEqual(presenter.presentQRCodeContainerCallsCount, 1)
        XCTAssertEqual(presenter.nameReceived, consumerInstance.name)
        XCTAssertEqual(presenter.keyReceived, selectedKey)
        XCTAssertEqual(presenter.imageReceived, qrCodeImage)
        XCTAssertEqual(presenter.qrCodeText, qrCodeText)
        XCTAssertEqual(presenter.newType, .qrcode)
        XCTAssertNil(presenter.previousType)
    }
    
    func testSelectReceivementType_WhenNewTypeIsDifferentToSelectedReceivementType_ShouldCallPresentReceivementSelectionStyle() {
        let newType = ReceivementPixType.copyPaste
        interactor.selectReceivementType(newType)
        XCTAssertEqual(presenter.presentReceivementSelectionStyleCallsCount, 1)
        XCTAssertEqual(presenter.newType, newType)
        XCTAssertEqual(presenter.previousType, .qrcode)
    }
    
    func testSelectReceivementType_WhenNewTypeIsEqualToSelectedReceivementType_ShouldDoNothing() {
        interactor.selectReceivementType(.qrcode)
        XCTAssertEqual(presenter.presentReceivementSelectionStyleCallsCount, 0)
    }
    
    /// Remover qualquer iteração com o simulador
//    func testCopyCode_ShouldHasQrCodeTextInUIPasteboardAndCallPresentCopyPasteNotification() {
//        interactor.copyCode()
//        XCTAssertEqual(UIPasteboard.general.string, qrCodeText)
//        XCTAssertEqual(presenter.presentCopyPasteNotificationCallsCount, 1)
//    }
}
