import Core
@testable import PIX
import XCTest

private final class FeedbackCancelContainerServiceMock: FeedbackCancelContainerServicing {
    var consumerId: Int?
    
    // MARK: - cancelClaim
    private(set) var cancelClaimCallsCount = 0
    var cancelClaimResult: Result<NoContent, ApiError>?
    
    func cancelClaim(keyId: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        guard let cancelClaimResult = cancelClaimResult else {
            XCTFail("Result (cancelClaim) not defined")
            return
        }
        
        cancelClaimCallsCount += 1
        completion(cancelClaimResult)
    }
}

private final class FeedbackCancelContainerPresenterSpy: FeedbackCancelContainerPresenting {
    var viewController: FeedbackCancelContainerDisplaying?
    
    // MARK: - presentFeedback
    private(set) var presentFeedbackCallsCount = 0
    private(set) var presentFeedbackType: FeedbackViewType?
    private(set) var presentFeedbackOrigin: PixUserNavigation?
    
    func presentFeedback(type: FeedbackViewType, from origin: PixUserNavigation) {
        presentFeedbackCallsCount += 1
        presentFeedbackType = type
        presentFeedbackOrigin = origin
    }
    
    // MARK: - didNextStep
    private(set) var didNextStepCallsCount = 0
    private(set) var didNextStepAction: FeedbackCancelContainerAction?

    func didNextStep(action: FeedbackCancelContainerAction) {
        didNextStepCallsCount += 1
        didNextStepAction = action
    }
    
    // MARK: - presentLoaderView
    private(set) var presentLoaderViewCallsCount = 0
    
    func presentLoaderView() {
        presentLoaderViewCallsCount += 1
    }
    
    // MARK: - hideLoaderView
    private(set) var hideLoaderViewCallsCount = 0
    
    func hideLoaderView() {
        hideLoaderViewCallsCount += 1
    }
    
    // MARK: - disableCloseButton
    private(set) var disableCloseButtonCallsCount = 0
    
    func disableCloseButton() {
        disableCloseButtonCallsCount += 1
    }
    
    // MARK: - enableCloseButton
    private(set) var enableCloseButtonCallsCount = 0
    
    func enableCloseButton() {
        enableCloseButtonCallsCount += 1
    }
    
    // MARK: - presentError
    private(set) var presentErrorCallsCount = 0
    private(set) var presentErrorTitle: String?
    private(set) var presentErrorMessage: String?
    private(set) var presentErrorButtonText: String?
    
    func presentError(title: String, message: String, buttonText: String) {
        presentErrorCallsCount += 1
        presentErrorTitle = title
        presentErrorMessage = message
        presentErrorButtonText = buttonText
    }
}

final class FeedbackCancelContainerInteractorTests: XCTestCase {
    private let serviceMock = FeedbackCancelContainerServiceMock()
    private let presenterSpy = FeedbackCancelContainerPresenterSpy()
    
    private func configureSut(with type: FeedbackCancelContainerType, from: PixUserNavigation) -> FeedbackCancelContainerInteracting {
        FeedbackCancelContainerInteractor(
            service: serviceMock,
            presenter: presenterSpy,
            type: type,
            origin: from,
            dependencies: DependencyContainerMock()
        )
    }
    
    func testConfigureContainer_WhenTypeIsClaimKeyCancel_ShouldPresentFeedbackClaimKeyCancel() {
        let sut = configureSut(with: .claimKeyCancel(uuid: "uuid"), from: .keyManagementScreen)
        
        sut.configureContainer()
        
        XCTAssertEqual(presenterSpy.presentFeedbackCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentFeedbackType, .claimKeyCancel(uuid: "uuid"))
        XCTAssertEqual(presenterSpy.presentFeedbackOrigin, .keyManagementScreen)
    }
    
    func testConfigureContainer_WhenTypeIsPortabilityKeyCancel_ShouldPresentFeedbackPortabilityKeyCancel() {
        let sut = configureSut(with: .portabilityKeyCancel(uuid: "uuid"), from: .keyManagementScreen)
        
        sut.configureContainer()
        
        XCTAssertEqual(presenterSpy.presentFeedbackCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentFeedbackType, .portabilityKeyCancel(uuid: "uuid"))
        XCTAssertEqual(presenterSpy.presentFeedbackOrigin, .keyManagementScreen)
    }
    
    func testExecute_WhenActionIsCancelClaim_ShouldCallCancelClaimService() {
        let sut = configureSut(with: .claimKeyCancel(uuid: "uuid"), from: .keyManagementScreen)
        serviceMock.cancelClaimResult = .success(NoContent())
        
        sut.execute(action: .cancelClaim, feedbackType: .claimKeyCancel(uuid: "uuid"))
        
        XCTAssertEqual(serviceMock.cancelClaimCallsCount, 1)
    }
    
    func testExecute_WhenActionIsCancelClaimAndServiceFailure_ShouldPresentError() {
        let sut = configureSut(with: .claimKeyCancel(uuid: "uuid"), from: .keyManagementScreen)
        serviceMock.cancelClaimResult = .failure(ApiError.badRequest(body: RequestError()))
        
        sut.execute(action: .cancelClaim, feedbackType: .claimKeyCancel(uuid: "uuid"))
        
        XCTAssertEqual(serviceMock.cancelClaimCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentLoaderViewCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoaderViewCallsCount, 1)
        XCTAssertEqual(presenterSpy.disableCloseButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.enableCloseButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentErrorTitle, Strings.Feedback.ClaimKeyCancel.title)
        XCTAssertEqual(presenterSpy.presentErrorMessage, Strings.Feedback.ClaimKeyCancel.message)
        XCTAssertEqual(presenterSpy.presentErrorButtonText, Strings.General.tryAgain)
    }
    
    func testExecute_WhenActionIsCancelClaimAndServiceSuccess_ShouldPresentFeedback() {
        let sut = configureSut(with: .claimKeyCancel(uuid: "uuid"), from: .keyManagementScreen)
        serviceMock.cancelClaimResult = .success(NoContent())
        
        sut.execute(action: .cancelClaim, feedbackType: .claimKeyCancel(uuid: "uuid"))
        
        XCTAssertEqual(serviceMock.cancelClaimCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentLoaderViewCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoaderViewCallsCount, 1)
        XCTAssertEqual(presenterSpy.disableCloseButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.enableCloseButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepAction, .feedback(feedbackType: .claimCancellationInProgress, from: .cancelClaimOrder))
    }
    
    func testExecute_WhenActionIsCancelPortability_ShouldCallCancelClaimService() {
        let sut = configureSut(with: .portabilityKeyCancel(uuid: "uuid"), from: .keyManagementScreen)
        serviceMock.cancelClaimResult = .success(NoContent())
        
        sut.execute(action: .cancelPortability, feedbackType: .portabilityKeyCancel(uuid: "uuid"))
        
        XCTAssertEqual(serviceMock.cancelClaimCallsCount, 1)
    }
    
    func testExecute_WhenActionIsCancelPortabilityAndServiceFailure_ShouldPresentError() {
        let sut = configureSut(with: .portabilityKeyCancel(uuid: "uuid"), from: .keyManagementScreen)
        serviceMock.cancelClaimResult = .failure(ApiError.badRequest(body: RequestError()))
        
        sut.execute(action: .cancelPortability, feedbackType: .portabilityKeyCancel(uuid: "uuid"))
        
        XCTAssertEqual(serviceMock.cancelClaimCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentLoaderViewCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoaderViewCallsCount, 1)
        XCTAssertEqual(presenterSpy.disableCloseButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.enableCloseButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentErrorTitle, Strings.Feedback.PortabilityKeyCancel.title)
        XCTAssertEqual(presenterSpy.presentErrorMessage, Strings.Feedback.PortabilityKeyCancel.message)
        XCTAssertEqual(presenterSpy.presentErrorButtonText, Strings.General.tryAgain)
    }
    
    func testExecute_WhenActionIsCancelPortabilityAndServiceSuccess_ShouldPresentFeedback() {
        let sut = configureSut(with: .portabilityKeyCancel(uuid: "uuid"), from: .keyManagementScreen)
        serviceMock.cancelClaimResult = .success(NoContent())
        
        sut.execute(action: .cancelPortability, feedbackType: .portabilityKeyCancel(uuid: "uuid"))
        
        XCTAssertEqual(serviceMock.cancelClaimCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentLoaderViewCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoaderViewCallsCount, 1)
        XCTAssertEqual(presenterSpy.disableCloseButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.enableCloseButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepAction, .feedback(feedbackType: .portabilityCancellationInProgress, from: .cancelPortabilityOrder))
    }
    
    func testExecute_WhenActionIsAny_ShouldCallDidNextStep() {
        let sut = configureSut(with: .claimKeyCancel(uuid: "uuid"), from: .keyManagementScreen)
        serviceMock.cancelClaimResult = .success(NoContent())
        
        sut.execute(action: .ignore, feedbackType: .claimKeyCancel(uuid: "uuid"))
        
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepAction, .close(feedbackType: .claimKeyCancel(uuid: "uuid")))
    }
    
    func testDidClose_ShouldCallDidNextStep() {
        let sut = configureSut(with: .portabilityKeyCancel(uuid: "uuid"), from: .keyManagementScreen)
        
        sut.didClose(feedbackType: .portabilityKeyCancel(uuid: "uuid"))
        
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepAction, .close(feedbackType: .portabilityKeyCancel(uuid: "uuid")))
    }
}
