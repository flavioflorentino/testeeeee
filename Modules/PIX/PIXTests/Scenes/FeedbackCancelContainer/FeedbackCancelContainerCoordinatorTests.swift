@testable import PIX
import XCTest

private final class FeedbackDelegateSpy: FeedbackCancelContainterDelegate {
    // MARK: - didClose
    private(set) var didCloseCallsCount = 0
    private(set) var type: FeedbackViewType?
    
    func didClose(feedbackType: FeedbackViewType) {
        didCloseCallsCount += 1
        type = feedbackType
    }
}

final class FeedbackCancelContainerCoordinatorTests: XCTestCase {
    private lazy var viewControllerSpy: SpyViewController = {
        let controller = SpyViewController()
        self.navigationViewControllerSpy.setViewControllers([controller], animated: false)
        return controller
    }()
    
    private let navigationViewControllerSpy = SpyNavigationController()
    private let delegateSpy = FeedbackDelegateSpy()
    
    private lazy var sut: FeedbackCancelContainerCoordinating = {
        let coordinator = FeedbackCancelContainerCoordinator()
        coordinator.viewController = viewControllerSpy
        coordinator.delegate = delegateSpy
        return coordinator
    }()
    
    func testPerfom_WhenActionIsClose_ShouldDismissViewControllerAndCallDelegate() {
        sut.perform(action: .close(feedbackType: .portabilityCancellationInProgress))
        
        XCTAssertEqual(viewControllerSpy.dismissCallCount, 1)
        XCTAssertEqual(delegateSpy.didCloseCallsCount, 1)
        XCTAssertEqual(delegateSpy.type, .portabilityCancellationInProgress)
    }
    
    func testPerfom_WhenActionIsFeedback_ShouldPushFeedbackViewController() {
        sut.perform(action: .feedback(feedbackType: .portabilityCancellationInProgress, from: .undefined))
        
        XCTAssertEqual(navigationViewControllerSpy.pushViewControllerCallCount, 1)
        XCTAssertTrue(navigationViewControllerSpy.pushedViewController is FeedbackViewController)
    }
    
    func testPerfom_WhenActionIsFeedbackAndDelegateWaCalled_ShouldDismissViewController() {
        sut.perform(action: .feedback(feedbackType: .portabilityCancellationInProgress, from: .undefined))
        (sut as? FeedbackDelegate)?.didAction(action: .close, feedbackType: .portabilityCancellationInProgress)
        
        XCTAssertEqual(viewControllerSpy.dismissCallCount, 1)
        XCTAssertEqual(delegateSpy.didCloseCallsCount, 1)
        XCTAssertEqual(delegateSpy.type, .portabilityCancellationInProgress)
    }
}
