import Foundation
@testable import PIX
import XCTest

private final class FeedbackCancelContainerCoordinatorSpy: FeedbackCancelContainerCoordinating {
    var viewController: UIViewController?
    var delegate: FeedbackCancelContainterDelegate?
    
    // MARK: - perform
    private(set) var performCallsCount = 0
    private(set) var action: FeedbackCancelContainerAction?
    
    func perform(action: FeedbackCancelContainerAction) {
        performCallsCount += 1
        self.action = action
    }
}

private final class FeedbackCancelContainerViewControllerSpy: FeedbackCancelContainerDisplaying {
    // MARK: - displayLoaderView
    private(set) var displayLoaderViewCallsCount = 0
    
    func displayLoaderView() {
        displayLoaderViewCallsCount += 1
    }
    
    // MARK: - hideLoaderView
    private(set) var hideLoaderViewCallsCount = 0
    
    func hideLoaderView() {
        hideLoaderViewCallsCount += 1
    }
    
    // MARK: - disableCloseButton
    private(set) var disableCloseButtonCallsCount = 0
    
    func disableCloseButton() {
        disableCloseButtonCallsCount += 1
    }
    
    // MARK: - enableCloseButton
    private(set) var enableCloseButtonCallsCount = 0
    
    func enableCloseButton() {
        enableCloseButtonCallsCount += 1
    }
    
    // MARK: - enableCloseButton
    private(set) var displayFeedbackViewCallsCount = 0
    private(set) var displayFeedbackType: FeedbackViewType?
    private(set) var displayFeedbackOrigin: PixUserNavigation?
    
    func displayFeedbackView(type: FeedbackViewType, from origin: PixUserNavigation) {
        displayFeedbackViewCallsCount += 1
        displayFeedbackType = type
        displayFeedbackOrigin = origin
    }
    
    // MARK: - displayError
    private(set) var displayErrorViewCallsCount = 0
    private(set) var displayErrorTitle: String?
    private(set) var displayErrorMessage: String?
    private(set) var displayErrorButtonText: String?
    
    func displayError(title: String, message: String, buttonText: String) {
        displayErrorViewCallsCount += 1
        displayErrorTitle = title
        displayErrorMessage = message
        displayErrorButtonText = buttonText
    }
}

final class FeedbackCancelContainerPresenterTests: XCTestCase {
    private let coordinatorSpy = FeedbackCancelContainerCoordinatorSpy()
    private let viewControllerSpy = FeedbackCancelContainerViewControllerSpy()
    
    private lazy var sut: FeedbackCancelContainerPresenting = {
        let presenter = FeedbackCancelContainerPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testPresentFeedback_ShouldDisplayFeedbackViewController() {
        sut.presentFeedback(type: .claimCancellationInProgress, from: .keyManagementScreen)
        
        XCTAssertEqual(viewControllerSpy.displayFeedbackViewCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.displayFeedbackType, .claimCancellationInProgress)
        XCTAssertEqual(viewControllerSpy.displayFeedbackOrigin, .keyManagementScreen)
    }
    
    func testDidNextStep_ShouldPassActionToCoordinator() {
        let action: FeedbackCancelContainerAction = .close(feedbackType: .claimCancellationInProgress)
        sut.didNextStep(action: action)
        
        XCTAssertEqual(coordinatorSpy.performCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.action, action)
    }
    
    func testDisableCloseButton_ShouldDisableCloseButton() {
        sut.disableCloseButton()
        
        XCTAssertEqual(viewControllerSpy.disableCloseButtonCallsCount, 1)
    }
    
    func testEnableCloseButton_ShouldEnableCloseButton() {
        sut.enableCloseButton()
        
        XCTAssertEqual(viewControllerSpy.enableCloseButtonCallsCount, 1)
    }
    
    func testPresentLoaderView_ShouldDisplayLoaderView() {
        sut.presentLoaderView()
        
        XCTAssertEqual(viewControllerSpy.displayLoaderViewCallsCount, 1)
    }
    
    func testHideLoaderView_ShouldHideLoaderView() {
        sut.hideLoaderView()
        
        XCTAssertEqual(viewControllerSpy.hideLoaderViewCallsCount, 1)
    }
    
    func testPresentError_ShouldDisplayError() {
        sut.presentError(title: "title", message: "message", buttonText: "buttonText")
        
        XCTAssertEqual(viewControllerSpy.displayErrorViewCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.displayErrorTitle, "title")
        XCTAssertEqual(viewControllerSpy.displayErrorMessage, "message")
        XCTAssertEqual(viewControllerSpy.displayErrorButtonText, "buttonText")
    }
}

extension FeedbackCancelContainerAction: Equatable {
    public static func == (lhs: FeedbackCancelContainerAction, rhs: FeedbackCancelContainerAction) -> Bool {
        switch (lhs, rhs) {
        case let (.close(feedbackTypeLhs), .close(feedbackTypeRhs)):
            return feedbackTypeLhs == feedbackTypeRhs
        case let (.feedback(feedbackLhs, originLhs), .feedback(feedbackRhs, originRhs)):
            return feedbackLhs == feedbackRhs && originLhs == originRhs
        default:
            return false
        }
    }
}
