import FeatureFlag
@testable import PIX
import UI
import XCTest

private final class KeySelectorCoordinatorSpy: KeySelectorCoordinating {
    var viewController: UIViewController?
    private(set) var performCallsCount = 0
    private(set) var action: KeySelectorAction?
    private(set) var details: PIXPaymentDetails?
    private(set) var origin: PixPaymentOrigin?
    private(set) var goToPaymentDetailsWithPaymentDetailsCallsCount = 0

    func perform(action: KeySelectorAction) {
        performCallsCount += 1
        self.action = action
    }
    
    func goToPaymentDetailsWithPaymentDetails(_ details: PIXPaymentDetails, origin: PixPaymentOrigin) {
        self.details = details
        self.origin = origin
        goToPaymentDetailsWithPaymentDetailsCallsCount += 1
    }
}

private final class KeySelectorViewControllerSpy: KeySelectorDisplay {
    private(set) var displayKeySelectionCallsCount = 0
    private(set) var title: String?
    private(set) var text: String?
    private(set) var keyboardType: UIKeyboardType?
    private(set) var keyboardContentType: UITextContentType?
    private(set) var displayMaskCallsCount = 0
    private(set) var mask: TextMask?
    private(set) var cleanTextFieldContentCallsCount = 0
    private(set) var bindTextFieldNormallyCallsCount = 0
    private(set) var bindTextFieldInvertedCallsCount = 0
    private(set) var displayButtonStyleCallsCount = 0
    private(set) var backgroundColor: UIColor?
    private(set) var titleColor: UIColor?
    private(set) var index: Int?
    private(set) var displayTextFieldStateCallsCount = 0
    private(set) var color: UIColor?
    private(set) var errorMessage: String?
    private(set) var displayForwardButtonStateCallsCount = 0
    private(set) var isEnabled: Bool?
    private(set) var displayDialogCallsCount = 0
    private(set) var alertData: StatusAlertData?
    private(set) var displayEndStateCallsCount = 0
    private(set) var displayTextFieldSelectionCallsCount = 0
    private(set) var isSelected: Bool?
    private(set) var displayErrorTitleSubtitleButtonsCallsCount = 0
    private(set) var errorTitle: String?
    private(set) var subtitle: String?
    private(set) var actions: [PopupAction]?
    private(set) var displayViewTitleCallsCount = 0
    private(set) var viewTitleText: String?
    private(set) var viewTitleColor: UIColor?
    private(set) var displayTextFieldRightViewCallsCount = 0
    private(set) var hideTextFieldRightViewCallsCount = 0
    private(set) var displaySelectedNumberCallsCount = 0
    private(set) var selectedNumber: String?
    private(set) var displayCopiedKeyCallsCount = 0
    private(set) var copiedKey: String?
    
    func displayKeySelection(title: String, text: String, keyboardType: UIKeyboardType, keyboardContentType: UITextContentType?) {
        displayKeySelectionCallsCount += 1
        self.title = title
        self.text = text
        self.keyboardType = keyboardType
        self.keyboardContentType = keyboardContentType
    }
    
    func displayMask(mask: TextMask) {
        displayMaskCallsCount += 1
        self.mask = mask
    }
    
    func cleanTextFieldContent() {
        cleanTextFieldContentCallsCount += 1
    }
    
    func bindTextFieldNormally() {
        bindTextFieldNormallyCallsCount += 1
    }
    
    func bindTextFieldInverted() {
        bindTextFieldInvertedCallsCount += 1
    }
    
    func displayButtonStyle(backgroundColor: UIColor, titleColor: UIColor, index: Int) {
        displayButtonStyleCallsCount += 1
        self.backgroundColor = backgroundColor
        self.titleColor = titleColor
        self.index = index
    }
    
    func displayTextFieldState(color: UIColor, errorMessage: String?) {
        displayTextFieldStateCallsCount += 1
        self.color = color
        self.errorMessage = errorMessage
    }
    
    func displayForwardButtonState(isEnabled: Bool) {
        displayForwardButtonStateCallsCount += 1
        self.isEnabled = isEnabled
    }
    
    func displayDialog(with alertData: StatusAlertData) {
        displayDialogCallsCount += 1
        self.alertData = alertData
    }
    
    func displayEndState() {
        displayEndStateCallsCount += 1
    }
    
    func displayTextFieldSelection(isSelected: Bool) {
        displayTextFieldSelectionCallsCount += 1
        self.isSelected = isSelected
    }

    func displayError(title: String, subtitle: String, actions: [PopupAction]) {
        displayErrorTitleSubtitleButtonsCallsCount += 1
        self.errorTitle = title
        self.subtitle = subtitle
        self.actions = actions
    }
    
    func displayViewTitle(text: String, color: UIColor) {
        displayViewTitleCallsCount += 1
        self.viewTitleText = text
        self.viewTitleColor = color
    }
    
    func displayTextFieldRightView() {
        displayTextFieldRightViewCallsCount += 1
    }
    
    func hideTextFieldRightView() {
        hideTextFieldRightViewCallsCount += 1
    }
    
    func displaySelectedNumber(_ number: String) {
        displaySelectedNumberCallsCount += 1
        selectedNumber = number
    }
    
    func displayCopiedKey(key: String) {
        displayCopiedKeyCallsCount += 1
        copiedKey = key
    }
}

final class KeySelectorPresenterTests: XCTestCase {
    // MARK: - Variables
    private let coordinator = KeySelectorCoordinatorSpy()
    private let viewController = KeySelectorViewControllerSpy()
    private let featureManager: FeatureManagerMock = {
        let manager = FeatureManagerMock()
        manager.override(key: .featurePixkeyPhoneWithDDI, with: false)
        return manager
    }()
    private lazy var presenter: KeySelectorPresenter = {
        let presenter = KeySelectorPresenter(coordinator: coordinator, dependencies: DependencyContainerMock(featureManager))
        presenter.viewController = viewController
        return presenter
    }()
    
    // MARK: - didNextStep
    func testDidNextStep_WhenSelectedCloseAction_ShouldCallPerformWithCloseAction() {
        let action = KeySelectorAction.close
        presenter.didNextStep(action: action)
        XCTAssertEqual(coordinator.performCallsCount, 1)
        XCTAssertEqual(coordinator.action, action)
    }
    
    func testDidNextStep_WhenSelectedAgendaAction_ShouldCallPerformWithAgendaAction() {
        let action = KeySelectorAction.agenda { _ in }
        presenter.didNextStep(action: action)
        XCTAssertEqual(coordinator.performCallsCount, 1)
        XCTAssertEqual(coordinator.action, action)
    }
    
    // MARK: - presentPaymentDetailsWithAccount
    func testPresentPaymentDetailsWithAccount_ShouldCallGoToPaymentDetailsWithPaymentDetails() throws {
        let account = PixAccount(
            name: "Teste",
            cpfCnpj: "123",
            bank: "Bank",
            agency: "",
            accountNumber: "",
            imageUrl: nil,
            id: nil
        )
        let keyType = KeyType.cnpj
        let origin = PixPaymentOrigin.cnpj
        let key = "22.896.431/0001-10"
        let payee = PIXPayee(name: account.name, description: "\(account.cpfCnpj) | \(account.bank)", imageURLString: nil, id: nil)
        let params = PIXParams(originType: .key, keyType: keyType.serviceValue, key: key, receiverData: nil)
        let details = PIXPaymentDetails(
            title: "Enviar pagamento",
            screenType: .default(commentPlaceholder: "Descrição (opcional)"),
            payee: payee,
            pixParams: params
        )

        presenter.presentPaymentDetailsWithAccount(account, keyType: keyType, key: key)

        let fakeDetails = try XCTUnwrap(coordinator.details)
        let fakeOrigin = try XCTUnwrap(coordinator.origin)
        XCTAssertEqual(fakeDetails, details)
        XCTAssertEqual(fakeOrigin, origin)
        XCTAssertEqual(viewController.displayEndStateCallsCount, 1)
        XCTAssertEqual(coordinator.goToPaymentDetailsWithPaymentDetailsCallsCount, 1)
    }
    
    // MARK: - presentKeySelectionStyle
    func testPresentKeySelectionStyle_WhenNewTypeIsCPFAndPreviousTypeIsNil_ShouldCallDisplayKeySelectionAndDisplayButtonStyleAndHasCPFInfo() throws {
        let newType = KeyType.cpf
        presenter.presentKeySelectionStyle(newType: newType, previousType: nil)
        XCTAssertEqual(viewController.displayKeySelectionCallsCount, 1)
        XCTAssertEqual(viewController.displayButtonStyleCallsCount, 1)
        XCTAssertEqual(viewController.title, Strings.KeySelection.cpfCnpjTitle)
        XCTAssertEqual(viewController.text, String())
        XCTAssertEqual(viewController.keyboardType, .numberPad)
        XCTAssertNil(viewController.keyboardContentType)
    }
    
    func testPresentKeySelectionStyle_WhenNewTypeIsCPFAndPreviousTypeIsNotNil_ShouldCallDisplayKeySelectionAndDisplayButtonStyleTwiceAndHasCPFInfo() throws {
        let newType = KeyType.cpf
        let previousType = KeyType.cnpj
        presenter.presentKeySelectionStyle(newType: newType, previousType: previousType)
        XCTAssertEqual(viewController.displayKeySelectionCallsCount, 1)
        XCTAssertEqual(viewController.displayButtonStyleCallsCount, 2)
        XCTAssertEqual(viewController.title, Strings.KeySelection.cpfCnpjTitle)
        XCTAssertEqual(viewController.text, String())
        XCTAssertEqual(viewController.keyboardType, .numberPad)
        XCTAssertNil(viewController.keyboardContentType)
    }
    
    func testPresentKeySelectionStyle_WhenNewTypeIsCNPJAndPreviousTypeIsNil_ShouldCallDisplayKeySelectionAndDisplayButtonStyleAndHasCNPJInfo() throws {
        let newType = KeyType.cnpj
        presenter.presentKeySelectionStyle(newType: newType, previousType: nil)
        XCTAssertEqual(viewController.displayKeySelectionCallsCount, 1)
        XCTAssertEqual(viewController.displayButtonStyleCallsCount, 1)
        XCTAssertEqual(viewController.title, Strings.KeySelection.cpfCnpjTitle)
        XCTAssertEqual(viewController.text, String())
        XCTAssertEqual(viewController.keyboardType, .numberPad)
        XCTAssertNil(viewController.keyboardContentType)
    }
    
    func testPresentKeySelectionStyle_WhenNewTypeIsCNPJAndPreviousTypeIsNotNil_ShouldCallDisplayKeySelectionAndDisplayButtonStyleTwiceAndHasCNPJInfo() throws {
        let newType = KeyType.cnpj
        let previousType = KeyType.cpf
        presenter.presentKeySelectionStyle(newType: newType, previousType: previousType)
        XCTAssertEqual(viewController.displayKeySelectionCallsCount, 1)
        XCTAssertEqual(viewController.displayButtonStyleCallsCount, 2)
        XCTAssertEqual(viewController.title, Strings.KeySelection.cpfCnpjTitle)
        XCTAssertEqual(viewController.text, String())
        XCTAssertEqual(viewController.keyboardType, .numberPad)
        XCTAssertNil(viewController.keyboardContentType)
    }
    
    func testPresentKeySelectionStyle_WhenNewTypeIsPhoneAndPreviousTypeIsNilAndDDIFlagIsOff_ShouldCallDisplayKeySelectionAndDisplayButtonStyleAndHasPhoneInfo() throws {
        let newType = KeyType.phone
        presenter.presentKeySelectionStyle(newType: newType, previousType: nil)
        XCTAssertEqual(viewController.displayKeySelectionCallsCount, 1)
        XCTAssertEqual(viewController.displayButtonStyleCallsCount, 1)
        XCTAssertEqual(viewController.title, Strings.KeySelection.cellphoneTitle)
        XCTAssertEqual(viewController.keyboardType, .numberPad)
        XCTAssertEqual(viewController.keyboardContentType, .telephoneNumber)
    }

    func testPresentKeySelectionStyle_WhenNewTypeIsPhoneAndPreviousTypeIsNilAndDDIFlagIsOn_ShouldHave55InTextAndTypePhonePad() throws {
        featureManager.override(key: .featurePixkeyPhoneWithDDI, with: true)
        let newType = KeyType.phone
        presenter.presentKeySelectionStyle(newType: newType, previousType: nil)
        XCTAssertEqual(viewController.text, "+55")
        XCTAssertEqual(viewController.keyboardType, .phonePad)
        XCTAssertEqual(viewController.keyboardContentType, .telephoneNumber)
    }
    
    func testPresentKeySelectionStyle_WhenNewTypeIsPhoneAndPreviousTypeIsNotNil_ShouldCallDisplayKeySelectionAndDisplayButtonStyleTwiceAndHasPhoneInfo() throws {
        let newType = KeyType.phone
        let previousType = KeyType.cpf
        presenter.presentKeySelectionStyle(newType: newType, previousType: previousType)
        XCTAssertEqual(viewController.displayKeySelectionCallsCount, 1)
        XCTAssertEqual(viewController.displayButtonStyleCallsCount, 2)
        XCTAssertEqual(viewController.title, Strings.KeySelection.cellphoneTitle)
        XCTAssertEqual(viewController.text, String())
        XCTAssertEqual(viewController.keyboardType, .numberPad)
        XCTAssertEqual(viewController.keyboardContentType, .telephoneNumber)
    }
    
    func testPresentKeySelectionStyle_WhenNewTypeIsEmailAndPreviousTypeIsNil_ShouldCallDisplayKeySelectionAndDisplayButtonStyleAndHasEmailInfo() throws {
        let newType = KeyType.email
        presenter.presentKeySelectionStyle(newType: newType, previousType: nil)
        XCTAssertEqual(viewController.displayKeySelectionCallsCount, 1)
        XCTAssertEqual(viewController.displayButtonStyleCallsCount, 1)
        XCTAssertEqual(viewController.title, Strings.KeySelection.emailTitle)
        XCTAssertEqual(viewController.keyboardType, .emailAddress)
        XCTAssertEqual(viewController.keyboardContentType, .emailAddress)
    }
    
    func testPresentKeySelectionStyle_WhenNewTypeIsEmailAndPreviousTypeIsNotNil_ShouldCallDisplayKeySelectionAndDisplayButtonStyleTwiceAndHasEmailInfo() throws {
        let newType = KeyType.email
        let previousType = KeyType.cpf
        presenter.presentKeySelectionStyle(newType: newType, previousType: previousType)
        XCTAssertEqual(viewController.displayKeySelectionCallsCount, 1)
        XCTAssertEqual(viewController.displayButtonStyleCallsCount, 2)
        XCTAssertEqual(viewController.title, Strings.KeySelection.emailTitle)
        XCTAssertEqual(viewController.keyboardType, .emailAddress)
        XCTAssertEqual(viewController.keyboardContentType, .emailAddress)
    }
    
    func testPresentKeySelectionStyle_WhenNewTypeIsRandomAndPreviousTypeIsNil_ShouldCallDisplayKeySelectionAndDisplayButtonStyleAndHasRandomInfo() throws {
        let newType = KeyType.random
        presenter.presentKeySelectionStyle(newType: newType, previousType: nil)
        XCTAssertEqual(viewController.displayKeySelectionCallsCount, 1)
        XCTAssertEqual(viewController.displayButtonStyleCallsCount, 1)
        XCTAssertEqual(viewController.title, Strings.KeySelection.randomKeyTitle)
        XCTAssertEqual(viewController.keyboardType, .default)
        XCTAssertNil(viewController.keyboardContentType)
    }
    
    func testPresentKeySelectionStyle_WhenNewTypeIsRandomAndPreviousTypeIsNotNil_ShouldCallDisplayKeySelectionAndDisplayButtonStyleTwiceAndHasRandomInfo() throws {
        let newType = KeyType.random
        let previousType = KeyType.cpf
        presenter.presentKeySelectionStyle(newType: newType, previousType: previousType)
        XCTAssertEqual(viewController.displayKeySelectionCallsCount, 1)
        XCTAssertEqual(viewController.displayButtonStyleCallsCount, 2)
        XCTAssertEqual(viewController.title, Strings.KeySelection.randomKeyTitle)
        XCTAssertEqual(viewController.keyboardType, .default)
        XCTAssertNil(viewController.keyboardContentType)
    }
    
    // MARK: - presentMask
    func testPresentMask_WhenKeyTypeIsCPF_ShouldCallDisplayMaskWithCPFMask() throws {
        presenter.presentMask(keyType: .cpf)
        let textMask = try XCTUnwrap(viewController.mask as? CustomStringMask)
        XCTAssertEqual(textMask.mask, PersonalDocumentMaskDescriptor.cpf.format)
        XCTAssertEqual(viewController.displayMaskCallsCount, 1)
    }
    
    func testPresentMask_WhenKeyTypeIsCNPJ_ShouldCallDisplayMaskWithCNPJMask() throws {
        presenter.presentMask(keyType: .cnpj)
        let textMask = try XCTUnwrap(viewController.mask as? CustomStringMask)
        XCTAssertEqual(textMask.mask, PersonalDocumentMaskDescriptor.cnpj.format)
        XCTAssertEqual(viewController.displayMaskCallsCount, 1)
    }
    
    func testPresentMask_WhenKeyTypeIsPhoneAndDDIIsOff_ShouldCallDisplayMaskWithCellphoneMask() throws {
        presenter.presentMask(keyType: .phone)
        let textMask = try XCTUnwrap(viewController.mask as? CustomStringMask)
        XCTAssertEqual(textMask.mask, PersonalDocumentMaskDescriptor.cellPhoneWithDDD.format)
        XCTAssertEqual(viewController.displayMaskCallsCount, 1)
    }

    func testPresentMask_WhenKeyTypeIsPhoneAndDDIIsOn_ShouldCallDisplayMaskWithCellphoneMaskWithDDI() throws {
        featureManager.override(key: .featurePixkeyPhoneWithDDI, with: true)
        presenter.presentMask(keyType: .phone)
        let textMask = try XCTUnwrap(viewController.mask as? CustomStringMask)
        XCTAssertEqual(textMask.mask, PersonalDocumentMaskDescriptor.cellPhoneWithDDDAndDDI.format)
        XCTAssertEqual(viewController.displayMaskCallsCount, 1)
    }
    
    func testPresentMask_WhenKeyTypeIsEmail_ShouldCallDisplayMaskWithEmailMask() throws {
        presenter.presentMask(keyType: .email)
        let textMask = try XCTUnwrap(viewController.mask as? CharacterLimitMask)
        XCTAssertEqual(textMask.maximumNumberOfCharacters, 77)
        XCTAssertEqual(viewController.displayMaskCallsCount, 1)
    }
    
    func testPresentMask_WhenKeyTypeIsRandom_ShouldCallDisplayMaskWithRandomKeyMask() throws {
        presenter.presentMask(keyType: .random)
        let textMask = try XCTUnwrap(viewController.mask as? CustomStringMask)
        XCTAssertEqual(textMask.mask, PixMaskDescriptor.randomKey.format)
        XCTAssertEqual(viewController.displayMaskCallsCount, 1)
    }

    // MARK: - presentTextFieldBindAndContent
    func testPresentTextFieldBindAndContent_WhenNewTypeIsCpfAndPreviousTypeIsDifferentToCnpj_ShouldCallCleanTextFieldContentAndBindTextFieldInverted() {
        presenter.presentTextFieldBindAndContent(newType: .cpf, previousType: .phone)
        XCTAssertEqual(viewController.cleanTextFieldContentCallsCount, 1)
        XCTAssertEqual(viewController.bindTextFieldInvertedCallsCount, 1)
    }
    
    func testPresentTextFieldBindAndContent_WhenNewTypeIsCpfAndPreviousTypeIsEqualToCnpj_ShouldCallBindTextFieldInvertedAndNotCallCleanTextFieldContent() {
        presenter.presentTextFieldBindAndContent(newType: .cpf, previousType: .cnpj)
        XCTAssertEqual(viewController.cleanTextFieldContentCallsCount, 0)
        XCTAssertEqual(viewController.bindTextFieldInvertedCallsCount, 1)
    }
    
    func testPresentTextFieldBindAndContent_WhenNewTypeIsCpfAndPreviousTypeIsNil_ShouldCallBindTextFieldInvertedAndNotCallCleanTextFieldContent() {
        presenter.presentTextFieldBindAndContent(newType: .cpf, previousType: nil)
        XCTAssertEqual(viewController.cleanTextFieldContentCallsCount, 0)
        XCTAssertEqual(viewController.bindTextFieldInvertedCallsCount, 1)
    }
    
    func testPresentTextFieldBindAndContent_WhenNewTypeIsCnpj_ShouldCallBindTextFieldNormally() {
        presenter.presentTextFieldBindAndContent(newType: .cnpj, previousType: nil)
        XCTAssertEqual(viewController.bindTextFieldNormallyCallsCount, 1)
    }
    
    func testPresentTextFieldBindAndContent_WhenNewTypeIsPhone_ShouldCallCleanTextFieldContentAndBindTextFieldNormally() {
        presenter.presentTextFieldBindAndContent(newType: .phone, previousType: nil)
        XCTAssertEqual(viewController.cleanTextFieldContentCallsCount, 1)
        XCTAssertEqual(viewController.bindTextFieldNormallyCallsCount, 1)
    }
    
    func testPresentTextFieldBindAndContent_WhenNewTypeIsEmail_ShouldCallCleanTextFieldContentAndBindTextFieldNormally() {
        presenter.presentTextFieldBindAndContent(newType: .email, previousType: nil)
        XCTAssertEqual(viewController.cleanTextFieldContentCallsCount, 1)
        XCTAssertEqual(viewController.bindTextFieldNormallyCallsCount, 1)
    }
    
    func testPresentTextFieldBindAndContent_WhenNewTypeIsRandom_ShouldCallCleanTextFieldContentAndBindTextFieldNormally() {
        presenter.presentTextFieldBindAndContent(newType: .random, previousType: nil)
        XCTAssertEqual(viewController.cleanTextFieldContentCallsCount, 1)
        XCTAssertEqual(viewController.bindTextFieldNormallyCallsCount, 1)
    }
    
    // MARK: - presentTextFieldNormalState
    func testPresentTextFieldNormalState_ShouldCallDisplayTextFieldStateWithoutErrorMessage() {
        presenter.presentTextFieldNormalState()
        XCTAssertEqual(viewController.displayTextFieldStateCallsCount, 1)
        XCTAssertNotNil(viewController.color)
        XCTAssertNil(viewController.errorMessage)
    }
    
    // MARK: - presentTextFieldFormatError
    func testPresentTextFieldFormatError_ShouldCallDisplayTextFieldStateWithErrorMessage() {
        presenter.presentTextFieldFormatError()
        XCTAssertEqual(viewController.displayTextFieldStateCallsCount, 1)
        XCTAssertNotNil(viewController.color)
        XCTAssertEqual(viewController.errorMessage, Strings.KeySelection.formatErrorMessage)
    }
    
    // MARK: - presentTextFieldPhoneFormatError
    func testPresentTextFieldPhoneFormatError_ShouldCallDisplayTextFieldStateWithPhoneErrorMessage() {
        presenter.presentTextFieldPhoneFormatError()
        XCTAssertEqual(viewController.displayTextFieldStateCallsCount, 1)
        XCTAssertNotNil(viewController.color)
        XCTAssertEqual(viewController.errorMessage, Strings.KeySelection.phoneFormatErrorMessage)
    }
    
    // MARK: - presentTextFieldNonexistentError
    func testPresentTextFieldNonexistentError_ShouldCallDisplayTextFieldStateWithErrorMessage() {
        presenter.presentTextFieldNonexistentError()
        XCTAssertEqual(viewController.displayTextFieldStateCallsCount, 1)
        XCTAssertNotNil(viewController.color)
        XCTAssertEqual(viewController.errorMessage, Strings.KeySelection.nonexistentErrorMessage)
    }
    
    // MARK: - presentTextFieldServiceError
    func testPresentTextFieldServiceError_ShouldCallDisplayTextFieldStateWithErrorMessageAndDisplayEndState() {
        let message = "Teste"
        presenter.presentTextFieldServiceError(message: message)
        XCTAssertEqual(viewController.displayTextFieldStateCallsCount, 1)
        XCTAssertEqual(viewController.displayEndStateCallsCount, 1)
        XCTAssertNotNil(viewController.color)
        XCTAssertEqual(viewController.errorMessage, message)
    }
    
    // MARK: - presentGenericError
    func testPresentGenericError_ShouldCallDisplayDialogWithAlertDataAndDisplayEndState() {
        presenter.presentGenericError()
        XCTAssertEqual(viewController.displayDialogCallsCount, 1)
        XCTAssertEqual(viewController.displayEndStateCallsCount, 1)
        XCTAssertNotNil(viewController.alertData)
    }
    
    // MARK: - presentForwardButtonState
    func testPresentForwardButtonState_WhenIsEnabledIsTrue_ShouldCallDisplayForwardButtonStateAndIsEnabledEqualAsReceived() {
        let state = true
        presenter.presentForwardButtonState(isEnabled: state)
        XCTAssertEqual(viewController.displayForwardButtonStateCallsCount, 1)
        XCTAssertEqual(viewController.isEnabled, state)
    }
    
    func testPresentForwardButtonState_WhenIsEnabledIsFalse_ShouldCallDisplayForwardButtonStateAndIsEnabledEqualAsReceived() {
        let state = false
        presenter.presentForwardButtonState(isEnabled: state)
        XCTAssertEqual(viewController.displayForwardButtonStateCallsCount, 1)
        XCTAssertEqual(viewController.isEnabled, state)
    }
    
    // MARK: - presentTextFieldSelection
    func testPresentTextFieldSelection_ShouldCallDisplayTextFieldSelectionWithGivenIsSelected() {
        let isSelected = true
        presenter.presentTextFieldSelection(isSelected: isSelected)
        XCTAssertEqual(viewController.displayTextFieldSelectionCallsCount, 1)
        XCTAssertEqual(viewController.isSelected, isSelected)
    }

    //MARK: - presentErrortTitleSubtitleJsonButtons
    func testPresentErrorTitleSubtitleJsonButtons_ShouldCallDisplayErrorTitleSubtitleActions() {
        let title = "Title"
        let subtitle = "Subtitle"
        let actionTitle = "ActionTitle"
        let jsonButtons = [["title": actionTitle]]
        presenter.presentError(title: title, subtitle: subtitle, jsonButtons: jsonButtons)

        XCTAssertEqual(viewController.displayErrorTitleSubtitleButtonsCallsCount, 1)
        XCTAssertEqual(viewController.errorTitle, title)
        XCTAssertEqual(viewController.subtitle, subtitle)
        XCTAssertEqual(viewController.actions?.first?.currentTitle, actionTitle)
    }
    
    // MARK: - presentTextFieldRightView
    func testPresentTextFieldRightView_WhenKeytypeIsPhone_ShouldCallDisplayTextFieldRightView() {
        let newKeyType = KeyType.phone
        presenter.presentTextFieldRightView(keyType: newKeyType)
        XCTAssertEqual(viewController.displayTextFieldRightViewCallsCount, 1)
    }
    
    func testPresentTextFieldRightView_WhenKeytypeIsNotPhone_ShouldCallHideTextFieldRightView() {
        let newKeyType = KeyType.cpf
        presenter.presentTextFieldRightView(keyType: newKeyType)
        XCTAssertEqual(viewController.hideTextFieldRightViewCallsCount, 1)
    }
    
    // MARK: - presentSelectedNumber
    func testPresentSelectedNumber_ShouldCallDisplaySelectedNumberWithGivenNumber() {
        let phoneNumber = "123456"
        presenter.presentSelectedNumber(phoneNumber)
        XCTAssertEqual(viewController.displaySelectedNumberCallsCount, 1)
        XCTAssertEqual(viewController.selectedNumber, phoneNumber)
    }
    
    // MARK: - presentCopiedKey
    func testPresentCopiedKey_ShouldDisplayCopiedKey() {
        let key = "12345678"
        presenter.presentCopiedKey(key: key)
        XCTAssertEqual(viewController.displayCopiedKeyCallsCount, 1)
        XCTAssertEqual(viewController.copiedKey, key)
    }
}
