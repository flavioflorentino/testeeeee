import XCTest
@testable import PIX

final class KeySelectorCoordinatorTests: XCTestCase {
    private lazy var viewController: SpyViewController = {
        let controller = SpyViewController()
        self.navigationController.pushViewController(controller, animated: false)
        return controller
    }()
    private let navigationController = SpyNavigationController()
    private let paymentOpening = FakePaymentOpening()
    private lazy var coordinator: KeySelectorCoordinator = {
        let coordinator = KeySelectorCoordinator(paymentOpening: paymentOpening)
        coordinator.viewController = viewController
        return coordinator

    }()
    
    // MARK: - goToPaymentDetailsWithPaymentDetails
    func testGoToPaymentDetailsWithPaymentDetails_ShouldCallOpenPaymentWithDetails() throws {
        let payee = PIXPayee(name: "name", description: "description", imageURLString: nil, id: nil)
        let params = PIXParams(originType: .key, keyType: KeyType.phone.serviceValue, key: nil, receiverData: nil)
        let details = PIXPaymentDetails(title: "title", screenType: .default(commentPlaceholder: "placeholder"), payee: payee, pixParams: params)
        coordinator.goToPaymentDetailsWithPaymentDetails(details, origin: PixPaymentOrigin.phone)
        XCTAssertEqual(paymentOpening.details, details)
        XCTAssert(paymentOpening.controller === viewController)
        XCTAssertEqual(paymentOpening.openPaymentWithDetailsCallsCount, 1)
    }
}
