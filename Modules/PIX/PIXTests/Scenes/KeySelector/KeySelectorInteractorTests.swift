import AnalyticsModule
import Core
import FeatureFlag
@testable import PIX
import XCTest

private final class KeySelectorServiceSpy: KeySelectorServicing {
    var result: Result<PixAccount, ApiError> = .failure(.unknown(nil))
    private(set) var validateCallsCount = 0
    private(set) var key: String?
    private(set) var type: KeyType?

    func validate(key: String, type: KeyType, completion: @escaping (Result<PixAccount, ApiError>) -> Void) {
        validateCallsCount += 1
        self.key = key
        self.type = type
        completion(result)
    }
}

private final class KeySelectorPresenterSpy: KeySelectorPresenting {
    var viewController: KeySelectorDisplay?
    private(set) var didNextStepCallsCount = 0
    private(set) var action: KeySelectorAction?
    private(set) var presentPaymentDetailsWithAccountCallsCount = 0
    private(set) var account: PixAccount?
    private(set) var keyType: KeyType?
    private(set) var key: String?
    private(set) var presentKeySelectionStyleCallsCount = 0
    private(set) var selectionStyleNewType: KeyType?
    private(set) var selectionStylePreviousType: KeyType?
    private(set) var presentMaskCallsCount = 0
    private(set) var maskKeyType: KeyType?
    private(set) var presentTextFieldBindAndContentCallsCount = 0
    private(set) var bindNewType: KeyType?
    private(set) var bindPreviousType: KeyType?
    private(set) var presentTextFieldNormalStateCallsCount = 0
    private(set) var presentTextFieldFormatErrorCallsCount = 0
    private(set) var presentTextFieldPhoneFormatErrorCallsCount = 0
    private(set) var presentTextFieldNonexistentErrorCallsCount = 0
    private(set) var presentTextFieldServiceErrorCallsCount = 0
    private(set) var message: String?
    private(set) var presentGenericErrorCallsCount = 0
    private(set) var presentForwardButtonStateCallsCount = 0
    private(set) var buttonIsEnabled: Bool?
    private(set) var presentTextFieldSelectionCallsCount = 0
    private(set) var isSelected: Bool?
    private(set) var presentErrorTitleSubtitleJsonButtonsCallsCount = 0
    private(set) var title: String?
    private(set) var subtitle: String?
    private(set) var cleanTextFieldContentCallsCount = 0
    private(set) var presentTextFieldRightViewCallsCount = 0
    private(set) var agendaKeyType: KeyType?
    private(set) var presentSelectedNumberCallsCount = 0
    private(set) var selectedNumber: String?
    private(set) var presentCopiedKeyCallsCount = 0
    private(set) var copiedKey: String?

    func didNextStep(action: KeySelectorAction) {
        didNextStepCallsCount += 1
        self.action = action
    }
    
    func presentPaymentDetailsWithAccount(_ account: PixAccount, keyType: KeyType, key: String) {
        presentPaymentDetailsWithAccountCallsCount += 1
        self.account = account
        self.keyType = keyType
        self.key = key
    }
    
    func presentKeySelectionStyle(newType: KeyType, previousType: KeyType?) {
        presentKeySelectionStyleCallsCount += 1
        selectionStyleNewType = newType
        selectionStylePreviousType = previousType
    }
    
    func presentMask(keyType: KeyType) {
        presentMaskCallsCount += 1
        maskKeyType = keyType
    }
    
    func presentTextFieldBindAndContent(newType: KeyType, previousType: KeyType?) {
        presentTextFieldBindAndContentCallsCount += 1
        bindNewType = newType
        bindPreviousType = previousType
    }
    
    func presentTextFieldNormalState() {
        presentTextFieldNormalStateCallsCount += 1
    }
    
    func presentTextFieldFormatError() {
        presentTextFieldFormatErrorCallsCount += 1
    }
    
    func presentTextFieldPhoneFormatError() {
        presentTextFieldPhoneFormatErrorCallsCount += 1
    }
    
    func presentTextFieldNonexistentError() {
        presentTextFieldNonexistentErrorCallsCount += 1
    }
    
    func presentTextFieldServiceError(message: String) {
        self.message = message
        presentTextFieldServiceErrorCallsCount += 1
    }
    
    func presentGenericError() {
        presentGenericErrorCallsCount += 1
    }
    
    func presentForwardButtonState(isEnabled: Bool) {
        buttonIsEnabled = isEnabled
        presentForwardButtonStateCallsCount += 1
    }
    
    func presentTextFieldSelection(isSelected: Bool) {
        presentTextFieldSelectionCallsCount += 1
        self.isSelected = isSelected
    }

    func presentError(title: String, subtitle: String, jsonButtons: [[String : Any]]) {
        presentErrorTitleSubtitleJsonButtonsCallsCount += 1
        self.title = title
        self.subtitle = subtitle
    }

    func cleanTextFieldContent() {
        cleanTextFieldContentCallsCount += 1
    }

    func presentTextFieldRightView(keyType: KeyType) {
        presentTextFieldRightViewCallsCount += 1
        agendaKeyType = keyType
    }
    
    func presentSelectedNumber(_ number: String) {
        presentSelectedNumberCallsCount += 1
        selectedNumber = number
    }
    
    func presentCopiedKey(key: String) {
        presentCopiedKeyCallsCount += 1
        copiedKey = key
    }
}

extension KeySelectorAction: Equatable {
    public static func == (lhs: Self, rhs: Self) -> Bool {
        switch (lhs, rhs) {
        case (.close, .close):
            return true
        case (.agenda, .agenda):
            return true
        default:
            return false
        }
    }
}

final class KeySelectorInteractorTests: XCTestCase {
    // MARK: - Variables
    private let analytics = AnalyticsSpy()
    private let featureFlag: FeatureManagerMock = {
        let mock = FeatureManagerMock()
        mock.override(key: .isPixKeysAgendaAvailable, with: false)
        mock.override(key: .featurePixkeyPhoneWithDDI, with: false)
        return mock
    }()
    private let service = KeySelectorServiceSpy()
    private let presenter = KeySelectorPresenterSpy()
    private lazy var interactor: KeySelectorInteracting = KeySelectorInteractor(
        service: service,
        presenter: presenter,
        dependencies: DependencyContainerMock(analytics, featureFlag),
        selectedKeyType: .cpf,
        copiedKey: String()
    )
    
    // MARK: - loadInitialData
    func testLoadInitialData_WhenFlagFeaturePixKeysAgendaIsFalse_ShouldCallPresentKeySelectionWithNewValueEqualToPhoneAndPreviousValueNilAndCallPresentMaskAndNotCallPresentTextFieldRightView() throws {
        let initialType = KeyType.cpf
        featureFlag.override(key: .isPixKeysAgendaAvailable, with: false)
        interactor.loadInitialData()
        XCTAssertEqual(presenter.selectionStyleNewType, initialType)
        XCTAssertNil(presenter.selectionStylePreviousType)
        XCTAssertEqual(presenter.maskKeyType, initialType)
        XCTAssertEqual(presenter.bindNewType, initialType)
        XCTAssertNil(presenter.bindPreviousType)
        XCTAssertEqual(presenter.presentKeySelectionStyleCallsCount, 1)
        XCTAssertEqual(presenter.presentMaskCallsCount, 1)
        XCTAssertEqual(presenter.presentTextFieldBindAndContentCallsCount, 1)
        XCTAssertEqual(presenter.presentTextFieldRightViewCallsCount, 0)
        XCTAssertEqual(analytics.logCalledCount, 2)
    }
    
    func testLoadInitialData_WhenFlagFeaturePixKeysAgendaIsTrue_ShouldCallPresentKeySelectionWithNewValueEqualToPhoneAndPreviousValueNilAndCallPresentMaskAndPresentTextFieldRightView() throws {
        let initialType = KeyType.cpf
        featureFlag.override(key: .isPixKeysAgendaAvailable, with: true)
        interactor.loadInitialData()
        XCTAssertEqual(presenter.selectionStyleNewType, initialType)
        XCTAssertNil(presenter.selectionStylePreviousType)
        XCTAssertEqual(presenter.maskKeyType, initialType)
        XCTAssertEqual(presenter.bindNewType, initialType)
        XCTAssertEqual(presenter.agendaKeyType, initialType)
        XCTAssertNil(presenter.bindPreviousType)
        XCTAssertEqual(presenter.presentKeySelectionStyleCallsCount, 1)
        XCTAssertEqual(presenter.presentMaskCallsCount, 1)
        XCTAssertEqual(presenter.presentTextFieldBindAndContentCallsCount, 1)
        XCTAssertEqual(presenter.presentTextFieldRightViewCallsCount, 1)
        XCTAssertEqual(analytics.logCalledCount, 2)
    }
    
    func testLoadInitialData_WhenUserDidNotCopiedAnPixKey_ShouldCallPresentCopiedKey() {
        interactor.loadInitialData()
        XCTAssertEqual(presenter.presentCopiedKeyCallsCount, 0)
        
    }
    
    func testLoadInitialData_WhenUserCopiedAnPixKey_ShouldCallPresentCopiedKey() {
        let interectorWithKey: KeySelectorInteracting = KeySelectorInteractor(
            service: service,
            presenter: presenter,
            dependencies: DependencyContainerMock(analytics, featureFlag),
            selectedKeyType: .random,
            copiedKey: "be481a08-72df-4e63-93cc-be4ff9096002"
        )
        interectorWithKey.loadInitialData()
        XCTAssertEqual(presenter.presentCopiedKeyCallsCount, 1)
    }
    
    // MARK: - selectKey
    func testSelectKey_WhenNewTypeIsEqualToPreviousType_ShouldNotCallAnything() {
        interactor.selectKey(type: .cpf)
        XCTAssertEqual(presenter.presentKeySelectionStyleCallsCount, 0)
        XCTAssertEqual(presenter.presentMaskCallsCount, 0)
        XCTAssertEqual(presenter.presentTextFieldBindAndContentCallsCount, 0)
        XCTAssertEqual(presenter.presentTextFieldNormalStateCallsCount, 0)
        XCTAssertEqual(presenter.presentForwardButtonStateCallsCount, 0)
        XCTAssertEqual(presenter.presentTextFieldRightViewCallsCount, 0)
        XCTAssertEqual(analytics.logCalledCount, 0)
    }
    
    func testSelectKey_WhenNewTypeIsDifferentToPreviousTypeAndFlagFeaturePixKeysAgendaIsFalse_ShouldCallPresentTextFieldBindAndPresentKeySelectionStyleAndPresentMaskAndPresentTextFieldNormalStateAndPresentForwardButtonStateWithRespectiveDataAndNotCallPresentTextFieldRightView() throws {
        let initialValue = KeyType.cpf
        let newType = KeyType.random
        featureFlag.override(key: .isPixKeysAgendaAvailable, with: false)
        interactor.selectKey(type: newType)
        XCTAssertEqual(presenter.selectionStyleNewType, newType)
        XCTAssertEqual(presenter.selectionStylePreviousType, initialValue)
        XCTAssertEqual(presenter.maskKeyType, newType)
        XCTAssertEqual(presenter.bindNewType, newType)
        XCTAssertEqual(presenter.bindPreviousType, initialValue)
        XCTAssertEqual(presenter.buttonIsEnabled, false)
        XCTAssertEqual(presenter.presentKeySelectionStyleCallsCount, 1)
        XCTAssertEqual(presenter.presentMaskCallsCount, 1)
        XCTAssertEqual(presenter.presentTextFieldBindAndContentCallsCount, 1)
        XCTAssertEqual(presenter.presentTextFieldNormalStateCallsCount, 1)
        XCTAssertEqual(presenter.presentForwardButtonStateCallsCount, 1)
        XCTAssertEqual(presenter.presentTextFieldRightViewCallsCount, 0)
        XCTAssertEqual(analytics.logCalledCount, 1)
    }
    
    func testSelectKey_WhenNewTypeIsDifferentToPreviousTypeAndFlagFeaturePixKeysAgendaIsTrue_ShouldCallPresentTextFieldBindAndPresentKeySelectionStyleAndPresentMaskAndPresentTextFieldNormalStateAndPresentForwardButtonStateAndPresentTextFieldRightViewWithRespectiveData() throws {
        let initialValue = KeyType.cpf
        let newType = KeyType.random
        featureFlag.override(key: .isPixKeysAgendaAvailable, with: true)
        interactor.selectKey(type: newType)
        XCTAssertEqual(presenter.selectionStyleNewType, newType)
        XCTAssertEqual(presenter.selectionStylePreviousType, initialValue)
        XCTAssertEqual(presenter.maskKeyType, newType)
        XCTAssertEqual(presenter.bindNewType, newType)
        XCTAssertEqual(presenter.bindPreviousType, initialValue)
        XCTAssertEqual(presenter.agendaKeyType, newType)
        XCTAssertEqual(presenter.buttonIsEnabled, false)
        XCTAssertEqual(presenter.presentKeySelectionStyleCallsCount, 1)
        XCTAssertEqual(presenter.presentMaskCallsCount, 1)
        XCTAssertEqual(presenter.presentTextFieldBindAndContentCallsCount, 1)
        XCTAssertEqual(presenter.presentTextFieldNormalStateCallsCount, 1)
        XCTAssertEqual(presenter.presentForwardButtonStateCallsCount, 1)
        XCTAssertEqual(presenter.presentTextFieldRightViewCallsCount, 1)
        XCTAssertEqual(analytics.logCalledCount, 1)
    }
    
    // MARK: - verifyKey
    func testVerifyKey_WhenKeyIsNil_ShouldNotCallAnything() {
        interactor.verifyKey(nil)
        XCTAssertEqual(presenter.presentMaskCallsCount, 0)
        XCTAssertEqual(presenter.presentTextFieldBindAndContentCallsCount, 0)
        XCTAssertEqual(presenter.presentTextFieldNormalStateCallsCount, 0)
        XCTAssertEqual(presenter.presentTextFieldSelectionCallsCount, 0)
        XCTAssertEqual(presenter.presentForwardButtonStateCallsCount, 0)
        XCTAssertEqual(presenter.presentTextFieldFormatErrorCallsCount, 0)
        XCTAssertEqual(analytics.logCalledCount, 0)
    }
    
    func testVerifyKey_WhenKeyIsCPFAndTextCountIsGreaterThan14AndTextCountWasLessThan15_ShouldCallPresentMaskWithTypeCPFAndPresentTextFieldBindAndContentWithNewTypeCNPJAndPreviousTypeCPF() throws {
        interactor.verifyKey("000.000.001-91")
        interactor.verifyKey("000.000.001-912")
        let maskKeyType = try XCTUnwrap(presenter.maskKeyType)
        let bindNewType = try XCTUnwrap(presenter.bindNewType)
        let bindPreviousType = try XCTUnwrap(presenter.bindPreviousType)
        XCTAssertEqual(maskKeyType, .cnpj)
        XCTAssertEqual(bindNewType, .cnpj)
        XCTAssertEqual(bindPreviousType, .cpf)
        XCTAssertEqual(presenter.presentMaskCallsCount, 1)
        XCTAssertEqual(presenter.presentTextFieldBindAndContentCallsCount, 1)
    }
    
    func testVerifyKey_WhenKeyIsCPFAndDataIsValid_ShouldCallPresentTextFieldNormalStateAndPresentTextFieldSelectionWithIsSelectedTrueAndPresentForwardButtonStateWithIsEnabledTrue() throws {
        interactor.verifyKey("000.000.001-91")
        XCTAssertEqual(presenter.buttonIsEnabled, true)
        XCTAssertEqual(presenter.isSelected, true)
        XCTAssertEqual(presenter.presentTextFieldNormalStateCallsCount, 1)
        XCTAssertEqual(presenter.presentTextFieldSelectionCallsCount, 1)
        XCTAssertEqual(presenter.presentForwardButtonStateCallsCount, 1)
    }
    
    func testVerifyKey_WhenKeyIsCPFAndDataIsInvalid_ShouldCallPresentTextFieldFormatErrorAndPresentTextFieldSelectionWithIsSelectedFalseAndPresentForwardButtonStateWithIsEnabledFalse() throws {
        interactor.verifyKey("000.000.001-92")
        XCTAssertEqual(presenter.buttonIsEnabled, false)
        XCTAssertEqual(presenter.isSelected, false)
        XCTAssertEqual(presenter.presentTextFieldFormatErrorCallsCount, 1)
        XCTAssertEqual(presenter.presentTextFieldSelectionCallsCount, 1)
        XCTAssertEqual(presenter.presentForwardButtonStateCallsCount, 1)
        XCTAssertEqual(analytics.logCalledCount, 1)
    }
    
    func testVerifyKey_WhenKeyIsCPFAndDataIsIncomplete_ShouldCallPresentTextFieldNormalStateAndPresentTextFieldSelectionWithIsSelectedFalseAndPresentForwardButtonStateWithIsEnabledFalse() throws {
        interactor.verifyKey("000.000.001")
        XCTAssertEqual(presenter.buttonIsEnabled, false)
        XCTAssertEqual(presenter.isSelected, false)
        XCTAssertEqual(presenter.presentTextFieldNormalStateCallsCount, 1)
        XCTAssertEqual(presenter.presentTextFieldSelectionCallsCount, 1)
        XCTAssertEqual(presenter.presentForwardButtonStateCallsCount, 1)
    }
    
    func testVerifyKey_WhenKeyIsCPFAndDataIsCNPJAndIsValid_ShouldCallPresentTextFieldNormalStateAndPresentTextFieldSelectionWithIsSelectedTrueAndPresentForwardButtonStateWithIsEnabledTrue() throws {
        interactor.verifyKey("00.000.000/0001-91")
        XCTAssertEqual(presenter.buttonIsEnabled, true)
        XCTAssertEqual(presenter.isSelected, true)
        XCTAssertEqual(presenter.presentTextFieldNormalStateCallsCount, 1)
        XCTAssertEqual(presenter.presentTextFieldSelectionCallsCount, 1)
        XCTAssertEqual(presenter.presentForwardButtonStateCallsCount, 1)
    }
    
    func testVerifyKey_WhenKeyIsCPFAndDataIsCNPJAndIsInvalid_ShouldCallPresentTextFieldFormatErrorAndPresentTextFieldSelectionWithIsSelectedFalseAndPresentForwardButtonStateWithIsEnabledFalse() throws {
        interactor.verifyKey("00.000.000/0001-92")
        XCTAssertEqual(presenter.buttonIsEnabled, false)
        XCTAssertEqual(presenter.isSelected, false)
        XCTAssertEqual(presenter.presentTextFieldFormatErrorCallsCount, 1)
        XCTAssertEqual(presenter.presentTextFieldSelectionCallsCount, 1)
        XCTAssertEqual(presenter.presentForwardButtonStateCallsCount, 1)
        XCTAssertEqual(analytics.logCalledCount, 1)
    }
    
    func testVerifyKey_WhenKeyIsCPFAndDataIsCNPJAndIsIncomplete_ShouldCallPresentTextFieldNormalStateAndPresentTextFieldSelectionWithIsSelectedFalseAndPresentForwardButtonStateWithIsEnabledFalse() throws {
        interactor.verifyKey("00.000.000/0001")
        XCTAssertEqual(presenter.buttonIsEnabled, false)
        XCTAssertEqual(presenter.isSelected, false)
        XCTAssertEqual(presenter.presentTextFieldNormalStateCallsCount, 1)
        XCTAssertEqual(presenter.presentTextFieldSelectionCallsCount, 1)
        XCTAssertEqual(presenter.presentForwardButtonStateCallsCount, 1)
    }
    
    func testVerifyKey_WhenKeyIsPhoneAndDataIsValidAndDDIFlagIsOff_ShouldCallPresentTextFieldNormalStateAndPresentTextFieldSelectionWithIsSelectedTrueAndPresentForwardButtonStateWithIsEnabledTrue() throws {
        interactor.selectKey(type: .phone)
        interactor.verifyKey("(11) 12345-1234")
        XCTAssertEqual(presenter.buttonIsEnabled, true)
        XCTAssertEqual(presenter.isSelected, true)
        XCTAssertEqual(presenter.presentTextFieldNormalStateCallsCount, 2)
        XCTAssertEqual(presenter.presentTextFieldSelectionCallsCount, 1)
        XCTAssertEqual(presenter.presentForwardButtonStateCallsCount, 2)
    }

    func testVerifyKey_WhenKeyIsPhoneAndDataIsValidAndDDIFlagIsOn_ShouldCallPresentTextFieldNormalStateAndPresentTextFieldSelectionWithIsSelectedTrueAndPresentForwardButtonStateWithIsEnabledTrue() throws {
        featureFlag.override(key: .featurePixkeyPhoneWithDDI, with: true)
        interactor.selectKey(type: .phone)
        interactor.verifyKey("+5511123451234")
        XCTAssertEqual(presenter.buttonIsEnabled, true)
        XCTAssertEqual(presenter.isSelected, true)
        XCTAssertEqual(presenter.presentTextFieldNormalStateCallsCount, 2)
        XCTAssertEqual(presenter.presentTextFieldSelectionCallsCount, 1)
        XCTAssertEqual(presenter.presentForwardButtonStateCallsCount, 2)
    }
    
    func testVerifyKey_WhenKeyIsPhoneAndDataIsEmptyAndDDIFlagIsOn_ShouldCallPresentTextFieldPhoneFormatErrorAndPresentTextFieldSelectionWithIsSelectedFalseAndPresentForwardButtonStateWithIsEnabledFalse() throws {
        featureFlag.override(key: .featurePixkeyPhoneWithDDI, with: true)
        interactor.selectKey(type: .phone)
        interactor.verifyKey("")
        XCTAssertEqual(presenter.buttonIsEnabled, false)
        XCTAssertEqual(presenter.isSelected, false)
        XCTAssertEqual(presenter.presentTextFieldPhoneFormatErrorCallsCount, 1)
        XCTAssertEqual(presenter.presentTextFieldSelectionCallsCount, 1)
        XCTAssertEqual(presenter.presentForwardButtonStateCallsCount, 2)
        XCTAssertEqual(analytics.logCalledCount, 2)
    }
    
    func testVerifyKey_WhenKeyIsPhoneAndDataIsIncompleteAndDDIFlagIsOn_ShouldCallPresentTextFieldNormalStateAndPresentTextFieldSelectionWithIsSelectedFalseAndPresentForwardButtonStateWithIsEnabledFalse() throws {
        featureFlag.override(key: .featurePixkeyPhoneWithDDI, with: true)
        interactor.selectKey(type: .phone)
        interactor.verifyKey("+")
        XCTAssertEqual(presenter.buttonIsEnabled, false)
        XCTAssertEqual(presenter.isSelected, false)
        XCTAssertEqual(presenter.presentTextFieldNormalStateCallsCount, 1)
        XCTAssertEqual(presenter.presentTextFieldSelectionCallsCount, 1)
        XCTAssertEqual(presenter.presentForwardButtonStateCallsCount, 2)
    }

    func testVerifyKey_WhenKeyIsPhoneAndDataIsInvalid_ShouldCallPresentTextFieldNormalStateAndPresentTextFieldSelectionWithIsSelectedFalseAndPresentForwardButtonStateWithIsEnabledFalse() throws {
        interactor.selectKey(type: .phone)
        interactor.verifyKey("+\(String(repeating: "1", count: 72))")
        XCTAssertEqual(presenter.buttonIsEnabled, false)
        XCTAssertEqual(presenter.isSelected, false)
        XCTAssertEqual(presenter.presentTextFieldNormalStateCallsCount, 1)
        XCTAssertEqual(presenter.presentTextFieldSelectionCallsCount, 1)
        XCTAssertEqual(presenter.presentForwardButtonStateCallsCount, 2)
    }
    
    func testVerifyKey_WhenKeyIsEmailAndDataIsValid_ShouldCallPresentTextFieldNormalStateAndPresentTextFieldSelectionWithIsSelectedTrueAndPresentForwardButtonStateWithIsEnabledTrue() throws {
        interactor.selectKey(type: .email)
        interactor.verifyKey("teste@test.com")
        XCTAssertEqual(presenter.buttonIsEnabled, true)
        XCTAssertEqual(presenter.isSelected, true)
        XCTAssertEqual(presenter.presentTextFieldNormalStateCallsCount, 2)
        XCTAssertEqual(presenter.presentTextFieldSelectionCallsCount, 1)
        XCTAssertEqual(presenter.presentForwardButtonStateCallsCount, 2)
    }
    
    func testVerifyKey_WhenKeyIsEmailAndDataIsInvalid_ShouldCallPresentTextFieldFormatErrorAndPresentTextFieldSelectionWithIsSelectedFalseAndPresentForwardButtonStateWithIsEnabledFalse() throws {
        interactor.selectKey(type: .email)
        interactor.verifyKey("teste.com")
        XCTAssertEqual(presenter.buttonIsEnabled, false)
        XCTAssertEqual(presenter.isSelected, false)
        XCTAssertEqual(presenter.presentTextFieldFormatErrorCallsCount, 1)
        XCTAssertEqual(presenter.presentTextFieldSelectionCallsCount, 1)
        XCTAssertEqual(presenter.presentForwardButtonStateCallsCount, 2)
        XCTAssertEqual(analytics.logCalledCount, 2)
    }
    
    func testVerifyKey_WhenKeyIsRandomAndDataIsValid_ShouldCallPresentTextFieldNormalStateAndPresentTextFieldSelectionWithIsSelectedTrueAndPresentForwardButtonStateWithIsEnabledTrue() throws {
        interactor.selectKey(type: .random)
        interactor.verifyKey("12345678-1234-1234-1234-123456789012")
        XCTAssertEqual(presenter.buttonIsEnabled, true)
        XCTAssertEqual(presenter.isSelected, true)
        XCTAssertEqual(presenter.presentTextFieldNormalStateCallsCount, 2)
        XCTAssertEqual(presenter.presentTextFieldSelectionCallsCount, 1)
        XCTAssertEqual(presenter.presentForwardButtonStateCallsCount, 2)
    }
    
    func testVerifyKey_WhenKeyIsRandomAndDataIsInvalid_ShouldCallPresentTextFieldFormatErrorAndPresentTextFieldSelectionWithIsSelectedFalseAndPresentForwardButtonStateWithIsEnabledFalse() throws {
        interactor.selectKey(type: .random)
        interactor.verifyKey("12345678-1234-1234-1234-1234567890123")
        XCTAssertEqual(presenter.buttonIsEnabled, false)
        XCTAssertEqual(presenter.isSelected, false)
        XCTAssertEqual(presenter.presentTextFieldFormatErrorCallsCount, 1)
        XCTAssertEqual(presenter.presentTextFieldSelectionCallsCount, 1)
        XCTAssertEqual(presenter.presentForwardButtonStateCallsCount, 2)
        XCTAssertEqual(analytics.logCalledCount, 2)
    }
    
    func testVerifyKey_WhenKeyIsRandomAndDataIsIncomplete_ShouldCallPresentTextFieldNormalStateAndPresentTextFieldSelectionWithIsSelectedFalseAndPresentForwardButtonStateWithIsEnabledFalse() throws {
        interactor.selectKey(type: .random)
        interactor.verifyKey("1234")
        XCTAssertEqual(presenter.buttonIsEnabled, false)
        XCTAssertEqual(presenter.isSelected, false)
        XCTAssertEqual(presenter.presentTextFieldNormalStateCallsCount, 2)
        XCTAssertEqual(presenter.presentTextFieldSelectionCallsCount, 1)
        XCTAssertEqual(presenter.presentForwardButtonStateCallsCount, 2)
    }
    
    // MARK: - forwardAction
    func testForwardAction_WhenResultIsFailureWithApiErrorWithUI_ShouldCallPresentError() throws {
        let decoder = JSONFileDecoder<RequestError>()
        let requestError = try decoder.load(
            resource: "RequestErrorUIAlert",
            typeDecoder: .convertFromSnakeCase
        )

        let apiError = ApiError.otherErrors(body: requestError)
        service.result = .failure(apiError)
        interactor.forwardAction()

        XCTAssertEqual(presenter.presentErrorTitleSubtitleJsonButtonsCallsCount, 1)
        XCTAssertEqual(presenter.title, "Transação não concluída")
        XCTAssertEqual(presenter.subtitle, "Falha na comunicação.\nTente novamente")
    }
    
    func testForwardAction_WhenResultIsFailureWithServerError_ShouldCallPresentGenericError() throws {
        service.result = .failure(ApiError.serverError)
        interactor.forwardAction()
        XCTAssertEqual(service.validateCallsCount, 1)
        XCTAssertEqual(presenter.presentGenericErrorCallsCount, 1)
    }

    func testForwardAction_WhenResultIsSuccess_ShouldCallPresentPaymentDetailsWithAccountKeyTypeKey() throws {
        let account = PixAccount(
            name: "Teste",
            cpfCnpj: "cpf",
            bank: "Bank",
            agency: "Agency",
            accountNumber: "Account Number",
            imageUrl: nil,
            id: nil
        )
        let keyType = KeyType.cnpj
        interactor.selectKey(type: keyType)
        let key = "22.896.431/0001-10"
        let cleanKey = "22896431000110"
        interactor.verifyKey(key)

        service.result = .success(account)
        interactor.forwardAction()

        let fakeAccount = try XCTUnwrap(presenter.account)
        let fakeKeyType = try XCTUnwrap(presenter.keyType)
        let fakeKey = try XCTUnwrap(presenter.key)

        XCTAssertEqual(presenter.presentPaymentDetailsWithAccountCallsCount, 1)
        XCTAssertEqual(fakeAccount, account)
        XCTAssertEqual(fakeKeyType, keyType)
        XCTAssertEqual(fakeKey, cleanKey)
    }

    // MARK: - testProccessInput
    func testProcessInput_WhenInputIsPlus55AndTypeIsPhoneAndDDIFlagIsOn_ShouldCallCleanTextFieldContentCallsCount() {
        featureFlag.override(key: .featurePixkeyPhoneWithDDI, with: true)
        interactor.selectKey(type: .phone)
        interactor.processTextInput("+55")
        XCTAssertEqual(presenter.cleanTextFieldContentCallsCount, 1)
    }

    func testProcessInput_WhenInputIsPlusAndDDIFlagIsOn_ShouldNotCallCleanTextFieldContentCallsCount() {
        featureFlag.override(key: .featurePixkeyPhoneWithDDI, with: true)
        interactor.processTextInput("+")
        XCTAssertEqual(presenter.cleanTextFieldContentCallsCount, 0)
    }

    func testProcessInput_WhenInputIs55AndDDIFlagIsOn_ShouldNotCallCleanTextFieldContentCallsCount() {
        featureFlag.override(key: .featurePixkeyPhoneWithDDI, with: true)
        interactor.processTextInput("55")
        XCTAssertEqual(presenter.cleanTextFieldContentCallsCount, 0)
    }
    
    // MARK: - close
    func testClose_ShouldCallDidNextStepWithActionClose() {
        interactor.close()
        XCTAssertEqual(presenter.didNextStepCallsCount, 1)
        XCTAssertEqual(presenter.action, .close)
    }
    
    // MARK: - showContactList
    func testShowContactList_WhenFirstCharIsPlus_ShouldCallDidNextStepWithActionAgendaAndPresentSelectedNumberWithOriginalNumber() {
        let phoneNumber = "+123456789"
        featureFlag.override(key: .featurePixkeyPhoneWithDDI, with: true)
        interactor.showContactList()
        XCTAssertEqual(presenter.didNextStepCallsCount, 1)
        guard let action = presenter.action, case let KeySelectorAction.agenda(completion) = action else {
            XCTFail("Wrong action type")
            return
        }
        completion(phoneNumber)
        XCTAssertEqual(presenter.presentSelectedNumberCallsCount, 1)
        XCTAssertEqual(presenter.selectedNumber, phoneNumber)
    }
    
    func testShowContactList_WhenFirstCharIsNotPlus_ShouldCallDidNextStepWithActionAgendaAndPresentSelectedNumberWithBrazillianAreaCodeIncludedInNumber() {
        let phoneNumber = "123456789"
        featureFlag.override(key: .featurePixkeyPhoneWithDDI, with: true)
        interactor.showContactList()
        XCTAssertEqual(presenter.didNextStepCallsCount, 1)
        guard let action = presenter.action, case let KeySelectorAction.agenda(completion) = action else {
            XCTFail("Wrong action type")
            return
        }
        completion(phoneNumber)
        XCTAssertEqual(presenter.presentSelectedNumberCallsCount, 1)
        XCTAssertEqual(presenter.selectedNumber, "+55" + phoneNumber)
    }
    
    // MARK: show
}
