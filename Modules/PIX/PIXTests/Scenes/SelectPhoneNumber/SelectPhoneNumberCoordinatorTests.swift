import XCTest
@testable import PIX

final class SelectPhoneNumberCoordinatorTests: XCTestCase {
    private lazy var viewController = SpyViewController()
    private lazy var coordinator: SelectPhoneNumberCoordinator = {
        let coordinator = SelectPhoneNumberCoordinator(numberHandler: completion)
        coordinator.viewController = viewController
        return coordinator
    }()
    private var completion: (String) -> Void = { _ in }
    
    // MARK: - perform
    func testPerform_WhereActionIsClose_ShouldCallDismiss() throws {
        coordinator.perform(action: .close)
        XCTAssertEqual(viewController.dismissCallCount, 1)
    }
    
    func testPerform_WhereActionIsAgenda_ShouldCallDismiss() throws {
        let number = "123456789"
        completion = { finalNumber in
            XCTAssertEqual(number, finalNumber)
        }
        coordinator.perform(action: .phoneSelection(number: number))
        XCTAssertEqual(viewController.dismissCallCount, 1)
    }
}

