@testable import PIX
import UI
import XCTest

private final class SelectPhoneNumberCoordinatorSpy: SelectPhoneNumberCoordinating {
    var viewController: UIViewController?
    private(set) var performCallsCount = 0
    private(set) var action: SelectPhoneNumberAction?

    func perform(action: SelectPhoneNumberAction) {
        performCallsCount += 1
        self.action = action
    }
}

private final class SelectPhoneNumberViewControllerSpy: SelectPhoneNumberDisplaying {
    private(set) var displayContactsCallsCount = 0
    private(set) var displayNoContactsCallsCount = 0
    private(set) var displayNoFetchingContactsCallsCount = 0
    private(set) var displayRequestContactsCallsCount = 0
    private(set) var performCallsCount = 0
    private(set) var contacts: [ContactModel]?

    func displayContacts(_ contacts: [ContactModel]) {
        displayContactsCallsCount += 1
        self.contacts = contacts
    }
    
    func displayRequestContactsPermission() {
        displayRequestContactsCallsCount += 1
    }
    
    func displayNoContacts(){
        displayNoContactsCallsCount += 1
    }
    
    func displayNoFetchingContacts(){
        displayNoFetchingContactsCallsCount += 1
    }
}

final class SelectPhoneNumberPresenterTests: XCTestCase {
    // MARK: - Variables
    private let coordinator = SelectPhoneNumberCoordinatorSpy()
    private let viewController = SelectPhoneNumberViewControllerSpy()
    private lazy var presenter: SelectPhoneNumberPresenter = {
        let presenter = SelectPhoneNumberPresenter(coordinator: coordinator)
        presenter.viewController = viewController
        return presenter
    }()

    // MARK: - presentContacts
    func testPresentContacts_ShouldCallDisplayContacts() {
        let contacts = [
            ContactModel(name: "Test1", phoneNumber: "1234"),
            ContactModel(name: "Test2", phoneNumber: "5678")
        ]
        presenter.presentContacts(contacts)
        XCTAssertEqual(viewController.displayContactsCallsCount, 1)
        XCTAssertEqual(viewController.contacts, contacts)
    }
    
    // MARK: - presentNoContacts
    func testPresentNoContacts_ShouldCallDisplayNoContacts() {
        presenter.presentNoContacts()
        XCTAssertEqual(viewController.displayNoContactsCallsCount, 1)
    }
    
    // MARK: - presentNoFetchingContacts
    func testPresentNoFetchingContacts_ShouldCallDisplayNoFetchingContacts() {
        presenter.presentFetchingNoContacts()
        XCTAssertEqual(viewController.displayNoFetchingContactsCallsCount, 1)
    }
    
    // MARK: - presentRequestPermission
    func testPresentRequestPermission_ShouldCallDisplayContacts() {
        presenter.presentRequestPermission()
        XCTAssertEqual(viewController.displayRequestContactsCallsCount, 1)
    }
    
    // MARK: - didNextStep
    func testDidNextStep_ShouldCallPerformWithCloseAction() {
        let action = SelectPhoneNumberAction.close
        presenter.didNextStep(action: action)
        XCTAssertEqual(coordinator.performCallsCount, 1)
        XCTAssertEqual(coordinator.action, action)
    }
    
    func testDidNextStep_ShouldCallPerformWithPhoneSelectionAction() {
        let number = "123456"
        let action = SelectPhoneNumberAction.phoneSelection(number: number)
        presenter.didNextStep(action: action)
        XCTAssertEqual(coordinator.performCallsCount, 1)
        XCTAssertEqual(coordinator.action, action)
    }
}
