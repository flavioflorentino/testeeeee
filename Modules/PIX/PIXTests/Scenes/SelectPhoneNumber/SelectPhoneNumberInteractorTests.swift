import AnalyticsModule
import PermissionsKit
@testable import PIX
import XCTest

private final class SelectPhoneNumberServiceMock: SelectPhoneNumberServicing {
    var contacts = [ContactModel]()
    var status: PermissionStatus = .authorized
    
    func requestContacts(completion: @escaping Permission.Callback) {
        completion(status)
    }
    
    func fetchContacts(result: ([ContactModel]) -> Void) {
        result(contacts)
    }
}

private final class SelectPhoneNumberPresenterSpy: SelectPhoneNumberPresenting {
    var viewController: SelectPhoneNumberDisplaying?
    private(set) var presentContactsCallsCount = 0
    private(set) var presentRequestPermissionCallsCount = 0
    private(set) var presentNoContactsCallsCount = 0
    private(set) var presentNoFetchingContactsCallsCount = 0
    private(set) var contacts: [ContactModel]?
    private(set) var didNextStepCallsCount = 0
    private(set) var action: SelectPhoneNumberAction?
    
    func presentNoContacts() {
        presentNoContactsCallsCount += 1
    }
    
    func presentFetchingNoContacts() {
        presentNoFetchingContactsCallsCount += 1
    }
    

    func presentContacts(_ contacts: [ContactModel]) {
        presentContactsCallsCount += 1
        self.contacts = contacts
    }

    func didNextStep(action: SelectPhoneNumberAction) {
        didNextStepCallsCount += 1
        self.action = action
    }
    
    func presentRequestPermission() {
        presentRequestPermissionCallsCount += 1
    }
}

extension SelectPhoneNumberAction: Equatable {
    public static func == (lhs: Self, rhs: Self) -> Bool {
        switch (lhs, rhs) {
        case (.close, .close):
            return true
        case let (.phoneSelection(numberLhs), .phoneSelection(numberRhs)) where numberLhs == numberRhs:
            return true
        default:
            return false
        }
    }
}

final class SelectPhoneNumberInteractorTests: XCTestCase {
    // MARK: - Variables
    private let analytics = AnalyticsSpy()
    private let service = SelectPhoneNumberServiceMock()
    private let presenter = SelectPhoneNumberPresenterSpy()
    private var contacts: [ContactModel] = []
    private lazy var interactor = SelectPhoneNumberInteractor(presenter: presenter, dependencies: DependencyContainerMock(analytics), service: service, contacts: contacts)

    // MARK: - RequestContactsPermission
    func testRequestContactsPermission_WhenServiceReturnNoContactsAndPermissionIsAuthorized_ShouldPresentNoContacts() throws {
        let contacts = [ContactModel]()
        service.contacts = contacts
        interactor.requestContactsPermission()
        XCTAssertEqual(presenter.presentNoContactsCallsCount, 1)
        XCTAssertEqual(presenter.presentRequestPermissionCallsCount, 0)
        XCTAssertEqual(analytics.logCalledCount, 2)
    }
    
    func testRequestContactsPermission_WhenServiceReturnContactsAndPermissionIsAuthorized_ShouldPresentContacts() throws {
        let contacts = [
            ContactModel(name: "Test1", phoneNumber: "12345"),
            ContactModel(name: "Test2", phoneNumber: "67890")
        ]
        service.contacts = contacts
        interactor.requestContactsPermission()
        XCTAssertEqual(presenter.presentContactsCallsCount, 1)
        XCTAssertEqual(presenter.presentRequestPermissionCallsCount, 0)
        XCTAssertEqual(presenter.contacts, contacts)
        XCTAssertEqual(analytics.logCalledCount, 1)
    }
    
    func testRequestContactsPermission_WhenServiceReturnContactsAndPermissionIsNotAuthorized_ShouldPresentContacts() throws {
        service.status = .denied
        interactor.requestContactsPermission()
        XCTAssertEqual(presenter.presentContactsCallsCount, 0)
        XCTAssertEqual(presenter.presentRequestPermissionCallsCount, 1)
    }

    // MARK: - searchContact
    func testSearchContact_WhenServiceReturnNoContact_ShouldPresentNoContact() {
        contacts = [ContactModel]()
        interactor.searchContact(by: "")
        XCTAssertEqual(presenter.presentNoContactsCallsCount, 1)
        XCTAssertEqual(analytics.logCalledCount, 1)
    }

    func testSearchContact_WhenServiceReturnContactNamedTest1AndTest2TestAndSearchIsTest1_ShouldPresentContactNamedTest1() {
        let contact1 = ContactModel(name: "Test1", phoneNumber: "12345")
        let contact2 = ContactModel(name: "Test2", phoneNumber: "12345")
        contacts = [contact1, contact2]
        interactor.searchContact(by: "Test1")
        XCTAssertEqual(presenter.presentContactsCallsCount, 1)
        XCTAssertEqual(presenter.contacts, [contact1])
        XCTAssertEqual(analytics.logCalledCount, 0)
    }

    func testSearchContact_WhenServiceReturnContactsNamedTest1AndTest2AndSearchIsE_ShouldPresentBothContacts() {
        contacts = [
            ContactModel(name: "Test1", phoneNumber: "12345"),
            ContactModel(name: "Test", phoneNumber: "12345")
        ]
        interactor.searchContact(by: "E")
        XCTAssertEqual(presenter.presentContactsCallsCount, 1)
        XCTAssertEqual(presenter.contacts, contacts)
        XCTAssertEqual(analytics.logCalledCount, 0)
    }

    func testSearchContact_WhenServiceReturnContactWithPhoneNumber2345AndSearchIs34_ShouldPresentThisContact() {
        contacts = [ContactModel(name: "Test1", phoneNumber: "2345")]
        interactor.searchContact(by: "34")
        XCTAssertEqual(presenter.presentContactsCallsCount, 1)
        XCTAssertEqual(presenter.contacts, contacts)
        XCTAssertEqual(analytics.logCalledCount, 0)
    }

    func testSearchContact_WhenServiceReturnContactsNamedTest1AndTest2AndSearchIsA_ShouldPresentNoFetchingContacts() {
        contacts = [
            ContactModel(name: "Test1", phoneNumber: "12345"),
            ContactModel(name: "Test", phoneNumber: "12345")
        ]
        interactor.searchContact(by: "A")
        XCTAssertEqual(presenter.presentNoFetchingContactsCallsCount, 1)
        XCTAssertEqual(analytics.logCalledCount, 1)
    }

    func testSearchContact_WhenServiceReturnContactsNamedTest1AndTest2AndSearchIsEmpty_ShouldShowAllContacts() {
        contacts = [
            ContactModel(name: "Test1", phoneNumber: "12345"),
            ContactModel(name: "Test", phoneNumber: "12345")
        ]
        interactor.searchContact(by: "")
        XCTAssertEqual(presenter.presentContactsCallsCount, 1)
        XCTAssertEqual(presenter.contacts, contacts)
        XCTAssertEqual(analytics.logCalledCount, 0)
    }
    
    // MARK: - verifyContactsPermission
    func testVerifyContactsPermission_WhenServiceReturnNoContactsAndPermissionIsAuthorized_ShouldPresentNoContacts() throws {
        let contacts = [ContactModel]()
        service.contacts = contacts
        interactor.requestContactsPermission()
        XCTAssertEqual(presenter.presentNoContactsCallsCount, 1)
        XCTAssertEqual(presenter.presentRequestPermissionCallsCount, 0)
        XCTAssertEqual(analytics.logCalledCount, 2)
    }
    
    func testVerifyContactsPermission_WhenServiceReturnContactsAndPermissionIsAuthorized_ShouldPresentContacts() throws {
        let contacts = [
            ContactModel(name: "Test1", phoneNumber: "12345"),
            ContactModel(name: "Test2", phoneNumber: "67890")
        ]
        service.contacts = contacts
        interactor.requestContactsPermission()
        XCTAssertEqual(presenter.presentContactsCallsCount, 1)
        XCTAssertEqual(presenter.presentRequestPermissionCallsCount, 0)
        XCTAssertEqual(presenter.contacts, contacts)
        XCTAssertEqual(analytics.logCalledCount, 1)
    }
    
    func testVerifyContactsPermission_WhenServiceReturnContactsAndPermissionIsNotAuthorized_ShouldPresentContacts() throws {
        service.status = .denied
        interactor.requestContactsPermission()
        XCTAssertEqual(presenter.presentContactsCallsCount, 0)
        XCTAssertEqual(presenter.presentRequestPermissionCallsCount, 1)
        XCTAssertEqual(analytics.logCalledCount, 0)
    }
    
    //MARK: - close
    func testClose_ShouldCallDidNextStepWithActionClose() {
        interactor.close()
        XCTAssertEqual(presenter.didNextStepCallsCount, 1)
        XCTAssertEqual(presenter.action, .close)
    }
    
    // MARK: - didSelectContact
    func testDidSelectContact_WhenHasNoFilteredContacts_ShouldCallDidNextStepWithProperSelectedContact() {
        let phoneNumber = "12345"
        contacts = [
            ContactModel(name: "Test1", phoneNumber: phoneNumber)
        ]
        let index = IndexPath(row: 0, section: 0)
        interactor.didSelectContact(at: index)
        XCTAssertEqual(presenter.didNextStepCallsCount, 1)
        XCTAssertEqual(presenter.action, .phoneSelection(number: phoneNumber))
        XCTAssertEqual(analytics.logCalledCount, 1)
    }
    
    func testDidSelectContact_WhenHasFilteredContacts_ShouldCallDidNextStepWithProperSelectedContact() {
        let filterText = "Test1"
        let phoneNumber = "12345"
        contacts = [
            ContactModel(name: "iOS", phoneNumber: "123456789"),
            ContactModel(name: filterText, phoneNumber: phoneNumber)
        ]
        let index = IndexPath(row: 0, section: 0)
        interactor.searchContact(by: filterText)
        interactor.didSelectContact(at: index)
        XCTAssertEqual(presenter.didNextStepCallsCount, 1)
        XCTAssertEqual(presenter.action, .phoneSelection(number: phoneNumber))
        XCTAssertEqual(analytics.logCalledCount, 1)
    }
    
    //MARK: - permissionDenied
    func testePermissionDenied_WhenPermissionIsNotAuthorized_ShouldCallDidNextStepWithActionClose() {
        service.status = .denied
        interactor.requestContactsPermission()
        interactor.permissionDenied()
        XCTAssertEqual(presenter.didNextStepCallsCount, 1)
        XCTAssertEqual(presenter.action, .close)
        XCTAssertEqual(analytics.logCalledCount, 1)
    }
}
