@testable import PIX
import UI
import XCTest

private final class HubCoordinatorSpy: HubCoordinating {
    var viewController: UIViewController?
    private(set) var performWithTypeCallCount = 0
    private(set) var type: HubItemType?
    private(set) var closeCallCount = 0
    
    func performWithType(_ type: HubItemType) {
        performWithTypeCallCount += 1
        self.type = type
    }
    
    func close() {
        closeCallCount += 1
    }
}

private final class HubViewControllerSpy: HubDisplay {
    private(set) var showSectionsCallCount = 0
    private(set) var sections: [HubSection]?

    func showSections(_ sections: [HubSection]) {
        self.showSectionsCallCount += 1
        self.sections = sections
    }
}

final class HubPresenterTests: XCTestCase {
    // MARK: - Variables
    private let viewController = HubViewControllerSpy()
    private let coordinator = HubCoordinatorSpy()
    private lazy var presenter: HubPresenter = {
        let presenter = HubPresenter(coordinator: coordinator)
        presenter.viewController = viewController
        return presenter
    }()
    
    // MARK: - didNextStepWithType
    func testDidNextStepWithType_ShouldCallCoordinatorDidNextStepWithType() throws {
        let type = HubItemType.keySelector
        presenter.didNextStepWithType(type)
        
        let fakeType = try XCTUnwrap(coordinator.type)
        XCTAssertEqual(coordinator.performWithTypeCallCount, 1)
        XCTAssertEqual(fakeType, type)
    }

    // MARK: - didNextStepWithType
    func testPresentSections_ShouldCallShowSections() throws {
        let sections = [HubSection(title: "Teste", rows: [.keyManagement])]
        presenter.presentSections(sections)
        XCTAssertEqual(viewController.showSectionsCallCount, 1)
        XCTAssertEqual(viewController.sections, sections)
    }
    
    // MARK: - close
    func testClose_ShouldCallCoordinatorClose() {
        presenter.close()
        XCTAssertEqual(coordinator.closeCallCount, 1)
    }
}
