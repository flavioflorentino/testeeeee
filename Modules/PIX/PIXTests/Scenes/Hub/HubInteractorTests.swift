import AnalyticsModule
import Core
import FeatureFlag
@testable import PIX
import XCTest

private final class HubPresenterSpy: HubPresenting {
    var viewController: HubDisplay?
    
    private(set) var didNextStepWithTypeCallCount = 0
    private(set) var type: HubItemType?
    private(set) var didPresentSectionsCallCount = 0
    private(set) var sections: [HubSection]?
    private(set) var closeCallCount = 0

    
    func didNextStepWithType(_ type: HubItemType) {
        didNextStepWithTypeCallCount +=
        1
        self.type = type
    }

    func presentSections(_ sections: [HubSection]) {
        didPresentSectionsCallCount += 1
        self.sections = sections
    }
    
    func close() {
        closeCallCount += 1
    }
}

final class HubInteractorTests: XCTestCase {
    // MARK: - Variables
    private let presenter = HubPresenterSpy()
    private let analytics = AnalyticsSpy()
    private let featureManagerMock: FeatureManagerMock = {
        let featureManagerMock = FeatureManagerMock()
        featureManagerMock.override(key: .featurePixHubKeys, with: false)
        featureManagerMock.override(key: .featurePixHubQrCode, with: false)
        featureManagerMock.override(key: .featurePixHubManualBank, with: false)
        featureManagerMock.override(key: .featurePixHubReceiving, with: false)
        featureManagerMock.override(key: .featurePixHubManageKeys, with: false)
        featureManagerMock.override(key: .featurePixHubCopyPaste, with: false)
        featureManagerMock.override(key: .isPixDailyLimitAvailable, with: false)
        return featureManagerMock
    }()
    
    private lazy var interactor = HubInteractor(
        presenter: presenter,
        dependencies: DependencyContainerMock(analytics, featureManagerMock), origin: "Origem")
    
    // MARK: - callAccessAnalytics
    func testCallAccessAnalytics_ShouldCallAnalytics() {
        interactor.callAccessAnalytics()
        XCTAssertTrue(analytics.equals(to: HubUserEvent.userNavigated(origin: "Origem").event()))
    }
    
    // MARK: - didNextStepWithType
    func testDidNextStepWithType_ShouldCallPresentDidNextStepWithTypeAndCallItemSelectedAnalytics() throws {
        let type = HubItemType.keySelector
        interactor.didNextStepWithType(type)
        
        XCTAssertEqual(presenter.didNextStepWithTypeCallCount, 1)
        let fakeType = try XCTUnwrap(presenter.type)
        XCTAssertEqual(type, fakeType)
        XCTAssertTrue(analytics.equals(to: HubUserEvent.itemSelected(item: type).event()))
    }

    // MARK: - buildHubItems
    func testBuildHubItems_WhenPixHubKeysIsTrue_ShouldCallPresentSectionsWithKeySelector() {
        featureManagerMock.override(key: .featurePixHubKeys, with: true)
        interactor.buildHubItems()

        XCTAssertEqual(presenter.didPresentSectionsCallCount, 1)
        XCTAssertEqual(presenter.sections, [HubSection(title: Strings.General.send, rows: [.keySelector])])
    }

    func testBuildHubItems_WhenPixHubQrCodeIsTrue_ShouldCallPresentSectionsWithQrCode() {
        featureManagerMock.override(key: .featurePixHubQrCode, with: true)
        interactor.buildHubItems()

        XCTAssertEqual(presenter.didPresentSectionsCallCount, 1)
        XCTAssertEqual(presenter.sections, [HubSection(title: Strings.General.send, rows: [.qrCode])])
    }

    func testBuildHubItems_WhenPixHubManualBankIsTrue_ShouldCallPresentSectionsWithManualInsert() {
        featureManagerMock.override(key: .featurePixHubManualBank, with: true)
        interactor.buildHubItems()

        XCTAssertEqual(presenter.didPresentSectionsCallCount, 1)
        XCTAssertEqual(presenter.sections, [HubSection(title: Strings.General.send, rows: [.manualInsert])])
    }

    func testBuildHubItems_WhenPixHubReceivingIsTrue_ShouldCallPresentSectionsWithReceivePayment() {
        featureManagerMock.override(key: .featurePixHubReceiving, with: true)
        interactor.buildHubItems()

        XCTAssertEqual(presenter.didPresentSectionsCallCount, 1)
        XCTAssertEqual(presenter.sections, [HubSection(title: Strings.Hub.receive, rows: [.receivePayment])])
    }
    
    func testBuildHubItems_WhenCopyPasteIsTrue_ShouldCallPresentSectionsWithCopyPaste() {
        featureManagerMock.override(key: .featurePixHubCopyPaste, with: true)
        interactor.buildHubItems()

        XCTAssertEqual(presenter.didPresentSectionsCallCount, 1)
        XCTAssertEqual(presenter.sections, [HubSection(title: Strings.General.send, rows: [.copyPaste])])
    }

    func testBuildHubItems_WhenPixHubManageKeysIsTrue_ShouldCallPresentSectionsWithKeyManagement() {
        featureManagerMock.override(key: .featurePixHubManageKeys, with: true)
        interactor.buildHubItems()

        XCTAssertEqual(presenter.didPresentSectionsCallCount, 1)
        XCTAssertEqual(presenter.sections, [HubSection(title: Strings.Hub.keyManagement, rows: [.keyManagement])])
    }
    
    func testBuildHubItems_WhenIsPixDailyLimitAvailableIsTrue_ShouldCallPresentSectionsWithKeyManagement() {
        featureManagerMock.override(key: .isPixDailyLimitAvailable, with: true)
        interactor.buildHubItems()

        XCTAssertEqual(presenter.didPresentSectionsCallCount, 1)
        XCTAssertEqual(presenter.sections, [HubSection(title: Strings.Hub.myLimitPix, rows: [.dailyLimit])])
    }

    func testBuildHubItems_WhenPixHubAllFeaturesAreTrue_ShouldCallPresentSectionsWithAllHubItems() {
        featureManagerMock.override(key: .featurePixHubKeys, with: true)
        featureManagerMock.override(key: .featurePixHubQrCode, with: true)
        featureManagerMock.override(key: .featurePixHubManualBank, with: true)
        featureManagerMock.override(key: .featurePixHubReceiving, with: true)
        featureManagerMock.override(key: .featurePixHubManageKeys, with: true)
        featureManagerMock.override(key: .featurePixHubCopyPaste, with: true)
        featureManagerMock.override(key: .isPixDailyLimitAvailable, with: true)
        interactor.buildHubItems()

        XCTAssertEqual(presenter.didPresentSectionsCallCount, 1)
        XCTAssertEqual(presenter.sections, [
            HubSection(title: Strings.General.send, rows: [.keySelector, .copyPaste, .qrCode, .manualInsert]),
            HubSection(title: Strings.Hub.receive, rows: [HubItemType.receivePayment]),
            HubSection(title: Strings.Hub.keyManagement, rows: [HubItemType.keyManagement]),
            HubSection(title: Strings.Hub.myLimitPix, rows: [.dailyLimit])
        ])
    }

    func testBuildHubItems_WhenPixHubAllFeaturesAreFalse_ShouldCallPresentSectionsWithNoHubItems() {
        interactor.buildHubItems()

        XCTAssertEqual(presenter.didPresentSectionsCallCount, 1)
        XCTAssertEqual(presenter.sections, [])
    }
    
    // MARK: - close
    func testClose_ShouldCallPresentClose() {
        interactor.close()
        
        XCTAssertEqual(presenter.closeCallCount, 1)
    }
}
