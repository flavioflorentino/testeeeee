import FeatureFlag
import Core
@testable import PIX
import XCTest

final class FakePaymentOpening: PIXPaymentOpening {
    private(set) var openPaymentWithDetailsCallsCount = 0
    private(set) var details: PIXPaymentDetails?
    private(set) var controller: UIViewController?
    
    func openPaymentWithDetails(_ details: PIXPaymentDetails,
                                onViewController controller: UIViewController,
                                origin: PixPaymentOrigin,
                                flowType: PixPaymentFlowType) {
        openPaymentWithDetailsCallsCount += 1
        self.details = details
        self.controller = controller
    }
}

final class HubCoordinatorTests: XCTestCase {
    // MARK: - Variables
    private lazy var viewController: SpyViewController = {
        let controller = SpyViewController()
        self.navigationController.pushViewController(controller, animated: true)
        return controller
    }()
    private lazy var navigationController = SpyNavigationController()
    
    private lazy var coordinator: HubCoordinator = {
        let coordinator = HubCoordinator(
            paymentOpening: FakePaymentOpening(),
            bankSelectorType: PIXBankSelector.self,
            qrCodeScannerType: PIXQrCodeScanningMock.self,
            dependencies: DependencyContainerMock(kvStoreMock)
        )
        coordinator.viewController = viewController
        return coordinator
    }()
    
    private let kvStoreMock = KVStoreMock()
    
    // MARK: - performWithType
    func testPerformWithType_WhenTypeIsKeySelector_ShouldPushKeySelectorViewController() throws {
        coordinator.performWithType(.keySelector)
        
        let fakeController = try XCTUnwrap(navigationController.pushedViewController)
        XCTAssertTrue(fakeController is KeySelectorViewController)
    }
    
    func testPerformWithType_WhenTypeIsKeyManagementAndWelcomeWasNotVisualized_ShouldPushKeyManagementWelccomeViewController() throws {
        kvStoreMock.set(value: false, with: KVKey.isPixKeyManagementWelcomeVisualized)
        coordinator.performWithType(.keyManagement)
        
        let fakeController = try XCTUnwrap(navigationController.pushedViewController)
        XCTAssertTrue(fakeController is KeyManagementWelcomeViewController)
    }
    
    func testPerformWithType_WhenTypeIsKeyManagementAndWelcomeWasVisualized_ShouldPushKeyManagementViewController() throws {
        kvStoreMock.set(value: true, with: KVKey.isPixKeyManagementWelcomeVisualized)
        coordinator.performWithType(.keyManagement)
        
        let fakeController = try XCTUnwrap(navigationController.pushedViewController)
        XCTAssertTrue(fakeController is KeyManagerConsumerViewController)
    }
    
    func testPerformWithType_WhenTypeIsManualInsert_ShouldPushManualInsertionViewController() throws {
        coordinator.performWithType(.manualInsert)
        
        let fakeController = try XCTUnwrap(navigationController.pushedViewController)
        XCTAssertTrue(fakeController is ManualInsertionViewController)
    }
    
    func testPerformWithType_WhenTypeIsQrCode_ShouldPushQrCodeScanningViewController() throws {
        coordinator.performWithType(.qrCode)
        
        let fakeController = try XCTUnwrap(navigationController.pushedViewController)
        XCTAssertTrue(fakeController is QrCodeScanning)
    }
    
    func testPerformWithType_WhenTypeIsManualInsert_ShouldPushReceiveLoadingViewController() throws {
        coordinator.performWithType(.receivePayment)
        
        let fakeController = try XCTUnwrap(navigationController.pushedViewController)
        XCTAssertTrue(fakeController is ReceiveLoadingViewController)
    }
    
    func testPerformWithType_WhenTypeIsDailyLimit_ShouldPushDailyLimitViewController() throws {
        coordinator.performWithType(.dailyLimit)
        
        let fakeController = try XCTUnwrap(navigationController.pushedViewController)
        XCTAssertTrue(fakeController is DailyLimitViewController)
    }
    
    // MARK: - close
    func testClose_ShouldDismissViewController() {
        coordinator.close()
        XCTAssertEqual(viewController.dismissCallCount, 1)
    }
}
