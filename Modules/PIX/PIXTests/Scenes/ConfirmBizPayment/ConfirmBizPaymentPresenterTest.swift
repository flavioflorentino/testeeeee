@testable import PIX
import UI
import UIKit
import XCTest

private final class ConfirmBizPaymentPresenterTestCoordinatorSpy: ConfirmBizPaymentCoordinating {
    var viewController: UIViewController?
    private(set) var action: ConfirmBizPaymentAction?
    private(set) var count = 0
    private(set) var didClose = false
    
    func perform(action: ConfirmBizPaymentAction) {
        self.action = action
        count += 1
    }
    
    func close() {
        self.didClose = true
    }
}

private final class ConfirmBizPaymentViewControllerTestSpy: ConfirmBizPaymentDisplaying {
    var models = [ConfirmCellModel]()
    private(set) var callDisplayTitle = 0
    private(set) var callDisplayCells = 0
    private(set) var callDisplayTableViewHeaderFooter = 0
    private(set) var callShowError = 0
    private(set) var callInput = 0
    private(set) var callStartLoading = 0
    private(set) var callStopLoading = 0

    func displayTitle(_ title: String) {
        callDisplayTitle += 1
    }
    
    func displayCells(with models: [ConfirmCellModel]) {
        self.models = models
        callDisplayCells += 1
    }
    
    func displayTableViewHeaderFooter(with model: AvatarTitleDetailsHeaderModel, title: String, placeholder: String) {
        callDisplayTableViewHeaderFooter += 1
    }
    
    func showError(title: String, message: String, buttonTitle: String) {
        callShowError += 1
    }
    
    func input(action: ConfirmBizPaymentInputAction) {
        callInput += 1
    }
    
    func startLoading() {
        callStartLoading += 1
    }
    
    func stopLoading() {
        callStopLoading += 1
    }
}

final class ConfirmBizPaymentPresenterTestTests: XCTestCase {
    // MARK: - Variables
    private let viewController = ConfirmBizPaymentViewControllerTestSpy()
    private let coordinator = ConfirmBizPaymentPresenterTestCoordinatorSpy()
    private lazy var sut: ConfirmBizPaymentPresenting = {
        let dependencies = DependencyContainerMock()
        let presenter = ConfirmBizPaymentPresenter(coordinator: coordinator,
                                                   flowType: .send(pixParams: .init(originType: .key,
                                                                                    keyType: "",
                                                                                    key: "",
                                                                                    receiverData: nil)))
        presenter.viewController = viewController
        return presenter
    }()
    
    func testSetupTableView_WhenExistDynamicCells_ShouldCreateModels() {
        let confirmCell = ConfirmCellValueModel(with: "", and: "")
        let confirmCell2 = ConfirmCellValueModel(with: "", and: "")
        let confirmCell3 = ConfirmCellSeparatortModel()
        let confirmCell4 = ConfirmCellEmptyViewModel()
        let dynamicCells = [[confirmCell, confirmCell, confirmCell2, confirmCell3, confirmCell4]]
        sut.setupTableView(with: 0, dynamicCells: dynamicCells)
        
        let empyCell = ConfirmCellEmptyViewModel()
        let valueModel = ConfirmCellValueModel(with: Strings.ConfirmPaymentBiz.Cell.valueTitle, and: "")
        let dateModel = ConfirmCellDetailModel(with: Strings.ConfirmPaymentBiz.Cell.dateTitle, and: "")
        let paymentModel = ConfirmCellDetailModel(with: Strings.ConfirmPaymentBiz.Cell.paymentTitle,
                                                  and: Strings.ConfirmPaymentBiz.Cell.paymentValue)
        
        let dynamicModels: [ConfirmCellModel] = [ConfirmCellSeparatortModel()] + dynamicCells.flatMap { $0 }
        let confirmCellModels: [ConfirmCellModel]  = [empyCell] + [valueModel, dateModel, paymentModel] + dynamicModels
        XCTAssertEqual(viewController.models, confirmCellModels)
    }
}
