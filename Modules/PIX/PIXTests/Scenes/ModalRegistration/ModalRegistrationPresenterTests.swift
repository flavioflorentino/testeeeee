@testable import PIX
import UI
import XCTest

private final class ModalRegistrationCoordinatorSpy: ModalRegistrationCoordinating {
    var viewController: UIViewController?
    weak var delegate: ModalRegistrationDelegate?
    
    // MARK: - perform
    private(set) var performCallsCount = 0
    private(set) var action: ModalRegistrationAction?
    
    func perform(action: ModalRegistrationAction) {
        performCallsCount += 1
        self.action = action
    }
}

private final class ModalRegistrationViewControllerSpy: ModalRegistrationDisplay {
    // MARK: - displayDetails
    private(set) var displayDetailsCallsCount = 0
    private(set) var details = [PixModalDetailItem]()
    
    func displayDetails(items: [PixModalDetailItem]) {
        displayDetailsCallsCount += 1
        details = items
    }
    
    // MARK: - displayRegisterButton
    private(set) var displayRegisterButtonCallsCount = 0
    
    func displayRegisterButton() {
        displayRegisterButtonCallsCount += 1
    }
    
    // MARK: - displayCloseButton
    private(set) var displayCloseButtonCallsCount = 0
    
    func displayCloseButton() {
        displayCloseButtonCallsCount += 1
    }
}

final class ModalRegistrationPresenterTests: XCTestCase {
    private let coordinatorSpy = ModalRegistrationCoordinatorSpy()
    private let viewControllerSpy = ModalRegistrationViewControllerSpy()
    private lazy var sut: ModalRegistrationPresenting = {
        let presenter = ModalRegistrationPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testPresentDetail_ShouldDisplayDetails() {
        sut.presentDetails()
        
        XCTAssertEqual(viewControllerSpy.displayDetailsCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.details.count, 3)
        XCTAssertEqual(viewControllerSpy.details[0].icon, Iconography.moneyStack)
        XCTAssertEqual(viewControllerSpy.details[0].text, Strings.Modal.Item.payAndReceive)
        XCTAssertEqual(viewControllerSpy.details[1].icon, Iconography.keySkeleton)
        XCTAssertEqual(viewControllerSpy.details[1].text, Strings.Modal.Item.youCanUse)
        XCTAssertEqual(viewControllerSpy.details[2].icon, Iconography.moneyBillSlash)
        XCTAssertEqual(viewControllerSpy.details[2].text, Strings.Modal.Item.noTaxes)
    }
    
    func testPresentRegisterButton_ShouldDisplayRegisterButton() {
        sut.presentRegisterButton()
        
        XCTAssertEqual(viewControllerSpy.displayRegisterButtonCallsCount, 1)
    }
    
    func testPresentCloseButton_ShouldDisplayCloseButton() {
        sut.presentCloseButton()
        
        XCTAssertEqual(viewControllerSpy.displayCloseButtonCallsCount, 1)
    }
    
    func testDidNextStep_WhenActionIsClose_ShouldPerformCloseAction() {
        sut.didNextStep(action: .close)
        
        XCTAssertEqual(coordinatorSpy.performCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .close)
    }
    
    func testDidNextStep_WhenActionIsRegister_ShouldPerformRegisterAction() {
        sut.didNextStep(action: .register)
        
        XCTAssertEqual(coordinatorSpy.performCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .register)
    }
}
