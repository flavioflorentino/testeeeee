import AnalyticsModule
@testable import PIX
import XCTest

private final class ModalRegistrationServiceSpy: ModalRegistrationServicing {
    // MARK: - setModalHomeVisualized
    private(set) var setModalHomeVisualizedCallsCount = 0
    
    func setModalHomeVisualized() {
        setModalHomeVisualizedCallsCount += 1
    }
}

private final class ModalRegistrationPresenterSpy: ModalRegistrationPresenting {
    var viewController: ModalRegistrationDisplay?
    
    // MARK: - presentDetails
    private(set) var presentDetailsCallsCount = 0
    
    func presentDetails() {
        presentDetailsCallsCount += 1
    }
    
    // MARK: - presentRegisterButton
    private(set) var presentRegisterButtonCallsCount = 0
    
    func presentRegisterButton() {
        presentRegisterButtonCallsCount += 1
    }
    
    // MARK: - presentCloseButton
    private(set) var presentCloseButtonCallsCount = 0
    
    func presentCloseButton() {
        presentCloseButtonCallsCount += 1
    }
    
    // MARK: - didNextStep
    private(set) var didNextStepCallsCount = 0
    private(set) var action: ModalRegistrationAction?
    
    func didNextStep(action: ModalRegistrationAction) {
        didNextStepCallsCount += 1
        self.action = action
    }
}

final class ModalRegistrationInteractorTests: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    private let presenterSpy = ModalRegistrationPresenterSpy()
    private let serviceSpy = ModalRegistrationServiceSpy()
    private lazy var sut: ModalRegistrationInteracting = {
        let interactor = ModalRegistrationInteractor(
            service: serviceSpy,
            presenter: presenterSpy,
            dependencies: DependencyContainerMock(analyticsSpy)
        )
        return interactor
    }()
    
    func testFetchModalInformation_ShouldPresentDetailsAndRegisterButtonAndCloseButton() {
        sut.fetchModalInformation()
        
        XCTAssertEqual(presenterSpy.presentDetailsCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentRegisterButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentCloseButtonCallsCount, 1)
        XCTAssertEqual(serviceSpy.setModalHomeVisualizedCallsCount, 1)
    }
    
    func testDidRegister_ShouldPresentNextViewController() {
        let expectedEvent = ModalRegistrationEvent.dialogInteracted(action: .register, title: Strings.Modal.title).event()
        sut.didRegister()
        
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .register)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testDidClose_ShouldDismissViewController() {
        let expectedEvent = ModalRegistrationEvent.dialogInteracted(action: .close, title: Strings.Modal.title).event()
        sut.didClose()
        
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .close)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
}
