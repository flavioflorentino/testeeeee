@testable import PIX
import XCTest

private final class ModalRegistrationDelegateMock: ModalRegistrationDelegate {
    // MARK: - didTapRegisterKeyOnPix
    private(set) var didTapRegisterKeyOnPixCallsCount = 0
    
    func didTapRegisterKeyOnPix() {
        didTapRegisterKeyOnPixCallsCount += 1
    }
}

final class ModalRegistrationCoordinatorTests: XCTestCase {
    private let viewController = SpyViewController()
    private let delegateMock = ModalRegistrationDelegateMock()
    
    private lazy var sut: ModalRegistrationCoordinating = {
        let coordinator = ModalRegistrationCoordinator()
        coordinator.viewController = viewController
        coordinator.delegate = delegateMock
        return coordinator
    }()
    
    func testPerform_WhenActionIsClose_ShouldDismissViewController() {
        sut.perform(action: .close)
        
        XCTAssertEqual(viewController.dismissCallCount, 1)
    }
    
    func testPerform_WhenActionIsRegister_ShouldDismissViewControllerAndCallDelegate() {
        sut.perform(action: .register)
        
        XCTAssertEqual(viewController.dismissCallCount, 1)
        XCTAssertEqual(delegateMock.didTapRegisterKeyOnPixCallsCount, 1)
    }
}
