import AnalyticsModule
import XCTest
import Core
@testable import PIX

private final class CreateBizKeyPresenterSpy: CreateBizKeyPresenting {
    var viewController: CreateBizKeyDisplaying?
    private(set) var startLoadingCallsCount = 0
    private(set) var stopLoadingCallsCount = 0
    private(set) var presentErrorCallsCount = 0
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didPresentGenericError = 0
    private(set) var didPresentClaimFlow = 0
    private(set) var didPresentPortabilityFlow = 0
    private(set) var didPresentNotRegisteredKeyError = 0
    private(set) var presentErrorReceived: ApiError?
    private(set) var createKey: CreateKey?
    private(set) var didNextStepActionReceived: CreateBizKeyOutputAction?
    
    func startLoading() {
        startLoadingCallsCount += 1
    }
    
    func stopLoading() {
        stopLoadingCallsCount += 1
    }
    
    func presentGenericError() {
        didPresentGenericError += 1
    }
    
    func presentNotRegisteredKeyError() {
        didPresentNotRegisteredKeyError += 1
    }
    
    func presentClaimFlow(createKey: CreateKey) {
        self.createKey = createKey
        didPresentClaimFlow += 1
    }
    
    func presentPortabilityFlow(createKey: CreateKey) {
        self.createKey = createKey
        didPresentPortabilityFlow += 1
    }

    func didNextStep(action: CreateBizKeyOutputAction) {
        didNextStepActionCallsCount += 1
        didNextStepActionReceived = action
    }
}

final class CreateBizKeyInteractorTests: XCTestCase {
    // MARK: - Variables
    private let analytics = AnalyticsSpy()
    private let serviceMock = PixBizKeyServiceMock()
    private let presenterSpy = CreateBizKeyPresenterSpy()
    private let key = CreateKey(userId: "1", userType: .company, value: "515.587.726-18", type: .cpf)
    private let userInfo = KeyManagerBizUserInfo(name: "", cnpj: "24.894.589/0001-87", mail: "miralta@picpay.com", phone: "(27) 91222 1929", userId: "1")
    private lazy var sut = CreateBizKeyInteractor(userInfo, dependencies: DependencyContainerMock(analytics),
                                                  service: serviceMock,
                                                  presenter: presenterSpy,
                                                  key: key,
                                                  authenticationHash: nil)
    
    func testCreateKey_WhenServiceSuccess_ShouldPresentStopLoadingAndPresentDidNextStepWithPendingAction() throws {
        let decoder = JSONDecoder()
        let data = try XCTUnwrap(Mocker().data("createKey_process_pending"))
        let requestSuccess = try XCTUnwrap(decoder.decode(PixKeyData.self, from: data))
        serviceMock.createNewPixKeyResult = .success(requestSuccess)
        sut.createKey()
        XCTAssertEqual(presenterSpy.startLoadingCallsCount, 1)
        XCTAssertEqual(serviceMock.createNewPixKeyCallsCount, 1)
        XCTAssertEqual(serviceMock.keyReceived, key)
        XCTAssertEqual(presenterSpy.stopLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceived, .pending(pixKey: requestSuccess.data))
    }
    
    func testCreateKey_WhenServiceSuccessAndHandleSucess_ShouldPresentPendingAction() throws {
        let decoder = JSONDecoder()
        let data = try XCTUnwrap(Mocker().data("createKey_processed"))
        let requestSuccess = try XCTUnwrap(decoder.decode(PixKeyData.self, from: data))
        serviceMock.createNewPixKeyResult = .success(requestSuccess)
        sut.createKey()
        
        XCTAssertEqual(presenterSpy.startLoadingCallsCount, 1)
        XCTAssertEqual(serviceMock.createNewPixKeyCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceived, .pending(pixKey: requestSuccess.data))
    }
    
    func testCreateKey_WhenServiceFailure_ShouldPresentError() {
        let error = ApiError.bodyNotFound
        serviceMock.createNewPixKeyResult = .failure(error)
        sut.createKey()
        
        XCTAssertEqual(presenterSpy.startLoadingCallsCount, 1)
        XCTAssertEqual(serviceMock.createNewPixKeyCallsCount, 1)
        XCTAssertEqual(serviceMock.keyReceived, key)
        XCTAssertEqual(presenterSpy.didPresentNotRegisteredKeyError, 1)
    }
    
    func testDismiss_WhenCalled_ShouldPresentDidNextStepWithDismissAction() {
        sut.dismiss()
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceived, .dismiss)
    }
}
