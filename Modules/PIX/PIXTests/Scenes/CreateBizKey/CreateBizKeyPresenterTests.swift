@testable import PIX
import UI
import AssetsKit
import Core
import XCTest

private final class CreateBizKeyCoordinatorSpy: CreateBizKeyCoordinating {
    private(set) var performActionCallsCount = 0
    private(set) var performActionReceived: CreateBizKeyOutputAction?

    func perform(action: CreateBizKeyOutputAction) {
        performActionCallsCount += 1
        performActionReceived = action
    }
}

private final class CreateBizKeyViewControllerSpy: CreateBizKeyDisplaying {
    private(set) var startLoadingCallsCount = 0
    private(set) var stopLoadingCallsCount = 0
    private(set) var displayErrorCallsCount = 0
    private(set) var displayErrorReceived: CreateBizKeyViewModel?
    private(set) var displayClaimPopupPopupCallsCount = 0
    private(set) var displayClaimPopupPopupReceived: CreateBizKeyViewModel?
    private(set) var displayPortabilityPopupPopupCallsCount = 0
    private(set) var displayPortabilityPopupPopupReceived: CreateBizKeyViewModel?

    func startLoading() {
        startLoadingCallsCount += 1
    }
    
    func stopLoading() {
        stopLoadingCallsCount += 1
    }
    
    func display(error: CreateBizKeyViewModel) {
        displayErrorCallsCount += 1
        displayErrorReceived = error
    }

    func displayClaimPopup(popup: CreateBizKeyViewModel) {
        displayClaimPopupPopupCallsCount += 1
        displayClaimPopupPopupReceived = popup
    }

    func displayPortabilityPopup(popup: CreateBizKeyViewModel) {
        displayPortabilityPopupPopupCallsCount += 1
        displayPortabilityPopupPopupReceived = popup
    }
}

final class CreateBizKeyPresenterTests: XCTestCase {
    private let coordinatorSpy = CreateBizKeyCoordinatorSpy()
    private let viewControllerSpy = CreateBizKeyViewControllerSpy()
    private lazy var sut: CreateBizKeyPresenter = {
        let presenter = CreateBizKeyPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    private lazy var pixKey = PixKey(id: "", statusSlug: .automaticConfirmed, status: "", name: "", keyType: .cnpj, createdAt: "", keyValue: "")
    
    func testStartLoading_WhenCalled_ShouldDisplayStartLoading() {
        sut.startLoading()
        
        XCTAssertEqual(viewControllerSpy.startLoadingCallsCount, 1)
    }
    
    func testStopLoading_WhenCalled_ShouldDisplayStopLoading() {
        sut.stopLoading()
        XCTAssertEqual(viewControllerSpy.stopLoadingCallsCount, 1)
    }
    

    func testDidNextStepAction_WhenCalledWithSuccess_ShouldCoordinatorPerformWithPendingAction() {
        let action = CreateBizKeyOutputAction.pending(pixKey: pixKey)
        sut.didNextStep(action: action)

        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.performActionReceived, action)
    }
    
    func testHandleError_WhenRequestFailureWithBadRequest_ShouldPerfomCoordinatorWithGenericError() throws {
        let decoder = JSONDecoder()
        let data = try XCTUnwrap(Mocker().data("badRequest_error"))
        let requestError = try XCTUnwrap(decoder.decode(RequestError.self, from: data))
        let error = ApiError.badRequest(body: requestError)
        let createKey = CreateKey()
        sut.present(createKey: createKey, error: error)
        let expectedViewModel = CreateBizKeyViewModel(image: Resources.Illustrations.iluConstruction.image,
                                                             title: Strings.KeyManager.error,
                                                             description: Strings.KeyManager.errorDescription,
                                                             buttonTitle: Strings.KeyManager.errorAction)
        XCTAssertEqual(viewControllerSpy.displayErrorCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.displayErrorReceived, expectedViewModel)
    }
    
    func testHandleError_WhenRequestFailureWithPortabiltiy_ShouldPerfomCoordinatorWithPortability() throws {
        let decoder = JSONDecoder()
        let data = try XCTUnwrap(Mocker().data("portability_error"))
        let requestError = try XCTUnwrap(decoder.decode(RequestError.self, from: data))
        let error = ApiError.badRequest(body: requestError)
        
        let createKey = CreateKey()
        sut.present(createKey: createKey, error: error)
        XCTAssertEqual(coordinatorSpy.performActionReceived, .portability(key: createKey))
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
    }
    
    func testHandleError_WhenRequestFailureWithClaimError_ShouldPerfomCoordinatorWithClaim() throws {
        let decoder = JSONDecoder()
        let data = try XCTUnwrap(Mocker().data("claim_error"))
        let requestError = try XCTUnwrap(decoder.decode(RequestError.self, from: data))
        let error = ApiError.badRequest(body: requestError)
        
        let createKey = CreateKey()
        sut.present(createKey: createKey, error: error)
        XCTAssertEqual(coordinatorSpy.performActionReceived, .claim(key: createKey))
    }
}

extension CreateKey {
    init() {
        self.init(userId: "", userType: .company, value: "", type: .cnpj, name: "")
    }
}
