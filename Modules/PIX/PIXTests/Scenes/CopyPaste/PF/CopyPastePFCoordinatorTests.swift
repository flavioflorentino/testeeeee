import XCTest
@testable import PIX

final class CopyPastePFCoordinatorTests: XCTestCase {
    private lazy var viewController: SpyViewController = {
        let controller = SpyViewController()
        self.navigationController.pushViewController(controller, animated: false)
        return controller
    }()
    private let navigationController = SpyNavigationController()
    private let paymentOpening = FakePaymentOpening()
    private lazy var coordinator: CopyPastePFCoordinating = {
        let coordinator = CopyPastePFCoordinator(paymentOpening: paymentOpening)
        coordinator.viewController = viewController
        return coordinator

    }()

    // MARK: - goToPaymentDetailsWithPaymentDetails
    func testGoToPaymentDetailsWithPaymentDetails_ShouldCallOpenPaymentWithDetails() throws {
        let payee = PIXPayee(name: "name", description: "description", imageURLString: nil, id: nil)
        let params = PIXParams(originType: .qrCode, keyType: "QRCODE", key: nil, receiverData: nil)

        let details = PIXPaymentDetails(
            title: "title",
            screenType: .qrCode(
                items: [[QRCodeAcessoryPaymentModel(title: "title", value: "value")]]
            ),
            payee: payee,
            pixParams: params
        )

        coordinator.goToPayment(with: details)
        XCTAssertEqual(paymentOpening.details, details)
        XCTAssert(paymentOpening.controller === viewController)
        XCTAssertEqual(paymentOpening.openPaymentWithDetailsCallsCount, 1)
    }
}
