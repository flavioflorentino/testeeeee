import AssetsKit
@testable import PIX
import UI
import XCTest

private final class CopyPastePFCoordinatorSpy: CopyPastePFCoordinating {
    var viewController: UIViewController?
    private(set) var performCallsCount = 0
    private(set) var action: CopyPastePFAction?
    private(set) var goToPaymentWithDetailsCallsCount = 0
    private(set) var details: PIXPaymentDetails?

    func perform(action: CopyPastePFAction) {
        performCallsCount += 1
        self.action = action
    }
    
    func goToPayment(with details: PIXPaymentDetails) {
        self.details = details
        goToPaymentWithDetailsCallsCount += 1
    }
}

private final class CopyPastePFViewControllerSpy:  CopyPastePFDisplaying {
    private(set) var displayErrorTitleSubtitleActionCallsCount = 0
    private(set) var title: String?
    private(set) var subtitle: String?
    private(set) var actions: [PopupAction]?
    private(set) var displayDialogWithAlertDataCallsCount = 0
    private(set) var alertData: StatusAlertData?
    private(set) var displayEndStateCallsCount = 0
    private(set) var enableForwardButtonCallsCount = 0
    private(set) var disableForwardButtonCallsCount = 0
    private(set) var displayCopiedCodeCallsCount = 0
    private(set) var copiedCode: String?

    func displayError(title: String, subtitle: String, actions: [PopupAction]) {
        displayErrorTitleSubtitleActionCallsCount += 1
        self.title = title
        self.subtitle = subtitle
        self.actions = actions
    }

    func displayDialog(with alertData: StatusAlertData) {
        displayDialogWithAlertDataCallsCount += 1
        self.alertData = alertData
    }

    func displayEndState() {
        displayEndStateCallsCount += 1
    }

    func enableForwardButton() {
        enableForwardButtonCallsCount += 1
    }

    func disableForwardButton() {
        disableForwardButtonCallsCount += 1
    }
    
    func displayCopiedCode(code: String) {
        displayCopiedCodeCallsCount += 1
        copiedCode = code
    }
}

final class CopyPastePFPresenterTests: XCTestCase {
    // MARK: - Variables
    private let coordinator = CopyPastePFCoordinatorSpy()
    private let viewController = CopyPastePFViewControllerSpy()
    private lazy var presenter: CopyPastePFPresenting = {
        let presenter = CopyPastePFPresenter(coordinator: coordinator)
        presenter.viewController = viewController
        return presenter
    }()
    
    // MARK: - didNextStep
    func testDidNextStep_ShouldCallPerform() {
        presenter.didNextStep(action: .close)
        XCTAssertEqual(coordinator.performCallsCount, 1)
        XCTAssertEqual(coordinator.action, .close)
    }

    // MARK: - presentPaymentDetailsWithAccount
    func testPresentPaymentWithDetails_ShouldCallGoToPaymentWithDetails() {
        let payerData = [QRCodeAcessoryPaymentModel(title: "title", value: "value")]
        let receiverData = [QRCodeAcessoryPaymentModel(title: "title", value: "value")]
        let qrCodeInfos = PixQRCodeInfo(payerData: payerData, receiverData: receiverData)
        let account = PixQrCodeAccount(
            name: "Name",
            cpfCnpj: "cpfCnpj",
            bank: "bank",
            imageUrl: nil,
            id: nil,
            qrcodeInfos: qrCodeInfos,
            value: 10.0, editable: false)
        let key = "key"
        let payee = PIXPayee(name: account.name, description: "description", imageURLString: nil, id: nil)
        let params = PIXParams(originType: .qrCode, keyType: "QRCODE", key: key, receiverData: nil)

        let details = PIXPaymentDetails(
            title: "title",
            screenType: .default(commentPlaceholder: "Description"),
            payee: payee,
            pixParams: params
        )

        presenter.presentPayment(with: details)

        XCTAssertEqual(coordinator.details, details)
        XCTAssertEqual(viewController.displayEndStateCallsCount, 1)
        XCTAssertEqual(coordinator.goToPaymentWithDetailsCallsCount, 1)
    }

    // MARK: - presentGenericError
    func testPresentGenericError_ShouldCallDisplayDialogWithAlertData() {
        presenter.presentGenericError()
        XCTAssertEqual(viewController.displayDialogWithAlertDataCallsCount, 1)
        XCTAssertNotNil(viewController.alertData)
    }

    // MARK: - presentErrorTitleSubtitleJsonButtons
    func testPresentErrorTitleSubtitleJsonButtons_ShouldCallDisplayErrorTitleSubtitleActions() {
        let title = "Title"
        let subtitle = "Subtitle"
        let actionTitle = "ActionTitle"
        let jsonButtons = [["title": actionTitle]]
        presenter.presentError(title: title, subtitle: subtitle, jsonButtons: jsonButtons)

        XCTAssertEqual(viewController.displayErrorTitleSubtitleActionCallsCount, 1)
        XCTAssertEqual(viewController.title, title)
        XCTAssertEqual(viewController.subtitle, subtitle)
        XCTAssertEqual(viewController.actions?.first?.currentTitle, actionTitle)
    }

    // MARK: - enableForwardButton
    func testEnableForwardButton_ShouldCallEnableForwardButton() {
        presenter.enableForwardButton()
        XCTAssertEqual(viewController.enableForwardButtonCallsCount, 1)
    }

    // MARK: - disableForwardButton
    func testDisableForwardButton_ShouldCallDisableForwardButton() {
        presenter.disableForwardButton()
        XCTAssertEqual(viewController.disableForwardButtonCallsCount, 1)
    }
    
    // MARK: - presentCopiedCode
    func testPresentCopiedCode_ShouldDisplayCopiedCode() {
        let code = "br.gov.bcb.pix"
        presenter.presentCopiedCode(code: code)
        XCTAssertEqual(viewController.displayCopiedCodeCallsCount, 1)
        XCTAssertEqual(viewController.copiedCode, code)
    }
}
