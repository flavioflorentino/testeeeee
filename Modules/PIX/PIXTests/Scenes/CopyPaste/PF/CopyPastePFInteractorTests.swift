import AnalyticsModule
import Core
@testable import PIX
import XCTest

private final class PixQrCodeServiceSpy: PixQrCodeServicing {
    var result: Result<PixQrCodeAccount, ApiError> = .failure(.unknown(nil))
    private(set) var validateCallsCount = 0
    private(set) var scannedText: String?

    func validate(scannedText: String, completion: @escaping (Result<PixQrCodeAccount, ApiError>) -> Void) {
        validateCallsCount += 1
        self.scannedText = scannedText
        completion(result)
    }
}

private final class CopyPastePFPresenterSpy: CopyPastePFPresenting {
    var viewController: CopyPastePFDisplaying?
    private(set) var didNextStepCallsCount = 0
    private(set) var action: CopyPastePFAction?
    private(set) var presentPaymentCallsCount = 0
    private(set) var details: PIXPaymentDetails?
    private(set) var presentGenericErrorCallsCount = 0
    private(set) var presentErrorTitleSubtitleJsonButtonsCallsCount = 0
    private(set) var title: String?
    private(set) var subtitle: String?
    private(set) var enableForwardButtonCallsCount = 0
    private(set) var disableForwardButtonCallsCount = 0
    private(set) var presentCopiedCodeCallsCount = 0
    private(set) var copiedCode: String?
    
    func didNextStep(action: CopyPastePFAction) {
        didNextStepCallsCount += 1
        self.action = action
    }
    
    func presentPayment(with details: PIXPaymentDetails) {
        presentPaymentCallsCount += 1
        self.details = details
    }

    func presentGenericError() {
        presentGenericErrorCallsCount += 1
    }

    func presentError(title: String, subtitle: String, jsonButtons: [[String : Any]]) {
        presentErrorTitleSubtitleJsonButtonsCallsCount += 1
        self.title = title
        self.subtitle = subtitle
    }

    func enableForwardButton() {
        enableForwardButtonCallsCount += 1
    }

    func disableForwardButton() {
        disableForwardButtonCallsCount += 1
    }
    
    func presentCopiedCode(code: String) {
        presentCopiedCodeCallsCount += 1
        copiedCode = code
    }
}

final class CopyPastePFInteractorTests: XCTestCase {
    // MARK: - Variables
    private let service = PixQrCodeServiceSpy()
    private let presenter = CopyPastePFPresenterSpy()
    private let analytics = AnalyticsSpy()
    private lazy var interactor: CopyPastePFInteracting = CopyPastePFInteractor(
        presenter: presenter,
        service: service, origin: .hubScreen,
        copiedCode: String(),
        dependencies: DependencyContainerMock(analytics)
    )

    // MARK: - callScreenAnalytics
    func testCallScreenAnalytics_ShouldCallAnalytics() {
        interactor.callScreenAnalytics()
        XCTAssertTrue(analytics.equals(to: PixNavigationEvent.userNavigated(from: .hubScreen, to: .sendCopyPasteScreen).event()))
    }

    // MARK: - validateBrCode
    func testValidateBrCode_WhenItIsAValidBrCode_ShouldCallPresentEnableForwardButton() throws {
        let code = "TesteTestebr.gov.bcb.pixTesteTeste"
        interactor.validateBrCode(code)
        XCTAssertEqual(presenter.enableForwardButtonCallsCount, 1)
    }

    func testValidateBrCode_WhenItIsANotValidBrCode_ShouldCallPresentDisableForwardButton() throws {
        let code = "TesteTesteTesteTeste"
        interactor.validateBrCode(code)
        XCTAssertEqual(presenter.disableForwardButtonCallsCount, 1)
    }

    // MARK: - forwardAction
    func testForwardAction_WhenServiceReturnsUnknown_ShouldCallPresentGenericError() throws {
        interactor.forwardAction(withCode: "code")
        XCTAssertEqual(presenter.presentGenericErrorCallsCount, 1)
    }

    func testForwardAction_WhenResultIsFailureWithApiErrorWithUI_ShouldCallPresentError() throws {
        let decoder = JSONFileDecoder<RequestError>()
        let requestError = try decoder.load(
            resource: "RequestErrorUIAlert",
            typeDecoder: .convertFromSnakeCase
        )

        let apiError = ApiError.otherErrors(body: requestError)
        service.result = .failure(apiError)
        interactor.forwardAction(withCode: "code")

        XCTAssertEqual(presenter.presentErrorTitleSubtitleJsonButtonsCallsCount, 1)
        XCTAssertEqual(presenter.title, "Transação não concluída")
        XCTAssertEqual(presenter.subtitle, "Falha na comunicação.\nTente novamente")
    }

    func testForwardAction_WhenResultIsSuccess_ShouldCallPresentPaymentDetailsWithAccountKeyTypeKey() throws {
        let payerData = [QRCodeAcessoryPaymentModel(title: "Title", value: "Value")]
        let receiverData = [QRCodeAcessoryPaymentModel(title: "Title", value: "Value")]
        let value = 10.0
        let name = "Name"
        let cpfCnpj = "cpfCnpj"
        let bank = "bank"

        let account = PixQrCodeAccount(
            name: name,
            cpfCnpj: cpfCnpj,
            bank: bank,
            imageUrl: nil,
            id: nil,
            qrcodeInfos: PixQRCodeInfo(
                payerData: payerData,
                receiverData: receiverData
            ),
            value: value,
            editable: false
        )
        service.result = .success(account)

        let payee = PIXPayee(
            name: name,
            description: "\(cpfCnpj) | \(bank)",
            imageURLString: nil,
            id: nil
        )

        let params = PIXParams(originType: .qrCode, keyType: "QRCODE", key: "code", receiverData: nil)

        let items = [payerData, receiverData]

        let details = PIXPaymentDetails(
            title: "Enviar pagamento",
            screenType: .qrCode(items: items),
            payee: payee,
            pixParams: params,
            value: value,
            isEditable: false
        )


        interactor.forwardAction(withCode: "code")
        XCTAssertEqual(presenter.presentPaymentCallsCount, 1)
        XCTAssertEqual(presenter.details, details)
    }
    
    // MARK: - close
    func testClose_ShouldCallDidNextStep() throws {
        interactor.close()
        XCTAssertEqual(presenter.didNextStepCallsCount, 1)
        XCTAssertEqual(presenter.action, .close)
    }
    
    // MARK: - showCopiedCode
    func testShowCopiedCode_WhenUserDidNotCopiedAValidCode_ShouldNotCallPresentCopiedCode() {
        interactor.showCopiedCode()
        XCTAssertEqual(presenter.presentCopiedCodeCallsCount, 0)
    }
    
    func testShowCopiedCode_WhenUserCopiedAValidCode_ShouldCallPresentCopiedCode() {
        let copiedCode = "br.gov.bcb.pix"
        let interactorWithValidCode: CopyPastePFInteracting = CopyPastePFInteractor(
            presenter: presenter,
            service: service, origin: .undefined,
            copiedCode: copiedCode,
            dependencies: DependencyContainerMock(analytics)
        )
        interactorWithValidCode.showCopiedCode()
        XCTAssertEqual(presenter.presentCopiedCodeCallsCount, 1)
        XCTAssertEqual(presenter.copiedCode, copiedCode)
    }
}
