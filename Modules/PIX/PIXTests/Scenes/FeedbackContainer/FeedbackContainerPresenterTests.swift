import Foundation
@testable import PIX
import XCTest

private final class SecurityValidationCodeDelegateMock: SecurityValidationCodeDelegate {
    func didValidateKeyWithSuccess(keyValue: String, keyType: PixKeyType) {}
}

private final class FeedbackContainerCoordinatorSpy: FeedbackContainerCoordinating {
    var delegate: FeedbackContainterDelegate?
    
    var viewController: UIViewController?
    
    // MARK: - perform
    private(set) var performCallsCount = 0
    private(set) var action: FeedbackContainerAction?
    
    func perform(action: FeedbackContainerAction) {
        performCallsCount += 1
        self.action = action
    }
}

private final class FeedbackContainerViewControllerSpy: FeedbackContainerDisplaying {
    // MARK: - displayLoaderView
    private(set) var displayLoaderViewCallsCount = 0
    
    func displayLoaderView() {
        displayLoaderViewCallsCount += 1
    }
    
    // MARK: - hideLoaderView
    private(set) var hideLoaderViewCallsCount = 0
    
    func hideLoaderView() {
        hideLoaderViewCallsCount += 1
    }
    
    // MARK: - disableCloseButton
    private(set) var disableCloseButtonCallsCount = 0
    
    func disableCloseButton() {
        disableCloseButtonCallsCount += 1
    }
    
    // MARK: - enableCloseButton
    private(set) var enableCloseButtonCallsCount = 0
    
    func enableCloseButton() {
        enableCloseButtonCallsCount += 1
    }
    
    // MARK: - displayFeedbackView
    private(set) var displayFeedbackViewCallsCount = 0
    private(set) var type: FeedbackViewType?
    private(set) var originOfDisplayedFeedback: PixUserNavigation?
    
    func displayFeedbackView(type: FeedbackViewType, from origin: PixUserNavigation) {
        displayFeedbackViewCallsCount += 1
        self.type = type
        originOfDisplayedFeedback = origin
    }
    
    // MARK: - displayError
    private(set) var displayErrorCallsCount = 0
    private(set) var titleDisplayError: String?
    private(set) var messageDisplayError: String?
    private(set) var buttonTextDisplayError: String?
    
    func displayError(title: String, message: String, buttonText: String) {
        displayErrorCallsCount += 1
        titleDisplayError = title
        messageDisplayError = message
        buttonTextDisplayError = buttonText
    }
}

final class FeedbackContainerPresenterTests: XCTestCase {
    private let coordinatorSpy = FeedbackContainerCoordinatorSpy()
    private let viewControllerSpy = FeedbackContainerViewControllerSpy()
    
    private lazy var sut: FeedbackContainerPresenting = {
        let presenter = FeedbackContainerPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testPresentLoading_ShouldDisplayLoaderView() {
        sut.presentLoading()
        
        XCTAssertEqual(viewControllerSpy.displayLoaderViewCallsCount, 1)
    }
    
    func testHideLoading_ShouldHideLoaderView() {
        sut.hideLoading()
        
        XCTAssertEqual(viewControllerSpy.hideLoaderViewCallsCount, 1)
    }
    
    func testDisableCloseButton_ShoulDisableCloseButton() {
        sut.disableCloseButton()
        
        XCTAssertEqual(viewControllerSpy.disableCloseButtonCallsCount, 1)
    }
    
    func testEnableCloseButton_ShoulEnableCloseButtons() {
        sut.enableCloseButton()
        
        XCTAssertEqual(viewControllerSpy.enableCloseButtonCallsCount, 1)
    }
    
    func testPresentFeedback_WhenPassTypeToViewController_ShouldPresentFeedbackWithType() {
        let type: FeedbackViewType = .portabilityDonorReceived(uuid: "uuid")
        
        sut.presentFeedback(type: type, from: .undefined)
        
        XCTAssertEqual(viewControllerSpy.displayFeedbackViewCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.type, type)
    }
    
    func testDidNextStep_WhenActionIsClose_ShouldPassActionToCoordinator() {
        let action: FeedbackContainerAction = .close(feedbackType: nil)
        
        sut.didNextStep(action: action)
        
        XCTAssertEqual(coordinatorSpy.performCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.action, action)
    }
    
    func testDidNextStep_WhenActionIsShowFeedback_ShouldPassActionToCoordinator() {
        let action: FeedbackContainerAction = .showFeedback(type: .portabilityDonorCompleted(uuid: "uuid"), from: .undefined)
        
        sut.didNextStep(action: action)
        
        XCTAssertEqual(coordinatorSpy.performCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.action, action)
    }
    
    func testDidNextStep_WhenActionIsRevalidateKey_ShouldPassActionToCoordinator() {
        let registredKey = RegisteredKey(
            id: "",
            statusSlug: .automaticConfirmed,
            status: "",
            name: "",
            keyType: .email,
            keyValue: "fulano@picpay.com"
        )
        let action: FeedbackContainerAction = .revalidateKey(key: registredKey, delegate: SecurityValidationCodeDelegateMock())
        
        sut.didNextStep(action: action)
        
        XCTAssertEqual(coordinatorSpy.performCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.action, action)
    }
    
    func testPresentError_WhenTypeIsClaimNewValidationKeyAvailable_ShouldDisplayError() {
        sut.presentError(title: "title", message: "message", buttonText: "buttonText")
        
        XCTAssertEqual(viewControllerSpy.displayErrorCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.titleDisplayError, "title")
        XCTAssertEqual(viewControllerSpy.messageDisplayError, "message")
        XCTAssertEqual(viewControllerSpy.buttonTextDisplayError, "buttonText")
    }
}
