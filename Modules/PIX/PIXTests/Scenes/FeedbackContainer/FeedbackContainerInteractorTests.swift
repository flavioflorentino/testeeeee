import AnalyticsModule
import Core
import Foundation
@testable import PIX
import XCTest

private final class SecurityValidationCodeDelegateMock: SecurityValidationCodeDelegate {
    func didValidateKeyWithSuccess(keyValue: String, keyType: PixKeyType) {}
}

private final class FeedbackContainerServiceMock: FeedbackContainerServicing {
    var consumerId: Int? {
        1
    }
    
    // MARK: - getKey
    private(set) var getKeyCallsCount = 0
    var getKeyResult: Result<PixKey, ApiError>?
    
    func getKey(id: String, completion: @escaping (Result<PixKey, ApiError>) -> Void) {
        guard let getKeyResult = getKeyResult else {
            XCTFail("Result (getKey) not defined")
            return
        }
        
        getKeyCallsCount += 1
        completion(getKeyResult)
    }
    
    // MARK: - confirmClaimNotAuthenticated
    private(set) var confirmClaimNotAuthenticatedCallsCount = 0
    var confirmClaimNotAuthenticatedResult: Result<NoContent, ApiError>?
    
    func confirmClaimNotAuthenticated(keyId: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        guard let confirmClaimNotAuthenticatedResult = confirmClaimNotAuthenticatedResult else {
            XCTFail("Result (confirmClaimNotAuthenticated) not defined")
            return
        }
        
        confirmClaimNotAuthenticatedCallsCount += 1
        completion(confirmClaimNotAuthenticatedResult)
    }
    
    // MARK: - confirmClaim
    private(set) var confirmClaimCallsCount = 0
    var confirmClaimResult: Result<NoContent, ApiError>?
    
    func confirmClaim(keyId: String, password: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        guard let confirmClaimResult = confirmClaimResult else {
            XCTFail("Result (confirmClaim) not defined")
            return
        }
        
        confirmClaimCallsCount += 1
        completion(confirmClaimResult)
    }
    
    // MARK: - cancelClaim
    private(set) var cancelClaimCallsCount = 0
    var cancelClaimResult: Result<NoContent, ApiError>?
    
    func cancelClaim(keyId: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        guard let cancelClaimResult = cancelClaimResult else {
            XCTFail("Result (cancelClaim) not defined")
            return
        }
        
        cancelClaimCallsCount += 1
        completion(cancelClaimResult)
    }
    
    // MARK: - completeClaim
    private(set) var completeClaimCallsCount = 0
    var completeClaimResult: Result<NoContent, ApiError>?
    
    func completeClaim(keyId: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        guard let completeClaimResult = completeClaimResult else {
            XCTFail("Result (confirmClaim) not defined")
            return
        }
        
        completeClaimCallsCount += 1
        completion(completeClaimResult)
    }
    
    // MARK: - requestPasswordFromUser
    private(set) var requestPasswordFromUserCallsCount = 0
    var requestPasswordFromUserResult: Result<String, AuthenticationStatus>?
    
    func requestPasswordFromUser(completion: @escaping (Result<String, AuthenticationStatus>) -> Void) {
        guard let requestPasswordFromUserResult = requestPasswordFromUserResult else {
            XCTFail("Result (requestPasswordFromUser) not defined")
            return
        }
        
        completion(requestPasswordFromUserResult)
        
        requestPasswordFromUserCallsCount += 1
    }
}

private final class FeedbackContainerPresenterSpy: FeedbackContainerPresenting {
    var viewController: FeedbackContainerDisplaying?
    
    // MARK: - presentFeedback
    private(set) var presentFeedbackCallsCount = 0
    private(set) var type: FeedbackViewType?
    private(set) var originFromPresentedFeedback: PixUserNavigation?
    
    func presentFeedback(type: FeedbackViewType, from origin: PixUserNavigation) {
        presentFeedbackCallsCount += 1
        self.type = type
        originFromPresentedFeedback = origin
    }
    
    // MARK: - didNextStep
    private(set) var didNextStepCallsCount = 0
    private(set) var action: FeedbackContainerAction?
    
    func didNextStep(action: FeedbackContainerAction) {
        didNextStepCallsCount += 1
        self.action = action
    }
    
    // MARK: - presentLoading
    private(set) var presentLoadingCallsCount = 0
    
    func presentLoading() {
        presentLoadingCallsCount += 1
    }
    
    // MARK: - hideLoading
    private(set) var hideLoadingCallsCount = 0
    
    func hideLoading() {
        hideLoadingCallsCount += 1
    }
    
    // MARK: - disableCloseButton
    private(set) var disableCloseButtonCallsCount = 0
    
    func disableCloseButton() {
        disableCloseButtonCallsCount += 1
    }
    
    // MARK: - enableCloseButton
    private(set) var enableCloseButtonCallsCount = 0
    
    func enableCloseButton() {
        enableCloseButtonCallsCount += 1
    }
    
    // MARK: - presentError
    private(set) var presentErrorCallsCount = 0
    private(set) var presentErrorTitle: String?
    private(set) var presentErrorMessage: String?
    private(set) var presentErrorButtonText: String?
    
    func presentError(title: String, message: String, buttonText: String) {
        presentErrorCallsCount += 1
        presentErrorTitle = title
        presentErrorMessage = message
        presentErrorButtonText = buttonText
    }
}

final class FeedbackContainerInteractorTests: XCTestCase {
    private let serviceMock = FeedbackContainerServiceMock()
    private let presenterSpy = FeedbackContainerPresenterSpy()
    private let analyticsSpy = AnalyticsSpy()
    
    func configureInteractor(type: FeedbackContainerType) -> FeedbackContainerInteracting {
        FeedbackContainerInteractor(
            service: serviceMock,
            presenter: presenterSpy,
            dependencies: DependencyContainerMock(analyticsSpy),
            type: type,
            origin: .undefined
        )
    }
    
    func testConfigureContainer_WhenTypeIPortabilityDonorReceived_ShouldPresentPortabilityDonorReceivedFeedback() {
        let sut = configureInteractor(type: .portabilityDonorReceived(uuid: "uuid"))
        
        sut.configureContainer()
        
        XCTAssertEqual(presenterSpy.presentFeedbackCallsCount, 1)
        XCTAssertEqual(presenterSpy.type, .portabilityDonorReceived(uuid: "uuid"))
    }
    
    func testConfigureContainer_WhenTypeIsClaimDonorReceived_ShouldCallServiceGetKey() {
        let key = PixKey(
            id: "uuid",
            statusSlug: .awaitingClaimConfirm,
            status: "",
            name: "",
            keyType: .email,
            createdAt: "",
            keyValue: ""
        )
        
        serviceMock.getKeyResult = .success(key)
        
        let sut = configureInteractor(type: .claimDonorReceived(uuid: "uuid"))
        sut.configureContainer()
        
        XCTAssertEqual(serviceMock.getKeyCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.disableCloseButtonCallsCount, 1)
    }
    
    func testConfigureContainer_WhenTypeIsClaimDonorReceivedAndServiceFailure_ShouldPresenterClaimDonorRequested() {
        serviceMock.getKeyResult = .failure(ApiError.badRequest(body: RequestError()))
        
        let sut = configureInteractor(type: .claimDonorReceived(uuid: "uuid"))
        sut.configureContainer()
        
        XCTAssertEqual(serviceMock.getKeyCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.disableCloseButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.enableCloseButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentFeedbackCallsCount, 1)
        XCTAssertEqual(presenterSpy.type, .claimDonorRequested(uuid: "uuid"))
    }
    
    func testExecute_WhenActionIsGrantPortabilityAndTypeIsPortabilityDonorReceived_ShouldCallServiceGetKey() {
        let key = PixKey(
            id: "uuid",
            statusSlug: .awaitingPortabilityConfirm,
            status: "",
            name: "",
            keyType: .email,
            createdAt: "",
            keyValue: ""
        )
        
        serviceMock.getKeyResult = .success(key)
        serviceMock.confirmClaimResult = .success(NoContent())
        serviceMock.requestPasswordFromUserResult = .success("1234")
        
        let sut = configureInteractor(type: .portabilityDonorReceived(uuid: "uuid"))
        
        sut.execute(action: .grantPortability, feedbackType: .portabilityDonorReceived(uuid: "uuid"))
        
        XCTAssertEqual(serviceMock.getKeyCallsCount, 1)
        XCTAssertEqual(serviceMock.requestPasswordFromUserCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.disableCloseButtonCallsCount, 1)
    }
    
    func testExecute_WhenActionIsGrantPortabilityAndTypeIsPortabilityDonorReceivedAndServiceGetKeyIsSuccess_ShouldCallConfirmClaim() {
        let key = PixKey(
            id: "uuid",
            statusSlug: .awaitingPortabilityConfirm,
            status: "",
            name: "",
            keyType: .email,
            createdAt: "",
            keyValue: ""
        )
        
        serviceMock.getKeyResult = .success(key)
        serviceMock.confirmClaimResult = .success(NoContent())
        serviceMock.requestPasswordFromUserResult = .success("1234")
        
        let sut = configureInteractor(type: .portabilityDonorReceived(uuid: "uuid"))
        
        sut.execute(action: .grantPortability, feedbackType: .portabilityDonorReceived(uuid: "uuid"))
        
        XCTAssertEqual(serviceMock.getKeyCallsCount, 1)
        XCTAssertEqual(serviceMock.confirmClaimCallsCount, 1)
        XCTAssertEqual(serviceMock.requestPasswordFromUserCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.disableCloseButtonCallsCount, 1)
    }
    
    func testExecute_WhenActionIsGrantPortabilityAndTypeIsPortabilityDonorReceivedAndServiceGetKeyAndConfirmClanIsSuccessAndStatusKeyIsAwaitingPortabilityConfirm_ShouldPresentPortabilityDonorInProgress() {
        let key = PixKey(
            id: "uuid",
            statusSlug: .awaitingPortabilityConfirm,
            status: "",
            name: "",
            keyType: .email,
            createdAt: "",
            keyValue: ""
        )
        
        serviceMock.getKeyResult = .success(key)
        serviceMock.confirmClaimResult = .success(NoContent())
        serviceMock.requestPasswordFromUserResult = .success("1234")
        
        let sut = configureInteractor(type: .portabilityDonorReceived(uuid: "uuid"))
        sut.execute(action: .grantPortability, feedbackType: .portabilityDonorReceived(uuid: "uuid"))
        
        XCTAssertEqual(serviceMock.getKeyCallsCount, 1)
        XCTAssertEqual(serviceMock.confirmClaimCallsCount, 1)
        XCTAssertEqual(serviceMock.requestPasswordFromUserCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.disableCloseButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .showFeedback(type: .portabilityDonorInProgress(uuid: "uuid"), from: .keyAuthenticationPortability))
    }
    
    func testExecute_WhenActionIsGrantPortabilityAndTypeIsPortabilityDonorReceivedAndServiceGetKeyAndConfirmClanIsSuccessAndStatusKeyIsAwaitingPortabilityConfirmAndPasswordIsWrong_ShouldPresentError() {
        let key = PixKey(
            id: "uuid",
            statusSlug: .awaitingPortabilityConfirm,
            status: "",
            name: "",
            keyType: .email,
            createdAt: "",
            keyValue: ""
        )
        
        serviceMock.getKeyResult = .success(key)
        serviceMock.confirmClaimResult = .failure(.unauthorized(body: RequestError()))
        serviceMock.requestPasswordFromUserResult = .success("1234")
        
        let sut = configureInteractor(type: .portabilityDonorReceived(uuid: "uuid"))
        sut.execute(action: .grantPortability, feedbackType: .portabilityDonorReceived(uuid: "uuid"))
        
        XCTAssertEqual(serviceMock.getKeyCallsCount, 1)
        XCTAssertEqual(serviceMock.confirmClaimCallsCount, 1)
        XCTAssertEqual(serviceMock.requestPasswordFromUserCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentErrorTitle, Strings.KeyManager.AuthError.title)
        XCTAssertEqual(presenterSpy.presentErrorMessage, Strings.KeyManager.AuthError.message)
        XCTAssertEqual(presenterSpy.presentErrorButtonText, Strings.General.gotIt)
    }
    
    func testExecute_WhenActionIsGrantPortabilityAndTypeIsPortabilityDonorReceivedAndServiceGetKeyAndConfirmClanIsSuccessAndStatusKeyIsAwaitingPortabilityComplete_ShouldPresentPortabilityDonorCompleted() {
        let key = PixKey(
            id: "uuid",
            statusSlug: .awaitingPortabilityComplete,
            status: "",
            name: "",
            keyType: .email,
            createdAt: "",
            keyValue: ""
        )
        
        serviceMock.getKeyResult = .success(key)
        serviceMock.confirmClaimResult = .success(NoContent())
        serviceMock.requestPasswordFromUserResult = .success("1234")
        
        let sut = configureInteractor(type: .portabilityDonorReceived(uuid: "uuid"))
        sut.execute(action: .grantPortability, feedbackType: .portabilityDonorReceived(uuid: "uuid"))
        
        XCTAssertEqual(serviceMock.getKeyCallsCount, 1)
        XCTAssertEqual(serviceMock.requestPasswordFromUserCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.disableCloseButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .showFeedback(type: .portabilityDonorCompleted(uuid: "uuid"), from: .keyAuthenticationPortability))
    }
    
    func testExecute_WhenActionIsGrantPortabilityAndTypeIsPortabilityDonorReceivedAndServiceGetKeyAndConfirmClanIsSuccessAndStatusKeyIanyOtherStatus_ShouldPresentPortabilityDonorRefused() {
        let key = PixKey(
            id: "uuid",
            statusSlug: .claimInProgress,
            status: "",
            name: "",
            keyType: .email,
            createdAt: "",
            keyValue: ""
        )
        
        serviceMock.getKeyResult = .success(key)
        serviceMock.confirmClaimResult = .success(NoContent())
        serviceMock.requestPasswordFromUserResult = .success("1234")
        
        let sut = configureInteractor(type: .portabilityDonorReceived(uuid: "uuid"))
        sut.execute(action: .grantPortability, feedbackType: .portabilityDonorReceived(uuid: "uuid"))
        
        XCTAssertEqual(serviceMock.getKeyCallsCount, 1)
        XCTAssertEqual(serviceMock.requestPasswordFromUserCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.disableCloseButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .showFeedback(type: .portabilityDonorRefused(uuid: "uuid"), from: .keyAuthenticationPortability))
    }
    
    func testExecute_WhenActionIsGrantPortabilityAndTypeIsPortabilityDonorReceivedAndServiceGetKeyIsFailure_ShouldPresentPortabilityDonorRefused() {
        serviceMock.requestPasswordFromUserResult = .success("1234")
        serviceMock.getKeyResult = .failure(.badRequest(body: RequestError()))
        
        let sut = configureInteractor(type: .portabilityDonorReceived(uuid: "uuid"))
        
        sut.execute(action: .grantPortability, feedbackType: .portabilityDonorReceived(uuid: "uuid"))
        
        XCTAssertEqual(serviceMock.getKeyCallsCount, 1)
        XCTAssertEqual(serviceMock.requestPasswordFromUserCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.disableCloseButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .showFeedback(type: .portabilityDonorRefused(uuid: "uuid"), from: .keyAuthenticationPortability))
    }
    
    func testExecute_WhenActionIsGrantPortabilityAndTypeIsPortabilityDonorReceivedAndServiceGetKeyIsSucessAndConfirmClaimIsFailure_ShouldPresentPortabilityDonorRefused() {
        let key = PixKey(
            id: "uuid",
            statusSlug: .awaitingPortabilityConfirm,
            status: "",
            name: "",
            keyType: .email,
            createdAt: "",
            keyValue: ""
        )
        
        serviceMock.getKeyResult = .success(key)
        serviceMock.confirmClaimResult = .failure(.badRequest(body: RequestError()))
        serviceMock.requestPasswordFromUserResult = .success("1234")
        
        let sut = configureInteractor(type: .portabilityDonorReceived(uuid: "uuid"))
        sut.execute(action: .grantPortability, feedbackType: .portabilityDonorReceived(uuid: "uuid"))
        
        XCTAssertEqual(serviceMock.getKeyCallsCount, 1)
        XCTAssertEqual(serviceMock.requestPasswordFromUserCallsCount, 1)
        XCTAssertEqual(serviceMock.confirmClaimCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.disableCloseButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .showFeedback(type: .portabilityDonorRefused(uuid: "uuid"), from: .keyAuthenticationPortability))
    }
    
    func testExecute_WhenActionIsAllowClaimAndServiceIsSuccess_ShouldCallConfirmClaim() {
        let key = PixKey(
            id: "uuid",
            statusSlug: .awaitingClaimConfirm,
            status: "",
            name: "",
            keyType: .email,
            createdAt: "",
            keyValue: ""
        )
        
        serviceMock.getKeyResult = .success(key)
        serviceMock.confirmClaimNotAuthenticatedResult = .success(NoContent())
        
        let sut = configureInteractor(type: .claimDonorReceived(uuid: "uuid"))
        sut.configureContainer()
        sut.execute(action: .allowClaim, feedbackType: .claimDonorReceived(uuid: "uuid"))
        
        XCTAssertEqual(serviceMock.getKeyCallsCount, 1)
        XCTAssertEqual(serviceMock.confirmClaimNotAuthenticatedCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.disableCloseButtonCallsCount, 2)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.enableCloseButtonCallsCount, 1)
    }
   
    func testExecute_WhenActionIsAllowClaimAndAllServiceIsSuccess_ShouldPresentClaimDonorInProgress() {
        let key = PixKey(
            id: "uuid",
            statusSlug: .awaitingClaimConfirm,
            status: "",
            name: "",
            keyType: .email,
            createdAt: "",
            keyValue: ""
        )
        
        serviceMock.getKeyResult = .success(key)
        serviceMock.confirmClaimNotAuthenticatedResult = .success(NoContent())
        
        let sut = configureInteractor(type: .claimDonorReceived(uuid: "uuid"))
        sut.configureContainer()
        sut.execute(action: .allowClaim, feedbackType: .claimDonorReceived(uuid: "uuid"))
        
        XCTAssertEqual(serviceMock.getKeyCallsCount, 1)
        XCTAssertEqual(serviceMock.confirmClaimNotAuthenticatedCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.disableCloseButtonCallsCount, 2)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.enableCloseButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .showFeedback(type: .claimDonorInProgress(uuid: "uuid"), from: .keyClaimReceived))
    }
    
    func testExecute_WhenActionIsAllowClaimAndAllServiceIsFailure_ShouldPresentClaimDonorNotCompleted() {
        let key = PixKey(
            id: "uuid",
            statusSlug: .awaitingClaimComplete,
            status: "",
            name: "",
            keyType: .email,
            createdAt: "",
            keyValue: ""
        )
        
        serviceMock.getKeyResult = .success(key)
        serviceMock.confirmClaimNotAuthenticatedResult = .failure(ApiError.badRequest(body: RequestError()))
        
        let sut = configureInteractor(type: .claimDonorReceived(uuid: "uuid"))
        sut.configureContainer()
        sut.execute(action: .allowClaim, feedbackType: .claimDonorReceived(uuid: "uuid"))
        
        XCTAssertEqual(serviceMock.getKeyCallsCount, 1)
        XCTAssertEqual(serviceMock.confirmClaimNotAuthenticatedCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.disableCloseButtonCallsCount, 2)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.enableCloseButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .showFeedback(type: .claimDonorNotCompleted(uuid: "uuid"), from: .keyClaimReceived))
    }
    
    func testExecute_WhenActionIsRevalidateKeyAndStatusIsAwaitingClaimConfirm_ShouldPresenteRevalidateKey() throws {
        let key = PixKey(
            id: "uuid",
            statusSlug: .awaitingClaimConfirm,
            status: "",
            name: "",
            keyType: .email,
            createdAt: "",
            keyValue: ""
        )
        
        let registeredKey = try XCTUnwrap(key.registeredKeys.first)
        
        serviceMock.getKeyResult = .success(key)
        
        let sut = configureInteractor(type: .claimDonorReceived(uuid: "uuid"))
        sut.configureContainer()
        sut.execute(action: .revalidateKey, feedbackType: .claimDonorReceived(uuid: "uuid"))
        
        XCTAssertEqual(serviceMock.getKeyCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.enableCloseButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .revalidateKey(key: registeredKey, delegate: SecurityValidationCodeDelegateMock()))
    }
    
    func testDidValidateKeyWithSuccessFromDelegate_WhenServiceIsSuccess_ShouldPresentClaimDonorRevalidationInProgress() {
        serviceMock.cancelClaimResult = .success(NoContent())
        
        let sut = configureInteractor(type: .claimDonorReceived(uuid: "uuid"))
        
        (sut as? SecurityValidationCodeDelegate)?.didValidateKeyWithSuccess(keyValue: "fulano@email.com", keyType: .email)
        
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.disableCloseButtonCallsCount, 1)
        XCTAssertEqual(serviceMock.cancelClaimCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentFeedbackCallsCount, 1)
        XCTAssertEqual(presenterSpy.type, .claimDonorRevalidationInProgress(uuid: "uuid"))
    }
    
    func testDidValidateKeyWithSuccessFromDelegate_WhenServiceIsFailure_ShouldPresentClaimDonorRevalidationNotCompleted() {
        serviceMock.cancelClaimResult = .failure(.badRequest(body: RequestError()))
        
        let sut = configureInteractor(type: .claimDonorReceived(uuid: "uuid"))
        
        (sut as? SecurityValidationCodeDelegate)?.didValidateKeyWithSuccess(keyValue: "fulano@email.com", keyType: .email)
        
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.disableCloseButtonCallsCount, 1)
        XCTAssertEqual(serviceMock.cancelClaimCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        XCTAssertEqual(
            presenterSpy.action,
            .showFeedback(type: .claimDonorRevalidationNotCompleted(uuid: "uuid"), from: .revalidationEmailVerification))
    }
    
    func testConfigureContainer_WhenTypeIsClaimNewValidationKeyAvailable_ShouldCallServiceGetKey() {
        let key = PixKey(
            id: "uuid",
            statusSlug: .claimNeedsValidation,
            status: "",
            name: "",
            keyType: .email,
            createdAt: "",
            keyValue: ""
        )
        
        serviceMock.getKeyResult = .success(key)
        
        let sut = configureInteractor(type: .claimNewValidationKeyAvailable(uuid: "uuid"))
        sut.configureContainer()
        
        XCTAssertEqual(serviceMock.getKeyCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.disableCloseButtonCallsCount, 1)
    }
    
    func testConfigureContainer_WhenTypeIsClaimNewValidationKeyAvailableAndServiceFailure_ShouldPresentError() {
        serviceMock.getKeyResult = .failure(ApiError.badRequest(body: RequestError()))
        
        let sut = configureInteractor(type: .claimNewValidationKeyAvailable(uuid: "uuid"))
        sut.configureContainer()
        
        XCTAssertEqual(serviceMock.getKeyCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.disableCloseButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.enableCloseButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentErrorCallsCount, 1)
    }
    
    func testConfigureContainer_WhenTypeIsClaimNewValidationKeyAvailableAndServiceSuccessAndStatusSlugIsClaimNeedsValidation_ShouldPresentFeedbackClaimNewValidationKeyAvailable() {
        let key = PixKey(
            id: "uuid",
            statusSlug: .claimNeedsValidation,
            status: "",
            name: "",
            keyType: .email,
            createdAt: "",
            keyValue: ""
        )
        
        serviceMock.getKeyResult = .success(key)
        
        let sut = configureInteractor(type: .claimNewValidationKeyAvailable(uuid: "uuid"))
        sut.configureContainer()
        
        XCTAssertEqual(serviceMock.getKeyCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.disableCloseButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.enableCloseButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentFeedbackCallsCount, 1)
        XCTAssertEqual(presenterSpy.type, .claimNewValidationKeyAvailable(uuid: "uuid"))
    }
    
    func testConfigureContainer_WhenTypeIsClaimNewValidationKeyAvailableAndServiceSuccessAndStatusSlugIsAny_ShouldPresentFeedbackClaimNewValidationDeadlineFinished() {
        let key = PixKey(
            id: "uuid",
            statusSlug: .processed,
            status: "",
            name: "",
            keyType: .email,
            createdAt: "",
            keyValue: ""
        )
        
        serviceMock.getKeyResult = .success(key)
        
        let sut = configureInteractor(type: .claimNewValidationKeyAvailable(uuid: "uuid"))
        sut.configureContainer()
        
        XCTAssertEqual(serviceMock.getKeyCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.disableCloseButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.enableCloseButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentFeedbackCallsCount, 1)
        XCTAssertEqual(presenterSpy.type, .claimNewValidationDeadlineFinished)
    }
    
    func testExecute_WhenActionIsValidateKeyAndStatusIsClaimNeedsValidation_ShouldPresenteRevalidateKey() throws {
        let key = PixKey(
            id: "uuid",
            statusSlug: .claimNeedsValidation,
            status: "",
            name: "",
            keyType: .email,
            createdAt: "",
            keyValue: ""
        )
        
        let registeredKey = try XCTUnwrap(key.registeredKeys.first)
        
        serviceMock.getKeyResult = .success(key)
        
        let sut = configureInteractor(type: .claimNewValidationKeyAvailable(uuid: "uuid"))
        sut.configureContainer()
        sut.execute(action: .validateKey, feedbackType: .claimNewValidationKeyAvailable(uuid: "uuid"))
        
        XCTAssertEqual(serviceMock.getKeyCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.enableCloseButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .revalidateKey(key: registeredKey, delegate: SecurityValidationCodeDelegateMock()))
    }
    
    func testDidValidateKeyWithSuccessFromDelegate_WhenTypeIsClaimNewValidationKeyAvailableAndServiceIsSuccess_ShouldPresentclaimNewValidationCompleted() {
        serviceMock.completeClaimResult = .success(NoContent())
        
        let sut = configureInteractor(type: .claimNewValidationKeyAvailable(uuid: "uuid"))
        
        (sut as? SecurityValidationCodeDelegate)?.didValidateKeyWithSuccess(keyValue: "fulano@email.com", keyType: .email)
        
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.disableCloseButtonCallsCount, 1)
        XCTAssertEqual(serviceMock.completeClaimCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentFeedbackCallsCount, 1)
        XCTAssertEqual(presenterSpy.type, .claimNewValidationCompleted)
    }
    
    func testDidValidateKeyWithSuccessFromDelegate_WhenTypeIsClaimNewValidationKeyAvailableAndServiceIsFailure_ShouldPresentClaimNewValidationNotCompleted() {
        serviceMock.completeClaimResult = .failure(.badRequest(body: RequestError()))
        
        let sut = configureInteractor(type: .claimNewValidationKeyAvailable(uuid: "uuid"))
        
        (sut as? SecurityValidationCodeDelegate)?.didValidateKeyWithSuccess(keyValue: "fulano@email.com", keyType: .email)
        
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.disableCloseButtonCallsCount, 1)
        XCTAssertEqual(serviceMock.completeClaimCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        XCTAssertEqual(
            presenterSpy.action,
            .showFeedback(type: .claimNewValidationNotCompleted, from: .revalidationEmailVerification))
    }
    
    func testExecute_WhenActionIsClose_ShouldDismissViewController() {
        let feedbackType = FeedbackViewType.portabilityDonorReceived(uuid: "uuid")
        let sut = configureInteractor(type: .portabilityDonorReceived(uuid: "uuid"))
        
        sut.execute(action: .close, feedbackType: feedbackType)
        
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .close(feedbackType: .portabilityDonorReceived(uuid: "uuid")))
    }
    
    func testDidClose_ShouldDismissViewController() {
        let feedbackType = FeedbackViewType.portabilityDonorReceived(uuid: "uuid")
        let sut = configureInteractor(type: .portabilityDonorReceived(uuid: "uuid"))
        
        sut.didClose(feedbackType: feedbackType)
        
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .close(feedbackType: feedbackType))
    }
}


extension FeedbackContainerAction: Equatable {
    public static func == (lhs: FeedbackContainerAction, rhs: FeedbackContainerAction) -> Bool {
        switch (lhs, rhs) {
        case let (.close(feedbackTypeLhs), .close(feedbackTypeRhs)):
            return feedbackTypeLhs == feedbackTypeRhs
        case let (.showFeedback(feedbackLhs, originLhs), .showFeedback(feedbackRhs, originRhs)):
            return feedbackLhs == feedbackRhs && originLhs == originRhs
        case let(.revalidateKey(revalidateKeyLhs, _), .revalidateKey(revalidateKeyRhs, _)):
            return revalidateKeyLhs == revalidateKeyRhs
        default:
            return false
        }
    }
}
