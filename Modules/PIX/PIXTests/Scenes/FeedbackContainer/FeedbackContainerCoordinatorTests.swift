import AnalyticsModule
import Foundation
@testable import PIX
import XCTest

private final class SecurityValidationCodeDelegateMock: SecurityValidationCodeDelegate {
    func didValidateKeyWithSuccess(keyValue: String, keyType: PixKeyType) {}
}

final class FeedbackContainerCoordinatorTests: XCTestCase {
    private lazy var viewControllerSpy: SpyViewController = {
        let controller = SpyViewController()
        self.navigationViewControllerSpy.setViewControllers([controller], animated: false)
        return controller
    }()
    
    private let navigationViewControllerSpy = SpyNavigationController()
    
    private lazy var sut: FeedbackContainerCoordinating = {
        let coordinator = FeedbackContainerCoordinator(dependencies: DependencyContainerMock(AnalyticsSpy()))
        coordinator.viewController = viewControllerSpy
        return coordinator
    }()
    
    func testPerform_WhenActionIsClose_ShouldDismmissViewController() {
        sut.perform(action: .close(feedbackType: nil))
        
        XCTAssertEqual(viewControllerSpy.dismissCallCount, 1)
    }
    
    func testPerform_WhenActionIsShowFeedback_ShouldPushFeedBackView() throws {
        let type: FeedbackViewType = .portabilityDonorCompleted(uuid: "uuid")
        let action: FeedbackContainerAction = .showFeedback(type: type, from: .undefined)
        
        sut.perform(action: action)
        
        let feedbackViewController = try XCTUnwrap(navigationViewControllerSpy.pushedViewController)
        XCTAssertEqual(navigationViewControllerSpy.pushViewControllerCallCount, 1)
        XCTAssertTrue(feedbackViewController is FeedbackViewController)
    }
    
    func testPerfomr_WhenActionIsRevalidateKey_ShouldPushSecurityCodeValidationViewController() throws {
        let registredKey = RegisteredKey(
            id: "",
            statusSlug: .automaticConfirmed,
            status: "",
            name: "",
            keyType: .email,
            keyValue: "fulano@picpay.com"
        )
        
        sut.perform(action: .revalidateKey(key: registredKey, delegate: SecurityValidationCodeDelegateMock()))
        
        let securityCodeValidationViewController = try XCTUnwrap(navigationViewControllerSpy.pushedViewController)
        XCTAssertEqual(navigationViewControllerSpy.pushViewControllerCallCount, 1)
        XCTAssertTrue(securityCodeValidationViewController is SecurityCodeValidationViewController)
    }
}
