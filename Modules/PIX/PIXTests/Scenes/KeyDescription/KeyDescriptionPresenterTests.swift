import XCTest
@testable import PIX

private final class KeyDescriptionCoordinatorSpy: KeyDescriptionCoordinating {
    var viewController: UIViewController?

    private(set) var performActionCallsCount = 0
    private(set) var performActionReceived: KeyDescriptionAction?
    
    func perform(action: KeyDescriptionAction) {
        performActionCallsCount += 1
        performActionReceived = action
    }
}

private final class KeyDescriptionViewControllerSpy: KeyDescriptionDisplaying {
    private(set) var didEnableContinueButtonCount = 0
    private(set) var isContinueButtonEnabled = false
    
    func enableContinueButton(_ isEnable: Bool) {
        didEnableContinueButtonCount += 1
        isContinueButtonEnabled = isEnable
    }
}

final class KeyDescriptionPresenterTests: XCTestCase {
    private let coordinatorSpy = KeyDescriptionCoordinatorSpy()
    private let viewControllerSpy = KeyDescriptionViewControllerSpy()
    
    private lazy var sut: KeyDescriptionPresenter = {
        let presenter = KeyDescriptionPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testEnableContinueButton_WhenTextEmpty_ShouldDisableContinueButton() {
        sut.enableContinueButton(false)
        
        XCTAssertEqual(viewControllerSpy.didEnableContinueButtonCount, 1)
        XCTAssertFalse(viewControllerSpy.isContinueButtonEnabled)
    }
    
    func testEnableContinueButton_WhenWhiteText_ShouldEnableContinueButton() {
        sut.enableContinueButton(true)
        
        XCTAssertEqual(viewControllerSpy.didEnableContinueButtonCount, 1)
        XCTAssertTrue(viewControllerSpy.isContinueButtonEnabled)
    }
    
    func testDidNextStepAction_WhenCalledWithSuccess_ShouldNavigateToCreateBizKey() {
        let userInfo = KeyManagerBizUserInfo(name: "", cnpj: "24.894.589/0001-87", mail: "miralta@picpay.com", phone: "(27) 91222 1929", userId: "1")
        let key = CreateKey(userId: "", userType: .company, value: "", type: .cnpj)
        let action = KeyDescriptionAction.createBizKey(key: key, userInfo: userInfo)
        sut.didNextStep(action: action)
        
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.performActionReceived, action)
    }
    
}
