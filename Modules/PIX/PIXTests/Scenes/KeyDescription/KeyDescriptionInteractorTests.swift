import XCTest
@testable import PIX

private final class KeyDescriptionPresenterSpy: KeyDescriptionPresenting {
    var viewController: KeyDescriptionDisplaying?
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didPresentEnableCotinueButtonCount = 0
    private(set) var isContinueButtonEnabled = false
    private(set) var keyDescriptionAction: KeyDescriptionAction?
    
    func enableContinueButton(_ isEnabled: Bool) {
        didPresentEnableCotinueButtonCount += 1
        isContinueButtonEnabled = isEnabled
    }
    
    func didNextStep(action: KeyDescriptionAction) {
        didNextStepActionCallsCount += 1
        keyDescriptionAction = action
    }
}

final class KeyDescriptionInteractorTests: XCTestCase {
    private let userInfo = KeyManagerBizUserInfo(name: "", cnpj: "24.894.589/0001-87", mail: "miralta@picpay.com", phone: "(27) 91222 1929", userId: "1")
    private let presenterSpy = KeyDescriptionPresenterSpy()
    private let key = CreateKey(userId: "", userType: .company, value: "", type: .cnpj)
    private lazy var sut = KeyDescriptionInteractor(userInfo: userInfo, presenter: presenterSpy, dependencies: DependencyContainer(), createKey: key)
    
    func testkeyDescriptionChanged_WhenTextEmpty_ShouldDisableContinueButton() {
        sut.keyDescriptionChanged("")
        
        XCTAssertEqual(presenterSpy.didPresentEnableCotinueButtonCount, 1)
        XCTAssertFalse(presenterSpy.isContinueButtonEnabled)
    }
    
    func testkeyDescriptionChanged_WhenTextContainJustWhiteSpaces_ShouldDisableContinueButton(){
        
        sut.keyDescriptionChanged("         ")
        
        XCTAssertEqual(presenterSpy.didPresentEnableCotinueButtonCount, 1)
        XCTAssertFalse(presenterSpy.isContinueButtonEnabled)
    }
    
    func testkeyDescriptionChanged_WhenWhiteText_ShouldEnableContinueButton() {
        
        sut.keyDescriptionChanged("teste")
        
        XCTAssertEqual(presenterSpy.didPresentEnableCotinueButtonCount, 1)
        XCTAssertTrue(presenterSpy.isContinueButtonEnabled)
    }
    
    func testGoToCreateKey_ShouldNavigteToCreateKey() {
        
        sut.goToCreateKey(with: "")
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertNotNil(presenterSpy.keyDescriptionAction)
    }
}
