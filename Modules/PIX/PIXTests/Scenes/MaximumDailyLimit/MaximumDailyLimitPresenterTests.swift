@testable import PIX
import UI
import XCTest

private final class MaximumDailyLimitCoordinatorSpy: MaximumDailyLimitCoordinating {
    var viewController: UIViewController?

    func perform(action: MaximumDailyLimitAction) {
    }
}

private final class MaximumDailyLimitViewControllerSpy: MaximumDailyLimitDisplaying {
    private(set) var enableForwardButtonStateCallsCount = 0
    func enableForwardButtonState() {
        enableForwardButtonStateCallsCount += 1
    }

    private(set) var disableForwardButtonStateCallsCount = 0
    func disableForwardButtonState() {
        disableForwardButtonStateCallsCount += 1
    }

    private(set) var updateInputFieldCallsCount = 0
    private(set) var value: String?
    func updateInputField(with value: String?) {
        updateInputFieldCallsCount += 1
        self.value = value
    }
    
    private(set) var displayUnavailableLimitAmountErrorCallsCount = 0
    func displayUnavailableLimitAmountError() {
        displayUnavailableLimitAmountErrorCallsCount += 1
    }
    
    private(set) var hideUnavailableLimitAmountErrorCallsCount = 0
    func hideUnavailableLimitAmountError() {
        hideUnavailableLimitAmountErrorCallsCount += 1
    }

    private(set) var showLoadingCallsCount = 0
    func showLoading() {
        showLoadingCallsCount += 1
    }

    private(set) var hideLoadingCallsCount = 0
    func hideLoading() {
        hideLoadingCallsCount += 1
    }

    private(set) var displayGenericErrorAlertCallsCount = 0
    private(set) var errorValue: String?
    func displayGenericErrorAlertWith(value: String) {
        displayGenericErrorAlertCallsCount += 1
        self.errorValue = value
    }
}

final class MaximumDailyLimitPresenterTests: XCTestCase {
    // MARK: - Variables
    private let coordinator = MaximumDailyLimitCoordinatorSpy()
    private let viewController = MaximumDailyLimitViewControllerSpy()

    private lazy var presenter: MaximumDailyLimitPresenter = {
        let presenter = MaximumDailyLimitPresenter(coordinator: coordinator)
        presenter.viewController = viewController
        return presenter
    }()

    // MARK: - enableForwardButtonState
    func testEnableForwardButtonState_ShouldCallEnableForwardButtonState() {
        presenter.enableForwardButtonState()
        XCTAssertEqual(viewController.enableForwardButtonStateCallsCount, 1)
    }

    // MARK: - disableForwardButtonState
    func testDisableForwardButtonState_ShouldCallDisableForwardButtonState() {
        presenter.disableForwardButtonState()
        XCTAssertEqual(viewController.disableForwardButtonStateCallsCount, 1)
    }

    // MARK: - updateInputField
    func testUpdateInputField_WhenInputIs1ShouldCallUpdateInputFieldWithValueInBrazilianCurrencyFormat() {
        presenter.updateInputField(with: 1.0)
        XCTAssertEqual(viewController.updateInputFieldCallsCount, 1)
        XCTAssertEqual(viewController.value, "R$ 1")
    }

    func testUpdateInputField_WhenInputIs123456ShouldCallUpdateInputFieldWithValueInBrazilianCurrencyFormat() {
        presenter.updateInputField(with: 1200)
        XCTAssertEqual(viewController.updateInputFieldCallsCount, 1)
        XCTAssertEqual(viewController.value, "R$ 1.200")
    }

    // MARK: - presentUnavailableLimitAmountError
    func testPresentUnavailableLimitAmountError_ShouldCallDisplayUnavailableLimitAmountError() {
        presenter.presentUnavailableLimitAmountError()
        XCTAssertEqual(viewController.displayUnavailableLimitAmountErrorCallsCount, 1)
    }
    
    // MARK: - hideUnavailableLimitAmountError
    func testHideUnavailableLimitAmountError_ShouldCallHideUnavailableLimitAmountError() {
        presenter.hideUnavailableLimitAmountError()
        XCTAssertEqual(viewController.hideUnavailableLimitAmountErrorCallsCount, 1)
    }

    // MARK: - presentLoading
    func testPresentLoading_ShouldCallShowLoading() {
        presenter.presentLoading()
        XCTAssertEqual(viewController.showLoadingCallsCount, 1)
    }

    // MARK: - hideLoading
    func testHideLoading_ShouldCallHideLoading() {
        presenter.hideLoading()
        XCTAssertEqual(viewController.hideLoadingCallsCount, 1)
    }

    // MARK: - presentAlertGenericError
    func testPresentAlertGenericError_ShouldCallDisplayGenericErrorAlert() {
        let value = "value"
        presenter.presentAlertGenericErrorWith(value: value)
        XCTAssertEqual(viewController.displayGenericErrorAlertCallsCount, 1)
        XCTAssertEqual(viewController.errorValue, value)
    }
}
