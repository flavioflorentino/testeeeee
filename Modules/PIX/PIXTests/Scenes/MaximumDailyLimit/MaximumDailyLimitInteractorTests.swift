import Core
@testable import PIX
import XCTest

private final class MaximumDailyLimitPresenterSpy: MaximumDailyLimitPresenting {
    var viewController: MaximumDailyLimitDisplaying?
    private(set) var enableForwardButtonStateCallsCount = 0
    func enableForwardButtonState() {
        enableForwardButtonStateCallsCount += 1
    }

    private(set) var disableForwardButtonStateCallsCount = 0
    func disableForwardButtonState() {
        disableForwardButtonStateCallsCount += 1
    }

    private(set) var updateInputFieldCallsCount = 0
    private(set) var number: Double?
    func updateInputField(with number: Double) {
        updateInputFieldCallsCount += 1
        self.number = number
    }

    private(set) var clearInputFieldCallsCount = 0
    func clearInputField() {
        clearInputFieldCallsCount += 1
    }

    func didNextStep(action: MaximumDailyLimitAction) {}
    
    private(set) var presentUnavailableLimitAmountErrorCallsCount = 0
    func presentUnavailableLimitAmountError() {
        presentUnavailableLimitAmountErrorCallsCount += 1
    }
    
    private(set) var hideUnavailableLimitAmountErrorCallsCount = 0
    func hideUnavailableLimitAmountError() {
        hideUnavailableLimitAmountErrorCallsCount += 1
    }

    private(set) var presentTicketListCallsCount = 0
    func presentTicketList() {
        presentTicketListCallsCount += 1
    }

    private(set) var presentLoadingCallsCount = 0
    func presentLoading() {
        presentLoadingCallsCount += 1
    }

    private(set) var hideLoadingCallsCount = 0
    func hideLoading() {
        hideLoadingCallsCount += 1
    }

    private(set) var presentAlertGenericErrorCallsCount = 0
    private(set) var value: String?
    func presentAlertGenericErrorWith(value: String) {
        presentAlertGenericErrorCallsCount += 1
        self.value = value
    }
}

final class MaximumDailyLimitInteractorTests: XCTestCase {
    // MARK: - Variables
    private let zendesk = ZendeskMock()
    private let presenter = MaximumDailyLimitPresenterSpy()
    private let queue = DispatchQueue(label: #function)
    private lazy var dependencies = DependencyContainerMock(zendesk, queue)
    private var currentMaxDailyLimit: Double = .zero
    private lazy var interactor = MaximumDailyLimitInteractor(
        presenter: presenter,
        currentMaxDailyLimit: currentMaxDailyLimit,
        appType: .pf,
        dependencies: dependencies
    )

    // MARK: - handleInputValue
    func testHandleInputValue_WhenValueIsNotNumeric_ShouldCallUpdateInputFieldWithPreviousValue() {
        interactor.handleInputValue("test")
        XCTAssertEqual(presenter.updateInputFieldCallsCount, 1)
        XCTAssertEqual(presenter.number, .zero)
    }
    
    func testHandleInputValue_WhenValueIsNumericAndGreaterThanMaxAllowedValue_ShouldCallUpdateInputFieldWithPreviousValue() {
        interactor.handleInputValue("R$ 10.000.000")
        XCTAssertEqual(presenter.updateInputFieldCallsCount, 1)
        XCTAssertEqual(presenter.number, .zero)
    }
    
    func testHandleInputValue_WhenValueIsNumericAndValueIsLessThanMaxAllowedValueAndValueIsLessThanOrEqualToCurrentMaxDailyLimit_ShouldCallPresentUnavailableLimitAmountErrorAndDisableForwardButtonStateAndUpdateInputFieldWithGivenValue() {
        currentMaxDailyLimit = 11_000
        interactor.handleInputValue("R$ 10.000")
        XCTAssertEqual(presenter.presentUnavailableLimitAmountErrorCallsCount, 1)
        XCTAssertEqual(presenter.disableForwardButtonStateCallsCount, 1)
        XCTAssertEqual(presenter.updateInputFieldCallsCount, 1)
        XCTAssertEqual(presenter.number, 10_000)
    }
    
    func testHandleInputValue_WhenValueIsNumericAndValueIsLessThanMaxAllowedValueAndValueIsGreaterThanCurrentMaxDailyLimit_ShouldCallHideUnavailableLimitAmountErrorAndEnableForwardButtonStateAndUpdateInputFieldWithGivenValue() {
        currentMaxDailyLimit = 11_000
        interactor.handleInputValue("R$ 11.001")
        XCTAssertEqual(presenter.hideUnavailableLimitAmountErrorCallsCount, 1)
        XCTAssertEqual(presenter.enableForwardButtonStateCallsCount, 1)
        XCTAssertEqual(presenter.updateInputFieldCallsCount, 1)
        XCTAssertEqual(presenter.number, 11_001)
    }

    // MARK: - openTicket
    func testOpenTicket_WhenRequestTicketReturnSuccess_ShouldPresentTicketList() {
        let value = "R$ 25000"
        zendesk.requestTicketCompletionResult = .success(())
        interactor.openTicketWith(value: value)
        let expectation = XCTestExpectation(description: "MaximumDailyLimitInteractorExpectation")
        queue.async {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 0.5)
        XCTAssertEqual(presenter.presentTicketListCallsCount, 1)
    }

    func testOpenTicket_WhenRequestReturnFailure_ShouldShowAlertError() {
        let value = "R$ 25000"
        zendesk.requestTicketCompletionResult = .failure(ApiError.serverError)
        interactor.openTicketWith(value: value)
        let expectation = XCTestExpectation(description: "MaximumDailyLimitInteractorExpectation")
        queue.async {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 0.5)
        XCTAssertEqual(presenter.presentAlertGenericErrorCallsCount, 1)
        XCTAssertEqual(presenter.value, value)
    }
}
