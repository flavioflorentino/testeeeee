@testable import PIX
import UI
import XCTest

private final class DailyLimitCoordinatorSpy: DailyLimitCoordinating {
    var viewController: UIViewController?
    private(set) var performCallsCount = 0
    
    func perform(action: DailyLimitAction) {
        performCallsCount += 1
    }
}

private final class DailyLimitViewControllerSpy: DailyLimitDisplaying {
    private(set) var displaySkeletonLoaderCallsCount = 0
    private(set) var hideSkeletonLoaderCallsCount = 0
    private(set) var displayFullscreenErrorCallsCount = 0
    private(set) var displayMinimumLimitCallsCount = 0
    private(set) var minValue: Float?
    private(set) var minStringValue: String?
    private(set) var displayMaximumLimitCallsCount = 0
    private(set) var maxValue: Float?
    private(set) var maxStringValue: String?
    private(set) var displayCurrentDailyLimitCallsCount = 0
    private(set) var currentValue: Float?
    private(set) var currentAmount: String?
    private(set) var displayUsedDailyLimitCallsCount = 0
    private(set) var usedAmount: String?
    private(set) var displayIncreaseMaxLimitCallsCount = 0
    private(set) var increaseMaxLimitText: NSAttributedString?
    private(set) var displayNewLimitCallsCount = 0
    private(set) var newLimitAmount: String?
    private(set) var displaySliderCallsCount = 0
    private(set) var sliderValue: Float?
    private(set) var displayUnavailableLimitAmountErrorCallsCount = 0
    private(set) var hideUnavailableLimitAmountErrorCallsCount = 0
    private(set) var displayUpdateButtonEnabledCallsCount = 0
    private(set) var displayUpdateButtonDisabledCallsCount = 0
    private(set) var displaySnackbarCallsCount = 0
    private(set) var displayAlertCallsCount = 0
    private(set) var displayUpdateLimitLoaderCallsCount = 0
    private(set) var hideUpdateLimitLoaderCallsCount = 0
    private(set) var displayExistingTicketAlertCallsCount = 0
    private(set) var id: String?
    private(set) var displayLoadingCallsCount = 0
    private(set) var hideLoadingCallsCount = 0
    
    func displaySkeletonLoader() {
        displaySkeletonLoaderCallsCount += 1
    }
    
    func hideSkeletonLoader() {
        hideSkeletonLoaderCallsCount += 1
    }
    
    func displayFullscreenError(image: UIImage?, title: String, description: NSAttributedString, buttonTitle: String) {
        displayFullscreenErrorCallsCount += 1
    }
    
    func displayMinimumLimit(value: Float, stringValue: String) {
        displayMinimumLimitCallsCount += 1
        minValue = value
        minStringValue = stringValue
    }
    
    func displayMaximumLimit(value: Float, stringValue: String) {
        displayMaximumLimitCallsCount += 1
        maxValue = value
        maxStringValue = stringValue
    }
    
    func displayCurrentDailyLimit(value: Float, amount: String) {
        displayCurrentDailyLimitCallsCount += 1
        currentValue = value
        currentAmount = amount
    }
    
    func displayUsedDailyLimit(amount: String) {
        displayUsedDailyLimitCallsCount += 1
        usedAmount = amount
    }
    
    func displayIncreaseMaxLimit(text: NSAttributedString) {
        displayIncreaseMaxLimitCallsCount += 1
        increaseMaxLimitText = text
    }
    
    func displayNewLimit(amount: String) {
        displayNewLimitCallsCount += 1
        newLimitAmount = amount
    }
    
    func displaySlider(value: Float) {
        displaySliderCallsCount += 1
        sliderValue = value
    }
    
    func displayUnavailableLimitAmountError() {
        displayUnavailableLimitAmountErrorCallsCount += 1
    }
    
    func hideUnavailableLimitAmountError() {
        hideUnavailableLimitAmountErrorCallsCount += 1
    }
    
    func displayUpdateButtonEnabled() {
        displayUpdateButtonEnabledCallsCount += 1
    }
    
    func displayUpdateButtonDisabled() {
        displayUpdateButtonDisabledCallsCount += 1
    }
    
    func displaySnackbar(text: String, iconType: ApolloFeedbackCardViewIconType, emphasis: ApolloSnackbarEmphasisStyle) {
        displaySnackbarCallsCount += 1
    }
    
    func displayAlert(type: ApolloAlertType, title: String, subtitle: NSAttributedString, primaryButtonTitle: String, secondaryButtonTitle: String) {
        displayAlertCallsCount += 1
    }
    
    func displayUpdateLimitLoader() {
        displayUpdateLimitLoaderCallsCount += 1
    }
    
    func hideUpdateLimitLoader() {
        hideUpdateLimitLoaderCallsCount += 1
    }

    func displayExistingTicketAlertWith(id: String) {
        displayExistingTicketAlertCallsCount += 1
        self.id = id
    }

    func displayLoading() {
        displayLoadingCallsCount += 1
    }

    func hideLoading() {
        hideLoadingCallsCount += 1
    }
}

final class DailyLimitPresenterTests: XCTestCase {
    private let coordinator = DailyLimitCoordinatorSpy()
    private let dependencies = DependencyContainerMock()
    private let viewController = DailyLimitViewControllerSpy()
    private lazy var presenter: DailyLimitPresenter = {
        let presenter = DailyLimitPresenter(coordinator: coordinator, dependencies: dependencies)
        presenter.viewController = viewController
        return presenter
    }()
    
    // MARK: - didNextStep
    func testDidNextStep_ShouldCallCoordinatorPerform() {
        presenter.didNextStep(action: .close)
        XCTAssertEqual(coordinator.performCallsCount, 1)
    }
    
    // MARK: - presentSkeletonLoader
    func testPresentSkeletonLoader_ShouldCallDisplaySkeletonLoader() {
        presenter.presentSkeletonLoader()
        XCTAssertEqual(viewController.displaySkeletonLoaderCallsCount, 1)
    }
    
    // MARK: - hideSkeletonLoader
    func testHideSkeletonLoader_ShouldCallHideSkeletonLoader() {
        presenter.hideSkeletonLoader()
        XCTAssertEqual(viewController.hideSkeletonLoaderCallsCount, 1)
    }
    
    // MARK: - presentLimitData
    func testPresentLimitData_ShouldCallDisplayMinimumLimitAndDisplayMaximumLimitAndDisplayCurrentDailyLimitAndDisplayUsedDailyLimitAndDisplayIncreaseMaxLimitWithDataFormattedAppropriately() {
        let current: Double = 5.0
        let max: Double = 10.0
        let usedToday: Double = 2.0
        let minValue: Double = 0.0
        let limitData = PixDailyLimit(
            current: current,
            max: max,
            usedToday: usedToday
        )
        let increaseMaxLimitText = Strings.DailyLimit.increaseMaxDailyLimit("R$ 10")
        presenter.presentLimitData(dailyLimit: limitData, minValue: minValue)
        XCTAssertEqual(viewController.displayMinimumLimitCallsCount, 1)
        XCTAssertEqual(viewController.minValue, Float(minValue))
        XCTAssertEqual(viewController.minStringValue, "0")
        XCTAssertEqual(viewController.displayIncreaseMaxLimitCallsCount, 1)
        XCTAssertEqual(viewController.maxValue, Float(max))
        XCTAssertEqual(viewController.maxStringValue, "10")
        XCTAssertEqual(viewController.displayCurrentDailyLimitCallsCount, 1)
        XCTAssertEqual(viewController.currentValue, Float(current))
        XCTAssertEqual(viewController.currentAmount, "R$ 5")
        XCTAssertEqual(viewController.displayUsedDailyLimitCallsCount, 1)
        XCTAssertEqual(viewController.usedAmount, "R$ 2,00")
        XCTAssertEqual(viewController.displayIncreaseMaxLimitCallsCount, 1)
        XCTAssertEqual(viewController.increaseMaxLimitText?.string, increaseMaxLimitText)
    }
    
    // MARK: - presentGenericError
    func testPresentGenericError_ShouldCallDisplayFullscreenError() {
        presenter.presentGenericError()
        XCTAssertEqual(viewController.displayFullscreenErrorCallsCount, 1)
    }
    
    // MARK: - presentNewDailyLimit
    func testPresentNewDailyLimit_ShouldCallDisplayNewLimitWithFormattedAmount() {
        presenter.presentNewDailyLimit(value: 10.0)
        XCTAssertEqual(viewController.displayNewLimitCallsCount, 1)
        XCTAssertEqual(viewController.newLimitAmount, "R$ 10")
    }
    
    // MARK: - presentSlider
    func testPresentSlider_ShouldCallDisplaySliderWithGivenValue() {
        let value: Float = 1.0
        presenter.presentSlider(value: value)
        XCTAssertEqual(viewController.displaySliderCallsCount, 1)
        XCTAssertEqual(viewController.sliderValue, value)
    }
    
    // MARK: - presentUnavailableLimitAmountError
    func testPresentUnavailableLimitAmountError_ShouldCallDisplayUnavailableLimitAmountError() {
        presenter.presentUnavailableLimitAmountError()
        XCTAssertEqual(viewController.displayUnavailableLimitAmountErrorCallsCount, 1)
    }
    
    // MARK: - hideUnavailableLimitAmountError
    func testHideUnavailableLimitAmountError_ShouldCallHideUnavailableLimitAmountError() {
        presenter.hideUnavailableLimitAmountError()
        XCTAssertEqual(viewController.hideUnavailableLimitAmountErrorCallsCount, 1)
    }
    
    // MARK: - presentUpdateButtonEnabled
    func testPresentUpdateButtonEnabled_ShouldCallDisplayUpdateButtonEnabled() {
        presenter.presentUpdateButtonEnabled()
        XCTAssertEqual(viewController.displayUpdateButtonEnabledCallsCount, 1)
    }
    
    // MARK: - presentUpdateButtonDisabled
    func testPresentUpdateButtonDisabled_ShouldCallDisplayUpdateButtonDisabled() {
        presenter.presentUpdateButtonDisabled()
        XCTAssertEqual(viewController.displayUpdateButtonDisabledCallsCount, 1)
    }
    
    // MARK: - presentUpdateLimitConfirmation
    func testPresentUpdateLimitConfirmation_ShouldCallDisplayAlert() {
        presenter.presentUpdateLimitConfirmation()
        XCTAssertEqual(viewController.displayAlertCallsCount, 1)
    }
    
    // MARK: - presentUpdateLimitSuccess
    func testPresentUpdateLimitSuccess_ShouldCallDisplaySnackbar() {
        presenter.presentUpdateLimitSuccess()
        XCTAssertEqual(viewController.displaySnackbarCallsCount, 1)
    }
    
    // MARK: - presentAlertGenericError
    func testPresentAlertGenericError_ShouldCallDisplayAlert() {
        presenter.presentAlertGenericError()
        XCTAssertEqual(viewController.displayAlertCallsCount, 1)
    }
    
    // MARK: - presentUpdateLimitLoader
    func testPresentUpdateLimitLoader_ShouldCallDisplayUpdateLimitLoader() {
        presenter.presentUpdateLimitLoader()
        XCTAssertEqual(viewController.displayUpdateLimitLoaderCallsCount, 1)
    }
    
    // MARK: - hideUpdateLimitLoader
    func testHideUpdateLimitLoader_ShouldCallHideUpdateLimitLoader() {
        presenter.hideUpdateLimitLoader()
        XCTAssertEqual(viewController.hideUpdateLimitLoaderCallsCount, 1)
    }

    // MARK: - presentExistingTicketAlert
    func testPresentExistingAlert_ShouldCallDisplayExistingAlert() {
        let id = "id"
        presenter.presentExistingTicketAlertWith(id: id)
        XCTAssertEqual(viewController.displayExistingTicketAlertCallsCount, 1)
        XCTAssertEqual(viewController.id, id)
    }

    // MARK: - presentTicketChat
    func testPresentTicketChat_ShouldCallTicketChatScreen() {
        presenter.presentTicketChatWith(id: "id")
        XCTAssertEqual(coordinator.performCallsCount, 1)
    }

    // MARK: - presentLoading
    func testPresentLoading_ShouldCallDisplayLoading() {
        presenter.presentLoading()
        XCTAssertEqual(viewController.displayLoadingCallsCount, 1)
    }

    // MARK: - hideLoading
    func testHideLoading_ShouldCallHideLoading() {
        presenter.hideLoading()
        XCTAssertEqual(viewController.hideLoadingCallsCount, 1)
    }
    
}
