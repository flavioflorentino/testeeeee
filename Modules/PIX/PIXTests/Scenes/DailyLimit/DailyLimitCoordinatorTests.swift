@testable import PIX
import XCTest

final class DailyLimitCoordinatorTests: XCTestCase {
    private lazy var viewController: SpyViewController? = {
        let controller = SpyViewController()
        self.navigationController.pushViewController(controller, animated: false)
        return controller
    }()
    private let navigationController = SpyNavigationController()
    lazy var coordinator: DailyLimitCoordinator = {
        let coordinator = DailyLimitCoordinator(dependencies: DependencyContainerMock())
        coordinator.viewController = viewController
        return coordinator
    }()
    
    // MARK: - performFAQ
    func testPerform_WhenActionIsFaq_ShouldPresentFAQ() throws {
        let url = try XCTUnwrap(URL(string: "abc.de"))
        coordinator.perform(action: .faq(link: url))
        XCTAssertEqual(viewController?.presentCallCount, 1)
    }
    
    func testPerform_WhenActionIsFaqAndViewControllerIsNil_ShouldDoNothing() throws {
        viewController = nil
        let url = try XCTUnwrap(URL(string: "abc.de"))
        coordinator.perform(action: .faq(link: url))
        XCTAssertNil(viewController?.presentedViewController)
    }

    // MARK: - performClose
    func testPerform_WhenActionIsClose_ShouldNavigationControllerCallDismiss() {
        coordinator.perform(action: .close)
        XCTAssertEqual(navigationController.dismissCallCount, 1)
    }

    // MARK: - performMaximumDailyLimit
    func testPerform_WhenActionIsMaximumDailyLimit_ShouldPushControllerOfTypeMaximumDailyLimitViewController() {
        coordinator.perform(action: .maximumDailyLimit(value: 1.0))
        XCTAssert(navigationController.pushedViewController is MaximumDailyLimitViewController)
    }
}
