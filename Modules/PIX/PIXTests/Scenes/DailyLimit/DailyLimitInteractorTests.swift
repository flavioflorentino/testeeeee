import Core
import CustomerSupport
@testable import PIX
import FeatureFlag
import XCTest

private final class DailyLimitServiceMock: DailyLimitServicing {
    private(set) var getLimitCallsCount = 0
    private(set) var password: String?
    var getLimitResult: Result<PixDailyLimit, ApiError> = .failure(ApiError.unknown(nil))
    private(set) var updateLimitCallsCount = 0
    var updateLimitResult: Result<NoContent, ApiError> = .failure(ApiError.unknown(nil))
    
    func getLimit(completion: @escaping (Result<PixDailyLimit, ApiError>) -> Void) {
        getLimitCallsCount += 1
        completion(getLimitResult)
    }
    
    func updateLimit(newAmount: String, password: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        updateLimitCallsCount += 1
        self.password = password
        completion(updateLimitResult)
    }
}

private final class DailyLimitPresenterSpy: DailyLimitPresenting {
    var viewController: DailyLimitDisplaying?
    private(set) var didNextStepCallsCount = 0
    private(set) var presentSkeletonLoaderCallsCount = 0
    private(set) var hideSkeletonLoaderCallsCount = 0
    private(set) var presentLimitDataCallsCount = 0
    private(set) var dailyLimit: PixDailyLimit?
    private(set) var minValue: Double?
    private(set) var presentGenericErrorCallsCount = 0
    private(set) var presentNewDailyLimitCallsCount = 0
    private(set) var newLimitValue: Double?
    private(set) var presentSliderCallsCount = 0
    private(set) var sliderValue: Float?
    private(set) var presentUnavailableLimitAmountErrorCallsCount = 0
    private(set) var hideUnavailableLimitAmountErrorCallsCount = 0
    private(set) var presentUpdateButtonEnabledCallsCount = 0
    private(set) var presentUpdateButtonDisabledCallsCount = 0
    private(set) var presentUpdateLimitConfirmationCallsCount = 0
    private(set) var presentUpdateLimitSuccessCallsCount = 0
    private(set) var presentAlertGenericErrorCallsCount = 0
    private(set) var presentUpdateLimitLoaderCallsCount = 0
    private(set) var hideUpdateLimitLoaderCallsCount = 0
    private(set) var presentExistingTicketAlertCallsCount = 0
    private(set) var existingTicketId: String?
    private(set) var presentTicketChatCallsCount = 0
    private(set) var chatTicketId: String?
    private(set) var presentLoadingCallsCount = 0
    private(set) var hideLoadingCallsCount = 0
    
    func didNextStep(action: DailyLimitAction) {
        didNextStepCallsCount += 1
    }
    
    func presentSkeletonLoader() {
        presentSkeletonLoaderCallsCount += 1
    }
    
    func hideSkeletonLoader() {
        hideSkeletonLoaderCallsCount += 1
    }
    
    func presentLimitData(dailyLimit: PixDailyLimit, minValue: Double) {
        presentLimitDataCallsCount += 1
        self.dailyLimit = dailyLimit
        self.minValue = minValue
    }
    
    func presentGenericError() {
        presentGenericErrorCallsCount += 1
    }
    
    func presentNewDailyLimit(value: Double) {
        presentNewDailyLimitCallsCount += 1
        newLimitValue = value
    }
    
    func presentSlider(value: Float) {
        presentSliderCallsCount += 1
        sliderValue = value
    }
    
    func presentUnavailableLimitAmountError() {
        presentUnavailableLimitAmountErrorCallsCount += 1
    }
    
    func hideUnavailableLimitAmountError() {
        hideUnavailableLimitAmountErrorCallsCount += 1
    }
    
    func presentUpdateButtonEnabled() {
        presentUpdateButtonEnabledCallsCount += 1
    }
    
    func presentUpdateButtonDisabled() {
        presentUpdateButtonDisabledCallsCount += 1
    }
    
    func presentUpdateLimitConfirmation() {
        presentUpdateLimitConfirmationCallsCount += 1
    }
    
    func presentUpdateLimitSuccess() {
        presentUpdateLimitSuccessCallsCount += 1
    }
    
    func presentAlertGenericError() {
        presentAlertGenericErrorCallsCount += 1
    }
    
    func presentUpdateLimitLoader() {
        presentUpdateLimitLoaderCallsCount += 1
    }
    
    func hideUpdateLimitLoader() {
        hideUpdateLimitLoaderCallsCount += 1
    }

    func presentExistingTicketAlertWith(id: String) {
        presentExistingTicketAlertCallsCount += 1
        self.existingTicketId = id
    }

    func presentTicketChatWith(id: String) {
        presentTicketChatCallsCount += 1
        self.chatTicketId = id
    }

    func presentLoading() {
        presentLoadingCallsCount += 1
    }

    func hideLoading() {
        hideLoadingCallsCount += 1
    }
}

final class DailyLimitInteractorTests: XCTestCase {
    private let service = DailyLimitServiceMock()
    private let presenter = DailyLimitPresenterSpy()
    private let zendesk = ZendeskMock()
    private let queue = DispatchQueue(label: #function)
    private lazy var dependencies = DependencyContainerMock(zendesk, queue, featureManager)
    private let pfAuth = AuthenticationContractMock()
    private let bizAuth = BizAuthManagerContractMock()
    private let featureManager = FeatureManagerMock()
    private var faqFlag: FeatureConfig = .pixDailyLimitFaqUrlPf
    private lazy var interactor = DailyLimitInteractor(
        service: service,
        presenter: presenter,
        dependencies: dependencies,
        faqFlag: faqFlag
    )
    
    // MARK: - getLimitData
    func testGetLimitData_WhenServiceResultIsFailure_ShouldCallPresentSkeletonLoaderAndHideSkeletonLoaderAndPresentGenericError() {
        interactor.getLimitData()
        XCTAssertEqual(presenter.presentSkeletonLoaderCallsCount, 1)
        XCTAssertEqual(presenter.hideSkeletonLoaderCallsCount, 1)
        XCTAssertEqual(presenter.presentGenericErrorCallsCount, 1)
    }
    
    func testGetLimitData_WhenServiceResultIsSuccess_ShouldCallPresentSkeletonLoaderAndHideSkeletonLoaderAndPresentLimitDataWithGivenDataAndMinValueEqualToZero() {
        let limitData = PixDailyLimit(
            current: 5.0,
            max: 10.0,
            usedToday: 2.0
        )
        service.getLimitResult = .success(limitData)
        interactor.getLimitData()
        XCTAssertEqual(presenter.presentSkeletonLoaderCallsCount, 1)
        XCTAssertEqual(presenter.hideSkeletonLoaderCallsCount, 1)
        XCTAssertEqual(presenter.presentLimitDataCallsCount, 1)
        XCTAssertEqual(presenter.dailyLimit, limitData)
        XCTAssertEqual(presenter.minValue, .zero)
    }
    
    // MARK: - changeDailyLimitValue
    func testChangeDailyLimitValue_WhenValueIsNotRounded_ShouldRoundTheValueBy100() {
        let limitData = PixDailyLimit(
            current: 500.0,
            max: 1000.0,
            usedToday: 200.0
        )
        let newValue: Float = 234.56
        service.getLimitResult = .success(limitData)
        interactor.getLimitData()
        interactor.changeDailyLimit(value: newValue)
        XCTAssertEqual(presenter.newLimitValue, 200.0)
    }
    
    func testChangeDailyLimitValue_WhenNewValueIsGreaterThanMaxLimit_ShouldCallPresentUnavailableLimitAmountErrorAndPresentUpdateButtonDisabledAndPresentNewDailyLimit() {
        let limitData = PixDailyLimit(
            current: 500.0,
            max: 1000.0,
            usedToday: 200.0
        )
        let newValue: Float = 1200.0
        service.getLimitResult = .success(limitData)
        interactor.getLimitData()
        interactor.changeDailyLimit(value: newValue)
        XCTAssertEqual(presenter.presentUnavailableLimitAmountErrorCallsCount, 1)
        XCTAssertEqual(presenter.presentUpdateButtonDisabledCallsCount, 1)
        XCTAssertEqual(presenter.presentNewDailyLimitCallsCount, 1)
        XCTAssertEqual(presenter.newLimitValue, Double(newValue))
    }
    
    func testChangeDailyLimitValue_WhenNewValueIsEqualToActualValue_ShouldCallHideUnavailableLimitAmountErrorAndPresentUpdateButtonDisabledAndPresentNewDailyLimit() {
        let limitData = PixDailyLimit(
            current: 500.0,
            max: 1000.0,
            usedToday: 200.0
        )
        let newValue: Float = 500.0
        service.getLimitResult = .success(limitData)
        interactor.getLimitData()
        interactor.changeDailyLimit(value: newValue)
        XCTAssertEqual(presenter.hideUnavailableLimitAmountErrorCallsCount, 1)
        XCTAssertEqual(presenter.presentUpdateButtonDisabledCallsCount, 1)
        XCTAssertEqual(presenter.presentNewDailyLimitCallsCount, 1)
        XCTAssertEqual(presenter.newLimitValue, Double(newValue))
    }
    
    func testChangeDailyLimitValue_WhenNewValueIsLessThanmaxLimitAndDifferentToActualLimit_ShouldCallPresentUnavailableLimitAmountErrorAndPresentUpdateButtonDisabledAndPresentNewDailyLimit() {
        let limitData = PixDailyLimit(
            current: 500.0,
            max: 1000.0,
            usedToday: 200.0
        )
        let newValue: Float = 700.0
        service.getLimitResult = .success(limitData)
        interactor.getLimitData()
        interactor.changeDailyLimit(value: newValue)
        XCTAssertEqual(presenter.hideUnavailableLimitAmountErrorCallsCount, 1)
        XCTAssertEqual(presenter.presentUpdateButtonEnabledCallsCount, 1)
        XCTAssertEqual(presenter.presentNewDailyLimitCallsCount, 1)
        XCTAssertEqual(presenter.newLimitValue, Double(newValue))
    }
    
    // MARK: - changeDailyLimitAmount
    func testChangeDailyLimitAmount_WhenAmountGreaterThanMaxAllowedValue_ShouldCallPresentNewDailyLimitWithCurrentDailyLimitValue() {
        let current: Double = 500.0
        let limitData = PixDailyLimit(
            current: current,
            max: 1000.0,
            usedToday: 200.0
        )
        let newValue = "R$ 10.000.000"
        service.getLimitResult = .success(limitData)
        interactor.getLimitData()
        interactor.changeDailyLimit(amount: newValue)
        XCTAssertEqual(presenter.presentNewDailyLimitCallsCount, 1)
        XCTAssertEqual(presenter.newLimitValue, current)
    }
    
    func testChangeDailyLimitAmount_WhenAmountHasNoNumbers_ShouldCallPresentNewDailyLimitWithZeroValue() {
        let limitData = PixDailyLimit(
            current: 500.0,
            max: 1000.0,
            usedToday: 200.0
        )
        let newValue = "teste testado"
        service.getLimitResult = .success(limitData)
        interactor.getLimitData()
        interactor.changeDailyLimit(amount: newValue)
        XCTAssertEqual(presenter.presentNewDailyLimitCallsCount, 1)
        XCTAssertEqual(presenter.newLimitValue, .zero)
    }
    
    func testChangeDailyLimitAmount_WhenNewValueIsGreaterThanMaxLimit_ShouldCallPresentUnavailableLimitAmountErrorAndPresentUpdateButtonDisabledAndPresentNewDailyLimit() {
        let limitData = PixDailyLimit(
            current: 500.0,
            max: 1000.0,
            usedToday: 200.0
        )
        let newValue = "R$ 1200"
        service.getLimitResult = .success(limitData)
        interactor.getLimitData()
        interactor.changeDailyLimit(amount: newValue)
        XCTAssertEqual(presenter.presentUnavailableLimitAmountErrorCallsCount, 1)
        XCTAssertEqual(presenter.presentUpdateButtonDisabledCallsCount, 1)
        XCTAssertEqual(presenter.presentNewDailyLimitCallsCount, 1)
        XCTAssertEqual(presenter.newLimitValue, 1200.0)
    }
    
    func testChangeDailyLimitAmount_WhenNewValueIsEqualToActualValue_ShouldCallPresentUpdateButtonDisabledAndPresentNewDailyLimit() {
        let limitData = PixDailyLimit(
            current: 500.0,
            max: 1000.0,
            usedToday: 200.0
        )
        let newValue = "R$ 500"
        service.getLimitResult = .success(limitData)
        interactor.getLimitData()
        interactor.changeDailyLimit(amount: newValue)
        XCTAssertEqual(presenter.presentUpdateButtonDisabledCallsCount, 1)
        XCTAssertEqual(presenter.presentNewDailyLimitCallsCount, 1)
        XCTAssertEqual(presenter.newLimitValue, 500.0)
    }
    
    func testChangeDailyLimitAmount_WhenNewValueIsLessThanmaxLimitAndDifferentToActualLimit_ShouldCallPresentUnavailableLimitAmountErrorAndPresentUpdateButtonDisabledAndPresentNewDailyLimit() {
        let limitData = PixDailyLimit(
            current: 500.0,
            max: 1000.0,
            usedToday: 200.0
        )
        let newValue = "R$ 700"
        service.getLimitResult = .success(limitData)
        interactor.getLimitData()
        interactor.changeDailyLimit(amount: newValue)
        XCTAssertEqual(presenter.hideUnavailableLimitAmountErrorCallsCount, 1)
        XCTAssertEqual(presenter.presentUpdateButtonEnabledCallsCount, 1)
        XCTAssertEqual(presenter.presentNewDailyLimitCallsCount, 1)
        XCTAssertEqual(presenter.newLimitValue, 700.0)
    }
    
    // MARK: - close
    func testClose_ShouldCallDidNextStep() {
        interactor.close()
        XCTAssertEqual(presenter.didNextStepCallsCount, 1)
    }

    // MARK: - updateDailyLimit
    func testUpdateDailyLimit_WhenDailyLimitIsLessThanOrEqualToZero_ShouldCallPresentUpdateLimitConfirmation() {
        interactor.updateDailyLimit()
        XCTAssertEqual(presenter.presentUpdateLimitConfirmationCallsCount, 1)
    }
    
    func testUpdateDailyLimit_WhenDailyLimitIsGreaterThanZeroAndAuthResultIsFailure_ShouldCallAuthenticateAndNotCallServiceUpdateLimitAndPresentUpdateLimitLoaderAndHideUpdateLimitLoaderAndPresentUpdateLimitSuccess() {
        let limitData = PixDailyLimit(
            current: 500.0,
            max: 1000.0,
            usedToday: 200.0
        )
        dependencies.authContract = pfAuth
        service.getLimitResult = .success(limitData)
        interactor.getLimitData()
        interactor.changeDailyLimit(value: 100)
        interactor.updateDailyLimit()
        XCTAssertEqual(pfAuth.authenticateCallsCount, 1)
        XCTAssertEqual(service.updateLimitCallsCount, 0)
        XCTAssertEqual(presenter.presentUpdateLimitLoaderCallsCount, 0)
        XCTAssertEqual(presenter.hideUpdateLimitLoaderCallsCount, 0)
        XCTAssertEqual(presenter.presentUpdateLimitSuccessCallsCount, 0)
    }
    
    func testUpdateDailyLimit_WhenDailyLimitIsGreaterThanZeroAndAuthResultIsSuccessAndResponseIsSuccess_ShouldCallAuthenticateAndServiceUpdateLimitAndPresentUpdateLimitLoaderAndHideUpdateLimitLoaderAndPresentUpdateLimitSuccess() {
        let limitData = PixDailyLimit(
            current: 500.0,
            max: 1000.0,
            usedToday: 200.0
        )
        let password = "1234"
        dependencies.authContract = pfAuth
        pfAuth.authenticationResult = .success(password)
        service.getLimitResult = .success(limitData)
        service.updateLimitResult = .success(NoContent())
        interactor.getLimitData()
        interactor.changeDailyLimit(value: 100)
        interactor.updateDailyLimit()
        XCTAssertEqual(pfAuth.authenticateCallsCount, 1)
        XCTAssertEqual(service.updateLimitCallsCount, 1)
        XCTAssertEqual(service.password, password)
        XCTAssertEqual(presenter.presentUpdateLimitLoaderCallsCount, 1)
        XCTAssertEqual(presenter.hideUpdateLimitLoaderCallsCount, 1)
        XCTAssertEqual(presenter.presentUpdateLimitSuccessCallsCount, 1)
    }
    
    func testUpdateDailyLimit_WhenDailyLimitIsGreaterThanZeroAndAuthResultIsSuccessAndResponseIsFailure_ShouldCallAuthenticateAndServiceUpdateLimitAndPresentUpdateLimitLoaderAndHideUpdateLimitLoaderAndPresentAlertGenericError() {
        let limitData = PixDailyLimit(
            current: 500.0,
            max: 1000.0,
            usedToday: 200.0
        )
        let password = "1234"
        dependencies.authContract = pfAuth
        pfAuth.authenticationResult = .success(password)
        service.getLimitResult = .success(limitData)
        interactor.getLimitData()
        interactor.changeDailyLimit(value: 100)
        interactor.updateDailyLimit()
        XCTAssertEqual(pfAuth.authenticateCallsCount, 1)
        XCTAssertEqual(service.updateLimitCallsCount, 1)
        XCTAssertEqual(service.password, password)
        XCTAssertEqual(presenter.presentUpdateLimitLoaderCallsCount, 1)
        XCTAssertEqual(presenter.hideUpdateLimitLoaderCallsCount, 1)
        XCTAssertEqual(presenter.presentAlertGenericErrorCallsCount, 1)
    }
    
    func testUpdateDailyLimit_WhenDailyLimitIsGreaterThanZeroAndPFAuthIsNilAndBizAuthResultIsNotSuccess_ShouldCallPerformActionWithAuthorizationAndNotCallServiceUpdateLimitAndPresentUpdateLimitLoaderAndHideUpdateLimitLoaderAndPresentUpdateLimitSuccess() {
        let limitData = PixDailyLimit(
            current: 500.0,
            max: 1000.0,
            usedToday: 200.0
        )
        dependencies.authContract = nil
        dependencies.businessAuthContract = bizAuth
        bizAuth.authorizationResult = .failure(errorMessage: "")
        service.getLimitResult = .success(limitData)
        interactor.getLimitData()
        interactor.changeDailyLimit(value: 100)
        interactor.updateDailyLimit()
        XCTAssertEqual(bizAuth.performActionWithAuthorizationCallsCount, 1)
        XCTAssertEqual(service.updateLimitCallsCount, 0)
        XCTAssertEqual(presenter.presentUpdateLimitLoaderCallsCount, 0)
        XCTAssertEqual(presenter.hideUpdateLimitLoaderCallsCount, 0)
        XCTAssertEqual(presenter.presentUpdateLimitSuccessCallsCount, 0)
    }
    
    func testUpdateDailyLimit_WhenDailyLimitIsGreaterThanZeroAndPFAuthIsNilAndBizAuthResultIsSuccessAndResponseIsSuccess_ShouldCallAuthenticateAndServiceUpdateLimitAndPresentUpdateLimitLoaderAndHideUpdateLimitLoaderAndPresentUpdateLimitSuccess() {
        let limitData = PixDailyLimit(
            current: 500.0,
            max: 1000.0,
            usedToday: 200.0
        )
        let password = "1234"
        dependencies.authContract = nil
        dependencies.businessAuthContract = bizAuth
        bizAuth.authorizationResult = .success(password)
        service.getLimitResult = .success(limitData)
        service.updateLimitResult = .success(NoContent())
        interactor.getLimitData()
        interactor.changeDailyLimit(value: 100)
        interactor.updateDailyLimit()
        XCTAssertEqual(bizAuth.performActionWithAuthorizationCallsCount, 1)
        XCTAssertEqual(service.updateLimitCallsCount, 1)
        XCTAssertEqual(service.password, password)
        XCTAssertEqual(presenter.presentUpdateButtonDisabledCallsCount, 1)
        XCTAssertEqual(presenter.presentUpdateLimitLoaderCallsCount, 1)
        XCTAssertEqual(presenter.hideUpdateLimitLoaderCallsCount, 1)
        XCTAssertEqual(presenter.presentUpdateLimitSuccessCallsCount, 1)
    }
    
    func testUpdateDailyLimit_WhenDailyLimitIsGreaterThanZeroAndPFAuthIsNilAndBizAuthResultIsSuccessAndResponseIsFailure_ShouldCallAuthenticateAndServiceUpdateLimitAndPresentUpdateLimitLoaderAndHideUpdateLimitLoaderAndPresentAlertGenericError() {
        let limitData = PixDailyLimit(
            current: 500.0,
            max: 1000.0,
            usedToday: 200.0
        )
        let password = "1234"
        dependencies.authContract = nil
        dependencies.businessAuthContract = bizAuth
        bizAuth.authorizationResult = .success(password)
        service.getLimitResult = .success(limitData)
        interactor.getLimitData()
        interactor.changeDailyLimit(value: 100)
        interactor.updateDailyLimit()
        XCTAssertEqual(bizAuth.performActionWithAuthorizationCallsCount, 1)
        XCTAssertEqual(service.updateLimitCallsCount, 1)
        XCTAssertEqual(service.password, password)
        XCTAssertEqual(presenter.presentUpdateLimitLoaderCallsCount, 1)
        XCTAssertEqual(presenter.hideUpdateLimitLoaderCallsCount, 1)
        XCTAssertEqual(presenter.presentAlertGenericErrorCallsCount, 1)
    }
    
    // MARK: - showFAQ
    func testShowFAQ_WhenFaqFlagIsPFFlag_ShouldCallDidNextStep() {
        featureManager.override(key: faqFlag, with: "testePF.br")
        interactor.showFAQ()
        XCTAssertEqual(presenter.didNextStepCallsCount, 1)
    }
    
    func testShowFAQ_WhenFaqFlagIsPJFlag_ShouldCallDidNextStep() {
        faqFlag = .pixDailyLimitFaqUrlPj
        featureManager.override(key: faqFlag, with: "testePJ.br")
        interactor.showFAQ()
        XCTAssertEqual(presenter.didNextStepCallsCount, 1)
    }

    // MARK: - showMaximumDailyLimit
    func testShowMaximumDailyLimit_WhenThereIsOtherTicketCreated_ShouldCallExistingTicketAlert() {
        let id = "id"
        zendesk.isAnyTicketOpenCompletionResult = id
        interactor.showMaximumDailyLimit()
        let expectation = XCTestExpectation(description: "DailyLimitInteractorExpectation")
        queue.async {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 0.5)
        XCTAssertEqual(presenter.presentExistingTicketAlertCallsCount, 1)
        XCTAssertEqual(presenter.existingTicketId, id)
    }

    func testShowMaximumDailyLimit_WhenThereIsNoTicketCreated_ShouldCallMaximumDailyLimitScreen() {
        zendesk.isAnyTicketOpenCompletionResult = nil
        interactor.showMaximumDailyLimit()
        let expectation = XCTestExpectation(description: "DailyLimitInteractorExpectation")
        queue.async {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 0.5)
        XCTAssertEqual(presenter.didNextStepCallsCount, 1)
        XCTAssertEqual(zendesk.isAnyTicketSubjects, ["Limite PIX"])
    }

    func testShowMaximumDailyLimit_ShouldCallPresentAndHideLoadingCalls() {
        interactor.showMaximumDailyLimit()
        let expectation = XCTestExpectation(description: "DailyLimitInteractorExpectation")
        queue.async {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 0.5)
        XCTAssertEqual(presenter.presentLoadingCallsCount, 1)
        XCTAssertEqual(presenter.hideLoadingCallsCount, 1)
    }

    // MARK: - openTicketChat
    func testOpenTicketChat_ShouldCallChatScreen() {
        let id = "id"
        interactor.openTicketChatWith(id: id)
        XCTAssertEqual(presenter.presentTicketChatCallsCount, 1)
        XCTAssertEqual(presenter.chatTicketId, id)
    }
}
