import AnalyticsModule
import Core
import XCTest
@testable import PIX

private final class FeedbacksDelegateMock: SpyViewController, FeedbackContainterDelegate, FeedbackDelegate {
    func didClose(feedbackType: FeedbackViewType) {}
    func didAction(action: FeedbackAction, feedbackType: FeedbackViewType) {}
}

private final class SecurityValidationCodeDelegateMock: SecurityValidationCodeDelegate {
    func didValidateKeyWithSuccess(keyValue: String, keyType: PixKeyType) {}
}

private final class FeedbackCancelContainterDelegateMock: FeedbackCancelContainterDelegate {
    func didClose(feedbackType: FeedbackViewType) {}
}

private final class TokenManagerMock: TokenManagerContract {
    func updateToken(_ token: String) {}
    var token: String = ""
}

final class KeyManagerConsumerCoordinatorTests: XCTestCase {
    private let viewControllerSpy = FeedbacksDelegateMock()
    private lazy var navControllerSpy = SpyNavigationController(rootViewController: viewControllerSpy)
    
    private lazy var sut: KeyManagerConsumerCoordinating = {
        let coordinator = KeyManagerConsumerCoordinator()
        coordinator.viewController = navControllerSpy.topViewController
        return coordinator
    }()
    
    private func topPresentedViewController() throws -> UIViewController {
        try XCTUnwrap((navControllerSpy.lastPresentedViewController as? UINavigationController)?.topViewController)
    }
    
    func testPerfom_WhenActionIsClose_ShouldDismissViewController() {
        sut.perform(action: .close)
        
        XCTAssertEqual(navControllerSpy.dismissCallCount, 1)
    }
    
    func testPerform_WhenActionIsFeedback_ShouldPresentFeedback() throws {
        sut.perform(action: .feedback(.keyRecordRegistered(uuid: "uuid"), origin: .undefined))

        XCTAssertEqual(navControllerSpy.presentViewControllerCallCount, 1)
        XCTAssertTrue(try topPresentedViewController() is FeedbackViewController)
    }

    func testPerform_WhenActionIsFeedbackAndFeedbackTypeIsPortabilityDonorReceived_ShouldPresentFeedbackContainer() throws {
        sut.perform(action: .feedback(.portabilityDonorReceived(uuid: "uuid"), origin: .undefined))

        XCTAssertEqual(navControllerSpy.presentViewControllerCallCount, 1)
        XCTAssertTrue(try topPresentedViewController() is FeedbackContainerViewController)
    }

    func testPerform_WhenActionIsFeedbackAndFeedbackTypeIsClaimDonorReceived_ShouldPresentFeedbackContainer() throws {
        sut.perform(action: .feedback(.claimDonorReceived(uuid: "uuid"), origin: .undefined))

        XCTAssertEqual(navControllerSpy.presentViewControllerCallCount, 1)
        XCTAssertTrue(try topPresentedViewController() is FeedbackContainerViewController)
    }
    
    func testPerform_WhenActionIsValidateKey_ShouldPushSecurityCodeValidationViewController() throws {
        let action = KeyManagerConsumerAction.validateKey(
            keyType: .email,
            keyValue: "email@email.com",
            codeResendDelay: 0,
            validationDelegate: SecurityValidationCodeDelegateMock()
        )
        sut.perform(action: action)
        
        XCTAssertEqual(navControllerSpy.pushViewControllerCallCount, 2)
        XCTAssertTrue(navControllerSpy.pushedViewController is SecurityCodeValidationViewController)
    }
    
    func testPerfom_WhenActionIsCancelClaimOrPortability_ShouldPresentFeedbackCancelContainer() {
        sut.perform(
            action: .cancelClaimOrPortability(
                type: .claimKeyCancel(uuid: ""),
                delegate: FeedbackCancelContainterDelegateMock(),
                origin: .keyManagementScreen
            )
        )
        
        XCTAssertEqual(navControllerSpy.presentViewControllerCallCount, 1)
        XCTAssertTrue(try topPresentedViewController() is FeedbackCancelContainerViewController)
    }
    
    func testPerfom_WhenActionIsIdentityValidation_ShouldPresentIdentityValidation() {
        PIXSetup.inject(instance: TokenManagerMock())
        
        sut.perform(action: .identityValidation({ _ in }))
        
        XCTAssertEqual(viewControllerSpy.presentCallCount, 1)
    }
}
