import XCTest

@testable import PIX

private final class SecurityValidationCodeDelegateMock: SecurityValidationCodeDelegate {
    func didValidateKeyWithSuccess(keyValue: String, keyType: PixKeyType) {}
}

private final class KeyManagerConsumerViewControllerSpy: KeyManagerConsumerDisplaying {
    // MARK: - displayKeys
    private(set) var displayKeysCallsCount = 0
    private(set) var sections = [KeyManagerSection]()
    
    func displayKeys(sections: [KeyManagerSection]) {
        displayKeysCallsCount += 1
        self.sections = sections
    }
    
    // MARK: - displayConsumerDataError
    private(set) var displayConsumerDataErrorCallsCount = 0
    private(set) var messageDisplayConsumerDataError: String?
    
    func displayConsumerDataError(message: String) {
        displayConsumerDataErrorCallsCount += 1
        messageDisplayConsumerDataError = message
    }
    
    // MARK: - displayErrorPopup
    private(set) var displayErrorPopupCallsCount = 0
    private(set) var popup: PixCreateKeyErrorModel?
    
    func displayErrorPopup(popup: PixCreateKeyErrorModel) {
        displayErrorPopupCallsCount += 1
        self.popup = popup
    }
    
    // MARK: - displayErrorPopupWithTitle
    private(set) var displayErrorPopupWithTitleCallsCount = 0
    private(set) var titleDisplayErrorPopupWithTitle: String?
    private(set) var messageDisplayErrorPopupWithTitle: String?
    
    func displayErrorPopup(title: String, message: String) {
        displayErrorPopupWithTitleCallsCount += 1
        titleDisplayErrorPopupWithTitle = title
        messageDisplayErrorPopupWithTitle = message
    }
    
    // MARK: - displayUpdateRegistrationPopup
    private(set) var displayUpdateRegistrationPopupCallsCount = 0
    private(set) var titleDisplayUpdateRegistrationPopup: String?
    private(set) var messageDisplayUpdateRegistrationPopup: String?
    
    func displayUpdateRegistrationPopup(title: String, message: String) {
        displayUpdateRegistrationPopupCallsCount += 1
        titleDisplayUpdateRegistrationPopup = title
        messageDisplayUpdateRegistrationPopup = message
    }
    
    // MARK: - displayLoading
    private(set) var displayLoadingCallsCount = 0
    
    func displayLoading() {
        displayLoadingCallsCount += 1
    }
    
    // MARK: - hideLoading
    private(set) var hideLoadingCallsCount = 0
    
    func hideLoading() {
        hideLoadingCallsCount += 1
    }
    
    // MARK: - displayDeleteKeyPopup
    private(set) var displayDeleteKeyPopupCallsCount = 0
    private(set) var titleDisplayDeleteKeyPopup: String?
    private(set) var messageDisplayDeleteKeyPopup: String?
    private(set) var keyIdDisplayDeleteKeyPopup: String?
    
    func displayDeleteKeyPopup(title: String, message: String, keyId: String) {
        displayDeleteKeyPopupCallsCount += 1
        titleDisplayDeleteKeyPopup = title
        messageDisplayDeleteKeyPopup = message
        keyIdDisplayDeleteKeyPopup = keyId
    }
    
    // MARK: - displayDeleteKeyConfirmationPopup
    private(set) var displayDeleteKeyConfirmationPopupCallsCount = 0
    private(set) var titleDisplayDeleteKeyConfirmationPopup: String?
    private(set) var messageDisplayDeleteKeyConfirmationPopup: String?
    
    func displayDeleteKeyConfirmationPopup(title: String, message: String) {
        displayDeleteKeyConfirmationPopupCallsCount += 1
        titleDisplayDeleteKeyConfirmationPopup = title
        messageDisplayDeleteKeyConfirmationPopup = message
    }
    
    // MARK: - displayBottomSheetMenu
    private(set) var displayBottomSheetMenuCallsCount = 0
    private(set) var keyItem: KeyManagerItem?
    private(set) var bottomSheetActions = [BottomSheetAction]()
    
    func displayBottomSheetMenu(with keyItem: KeyManagerItem, actions: [BottomSheetAction]) {
        displayBottomSheetMenuCallsCount += 1
        self.keyItem = keyItem
        bottomSheetActions = actions
    }
    
    // MARK: - displayShareMenu
    private(set) var displayShareMenuCallsCount = 0
    private(set) var sharingText: String?
    
    func displayShareMenu(sharingText: String) {
        displayShareMenuCallsCount += 1
        self.sharingText = sharingText
    }
    
    // MARK: - displayKeyCopiedAlert
    private(set) var displayKeyCopiedAlertCallsCount = 0
    private(set) var keyCopiedMessage: String?
    
    func displayKeyCopiedAlert(_ message: String) {
        displayKeyCopiedAlertCallsCount += 1
        keyCopiedMessage = message
    }
    
    // MARK: - displayCloseButton
    private(set) var displayCloseButtonCallsCount = 0
    
    func displayCloseButton() {
        displayCloseButtonCallsCount += 1
    }
}

private final class KeyManagerConsumerCoordinatorSpy: KeyManagerConsumerCoordinating {
    var viewController: UIViewController?
    
    // MARK: - perform
    private(set) var performCallsCount = 0
    private(set) var action: KeyManagerConsumerAction?
    
    func perform(action: KeyManagerConsumerAction) {
        performCallsCount += 1
        self.action = action
    }
}

final class KeyManagerConsumerPresenterTests: XCTestCase {
    private let coordinatorSpy = KeyManagerConsumerCoordinatorSpy()
    private let viewControllerSpy = KeyManagerConsumerViewControllerSpy()
    
    private lazy var sut: KeyManagerConsumerPresenting = {
        let presenter = KeyManagerConsumerPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testHideLoading_ShouldHideLoading() {
        sut.hideLoading()
        
        XCTAssertEqual(viewControllerSpy.hideLoadingCallsCount, 1)
    }
    
    func testPresentLoading_ShouldDisplayLoading() {
        sut.presentLoading()
        
        XCTAssertEqual(viewControllerSpy.displayLoadingCallsCount, 1)
    }
    
    func testPresentErrorScreen_ShouldDisplayConsumerDataError() {
        sut.presentErrorScreen(message: "Error")
        
        XCTAssertEqual(viewControllerSpy.displayConsumerDataErrorCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.messageDisplayConsumerDataError, "Error")
    }
    
    func testPresentErrorPopup_WithParameterIsPopup_ShouldDisplayErrorPopup() {
        let popup = PixCreateKeyErrorModel(image: nil, title: "title", description: "description", buttonTitle: "buttonTitle", errorStatus: .none)
        
        sut.presentErrorPopup(popup: popup)
        
        XCTAssertEqual(viewControllerSpy.displayErrorPopupCallsCount, 1)
        XCTAssertNil(viewControllerSpy.popup?.image)
        XCTAssertEqual(viewControllerSpy.popup?.title, "title")
        XCTAssertEqual(viewControllerSpy.popup?.description, "description")
        XCTAssertEqual(viewControllerSpy.popup?.buttonTitle, "buttonTitle")
        XCTAssertEqual(viewControllerSpy.popup?.errorStatus, CreateKeyClaimStatus.none)
    }
    
    func testPresentErrorPopup_WithParameterIsTitleAndMessage_ShoulddisplayErrorPopup() {
        sut.presentErrorPopup(title: "title", message: "message")
        
        XCTAssertEqual(viewControllerSpy.displayErrorPopupWithTitleCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.titleDisplayErrorPopupWithTitle, "title")
        XCTAssertEqual(viewControllerSpy.messageDisplayErrorPopupWithTitle, "message")
    }
    
    func testPresentUpdateRegistrationPopup_ShouldDisplayUpdateRegistrationPopup() {
        sut.presentUpdateRegistrationPopup(title: "title", message: "message")
        
        XCTAssertEqual(viewControllerSpy.displayUpdateRegistrationPopupCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.titleDisplayUpdateRegistrationPopup, "title")
        XCTAssertEqual(viewControllerSpy.messageDisplayUpdateRegistrationPopup, "message")
    }
    
    func testPresentDeleteKeyPopup_ShouldDisplayDeleteKeyPopup() {
        sut.presentDeleteKeyPopup(keyId: "keyId")
        
        XCTAssertEqual(viewControllerSpy.displayDeleteKeyPopupCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.titleDisplayDeleteKeyPopup, Strings.KeyManager.Consumer.DeleteKey.popupTitle)
        XCTAssertEqual(viewControllerSpy.messageDisplayDeleteKeyPopup, Strings.KeyManager.Consumer.DeleteKey.popupDescription)
        XCTAssertEqual(viewControllerSpy.keyIdDisplayDeleteKeyPopup, "keyId")
    }
    
    func testDidNextStep_WhenActionIsClose_ShouldPassActionToCoordinator() {
        sut.didNextStep(action: .close)
        
        XCTAssertEqual(coordinatorSpy.performCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .close)
    }
    
    func testDidNextStep_WhenActionIsRegistrationRenewal_ShouldPassActionToCoordinator() {
        sut.didNextStep(action: .registrationRenewal)
        
        XCTAssertEqual(coordinatorSpy.performCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .registrationRenewal)
    }
    
    func testDidNextStep_WhenActionIsFeedback_ShouldPassActionToCoordinator() {
        let feedback: KeyManagerConsumerAction = .feedback(.claimCompleted(uuid: "uuid"), origin: .keyManagementScreen)
        
        sut.didNextStep(action: feedback)
        
        XCTAssertEqual(coordinatorSpy.performCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.action, feedback)
    }
    
    func testDidNextStep_WhenActionIsValidateKey_ShouldPassActionToCoordinator() {
        let validateKey: KeyManagerConsumerAction = .validateKey(keyType: .email, keyValue: "email@email.com", codeResendDelay: 0, validationDelegate: SecurityValidationCodeDelegateMock())
        
        sut.didNextStep(action: validateKey)
        
        XCTAssertEqual(coordinatorSpy.performCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.action, validateKey)
    }
    
    func testPresentKeySections_ShouldDisplayKeys() {
        let userDate = PixUserData(email: "email@email.com", cpf: "000.000.000-00", phoneNumber: nil)
        
        let sections = KeyManagerConsumerSectionMock().mixedKeysMock
        
        let sectionRegistred = [KeyManagerSection(
            title: Strings.KeyManager.Consumer.Section.RegisteredKeys.title,
            items: [
                KeyManagerItem(
                    key: PixConsumerKeyMocks().processedKey(.cpf),
                    userData: userDate
                ),
                KeyManagerItem(
                    key: PixConsumerKeyMocks().processPendingKey(.email),
                    userData: userDate
                )
            ],
            description: ""
        )]
        
        let sectionUnregistered = [KeyManagerSection(
            title: Strings.KeyManager.Consumer.Section.UnregisteredKeys.title,
            items: [
                KeyManagerItem(
                    key: PixConsumerKeyMocks().unregisteredKey(.phone),
                    userData: userDate
                ),
                KeyManagerItem(
                    key: PixConsumerKeyMocks().unregisteredKey(.random),
                    userData: userDate
                )
            ],
            description: Strings.KeyManager.Consumer.Section.UnregisteredKeys.description
        )]
        
        sut.presentKeySections(sections, userData: userDate)
        
        XCTAssertEqual(viewControllerSpy.displayKeysCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.sections, sectionRegistred + sectionUnregistered)
    }
    
    func testPresentBottomSheetMenu_ShouldDisplayBottomSheetMenu() throws {
        let userDataMock = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        let keyMock = PixConsumerKeyMocks().key(.email, status: .processed)
        let deleteAction = BottomSheetAction(type: .delete) { _ in }
        let shareAction = BottomSheetAction(type: .share) { _ in }
        
        sut.presentBottomSheetMenu(userData: userDataMock, key: keyMock, actions: [deleteAction, shareAction])
        
        XCTAssertEqual(viewControllerSpy.displayBottomSheetMenuCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.keyItem, KeyManagerItem(key: keyMock, userData: userDataMock))
        XCTAssertEqual(viewControllerSpy.bottomSheetActions.count, 2)
        XCTAssertEqual(viewControllerSpy.bottomSheetActions.first?.type, .delete)
        XCTAssertEqual(viewControllerSpy.bottomSheetActions.last?.type, .share)
    }
    
    func testPresentShareMenu_ShouldDisplayShareMenu() throws {
        sut.presentShareMenu(keyValue: "02273410581")
        
        XCTAssertEqual(viewControllerSpy.displayShareMenuCallsCount, 1)
        XCTAssertEqual(
            viewControllerSpy.sharingText,
            "Olá! Essa é minha chave Pix: 02273410581. Use ela para me enviar pagamentos em qualquer horário, sem taxas."
        )
    }
    
    func testPresentKeyCopiedAlert_ShouldDisplayKeyCopiedAlert() throws {
        sut.presentKeyCopiedAlert()
        
        XCTAssertEqual(viewControllerSpy.displayKeyCopiedAlertCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.keyCopiedMessage, Strings.KeyManager.keyCopiedMessage)
    }
    
    func testPresentCloseButton_ShouldDisplayCloseButton() {
        sut.presentCloseButton()
        
        XCTAssertEqual(viewControllerSpy.displayCloseButtonCallsCount, 1)
    }
}
