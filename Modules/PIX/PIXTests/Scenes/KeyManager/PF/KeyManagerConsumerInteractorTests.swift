import AnalyticsModule
import AssetsKit
import Core
@testable import PIX
import XCTest

private final class FeedbackCancelContainterDelegateMock: FeedbackCancelContainterDelegate {
    func didClose(feedbackType: FeedbackViewType) {}
}

final class KeyManagerConsumerInteractorTests: XCTestCase {
    private let consumerServiceMock = KeyManagerConsumerServicingMock()
    private let pixKeyServiceMock = PixConsumerKeyServicingMock()
    private let presenterSpy = KeyManagerConsumerPresentingSpy()
    private let analyticsSpy = AnalyticsSpy()
    private lazy var dependenciesMock = DependencyContainerMock(analyticsSpy)
    
    private typealias InteractorType = KeyManagerConsumerInteracting & SecurityValidationCodeDelegate & PixCreateKeyErrorHandler
    
    private func createInteractor(
        origin: PixUserNavigation = .hub,
        checkForPreRegisteredKeys: Bool = false,
        isKeyRemovalCompleted: Bool = false,
        feedbackViewType: FeedbackViewType? = nil
    ) -> InteractorType {
        KeyManagerConsumerInteractor(
            consumerService: consumerServiceMock,
            pixKeyService: pixKeyServiceMock,
            presenter: presenterSpy,
            dependencies: dependenciesMock,
            origin: origin,
            checkForPreRegisteredKeys: checkForPreRegisteredKeys,
            feedbackViewType: feedbackViewType
        )
    }
    
    // MARK: setupCloseButton tests
    private func mockFullUserDataAndMixedKeys() throws {
        let userDataMock = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        consumerServiceMock.getConsumerDataResult = .success(userDataMock)
        consumerServiceMock.consumerId = 343523
        
        let keyListMock = PixKeyListResponseMocks().mixedKeysMock
        pixKeyServiceMock.listPixKeysResult = .success(keyListMock)
    }
    
    func testSetupCloseButton_WhenOriginIsHub_ShoudNotPresentCloseButton() {
        let sut = createInteractor(origin: .hub)
        
        sut.setupCloseButton()
        
        XCTAssertEqual(presenterSpy.presentCloseButtonCallsCount, 0)
    }
    
    func testSetupCloseButton_WhenOriginIsNotHub_ShoudPresentCloseButton() {
        let sut = createInteractor(origin: .deeplink)
        
        sut.setupCloseButton()
        
        XCTAssertEqual(presenterSpy.presentCloseButtonCallsCount, 1)
    }
    
    func testSetupCloseButton_WhenOriginIsNotHub_ShoudNotPresentCloseButton() {
        let sut = createInteractor(origin: .settings)
        
        sut.setupCloseButton()
        
        XCTAssertEqual(presenterSpy.presentCloseButtonCallsCount, 0)
    }
    
    // MARK: loadAndPresentKeys tests
    func testLoadAndPresentKeys_WhensReceivesFullUserDataAndUserKeys_ShouldLoadDataAndPresentKeys() throws {
        let userDataMock = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        consumerServiceMock.getConsumerDataResult = .success(userDataMock)
        consumerServiceMock.consumerId = 343523
        
        let keyListMock = PixKeyListResponseMocks().mixedKeysMock
        pixKeyServiceMock.listPixKeysResult = .success(keyListMock)
        
        let sut = createInteractor()

        sut.loadAndPresentKeys()

        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)

        XCTAssertEqual(consumerServiceMock.getConsumerDataCallsCount, 1)
        XCTAssertEqual(pixKeyServiceMock.listPixKeysCallsCount, 1)
        XCTAssertEqual(pixKeyServiceMock.userId, "\(343523)")

        XCTAssertEqual(presenterSpy.presentKeySectionsCallsCount, 1)
        let expectedKeySections = KeyManagerConsumerSectionMock().mixedKeysMock
        XCTAssertEqual(presenterSpy.keySections, expectedKeySections)
        XCTAssertEqual(presenterSpy.userData, userDataMock)
    }
    
    func testLoadAndPresentKeys_WhensReceivesNotFullUserDataAndUserKeys_ShouldLoadDataAndPresentKeys() throws {
        let userDataMock = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataNoPhoneCpfAndEmailMock")
        consumerServiceMock.getConsumerDataResult = .success(userDataMock)
        consumerServiceMock.consumerId = 343523
        
        let keyListMock = PixKeyListResponseMocks().mixedKeysMock
        pixKeyServiceMock.listPixKeysResult = .success(keyListMock)
        
        let sut = createInteractor()

        sut.loadAndPresentKeys()

        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)

        XCTAssertEqual(consumerServiceMock.getConsumerDataCallsCount, 1)
        XCTAssertEqual(pixKeyServiceMock.listPixKeysCallsCount, 1)
        XCTAssertEqual(pixKeyServiceMock.userId, "\(343523)")

        XCTAssertEqual(presenterSpy.presentKeySectionsCallsCount, 1)
        let expectedKeySections = KeyManagerConsumerSectionMock().mixedKeysMock
        XCTAssertEqual(presenterSpy.keySections, expectedKeySections)
        XCTAssertEqual(presenterSpy.userData, userDataMock)
    }
    
    func checkIfLoadedKeysAreCorrectlyPresented(keysResponse: PixConsumerKeyListResponse, expectedSections: [KeyManagerConsumerSection]) throws {
        let userDataMock = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        consumerServiceMock.getConsumerDataResult = .success(userDataMock)
        consumerServiceMock.consumerId = 343523

        pixKeyServiceMock.listPixKeysResult = .success(keysResponse)
        let sut = createInteractor()

        sut.loadAndPresentKeys()

        XCTAssertEqual(presenterSpy.presentKeySectionsCallsCount, 1)
        XCTAssertEqual(presenterSpy.keySections, expectedSections)
    }
    
    func testLoadAndPresentKeys_WhensReceivesUserDataAndOnlyUnregisteredKeys_ShouldLoadDataAndPresentKeys() throws {
        let keyListMock = PixKeyListResponseMocks().unregisteredKeysMock
        let expectedKeySections = KeyManagerConsumerSectionMock().unregisteredKeysMock
        try checkIfLoadedKeysAreCorrectlyPresented(keysResponse: keyListMock, expectedSections: expectedKeySections)
    }
    
    func testLoadAndPresentKeys_WhensReceivesUserDataAndOnlyRegisteredKeys_ShouldLoadDataAndPresentKeys() throws {
        let keyListMock = PixKeyListResponseMocks().registeredKeysMock
        let expectedKeySections = KeyManagerConsumerSectionMock().registeredKeysMock
        try checkIfLoadedKeysAreCorrectlyPresented(keysResponse: keyListMock, expectedSections: expectedKeySections)
    }
    
    func testLoadAndPresentKeys_WhensReceivesUserDataAndOnlyPreRegisteredKeys_ShouldLoadDataAndPresentKeys() throws {
        let keyListMock = PixKeyListResponseMocks().preRegisteredKeysMock
        let expectedKeySections = KeyManagerConsumerSectionMock().preRegisteredKeysMock
        try checkIfLoadedKeysAreCorrectlyPresented(keysResponse: keyListMock, expectedSections: expectedKeySections)
    }
    
    func testLoadAndPresentKeys_WhensReceivesPreRegisteredKeyCpfAndCheckForPreRegisteredKeysIsTrueAndValidationIsEnabled_ShouldOpenAuthentication() throws {
        let userDataMock = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        consumerServiceMock.getConsumerDataResult = .success(userDataMock)
        consumerServiceMock.consumerId = 343523
        consumerServiceMock.authenticationIsEnabled = true
        consumerServiceMock.requestPasswordFromUserResult = .success("")
        
        let keyListMock = PixKeyListResponseMocks().preRegisteredKeyMock(.cpf)
        pixKeyServiceMock.listPixKeysResult = .success(keyListMock)
        
        let createKeyResponse = PixConsumerKeyDataResponse(data: PixConsumerKeyMocks().processedKey(.cpf))
        pixKeyServiceMock.createNewPixKeyResult = .success(createKeyResponse)
        
        let sut = createInteractor(checkForPreRegisteredKeys: true)
        

        sut.loadAndPresentKeys()

        XCTAssertEqual(consumerServiceMock.requestPasswordFromUserCallsCount, 1)
    }
    
    func testLoadAndPresentKeys_WhensReceivesPreRegisteredKeyRamdomAndCheckForPreRegisteredKeysIsTrueAndValidationIsEnabled_ShouldOpenAuthentication() throws {
        let userDataMock = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        consumerServiceMock.getConsumerDataResult = .success(userDataMock)
        consumerServiceMock.consumerId = 343523
        consumerServiceMock.authenticationIsEnabled = true
        consumerServiceMock.requestPasswordFromUserResult = .success("")
        
        let keyListMock = PixKeyListResponseMocks().preRegisteredKeyMock(.cpf)
        pixKeyServiceMock.listPixKeysResult = .success(keyListMock)
        
        let createKeyResponse = PixConsumerKeyDataResponse(data: PixConsumerKeyMocks().processedKey(.random))
        pixKeyServiceMock.createNewPixKeyResult = .success(createKeyResponse)
        
        let sut = createInteractor(checkForPreRegisteredKeys: true)
        

        sut.loadAndPresentKeys()

        XCTAssertEqual(consumerServiceMock.requestPasswordFromUserCallsCount, 1)
    }
    
    func testLoadAndPresentKeys_WhensReceivesPreRegisteredKeyEmailAndCheckForPreRegisteredKeysIsTrue_ShouldOpenValidation() throws {
        let userDataMock = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        consumerServiceMock.getConsumerDataResult = .success(userDataMock)
        consumerServiceMock.consumerId = 343523
        
        let requestValidationCodeResponse = try JSONFileDecoder<SecurityCodeRequestResponse>().load(resource: "securityCodeRequestResponse")
        consumerServiceMock.requestNewValidationCodeResult = .success(requestValidationCodeResponse)
        
        let keyListMock = PixKeyListResponseMocks().preRegisteredKeyMock(.email)
        pixKeyServiceMock.listPixKeysResult = .success(keyListMock)
        
        let sut = createInteractor(checkForPreRegisteredKeys: true)
        
        sut.loadAndPresentKeys()

        XCTAssertEqual(consumerServiceMock.requestNewValidationCodeCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        let expectedAction = KeyManagerConsumerAction.validateKey(
            keyType: .email,
            keyValue: "eduardo.oliveira@picpay.com",
            codeResendDelay: requestValidationCodeResponse.emailDelay,
            validationDelegate: sut
        )
        XCTAssertEqual(presenterSpy.action, expectedAction)
    }
    
    func testLoadAndPresentKeys_WhensReceivesPreRegisteredKeyPhoneAndCheckForPreRegisteredKeysIsTrue_ShouldOpenValidation() throws {
        let userDataMock = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        consumerServiceMock.getConsumerDataResult = .success(userDataMock)
        consumerServiceMock.consumerId = 343523
        
        let requestValidationCodeResponse = try JSONFileDecoder<SecurityCodeRequestResponse>().load(resource: "securityCodeRequestResponse")
        consumerServiceMock.requestNewValidationCodeResult = .success(requestValidationCodeResponse)
        
        let keyListMock = PixKeyListResponseMocks().preRegisteredKeyMock(.phone)
        pixKeyServiceMock.listPixKeysResult = .success(keyListMock)
        
        let sut = createInteractor(checkForPreRegisteredKeys: true)
        
        sut.loadAndPresentKeys()

        XCTAssertEqual(consumerServiceMock.requestNewValidationCodeCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        let expectedAction = KeyManagerConsumerAction.validateKey(
            keyType: .phone,
            keyValue: "(11) 94141-4144",
            codeResendDelay: requestValidationCodeResponse.smsDelay,
            validationDelegate: sut
        )
        XCTAssertEqual(presenterSpy.action, expectedAction)
    }
    
    func testLoadAndPresentKeys_WhensReceivesUserDataErrorAndUserKeys_ShouldPresentScreenError() throws {
        consumerServiceMock.getConsumerDataResult = .failure(.connectionFailure)
        consumerServiceMock.consumerId = 343523
        
        let keyListMock = PixKeyListResponseMocks().mixedKeysMock
        pixKeyServiceMock.listPixKeysResult = .success(keyListMock)
        
        let sut = createInteractor()

        sut.loadAndPresentKeys()

        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)

        XCTAssertEqual(consumerServiceMock.getConsumerDataCallsCount, 1)
        XCTAssertEqual(pixKeyServiceMock.listPixKeysCallsCount, 0)
        
        XCTAssertEqual(presenterSpy.presentErrorScreenCallsCount, 1)
        XCTAssertEqual(presenterSpy.screenErrorMessage, Strings.KeyManager.Consumer.Error.UserData.description)
    }
    
    func testLoadAndPresentKeys_WhensReceivesFullUserDataAndUserKeysError_ShouldPresentScreenError() throws {
        let userDataMock = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        consumerServiceMock.getConsumerDataResult = .success(userDataMock)
        consumerServiceMock.consumerId = 343523
        
        pixKeyServiceMock.listPixKeysResult = .failure(.connectionFailure)
        
        let sut = createInteractor()

        sut.loadAndPresentKeys()

        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)

        XCTAssertEqual(consumerServiceMock.getConsumerDataCallsCount, 1)
        XCTAssertEqual(pixKeyServiceMock.listPixKeysCallsCount, 1)

        XCTAssertEqual(presenterSpy.presentErrorScreenCallsCount, 1)
        XCTAssertEqual(presenterSpy.screenErrorMessage, Strings.KeyManager.Consumer.Error.Request.description)
    }
    
    func testLoadAndPresentKeys_WhenInitiatedWithFeedbackTypeTrue_ShouldPresentFeedback() throws {
        let userDataMock = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        consumerServiceMock.getConsumerDataResult = .success(userDataMock)
        
        let feedbackType = FeedbackViewType.keyRecordNotRegistered(uuid: "1234")
        let sut = createInteractor(feedbackViewType: feedbackType)

        sut.loadAndPresentKeys()

        let expectedAction = KeyManagerConsumerAction.feedback(feedbackType, origin: .hub)

        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, expectedAction)
    }
    
    // MARK: reloadKeys tests
    func testReloadKeys_WhenUserDataIsNotLoaded_ShouldLoadUserDataAndKeys() throws {
        try mockFullUserDataAndMixedKeys()
        
        let sut = createInteractor()

        sut.reloadKeys()

        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 0)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)

        XCTAssertEqual(consumerServiceMock.getConsumerDataCallsCount, 1)
        XCTAssertEqual(pixKeyServiceMock.listPixKeysCallsCount, 1)

        XCTAssertEqual(presenterSpy.presentKeySectionsCallsCount, 1)
    }
    
    func testReloadKeys_WhenUserDataIsAlreadyLoaded_ShouldLoadUserDataOnlyOnce() throws {
        try mockFullUserDataAndMixedKeys()
        
        let sut = createInteractor()

        sut.loadAndPresentKeys()
        sut.reloadKeys()

        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 2)

        XCTAssertEqual(consumerServiceMock.getConsumerDataCallsCount, 1)
        XCTAssertEqual(pixKeyServiceMock.listPixKeysCallsCount, 2)

        XCTAssertEqual(presenterSpy.presentKeySectionsCallsCount, 2)
    }
    
    // MARK: didTapCell - Registered keys test
    func checkIfPresentedCorrectFeedback(_ feedback: FeedbackViewType, forKeyAtRow keyRow: Int) throws {
        let userDataMock = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        consumerServiceMock.getConsumerDataResult = .success(userDataMock)
        consumerServiceMock.consumerId = 343523
        let keyListMock = PixKeyListResponseMocks().registeredKeysWithMixedStatusMock
        pixKeyServiceMock.listPixKeysResult = .success(keyListMock)
        let sut = createInteractor()

        sut.loadAndPresentKeys()
        sut.didTapCell(at: IndexPath(row: keyRow, section: 0))
        
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        let expectedAction = KeyManagerConsumerAction.feedback(feedback, origin: .keyManagementScreen)
        XCTAssertEqual(presenterSpy.action, expectedAction)
    }
    
    func testDidTapCell_WhenTapKeyWithProcessPendingStatus_ShouldShowKeyRecordInProgressFeedback() throws {
        try checkIfPresentedCorrectFeedback(.keyRecordInProgress(uuid: ""), forKeyAtRow: 0)
    }
    
    func testDidTapCell_WhenTapKeyWithAwitingClaimConfirmStatus_ShouldShowClaimDonorReceivedFeedback() throws {
        try checkIfPresentedCorrectFeedback(.claimDonorReceived(uuid: ""), forKeyAtRow: 3)
    }
    
    func testDidTapCell_WhenTapKeyWithInactiveButCanRevalidateStatus_ShouldShowClaimDonorReceivedFeedback() throws {
        try checkIfPresentedCorrectFeedback(.claimDonorReceived(uuid: ""), forKeyAtRow: 4)
    }
    
    func testDidTapCell_WhenTapKeyWithAwaitingPortabilityConfirmStatus_ShouldShowPortabilityDonorReceivedFeedback() throws {
        try checkIfPresentedCorrectFeedback(.portabilityDonorReceived(uuid: ""), forKeyAtRow: 5)
    }
    
    func testDidTapCell_WhenTapKeyWithDeleteFromBacenStatus_ShouldShowPendingKeyDeletionFeedback() throws {
        try checkIfPresentedCorrectFeedback(.pendingKeyDeletion(uuid: ""), forKeyAtRow: 6)
    }
    
    func testDidTapCell_WhenTapKeyWithDeletedFromBacenStatus_ShouldShowPendingKeyDeletionFeedback() throws {
        try checkIfPresentedCorrectFeedback(.pendingKeyDeletion(uuid: ""), forKeyAtRow: 7)
    }
    
    // MARK: didTapCell - Registered keys test (FeedbackCancelContainer)
    func checkIfPresentedCorrectFeedbackCancelContainer(_ feedback: FeedbackCancelContainerType, forKeyAtRow keyRow: Int) throws {
        let userDataMock = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        consumerServiceMock.getConsumerDataResult = .success(userDataMock)
        consumerServiceMock.consumerId = 343523
        let keyListMock = PixKeyListResponseMocks().registeredKeysWithMixedStatusMock
        pixKeyServiceMock.listPixKeysResult = .success(keyListMock)
        let sut = createInteractor()

        sut.loadAndPresentKeys()
        sut.didTapCell(at: IndexPath(row: keyRow, section: 0))
        
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        let expectedAction = KeyManagerConsumerAction.cancelClaimOrPortability(type: feedback, delegate: FeedbackCancelContainterDelegateMock(), origin: .keyManagementScreen)
        XCTAssertEqual(presenterSpy.action, expectedAction)
    }
    
    func testDidTapCell_WhenTapdKeyWithClaimInProgressStatus_ShouldShowFeedbackCancelContainer() throws {
        try checkIfPresentedCorrectFeedbackCancelContainer(.claimKeyCancel(uuid: ""), forKeyAtRow: 1)
    }
    
    func testDidTapCell_WhenTapKeyWithPortabilityInProgressStatus_ShouldShowPortabilityInProgressFeedback() throws {
        try checkIfPresentedCorrectFeedbackCancelContainer(.portabilityKeyCancel(uuid: ""), forKeyAtRow: 2)
    }
    
    func testDidTapCell_WhenTapKeyWithProcessedStatus_ShouldPresentBottomSheetMenu() throws {
        let userDataMock = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        consumerServiceMock.getConsumerDataResult = .success(userDataMock)
        consumerServiceMock.consumerId = 343523
        
        let keyListMock = PixKeyListResponseMocks().registeredKeysMock
        pixKeyServiceMock.listPixKeysResult = .success(keyListMock)
        
        let sut = createInteractor()

        sut.loadAndPresentKeys()
        sut.didTapCell(at: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(presenterSpy.presentBottomSheetMenuCallsCount, 1)
        XCTAssertEqual(presenterSpy.userDataForBottomSheet, userDataMock)
        XCTAssertEqual(presenterSpy.keyForBottomSheet, keyListMock.data.first)
        XCTAssertEqual(presenterSpy.actionsForBottomSheet.count, 3)
        XCTAssertEqual(presenterSpy.actionsForBottomSheet[0].type, .delete)
        XCTAssertEqual(presenterSpy.actionsForBottomSheet[1].type, .share)
        XCTAssertEqual(presenterSpy.actionsForBottomSheet[2].type, .copy)
    }
    
    func testDidTapCell_WhenTapKeyWithProcessedStatusAndTapDelete_ShouldPresentBottomSheetMenuAndDeletePopup() throws {
        let userDataMock = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        consumerServiceMock.getConsumerDataResult = .success(userDataMock)
        consumerServiceMock.consumerId = 343523
        
        let keyListMock = PixKeyListResponseMocks().registeredKeysMock
        pixKeyServiceMock.listPixKeysResult = .success(keyListMock)
        
        let selectedKey = PixConsumerKeyMocks().processedKey(.cpf)
        
        let sut = createInteractor()

        sut.loadAndPresentKeys()
        sut.didTapCell(at: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(presenterSpy.presentBottomSheetMenuCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionsForBottomSheet.count, 3)
        
        let bottomSheetAction = presenterSpy.actionsForBottomSheet[0]
        XCTAssertEqual(bottomSheetAction.type, .delete)
        
        bottomSheetAction.handler(KeyManagerItem(key: selectedKey, userData: userDataMock))
        XCTAssertEqual(presenterSpy.presentDeleteKeyPopupCallsCount, 1)
        XCTAssertEqual(presenterSpy.keyIdDeleteKeyPopup, selectedKey.id)
    }
    
    func testDidTapCell_WhenTapKeyWithProcessedStatusAndTapShare_ShouldPresentBottomSheetMenuAndShareKey() throws {
        let userDataMock = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        consumerServiceMock.getConsumerDataResult = .success(userDataMock)
        consumerServiceMock.consumerId = 343523
        
        let keyListMock = PixKeyListResponseMocks().registeredKeysMock
        pixKeyServiceMock.listPixKeysResult = .success(keyListMock)
        
        let selectedKey = PixConsumerKeyMocks().processedKey(.cpf)
        
        let sut = createInteractor()

        sut.loadAndPresentKeys()
        sut.didTapCell(at: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(presenterSpy.presentBottomSheetMenuCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionsForBottomSheet.count, 3)
        
        let bottomSheetAction = presenterSpy.actionsForBottomSheet[1]
        XCTAssertEqual(bottomSheetAction.type, .share)
        
        bottomSheetAction.handler(KeyManagerItem(key: selectedKey, userData: userDataMock))
        XCTAssertEqual(presenterSpy.presentShareMenuCallsCount, 1)
        XCTAssertEqual(presenterSpy.keyValuePresentShareMenu, selectedKey.attributes.keyValue)
    }
    
    func testDidTapCell_WhenTapKeyWithProcessedStatusAndTapCopy_ShouldPresentBottomSheetMenuAndCopyKey() throws {
        let userDataMock = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        consumerServiceMock.getConsumerDataResult = .success(userDataMock)
        consumerServiceMock.consumerId = 343523
        
        let keyListMock = PixKeyListResponseMocks().registeredKeysMock
        pixKeyServiceMock.listPixKeysResult = .success(keyListMock)
        
        let selectedKey = PixConsumerKeyMocks().processedKey(.cpf)
        
        let sut = createInteractor()

        sut.loadAndPresentKeys()
        sut.didTapCell(at: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(presenterSpy.presentBottomSheetMenuCallsCount, 1)
        XCTAssertEqual(presenterSpy.actionsForBottomSheet.count, 3)
        
        let bottomSheetAction = presenterSpy.actionsForBottomSheet[2]
        XCTAssertEqual(bottomSheetAction.type, .copy)
        
        bottomSheetAction.handler(KeyManagerItem(key: selectedKey, userData: userDataMock))
        XCTAssertEqual(consumerServiceMock.setOnClipboardCallsCount, 1)
        XCTAssertEqual(consumerServiceMock.keyValueToClipboard, selectedKey.attributes.keyValue)
        XCTAssertEqual(presenterSpy.presentKeyCopiedAlertCallsCount, 1)
    }
    
    func testDidTapCell_WhenTapKeyWithInvalidIndex_ShouldNotShowAnyFeedback() throws {
        let userDataMock = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        consumerServiceMock.getConsumerDataResult = .success(userDataMock)
        consumerServiceMock.consumerId = 343523
        
        let keyListMock = PixKeyListResponseMocks().registeredKeysMock
        pixKeyServiceMock.listPixKeysResult = .success(keyListMock)
        
        let sut = createInteractor()

        sut.loadAndPresentKeys()
        sut.didTapCell(at: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 0)
    }
    
    // MARK: didTapCell - Unregistered keys tests
    func testDidTapCell_WhenTapUnregistedCpfCellAndValidationIsEnabledAndSucccessAuthenticatedAndSuccessRegisteringKey_ShouldRegisterKey() throws {
        let userDataResponse = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        consumerServiceMock.getConsumerDataResult = .success(userDataResponse)
        consumerServiceMock.consumerId = 343523
        consumerServiceMock.authenticationIsEnabled = true
        consumerServiceMock.identityValidationIsEnabled = false
        consumerServiceMock.getIdValidationStatusResult = .success(IdentityValidationStatus(status: .verified))
        
        let authenticationResponse = "1234"
        consumerServiceMock.requestPasswordFromUserResult = .success(authenticationResponse)
        
        let keyListResponse = PixKeyListResponseMocks().unregisteredKeysMock
        pixKeyServiceMock.listPixKeysResult = .success(keyListResponse)
        
        let createKeyResponse = PixConsumerKeyDataResponse(data: PixConsumerKeyMocks().processedKey(.cpf))
        pixKeyServiceMock.createNewPixKeyResult = .success(createKeyResponse)
        
        let sut = createInteractor()
        
        sut.loadAndPresentKeys()
        sut.didTapCell(at: IndexPath(item: 0, section: 0))
        
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 3)
        XCTAssertEqual(presenterSpy.presentErrorScreenCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentErrorPopupCallsCount, 0)

        XCTAssertEqual(consumerServiceMock.getConsumerDataCallsCount, 1)
        XCTAssertEqual(pixKeyServiceMock.listPixKeysCallsCount, 2)
        XCTAssertEqual(consumerServiceMock.requestPasswordFromUserCallsCount, 1)
        XCTAssertEqual(pixKeyServiceMock.createNewPixKeyCallsCount, 1)
        XCTAssertEqual(pixKeyServiceMock.password, "1234")
        let expectedKeyToBeCreated = CreateKey(userId: "343523", userType: .person, value: "52709314274", type: .cpf, name: nil)
        XCTAssertEqual(pixKeyServiceMock.keyToBeCreated, expectedKeyToBeCreated)
    }
    
    func testDidTapCell_WhenTapUnregistedRandomKeyCellAndValidationIsEnabledAndSucccessAuthenticatedAndSuccessRegisteringKey_ShouldRegisterKey() throws {
        let userDataResponse = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        consumerServiceMock.getConsumerDataResult = .success(userDataResponse)
        consumerServiceMock.consumerId = 546743
        consumerServiceMock.authenticationIsEnabled = true
        consumerServiceMock.identityValidationIsEnabled = false
        consumerServiceMock.getIdValidationStatusResult = .success(IdentityValidationStatus(status: .verified))
        
        let authenticationResponse = "3456"
        consumerServiceMock.requestPasswordFromUserResult = .success(authenticationResponse)
        
        let keyListResponse = PixKeyListResponseMocks().unregisteredKeysMock
        
        pixKeyServiceMock.listPixKeysResult = .success(keyListResponse)
        let createKeyResponse = PixConsumerKeyDataResponse(data: PixConsumerKeyMocks().processedKey(.cpf))
        
        pixKeyServiceMock.createNewPixKeyResult = .success(createKeyResponse)
        
        let sut = createInteractor()
        
        sut.loadAndPresentKeys()
        sut.didTapCell(at: IndexPath(item: 3, section: 0))
        
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 3)
        XCTAssertEqual(presenterSpy.presentErrorScreenCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentErrorPopupCallsCount, 0)

        XCTAssertEqual(consumerServiceMock.getConsumerDataCallsCount, 1)
        XCTAssertEqual(pixKeyServiceMock.listPixKeysCallsCount, 2)
        XCTAssertEqual(consumerServiceMock.requestPasswordFromUserCallsCount, 1)
        XCTAssertEqual(pixKeyServiceMock.createNewPixKeyCallsCount, 1)
        XCTAssertEqual(pixKeyServiceMock.password, "3456")
        let expectedKeyToBeCreated = CreateKey(
            userId: "546743",
            userType: .person,
            value: "",
            type: .random,
            name: Strings.KeyManager.Consumer.KeyTitle.random
        )
        XCTAssertEqual(pixKeyServiceMock.keyToBeCreated, expectedKeyToBeCreated)
    }
    
    func testDidTapCell_WhenTapUnregistedPhoneCell_ShouldOpenKeyValidation() throws {
        let userDataResponse = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        consumerServiceMock.getConsumerDataResult = .success(userDataResponse)
        consumerServiceMock.consumerId = 435345
        consumerServiceMock.authenticationIsEnabled = true
        consumerServiceMock.identityValidationIsEnabled = false
        consumerServiceMock.getIdValidationStatusResult = .success(IdentityValidationStatus(status: .verified))
        
        let requestValidationCodeResponse = try JSONFileDecoder<SecurityCodeRequestResponse>().load(resource: "securityCodeRequestResponse")
        consumerServiceMock.requestNewValidationCodeResult = .success(requestValidationCodeResponse)
        
        let keyListResponse = PixKeyListResponseMocks().unregisteredKeysMock
        pixKeyServiceMock.listPixKeysResult = .success(keyListResponse)
        
        let sut = createInteractor()
        
        sut.loadAndPresentKeys()
        sut.didTapCell(at: IndexPath(item: 1, section: 0))
        
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.presentErrorScreenCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentErrorPopupCallsCount, 0)

        XCTAssertEqual(consumerServiceMock.getConsumerDataCallsCount, 1)
        XCTAssertEqual(pixKeyServiceMock.listPixKeysCallsCount, 1)
        XCTAssertEqual(consumerServiceMock.requestNewValidationCodeCallsCount, 1)
        
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        let expectedAction = KeyManagerConsumerAction.validateKey(
            keyType: .phone,
            keyValue: "(11) 94141-4144",
            codeResendDelay: requestValidationCodeResponse.smsDelay,
            validationDelegate: sut
        )
        XCTAssertEqual(presenterSpy.action, expectedAction)
    }
    
    func testDidTapCell_WhenTapUnregistedEmailCell_ShouldOpenKeyValidation() throws {
        let userDataResponse = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        consumerServiceMock.getConsumerDataResult = .success(userDataResponse)
        consumerServiceMock.consumerId = 546743
        consumerServiceMock.authenticationIsEnabled = true
        consumerServiceMock.identityValidationIsEnabled = false
        consumerServiceMock.getIdValidationStatusResult = .success(IdentityValidationStatus(status: .verified))
        
        let requestValidationCodeResponse = try JSONFileDecoder<SecurityCodeRequestResponse>().load(resource: "securityCodeRequestResponse")
        consumerServiceMock.requestNewValidationCodeResult = .success(requestValidationCodeResponse)
        
        let keyListResponse = PixKeyListResponseMocks().unregisteredKeysMock
        pixKeyServiceMock.listPixKeysResult = .success(keyListResponse)
        
        let sut = createInteractor()
        
        sut.loadAndPresentKeys()
        sut.didTapCell(at: IndexPath(item: 2, section: 0))
        
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.presentErrorScreenCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentErrorPopupCallsCount, 0)

        XCTAssertEqual(consumerServiceMock.getConsumerDataCallsCount, 1)
        XCTAssertEqual(pixKeyServiceMock.listPixKeysCallsCount, 1)
        XCTAssertEqual(consumerServiceMock.requestNewValidationCodeCallsCount, 1)
        
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        let expectedAction = KeyManagerConsumerAction.validateKey(
            keyType: .email,
            keyValue: userDataResponse.email,
            codeResendDelay: requestValidationCodeResponse.emailDelay,
            validationDelegate: sut
        )
        XCTAssertEqual(presenterSpy.action, expectedAction)
    }
    
    func testDidTapCell_WhenTapUnregistedCellAndIdIsNotVerifiedAndIdValidationIsEnabled_ShouldOpenIdentityValidation() throws {
        let userDataResponse = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        consumerServiceMock.getConsumerDataResult = .success(userDataResponse)
        consumerServiceMock.consumerId = 546743
        consumerServiceMock.authenticationIsEnabled = true
        consumerServiceMock.identityValidationIsEnabled = true
        consumerServiceMock.getIdValidationStatusResult = .success(IdentityValidationStatus(status: .notVerified))
        
        let keyListResponse = PixKeyListResponseMocks().unregisteredKeysMock
        pixKeyServiceMock.listPixKeysResult = .success(keyListResponse)
        
        let sut = createInteractor()
        
        sut.loadAndPresentKeys()
        sut.didTapCell(at: IndexPath(item: 0, section: 0))
        
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.presentErrorScreenCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentErrorPopupCallsCount, 0)

        XCTAssertEqual(consumerServiceMock.getConsumerDataCallsCount, 1)
        XCTAssertEqual(pixKeyServiceMock.listPixKeysCallsCount, 1)
        
        let expectedAction = KeyManagerConsumerAction.identityValidation { _ in }
        XCTAssertEqual(presenterSpy.action, expectedAction)
    }
    
    func testDidTapCell_WhenTapUnregistedCellAndIdIsNotVerifiedAndIdValidationIsEnabledAndValidationReturnsApproved_ShouldOpenIdentityValidationAndAuthentication() throws {
        let userDataResponse = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        consumerServiceMock.getConsumerDataResult = .success(userDataResponse)
        consumerServiceMock.consumerId = 546743
        consumerServiceMock.authenticationIsEnabled = true
        consumerServiceMock.identityValidationIsEnabled = true
        consumerServiceMock.getIdValidationStatusResult = .success(IdentityValidationStatus(status: .notVerified))
        consumerServiceMock.requestPasswordFromUserResult = .success("")
        
        let keyListResponse = PixKeyListResponseMocks().unregisteredKeysMock
        pixKeyServiceMock.listPixKeysResult = .success(keyListResponse)
        pixKeyServiceMock.createNewPixKeyResult = .failure(.connectionFailure)
        
        let sut = createInteractor()
        
        sut.loadAndPresentKeys()
        sut.didTapCell(at: IndexPath(item: 0, section: 0))
        
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.presentErrorScreenCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentErrorPopupCallsCount, 0)

        XCTAssertEqual(consumerServiceMock.getConsumerDataCallsCount, 1)
        XCTAssertEqual(pixKeyServiceMock.listPixKeysCallsCount, 1)
        
        let expectedAction = KeyManagerConsumerAction.identityValidation { _ in }
        XCTAssertEqual(presenterSpy.action, expectedAction)
        
        guard case .identityValidation(let completion) = presenterSpy.action else {
            return
        }
        completion(.approved)
        XCTAssertEqual(consumerServiceMock.requestPasswordFromUserCallsCount, 1)
    }
    
    func testDidTapCell_WhenTapUnregistedCellAndIdIsNotVerifiedAndIdValidationIsEnabledAndValidationReturnsVerified_ShouldOpenIdentityValidationAndAuthentication() throws {
        let userDataResponse = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        consumerServiceMock.getConsumerDataResult = .success(userDataResponse)
        consumerServiceMock.consumerId = 546743
        consumerServiceMock.authenticationIsEnabled = true
        consumerServiceMock.identityValidationIsEnabled = true
        consumerServiceMock.getIdValidationStatusResult = .success(IdentityValidationStatus(status: .notVerified))
        consumerServiceMock.requestPasswordFromUserResult = .success("")
        
        let keyListResponse = PixKeyListResponseMocks().unregisteredKeysMock
        pixKeyServiceMock.listPixKeysResult = .success(keyListResponse)
        pixKeyServiceMock.createNewPixKeyResult = .failure(.connectionFailure)
        
        let sut = createInteractor()
        
        sut.loadAndPresentKeys()
        sut.didTapCell(at: IndexPath(item: 0, section: 0))
        
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.presentErrorScreenCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentErrorPopupCallsCount, 0)

        XCTAssertEqual(consumerServiceMock.getConsumerDataCallsCount, 1)
        XCTAssertEqual(pixKeyServiceMock.listPixKeysCallsCount, 1)
        
        let expectedAction = KeyManagerConsumerAction.identityValidation { _ in }
        XCTAssertEqual(presenterSpy.action, expectedAction)
        
        guard case .identityValidation(let completion) = presenterSpy.action else {
            return
        }
        completion(.verified)
        XCTAssertEqual(consumerServiceMock.requestPasswordFromUserCallsCount, 1)
    }
    
    func testDidTapCell_WhenTapUnregistedCellAndIdIsNotVerifiedAndIdValidationIsEnabledAndValidationReturnsPending_ShouldOpenIdentityValidationAndNotAuthentication() throws {
        let userDataResponse = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        consumerServiceMock.getConsumerDataResult = .success(userDataResponse)
        consumerServiceMock.consumerId = 546743
        consumerServiceMock.authenticationIsEnabled = true
        consumerServiceMock.identityValidationIsEnabled = true
        consumerServiceMock.getIdValidationStatusResult = .success(IdentityValidationStatus(status: .notVerified))
        consumerServiceMock.requestPasswordFromUserResult = .success("")
        
        let keyListResponse = PixKeyListResponseMocks().unregisteredKeysMock
        pixKeyServiceMock.listPixKeysResult = .success(keyListResponse)
        pixKeyServiceMock.createNewPixKeyResult = .failure(.connectionFailure)
        
        let sut = createInteractor()
        
        sut.loadAndPresentKeys()
        sut.didTapCell(at: IndexPath(item: 0, section: 0))
        
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.presentErrorScreenCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentErrorPopupCallsCount, 0)

        XCTAssertEqual(consumerServiceMock.getConsumerDataCallsCount, 1)
        XCTAssertEqual(pixKeyServiceMock.listPixKeysCallsCount, 1)
        
        let expectedAction = KeyManagerConsumerAction.identityValidation { _ in }
        XCTAssertEqual(presenterSpy.action, expectedAction)
        
        guard case .identityValidation(let completion) = presenterSpy.action else {
            return
        }
        completion(.pending)
        XCTAssertEqual(consumerServiceMock.requestPasswordFromUserCallsCount, 0)
    }
    
    func testDidTapCell_WhenTapUnregistedCellAndIdStatusReturnsErrorAndIdValidationIsEnabled_ShouldOpenIdentityValidation() throws {
        let userDataResponse = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        consumerServiceMock.getConsumerDataResult = .success(userDataResponse)
        consumerServiceMock.consumerId = 546743
        consumerServiceMock.authenticationIsEnabled = true
        consumerServiceMock.identityValidationIsEnabled = true
        consumerServiceMock.getIdValidationStatusResult = .failure(.connectionFailure)
        
        let keyListResponse = PixKeyListResponseMocks().unregisteredKeysMock
        pixKeyServiceMock.listPixKeysResult = .success(keyListResponse)
        
        let sut = createInteractor()
        
        sut.loadAndPresentKeys()
        sut.didTapCell(at: IndexPath(item: 0, section: 0))
        
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.presentErrorScreenCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentErrorPopupCallsCount, 0)

        XCTAssertEqual(consumerServiceMock.getConsumerDataCallsCount, 1)
        XCTAssertEqual(pixKeyServiceMock.listPixKeysCallsCount, 1)
        
        let expectedAction = KeyManagerConsumerAction.identityValidation { _ in }
        XCTAssertEqual(presenterSpy.action, expectedAction)
    }
    
    func testDidTapCell_WhenTapUnregistedCellAndIdIsVerifiedAndIdValidationIsEnabled_ShouldNotOpenIdentityValidation() throws {
        let userDataResponse = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        consumerServiceMock.getConsumerDataResult = .success(userDataResponse)
        consumerServiceMock.consumerId = 546743
        consumerServiceMock.authenticationIsEnabled = true
        consumerServiceMock.identityValidationIsEnabled = true
        consumerServiceMock.requestPasswordFromUserResult = .success("")
        consumerServiceMock.getIdValidationStatusResult = .success(IdentityValidationStatus(status: .verified))
        
        let keyListResponse = PixKeyListResponseMocks().unregisteredKeysMock
        pixKeyServiceMock.listPixKeysResult = .success(keyListResponse)
        
        let createKeyResponse = PixConsumerKeyDataResponse(data: PixConsumerKeyMocks().processedKey(.cpf))
        pixKeyServiceMock.createNewPixKeyResult = .success(createKeyResponse)
        
        let sut = createInteractor()
        
        sut.loadAndPresentKeys()
        sut.didTapCell(at: IndexPath(item: 0, section: 0))
        
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 3)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 4)
        XCTAssertEqual(presenterSpy.presentErrorScreenCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentErrorPopupCallsCount, 0)

        XCTAssertEqual(consumerServiceMock.getConsumerDataCallsCount, 1)
        XCTAssertEqual(pixKeyServiceMock.listPixKeysCallsCount, 2)
        XCTAssertEqual(consumerServiceMock.requestPasswordFromUserCallsCount, 1)
        
        let expectedAction = KeyManagerConsumerAction.feedback(
            .keyRecordRegistered(uuid: "e9a319ce-c2cf-4dcc-8212-35b111b33c7d"),
            origin: .keyAuthentication
        )
        XCTAssertEqual(presenterSpy.action, expectedAction)
    }
    
    // MARK: didValidateKeyWithSuccess tests
    func testDidValidateKeyWithSuccess_WhenIsPhone_ShouldRegisterKeyWithSuccess() throws {
        let userDataResponse = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        consumerServiceMock.getConsumerDataResult = .success(userDataResponse)
        consumerServiceMock.consumerId = 435345
        consumerServiceMock.authenticationIsEnabled = true
        
        let authenticationResponse = "3456"
        consumerServiceMock.requestPasswordFromUserResult = .success(authenticationResponse)
        
        let requestValidationCodeResponse = try JSONFileDecoder<SecurityCodeRequestResponse>().load(resource: "securityCodeRequestResponse")
        consumerServiceMock.requestNewValidationCodeResult = .success(requestValidationCodeResponse)
        
        let keyListResponse = PixKeyListResponseMocks().unregisteredKeysMock
        pixKeyServiceMock.listPixKeysResult = .success(keyListResponse)
        
        let createKeyResponse = PixConsumerKeyDataResponse(data: PixConsumerKeyMocks().processedKey(.cpf))
        pixKeyServiceMock.createNewPixKeyResult = .success(createKeyResponse)
        
        let sut = createInteractor()
        
        sut.loadAndPresentKeys()
        sut.didValidateKeyWithSuccess(keyValue: "11941414144", keyType: .phone)
        
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 3)
        XCTAssertEqual(presenterSpy.presentErrorScreenCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentErrorPopupCallsCount, 0)

        XCTAssertEqual(consumerServiceMock.getConsumerDataCallsCount, 1)
        XCTAssertEqual(pixKeyServiceMock.listPixKeysCallsCount, 2)
        XCTAssertEqual(consumerServiceMock.requestNewValidationCodeCallsCount, 0)
        
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        let expectedAction = KeyManagerConsumerAction.feedback(
            .keyRecordRegistered(uuid: "e9a319ce-c2cf-4dcc-8212-35b111b33c7d"),
            origin: .keyAuthentication
        )
        XCTAssertEqual(presenterSpy.action, expectedAction)
    }
    
    func testDidValidateKeyWithSuccess_WhenIsEmail_ShouldRegisterKeyWithSuccess() throws {
        let userDataResponse = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        consumerServiceMock.getConsumerDataResult = .success(userDataResponse)
        consumerServiceMock.consumerId = 435345
        consumerServiceMock.authenticationIsEnabled = true
        
        let authenticationResponse = "3456"
        consumerServiceMock.requestPasswordFromUserResult = .success(authenticationResponse)
        
        let requestValidationCodeResponse = try JSONFileDecoder<SecurityCodeRequestResponse>().load(resource: "securityCodeRequestResponse")
        consumerServiceMock.requestNewValidationCodeResult = .success(requestValidationCodeResponse)
        
        let keyListResponse = PixKeyListResponseMocks().unregisteredKeysMock
        pixKeyServiceMock.listPixKeysResult = .success(keyListResponse)
        
        let createKeyResponse = PixConsumerKeyDataResponse(data: PixConsumerKeyMocks().processedKey(.email))
        pixKeyServiceMock.createNewPixKeyResult = .success(createKeyResponse)
        
        let sut = createInteractor()
        
        sut.loadAndPresentKeys()
        sut.didValidateKeyWithSuccess(keyValue: "abc@google.com", keyType: .email)
        
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 3)
        XCTAssertEqual(presenterSpy.presentErrorScreenCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentErrorPopupCallsCount, 0)

        XCTAssertEqual(consumerServiceMock.getConsumerDataCallsCount, 1)
        XCTAssertEqual(pixKeyServiceMock.listPixKeysCallsCount, 2)
        XCTAssertEqual(consumerServiceMock.requestNewValidationCodeCallsCount, 0)
        
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        let expectedAction = KeyManagerConsumerAction.feedback(
            .keyRecordRegistered(uuid: "e9a319ce-c2cf-4dcc-8212-35b111b33c7d"),
            origin: .keyAuthentication
        )
        XCTAssertEqual(presenterSpy.action, expectedAction)
    }
    
    // MARK: didTapDelete tests
    func testDidTapDelete_WhenPasswordIsCorrectAndResponseIsSuccess_ShouldPresentPendingKeyDeletionFeedback() throws {
        try mockFullUserDataAndMixedKeys()
        consumerServiceMock.requestPasswordFromUserResult = .success("1234")
        pixKeyServiceMock.deleteKeyResult = .success(NoContent())
        
        let sut = createInteractor()
        sut.loadAndPresentKeys()
        sut.didTapDelete(keyId: "34234")

        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 2)
        
        XCTAssertEqual(consumerServiceMock.requestPasswordFromUserCallsCount, 1)
        XCTAssertEqual(pixKeyServiceMock.deleteKeyCallsCount, 1)
        XCTAssertEqual(pixKeyServiceMock.passwordToDelete, "1234")
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        let expectedAction = KeyManagerConsumerAction.feedback(.pendingKeyDeletion(uuid: "34234"), origin: .keyAuthenticationExclusion)
        XCTAssertEqual(presenterSpy.action, expectedAction)
    }
    
    func testDidTapDelete_WhenPasswordIsCorrectAndResponseIsFailure_ShouldPresentErrorPopup() throws {
        try mockFullUserDataAndMixedKeys()
        
        let requestError: RequestError = {
            var requestError = RequestError()
            requestError.title = "Ops!"
            requestError.message = "Erro de exclusão"
            return requestError
        }()
        
        consumerServiceMock.requestPasswordFromUserResult = .success("1234")
        pixKeyServiceMock.deleteKeyResult = .failure(.badRequest(body: requestError))
        
        let sut = createInteractor()
        sut.loadAndPresentKeys()
        sut.didTapDelete(keyId: "34234")

        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 2)
        XCTAssertEqual(pixKeyServiceMock.deleteKeyCallsCount, 1)
        XCTAssertEqual(pixKeyServiceMock.passwordToDelete, "1234")
        XCTAssertEqual(presenterSpy.presentErrorPopupTitleMessageCallsCount, 1)
        XCTAssertEqual(presenterSpy.popupErrorTitle, requestError.title)
        XCTAssertEqual(presenterSpy.popupErrorMessage, requestError.message)
    }
    
    func testDidTapDeletey_WhenPasswordIsIncorrect_ShouldPresentErrorPopup() throws {
        try mockFullUserDataAndMixedKeys()
        
        var requestError = RequestError()
        requestError.title = "title"
        requestError.message = "message"
        
        consumerServiceMock.requestPasswordFromUserResult = .success("1235")
        pixKeyServiceMock.deleteKeyResult = .failure(ApiError.unauthorized(body: requestError))
        
        let sut = createInteractor()
        sut.loadAndPresentKeys()
        sut.didTapDelete(keyId: "34234")

        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.presentErrorPopupTitleMessageCallsCount, 1)
        XCTAssertEqual(presenterSpy.popupErrorTitle, "title")
        XCTAssertEqual(presenterSpy.popupErrorMessage, "message")
        
        XCTAssertEqual(consumerServiceMock.requestPasswordFromUserCallsCount, 1)
    }
    
    // MARK: sendFeedbackEventForCloseAction tests
    func testSendFeedbackEventForCloseAction_WhenFeedbackIsPendingKeyDeletion_ShouldReloadKeys() throws {
        try mockFullUserDataAndMixedKeys()
        
        let sut = createInteractor()
        sut.loadAndPresentKeys()
        
        sut.sendFeedbackEventForCloseAction(feedbackType: .pendingKeyDeletion(uuid: ""))
        
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 2)
        
        XCTAssertEqual(pixKeyServiceMock.listPixKeysCallsCount, 2)
        XCTAssertEqual(consumerServiceMock.getConsumerDataCallsCount, 1)
    }
    
    func testSendFeedbackEventForCloseAction_WhenFeedbackIsDiferentOfPendingKeyDeletion_ShouldNotReloadKeys() throws {
        let sut = createInteractor()
        
        sut.sendFeedbackEventForCloseAction(feedbackType: .keyDeletionError(uuid: ""))
        
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 0)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 0)
        
        XCTAssertEqual(pixKeyServiceMock.listPixKeysCallsCount, 0)
        XCTAssertEqual(consumerServiceMock.getConsumerDataCallsCount, 0)
    }
    
    // MARK: didClose tests
    func testDidClose_ShouldCallCloseAction() {
        let sut = createInteractor()
        
        sut.didClose()
        
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .close)
    }
    
    // MARK: requestPortability tests
    func testRequestPortability_WhenResponseIsSuccess_ShouldPresentSuccessFeedbacck() {
        pixKeyServiceMock.createClaimResult = .success(NoContent())
        let claimKey = ClaimKey(userId: "", userType: .person, keyValue: "", keyType: .cpf, type: .portability)
        let sut = createInteractor()
        
        sut.requestPortability(claimKey: claimKey)
        
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 0)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
        
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        let expectedAction = KeyManagerConsumerAction.feedback(.portabilityInProgress(uuid: ""), origin: .keyPortabilityDialog)
        XCTAssertEqual(presenterSpy.action, expectedAction)
    }
    
    func testRequestPortability_WhenResponseIsFailure_ShouldPresentErrorPopup() {
        pixKeyServiceMock.createClaimResult = .failure(.connectionFailure)
        let claimKey = ClaimKey(userId: "", userType: .person, keyValue: "", keyType: .cpf, type: .portability)
        let sut = createInteractor()
        
        sut.requestPortability(claimKey: claimKey)
        
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 0)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
        
        XCTAssertEqual(presenterSpy.presentErrorPopupCallsCount, 1)
        let expectedError = PixCreateKeyErrorModel(
            image: Resources.Illustrations.iluConstruction.image,
            title: Strings.KeyManager.error,
            description: Strings.KeyManager.errorDescription,
            buttonTitle: Strings.KeyManager.errorAction,
            errorStatus: .none
        )
        XCTAssertEqual(presenterSpy.errorPopup, expectedError)
    }
    
    // MARK: requestClaim tests
    func testRequestClaim_WhenResponseIsSuccess_ShouldPresentSuccessFeedbacck() {
        pixKeyServiceMock.createClaimResult = .success(NoContent())
        let claimKey = ClaimKey(userId: "", userType: .person, keyValue: "", keyType: .cpf, type: .claim)
        let sut = createInteractor()
        
        sut.requestClaim(claimKey: claimKey)
        
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 0)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
        
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        let expectedAction = KeyManagerConsumerAction.feedback(.claimInProgress(uuid: ""), origin: .dialogClaimUse)
        XCTAssertEqual(presenterSpy.action, expectedAction)
    }
    
    func testRequestClaim_WhenResponseIsFailure_ShouldPresentErrorPopup() {
        pixKeyServiceMock.createClaimResult = .failure(.connectionFailure)
        let claimKey = ClaimKey(userId: "", userType: .person, keyValue: "", keyType: .cpf, type: .claim)
        let sut = createInteractor()
        
        sut.requestClaim(claimKey: claimKey)
        
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 0)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
        
        XCTAssertEqual(presenterSpy.presentErrorPopupCallsCount, 1)
        let expectedError = PixCreateKeyErrorModel(
            image: Resources.Illustrations.iluConstruction.image,
            title: Strings.KeyManager.error,
            description: Strings.KeyManager.errorDescription,
            buttonTitle: Strings.KeyManager.errorAction,
            errorStatus: .none
        )
        XCTAssertEqual(presenterSpy.errorPopup, expectedError)
    }
    
    // MARK: updateUserRegistration tests
    func testUpdateUserRegistration_ShouldOpenRegistrationRenewal() {
        let sut = createInteractor()
        
        sut.updateUserRegistration()
        
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .registrationRenewal)
    }
}
