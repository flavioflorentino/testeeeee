import XCTest
@testable import PIX

final class SecurityCodeRequestResponseTests: XCTestCase {
    func testInitFromDecoder_WhenKeysArePresent_ShouldInstantiateObject() throws {
        let sut = try JSONFileDecoder<SecurityCodeRequestResponse>().load(
            resource: "securityCodeRequestResponseMock",
            typeDecoder: .convertFromSnakeCase
        )
        
        XCTAssertEqual(sut.smsDelay, 30)
        XCTAssertEqual(sut.emailDelay, 60)
    }
    
    func testInitFromDecoder_WhenKeysAreNotPresent_ShouldInstantiateObjectWithZeroDelay() throws {
        let sut = try JSONFileDecoder<SecurityCodeRequestResponse>().load(
            resource: "securityCodeRequestResponseEmptyMock",
            typeDecoder: .convertFromSnakeCase
        )
        
        XCTAssertEqual(sut.smsDelay, 0)
        XCTAssertEqual(sut.emailDelay, 0)
    }
}
