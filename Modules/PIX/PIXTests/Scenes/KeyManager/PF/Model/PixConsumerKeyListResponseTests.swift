import XCTest
@testable import PIX

final class PixConsumerKeyListResponseTests: XCTestCase {
    func testInitFromDecoder_ShouldInitiateObject() throws {
        let sut = try JSONFileDecoder<PixConsumerKeyListResponse>().load(
            resource: "pixConsumerKeyListResponseMock",
            typeDecoder: .convertFromSnakeCase
        )
        
        let expectedKeys: [PixConsumerKey] = [
            PixConsumerKey(
                type: .registeredKey,
                id: "e9a319ce-c2cf-4dcc-8212-35b111b33c7d",
                attributes: PixConsumerKeyAttributes(
                    statusSlug: .processed,
                    keyType: .cpf,
                    status: "Chave cadastrada",
                    name: "Foo",
                    keyValue: "12345678900",
                    createdAt: "2020-09-10T19:46:19.366249Z"
                )
            ),
            PixConsumerKey(
                type: .registeredKey,
                id: "87ab32a4-4caa-4569-a0f7-4508c7ecb10f",
                attributes: PixConsumerKeyAttributes(
                    statusSlug: .deleteFromBacen,
                    keyType: .phone,
                    status: "Em processamento interno",
                    name: "Foo2",
                    keyValue: "27938479876",
                    createdAt: "2020-10-03 21:31:07"
                )
            ),
            PixConsumerKey(
                type: .registeredKey,
                id: "974bc97a-8dcb-477c-a8d3-40dfa61cb4d9",
                attributes: PixConsumerKeyAttributes(
                    statusSlug: .preRegistered,
                    keyType: .phone,
                    status: "Clique aqui para finalizar seu cadastro de chave",
                    name: "974bc97a-8dcb-477c-a8d3-40dfa61cb4d9",
                    keyValue: "28967549381",
                    createdAt: "2020-10-03 20:48:28"
                )
            ),
            PixConsumerKey(
                type: .unregisteredKey,
                id: nil,
                attributes: PixConsumerKeyAttributes(
                    statusSlug: nil,
                    keyType: .phone,
                    status: nil,
                    name: nil,
                    keyValue: nil,
                    createdAt: nil
                )
            ),
            PixConsumerKey(
                type: .unregisteredKey,
                id: nil,
                attributes: PixConsumerKeyAttributes(
                    statusSlug: nil,
                    keyType: .email,
                    status: nil,
                    name: nil,
                    keyValue: nil,
                    createdAt: nil
                )
            ),
            PixConsumerKey(
                type: .unregisteredKey,
                id: nil,
                attributes: PixConsumerKeyAttributes(
                    statusSlug: nil,
                    keyType: .cpf,
                    status: nil,
                    name: nil,
                    keyValue: nil,
                    createdAt: nil
                )
            ),
            PixConsumerKey(
                type: .unregisteredKey,
                id: nil,
                attributes: PixConsumerKeyAttributes(
                    statusSlug: nil,
                    keyType: .random,
                    status: nil,
                    name: nil,
                    keyValue: nil,
                    createdAt: nil
                )
            ),
        ]
        
        XCTAssertEqual(sut.data, expectedKeys)
    }
}
