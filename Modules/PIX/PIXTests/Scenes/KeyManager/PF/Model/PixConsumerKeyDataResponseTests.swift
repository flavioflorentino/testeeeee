import XCTest
@testable import PIX

final class PixConsumerKeyDataResponseTests: XCTestCase {
    func testInitFromDecoder_ShouldInitiateObject() throws {
        let sut = try JSONFileDecoder<PixConsumerKeyDataResponse>().load(
            resource: "pixConsumerKeyDataResponseMock",
            typeDecoder: .convertFromSnakeCase
        )
        
        let expectedKey = PixConsumerKey(
            type: .registeredKey,
            id: "bf03f76d-5f81-4a87-bd4d-327e55ba5166",
            attributes: PixConsumerKeyAttributes(
                statusSlug: .processed,
                keyType: .cpf,
                status: "Pedido de chave Pix recebido",
                name: "teste",
                keyValue: "888888888",
                createdAt: "2020-09-14T20:41:57.651853Z"
            )
        )
        
        XCTAssertEqual(sut.data, expectedKey)
    }
}
