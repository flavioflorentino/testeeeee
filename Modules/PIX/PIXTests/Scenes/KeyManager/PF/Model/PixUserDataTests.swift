import XCTest
@testable import PIX

final class PixUserDataTests: XCTestCase {
    func testInitFromDecoder_WhenReceivedAllFields_ShouldIntantiateObject() throws {
        let sut = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        
        XCTAssertEqual(sut.email, "eduardo.oliveira@picpay.com")
        XCTAssertEqual(sut.cpf, "52709314274")
        XCTAssertEqual(sut.phoneNumber?.fullNumber, "+5511941414144")
        XCTAssertEqual(sut.phoneNumber?.formattedFullNumber, "(11) 94141-4144")
    }
    
    func testInitFromDecoder_WhenReceivedOptionalFieldsAsNil_ShouldIntantiateObject() throws {
        let sut = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataNoPhoneCpfAndEmailMock")
        
        XCTAssertEqual(sut.email, "")
        XCTAssertNil(sut.cpf)
        XCTAssertNil(sut.phoneNumber)
    }
    
    func testFormatForType_WhenKeyTypeIsCpf_ShoudReturnFormatedCpf() throws {
        let sut = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        
        let formatedValue = sut.formatForType(keyType: .cpf)
        
        XCTAssertEqual(formatedValue, "527.093.142-74")
    }
    
    func testFormatForType_WhenKeyTypeIsEmail_ShoudReturnEmail() throws {
        let sut = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        
        let formatedValue = sut.formatForType(keyType: .email)
        
        XCTAssertEqual(formatedValue, "eduardo.oliveira@picpay.com")
    }
    
    func testFormatForType_WhenKeyTypeIsPhone_ShoudReturnFormatedPhone() throws {
        let sut = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        
        let formatedValue = sut.formatForType(keyType: .phone)
        
        XCTAssertEqual(formatedValue, "(11) 94141-4144")
    }
    
    func testFormatForType_WhenKeyTypeIsRandom_ShoudReturnEmptyString() throws {
        let sut = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        
        let formatedValue = sut.formatForType(keyType: .random)
        
        XCTAssertEqual(formatedValue, "")
    }
    
    func testValueForType_WhenKeyTypeIsCpf_ShoudReturnCpf() throws {
        let sut = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        
        let value = sut.valueForType(keyType: .cpf)
        
        XCTAssertEqual(value, "52709314274")
    }
    
    func testValueForType_WhenKeyTypeIsEmail_ShoudReturnEmail() throws {
        let sut = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        
        let value = sut.valueForType(keyType: .email)
        
        XCTAssertEqual(value, "eduardo.oliveira@picpay.com")
    }
    
    func testValueForType_WhenKeyTypeIsPhone_ShoudReturnPhone() throws {
        let sut = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        
        let value = sut.valueForType(keyType: .phone)
        
        XCTAssertEqual(value, "+5511941414144")
    }
    
    func testValueForType_WhenKeyTypeIsRandom_ShoudReturnNil() throws {
        let sut = try JSONFileDecoder<PixUserData>().load(resource: "pixUserDataFullMock")
        
        let value = sut.valueForType(keyType: .random)
        
        XCTAssertNil(value)
    }
}


final class KeyManagerConsumerPhoneNumberTests: XCTestCase {
    func testFullNumber_WhenCountryAndNumberIsZero_ShouldReturnFormatedNumber() {
        let sut = KeyManagerConsumerPhoneNumber(country: 55, area: 0, number: 0)
        
        let formatedNumber = sut.formattedFullNumber
        
        XCTAssertEqual(formatedNumber, "")
    }
    
    func testFullNumber_WhenCountryAndNumberIsNotZero_ShouldReturnFormatedNumber() {
        let sut = KeyManagerConsumerPhoneNumber(country: 55, area: 27, number: 932329847)
        
        let formatedNumber = sut.formattedFullNumber
        
        XCTAssertEqual(formatedNumber, "(27) 93232-9847")
    }
    
    func testFormated_WhenAreaIsCodeZero_ShouldReturnFullNumber() {
        let sut = KeyManagerConsumerPhoneNumber(country: 55, area: 27, number: 932329847)
        
        let fullNumber = sut.fullNumber
        
        XCTAssertEqual(fullNumber, "+5527932329847")
    }
}
