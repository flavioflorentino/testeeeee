@testable import PIX
import XCTest

extension KeyManagerConsumerAction: Equatable {
    public static func == (lhs: KeyManagerConsumerAction, rhs: KeyManagerConsumerAction) -> Bool {
        switch (lhs, rhs) {
        case let (.validateKey(keyTypeLhs, keyValueLhs, codeResendDelayLhs, _),
              .validateKey(keyTypeRhs, keyValueRhs, codeResendDelayRhs, _)):
            return keyTypeLhs == keyTypeRhs && keyValueLhs == keyValueRhs && codeResendDelayLhs == codeResendDelayRhs
        case let (.feedback(typeLhs, origin: originLhs), .feedback(typeRhs, origin: originRhs)):
            return typeLhs == typeRhs && originLhs == originRhs
        case (.registrationRenewal, .registrationRenewal):
            return true
        case (.identityValidation, .identityValidation):
            return true
        case (.close, .close):
            return true
        case let (.cancelClaimOrPortability(typeLhs, _, originLhs), .cancelClaimOrPortability(typeRhs, _, originRhs)):
            return typeLhs == typeRhs && originLhs == originRhs
        default:
            return false
        }
    }
}

final class KeyManagerConsumerPresentingSpy: KeyManagerConsumerPresenting {
    var viewController: KeyManagerConsumerDisplaying?
    
    // MARK: - presentLoading
    private(set) var presentLoadingCallsCount = 0
    
    func presentLoading() {
        presentLoadingCallsCount += 1
    }
    
    // MARK: - hideLoading
    private(set) var hideLoadingCallsCount = 0
    
    func hideLoading() {
        hideLoadingCallsCount += 1
    }
    
    // MARK: - presentErrorScreen
    private(set) var presentErrorScreenCallsCount = 0
    private(set) var screenErrorMessage: String?

    func presentErrorScreen(message: String) {
        presentErrorScreenCallsCount += 1
        screenErrorMessage = message
    }
    
    // MARK: - didNextStep
    private(set) var didNextStepCallsCount = 0
    private(set) var action: KeyManagerConsumerAction?

    func didNextStep(action: KeyManagerConsumerAction) {
        didNextStepCallsCount += 1
        self.action = action
    }
    
    // MARK: - presentErrorPopup
    private(set) var presentErrorPopupCallsCount = 0
    private(set) var errorPopup: PixCreateKeyErrorModel?
    
    func presentErrorPopup(popup: PixCreateKeyErrorModel) {
        presentErrorPopupCallsCount += 1
        errorPopup = popup
    }
    
    // MARK: - presentErrorPopup
    private(set) var presentErrorPopupTitleMessageCallsCount = 0
    private(set) var popupErrorTitle: String?
    private(set) var popupErrorMessage: String?

    func presentErrorPopup(title: String, message: String) {
        presentErrorPopupTitleMessageCallsCount += 1
        popupErrorTitle = title
        popupErrorMessage = message
    }
    
    // MARK: - presentUpdateRegistrationPopup
    private(set) var presentUpdateRegistrationPopupCallsCount = 0
    private(set) var updateRegistrationTitle: String?
    private(set) var updateRegistrationMessage: String?
    
    func presentUpdateRegistrationPopup(title: String, message: String) {
        presentUpdateRegistrationPopupCallsCount += 1
        updateRegistrationTitle = title
        updateRegistrationMessage = message
    }
    
    // MARK: - presentDeleteKeyPopup
    private(set) var presentDeleteKeyPopupCallsCount = 0
    private(set) var keyIdDeleteKeyPopup: String?
    
    func presentDeleteKeyPopup(keyId: String) {
        presentDeleteKeyPopupCallsCount += 1
        keyIdDeleteKeyPopup = keyId
    }
    
    // MARK: - presentKeySections
    private(set) var presentKeySectionsCallsCount = 0
    private(set) var keySections = [KeyManagerConsumerSection]()
    private(set) var userData: PixUserData?
    
    func presentKeySections(_ sections: [KeyManagerConsumerSection], userData: PixUserData) {
        presentKeySectionsCallsCount += 1
        keySections = sections
        self.userData = userData
    }
    
    // MARK: - presentKeySections
    private(set) var presentBottomSheetMenuCallsCount = 0
    private(set) var userDataForBottomSheet: PixUserData?
    private(set) var keyForBottomSheet: PixConsumerKey?
    private(set) var actionsForBottomSheet: [BottomSheetAction] = []
    
    func presentBottomSheetMenu(userData: PixUserData, key: PixConsumerKey, actions: [BottomSheetAction]) {
        presentBottomSheetMenuCallsCount += 1
        userDataForBottomSheet = userData
        keyForBottomSheet = key
        actionsForBottomSheet = actions
    }
    
    // MARK: - presentKeySections
    private(set) var presentShareMenuCallsCount = 0
    private(set) var keyValuePresentShareMenu: String?
    
    func presentShareMenu(keyValue: String) {
        presentShareMenuCallsCount += 1
        keyValuePresentShareMenu = keyValue
    }
    
    // MARK: - presentKeySections
    private(set) var presentKeyCopiedAlertCallsCount = 0
    
    func presentKeyCopiedAlert() {
        presentKeyCopiedAlertCallsCount += 1
    }
    
    // MARK: - presentCloseButton
    private(set) var presentCloseButtonCallsCount = 0
    
    func presentCloseButton() {
        presentCloseButtonCallsCount += 1
    }
}
