import Core
@testable import PIX
import XCTest

final class PixConsumerKeyServicingMock: PixConsumerKeyServicing {
    // MARK: - createNewPixKey
    private(set) var createNewPixKeyCallsCount = 0
    private(set) var keyToBeCreated: CreateKey?
    private(set) var password: String?
    var createNewPixKeyResult: Result<PixConsumerKeyDataResponse, ApiError>?

    func createNewPixKey(key: CreateKey,
                         password: String?,
                         completion: @escaping(Result<PixConsumerKeyDataResponse, ApiError>) -> Void) {
        createNewPixKeyCallsCount += 1
        keyToBeCreated = key
        self.password = password
        
        guard let result = createNewPixKeyResult else {
            XCTFail("Expected mocked result for method createNewPixKey on PixConsumerKeyServicingMock")
            return
        }
        completion(result)
    }

    // MARK: - listPixKeys
    private(set) var listPixKeysCallsCount = 0
    private(set) var userId: String?
    var listPixKeysResult: Result<PixConsumerKeyListResponse, ApiError>?

    func listPixKeys(with userId: String, completion: @escaping (Result<PixConsumerKeyListResponse, ApiError>) -> Void) {
        listPixKeysCallsCount += 1
        self.userId = userId
        
        guard let result = listPixKeysResult else {
            XCTFail("Expected mocked result for method listPixKeys on PixConsumerKeyServicingMock")
            return
        }
        completion(result)
    }

    // MARK: - deleteKey
    private(set) var deleteKeyCallsCount = 0
    private(set) var keyIdOfKeyToDelete: String?
    private(set) var passwordToDelete: String?
    var deleteKeyResult: Result<NoContent, ApiError>?

    func deleteKey(keyId: String, password: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        guard let result = deleteKeyResult else {
            XCTFail("Expected mocked result for method deleteKey on PixConsumerKeyServicingMock")
            return
        }
        
        deleteKeyCallsCount += 1
        keyIdOfKeyToDelete = keyId
        passwordToDelete = password
        
        completion(result)
    }

    // MARK: - createClaim
    private(set) var createClaimCallsCount = 0
    private(set) var keyToClaim: ClaimKey?
    var createClaimResult: Result<NoContent, ApiError>?

    func createClaim(claimKey: ClaimKey, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        createClaimCallsCount += 1
        keyToClaim = claimKey
        
        guard let result = createClaimResult else {
            XCTFail("Expected mocked result for method createClaim on PixConsumerKeyServicingMock")
            return
        }
        completion(result)
    }

    // MARK: - confirmClaim
    private(set) var confirmClaimCallsCount = 0
    private(set) var keyIdOfKeyToConfirmClaim: String?
    private(set) var passwordToConfirm: String?
    var confirmClaimResult: Result<NoContent, ApiError>?

    func confirmClaim(keyId: String, password: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        guard let result = confirmClaimResult else {
            XCTFail("Expected mocked result for method confirmClaim on PixConsumerKeyServicingMock")
            return
        }
        
        confirmClaimCallsCount += 1
        keyIdOfKeyToConfirmClaim = keyId
        passwordToConfirm = password
        
        completion(result)
    }

    // MARK: - completeClaim
    private(set) var completeClaimCallsCount = 0
    private(set) var keyIdOfKeyToCompleteClaim: String?
    var completeClaimResult: Result<NoContent, ApiError>?

    func completeClaim(keyId: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        completeClaimCallsCount += 1
        keyIdOfKeyToCompleteClaim = keyId
        
        guard let result = completeClaimResult else {
            XCTFail("Expected mocked result for method completeClaim on PixConsumerKeyServicingMock")
            return
        }
        completion(result)
    }

    // MARK: - cancelClaim
    private(set) var cancelClaimCallsCount = 0
    private(set) var keyIdOfKeyToCancelClaim: String?
    var cancelClaimResult: Result<NoContent, ApiError>?

    func cancelClaim(keyId: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        cancelClaimCallsCount += 1
        keyIdOfKeyToCancelClaim = keyId
        
        guard let result = cancelClaimResult else {
            XCTFail("Expected mocked result for method cancelClaim on PixConsumerKeyServicingMock")
            return
        }
        completion(result)
    }
}
