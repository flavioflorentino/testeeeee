import Foundation
@testable import PIX

struct PixKeyListResponseMocks {
    private let keyMock = PixConsumerKeyMocks()
    
    var mixedKeysMock: PixConsumerKeyListResponse {
        PixConsumerKeyListResponse(data:
            [
                keyMock.processedKey(.cpf),
                keyMock.processPendingKey(.email),
                keyMock.unregisteredKey(.random),
                keyMock.preRegisteredKey(.phone)
            ]
        )
    }
    
    var unregisteredKeysMock: PixConsumerKeyListResponse {
        PixConsumerKeyListResponse(data:
            [
                keyMock.unregisteredKey(.cpf),
                keyMock.unregisteredKey(.phone),
                keyMock.unregisteredKey(.email),
                keyMock.unregisteredKey(.random)
            ]
        )
    }
    
    var registeredKeysMock: PixConsumerKeyListResponse {
        PixConsumerKeyListResponse(data:
            [
                keyMock.processedKey(.cpf),
                keyMock.processedKey(.phone),
                keyMock.processedKey(.email),
                keyMock.processedKey(.random)
            ]
        )
    }
    
    var preRegisteredKeysMock: PixConsumerKeyListResponse {
        PixConsumerKeyListResponse(data:
            [
                keyMock.preRegisteredKey(.cpf),
                keyMock.preRegisteredKey(.phone),
                keyMock.preRegisteredKey(.email)
            ]
        )
    }
    
    var registeredKeysWithMixedStatusMock: PixConsumerKeyListResponse {
        PixConsumerKeyListResponse(data:
            [
                keyMock.key(.cpf, status: .processPending),
                keyMock.key(.email, status: .claimInProgress),
                keyMock.key(.random, status: .portabilityInProgress),
                keyMock.key(.phone, status: .awaitingClaimConfirm),
                keyMock.key(.email, status: .inactiveButCanRevalidate),
                keyMock.key(.random, status: .awaitingPortabilityConfirm),
                keyMock.key(.phone, status: .deleteFromBacen),
                keyMock.key(.email, status: .deletedFromBacen),
                keyMock.key(.random, status: .inactive)
            ]
        )
    }
    
    func preRegisteredKeyMock(_ type: PixConsumerKeyType) -> PixConsumerKeyListResponse {
        PixConsumerKeyListResponse(data:
            [
                keyMock.preRegisteredKey(type),
                keyMock.unregisteredKey(.phone),
                keyMock.unregisteredKey(.email),
                keyMock.unregisteredKey(.random)
            ]
        )
    }
}
