@testable import PIX

struct KeyManagerConsumerSectionMock {
    private let keyMock = PixConsumerKeyMocks()
    
    var mixedKeysMock: [KeyManagerConsumerSection] {
        [
            KeyManagerConsumerSection(
                type: .registered,
                keys: [
                    keyMock.processedKey(.cpf),
                    keyMock.processPendingKey(.email)
                ]
            ),
            KeyManagerConsumerSection(
                type: .unregistered,
                keys: [
                    keyMock.preRegisteredKey(.phone),
                    keyMock.unregisteredKey(.random)
                ]
            )
        ]
    }
    
    var unregisteredKeysMock: [KeyManagerConsumerSection] {
        [
            KeyManagerConsumerSection(
                type: .unregistered,
                keys: [
                    keyMock.unregisteredKey(.cpf),
                    keyMock.unregisteredKey(.phone),
                    keyMock.unregisteredKey(.email),
                    keyMock.unregisteredKey(.random)
                ]
            )
        ]
    }
    
    var registeredKeysMock: [KeyManagerConsumerSection] {
        [
            KeyManagerConsumerSection(
                type: .registered,
                keys: [
                    keyMock.processedKey(.cpf),
                    keyMock.processedKey(.phone),
                    keyMock.processedKey(.email),
                    keyMock.processedKey(.random)
                ]
            )
        ]
    }
    
    var preRegisteredKeysMock: [KeyManagerConsumerSection] {
        [
            KeyManagerConsumerSection(
                type: .unregistered,
                keys: [
                    keyMock.preRegisteredKey(.cpf),
                    keyMock.preRegisteredKey(.phone),
                    keyMock.preRegisteredKey(.email)
                ]
            )
        ]
    }
}
