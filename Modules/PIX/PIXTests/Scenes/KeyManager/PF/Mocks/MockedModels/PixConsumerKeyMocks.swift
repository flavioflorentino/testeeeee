import Foundation
@testable import PIX

struct PixConsumerKeyMocks {
    func processedKey(_ type: PixConsumerKeyType) -> PixConsumerKey {
        PixConsumerKey(
            type: .registeredKey,
            id: "e9a319ce-c2cf-4dcc-8212-35b111b33c7d",
            attributes: PixConsumerKeyAttributes(
                statusSlug: .processed,
                keyType: type,
                status: "Em validação interna",
                name: type.rawValue,
                keyValue: type.mockedRegisteredValue,
                createdAt: "2020-09-10T19:46:19.366249Z"
            )
        )
    }
    
    func processPendingKey(_ type: PixConsumerKeyType) -> PixConsumerKey {
        PixConsumerKey(
            type: .registeredKey,
            id: "974bc97a-8dcb-477c-a8d3-40dfa61cb4d9",
            attributes: PixConsumerKeyAttributes(
                statusSlug: .processPending,
                keyType: type,
                status: "Cadastro da chave em andamento",
                name: type.rawValue,
                keyValue: type.mockedUnregisteredValue,
                createdAt: "2020-10-03 20:48:28"
            )
        )
    }
    
    func preRegisteredKey(_ type: PixConsumerKeyType) -> PixConsumerKey {
        PixConsumerKey(
            type: .registeredKey,
            id: "19afde69-bfa6-41d5-9731-0ab168797a3c",
            attributes: PixConsumerKeyAttributes(
                statusSlug: .preRegistered,
                keyType: type,
                status: "Clique aqui para finalizar seu cadastro de chave",
                name: type.rawValue,
                keyValue: type.mockedUnregisteredValue,
                createdAt: "2020-10-03 21:31:07"
            )
        )
    }
    
    func unregisteredKey(_ type: PixConsumerKeyType) -> PixConsumerKey {
        PixConsumerKey(
            type: .unregisteredKey,
            id: nil,
            attributes: PixConsumerKeyAttributes(
                statusSlug: nil,
                keyType: type,
                status: nil,
                name: nil,
                keyValue: nil,
                createdAt: nil
            )
        )
    }
    
    func key(_ type: PixConsumerKeyType, status: PixKeyStatus) -> PixConsumerKey {
        PixConsumerKey(
            type: .registeredKey,
            id: "",
            attributes: PixConsumerKeyAttributes(
                statusSlug: status,
                keyType: type,
                status: "",
                name: type.rawValue,
                keyValue: type.mockedRegisteredValue,
                createdAt: "2020-10-03 21:31:07"
            )
        )
    }
}

private extension PixConsumerKeyType {
    var mockedRegisteredValue: String {
        switch self {
        case .cpf:
            return "02273410581"
        case .phone:
            return "11997551209"
        case .email:
            return "abc.def@uol.com"
        case .random:
            return "f27c64b3-4476-4a34-b91b-9fc06f66ea7d"
        }
    }
    
    var mockedUnregisteredValue: String {
        self == .random ? "" : mockedRegisteredValue
    }
}
