import Core
@testable import PIX
import XCTest

final class KeyManagerConsumerServicingMock: KeyManagerConsumerServicing {
    var consumerId: Int?
    
    var authenticationIsEnabled: Bool = false
    var identityValidationIsEnabled: Bool = false
    
    // MARK: - getConsumerData
    private(set) var getConsumerDataCallsCount = 0
    var getConsumerDataResult: Result<PixUserData, ApiError>?

    func getConsumerData(completion: @escaping (Result<PixUserData, ApiError>) -> Void) {
        getConsumerDataCallsCount += 1
        
        guard let result = getConsumerDataResult else {
            XCTFail("Expected mocked result for method getConsumerData on KeyManagerConsumerServicingMock")
            return
        }
        completion(result)
    }
    
    // MARK: - getIdValidationStatus
    private(set) var getIdValidationStatusCallsCount = 0
    var getIdValidationStatusResult: Result<IdentityValidationStatus, ApiError>?
    
    func getIdValidationStatus(completion: @escaping (Result<IdentityValidationStatus, ApiError>) -> Void) {
        getIdValidationStatusCallsCount += 1
        
        guard let result = getIdValidationStatusResult else {
            XCTFail("Expected mocked result for method getIdValidationStatus on KeyManagerConsumerServicingMock")
            return
        }
        completion(result)
    }

    // MARK: - requestNewValidationCode
    private(set) var requestNewValidationCodeCallsCount = 0
    private(set) var inputType: SecurityCodeValidationType?
    var requestNewValidationCodeResult: Result<SecurityCodeRequestResponse, ApiError>?

    func requestNewValidationCode(inputType: SecurityCodeValidationType,
                                  completion: @escaping (Result<SecurityCodeRequestResponse, ApiError>) -> Void) {
        requestNewValidationCodeCallsCount += 1
        self.inputType = inputType

        guard let result = requestNewValidationCodeResult else {
            XCTFail("Expected mocked result for method requestNewValidationCode on KeyManagerConsumerServicingMock")
            return
        }
        completion(result)
    }

    // MARK: - requestPasswordFromUser
    private(set) var requestPasswordFromUserCallsCount = 0
    var requestPasswordFromUserResult: Result<String, AuthenticationStatus>?

    func requestPasswordFromUser(completion: @escaping (Result<String, AuthenticationStatus>) -> Void) {
        requestPasswordFromUserCallsCount += 1
        
        guard let result = requestPasswordFromUserResult else {
            XCTFail("Expected mocked result for method requestPasswordFromUser on KeyManagerConsumerServicingMock")
            return
        }
        completion(result)
    }
    
    // MARK: - setOnClipboard
    private(set) var setOnClipboardCallsCount = 0
    var keyValueToClipboard: String?
    
    func setOnClipboard(keyValue: String) {
        setOnClipboardCallsCount += 1
        keyValueToClipboard = keyValue
    }
}
