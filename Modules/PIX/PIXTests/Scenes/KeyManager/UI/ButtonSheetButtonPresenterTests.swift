import XCTest
@testable import PIX

private final class BottomSheetButtonViewSpy: BottomSheetButtonDisplay {
    private(set) var displayTitleCountCalls = 0
    private(set) var title: String?
    
    func displayTitle(_ title: String) {
        displayTitleCountCalls += 1
        self.title = title
    }
}

private final class BottomSheetButtonViewDelegateSpy: BottomSheetButtonViewDelegate {
    private(set) var dismissBottomSheetCountCalls = 0
    
    func dismissBottomSheet(completion: (() -> Void)?) {
        dismissBottomSheetCountCalls += 1
        completion?()
    }
}

final class ButtonSheetButtonPresenterTests: XCTestCase {
    private let viewSpy = BottomSheetButtonViewSpy()
    private let delegateSpy = BottomSheetButtonViewDelegateSpy()
    private let keyManagerItemMock = KeyManagerItem(title: "", description: "", icon: .apps, isFilledBackgroundIcon: false)
    
    private func createPresenter(action: BottomSheetAction) -> BottomSheetButtonPresenting {
        let presenter = BottomSheetButtonPresenter(
            action: action,
            keyManagerItem: keyManagerItemMock,
            delegate: delegateSpy
        )
        
        presenter.view = viewSpy
        return presenter
    }
    
    func testSetupTitle_WhenActionIsDelete_ShouldSetCorrectTitle() {
        let sut = createPresenter(action: BottomSheetAction(type: .delete, handler: { _  in }))
        
        sut.setupTitle()
        
        XCTAssertEqual(viewSpy.title, Strings.KeyManager.deleteButton)
    }
    
    func testSetupTitle_WhenActionIsShare_ShouldSetCorrectTitle() {
        let sut = createPresenter(action: BottomSheetAction(type: .share, handler: { _  in }))
        
        sut.setupTitle()
        
        XCTAssertEqual(viewSpy.title, Strings.General.share)
    }
    
    func testSetupTitle_WhenActionIsCancelClaim_ShouldSetCorrectTitle() {
        let sut = createPresenter(action: BottomSheetAction(type: .cancelClaim, handler: { _  in }))
        
        sut.setupTitle()
        
        XCTAssertEqual(viewSpy.title, Strings.KeyManager.Claim.action)
    }
    
    func testSetupTitle_WhenActionIsCancelPortabilityRequest_ShouldSetCorrectTitle() {
        let sut = createPresenter(action: BottomSheetAction(type: .cancelPortabilityRequest, handler: { _  in }))
        
        sut.setupTitle()
        
        XCTAssertEqual(viewSpy.title, Strings.KeyManager.Cancel.Request.Portability.action)
    }
    
    func testDidTap_ShouldDismissBottomSheetAndExecuteActionHandler() {
        let expectation = XCTestExpectation(description: "Expected to dismiss bottom sheet after tapping key cell")
        
        let action = BottomSheetAction(type: .delete, handler: { _  in
            expectation.fulfill()
        })
        let sut = createPresenter(action: action)
        
        sut.didTap()
        
        XCTAssertEqual(delegateSpy.dismissBottomSheetCountCalls, 1)
        wait(for: [expectation], timeout: 1.0)
    }
}
