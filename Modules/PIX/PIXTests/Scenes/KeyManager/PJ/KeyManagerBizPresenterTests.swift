@testable import PIX
import UI
import UIKit
import XCTest

private final class KeyManagerBizCoordinatorSpy: KeyManagerBizCoordinating {
    private(set) var action: KeyManagerBizOutputAction?
    private(set) var count = 0
    
    func perform(action: KeyManagerBizOutputAction) {
        self.action = action
        count += 1
    }
}

private final class KeyManagerBizControllerSpy: KeyManagerBizDisplay {
    var keySections: [KeyManagerSection] = []
    var footerCount: Int = 0
    var didErrorCount = 0
    var keyItem: KeyManagerItem?
    var keyValue: String?
    var keyType: PixKeyType?
    var input: KeyManagerInputAction?
    var popUpType: KeyManagerPopUpType?
    private(set) var callStartLoading = 0
    private(set) var callStopLoading = 0
    private(set) var callShowPopup = 0
    private(set) var callInput = 0
    private(set) var callShowCancelClaimAlert = 0
    private(set) var callShowCancelRequestPortability = 0
    private(set) var callFindKeyType = 0
    private(set) var callSendIgnorePortabilityEvent = 0

    func displayError(message: String) {
        self.didErrorCount += 1
    }
    
    func displayData(with keySections: [KeyManagerSection]) {
        self.keySections = keySections
    }
    
    func changeFooterCount(with keyCount: Int) {
        self.footerCount = keyCount
    }
    
    func showDeleteAlert(keyItem: KeyManagerItem) {
        self.keyItem = keyItem
    }
    
    func presentKeyDeleteSuccessModal(with keyValue: String, keyType: PixKeyType) {
        self.keyValue = keyValue
        self.keyType = keyType
    }
    
    func startLoading() {
        self.callStartLoading += 1
    }
    
    func stopLoading() {
        self.callStopLoading += 1
    }
    
    func showPopup(with popUpType: KeyManagerPopUpType) {
        self.callShowPopup += 1
        self.popUpType = popUpType
    }
    
    func input(action: KeyManagerInputAction) {
        self.callInput += 1
        self.input = action
    }

    func showCancelClaimAlert(keyItem: KeyManagerItem) {
        self.keyItem = keyItem
        callShowCancelClaimAlert += 1
    }

    func showCancelRequestPortability(keyItem: KeyManagerItem) {
        self.keyItem = keyItem
        callShowCancelRequestPortability += 1
    }
    
    func findKeyType(uuid: String) -> PixKeyType {
        callFindKeyType += 1
        return keyType ?? .random
    }
    
    func sendIgnorePortabilityEvent(uuid: String) {
        callSendIgnorePortabilityEvent += 1
    }
}

final class KeyManagerBizPresenterTests: XCTestCase {
    // MARK: - Variables
    private let viewController = KeyManagerBizControllerSpy()
    private let coordinator = KeyManagerBizCoordinatorSpy()
    private let userInfo = KeyManagerBizUserInfo(name: "", cnpj: "24894589000187", mail: "", phone: "27971830410", userId: "1")
    private lazy var sut: KeyManagerBizPresenting = {
        let dependencies = DependencyContainerMock()
        let presenter = KeyManagerBizPresenter(coordinator: coordinator, dependencies: dependencies)
        presenter.viewController = viewController
        return presenter
    }()
    
    let maxNumberCount = 20
    let cnpjText = "24.894.589/0001-87"
    let phoneNumberFormattedText = "(27) 97183-0410"
    let mailText = "miralta@picpay.com"
    let pixRandonKey = "CNJS2359DNW939NSKK25"
    let pixRandomNickname = "BIP - Vila Leopoldina"
    let randomPlaceholder = "Chave aleatória"
    let phoneNumberText = "27971830410"
    
    private var cnpjKey: PixKey {
        PixKey(id: "", statusSlug: .awaitingClaimConfirm, status: "", name: "", keyType: .cnpj, createdAt: "", keyValue: cnpjText)
    }
    
    private var phoneNumberKey: PixKey {
        PixKey(id: "", statusSlug: .awaitingClaimConfirm, status: "", name: "", keyType: .phone, createdAt: "", keyValue: phoneNumberText)
    }
    
    private var mailKey: PixKey {
        PixKey(id: "", statusSlug: .awaitingClaimConfirm, status: "", name: "", keyType: .email, createdAt: "", keyValue: mailText)
    }
    
    private var randomKey: PixKey {
        PixKey(id: "", statusSlug: .awaitingClaimConfirm, status: "", name: pixRandomNickname, keyType: .random, createdAt: "", keyValue: pixRandonKey)
    }
    
    private func createPixList(with keys: [PixKey], placeholderTypes: [PixKeyType] = []) -> PixKeyDataList {
        let placeholders = placeholderTypes.map { PixKey(keyType: $0) }
        return PixKeyDataList(data: keys + placeholders)
    }
    
    func testCreateCells_WhenExistCPNJKey_ShouldCreateThreePlaceholderCellsAndOneKeyCell() {
        sut.populateData(with: createPixList(with: [cnpjKey], placeholderTypes: [.phone, .email, .random]).data, userInfo: userInfo)
        let firstSection = viewController.keySections[0]
        let secondSection = viewController.keySections[1]
        XCTAssertTrue(firstSection.items.contains(where: { $0.description == cnpjText }))
        XCTAssertEqual(firstSection.items.count, 1)
        XCTAssertEqual(secondSection.items.count, 3)
    }
    
    func testOpenInformation_WhenUserTapOpenInformation_ShouldCommunicateCoordinator() {
        sut.openInformation()
        XCTAssertEqual(coordinator.action, .some(.openInformation))
    }
    
    func testOpenCreateCellFlow_WhenUserTapToOpenCell_ShouldCommunicateCoordinator() {
        sut.openCreateCellFlow(userInfo: userInfo, key: CreateKey(userId: "", userType: .company, value: "", type: .phone), validated: true)
        XCTAssertEqual(coordinator.action, .some(.openCreateKeyFlow(userInfo: userInfo, key: CreateKey(userId: "", userType: .company, value: "", type: .phone))))
    }
    
    func testCreateCells_WhenExistPhoneAndCPNJKeys_ShouldCreateTwoPlaceholderCellsAndTwoKeyCell() {
        sut.populateData(with: createPixList(with: [cnpjKey, phoneNumberKey], placeholderTypes: [.email, .random]).data, userInfo: userInfo)
        let firstSection = viewController.keySections[0]
        let secondSection = viewController.keySections[1]
        XCTAssertTrue(firstSection.items.contains(where: { $0.description == cnpjText }))
        XCTAssertTrue(firstSection.items.contains(where: { $0.description == phoneNumberFormattedText }))
        XCTAssertEqual(firstSection.items.count, 2)
        XCTAssertEqual(secondSection.items.count, 2)
    }
    
    func testCreateCells_WhenExistAllKeys_ShouldCreateOneRandomPlaceholderCellAndThreeCells() {
        sut.populateData(with: createPixList(with: [cnpjKey, phoneNumberKey, mailKey], placeholderTypes: [.random]).data, userInfo: userInfo)
        let firstSection = viewController.keySections[0]
        let secondSection = viewController.keySections[1]
        XCTAssertTrue(firstSection.items.contains(where: { $0.description == cnpjText }))
        XCTAssertTrue(firstSection.items.contains(where: { $0.description == phoneNumberFormattedText }))
        XCTAssertTrue(firstSection.items.contains(where: { $0.description == mailText }))
        XCTAssertTrue(secondSection.items.contains(where: { $0.title == randomPlaceholder }))
        XCTAssertEqual(firstSection.items.count, 3)
        XCTAssertEqual(secondSection.items.count, 1)
    }
    
    func testCreateCells_WhenExistAllKeysAndRandom_ShouldCreateOneRandomPlaceholderCellAndFourCells() {
        sut.populateData(with: createPixList(with: [cnpjKey, phoneNumberKey, mailKey, randomKey], placeholderTypes: [.random]).data, userInfo: userInfo)
        let firstSection = viewController.keySections[0]
        let secondSection = viewController.keySections[1]
        XCTAssertTrue(firstSection.items.contains(where: { $0.description == pixRandomNickname }))
        XCTAssertTrue(firstSection.items.contains(where: { $0.secondDescription == pixRandonKey }))
        XCTAssertTrue(secondSection.items.contains(where: { $0.title == randomPlaceholder }))
        XCTAssertEqual(firstSection.items.count, 4)
        XCTAssertEqual(secondSection.items.count, 1)
    }
    
    func testCreateCells_WhenExistMaxKeys_ShouldntCreateRandomPlaceholderCellAndShouldCreateMaxCells() {
        let randomKeys = [PixKey].init(repeating: randomKey, count: maxNumberCount - 3)
        sut.populateData(with: createPixList(with: (randomKeys + [cnpjKey, phoneNumberKey, mailKey])).data, userInfo: userInfo)
        let firstSection = viewController.keySections[0]
        let secondSection = viewController.keySections[1]
        XCTAssertFalse(secondSection.items.contains(where: { $0.title == randomPlaceholder }))
        XCTAssertEqual(firstSection.items.count, maxNumberCount)
        XCTAssertEqual(secondSection.items.count, 0)
    }
    
    func testCreateCells_WhenExist19Keys_ShouldCreateRandomPlaceholderCellNighteenCells() {
        let randomKeys = [PixKey].init(repeating: randomKey, count: maxNumberCount - 4)
        sut.populateData(with: createPixList(with: (randomKeys + [cnpjKey, phoneNumberKey, mailKey]), placeholderTypes: [.random]).data, userInfo: userInfo)
        let firstSection = viewController.keySections[0]
        let secondSection = viewController.keySections[1]
        XCTAssertTrue(secondSection.items.contains(where: { $0.title == randomPlaceholder }))
        XCTAssertEqual(firstSection.items.count, maxNumberCount - 1)
        XCTAssertEqual(secondSection.items.count, 1)
    }
    
    func testCreateCells_WhenExistOneRandomKey_ShouldCreateRandomPlaceholder() {
        sut.populateData(with: createPixList(with: [randomKey], placeholderTypes: [.random]).data, userInfo: userInfo)
        let firstSection = viewController.keySections[0]
        let secondSection = viewController.keySections[1]
        XCTAssertFalse(firstSection.items[0].disclosureIndicator)
        XCTAssertTrue(secondSection.items[0].disclosureIndicator)
        XCTAssertTrue(secondSection.items.contains(where: { $0.title == randomPlaceholder }))
        XCTAssertEqual(firstSection.items.count, 1)
    }
    
    func testCreateCells_WhenExistOneRandomKey_ShouldCreateTwoSectionsWithTitle() {
        sut.populateData(with: createPixList(with: [randomKey]).data, userInfo: userInfo)
        let firstSection = viewController.keySections[0]
        let secondSection = viewController.keySections[1]
        XCTAssertEqual(firstSection.title, "Chaves cadastradas")
        XCTAssertEqual(secondSection.title, "Cadastrar nova chave")
    }
    
    func testCreateCells_WhenDidntExistAnyKey_ShouldCreateTwoSectionWithoutTitle() {
        sut.populateData(with: createPixList(with: [], placeholderTypes: [.phone, .email, .cnpj, .random]).data, userInfo: userInfo)
        let firstSection = viewController.keySections[0]
        let secondSection = viewController.keySections[1]
        XCTAssertEqual(firstSection.title, "")
        XCTAssertEqual(secondSection.title, "")
        XCTAssertEqual(secondSection.items.count, 4)
    }
    
    func testCreateCells_WhenExistFourKeys_ShouldUpdateFooterNumberWithFour() {
        sut.populateData(with: createPixList(with: ([cnpjKey, phoneNumberKey, mailKey, randomKey])).data, userInfo: userInfo)
        let footerCount = viewController.footerCount
        XCTAssertEqual(footerCount, 4)
    }
    
    func testCreateCells_WhenDidntExistAnyKeys_ShouldUpdateFooterNumberWithZero() {
        sut.populateData(with: createPixList(with: []).data, userInfo: userInfo)
        let footerCount = viewController.footerCount
        XCTAssertEqual(footerCount, 0)
    }
    
    func testDeleteKey_WhenUserTryToDeleteKey_ShouldDeleteKey() {
        let keyManagerItem = KeyManagerItem(
            title: "00.000.000/0001-91",
            description: "",
            icon: .postcard,
            isFilledBackgroundIcon: false,
            secondDescription: "",
            infoText: "",
            disclosureIndicator: true
        )
        sut.prepareToDelete(keyItem: keyManagerItem)
        XCTAssertEqual(keyManagerItem, viewController.keyItem)
    }

    func testDisplayCancelClaimSuccess_WhenReceiveSuccessOfCancelClaim_ShouldPresentAModal() {
        sut.displayCancelClaimSuccess(with: phoneNumberKey)
        XCTAssertEqual(viewController.popUpType, .cancelClaimSuccess(keyType: .phone))
        XCTAssertEqual(viewController.callShowPopup, 1)
    }

    func testDisplayCancelRequestPortabilitySuccess_WhenReceiveSuccessOfCancelClaim_ShouldPresentAModal() {
        sut.displayCancelRequestPortabilitySuccess(with: phoneNumberKey)
        XCTAssertEqual(viewController.popUpType, .cancelRequestPortabilitySuccess(keyType: .phone))
        XCTAssertEqual(viewController.callShowPopup, 1)
    }

    func testDisplayClaimFailure_WhenReceiveFailureOfCancelClaim_ShouldAnErrorScreen() {
        let id = phoneNumberKey.registeredKeys.first?.id ?? ""
        sut.openFeedbackScreen(with: .cancelClaimError(id: id))
        XCTAssertEqual(coordinator.action, .openFeedback(feedbackType: .cancelClaimError(id: id)))
        XCTAssertEqual(coordinator.count, 1)
    }

    func testPrepareToCancelClaim_WhenReceivePrepareToCancelClaimAction_ShouldDisplayAnAlert() {
        let keyManagerItem = KeyManagerItem(title: "00.000.000/0001-91", description: "", icon: .abacus, isFilledBackgroundIcon: true, secondDescription: "", infoText: "", disclosureIndicator: true)
        sut.prepareToCancelClaim(keyItem: keyManagerItem)
        XCTAssertEqual(viewController.callShowCancelClaimAlert, 1)
        XCTAssertEqual(viewController.keyItem, keyManagerItem)
    }

    func testPrepareToCancelRequestPortability_WhenReceivePrepareToCancelClaimAction_ShouldDisplayAnAlert() {
        let keyManagerItem = KeyManagerItem(title: "00.000.000/0001-91", description: "", icon: .abacus, isFilledBackgroundIcon: true, secondDescription: "", infoText: "", disclosureIndicator: true)
        sut.prepareToCancelRequestPortability(keyItem: keyManagerItem)
        XCTAssertEqual(viewController.callShowCancelRequestPortability, 1)
        XCTAssertEqual(viewController.keyItem, keyManagerItem)
    }
}
