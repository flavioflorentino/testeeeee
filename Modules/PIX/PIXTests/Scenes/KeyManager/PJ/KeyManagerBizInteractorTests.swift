import AnalyticsModule
import Core
import Foundation
import UI
import UIKit
import XCTest

@testable import PIX

private final class KeyManagerBizPresenterSpy: KeyManagerBizPresenting {
    var viewController: KeyManagerBizDisplay?

    private(set) var callPopulateData = 0
    private(set) var callDidNextStepCount = 0
    private(set) var callTapCreateCellCount = 0
    private(set) var callOpenInformation = 0
    private(set) var callErrorCount = 0
    private(set) var callPrepareToDelete = 0
    private(set) var callPresentKeyDeleteSuccessModal = 0
    private(set) var callOpenKeyDeleteErrorFeedback = 0
    private(set) var callStartLoading = 0
    private(set) var callOpenFeedbackScreen = 0
    private(set) var callStopLoading = 0
    private(set) var callShowPopUp = 0
    private(set) var callDismissPortability = 0
    private(set) var key: CreateKey?
    private(set) var pixKeys: [PixKey]?
    private(set) var pixKey: PixKey?
    private(set) var action: KeyManagerBizOutputAction?
    private(set) var keyItem: KeyManagerItem?
    private(set) var apiError: ApiError?
    private(set) var feedBackType: FeedbackViewType?
    private(set) var userInfo: KeyManagerBizUserInfo?
    private(set) var popUpType: KeyManagerPopUpType?
    private(set) var callDisplayCancelClaimSuccess = 0
    private(set) var callDisplayCancelRequestPortabilitySuccess = 0
    private(set) var callPrepareToCancelClaim = 0
    private(set) var callPrepareToCancelRequestPortability = 0

    func didNextStep(action: KeyManagerBizOutputAction) {
        self.action = action
        callDidNextStepCount += 1
    }

    func openCreateCellFlow(userInfo: KeyManagerBizUserInfo, key: CreateKey, validated: Bool) {
        self.key = key
        callTapCreateCellCount += 1
    }

    func populateData(with pixKeys: [PixKey], userInfo: KeyManagerBizUserInfo) {
        self.pixKeys = pixKeys
        callPopulateData += 1
    }

    func presentError(errorDescription: String) {
        callErrorCount += 1
    }

    func openInformation() {
        callOpenInformation += 1
    }
    
    func presentKeyDeleteSuccessModal(with pixKey: PixKey) {
        self.pixKey = pixKey
        callPresentKeyDeleteSuccessModal += 1
    }
    
    func prepareToDelete(keyItem: KeyManagerItem) {
        self.keyItem = keyItem
        callPrepareToDelete += 1
    }
    
    func startLoading() {
        self.callStartLoading += 1
    }
    
    func stopLoading() {
        self.callStopLoading += 1
    }

    func displayCancelClaimSuccess(with pixKey: PixKey) {
        self.pixKey = pixKey
        callDisplayCancelClaimSuccess += 1
    }

    func displayCancelRequestPortabilitySuccess(with pixKey: PixKey) {
        self.pixKey = pixKey
        callDisplayCancelRequestPortabilitySuccess += 1
    }

    func prepareToCancelClaim(keyItem: KeyManagerItem) {
        self.keyItem = keyItem
        callPrepareToCancelClaim += 1
    }

    func prepareToCancelRequestPortability(keyItem: KeyManagerItem) {
        self.keyItem = keyItem
        callPrepareToCancelRequestPortability += 1
    }
    
    func showPopup(with popUpType: KeyManagerPopUpType) {
        self.callShowPopUp += 1
        self.popUpType = popUpType
    }
    
    func openFeedbackScreen(with feedbackType: FeedbackViewType) {
        self.feedBackType = feedbackType
        self.callOpenFeedbackScreen += 1
    }
    
    func dismissPortabilityFeedbackAndShowError(errorDescription: String) {
        callDismissPortability += 1
    }
    
    func updateUserInfoInFlow(with userInfo: KeyManagerBizUserInfo) {
        self.userInfo = userInfo
    }
}

final class KeyManagerBizInteractorTests: XCTestCase {
    // MARK: - Variables
    private let presenter = KeyManagerBizPresenterSpy()
    private let analytics = AnalyticsSpy()
    private let randomKey = "CNJS2359DNW939NSKK25"
    private let cnpjKey = "24.894.589/0001-87"
    private let service = PixBizKeyServiceMock()
    
    lazy private var cnpjPixKey = PixKey(id: "123",
                                         statusSlug: .processed,
                                         status: "", name: "",
                                         keyType: .cnpj,
                                         createdAt: "",
                                         keyValue: cnpjKey)
    lazy private var randomAwaitingClaimConfirmKey = PixKey(id: "1233",
                                                            statusSlug: .awaitingClaimConfirm,
                                                            status: "",
                                                            name: "",
                                                            keyType: .random,
                                                            createdAt: "",
                                                            keyValue: "CNJS2359DNW939NSKK25")
    lazy private var randomAwaitingPortabilityConfirmKey = PixKey(id: "12334",
                                                                  statusSlug: .awaitingPortabilityConfirm,
                                                                  status: "",
                                                                  name: "",
                                                                  keyType: .random,
                                                                  createdAt: "",
                                                                  keyValue: "CNJS2359DNW939NSKK25")
    lazy var keys: [PixKey] = {
        [cnpjPixKey,
         PixKey(id: "123", statusSlug: .processed, status: "", name: "", keyType: .random, createdAt: "", keyValue: randomKey),
         randomAwaitingClaimConfirmKey,
         randomAwaitingPortabilityConfirmKey]
    }()
    
    private lazy var sut: KeyManagerBizInteracting = {
        let dependenciesContainer = DependencyContainerMock(analytics)
        let userInfo = KeyManagerBizUserInfo(name: "", cnpj: "", mail: "", phone: "", userId: "")
        return KeyManagerBizInteractor(service: service, presenter: presenter,
                                       dependencies: dependenciesContainer,
                                       userInfo: userInfo,
                                       pixKeys: keys)
    }()

    func testPresentKeyDeleteSuccessModal_WhenServiceDeleteKeyWithSuccess_ShouldOpenModal() {
        let keyManagerItem = KeyManagerItem(title: "CNPJ", description: cnpjKey, icon: .postcard, isFilledBackgroundIcon: true, itemId: "123")
        service.deleteResult = .success(NoContent())
        service.listPixKeysResult = .success(PixKeyDataList(data: keys))
        sut.deleteKey(keyManagerItem: keyManagerItem)
        XCTAssertEqual(presenter.callOpenFeedbackScreen, 1)
        XCTAssertEqual(presenter.feedBackType, .pendingKeyDeletion(uuid: "123"))
        
        let keyManagerRandomItem = KeyManagerItem(
            title: "Chave aleatória",
            description: "BIP - Itaim Bibi",
            icon: .cube,
            isFilledBackgroundIcon: true,
            secondDescription: randomKey
        )
        sut.deleteKey(keyManagerItem: keyManagerRandomItem)
        XCTAssertEqual(presenter.feedBackType, .pendingKeyDeletion(uuid: "123"))
    }

    func testRequestKeyManagerData_WhenUserEnterInScreen_ShouldLoadKeyList() {
        let keyList = PixKeyDataList(data: [])
        service.listPixKeysResult = .success(keyList)
        sut.requestKeyManagerData(shouldTrackEvent: true)
        XCTAssertEqual(presenter.pixKeys, keyList.data)
    }

    func testOpenRandonKeyInformation_WhenOpenRandonKeyInformation_ShouldSendEvent() {
        sut.openRandonKeyInformation()
        XCTAssertEqual(analytics.logCalledCount, 1)
    }

    func testOpenInformation_WhenTapOpenInformation_ShouldCommunicateCoordinator() {
        sut.openInformation()
        XCTAssertEqual(presenter.callOpenInformation, 1)
        XCTAssertEqual(analytics.logCalledCount, 1)
    }

    func testCreatePixItem_WhenCellIsCNPJPlaceholder_ShouldCallDidTapPlaceholderInPresenter() {
        let keyManagerItem = KeyManagerItem(title: "CNPJ", description: cnpjKey, icon: .postcard, isFilledBackgroundIcon: false)
        sut.createPixItem(item: keyManagerItem, isPlaceholder: true)
        XCTAssertEqual(presenter.callTapCreateCellCount, 1)
        let createKey = CreateKey(userId: "", userType: .company, value: "", type: .cnpj, name: "")
        XCTAssertEqual(presenter.key?.type, createKey.type)
    }

    func testCreatePixItem_WhenCellIsPhonePlaceholder_ShouldCallDidTapPlaceholderInPresenter() {
        let keyManagerItem = KeyManagerItem(
            title: "Telefone celular",
            description: "(27) 91222 1929",
            icon: .mobileAndroid,
            isFilledBackgroundIcon: false
        )
        sut.createPixItem(item: keyManagerItem, isPlaceholder: true)
        XCTAssertEqual(presenter.callTapCreateCellCount, 1)
        let createKey = CreateKey(userId: "", userType: .company, value: "", type: .phone, name: "")
        XCTAssertEqual(presenter.key?.type, createKey.type)
        XCTAssertEqual(analytics.logCalledCount, 1)
    }

    func testCreatePixItem_WhenCellIsMailPlaceholder_ShouldCallDidTapPlaceholderInPresenter() {
        let keyManagerItem = KeyManagerItem(
            title: "E-mail",
            description: "miralta@picpay.com",
            icon: .envelope,
            isFilledBackgroundIcon: false
        )
        sut.createPixItem(item: keyManagerItem, isPlaceholder: true)
        XCTAssertEqual(presenter.callTapCreateCellCount, 1)
        let createKey = CreateKey(userId: "", userType: .company, value: "", type: .email, name: "")
        XCTAssertEqual(presenter.key?.type, createKey.type)
    }

    func testCreatePixItem_WhenCellIsRandomPlaceholder_ShouldCallDidTapPlaceholderInPresenter() {
        let keyManagerItem = KeyManagerItem(
            title: "Chave aleatória",
            description: "Gerar chave aleatória",
            icon: .cube,
            isFilledBackgroundIcon: false
        )
        sut.createPixItem(item: keyManagerItem, isPlaceholder: true)
        XCTAssertEqual(presenter.callTapCreateCellCount, 1)
        let createKey = CreateKey(userId: "", userType: .company, value: "", type: .random, name: "")
        XCTAssertEqual(presenter.key?.type, createKey.type)
    }

    func testCreatePixItem_WhenCellIsAFirstSectionCell_ShouldntCallDidTapPlaceholderInPresenter() {
        let keyManagerItem = KeyManagerItem(
            title: "Chave aleatória",
            description: "BIP - Itaim Bibi",
            icon: .postcard,
            isFilledBackgroundIcon: false,
            secondDescription: randomKey
        )
        sut.createPixItem(item: keyManagerItem, isPlaceholder: false)
        XCTAssertEqual(presenter.callTapCreateCellCount, 0)
    }

    func testCreatePixItem_WhenCellIsAwaitConfirmClaim_ShouldPrepareToCancelClaim() {
        let keyManagerItem = KeyManagerItem(title: "", description: "", icon: .abacus, isFilledBackgroundIcon: true, secondDescription: "await_confirm_claim", itemId: "1233")
        sut.createPixItem(item: keyManagerItem, isPlaceholder: false)
        XCTAssertEqual(presenter.callPrepareToCancelClaim, 1)
        XCTAssertEqual(presenter.keyItem, keyManagerItem)
    }

    func testCreatePixItem_WhenCellIsAwaitingPortabilityConfirm_ShouldPrepareToCancelRequestPortability() {
        let keyManagerItem = KeyManagerItem(title: "", description: "", icon: .abacus, isFilledBackgroundIcon: true, secondDescription: "awaiting_portability_confirm", itemId: "12334")
        sut.createPixItem(item: keyManagerItem, isPlaceholder: false)
        XCTAssertEqual(presenter.callPrepareToCancelRequestPortability, 1)
        XCTAssertEqual(presenter.keyItem, keyManagerItem)
    }
    
    func testDeleteItem_WhenUserTryToDeleteWithSuccess_ShouldDeleteItem() {
        let keyManagerItem = KeyManagerItem(
            title: "Chave aleatória",
            description: "BIP - Itaim Bibi",
            icon: .cube,
            isFilledBackgroundIcon: false,
            secondDescription: randomKey, itemId: "1233"
        )
        service.deleteResult = .success(NoContent())
        service.listPixKeysResult = .success(PixKeyDataList(data: keys))
        sut.deleteKey(keyManagerItem: keyManagerItem)
        XCTAssertEqual(presenter.callOpenFeedbackScreen, 1)
        XCTAssertEqual(presenter.feedBackType, .pendingKeyDeletion(uuid: "1233"))
    }
    
    func testDeleteItem_WhenUserTryToDeleteWithFailure_ShouldntDeleteItem() {
        let keyManagerItem = KeyManagerItem(
            title: "Chave aleatória",
            description: "BIP - Itaim Bibi",
            icon: .cube,
            isFilledBackgroundIcon: false,
            secondDescription: randomKey,
            itemId: "123"
        )
        service.deleteResult = Result<NoContent, ApiError>.failure(ApiError.bodyNotFound)
        service.listPixKeysResult = .success(PixKeyDataList(data: keys))
        sut.deleteKey(keyManagerItem: keyManagerItem)
        XCTAssertEqual(presenter.callOpenFeedbackScreen, 1)
    }
    
    func testDeleteItem_WhenUserTryToDeleteWithSuccess_ShouldSendEvent() {
        let keyManagerItem = KeyManagerItem(
            title: "Chave aleatória",
            description: "BIP - Itaim Bibi",
            icon: .cube,
            isFilledBackgroundIcon: false,
            secondDescription: randomKey,
            itemId: "123"
        )
        sut.sendShowDeleteModalEvent(keyManagerItem: keyManagerItem)
        XCTAssertEqual(analytics.logCalledCount, 1)
    }
    
    func testTryAgainRequest_WhenUserTapTryAgain_ShouldSendEventAndStartStopLoading() {
        service.deleteResult = .success(NoContent())
        service.listPixKeysResult = .success(PixKeyDataList(data: keys))
        sut.tryAgainRequest(of: .keyDeletionError(uuid: "123"))
        XCTAssertEqual(analytics.logCalledCount, 1)
        XCTAssertEqual(presenter.callStartLoading, 1)
        XCTAssertEqual(presenter.callStopLoading, 2)
        XCTAssertEqual(presenter.callOpenFeedbackScreen, 1)
        XCTAssertEqual(presenter.feedBackType, .pendingKeyDeletion(uuid: "123"))
    }
    
    func testTryAgainRequest_WhenUserTapTryAgainAndServiceFail_ShouldSendEventAndStartStopLoading() {
        service.deleteResult = Result<NoContent, ApiError>.failure(.bodyNotFound)
        service.listPixKeysResult = .success(PixKeyDataList(data: keys))
        sut.tryAgainRequest(of: .keyDeletionError(uuid: "123"))
        XCTAssertEqual(presenter.callStartLoading, 1)
        XCTAssertEqual(presenter.callStopLoading, 2)
        XCTAssertEqual(presenter.callOpenFeedbackScreen, 1)
    }
    
    func testFindKeyValue_WhenTryToFindKeyValue_ShouldFindCorrentKeyValue() {
        let keyManagerItem = KeyManagerItem(
            title: "Chave aleatória",
            description: "BIP - Itaim Bibi",
            icon: .cube,
            isFilledBackgroundIcon: false,
            secondDescription: randomKey,
            itemId: "1233"
        )
        let result = sut.findKeyValue(keyManagerItem: keyManagerItem)
        XCTAssertEqual(result, randomKey)
        let keyManagerItemPhone = KeyManagerItem(
            title: "CNPJ",
            description: cnpjKey,
            icon: .postcard,
            isFilledBackgroundIcon: false,
            itemId: "123"
        )
        let resultPhone = sut.findKeyValue(keyManagerItem: keyManagerItemPhone)
        XCTAssertEqual(resultPhone, cnpjKey)
    }

    func testTryAgainRequest_WhenUserTapTryAgain_ShouldTryCancelClaimAgain() {
        sut.tryAgainRequest(of: .cancelClaimError(id: "123"))
        XCTAssertEqual(presenter.callStartLoading, 1)
        XCTAssertEqual(presenter.callStopLoading, 1)
    }

    func testTryAgainRequest_WhenUserTapTryAgain_ShouldDisplayCancelClaimSuccess() {
        service.cancelClaimResult = .success(NoContent())
        sut.tryAgainRequest(of: .cancelClaimError(id: "1233"))
        XCTAssertEqual(presenter.callStartLoading, 1)
        XCTAssertEqual(presenter.callStopLoading, 1)
        XCTAssertEqual(presenter.callDisplayCancelClaimSuccess, 1)
        XCTAssertEqual(presenter.pixKey, randomAwaitingClaimConfirmKey)
    }

    func testTryAgainRequest_WhenUserTapTryAgain_ShouldDisplayCancelClaimFailure() {
        service.cancelClaimResult = .failure(.bodyNotFound)
        sut.tryAgainRequest(of: .cancelClaimError(id: "123"))
        XCTAssertEqual(presenter.callStartLoading, 1)
        XCTAssertEqual(presenter.callStopLoading, 1)
        XCTAssertEqual(presenter.callOpenFeedbackScreen, 1)
        XCTAssertEqual(presenter.feedBackType, .cancelClaimError(id: "123"))
    }

    func testTryAgainRequest_WhenUserTapTryAgain_ShouldReturn() {
        sut.tryAgainRequest(of: .cancelClaimError(id: "1234"))
        XCTAssertEqual(presenter.callStartLoading, 0)
        XCTAssertEqual(presenter.callStopLoading, 0)
        XCTAssertEqual(presenter.callDisplayCancelClaimSuccess, 0)
        XCTAssertEqual(presenter.callOpenFeedbackScreen, 0)
        XCTAssertEqual(presenter.callErrorCount, 1)
        XCTAssertNil(presenter.feedBackType)
        XCTAssertNil(presenter.pixKey)
    }

    func testCancelClaim_WhenReceiveCancelClaim_ShouldDisplayCancelClaimSuccess() {
        service.cancelClaimResult = .success(NoContent())
        let keyManagerItem = KeyManagerItem(title: "", description: cnpjKey, icon: .abacus, isFilledBackgroundIcon: true, secondDescription: "", itemId: "1233")
        sut.cancelClaim(keyManagerItem: keyManagerItem)
        XCTAssertEqual(presenter.callStartLoading, 1)
        XCTAssertEqual(presenter.callStopLoading, 1)
        XCTAssertEqual(presenter.callDisplayCancelClaimSuccess, 1)
        XCTAssertEqual(presenter.pixKey, randomAwaitingClaimConfirmKey)
    }

    func testCancelClaim_WhenReceiveCancelClaim_ShouldDisplayCancelRequestPortabilitySuccess() {
        service.cancelClaimResult = .success(NoContent())
        let keyManagerItem = KeyManagerItem(title: "", description: cnpjKey, icon: .abacus, isFilledBackgroundIcon: true, secondDescription: "", itemId: "12334")
        sut.cancelClaim(keyManagerItem: keyManagerItem)
        XCTAssertEqual(presenter.callStartLoading, 1)
        XCTAssertEqual(presenter.callStopLoading, 1)
        XCTAssertEqual(presenter.callDisplayCancelRequestPortabilitySuccess, 1)
        XCTAssertEqual(presenter.pixKey, randomAwaitingPortabilityConfirmKey)
    }

    func testCancelClaim_WhenReceiveCancelClaim_ShouldDisplayCancelClaimFailure() {
        service.cancelClaimResult = .failure(.bodyNotFound)
        let keyManagerItem = KeyManagerItem(title: "", description: cnpjKey, icon: .abacus, isFilledBackgroundIcon: true, secondDescription: "", itemId: "123")
        sut.cancelClaim(keyManagerItem: keyManagerItem)
        XCTAssertEqual(presenter.callStartLoading, 1)
        XCTAssertEqual(presenter.callStopLoading, 1)
        XCTAssertEqual(presenter.callOpenFeedbackScreen, 1)
    }

    func testCancelClaim_WhenReceiveCancelClaim_ShouldReturn() {
        let keyManagerItem = KeyManagerItem(title: "", description: "", icon: .abacus, isFilledBackgroundIcon: true, secondDescription: "")
        sut.cancelClaim(keyManagerItem: keyManagerItem)
        XCTAssertEqual(presenter.callStartLoading, 0)
        XCTAssertEqual(presenter.callStopLoading, 0)
        XCTAssertEqual(presenter.callDisplayCancelClaimSuccess, 0)
        XCTAssertEqual(presenter.callOpenFeedbackScreen, 0)
        XCTAssertEqual(presenter.callErrorCount, 1)
        XCTAssertNil(presenter.pixKey)
    }

    func testSendShowCancelClaimModalEvent_WhenReceiveSendShowCancelClaimModalEvent_ShouldLog() {
        let event = KeyBizTracker(companyName: "", eventType: .cancelClaimModal(method: .cnpj))
        sut.sendShowCancelClaimModalEvent(keyType: .cnpj)
        guard let loggedEvent = analytics.event else {
            return XCTFail("event logged is nil and it is not equal to \(event.event())")
        }
        XCTAssertTrue(analytics.equals(to: event.event()), "\(loggedEvent) it is not equal to \(event.event())")
    }

    func testComplete_WhenReceiveComplete_ShouldPresentFeedbackClaimCompleted() {
        service.completeClaimResult = .success(NoContent())
        let keyId = "123456789"
        sut.complete(keyId: keyId, hash: "1234")
        XCTAssertEqual(service.didCallCompleteClaim, 1)
        XCTAssertEqual(presenter.callOpenFeedbackScreen, 1)
        XCTAssertEqual(presenter.feedBackType, .claimCompleted(uuid: keyId))
    }

    func testComplete_WhenReceiveComplete_ShouldPresentError() {
        service.completeClaimResult = .failure(.bodyNotFound)
        let keyId = "123456789"
        sut.complete(keyId: keyId, hash: "1234")
        XCTAssertEqual(service.didCallCompleteClaim, 1)
        XCTAssertEqual(presenter.callErrorCount, 1)
    }
}

extension PixKey {
    init(id: String, statusSlug: PixKeyStatus, status: String, name: String, keyType: PixKeyType, createdAt: String, keyValue: String) {
        self.init(type: .registeredKey)
        registeredKeys = [RegisteredKey(id: id,
                                        statusSlug: statusSlug,
                                        status: status,
                                        name: name,
                                        keyType: keyType,
                                        keyValue: keyValue)]
    }
    
    init(keyType: PixKeyType) {
        self.init(type: .unregisteredKey)
        unregisteredKeys = [UnregisteredKey.init(keyType: keyType)]
    }
}
