import AnalyticsModule
import Core
@testable import PIX
import XCTest

private final class ReturnLoadingServiceSpy: ReturnLoadingServicing {
    var resultGetRefundPixAccount: Result<ReturnLoadingResult, ApiError> = .failure(.unknown(nil))
    var resultUserBalance: Result<AvailableAmountViewModel?, ApiError> = .failure(.unknown(nil))
    var resultGetQrCodeData: Result<PixQrCodeAccount, ApiError> = .failure(.unknown(nil))
    var resultGetCopyPasteData: Result<PixQrCodeAccount, ApiError> = .failure(.unknown(nil))
    var resultGetPixAccountWithKey: Result<PixAccount, ApiError> = .failure(.unknown(nil))
    var resultGetPixAccountWithParams: Result<PixAccount, ApiError> = .failure(.unknown(nil))
    
    func getRefundPixAccount(transactionId: String, completion: @escaping (Result<ReturnLoadingResult, ApiError>) -> Void) {
        completion(resultGetRefundPixAccount)
    }
    
    func userBalance(_ completion: @escaping (Result<AvailableAmountViewModel?, ApiError>) -> Void) {
        completion(resultUserBalance)
    }
    
    func getQrCodeData(with qrCode: String, completion: @escaping (Result<PixQrCodeAccount, ApiError>) -> Void) {
        completion(resultGetQrCodeData)
    }
    
    func getCopyPasteData(with code: String, completion: @escaping (Result<PixQrCodeAccount, ApiError>) -> Void) {
        completion(resultGetCopyPasteData)
    }
    
    func getPixAccount(key: String, keyType: KeyType, completion: @escaping (Result<PixAccount, ApiError>) -> Void) {
        completion(resultGetPixAccountWithKey)
    }
    
    func getPixAccount(pixParams: PIXParams, completion: @escaping (Result<PixAccount, ApiError>) -> Void) {
        completion(resultGetPixAccountWithParams)
    }
}

private final class ReturnLoadingPresenterSpy: ReturnLoadingPresenting {
    var viewController: ReturnLoadingDisplay?
    var action: ReturnLoadingAction?
    var error: ApiError?
    var didNextStepCount = 0
    var didShowErrorCount = 0
    
    func didNextStep(action: ReturnLoadingAction) {
        self.action = action
        didNextStepCount += 1
    }
    
    func showError(error: ApiError) {
        self.error = error
        didShowErrorCount += 1
    }
}

final class ReturnLoadingInteractorTests: XCTestCase {
    // MARK: - Variables
    private let userInfo = KeyManagerBizUserInfo(name: "", cnpj: "", mail: "", phone: "", userId: "")
    private let pixAccount = PixAccount(name: "", cpfCnpj: "", bank: "", agency: "", accountNumber: "", imageUrl: "", id: nil)
    private let service = ReturnLoadingServiceSpy()
    private lazy var presenter = ReturnLoadingPresenterSpy()
    private func interactor(with flowType: CashoutLoadingFlowType) -> ReturnLoadingInteracting {
        let analytics = AnalyticsSpy()
        let container = DependencyContainerMock(analytics)
        return ReturnLoadingInteractor(service: service,
                                       presenter: presenter,
                                       dependencies: container,
                                       flowType: flowType)
    }
 
    func testLoadPixData_WhenFlowTypeIsCopyPaste_ShouldReturnSuccessQRSendType() throws {
        let available = try JSONFileDecoder<AvailableAmountViewModel>().load(resource: "userBalance")
        service.resultUserBalance = .success(available)
        
        let pixQrCodeAccount = mockPixQrCodeAccount()
        let pixParams = PIXParams(originType: .qrCode, keyType: "QRCODE", key: "", receiverData: nil)
        service.resultGetCopyPasteData = .success(pixQrCodeAccount)
        interactor(with: .copyPaste(code: "")).loadPixData()
        XCTAssertEqual(presenter.action, ReturnLoadingAction.successQRCodeSend(pixParams: pixParams,
                                                                               amount: 0.0,
                                                                               availableAmount: "193",
                                                                               pixAccount: pixAccount,
                                                                               qrPixAccount: pixQrCodeAccount,
                                                                               userInfo: userInfo))
    }
    
    func testLoadPixData_WhenFlowTypeIsQRCode_ShouldReturnSuccessQRSendType() throws {
        let available = try JSONFileDecoder<AvailableAmountViewModel>().load(resource: "userBalance")
        service.resultUserBalance = .success(available)
        
        let pixQrCodeAccount = mockPixQrCodeAccount()
        let pixParams = PIXParams(originType: .qrCode, keyType: "QRCODE", key: "", receiverData: nil)
        service.resultGetQrCodeData = .success(pixQrCodeAccount)
        interactor(with: .qrCode(code: "")).loadPixData()
        XCTAssertEqual(presenter.action, ReturnLoadingAction.successQRCodeSend(pixParams: pixParams,
                                                                               amount: 0.0,
                                                                               availableAmount: "193",
                                                                               pixAccount: pixAccount,
                                                                               qrPixAccount: pixQrCodeAccount,
                                                                               userInfo: userInfo))
    }
    
    func testLoadPixData_WhenFlowTypeIsManual_ShouldReturnSuccessSendType() throws {
        let available = try JSONFileDecoder<AvailableAmountViewModel>().load(resource: "userBalance")
        service.resultUserBalance = .success(available)
        let pixParams = PIXParams(originType: .manual, keyType: "MANUAL", key: "", receiverData: nil)
        service.resultGetPixAccountWithParams = .success(pixAccount)
        interactor(with: .bankAccount(pixParams: pixParams)).loadPixData()
        XCTAssertEqual(presenter.action, ReturnLoadingAction.successSend(pixParams: pixParams,
                                                                         amount: nil,
                                                                         availableAmount: "193",
                                                                         pixAccount: pixAccount,
                                                                         userInfo: userInfo))
    }
    
    func testLoadPixData_WhenFlowTypeIsKey_ShouldReturnSuccessSendType() throws {
        let available = try JSONFileDecoder<AvailableAmountViewModel>().load(resource: "userBalance")
        service.resultUserBalance = .success(available)
        let pixParams = PIXParams(originType: .key, keyType: "CNPJ", key: "", receiverData: nil)
        service.resultGetPixAccountWithKey = .success(pixAccount)
        interactor(with: .pixKey(key: "", keyType: .cnpj)).loadPixData()
        XCTAssertEqual(presenter.action, ReturnLoadingAction.successSend(pixParams: pixParams,
                                                                         amount: nil,
                                                                         availableAmount: "193",
                                                                         pixAccount: pixAccount,
                                                                         userInfo: userInfo))
    }
    
    private func mockPixQrCodeAccount() -> PixQrCodeAccount {
        let pixQRCodeInfo = PixQRCodeInfo(payerData: [], receiverData: [])
        return PixQrCodeAccount(name: "",
                                cpfCnpj: "",
                                bank: "",
                                imageUrl: "",
                                id: "",
                                qrcodeInfos: pixQRCodeInfo,
                                value: 0,
                                editable: true)
    }
}
