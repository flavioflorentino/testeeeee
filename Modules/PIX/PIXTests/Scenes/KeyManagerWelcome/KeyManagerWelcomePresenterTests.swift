@testable import PIX
import UI
import XCTest

private final class KeyManagementWelcomeCoordinatorSpy: KeyManagementWelcomeCoordinating {
    var viewController: UIViewController?
    
    // MARK: - performAction
    private(set) var displayPerformActionCallsCount = 0
    private(set) var action: KeyManagementWelcomeAction?
    
    func perform(action: KeyManagementWelcomeAction) {
        displayPerformActionCallsCount += 1
        self.action = action
    }
}

private final class KeyManagementWelcomeViewControllerSpy: KeyManagementWelcomeDisplay {
    private(set) var hideNavigationSkipButtonCount = 0
    
    func hideSkipButton() {
        hideNavigationSkipButtonCount += 1
    }
    
    private(set) var displayNavigationCloseButtonCount = 0
    
    func showNavigationCloseButton() {
        displayNavigationCloseButtonCount += 1
    }
    
    private(set) var displayNavigationSkipButtonCount = 0
    
    func showNavigationSkipButton() {
        displayNavigationSkipButtonCount += 1
    }
    
    // MARK: - displayWelcomePages
    private(set) var displayWelcomePagesCallsCount = 0
    private(set) var welcomePages = [KeyManagementWelcomePageModel]()

    func display(welcomePages: [KeyManagementWelcomePageModel]) {
        displayWelcomePagesCallsCount += 1
        self.welcomePages = welcomePages
    }

    // MARK: - scrollToNextPage
    private(set) var scrollToNextPageCallsCount = 0

    func scrollToNextPage() {
        scrollToNextPageCallsCount += 1
    }

    // MARK: - displayPageControl
    private(set) var displayPageControlCallsCount = 0
    private(set) var currentPageIndex: Int?

    func displayPageControl(currentPageIndex: Int) {
        displayPageControlCallsCount += 1
        self.currentPageIndex = currentPageIndex
    }
    
    // MARK: - displayButtons
    private(set) var displayButtonsCallsCount = 0
    private(set) var displayButtons = [KeyManagementWelcomeButtonModel]()
    
    func displayButtons(_ buttons: [KeyManagementWelcomeButtonModel]) {
        displayButtonsCallsCount += 1
        displayButtons = buttons
    }
}

final class KeyManagementWelcomePresenterTests: XCTestCase {
    // MARK: - Variables
    private let viewControllerSpy = KeyManagementWelcomeViewControllerSpy()
    private let coordinatorSpy = KeyManagementWelcomeCoordinatorSpy()
    private lazy var sut: KeyManagementWelcomePresenter = {
        let presenter = KeyManagementWelcomePresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testPresentWelcomePages_ShouldDisplayWelcomePages() {
        let pageModel = KeyManagementWelcomePages.pf.model
        
        sut.present(welcomePages: pageModel)
        
        XCTAssertEqual(viewControllerSpy.displayWelcomePagesCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.welcomePages, pageModel)
    }
    
    func testPresentPageControl_ShouldDisplayPageControl() {
        let pageIndex = 1
        
        sut.presentPageControl(currentPageIndex: pageIndex)
        
        XCTAssertEqual(viewControllerSpy.displayPageControlCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.currentPageIndex, pageIndex)
    }
    
    func testPresentButtons_ShouldDisplayButtons() {
        let buttons = [
            KeyManagementWelcomePageModel.ActionButton(title: "Button1", action: .continue, style: .primary),
            KeyManagementWelcomePageModel.ActionButton(title: "Button2", action: .custom, style: .secundary)
        ]
        
        sut.presentButtons(buttons)
        
        XCTAssertEqual(viewControllerSpy.displayButtonsCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.displayButtons.count, 2)
        XCTAssertEqual(viewControllerSpy.displayButtons.first?.title, "Button1")
        XCTAssertEqual(viewControllerSpy.displayButtons.first?.action, .continue)
        XCTAssertEqual(viewControllerSpy.displayButtons.last?.title, "Button2")
        XCTAssertEqual(viewControllerSpy.displayButtons.last?.action, .custom)
    }
    
    func testScrollToNextPage_ShouldScrollToNextPage() {
        sut.scrollToNextPage()
        
        XCTAssertEqual(viewControllerSpy.scrollToNextPageCallsCount, 1)
    }
    
    func testDidNextStep_WhenActionIsClose_ShouldClose() {
        sut.didNextStep(action: .close)
        
        XCTAssertEqual(coordinatorSpy.action, .close)
    }
    
    func testDidNextStep_WhenActionIsKeyManagement_ShouldOpenKeyManagement() {
        sut.didNextStep(action: .finishOnboarding)
        
        XCTAssertEqual(coordinatorSpy.action, .finishOnboarding)
    }
    
    func testAddNavigationCloseButton_ShouldShowCloseButton() {
        sut.presentNavigationCloseButton()
        
        XCTAssertEqual(viewControllerSpy.displayNavigationCloseButtonCount, 1)
    }
    
    func testAddNavigationSkipButton_ShouldShowSkipButton() {
        sut.presentNavigationSkipButton()
        
        XCTAssertEqual(viewControllerSpy.displayNavigationSkipButtonCount, 1)
    }
    
    func testHideSkipButton_ShouldHideNavigationSkipButton() {
        sut.hideSkipButton()
        
        XCTAssertEqual(viewControllerSpy.hideNavigationSkipButtonCount, 1)
    }
}
