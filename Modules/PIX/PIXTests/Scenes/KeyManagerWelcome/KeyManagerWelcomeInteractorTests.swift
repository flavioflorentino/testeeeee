import AnalyticsModule
import Core
@testable import PIX
import XCTest

private final class KeyManagementWelcomePresenterSpy: KeyManagementWelcomePresenting {
    var viewController: KeyManagementWelcomeDisplay?

    // MARK: - present
    private(set) var presentWelcomePagesCallsCount = 0
    private(set) var welcomePages = [KeyManagementWelcomePageModel]()

    func present(welcomePages: [KeyManagementWelcomePageModel]) {
        presentWelcomePagesCallsCount += 1
        self.welcomePages = welcomePages
    }

    // MARK: - didNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var action: KeyManagementWelcomeAction?

    func didNextStep(action: KeyManagementWelcomeAction) {
        didNextStepActionCallsCount += 1
        self.action = action
    }

    // MARK: - presentPageControl
    private(set) var presentPageControlCallsCount = 0
    private(set) var currentPageIndex: Int?

    func presentPageControl(currentPageIndex: Int) {
        presentPageControlCallsCount += 1
        self.currentPageIndex = currentPageIndex
    }
    
    // MARK: - presentButtons
    private(set) var presentButtonsCallsCount = 0
    private(set) var presentButtons = [KeyManagementWelcomePageModel.ActionButton]()
    
    func presentButtons(_ buttons: [KeyManagementWelcomePageModel.ActionButton]) {
        presentButtonsCallsCount += 1
        presentButtons = buttons
    }

    // MARK: - scrollToNextPage
    private(set) var scrollToNextPageCallsCount = 0

    func scrollToNextPage() {
        scrollToNextPageCallsCount += 1
    }
    
    // MARK: - Add Skip Button on the navigation bar
    private(set) var didAddNavigationSkipButton = 0
    
    func presentNavigationSkipButton() {
        didAddNavigationSkipButton += 1
    }
    // MARK: - Add Close Button on the navigation bar
    private(set) var didAddNavigationCloseButton = 0
    
    func presentNavigationCloseButton() {
        didAddNavigationCloseButton += 1
    }
    
    // MARK: - HideSkipButton
    private(set) var didHideSkipButtonCount = 0
    
    func hideSkipButton() {
        didHideSkipButtonCount += 1
    }
}

private final class KeyManagementWelcomeServiceSpy: KeyManagementWelcomeServicing {
    private(set) var setWelcomePagesVisualizedCallsCount = 0
    
    func setWelcomePagesVisualized() {
        setWelcomePagesVisualizedCallsCount += 1
    }
}

final class KeyManagementWelcomeInteractorTests: XCTestCase {
    // MARK: - Variables
    private let serviceSpy = KeyManagementWelcomeServiceSpy()
    private let presenterSpy = KeyManagementWelcomePresenterSpy()
    private let analyticsSpy = AnalyticsSpy()
    private var pages = KeyManagementWelcomePages.pf
    
    private var sut: KeyManagementWelcomeInteracting?
    
    override func setUp() {
        sut = KeyManagementWelcomeInteractor(
            service: serviceSpy,
            presenter: presenterSpy,
            dependencies: DependencyContainerMock(analyticsSpy),
            pages: pages, userInfo: nil
        )
    }

    func testConfigureWelcomePages_ShouldeConfigureWelcomePagesAndScreenViews() {
        sut?.configureWelcomePages()
        
        XCTAssertEqual(presenterSpy.presentWelcomePagesCallsCount, 1)
        XCTAssertEqual(presenterSpy.welcomePages, KeyManagementWelcomePages.pf.model)
        
        XCTAssertEqual(presenterSpy.presentPageControlCallsCount, 1)
        XCTAssertEqual(presenterSpy.currentPageIndex, .zero)
        
        let buttons = KeyManagementWelcomePages.pf.model[0].buttons
        XCTAssertEqual(presenterSpy.presentButtonsCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentButtons, buttons)
    }
    
    func testDidScrollToPage_WhenPassedValidPageIndex_ShouldUpdateButtonAndPageControl() {
        let pageIndex = 2
        sut?.didScrollToPage(at: pageIndex)
        
        XCTAssertEqual(presenterSpy.presentPageControlCallsCount, 1)
        XCTAssertEqual(presenterSpy.currentPageIndex, pageIndex)
        
        let buttons = KeyManagementWelcomePages.pf.model[2].buttons
        XCTAssertEqual(presenterSpy.presentButtonsCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentButtons, buttons)
    }
    
    func testDidScrollToPage_WhenPassedInvalidPageIndex_ShouldUpdateButtonAndPageControl() {
        let pageIndex = 3
        sut?.didScrollToPage(at: pageIndex)
        
        XCTAssertEqual(presenterSpy.presentPageControlCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentButtonsCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentButtons, [])
    }
    
    func testDidTap_WhenActionIsContinueAndIsLastPage_ShouldSetLocalFlagAndPresentKeyManagement() {
        let pageIndex = KeyManagementWelcomePages.pf.model.count - 1
        
        sut?.didScrollToPage(at: pageIndex)
        sut?.didTap(action: .continue)
        
        XCTAssertEqual(serviceSpy.setWelcomePagesVisualizedCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .finishOnboarding)
    }
    
    func testDidTap_WhenActionIsContinue_ShouldScrollToNextPage() {
        sut?.didTap(action: .continue)
        
        XCTAssertEqual(presenterSpy.scrollToNextPageCallsCount, 1)
    }
    
    func testDidClose_ShouldCallPresentClose() {
        sut?.didClose()
        
        XCTAssertEqual(presenterSpy.action, .close)
    }
    
    func testSkipOnboard_ShouldCallKeyManagement(){
        sut?.skipOnboard()
        
        XCTAssertEqual(presenterSpy.action, .finishOnboarding)
    }
    
    func testConfigureNavigationBarItems_ShouldPresentCloseButtonOnTheNavigation() {
        
        sut?.configureNavigationBarButtons()
        
        XCTAssertEqual(presenterSpy.didAddNavigationCloseButton, 1)
    }
    
    func testConfigureNavigationBarItems_ShouldPresentSkipButtonOnTheNavigation() {
        pages = .biz( { _ in  })
        
        setUp()
        sut?.configureNavigationBarButtons()
        
        XCTAssertEqual(presenterSpy.didAddNavigationSkipButton, 1)
    }

    func testDidScrollToPage_WhenReachLastIndex_ShouldHideSkipButton() {
        pages = .biz( { _ in  })
        setUp()
        
        let pageIndex = 2
        sut?.didScrollToPage(at: pageIndex)
        
        XCTAssertEqual(presenterSpy.didHideSkipButtonCount, 1)
    }
}
