@testable import PIX
import XCTest

final class KeyManagementWelcomeCoordinatorTests: XCTestCase {
    private lazy var navControllerSpy = SpyNavigationController(rootViewController: UIViewController())
    
    func testPerform_WhenActionIsClose_ShouldDismissNavController() {
        let sut = KeyManagementWelcomeCoordinator(pages: .pf, from: .hub)
        sut.viewController = navControllerSpy.topViewController
        
        sut.perform(action: .close)
        
        XCTAssertEqual(navControllerSpy.dismissCallCount, 1)
    }
    
    func testPerform_WhenActionIsKeyManager_ShouldPresentKeyManagement() {
        let sut = KeyManagementWelcomeCoordinator(pages: .pf, from: .hub)
        sut.viewController = navControllerSpy.topViewController
        
        sut.perform(action: .finishOnboarding)
        
        XCTAssertTrue(navControllerSpy.lastSetViewControllers.last is KeyManagerConsumerViewController)
        XCTAssertNotNil(navControllerSpy.setViewControllersCallsCount)
    }
    
    func testPerform_WhenActionIsClose_ShouldPopNavController() {
        let mockViewController = SpyViewController()
        let sut = KeyManagementWelcomeCoordinator(pages: .pf, from: .hub)
        sut.viewController = mockViewController
        navControllerSpy.pushViewController(mockViewController, animated: false)
        
        sut.perform(action: .close)
        
        XCTAssertEqual(navControllerSpy.poppedViewControllerCount, 1)
    }
}
