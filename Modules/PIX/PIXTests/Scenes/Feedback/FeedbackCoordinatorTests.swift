@testable import PIX
import XCTest

private final class FeedbackDelegateSpy: FeedbackDelegate {
    // MARK: - didAction
    private(set) var didActionCallsCount = 0
    private(set) var action: FeedbackAction?
    
    func didAction(action: FeedbackAction, feedbackType: FeedbackViewType) {
        didActionCallsCount += 1
        self.action = action
    }
}

final class FeedbackCoordinatorTests: XCTestCase {
    private let viewControllerSpy = SpyViewController()
    private let delegateSpy = FeedbackDelegateSpy()
    
    private lazy var sut: FeedbackCoordinating = {
        let coordinator = FeedbackCoordinator()
        coordinator.viewController = viewControllerSpy
        coordinator.delegate = delegateSpy
        return coordinator
    }()
    
    func testExecute_WhenActionIsAllowClaim_ShouldPassTheActionToTheDelegate() {
        sut.execute(action: .allowClaim, feedbackType: .claimInProgress(uuid: "uiid"))
        
        XCTAssertEqual(delegateSpy.didActionCallsCount, 1)
        XCTAssertEqual(delegateSpy.action, .allowClaim)
    }
    
    func testExecute_WhenActionIsClose_ShouldPassTheActionToTheDelegate() {
        sut.execute(action: .close, feedbackType: .claimInProgress(uuid: "uiid"))
        
        XCTAssertEqual(delegateSpy.didActionCallsCount, 1)
        XCTAssertEqual(delegateSpy.action, .close)
    }
    
    func testExecute_WhenActionIsGrantPortability_ShouldPassTheActionToTheDelegate() {
        sut.execute(action: .grantPortability, feedbackType: .claimInProgress(uuid: "uiid"))
        
        XCTAssertEqual(delegateSpy.didActionCallsCount, 1)
        XCTAssertEqual(delegateSpy.action, .grantPortability)
    }
    
    func testExecute_WhenActionIsIgnore_ShouldPassTheActionToTheDelegate() {
        sut.execute(action: .ignore, feedbackType: .claimInProgress(uuid: "uiid"))
        
        XCTAssertEqual(delegateSpy.didActionCallsCount, 1)
        XCTAssertEqual(delegateSpy.action, .ignore)
    }
    
    func testExecute_WhenActionIsRevalidateKey_ShouldPassTheActionToTheDelegate() {
        sut.execute(action: .revalidateKey, feedbackType: .claimInProgress(uuid: "uiid"))
        
        XCTAssertEqual(delegateSpy.didActionCallsCount, 1)
        XCTAssertEqual(delegateSpy.action, .revalidateKey)
    }
    
    func testExecute_WhenActionIsTryAgain_ShouldPassTheActionToTheDelegate() {
        sut.execute(action: .tryAgain, feedbackType: .claimInProgress(uuid: "uiid"))
        
        XCTAssertEqual(delegateSpy.didActionCallsCount, 1)
        XCTAssertEqual(delegateSpy.action, .tryAgain)
    }
}
