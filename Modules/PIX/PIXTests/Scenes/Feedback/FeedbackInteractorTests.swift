import AnalyticsModule
@testable import PIX
import XCTest

private final class FeedbackPresenterSpy: FeedbackPresenting {
    var viewController: FeedbackDisplaying?
    
    // MARK: - execute
    private(set) var executeCallsCount = 0
    private(set) var action: FeedbackAction?
    
    func execute(action: FeedbackAction) {
        executeCallsCount += 1
        self.action = action
    }
    
    // MARK: - presentData
    
    private(set) var presentDataCallsCount = 0
    private(set) var viewType: FeedbackViewType?
    
    func presentData(with type: FeedbackViewType) {
        presentDataCallsCount += 1
        viewType = type
    }
}

final class FeedbackInteractorTests: XCTestCase {
    private let analytics = AnalyticsSpy()
    private let presenterSpy = FeedbackPresenterSpy()
    
    func configureInteractor(type: FeedbackViewType) -> FeedbackInteracting {
        FeedbackInteractor(
            type: type,
            presenter: presenterSpy,
            dependencies: DependencyContainerMock(analytics),
            origin: .undefined
        )
    }
    
    func testFetchData_WhenViewTypeIsClaimCompleted_ShouldPresentClaimCompletedData() {
        let viewType: FeedbackViewType = .claimCompleted(uuid: "uiid")
        let sut = configureInteractor(type: viewType)
        
        sut.fetchData()
        
        XCTAssertEqual(presenterSpy.presentDataCallsCount, 1)
        XCTAssertEqual(presenterSpy.viewType, viewType)
    }
    
    func testFetchData_WhenViewTypeIsClaimDonorPermited_ShouldPresentClaimDonorPermited() {
        let viewType: FeedbackViewType = .claimDonorPermitted(uuid: "uiid")
        let sut = configureInteractor(type: viewType)
        
        sut.fetchData()
        
        XCTAssertEqual(presenterSpy.presentDataCallsCount, 1)
        XCTAssertEqual(presenterSpy.viewType, viewType)
    }
    
    func testFetchData_WhenViewTypeIsClaimDonorInProgress_ShouldPresentClaimDonorInProgressData() {
        let viewType: FeedbackViewType = .claimDonorInProgress(uuid: "uiid")
        let sut = configureInteractor(type: viewType)
        
        sut.fetchData()
        
        XCTAssertEqual(presenterSpy.presentDataCallsCount, 1)
        XCTAssertEqual(presenterSpy.viewType, viewType)
    }
    
    func testFetchData_WhenViewTypeIsClaimDonorRequested_ShouldPresentClaimDonorRequestedData() {
        let viewType: FeedbackViewType = .claimDonorRequested(uuid: "uiid")
        let sut = configureInteractor(type: viewType)
        
        sut.fetchData()
        
        XCTAssertEqual(presenterSpy.presentDataCallsCount, 1)
        XCTAssertEqual(presenterSpy.viewType, viewType)
    }
    
    func testFetchData_WhenViewTypeIsClaimDonorNotCompleted_ShouldPresentClaimDonorNotCompletedData() {
        let viewType: FeedbackViewType = .claimDonorNotCompleted(uuid: "uuid")
        let sut = configureInteractor(type: viewType)
        
        sut.fetchData()
        
        XCTAssertEqual(presenterSpy.presentDataCallsCount, 1)
        XCTAssertEqual(presenterSpy.viewType, viewType)
    }
    
    func testFetchData_WhenViewTypeIsClaimDonorReceived_ShouldPresentClaimDonorReceivedData() {
        let viewType: FeedbackViewType = .claimDonorReceived(uuid: "uuid")
        let sut = configureInteractor(type: viewType)
        
        sut.fetchData()
        
        XCTAssertEqual(presenterSpy.presentDataCallsCount, 1)
        XCTAssertEqual(presenterSpy.viewType, viewType)
    }
    
    func testFetchData_WhenViewTypeIsClaimDonorRevalidationCompleted_ShouldPresentClaimDonorRevalidationCompletedData() {
        let viewType: FeedbackViewType = .claimDonorRevalidationCompleted(uuid: "uiid")
        let sut = configureInteractor(type: viewType)
        
        sut.fetchData()
        
        XCTAssertEqual(presenterSpy.presentDataCallsCount, 1)
        XCTAssertEqual(presenterSpy.viewType, viewType)
    }
    
    func testFetchData_WhenViewTypeIsClaimDonorRevalidationInProgress_ShouldPresentClaimDonorRevalidationInProgressData() {
        let viewType: FeedbackViewType = .claimDonorRevalidationInProgress(uuid: "uiid")
        let sut = configureInteractor(type: viewType)
        
        sut.fetchData()
        
        XCTAssertEqual(presenterSpy.presentDataCallsCount, 1)
        XCTAssertEqual(presenterSpy.viewType, viewType)
    }
    
    func testFetchData_WhenViewTypeIsClaimDonorRevalidationNotCompleted_ShouldPresentClaimDonorRevalidationNotCompletedData() {
        let viewType: FeedbackViewType = .claimDonorRevalidationInProgress(uuid: "uiid")
        let sut = configureInteractor(type: viewType)
        
        sut.fetchData()
        
        XCTAssertEqual(presenterSpy.presentDataCallsCount, 1)
        XCTAssertEqual(presenterSpy.viewType, viewType)
    }
    
    func testFetchData_WhenViewTypeIsClaimInProgress_ShouldPresentClaimInProgressData() {
        let viewType: FeedbackViewType = .claimInProgress(uuid: "uiid")
        let sut = configureInteractor(type: viewType)
        
        sut.fetchData()
        
        XCTAssertEqual(presenterSpy.presentDataCallsCount, 1)
        XCTAssertEqual(presenterSpy.viewType, viewType)
    }
    
    func testFetchData_WhenViewTypeIsClaimRefused_ShouldPresentClaimRefusedData() {
        let viewType: FeedbackViewType = .claimRefused(uuid: "uuid")
        let sut = configureInteractor(type: viewType)
        
        sut.fetchData()
        
        XCTAssertEqual(presenterSpy.presentDataCallsCount, 1)
        XCTAssertEqual(presenterSpy.viewType, viewType)
    }
    
    func testFetchData_WhenViewTypeIsKeyRecordInProgress_ShouldPresentKeyRecordInProgressData() {
        let viewType: FeedbackViewType = .keyRecordInProgress(uuid: "uiid")
        let sut = configureInteractor(type: viewType)
        
        sut.fetchData()
        
        XCTAssertEqual(presenterSpy.presentDataCallsCount, 1)
        XCTAssertEqual(presenterSpy.viewType, viewType)
    }
    
    func testFetchData_WhenViewTypeIsKeyRecordNotRegistered_ShouldPresentKeyRecordNotRegisteredData() {
        let viewType: FeedbackViewType = .keyRecordNotRegistered(uuid: "uuid")
        let sut = configureInteractor(type: viewType)
        
        sut.fetchData()
        
        XCTAssertEqual(presenterSpy.presentDataCallsCount, 1)
        XCTAssertEqual(presenterSpy.viewType, viewType)
    }
    
    func testFetchData_WhenViewTypeIsKeyRecordRegistered_ShouldPresentKeyRecordRegisteredData() {
        let viewType: FeedbackViewType = .keyRecordRegistered(uuid: "uiid")
        let sut = configureInteractor(type: viewType)
        
        sut.fetchData()
        
        XCTAssertEqual(presenterSpy.presentDataCallsCount, 1)
        XCTAssertEqual(presenterSpy.viewType, viewType)
    }
    
    func testFetchData_WhenViewTypeIsPendingKeyDeletion_ShouldPresentPendingKeyDeletionData() {
        let viewType: FeedbackViewType = .pendingKeyDeletion(uuid: "uiid")
        let sut = configureInteractor(type: viewType)
        
        sut.fetchData()
        
        XCTAssertEqual(presenterSpy.presentDataCallsCount, 1)
        XCTAssertEqual(presenterSpy.viewType, viewType)
    }
    
    func testFetchData_WhenViewTypeIsPortabilityCompleted_ShouldPresentPortabilityCompletedData() {
        let viewType: FeedbackViewType = .portabilityCompleted(uuid: "uiid")
        let sut = configureInteractor(type: viewType)
        
        sut.fetchData()
        
        XCTAssertEqual(presenterSpy.presentDataCallsCount, 1)
        XCTAssertEqual(presenterSpy.viewType, viewType)
    }
    
    func testFetchData_WhenViewTypeIsPortabilityDonorCompleted_ShouldPresentPortabilityDonorCompletedData() {
        let viewType: FeedbackViewType = .portabilityDonorCompleted(uuid: "uiid")
        let sut = configureInteractor(type: viewType)
        
        sut.fetchData()
        
        XCTAssertEqual(presenterSpy.presentDataCallsCount, 1)
        XCTAssertEqual(presenterSpy.viewType, viewType)
    }
    
    func testFetchData_WhenViewTypeIsPortabilityDonorInProgress_ShouldPresentPortabilityDonorInProgressData() {
        let viewType: FeedbackViewType = .portabilityDonorInProgress(uuid: "uiid")
        let sut = configureInteractor(type: viewType)
        
        sut.fetchData()
        
        XCTAssertEqual(presenterSpy.presentDataCallsCount, 1)
        XCTAssertEqual(presenterSpy.viewType, viewType)
    }
    
    func testFetchData_WhenViewTypeIsPortabilityDonorReceived_ShouldPresentPortabilityDonorReceivedData() {
        let viewType: FeedbackViewType = .portabilityDonorReceived(uuid: "uuid")
        let sut = configureInteractor(type: viewType)
        
        sut.fetchData()
        
        XCTAssertEqual(presenterSpy.presentDataCallsCount, 1)
        XCTAssertEqual(presenterSpy.viewType, viewType)
    }
    
    func testFetchData_WhenViewTypeIsPortabilityDonorRefused_ShouldPresentPortabilityDonorRefusedData() {
        let viewType: FeedbackViewType = .portabilityDonorRefused(uuid: "uuid")
        let sut = configureInteractor(type: viewType)
        
        sut.fetchData()
        
        XCTAssertEqual(presenterSpy.presentDataCallsCount, 1)
        XCTAssertEqual(presenterSpy.viewType, viewType)
    }
    
    func testFetchData_WhenViewTypeIsPortabilityInProgress_ShouldPresentPortabilityInProgressData() {
        let viewType: FeedbackViewType = .portabilityInProgress(uuid: "uiid")
        let sut = configureInteractor(type: viewType)
        
        sut.fetchData()
        
        XCTAssertEqual(presenterSpy.presentDataCallsCount, 1)
        XCTAssertEqual(presenterSpy.viewType, viewType)
    }
    
    func testFetchData_WhenViewTypeIsPortabilityRefused_ShouldPresentPortabilityRefusedData() {
        let viewType: FeedbackViewType = .portabilityRefused(uuid: "uuid")
        let sut = configureInteractor(type: viewType)
        
        sut.fetchData()
        
        XCTAssertEqual(presenterSpy.presentDataCallsCount, 1)
        XCTAssertEqual(presenterSpy.viewType, viewType)
    }
    
    func testFetchData_WhenViewTypeIsIdentityValidationCompleted_ShouldPresentIdentityValidationCompleted() {
        let viewType: FeedbackViewType = .identityValidationCompleted
        let sut = configureInteractor(type: viewType)
        
        sut.fetchData()
        
        XCTAssertEqual(presenterSpy.presentDataCallsCount, 1)
        XCTAssertEqual(presenterSpy.viewType, viewType)
    }
    
    func testFetchData_WhenViewTypeIsIdentityValidationNotCompleted_ShouldPresentIdentityValidationNotCompleted() {
        let viewType: FeedbackViewType = .identityValidationNotCompleted
        let sut = configureInteractor(type: viewType)
        
        sut.fetchData()
        
        XCTAssertEqual(presenterSpy.presentDataCallsCount, 1)
        XCTAssertEqual(presenterSpy.viewType, viewType)
    }
    
    func testFetchData_WhenViewTypeIsIdentityValidationInReview_ShouldPresentIdentityValidationInReview() {
        let viewType: FeedbackViewType = .identityValidationInReview
        let sut = configureInteractor(type: viewType)
        
        sut.fetchData()
        
        XCTAssertEqual(presenterSpy.presentDataCallsCount, 1)
        XCTAssertEqual(presenterSpy.viewType, viewType)
    }
    
    func testDidClose_ShouldExecuteCloseAction() {
        let viewType: FeedbackViewType = .claimCompleted(uuid: "uiid")
        let sut = configureInteractor(type: viewType)
        
        sut.didClose()
        
        XCTAssertEqual(presenterSpy.executeCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .close)
    }
    
    func testExecute_ShouldPassActionToPresenter() {
        let viewType: FeedbackViewType = .claimCompleted(uuid: "uiid")
        let sut = configureInteractor(type: viewType)
        
        sut.execute(action: .allowClaim)
        
        XCTAssertEqual(presenterSpy.executeCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .allowClaim)
    }
}
