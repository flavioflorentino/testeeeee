import AssetsKit
@testable import PIX
import UI
import XCTest

private final class FeedbackCoordinatorSpy: FeedbackCoordinating {
    var viewController: UIViewController?
    var delegate: FeedbackDelegate?
    
    // MARK: - execute
    private(set) var executeCallsCount = 0
    private(set) var action: FeedbackAction?
    
    func execute(action: FeedbackAction, feedbackType: FeedbackViewType) {
        executeCallsCount += 1
        self.action = action
    }
}

private final class FeedbackViewControllerSpy: FeedbackDisplaying {
    // MARK: - displayViewModel
    private(set) var displayViewModelCallsCount = 0
    private(set) var viewModel: FeedbackViewModeling?
    
    func display(viewModel: FeedbackViewModeling) {
        displayViewModelCallsCount += 1
        self.viewModel = viewModel
    }
    
    // MARK: - displayNotification
    private(set) var displayNotificationCallsCount = 0
    private(set) var notification: String?
    
    func display(notification: String) {
        displayNotificationCallsCount += 1
        self.notification = notification
    }
}

final class FeedbackPresenterTests: XCTestCase {
    private let coordinatorSpy = FeedbackCoordinatorSpy()
    private let viewControllerSpy = FeedbackViewControllerSpy()
    
    private lazy var sut: FeedbackPresenting = {
        let presenter = FeedbackPresenter(coordinator: coordinatorSpy, type: .claimDonorInProgress(uuid: "uuid"))
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testPresentData_WhenViewTypeIsClaimCompleted_ShouldDisplayClaimCompletedData() {
        sut.presentData(with: .claimCompleted(uuid: "uuid"))
        
        XCTAssertEqual(viewControllerSpy.displayViewModelCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.image, Resources.Illustrations.iluVictory.image)
        XCTAssertEqual(viewControllerSpy.viewModel?.title, Strings.Feedback.ClaimCompleted.title)
        XCTAssertEqual(viewControllerSpy.viewModel?.description, Strings.Feedback.ClaimCompleted.description)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.count, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.title, Strings.General.gotIt)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.action, .close)
        XCTAssertEqual(viewControllerSpy.displayNotificationCallsCount, 0)
        XCTAssertNil(viewControllerSpy.viewModel?.notification)
    }
    
    func testPresentData_WhenViewTypeIsClaimDonorPermited_ShouldDisplayClaimDonorPermitedData() {
        sut.presentData(with: .claimDonorPermitted(uuid: "uuid"))
        
        XCTAssertEqual(viewControllerSpy.displayViewModelCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.image, Resources.Illustrations.iluEmail.image)
        XCTAssertEqual(viewControllerSpy.viewModel?.title, Strings.Feedback.ClaimDonorPermitted.title)
        XCTAssertEqual(viewControllerSpy.viewModel?.description, Strings.Feedback.ClaimDonorPermitted.description)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.count, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.title, Strings.General.gotIt)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.action, .close)
        XCTAssertEqual(viewControllerSpy.displayNotificationCallsCount, 0)
        XCTAssertNil(viewControllerSpy.viewModel?.notification)
    }
    
    func testPresentData_WhenViewTypeIsClaimDonorInProgress_ShouldDisplayClaimDonorInProgressData() {
        sut.presentData(with: .claimDonorInProgress(uuid: "uuid"))
        
        XCTAssertEqual(viewControllerSpy.displayViewModelCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.image, Resources.Illustrations.iluClock2.image)
        XCTAssertEqual(viewControllerSpy.viewModel?.title, Strings.Feedback.ClaimDonorInProgress.title)
        XCTAssertEqual(viewControllerSpy.viewModel?.description, Strings.Feedback.ClaimDonorInProgress.description)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.count, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.title, Strings.General.gotIt)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.action, .close)
        XCTAssertEqual(viewControllerSpy.displayNotificationCallsCount, 0)
        XCTAssertNil(viewControllerSpy.viewModel?.notification)
    }
    
    func testPresentData_WhenViewTypeIsClaimDonorKey_ShouldDisplayClaimDonorKeyData() {
        sut.presentData(with: .claimDonorRequested(uuid: "uuid"))
        
        XCTAssertEqual(viewControllerSpy.displayViewModelCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.image, Resources.Illustrations.iluEmail.image)
        XCTAssertEqual(viewControllerSpy.viewModel?.title, Strings.Feedback.ClaimDonorRequested.title)
        XCTAssertEqual(viewControllerSpy.viewModel?.description, Strings.Feedback.ClaimDonorRequested.description)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.count, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.title, Strings.General.gotIt)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.action, .close)
        XCTAssertEqual(viewControllerSpy.displayNotificationCallsCount, 0)
        XCTAssertNil(viewControllerSpy.viewModel?.notification)
    }
    
    func testPresentData_WhenViewTypeIsClaimDonorNotCompleted_ShouldDisplayClaimDonorNotCompletedData() {
        sut.presentData(with: .claimDonorNotCompleted(uuid: "uuid"))
        
        XCTAssertEqual(viewControllerSpy.displayViewModelCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.image, Resources.Illustrations.iluPeople.image)
        XCTAssertEqual(viewControllerSpy.viewModel?.title, Strings.Feedback.ClaimDonorNotCompleted.title)
        XCTAssertEqual(viewControllerSpy.viewModel?.description, Strings.Feedback.ClaimDonorNotCompleted.description)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.count, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.title, Strings.General.tryAgain)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.action, .tryAgain)
        XCTAssertEqual(viewControllerSpy.displayNotificationCallsCount, 0)
        XCTAssertNil(viewControllerSpy.viewModel?.notification)
    }
    
    func testPresentData_WhenViewTypeIsClaimDonorReceived_ShouldDisplayClaimDonorReceivedData() {
        sut.presentData(with: .claimDonorReceived(uuid: "uuid"))
        
        XCTAssertEqual(viewControllerSpy.displayViewModelCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.image, Resources.Illustrations.iluEmail.image)
        XCTAssertEqual(viewControllerSpy.viewModel?.title, Strings.Feedback.ClaimDonorReceived.title)
        XCTAssertEqual(viewControllerSpy.viewModel?.description, Strings.Feedback.ClaimDonorReceived.description)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.count, 2)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.title, Strings.General.revalidateKey)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.action, .revalidateKey)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.last?.title, Strings.General.allowClaim)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.last?.action, .allowClaim)
        XCTAssertEqual(viewControllerSpy.displayNotificationCallsCount, 0)
        XCTAssertNil(viewControllerSpy.viewModel?.notification)
    }
    
    func testPresentData_WhenViewTypeIsClaimDonorRevalidationCompleted_ShouldDisplayClaimDonorRevalidationCompletedData() {
        sut.presentData(with: .claimDonorRevalidationCompleted(uuid: "uuid"))
        
        XCTAssertEqual(viewControllerSpy.displayViewModelCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.image, Resources.Illustrations.iluVictory.image)
        XCTAssertEqual(viewControllerSpy.viewModel?.title, Strings.Feedback.ClaimDonorRevalidationCompleted.title)
        XCTAssertEqual(viewControllerSpy.viewModel?.description, Strings.Feedback.ClaimDonorRevalidationCompleted.description)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.count, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.title, Strings.General.gotIt)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.action, .close)
        XCTAssertEqual(viewControllerSpy.displayNotificationCallsCount, 0)
        XCTAssertNil(viewControllerSpy.viewModel?.notification)
    }
    
    func testPresentData_WhenViewTypeIsClaimDonorRevalidationInProgress_ShouldDisplayClaimDonorRevalidationInProgressData() {
        sut.presentData(with: .claimDonorRevalidationInProgress(uuid: "uuid"))
        
        XCTAssertEqual(viewControllerSpy.displayViewModelCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.image, Resources.Illustrations.iluClock2.image)
        XCTAssertEqual(viewControllerSpy.viewModel?.title, Strings.Feedback.ClaimDonorRevalidationInProgress.title)
        XCTAssertEqual(viewControllerSpy.viewModel?.description, Strings.Feedback.ClaimDonorRevalidationInProgress.description)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.count, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.title, Strings.General.gotIt)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.action, .close)
        XCTAssertEqual(viewControllerSpy.displayNotificationCallsCount, 0)
        XCTAssertNil(viewControllerSpy.viewModel?.notification)
    }
    
    func testPresentData_WhenViewTypeIsClaimDonorRevalidationNotCompleted_ShouldDisplayClaimDonorRevalidationNotCompletedData() {
        sut.presentData(with: .claimDonorRevalidationNotCompleted(uuid: "uuid"))
        
        XCTAssertEqual(viewControllerSpy.displayViewModelCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.image, Resources.Illustrations.iluPeople.image)
        XCTAssertEqual(viewControllerSpy.viewModel?.title, Strings.Feedback.ClaimDonorRevalidationNotCompleted.title)
        XCTAssertEqual(viewControllerSpy.viewModel?.description, Strings.Feedback.ClaimDonorRevalidationNotCompleted.description)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.count, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.title, Strings.General.tryAgain)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.action, .tryAgain)
        XCTAssertEqual(viewControllerSpy.displayNotificationCallsCount, 0)
        XCTAssertNil(viewControllerSpy.viewModel?.notification)
    }
    
    func testPresentData_WhenViewTypeIsClaimInProgress_ShouldDisplayClaimInProgressData() {
        sut.presentData(with: .claimInProgress(uuid: ""))
        
        XCTAssertEqual(viewControllerSpy.displayViewModelCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.image, Resources.Illustrations.iluEmail.image)
        XCTAssertEqual(viewControllerSpy.viewModel?.title, Strings.Feedback.ClaimInProgress.title)
        XCTAssertEqual(viewControllerSpy.viewModel?.description, Strings.Feedback.ClaimInProgress.description)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.count, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.title, Strings.General.gotIt)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.action, .close)
        XCTAssertEqual(viewControllerSpy.displayNotificationCallsCount, 0)
        XCTAssertNil(viewControllerSpy.viewModel?.notification)
    }
    
    func testPresentData_WhenViewTypeIsClaimRefused_ShouldDisplayClaimRefusedData() {
        sut.presentData(with: .claimRefused(uuid: "uuid"))
        
        XCTAssertEqual(viewControllerSpy.displayViewModelCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.image, Resources.Illustrations.iluPeople.image)
        XCTAssertEqual(viewControllerSpy.viewModel?.title, Strings.Feedback.ClaimRefused.title)
        XCTAssertEqual(viewControllerSpy.viewModel?.description, Strings.Feedback.ClaimRefused.description)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.count, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.title, Strings.General.tryAgain)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.action, .tryAgain)
        XCTAssertEqual(viewControllerSpy.displayNotificationCallsCount, 0)
        XCTAssertNil(viewControllerSpy.viewModel?.notification)
    }
    
    func testPresentData_WhenViewTypeIsKeyRecordInProgress_ShouldDisplayKeyRecordInProgressData() {
        sut.presentData(with: .keyRecordInProgress(uuid: ""))
        
        XCTAssertEqual(viewControllerSpy.displayViewModelCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.image, Resources.Illustrations.iluClock2.image)
        XCTAssertEqual(viewControllerSpy.viewModel?.title, Strings.Feedback.KeyRecordInProgress.title)
        XCTAssertEqual(viewControllerSpy.viewModel?.description, Strings.Feedback.KeyRecordInProgress.description)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.count, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.title, Strings.General.gotIt)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.action, .close)
        XCTAssertEqual(viewControllerSpy.displayNotificationCallsCount, 0)
        XCTAssertNil(viewControllerSpy.viewModel?.notification)
    }
    
    func testPresentData_WhenViewTypeIsKeyRecordNotRegistered_ShouldDisplayKeyRecordNotRegisteredData() {
        sut.presentData(with: .keyRecordNotRegistered(uuid: "uuid"))
        
        XCTAssertEqual(viewControllerSpy.displayViewModelCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.image, Resources.Illustrations.iluPeople.image)
        XCTAssertEqual(viewControllerSpy.viewModel?.title, Strings.Feedback.KeyRecordNotRegistered.title)
        XCTAssertEqual(viewControllerSpy.viewModel?.description, Strings.Feedback.KeyRecordNotRegistered.description)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.count, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.title, Strings.General.tryAgain)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.action, .tryAgain)
        XCTAssertEqual(viewControllerSpy.displayNotificationCallsCount, 0)
        XCTAssertNil(viewControllerSpy.viewModel?.notification)
    }
    
    func testPresentData_WhenViewTypeIsKeyRecordRegistered_ShouldDisplayKeyRecordRegisteredData() {
        sut.presentData(with: .keyRecordRegistered(uuid: ""))
        
        XCTAssertEqual(viewControllerSpy.displayViewModelCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.image, Resources.Illustrations.iluVictory.image)
        XCTAssertEqual(viewControllerSpy.viewModel?.title, Strings.Feedback.KeyRecordRegistered.title)
        XCTAssertEqual(viewControllerSpy.viewModel?.description, Strings.Feedback.KeyRecordRegistered.description)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.count, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.title, Strings.General.gotIt)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.action, .close)
        XCTAssertEqual(viewControllerSpy.displayNotificationCallsCount, 0)
        XCTAssertNil(viewControllerSpy.viewModel?.notification)
    }
    
    func testPresentData_WhenViewTypeIsPendingKeyDeletion_ShouldDisplayPendingKeyDeletionData() {
        sut.presentData(with: .pendingKeyDeletion(uuid: ""))
        
        XCTAssertEqual(viewControllerSpy.displayViewModelCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.image, Resources.Illustrations.iluClock2.image)
        XCTAssertEqual(viewControllerSpy.viewModel?.title, Strings.Feedback.PendingKeyDeletion.title)
        XCTAssertEqual(viewControllerSpy.viewModel?.description, Strings.Feedback.PendingKeyDeletion.description)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.count, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.title, Strings.General.gotIt)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.action, .close)
        XCTAssertEqual(viewControllerSpy.displayNotificationCallsCount, 0)
        XCTAssertNil(viewControllerSpy.viewModel?.notification)
    }
    
    func testPresentData_WhenViewTypeIsPortabilityCompleted_ShouldDisplayPortabilityCompletedData() {
        sut.presentData(with: .portabilityCompleted(uuid: ""))
        
        XCTAssertEqual(viewControllerSpy.displayViewModelCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.image, Resources.Illustrations.iluVictory.image)
        XCTAssertEqual(viewControllerSpy.viewModel?.title, Strings.Feedback.PortabilityCompleted.title)
        XCTAssertEqual(viewControllerSpy.viewModel?.description, Strings.Feedback.PortabilityCompleted.description)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.count, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.title, Strings.General.gotIt)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.action, .close)
        XCTAssertEqual(viewControllerSpy.displayNotificationCallsCount, 0)
        XCTAssertNil(viewControllerSpy.viewModel?.notification)
    }
    
    func testPresentData_WhenViewTypeIsPortabilityDonorCompleted_ShouldDisplayPortabilityDonorCompletedData() {
        sut.presentData(with: .portabilityDonorCompleted(uuid: ""))
        
        XCTAssertEqual(viewControllerSpy.displayViewModelCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.image, Resources.Illustrations.iluVictory.image)
        XCTAssertEqual(viewControllerSpy.viewModel?.title, Strings.Feedback.PortabilityDonorCompleted.title)
        XCTAssertEqual(viewControllerSpy.viewModel?.description, Strings.Feedback.PortabilityDonorCompleted.description)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.count, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.title, Strings.General.gotIt)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.action, .close)
        XCTAssertEqual(viewControllerSpy.displayNotificationCallsCount, 0)
        XCTAssertNil(viewControllerSpy.viewModel?.notification)
    }
    
    func testPresentData_WhenViewTypeIsPortabilityDonorInProgress_ShouldDisplayPortabilityDonorInProgressData() {
        sut.presentData(with: .portabilityDonorInProgress(uuid: ""))
        
        XCTAssertEqual(viewControllerSpy.displayViewModelCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.image, Resources.Illustrations.iluClock2.image)
        XCTAssertEqual(viewControllerSpy.viewModel?.title, Strings.Feedback.PortabilityDonorInProgress.title)
        XCTAssertEqual(viewControllerSpy.viewModel?.description, Strings.Feedback.PortabilityDonorInProgress.description)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.count, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.title, Strings.General.gotIt)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.action, .close)
        XCTAssertEqual(viewControllerSpy.displayNotificationCallsCount, 0)
        XCTAssertNil(viewControllerSpy.viewModel?.notification)
    }
    
    func testPresentData_WhenViewTypeIsPortabilityDonorReceived_ShouldDisplayPortabilityDonorReceivedData() {
        sut.presentData(with: .portabilityDonorReceived(uuid: "uuid"))
        
        XCTAssertEqual(viewControllerSpy.displayViewModelCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.image, Resources.Illustrations.iluEmail.image)
        XCTAssertEqual(viewControllerSpy.viewModel?.title, Strings.Feedback.PortabilityDonorReceived.title)
        XCTAssertEqual(viewControllerSpy.viewModel?.description, Strings.Feedback.PortabilityDonorReceived.description)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.count, 2)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.title, Strings.General.continuePixKey)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.action, .ignore)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.last?.title, Strings.Feedback.grantPortability)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.last?.action, .grantPortability)
        XCTAssertEqual(viewControllerSpy.displayNotificationCallsCount, 0)
        XCTAssertNil(viewControllerSpy.viewModel?.notification)
    }
    
    func testPresentData_WhenViewTypeIsPortabilityDonorRefused_ShouldDisplayPortabilityDonorRefusedData() {
        sut.presentData(with: .portabilityDonorRefused(uuid: "uuid"))
        
        XCTAssertEqual(viewControllerSpy.displayViewModelCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.image, Resources.Illustrations.iluPeople.image)
        XCTAssertEqual(viewControllerSpy.viewModel?.title, Strings.Feedback.PortabilityDonorRefused.title)
        XCTAssertEqual(viewControllerSpy.viewModel?.description, Strings.Feedback.PortabilityDonorRefused.description)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.count, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.title, Strings.General.gotIt)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.action, .close)
        XCTAssertEqual(viewControllerSpy.displayNotificationCallsCount, 0)
        XCTAssertNil(viewControllerSpy.viewModel?.notification)
    }
    
    func testPresentData_WhenViewTypeIsPortabilityInProgress_ShouldDisplayPortabilityInProgressData() {
        sut.presentData(with: .portabilityInProgress(uuid: "uuid"))
        
        XCTAssertEqual(viewControllerSpy.displayViewModelCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.image, Resources.Illustrations.iluEmail.image)
        XCTAssertEqual(viewControllerSpy.viewModel?.title, Strings.Feedback.PortabilityInProgress.title)
        XCTAssertEqual(viewControllerSpy.viewModel?.description, Strings.Feedback.PortabilityInProgress.description)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.count, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.title, Strings.General.gotIt)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.action, .close)
        XCTAssertEqual(viewControllerSpy.displayNotificationCallsCount, 0)
        XCTAssertNil(viewControllerSpy.viewModel?.notification)
    }
    
    func testPresentData_WhenViewTypeIsPortabilityRefused_ShouldDisplayPortabilityRefusedData() {
        sut.presentData(with: .portabilityRefused(uuid: "uuid"))
        
        XCTAssertEqual(viewControllerSpy.displayViewModelCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.image, Resources.Illustrations.iluPeople.image)
        XCTAssertEqual(viewControllerSpy.viewModel?.title, Strings.Feedback.PortabilityRefused.title)
        XCTAssertEqual(viewControllerSpy.viewModel?.description, Strings.Feedback.PortabilityRefused.description)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.count, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.title, Strings.General.tryAgain)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.action, .tryAgain)
        XCTAssertEqual(viewControllerSpy.displayNotificationCallsCount, 0)
        XCTAssertNil(viewControllerSpy.viewModel?.notification)
    }
    
    func testPresentData_WhenViewTypeIsIdentityValidationCompleted_ShouldDisplayIdentityValidationCompletedData() {
        sut.presentData(with: .identityValidationCompleted)
        
        XCTAssertEqual(viewControllerSpy.displayViewModelCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.image, Resources.Illustrations.iluSuccess.image)
        XCTAssertEqual(viewControllerSpy.viewModel?.title, Strings.Feedback.IdentityValidationCompleted.title)
        XCTAssertEqual(viewControllerSpy.viewModel?.description, Strings.Feedback.IdentityValidationCompleted.description)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.count, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.title, Strings.General.continue)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.action, .close)
        XCTAssertEqual(viewControllerSpy.displayNotificationCallsCount, 0)
        XCTAssertNil(viewControllerSpy.viewModel?.notification)
    }
    
    func testPresentData_WhenViewTypeIsIdentityValidationNotCompleted_ShouldDisplayIdentityValidationNotCompletedData() {
        sut.presentData(with: .identityValidationNotCompleted)
        
        XCTAssertEqual(viewControllerSpy.displayViewModelCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.image, Resources.Illustrations.iluError.image)
        XCTAssertEqual(viewControllerSpy.viewModel?.title, Strings.Feedback.IdentityValidationNotCompleted.title)
        XCTAssertEqual(viewControllerSpy.viewModel?.description, Strings.Feedback.IdentityValidationNotCompleted.description)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.count, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.title, Strings.General.gotIt)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.action, .close)
        XCTAssertEqual(viewControllerSpy.displayNotificationCallsCount, 0)
        XCTAssertNil(viewControllerSpy.viewModel?.notification)
    }
    
    func testPresentData_WhenViewTypeIsIdentityValidationInReview_ShouldDisplayIdentityValidationInReviewData() {
        sut.presentData(with: .identityValidationInReview)
        
        XCTAssertEqual(viewControllerSpy.displayViewModelCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.image, Resources.Illustrations.iluWarning.image)
        XCTAssertEqual(viewControllerSpy.viewModel?.title, Strings.Feedback.IdentityValidationInReview.title)
        XCTAssertEqual(viewControllerSpy.viewModel?.description, Strings.Feedback.IdentityValidationInReview.description)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.count, 1)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.title, Strings.General.gotIt)
        XCTAssertEqual(viewControllerSpy.viewModel?.buttons.first?.action, .close)
        XCTAssertEqual(viewControllerSpy.displayNotificationCallsCount, 0)
        XCTAssertNil(viewControllerSpy.viewModel?.notification)
    }
    
    func testExecute_WhenActionIsAllowClaim_ShouldPassActionToCoordinator() {
        sut.execute(action: .allowClaim)
        
        XCTAssertEqual(coordinatorSpy.executeCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .allowClaim)
    }
    
    func testExecute_WhenActionIsClose_ShouldPassActionToCoordinator() {
        sut.execute(action: .close)
        
        XCTAssertEqual(coordinatorSpy.executeCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .close)
    }
    
    func testExecute_WhenActionIsGrantPortability_ShouldPassActionToCoordinator() {
        sut.execute(action: .grantPortability)
        
        XCTAssertEqual(coordinatorSpy.executeCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .grantPortability)
    }
    
    func testExecute_WhenActionIsIgnore_ShouldPassActionToCoordinator() {
        sut.execute(action: .ignore)
        
        XCTAssertEqual(coordinatorSpy.executeCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .ignore)
    }
    
    func testExecute_WhenActionIsRevalidateKey_ShouldPassActionToCoordinator() {
        sut.execute(action: .revalidateKey)
        
        XCTAssertEqual(coordinatorSpy.executeCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .revalidateKey)
    }
    
    func testExecute_WhenActionIsTryAgain_ShouldPassActionToCoordinator() {
        sut.execute(action: .tryAgain)
        
        XCTAssertEqual(coordinatorSpy.executeCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.action, .tryAgain)
    }
}
