import FeatureFlag
@testable import PIX
import UI
import XCTest

private final class SecurityCodeValidationCoordinatorSpy: SecurityCodeValidationCoordinating {
    var viewController: UIViewController?
    
    // MARK: - performAction
    private(set) var displayPerformActionCallsCount = 0
    private(set) var action: SecurityCodeValidationAction?
    
    func perform(action: SecurityCodeValidationAction) {
        displayPerformActionCallsCount += 1
        self.action = action
    }
}

private final class SecurityCodeValidationViewDisplayingSpy: SecurityCodeValidationDisplaying {
    // MARK: - display
    private(set) var displayMessageCallsCount = 0
    private(set) var message: NSAttributedString?

    func display(message: NSAttributedString) {
        displayMessageCallsCount += 1
        self.message = message
    }

    // MARK: - updateResendCodeButton
    private(set) var updateResendCodeButtonCallsCount = 0
    private(set) var isResendButtonHidden: Bool?
    private(set) var isResendButtonEnabled: Bool?
    private(set) var resendButtonTitle: String?

    func updateResendCodeButton(isHidden: Bool, isEnabled: Bool, title: String?) {
        updateResendCodeButtonCallsCount += 1
        isResendButtonHidden = isHidden
        isResendButtonEnabled = isEnabled
        resendButtonTitle = title
    }

    // MARK: - showLoading
    private(set) var showLoadingCallsCount = 0

    func showLoading() {
        showLoadingCallsCount += 1
    }

    // MARK: - hideLoading
    private(set) var hideLoadingCallsCount = 0

    func hideLoading() {
        hideLoadingCallsCount += 1
    }

    // MARK: - displayGeneralError
    private(set) var displayGeneralErrorCallsCount = 0
    private(set) var generalErrortitle: String?
    private(set) var generalErrorMessage: String?

    func displayGeneralError(title: String, message: String) {
        displayGeneralErrorCallsCount += 1
        generalErrortitle = title
        generalErrorMessage = message
    }

    // MARK: - displayCodeError
    private(set) var displayCodeErrorCallsCount = 0
    private(set) var codeErrorMessage: String?

    func displayCodeError(message: String) {
        displayCodeErrorCallsCount += 1
        codeErrorMessage = message
    }

    // MARK: - hideCodeErrorMessage
    private(set) var hideCodeErrorCallsCount = 0

    func hideCodeErrorMessage() {
        hideCodeErrorCallsCount += 1
    }

    // MARK: - cleanCodeTextField
    private(set) var cleanCodeTextFieldCallsCount = 0

    func cleanCodeTextField() {
        cleanCodeTextFieldCallsCount += 1
    }

    // MARK: - animateResendSuccess
    private(set) var animateResendSuccessCallsCount = 0

    func animateResendSuccess() {
        animateResendSuccessCallsCount += 1
    }
}

final class SecurityCodeValidationPresenterTests: XCTestCase {
    private let viewControllerSpy = SecurityCodeValidationViewDisplayingSpy()
    private let coordinatorSpy = SecurityCodeValidationCoordinatorSpy()
    private let featureManagerMock = FeatureManagerMock()
    
    private lazy var sut: SecurityCodeValidationPresenter = {
        let presenter = SecurityCodeValidationPresenter(
            coordinator: coordinatorSpy,
            dependencies: DependencyContainerMock(featureManagerMock)
        )
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testPopulateView_WhenInputTypeIsPhoneAndOfuscationIsEnabledShouldPopulateViews() throws {
        featureManagerMock.override(key: .featureAppSecObfuscationMask, with: true)
        let inputType = SecurityCodeValidationType.phone
        let mockedPhoneNumber = "(29) 94534-7453"
            
        sut.populateView(for: inputType, inputedValue: mockedPhoneNumber)
        
        let expectedMessage = "Insira o código que enviamos para o seu telefone (**) *****-7453, no campo abaixo, para validar seu número."
        XCTAssertEqual(viewControllerSpy.message?.string, expectedMessage)
        XCTAssertEqual(viewControllerSpy.displayMessageCallsCount, 1)
    }
    
    func testPopulateView_WhenInputTypeIsPhoneAndOfuscationIsDisabled_ShouldPopulateViews() throws {
        featureManagerMock.override(key: .featureAppSecObfuscationMask, with: false)
        let mockedPhoneNumber = "(29) 94534-7453"

        sut.populateView(for: .phone, inputedValue: mockedPhoneNumber)

        let expectedMessage = "Insira o código que enviamos para o seu telefone (29) 94534-7453, no campo abaixo, para validar seu número."
        XCTAssertEqual(viewControllerSpy.message?.string, expectedMessage)
        XCTAssertEqual(viewControllerSpy.displayMessageCallsCount, 1)
    }

    func testPopulateView_WhenInputTypeIsEmailAndOfuscationIsEnabled_ShouldPopulateViews() throws {
        featureManagerMock.override(key: .featureAppSecObfuscationMask, with: true)
        let mockedEmail = "joao.almeida@gmail.com"
        
        sut.populateView(for: .email, inputedValue: mockedEmail)
        
        let expectedMessage = "Insira o código que enviamos para j**********a@g***l.com, no campo abaixo, para validar seu e-mail."
        XCTAssertEqual(viewControllerSpy.message?.string, expectedMessage)
        XCTAssertEqual(viewControllerSpy.displayMessageCallsCount, 1)
    }

    func testPopulateView_WhenInputTypeIsEmailAndOfuscationIsDisabled_ShouldPopulateViews() throws {
        featureManagerMock.override(key: .featureAppSecObfuscationMask, with: false)
        let mockedEmail = "joao.almeida@gmail.com"

        sut.populateView(for: .email, inputedValue: mockedEmail)

        let expectedMessage = "Insira o código que enviamos para joao.almeida@gmail.com, no campo abaixo, para validar seu e-mail."
        XCTAssertEqual(viewControllerSpy.message?.string, expectedMessage)
        XCTAssertEqual(viewControllerSpy.displayMessageCallsCount, 1)
    }

    func testUpdateResendCodeButton_WhenIsPhoneTypeAndStateIsEnabled_ShouldEnableResendButton() {
        let inputType = SecurityCodeValidationType.phone
        sut.updateResendCodeButton(state: .enabled, inputType: inputType)

        XCTAssertEqual(viewControllerSpy.updateResendCodeButtonCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.isResendButtonHidden, false)
        XCTAssertEqual(viewControllerSpy.isResendButtonEnabled, true)
        XCTAssertEqual(viewControllerSpy.resendButtonTitle , Strings.CodeValidation.resentCode)
    }

    func testUpdateResendCodeButton_WhenIsPhoneAndStateIsDisabled_ShouldDisableResendButton() {
        let inputType = SecurityCodeValidationType.phone
        sut.updateResendCodeButton(state: .disabled(seconds: 60), inputType: inputType)

        XCTAssertEqual(viewControllerSpy.updateResendCodeButtonCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.isResendButtonHidden, false)
        XCTAssertEqual(viewControllerSpy.isResendButtonEnabled, false)
        XCTAssertEqual(viewControllerSpy.resendButtonTitle , Strings.CodeValidation.requestNewCode("01:00"))
    }

    func testUpdateResendCodeButton_WhenIsPhoneAndStateIsHidden_ShouldHideResendButton() {
        let inputType = SecurityCodeValidationType.phone
        sut.updateResendCodeButton(state: .hidden, inputType: inputType)

        XCTAssertEqual(viewControllerSpy.updateResendCodeButtonCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.isResendButtonHidden, true)
    }
    
    func testUpdateResendCodeButton_WhenIsEmailTypeAndStateIsEnabled_ShouldEnableResendButton() {
        let inputType = SecurityCodeValidationType.email
        sut.updateResendCodeButton(state: .enabled, inputType: inputType)

        XCTAssertEqual(viewControllerSpy.updateResendCodeButtonCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.isResendButtonHidden, false)
        XCTAssertEqual(viewControllerSpy.isResendButtonEnabled, true)
        XCTAssertEqual(viewControllerSpy.resendButtonTitle , Strings.CodeValidation.resentCode)
    }

    func testUpdateResendCodeButton_WhenIsEmailAndStateIsDisabled_ShouldDisableResendButton() {
        let inputType = SecurityCodeValidationType.email
        sut.updateResendCodeButton(state: .disabled(seconds: 60), inputType: inputType)

        XCTAssertEqual(viewControllerSpy.updateResendCodeButtonCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.isResendButtonHidden, false)
        XCTAssertEqual(viewControllerSpy.isResendButtonEnabled, false)
        XCTAssertEqual(viewControllerSpy.resendButtonTitle , Strings.CodeValidation.requestNewCode("01:00"))
    }

    func testUpdateResendCodeButton_WhenIsEmailAndStateIsHidden_ShouldHideResendButton() {
        let inputType = SecurityCodeValidationType.email
        sut.updateResendCodeButton(state: .hidden, inputType: inputType)

        XCTAssertEqual(viewControllerSpy.updateResendCodeButtonCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.isResendButtonHidden, true)
    }
    
    func testDidNextStep_WhenActionIsNextScene_ShouldPerformNextSceneAction() {
        let mockedEmail = "joao.almeida@gmail.com"
        sut.didNextStep(action: .successfulValidation(for: .email, inputValue: mockedEmail))

        XCTAssertEqual(coordinatorSpy.action, .successfulValidation(for: .email, inputValue: mockedEmail))
    }

    func testPresentGeneralError_ShouldDisplayGeneralError() {
        let title = "Ops!"
        let message = "Erro de rede"

        sut.presentGeneralError(title: title, message: message)

        XCTAssertEqual(viewControllerSpy.displayGeneralErrorCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.generalErrortitle, title)
        XCTAssertEqual(viewControllerSpy.generalErrorMessage, message)
    }
    
    func testPresentCodeError_ShouldDisplayCodeError() {
        let message = "Código inválido"

        sut.presentCodeError(message: message)

        XCTAssertEqual(viewControllerSpy.displayCodeErrorCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.codeErrorMessage, message)
    }
    
    func testHideCodeError_ShouldHideCodeError() {
        sut.hideCodeError()
        
        XCTAssertEqual(viewControllerSpy.hideCodeErrorCallsCount, 1)
    }
    
    func testShowLoading_ShouldShowLoading() {
        sut.showLoading()

        XCTAssertEqual(viewControllerSpy.showLoadingCallsCount, 1)
    }

    func testHideLoading_ShouldHideLoading() {
        sut.hideLoading()

        XCTAssertEqual(viewControllerSpy.hideLoadingCallsCount, 1)
    }
    
    func testCleanCodeTextField_ShouldCleanCodeTextField() {
        sut.cleanCodeTextField()
        
        XCTAssertEqual(viewControllerSpy.cleanCodeTextFieldCallsCount, 1)
    }
    
    func testAnimateResendSucccess_ShouldAnimateResendSuccess() {
        sut.animateResendSuccess()
        
        XCTAssertEqual(viewControllerSpy.animateResendSuccessCallsCount, 1)
    }
}
